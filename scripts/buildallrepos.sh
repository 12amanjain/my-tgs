cd ../tgs-libs
git reset --hard
git pull origin master
cd ../tgs-data-model/
git stash
git pull origin master
git stash pop
cd ../tgs-rest-model/
git stash
git pull origin master
git stash pop
cd ../tgs-java-utils
git stash
git pull origin master
git stash pop
cd ../tgs-java-based-services/
git stash
git pull origin master
git stash pop
cd ../tgs-data-model/
gradle build
cd ../tgs-rest-model/
gradle build
cd ../tgs-java-utils
gradle build
cd ../tgs-java-based-services/
gradle build
