package com.tgs.services.accounting.analytics;

import com.tgs.services.accounting.datamodel.AccountingAnalyticsType;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.BaseAnalyticsQuery;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import lombok.extern.slf4j.Slf4j;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class AccountingAnalyticsHelper {

	@Autowired
	ElasticSearchCommunicator esStackCommunicator;

	@Autowired
	KafkaServiceCommunicator kafkaService;

	public <T extends BaseAnalyticsQuery> void pushToAnalytics(Mapper<T> mapper, AccountingAnalyticsType accountingAnalyticsType,
			ESMetaInfo metaInfo) {
		ExecutorUtils.getAnalyticsThreadPool().submit(() -> {
			T tripQuery = null;
			try {
				tripQuery = mapper.convert();
				tripQuery.setAnalyticstype(accountingAnalyticsType.name());
				QueueData queueData = QueueData.builder().key(metaInfo.getIndex())
						.value(GsonUtils.getGson().toJson(tripQuery)).build();
				List<QueueDataType> queueTypes = Arrays.asList(QueueDataType.ELASTICSEARCH);
				kafkaService.queue(queueTypes, queueData);
			} catch (Exception e) {
				log.error("Failed to Push Analytics type {} ", accountingAnalyticsType.name(), e);
			}
		});
	}

}