package com.tgs.services.accounting.analytics;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OrderToAnalyticsOrderMapper extends Mapper<AnalyticsOrderQuery> {
	
	private User user;
	private ContextData contextData;
	private String bookingId;
	private String pnr;
	private AirAnalyticsType analyticsType;

	@Override
	protected void execute() throws CustomGeneralException {
		output = output != null ? output : AnalyticsOrderQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output.setBookingId(bookingId);
		output.setPnr(pnr);
	}

}
