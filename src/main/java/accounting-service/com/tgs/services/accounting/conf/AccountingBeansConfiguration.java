package com.tgs.services.accounting.conf;

import java.time.LocalDateTime;

import com.tgs.services.base.ruleengine.GeneralBasicFact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.InvoiceIdConfiguration;
import com.tgs.services.gms.datamodel.InvoiceIdManagerType;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.manager.DefaultInvoiceIdManager;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class AccountingBeansConfiguration {

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private HotelOrderItemService hotelOrderItemService;

	@Autowired
	private UserServiceCommunicator userCommunicator;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	@Bean
	protected AbstractInvoiceIdManager invoiceIdManager() {
		InvoiceIdConfiguration invoiceIdConfiguration = getInvoiceIdConfiguration();
		InvoiceIdManagerType invoiceIdManagerType = getInvoiceIdManagerType(invoiceIdConfiguration);
		log.debug("invoiceIdManagerType in config is {}", invoiceIdManagerType);
		if (invoiceIdManagerType == null) {
			invoiceIdManagerType = InvoiceIdManagerType.DEFAULT;
		}
		switch (invoiceIdManagerType) {
			case DEFAULT:
			default:
				log.info("Using DefaultInvoiceIdManager");
				return DefaultInvoiceIdManager.builder().airOrderItemService(airOrderItemService)
						.hotelOrderItemService(hotelOrderItemService).userCommunicator(userCommunicator)
						.invoiceIdConfiguration(invoiceIdConfiguration).build();
		}
	}

	private InvoiceIdConfiguration getInvoiceIdConfiguration() {
		GeneralBasicFact generalBasicFact = GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build();
		ClientGeneralInfo clientGeneralInfo =
				gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, generalBasicFact);
		if (clientGeneralInfo != null) {
			return clientGeneralInfo.getInvoiceIdConfiguration();
		}
		return null;
	}

	private InvoiceIdManagerType getInvoiceIdManagerType(InvoiceIdConfiguration invoiceIdConfiguration) {
		if (invoiceIdConfiguration == null) {
			return null;
		}
		return invoiceIdConfiguration.getInvoiceIdManagerType();
	}

}
