package com.tgs.services.accounting.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import lombok.Getter;

@Getter
@Component
public class AtlasHotelAccountingDbConfiguration {
	@Value("${hotel.account.atlas.database.server}")
	private String serverAddress;

	@Value("${hotel.account.atlas.database.name}")
	private String databaseName;

	@Value("${hotel.account.atlas.database.username}")
	private String username;

	@Value("${hotel.account.atlas.database.password}")
	private String password;

	@Value("${hotel.account.atlas.database.table}")
	private String tableName;
}
