package com.tgs.services.accounting.datamodel;

import java.util.StringJoiner;
import lombok.Getter;

@Getter
public enum OrderColumn {
	DOC_PRF(2, true),
	DOC_NOS(7, true),
	DOC_SRNO(3, true),
	IDM_FLAG(1),
	IL_REF(19),
	CCODE(6),
	DCODE(6),
	ECODE(6),
	BCODE(6),
	NARRATION(35),
	XO_REF(1),
	LOC_CODE(3),
	CST_CODE(3),
	Curcode_C(3),
	Curcode_S(3),
	ACODE(6),
	SCODE(6),
	XO_NOS(9),
	PNR_NO(8),
	TICKETNO(11),
	PAX(30),
	SECTOR(19),
	CRS_ID(2),
	FARE_BASIS(10),
	DEAL_CODE(15),
	NOS_PAX_A,
	NOS_PAX_C,
	NOS_PAX_I,
	FLT_DTLS1(18),
	FLT_DTLS2(18),
	FLT_DTLS3(18),
	FLT_DTLS4(18),
	R_O_E_C,
	R_O_E_S,
	BASIC_PBL,
	BASIC_FARE,
	TAX_1,
	TAX_2,
	TAX_3,
	TAX_4,
	TAX_5,
	TAX_6,
	DISC_PAIDM1(2),
	DISC_PAIDM2(3),
	DISC_PAIDM3(3),
	DISC_RECDM1(2),
	DISC_RECDM2(3),
	DISC_RECDM3(3),
	BROK_PAIDM1(2),
	DISC_PAIDV1,
	DISC_PAIDV2,
	DISC_PAIDV3,
	DISC_RECDV1,
	DISC_RECDV2,
	DISC_RECDV3,
	BROK_PAIDV1,
	DISC_PAID1,
	DISC_PAID2,
	DISC_PAID3,
	DISC_RECD1,
	DISC_RECD2,
	DISC_RECD3,
	BROK_PAID1,
	SRV_CHRG1C,
	SRV_CHRG2C,
	SRV_CHRG3C,
	SRV_CHRG4C,
	SRV_CHRG5C,
	RAF_C,
	SRV_CHRG1P,
	SRV_CHRG2P,
	SRV_CHRG3P,
	SRV_CHRG4P,
	SRV_CHRG5P,
	RAF_P,
	SERV_TAXC,
	SERV_EDUC,
	SERV_CS1C,
	TDC_PAIDV1,
	TDS_C,
	SERV_TAXP,
	SERV_EDUP,
	TDS_PAIDV1,
	TDS_P,
	TDB_PAIDV1,
	TDS_B,
	Created_By(15),
	Pay_Type(1),
	SCODE_B(6),
	XXL_C,
	XXL_P,
	SERV1_TAXC,
	SERV1_EDUC,
	SERV1_CS1C,
	SERV3_TAXC,
	SERV3_EDUC,
	SERV3_CS1C,
	SERV3_TAXP,
	SERV3_EDUP,
	Stax_PayBy(1),
	TDC_Index,
	DISC_PAIDM5(3),
	DISC_PAIDV5,
	DISC_PAID5,
	DISC_RECDM5(3),
	DISC_RECDV5,
	DISC_RECD5,
	SRV_CHRG2_H(1),
	IDATE,
	REFR_KEY(10),
	SRV_CHRG1_H(1),
	SRV_CHRG3_H(1),
	VD_REF(19),
	SaleDATE,
	Created_On,
	SERV_CS1P,
	SERV3_CS1P,
	SERV1_TAXP,
	SERV1_EDUP,
	SERV1_CS1P,
	SERV_CS2C,
	SERV1_CS2C,
	SERV2_CS1C,
	SERV2_CS2C,
	SERV3_CS2C,
	SERV_CS2P,
	SERV1_CS2P,
	SERV3_CS2P,
	NARRATION_5,
	NARRATION_6,
	GST_TYPE,
	GST_CalcOn,
	SAC_CODE1;

	// All columns separated by comma
	public final static String ALL;

	private int length;
	private boolean notNull;

	static {
		StringJoiner sj = new StringJoiner(", ");
		for (OrderColumn column : OrderColumn.values()) {
			sj.add(column.name());
		}
		ALL = sj.toString();
	}

	OrderColumn() {
		this(-1);
	}

	OrderColumn(int maxLength) {
		this(maxLength, false);
	}

	OrderColumn(boolean notNull) {
		this(-1, notNull);
	}

	OrderColumn(int length, boolean notNull) {
		this.length = length;
		this.notNull = notNull;
	}

	public boolean hasLength() {
		return length >= 0;
	}

}

