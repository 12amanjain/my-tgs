package com.tgs.services.accounting.datamodel;

import java.util.StringJoiner;
import lombok.Getter;

@Getter
public enum UserColumn {

	COCODE,
	GCODE,
	GNAME(50),
	GNAME_1,
	NAVCODE,
	ADDRESS1,
	ADDRESS2,
	ADDRESS3,
	TELNO,
	FAXNO,
	MOBILENO_1,
	MOBILENO_2,
	EMAILNO,
	EMAILNO_1,
	CONPER,
	C_ITNO_1,
	C_ITNO_2,
	C_MESG_1,
	C_MESG_2,
	C_LOGINNAME,
	C_LOGINPWD,
	STATUS_DL,
	CREATED_BY,
	CREATED_ON,
	AGENCY_ID,
	GST_STATE,
	GST_NUMBER,
	CATG_CODE,
	FAML_CODE,
	SALM_CODE,
	COLL_CODE,
	DISC_CODE,
	CITY_CODE,
	CREDIT_LIMIT,
	LOCK_BILL,
	CHRG_TDS,
	PRINT_SC1,
	PRINT_SC2,
	PRINT_SC3;

	// All columns separated by comma
	public final static String ALL;

	private int length;

	static {
		StringJoiner sj = new StringJoiner(", ");
		for (UserColumn column : UserColumn.values()) {
			sj.add(column.name());
		}
		ALL = sj.toString();
	}

	UserColumn() {
		this(-1);
	}

	UserColumn(int maxLength) {
		this.length = maxLength;
	}

	public boolean hasLength() {
		return length >= 0;
	}
}
