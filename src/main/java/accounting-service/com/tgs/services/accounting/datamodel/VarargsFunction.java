package com.tgs.services.accounting.datamodel;

public interface VarargsFunction<T, R> {
	
	@SuppressWarnings("unchecked")
	R apply(T... args);
}
