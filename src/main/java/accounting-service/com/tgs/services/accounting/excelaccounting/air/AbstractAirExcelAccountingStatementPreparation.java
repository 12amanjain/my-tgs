package com.tgs.services.accounting.excelaccounting.air;


import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;

public abstract class AbstractAirExcelAccountingStatementPreparation {

	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + OrderColumn.ALL + ") VALUES (%s)";

	/**
	 * 
	 * @param order
	 * @param airOrderItems with same PNR, for same supplier
	 * @param paxSerialNo Serial number of each entry/sql (starting from 1)
	 * @throws SQLException
	 */
	public String prepareInsertSql(String tableName, DbOrder order, List<DbAirOrderItem> airOrderItems, int serialNo,
			int pnrCountAlreadyPushed) throws SQLException {
		Map<OrderColumn, CharSequence> statementParams =
				getColumnValues(order, airOrderItems, serialNo, pnrCountAlreadyPushed);
		StringJoiner joiner = new StringJoiner(", ");
		for (OrderColumn column : OrderColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}


	protected abstract Map<OrderColumn, CharSequence> getColumnValues(DbOrder order, List<DbAirOrderItem> airOrderItems,
			int serialNo, int pnrCountAlreadyPushed);


}
