package com.tgs.services.accounting.excelaccounting.air;

import java.sql.Connection;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import com.tgs.services.accounting.analytics.AccountingAnalyticsHelper;
import com.tgs.services.accounting.analytics.OrderToAnalyticsOrderMapper;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.datamodel.AccountingAnalyticsType;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.air.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.utils.common.SimpleCache;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class AirExcelAccounting {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	private ElasticSearchCommunicator esSearchComm;

	@Autowired
	private AbstractAirExcelAccountingStatementPreparation statementPreparation;

	@Autowired
	KafkaServiceCommunicator kafkaService;

	@Autowired
	private AccountingAnalyticsHelper analyticsHelper;

	public void pushInvoicesFor(OrderFilter orderFilter) {
		if (orderFilter == null) {
			return;
		}
		Connection con = null;
		Statement statement = null;
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			List<Object[]> orderObjectsList = airOrderItemService.findByJsonSearch(orderFilter);
			Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = groupOrderItems(orderObjectsList);
			List<DbOrder> ordersInBatch = new ArrayList<>();
			Map<String, List<String>> sqlsLogs = new HashMap<>();

			Iterator<Entry<DbOrder, List<DbAirOrderItem>>> orderEntryIterator = orderItemsGroup.entrySet().iterator();

			while (orderEntryIterator.hasNext()) {
				try {
					Entry<DbOrder, List<DbAirOrderItem>> orderEntry = orderEntryIterator.next();
					DbOrder order = orderEntry.getKey();
					List<DbAirOrderItem> items = orderEntry.getValue();
					if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getInvoicePushStatus())) {
						List<String> sqlList = createSqlsFor(order, items);
						sqlsLogs.put(order.getBookingId(), sqlList);
						if (!CollectionUtils.isEmpty(sqlList)) {
							for (String sql : sqlList) {
								statement.addBatch(sql);
								ordersInBatch.add(order);
							}
						}
					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last order.
					 */
					if (ordersInBatch.size() >= BATCH_SIZE || !orderEntryIterator.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = false;
							log.error("Failed to insert invoice for bookingIds {} due to ", sqlsLogs.keySet(), e);
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "AirAccounting", e);
							}
						} finally {
							/**
							 * Batch should be cleared even if it fails so that upcoming batches can be served.
							 */
							statement.clearBatch();
							for (DbOrder order_local : ordersInBatch) {
								order_local.getAdditionalInfo().setInvoicePushStatus(status);
								if (status) {
									addOrderToEsStack(order);
								}
							}
							orderService.save(ordersInBatch);
							ordersInBatch.clear();
						}
						accountingUtils.logSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (Exception e) {
					log.error(
							"Failed to insert invoice for bookingIds {}. Trying to build connections again. Reason: {} ",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "AirAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}
			}
		} catch (Exception e) {
			log.error("Failed to insert invoice for orderFilter {} due to {}", orderFilter, e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}

	private Map<DbOrder, List<DbAirOrderItem>> groupOrderItems(List<Object[]> orderObjectsList) {
		Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = new HashMap<>();
		List<DbAirOrderItem> orderItems = null;
		DbOrder previousOrder = null;
		SimpleCache<String, DbOrder> orderCache = new SimpleCache<String, DbOrder>() {
			@Override
			protected String getKey(DbOrder value) {
				return value.getBookingId();
			}
		};
		for (Object[] orderObjects : orderObjectsList) {
			DbAirOrderItem orderItem = (DbAirOrderItem) orderObjects[0];
			DbOrder order = (DbOrder) orderObjects[1];
			order = orderCache.getCached(order);
			if (!order.equals(previousOrder)) {
				if (previousOrder != null) {
					orderItemsGroup.put(previousOrder, orderItems);
				}
				orderItems = new ArrayList<>();
			}
			orderItems.add(orderItem);
			previousOrder = order;
		}
		if (previousOrder != null) {
			orderItemsGroup.put(previousOrder, orderItems);
		}
		return orderItemsGroup;
	}

	/**
	 * Inserts invoice(s) for given {@code order}.<br>
	 * <br>
	 * Items in the list are grouped by supplier and PNR. Each such group is a list of {@code AirOrderItem} which
	 * contains travellerInfos for segments with same PNR obtained by using same supplier. For each traveller, flight
	 * details and pricing information for such segments, and traveller details are inserted in database.
	 * 
	 * @param order
	 * @param airOrderItems
	 * @param statementPreparation
	 * @return {@code List} of SQLs if successful, otherwise {@code null}.
	 */
	private List<String> createSqlsFor(DbOrder order, List<DbAirOrderItem> airOrderItems) {
		if (order == null) {
			return null;
		}
		List<String> sqlList = new ArrayList<>();
		try {
			final int travellersCount = airOrderItems.get(0).getTravellerInfo().size();
			// grouping by PNR
			Map<String, List<DbAirOrderItem>> pnrWiseAirOrderItems = airOrderItems.stream()
					.collect(Collectors.groupingBy(orderItem -> orderItem.getTravellerInfo().get(0).getPnr()));

			int srno = 1;
			int pnrCountAlreadyPushed = 0;
			for (List<DbAirOrderItem> perPnrOrderItemList : pnrWiseAirOrderItems.values()) {
				// Check only for inventory bookings
				// Source id can be null for Re issued carts
				if (perPnrOrderItemList.get(0).getAdditionalInfo().getSourceId() != null
						&& perPnrOrderItemList.get(0).getAdditionalInfo().getSourceId() == 11) {
					ClientGeneralInfo clientInfo = accountingUtils.getClientInfo();
					String pnr = perPnrOrderItemList.get(0).getTravellerInfo().get(0).getPnr();
					if (BooleanUtils.isTrue(clientInfo.getIsAccountingAnalyticsEnabled())) { 
						ESAutoSuggestionResponse autoSuggestionResponse = getSuggestions(pnr, ESMetaInfo.ORDER.name());
						pnrCountAlreadyPushed = autoSuggestionResponse.getSuggestions().size();
					} else {
						AirOrderItemFilter airOrderFilter = AirOrderItemFilter.builder()
								.travellerInfoFilter(TravellerInfoFilter.builder().pnr(pnr).build()).build();
						OrderFilter filter = OrderFilter.builder().products(Arrays.asList(OrderType.AIR))
								.isPushedToAccounting(true).createdOnAfterDate(LocalDate.now().minusMonths(5))
								.itemFilter(airOrderFilter).build();
						List<Object[]> orderObjectsList = airOrderItemService.findByJsonSearch(filter);
						Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = groupOrderItems(orderObjectsList);
						pnrCountAlreadyPushed = orderItemsGroup.size();
					}
				}
				for (int i = 0; i < travellersCount; i++) {
					sqlList.add(statementPreparation.prepareInsertSql(dbConf.getTableName(), order, perPnrOrderItemList,
							srno, pnrCountAlreadyPushed));
					srno++;
				}	
			}
		} catch (Exception e) {
			log.error("Failed to prepare invoice statements for bookingId {} due to {} ", order.getBookingId(),
					e.getMessage(), e);
			LogUtils.log(order.getBookingId(), "AirAccounting", e);
			return null;
		}
		return sqlList;
	}

	public ESAutoSuggestionResponse getSuggestions(String searchKey, String metaIndex) {
		ESSearchRequest searchRequest = ESSearchRequest.builder().source(searchKey).metaInfo(metaIndex).build();
		return esSearchComm.getSuggestions(searchRequest);
	}

	private void addOrderToEsStack(DbOrder order) {
		ClientGeneralInfo clientInfo = accountingUtils.getClientInfo();
		if (BooleanUtils.isTrue(clientInfo.getIsAccountingAnalyticsEnabled()) && order != null) {
			List<DbAirOrderItem> items = airOrderItemService.findByBookingId(order.getBookingId());
			Map<String, List<DbAirOrderItem>> pnrWiseAirOrderItems = items.stream()
					.collect(Collectors.groupingBy(orderItem -> orderItem.getTravellerInfo().get(0).getPnr()));
			for (Map.Entry<String, List<DbAirOrderItem>> entry : pnrWiseAirOrderItems.entrySet()) {
				addOrderToESSTack(entry.getValue().get(0), entry.getKey(), AccountingAnalyticsType.ORDER);
			}
				
		}
	}

	public void addOrderToESSTack(DbAirOrderItem order, String pnr,
			AccountingAnalyticsType analyticsType) {
		try {
			if (order != null && pnr != null) {
				OrderToAnalyticsOrderMapper queryMapper = OrderToAnalyticsOrderMapper.builder()
						.bookingId(order.getBookingId()).pnr(pnr).build();
				analyticsHelper.pushToAnalytics(queryMapper, analyticsType, ESMetaInfo.ORDER);
			}
		} catch (Exception e) {
			log.error("Failed to Push Order to ESStack", e);
		}
	}

	public void pushInvoicestoESStack(OrderFilter orderFilter) {
		List<Object[]> orderObjectsList = airOrderItemService.findByJsonSearch(orderFilter);
		Map<DbOrder, List<DbAirOrderItem>> orderItemsGroup = groupOrderItems(orderObjectsList);
		Iterator<Entry<DbOrder, List<DbAirOrderItem>>> orderEntryIterator = orderItemsGroup.entrySet().iterator();
		while (orderEntryIterator.hasNext()) {
			Entry<DbOrder, List<DbAirOrderItem>> orderEntry = orderEntryIterator.next();
			DbOrder order = orderEntry.getKey();
			addOrderToEsStack(order);
		}

	}

}
