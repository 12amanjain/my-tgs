package com.tgs.services.accounting.excelaccounting.air;

import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import com.tgs.services.base.SpringContext;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Configuration
public class AirExcelAccountingConfiguration {

	@Value("${air.accounting.bean.statementpreparation}")
	private String statementPreparationBean;


	@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
	@Bean
	protected AbstractAirExcelAccountingStatementPreparation statementPreparation() {
		try {
			return SpringContext.getApplicationContext().getBean(statementPreparationBean,
					AbstractAirExcelAccountingStatementPreparation.class);
		} catch (NoSuchBeanDefinitionException ex) {
			log.debug(
					"No bean found for AbstractAirExcelAccountingStatementPreparation, beanName={}. Returning bean 'airExcelAccountingStatementPreparation'",
					statementPreparationBean);
			return SpringContext.getApplicationContext().getBean("airExcelAccountingStatementPreparation",
					AbstractAirExcelAccountingStatementPreparation.class);
		}
	}
}
