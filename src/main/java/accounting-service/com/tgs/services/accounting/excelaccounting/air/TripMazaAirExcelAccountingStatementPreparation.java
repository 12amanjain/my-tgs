package com.tgs.services.accounting.excelaccounting.air;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.excelaccounting.amendment.AirExcelAccountingStatementPreparation;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class TripMazaAirExcelAccountingStatementPreparation extends AirExcelAccountingStatementPreparation {

	@Override
	protected BigDecimal getCommissionAmount(DbAirOrderItem airOrderItem,
			VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		if (airOrderItem.getCreatedOn().isBefore(LocalDateTime.of(2020, 8, 24, 16, 0))) {
			return getFareComponentsAsBigDecimal.apply(FareComponent.SC).divide(new BigDecimal(1.0375), 2,
					RoundingMode.HALF_UP);
		}
		return getFareComponentsAsBigDecimal.apply(FareComponent.SC);
	}

	@Override
	protected BigDecimal getSRV_CHRG1P(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.SMF).divide(new BigDecimal(1.18), 2,
				RoundingMode.HALF_UP);
	}

	@Override
	protected BigDecimal getSRV_CHRG1C(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.CMU);
	}

	@Override
	protected BigDecimal getSRV_CHRG2C(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.PF);
	}

	@Override
	protected BigDecimal getTax3(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.AT, FareComponent.UDF, FareComponent.PSF,
				FareComponent.RCF, FareComponent.OT, FareComponent.WO, FareComponent.WC, FareComponent.YM,
				FareComponent.BP, FareComponent.MP, FareComponent.SP);
	}

	@Override
	protected BigDecimal getTax4(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.OC);
	}

	@Override
	protected String getTicketNumber(String ticketNo, int pnrCountAlreadyPushed, boolean isReissuedCartWithSamePnr) {
		// if same PNR has already been pushed like TEST-1, for next booking it should be TEST-11 (TEST-21,.. if
		// multiple pax) , for third booking TEST-12 , and so on...
		if (pnrCountAlreadyPushed > 0) {
			ticketNo = new StringBuilder(ticketNo).append(pnrCountAlreadyPushed).toString();
		}
		return ticketNo;
	}
}
