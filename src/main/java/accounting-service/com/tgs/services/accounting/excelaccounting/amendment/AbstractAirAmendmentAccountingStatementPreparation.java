package com.tgs.services.accounting.excelaccounting.amendment;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import com.google.api.client.repackaged.com.google.common.base.Joiner;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;

public abstract class AbstractAirAmendmentAccountingStatementPreparation {

	final private static String SELECT_SQL_TEMPLATE = "SELECT * FROM %s WHERE NARRATION IN ('%s');";

	final private static String INSERT_SQL_TEMPLATE = "INSERT INTO %s (" + OrderColumn.ALL + ") VALUES (%s);";

	public String prepareSelectSql(String tableName, Set<String> bookingIds) throws SQLException {
		return String.format(SELECT_SQL_TEMPLATE, tableName, Joiner.on("','").join(bookingIds));
	}

	public String prepareInsertSql(String tableName, Map<OrderColumn, CharSequence> existingInvoiceData, Amendment amendMent,
			List<AirOrderItem> airOrderItems, Long paxId) throws SQLException {
		Map<OrderColumn, CharSequence> statementParams =
				getColumnValues(existingInvoiceData, amendMent, airOrderItems, paxId);
		StringJoiner joiner = new StringJoiner(", ");
		for (OrderColumn column : OrderColumn.values()) {
			joiner.add(statementParams.get(column));
		}
		return String.format(INSERT_SQL_TEMPLATE, tableName, joiner.toString());
	}

	protected abstract Map<OrderColumn, CharSequence> getColumnValues(Map<OrderColumn, CharSequence> existingInvoiceData,
			Amendment amendMent, List<AirOrderItem> airOrderItems, Long paxId);

}
