package com.tgs.services.accounting.excelaccounting.amendment;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.google.common.base.Function;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.utils.AirAccountingUtils;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.manager.AbstractAmendmentInvoiceIdManager;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.utils.air.AirBookingUtils;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class AirAmendmentAccountingStatementPreparation extends AbstractAirAmendmentAccountingStatementPreparation {

	@Autowired
	protected AbstractAmendmentInvoiceIdManager invoiceIdManager;
	
	@Autowired
	protected AirAccountingUtils airAccountingUtils;

	private Map<OrderColumn, CharSequence> invoiceData;

	@Override
	protected Map<OrderColumn, CharSequence> getColumnValues(Map<OrderColumn, CharSequence> existingInvoiceData,
			Amendment amendment, List<AirOrderItem> airOrderItems, Long paxId) {
		invoiceData = existingInvoiceData;
		Map<String, Double> paxWiseAmendmentCharges = amendment.getModifiedInfo().getPaxWiseAmount();
		Double totalAmount = 0.0;
		List<FareDetail> fareDetails = new ArrayList<>();
		for (AirOrderItem airorderitem : airOrderItems) {
			totalAmount += paxWiseAmendmentCharges.getOrDefault(airorderitem.getId() + "_" + paxId, 0.0);
			FlightTravellerInfo travellerInfo = airorderitem.getTravellerInfo().stream()
					.filter(t -> t.getId().equals(paxId)).findFirst().orElseGet(null);
			if (travellerInfo.getFareDetail() != null)
				fareDetails.add(travellerInfo.getFareDetail());

		}
		FlightTravellerInfo travellerInfo = airOrderItems.get(0).getTravellerInfo().stream()
				.filter(t -> t.getId().equals(paxId)).findFirst().orElseGet(null);
		// pricing for given traveller
		final Map<FareComponent, Double> fareComponents = AirBookingUtils.getMergedComponents(fareDetails);
		// BigDecimal behaves abnormally when used with double. Use String instead.
		final Function<Double, BigDecimal> getDoubleAsBigDecimal =
				doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final VarargsFunction<FareComponent, Double> getFareComponentsAsDouble = components -> {
			double sum = 0.0;
			for (FareComponent fareComponent : components) {
				sum += fareComponents.getOrDefault(fareComponent, 0.0);
			}
			return sum;
		};

		// Get sum of all FareComponents as BigDecimal
		final VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
			return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
		};

		ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(amendment.getBookingId());

		String invoiceId = amendment.getAdditionalInfo().getInvoiceId();
		if (StringUtils.isBlank(invoiceId)) {
			invoiceId = invoiceIdManager.getInvoiceId(amendment, productMetaInfo);
			amendment.getAdditionalInfo().setInvoiceId(invoiceId);
		}

		Integer numericInvoiceId = invoiceIdManager.getNumericInvoiceId(invoiceId, amendment, productMetaInfo);

		setString(invoiceIdManager.getInvoiceIdPrefix(amendment, productMetaInfo), OrderColumn.DOC_PRF);
		setString(String.valueOf(numericInvoiceId), OrderColumn.DOC_NOS);
		setBigDecimal(BigDecimal.ZERO, OrderColumn.SERV_CS1P, OrderColumn.SERV3_CS1P, OrderColumn.SERV1_TAXP, OrderColumn.SERV1_EDUP, OrderColumn.SERV1_CS1P, OrderColumn.SERV_CS2C, OrderColumn.SERV1_CS2C,
				OrderColumn.SERV2_CS1C, OrderColumn.SERV2_CS2C, OrderColumn.SERV3_CS2C, OrderColumn.SERV_CS2P, OrderColumn.SERV1_CS2P, OrderColumn.SERV3_CS2P, OrderColumn.SERV1_CS1C, OrderColumn.SERV3_TAXP,
				OrderColumn.SERV3_EDUP);


		// Can change in correction amendment
		String paxName = airAccountingUtils.getPaxName(travellerInfo);
		final PaxType paxType = travellerInfo.getPaxType();

		boolean isSSROrFc = AmendmentType.SSR.equals(amendment.getAmendmentType())
				|| AmendmentType.FARE_CHANGE.equals(amendment.getAmendmentType());
		boolean isReissue = AmendmentType.REISSUE.equals(amendment.getAmendmentType());
		BigDecimal reissueOrCancelCharges = isReissue ? getFareComponentsAsBigDecimal.apply(FareComponent.ARF)
				: getFareComponentsAsBigDecimal.apply(FareComponent.ACF);
		BigDecimal reissueOrCancelChargesTaxes = isReissue ? getFareComponentsAsBigDecimal.apply(FareComponent.ARFT)
				: getFareComponentsAsBigDecimal.apply(FareComponent.ACFT);
		BigDecimal clientFee = isReissue ? getFareComponentsAsBigDecimal.apply(FareComponent.CRF)
				: getFareComponentsAsBigDecimal.apply(FareComponent.CCF);
		BigDecimal clientAmendmentCharges = getFareComponentsAsBigDecimal.apply(FareComponent.CAF);
		BigDecimal clientAmendmentMarkUp = getFareComponentsAsBigDecimal.apply(FareComponent.CAMU);

		setBigDecimal(reissueOrCancelCharges.add(reissueOrCancelChargesTaxes).add(clientAmendmentMarkUp), OrderColumn.XXL_C);
		setBigDecimal(reissueOrCancelCharges.add(reissueOrCancelChargesTaxes), OrderColumn.XXL_P);

		LocalDateTime amendmentTime = amendment.getProcessedOn();
		LocalDateTime orderTime = airOrderItems.get(0).getCreatedOn();
		// If both in same month then pull from sale invoice, otherwise 0
		if (AmendmentType.NO_SHOW.equals(amendment.getAmendmentType())
				|| !amendment.getCreatedOn().getMonth().equals(orderTime.getMonth())) {
			setBigDecimal(BigDecimal.ZERO, OrderColumn.TDS_C);
		}
		if (!AmendmentType.VOIDED.equals(amendment.getAmendmentType())) {
			setBigDecimal(BigDecimal.ZERO, OrderColumn.SRV_CHRG3C, OrderColumn.SERV3_TAXC, OrderColumn.SERV3_EDUC, OrderColumn.SERV3_CS1C);
		} else {
			setString("", OrderColumn.Pay_Type, OrderColumn.SCODE_B);
		}

		setBigDecimal(getFareComponentsAsBigDecimal.apply(FareComponent.IGST), OrderColumn.SERV1_TAXC);
		setBigDecimal(getFareComponentsAsBigDecimal.apply(FareComponent.SGST), OrderColumn.SERV1_EDUC);
		setBigDecimal(getFareComponentsAsBigDecimal.apply(FareComponent.CGST), OrderColumn.SERV1_CS1C);

		// In case of SSR or FC, in case of other amendments BF will be same as previous sale invoice
		if (isSSROrFc) {
			setBigDecimal(BigDecimal.ZERO, OrderColumn.BASIC_FARE);
			setBigDecimal(BigDecimal.ZERO, OrderColumn.TAX_1, OrderColumn.TAX_2, OrderColumn.XXL_C, OrderColumn.XXL_P, OrderColumn.SRV_CHRG4C, OrderColumn.SRV_CHRG5C, OrderColumn.SRV_CHRG1P, OrderColumn.SRV_CHRG2P,
					OrderColumn.SRV_CHRG3P, OrderColumn.SRV_CHRG4P, OrderColumn.SRV_CHRG5P, OrderColumn.RAF_P, OrderColumn.SERV_TAXC, OrderColumn.SERV_EDUC, OrderColumn.SERV_CS1C, OrderColumn.TDC_PAIDV1, OrderColumn.TDS_C,
					OrderColumn.SERV_TAXP, OrderColumn.SERV_EDUP, OrderColumn.TDS_PAIDV1, OrderColumn.TDS_P, OrderColumn.TDB_PAIDV1, OrderColumn.TDS_B, OrderColumn.DISC_PAIDV5, OrderColumn.DISC_PAID5, OrderColumn.TAX_5,
					OrderColumn.DISC_PAIDV1, OrderColumn.DISC_RECDV1, OrderColumn.DISC_PAID1, OrderColumn.DISC_RECD1);
			setBigDecimal(BigDecimal.ZERO, OrderColumn.SRV_CHRG1C);
			String ticketNo =
					invoiceData.get(OrderColumn.TICKETNO).toString().replaceAll("\"", "").replaceAll("'", "").trim().concat("A");
			setString(ticketNo, OrderColumn.TICKETNO);
			Boolean isFC = AmendmentType.FARE_CHANGE.equals(amendment.getAmendmentType());
			// In case of FC, push the total charged amount. In case of SSR, only MP+BP+SP
			BigDecimal totalAmountCharged = isFC ? getDoubleAsBigDecimal.apply(totalAmount)
					: getFareComponentsAsBigDecimal
							.apply(FareComponent.getSSRComponents().toArray(new FareComponent[0]));
			setBigDecimal(totalAmountCharged, OrderColumn.TAX_3);
			if (isFC)
				setBigDecimal(BigDecimal.ZERO, OrderColumn.SERV1_TAXC, OrderColumn.SERV1_EDUC, OrderColumn.SERV1_CS1C);
		}
		setString(amendment.getBookingId().concat("/").concat(amendment.getAmendmentId()), OrderColumn.NARRATION);
		setString(amendmentTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), OrderColumn.IDATE);
		setString(orderTime.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")), OrderColumn.SaleDATE);
		setString(paxName, OrderColumn.PAX);
		setBigDecimal(clientAmendmentCharges.add(clientFee), OrderColumn.RAF_C);
		setString("", OrderColumn.NARRATION_5, OrderColumn.NARRATION_6, OrderColumn.SAC_CODE1, OrderColumn.DCODE, OrderColumn.ECODE, OrderColumn.BCODE, OrderColumn.IL_REF, OrderColumn.VD_REF);
		setString("G", OrderColumn.GST_TYPE);
		setString("S", OrderColumn.GST_CalcOn);
		setString("RYN", OrderColumn.DISC_RECDM2);
		setString("L", OrderColumn.Stax_PayBy);
		setBigDecimal(PaxType.ADULT.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO, OrderColumn.NOS_PAX_A);
		setBigDecimal(PaxType.CHILD.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO, OrderColumn.NOS_PAX_C);
		setBigDecimal(PaxType.INFANT.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO, OrderColumn.NOS_PAX_I);

		setString(airAccountingUtils.getSector(airOrderItems), OrderColumn.SECTOR);
		return existingInvoiceData;
	}

	private void setString(String str, OrderColumn... columns) {
		airAccountingUtils.setString(invoiceData, str, columns);
	}

	private void setBigDecimal(BigDecimal bigDecimal, OrderColumn... columns) {
		airAccountingUtils.setBigDecimal(invoiceData, bigDecimal, columns);
	}
}
