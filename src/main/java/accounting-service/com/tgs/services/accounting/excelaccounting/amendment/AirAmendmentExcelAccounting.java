package com.tgs.services.accounting.excelaccounting.amendment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.filters.AmendmentFilter;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.accounting.utils.AirAccountingUtils;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.jparepository.AmendmentService;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class AirAmendmentExcelAccounting {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	AirAmendmentManager airAmendmentManager;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	protected AirAccountingUtils airAccountingUtils;

	@Autowired
	private AbstractAirAmendmentAccountingStatementPreparation statementPreparation;

	private static List<AmendmentType> notToBePushedAmendments =
			Arrays.asList(AmendmentType.REISSUE, AmendmentType.CORRECTION);

	public void pushInvoicesFor(AmendmentFilter filter) {
		if (filter == null) {
			return;
		}

		Connection con = null;
		Statement statement = null;
		try {
			List<Amendment> amendments = amendmentService.search(filter);
			Set<String> bookingIds = amendments.stream().map(a -> a.getBookingId()).collect(Collectors.toSet());
			log.debug("Booking ids found : {} ", bookingIds.toString());
			Map<String, Map<OrderColumn, CharSequence>> rowsData = getExistingRows(bookingIds);
			log.debug("Existing data is {} ", GsonUtils.getGson().toJson(rowsData));
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			Map<String, List<String>> sqlsLogs = new HashMap<>();
			List<Amendment> amendmentsInBatch = new ArrayList<>();
			Iterator<Amendment> amendmentEntryItr = amendments.iterator();
			while (amendmentEntryItr.hasNext()) {
				try {
					Amendment amendment = amendmentEntryItr.next();
					if (!notToBePushedAmendments.contains(amendment.getAmendmentType())
							&& BooleanUtils.isNotTrue(amendment.getAdditionalInfo().getInvoicePushStatus())) {
						List<String> sqlList = createSqlsFor(amendment, rowsData);
						sqlsLogs.put(amendment.getBookingId(), sqlList);
						for (String sql : sqlList) {
							statement.addBatch(sql);
							amendmentsInBatch.add(amendment);
						}
					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last order.
					 */
					if (amendmentsInBatch.size() >= BATCH_SIZE || !amendmentEntryItr.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = false;
							log.error("Failed to insert invoice for bookingIds {} due to {} ", sqlsLogs.keySet(),
									e.getMessage(), e);
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "AmendmentAccounting", e);
							}
						} finally {
							/**
							 * Batch should be cleared even if it fails so that upcoming batches can be served.
							 */
							statement.clearBatch();
							for (Amendment order_local : amendmentsInBatch) {
								order_local.getAdditionalInfo().setInvoicePushStatus(status);
							}
							amendmentService.saveWithoutProcessedOn(amendmentsInBatch);
							amendmentsInBatch.clear();
						}
						accountingUtils.logSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (Exception e) {
					log.error(
							"Failed to insert amendment invoice for bookingIds {}. Trying to build connections again. Reason: {}",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "AmendmentAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}
			}
		} catch (Exception e) {
			log.error("Failed to insert invoice for amendment Filter {} due to {}", GsonUtils.getGson().toJson(filter),
					e.getMessage(), e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}

	private List<String> createSqlsFor(Amendment amendment, Map<String, Map<OrderColumn, CharSequence>> rowsData) {
		// Pnr and Name taken from old snapshot because these can be changed through amendments
		// List<AirOrderItem> modifiedOrderItems = amendment.getModifiedInfo().getAirOrderItems();
		List<AirOrderItem> modifiedOrderItems = airAmendmentManager.getAmendmentSnapshot(amendment, false);
		// grouping by PNR
		Map<String, List<AirOrderItem>> pnrWiseAirOrderItems = modifiedOrderItems.stream()
				.collect(Collectors.groupingBy(orderItem -> orderItem.getTravellerInfo().get(0).getPnr()));
		List<String> sqls = new ArrayList<>();
		for (List<AirOrderItem> perPnrOrderItemList : pnrWiseAirOrderItems.values()) {
			// Find distinct pax ids involved in amendment
			Set<Long> paxIds = new HashSet<>();
			perPnrOrderItemList.forEach(orderitem -> {
				Set<Long> unqiuePaxIds = orderitem.getTravellerInfo().stream()
						.collect(Collectors.mapping(p -> p.getId(), Collectors.toSet()));
				paxIds.addAll(unqiuePaxIds);
			});

			paxIds.forEach(paxId -> {
				Long airOrderItemId = perPnrOrderItemList.get(0).getId();
				// Find matching old airorderitem and construct key
				AirOrderItem oldItem = amendment.getAirAdditionalInfo().getOrderPreviousSnapshot().stream()
						.filter(a -> a.getId().equals(airOrderItemId)).findAny().orElse(null);
				FlightTravellerInfo oldTravellerData =
						oldItem.getTravellerInfo().stream().filter(t -> t.getId().equals(paxId)).findAny().orElse(null);
				String paxName = airAccountingUtils.getPaxName(oldTravellerData);
				String paxName30chars = StringUtils.left(paxName, 30);
				String key = getUniqueKey(amendment.getBookingId(), oldTravellerData.getPnr(), paxName30chars);
				Map<OrderColumn, CharSequence> existingInvoiceData = rowsData.get(key);
				log.debug("Existing sale invoice data for key {} is {}", existingInvoiceData, key);
				String keyWithSector = "";
				if (MapUtils.isEmpty(existingInvoiceData)) {
					String sector = airAccountingUtils.getSector(perPnrOrderItemList);
					keyWithSector = getUniqueKey(amendment.getBookingId(), sector, paxName30chars);
					existingInvoiceData = rowsData.get(keyWithSector);
					log.debug("Existing sale invoice data for key {} is {}", existingInvoiceData, keyWithSector);
				}
				if (existingInvoiceData == null)
					log.info("Invoice data not found in accounting package for keys {} {}", key, keyWithSector);
				else {
					try {
						String sql = statementPreparation.prepareInsertSql(dbConf.getTableName(), existingInvoiceData,
								amendment, perPnrOrderItemList, paxId);
						log.debug("Sql prepared is {} ", sql);
						sqls.add(sql);
					} catch (SQLException e) {
						log.error("Failed to prepare amendment invoice data for amendment ids {} pax id {} due to {}",
								amendment.getAmendmentId(), paxId, e.getMessage(), e);
					}
				}
			});

		}
		return sqls;
	}

	private String getUniqueKey(String bookingId, String pnr, String paxName) {
		return bookingId.trim().concat("_").concat(pnr.trim().concat("_").concat(paxName.trim()));
	}

	private Map<String, Map<OrderColumn, CharSequence>> getExistingRows(Set<String> bookingIds) throws SQLException {
		Connection con = null;
		Statement statement = null;
		Map<String, Map<OrderColumn, CharSequence>> rowDataPerKey = new HashMap<>();
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			String selectStmt = statementPreparation.prepareSelectSql(dbConf.getTableName(), bookingIds);
			log.debug("Select statement is {} ", selectStmt);
			ResultSet resultSet = statement.executeQuery(selectStmt);
			log.debug("Resultset size is {} ", resultSet.getFetchSize());
			while (resultSet.next()) {
				try {
					Map<OrderColumn, CharSequence> rowData = resultSetToHashMap(resultSet);
					log.debug("Rowdata in the result set is {} ", GsonUtils.getGson().toJson(rowData));
					String bookingId = (String) rowData.get(OrderColumn.NARRATION);
					String paxPnr = (String) rowData.get(OrderColumn.PNR_NO);
					String paxName = (String) rowData.get(OrderColumn.PAX);
					String sector = (String) rowData.get(OrderColumn.SECTOR);
					String key = getUniqueKey(bookingId.substring(1, bookingId.length() - 1),
							paxPnr.substring(1, paxPnr.length() - 1), paxName.substring(1, paxName.length() - 1));
					// This is required because in case of auto cancellation amendments , old pnr is same as new pnr
					// If this logic works, in future we can remove pnr from key
					String keyWithSector = getUniqueKey(bookingId.substring(1, bookingId.length() - 1),
							sector.substring(1, sector.length() - 1), paxName.substring(1, paxName.length() - 1));
					log.debug("Unique keys are {}  {} ", key, keyWithSector);
					rowDataPerKey.put(key, rowData);
					rowDataPerKey.put(keyWithSector, rowData);
				} catch (Exception e) {
					log.error("Unable to convert result set for selectStmt {} due to {} ", selectStmt, e.getMessage(),
							e);
				}
			}
		} finally {
			accountingUtils.tryClosingResources(con, statement);
		}
		return rowDataPerKey;
	}

	public Map<OrderColumn, CharSequence> resultSetToHashMap(ResultSet rs) throws SQLException {
		ResultSetMetaData metadata = rs.getMetaData();
		int columns = metadata.getColumnCount();
		log.debug("Column count is {}", columns);
		Map<OrderColumn, CharSequence> row = new HashMap<>(columns);
		for (int i = 1; i <= columns; i++) {
			log.debug("Column name for metadata is {} ", metadata.getColumnName(i));
			OrderColumn column = null;
			try {
				column = OrderColumn.valueOf(metadata.getColumnName(i));
			} catch (Exception e) {
				log.debug("No mapping found for {} ", metadata.getColumnName(i));
			}
			// Avoid extra columns not known to us
			if (column != null) {
				row.put(column,
						rs.getObject(i) != null ? "'".concat(String.valueOf(rs.getObject(i))).concat("'") : "''");
			}
		}
		return row;
	}

}
