package com.tgs.services.accounting.excelaccounting.amendment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.datamodel.OrderColumn;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.accounting.excelaccounting.air.AbstractAirExcelAccountingStatementPreparation;
import com.tgs.services.accounting.utils.AirAccountingUtils;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.gms.datamodel.InvoiceIdData;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.air.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
public class AirExcelAccountingStatementPreparation extends AbstractAirExcelAccountingStatementPreparation {

	@Autowired
	private UserServiceCommunicator usCommunicator;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private CommercialCommunicator cmsCommunicator;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;
	
	@Autowired
	protected AirAccountingUtils airAccountingUtils;

	@Autowired
	private GstInfoService gstService;

	private Map<OrderColumn, CharSequence> statementParams = new HashMap<>();

	@Override
	protected Map<OrderColumn, CharSequence> getColumnValues(DbOrder order, List<DbAirOrderItem> airOrderItems, int serialNo,
			int pnrCountAlreadyPushed) {
		statementParams.clear();

		ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(order.getBookingId());

		Order domainOrder = order.toDomain();

		DbOrderBilling orderBilling = gstService.findByBookingId(order.getBookingId());

		final User bookingUser = usCommunicator.getUserFromCache(order.getBookingUserId());

		List<AirOrderItem> domainOrderItems = DbAirOrderItem.toDomainList(airOrderItems);

		int paxIndex = (serialNo - 1) % airOrderItems.get(0).getTravellerInfo().size();

		final FlightTravellerInfo travellerInfo = airOrderItems.get(0).getTravellerInfo().get(paxIndex);

		boolean isCreditShellUsed =
				(BaseUtils.calculateAvailablePNRCredit(travellerInfo.getFareDetail()) > 0) ? true : false;

		String invoiceId = null;
		if (UserRole.corporate(bookingUser.getRole())) {
			invoiceId = travellerInfo.getInvoice();
			if (StringUtils.isBlank(invoiceId)) {
				invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo);
				travellerInfo.setInvoice(invoiceId);
				airOrderItemService.save(airOrderItems);
			}
		} else {
			invoiceId = order.getAdditionalInfo().getInvoiceId();
			if (StringUtils.isBlank(invoiceId)) {
				invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo);
				order.getAdditionalInfo().setInvoiceId(invoiceId);
			}
		}

		// pricing for given traveller
		final Map<FareComponent, Double> fareComponents =
				AirBookingUtils.getMergedComponents(domainOrderItems, paxIndex);

		// BigDecimal behaves abnormally when used with double. Use String instead.
		final Function<Double, BigDecimal> getDoubleAsBigDecimal =
				doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

		final VarargsFunction<FareComponent, Double> getFareComponentsAsDouble = components -> {
			double sum = 0.0;
			for (FareComponent fareComponent : components) {
				sum += fareComponents.getOrDefault(fareComponent, 0.0);
			}
			return sum;
		};

		// Get sum of all FareComponents as BigDecimal
		final VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
			return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
		};

		// Airline Code + flightNumber + Class of Booking + O + TRAVELDATE
		final Function<Integer, String> getFlightDetail = orderItemIndex -> {
			if (orderItemIndex < domainOrderItems.size()) {
				AirOrderItem orderItem = domainOrderItems.get(orderItemIndex);
				String classOfBooking = orderItem.getTravellerInfo().get(paxIndex).getFareDetail().getClassOfBooking();
				classOfBooking = StringUtils.defaultString(StringUtils.left(classOfBooking, 1));
				String fN = orderItem.getFlightNumber();
				return new StringBuilder(orderItem.getPlatingCarrier())
						.append(StringUtils.leftPad(fN, 4, '0')).append(classOfBooking).append("O").append(orderItem
								.getDepartureTime().toLocalDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")))
						.toString();
			}
			return "";
		};

		final PaxType paxType = travellerInfo.getPaxType();

		// first order item?
		final AirlineInfo airlineInfo = fmsCommunicator.getAirlineInfo(domainOrderItems.get(0).getPlatingCarrier());

		final boolean isDomestic = AirType.DOMESTIC.getName().equals(productMetaInfo.getSubProduct()),
				isLCC = airlineInfo.getIsLcc();

		setString("", OrderColumn.IL_REF, OrderColumn.VD_REF, OrderColumn.DCODE, OrderColumn.ECODE, OrderColumn.XO_NOS, OrderColumn.REFR_KEY); // empty

		setString("VL", OrderColumn.DISC_PAIDM1, OrderColumn.DISC_RECDM1); // fixed

		setString("RBN", OrderColumn.DISC_PAIDM2, OrderColumn.DISC_PAIDM3, OrderColumn.DISC_RECDM2, OrderColumn.DISC_RECDM3); // fixed

		setBigDecimal(BigDecimal.ONE, OrderColumn.R_O_E_C, OrderColumn.R_O_E_S); // fixed

		setBigDecimal(BigDecimal.ZERO, OrderColumn.BASIC_PBL, OrderColumn.TAX_4, OrderColumn.TAX_6, OrderColumn.DISC_PAIDV2, OrderColumn.DISC_PAIDV3, OrderColumn.DISC_RECDV2, OrderColumn.DISC_RECDV3,
				OrderColumn.BROK_PAIDV1, OrderColumn.DISC_PAID2, OrderColumn.DISC_PAID3, OrderColumn.DISC_RECD2, OrderColumn.DISC_RECD3, OrderColumn.BROK_PAID1, OrderColumn.SRV_CHRG2C, OrderColumn.SRV_CHRG4C,
				OrderColumn.SRV_CHRG5C, OrderColumn.RAF_C, OrderColumn.SRV_CHRG2P, OrderColumn.SRV_CHRG3P, OrderColumn.SRV_CHRG4P, OrderColumn.SRV_CHRG5P, OrderColumn.RAF_P, OrderColumn.SERV_TAXC, OrderColumn.SERV_EDUC,
				OrderColumn.SERV_CS1C, OrderColumn.TDC_PAIDV1, OrderColumn.SERV_TAXP, OrderColumn.SERV_EDUP, OrderColumn.TDS_PAIDV1, OrderColumn.TDS_P, OrderColumn.TDB_PAIDV1, OrderColumn.TDS_B, OrderColumn.XXL_C, OrderColumn.XXL_P,
				OrderColumn.SERV3_TAXC, OrderColumn.SERV3_EDUC, OrderColumn.SERV3_CS1C, OrderColumn.SERV3_TAXP, OrderColumn.SERV3_EDUP, OrderColumn.DISC_PAIDV5, OrderColumn.DISC_PAID5, OrderColumn.DISC_RECD5,
				OrderColumn.DISC_RECDV5); // fixed

		User distributorUser = usCommunicator.getUserFromCache(bookingUser.getUserConf().getDistributorId());
		String distributorCode = "";
		if (distributorUser != null) {
			distributorCode = distributorUser.getAdditionalInfo().getAccountingCode();
		}
		setString(OrderColumn.BCODE, distributorCode);

		setString("INR", OrderColumn.Curcode_C, OrderColumn.Curcode_S); // fixed

		final InvoiceIdData invoiceIdData = invoiceIdManager.getInvoiceIdData(invoiceId, domainOrder, productMetaInfo);
		setString(OrderColumn.DOC_PRF, invoiceIdData.getPrefix());
		setString(OrderColumn.DOC_NOS, String.valueOf(invoiceIdData.getNumericInvoiceId()));
		setString(OrderColumn.DOC_SRNO, String.format("%03d", serialNo)); // 3-digit srno
		setString(OrderColumn.IDM_FLAG, isDomestic ? "D" : "I");

		String clientCode = bookingUser.getAdditionalInfo().getAccountingCode();
		if (StringUtils.isBlank(clientCode)) {
			clientCode = "CT01ET";
		} else {
			clientCode = "C".concat(clientCode);
		}
		setString(OrderColumn.CCODE, clientCode);

		setString(OrderColumn.LOC_CODE, "000"); // fixed
		setString(OrderColumn.CST_CODE, "001"); // fixed
		if (UserUtils.isCorporate(bookingUser)) {
			setString(OrderColumn.CST_CODE, "006");
			if (orderBilling != null) {
				setString(OrderColumn.CCODE,
						(StringUtils.isNotBlank(orderBilling.getInfo().getAccountCode()))
								? "C".concat(orderBilling.getInfo().getAccountCode())
								: clientCode);
			}
		}
		setString(OrderColumn.ACODE, airlineInfo.getCode()
				+ StringUtils.defaultString(StringUtils.leftPad(airlineInfo.getAccountingCode(), 3, '0')));

		SupplierInfo supplierInfo = fmsCommunicator.getSupplierInfo(airOrderItems.get(0).getSupplierId());
		String supplierCode = supplierInfo.getCredentialInfo().getAccountingCode();

		if (StringUtils.isBlank(supplierCode)) {
			supplierCode = "P 799";
			setString(OrderColumn.XO_REF, "B");
		} else if (supplierCode.toUpperCase().startsWith("S")) {
			setString(OrderColumn.XO_REF, "C");
		} else {
			setString(OrderColumn.XO_REF, "E");
		}
		setString(OrderColumn.SCODE, supplierCode);

		setString(OrderColumn.NARRATION, order.getBookingId());
		setString(OrderColumn.PNR_NO, travellerInfo.getPnr());

		boolean isReissuedCartWithSamePnr = false;
		if (OrderFlowType.REISSUED.equals(order.getAdditionalInfo().getFlowType())) {
			String oldBookingId = travellerInfo.getOldBookingId();
			String pnr = travellerInfo.getPnr();
			AirOrderItemFilter airOrderFilter = AirOrderItemFilter.builder()
					.travellerInfoFilter(TravellerInfoFilter.builder().pnr(pnr).build()).build();
			OrderFilter filter = OrderFilter.builder().products(Arrays.asList(OrderType.AIR)).bookingId(oldBookingId)
					.createdOnAfterDate(LocalDate.now().minusMonths(5)).itemFilter(airOrderFilter).build();
			List<Object[]> orderObjectsList = airOrderItemService.findByJsonSearch(filter);
			isReissuedCartWithSamePnr = CollectionUtils.isNotEmpty(orderObjectsList);
		}

		String ticketNo = travellerInfo.getTicketNumber();

		if (StringUtils.isBlank(ticketNo)) {
			ticketNo = new StringBuilder(travellerInfo.getPnr()).append("-").append(serialNo).toString();
			ticketNo = getTicketNumber(ticketNo, pnrCountAlreadyPushed, isReissuedCartWithSamePnr);
		} else {
			/**
			 * In order to handle conjunction numbers, Conjunction Tickets are those which consist of more than 4
			 * segments. It consist of "-" hypen or "/" slash
			 */
			if (ticketNo.contains("-")) {
				ticketNo = ticketNo.substring(0, ticketNo.indexOf("-"));
			}
			if (ticketNo.contains("/")) {
				ticketNo = ticketNo.substring(0, ticketNo.indexOf("/"));
			}
		}
		setString(OrderColumn.TICKETNO, StringUtils.right(ticketNo, 10));

		String paxName = airAccountingUtils.getPaxName(travellerInfo);
		setString(OrderColumn.PAX, paxName);

		setString(OrderColumn.SECTOR, airAccountingUtils.getSector(domainOrderItems));

		switch (supplierInfo.getSourceId()) {
			case 2:
				setString(OrderColumn.CRS_ID, "SA");
				break;
			case 10:
				setString(OrderColumn.CRS_ID, "AM");
				break;
			case 15:
				setString(OrderColumn.CRS_ID, "GA");
				break;
			default:
				if (isLCC) {
					setString(OrderColumn.CRS_ID, airlineInfo.getCode());
				} else {
					setString(OrderColumn.CRS_ID, "");
				}
		}

		String fareBasis = airOrderItems.get(0).getTravellerInfo().get(paxIndex).getFareDetail().getFareBasis();
		setString(OrderColumn.FARE_BASIS, fareBasis);
		setString(OrderColumn.DEAL_CODE, ObjectUtils.firstNonNull(airOrderItems.get(0).getAdditionalInfo().getAccountCode(),
				airOrderItems.get(0).getAdditionalInfo().getTourCode(), ""));
		setBigDecimal(OrderColumn.NOS_PAX_A, PaxType.ADULT.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO);
		setBigDecimal(OrderColumn.NOS_PAX_C, PaxType.CHILD.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO);
		setBigDecimal(OrderColumn.NOS_PAX_I, PaxType.INFANT.equals(paxType) ? BigDecimal.ONE : BigDecimal.ZERO);

		setString(OrderColumn.FLT_DTLS1, getFlightDetail.apply(0));
		setString(OrderColumn.FLT_DTLS2, getFlightDetail.apply(1));
		setString(OrderColumn.FLT_DTLS3, getFlightDetail.apply(2));
		setString(OrderColumn.FLT_DTLS4, getFlightDetail.apply(3));

		if (isCreditShellUsed) {
			setBigDecimal(OrderColumn.BASIC_FARE, BigDecimal.ZERO);
			setBigDecimal(OrderColumn.TAX_1, BigDecimal.ZERO);
			setBigDecimal(OrderColumn.TAX_2, BigDecimal.ZERO);
			setBigDecimal(OrderColumn.TAX_3, BigDecimal.ZERO);
			setBigDecimal(OrderColumn.TAX_5, BigDecimal.ZERO);

			BigDecimal airlineFare = getFareComponentsAsBigDecimal.apply(Arrays.stream(FareComponent.values())
					.filter(FareComponent::airlineComponent).toArray(length -> new FareComponent[length]));
			BigDecimal creditShellBalance = getFareComponentsAsBigDecimal.apply(FareComponent.CS);
			if (airlineFare.compareTo(creditShellBalance) > 0) {
				setBigDecimal(OrderColumn.TAX_3, airlineFare.subtract(creditShellBalance));
			}
		} else {
			setBigDecimal(OrderColumn.BASIC_FARE, getFareComponentsAsBigDecimal.apply(FareComponent.BF));
			setBigDecimal(OrderColumn.TAX_1, getFareComponentsAsBigDecimal.apply(FareComponent.YQ));
			setBigDecimal(OrderColumn.TAX_2, getFareComponentsAsBigDecimal.apply(FareComponent.AGST));
			setBigDecimal(OrderColumn.TAX_3, getTax3(getFareComponentsAsBigDecimal));
			setBigDecimal(OrderColumn.TAX_4, getTax4(getFareComponentsAsBigDecimal));
			setBigDecimal(OrderColumn.TAX_5, getFareComponentsAsBigDecimal.apply(FareComponent.YR));
		}


		BigDecimal totalCommission = getFareComponentsAsBigDecimal
				.apply(FareComponent.getCommissionComponents().toArray(new FareComponent[0]));
		setBigDecimal(totalCommission, OrderColumn.DISC_PAIDV1, OrderColumn.DISC_PAID1);

		setBigDecimal(getCommissionAmount(airOrderItems.get(0), getFareComponentsAsBigDecimal), OrderColumn.DISC_RECDV1,
				OrderColumn.DISC_RECD1);

		setBigDecimal(OrderColumn.SRV_CHRG1P, getSRV_CHRG1P(getFareComponentsAsBigDecimal));

		setBigDecimal(OrderColumn.SRV_CHRG1C, getSRV_CHRG1C(getFareComponentsAsBigDecimal));
		setBigDecimal(OrderColumn.SRV_CHRG2C, getSRV_CHRG2C(getFareComponentsAsBigDecimal));

		setBigDecimal(OrderColumn.SRV_CHRG3C, getFareComponentsAsBigDecimal.apply(FareComponent.MF));

		setBigDecimal(OrderColumn.TDS_C, getFareComponentsAsBigDecimal.apply(FareComponent.TDS));

		setBigDecimal(OrderColumn.SERV1_CS1C, getFareComponentsAsBigDecimal.apply(FareComponent.CGST));
		setBigDecimal(OrderColumn.SERV1_EDUC, getFareComponentsAsBigDecimal.apply(FareComponent.SGST));
		setBigDecimal(OrderColumn.SERV1_TAXC, getFareComponentsAsBigDecimal.apply(FareComponent.IGST));

		BigDecimal pointsDiscount = getFareComponentsAsBigDecimal.apply(FareComponent.RP);
		if (pointsDiscount.compareTo(BigDecimal.ZERO) > 0) {
			setBigDecimal(pointsDiscount, OrderColumn.DISC_PAID3, OrderColumn.DISC_PAIDV3);
			setString(OrderColumn.DISC_PAIDM3, "VLN");
		}

		setString(OrderColumn.BROK_PAIDM1, isLCC ? "RB" : "VL");
		setString(OrderColumn.Created_By, "null"); // fixed

		Integer ccId = airOrderItems.get(0).getAdditionalInfo().getCcId();
		// if credit card is not used , In case of LCC credit card is never used . Even for cases like Corporate where
		// virtual payment is used for LCC as well but we need to pass empty code
		if (ccId == null || isLCC) {
			setString(OrderColumn.Pay_Type, "");
			setString(OrderColumn.SCODE_B, "");
		} else {
			setString(OrderColumn.Pay_Type, "B");
			CreditCardInfo ccInfo = cmsCommunicator.getCreditCardById(Long.valueOf(ccId));
			setString(OrderColumn.SCODE_B, ccInfo.getAccountCode());
		}

		setString("RDG", OrderColumn.DISC_PAIDM5, OrderColumn.DISC_RECDM5); // fixed

		setString(OrderColumn.Stax_PayBy, "G"); // fixed
		setString(OrderColumn.IDATE, airOrderItems.get(0).getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

		setString("T", OrderColumn.SRV_CHRG1_H, OrderColumn.SRV_CHRG2_H);
		setString("N", OrderColumn.SRV_CHRG3_H);
		setString("G", OrderColumn.GST_TYPE); // fixed
		setString("", OrderColumn.NARRATION_5, OrderColumn.NARRATION_6, OrderColumn.SAC_CODE1);
		setString("B", OrderColumn.GST_CalcOn);

		statementParams.put(OrderColumn.TDC_Index, "-1");

		return statementParams;
	}

	protected final void setString(OrderColumn column, String str) {
		airAccountingUtils.setString(statementParams, column, str);
	}

	protected final void setString(String str, OrderColumn... columns) {
		airAccountingUtils.setString(statementParams, str, columns);
	}

	protected final void setBigDecimal(OrderColumn column, BigDecimal bigDecimal) {
		airAccountingUtils.setBigDecimal(statementParams, column, bigDecimal);
	}

	protected final void setBigDecimal(BigDecimal bigDecimal, OrderColumn... columns) {
		airAccountingUtils.setBigDecimal(statementParams, bigDecimal, columns);
	}

	protected BigDecimal getCommissionAmount(DbAirOrderItem airOrderItem,
			VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.BCM);
	}

	protected BigDecimal getSRV_CHRG1P(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return BigDecimal.ZERO;
	}

	protected BigDecimal getTax3(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.AT, FareComponent.UDF, FareComponent.PSF,
				FareComponent.RCF, FareComponent.OT, FareComponent.WO, FareComponent.WC, FareComponent.YM,
				FareComponent.OC, FareComponent.BP, FareComponent.MP, FareComponent.SP);
	}

	protected BigDecimal getTax4(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return BigDecimal.ZERO;
	}

	protected BigDecimal getSRV_CHRG1C(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return getFareComponentsAsBigDecimal.apply(FareComponent.CMU, FareComponent.PF, FareComponent.XT);
	}

	protected BigDecimal getSRV_CHRG2C(VarargsFunction<FareComponent, BigDecimal> getFareComponentsAsBigDecimal) {
		return BigDecimal.ZERO;
	}

	protected String getTicketNumber(String ticketNo, int pnrCountAlreadyPushed, boolean isReissuedCartWithSamePnr) {
		// if same PNR has already been pushed like TEST-1, for next booking it should be TEST-11 (TEST-21,.. if
		// multiple pax) , for third booking TEST-12 , and so on...
		if (pnrCountAlreadyPushed > 0) {
			ticketNo = new StringBuilder(ticketNo).append(pnrCountAlreadyPushed).toString();
		}
		// In case of reissued bookings, if old pnr is same as new pnr, we need to append "R" to the new accounting row.
		if (isReissuedCartWithSamePnr) {
			ticketNo = ticketNo.concat("R");
		}
		return ticketNo;
	}
}

