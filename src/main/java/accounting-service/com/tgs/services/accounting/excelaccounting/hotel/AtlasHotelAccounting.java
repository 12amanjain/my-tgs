package com.tgs.services.accounting.excelaccounting.hotel;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.function.Function;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.accounting.conf.AtlasHotelAccountingDbConfiguration;
import com.tgs.services.accounting.datamodel.VarargsFunction;
import com.tgs.services.base.LogData;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.gms.datamodel.InvoiceIdData;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.oms.manager.DefaultInvoiceIdManager;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Deprecated
public class AtlasHotelAccounting {

	private static AtlasHotelAccountingDbConfiguration dbConf =
			SpringContext.getApplicationContext().getBean(AtlasHotelAccountingDbConfiguration.class);

	final private static int BATCH_SIZE = 1;

	final private static String ID_SUFFIX = "_acc",
			sqlTemplate = "INSERT INTO " + dbConf.getTableName() + " (" + Column.ALL + ") VALUES (%s)";

	private static UserServiceCommunicator usCommunicator =
			SpringContext.getApplicationContext().getBean(UserServiceCommunicator.class);

	private static OrderService orderService = SpringContext.getApplicationContext().getBean(OrderService.class);

	private static HotelOrderItemManager hotelOrderItemManager =
			SpringContext.getApplicationContext().getBean(HotelOrderItemManager.class);

	private static DefaultInvoiceIdManager invoiceIdManager =
			SpringContext.getApplicationContext().getBean(DefaultInvoiceIdManager.class);

	private static HMSCommunicator hmsCommunicator =
			SpringContext.getApplicationContext().getBean(HMSCommunicator.class);

	private static final long LOGS_TTL = 7 * 24 * 60 * 60;

	public static void pushInvoicesFor(OrderFilter orderFilter) {
		if (orderFilter == null) {
			return;
		}

		// close resources finally
		try (Connection con = getConnection(); Statement statement = con.createStatement()) {
			con.setAutoCommit(false);
			List<DbHotelOrder> orderList = hotelOrderItemManager.getHotelOrderList(orderFilter);
			List<DbOrder> ordersInBatch = new ArrayList<>();
			Map<String, List<String>> sqlsLogs = new HashMap<>();

			StatementPreparation statementPreparation = new StatementPreparation();

			Iterator<DbHotelOrder> orderEntryIterator = orderList.iterator();

			while (orderEntryIterator.hasNext()) {
				DbHotelOrder orderEntry = orderEntryIterator.next();
				DbOrder order = orderEntry.getOrder();
				if (BooleanUtils.isNotTrue(order.getAdditionalInfo().getInvoicePushStatus())) {
					List<String> sqlList = createSqlsFor(order, orderEntry.getOrderItems(), statementPreparation);
					sqlsLogs.put(order.getBookingId(), sqlList);
					if (!CollectionUtils.isEmpty(sqlList)) {
						for (String sql : sqlList) {
							statement.addBatch(sql);
							ordersInBatch.add(order);
						}
					}
				}
				/**
				 * Execute batch if batch-size is crossed or if it is the last order.
				 */
				if (ordersInBatch.size() >= BATCH_SIZE || !orderEntryIterator.hasNext()) {
					boolean status = true;
					try {
						statement.executeBatch();
						con.commit();
					} catch (Exception e) {
						status = false;
						log.error("Failed to insert invoice for bookingIds {} due to ", sqlsLogs.keySet(), e);
					} finally {
						/**
						 * Batch should be cleared even if it fails so that upcoming batches can be served.
						 */
						statement.clearBatch();
						for (DbOrder order_local : ordersInBatch) {
							order_local.getAdditionalInfo().setInvoicePushStatus(status);
						}
						orderService.save(ordersInBatch);
						ordersInBatch.clear();
					}
					logSqls(sqlsLogs, status);
					sqlsLogs.clear();
				}
			}
		} catch (ClassNotFoundException | SQLException e) {
			log.error("Failed to insert invoice for orderFilter {} due to {}", orderFilter, e);
		}
	}

	private static void logSqls(Map<String, List<String>> sqlsLogs, boolean status) {
		for (Entry<String, List<String>> sqlsLog : sqlsLogs.entrySet()) {
			String logId = sqlsLog.getKey().concat(ID_SUFFIX);
			log.info("Sqls for bookingId {} are {}", sqlsLog.getKey(), sqlsLog.getValue());
			LogData sqlLogData, sqlStatusData;
			sqlLogData = LogData.builder().type("SQLs").ttl(LOGS_TTL).key(logId).logData(getSqlsLog(sqlsLog.getValue()))
					.logType("AirSupplierAPILogs").build();
			sqlStatusData = LogData.builder().type("Status").ttl(LOGS_TTL).key(logId).logType("AirSupplierAPILogs")
					.logData(status && !CollectionUtils.isEmpty(sqlsLog.getValue()) ? "SUCCESS" : "FAILED").build();
			LogUtils.addToLogList(sqlLogData);
			LogUtils.addToLogList(sqlStatusData);
		}
		LogUtils.clearLogList();
	}

	private static String getSqlsLog(List<String> sqls) {
		if (CollectionUtils.isEmpty(sqls)) {
			return "No SQLs created";
		}
		StringJoiner sj = new StringJoiner("\n\n");
		for (String sql : sqls) {
			sj.add(sql);
		}
		return sj.toString();
	}

	/**
	 * Inserts invoice(s) for given {@code bookingId}.<br>
	 * <br>
	 * {@code List<DbHotelOrderItem>} is fetched using {@code bookingId}. Items in the list are grouped by supplier and
	 * PNR. Each such group is a list of {@code DbHotelOrderItem} which contains travellerInfos for segments with same
	 * PNR obtained by using same supplier. For each traveller, flight details and pricing information for such
	 * segments, and traveller details are inserted in database.
	 * 
	 * @param bookingId for which invoice(s) has to be inserted
	 * @return {@code List} of SQLs if successful, otherwise {@code null}.
	 */
	private static List<String> createSqlsFor(DbOrder order, List<DbHotelOrderItem> airOrderItems,
			StatementPreparation statementPreparation) {
		if (order == null) {
			return null;
		}
		List<String> sqlList = new ArrayList<>();
		try {
			int srno = 1;
			for (DbHotelOrderItem airOrderItem : airOrderItems) {
				statementPreparation.setValues(order, airOrderItem, srno);
				sqlList.add(statementPreparation.prepareSql());
				srno++;
			}
		} catch (Exception e) {
			log.error("Failed to prepare invoice statements for bookingId {} due to ", order.getBookingId(), e);
			return null;
		}
		return sqlList;
	}

	private static Connection getConnection() throws SQLException, ClassNotFoundException {
		// check driver's availability
		Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

		String url = "jdbc:sqlserver://" + dbConf.getServerAddress() + ";database=" + dbConf.getDatabaseName();
		return DriverManager.getConnection(url, dbConf.getUsername(), dbConf.getPassword());
	}

	@Getter
	private static class StatementPreparation {
		final CharSequence[] params = new CharSequence[Column.COUNT];

		/**
		 * 
		 * @param order
		 * @param hotelOrderItem
		 * @param serialNo Serial number of each entry/sql (starting from 1)
		 * @throws SQLException
		 */
		void setValues(DbOrder order, DbHotelOrderItem hotelOrderItem, int serialNo) throws SQLException {
			ProductMetaInfo productMetaInfo = Product.getProductMetaInfoFromId(order.getBookingId());

			Order domainOrder = order.toDomain();

			String invoiceId = order.getAdditionalInfo().getInvoiceId();
			if (StringUtils.isBlank(invoiceId)) {
				invoiceId = invoiceIdManager.getInvoiceIdFor(domainOrder, productMetaInfo);
				order.getAdditionalInfo().setInvoiceId(invoiceId);
			}

			final InvoiceIdData invoiceIdData =
					invoiceIdManager.getInvoiceIdData(invoiceId, domainOrder, productMetaInfo);

			final User bookingUser = usCommunicator.getUserFromCache(order.getBookingUserId());

			// pricing for given traveller
			final Map<HotelFareComponent, Double> fareComponents =
					hotelOrderItem.getRoomInfo().getTotalFareComponents();

			// BigDecimal behaves abnormally when used with double. Use String instead.
			final Function<Double, BigDecimal> getDoubleAsBigDecimal =
					doubleVal -> new BigDecimal(doubleVal.toString()).setScale(2, BigDecimal.ROUND_HALF_EVEN);

			final VarargsFunction<HotelFareComponent, Double> getFareComponentsAsDouble = components -> {
				double sum = 0.0;
				for (HotelFareComponent fareComponent : components) {
					sum += fareComponents.getOrDefault(fareComponent, 0.0);
				}
				return sum;
			};

			// Get sum of all FareComponents as BigDecimal
			final VarargsFunction<HotelFareComponent, BigDecimal> getFareComponentsAsBigDecimal = components -> {
				return getDoubleAsBigDecimal.apply(getFareComponentsAsDouble.apply(components));
			};

			setString("", Column.IL_REF, Column.VD_REF, Column.DCODE, Column.ECODE, Column.XO_NOS, Column.REFR_KEY,
					Column.BCODE, Column.Created_By); // empty

			setBigDecimal(BigDecimal.ONE, Column.R_O_E_C, Column.R_O_E_S); // fixed

			setString("INR", Column.Curcode_C, Column.Curcode_S); // fixed

			setString(Column.DOC_PRF, invoiceIdData.getPrefix());
			setString(Column.DOC_NOS, String.valueOf(invoiceIdData.getNumericInvoiceId()));
			setString(Column.DOC_SRNO, String.format("%03d", serialNo)); // 3-digit srno

			setBigDecimal(BigDecimal.ZERO, Column.BROK_PAIDV1, Column.BROK_PAID1, Column.SRV_CHRG2C, Column.RAF_C,
					Column.SRV_CHRG1P, Column.SRV_CHRG2P, Column.SRV_CHRG3P, Column.RAF_P, Column.SERV_TAXC,
					Column.SERV_EDUC, Column.SERV_CS1C, Column.TDC_PAIDV1, Column.SERV_TAXP, Column.SERV_EDUP,
					Column.TDS_PAIDV1, Column.TDS_P, Column.TDB_PAIDV1, Column.TDS_B, Column.SERV3_CS1C); // fixed

			setString(Column.IDM_FLAG, "M");

			String clientCode = bookingUser.getAdditionalInfo().getAccountingCode();
			if (clientCode == null) {
				clientCode = "";
			} else if (StringUtils.isNotBlank(clientCode)) {
				clientCode = "C".concat(clientCode);
			}
			setString(Column.CCODE, clientCode);

			setString("000", Column.LOC_CODE, Column.CST_CODE);

			setString(Column.XO_REF, "H");
			setString(Column.GCODE, "GI03HT");
			setString(Column.STX_CENVAT, "C");
			setString(Column.STX_METHOD, "N");

			setString(Column.NARR_1, hotelOrderItem.getHotel());

			Address address = hotelOrderItem.getAdditionalInfo().getHInfo().getAddress();
			// StringBuilder location = new StringBuilder(address.getCity().getName()).append(", ")
			// .append(address.getState().getName());
			setString(Column.NARR_2, StringUtils.left(address.getAddressLine1(), 35));

			String checkinDate = hotelOrderItem.getCheckInDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			setString(Column.SRV_DATE, checkinDate);

			String checkoutDate = hotelOrderItem.getCheckOutDate().format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
			StringBuilder narr_3 = new StringBuilder(checkinDate).append(" to ").append(checkoutDate);
			setString(Column.NARR_3, narr_3.toString());

			setString("", Column.NARR_4, Column.NARR_5, Column.NARR_6);

			HotelSupplierInfo supplierInfo = hmsCommunicator.getHotelSupplierInfo(hotelOrderItem.getSupplierId());
			String supplierCode = supplierInfo.getCredentialInfo().getAccountingCode();
			setString(Column.SCODE, supplierCode);

			setString(Column.NARRATION, order.getBookingId());

			String ticketNo = hotelOrderItem.getAdditionalInfo().getSupplierBookingReference();
			if (StringUtils.isBlank(ticketNo)) {
				setString(Column.TICKETNO, StringUtils.right(ticketNo, 10));
			}

			TravellerInfo travellerInfo = hotelOrderItem.getRoomInfo().getTravellerInfo().get(0);
			String paxName = new StringBuilder(travellerInfo.getTitle()).append(" ")
					.append(travellerInfo.getFirstName()).append(" ").append(travellerInfo.getLastName()).toString();
			setString(Column.PAX, StringUtils.left(paxName, 30));

			setString("VL", Column.DISC_PAIDM1, Column.DISC_RECDM1); // fixed
			setBigDecimal(BigDecimal.ZERO, Column.DISC_PAIDV1, Column.DISC_PAID1, Column.DISC_RECDV1,
					Column.DISC_RECD1);
			setString(Column.DISC_PAIDM2, "N");

			setBigDecimal(Column.BASIC_C, getFareComponentsAsBigDecimal.apply(HotelFareComponent.TF));
			setBigDecimal(Column.BASIC_S, getFareComponentsAsBigDecimal.apply(HotelFareComponent.BF));

			setString(Column.GST_C_M, "N");
			setString(Column.GST_TYPE, "G");

			// no commercial
			setBigDecimal(BigDecimal.ZERO, Column.TAX_C, Column.TAX_S, Column.SRV_CHRG3C, Column.SRV_CHRG1C,
					Column.TDS_C, Column.SERV1_CS1C, Column.GST_C_R);

			setString(Column.BROK_PAIDM1, "RB");

			setString(Column.SRV_PAIDM2, "B");

			setString(Column.IDATE, hotelOrderItem.getCreatedOn().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
		}

		void setString(Column column, String str) throws SQLException {
			params[column.ordinal()] = str == null ? null : new StringBuilder("'").append(str).append("'").toString();
		}

		void setString(String str, Column... columns) throws SQLException {
			for (Column column : columns) {
				setString(column, str);
			}
		}

		void setBigDecimal(Column column, BigDecimal bigDecimal) throws SQLException {
			params[column.ordinal()] = bigDecimal == null ? null : bigDecimal.toString();
		}

		void setBigDecimal(BigDecimal bigDecimal, Column... columns) throws SQLException {
			for (Column column : columns) {
				setBigDecimal(column, bigDecimal);
			}
		}

		String prepareSql() {
			StringJoiner joiner = new StringJoiner(", ");
			for (CharSequence param : params) {
				joiner.add(param);
			}
			return String.format(sqlTemplate, joiner.toString());
		}
	}

	private static enum Column {
		DOC_PRF,
		DOC_NOS,
		DOC_SRNO,
		IDM_FLAG,
		IL_REF,
		VD_REF,
		IDATE,
		CCODE,
		DCODE,
		ECODE,
		BCODE,
		NARRATION,
		XO_REF,
		LOC_CODE,
		CST_CODE,
		Curcode_C,
		Curcode_S,
		REFR_KEY,
		GCODE,
		SCODE,
		XO_NOS,
		SRV_DATE,
		TICKETNO,
		PAX,
		STX_CENVAT,
		STX_METHOD,
		NARR_1,
		NARR_2,
		NARR_3,
		NARR_4,
		NARR_5,
		NARR_6,
		R_O_E_C,
		R_O_E_S,
		BASIC_C,
		BASIC_S,
		TAX_C,
		TAX_S,
		DISC_PAIDM1,
		DISC_PAIDM2,
		DISC_RECDM1,
		BROK_PAIDM1,
		DISC_PAIDV1,
		DISC_RECDV1,
		BROK_PAIDV1,
		DISC_PAID1,
		DISC_RECD1,
		BROK_PAID1,
		SRV_PAIDM2,
		SRV_CHRG1C,
		SRV_CHRG2C,
		SRV_CHRG3C,
		RAF_C,
		SRV_CHRG1P,
		SRV_CHRG2P,
		SRV_CHRG3P,
		RAF_P,
		SERV_TAXC,
		SERV_EDUC,
		SERV_CS1C,
		TDC_PAIDV1,
		TDS_C,
		SERV_TAXP,
		SERV_EDUP,
		SERV_CS1P,
		TDS_PAIDV1,
		TDS_P,
		TDB_PAIDV1,
		TDS_B,
		GST_C_M,
		GST_C_R,
		Created_By,
		SERV1_CS1C,
		SERV3_CS1C,
		GST_TYPE;

		// All columns separated by comma
		final static String ALL;
		final static int COUNT = values().length;

		static {
			StringJoiner sj = new StringJoiner(", ");
			for (Column column : Column.values()) {
				sj.add(column.name());
			}
			ALL = sj.toString();
		}
	}
}
