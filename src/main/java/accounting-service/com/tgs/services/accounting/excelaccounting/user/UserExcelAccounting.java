package com.tgs.services.accounting.excelaccounting.user;

import java.sql.Connection;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.filters.UserFilter;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.accounting.utils.AccountingUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class UserExcelAccounting {

	final private static int BATCH_SIZE = 1;

	@Autowired
	private AccountingUtils accountingUtils;

	@Autowired
	AbstractUserAccountingStatementPreparation statementPreparation;

	@Autowired
	UserService userService;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	public void pushInvoicesFor(UserFilter userFilter) {
		Connection con = null;
		Statement statement = null;
		try {
			con = accountingUtils.getConnection();
			statement = con.createStatement();
			con.setAutoCommit(false);
			Map<String, String> sqlsLogs = new HashMap<>();
			List<DbUser> usersInBatch = new ArrayList<>();
			List<DbUser> users = userService.search(userFilter);
			log.debug("User ids found {} ",
					users.stream().map(u -> u.getUserId()).collect(Collectors.toList()).toString());
			Iterator<DbUser> usersItr = users.iterator();
			while (usersItr.hasNext()) {
				try {
					DbUser dbUser = usersItr.next();
					if (BooleanUtils.isNotTrue(dbUser.getAdditionalInfo().getIsPushedToAccounting())
							&& BooleanUtils.isTrue(dbUser.getAdditionalInfo().getIsAutoOnBoarding())) {
						// If they have manully added the entry in accounting , we just need to update status
						if (StringUtils.isEmpty(dbUser.getAdditionalInfo().getAccountingCode())) {
							User user = dbUser.toDomain();
							String sql = statementPreparation.prepareInsertSql(dbConf.getUserTableName(), user);
							dbUser.getAdditionalInfo().setAccountingCode(user.getAdditionalInfo().getAccountingCode());

							sqlsLogs.put(user.getUserId(), sql);
							statement.addBatch(sql);
							log.debug("Sql created for userid {} is {}", user.getUserId(), sql);
						}
						usersInBatch.add(dbUser);

					}
					/**
					 * Execute batch if batch-size is crossed or if it is the last user.
					 */
					if (usersInBatch.size() >= BATCH_SIZE || !usersItr.hasNext()) {
						boolean status = true;
						try {
							statement.executeBatch();
							con.commit();
						} catch (Exception e) {
							status = false;
							log.error("Failed to insert user for userId {} due to {} ", sqlsLogs.keySet(),
									e.getMessage(), e);
							for (String key : sqlsLogs.keySet()) {
								LogUtils.log(key, "UserAccounting", e);
							}
						} finally {
							/**
							 * Batch should be cleared even if it fails so that upcoming batches can be served.
							 */
							statement.clearBatch();
							for (DbUser user_local : usersInBatch) {
								user_local.getAdditionalInfo().setIsPushedToAccounting(status);
								userService.save(user_local);
							}
							usersInBatch.clear();
						}
						accountingUtils.userlogSqls(sqlsLogs, status);
						sqlsLogs.clear();
					}
				} catch (Exception e) {
					log.error(
							"Failed to insert users for userFilter {}. Trying to build connections again. Reason: {} ",
							sqlsLogs.keySet(), e.getMessage(), e);
					for (String key : sqlsLogs.keySet()) {
						LogUtils.log(key, "UserAccounting", e);
					}
					accountingUtils.tryClosingResources(con, statement);
					con = accountingUtils.getConnection();
					statement = con.createStatement();
				}
			}
		} catch (Exception e) {
			log.error("Failed to insert users for userFilter {} due to {}", userFilter, e.getMessage(), e);
		} finally {
			LogUtils.clearLogList();
			accountingUtils.tryClosingResources(con, statement);
		}
	}

}
