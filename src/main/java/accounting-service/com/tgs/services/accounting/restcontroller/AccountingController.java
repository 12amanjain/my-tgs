package com.tgs.services.accounting.restcontroller;

import java.time.LocalDateTime;
import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.accounting.servicehandler.AirAccountingServiceHandler;
import com.tgs.services.accounting.servicehandler.AmendmentAccoutingServiceHandler;
import com.tgs.services.accounting.servicehandler.UserAccountingHandler;


@RequestMapping("/accounting/v1/job")
@RestController
public class AccountingController {

	@Autowired
	private AirAccountingServiceHandler accountingServiceHandler;
	
	@Autowired
	private UserAccountingHandler userAccountingHandler;

	@Autowired
	private AmendmentAccoutingServiceHandler amendmentAccountingServiceHandler;

	@RequestMapping(value = "/air/push-invoice", method = RequestMethod.POST)
	protected BaseResponse pushAirInvoices(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.AIR));
		return getResponse(orderFilter);
	}
	
	@RequestMapping(value = "/air/push-esstack", method = RequestMethod.POST)
	protected BaseResponse pushAirAllInvoicesToESStack(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.AIR));
		orderFilter.setIsPushedToAccounting(true);
		if (orderFilter.getProcessedOnAfterDateTime() == null && orderFilter.getProcessedOnBeforeDateTime() == null) {
			orderFilter.setProcessedOnAfterDateTime(LocalDateTime.now().minusMonths(6));
		}
		return accountingServiceHandler.pushRemainigInvoices(orderFilter);
	}

	@RequestMapping(value = "/hotel/push-invoice", method = RequestMethod.POST)
	protected BaseResponse pushHotelInvoices(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.HOTEL));
		return getResponse(orderFilter);
	}

	@RequestMapping(value = "/air/amendment/push-invoice", method = RequestMethod.POST)
	protected BaseResponse pushAirAmendmentInvoices(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AmendmentFilter amendmentFilter) throws Exception {
		amendmentFilter.setOrderTypeIn(Arrays.asList(OrderType.AIR));
		if (amendmentFilter.getProcessedOnGreaterThan() == null && amendmentFilter.getProcessedOnLessThan() == null) {
			amendmentFilter.setProcessedOnGreaterThan(LocalDateTime.now().minusMinutes(60));
			amendmentFilter.setStatusIn(Arrays.asList(AmendmentStatus.SUCCESS));
			// Job does not have any JWT token . Its for internal use only
			amendmentFilter.setIsInternalQuery(true);
		}
		amendmentAccountingServiceHandler.initData(amendmentFilter, new BaseResponse());
		return amendmentAccountingServiceHandler.getResponse();
	}
	
	@RequestMapping(value = "/user/push-invoice", method = RequestMethod.POST)
	protected BaseResponse pushUsers(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody UserFilter userFilter) throws Exception {
		userAccountingHandler.initData(userFilter, new BaseResponse());
		return userAccountingHandler.getResponse();
	}

	private BaseResponse getResponse(OrderFilter orderFilter) throws Exception {
		addDateTimeFilterIfRequired(orderFilter);
		accountingServiceHandler.initData(orderFilter, new BaseResponse());
		return accountingServiceHandler.getResponse();
	}

	private void addDateTimeFilterIfRequired(OrderFilter orderFilter) {
		if (orderFilter.getProcessedOnAfterDateTime() == null && orderFilter.getProcessedOnBeforeDateTime() == null) {
			orderFilter.setProcessedOnAfterDateTime(LocalDateTime.now().minusMinutes(60));
		}
		if (CollectionUtils.isEmpty(orderFilter.getStatuses())) {
			orderFilter.setStatuses(Arrays.asList(OrderStatus.SUCCESS));
		}
	}

}