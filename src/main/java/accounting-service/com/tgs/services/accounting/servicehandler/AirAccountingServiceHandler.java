package com.tgs.services.accounting.servicehandler;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.excelaccounting.air.AirExcelAccounting;
import com.tgs.services.accounting.excelaccounting.hotel.AtlasHotelAccounting;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.OrderFilter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AirAccountingServiceHandler extends ServiceHandler<OrderFilter, BaseResponse> {

	@Autowired
	private AirExcelAccounting airExcelAccounting;

	@Override
	public void beforeProcess() throws Exception {
		if (CollectionUtils.isEmpty(request.getProducts())) {
			throw new CustomGeneralException(SystemError.INVALID_PRODUCT);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			if (request.getProducts().contains(OrderType.AIR)) {
				airExcelAccounting.pushInvoicesFor(request);
			} else if (request.getProducts().contains(OrderType.HOTEL)) {
				AtlasHotelAccounting.pushInvoicesFor(request);
			} else {
				throw new IllegalArgumentException(
						"Accounting not available for this product: " + request.getProducts().get(0));
			}
		} catch (Exception e) {
			log.error("Pushing Invoice failed for {} due to {}", request, e);
			throw new CustomGeneralException(SystemError.INVOICE_FAILED);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

	public BaseResponse pushRemainigInvoices(OrderFilter orderFilter) {
		BaseResponse response = new BaseResponse();
		try {
			airExcelAccounting.pushInvoicestoESStack(orderFilter);
		} catch (Exception e) {
			log.error("Failed to push invoices to esStack for filter {}", orderFilter);
			response.addError(new ErrorDetail(SystemError.UNABLE_TO_PUSH_DATA_TO_ESSTACK));
		}
		return response;
	}

}
