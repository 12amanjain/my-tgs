package com.tgs.services.accounting.servicehandler;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.tgs.filters.AmendmentFilter;
import com.tgs.services.accounting.excelaccounting.amendment.AirAmendmentExcelAccounting;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AmendmentAccoutingServiceHandler extends ServiceHandler<AmendmentFilter, BaseResponse> {

	@Autowired
	AirAmendmentExcelAccounting airExcelAccounting;

	@Override
	public void beforeProcess() throws Exception {
		if (CollectionUtils.isEmpty(request.getOrderTypeIn())) {
			throw new CustomGeneralException(SystemError.INVALID_PRODUCT);
		}

	}

	@Override
	public void process() throws Exception {
		try {
			if (request.getOrderTypeIn().contains(OrderType.AIR)) {
				airExcelAccounting.pushInvoicesFor(request);
			}
		} catch (Exception e) {
			log.error("Pushing Invoice failed for {} due to {}", GsonUtils.getGson().toJson(request), e.getMessage(),
					e);
			throw new CustomGeneralException(SystemError.INVOICE_FAILED);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

}
