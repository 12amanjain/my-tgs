package com.tgs.services.accounting.servicehandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.accounting.excelaccounting.user.UserExcelAccounting;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.BaseResponse;

@Service
public class UserAccountingHandler extends ServiceHandler<UserFilter, BaseResponse> {

	@Autowired
	UserExcelAccounting userExcelAccounting;

	@Override
	public void beforeProcess() throws Exception {
		if (request == null)
			return;
		request.setRoles(Arrays.asList(UserRole.AGENT, UserRole.CORPORATE));
		if (request.getProcessedOnGreaterThan() == null && request.getProcessedOnLessThan() == null) {
			request.setProcessedOnGreaterThan(LocalDateTime.now().minusMinutes(15));
		}
	}

	@Override
	public void process() throws Exception {
		userExcelAccounting.pushInvoicesFor(request);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
