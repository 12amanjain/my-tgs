package com.tgs.services.accounting.utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.tgs.services.accounting.conf.AccoutingDbConfiguration;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import lombok.extern.slf4j.Slf4j;

@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
@Component
@Slf4j
public class AccountingUtils {

	private static final long LOGS_TTL = 7 * 24 * 60 * 60;
	
	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	private AccoutingDbConfiguration dbConf;

	public Connection getConnection() {
		int attempt = 0;
		while (attempt++ < 3) {
			try {
				Thread.sleep(1000);
				// check driver's availability
				Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");

				String url = "jdbc:sqlserver://" + dbConf.getServerAddress() + ";database=" + dbConf.getDatabaseName();
				return DriverManager.getConnection(url, dbConf.getUsername(), dbConf.getPassword());
			} catch (Exception e) {
				log.error("Not able to get connection, trying again , attemptNum {}", attempt, e);
			}
		}
		throw new RuntimeException("Unable to connect to database");
	}

	public void tryClosingResources(Connection con, Statement statement) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				log.error("Failed to close connection for request {} due to ", e);
			}
		}
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				log.error("Failed to close statement for request {} due to ", e);
			}
		}
	}
	
	public ClientGeneralInfo getClientInfo() {
		ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		return clientInfo;
	}

	public void logSqls(Map<String, List<String>> sqlsLogs, boolean status) {
		for (Entry<String, List<String>> sqlsLog : sqlsLogs.entrySet()) {
			String logId = sqlsLog.getKey();
			log.info("Sqls for id {} are {}", sqlsLog.getKey(), sqlsLog.getValue());
			LogData sqlLogData, sqlStatusData;
			sqlLogData = LogData.builder().type("SQLs").ttl(LOGS_TTL).key(logId).logData(getSqlsLog(sqlsLog.getValue()))
					.logType("AirSupplierAPILogs").build();
			sqlStatusData = LogData.builder().type("Status").ttl(LOGS_TTL).key(logId).logType("AirSupplierAPILogs")
					.logData(status && !CollectionUtils.isEmpty(sqlsLog.getValue()) ? "SUCCESS" : "FAILED").build();
			LogUtils.addToLogList(sqlLogData);
			LogUtils.addToLogList(sqlStatusData);
		}
		LogUtils.clearLogList();
	}
	
	public void userlogSqls(Map<String, String> sqlsLogs, boolean status) {
		for (Entry<String, String> sqlsLog : sqlsLogs.entrySet()) {
			String logId = sqlsLog.getKey();
			log.info("Sqls for id {} are {}", sqlsLog.getKey(), sqlsLog.getValue());
			LogData sqlLogData, sqlStatusData;
			sqlLogData = LogData.builder().type("SQLs").ttl(LOGS_TTL).key(logId).logData(sqlsLog.getValue())
					.logType("AirSupplierAPILogs").build();
			sqlStatusData = LogData.builder().type("Status").ttl(LOGS_TTL).key(logId).logType("AirSupplierAPILogs")
					.logData(status && !StringUtils.isEmpty(sqlsLog.getValue()) ? "SUCCESS" : "FAILED").build();
			LogUtils.addToLogList(sqlLogData);
			LogUtils.addToLogList(sqlStatusData);
		}
		LogUtils.clearLogList();
	}

	protected String getSqlsLog(List<String> sqls) {
		if (CollectionUtils.isEmpty(sqls)) {
			return "No SQLs created";
		}
		StringJoiner sj = new StringJoiner("\n\n");
		for (String sql : sqls) {
			sj.add(sql);
		}
		return sj.toString();
	}

}

