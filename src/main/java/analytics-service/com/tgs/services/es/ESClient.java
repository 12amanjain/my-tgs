package com.tgs.services.es;

import org.apache.http.HttpHost;
import org.apache.http.client.config.RequestConfig;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ESClient {

	@Autowired
	ESConfiguration configuration;
	
    @Bean
	@ConditionalOnProperty(name = "esstack.isUsed", havingValue = "false", matchIfMissing= true)
	public RestHighLevelClient esInit() {
		return null;
	}

	@Bean
	@ConditionalOnProperty(name = "esstack.isUsed", havingValue = "true")
	public RestHighLevelClient esClient() {
		RestHighLevelClient restClient = null;
		try {
			ESConfiguration.ESConfigurationData config = configuration.getConfData();

			restClient = new RestHighLevelClient(RestClient
					.builder(
							new HttpHost(config.getEsHost(), Integer.valueOf(config.getEsPort()), config.getEsScheme()))
					.setRequestConfigCallback(new RestClientBuilder.RequestConfigCallback() {
						@Override
						public RequestConfig.Builder customizeRequestConfig(
								RequestConfig.Builder requestConfigBuilder) {
							return requestConfigBuilder.setConnectTimeout(5000).setSocketTimeout(60000);
						}
					}).setMaxRetryTimeoutMillis(60000));
		} catch (Exception e) {
			log.error("Error While Creating Rest Client connection {}", e);
		}
		return restClient;
	}
}