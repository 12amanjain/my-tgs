package com.tgs.services.es;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import com.tgs.services.base.gson.GsonUtils;

import lombok.Getter;

@Configuration
@Component
public class ESConfiguration {


    @Getter
    public static class ESConfigurationData {
        private String esHost;
        private String esPort;
        private String esClusterName;
        private String esScheme;
    }

    @Value("${esstack.conf}")
    private String conf;

    private ESConfigurationData confData;

    public ESConfigurationData getConfData() {
        if (confData == null) {
            return GsonUtils.getGson().fromJson(conf, ESConfigurationData.class);
        }

        return confData;
    }

}