package com.tgs.services.es.communicator.impl;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.helper.ElasticSearchHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ESCommunicatorImpl implements ElasticSearchCommunicator {

	@Autowired
	ElasticSearchHelper esHelper;

	@Override
	public void addDocument(@NotEmpty Object input, ESMetaInfo metaInfo) {
		esHelper.addDocument(input, metaInfo);
	}

	@Override
	public void addBulkDocument(List<? extends Object> input, ESMetaInfo metaInfo) {
		esHelper.addBulkDocument(input, metaInfo);
	}

	@Override
	public void addBulkDocuments(@NotNull List<? extends Object> documents, ESMetaInfo esMetaInfo) {
		esHelper.addBulkDocuments(documents, esMetaInfo);
	}

	@Override
	public ESAutoSuggestionResponse getSuggestions(ESSearchRequest sRequest) {
		ESAutoSuggestionResponse suggestionResponse = new ESAutoSuggestionResponse();
		suggestionResponse.setSuggestions(esHelper.searchDocument(sRequest.getSource(), sRequest.getMetaInfo()));
		return suggestionResponse;
	}

	@Override
	public void deleteIndex(ESMetaInfo metaInfo) {
		esHelper.deleteIndex(metaInfo);
	}
}
