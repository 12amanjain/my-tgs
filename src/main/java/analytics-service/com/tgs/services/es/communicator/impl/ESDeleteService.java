package com.tgs.services.es.communicator.impl;

import com.tgs.services.es.utils.ESUtils;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.Requests;
import org.springframework.stereotype.Service;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.servicehandler.ESServiceHandler;
import lombok.extern.slf4j.Slf4j;
import javax.validation.constraints.NotNull;
import java.io.IOException;

@Slf4j
@Service
public class ESDeleteService extends ESServiceHandler {

	@Deprecated
	public void deleteIndex(@NotNull ESMetaInfo metaInfo) {
		DeleteIndexRequest deleteIndexRequest = Requests.deleteIndexRequest(ESUtils.getIndex(metaInfo, getSetPrefix()));
		try {
			client.indices().deleteAsync(deleteIndexRequest, bulkActionResponseListener());
		} catch (Exception e) {
			log.error("Unable to delete index ", e);
		}
	}


	public void deleteIndexAsync(@NotNull ESMetaInfo metaInfo) {
		DeleteIndexRequest deleteIndexRequest = Requests.deleteIndexRequest(ESUtils.getIndex(metaInfo, getSetPrefix()));
		client.indices().deleteAsync(deleteIndexRequest, bulkActionResponseListener());
	}

	public ActionListener<DeleteIndexResponse> bulkActionResponseListener() {
		ActionListener<DeleteIndexResponse> bulkResponseActionListener = new ActionListener<DeleteIndexResponse>() {

			@Override
			public void onResponse(DeleteIndexResponse deleteIndexResponse) {
				if (deleteIndexResponse.isAcknowledged()) {
					log.info("Index Acknowledged & Deleted ");
				}
			}

			@Override
			public void onFailure(Exception e) {
				log.error("Indices Delete Failed ", e);
			}
		};
		return bulkResponseActionListener;
	}

}
