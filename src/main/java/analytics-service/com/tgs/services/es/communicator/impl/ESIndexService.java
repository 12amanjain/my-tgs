package com.tgs.services.es.communicator.impl;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import org.elasticsearch.action.ActionListener;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.bulk.BulkItemResponse;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.Requests;
import org.springframework.stereotype.Service;
import com.tgs.services.base.LogData;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.helper.Document;
import com.tgs.services.es.servicehandler.IndexServiceHandler;
import com.tgs.services.es.utils.ESUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ESIndexService extends IndexServiceHandler {

	public void createIndex(@NotNull ESMetaInfo metaInfo) throws IOException {
		CreateIndexRequest indexRequest = Requests.createIndexRequest(ESUtils.getIndex(metaInfo, getSetPrefix()));
		client.indices().createAsync(indexRequest, bulkCreateIndexActionResponseListener());
	}

	public void addDocument(@NotNull Object value, @NotNull ESMetaInfo esMetaInfo) {
		Document<Object> document = new Document<Object>(value);
		if (esMetaInfo != null && document != null) {
			IndexRequest indexRequest = ESUtils.createIndexRequest(document, esMetaInfo, getSetPrefix());
			client.indexAsync(indexRequest, indexActionResponseListener());
		}
	}

	public void addBulkDocuments(@NotEmpty List<? extends Object> documents, @NotNull ESMetaInfo esMetaInfo) {
		BulkRequest bulkRequest = new BulkRequest();
		documents.forEach(value -> {
			Document<Object> document = new Document<Object>(value);
			IndexRequest indexRequest = ESUtils.createIndexRequest(document, esMetaInfo, getSetPrefix());
			bulkRequest.add(indexRequest);
		});
		client.bulkAsync(bulkRequest, bulkActionResponseListener());
	}

	public void addDocumentsInSingleClient(@NotEmpty List<? extends Object> documents, @NotNull ESMetaInfo esMetaInfo) {
		documents.forEach(value -> {
			Document<Object> document = new Document<Object>(value);
			IndexRequest indexRequest = ESUtils.createIndexRequest(document, esMetaInfo, getSetPrefix());
			try {
				IndexResponse response = client.index(indexRequest);
			} catch (IOException e) {
				log.error("Indexing Document Failed for {} ", document, e);
			}
		});
		log.info("Total Documents Updated Index {} Size {}", esMetaInfo.getIndex(), documents.size());
	}

	public void addSupplierLogs(@NotEmpty List<LogData> logDatas, @NotNull ESMetaInfo esMetaInfo) throws IOException {
		AtomicInteger count = new AtomicInteger(1);
		logDatas.forEach(value -> {
			IndexRequest indexRequest = ESUtils.createIndexRequestForLogdata(value, esMetaInfo, getSetPrefix());
			try {
				indexRequest.id(indexRequest.id() + "_" + count);
				IndexResponse indexResponse = client.index(indexRequest);
				count.getAndIncrement();
			} catch (IOException e) {
				log.error("Indexing Document {} Failed ", indexRequest, e);
			}
			log.info("Supplier Logs Uploaded Index {} Size {} ", esMetaInfo.getIndex(), logDatas.size());
		});
	}

	@Deprecated
	public void addOneByOneDocument(@NotEmpty List<? extends Object> documents, @NotNull ESMetaInfo esMetaInfo)
			throws IOException {
		documents.forEach(value -> {
			Document<Object> document = new Document<Object>(value);
			IndexRequest indexRequest = ESUtils.createIndexRequest(document, esMetaInfo, getSetPrefix());
			client.indexAsync(indexRequest, indexActionResponseListener());
		});
	}


	private ActionListener<IndexResponse> indexActionResponseListener() {
		ActionListener<IndexResponse> bulkResponseActionListener = new ActionListener<IndexResponse>() {

			@Override
			public void onResponse(IndexResponse indexResponse) {
				// TODO
			}

			@Override
			public void onFailure(Exception e) {
				log.error("Adding | Updating Failed {}", e);
			}
		};
		return bulkResponseActionListener;
	}

	public ActionListener<CreateIndexResponse> bulkCreateIndexActionResponseListener() {
		ActionListener<CreateIndexResponse> bulkResponseActionListener = new ActionListener<CreateIndexResponse>() {

			@Override
			public void onResponse(CreateIndexResponse createIndexResponse) {
				if (createIndexResponse.isAcknowledged()) {
					// TODO
				}
			}

			@Override
			public void onFailure(Exception e) {
				log.error("Indices Creation Failed", e);
			}
		};
		return bulkResponseActionListener;
	}


	public static ActionListener<BulkResponse> bulkActionResponseListener() {
		ActionListener<BulkResponse> bulkResponseActionListener = new ActionListener<BulkResponse>() {
			@Override
			public void onResponse(BulkResponse bulkItemResponses) {

				BulkItemResponse[] itemResponses = bulkItemResponses.getItems();

				Arrays.stream(itemResponses).forEach(bulkItemResponse -> {
					if (bulkItemResponse.isFailed()) {
						log.error("Failed ID", bulkItemResponse.getId());
					}
				});

				// even if any one has failure then it will say failure
				if (bulkItemResponses.hasFailures()) {
					log.error(bulkItemResponses.buildFailureMessage());
				}
			}

			@Override
			public void onFailure(Exception e) {
				log.error("Data Processed Failed", e);
			}
		};
		return bulkResponseActionListener;
	}


	public void executeBulkIndexRequest(BulkRequest bulkRequest) throws IOException {
		client.bulkAsync(bulkRequest, bulkActionResponseListener());
	}

}
