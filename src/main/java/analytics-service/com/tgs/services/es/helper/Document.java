package com.tgs.services.es.helper;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Document<T> {

    T input;

    public Document(T input) {
        this.input = input;
    }

    public T getInput() {
        return this.input;
    }
}