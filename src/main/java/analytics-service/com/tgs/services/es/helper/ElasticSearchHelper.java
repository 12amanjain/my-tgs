package com.tgs.services.es.helper;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.LogData;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import org.apache.commons.lang3.time.StopWatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.es.communicator.impl.ESDeleteService;
import com.tgs.services.es.communicator.impl.ESIndexService;
import com.tgs.services.es.communicator.impl.ESSearchService;

import lombok.extern.slf4j.Slf4j;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Slf4j
@Service
public class ElasticSearchHelper {

	@Autowired
	ESIndexService indexService;

	@Autowired
	ESDeleteService deleteService;

	@Autowired
	ESSearchService searchService;

	@Autowired
	ApplicationContext context;

	@Value("${setPrefix}")
	private String setPrefix;

	public void addDocument(@NotNull Object input, @NotNull ESMetaInfo metaInfo) {
		indexService.addDocument(input, metaInfo);
	}

	@Deprecated
	public void addBulkDocument(@NotNull List<? extends Object> input, @NotNull ESMetaInfo metaInfo) {
		indexService.addDocumentsInSingleClient(input, metaInfo);
	}

	public void addBulkDocuments(@NotNull List<? extends Object> input, @NotNull ESMetaInfo metaInfo) {
		indexService.addBulkDocuments(input, metaInfo);
	}

	public void addSupplierLogs(@NotEmpty List<LogData> logDatas, @NotNull ESMetaInfo metaInfo) throws Exception {
		indexService.addSupplierLogs(logDatas, metaInfo);
	}

	public List<Map<String, ? extends Object>> searchDocument(@NotNull String source, @NotNull String esMetaInfo) {
		ESMetaInfo searchMetaInfo = ESMetaInfo.getESMetaInfo(esMetaInfo);
		return searchService.searchDocument(source, searchMetaInfo);
	}

	public void createIndex(ESMetaInfo esMetaInfo) throws IOException {
		indexService.createIndex(esMetaInfo);
	}

	public void deleteIndex(ESMetaInfo esMetaInfo) {
		deleteService.deleteIndex(esMetaInfo);
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 7 * 24 * 60 * 60 * 1000)
	public void reloadAll() throws Exception {
		log.info("Doing Auto Reload of ElasticSearch Static Data");
		doReload("load", "all");
	}

	public BaseResponse doReload(String operationType, String reloadType) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			Set<Class<? extends ESStackInitializer>> allIntializers = new HashSet<>();
			if ("all".equalsIgnoreCase(reloadType)) {
				allIntializers =
						TgsObjectUtils.findAllMatchingInheritedTypes(ESStackInitializer.class, "com.tgs.services");
			} else {
				allIntializers.add((Class<? extends ESStackInitializer>) Class.forName(reloadType));
			}
			reload(allIntializers);
		} catch (Exception e) {
			log.error("ElasticSearch static Reload Failed  for ", e);
			baseResponse.addError(ErrorDetail.builder().message("Elastic Search Reload Failed").errCode("").build());
			baseResponse.getStatus().setSuccess(false);
		}
		return baseResponse;
	}

	public void reload(Set<Class<? extends ESStackInitializer>> allIntializers) {
		StopWatch stopWatch = new StopWatch();
		allIntializers.forEach(intializer -> {
			try {
				stopWatch.start();
				log.debug("Intializing {}", intializer.getClass());
				Object bean = context.getBean(Class.forName(intializer.getName()));
				Method method = bean.getClass().getMethod("initialize", String.class);
				method.invoke(bean, intializer.getName());
				log.debug("Total time took to intialize {} , is {} ", intializer.getClass(), stopWatch.getTime());
			} catch (Exception e) {
				log.error("Unable to intialize {}", intializer.getClass(), e);
			} finally {
				stopWatch.stop();
				stopWatch.reset();
			}
		});
	}

}
