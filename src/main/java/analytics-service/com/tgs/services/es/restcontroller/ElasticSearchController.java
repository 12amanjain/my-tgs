package com.tgs.services.es.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.helper.ElasticSearchHelper;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import com.tgs.services.es.servicehandler.ElasticSearchHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequestMapping("/es/v1")
@RestController
public class ElasticSearchController {

	@Autowired
	ElasticSearchHandler esHandler;

	@Autowired
	ElasticSearchHelper esHelper;

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/es-reload/{operation_type}/{type}", method = RequestMethod.GET)
	protected BaseResponse reloadAll(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("operation_type") String operationType, @PathVariable("type") String type) throws Exception {
		return esHelper.doReload(operationType, type);
	}
	

	/*
	 * @RequestMapping(value = "/fms/v1/{metaInfo}/{source}", method = RequestMethod.GET) protected
	 * ESAutoSuggestionResponse getSearchResponse(HttpServletRequest request, HttpServletResponse httpResoponse,
	 * 
	 * @PathVariable("metaInfo") String metaInfo, @PathVariable("source") String source) throws Exception {
	 * ESAutoSuggestionResponse autoSuggestionResponse = new ESAutoSuggestionResponse(); List<Map<String, ? extends
	 * Object>> esResponse = esHandler.getSearchResponse(metaInfo, source);
	 * autoSuggestionResponse.setSuggestions(esResponse); return autoSuggestionResponse; }
	 */

	@RequestMapping(value = "/{metaInfo}/{source}", method = RequestMethod.GET)
	protected ESAutoSuggestionResponse getSearchSuggestions(HttpServletRequest request,
			HttpServletResponse httpResponse, @PathVariable("metaInfo") String metaInfo,
			@PathVariable("source") String source) throws Exception {
		ESSearchRequest searchRequest = ESSearchRequest.builder().metaInfo(metaInfo).source(source).build();
		esHandler.initData(searchRequest, new ESAutoSuggestionResponse());
		return esHandler.getResponse();
	}

}
