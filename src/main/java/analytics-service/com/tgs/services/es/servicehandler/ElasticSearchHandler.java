package com.tgs.services.es.servicehandler;

import java.util.List;
import java.util.Map;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.es.helper.ElasticSearchHelper;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ElasticSearchHandler extends ServiceHandler<ESSearchRequest, ESAutoSuggestionResponse> {


	@Autowired
	ElasticSearchHelper esHelper;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		List<Map<String, ? extends Object>> suggestions =
				esHelper.searchDocument(request.getSource(), request.getMetaInfo());
		response.setSuggestions(suggestions);
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public List<Map<String, ? extends Object>> getSearchResponse(String metaInfo, String source) {
		try {
			return esHelper.searchDocument(source, metaInfo);
		} catch (Exception e) {
			log.error("Search failed {}", e);
		}
		return null;
	}
}
