package com.tgs.services.es.servicehandler;

import org.elasticsearch.client.RestHighLevelClient;

import com.tgs.services.es.helper.Document;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndexServiceHandler<T> extends ESServiceHandler {

    protected Document input;

    public void initData(Document request, RestHighLevelClient client) {
        this.input = request;
    }

    public void initData(Document request) {
        this.input = request;
    }

}