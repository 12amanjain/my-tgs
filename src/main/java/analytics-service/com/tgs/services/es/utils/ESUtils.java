package com.tgs.services.es.utils;

import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import com.tgs.services.es.helper.Document;
import com.tgs.services.es.datamodel.ESMetaInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ESUtils {

	public static List<IndexRequest> createIndexRequests(Document request, ESMetaInfo esMetaInfo, String prefix) {
		List<IndexRequest> requests = new ArrayList<>();
		requests.add(createIndexRequest(request, esMetaInfo, prefix));
		return requests;
	}

	public static IndexRequest createIndexRequest(Document model, ESMetaInfo esMetaInfo, String prefix) {
		IndexRequest request = new IndexRequest(ESUtils.getIndex(esMetaInfo, prefix), esMetaInfo.getType());
		String source = convertToSource(model);
		request.source(source, XContentType.JSON);
		request.id(getID(source));
		return request;
	}

	public static IndexRequest createIndexRequestForLogdata(LogData logData, ESMetaInfo esMetaInfo, String prefix) {
		IndexRequest request = new IndexRequest(ESUtils.getIndex(esMetaInfo, prefix), esMetaInfo.getType());
		request.id(logData.getKey());
		Document<Object> document = new Document<Object>(logData);
		request.source(convertToSource(document), XContentType.JSON);
		return request;
	}


	@Deprecated
	public static String getID(String source) {
		if (StringUtils.isNotBlank(source)) {
			Map<String, String> objMap = GsonUtils.getGson().fromJson(source, (Type) Object.class);
			if (MapUtils.isNotEmpty(objMap) &&  objMap.containsKey("id")) {
				return String.valueOf(objMap.get("id"));
			}
		}
		return RandomStringUtils.random(4, true, true);
	}

	public static String convertToSource(Document obj) {
		Map<String, Object> result = new HashMap<>();
		try {
			BeanInfo info = Introspector.getBeanInfo(obj.getClass());
			for (PropertyDescriptor pd : info.getPropertyDescriptors()) {
				Method reader = pd.getReadMethod();
				if (reader != null && !pd.getName().equals("class")) {
					result.put(pd.getName(), reader.invoke(obj));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return GsonUtils.getGson().toJson(result.get("input"));
	}

	public static String convertToJSON(Map<String, Object> input) {
		return GsonUtils.getGson().toJson(input);
	}

	public static String getIndex(ESMetaInfo metaInfo, String setPrefix) {
		return StringUtils.join(setPrefix, "_", metaInfo.getIndex());
	}
}
