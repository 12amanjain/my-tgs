package com.tgs.services.messagebroker.communicator;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.messagebroker.servicehandler.KafkaHandler;


@Service
public class KafkaServiceCommunicatorImpl implements KafkaServiceCommunicator {

    @Autowired
    KafkaHandler kafkaHandler;

    @Override
    public void queue(QueueDataType queueType, QueueData queueData) {
        if (queueType == null) {
            queueType = QueueDataType.ELASTICSEARCH;
        }
        kafkaHandler.queue(queueType, queueData);
    }

    @Override
    public void queue(List<QueueDataType> queueTypes, QueueData queueData) {
        queueTypes.forEach(queueType -> {
            kafkaHandler.queue(queueType, queueData);
        });
    }
}
