package com.tgs.services.messagebroker.config.kafka;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaAdmin;
import org.springframework.stereotype.Component;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.gson.GsonUtils;

import lombok.Getter;

@Component
public class KafkaConfiguration {

	@Value("${kafka.conf}")
	private String bootStrapServers;

	private List<KafkaConfigurationData> kafkaAddress;

	@Bean
	public KafkaAdmin kafkaAdmin() {
		Map<String, Object> configObj = new HashMap<>();
		configObj.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, getDefaultReference().getHost());
		configObj.put(AdminClientConfig.RETRIES_CONFIG, "1");
		configObj.put(AdminClientConfig.RECONNECT_BACKOFF_MS_CONFIG, "10000");
		return new KafkaAdmin(configObj);
	}

	public KafkaConfigurationData getDefaultReference() {
		List<KafkaConfigurationData> configDatas = getKafkaAddress();
		return configDatas.stream().filter(topicReference -> StringUtils.isBlank(topicReference.getTopic())).findFirst()
				.orElse(null);
	}

	@Getter
	public static class KafkaConfigurationData {
		private String host;
		private String topic;
	}

	public List<KafkaConfigurationData> getKafkaAddress() {
		if (kafkaAddress == null) {
			kafkaAddress = GsonUtils.getGson().fromJson(bootStrapServers,
					new TypeToken<List<KafkaConfigurationData>>() {
					}.getType());
		}
		if (CollectionUtils.isNotEmpty(kafkaAddress)) {
			return kafkaAddress;
		}
		return null;
	}
}
