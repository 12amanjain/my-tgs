package com.tgs.services.cacheservice.aerospike;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.aerospike.config.AbstractAerospikeDataConfiguration;
import org.springframework.stereotype.Component;

import com.aerospike.client.Host;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.gson.GsonUtils;

import lombok.Getter;

@Configuration
@Component
public class AerospikeConfiguration extends AbstractAerospikeDataConfiguration {

	@Getter
	public static class AerospikeConfigurationData {
		private String host;
		private Map<String, String> namespaceMapping;
	}

	@Value("${aerospike.conf}")
	private String conf;

	private List<AerospikeConfigurationData> confList;

	@SuppressWarnings("serial")
	@Override
	protected Collection<Host> getHosts() {
		if (confList == null) {
			confList = GsonUtils.getGson().fromJson(conf, new TypeToken<List<AerospikeConfigurationData>>() {
			}.getType());
		}
		List<Host> hosts = new ArrayList<>();
		for (AerospikeConfigurationData conf : confList) {
			hosts.add(new Host(conf.getHost(), 3000));
		}
		return hosts;
	}

	@Override
	protected String nameSpace() {
		return getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
	}

	@SuppressWarnings("serial")
	public String getMappedNameSpace(String nameSpace) {
		if (confList == null) {
			confList = GsonUtils.getGson().fromJson(conf, new TypeToken<List<AerospikeConfigurationData>>() {
			}.getType());
		}
		String nameSpaceMapping = null;
		for (AerospikeConfigurationData conf : confList) {
			if (nameSpaceMapping == null)
				nameSpaceMapping = conf.getNamespaceMapping().get(nameSpace.toLowerCase());
		}
		return ObjectUtils.firstNonNull(nameSpaceMapping, "test");
	}

}
