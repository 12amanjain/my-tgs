package com.tgs.services.cacheservice.aerospike;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.CollectionUtils;

import com.aerospike.client.Bin;
import com.aerospike.client.Record;
import com.aerospike.client.Value;
import com.aerospike.client.policy.RecordExistsAction;
import com.aerospike.client.policy.WritePolicy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.utils.encryption.KryoUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AerospikeHelper {

	public static WritePolicy getWritePolicy(int expiration, CacheMetaInfo cacheMetaInfo) {
		WritePolicy wpolicy = new WritePolicy();
		wpolicy.expiration = expiration;
		wpolicy.sendKey = true;
		if (cacheMetaInfo != null && BooleanUtils.isTrue(cacheMetaInfo.getCreateOnly())) {
			wpolicy.recordExistsAction = RecordExistsAction.CREATE_ONLY;
		}
		return wpolicy;
	}

	public static WritePolicy getWritePolicy(int expiration) {
		return getWritePolicy(expiration, null);
	}

	public static <V> List<Bin> generateBinList(Map<String, V> binMap, boolean compress, boolean plainData,
			Boolean kyroCompress) {
		List<Bin> binList = new ArrayList<>();
		if (compress) {
			binList.add(new Bin(BinName.COMPRESS.getName(), compress));
		} else if (plainData) {
			binList.add(new Bin(BinName.PLAINDATA.getName(), plainData));
		}

		for (Entry<String, V> entrySet : binMap.entrySet()) {
			if (BooleanUtils.isTrue(kyroCompress)) {
				byte[] bArray = KryoUtils.serialize(entrySet.getValue(), true).get();
				// log.info("Byte Array size after kyro compress is {}", bArray.length);
				binList.add(new Bin(entrySet.getKey(), bArray));
			} else if (compress) {
				byte[] bArray = KryoUtils.serialize(entrySet.getValue()).get();
				binList.add(new Bin(entrySet.getKey(), bArray));
			} else if (plainData) {
				binList.add(new Bin(entrySet.getKey(), entrySet.getValue()));
			} else {
				binList.add(new Bin(entrySet.getKey(), GsonUtils.getGson().toJson(entrySet.getValue())));
			}
		}
		return binList;
	}

	public static <V> List<Bin> generateBinList(Map<String, V> binMap, boolean compress, boolean plainData) {
		return generateBinList(binMap, compress, plainData, null);
	}

	public static <V> List<Value> generateValueList(String bin, List<V> values, CacheMetaInfo metaInfo) {
		List<Value> valueList = new ArrayList<>();
		for (V value : values) {
			if (BooleanUtils.isTrue(metaInfo.getKyroCompress())) {
				byte[] bArray = KryoUtils.serialize(value, true).get();
				// log.info("Byte Array size after kyro compress is {}, estimated size is {}",
				// bArray.length,
				// Value.get(bArray).estimateSize());
				valueList.add(Value.get(bArray));
			} else if (BooleanUtils.isTrue(metaInfo.getCompress())) {
				byte[] bArray = KryoUtils.serialize(value).get();
				// log.info("Byte Array size after compress is {}, estimated size is {}",
				// bArray.length,
				// Value.get(bArray).estimateSize());
				valueList.add(Value.get(bArray));
			} else if (BooleanUtils.isTrue(metaInfo.getPlainData())) {
				valueList.add(Value.get(value));
			} else {
				valueList.add(Value.get(GsonUtils.getGson().toJson(value)));
			}
		}
		return valueList;
	}
	
	public static <K, V> Map<Value, Value> generateValueMap(String bin, Map<K,V> values, CacheMetaInfo metaInfo) {
		Map<Value, Value> valueMap = new HashMap<>();
		for (Map.Entry<K, V> value : values.entrySet()) {
			if (BooleanUtils.isTrue(metaInfo.getKyroCompress())) {
				byte[] bArray = KryoUtils.serialize(value.getValue(), true).get();
				valueMap.put(Value.get(value.getKey()), Value.get(bArray));
			} else if (BooleanUtils.isTrue(metaInfo.getCompress())) {
				byte[] bArray = KryoUtils.serialize(value).get();
				valueMap.put(Value.get(value.getKey()), Value.get(bArray));
			} else if (BooleanUtils.isTrue(metaInfo.getPlainData())) {
				valueMap.put(Value.get(value.getKey()), Value.get(value));
			} else {
				valueMap.put(Value.get(value.getKey()), Value.get(GsonUtils.getGson().toJson(value)));
			}
		}
		return valueMap;
	}

	public static <V> Map<String, V> convertingBinMapToUserDefinedMap(Record record, Class<V> classofV,
			CacheMetaInfo metaInfo) {
		Map<String, V> vMap = new HashMap<>();
		if (record != null && !CollectionUtils.isEmpty(record.bins)) {
			for (Entry<String, Object> iter : record.bins.entrySet()) {
				if (iter.getKey().equals(BinName.COMPRESS.getName())
						|| iter.getKey().equals(BinName.PLAINDATA.getName())) {
					continue;
				}
				vMap.put(iter.getKey(), getValue(iter.getValue(), record, classofV, metaInfo, metaInfo.getTypeOfT()));
			}
		}
		return vMap;
	}

	public static <V> Map<String, List<V>> convertingBinMapToUserDefinedMapList(CacheMetaInfo metaInfo, Record record,
			Class<V> classofV, Boolean isCompress, Boolean isPlainData) {
		Map<String, List<V>> vMap = new HashMap<>();
		if (record != null && !CollectionUtils.isEmpty(record.bins)) {
			for (Entry<String, Object> iter : record.bins.entrySet()) {
				if (iter.getKey().equals(BinName.COMPRESS.getName())
						|| iter.getKey().equals(BinName.PLAINDATA.getName())) {
					continue;
				}
				if (iter.getValue() instanceof List) {
					List<?> list = record.getList(iter.getKey());
					for (Object o : list) {
						if (vMap.get(iter.getKey()) == null) {
							vMap.put(iter.getKey(), new ArrayList<>());
						}
						vMap.get(iter.getKey()).add(getValue(o, record, classofV, metaInfo, metaInfo.getTypeOfT()));
					}
				}
			}
		}
		return vMap;
	}

	public static <V> V getValue(Object data, Record record, Class<V> classofV, CacheMetaInfo metaInfo, Type type) {
		if (BooleanUtils.isTrue(metaInfo.getKyroCompress())) {
			return KryoUtils.deserialize((byte[]) data, classofV, true).get();
		} else if (record.getBoolean(BinName.COMPRESS.getName()) || BooleanUtils.isTrue(metaInfo.getCompress())) {
			return KryoUtils.deserialize((byte[]) data, classofV).get();
		} else if (record.getBoolean(BinName.PLAINDATA.getName()) || BooleanUtils.isTrue(metaInfo.getPlainData())) {
			return (V) data;
		} else {
			if (type != null) {
				return GsonUtils.getGson().fromJson(data.toString(), type);
			}
			return GsonUtils.getGson().fromJson((String) data, classofV);
		}
	}

}
