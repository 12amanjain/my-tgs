package com.tgs.services.cacheservice.aerospike;

public enum AerospikeNamespace {
	STATIC_MAP, FLIGHT, HOTEL, GENERAL_PURPOSE, LOGS, USERS, FLIGHT_CACHE , HOTEL_CACHE;
}
