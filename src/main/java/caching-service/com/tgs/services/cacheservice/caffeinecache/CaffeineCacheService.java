package com.tgs.services.cacheservice.caffeinecache;


import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleValueWrapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;


@Slf4j
@Service
public class CaffeineCacheService {

    @Autowired
    CacheManager cacheManager;

    private static final String defaultMap = "DEFAULT";


    /**
     * Returns void
     *
     * @implSpec : This will clear ConcurrentHashMap(Cache) to invalidate All keys on all service
     */
    public void evictAllCache() {
        cacheManager.getCacheNames().forEach(cacheName -> {
            evictAll(cacheName);
        });
    }


    /**
     * Returns void
     *
     * @implSpec : This will clear ConcurrentHashMap(Cache) to invalidate All keys or based on type
     */
    public void evictAll(String cacheName) {
        CaffeineCache cache = (CaffeineCache) cacheManager.getCache(cacheName);
        if (cache != null) {
            //clear all the keys
            if (cache.getNativeCache().estimatedSize() >= 1) {
                log.info("{} Entries - Size {}", cacheName, cache.getNativeCache().estimatedSize());
                cache.getNativeCache().invalidateAll();
                log.info("{} Entries Evicted {} ", cacheName, LocalDateTime.now());
            }
        }
    }

    @Scheduled(fixedDelay = 60000)
    public void logCacheSizes() {
        logCacheSize();
    }

    @Scheduled(fixedDelay = 18000000)
    public void evictCacheSizes() {
        evictAllCache();
    }


    /**
     * Returns void
     *
     * @implSpec : This will clear ConcurrentHashMap(Cache) to invalidate All keys or based on type
     */
    public void clearCacheOnType(String type) {
        logCacheSize();
        if (type.equalsIgnoreCase("ALL")) {
            evictAllCache();
        } else if (StringUtils.isNotEmpty(type)) {
            evictAll(type);
        }
    }

    /**
     * Returns void
     *
     * @implSpec : to capture map size on service map
     */
    public void logCacheSize() {
        cacheManager.getCacheNames().forEach(cacheName -> {
            CaffeineCache cache = (CaffeineCache) cacheManager.getCache(cacheName);
            long estimatedSize = cache.getNativeCache().estimatedSize();
            if (estimatedSize >= 1) {
                log.info("Cache Name {} - Size {}", cache.getName(), estimatedSize);
                cache.getNativeCache().asMap().forEach((key, value) -> {
                    log.info("Key {}", key);
                });
            }
        });
    }

}


