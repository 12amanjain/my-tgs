package com.tgs.services.cacheservice.caffeinecache.restcontroller;


import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cacheservice.caffeinecache.servicehandler.CaffeineCacheHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequestMapping("/cache/v1")
@RestController
public class CaffeineCacheController {

	@Autowired
	CaffeineCacheHandler cacheHandler;


	@RequestMapping(value = "/clear-cache/{type}", method = RequestMethod.POST)
	protected BaseResponse clearCache(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("type") String type) throws Exception {
		return cacheHandler.clearCache(type);
	}
}
