package com.tgs.services.cacheservice.caffeinecache.servicehandler;


import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cacheservice.caffeinecache.CaffeineCacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CaffeineCacheHandler {

	@Autowired
	CaffeineCacheService cacheService;

	public BaseResponse clearCache(String type) {
		BaseResponse baseResponse = new BaseResponse();
		cacheService.clearCacheOnType(type);
		return baseResponse;
	}


}
