package com.tgs.services.cacheservice.communicator.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.amazonaws.util.CollectionUtils;
import com.google.common.collect.Lists;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.runtime.impl.FlightAerospikeClient;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FMSCachingServiceCommunicatorImpl implements FMSCachingServiceCommunicator {

	@Autowired
	FlightAerospikeClient aeroClient;

	private static final Integer MAX_FLIGHT_IN_PARTITION = 100;

	@Override
	public <V> boolean store(String key, String bin, V value, CacheMetaInfo metaInfo) {
		metaInfo.setNamespace(AerospikeNamespace.FLIGHT.name());
		Map<String, V> binMap = new HashMap<>();
		binMap.put(bin, value);
		aeroClient.store(key, binMap, metaInfo);
		return true;
	}

	@Override
	public boolean cacheFlightListing(List<TripInfo> trips, String key, CacheMetaInfo metaInfo) {
		metaInfo.setNamespace(AerospikeNamespace.FLIGHT_CACHE.name());
		int i = 0;
		List<List<TripInfo>> tripInfoPartition = Lists.partition(trips, MAX_FLIGHT_IN_PARTITION);
		int index = 1;
		String tempKey = key;
		for (List<TripInfo> tripInfoList : tripInfoPartition) {
			Map<String, TripInfo> binMap = new HashMap<>();
			for (TripInfo trip : tripInfoList) {
				binMap.put(i + "", trip);
				i++;
			}
			aeroClient.store(tempKey, binMap, metaInfo);
			tempKey = key + "_" + index;
			index++;
		}
		return true;
	}

	@Override
	public <V> List<V> fetchValue(String key, Class<V> classofV, CacheMetaInfo metaInfo) {
		metaInfo.setNamespace(AerospikeNamespace.FLIGHT_CACHE.name());
		if (classofV.getTypeName().equals(TripInfo.class.getTypeName())) {
			return fetchPartionValues(key, classofV, metaInfo);
		}
		return aeroClient.get(key, classofV, metaInfo);
	}

	@Override
	public <V> List<V> fetchPartionValues(String key, Class<V> classofV, CacheMetaInfo metaInfo) {
		List<V> resultList = new ArrayList<V>();
		List<V> templist = aeroClient.get(key, classofV, metaInfo);
		resultList.addAll(templist);
		int index = 1;
		String tempKey = key;
		while (templist.size() >= MAX_FLIGHT_IN_PARTITION) {
			tempKey = key + "_" + index;
			templist = aeroClient.get(tempKey, classofV, metaInfo);
			if (!templist.isEmpty())
				resultList.addAll(templist);
			index++;
		}
		return resultList;
	}

	@Override
	public <V> V fetchValue(String key, Class<V> classofV, String set, String binValue) {
		return aeroClient.get(key, classofV, set, binValue);
	}

	@Override
	public <V> Map<String, V> fetchValue(String key, Class<V> classofV, String set, String... binValue) {
		return aeroClient.get(key, classofV, set, binValue);
	}

	@Override
	public boolean deleteRecord(CacheMetaInfo metaInfo) {
		return aeroClient.delete(metaInfo);
	}

	@Override
	public TripInfo getTripInfo(String priceId) {
		List<TripInfo> tripInfos = fetchValue(priceId, TripInfo.class,
				CacheMetaInfo.builder().set(CacheSetName.PRICE_INFO.name()).build());
		if (CollectionUtils.isNullOrEmpty(tripInfos)) {
			log.error("No tripInfo found for priceId {}", priceId);
			throw new CachingLayerException(CachingLayerError.READ_ERROR);
		}
		return tripInfos.get(0);
	}

	@Override
	public AirSearchQuery getSearchQuery(String searchId) {
		AirSearchQuery searchQuery = fetchValue(searchId, AirSearchQuery.class, CacheSetName.SEARCHQUERY.name(),
				BinName.SEARCHQUERYBIN.getName());
		if (searchQuery == null) {
			log.error("No AirSearchQuery found for searchId {}", searchId);
			throw new CachingLayerException(CachingLayerError.READ_ERROR);
		}
		return searchQuery;
	}

	@Override
	public AirReviewResponse getAirReviewResponse(String bookingId) {
		AirReviewResponse reviewResponse = fetchValue(bookingId, AirReviewResponse.class,
				CacheSetName.AIR_REVIEW.getName(), BinName.AIRREVIEW.getName());
		if (reviewResponse == null) {
			log.error("No AirSearchResponse found for bookingId {}", bookingId);
			throw new CachingLayerException(CachingLayerError.READ_ERROR);
		}
		return reviewResponse;
	}

	@Override
	public void scanAndDelete(CacheMetaInfo metaInfo) {
		aeroClient.scanAndDelete(metaInfo);
	}
}
