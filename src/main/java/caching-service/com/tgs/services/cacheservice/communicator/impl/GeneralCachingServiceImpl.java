package com.tgs.services.cacheservice.communicator.impl;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.Operation;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.caffeinecache.CaffeineCacheService;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.datamodel.CacheType;
import com.tgs.services.cacheservice.runtime.impl.GeneralPurposeAerospikeClient;
import com.tgs.services.cacheservice.runtime.impl.InMemoryClient;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GeneralCachingServiceImpl implements GeneralCachingCommunicator {

	@Autowired
	GeneralPurposeAerospikeClient aeroClient;

	@Autowired
	AerospikeConfiguration conf;

	@Autowired
	CaffeineCacheService caffeineCacheService;

	@Autowired
	InMemoryClient inMemoryClient;

	@Override
	public <V> void store(CacheMetaInfo metaInfo, Map<String, V> binMap, boolean compress, boolean plainData,
			int expiration) {
		aeroClient.store(getNameSpace(metaInfo), metaInfo.getSet(), metaInfo.getKey(), binMap, compress, plainData,
				expiration, metaInfo);

	}

	@Override
	public <V> Map<String, Map<String, V>> getResultSet(CacheMetaInfo metaInfo, Class<V> classofV, Filter filter) {
		return aeroClient.getResultSet(metaInfo, classofV, filter);
	}

	public List<Map<String, Object>> getResultSetFromUDF(CacheMetaInfo metaInfo, String functionName, String methodName) {
		return aeroClient.getResultSetFromUDF(metaInfo, functionName, methodName);
	}

	@Override
	public boolean delete(CacheMetaInfo metaInfo) {
		return aeroClient.delete(getNameSpace(metaInfo), metaInfo.getSet(), metaInfo.getKey());
	}

	@Override
	public void truncate(CacheMetaInfo metaInfo) {
		aeroClient.truncate(getNameSpace(metaInfo), metaInfo.getSet());
	}

	@Override
	public <V> Map<String, V> get(CacheMetaInfo metaInfo, Class<V> classofV, Boolean compress, Boolean plainData,
			String... bin) {
		return aeroClient.get(getNameSpace(metaInfo), metaInfo.getSet(), metaInfo.getKey(), classofV,
				metaInfo.getTypeOfT(), compress, plainData, bin);
	}

	@Override
	public <V> V getBinValue(CacheMetaInfo metaInfo, Class<V> classofV, Boolean compress, Boolean plainData,
			String bin) {
		return aeroClient.getBinValue(getNameSpace(metaInfo), metaInfo.getSet(), metaInfo.getKey(), classofV,
				metaInfo.getTypeOfT(), compress, plainData, bin);
	}

	public String getNameSpace(CacheMetaInfo metaInfo) {
		return conf
				.getMappedNameSpace(ObjectUtils.firstNonNull(metaInfo.getNamespace(), CacheNameSpace.FLIGHT.getName()));
	}

	@Override
	public <V> void store(CacheMetaInfo metaInfo, String bin, List<V> values, boolean compress, boolean plainData,
			int expiration) {
		metaInfo.setNamespace(getNameSpace(metaInfo));
		aeroClient.store(metaInfo, bin, values, compress, plainData, expiration);

	}

	@Override
	public <V> Map<String, List<V>> getList(CacheMetaInfo metaInfo, Class<V> classofV, Boolean compress,
			Boolean plainData, String... bin) {
		return aeroClient.getList(metaInfo, getNameSpace(metaInfo), metaInfo.getSet(), metaInfo.getKey(), classofV,
				compress, plainData, bin);
	}

	@Override
	public <V> Map<String, Map<String, V>> get(CacheMetaInfo metaInfo, Class<V> classofV) {
		return aeroClient.get(metaInfo, classofV);
	}

	@Override
	public <V> void storeInQueue(CacheMetaInfo metaInfo) {
		if (CacheType.INMEMORY.equals(metaInfo.getCacheType())) {
			inMemoryClient.storeInQueue(metaInfo);
		} else {
			metaInfo.setNamespace(getNameSpace(metaInfo)).setPlainData(false);
			if (StringUtils.isBlank(metaInfo.getSet())) {
				metaInfo.setSet(CacheSetName.SESSION_POOL.name());
			}
			aeroClient.store(metaInfo, "sessionlist", Arrays.asList(metaInfo), metaInfo.getCompress(), false, -1);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public CacheMetaInfo fetchFromQueue(CacheMetaInfo metaInfo) {
		int index = metaInfo.getIndex();
		CacheType cacheType = metaInfo.getCacheType();
		String key = metaInfo.getKey();
		if (CacheType.INMEMORY.equals(metaInfo.getCacheType())) {
			metaInfo = inMemoryClient.getFromQueue(metaInfo);
		} else {
			metaInfo.setNamespace(getNameSpace(metaInfo)).setIndex(metaInfo.getIndex());
			if (StringUtils.isBlank(metaInfo.getSet())) {
				metaInfo.setSet(CacheSetName.SESSION_POOL.name());
			}
			metaInfo = aeroClient.pop(metaInfo, "sessionlist", metaInfo.getClass());
		}
		if (metaInfo != null && metaInfo.getExpiryTime() != null
				&& metaInfo.getExpiryTime().isBefore(LocalDateTime.now())) {
			log.info("Expired key , hence removing from queue {}", metaInfo.getKey());
			metaInfo = null;
		} else if (metaInfo != null && index == -1)
			log.info("Returning last key from queue {}, cacheType {} ", metaInfo.getKey(), cacheType);


		if (metaInfo == null) {
			log.info("Unable to find data from Caching Queue for key {}", key);
		}

		return metaInfo;
	}

	@Override
	public <V> void asyncStore(CacheMetaInfo metaInfo, Map<String, Map<String, V>> keyValueMap) {
		aeroClient.asyncStore(getNameSpace(metaInfo), metaInfo.getSet(), keyValueMap, metaInfo);
	}

	@Override
	public <V> V performOperationsAndFetchBinValue(CacheMetaInfo metaInfo, String bin,Class<V> classofV,int expiration,Operation... operations) {
		return aeroClient.performOperationsAndFetchBinValue(metaInfo, bin, classofV, expiration, operations);
	}

	@Override
	public <K, V> void storeInMap(CacheMetaInfo metaInfo, String bin, Map<K, V> map) {
		metaInfo.setNamespace(getNameSpace(metaInfo)).setPlainData(false);
		aeroClient.store(metaInfo, bin, map, BooleanUtils.isTrue(metaInfo.getCompress()), false, -1);
	}

}
