package com.tgs.services.cacheservice.communicator.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.runtime.impl.HotelAerospikeClient;

@Service
public class HMSCachingServiceCommunicatorImpl implements HMSCachingServiceCommunicator {

	@Autowired
	HotelAerospikeClient aeroClient;

	@Override
	public <V> V fetchValue(Class<V> classofV, CacheMetaInfo metaInfo) {
		return aeroClient.get(classofV, metaInfo);
	}

	@Override
	public <V> boolean store(CacheMetaInfo metaInfo) {
		aeroClient.store(metaInfo);
		return true;
	}

	@Override
	public boolean delete(CacheMetaInfo metaInfo) {
		aeroClient.delete(metaInfo);
		return true;
	}

	@Override
	public void truncate(CacheMetaInfo metaInfo) {
		aeroClient.truncate(metaInfo);
	}

}
