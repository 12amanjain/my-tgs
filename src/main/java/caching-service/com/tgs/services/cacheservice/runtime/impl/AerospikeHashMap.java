package com.tgs.services.cacheservice.runtime.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Language;
import com.aerospike.client.Record;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.WritePolicy;
import com.aerospike.client.query.Statement;
import com.aerospike.client.task.ExecuteTask;
import com.aerospike.client.task.RegisterTask;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.aerospike.AerospikeHelper;
import com.tgs.services.cacheservice.aerospike.AerospikeNamespace;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.utils.encryption.KryoUtils;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AerospikeHashMap implements CustomInMemoryHashMap {

	@Autowired
	AerospikeClient client;

	@Autowired
	AerospikeConfiguration conf;

	@Value("${setPrefix}")
	private String setPrefix;

	@Override
	public <V> V put(String key, String field, V value, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		List<Bin> bins = new ArrayList<>();
		if (metaInfo.getCompress()) {
			bins.add(new Bin(field, KryoUtils.serialize(value).get()));
			bins.add(new Bin("compression", metaInfo.getCompress()));
		} else {
			bins.add(new Bin(field, GsonUtils.getGson().toJson(value)));
		}
		client.put(AerospikeHelper.getWritePolicy(metaInfo.getExpiration()), aerospikekey, bins.toArray(new Bin[0]));
		return value;
	}


	@Override
	public <V> V get(String key, String field, Class<V> classofV, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		ContextData contextData = SystemContextHolder.getContextData();
		if (contextData != null) {
			Optional<V> optionValue = SystemContextHolder.getContextData().getValue(String.join("", key, field));
			if (optionValue != null) {
				return optionValue.orElse(null);
			}
		}
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		V value = null;
		try {
			Policy policy = new Policy();
			String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
			Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
			Record result = client.get(policy, aerospikekey);
			if (result == null || result.getValue(field) == null) {
				return value;
			}
			try {
				if (result.getBoolean("compression")) {
					value = KryoUtils.deserialize((byte[]) result.getValue(field), classofV).get();
					return value;
				}
			} catch (Exception e) {
				log.error("Unable to fetch value of key {},field {}", field, key, e);
				return value;
			}
			if (metaInfo.getTypeOfT() != null) {
				value = GsonUtils.getGson().fromJson(result.getString(field), metaInfo.getTypeOfT());
			} else {
				value = GsonUtils.getGson().fromJson(result.getString(field), classofV);
			}
		} finally {
			if (contextData != null) {
				contextData.setValue(String.join("", key, field), value);
			}
		}
		return value;
	}

	@Override
	public void delete(String key, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		WritePolicy wpolicy = new WritePolicy();
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), key);
		client.delete(wpolicy, aerospikekey);
	}

	@Override
	public void truncate(String set) {
		verifySetInRequest(set);
		String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
		truncate(mappedNameSpace, setPrefix.concat("_").concat(set));
	}

	public void truncate(String namespace, String set) {
		client.truncate(null, namespace, set, null);
	}

	public void verifySetInRequest(String set) {
		try {
			if (StringUtils.isEmpty(set)) {
				throw new Exception();
			}
		} catch (Exception e) {
			log.info("Set is empty {}", e);
		}
	}


	@Override
	public void modifyTtl(String setName, int ttl) {
		try {
			RegisterTask task = client.register(null, AerospikeHashMap.class.getClassLoader(), "udf/modify_ttl.lua",
					"modify_ttl.lua", Language.LUA);
			task.waitTillComplete(1000, 5000);
			WritePolicy queryPolicy = AerospikeHelper.getWritePolicy(ttl);
			queryPolicy.setTimeouts(30000, 30000);
			queryPolicy.maxRetries = 4;
			Statement stmt = new Statement();
			String mappedNameSpace = conf.getMappedNameSpace(AerospikeNamespace.STATIC_MAP.name().toLowerCase());
			stmt.setNamespace(mappedNameSpace);
			com.aerospike.client.Value[] values = new com.aerospike.client.Value[1];
			values[0] = com.aerospike.client.Value.get(ttl);
			stmt.setSetName(setPrefix.concat("_").concat(setName));
			ExecuteTask executeTask =
					client.execute(queryPolicy, stmt, "modify_ttl", "modify_ttl", com.aerospike.client.Value.get(ttl));
			executeTask.waitTillComplete(1000, 5000);
		} catch (Exception e) {
			log.error("Unable to update ttl through udf", e);
			throw e;
		}

	}

}
