package com.tgs.services.cacheservice.runtime.impl;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Operation;
import com.aerospike.client.Record;
import com.aerospike.client.async.EventLoop;
import com.aerospike.client.async.EventLoops;
import com.aerospike.client.async.EventPolicy;
import com.aerospike.client.async.NioEventLoops;
import com.aerospike.client.cdt.ListOperation;
import com.aerospike.client.cdt.MapOperation;
import com.aerospike.client.cdt.MapPolicy;
import com.aerospike.client.policy.BatchPolicy;
import com.aerospike.client.policy.ClientPolicy;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.QueryPolicy;
import com.aerospike.client.policy.WritePolicy;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.IndexType;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.ResultSet;
import com.aerospike.client.query.Statement;
import com.google.common.collect.Lists;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.aerospike.AerospikeHelper;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GeneralPurposeAerospikeClient {

	@Autowired
	AerospikeClient client;

	@Autowired
	AerospikeConfiguration conf;

	@Value("${setPrefix}")
	private String setPrefix;

	@Value("${aerospike.sockettimeout:}")
	private Integer socketTimeout;

	public <V> void store(String namespace, String set, String key, Map<String, V> binMap, boolean compress,
			boolean plainData, int expiration, CacheMetaInfo cacheMetaInfo) {
		verifySetInRequest(cacheMetaInfo.getSet());
		try {

			// IndexTask task = client.createIndex(null, mappedNameSpace, set, "idx_userId",
			// bin, IndexType.STRING);
			//
			// task.waitTillComplete();
			Key aerospikekey = new Key(namespace, setPrefix.concat("_").concat(set), key);

			WritePolicy writePolicy = AerospikeHelper.getWritePolicy(expiration, cacheMetaInfo);
			/**
			 * It changes writePolicy's action to handle existing record if any valid name for it is found in
			 * cacheMetaInfo.
			 */

			client.put(writePolicy, aerospikekey,
					AerospikeHelper.generateBinList(binMap, compress, plainData).toArray(new Bin[0]));

		} catch (AerospikeException e) {
			if (e.getResultCode() == com.aerospike.client.ResultCode.RECORD_TOO_BIG) {
				throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
			}

			if (e.getResultCode() == com.aerospike.client.ResultCode.KEY_EXISTS_ERROR) {
				throw new CachingLayerException(CachingLayerError.DUPLICATE_KEY);
			}
			log.error("Failed to store set {} key {} , binMap {} in aerospike", setPrefix.concat("_").concat(set), key,
					binMap, e);
			throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
		}
	}

	// for async bulk write
	public <V> void asyncStore(String namespace, String set, Map<String, Map<String, V>> keyBinMap,
			CacheMetaInfo cacheMetaInfo) {
		verifySetInRequest(cacheMetaInfo.getSet());
		EventPolicy eventPolicy = new EventPolicy();
		int eventLoopSize = Runtime.getRuntime().availableProcessors();
		EventLoops eventLoops = new NioEventLoops(eventPolicy, eventLoopSize);
		ClientPolicy clientPolicy = new ClientPolicy();
		clientPolicy.eventLoops = eventLoops;
		// AerospikeClient aeroClient = new AerospikeClient(clientPolicy,
		// conf.getHost(), 3000);
		WritePolicy writePolicy = AerospikeHelper.getWritePolicy(cacheMetaInfo.getExpiration(), cacheMetaInfo);
		try {
			for (Entry<String, Map<String, V>> entrySet : keyBinMap.entrySet()) {
				Key aerospikekey = new Key(namespace, setPrefix.concat("_").concat(set), entrySet.getKey());
				List<Bin> bins = new ArrayList<>();
				for (Entry<String, V> binMap : entrySet.getValue().entrySet()) {
					bins.add(new Bin(binMap.getKey(), binMap.getValue()));
				}
				EventLoop eventLoop = eventLoops.next();
				// aeroClient.put(eventLoop, null, writePolicy, aerospikekey, bins.toArray(new
				// Bin[0]));
			}
		} catch (AerospikeException e) {
			log.error("async store failed {} ", e);
			throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
		}
	}

	public <V> void store(CacheMetaInfo metaInfo, String bin, List<V> values, boolean compress, boolean plainData,
			int expiration) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(metaInfo.getKey())) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			if (bin.length() > 14) {
				log.error("Reducing binname length to 14 character length, bin name {}", bin);
				bin = bin.substring(0, 13);
			}
			Key aerospikekey = new Key(metaInfo.getNamespace(), setPrefix.concat("_").concat(metaInfo.getSet()),
					metaInfo.getKey());
			metaInfo.setCompress(compress);
			metaInfo.setPlainData(plainData);
			client.operate(AerospikeHelper.getWritePolicy(expiration), aerospikekey,
					ListOperation.appendItems(bin, AerospikeHelper.generateValueList(bin, values, metaInfo)));
		} catch (AerospikeException e) {
			if (e.getResultCode() == com.aerospike.client.ResultCode.RECORD_TOO_BIG) {
				throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
			}
			log.error("Failed to store set{}, key {} , bin {} in aerospike",
					setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey(), bin, e);
			throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
		}
	}

	@SuppressWarnings({"deprecation", "unchecked"})
	public <V> Map<String, Map<String, V>> getResultSet(CacheMetaInfo metaInfo, Class<V> classofV, Filter filter) {
		if (metaInfo != null && BooleanUtils.isTrue(metaInfo.getCacheResult())) {
			verifySetInRequest(metaInfo.getSet());
			ContextData contextData = SystemContextHolder.getContextData();
			if (contextData != null) {
				Optional<V> optionValue = SystemContextHolder.getContextData()
						.getValue(String.join("", Arrays.toString(metaInfo.getKeys()), metaInfo.getKey()));
				if (optionValue != null) {
					return (Map<String, Map<String, V>>) optionValue.orElse(null);
				}
			}
		}

		if (filter == null) {
			throw new CachingLayerException(CachingLayerError.NULL_FILTER);
		}

		Map<String, Map<String, V>> resultSet = new HashMap<>();
		try {
			Statement statement = new Statement();
			statement.setNamespace(getNameSpace(metaInfo));
			statement.setSetName(setPrefix.concat("_").concat(metaInfo.getSet()));
			statement.setFilter(filter);
			QueryPolicy policy = getQueryPolicy();
			if (metaInfo.getBins() != null) {
				statement.setBinNames(metaInfo.getBins());
			}
			RecordSet rs = client.query(policy, statement);
			while (rs.next()) {
				Key key = rs.getKey();
				Record record = rs.getRecord();
				Map<String, V> innerMap = AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);
				if (resultSet.get(key.userKey.toString()) == null) {
					resultSet.put(key.userKey.toString(), innerMap);
				}
			}
		} catch (Exception e) {
			log.error("Unable to generate Result Set for set {} , key {} and class {} ",
					setPrefix.concat("_").concat(metaInfo.getSet()), Arrays.toString(metaInfo.getKeys()),
					metaInfo.getKey(), classofV.getCanonicalName(), e);
		}
		return resultSet;

	}

	@SuppressWarnings("deprecation")
	public <V> V pop(CacheMetaInfo metaInfo, final String bin, Class<V> classOfV) {
		Record record = null;
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(metaInfo.getKey())) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			Key aerospikekey = new Key(metaInfo.getNamespace().toLowerCase(),
					setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());
			String binName = TgsObjectUtils.firstNonNull(() -> bin, () -> metaInfo.getBins()[0]);
			record = client.operate(AerospikeHelper.getWritePolicy(metaInfo.getExpiration()), aerospikekey,
					ListOperation.pop(binName, metaInfo.getIndex()));

			return AerospikeHelper.convertingBinMapToUserDefinedMap(record, classOfV, metaInfo).get(binName);

		} catch (AerospikeException e) {
			if (e.getResultCode() == com.aerospike.client.ResultCode.KEY_NOT_FOUND_ERROR) {
				log.info("No such key present in aerospike {}", metaInfo.getKey());
				// throw new CachingLayerException(CachingLayerError.KEY_NOT_FOUND_ERROR);
			} else if (e.getResultCode() == com.aerospike.client.ResultCode.PARAMETER_ERROR) {
				log.info("Unable to pop value from aerospike  , key {} , bin {} , classV {}", metaInfo, bin, classOfV);
			} else {
				log.error("Unable to pop value from aerospike ", e);
			}

		} catch (Exception e) {
			log.error("Unable to pop value from aerospike for set {} and key {} ",
					setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey(), e);
		}
		return null;

	}

	public <K, V> void store(CacheMetaInfo metaInfo, String bin, Map<K, V> values, boolean compress, boolean plainData,
			int expiration) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(metaInfo.getKey())) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			if (bin.length() > 14) {
				log.error("Reducing binname length to 14 character length, bin name {}", bin);
				bin = bin.substring(0, 13);
			}
			Key aerospikekey = new Key(metaInfo.getNamespace(), setPrefix.concat("_").concat(metaInfo.getSet()),
					metaInfo.getKey());
			metaInfo.setCompress(compress);
			metaInfo.setPlainData(plainData);
			client.operate(AerospikeHelper.getWritePolicy(expiration), aerospikekey, MapOperation
					.putItems(MapPolicy.Default, bin, AerospikeHelper.generateValueMap(bin, values, metaInfo)));
		} catch (AerospikeException e) {
			if (e.getResultCode() == com.aerospike.client.ResultCode.RECORD_TOO_BIG) {
				throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
			}
			log.error("Failed to store set{}, key {} , bin {} in aerospike",
					setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey(), bin, e);
			throw new CachingLayerException(CachingLayerError.WRITE_ERROR);
		}
	}

	public <V> Map<String, Map<String, V>> get(CacheMetaInfo metaInfo, Class<V> classofV) {
		verifySetInRequest(metaInfo.getSet());
		Map<String, Map<String, V>> resultSet = new HashMap<>();
		try {
			BatchPolicy policy = new BatchPolicy();
			// Maximum number of keys allowed per node in aerospike is 5000, else we get batch-max-requests exceeded
			// exception
			List<List<String>> partitions = Lists.partition(Arrays.asList(metaInfo.getKeys()), 5000);
			partitions.forEach(keySet -> {
				int i = 0;
				List<String> keys = new ArrayList<>();
				List<Key> aerospikekeys = new ArrayList<Key>();
				for (String key : keySet) {
					if (StringUtils.isNotBlank(key)) {
						aerospikekeys.add(
								new Key(getNameSpace(metaInfo), setPrefix.concat("_").concat(metaInfo.getSet()), key));
						keys.add(key);
					}
				}
				Record records[] = null;
				if (metaInfo.getBins() != null && metaInfo.getBins().length > 0) {
					records = client.get(policy, aerospikekeys.toArray(new Key[0]), metaInfo.getBins());
				} else {
					records = client.get(policy, aerospikekeys.toArray(new Key[0]));
				}
				for (Record record : records) {
					if (record != null) {
						Map<String, V> innerMap =
								AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);
						if (resultSet.get(keys.get(i)) == null) {
							resultSet.put(keys.get(i), innerMap);
						}
					}
					i++;
				}
			});
		} catch (Exception e) {
			log.error("Unable to fetch value from aerospike for set {} and  key {}",
					setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey(), e);
		}
		return resultSet;
	}

	public boolean delete(String namespace, String set, String key) {
		verifySetInRequest(set);
		WritePolicy wpolicy = new WritePolicy();
		Key aerospikekey = new Key(namespace, setPrefix.concat("_").concat(set), key);
		return client.delete(wpolicy, aerospikekey);
	}

	public void truncate(String namespace, String set) {
		verifySetInRequest(set);
		client.truncate(null, namespace, setPrefix.concat("_").concat(set), null);
	}

	public <V> V getBinValue(String namespace, String set, String key, Class<V> classofV, Type type, Boolean compress,
			Boolean plainData, String bin) {
		return get(namespace, set, key, classofV, type, compress, plainData, bin).get(bin);
	}

	public <V> Map<String, V> get(String namespace, String set, String key, Class<V> classofV, Type type,
			Boolean compress, Boolean plainData, String... bin) {
		verifySetInRequest(set);
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		Policy policy = new Policy();
		Key aerospikekey = new Key(namespace, setPrefix.concat("_").concat(set), key);
		Record record = null;
		if (bin != null && bin.length > 0) {
			record = client.get(policy, aerospikekey, bin);
		} else {
			record = client.get(policy, aerospikekey);
		}
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().compress(compress).plainData(plainData).typeOfT(type).build();
		return AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);
	}

	public <V> Map<String, List<V>> getList(CacheMetaInfo metaInfo, String namespace, String set, String key,
			Class<V> classofV, Boolean compress, Boolean plainData, String... bin) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(key)) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		Policy policy = new Policy();
		Key aerospikekey = new Key(namespace, setPrefix.concat("_").concat(set), key);
		Record record = null;
		if (bin != null && bin.length > 0) {
			record = client.get(policy, aerospikekey, bin);
		} else {
			record = client.get(policy, aerospikekey);
		}
		return AerospikeHelper.convertingBinMapToUserDefinedMapList(metaInfo, record, classofV, compress, plainData);
	}

	public String getNameSpace(CacheMetaInfo metaInfo) {
		return conf
				.getMappedNameSpace(ObjectUtils.firstNonNull(metaInfo.getNamespace(), CacheNameSpace.FLIGHT.getName()));
	}

	public void verifySetInRequest(String set) {
		try {
			if (StringUtils.isEmpty(set)) {
				throw new Exception();
			}
		} catch (Exception e) {
			log.info("Set is empty ", e);
		}
	}

	@SuppressWarnings("unchecked")
	public List<Map<String, Object>> getResultSetFromUDF(CacheMetaInfo metaInfo, String functionName,
			String methodName) {
		try {
			log.info("Inside result set from udf {}", metaInfo);
			List<Map<String, Object>> resultSet = new ArrayList<>();
			QueryPolicy queryPolicy = new QueryPolicy();
			queryPolicy.setTimeouts(30000, 30000);
			queryPolicy.maxRetries = 4;
			Statement stmt = new Statement();
			stmt.setNamespace(getNameSpace(metaInfo));
			com.aerospike.client.Value[] values = getValues(metaInfo.getValues());
			stmt.setAggregateFunction(GeneralPurposeAerospikeClient.class.getClassLoader(),
					"udf/" + functionName + ".lua", functionName, methodName, values);
			stmt.setSetName(setPrefix.concat("_").concat(metaInfo.getSet()));
			ResultSet rs = client.queryAggregate(queryPolicy, stmt);

			while (rs.next()) {
				Map<String, Object> object = (Map<String, Object>) rs.getObject();
				resultSet.add(object);
				log.debug("Got result {}", object);
			}
			rs.close();
			return resultSet;
		} catch (Exception e) {
			log.error("Unable to get result from udf", e);
			throw e;
		}
	}


	private QueryPolicy getQueryPolicy() {
		QueryPolicy policy = null;
		if (!org.springframework.util.ObjectUtils.isEmpty(socketTimeout)) {
			policy = new QueryPolicy();
			policy.setTimeouts(socketTimeout, socketTimeout);
			policy.maxRetries = 3;
		}
		return policy;
	}

	private com.aerospike.client.Value[] getValues(String... strValues) {

		com.aerospike.client.Value[] values = new com.aerospike.client.Value[strValues.length];
		for (int i = 0; i < values.length; i++) {
			values[i] = com.aerospike.client.Value.get(strValues[i]);
		}
		return values;
	}

	public <V> V performOperationsAndFetchBinValue(CacheMetaInfo metaInfo, String bin, Class<V> classofV,
			int expiration, Operation... operations) {
		try {
			Key aerospikekey =
					new Key(getNameSpace(metaInfo), setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());
			Record record = client.operate(AerospikeHelper.getWritePolicy(expiration), aerospikekey, operations);
			Map<String, V> map = AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo);
			return map.get(bin);
		} catch (AerospikeException e) {
			if (e.getResultCode() == com.aerospike.client.ResultCode.KEY_NOT_FOUND_ERROR) {
				log.info("No such key present in aerospike {}", metaInfo.getKey());
				throw new CachingLayerException(CachingLayerError.KEY_NOT_FOUND_ERROR);
			}
			throw new CachingLayerException();
		}
	}
}
