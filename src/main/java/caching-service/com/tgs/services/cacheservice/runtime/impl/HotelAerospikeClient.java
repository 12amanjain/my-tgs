package com.tgs.services.cacheservice.runtime.impl;

import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.ScanCallback;
import com.aerospike.client.policy.Policy;
import com.aerospike.client.policy.WritePolicy;
import com.tgs.services.cacheservice.aerospike.AerospikeConfiguration;
import com.tgs.services.cacheservice.aerospike.AerospikeHelper;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelAerospikeClient implements ScanCallback {

	@Autowired
	AerospikeClient client;

	@Autowired
	AerospikeConfiguration conf;

	@Value("${setPrefix}")
	private String setPrefix;

	public <V> void store(CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(metaInfo.getKey())) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
			Key aerospikekey =
					new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());

			WritePolicy writePolicy = AerospikeHelper.getWritePolicy(metaInfo.getExpiration(), metaInfo);
			/**
			 * It changes writePolicy's action to handle existing record if any valid name for it is found in
			 * cacheMetaInfo.
			 */

			client.put(writePolicy, aerospikekey,
					AerospikeHelper
							.generateBinList(metaInfo.getBinValues(), BooleanUtils.toBoolean(metaInfo.getCompress()),
									BooleanUtils.toBoolean(metaInfo.getPlainData()), metaInfo.getKyroCompress())
							.toArray(new Bin[0]));
		} catch (AerospikeException e) {
			log.error("Failed to store key for metaInfo {} ", metaInfo.getKey(), e);
			throw e;
		} catch (Exception e) {
			log.error("Failed to store key for metaInfo {} ", metaInfo, e);
		}
	}

	public <V> V get(Class<V> classofV, CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		if (StringUtils.isBlank(metaInfo.getKey())) {
			throw new CachingLayerException(CachingLayerError.BLANK_KEY);
		}
		try {
			Policy policy = new Policy();
			String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
			Key aerospikekey =
					new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());
			Record record = client.get(policy, aerospikekey, metaInfo.getBins()[0]);
			return AerospikeHelper.convertingBinMapToUserDefinedMap(record, classofV, metaInfo)
					.get(metaInfo.getBins()[0]);
		} catch (Exception e) {
			log.error("Failed to get metaInfo {} ", metaInfo, e);
		}
		return null;
	}

	public boolean delete(CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		WritePolicy wpolicy = new WritePolicy();
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		Key aerospikekey = new Key(mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), metaInfo.getKey());
		return client.delete(wpolicy, aerospikekey);
	}

	public void truncate(CacheMetaInfo metaInfo) {
		verifySetInRequest(metaInfo.getSet());
		String mappedNameSpace = conf.getMappedNameSpace(metaInfo.getNamespace().toLowerCase());
		client.truncate(null, mappedNameSpace, setPrefix.concat("_").concat(metaInfo.getSet()), null);
	}

	@Override
	public void scanCallback(Key key, Record record) throws AerospikeException {
		// TODO Auto-generated method stub

	}

	public void verifySetInRequest(String set) {
		try {
			if (StringUtils.isEmpty(set)) {
				throw new Exception();
			}
		} catch (Exception e) {
			log.info("Set is empty {}", e);
		}
	}
}
