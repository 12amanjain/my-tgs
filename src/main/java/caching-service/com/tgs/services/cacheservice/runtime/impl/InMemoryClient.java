package com.tgs.services.cacheservice.runtime.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InMemoryClient {

	private static Map<String, LinkedList<CacheMetaInfo>> inMemCache = new HashMap<>();

	public void storeInQueue(CacheMetaInfo cacheMetaInfo) {
		synchronized (this) {
			if (inMemCache.get(cacheMetaInfo.getKey()) == null) {
				log.debug("initializing list for key {}", cacheMetaInfo.getKey());
				inMemCache.put(cacheMetaInfo.getKey(), new LinkedList<>());
			}
			inMemCache.get(cacheMetaInfo.getKey()).addLast(cacheMetaInfo);
			log.debug("Queue size for key {} , is {} ", cacheMetaInfo.getKey(),
					inMemCache.get(cacheMetaInfo.getKey()).size());
		}
	}

	public CacheMetaInfo getFromQueue(CacheMetaInfo cacheMetaInfo) {
		synchronized (this) {
			LinkedList<CacheMetaInfo> sessionList = Optional.ofNullable(inMemCache.get(cacheMetaInfo.getKey()))
					.orElse(null);
			if (CollectionUtils.isNotEmpty(sessionList)) {
				log.debug("Session size for key {} is {}", cacheMetaInfo.getKey(), CollectionUtils.size(sessionList));
				if (cacheMetaInfo.getIndex() == 0)
					return sessionList.removeFirst();
				else {
					return sessionList.removeLast();
				}
			}
			return null;
		}

	}
}
