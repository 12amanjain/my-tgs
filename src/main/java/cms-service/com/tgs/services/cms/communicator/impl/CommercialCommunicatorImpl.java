package com.tgs.services.cms.communicator.impl;

import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.cms.dbmodel.DbCreditCardInfo;
import com.tgs.services.cms.helper.AirSupplierCommissionEngine;
import com.tgs.services.cms.helper.CommercialType;
import com.tgs.services.cms.helper.CommissionHelper;
import com.tgs.services.cms.helper.CreditCardHelper;
import com.tgs.services.cms.helper.TourCodeEngine;
import com.tgs.services.cms.helper.air.AirCommissionEngine;
import com.tgs.services.cms.helper.hotel.HotelCommissionEngine;
import com.tgs.services.cms.jparepository.commission.CommissionPlanService;
import com.tgs.services.cms.jparepository.creditcard.CreditCardInfoService;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommercialCommunicatorImpl implements CommercialCommunicator {

	@Autowired
	AirCommissionEngine airEngine;

	@Autowired
	HotelCommissionEngine hotelEngine;

	@Autowired
	TourCodeEngine tourCodeEngine;

	@Autowired
	AirSupplierCommissionEngine airSupplierCommissionEngine;

	@Autowired
	CreditCardInfoService ccInfoService;

	@Autowired
	CommissionPlanService cPlanService;

	@Autowired
	AirOrderItemCommunicator orderItemCommunicator;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Override
	public void processUserCommission(TripInfo tripInfo, User user, AirSearchQuery searchQuery) {
		airEngine.processCommission(tripInfo, user, searchQuery);
	}

	@Override
	public void processUserCommission(List<TripInfo> tripInfos, User user, AirSearchQuery searchQuery) {
		tripInfos.forEach(tripInfo -> {
			processUserCommission(tripInfo, user, searchQuery);
		});
	}

	@Override
	public boolean processUserCommissionInHotel(HotelInfo hotelInfo, User user, HotelSearchQuery searchQuery) {
		if (hotelEngine.isCommissionApplicableOnHotel(user, searchQuery)) {
			hotelEngine.processCommission(hotelInfo, user, searchQuery);
			return true;
		}
		return false;
	}

	@Override
	public boolean processUserCommissionInHotels(List<HotelInfo> hotelInfos, User user, HotelSearchQuery searchQuery) {
		log.info("Applying commercial for search id {} ", searchQuery.getSearchId());
		if (hotelEngine.isCommissionApplicableOnHotel(user, searchQuery)) {
			hotelInfos.forEach(hotelInfo -> {
				hotelEngine.processCommission(hotelInfo, user, searchQuery);
			});
			log.info("Commercial is applied for search id {} ", searchQuery.getSearchId());
			return true;
		}
		log.info("Commercial is not applicable for search id {} ", searchQuery.getSearchId());
		return false;
	}

	@Override
	public void processTourCode(TripInfo tripInfo, User user, AirSearchQuery searchQuery) {
		tourCodeEngine.processTourCodeCommission(tripInfo, user, searchQuery);
	}

	@Override
	public void processAirCommission(TripInfo tripInfo, User user) {
		airSupplierCommissionEngine.processAirCommission(tripInfo, user);
	}

	@Override
	public void processAirCommission(List<TripInfo> tripInfos, User user) {
		tripInfos.forEach(tripInfo -> {
			processAirCommission(tripInfo, user);
		});
	}

	@Override
	public void resetCommission(List<TripInfo> tripInfos, User user) {
		tripInfos.forEach(tripInfo -> {
			resetCommission(tripInfo, user);
		});
	}

	@Override
	public void resetCommission(TripInfo tripInfo, User user) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {

			if (segmentInfo.getBookingRelatedInfo() != null) {
				// Its Applicable for All Pax - Mostly from Post Booking
				List<FlightTravellerInfo> flightTravellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				if (CollectionUtils.isNotEmpty(flightTravellerInfos)) {
					flightTravellerInfos.forEach(travellerInfo -> {
						resetFareDetail(travellerInfo.getFareDetail(), user);
					});
				}
			} else {
				// Its Applicable for All Pax - Mostly from Pre Booking
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
						resetFareDetail(fareDetail, user);
					});
				});
			}
		});
	}

	public static void resetFareDetail(FareDetail fareDetail, User user) {
		Map<FareComponent, Double> fareCompenents = fareDetail.getFareComponents();
		for (CommercialType commercialType : CommercialType.values()) {
			FareComponent fareComponent = commercialType.getTargetComponent();
			if (fareComponent != null) {
				removeUpdatedTotalfare(fareCompenents, fareComponent);
				fareCompenents.remove(fareComponent);
			}
		}
		for (FareComponent component : FareComponent.getPartnerCommissionComponents()) {
			removeUpdatedTotalfare(fareCompenents, component);
			fareCompenents.remove(component);
		}
		removeUpdatedTotalfare(fareCompenents, FareComponent.PCTDS);
		fareCompenents.remove(FareComponent.PCTDS);
		removeUpdatedTotalfare(fareCompenents, FareComponent.PMU);
		fareCompenents.remove(FareComponent.PMU);
		removeUpdatedTotalfare(fareCompenents, FareComponent.PMTDS);
		fareCompenents.remove(FareComponent.PMTDS);
		removeUpdatedTotalfare(fareCompenents, FareComponent.TDS);
		fareCompenents.remove(FareComponent.TDS);
		removeUpdatedTotalfare(fareCompenents, FareComponent.MFT);
		fareCompenents.remove(FareComponent.MFT);
	}

	public static void removeUpdatedTotalfare(Map<FareComponent, Double> fareCompenents,
			FareComponent targetComponent) {
		if (targetComponent.isUpdateTotalFare()) {
			double initialTotal = fareCompenents.getOrDefault(FareComponent.TF, 0.0);
			if (initialTotal > 0) {
				fareCompenents.put(FareComponent.TF, initialTotal - fareCompenents.getOrDefault(targetComponent, 0.0));
			}
		}
	}

	@Override
	public CreditCardInfo getCreditCardById(Long id) {
		return ccInfoService.findById(id).toDomain();
	}

	@Override
	public CreditCardInfo getCreditCardInfo(TripInfo tripInfo, User user) {
		return CreditCardHelper.getCreditCardInfo(tripInfo.getSupplierInfo().getSupplierId(),
				tripInfo.getPlatingCarrier());
	}

	@Override
	public CommissionPlan getCommissionPlanFromCache(String planId) {
		return CommissionHelper.getCommissionPlanFromCache(planId);
	}

	@Override
	public CreditCardInfo getVirtualCreditCard(TripInfo tripInfo, User bookingUser, GstInfo billingEntity) {
		return CreditCardHelper.getCorporateCreditCardInfo(tripInfo.getSupplierInfo().getSupplierId(),
				tripInfo.getPlatingCarrier(), billingEntity, bookingUser);
	}

	@Override
	public boolean isVirtualCreditCardSupported(String bookingId, User user, GstInfo billingEntity) {
		// In case of direct booking, trips can be fetched from review response.
		List<TripInfo> trips = fmsCommunicator.getTripInfosFromReviewResponse(bookingId);
		if (StringUtils.isNotBlank(bookingId) && CollectionUtils.isEmpty(trips)) {
			// In case of hold confirm booking. Trips may not available in review response. So it should be fetched from
			// orderItems.
			trips = orderItemCommunicator.findTripByBookingId(bookingId);
		}
		return isVirtualCreditCardSupported(trips, user, billingEntity);
	}

	@Override
	public boolean isVirtualCreditCardSupported(List<TripInfo> trips, User user, GstInfo billingEntity) {
		Boolean isVirtualCreditCardSupported = null;
		if (CollectionUtils.isNotEmpty(trips)) {
			isVirtualCreditCardSupported = true;
			for (TripInfo trip : trips) {
				CreditCardInfo creditCard = getVirtualCreditCard(trip, user, billingEntity);
				isVirtualCreditCardSupported = isVirtualCreditCardSupported && creditCard != null;
			}
		}
		return BooleanUtils.isTrue(isVirtualCreditCardSupported);
	}

	@Override
	public List<CreditCardInfo> getCreditCardInfoList(CreditCardFilter cardFilter) {
		List<DbCreditCardInfo> cardInfoList = ccInfoService.findAll(cardFilter);
		if (CollectionUtils.isEmpty(cardInfoList))
			return null;
		return DbCreditCardInfo.toDomainList(cardInfoList);
	}
}
