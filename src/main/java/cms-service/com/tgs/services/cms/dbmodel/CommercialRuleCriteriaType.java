package com.tgs.services.cms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.cms.datamodel.CommercialRuleCriteria;

public class CommercialRuleCriteriaType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return CommercialRuleCriteria.class;
    }
}