package com.tgs.services.cms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.CardType;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.utils.TgsSecurityUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardAdditionalInfo;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.cms.hibernate.CreditCardAdditionalInfoType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Entity
@Table(name = "creditcardinfo")
@Audited
@TypeDefs({@TypeDef(name = "CreditCardAdditionalInfoType", typeClass = CreditCardAdditionalInfoType.class)})
public class DbCreditCardInfo extends BaseModel<DbCreditCardInfo, CreditCardInfo> {

	@CreationTimestamp
	@Column
	private LocalDateTime createdOn;

	@Column
	@UpdateTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	private String product;

	@Column
	private String subProduct;

	@Column
	private String supplierId;

	@Column
	private CardType cardType;

	@Column
	private String bankName;

	@Column
	private String cardNumber;

	@Column
	private String expiry;

	@Column
	private String cvv;

	@Column
	private String holderName;

	@Column
	private String street;

	@Column
	private String city;

	@Column
	private String state;

	@Column
	private String country;

	@Column
	private String pinCode;

	@Column
	private String accountCode;

	@Column
	private double priority;

	@Column
	private boolean isDeleted;

	@Column
	private Double minPassthru;

	@Column
	@Type(type = "CreditCardAdditionalInfoType")
	private CreditCardAdditionalInfo additionalInfo;

	@Column
	private String userId;

	@Override
	public CreditCardInfo toDomain() {
		return toDomain(true);
	}

	public CreditCardInfo toDomain(boolean decrypt) {
		CreditCardInfo cci = new GsonMapper<>(this, CreditCardInfo.class).convert();
		if (decrypt) {
			try {
				cci.setCardNumber(TgsSecurityUtils.decryptData(getCardNumber()));
				cci.setCvv(TgsSecurityUtils.decryptData(getCvv()));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return cci;
	}

	@Override
	public DbCreditCardInfo from(CreditCardInfo dataModel) {
		return fromDomain(dataModel, true);
	}

	public DbCreditCardInfo fromDomain(CreditCardInfo dataModel, boolean encrypt) {
		CreditCardInfo storedDataModel = this.toDomain();
		CreditCardInfo merged = new GsonMapper<>(dataModel, storedDataModel, CreditCardInfo.class).convert();
		DbCreditCardInfo dbModel = new GsonMapper<>(merged, this, DbCreditCardInfo.class).convert();
		if (encrypt) {
			try {
				setCardNumber(TgsSecurityUtils.encryptData(getCardNumber()));
				setCvv(TgsSecurityUtils.encryptData(getCvv()));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return dbModel;
	}

}
