package com.tgs.services.cms.dbmodel;

import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;

public class IRuleCriteriaType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return IRuleCriteria.class;
    }
}
