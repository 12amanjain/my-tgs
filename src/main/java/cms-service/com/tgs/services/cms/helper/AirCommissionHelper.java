package com.tgs.services.cms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionFilter;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.cms.dbmodel.DBAirCommissionRule;
import com.tgs.services.cms.jparepository.commission.air.AirCommissionService;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirCommissionHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap airCommissionRulesMap;

	private static final String FIELD = "airCommission";
	
	private static Map<String, List<AirCommissionRule>> inMemoryCommissionRuleMap;

	@Autowired
	AirCommissionService airCommissionService;

	public AirCommissionHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap airCommissionRules) {
		super(configurationHashMap);
		AirCommissionHelper.airCommissionRulesMap = airCommissionRules;
	}

	@Override
	public void process() {
		processInMemory();
		if(MapUtils.isNotEmpty(inMemoryCommissionRuleMap)){
			inMemoryCommissionRuleMap.forEach((k,v) -> {
				airCommissionRulesMap.put(k, FIELD, v,
						CacheMetaInfo.builder().set(CacheSetName.IATA_COMMISSION.getName())
								.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
			});
		}
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<DBAirCommissionRule> dbAirCommissionRules =
				airCommissionService.findAll(AirCommissionFilter.builder().enabled(true).build());
		Map<String, List<AirCommissionRule>> ruleMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(dbAirCommissionRules)) {
			List<AirCommissionRule> airCommissionRules = BaseModel.toDomainList(dbAirCommissionRules);
			ruleMap = airCommissionRules.stream().collect(Collectors.groupingBy(rule -> getKey(rule)));
		}
		inMemoryCommissionRuleMap =
				Optional.<Map<String, List<AirCommissionRule>>>ofNullable(ruleMap).orElseGet(HashMap::new);
	}
	
	public static void cacheAirCommissionRule(AirCommissionRule airCommissionRule) {
		List<AirCommissionRule> commissionRules = getAirCommissionRules(getKey(airCommissionRule));
		// In case of update, remove old value from already cached data, since this code is called while updating status
		// also.
		commissionRules = commissionRules.stream().filter(p -> p.getId() != airCommissionRule.getId())
				.collect(Collectors.toList());
		// Add new value to save/update in cache
		commissionRules.add(airCommissionRule);
		inMemoryCommissionRuleMap.put(getKey(airCommissionRule), commissionRules);
	}

	@SuppressWarnings("unchecked")
	public static AirCommissionRule getAirCommissionRule(IFact flightFact, PriceInfo priceInfo, String airline) {
		List<AirCommissionRule> airCommissionRules = getAirCommissionRules(getKey(priceInfo, airline));
		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(airCommissionRules, flightFact, fieldResolverMap);
		airCommissionRules = (List<AirCommissionRule>) ruleEngine.fireAllRules();

		if (CollectionUtils.isNotEmpty(airCommissionRules)) {
			log.debug("Air commission rule found {} for airline {} ", GsonUtils.getGson().toJson(airCommissionRules.get(0)), airline);
			return airCommissionRules.get(0);
		}
		return null;
	}

	private static List<AirCommissionRule> getAirCommissionRules(String key) {
		List<AirCommissionRule> rules = new ArrayList<>();
		if(MapUtils.isEmpty(inMemoryCommissionRuleMap) || CollectionUtils.isEmpty(inMemoryCommissionRuleMap.get(key)))
			return rules;
		List<AirCommissionRule> cachedRules = inMemoryCommissionRuleMap.get(key);
		Gson gson = GsonUtils.getGson();
		cachedRules.forEach(item -> {
			AirCommissionRule airCommision = item.toBuilder().build();
			rules.add(airCommision);
		});
		log.debug("Air commission rules {} for key {} " , gson.toJson(rules), key);
		return rules;
	}

	public static String getKey(AirCommissionRule airCommissionRule) {
		return StringUtils.join(airCommissionRule.getAirline());
	}

	public static String getKey(PriceInfo priceInfo, String airline) {
		return StringUtils.join(airline);
	}

	@Override
	public void deleteExistingInitializer() {
		airCommissionRulesMap.truncate(CacheSetName.IATA_COMMISSION.getName());
	}
}
