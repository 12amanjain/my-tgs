package com.tgs.services.cms.helper;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;

@Service
public class CMSStaticContextInitializer {

	@Autowired
	GeneralServiceCommunicator gnComm;

	@PostConstruct
	public void init() {
		CreditCardHelper.init(gnComm);
	}

}
