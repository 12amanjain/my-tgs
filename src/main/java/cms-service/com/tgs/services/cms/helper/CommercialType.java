package com.tgs.services.cms.helper;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cms.datamodel.CommercialInfo;
import com.tgs.services.cms.datamodel.HotelCommercialInfo;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum CommercialType {

	TOURCODE("TourCode") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.TC;
		}
	},
	IATA("IATA") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.BCM;
		}
	},
	PLB("PLB") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.PLB;
		}
	},
	SM("Segment Money") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.SM;
		}
	},

	CC("CreditCard Cashback") {
		@Override
		public CommercialInfo updateTargetFareComponent(Double commissionAmount, FareDetail fareDetail,
				FareComponent targetComponent, Double threshold, User user, CommercialInfo info) {
			if (info != null) {
				CreditCardInfo cardInfo =
						CreditCardHelper.getCreditCardInfo(info.getBasicInfo().getSupplierId(), info.getSubProduct());
				if (cardInfo != null) {
					super.updateTargetFareComponent(commissionAmount, fareDetail, targetComponent, threshold, user,
							info);
					if (info.getIsUpdated()) {
						info.setCcInfoId(cardInfo.getId());
					}
				}
			}
			return info;
		}

		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.CC;
		}
	},
	MF("Management Fee") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.MF;
		}
	},
	AI("Additional Incentive") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.AI;
		}
	},
	CAP("Cut and Pay") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.PFCM;
		}
	},
	MU("Markup") {
		@Override
		public FareComponent getTargetComponent() {
			return FareComponent.CMU;
		}

		@Override
		public HotelFareComponent getHotelTargetComponent() {
			return HotelFareComponent.CMU;
		}
	},
	DS("Discount") {
		@Override
		public FareComponent getTargetComponent() {
			return null;
		}

		@Override
		public HotelFareComponent getHotelTargetComponent() {
			return HotelFareComponent.DS;
		}
	};

	public String desc;

	CommercialType(String description) {
		this.desc = description;
	}

	public CommercialInfo updateHotelTargetFareComponent(Double commissionAmount, HotelInfo hInfo, Option option,
			HotelFareComponent targetComponent, Double threshold, User user, HotelCommercialInfo cInfo,
			PriceInfo priceInfo) {

		targetComponent = getHotelTargetComponent(targetComponent);
		Double thresholdAmount = threshold;
		if (targetComponent != null) {
			if (Objects.nonNull(thresholdAmount) && commissionAmount > thresholdAmount) {
				log.error(
						"Option amount is set to threshold amount {} because it is exceeded by commission amount {} for commercial component {}, hotel id {} and option id {}",
						thresholdAmount, commissionAmount, this.desc, hInfo.getId(), option.getId());
				commissionAmount = thresholdAmount;
			}

			if ((targetComponent.isPriceValidationRequired() || commissionAmount < 0)
					&& !isValidCommission(priceInfo, Math.abs(commissionAmount))) {
				cInfo.setIsOptionRequired(false);
				return cInfo;
			}
			priceInfo.getFareComponents().put(targetComponent,
					priceInfo.getFareComponents().getOrDefault(targetComponent, 0.0) + commissionAmount);
			option.getMiscInfo().setIsCommissionApplied(true);
			cInfo.setIsUpdated(Boolean.TRUE);
		} else {
			log.error("Target component is null for commercial component {}, hotel id {} and option id {}", this.desc,
					hInfo.getId(), option.getId());
		}
		return cInfo;
	}

	private static boolean isValidCommission(PriceInfo priceInfo, Double perNightCommissionAmount) {

		double totalOptionFare = priceInfo.getMiscInfo().getSupplierPrice()
				+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.CMU, 0.0)
				- priceInfo.getFareComponents().getOrDefault(HotelFareComponent.DS, 0.0);
		return perNightCommissionAmount < totalOptionFare;
	}

	public CommercialInfo updateTargetFareComponent(Double commissionAmount, FareDetail fareDetail,
			FareComponent targetComponent, Double threshold, User user, CommercialInfo cInfo) {
		targetComponent = getTargetComponent(targetComponent);
		Double thresholdAmount = threshold;
		if (targetComponent != null) {
			if (Objects.nonNull(thresholdAmount) && commissionAmount > thresholdAmount) {
				log.error("{} set to Threshold because Threshold amount {} exceeded by Commission Amount {} ",
						this.desc, thresholdAmount, commissionAmount);
				commissionAmount = thresholdAmount;
			}
			BaseUtils.updateFareComponent(fareDetail.getFareComponents(), targetComponent, commissionAmount, user);
			cInfo.setIsUpdated(Boolean.TRUE);
		} else {
			log.error("Target Component Should Not be null for {}", this.desc);
		}
		return cInfo;
	}

	public Double getCommission(String expression, FareDetail fareDetail) {
		return BaseUtils.evaluateExpression(expression, fareDetail, this.desc, true);
	}

	public Double getHotelCommission(String expression, PriceInfo priceInfo) {
		return BaseHotelUtils.evaluateHotelExpression(expression, priceInfo);
	}

	public HotelFareComponent getHotelTargetComponent(HotelFareComponent targetComponent) {
		if (targetComponent != null) {
			return targetComponent;
		}
		return getHotelTargetComponent();
	}

	public FareComponent getTargetComponent(FareComponent targetComponent) {
		if (targetComponent != null) {
			return targetComponent;
		}
		return getTargetComponent();
	}

	public HotelFareComponent getHotelTargetComponent() {
		return null;
	}

	public abstract FareComponent getTargetComponent();

	// return available commercial type
	public List<CommercialType> getCommercialType() {
		return Arrays.asList(CommercialType.values());
	}
}
