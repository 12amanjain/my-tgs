package com.tgs.services.cms.helper;

import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cms.datamodel.AirCommercialConditions;
import com.tgs.services.cms.datamodel.CommercialComponent;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialComponent;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommercialUtils {

	/**
	 * rule before Applying commission will be handled
	 */
	public static boolean isAirRuleApplicable(SegmentInfo segmentInfo, CommercialComponent commercialRule,
			PaxType paxType) {
		boolean isApplicable = true;
		AirCommercialConditions aCond = (AirCommercialConditions) commercialRule.getCondition();
		// no condition check
		if (aCond == null && !PaxType.INFANT.equals(paxType)) {
			return true;
		}
		if (aCond != null) {
			if (BooleanUtils.isTrue(aCond.getIsApplicableOnBooking())) {
				isApplicable = segmentInfo.getSegmentNum().equals(0)
						&& BooleanUtils.isNotTrue(segmentInfo.getIsReturnSegment());
			} else if (Objects.nonNull(aCond.getIsApplicableOnAllSegment())) {
				// First Segment By Default Applicable
				isApplicable = segmentInfo.getSegmentNum().equals(0) ? true : aCond.getIsApplicableOnAllSegment();
			}
		}
		if (paxType.equals(PaxType.INFANT) && (aCond == null || Objects.nonNull(aCond.getIsApplicableOnInfant()))) {
			isApplicable = aCond == null ? false : BooleanUtils.isTrue(aCond.getIsApplicableOnInfant());
		}

		return isApplicable;
	}

	public static String getFareComponentExpression(String expression, FareDetail fareDetail,
			boolean areCoefficientsPercentage) {
		for (FareComponent fareComponent : FareComponent.values()) {
			Double amount = fareDetail.getFareComponents().getOrDefault(fareComponent, 0.0);
			expression =
					expression.replaceAll("%\\*\\b" + fareComponent.name() + "\\b", "*" + String.valueOf(amount / 100));
			expression = expression.replaceAll("\\b" + fareComponent.name() + "\\b", String.valueOf(amount));
		}
		return expression;
	}

	public static Class<? extends IRuleCriteria> getConvertType(String product) {
		if (OrderType.getEnumFromCode(product).equals(OrderType.AIR)) {
			return FlightBasicRuleCriteria.class;
		}
		return null;
	}

	/**
	 * rule before Applying commission will be handled
	 */
	public static boolean isAirRuleApplicable(SegmentInfo segmentInfo, AirCommercialComponent commercialComponent,
			PaxType paxType) {
		boolean isApplicable = true;
		if (Objects.isNull(commercialComponent.getCondition())) {
			return true;
		}

		AirCommercialConditions aCond = commercialComponent.getCondition();
		// First Segment By Default Applicable
		if (Objects.nonNull(aCond.getIsApplicableOnAllSegment())) {
			isApplicable = segmentInfo.getSegmentNum().equals(0) ? true
					: BooleanUtils.isTrue(aCond.getIsApplicableOnAllSegment());
		}
		if (isApplicable && Objects.nonNull(aCond.getIsApplicableOnInfant()) && paxType.equals(PaxType.INFANT)) {
			isApplicable = aCond.getIsApplicableOnInfant();
		}

		return isApplicable;
	}

	public static boolean isUserCommissionApplicable(User user) {
		if (user != null && UserUtils.getParentUserConf(user) != null
				&& MapUtils.isNotEmpty(UserUtils.getParentUserConf(user).getCommPlanMap())) {
			return true;
		}
		return false;
	}

}
