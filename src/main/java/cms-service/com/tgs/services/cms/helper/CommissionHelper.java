package com.tgs.services.cms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cms.datamodel.commission.CommissionFilter;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.datamodel.commission.CommissionRule;
import com.tgs.services.cms.dbmodel.CommissionPlanMapper;
import com.tgs.services.cms.dbmodel.DbCommissionPlan;
import com.tgs.services.cms.dbmodel.DbCommissionRule;
import com.tgs.services.cms.jparepository.commission.CommissionPlanMapperService;
import com.tgs.services.cms.jparepository.commission.CommissionPlanService;
import com.tgs.services.cms.jparepository.commission.CommissionRuleService;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CommissionHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap commissionRules;

	private static Map<Long, List<CommissionRule>> inMemoryCommissionRuleMap = new HashMap<>();

	private static Map<Long, String> inMemoryCommissionPlanMap = new HashMap<>();

	@Autowired
	CommissionRuleService ruleService;

	@Autowired
	CommissionPlanMapperService mapperService;

	@Autowired
	CommissionPlanService planService;

	public CommissionHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap commissionRules) {
		super(configurationHashMap);
		CommissionHelper.commissionRules = commissionRules;
	}

	@Override
	public void process() {
		processInMemory();
		if (MapUtils.isNotEmpty(inMemoryCommissionRuleMap)) {
			inMemoryCommissionRuleMap.forEach((availablePlanId, mappedRules) -> {
				commissionRules.put(availablePlanId.toString(), "rules", mappedRules,
						CacheMetaInfo.builder().set(CacheSetName.COMMISSION_RULE.getName())
								.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(true).build());
			});
		}
		if (MapUtils.isNotEmpty(inMemoryCommissionPlanMap)) {
			inMemoryCommissionPlanMap.forEach((commissionPlanId, commissionPlan) -> {
				commissionRules.put(commissionPlan.toString(), "plan", commissionPlan,
						CacheMetaInfo.builder().set(CacheSetName.COMMISSION_PLAN.getName())
								.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(true).build());
			});
		}
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	public void processInMemory() {
		List<DbCommissionPlan> availablePlans = planService.findAll(CommissionFilter.builder().build());
		availablePlans.forEach(availablePlan -> {
			storeInCache(availablePlan);
		});
	}

	public void storeInCache(DbCommissionPlan availablePlan) {
		List<CommissionPlanMapper> planMapper = mapperService.findByCommPlanID(availablePlan.getId().intValue());
		storeInCache(planMapper, availablePlan);
	}

	public void storeInCache(List<CommissionPlanMapper> planMapper, DbCommissionPlan availablePlan) {
		List<Integer> ruleIds =
				planMapper.stream().map(CommissionPlanMapper::getCommissionRuleId).collect(Collectors.toList());
		List<CommissionRule> mappedRules;
		if (!ruleIds.isEmpty()) {
			List<DbCommissionRule> rules =
					ruleService.findAll(CommissionFilter.builder().deleted(false).ruleIds(ruleIds).build());
			mappedRules = DbCommissionRule.toDomainList(rules);
			mappedRules.forEach(rule -> {
				if (rule.getProduct().equals(Product.AIR)) {
					((FlightBasicRuleCriteria) rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()))
							.setAirType(rule.getAirType());
					((FlightBasicRuleCriteria) rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()))
							.setSearchType(rule.getSearchType());
				} else if (rule.getProduct().equals(Product.HOTEL)) {
					((HotelBasicRuleCriteria) rule.getInclusionRuleCriteria(new HotelBasicRuleCriteria()))
							.setHotelType(null);
				}
			});
		} else {
			mappedRules = new ArrayList<>();
		}

		inMemoryCommissionRuleMap.put(availablePlan.getId(), mappedRules);
		createUpdatePlanInCache(availablePlan);
	}

	public void updateCache(Long ruleId) {
		List<CommissionPlanMapper> planMapper = mapperService.findByCommissionRuleId(ruleId.intValue());
		if (CollectionUtils.isNotEmpty(planMapper)) {
			List<Integer> commPlanIds =
					planMapper.stream().map(CommissionPlanMapper::getCommPlanId).collect(Collectors.toList());
			List<DbCommissionPlan> availablePlans = planService.findAll(commPlanIds);
			availablePlans.forEach(availablePlan -> {
				storeInCache(availablePlan);
			});
		}
	}

	public void createUpdatePlanInCache(DbCommissionPlan commissionPlan) {
		inMemoryCommissionPlanMap.put(commissionPlan.getId(), GsonUtils.getGson().toJson(commissionPlan.toDomain()));
	}

	@Override
	public void deleteExistingInitializer() {
		if (MapUtils.isNotEmpty(inMemoryCommissionPlanMap))
			inMemoryCommissionPlanMap.clear();
		if (MapUtils.isNotEmpty(inMemoryCommissionRuleMap))
			inMemoryCommissionRuleMap.clear();
		commissionRules.truncate(CacheSetName.COMMISSION_PLAN.getName());
		commissionRules.truncate(CacheSetName.COMMISSION_RULE.getName());
	}

	@SuppressWarnings("unchecked")
	public static CommissionRule getCommissionRule(IFact fact, User user, String planKey,
			Map<String, ? extends IRuleField> fieldResolverMap) {
		List<CommissionRule> commissionRules = getCommissionRulesOnUserCommissionPlanID(user, planKey);
		if (CollectionUtils.isNotEmpty(commissionRules)) {
			commissionRules = commissionRules.stream().filter(rule -> {
				return (StringUtils.isEmpty(rule.getCode())
						|| rule.getCode().equals(((FlightBasicFact) fact).getAirline()));
			}).collect(Collectors.toList());

			log.debug("Total rules applicable for plankey {} is {}", planKey, commissionRules.size());

			/**
			 * Adding rule.getCode() into inclusionCriteria.airlineList for each rule which has non-empty code. Empty
			 * code means it applies to all airlines.
			 */
			for (CommissionRule rule : commissionRules) {
				if (!StringUtils.isEmpty(rule.getCode())) {
					List<String> airlines =
							((FlightBasicRuleCriteria) rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()))
									.getAirlineList();
					if (airlines == null) {
						((FlightBasicRuleCriteria) rule.getInclusionCriteria())
								.setAirlineList(airlines = new ArrayList<>());
					}
					if (!airlines.contains(rule.getCode()))
						airlines.add(rule.getCode());
				}
			}

			CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(commissionRules, fact, fieldResolverMap);
			commissionRules = (List<CommissionRule>) ruleEngine.fireAllRules();

			// log.debug("Total rules satisfied after rule engine for plankey {} is {}",
			// planKey, commissionRules.size());

			if (CollectionUtils.isNotEmpty(commissionRules)) {
				// log.info("Total rules satisfied for fact {} is {}", fact,
				// commissionRules.size());
				return commissionRules.get(0);
			}
		}
		return null;
	}

	private static List<CommissionRule> getCommissionRulesOnUserCommissionPlanID(User user, String plankey) {
		List<CommissionRule> rules = new ArrayList<>();
		if (user == null || UserUtils.getParentUserConf(user) == null
				|| UserUtils.getParentUserConf(user).getPlanId(plankey) == null) {
			log.info("User conf is null or plan is not defined for userId {}", UserUtils.getUserId(user));
			return null;
		}
		Integer planId = UserUtils.getParentUserConf(user).getPlanId(plankey);
		if (MapUtils.isEmpty(inMemoryCommissionRuleMap)
				|| CollectionUtils.isEmpty(inMemoryCommissionRuleMap.get(Long.valueOf(planId))))
			return rules;
		List<CommissionRule> cachedRules = inMemoryCommissionRuleMap.get(Long.valueOf(planId));
		log.debug("Fetched commissionRules for userId {} and planKey {} ", user.getUserId(), plankey);
		cachedRules.forEach(item -> {
			// Creates a new copy.
			CommissionRule cr = item.toBuilder().build();
			rules.add(cr);
		});
		log.debug("Copy of commissionRules created userId {}  and planKey {} ", user.getUserId(), plankey);
		return rules;
	}

	public static CommissionPlan getCommissionPlanFromCache(String planId) {
		if (StringUtils.isNotEmpty(planId)) {
			CommissionPlan plan = GsonUtils.getGson().fromJson(inMemoryCommissionPlanMap.get(Long.valueOf(planId)),
					CommissionPlan.class);
			if (plan == null) {
				log.info("Plan id is : {} ", planId);
			}
			return plan;
		}
		return null;
	}
}
