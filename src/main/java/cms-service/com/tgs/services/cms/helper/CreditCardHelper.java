package com.tgs.services.cms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.filters.GstInfoFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsSecurityUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.cms.dbmodel.DbCreditCardInfo;
import com.tgs.services.cms.jparepository.creditcard.CreditCardInfoService;
import com.tgs.services.ums.datamodel.User;

@Slf4j
@Service
public class CreditCardHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap ccRules;

	private static final String FIELD = "credit_card";

	@Autowired
	CreditCardInfoService ccService;

	private static GeneralServiceCommunicator gmsCommunicator;

	public static void init(GeneralServiceCommunicator gnComm) {
		gmsCommunicator = gnComm;
	}

	public CreditCardHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap ccRules) {
		super(configurationHashMap);
		CreditCardHelper.ccRules = ccRules;
	}

	@Override
	public void process() {
		/**
		 * It has concurrency issue -- fetching credit card info might fail for some threads during this cache reload.
		 */
		modifyTTLOfSet();
		List<DbCreditCardInfo> dbCcList = ccService.findAll(CreditCardFilter.builder().build());
		if (CollectionUtils.isNotEmpty(dbCcList)) {
			List<CreditCardInfo> ccList = new ArrayList<>();
			dbCcList.forEach(cc -> ccList.add(cc.toDomain(false)));
			updateCache(ccList);
		}
	}

	private void updateCache(List<CreditCardInfo> ccList) {
		ccList.sort(new Comparator<CreditCardInfo>() {
			@Override
			public int compare(CreditCardInfo c1, CreditCardInfo c2) {
				return c1.getPriority() > c2.getPriority() ? -1 : 1;
			}
		});

		Map<String, List<CreditCardInfo>> supplierWiseCreditCardList =
				ccList.stream().collect(Collectors.groupingBy(CreditCardInfo::getSupplierId));

		supplierWiseCreditCardList.forEach((key, value) -> {
			ccRules.put(key, FIELD, value, CacheMetaInfo.builder().set(CacheSetName.CREDIT_CARD.getName())
					.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		});
	}

	public void refreshCache(String supplierId) {
		List<DbCreditCardInfo> dbCcList = ccService.findAll(CreditCardFilter.builder().supplierId(supplierId).build());
		if (CollectionUtils.isNotEmpty(dbCcList)) {
			List<CreditCardInfo> ccList = new ArrayList<>();
			dbCcList.forEach(cc -> ccList.add(cc.toDomain(false)));
			updateCache(ccList);
		}
	}

	@Override
	public void deleteExistingInitializer() {
		ccRules.truncate(CacheSetName.CREDIT_CARD.getName());
	}

	public void modifyTTLOfSet() {
		try {
			ccRules.modifyTtl(CacheSetName.CREDIT_CARD.getName(), 10);
		} catch (Exception e) {
			deleteExistingInitializer();
		}
	}

	public static CreditCardInfo getCreditCardInfo(String supplierId, String subProduct) {
		List<CreditCardInfo> creditCardRules = getCreditCardInfos(supplierId);
		if (CollectionUtils.isNotEmpty(creditCardRules)) {
			for (CreditCardInfo info : creditCardRules) {
				if (BooleanUtils.isTrue(info.getEnabled())
						&& (StringUtils.isEmpty(info.getSubProduct()) || info.getSubProduct().contains(subProduct))) {
					String userId = SystemContextHolder.getContextData().getUser().getParentUserId();
					if (StringUtils.isBlank(info.getUserId()) && (info.getAdditionalInfo() == null
							|| CollectionUtils.isEmpty(info.getAdditionalInfo().getBillingEntityIds()))) {
						// Supplier credit card
						return info;
					} else if (userId.equals(info.getUserId())) {
						// Corporate virtual credit card.
						return info;
					}
				}
			}
		}
		return null;
	}

	public static CreditCardInfo getCorporateCreditCardInfo(String supplierId, String subProduct,
			GstInfo orderBillingEntity, User user) {
		List<CreditCardInfo> creditCardRules = getCreditCardInfos(supplierId);
		if (CollectionUtils.isNotEmpty(creditCardRules)) {
			for (CreditCardInfo info : creditCardRules) {
				if (BooleanUtils.isTrue(info.getEnabled())
						&& (StringUtils.isEmpty(info.getSubProduct()) || info.getSubProduct().contains(subProduct))) {
					if (user != null && user.getParentUserId().equals(info.getUserId())) {
						if (orderBillingEntity != null && info.getAdditionalInfo() != null
								&& CollectionUtils.isNotEmpty(info.getAdditionalInfo().getBillingEntityIds())) {
							// orderBillingEntity may not have id in some cases ex: Booking, confirm booking,
							// So we are fetching billing Entities of each card info.
							GstInfoFilter filter =
									GstInfoFilter.builder().idIn(info.getAdditionalInfo().getBillingEntityIds())
											.userIdIn(Arrays.asList(user.getParentUserId())).build();
							List<GstInfo> billingEntities = gmsCommunicator.searchBillingEntity(filter);
							if (CollectionUtils.isNotEmpty(billingEntities)) {
								List<GstInfo> matchedBillingEntity = billingEntities.stream()
										.filter(be -> be.getGstNumber().equals(orderBillingEntity.getGstNumber())
												&& be.getBillingCompanyName()
														.equals(orderBillingEntity.getBillingCompanyName()))
										.collect(Collectors.toList());
								if (CollectionUtils.isNotEmpty(matchedBillingEntity)) {
									return info;
								}
							}
						} else if (info.getAdditionalInfo() == null
								|| CollectionUtils.isEmpty(info.getAdditionalInfo().getBillingEntityIds())) {
							return info;
						}
					}
				}
			}
		}
		return null;
	}

	@SuppressWarnings({"unchecked", "serial"})
	private static List<CreditCardInfo> getCreditCardInfos(String supplierId) {
		List<CreditCardInfo> ruleList = new ArrayList<>();
		ruleList = ccRules.get(supplierId, FIELD, ruleList.getClass(),
				CacheMetaInfo.builder().set(CacheSetName.CREDIT_CARD.getName())
						.typeOfT(new TypeToken<List<CreditCardInfo>>() {}.getType()).build());
		if (CollectionUtils.isNotEmpty(ruleList)) {
			/**
			 * Temporary fix - creating copy of cached list. This code should be moved to
			 * {@link AerospikeHashMap#get(String, String, Class<V>, CacheMetaInfo)}
			 */
			Gson gson = GsonUtils.getGson();
			String json = gson.toJson(ruleList);
			List<CreditCardInfo> ruleListCopy = gson.fromJson(json, new TypeToken<List<CreditCardInfo>>() {}.getType());
			Collections.sort(ruleListCopy, (first, second) -> (first.getPriority() >= second.getPriority() ? -1 : 1));
			ruleListCopy.forEach(cc -> {
				try {
					cc.setCardNumber(TgsSecurityUtils.decryptData(cc.getCardNumber()));
					cc.setCvv(TgsSecurityUtils.decryptData(cc.getCvv()));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			});
			return ruleListCopy;
		}
		return ruleList;
	}

}
