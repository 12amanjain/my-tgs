package com.tgs.services.cms.helper;

import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialComponent;
import com.tgs.services.cms.datamodel.tourcode.TourCode;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TourCodeEngine {


	public void processTourCodeCommission(TripInfo tripInfo, User user, AirSearchQuery searchQuery) {


		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			String airlineCode = segmentInfo.getFlightDesignator().getAirlineInfo().getCode();
			FlightBasicFact flightFact =
					FlightBasicFact.builder().build().generateFact(tripInfo).generateFact(searchQuery);
			flightFact.setAirType(BaseUtils.getTripType(tripInfo));
			BaseUtils.createFactOnUser(flightFact, user);
			if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
				for (int priceIndex = 0; priceIndex < segmentInfo.getPriceInfoList().size(); priceIndex++) {
					flightFact.generateFact(tripInfo, priceIndex);
					PriceInfo priceInfo = segmentInfo.getPriceInfoList().get(priceIndex);
					if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
						try {
							flightFact.generateFact(priceInfo, PaxType.ADULT);
							String plattingAirline =
									ObjectUtils.firstNonNull(priceInfo.getPlattingCarrier(), airlineCode);
							TourCode tourCode = TourCodeHelper.getTourCodeRule(flightFact, plattingAirline);
							if (tourCode != null) {
								AirCommercialComponent component = tourCode.getTourCodeCriteria();
								if (Objects.nonNull(component) && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
									priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
										if (CommercialUtils.isAirRuleApplicable(segmentInfo, component, paxType)) {
											// CommercialType type = CommercialType
											// .valueOf(com.tgs.services.cms.datamodel.commission.CommercialType.TOURCODE
											// .getName());
											// Double amount = type.getCommission(component.getExpression(),
											// fareDetail);
											// type.updateTargetFareComponent(amount, fareDetail, FareComponent.TC,
											// component.getThresholdAmount(), user, commercialInfo);
											priceInfo.getMiscInfo().setTourCode(tourCode.getTourCode());
										}
									});
								}
							}
						} catch (Exception e) {
							log.error("Tour Code Not Applied Error on trip {}", tripInfo.toString(), e);
							throw new CustomGeneralException(SystemError.COMMISSION_FAILED);
						}
					}
				}
			}
		});
	}

}
