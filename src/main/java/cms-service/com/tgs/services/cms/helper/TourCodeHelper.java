package com.tgs.services.cms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cms.datamodel.tourcode.TourCode;
import com.tgs.services.cms.datamodel.tourcode.TourCodeFilter;
import com.tgs.services.cms.dbmodel.DbTourCode;
import com.tgs.services.cms.jparepository.tourcode.TourCodeService;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;

@Service
public class TourCodeHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap tourCodeRules;

	private static final String FIELD = "tour_code", KEY = "tour_code";

	private static Map<String, String> inmemoryTourCode = new HashMap<>();

	@Autowired
	TourCodeService tourCodeService;

	public TourCodeHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap tourCodeRules) {
		super(configurationHashMap);
		TourCodeHelper.tourCodeRules = tourCodeRules;
	}

	@Override
	public void process() {
		processInMemory();
		if (MapUtils.isNotEmpty(inmemoryTourCode)) {
			List<TourCode> tCodes = new ArrayList<>();
			inmemoryTourCode.forEach((k, v) -> {
				tCodes.addAll(GsonUtils.getGson().fromJson(inmemoryTourCode.get(k),
						new TypeToken<List<TourCode>>() {}.getType()));
			});
			updateCache(tCodes);
		}
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<TourCode> tourCodeList = new ArrayList<>();
		List<DbTourCode> dbRules = tourCodeService.findAll(TourCodeFilter.builder().enabled(true).build());
		Map<String, String> tourCodes = new HashMap<>();
		if (CollectionUtils.isNotEmpty(dbRules)) {
			tourCodeList = BaseModel.toDomainList(dbRules);
			Map<String, List<TourCode>> tourCodesRules =
					tourCodeList.stream().collect(Collectors.groupingBy(tourCode -> getKey(tourCode)));
			tourCodesRules.forEach((airline, rules) -> {
				tourCodes.put(airline, GsonUtils.getGson().toJson(rules));
			});
		}
		inmemoryTourCode = tourCodes;
	}

	public String getKey(TourCode tourCode) {
		return tourCode.getAirline();
	}

	public void updateCache(List<TourCode> rules) {
		List<TourCode> tourCodes = new ArrayList<>();
		rules.forEach(rule -> {
			rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()).setAirType(rule.getAirType());
			rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()).setSearchType(rule.getSearchType());
			tourCodes.add(rule);
		});

		if (CollectionUtils.isNotEmpty(tourCodes)) {
			tourCodeRules.put(KEY, FIELD, tourCodes, CacheMetaInfo.builder().set(CacheSetName.TOUR_CODE.getName())
					.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		}
	}

	public static TourCode getTourCodeRule(IFact flightFact, String plattingAirline) {

		List<TourCode> tourCodeRules = getTourCodeRules(plattingAirline);
		if (CollectionUtils.isEmpty(tourCodeRules)) {
			return null;
		}

		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(tourCodeRules, flightFact, fieldResolverMap);

		List<TourCode> tourCodesList = (List<TourCode>) ruleEngine.fireAllRules();

		if (CollectionUtils.isNotEmpty(tourCodesList)) {
			return tourCodesList.get(0);
		}
		return null;
	}

	@SuppressWarnings({"serial"})
	private static List<TourCode> getTourCodeRules(String airline) {
		List<TourCode> ruleList = new ArrayList<>();
		if (MapUtils.isNotEmpty(inmemoryTourCode) && MapUtils.getObject(inmemoryTourCode, airline) != null) {
			ruleList.addAll(GsonUtils.getGson().fromJson(inmemoryTourCode.get(airline),
					new TypeToken<List<TourCode>>() {}.getType()));
		}

		return ruleList;
	}

	@Override
	public void deleteExistingInitializer() {
		tourCodeRules.truncate(CacheSetName.TOUR_CODE.getName());
	}

}
