package com.tgs.services.cms.helper.air;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cms.datamodel.CommercialComponent;
import com.tgs.services.cms.datamodel.CommercialInfo;
import com.tgs.services.cms.datamodel.CommercialRuleCriteria;
import com.tgs.services.cms.datamodel.commission.CommissionRule;
import com.tgs.services.cms.helper.CommercialType;
import com.tgs.services.cms.helper.CommercialUtils;
import com.tgs.services.cms.helper.CommissionHelper;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirCommissionEngine {

	@Autowired
	UserServiceCommunicator userService;

	static Map<String, ? extends IRuleField> resolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

	/**
	 * @param tripInfo
	 * @param user
	 * @param searchQuery required only for {@code com.tgs.services.fms.datamodel.SearchType}
	 * @implNote : No where should swap user partneruser & user, ends up in showing different TDS if any
	 */
	public void processCommission(TripInfo tripInfo, User user, AirSearchQuery searchQuery) {

		User partnerUser = null;
		boolean isPartnerCommissionPlan = false;

		try {

			boolean isCommissionApp = isCommissionApplicableOnTrip(user, tripInfo, searchQuery);

			if (!isCommissionApp && UserUtils.isPartnerUser(user)) {
				UserFilter userFilter = UserFilter.builder().userId(user.getPartnerId()).build();
				partnerUser = userService.getUser(userFilter);
				isCommissionApp = isCommissionApplicableOnTrip(partnerUser, tripInfo, searchQuery);
				isPartnerCommissionPlan = true;
			}

			if (isCommissionApp && tripInfo.isPriceInfosNotEmpty()) {
				FlightBasicFact flightFact = FlightBasicFact.builder().build();
				flightFact.generateFact(searchQuery);
				flightFact.generateFact(tripInfo);
				flightFact.setAirType(BaseUtils.getTripType(tripInfo));
				BaseUtils.createFactOnUser(flightFact, user);
				for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
					String platingCarrier = tripInfo.getPlatingCarrier();
					flightFact.generateFact(segmentInfo);
					/**
					 * Operating airline has to be for entire trip instead of segment wise
					 */
					flightFact.setOperatingAirline(tripInfo.getAirlineInfo(true));
					if (segmentInfo.getBookingRelatedInfo() != null) {
						// Its Applicable for All Pax - Mostly from Post Booking

						List<FlightTravellerInfo> travellerInfos =
								segmentInfo.getBookingRelatedInfo().getTravellerInfo();
						if (CollectionUtils.isNotEmpty(travellerInfos)) {
							for (FlightTravellerInfo travellerInfo : travellerInfos) {
								PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
								flightFact.setAccountCode(priceInfo.getMiscInfo().getAccountCode());
								Map<PaxType, FareDetail> fareDetail = new HashMap<>();
								priceInfo.setFareDetails(fareDetail);
								fareDetail.put(travellerInfo.getPaxType(), travellerInfo.getFareDetail());
								applyCommission(tripInfo, 0, priceInfo, segmentInfo, flightFact, user, platingCarrier,
										isPartnerCommissionPlan, partnerUser);
								travellerInfo.setFareDetail(fareDetail.get(travellerInfo.getPaxType()));
								priceInfo.setFareDetails(null);
							}
						}
					} else {
						// Its Applicable for Pax Type Wise - Mostly from Pre Booking
						if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
							for (int priceIndex = 0; priceIndex < segmentInfo.getPriceInfoList().size(); priceIndex++) {
								flightFact.generateFact(tripInfo, priceIndex);
								PriceInfo priceInfo = segmentInfo.getPriceInfoList().get(priceIndex);
								if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
									applyCommission(tripInfo, priceIndex, priceInfo, segmentInfo, flightFact, user,
											platingCarrier, isPartnerCommissionPlan, partnerUser);
								}
							}
						}
					}
				}

			}
		} catch (Exception e) {
			log.error("Commission Not Applied on trip {}", tripInfo, e);
			throw new CustomGeneralException(SystemError.COMMISSION_FAILED);
		}
	}


	private boolean isCommissionApplicableOnTrip(User user, TripInfo tripInfo, AirSearchQuery searchQuery) {
		boolean isApplicable = false;
		if (user != null) {
			String commissionPlanKey = OrderType.AIR.name() + "_" + BaseUtils.getTripType(tripInfo).getCode();
			if (CommercialUtils.isUserCommissionApplicable(user) && UserUtils.getParentUserConf(user).getCommPlanMap()
					.getOrDefault(commissionPlanKey, UserUtils.getParentUserConf(user).getCommPlanMap()
							.get(OrderType.AIR.name() + "_" + AirType.ALL.getCode())) != null) {
				isApplicable = true;
			}
		}

		// Incase of credit shell search Commission is not applicable
		if (searchQuery.isPNRCreditSearch()) {
			isApplicable = false;
		}
		return isApplicable;
	}

	/**
	 * This will update which commission rule is applied and Which plan applied for that user during that time
	 */
	public static void updateMiscInfo(PriceInfo priceInfo, CommercialInfo cInfo) {
		if (BooleanUtils.isTrue(cInfo.getIsUpdated())) {
			PriceMiscInfo miscInfo = priceInfo.getMiscInfo();
			miscInfo.setCommericialRuleId(Long.valueOf(cInfo.getCommissionId()));
			if (cInfo.getCcInfoId() != null) {
				miscInfo.setCcInfoId(cInfo.getCcInfoId().intValue());
			}
		}
	}

	public static String getCommissionPlanKey(FlightBasicFact fact) {
		String commissionPlanKey = null;
		if (fact.getAirType() != null) {
			AirType airType = fact.getAirType();
			if (airType.equals(AirType.DOMESTIC)) {
				commissionPlanKey = OrderType.AIR.name() + "_" + AirType.DOMESTIC.getCode();
			} else if (airType.equals(AirType.INTERNATIONAL)) {
				commissionPlanKey = OrderType.AIR.name() + "_" + AirType.INTERNATIONAL.getCode();
			}
		} else {
			commissionPlanKey = OrderType.AIR.name() + "_" + AirType.ALL.getCode();
		}
		return commissionPlanKey;
	}

	public void applyCommission(TripInfo tripInfo, int priceIndex, PriceInfo priceInfo, SegmentInfo segmentInfo,
			FlightBasicFact flightFact, User user, String platingCarrier, boolean isPartnerCommissionPlan,
			User partnerUser) {

		String commissionPlanKey = getCommissionPlanKey(flightFact);
		priceInfo.getFareDetails().forEach((pax, fareDetail) -> {
			if (tripInfo.getTripPriceInfos() != null
					&& MapUtils.isNotEmpty(tripInfo.getTripPriceInfos().get(priceIndex).getFareDetails())) {
				flightFact.setTripFareComponents(
						tripInfo.getTripPriceInfos().get(priceIndex).getFareDetail(pax).getFareComponents());
			}
			flightFact.generateFact(priceInfo, pax);

			CommissionRule commissionRule = null;
			if (isPartnerCommissionPlan) {
				commissionRule =
						CommissionHelper.getCommissionRule(flightFact, partnerUser, commissionPlanKey, resolverMap);
			} else {
				commissionRule = CommissionHelper.getCommissionRule(flightFact, user, commissionPlanKey, resolverMap);
			}

			if (Objects.nonNull(commissionRule)) {
				CommercialInfo commercialInfo = CommercialInfo.builder().basicInfo(priceInfo.getSupplierBasicInfo())
						.subProduct(platingCarrier).build();
				commercialInfo.setCommissionId(Long.valueOf(commissionRule.getId()));
				CommercialRuleCriteria ruleCriteria = commissionRule.getCommercialCriteria();
				// log.error("Applying ruleId {} for carrier {}", commissionRule.getId(),
				// platingCarrier);
				if (CollectionUtils.isNotEmpty(ruleCriteria.getComponents())) {
					List<CommercialComponent> components = ruleCriteria.getComponents();
					components.forEach(commercialComponent -> {
						if (CommercialUtils.isAirRuleApplicable(segmentInfo, commercialComponent, pax)) {
							CommercialType type = CommercialType.valueOf(commercialComponent.getType().getName());
							Double amount = type.getCommission(commercialComponent.getExpression(), fareDetail);
							// always it should be end user
							type.updateTargetFareComponent(amount, fareDetail, null,
									commercialComponent.getThresholdAmount(), user, commercialInfo);
						}
					});
					updateMiscInfo(priceInfo, commercialInfo);
				}
			}
		});
	}
}
