
package com.tgs.services.cms.helper.hotel;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cms.datamodel.CommercialComponent;
import com.tgs.services.cms.datamodel.CommercialInfo;
import com.tgs.services.cms.datamodel.CommercialRuleCriteria;
import com.tgs.services.cms.datamodel.HotelCommercialInfo;
import com.tgs.services.cms.datamodel.commission.CommissionRule;
import com.tgs.services.cms.helper.CommercialType;
import com.tgs.services.cms.helper.CommercialUtils;
import com.tgs.services.cms.helper.CommissionHelper;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.HotelBasicRuleField;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelCommissionEngine {

	static Map<String, ? extends IRuleField> resolverMap = EnumUtils.getEnumMap(HotelBasicRuleField.class);

	public void processCommission(HotelInfo hotelInfo, User user, HotelSearchQuery searchQuery) {
		try {
			HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromHotelInfo(hotelInfo)
					.generateFactFromSearchQuery(searchQuery).generateFactFromUserId(user.getUserId());
			hotelFact.setHotelType(BaseHotelUtils.getTripType(hotelInfo));

			String commissionPlanKey = getCommissionPlanKey(hotelFact);
			CommissionRule commissionRule =
					CommissionHelper.getCommissionRule(hotelFact, user, commissionPlanKey, resolverMap);

			if (Objects.nonNull(commissionRule)) {
				HotelCommercialInfo commercialInfo =
						HotelCommercialInfo.builder().commissionId(Long.valueOf(commissionRule.getId())).build();
				CommercialRuleCriteria ruleCriteria = commissionRule.getCommercialCriteria();
				if (CollectionUtils.isNotEmpty(ruleCriteria.getComponents())) {
					List<CommercialComponent> components = ruleCriteria.getComponents();
					for (Iterator<Option> optionItr = hotelInfo.getOptions().iterator(); optionItr.hasNext();) {
						Option option = optionItr.next();
						// hotelFact.generateFactFromOption(option);
						if (!isCommissionAlreadyAppliedOnOption(option)) {
							setBaseFare(option);
							Boolean isCommissionApplied = applyCommissionOnOption(hotelInfo, option, hotelFact, user,
									components, commercialInfo);
							if (BooleanUtils.isFalse(isCommissionApplied)) {
								optionItr.remove();
							}
							BaseHotelUtils.updateTotalFareComponents(option);
						}
					}

				}
			}
		} catch (Exception e) {
			log.error("Commission not applied on hotel id {} for search query {}", hotelInfo.getId(),
					GsonUtils.getGson().toJson(searchQuery), e);
			throw new CustomGeneralException(SystemError.COMMISSION_FAILED);
		}
	}

	private Boolean applyCommissionOnOption(HotelInfo hotelInfo, Option option, HotelBasicFact hotelFact, User user,
			List<CommercialComponent> components, HotelCommercialInfo commercialInfo) {
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				if (MapUtils.isNotEmpty(priceInfo.getFareComponents())) {
					Boolean isCommissionApplied =
							applyCommission(hotelInfo, option, priceInfo, hotelFact, user, components, commercialInfo);
					if (BooleanUtils.isFalse(isCommissionApplied)) {
						return false;
					}
				}
			}
		}
		return null;
	}

	/*
	 * It returns false if the commission can't be applied on the entire option else returns null.
	 */
	public Boolean applyCommission(HotelInfo hotelInfo, Option option, PriceInfo priceInfo, HotelBasicFact hotelFact,
			User user, List<CommercialComponent> components, HotelCommercialInfo commercialInfo) {

		for (CommercialComponent commercialComponent : components) {
			CommercialType type = CommercialType.valueOf(commercialComponent.getType().getName());
			Double amount = type.getHotelCommission(commercialComponent.getExpression(), priceInfo);
			type.updateHotelTargetFareComponent(amount, hotelInfo, option, null,
					commercialComponent.getThresholdAmount(), user, commercialInfo, priceInfo);
			if (BooleanUtils.isFalse(commercialInfo.getIsOptionRequired())) {
				return false;
			}
		}
		updateMiscInfo(priceInfo, commercialInfo);
		return null;
	}

	private static void setBaseFare(Option option) {
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				priceInfo.getMiscInfo().setSupplierPrice(priceInfo.getFareComponents().get(HotelFareComponent.BF));
			}
		}
	}

	public static void updateMiscInfo(PriceInfo priceInfo, CommercialInfo cInfo) {
		if (BooleanUtils.isTrue(cInfo.getIsUpdated())) {
			priceInfo.getMiscInfo().setCommericialRuleId(Long.valueOf(cInfo.getCommissionId()));
		}
	}

	public static String getCommissionPlanKey(HotelBasicFact fact) {
		String commissionPlanKey = null;
		if (fact.getHotelType() != null) {
			AirType airType = fact.getHotelType();
			if (airType.equals(AirType.DOMESTIC)) {
				commissionPlanKey = OrderType.HOTEL.name() + "_" + AirType.DOMESTIC.getCode();
			} else if (airType.equals(AirType.INTERNATIONAL)) {
				commissionPlanKey = OrderType.HOTEL.name() + "_" + AirType.INTERNATIONAL.getCode();
			}
		} else {
			commissionPlanKey = OrderType.HOTEL.name() + "_" + AirType.ALL.getCode();
		}
		return commissionPlanKey;
	}

	private boolean isCommissionAlreadyAppliedOnOption(Option option) {
		if (!ObjectUtils.isEmpty(option.getMiscInfo())
				&& BooleanUtils.isTrue(option.getMiscInfo().getIsCommissionApplied())) {
			return true;
		}
		return false;
	}

	public boolean isCommissionApplicableOnHotel(User user, HotelSearchQuery searchQuery) {
		String commissionPlanKey = OrderType.HOTEL.name() + "_" + BaseHotelUtils.getTripType(searchQuery).getCode();
		boolean isApplicable = false;
		if (CommercialUtils.isUserCommissionApplicable(user) && UserUtils.getParentUserConf(user).getCommPlanMap()
				.getOrDefault(commissionPlanKey, UserUtils.getParentUserConf(user).getCommPlanMap()
						.get(OrderType.HOTEL.name() + "_" + AirType.ALL.getCode())) != null) {
			isApplicable = true;
		}
		return isApplicable;
	}
}
