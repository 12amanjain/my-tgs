package com.tgs.services.cms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.cms.datamodel.creditcard.CreditCardAdditionalInfo;

public class CreditCardAdditionalInfoType extends CustomUserType {
	@Override
	public Class returnedClass() {
		return CreditCardAdditionalInfo.class;
	}
}
