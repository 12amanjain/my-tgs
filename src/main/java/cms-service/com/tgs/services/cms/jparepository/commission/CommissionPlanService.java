package com.tgs.services.cms.jparepository.commission;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.filters.CommissionPlanFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.cms.datamodel.commission.CommissionFilter;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.dbmodel.DbCommissionPlan;
import com.tgs.services.cms.helper.CommissionHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class CommissionPlanService extends SearchService<DbCommissionPlan> {

	@Autowired
	CommissionPlanRepository planRepository;

	@Autowired
	CommissionHelper commissionHelper;
	
	public List<DbCommissionPlan> findAll(CommissionFilter filter) {
		Specification<DbCommissionPlan> specfication = new Specification<DbCommissionPlan>() {

			@Override
			public Predicate toPredicate(Root<DbCommissionPlan> root, CriteriaQuery<?> query,
					CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter.getRuleIds() != null) {
					Expression<Integer> exp = root.get("id");
					predicates.add(exp.in(filter.getRuleIds()));
				}
			
				if (filter.getEnabled() != null) {
					predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
				}
				
				if(filter.getProduct() != null) {
					predicates.add(criteriaBuilder.equal(root.get("product"), Product.valueOf(filter.getProduct()).getCode()));
				}

				if (!TgsCollectionUtils.isEmptyStringCollection(filter.getCodes())) {
					predicates.add(root.get("code").in(filter.getCodes()));
				}

				SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		}

		return planRepository.findAll(specfication, sort);
	}

	public DbCommissionPlan save(DbCommissionPlan commissionPlan) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			commissionPlan.setProcessedOn(LocalDateTime.now());
			commissionPlan = planRepository.save(commissionPlan);
		}
		commissionHelper.createUpdatePlanInCache(commissionPlan);
		return commissionPlan;
	}

	public DbCommissionPlan findById(Long id) {
		return planRepository.findOne(id);
	}

	public List<DbCommissionPlan> findAll(List<Integer> ids) {
		Specification<DbCommissionPlan> specification = new Specification<DbCommissionPlan>() {
			@Override
			public Predicate toPredicate(Root<DbCommissionPlan> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				Expression<String> exp = root.get("id");
				predicates.add(exp.in(ids));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		return planRepository.findAll(specification);
	}

	public DbCommissionPlan findByIdOrDescription(Long id, String description) {
		return planRepository.findOne(id);
	}

	public DbCommissionPlan findByIdOrName(Long id, String name) {
		return planRepository.findByIdOrName(id, name);
	}

	public List<CommissionPlan> search(CommissionPlanFilter filter) {
		return BaseModel.toDomainList(super.search(filter, planRepository));
	}

	@Override
	public List<DbCommissionPlan> search(QueryFilter filter) {
		return super.search(filter, planRepository);
	}
}