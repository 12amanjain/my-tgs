package com.tgs.services.cms.jparepository.commission;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.cms.dbmodel.DbCommissionRule;

@Repository
public interface CommissionRuleRepository
		extends JpaRepository<DbCommissionRule, Long>, JpaSpecificationExecutor<DbCommissionRule> {

	public List<DbCommissionRule> findByEnabledOrderByProcessedOnDesc(Boolean enabled);

	public DbCommissionRule findByIdAndEnabled(Long id, Boolean enabled);
}