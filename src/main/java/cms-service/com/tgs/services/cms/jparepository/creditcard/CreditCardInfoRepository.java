package com.tgs.services.cms.jparepository.creditcard;


import com.tgs.services.cms.dbmodel.DbCreditCardInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface CreditCardInfoRepository extends JpaRepository<DbCreditCardInfo, Long>, JpaSpecificationExecutor<DbCreditCardInfo> {
}