package com.tgs.services.cms.jparepository.creditcard;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.dbmodel.DbCreditCardInfo;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum CreditCardSearchPredicate {

	CREATED_ON {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	ENABLED {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getEnabled()))
				predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
		}
	},
	SUPPLIERID {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getSupplierId()))
				predicates.add(criteriaBuilder.equal(root.get("supplierId"), filter.getSupplierId()));
		}
	},
	SUBPRODUCT {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getSubProduct()))
				predicates.add(criteriaBuilder.equal(root.get("subProduct"), filter.getSubProduct()));
		}
	},
	CARDTYPE {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getCardType()))
				predicates.add(criteriaBuilder.equal(root.get("cardType"), filter.getCardType()));
		}
	},
	BANKNAME {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotEmpty(filter.getBankName()))
				predicates.add(criteriaBuilder.equal(root.get("bankName"), filter.getBankName()));
		}
	},
	ACCOUNTCODE {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getAccountCode()))
				predicates.add(criteriaBuilder.equal(root.get("accountCode"), filter.getAccountCode()));
		}
	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (filter.isDeleted())
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.isDeleted()));
		}
	},
	PRIORITY {
		@Override
		public void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				CreditCardFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getPriority()))
				predicates.add(criteriaBuilder.equal(root.get("priority"), filter.getPriority()));
		}
	};

	public abstract void addPredicate(Root<DbCreditCardInfo> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, CreditCardFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnCreditCardFilter(Root<DbCreditCardInfo> root,
			CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, CreditCardFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (CreditCardSearchPredicate ruleSearchPredicate : CreditCardSearchPredicate.values()) {
			ruleSearchPredicate.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
