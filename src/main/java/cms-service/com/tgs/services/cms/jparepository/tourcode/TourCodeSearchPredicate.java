package com.tgs.services.cms.jparepository.tourcode;

import com.tgs.services.cms.datamodel.tourcode.TourCodeFilter;
import com.tgs.services.cms.dbmodel.DbTourCode;
import com.tgs.utils.springframework.data.SpringDataUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public enum TourCodeSearchPredicate {

    CREATED_ON {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
        }
    }, ENABLED {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (Objects.nonNull(filter.getEnabled()))
                predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
        }
    }, SUPPLIERID {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (StringUtils.isNotEmpty(filter.getSupplierId()))
                predicates.add(criteriaBuilder.equal(root.get("supplierId"), filter.getSupplierId()));
        }
    }, SOURCEID {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (StringUtils.isNotEmpty(filter.getSourceId()))
                predicates.add(criteriaBuilder.equal(root.get("sourceId"), filter.getSourceId()));
        }
    }, AIRLINE {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (StringUtils.isNotEmpty(filter.getAirline()))
                predicates.add(criteriaBuilder.equal(root.get("airline"), filter.getAirline()));
        }
    }, TOURCODE {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (StringUtils.isNotEmpty(filter.getTourCode()))
                predicates.add(criteriaBuilder.equal(root.get("tourCode"), filter.getTourCode()));
        }
    }, PRIORITY {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (Objects.nonNull(filter.getPriority()))
                predicates.add(criteriaBuilder.equal(root.get("priority"), filter.getPriority()));
        }
    },
    ISDELETED {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (filter.getDeleted() != null) {
                predicates.add(criteriaBuilder.equal(root.get("isDeleted"), BooleanUtils.isTrue(filter.getDeleted())));
            }
        }
    },
    ID {
        @Override
        public void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                 TourCodeFilter filter, List<Predicate> predicates) {
            if (filter.getId() != null)
                predicates.add(criteriaBuilder.equal(root.get("id"), filter.getId()));

        }
    };

    public abstract void addPredicate(Root<DbTourCode> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
                                      TourCodeFilter filter, List<Predicate> predicates);

    public static List<Predicate> getPredicateListBasedOnTourCodeFilter(Root<DbTourCode> root, CriteriaQuery<?> query,
                                                                        CriteriaBuilder criteriaBuilder, TourCodeFilter filter) {
        List<Predicate> predicates = new ArrayList<>();
        for (TourCodeSearchPredicate ruleSearchPredicate : TourCodeSearchPredicate.values()) {
            ruleSearchPredicate.addPredicate(root, query, criteriaBuilder, filter, predicates);
        }
        return predicates;
    }
}
