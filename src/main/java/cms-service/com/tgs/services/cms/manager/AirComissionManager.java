package com.tgs.services.cms.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.cms.restmodel.commission.IataCommissionUploadRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.cms.dbmodel.DBAirCommissionRule;
import com.tgs.services.cms.jparepository.commission.air.AirCommissionService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirComissionManager {

	@Autowired
	AirCommissionService airCommissionService;

	public BulkUploadResponse uploadIataCommission(IataCommissionUploadRequest uploadRequest) throws Exception {
		if (uploadRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(uploadRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable saveIataCommissionTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (AirCommissionRule updateQuery : uploadRequest.getUploadQuery()) {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(updateQuery.getRowId());
					try {
						saveIataCommission(updateQuery);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error(
								"Unable to update iata commission with priority {}, airline {}, commissionCriteria {} ",
								updateQuery.getPriority(), updateQuery.getAirline(),
								updateQuery.getCommissionCriteria(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, "airCommissionHelper");
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting iata commission for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(saveIataCommissionTask, uploadRequest.getUploadType(), jobId);
	}

	private void saveIataCommission(AirCommissionRule commissionRule) throws Exception {
		DBAirCommissionRule dbAirCommissionRule = new DBAirCommissionRule().from(commissionRule);
		airCommissionService.save(dbAirCommissionRule);
	}

}
