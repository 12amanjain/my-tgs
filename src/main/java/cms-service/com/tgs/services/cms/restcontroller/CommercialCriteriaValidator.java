package com.tgs.services.cms.restcontroller;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cms.datamodel.CommercialComponent;
import com.tgs.services.cms.datamodel.CommercialRuleCriteria;

@Service
public class CommercialCriteriaValidator {

	public void validateCommercialCriteria(Errors errors, String fieldName, CommercialRuleCriteria commercialCriteria) {
		if (commercialCriteria == null) {
			rejectValue(errors, fieldName, SystemError.NULL_COMMERCIAL_CRITERIA);
		}
		if (CollectionUtils.isEmpty(commercialCriteria.getComponents())) {
			rejectValue(errors, fieldName + ".components", SystemError.EMPTY_COMMERCIAL_COMPONENT);
		} else {
			for (int i = 0; i < commercialCriteria.getComponents().size(); i++) {
				CommercialComponent commercialComponent = commercialCriteria.getComponents().get(i);
				String exp = commercialComponent.getExpression();
				if (commercialComponent.getType() == null && StringUtils.isEmpty(exp)) {
					commercialCriteria.getComponents().remove(i--);
					continue;
				}
				if (commercialComponent.getType() == null) {
					rejectValue(errors, fieldName + ".components", SystemError.INVALID_COMMERCIAL_COMPONENT_TYPE);
				}
				if (!BaseUtils.validateExpression(exp)) {
					rejectValue(errors, fieldName + ".components", SystemError.INVALID_COMPONENT_EXPRESSION, exp);
				}
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
