package com.tgs.services.cms.restcontroller;

import java.util.Arrays;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.RestExcludeStrategy;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.cms.datamodel.commission.CommissionFilter;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.dbmodel.DbCommissionRule;
import com.tgs.services.cms.jparepository.commission.CommissionPlanService;
import com.tgs.services.cms.restmodel.commission.plan.CommissionPlanResponse;
import com.tgs.services.cms.restmodel.commission.rule.CommissionPlanRules;
import com.tgs.services.cms.restmodel.commission.rule.CommissionRuleRequest;
import com.tgs.services.cms.restmodel.commission.rule.CommissionRuleResponse;
import com.tgs.services.cms.servicehandler.CommissionHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/cms/v1/commission")
@CustomRequestProcessor(areaRole = AreaRole.CMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.SUPERVISOR, UserRole.CALLCENTER, UserRole.SALES},
		emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
public class CommissionController {

	@Autowired
	CommissionHandler commissionHandler;

	@Autowired
	CommissionRuleValidator ruleValidator;

	@Autowired
	CommissionPlanService planService;

	@Autowired
	private AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ruleValidator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected CommissionRuleResponse addOsrUpdateCommissionRule(HttpServletRequest request,
			HttpServletResponse response, @RequestBody @Valid CommissionRuleRequest ruleRequest) throws Exception {
		commissionHandler.initData(ruleRequest, new CommissionRuleResponse());
		return commissionHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected CommissionRuleResponse getCommissionRules(HttpServletRequest request, HttpServletResponse response,
			@RequestBody CommissionFilter filter) throws Exception {
		return commissionHandler.getCommissionRules(filter);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteCommissionRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return commissionHandler.deleteCommissionRule(id);
	}

	@RequestMapping(value = "/plan/save", method = RequestMethod.POST)
	protected CommissionPlanRules crudCommissionPlan(HttpServletRequest request, HttpServletResponse response,
			@RequestBody CommissionPlanRules planRules) throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(Arrays.asList(new RestExcludeStrategy()));
		return commissionHandler.crudCommissionPlanRule(planRules);
	}

	@RequestMapping(value = "/plan/list", method = RequestMethod.POST)
	protected CommissionPlanRules listCommissionPlan(HttpServletRequest request, HttpServletResponse response,
			@RequestBody CommissionPlanRules planRules) throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(Arrays.asList(new RestExcludeStrategy()));
		return commissionHandler.getCommissionPlanInfoById(planRules);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateRuleStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return commissionHandler.updateCommissionRuleStatus(id, status);
	}

	@RequestMapping(value = "/plan", method = RequestMethod.POST)
	protected CommissionPlanResponse listCommissionPlan(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CommissionPlan planRequest) throws Exception {
		CommissionPlanResponse planResponse = new CommissionPlanResponse();
		planResponse.setCommissionPlans(commissionHandler.getCommissionPlans(planRequest));
		return planResponse;
	}

	@RequestMapping(value = "/commissionrules-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbCommissionRule.class, ""));
		return auditResponse;
	}
}
