package com.tgs.services.cms.restcontroller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.cms.datamodel.commission.CommissionRule;
import com.tgs.services.cms.restmodel.commission.rule.CommissionPlanRules;
import com.tgs.services.cms.restmodel.commission.rule.CommissionRuleRequest;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

@Service
public class CommissionRuleValidator implements Validator {

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	FlightBasicRuleCriteriaValidator flightBasicRuleCriteriaValidator;

	@Autowired
	CommercialCriteriaValidator commercialCriteriaValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof CommissionPlanRules) {
			CommissionPlanRules commissionPlan = (CommissionPlanRules) target;
			if (StringUtils.isBlank(commissionPlan.getPlanName())) {
				rejectValue(errors, "planName", SystemError.EMPTYPLANNAME);
			}
		}

		else if (target instanceof CommissionRuleRequest) {
			CommissionRule rule = ((CommissionRuleRequest) target).getCommissionRule();
			if (rule == null) {
				rejectValue(errors, "commissionRule", SystemError.NULL_RULE);
			} else {
				if (rule.getProduct() == null) {
					rejectValue(errors, "commissionRule.product", SystemError.INVALID_PRODUCT);
				}

				if (StringUtils.isBlank(rule.getDescription())) {
					rejectValue(errors, "commissionRule.description", SystemError.BLANK_DESCRIPTION);
				}

				if (Product.AIR.equals(rule.getProduct())) {
					String code = rule.getCode();

					if (StringUtils.isEmpty(code)) {
						FlightBasicRuleCriteria inclusionCriteria = (FlightBasicRuleCriteria) rule
								.getInclusionCriteria(),
								exclusionCriteria = (FlightBasicRuleCriteria) rule.getExclusionCriteria();

						if (TgsCollectionUtils.isEmptyStringCollection(inclusionCriteria.getAirlineList())
								&& TgsCollectionUtils.isEmptyStringCollection(exclusionCriteria.getAirlineList())) {
							rejectValue(errors, "commissionRule.inclusionCriteria.airlineList",
									SystemError.EMPTY_FBRC_AIRLINE);
						}
					} else if (fmsCommunicator.getAirlineInfo(code) == null) {
						rejectValue(errors, "commissionRule.code", SystemError.INVALID_CODE);
					}

					flightBasicRuleCriteriaValidator.validateCriteria(errors, "commissionRule.inclusionCriteria",
							rule.getInclusionCriteria());
					flightBasicRuleCriteriaValidator.validateCriteria(errors, "commissionRule.exclusionCriteria",
							rule.getExclusionCriteria());
				}

				commercialCriteriaValidator.validateCommercialCriteria(errors, "commissionRule.commercialCriteria",
						rule.getCommercialCriteria());
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
