package com.tgs.services.cms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.cms.restmodel.creditcard.CreditCardRequest;
import com.tgs.services.cms.restmodel.creditcard.CreditCardResponse;
import com.tgs.services.cms.servicehandler.CreditCardInfoHandler;
import com.tgs.services.cms.validator.CreditCardValidator;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/cms/v1/creditcard")
@CustomRequestProcessor(areaRole = AreaRole.CMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.SUPERVISOR, UserRole.CALLCENTER, UserRole.SALES},
		emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
public class CreditCardController {


	@Autowired
	CreditCardValidator ruleValidator;

	@Autowired
	CreditCardInfoHandler cardInfoHandler;

	@Autowired
	private AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ruleValidator);
	}


	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected CreditCardResponse addorUpdateCreditCard(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CreditCardRequest creditCardRequest) throws Exception {
		cardInfoHandler.initData(creditCardRequest, new CreditCardResponse());
		return cardInfoHandler.getResponse();
	}

	@RequestMapping(value = "/save/list", method = RequestMethod.POST)
	protected CreditCardResponse addorUpdateCreditCards(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid List<CreditCardInfo> creditCardRequests) throws Exception {
		CreditCardResponse creditCardResponse = new CreditCardResponse();
		for (CreditCardInfo creditCardInfo : creditCardRequests) {
			CreditCardRequest creditCardRequest = new CreditCardRequest();
			creditCardRequest.setCardInfo(creditCardInfo);
			creditCardResponse.getCardInfoList()
					.addAll(addorUpdateCreditCard(request, response, creditCardRequest).getCardInfoList());
		}
		return creditCardResponse;
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected CreditCardResponse listCreditCard(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid CreditCardFilter cardFilter) throws Exception {
		return cardInfoHandler.getCreditCardList(cardFilter);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteCreditCard(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return cardInfoHandler.deleteCreditCard(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateCreditCardStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return cardInfoHandler.updateCreditCardStatus(id, status);
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(
				auditHandler.fetchAudits(auditsRequestData, com.tgs.services.cms.dbmodel.DbCreditCardInfo.class, ""));
		return auditResponse;
	}
}
