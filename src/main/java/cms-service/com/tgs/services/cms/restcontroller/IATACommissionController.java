package com.tgs.services.cms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionFilter;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;
import com.tgs.services.cms.manager.AirComissionManager;
import com.tgs.services.cms.restmodel.commission.IataCommissionUploadRequest;
import com.tgs.services.cms.restmodel.commission.air.AirCommissionResponse;
import com.tgs.services.cms.servicehandler.AirCommissionHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/cms/v1/aircommission")
@CustomRequestProcessor(areaRole = AreaRole.CMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.SUPERVISOR, UserRole.CALLCENTER, UserRole.SALES},
		emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
public class IATACommissionController {

	@Autowired
	AirCommissionHandler airCommissionHandler;

	@Autowired
	AirComissionManager airCommissionManager;

	@Autowired
	private AuditsHandler auditHandler;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected AirCommissionResponse addorUpdateIATACommission(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirCommissionRule airCommissionRequest) throws Exception {
		airCommissionHandler.initData(airCommissionRequest, new AirCommissionResponse());
		return airCommissionHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected AirCommissionResponse listIATACommission(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirCommissionFilter airCommissionFilter) throws Exception {
		return airCommissionHandler.getAirCommissionList(airCommissionFilter);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteAirCommissionRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return airCommissionHandler.deleteAirCommissionRule(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateAirCommissionRuleStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return airCommissionHandler.updateAirCommissionRuleStatus(id, status);
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData,
				com.tgs.services.cms.dbmodel.DBAirCommissionRule.class, ""));
		return auditResponse;
	}

	@RequestMapping(value = "/upload-commission", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadIataCommission(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody IataCommissionUploadRequest updateRequest)
			throws Exception {
		return airCommissionManager.uploadIataCommission(updateRequest);
	}


}
