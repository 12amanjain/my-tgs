package com.tgs.services.cms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.cms.dbmodel.DbTourCode;
import com.tgs.services.cms.restmodel.tourcode.TourCodeListRequest;
import com.tgs.services.cms.restmodel.tourcode.TourCodeRequest;
import com.tgs.services.cms.restmodel.tourcode.TourCodeResponse;
import com.tgs.services.cms.servicehandler.TourCodeHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/cms/v1/tourcode")
@CustomRequestProcessor(areaRole = AreaRole.CMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.SUPERVISOR, UserRole.CALLCENTER, UserRole.SALES},
		emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
public class TourCodeController {

	@Autowired
	TourCodeHandler tourCodeHandler;

	@Autowired
	TourCodeValidator ruleValidator;

	@Autowired
	private AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ruleValidator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected TourCodeResponse addOrUpdateTourCode(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid TourCodeRequest tourCodeRequest) throws Exception {
		tourCodeHandler.initData(tourCodeRequest, new TourCodeResponse());
		return tourCodeHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected TourCodeResponse listTourCode(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid TourCodeListRequest tourCodeRequest) throws Exception {
		return tourCodeHandler.getTourCodeList(tourCodeRequest);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteTourCode(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return tourCodeHandler.deleteTourCode(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateTourCodeStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return tourCodeHandler.updateTourCodeStatus(id, status);
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchPlanAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbTourCode.class, ""));
		return auditResponse;
	}
}
