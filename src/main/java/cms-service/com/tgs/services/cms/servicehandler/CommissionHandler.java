package com.tgs.services.cms.servicehandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.cms.datamodel.commission.CommissionFilter;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.datamodel.commission.CommissionRule;
import com.tgs.services.cms.dbmodel.CommissionPlanMapper;
import com.tgs.services.cms.dbmodel.DbCommissionPlan;
import com.tgs.services.cms.dbmodel.DbCommissionRule;
import com.tgs.services.cms.jparepository.commission.CommissionPlanMapperService;
import com.tgs.services.cms.jparepository.commission.CommissionPlanService;
import com.tgs.services.cms.jparepository.commission.CommissionRuleService;
import com.tgs.services.cms.restmodel.commission.rule.CommissionPlanRules;
import com.tgs.services.cms.restmodel.commission.rule.CommissionRuleRequest;
import com.tgs.services.cms.restmodel.commission.rule.CommissionRuleResponse;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommissionHandler extends ServiceHandler<CommissionRuleRequest, CommissionRuleResponse> {

	@Autowired
	CommissionRuleService commissionRuleService;

	@Autowired
	CommissionPlanService planService;

	@Autowired
	CommissionPlanMapperService mapperService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		// save or update commission rule
		DbCommissionRule dbCommissionRule = null;
		if (Objects.nonNull(request.getCommissionRule()) && Objects.nonNull(request.getCommissionRule().getId())) {
			dbCommissionRule = commissionRuleService.findById(request.getCommissionRule().getId());
			request.getCommissionRule().setCreatedOn(dbCommissionRule.getCreatedOn());
		}

		try {
			if (dbCommissionRule != null) {
				CommissionRule commissionRule = dbCommissionRule.toDomain();
				patchUpdate(commissionRule, request.getCommissionRule());
			}
			request.getCommissionRule().cleanData();
			dbCommissionRule = new DbCommissionRule().from(request.getCommissionRule());
			dbCommissionRule = commissionRuleService.save(dbCommissionRule);
			request.getCommissionRule().setId((Integer) ConvertUtils.convert(dbCommissionRule.getId(), Integer.class));
			syncService.sync("cms", request);
		} catch (CustomGeneralException e) {
			log.error("Unable to save comission {} for request", e, request);
			throw e;
		} catch (Exception e) {
			log.error("Unable to save comission ", e);
			throw new Exception("Commission Save Or Update Failed ", e);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.getCommissionRule().add(request.getCommissionRule());
	}

	/**
	 * ComissionRuleListResponse - Will list based on Plan Id,Plan Description, Plan mapped commission id
	 */
	public CommissionPlanRules crudCommissionPlanRule(CommissionPlanRules planRule) {
		if (planRule.getId() != null) {
			DbCommissionPlan plan = planService.findById(planRule.getId());
			if (plan != null) {
				// Updating Plan fields
				plan.setName(planRule.getPlanName());
				plan.setDescription(planRule.getDescription());
				plan.setEnabled(BooleanUtils.isTrue(planRule.getEnabled()));
				planService.save(plan);
				mapperService.deleteByCommPlanId(planRule.getId().intValue());
			}
		} else {
			createCommissionPlan(planRule);
		}

		List<CommissionPlanMapper> commissionPlanMappers = new ArrayList<>();
		planRule.getIncludedRules().forEach(rl -> {
			commissionPlanMappers.add(CommissionPlanMapper.builder().commPlanId(planRule.getId().intValue())
					.commissionRuleId(rl.getId()).build());
		});
		mapperService.save(commissionPlanMappers, planService.findById(planRule.getId()).toDomain());

		return planRule;

	}

	public CommissionPlanRules getCommissionPlanInfoById(CommissionPlanRules cPlanRules) {

		CommissionPlanRules planRule = new CommissionPlanRules();
		Map<Long, DbCommissionRule> currentRules = null;
		try {
			if (cPlanRules.getId() != null) {
				DbCommissionPlan plan = planService.findById(cPlanRules.getId());
				if (plan == null) {
					throw new CustomGeneralException(SystemError.INVALID_ID);
				}
				planRule.setId(plan.getId());
				planRule.setPlanName(plan.getName());
				planRule.setEnabled(plan.isEnabled());

				List<CommissionPlanMapper> planRules = mapperService.findByCommPlanID(cPlanRules.getId().intValue());

				List<Integer> ruleList = null;
				if (CollectionUtils.isNotEmpty(planRules)) {
					ruleList = planRules.stream().map(rule -> rule.getCommissionRuleId()).collect(Collectors.toList());
					List<DbCommissionRule> existingRules = commissionRuleService
							.findAll(CommissionFilter.builder().deleted(false).ruleIds(ruleList).build());
					if (CollectionUtils.isNotEmpty(existingRules)) {
						currentRules = existingRules.stream()
								.collect(Collectors.toMap(DbCommissionRule::getId, Function.identity()));
						planRule.getIncludedRules().addAll(existingRules.get(0).toDomainList(existingRules));
					}
				}
			}
		} finally {
			List<DbCommissionRule> cRules = commissionRuleService.findAll(CommissionFilter.builder()
					.product(cPlanRules.getProduct() != null ? cPlanRules.getProduct().name() : null).deleted(false)
					.build());

			Map<Long, DbCommissionRule> allRules =
					cRules.stream().collect(Collectors.toMap(DbCommissionRule::getId, Function.identity()));
			Map<Long, DbCommissionRule> copyallRules = new HashMap<Long, DbCommissionRule>(allRules);

			for (Long key : copyallRules.keySet()) {
				if (currentRules == null || currentRules.get(key) == null) {
					planRule.getExcludedRules().add(allRules.get(key).toDomain());
				}
			}
		}

		return planRule;

	}

	public List<Integer> getCommissionRuleId(List<CommissionPlanMapper> planRules) {
		List<Integer> commId = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(planRules)) {
			planRules.forEach(plan -> {
				commId.add(plan.getCommissionRuleId());
			});
		}
		return commId;
	}

	public CommissionPlanRules createCommissionPlan(CommissionPlanRules planRule) {
		if (planRule.getId() == null) {
			if (planService.findByIdOrName(null, planRule.getPlanName()) != null) {
				throw new CustomGeneralException(SystemError.DUPLICATE_PLAN_NAME);
			}

			if (StringUtils.isBlank(planRule.getPlanName())) {
				throw new CustomGeneralException(SystemError.EMPTYPLANNAME);
			}

			if (planRule.getProduct() == null) {
				throw new CustomGeneralException(SystemError.INVALID_PRODUCT);
			}

			DbCommissionPlan plan = DbCommissionPlan.builder().name(planRule.getPlanName())
					.product(planRule.getProduct().getCode()).enabled(BooleanUtils.isTrue(planRule.getEnabled()))
					.description(planRule.getDescription()).build();
			plan = planService.save(plan);
			planRule.setId(plan.getId());
		}
		return planRule;
	}

	public CommissionRuleResponse getCommissionRules(CommissionFilter filter) {
		CommissionRuleResponse commissionRuleResponse = new CommissionRuleResponse();
		commissionRuleService.findAll(filter).forEach(commissionRule -> {
			commissionRuleResponse.getCommissionRule().add(commissionRule.toDomain());
		});
		return commissionRuleResponse;
	}

	public BaseResponse deleteCommissionRule(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbCommissionRule commissionRule = commissionRuleService.findById(id);
		if (Objects.nonNull(commissionRule)) {
			commissionRule.setDeleted(true);
			commissionRule.setEnabled(false);
			commissionRuleService.save(commissionRule);
			syncService.sync("cms", null, "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	@Override
	public void process() throws Exception {
		// TODO Auto-generated method stub

	}

	public BaseResponse updateCommissionRuleStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbCommissionRule commissionRule = commissionRuleService.findById(id);
		if (Objects.nonNull(commissionRule)) {
			commissionRule.setEnabled(status);
			commissionRuleService.save(commissionRule);
			syncService.sync("cms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public void patchUpdate(CommissionRule oldCommissionRule, CommissionRule newCommissionRule) {
		if (oldCommissionRule == null) {
			return;
		}
		if (oldCommissionRule.getProduct().equals(Product.AIR)) {
			newCommissionRule.setInclusionCriteria(
					new GsonMapper<>((FlightBasicRuleCriteria) newCommissionRule.getInclusionCriteria(),
							(FlightBasicRuleCriteria) oldCommissionRule.getInclusionCriteria(),
							FlightBasicRuleCriteria.class).convert());
			newCommissionRule.setExclusionCriteria(
					new GsonMapper<>((FlightBasicRuleCriteria) newCommissionRule.getExclusionCriteria(),
							(FlightBasicRuleCriteria) oldCommissionRule.getExclusionCriteria(),
							FlightBasicRuleCriteria.class).convert());
		}
		newCommissionRule.cleanData();
	}

	public List<CommissionPlan> getCommissionPlans(CommissionPlan planRequest) {
		List<CommissionPlan> planList = new ArrayList<>();
		if (planRequest.getRuleId() != null) {
			List<CommissionPlanMapper> plans = mapperService.findByCommissionRuleId(planRequest.getRuleId());
			if (CollectionUtils.isNotEmpty(plans)) {
				plans.forEach(plan -> {
					planList.add(planService.findById(Long.valueOf(plan.getCommPlanId())).toDomain());
				});
			}
		} else {
			planService.findAll(CommissionFilter.builder()
					.product(planRequest.getProduct() != null ? planRequest.getProduct().getName() : null).build())
					.forEach(plan -> {
						planList.add(plan.toDomain());
					});
		}

		return planList;
	}

}
