package com.tgs.services.cms.validator;

import com.tgs.filters.GstInfoFilter;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.cms.restmodel.creditcard.CreditCardRequest;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Service
public class CreditCardValidator implements Validator {

	@Autowired
	GeneralServiceCommunicator generalCommunicator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof CreditCardRequest) {
			CreditCardRequest creditCardRequest = (CreditCardRequest) target;
			if (creditCardRequest.getCardInfo() == null) {
				rejectValue(errors, "cardInfo", SystemError.NULL_RULE);
			}
			CreditCardInfo cardInfo = creditCardRequest.getCardInfo();

			if (cardInfo.getProduct() == null) {
				rejectValue(errors, "cardInfo.product", SystemError.INVALID_PRODUCT);
			}
			if (StringUtils.isBlank(cardInfo.getCardNumber())) {
				rejectValue(errors, "cardInfo.cardNumber", SystemError.INVALID_CREDIT_CARD_NUMBER);
			}
			if (StringUtils.isBlank(cardInfo.getSupplierId())) {
				rejectValue(errors, "cardInfo.supplierId", SystemError.INVALID_CREDIT_CARD_SUPPLIER);
			}
			if (StringUtils.isBlank(cardInfo.getExpiry())) {
				rejectValue(errors, "cardInfo.expiry", SystemError.INVALID_CREDIT_CARD_EXPIRY);
			}
			if (cardInfo.getCardType() == null) {
				rejectValue(errors, "cardInfo.cardType", SystemError.INVALID_CREDIT_CARD_TYPE);
			}
			if (cardInfo.getAdditionalInfo() != null) {
				if (CollectionUtils.isNotEmpty(cardInfo.getAdditionalInfo().getBillingEntityIds())) {
					GstInfoFilter filter =
							GstInfoFilter.builder().idIn(cardInfo.getAdditionalInfo().getBillingEntityIds())
									.userIdIn(Arrays.asList(cardInfo.getUserId())).build();
					List<GstInfo> billingEntities = generalCommunicator.searchBillingEntity(filter);
					if (CollectionUtils.isEmpty(billingEntities)
							|| cardInfo.getAdditionalInfo().getBillingEntityIds().size() != billingEntities.size()) {
						rejectValue(errors, "cardInfo.additionalInfo.billingEntityIds",
								SystemError.INVALID_CREDIT_CARD_BILLINGENTITY);
					}
				}
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

}
