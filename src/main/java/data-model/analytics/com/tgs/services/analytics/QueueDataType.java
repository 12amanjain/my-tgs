package com.tgs.services.analytics;

import lombok.Getter;

@Getter
public enum QueueDataType {

    ELASTICSEARCH,
    BQ;

}
