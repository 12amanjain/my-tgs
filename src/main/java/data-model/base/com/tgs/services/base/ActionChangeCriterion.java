package com.tgs.services.base;

import java.util.List;

import lombok.Getter;

@Getter
public class ActionChangeCriterion {

	//List of dependent fields
	private List<String> fields;
	
	private String fromAction;
	
	private String toAction;
	
}
