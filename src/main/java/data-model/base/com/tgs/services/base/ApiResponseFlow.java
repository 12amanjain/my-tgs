package com.tgs.services.base;

import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Getter
@Builder
@EqualsAndHashCode
public class ApiResponseFlow {

	private String type;
	private Long apitime;
	private Long startTime;

}
