package com.tgs.services.base;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Builder
@Getter
@Setter
public class AuditChange {

	private String fieldName;

	private Object oldValue;

	private Object newValue;
}
