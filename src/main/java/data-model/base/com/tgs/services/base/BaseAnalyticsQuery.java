package com.tgs.services.base;

import java.time.LocalDateTime;
import com.tgs.services.base.enums.UserRole;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class BaseAnalyticsQuery {

	private String ip;
	private String userid;
	private String parentuserid;
	private String username;
	private UserRole role;
	private Boolean canbeemulated;
	private String emulateuserid;
	private String emulateusername;
	private String browser;
	private String device;
	private String hostname;
	private String browserversion;
	private LocalDateTime generationtime;
	private String analyticstype;

	private Long responsetimems;
	private Long externalapiparsingtimems;
	private Long externalapitimems;
	private String deviceid;

	private String key1;
	private String key2;
	private String key3;


}
