package com.tgs.services.base;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BulkUploadQuery {

	private String rowId;
}
