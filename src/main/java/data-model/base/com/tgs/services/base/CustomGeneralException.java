package com.tgs.services.base;

import com.tgs.services.base.helper.SystemError;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomGeneralException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private SystemError error;

	public CustomGeneralException(SystemError error) {
		super(error.getMessage());
		this.error = error;
	}

	public CustomGeneralException(SystemError error, String message) {
		super(message);
		this.error = error;
	}

	public CustomGeneralException(String message) {
		super(message);
	}

}
