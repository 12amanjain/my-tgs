package com.tgs.services.base;

import com.tgs.services.base.helper.SystemError;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ErrorDetail {
	private String errCode;
	private String message;
	private String details;

	public ErrorDetail(SystemError apiError) {
		this.errCode = String.valueOf(apiError.getErrorCode());
		this.message = apiError.getMessage();
	}

	public ErrorDetail(SystemError apiError, String details) {
		this.errCode = String.valueOf(apiError.getErrorCode());
		this.message = apiError.getMessage();
		this.details = details;
	}

	public ErrorDetail(Integer errCode, String message) {
		this.errCode = errCode.toString();
		this.message = message;
	}

	public ErrorDetail(String errCode, String message) {
		this.errCode = errCode;
		this.message = message;
	}
}
