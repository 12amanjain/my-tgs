package com.tgs.services.base;

import java.util.List;

import lombok.Getter;

@Getter
public class FilterValidation {

	List<ValidateCriterion> validationCriterion;
	
}
