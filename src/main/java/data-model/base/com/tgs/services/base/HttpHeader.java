package com.tgs.services.base;

import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.datamodel.PaymentHeader;
import com.tgs.services.base.enums.ChannelType;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@Data
public class HttpHeader {
	private String ip;
	private String browserVersion;
	private String browser;
	private String client;
	private String device;
	private String jwtToken;

	private ChannelType channelType;
	private PaymentHeader payHeader;
	private String deviceid;
	private String currEnv;
	private String partnerId;
	private String apikey;
	
	@Getter(AccessLevel.NONE)
	private Boolean isSyncRequest;
	
	/**
	 * This is to track any additional values coming in API request
	 */
	private String key1;
	private String key2;
	private String key3;

	public String getIp() {
		ip = ip.contains(",") ? ip.substring(0, ip.indexOf(",")) : ip;
		return ip;
	}
	
	public boolean isPersistenceNotRequired() {
		return isSyncRequest();
	}
	
	public boolean isSyncRequest() {
		return BooleanUtils.isTrue(isSyncRequest);
	}
}
