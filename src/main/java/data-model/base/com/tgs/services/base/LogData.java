package com.tgs.services.base;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class LogData {
	private String key;
	private LocalDateTime generationTime;
	private String logData;
	private List<String> logDataList;

	/**
	 * Supplier will be Tgs in case of Rest API logs
	 */
	private String supplier;
	/**
	 * Logtype would be RestAPILogs,AirSupplierLogs etc. Intentionally we are not
	 * binding it to any enum to keep this flexible.
	 */
	private String logType;

	/**
	 * Inside logType , this is next level bifurcation to identify request. For
	 * example BFMRequest, Review request.
	 */
	private String type;
	
	private List<String> visibilityGroups;

	private long diff;

	/**
	 * ttl is defined in min , this is used to override ttl defined at the global
	 * level
	 */
	private Long ttl;
	private String loggedInUserId;
	private String userRole;
	private String userId;

	public String getLoggedInUserId() {
		return StringUtils.isBlank(loggedInUserId) ? "NA" : loggedInUserId;
	}

}
