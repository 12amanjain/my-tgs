package com.tgs.services.base;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class MaskedFieldSettings {
	
	private String field;
	
	@Builder.Default
	private int unmaskedBeg=0;
	
	@Builder.Default
	private int unmaskedEnd=0;

}
