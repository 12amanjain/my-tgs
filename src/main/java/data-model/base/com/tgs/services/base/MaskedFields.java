package com.tgs.services.base;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class MaskedFields {
	
	List<MaskedFieldSettings> maskedFields;

}
