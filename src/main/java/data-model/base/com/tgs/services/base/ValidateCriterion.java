package com.tgs.services.base;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ValidateCriterion {

	List<FieldValue> combination;
	
	int days;
}
