package com.tgs.services.base.configurationmodel;


import java.util.Map;
import lombok.Getter;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.ruleengine.IRuleOutPut;

@Getter
public class FareBreakUpConfigOutput implements IRuleOutPut {
	
	private String endpoints;

	private Map<FareComponent, List<FareComponent>> mapComponent;

	public Map<FareComponent, List<FareComponent>> getMapComponent() {
		if (MapUtils.isEmpty(mapComponent)) {
			mapComponent = new HashMap<>();
		}
		return mapComponent;
	}

}
