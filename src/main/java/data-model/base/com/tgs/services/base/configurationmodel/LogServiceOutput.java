package com.tgs.services.base.configurationmodel;

import java.util.List;
import java.util.Map;

import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;

@Getter
public class LogServiceOutput implements IRuleOutPut {

	Map<String, List<LoggingInfo>> logInfoMap;
}
