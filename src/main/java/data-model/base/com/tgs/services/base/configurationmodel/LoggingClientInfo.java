package com.tgs.services.base.configurationmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoggingClientInfo {

	/**
	 * This variable can be used for multiple purpose. One example would be to
	 * define time in messagingService.
	 */
	Double threshold;

	Boolean captureLog;

	Boolean isAlertRequired;

	/**
	 * ttl should in min
	 */
	private Integer ttl;

	private String clientName;
}
