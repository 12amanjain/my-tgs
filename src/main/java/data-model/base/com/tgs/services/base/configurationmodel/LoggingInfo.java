package com.tgs.services.base.configurationmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoggingInfo {

	private List<LoggingClientInfo> clients;
	private String key;
	private List<String> keys;
	private String logType;

	@SerializedName("it")
	private List<String> includedTypes;

	@SerializedName("et")
	private List<String> excludedTypes;

	private List<String> userRoles;
	private List<String> userIds;
}
