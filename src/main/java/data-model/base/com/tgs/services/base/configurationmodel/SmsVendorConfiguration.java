package com.tgs.services.base.configurationmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;

@Getter
public class SmsVendorConfiguration implements IRuleOutPut {

	private String url;

	private String userName;

	private String password;

	private String sender;
	
	private String route;
		
	private String authkey;
	
	private String beanName;
	
	@SerializedName("ap")
	private List<String> additionalParameters;
}

