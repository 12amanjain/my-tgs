package com.tgs.services.base.datamodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class APIPartnerMessageConfig implements IRuleOutPut {
	
	@SerializedName("issa")
	private Boolean isSmsAllowed;
	
	@SerializedName("isea")
	private Boolean isEmailAllowed;
	
	@SerializedName("ask")
	private List<String> allowedSmsTemplateKeys;
	
	@SerializedName("esk")
	private List<String> excludedSmsTemplateKeys;
	
	@SerializedName("aek")
	private List<String> allowedEmailTemplateKeys;
	
	@SerializedName("eek")
	private List<String> excludedEmailTemplateKeys;

	public List<String> getAllowedSmsTemplateKeys() {
		if(allowedSmsTemplateKeys==null)
			allowedSmsTemplateKeys = new ArrayList<>();
		return allowedSmsTemplateKeys;
	}

	public List<String> getExcludedSmsTemplateKeys() {
		if(excludedSmsTemplateKeys==null)
			excludedSmsTemplateKeys = new ArrayList<>();
		return excludedSmsTemplateKeys;
	}

	public List<String> getAllowedEmailTemplateKeys() {
		if(allowedEmailTemplateKeys==null)
			allowedEmailTemplateKeys = new ArrayList<>();
		return allowedEmailTemplateKeys;
	}

	public List<String> getExcludedEmailTemplateKeys() {
		if(excludedEmailTemplateKeys==null)
			excludedEmailTemplateKeys = new ArrayList<>();
		return excludedEmailTemplateKeys;
	}
	
	


}
