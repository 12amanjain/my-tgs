package com.tgs.services.base.datamodel;

import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class APIRequestData extends BaseAnalyticsQuery {

	private String endpoint;
	private Long timeinmillsec;
	private Integer errorcode;
	private String errmsg;
	private Object requestObj;
	private String combination;
}
