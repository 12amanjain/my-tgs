package com.tgs.services.base.datamodel;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AddressInfo {
	@NotNull
	@ApiModelProperty(required = true)
	private String address;
	@NotNull
	@ApiModelProperty(required = true)
	private String pincode;
	@Valid
	@NotNull
	@ApiModelProperty(required = true)
	private CityInfo cityInfo;
}
