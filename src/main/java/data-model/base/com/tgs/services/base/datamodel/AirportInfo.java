package com.tgs.services.base.datamodel;

import com.tgs.services.base.helper.RestExclude;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
@Accessors(chain = true)
public class AirportInfo {

	@RestExclude
	private Long id;

	@ToString.Include
	@NonNull
	private String code;
	private String name;
	private String cityCode;
	private String city;
	private String country;
	private String countryCode;
	private String terminal;

	@RestExclude
	private Boolean enabled;
	@RestExclude
	private Double priority;

	/**
	 * <code>AirportInfo</code> instance must have at least a non-null code.<br>
	 * <u>Note:</u> Applicable only for domestic trips.
	 */
	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof AirportInfo))
			return false;

		return code.equals(((AirportInfo) obj).code);
	}
}
