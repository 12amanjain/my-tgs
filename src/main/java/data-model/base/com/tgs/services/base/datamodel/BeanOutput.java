package com.tgs.services.base.datamodel;

import java.util.Map;

import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;

@Getter
public class BeanOutput implements IRuleOutPut {

	Map<BeanType, Map<String, String>> beanMap;

}
