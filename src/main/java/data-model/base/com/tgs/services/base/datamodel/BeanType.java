package com.tgs.services.base.datamodel;

import lombok.Getter;

@Getter
public enum BeanType {

	AIRINFOFACTORY, AIRBOOKINGFACTORY, AIRBOOKINGRETRIEVEFACTORY, GENERAL, AIRCANCELLATIONFACTORY;

}
