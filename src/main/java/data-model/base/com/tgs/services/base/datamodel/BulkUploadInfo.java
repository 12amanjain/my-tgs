package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulkUploadInfo {
	private String id;
	private String errorMessage;
	private UploadStatus status;
}
