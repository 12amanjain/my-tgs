package com.tgs.services.base.datamodel;

import java.util.Objects;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class BulkUploadRequest {

	private String uploadId;
	private UploadType uploadType;

	public UploadType getUploadType() {
		return Objects.isNull(uploadType) ? UploadType.SYNCHRONOUS : this.uploadType;
	}
}
