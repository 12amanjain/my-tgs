package com.tgs.services.base.datamodel;

import lombok.Getter;


@Getter
public enum CardType {

    VISA("V", "VI"),
    MASTERCARD("M", "CA"),
    DINERS_CLUB("D", "DC"),
    AMERICAN_EXPRESS("A", "AX");

    private String code;
    private String vendorCode;

    CardType(String code, String vendorCode) {
        this.code = code;
        this.vendorCode = vendorCode;
    }


    public static CardType getEnumFromCode(String code) {
        return getCardType(code);
    }

    public static CardType getCardType(String code) {
        for (CardType status : CardType.values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

}



