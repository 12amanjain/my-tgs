package com.tgs.services.base.datamodel;

import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CityInfo {
	@NotNull
	@NotNull
	@ApiModelProperty(required = true)
	private String name;
	@NotNull
	@NotNull
	@ApiModelProperty(required = true)
	private String state;
	
	@NotNull
	@NotNull
	@ApiModelProperty(required = true)
	private String country;
}
