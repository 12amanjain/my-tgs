package com.tgs.services.base.datamodel;

import java.math.BigDecimal;

public class CurrencyConverter {

	private static final BigDecimal HUNDRED = new BigDecimal(100);

	public static Long toSubUnit(BigDecimal amount) {
		return amount.setScale(2, BigDecimal.ROUND_HALF_UP).multiply(HUNDRED).longValue();
	}

	public static BigDecimal toUnit(long amount) {
		return BigDecimal.valueOf(amount).divide(HUNDRED, 2, BigDecimal.ROUND_HALF_UP);
	}
}
