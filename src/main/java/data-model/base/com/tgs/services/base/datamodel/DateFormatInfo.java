package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DateFormatInfo {

	private String dateFormat;
	private String dateTimeFormat;
	
}
