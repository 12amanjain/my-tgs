package com.tgs.services.base.datamodel;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DeliveryInfo implements Validatable, VoidClean {

	private List<String> emails;
	private List<String> contacts;
	private String address;
	private List<String> code;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		int i = 0;

		if (CollectionUtils.isEmpty(getEmails())) {
			errors.put("emails", SystemError.INVALID_EMAIL.getErrorDetail());
		} else {
			for (String email : getEmails()) {
				if (!EmailValidator.getInstance().isValid(email)) {
					errors.put("emails[" + i++ + "]", SystemError.INVALID_EMAIL.getErrorDetail());
				}
			}
		}

		if (CollectionUtils.isEmpty(getContacts())) {
			errors.put("contacts", SystemError.INVALID_MOBILE.getErrorDetail());
		} else {
			i = 0;

			for (String contact : getContacts()) {
				if (StringUtils.isEmpty(contact)) {
					errors.put("cantacts[" + i++ + "]", SystemError.INVALID_MOBILE.getErrorDetail());
				}
			}
		}


		/*
		 * Disabling this feature because currently this API doesn't support number starting with prefix 6
		 * 
		 * for (String mobile : getContacts()) { PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance(); String
		 * fieldname = "contacts[" + i++ + "]"; try { if (!phoneUtil.isValidNumber(phoneUtil.parse(mobile, "IN"))) {
		 * errors.put(fieldname, SystemError.INVALID_MOBILE.getErrorDetail()); } } catch (NumberParseException ex) {
		 * errors.put(fieldname, SystemError.INVALID_MOBILE.getErrorDetail()); } }
		 */
		return errors;
	}

	@Override
	public void cleanData() {
		setEmails(TgsCollectionUtils.getCleanStringList(emails));
		setContacts(TgsCollectionUtils.getCleanStringList(contacts));
	}

	@Override
	public boolean isVoid() {
		return emails == null && contacts == null;
	}
}
