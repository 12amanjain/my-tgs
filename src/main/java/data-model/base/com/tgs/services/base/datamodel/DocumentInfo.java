package com.tgs.services.base.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DocumentInfo {

	@SerializedName("isAv")
	private Boolean isAvailable;
	@SerializedName("lUrl")
	private String locationURL;
	private DocumentType type;
	@SerializedName("isVr")
	private Boolean isVerified;
	@SerializedName("cmt")
	private String comments;

}
