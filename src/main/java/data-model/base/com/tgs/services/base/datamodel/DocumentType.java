package com.tgs.services.base.datamodel;

public enum DocumentType {
	AADHAR, PANCARD, ADDRESS_PROOF, AGENCY_AGREEMENT, SECURITY_CHEQUE
}
