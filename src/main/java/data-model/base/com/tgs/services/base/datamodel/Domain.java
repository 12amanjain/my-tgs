package com.tgs.services.base.datamodel;

import lombok.Getter;

@Getter
public enum Domain {
    B2B("B2B"),
    CORPORATE("CORP"),
    B2C("B2C");

    private String code;

    Domain(String code) {this.code = code;}

    public static Domain getEnumFromCode(String code) {
        return getOrderType(code);
    }

    public static Domain getOrderType(String code) {
        for (Domain domain : Domain.values()) {
            if (domain.getCode().equals(code)) {
                return domain;
            }
        }
        return null;
    }
}
