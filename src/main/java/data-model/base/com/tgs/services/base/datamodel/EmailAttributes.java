package com.tgs.services.base.datamodel;

import com.tgs.services.base.enums.UserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class EmailAttributes {
	private String value;
	private String toEmailId;
	private String bccEmailId;
	private String ccEmailId;
	private String logoUrl;
	/**
	 * It is unique key to identify which template to pick
	 */
	private String key;

	// private User user;

	@Builder.Default
	private String partnerId = "0";

	private UserRole role;

	private String toEmailUserId;

	private boolean addDateTimeInSubject;

	private AttachmentMetadata attachmentData;

	private String fromEmail;

	private String salesRepName;

	private String salesRepMobile;

	private String salesRepEmail;

	private String policyInfos;
}
