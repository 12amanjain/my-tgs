package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class KeyValue implements VoidClean{
	private String key;
	private String value;

	@Override
	public void cleanData() {
		setKey(StringUtils.defaultIfBlank(key, null));
		setValue(StringUtils.defaultIfBlank(value, null));		
	}

	@Override
	public boolean isVoid() {
		return key==null && value==null;
	}

}
