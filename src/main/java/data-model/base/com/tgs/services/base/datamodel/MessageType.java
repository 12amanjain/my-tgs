package com.tgs.services.base.datamodel;

public enum MessageType {

	CONSENT, PROMO, GENERAL, QUALITY_ASSURANCE, CANCELLATION
}
