package com.tgs.services.base.datamodel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum OrderType {
	AIR("A"),
	HOTEL("H");

	public String getName() {
		return this.name();
	}

	private String code;

	private OrderType(String code) {
		this.code = code;
	}

	public static OrderType getEnumFromCode(String code) {
		return getOrderType(code);
	}

	public static OrderType getOrderType(String code) {
		for (OrderType status : OrderType.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<OrderType> types) {
		List<String> orderCodes = new ArrayList<>();
		types.forEach(type -> {
			orderCodes.add(type.getCode());
		});
		return orderCodes;
	}
}
