package com.tgs.services.base.datamodel;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PaymentHeader {

    private String url;
    private String successurl;
    private String failurl;
}
