package com.tgs.services.base.datamodel;

import java.util.Objects;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.HotelSearchType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointsConfiguration {

	@SerializedName("ad")
	private Long airDomesticMaxRedeemable;

	@SerializedName("ai")
	private Long airInternationalMaxRedeemable;

	@SerializedName("hd")
	private Long hotelDomesticMaxRedeemable;

	@SerializedName("hi")
	private Long hotelInternationalMaxRedeemable;

	@SerializedName("aop")
	private Double amountOfOnePoint;

	@SerializedName("de")
	private String defaultExpiry;


	public Double getAirMaxRedeemablePoints(AirType airType) {
		if (AirType.DOMESTIC.equals(airType)) {
			return airDomesticMaxRedeemable.doubleValue();
		} else {
			return airInternationalMaxRedeemable.doubleValue();
		}
	}

	public Double getHotelMaxRedeemablePoints(HotelSearchType hotelType) {
		if (HotelSearchType.INTERNATIONAL.equals(hotelType)) {
			return hotelInternationalMaxRedeemable.doubleValue();
		} else {
			return hotelDomesticMaxRedeemable.doubleValue();
		}
	}

	public Double getAmountOfOnePoint() {
		return Objects.nonNull(amountOfOnePoint) ? amountOfOnePoint : 0;
	}
}
