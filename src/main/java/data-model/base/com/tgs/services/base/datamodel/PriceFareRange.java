package com.tgs.services.base.datamodel;

import java.util.ArrayList;
import java.util.List;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.FareComponent;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceFareRange implements VoidChecked {

	@SerializedName("fcs")
	List<FareComponent> fareComponents;

	private double minAmount;

	private double maxAmount;

	public List<FareComponent> getFareComponents() {
		if (fareComponents == null) {
			fareComponents = new ArrayList<>();
		}
		return fareComponents;
	}

	public boolean isValidFare(AtomicDouble paxFare) {
		boolean isValid = false;
		if (paxFare.doubleValue() >= this.getMinAmount() && paxFare.doubleValue() <= this.getMaxAmount()) {
			isValid = true;
		}
		return isValid;
	}

	@Override
	public boolean isVoid() {
		return getFareComponents().isEmpty() && minAmount == 0 && maxAmount == 0;
	}

}
