package com.tgs.services.base.datamodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.AirType;
import lombok.Getter;

@Getter
public enum Product {

	/**
	 * Prefixes 50, 51, 52, 60 are deprecated.
	 */
	AIR("A", "10") {
		@Override
		public String getPrefix(ProductMetaInfo metaInfo) {
			if (metaInfo.getSubProduct().equals(AirType.INTERNATIONAL.getName())) {
				return "11";
			}
			return this.getPrefix();
		}

		@Override
		public boolean hasOrder() {
			return true;
		}

		@Override
		public ProductMetaInfo getMetaInfo(String id) {
			ProductMetaInfo metaInfo = ProductMetaInfo.builder().product(Product.AIR).build();
			if (id.matches("\\D*10.*") || id.matches("\\D*50.*")) {
				return metaInfo.setSubProduct(AirType.DOMESTIC.getName());
			}

			if (id.matches("\\D*11.*") || id.matches("\\D*51.*")) {
				return metaInfo.setSubProduct(AirType.INTERNATIONAL.getName());
			}

			return null;
		}
	},
	HOTEL("H", "20") {
		@Override
		public ProductMetaInfo getMetaInfo(String id) {
			ProductMetaInfo metaInfo = ProductMetaInfo.builder().product(Product.HOTEL).build();
			if (id.matches("\\D*20.*") || id.matches("\\D*60.*")) {
				return metaInfo;
			}
			return null;
		}

		@Override
		public boolean hasOrder() {
			return true;
		}
	},
	WALLET_TOPUP("T", "30") {
		@Override
		public ProductMetaInfo getMetaInfo(String id) {
			ProductMetaInfo metaInfo = ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build();
			if (id.matches("\\D*30.*") || id.matches("\\D*52.*")) {
				return metaInfo;
			}
			return null;
		}
	},
	FUND_TRANSFER("FT", "FT00") {
		@Override
		public ProductMetaInfo getMetaInfo(String id) {
			ProductMetaInfo metaInfo = ProductMetaInfo.builder().product(Product.FUND_TRANSFER).build();
			if (id.matches("\\D*FT00.*")) {
				return metaInfo;
			}
			return null;
		}
	},
	AMENDMENT("AMD", "") {
		@Override
		public ProductMetaInfo getMetaInfo(String id) {
			return null;
		}
	},
	NA("NA", "00") {
		@Override
		public ProductMetaInfo getMetaInfo(String id) {
			ProductMetaInfo metaInfo = ProductMetaInfo.builder().product(Product.NA).build();
			if (id.matches("\\D*00.*")) {
				return metaInfo;
			}
			return null;
		}
	};

	private String code;
	private String prefix;

	/**
	 * 
	 * @param code
	 * @param prefix
	 * @param url can be null
	 */
	Product(String code, String prefix) {
		this.code = code;
		this.prefix = prefix;
	}

	public String getPrefix(ProductMetaInfo metaInfo) {
		return prefix;
	}

	public static Product getEnumFromCode(String code) {
		return getProduct(code);
	}

	public static Product getProduct(String code) {
		for (Product product : Product.values()) {
			if (product.getCode().equals(code)) {
				return product;
			}
		}
		return null;
	}

	public static List<Product> getProductList(String[] codes) {
		List<Product> productList = new ArrayList<>();
		Arrays.asList(codes).forEach(c -> productList.add(getEnumFromCode(c)));
		return productList;
	}

	public static String[] getCodes(List<Product> products) {
		String[] codes = new String[products.size()];
		products.forEach(p -> ArrayUtils.add(codes, p.getCode()));
		return codes;
	}

	public static Product getProductFromId(String id) {
		ProductMetaInfo metaInfo = getProductMetaInfoFromId(id);
		if (metaInfo != null) {
			return metaInfo.getProduct();
		}
		return Product.NA;
	}

	public abstract ProductMetaInfo getMetaInfo(String id);

	public boolean hasOrder() {
		return false;
	}

	public static ProductMetaInfo getProductMetaInfoFromId(String id) {
		if (StringUtils.isNotBlank(id)) {
			for (Product pt : Product.values()) {
				ProductMetaInfo metaInfo = pt.getMetaInfo(id);
				if (metaInfo != null)
					return metaInfo;
			}
		}
		return ProductMetaInfo.builder().product(Product.NA).build();
	}

	public static List<Product> getBookingProducts() {
		List<Product> productList = Arrays.asList(Product.values());
		return productList.stream().filter(Product::hasOrder).collect(Collectors.toList());
	}
}
