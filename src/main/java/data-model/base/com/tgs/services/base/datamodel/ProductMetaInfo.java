package com.tgs.services.base.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@Accessors(chain = true)
public class ProductMetaInfo {

	private Product product;

	private String subProduct;

	// private String device;
}
