package com.tgs.services.base.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.ForbidInAPIRequest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class QueryFilter {

	@Valid
	private PageAttributes pageAttr;

	@Valid
	private List<SortByAttributes> sortByAttr;

	@ApiModelProperty(notes = "To fetch records based on creation date. For example if you want to fetch users created after 25th May 2018.It is mandatory to use both createdOnAfterDate and createdOnBeforeDate simultaneously", example = "2018-05-25")
	@SearchPredicate(genFilter = false, type = PredicateType.GT, dbAttribute = "createdOn", convertToDateTime = "FROM")
	private LocalDate createdOnAfterDate;

	@ApiModelProperty(notes = "To fetch records based on creation date. For example if you want to fetch users created before 25th May 2018.It is mandatory to use both createdOnAfterDate and createdOnBeforeDate simultaneously", example = "2018-05-25")
	@SearchPredicate(genFilter = false, type = PredicateType.LT, dbAttribute = "createdOn", convertToDateTime = "TO")
	private LocalDate createdOnBeforeDate;

	@ApiModelProperty(notes = "To fetch records based on creation datetime. For example if you want to fetch users created after 25th May 2018 12:50", example = "2018-05-25T12:50")
	@SearchPredicate(genFilter = false, type = PredicateType.GTE, dbAttribute = "createdOn")
	private LocalDateTime createdOnAfterDateTime;

	@ApiModelProperty(notes = "To fetch records based on creation datetime. For example if you want to fetch users created before 25th May 2018 14:50", example = "2018-05-25T14:50")
	@SearchPredicate(genFilter = false, type = PredicateType.LTE, dbAttribute = "createdOn")
	private LocalDateTime createdOnBeforeDateTime;

	@SearchPredicate(genFilter = false, type = PredicateType.EQUAL, dbAttribute = "enabled")
	private Boolean enabled;

	@ApiModelProperty(notes = "To fetch records from cache or live ", example = "LIVE")
	private FetchType fetchType;

	@ForbidInAPIRequest
	private Boolean isInternalQuery;

	private Boolean deleted;
}
