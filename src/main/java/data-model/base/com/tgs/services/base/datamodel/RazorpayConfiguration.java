package com.tgs.services.base.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class RazorpayConfiguration {

	private String userId;

	@SerializedName("ntdpf")
	private Boolean needToDeductPaymentFee;

	private String razorpayId;

	private String apiKey;

	private String apiSecret;
}
