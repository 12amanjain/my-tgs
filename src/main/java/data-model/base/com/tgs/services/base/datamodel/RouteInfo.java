package com.tgs.services.base.datamodel;

import java.time.LocalDate;

import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.SystemError;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString(onlyExplicitlyIncluded = true)
public class RouteInfo implements Validatable {

	@ToString.Include
	@Valid
	@NonNull
	private AirportInfo fromCityOrAirport;

	@ToString.Include
	@Valid
	@NonNull
	private AirportInfo toCityOrAirport;

	@Exclude
	private AirportInfo nearByFromCityOrAirport;

	@Exclude
	private AirportInfo nearByToCityOrAirport;

	@ToString.Include
	@NonNull
	private LocalDate travelDate;
	
	private String arrivalTime;
	private String departureTime;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (fromCityOrAirport == null) {
			errors.put("fromCityOrAirport", SystemError.INVALID_AIRPORT.getErrorDetail());
		}
		if (toCityOrAirport == null) {
			errors.put("toCityOrAirport", SystemError.INVALID_AIRPORT.getErrorDetail());
		}
		if (travelDate == null) {
			errors.put("travelDate", SystemError.INVALID_TRAVEL_DATE.getErrorDetail());
		}

		return errors;
	}

	@ApiModelProperty(hidden = true)
	public String getFromCityAirportCode() {
		if (nearByFromCityOrAirport != null) {
			return nearByFromCityOrAirport.getCode();
		}
		return fromCityOrAirport.getCode();
	}

	@ApiModelProperty(hidden = true)
	public String getToCityAirportCode() {
		if (nearByToCityOrAirport != null) {
			return nearByToCityOrAirport.getCode();
		}
		return toCityOrAirport.getCode();
	}

	@ApiModelProperty(hidden = true)
	public String getRouteKey() {
		return StringUtils.join(this.getFromCityAirportCode(), "_", this.getToCityAirportCode(), "_", this.travelDate);
	}

	@ApiModelProperty(hidden = true)
	public AirportInfo originalFromCityOrAirport() {
		return fromCityOrAirport;
	}

	@ApiModelProperty(hidden = true)
	public AirportInfo originalToCityOrAirport() {
		return toCityOrAirport;
	}

	@ApiModelProperty(hidden = true)
	public AirportInfo getNearByFromCityOrAirport() {
		if (nearByFromCityOrAirport != null) {
			return nearByFromCityOrAirport;
		}
		return fromCityOrAirport;
	}

	@ApiModelProperty(hidden = true)
	public AirportInfo getNearByToCityOrAirport() {
		if (nearByToCityOrAirport != null) {
			return nearByToCityOrAirport;
		}
		return toCityOrAirport;
	}
}
