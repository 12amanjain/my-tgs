package com.tgs.services.base.datamodel;

import java.util.List;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "This is useful to define ordering of results ")
public class SortByAttributes {

	@NotNull
	@ApiModelProperty(notes = "For sorting preference. Possible values are asc or desc ", required = true, example = "asc")
	private String orderBy;

	@NotNull
	@ApiModelProperty(notes = "List of sorting params. ", required = true, example = "[\"userId\",\"name\"]")
	private List<String> params;
}
