package com.tgs.services.base.datamodel;

import java.time.LocalDate;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.SystemError;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravellerInfo implements Validatable<TravellerInfoValidatingData> {

	@SerializedName("ti")
	private String title;
	@SerializedName("pt")
	private PaxType paxType;
	@SerializedName("fN")
	private String firstName;
	@SerializedName("lN")
	private String lastName;
	protected LocalDate dob;
	private Integer age;
	@SerializedName("pNum")
	private String passportNumber;
	@SerializedName("eD")
	private LocalDate expiryDate;
	@SerializedName("pNat")
	private String passportNationality;
	@SerializedName("pid")
	private LocalDate passportIssueDate;
	@SerializedName("ff")
	private Map<String, String> frequentFlierMap;

	@SerializedName("nbId")
	private String newBookingId;

	@SerializedName("obId")
	private String oldBookingId;

	@SerializedName("cp")
	private Double costPrice;

	// corporate employee
	@DBExclude
	private UserProfile userProfile;

	// corporate employee
	private String userId;

	private String invoice;

	private Long id;

	@SerializedName("pan")
	private String panNumber;

	@DBExclude
	@Exclude
	private Boolean isSave;

	public PaxType getPaxType() {
		return paxType;
	}

	public UserProfile getUserProfile() {
		return userProfile == null ? UserProfile.builder().build() : userProfile;
	}

	@Override
	public FieldErrorMap validate(TravellerInfoValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		// validate name
		if (StringUtils.isBlank(title)) {
			errors.put("title", SystemError.INVALID_TITLE.getErrorDetail());
		}
		// if (StringUtils.isBlank(firstName)) {
		// errors.put("firstName", SystemError.INVALID_NAME.getErrorDetail());
		// }
		if (StringUtils.isBlank(firstName) && StringUtils.isBlank(lastName)) {
			errors.put("firstName", SystemError.EMPTY_NAME.getErrorDetail());
		}
		if (getPaxType() == null) {
			errors.put("paxType", SystemError.INVALID_PAXTYPE.getErrorDetail());
		}
		return errors;
	}

	@ApiModelProperty(hidden = true)
	public String getMiddleName() {
		return StringUtils.EMPTY;
	}

	public String getFullName() {
		return StringUtils.join(title, " ", firstName, " ", lastName);
	}

	public String getPaxKey() {
		return StringUtils.join(firstName, " ", lastName);
	}
}
