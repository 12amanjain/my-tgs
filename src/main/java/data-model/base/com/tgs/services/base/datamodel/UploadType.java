package com.tgs.services.base.datamodel;

import lombok.Getter;

@Getter
public enum UploadType {

	SYNCHRONOUS("S"), ASYNCHRONOUS("A");

	private String code;

	UploadType(String code) {
		this.code = code;
	}
}
