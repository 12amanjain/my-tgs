package com.tgs.services.base.datamodel;

import java.math.BigDecimal;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.enums.PointsType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserBalanceSummary {

	private BigDecimal totalBalance;
	private BigDecimal walletBalance;
	private BigDecimal creditBalance;
	private Map<PointsType, BigDecimal> pointsBalance;

	private String status;

	public BigDecimal getTotalBalance() {
		return ObjectUtils.firstNonNull(totalBalance, BigDecimal.ZERO);
	}

	public BigDecimal getCreditBalance() {
		return ObjectUtils.firstNonNull(creditBalance, BigDecimal.ZERO);
	}
}
