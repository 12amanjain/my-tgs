package com.tgs.services.base.datamodel;

import java.util.HashMap;
import java.util.Map;
import lombok.Builder;
import lombok.Setter;

@Setter
@Builder
public class UserProfile {

	private Map<String, Object> data;

	public Map<String, Object> getData() {
		if (data == null)
			data = new HashMap<>();
		return data;
	}
}
