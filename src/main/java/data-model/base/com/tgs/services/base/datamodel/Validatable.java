package com.tgs.services.base.datamodel;

import org.springframework.validation.Errors;

/**
 * A data-model that needs validation can implement this interface giving
 * {@code ErrorDetail}s against field-names ({@code FieldErrorMap}) on call to
 * {@code validate(ValidatingData)}.
 * <p>
 * A {@code Validatable} might not require any {@code ValidaingData}. In that
 * case, you may completely ignore it.
 * <p>
 * Create a {@code TgsValidator} and these errors can be registered on
 * {@link Errors} easily by a call to
 * {@link TgsValidator  to registerErrors(Errors, String, Validatable, ValidatingData)}
 * or {@link TgsValidator to registerErrors(Errors, String, Validatable)} (ignoring {@code ValidaingData})
 *
 * @author Abhineet Kumar, Technogram Solutions
 * @see TgsValidator
 */
public interface Validatable<E extends ValidatingData> {

    /**
     * If validation requires some data for validation, an instance of a class
     * extending {@code ValidatingData} which encapsulates those data should be
     * expected by this function.
     * <p>
     * <u>Note:</u> It is recommended to not validate against a null data.
     *
     * @param validatingData which encapsulates data required for validation
     * @return {@code ErrorDetail}s against field-names
     * @see FieldErrorMap
     */

    public FieldErrorMap validate(E validatingData);
}
