package com.tgs.services.base.datamodel;

public interface VoidChecked {

	boolean isVoid();
	
	public static <V extends VoidChecked> boolean voidCheck(V v) {
		return v == null || v.isVoid();
	}
}
