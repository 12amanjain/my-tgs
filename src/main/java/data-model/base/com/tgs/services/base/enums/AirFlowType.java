package com.tgs.services.base.enums;

public enum AirFlowType {

    SEARCH("S"),
    REVIEW("R"),
    BOOKING_DETAIL("BD"),
    ALTERNATE_CLASS("AC");

    private String code;

    public String getCode() {
        return this.code;
    }

    AirFlowType(String code) {
        this.code = code;
    }

    public static AirFlowType getAirFlowType(String code) {
        for (AirFlowType flowType : AirFlowType.values()) {
            if (flowType.getCode().equals(code))
                return flowType;
        }
        return null;
    }


}
