package com.tgs.services.base.enums;

public enum AlertType {
	FAREALERT,
	SOLDOUT,
	DUPLICATE_ORDER,
	COMMISSION_DIFF;
}
