package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum AmendmentType {

    SSR("SSR") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_ANCILLARY_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_PROCESSED_ANCILLARY_EMAIL; }

        @Override
        public String prefix() { return "11"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
        
    },

    CANCELLATION("CAN") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_CANCELLATION_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_PROCESSED_CANCELLATION_EMAIL; }

        @Override
        public String prefix() { return "12"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
    },

    NO_SHOW("NS") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_NOSHOW_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_PROCESSED_NOSHOW_EMAIL; }

        @Override
        public String prefix() { return "13"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
    },

    VOIDED("VD") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_VOID_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_PROCESSED_VOID_EMAIL; }

        @Override
        public String prefix() { return "14"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
    },

    REISSUE("RE") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_REISSUE_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_PROCESSED_REISSUE_EMAIL; }

        @Override
        public String prefix() { return "15"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
    },

    FARE_CHANGE("FC") {
        @Override
        public EmailTemplateKey abortMailKey() { return null; }

        @Override
        public EmailTemplateKey mergeMailKey() { return null; }

        @Override
        public String prefix() { return "16"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
    },

    CANCELLATION_QUOTATION("CQ") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_CANCELLATION_QUOTATION_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_CANCELLATION_QUOTATION_EMAIL; }

        @Override
        public String prefix() { return "17"; }
    },

    REISSUE_QUOTATION("RQ") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_REISSUE_QUOTATION_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_REISSUE_QUOTATION_EMAIL; }

        @Override
        public String prefix() { return "18"; }
    },

    MISCELLANEOUS("MISC") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_MISCELLANEOUS_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_MERGE_EMAIL; }

        @Override
        public String prefix() { return "19"; }
    },
    CORRECTION("CORR") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_MISCELLANEOUS_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_MERGE_EMAIL; }

        @Override
        public String prefix() { return "10"; }
    },
    FULL_REFUND("FR") {
        @Override
        public EmailTemplateKey abortMailKey() { return EmailTemplateKey.AMENDMENT_ABORT_CANCELLATION_EMAIL; }

        @Override
        public EmailTemplateKey mergeMailKey() { return EmailTemplateKey.AMENDMENT_PROCESSED_CANCELLATION_EMAIL; }

        @Override
        public String prefix() { return "20"; }
        
        @Override
        public boolean isVirtualPaymentAllowed() {
        	return true;
        }
    },
    HOTEL_CORRECTION("H_CORR"){

		@Override
		public EmailTemplateKey abortMailKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public EmailTemplateKey mergeMailKey() {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String prefix() {
			return "31";
		}
    	
    };


    private String code;

    AmendmentType(String code) {
        this.code = code;
    }

    public static AmendmentType getEnumFromCode(String code) {
        return getAmendmentType(code);
    }

    public static AmendmentType getAmendmentType(String code) {
        for (AmendmentType type : AmendmentType.values()) {
            if (type.getCode().equals(code)) {
                return type;
            }
        }
        return null;
    }

    public abstract EmailTemplateKey abortMailKey();

    public abstract EmailTemplateKey mergeMailKey();

    public abstract String prefix();
    
    public  boolean isVirtualPaymentAllowed() {
    	return false;
    };

    public static AmendmentType getAmendmentTypeFromId(String amendmentId) {
        if (amendmentId == null)
            return null;
        String prefix = amendmentId.substring(3,5);
        for (AmendmentType type : AmendmentType.values()) {
            if (type.prefix().equals(prefix)) {
                return type;
            }
        }
        return null;
    }
}
