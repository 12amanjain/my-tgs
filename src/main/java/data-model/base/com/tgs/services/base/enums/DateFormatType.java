package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum DateFormatType {

	REPORT_FORMAT,
	BOOKING_EMAIL_FORMAT,
	AMENDMENT_EMAIL_FORMAT,
	CREDIT_EMAIL_FORMAT,
	DEFAULT_FORMAT;

}
