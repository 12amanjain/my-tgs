package com.tgs.services.base.enums;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.FareComponentDetail;
import lombok.Getter;

@Getter
public enum FareComponent {

	BF("BaseFare") {
		@Override
		public boolean fareChangeApplicable() {
			return true;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},

	TF("TotalFare") {},

	NF("Net Fare") {
		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},

	OB("Carrier Ticketing Fee") {
		// This fare will not be charged from customer
		@Override
		public boolean airlineComponent() {
			return false;
		}
	},

	YR("Carrier Misc Fee") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},

	RCF("Travel Fee") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},

	YQ("FuelSurcharge") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},

	OT("Other"),

	WO("Airport Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	PSF("PassengerServiceFee") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	DS("Discount") {
		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},
	KKC("Krishi Kalyan Cess") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	TAF("Taxes and Fee") {
		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},
	UDF("User Development Fee") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	WC("Airport Arrival Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	YM("Development Fee") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	OC("Other Charges") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean fareChangeApplicable() {

			return true;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	SBC("Swachh Bharat Cess") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	AGST("Airline GST") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public boolean fareChangeApplicable() {

			return true;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}

	},
	CMU("Client Mark Up") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean isUpdateTotalFare() {
			return true;
		}
	},
	GST("GST") {

		@Override
		public boolean isUpdateTotalFare() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	AT("Airline Taxes") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},

	SC("Supplier Commission") {

		/**
		 * We will populate this component at supplier level , this will not add to any other components, It's just for
		 * information purpose only
		 */

	},
	SSRP("Special Service Price") {
		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},

	BP("Baggage Price") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.SSRP;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	MP("Meal Price") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.SSRP;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	SP("Seat Price") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.SSRP;
		}

		@Override
		public boolean airlineComponent() {
			return true;
		}
	},
	MU("Mark Up") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},

	PMU("Mark Up") {
		/**
		 * Partner (Whitelabel,Distributor) Markup
		 * 
		 * @implNote : This component is used by partner to keep their Markup <br>
		 *           to their agents/sub roles under partners
		 * 
		 */
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PMTDS);
		}

	},

	MF("Management Fee") {

		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(MFT);
		}

		@Override
		public boolean isUpdateTotalFare() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	AI("Additional Incentive") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PAI;
		}
	},
	PAI("Partner Additional Incentive") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}
	},
	MFT("Management Fee Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean isUpdateTotalFare() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	NCM("Net Commission") {

		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},
	PNCM("Partner Net Commission") {

		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},
	TDS("TDS") {

		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public double getAmount(double amount) {
			return -1 * amount;
		}

	},
	PMTDS("Partner Markup TDS") {

		@Override
		public FareComponent mapComponent() {
			// This is chargeable from partner agents, so it will go as TAF
			return FareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public double getAmount(double amount) {
			return -1 * amount;
		}

	},
	PCTDS("Partner Commission TDS") {

		@Override
		public FareComponent mapComponent() {
			// This is chargeable from partner agents, for commission bifurcation it will go PNCM
			return FareComponent.PNCM;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public double getAmount(double amount) {
			return -1 * amount;
		}

	},
	BCM("Commission") {
		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PBCM;
		}
	},
	PBCM("Partner Commission") {
		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}
	},
	TC("Tour Code") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}
	},
	IATA("IATA Commission") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PIATA;
		}
	},
	PIATA("Partner IATA Commission") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}
	},
	PLB("PLB") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PPLB;
		}
	},
	PPLB("Partner PLB") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}
	},
	SM("Segment Money") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PSM;
		}
	},
	PSM("Partner Segment Money") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}
	},
	CC("CC") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PCC;
		}
	},
	PCC("Partner CC") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}
	},
	PFCM("PFCM") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.NCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(TDS);
		}

		@Override
		public FareComponent getPartnerComponent() {
			return FareComponent.PPFCM;
		}
	},
	PPFCM("Partner PFCM") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.PNCM;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(PCTDS);
		}
	},
	ACF("Airline Cancellation Fee") {
		@Override
		public boolean usedInAmendment() {

			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(ACFT);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	ACFT("Airline Cancellation Taxes") {
		@Override
		public boolean usedInAmendment() {

			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}
	},
	CCF("Client Cancellation Fee") {
		@Override
		public boolean usedInAmendment() {

			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(CCFT);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	CCFT("Client Cancellation Fee Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override

		public boolean keepCurrentComponent() {
			return true;
		}
	},
	ARF("Airline Rescheduling Fee") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(ARFT);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	ARFT("Airline Rescheduling Tax") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}
	},
	CRF("Client Rescheduling Fee") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(CRFT);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	CRFT("Client Reschedule Fee Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	AAF("Airline Amendment Fee") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(AAFT);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	AAFT("Airline Amendment Fee Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean usedInAmendment() {

			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	CAF("Client Amendment Fee") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public List<FareComponent> dependentComponents() {
			return Arrays.asList(CAFT);
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	CAFT("Client Amendment Fee Tax") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean usedInAmendment() {

			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	CAMU("Client Amendment Mark Up") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public FareComponent mapComponent() {
			return FareComponent.AFS;
		}
	},
	AAR("Amount Applicable for Refund") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	AFS("Amendment Fee Sum") {
		@Override
		public boolean isPersistenceAllowed() {
			return false;
		}
	},
	CGST("CGST") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}
	},
	SGST("SGST") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}
	},
	IGST("IGST") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}
	},
	UGST("USGT") {
		@Override
		public boolean usedInAmendment() {
			return true;
		}
	},
	XT("Additional Taxes") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean isUpdateTotalFare() {
			return true;
		}
	},
	PF("Payment Fee") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}
	},
	CS("Credit Shell") {

	},
	SMF("Supplier Management Fee") {
		/**
		 * We will populate this component at supplier level , this will not add to any other components, It's just for
		 * information purpose only
		 */
	},
	VD("Voucher Discount") {

		@Override
		public double getAmount(double amount) {
			return amount;
		}

	},
	RP("Redeemed Points") {
		@Override
		public FareComponent mapComponent() {
			return FareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	};

	private String desc;

	FareComponent(String desc) {
		this.desc = desc;
	}

	/**
	 * This is non useful when you want to update dependent component as well. For example if BC is set then TF should
	 * also be updated.
	 * 
	 * @return
	 */
	public List<FareComponent> dependentComponents() {
		return new ArrayList<>();
	}

	public FareComponent mapComponent(FareBreakUpConfigOutput fbConfig, String event) {

		if (fbConfig != null && MapUtils.isNotEmpty(fbConfig.getMapComponent())) {
			// Override mappedcomponent from config.
			for (Entry<FareComponent, List<FareComponent>> entry : fbConfig.getMapComponent().entrySet()) {
				if (CollectionUtils.isNotEmpty(entry.getValue()) && entry.getValue().contains(this)) {
					return entry.getKey();
				}
			}

		}
		return mapComponent();
	}

	protected FareComponent mapComponent() {
		return null;
	}

	public boolean keepCurrentComponent() {
		return false;
	}

	public boolean isPersistenceAllowed() {
		return true;
	}

	public boolean isUpdateTotalFare() {
		return false;
	}

	public double getAmount(double amount) {
		return amount;
	}

	public boolean fareChangeApplicable() {
		return false;
	}

	public boolean airlineComponent() {
		return false;
	}

	public boolean usedInAmendment() {
		return false;
	}

	public FareComponent getPartnerComponent() {
		return null;
	}

	public static List<FareComponentDetail> getDetailList() {
		List<FareComponentDetail> componentDetailList = new ArrayList<>();
		Arrays.asList(FareComponent.values()).forEach(fc -> componentDetailList.add(getDetail(fc)));
		return componentDetailList;
	}

	public static FareComponentDetail getDetail(FareComponent fc) {
		FareComponentDetail detail = new FareComponentDetail();
		detail.setCode(fc.name());
		detail.setName(fc.getDesc());
		detail.setAirlineComponent(fc.airlineComponent());
		detail.setAmenable(fc.fareChangeApplicable());
		return detail;
	}

	public static Set<FareComponent> getNoShowRefundableComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.WO);
		components.add(FareComponent.WC);
		return components;
	}

	public static Set<FareComponent> getGstComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.MFT);
		components.add(FareComponent.CCFT);
		components.add(FareComponent.CRFT);
		components.add(FareComponent.CAFT);
		return components;
	}

	public static Set<FareComponent> getCommissionComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.BCM);
		components.add(FareComponent.TC);
		components.add(FareComponent.CC);
		components.add(FareComponent.AI);
		components.add(FareComponent.PLB);
		components.add(FareComponent.SM);
		components.add(FareComponent.PFCM);
		return components;
	}

	public static Set<FareComponent> getPartnerCommissionComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.PBCM);
		components.add(FareComponent.PCC);
		components.add(FareComponent.PAI);
		components.add(FareComponent.PPLB);
		components.add(FareComponent.PSM);
		components.add(FareComponent.PPFCM);
		return components;
	}

	public static Set<FareComponent> getAllCommisionComponents() {
		Set<FareComponent> components = getCommissionComponents();
		components.addAll(getPartnerCommissionComponents());
		return components;
	}

	public static Set<FareComponent> getCommercialComponents() {
		Set<FareComponent> components = new HashSet<>(getCommissionComponents());
		components.add(FareComponent.MF);
		components.add(FareComponent.CMU);
		return components;
	}

	public static Set<FareComponent> getSSRComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.BP);
		components.add(FareComponent.MP);
		components.add(FareComponent.SP);
		return components;
	}

	public static Set<FareComponent> getRefundableComponents(boolean isSSRRefundable) {
		Set<FareComponent> components = Arrays.stream(FareComponent.values()).filter(FareComponent::airlineComponent)
				.collect(Collectors.toSet());
		if (!isSSRRefundable) {
			components.removeAll(getSSRComponents());
		}
		components.add(XT);
		return components;
	}

	public static Set<FareComponent> taxComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(IGST);
		components.add(CGST);
		components.add(SGST);
		components.add(UGST);
		return components;
	}

	public static Set<FareComponent> getManualRefundableFC() {
		Set<FareComponent> components = new HashSet<>(getSSRComponents());
		components.add(CMU);
		return components;
	}

	public static Set<FareComponent> voidComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(ACF);
		components.add(ACFT);
		components.add(CCF);
		components.add(CCFT);
		components.add(AAR);
		// PMU is not refundable & recallable
		components.add(PMU);
		components.addAll(pointComponents());
		return components;
	}

	// Non Refundable Cancellation Components
	public static Set<FareComponent> getCancellationComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(ACF);
		components.add(ACFT);
		components.add(CCF);
		components.add(CCFT);
		components.add(MF);
		components.add(MFT);
		components.add(PF);
		// PMU is not refundable & recallable
		components.add(PMU);
		components.add(RP);
		return components;
	}

	// Non refundable paxCancel components
	public static Set<FareComponent> paxCancelComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(ACF);
		components.add(ACFT);
		components.add(CCF);
		components.add(CCFT);
		components.add(CAMU);
		// PMU is not refundable & recallable
		components.add(PMU);
		return components;
	}

	// Client fee components where partner used to apply his client fee for Fare rule
	public static Set<FareComponent> getClientFeeComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(CCF);
		components.add(CRF);
		return components;
	}

	// Client fee tax components where partner used to apply his client fee for Fare rule
	public static Set<FareComponent> getClientFeeTaxComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(CCFT);
		components.add(CRFT);
		return components;
	}

	public static Set<FareComponent> voucherComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.VD);
		return components;
	}

	public static Set<FareComponent> pointComponents() {
		Set<FareComponent> components = new HashSet<>();
		components.add(FareComponent.RP);
		return components;
	}

	public static double getGSTRate() {
		return 0.18;
	}

}
