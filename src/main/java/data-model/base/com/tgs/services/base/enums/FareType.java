package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum FareType {

	SPECIAL_RETURN,
	SALE,
	PUBLISHED,
	SME,
	CORPORATE,
	PROMO,
	FAMILY,
	HANDBAGGAGE,
	COUPON,
	OFFER_FARE_WITH_PNR,
	OFFER_FARE_WITHOUT_PNR,
	BUSINESS,

	// Indigo
	LITE,
	FLEXI,
	TACTICAL,

	// FlyScoot
	FLY,
	FLYBAG,
	FLYBAGEAT,
	SCOOTBIZ,
	SCOOTPLUS,

	// Jazzera
	EXTRA,

	// Go Air
	GOMORE,

	// AirAsia
	ECONOMY,
	ECONOMY_PROMO,
	PREMIUM_FLEX,
	PREMIUM_FLATBED,
	CORPORATE_LITE,
	CORPORATE_FLEX,

	// Radixx
	EXPRESS_PROMO,
	EXPRESS_VALUE,
	VALUE,
	EXPRESS_FLEXI,
	EXPRESS_MIXED,
	FLEX,
	FLEXI_PLUS,
	PAYTOCHANGE,
	FREETOCHANGE,
	NOCHANGE,
	LIGHT,
	WLIGHT,
	FRIENDLY,
	WFRIENDLY,
	WFLEX,
	
	// SpiceJet
	COVID_SPECIAL;

	public String getName() {
		return this.name();
	}

	public static FareType getEnumFromName(String name) {
		for (FareType fareType : FareType.values()) {
			if (fareType.getName().equals(name)) {
				return fareType;
			}
		}
		return FareType.PUBLISHED;
	}
}
