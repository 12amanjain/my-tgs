package com.tgs.services.base.enums;

import com.tgs.services.base.LogData;

/**
 * It is only suggestive about who the target audience is. Complex rules can be used for each visibility group when
 * trying to create visibility filters like who among the group can see the log data from which IPs.
 * 
 * @see ProbeController
 * @see LogData#getVisibility()
 * @see UserAdditionalInfo#getGroups()
 * 
 * @author Abhineet Kumar
 *
 */
public enum LogDataVisibilityGroup {

	PUBLIC, CLIENT, DEVELOPER;

	public static LogDataVisibilityGroup getValue(String name) {
		for (LogDataVisibilityGroup visibilityGroup : values()) {
			if (visibilityGroup.name().equals(name)) {
				return visibilityGroup;
			}
		}
		return PUBLIC;
	}
}
