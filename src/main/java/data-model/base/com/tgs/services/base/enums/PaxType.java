package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum PaxType {

	ADULT("ADT", "A"), CHILD("CNN", "C"), INFANT("INF", "I");

	private String type;
	private String code;

	PaxType(String type, String code) {
		this.type = type;
		this.code = code;
	}

	public static PaxType getPaxTypeFromCode(String code) {
		for (PaxType paxType : PaxType.values()) {
			if (paxType.getCode().equals(code))
				return paxType;
		}
		return null;
	}

	public static PaxType getEnumFromCode(String code) {
		return getPaxTypeFromCode(code);
	}

	public static PaxType getPaxType(String type) {
		for (PaxType paxType : PaxType.values()) {
			if (paxType.getType().equals(type))
				return paxType;
		}
		return null;
	}

	public static String getStringReprestation(PaxType paxType) {
		switch(paxType) {
			case ADULT:
				return "ADT";
			case CHILD:
				return "CHD";
			case INFANT:
				return "INF";
			default:
				return paxType.getCode();
		}		
	}
}
