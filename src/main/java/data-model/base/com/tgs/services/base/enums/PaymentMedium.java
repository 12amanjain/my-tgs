package com.tgs.services.base.enums;

import lombok.Getter;

@Getter
public enum PaymentMedium {
	WALLET("W") {
		@Override
		public boolean isWalletPaymentMedium() {
			return true;
		}
	},
	CREDIT_LINE("CL") {
		@Override
		public boolean isWalletPaymentMedium() {
			return true;
		}
	},
	CREDIT("NRC") {
		@Override
		public boolean isWalletPaymentMedium() {
			return true;
		}
	},
	CREDITCARD("CC") {
		@Override
		public boolean isExternalPaymentMedium() {
			return true;
		}
	},
	DEBITCARD("DC") {
		@Override
		public boolean isExternalPaymentMedium() {
			return true;
		}
	},
	NETBANKING("NB") {
		@Override
		public boolean isExternalPaymentMedium() {
			return true;
		}
	},
	VIRTUAL_PAYMENT("VP") {
		@Override
		public boolean isExternalPaymentMedium() {
			return false;
		}
	},
	POINTS("P") {
		@Override
		public boolean isWalletPaymentMedium() {
			return true;
		}
	},
	FUND_HANDLER("FH");

	public String getPaymentMedium() {
		return this.getCode();
	}

	private String code;

	PaymentMedium(String code) {
		this.code = code;
	}

	public static PaymentMedium getEnumFromCode(String code) {
		return getPaymentMedium(code);
	}

	public static PaymentMedium getPaymentMedium(String code) {
		for (PaymentMedium medium : PaymentMedium.values()) {
			if (medium.getCode().equals(code)) {
				return medium;
			}
		}
		return null;
	}

	public boolean isExternalPaymentMedium() {
		return false;
	}

	public boolean isWalletPaymentMedium() {
		return false;
	}

}
