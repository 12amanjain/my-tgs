package com.tgs.services.base.enums;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Getter;

@Getter
public enum UserRole {

	AGENT("A", "1") {
		@Override
		public boolean isDepositWalletAllowed() {
			return true;
		}

		@Override
		public boolean isCreditLineAllowed() {
			return true;
		}
	},
	AGENT_STAFF("AS", "2"),
	DISTRIBUTOR("D", "3") {
		@Override
		public boolean isDepositWalletAllowed() {
			return true;
		}

		@Override
		public boolean canTransferFunds() {
			return true;
		}

		@Override
		public boolean isCreditLineAllowed() {
			return true;
		}
	},
	DISTRIBUTOR_STAFF("DS", "4"),
	ADMIN("AD", "5"),
	ACCOUNTS("AC", "6"),
	CALLCENTER("C", "7"),
	SALES("SL", "8"),
	SUPPLIER("SU", "9"),
	SUPERVISOR("SV", "10"),
	SALES_SUPPORT("SS", "11"),
	SUPPLIER_ADMIN("SA", "12"),
	GUEST("G","13"),
	CUSTOMER("CR","14"),
	CORPORATE("CORP","15") {
		@Override
		public boolean isDepositWalletAllowed() {
			return true;
		}

		@Override
		public boolean isCreditLineAllowed() {
			return true;
		}
	},
	CORPORATE_STAFF("CORPST","16") {
		@Override
		public boolean isDepositWalletAllowed() {
			return true;
		}

		@Override
		public boolean isCreditLineAllowed() {
			return true;
		}
	},
	CORPORATE_EMPLOYEE("CORPEM","17"),
	WHITELABEL_PARTNER("WLAD", "18") {
		@Override
		public boolean isDepositWalletAllowed() {
			return true;
		}

		@Override
		public boolean isCreditLineAllowed() {
			return true;
		}
	},
	WHITELABEL_STAFF("WLST","19"),
	HOTEL_SUPPLIER("HSU", "20");

	private String code;
	private String prefix;
	private static Set<UserRole> midOfficeRoles;

	static {
		midOfficeRoles = new HashSet<>();
		midOfficeRoles.add(UserRole.ADMIN);
		midOfficeRoles.add(UserRole.ACCOUNTS);
		midOfficeRoles.add(UserRole.CALLCENTER);
		midOfficeRoles.add(UserRole.SUPERVISOR);
		midOfficeRoles.add(UserRole.SALES);
		midOfficeRoles.add(UserRole.SALES_SUPPORT);
	}

	UserRole(String code, String prefix) {
		this.code = code;
		this.prefix = prefix;
	}

	public static List<String> displayNameList() {
		return null;
	}

	public String getRoleDisplayName() {
		return this.name();
	}

	public static UserRole getEnumFromCode(String code) {
		return getUserRole(code);
	}

	public static UserRole getUserRole(String code) {
		for (UserRole role : UserRole.values()) {
			if (role.getCode().equals(code)) {
				return role;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<UserRole> userRoles) {
		List<String> userCodes = new ArrayList<>();
		userRoles.forEach(userRole -> userCodes.add(userRole.getCode()));
		return userCodes;
	}

	public static Set<UserRole> getMidOfficeRoles() {
		return midOfficeRoles;
	}

	public boolean isDepositWalletAllowed() {
		return false;
	}

	public boolean isCreditLineAllowed() {
		return false;
	}

	public boolean canTransferFunds() {
		return false;
	}

	public static boolean corporate(UserRole role) {
		return (role.equals(CORPORATE_EMPLOYEE) || role.equals(CORPORATE) || role.equals(CORPORATE_STAFF));
	}
}
