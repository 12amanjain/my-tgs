package com.tgs.services.base.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.tgs.services.base.helper.Exclude;

public class AnnotationExclusionStrategy implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes f) {

		return (f.getAnnotation(Exclude.class) != null
				|| f.getDeclaredClass().getAnnotation(Exclude.class) != null);
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}
}
