package com.tgs.services.base.gson;

import java.lang.reflect.Field;

import com.google.gson.FieldNamingStrategy;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.CustomSerializedName;

public enum CustomFieldNamingPolicy implements FieldNamingStrategy {
	
	
	STANDARDNAME(){

	@Override
	public String translateName(Field f) {
		if(f.getAnnotation(CustomSerializedName.class) != null) {
			return f.getAnnotation(CustomSerializedName.class).key().getName();
		}else if(f.getAnnotation(SerializedName.class) != null) {
			return f.getAnnotation(SerializedName.class).value();
		}
		return f.getName();
	}
  };
	
	
}