package com.tgs.services.base.gson;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.TypeAdapterFactory;
import com.tgs.services.base.helper.ToUpper;
import com.tgs.services.base.utils.ApplicationConstant;
import lombok.Builder;

@Builder
public class GsonUtils {

	private List<ExclusionStrategy> strategies;
	private List<TypeAdapterFactory> typeFactories;

	public Gson buildGson() {
		return getGson(strategies, typeFactories);
	}

	public GsonBuilder buildGsonBuilder() {
		return getGsonBuilder(strategies, typeFactories);
	}

	public static Gson getGson() {
		Gson gson = getGsonBuilder().create();
		return gson;
	}

	public static Gson getGson(List<ExclusionStrategy> strategies, List<TypeAdapterFactory> typeFactories) {
		GsonBuilder gsonBuilder = getGsonBuilder(strategies, typeFactories);

		return gsonBuilder.create();
	}

	public static GsonBuilder getGsonBuilder() {
		return getGsonBuilderWithDefaultAdapters();
	}

	public static GsonBuilder getGsonBuilder(List<ExclusionStrategy> strategies,
			List<TypeAdapterFactory> typeFactories) {
		// new GsonBuilder().setExclusionStrategies(strategies);
		// getGsonBuilder().addDeserializationExclusionStrategy(new
		// AnnotationExclusionStrategy());
		GsonBuilder gsonBuilder = getGsonBuilderWithDefaultAdapters();
		if (strategies != null) {
			for (ExclusionStrategy strategy : strategies) {
				gsonBuilder = gsonBuilder.addSerializationExclusionStrategy(strategy);
			}
		}
		if (typeFactories != null) {
			for (TypeAdapterFactory typeFactory : typeFactories) {
				gsonBuilder.registerTypeAdapterFactory(typeFactory);
			}
		}
		return gsonBuilder;
	}

	private static GsonBuilder getGsonBuilderWithDefaultAdapters() {
		GsonBuilder builder = new GsonBuilder().registerTypeAdapter(Calendar.class, new CalendarAdapter(null))
				.registerTypeAdapter(LocalDate.class, new LocalDateAdapter(null))
				.registerTypeAdapter(LocalTime.class, new LocalTimeAdapter(null))
				.registerTypeAdapter(Double.class, new JsonDoubleSerializer())
				.registerTypeAdapter(Double.class, new JsonDoubleDeserializer())
				.registerTypeAdapter(String.class, new StringGsonTypeAdapter())
				.registerTypeAdapter(BigDecimal.class, new JsonBigDecimalAdapter())
				.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter(null)).disableHtmlEscaping();

		
		builder.setFieldNamingStrategy(CustomFieldNamingPolicy.STANDARDNAME);
		return builder;
	}

	public <T> Gson buildGson(String str, Class<T> classOfT) throws JSONException {
		GsonBuilder gsonBuilder = getGsonBuilder(strategies, typeFactories);
		Class<? super T> superClass = classOfT;
		do {
			registerAdapter(str, superClass, gsonBuilder, null, null);
		} while ((superClass = superClass.getSuperclass()) != null
				&& !superClass.getName().equals(Object.class.getName()));
		return gsonBuilder.create();
	}

	public <T> Gson buildGson(String str, Class<T> classOfT, Optional<Class> typeArgClass) throws JSONException {
		GsonBuilder gsonBuilder = getGsonBuilder(strategies, typeFactories);
		Class<? super T> superClass = classOfT;
		do {
			registerAdapter(str, superClass, gsonBuilder, null, typeArgClass);
		} while ((superClass = superClass.getSuperclass()) != null
				&& !superClass.getName().equals(Object.class.getName()));
		return gsonBuilder.create();
	}

	public static <T> Gson buildGson(String str, Class<T> classOfT, GsonBuilder gsonBuilder) throws JSONException {

		Class<? super T> superClass = classOfT;
		do {
			registerAdapter(str, superClass, gsonBuilder, null, null);
		} while ((superClass = superClass.getSuperclass()) != null
				&& !superClass.getName().equals(Object.class.getName()));
		return gsonBuilder.create();
	}


	@SuppressWarnings({ "rawtypes", "unchecked" })
	private static <T> void registerPolymorphismAdapter(GsonBuilder gsonBuilder, Field[] fields, Class<T> classofT) {
		RuntimeTypeAdapterFactory runtimeTypeAdapterFactory = null;
		for (Field innerFields : fields) {
			innerFields.setAccessible(true);
			GsonPolymorphismMapping mappingAnon = innerFields.getAnnotation(GsonPolymorphismMapping.class);
			if (mappingAnon != null) {
				runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory.of(classofT, innerFields.getName());
				for (ClassType keyValue : mappingAnon.value()) {
					runtimeTypeAdapterFactory.registerSubtype(keyValue.value(), keyValue.key());
				}
				gsonBuilder.registerTypeAdapterFactory(runtimeTypeAdapterFactory);
			}
		}
	}

	private static <T> void registerDependantPolymorphismAdapter(GsonBuilder gsonBuilder, JSONObject json,
			JSONObject rootJson, Class<T> clazz) {

		GsonRunTimeAdaptorRequired runTimeAnnon = clazz.getAnnotation(GsonRunTimeAdaptorRequired.class);
		String dependUpon = runTimeAnnon.dependOn();
		GsonPolymorphismMapping mappingAnon = clazz.getAnnotation(GsonPolymorphismMapping.class);
		String value = !json.optString(dependUpon).equals("") ? json.optString(dependUpon)
				: JsonUtils.findValue(rootJson, dependUpon);
		if (value != null) {
			RuntimeTypeAdapterFactory runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory.of(clazz);
			for (ClassType keyValue : mappingAnon.value()) {
				for (String key : keyValue.keys()) {
					if (key.equals(value)) {
						runtimeTypeAdapterFactory.registerSubtype(keyValue.value());
						gsonBuilder.registerTypeAdapterFactory(runtimeTypeAdapterFactory);
					}
				}
			}
		}
	}

	private static List<Field> getInheritedFields(Class<?> type) {
		List<Field> allFields = new ArrayList<Field>();
		Class<?> i = type;
		while (i != null && i != Object.class) {
			Collections.addAll(allFields, i.getDeclaredFields());
			i = i.getSuperclass();
		}
		return allFields;
	}

	private static JSONArray getJSONArray(String jsonString) throws JSONException {
		try {
			return new JSONArray(jsonString);
		} catch (Exception e) {
			return (new JSONArray()).put(new JSONObject(jsonString));
		}
	}

	@SuppressWarnings("unchecked")
	private static <T> Class<T> getClass(Field field) {
		if (field.getGenericType() instanceof ParameterizedType) {
			return (Class<T>) ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
		}
		return (Class<T>) field.getGenericType();
	}

	private static boolean isValidFieldType(Field f) {
		if (f.getGenericType() instanceof ParameterizedType) {
			Class argType = (Class) ((ParameterizedType) f.getGenericType()).getActualTypeArguments()[0];
			return argType.getName().contains(ApplicationConstant.ADAPTABLE_PACKAGE) && !argType.isEnum()
					&& argType != Class.class;
		}
		return f.getGenericType().getTypeName().contains(ApplicationConstant.ADAPTABLE_PACKAGE) && !f.getType().isEnum()
				&& (f.getType() != Class.class);
	}

	@SuppressWarnings("unchecked")
	public static <T> void registerAdapter(String str, Class<T> classOfT, GsonBuilder gsonBuilder, JSONObject rootJson,
			Optional<Class> typeArgClass) throws JSONException {
		@SuppressWarnings("rawtypes")
		RuntimeTypeAdapterFactory runtimeTypeAdapterFactory = null;
		if (StringUtils.isBlank(str)) {
			return;
		}

		if (Collection.class.isAssignableFrom(classOfT) && typeArgClass.isPresent()) {
			classOfT = typeArgClass.get();
		}
		JSONArray jsonArray = getJSONArray(str);
		for (int i = 0; i < jsonArray.length(); i++) {
			JSONObject json = jsonArray.optJSONObject(i);

			if (rootJson == null) {
				rootJson = json;
			}
			if (classOfT.getAnnotation(GsonRunTimeAdaptorRequired.class) != null) {
				if (!classOfT.getAnnotation(GsonRunTimeAdaptorRequired.class).dependOn().isEmpty()) {
					registerDependantPolymorphismAdapter(gsonBuilder, json, rootJson, classOfT);
				} else {
					registerPolymorphismAdapter(gsonBuilder, classOfT.getDeclaredFields(), classOfT);
				}
			}
			for (Field f : getInheritedFields(classOfT)) {
				Class<?> classOfField = getClass(f);
				if (f.getAnnotation(GsonRunTimeAdaptorRequired.class) != null) {
					f.setAccessible(true);
					GsonRunTimeAdaptorRequired runTimeAnnon = f.getAnnotation(GsonRunTimeAdaptorRequired.class);
					String dependUpon = runTimeAnnon.dependOn();
					if (StringUtils.isBlank(dependUpon)) {
						registerPolymorphismAdapter(gsonBuilder, f.getType().getDeclaredFields(), f.getType());
					} else {
						GsonPolymorphismMapping mappingAnon = f.getAnnotation(GsonPolymorphismMapping.class);
						String value = !json.optString(dependUpon).equals("") ? json.optString(dependUpon)
								: JsonUtils.findValue(rootJson, dependUpon);
						if (value != null) {
							runtimeTypeAdapterFactory = RuntimeTypeAdapterFactory.of(f.getType());
							for (ClassType keyValue : mappingAnon.value()) {
								for (String key : keyValue.keys()) {
									if (key.equals(value)) {
										runtimeTypeAdapterFactory.registerSubtype(classOfField = keyValue.value());
										gsonBuilder.registerTypeAdapterFactory(runtimeTypeAdapterFactory);
									}
								}
							}
						}
					}
				}
				if (f.getAnnotation(ToUpper.class) != null && StringUtils.isNotEmpty(json.optString(f.getName()))) {
					json = json.put(f.getName(), StringUtils.upperCase(json.getString(f.getName())));
				}
				if (isValidFieldType(f)) {
					registerAdapter(json.optString(f.getName()), classOfField, gsonBuilder, rootJson, Optional.empty());
				}
			}
		}
	}

	/**
	 * Gson returned will only serialize the fields mentioned in fieldInclusions
	 * list
	 * 
	 * @param fieldInclusions
	 * @return
	 */
	public static Gson buildGsonToSerializeSomeFields(final List<String> fieldInclusions,
			final List<String> fieldExclusions) {
		GsonBuilder b = getGsonBuilderWithDefaultAdapters();
		b.addSerializationExclusionStrategy(new FieldExclusionStrategy(fieldInclusions, fieldExclusions));
		b.serializeNulls();
		return b.create();
	}
}
