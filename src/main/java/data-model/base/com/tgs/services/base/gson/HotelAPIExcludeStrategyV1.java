package com.tgs.services.base.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.tgs.services.base.helper.HotelAPIExcludeV1;

public class HotelAPIExcludeStrategyV1 implements ExclusionStrategy {

	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(HotelAPIExcludeV1.class) != null
				|| f.getDeclaredClass().getAnnotation(HotelAPIExcludeV1.class) != null;
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
