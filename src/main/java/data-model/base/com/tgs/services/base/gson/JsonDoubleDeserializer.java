package com.tgs.services.base.gson;

import java.lang.reflect.Type;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;

public class JsonDoubleDeserializer implements JsonDeserializer<Double> {

	@Override
	public Double deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
			throws JsonParseException {
		try {
			return Double.valueOf(json.getAsString());
		} catch (NumberFormatException e) {
			throw new CustomGeneralException(SystemError.INVALID_DATA_FORMAT,
					"Excepting double number but found " + json.getAsString());
		}
	}

}
