package com.tgs.services.base.gson;

import java.util.Iterator;
import org.json.JSONObject;

public class JsonUtils {

	public static String findValue(JSONObject object, String searchedKey) {
		String value = object.optString(searchedKey, null);
		if (value == null) {
			Iterator<?> keys = object.keys();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				if (object.get(key) instanceof JSONObject) {
					value = findValue(object.getJSONObject(key), searchedKey);
				}
				/*
				 * if (object.get(key) instanceof JSONArray) { JSONArray jsonArray = (JSONArray) object.get(key); for
				 * (int i = 0; i < jsonArray.length(); i++) { JSONObject json = jsonArray.optJSONObject(i); if (json !=
				 * null) value = findValue(json, searchedKey); } }
				 */
			}
		}
		return value;
	}


}
