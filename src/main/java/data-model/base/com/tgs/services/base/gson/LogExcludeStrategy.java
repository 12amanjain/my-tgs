package com.tgs.services.base.gson;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.tgs.services.base.helper.LogExclude;

public class LogExcludeStrategy implements ExclusionStrategy {
	@Override
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(LogExclude.class) != null
				|| f.getDeclaredClass().getAnnotation(LogExclude.class) != null;
	}

	@Override
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}
}
