package com.tgs.services.base.gson;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.annotations.SerializedName;
import com.google.gson.internal.bind.TypeAdapters;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.tgs.services.base.datamodel.Mask;
import com.tgs.services.base.helper.MaskedField;

/**
 * Type-adapter factory for masking fields of any class type. During
 * serialization, it masks <i>primitive</i> fields annotated with
 * {@link MaskedField} using the information contained in the annotation.<br>
 * Masking is done by replacing alphanumerics by masking character. Output of
 * masking is always string and special characters are retained in masked
 * field.<br>
 * <br>
 * <i>Non-primitive</i> fields are ignored.<br>
 * <br>
 * Deserialization is unaffected (done by delegate adapter).<br>
 * <br>
 * For example,
 * 
 * <pre>
 * &#64JsonAdapter(MaskedFieldTypeAdapterFactory.class) 
 * public class User {
 * 	&#64MaskedField(unmaskedBeg = 1, unmaskedEnd = 4, maskingChar='#') 
 * 	private String mob = "984-567-1312"; 
 * }
 * </pre>
 * 
 * serializes to
 * 
 * <pre>
 * {"mob": "9##-###-1312"}
 * </pre>
 * 
 * @author Abhineet Kumar, Technogram Solutions
 *
 */
public class MaskedFieldTypeAdapterFactory implements TypeAdapterFactory {

	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		final TypeAdapter<T> delegateAdapter = gson.getDelegateAdapter(this, type);
		final Map<String, Mask> fieldMasks = new HashMap<>();

		for (Field field : type.getRawType().getDeclaredFields()) {
			field.setAccessible(true);
			MaskedField maskedFieldAnnot = field.getAnnotation(MaskedField.class);
			if (maskedFieldAnnot != null) {
				for (String fieldName : getFieldNames(field)) {
					Mask mask = Mask.get(maskedFieldAnnot);
					fieldMasks.put(fieldName, mask);
				}
			}
		}

		if (fieldMasks.isEmpty()) {
			return delegateAdapter;
		}

		return new TypeAdapter<T>() {

			@Override
			public void write(JsonWriter out, T value) throws IOException {
				if (value == null) {
					out.nullValue();
					return;
				}

				JsonElement jsonElement = delegateAdapter.toJsonTree(value);
				if (!jsonElement.isJsonObject()) {
					TypeAdapters.JSON_ELEMENT.write(out, jsonElement);
					return;
				}

				out.beginObject();
				for (Entry<String, JsonElement> entry : jsonElement.getAsJsonObject().entrySet()) {
					String fieldName = entry.getKey();
					Mask mask = fieldMasks.get(fieldName);
					JsonElement fieldVal = entry.getValue();
					out.name(fieldName);
					if (mask != null && fieldVal != null && fieldVal.isJsonPrimitive()
							&& fieldVal.getAsJsonPrimitive().isString()) {
						out.value(mask.mask(fieldVal.getAsString()));
						continue;
					}
					TypeAdapters.JSON_ELEMENT.write(out, fieldVal);
				}
				out.endObject();
			}

			@Override
			public T read(JsonReader in) throws IOException {
				return delegateAdapter.read(in);
			}
		};
	}

	/**
	 * 
	 * @param f
	 * @return All names that'll be possibly used for serialization/deserialization
	 *         of {@code Field f}}
	 */
	private List<String> getFieldNames(Field f) {
		SerializedName annotation = f.getAnnotation(SerializedName.class);
		if (annotation == null) {
			return Collections.singletonList(f.getName());
		}

		String serializedName = annotation.value();
		String[] alternates = annotation.alternate();
		if (alternates.length == 0) {
			return Collections.singletonList(serializedName);
		}

		List<String> fieldNames = new ArrayList<String>(alternates.length + 1);
		fieldNames.add(serializedName);
		for (String alternate : alternates) {
			fieldNames.add(alternate);
		}
		return fieldNames;
	}
}
