package com.tgs.services.base.helper;

import javax.validation.ConstraintValidatorContext;

public class BooleanTypeValidator extends NotSupportedValidator<Boolean> {

    @Override
    public boolean isValid(Boolean value, ConstraintValidatorContext context) {
        return value == null;
    }
}
