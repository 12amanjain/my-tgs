package com.tgs.services.base.helper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.tgs.services.base.enums.UserRole;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.FIELD, ElementType.TYPE})
public @interface DefaultReportField {
	public boolean midOfficeRole() default true;
	public boolean nonMidOfficeRole() default false;
	public UserRole[] includedRoles() default {};
	public UserRole[] excludedRoles() default {};
}