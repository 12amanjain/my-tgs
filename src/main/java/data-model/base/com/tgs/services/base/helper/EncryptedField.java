package com.tgs.services.base.helper;

import static java.lang.annotation.ElementType.FIELD;
import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This annotation conveys that the field has to be encrypted/decrypted by the transform.
 * 
 * @author Abhineet Kumar
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(FIELD)
public @interface EncryptedField {

}
