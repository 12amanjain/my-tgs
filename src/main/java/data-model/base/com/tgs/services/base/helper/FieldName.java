package com.tgs.services.base.helper;

import lombok.Getter;

@Getter
public enum FieldName {


	/*
	 * Serialized Names For HotelInfo Datamodel
	 */
	HOTEL_MISC_INFO("mi"),
	GIATA_ID("gid"),
	OPTIONS("ops"),
	ROOM_MISC_INFO("rmi"),
	PROCESSED_OPTIONS("pops"),
	HOTEL_PROMOTION_INFO("pr"),
	TRIP_ADVISOR_ID("taid"),
	SUPPLIER_SET("ss"),
	IS_PREFERRED_HOTEL("iph"),
	IS_CROSS_SELL_HOTEL("icsh"),
	HOTEL_TERMS_AND_CONDITIONS("tac"),
	HOTEL_IMAGES("img"),
	HOTEL_INSTRUCTIONS("inst"),
	HOTEL_DESCRIPTION("des"),
	HOTEL_LONG_DESCRIPTION("ldes"),
	HOTEL_RATING("rt"),
	HOTEL_GEOLOCATION("gl"),
	HOTEL_ADDRESS("ad"),
	HOTEL_FACILITIES("fl"),
	HOTEL_ID("id"),
	HOTEL_PROPERTY_TYPE("pt"),
	HOTEL_CONTACT("cnt"),
	HOTEL_WEBSITE("wb"),
	IS_PACKAGE_RATE("ispr"),

	/*
	 * Serialized Names For Images Datamodel
	 */
	HOTEL_THUMBNAILS("tns"),
	HOTEL_IMAGE_URL("url"),


	/*
	 * Serialized Names For Hotel Address Datamodel
	 */
	HOTEL_ADDRESS_LINE1("adr"),
	HOTEL_ADDRESS_LINE2("adr2"),

	/*
	 * Geolocation
	 */
	HOTEL_LONGITUDE("ln"),
	HOTEL_LATITUDE("lt"),

	/*
	 * Contact Info
	 */
	HOTEL_PHONE("ph"),
	HOTEL_EMAIL("em"),


	/*
	 * Serialized Names For HotelPromotion Datamodel
	 */
	IS_LOWEST_PRICE_ALLOWED("ilpa"),
	IS_FLEXIBLE_TIMING_ALLOWED("ifta"),
	// IS_PREFERRED_HOTEL("ph"),

	/*
	 * Serialized Names For Hotel MiscInfo
	 */
	SUPPLIER_STATIC_HOTEL_ID("sshid"),
	SEARCH_KEY_EXPIRY_TIME("set"),
	SUPPLIER_BOOKING_REFERENCE("sbid"),
	SUPPLIER_BOOKING_ID("sbr"),
	SUPPLIER_BOOKING_CONFIRMATION("sbc"),
	HOTEL_BOOKING_REFERENCE("hbr"),
	HOTEL_BOOKING_CANCELLATION_REFERENCE("bcr"),
	SUPPLIER_BOOKING_URL("sbu"),
	CREDIT_CARD_ID("cci"),

	/*
	 * Serialized Names For Processed Options
	 */

	OPTION_FACILITIES("fc"),
	PROCESSED_OPTION_TOTAL_PRICE("tpc"),
	PROCESSED_OPTION_DISCOUNTED_PRICE("dpc"),
	OPTION_PRICE("tp"),

	/*
	 * Serialized Names For Option
	 */

	ROOMINFO_LIST("ris"),
	OPTION_CANCELLATION_POLICY("cnp"),
	ROOM_CANCELLATION_POLICY_LIST("rcnp"),
	OPTION_DEADLINEDATETIME("ddt"),
	OPTION_MISC_INFO("omi"),
	IS_OPTION_ON_REQUEST("iopr"),
	ROOM_SSR_LIST("ssr"),

	/*
	 * Option Misc Info
	 */

	SUPPLIER_SEARCH_ID("ssid"),
	SECONDARY_SUPPLIER("ss"),
	SUPPLIER_HOTEL_ID("shid"),
	SUPPLIER_ID("sid"),
	SOURCE_ID("soid"),
	IS_MARKUP_UPDATED("imu"),
	OPTION_BOOKING_PRICE("obp"),
	IS_COMMISSION_APPLIED("ica"),
	IS_NOT_REQUIRED_ON_DETAIL("inrod"),

	/*
	 * Cancellation Policy
	 */
	IS_NO_REFUND_ALLOWED("inra"),
	IS_FULL_REFUND_ALLOWED("ifra"),
	PENALTY_DETAILS("pd"),
	CANCELLATION_POLICY_STRING("scnp"),
	CANCELLATION_POLICY_MISC_INFO("mi"),

	/*
	 * Cancellation Policy MiscInfo
	 */
	IS_BOOKING_ALLOWED("iba"),
	IS_SOLD_OUT("iso"),

	/*
	 * Penalty Details
	 */

	FROM_DATE("fdt"),
	TO_DATE("tdt"),
	PENALTY_AMOUNT("am"),
	PENALTY_PERCENT("pp"),
	PENALTY_ROOM_NIGHTS("rn"),
	CANCELLATION_TERMS_AND_CONDITIONS("tnc"),
	IS_CANCELLATION_RESTRICED("icr"),

	/*
	 * SSR
	 */
	REQUEST_MESSAGE("rm"),
	QUITE_ROOM_REQUIRED("qrr"),
	ARRIVAL_TIME("at"),


	/*
	 * Fetch Static Data
	 */
	HOTEL_CITY_INFO_LIST("cil"),
	HOTEL_INFO_LIST("hil");

	private String name;

	private FieldName(String name) {
		this.name = name;
	}


}
