package com.tgs.services.base.helper;

import javax.validation.ConstraintValidatorContext;
import java.time.LocalDateTime;

public class LocalDateTimeTypeValidator extends NotSupportedValidator<LocalDateTime> {
    @Override
    public boolean isValid(LocalDateTime value, ConstraintValidatorContext context) {
        return value == null;
    }
}
