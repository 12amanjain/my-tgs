package com.tgs.services.base.helper;

import javax.validation.ConstraintValidatorContext;

public class LongTypeValidator extends NotSupportedValidator<Long> {
   
	@Override
    public boolean isValid(Long value, ConstraintValidatorContext context) {
        return value == null;
    }
}

