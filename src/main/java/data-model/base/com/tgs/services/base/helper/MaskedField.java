package com.tgs.services.base.helper;

import static java.lang.annotation.ElementType.FIELD;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * It is used to annotate (only) the fields of class type using
 * serialization/deserialization for transformation. This annotation conveys
 * that the field has to be masked by the transform or the adapter (during
 * serialization).<br>
 * <br>
 * {@code unmaskedBeg} determines the length of field to remain unmasked, from
 * <i>beginning</i>. Negative number is treated as 0. Default value is 0.<br>
 * {@code unmaskedEnd} determines the length of field to remain unmasked, from
 * <i>end</i>. Negative number is treated as 0. Default value is 0.<br>
 * {@code maxMaskLength} determines the maximum length of mask. 0 and negative
 * number means complete mask length. Default value is 0.<br>
 * {@code maskingChar} determines the character used for masking. Default value
 * is '*'.
 * 
 * @author Abhineet Kumar, Technogram Solutions
 *
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(FIELD)
public @interface MaskedField {

	int unmaskedBeg() default 0;

	int unmaskedEnd() default 0;

	int maxMaskLength() default 0;

	char maskingChar() default '*';

	boolean maskSpecialCharacer() default true;
}
