package com.tgs.services.base.helper;

import javax.validation.ConstraintValidator;

public abstract class NotSupportedValidator<T> implements ConstraintValidator<ForbidInAPIRequest, T> {

    @Override
    public void initialize(ForbidInAPIRequest constraintAnnotation) {
    }

}
