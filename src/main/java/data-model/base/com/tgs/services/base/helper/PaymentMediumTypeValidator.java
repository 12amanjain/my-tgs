package com.tgs.services.base.helper;

import javax.validation.ConstraintValidatorContext;

import com.tgs.services.base.enums.PaymentMedium;

public class PaymentMediumTypeValidator extends NotSupportedValidator<PaymentMedium> {

    @Override
    public boolean isValid(PaymentMedium value, ConstraintValidatorContext context) {
        return value == null;
    }
}
