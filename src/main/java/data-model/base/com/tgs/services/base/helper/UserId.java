package com.tgs.services.base.helper;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface UserId {

	public boolean creditLine() default false;

	public boolean userRelation() default false;

	public boolean userBalance() default true;

	public boolean walletBalance() default false;

	public boolean userRole() default true;

	public boolean userPolicies() default false;

	public boolean userEmail() default false;

	public boolean userMobile() default false;
}

