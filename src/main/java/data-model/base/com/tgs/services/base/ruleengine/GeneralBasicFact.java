package com.tgs.services.base.ruleengine;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import lombok.*;
import lombok.experimental.SuperBuilder;
import java.time.LocalDateTime;
import java.util.List;


@Setter
@Getter
@SuperBuilder
@AllArgsConstructor
@NoArgsConstructor
public class GeneralBasicFact implements IFact {


	private UserRole role;
	private String userId;
	private String partnerId;
	private ChannelType channelType;

	@SerializedName("at")
	private LocalDateTime applicableTime;

	private List<PaymentMedium> paymentMediums;

	private String bankName;

	// number of pax
	private Integer paxCount;

	public GeneralBasicFact generateFact(UserRole userRole) {
		setRole(userRole);
		return this;
	}


}

