package com.tgs.services.base.ruleengine;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.List;

@Getter
@Setter
@ToString
public class GeneralBasicRuleCriteria extends BasicRuleCriteria {

	private List<UserRole> roles;
	private List<String> userIds;
	private List<String> partnerIds;

	@SerializedName("cT")
	private List<ChannelType> channelTypes;

	@SerializedName("timeP")
	private TimePeriod timePeriod;

	@SerializedName("bn")
	private List<String> bankNames;

	@SerializedName("pm")
	private List<PaymentMedium> paymentMediums;

	@SerializedName("pc")
	private Integer paxCount;
}
