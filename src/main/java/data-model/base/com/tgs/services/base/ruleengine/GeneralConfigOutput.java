package com.tgs.services.base.ruleengine;

import lombok.Getter;

@Getter
public class GeneralConfigOutput implements IRuleOutPut {

    private String value;
}
