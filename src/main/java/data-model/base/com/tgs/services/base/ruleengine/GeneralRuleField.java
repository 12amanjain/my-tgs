package com.tgs.services.base.ruleengine;

import java.time.LocalDateTime;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import com.tgs.services.base.datamodel.TimePeriod;

@Slf4j
public enum GeneralRuleField implements IRuleField {

	ROLES {
		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria rule) {
			return rule.getRoles().isEmpty() || rule.getRoles().contains(fact.getRole());
		}
	},
	USERIDS {

		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria rule) {
			return rule.getUserIds().isEmpty() || rule.getUserIds().contains(fact.getUserId());
		}
	},
	PARTNERIDS {

		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria rule) {
			return rule.getPartnerIds().isEmpty() || rule.getPartnerIds().contains(fact.getPartnerId());
		}
	},
	CHANNELTYPES {

		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria rule) {
			return CollectionUtils.isEmpty(rule.getChannelTypes())
					|| rule.getChannelTypes().contains(fact.getChannelType());
		}

	},
	TIMEPERIOD {
		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria rule) {

			LocalDateTime applicableTime =
					(fact == null || fact.getApplicableTime() == null) ? LocalDateTime.now() : fact.getApplicableTime();
			TimePeriod timePeriod = rule.getTimePeriod();
			if ((timePeriod.getStartTime().isEqual(applicableTime)
					|| timePeriod.getStartTime().isBefore(applicableTime))
					&& (timePeriod.getEndTime().isEqual(applicableTime)
							|| timePeriod.getEndTime().isAfter(applicableTime))) {
				return true;
			}
			return false;
		}
	},
	PAYMENTMEDIUMS {
		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getPaymentMediums())
					|| input.getPaymentMediums().containsAll(fact.getPaymentMediums());
		}
	},
	PAXCOUNT {
		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria input) {
			if (input.getPaxCount() == null) {
				return true;
			}
			if (fact.getPaxCount() == null) {
				return false;
			}
			if (input.getPaxCount() <= fact.getPaxCount()) {
				return true;
			}
			return false;
		}
	},
	BANKNAMES {
		@Override
		public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getBankNames()) || input.getBankNames().contains(fact.getBankName());
		}
	};

	public boolean isValidAgainstFact(GeneralBasicFact fact, GeneralBasicRuleCriteria rule) {
		return false;
	}

	@Override
	public boolean isValidAgainstFact(IFact fact, IRuleCriteria rule) {
		return this.isValidAgainstFact((GeneralBasicFact) fact, (GeneralBasicRuleCriteria) rule);
	}

}
