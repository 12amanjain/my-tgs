package com.tgs.services.base.ruleengine;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class RuleEngine {

	protected List<? extends IRule> rules;
	private IFact fact;

	public RuleEngine(List<? extends IRule> rules, IFact fact) {
		this.rules = rules;
		this.fact = fact;
	}

	public abstract List<? extends IRule> fireAllRules();
}
