package com.tgs.services.base.ruleengine;

import com.tgs.services.base.enums.AirType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;

@Slf4j
public enum UserFeeAirRuleField implements IRuleField {

	AIRTYPE {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return input.getAirType().equals(AirType.ALL) || input.getAirType().equals(fact.getAirType());
		}
	},
	USERIDS {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getUserIds()) || input.getUserIds().contains(fact.getUserId());
		}
	},
	AIRLINELIST {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getAirLineList()) || input.getAirLineList().contains("*")
					|| input.getAirLineList().contains(fact.getAirline());
		}
	},
	PAXTYPES {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return true;
		}
	},
	ROUTEINFOLIST {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return true;
		}
	},
	ISDOMESTIC {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return true;
		}
	},
	FARETYPELIST {
		@Override
		public boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria input) {
			return true;
		}
	};


	public abstract boolean isInputValidAgainstFact(UserFeeFact fact, UserAirFeeRuleCriteria rule);

	@Override
	public boolean isValidAgainstFact(IFact fact, IRuleCriteria rule) {
		return this.isInputValidAgainstFact((UserFeeFact) fact, (UserAirFeeRuleCriteria) rule);
	}

}
