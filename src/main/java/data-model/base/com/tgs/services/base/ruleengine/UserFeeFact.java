package com.tgs.services.base.ruleengine;

import com.tgs.services.base.enums.AirType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Builder
@ToString
public class UserFeeFact implements IFact{

	String airline;
	private AirType airType;
	String userId;

	public static UserFeeFact createFact() {
		return UserFeeFact.builder().build();
	}

	public static UserFeeFact getNonNull(UserFeeFact fact) {
		if (fact == null) {
			return createFact();
		}
		return fact;
	}

}
