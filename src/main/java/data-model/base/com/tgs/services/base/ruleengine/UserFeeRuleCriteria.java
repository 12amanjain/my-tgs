package com.tgs.services.base.ruleengine;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.Set;


@Getter
@Setter
public class UserFeeRuleCriteria implements IRuleCriteria, VoidClean {

	@SerializedName("pt")
	private List<String> paxTypes;

	@SerializedName("ul")
	private Set<String> userIds;


	@Override
	public boolean isVoid() {
		return paxTypes == null && userIds == null;
	}

	@Override
	public void cleanData() {
		setPaxTypes(TgsCollectionUtils.getCleanStringList(getPaxTypes()));
	}
}
