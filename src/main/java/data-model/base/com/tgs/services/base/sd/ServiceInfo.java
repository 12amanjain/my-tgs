package com.tgs.services.base.sd;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ServiceInfo {

	private String type;
	private String name;
	private int port;

}
