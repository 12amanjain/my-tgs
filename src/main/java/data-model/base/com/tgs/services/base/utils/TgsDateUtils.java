package com.tgs.services.base.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.Year;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang3.StringUtils;

public class TgsDateUtils {

	public static LocalDateTime calenderToLocalDateTime(Calendar date) {
		TimeZone timeZone = date.getTimeZone();
		ZoneId zoneId = timeZone.toZoneId();
		LocalDateTime localDateTime = LocalDateTime.ofInstant(Instant.ofEpochMilli(date.getTimeInMillis()), zoneId);
		return localDateTime;
	}

	public static LocalDateTime toLocalDateTime(Calendar calendar) {
		if (calendar == null) {
			return null;
		}
		TimeZone tz = calendar.getTimeZone();
		ZoneId zid = tz == null ? ZoneId.systemDefault() : tz.toZoneId();
		return LocalDateTime.ofInstant(calendar.toInstant(), zid);
	}

	public static LocalDate calenderToLocalDate(Calendar date) {
		if (date == null) {
			return null;
		}
		TimeZone timeZone = date.getTimeZone();
		ZoneId zoneId = timeZone.toZoneId();
		LocalDateTime localDateTime = LocalDateTime.ofInstant(date.toInstant(), zoneId);
		return localDateTime.toLocalDate();
	}

	public static Calendar getCalendar(LocalDateTime localDateTime) {
		if (localDateTime == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.set(localDateTime.getYear(), localDateTime.getMonthValue() - 1, localDateTime.getDayOfMonth());
		calendar.set(Calendar.HOUR_OF_DAY, localDateTime.getHour());
		calendar.set(Calendar.MINUTE, localDateTime.getMinute());
		calendar.set(Calendar.SECOND, localDateTime.getSecond());
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.setTimeInMillis(calendar.getTimeInMillis());
		return calendar;
	}

	public static Calendar localDateToCalendar(LocalDate localDate) {
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth());
		calendar.clear(Calendar.ZONE_OFFSET);
		return calendar;
	}

	/**
	 * Return Calendar without TimeZone
	 */
	public static Calendar getCalendar(LocalDate date) {
		Instant instant = date.atStartOfDay().atZone(TimeZone.getTimeZone("").toZoneId()).toInstant();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(""));
		calendar.setTime(Date.from(instant));
		return calendar;
	}

	/**
	 * Return Calendar without TimeZone
	 */
	public static Calendar getCalendarWithoutTimeZone(LocalDateTime date) {
		Instant instant = date.toLocalDate().atStartOfDay().atZone(TimeZone.getTimeZone("").toZoneId()).toInstant();
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeZone(TimeZone.getTimeZone(""));
		calendar.set(Calendar.HOUR_OF_DAY, date.getHour());
		calendar.set(Calendar.MINUTE, date.getMinute());
		calendar.setTime(Date.from(instant));
		return calendar;
	}

	public static Calendar localDateTimeToDate(LocalDateTime localDateTime) {
		Calendar calendar = Calendar.getInstance();
		calendar.clear();
		calendar.set(localDateTime.getYear(), localDateTime.getMonthValue() - 1, localDateTime.getDayOfMonth(),
				localDateTime.getHour(), localDateTime.getMinute(), localDateTime.getSecond());
		return calendar;
	}

	public static LocalDateTime convertDateToLocalDateTime(Date dateToConvert) {
		return dateToConvert.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
	}

	/**
	 * @param birthDate -> travvlerInfo dob
	 * @return difference from Now to Pax DOB (int)
	 */
	public static int getAgeFromBirthDate(LocalDate birthDate) {
		LocalDate currentDate = LocalDate.now();
		if (birthDate != null) {
			return Period.between(birthDate, currentDate).getYears();
		}
		return 0;
	}

	public static Date getDOBasDate(LocalDate dob) throws Exception {
		if (dob != null) {
			return new SimpleDateFormat("yyyy-MM-dd").parse(dob.toString());
			// return Date.from(dob.atStartOfDay(ZoneId.systemDefault()).toInstant());
		}
		return null;
	}

	public static Calendar getDefaultDOB() {
		LocalDate dateTime = LocalDate.of(0001, 01, 01);
		Calendar calendar = getCalendar(dateTime);
		calendar.clear(Calendar.ZONE_OFFSET);
		return calendar;
	}

	public static YearMonth getYearMonth(String dateText) {
		String[] date = dateText.split("/");
		Integer month = Integer.valueOf(date[0]);
		Year year = (date[1].length() == 4) ? Year.of(Integer.valueOf(date[1]))
				: Year.parse(date[1], DateTimeFormatter.ofPattern("yy"));
		YearMonth yearMonth = YearMonth.of(year.getValue(), month);
		return yearMonth;
	}

	public static LocalDate getCurrentDate() {
		return LocalDate.now();
	}

	// With out time.
	public static Calendar localDateToCalendarWithCurrentZone(LocalDate localDate) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(localDate.getYear(), localDate.getMonthValue() - 1, localDate.getDayOfMonth());
		return calendar;
	}

	public static TimeZone getTimeZoneOfCountry(String countryCode) {
		String[] timezones = com.ibm.icu.util.TimeZone.getAvailableIDs(countryCode);
		return TimeZone.getTimeZone(timezones[0]);
	}

	public static LocalDateTime convertStringToDate(String dateTime, String format) throws ParseException {

		if (StringUtils.isNotEmpty(dateTime) && StringUtils.isNotEmpty(format)) {
			SimpleDateFormat sdf = new SimpleDateFormat(format);
			Date date = sdf.parse(dateTime);
			return new Timestamp(date.getTime()).toLocalDateTime();
		}
		return null;

	}

	public static String convertDateToString(LocalDateTime localDateTime, String format) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
		return localDateTime.format(formatter);
	}
}
