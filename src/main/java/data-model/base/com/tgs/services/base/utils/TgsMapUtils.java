package com.tgs.services.base.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.datamodel.VoidClean;

public class TgsMapUtils {

	public static <K, V> V getOrDefault(Map<K, V> map, K key, V defaultValue) {
		if (map.get(key) == null) {
			map.put(key, defaultValue);
		}
		return map.get(key);
	}

	public static <K> Map<K, String> removeKeyIfEmpty(Map<K, String> map) {
		if (MapUtils.isEmpty(map)) {
			return null;
		}
		for (Iterator<K> iter = map.keySet().iterator(); iter.hasNext();) {
			K key = iter.next();
			if (StringUtils.isBlank(map.get(key))) {
				iter.remove();
			}
		}
		if (MapUtils.isEmpty(map)) {
			return null;
		}
		return map;
	}

	public static <K, V> boolean isEmpty(Map<K, V> map) {
		return isEmpty(map, t -> t == null);
	}

	/**
	 * 
	 * @param map
	 * @param isDirtyCheck
	 * @return {@code false} if there's any key for which the value is not dirty
	 *         (null/empty/blank), {@code true} if all keys are mapped to dirty
	 *         values.
	 */
	public static <K, V> boolean isEmpty(Map<K, V> map, Predicate<V> isDirtyCheck) {
		if (map == null || map.size() == 0)
			return true;

		if (isDirtyCheck == null) {
			isDirtyCheck = t -> t == null;
		}

		for (K key : map.keySet()) {
			if (!isDirtyCheck.test(map.get(key)))
				return false;
		}

		return true;
	}

	public static <K, V extends VoidClean> Map<K, V> getCleanMap(Map<K, V> map) {
		return getCleanMap(map, Cleanable.Cleaner::clean, t -> t.isVoid());
	}

	public static <K, V> Map<K, V> getCleanMap(Map<K, V> map, UnaryOperator<V> elementCleaner,
			Predicate<V> isDirtyCheck) {
		if (isDirtyCheck == null) {
			return map;
		}

		if (map != null) {
			cleanMap(map, elementCleaner, isDirtyCheck);
		}

		if (isEmpty(map)) {
			return null;
		}

		return map;
	}

	/**
	 * Clean elements and remove void elements.
	 * 
	 * @param map
	 * @param elementCleaner Must be able to handle {@code null}s if
	 *                       {@code isDirtyCheck} allows {@code null}. May be null
	 * @param isVoidCheck    non-null
	 */
	private static <K, V> void cleanMap(Map<K, V> map, UnaryOperator<V> elementCleaner, Predicate<V> isVoidCheck) {
		if (MapUtils.isEmpty(map)) {
			return;
		}
		Map<K, V> temp_map = new HashMap<>();
		for (K key : map.keySet()) {
			V value = map.get(key);
			if (elementCleaner != null) {
				value = elementCleaner.apply(value);
			}
			if (!isVoidCheck.test(value)) {
				temp_map.put(key, value);
			}
		}
		map.clear();
		map.putAll(temp_map);
	}

}
