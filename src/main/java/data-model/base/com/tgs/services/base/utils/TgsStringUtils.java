package com.tgs.services.base.utils;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Random;

import org.apache.commons.lang3.StringUtils;

import com.ibm.icu.text.NumberFormat;

public class TgsStringUtils {

	public static final String ALPHABET_CHARS = "abcdefghijklmnopqrstuvwxyz";
	public static final String ALPHABET_CHARS_WITH_SPACE = "abcdefghijklmnopqrstuvwxyz ";

	public static String generateOTP(int len) {
		Random random = new Random();
		StringBuffer temp = new StringBuffer();
		for (int i = 1; i <= len; i++) {
			temp.append(random.nextInt(10));
		}
		return temp.toString();
	}

	public static String generateRandomNumber(int len) {
		return generateRandomNumber(len, "");
	}

	public static String generateRandomNumber(int len, String prefix) {
		int prefixLength = String.valueOf(prefix).length();
		StringBuffer temp = new StringBuffer();
		temp.append(prefix);
		Random random = new Random();
		for (int i = 0; i < (len - prefixLength); i++) {
			temp.append(random.nextInt(10));
		}
		return temp.toString();
	}

	public static boolean containsOnlyAlphabet(String input) {
		return StringUtils.containsOnly(input.toLowerCase(), ALPHABET_CHARS);
	}

	public static boolean containsOnlyAlphabetAndSpace(String input) {
		return StringUtils.containsOnly(input.toLowerCase(), ALPHABET_CHARS_WITH_SPACE);
	}

	public static String formatCurrency(Double amount) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
		return formatter.format(amount);
	}

	public static String formatCurrency(BigDecimal amount) {
		NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
		return formatter.format(amount);
	}

	public static boolean matchesAnyIgnoreCase(String chars, String input) {
		return input.toLowerCase().matches(chars.toLowerCase());
	}

}
