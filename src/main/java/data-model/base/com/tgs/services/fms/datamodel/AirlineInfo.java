package com.tgs.services.fms.datamodel;

import com.tgs.services.base.helper.Exclude;

import com.tgs.services.base.helper.RestExclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class AirlineInfo {

	@ToString.Include
	private String code;
	private String name;
	private Boolean isLcc;

	@RestExclude
	@Exclude
	private Boolean isTkRequired;

	@RestExclude
	private String accountingCode;

}
