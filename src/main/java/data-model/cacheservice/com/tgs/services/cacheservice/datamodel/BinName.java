package com.tgs.services.cacheservice.datamodel;

import lombok.Getter;

@Getter
public enum BinName {
	/**
	 * length of binName should be less than 14 characters for aerospike
	 */
	USERID,
	ROLE,
	USERINFO,
	AT,
	SEARCHQUERYBIN,
	PRICEINFOBIN,
	AIRREVIEW,
	COMPRESS,
	PLAINDATA,
	APILOGS,
	STOREAT,
	BOOKINGID,
	PAYMENTS,
	ALLOWEDUSERID,
	CREDITLINE,
	CREDITBILL,
	NRCREDIT,
	SEATALLOCATION,
	RATEPLAN,
	AIRINVENTORY,
	SEARCHKEY,
	AIRINVENTORYID,
	SECTOR,
	DOCKEY,
	DOCTYPE,
	DOCDATA,
	USAGE,
	USERNAME,
	USERRELATION,
	USERBALANCE,
	USERPOINTS,
	USERPOINTSTYPE,
	SESSIONINFO,
	HOTELINFO,
	HOTELRESULTS,
	COMPLETEAT,
	HOTELDETAILS,
	HOTELRATING,
	SPLITCOUNT,
	SEARCHCOUNT,
	REVIEWID,
	COUNTRY,
	STATE,
	CITY,
	SUCCESSDEPOSIT,
	FARECOMPONENT,
	ROUTEINFO,
	RETRIEVEDBOOK,
	MEALBASIS,
	MONEYEXCHANGE,
	UPLOADRESPONSE,
	BEANNAME,
	APIKEY,
	RATING,
	DEVICEINFO,
	ID,
	COUNT,
	INFANTFARE,
	CANCELLATION,
	CONFIRMCANCEL,
	USERREVIEWID,
	UNMAPPEDUR,
	HOTELMANUAL,
	LPIDS,
	IATA,
	SIGNIN_ATTEMPT,
	HOTELMODIFY,
	UICONF,
	INVENTORYKEY,
	INVENTORYTIME,
	HOTELNAME,
	H_RATEPLAN,
	ROOMCATEGORY,
	SOURCEID,
	HOTEL_INFO,
	HOTEL_DETAIL;

	public String getName() {
		return this.name();
	}
}
