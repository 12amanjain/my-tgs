package com.tgs.services.cacheservice.datamodel;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.Map;
import com.aerospike.client.Value;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@Accessors(chain = true)
@ToString
public class CacheMetaInfo {

	private String key;
	private String namespace;
	private String set;
	private Boolean createOnly;
	private String keys[];
	private String bins[];
	private String values[];
	private Boolean compress;
	private Boolean plainData;
	private int expiration;
	private Type typeOfT;
	private Boolean cacheResult;
	private Boolean kyroCompress;
	private Integer index;
	private CacheType cacheType;
	private LocalDateTime expiryTime;
	private Map<String, Object> binValues;
}
