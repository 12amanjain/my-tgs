package com.tgs.services.cacheservice.datamodel;

public enum CacheNameSpace {
	STATIC_MAP, FLIGHT, GENERAL_PURPOSE, LOGS, USERS, HOTEL;

	public String getName() {
		return this.name().toLowerCase();
	}
}
