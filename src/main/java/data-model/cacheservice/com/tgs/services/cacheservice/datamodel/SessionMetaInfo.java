package com.tgs.services.cacheservice.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@Accessors(chain = true)
public class SessionMetaInfo extends CacheMetaInfo {

	/**
	 * Value will be stub
	 */
	private Object value;

}
