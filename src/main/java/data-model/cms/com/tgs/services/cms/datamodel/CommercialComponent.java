package com.tgs.services.cms.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.cms.datamodel.commission.CommercialType;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CommercialComponent {

	@SerializedName("cond")
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = AirCommercialConditions.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelCommercialConditions.class)})
	private BaseCommericialConditions condition;

	private CommercialType type;

	@NotNull
	@SerializedName("exp")
	private String expression;

	@SerializedName("ta")
	private Double thresholdAmount;
}
