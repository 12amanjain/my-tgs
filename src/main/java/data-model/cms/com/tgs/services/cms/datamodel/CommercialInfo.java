package com.tgs.services.cms.datamodel;

import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class CommercialInfo {

	private SupplierBasicInfo basicInfo;

	private String subProduct;

	private Boolean isUpdated;

	private Long ccInfoId;

	private Long commissionId;
}
