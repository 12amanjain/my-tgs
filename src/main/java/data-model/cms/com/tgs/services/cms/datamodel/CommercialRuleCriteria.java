package com.tgs.services.cms.datamodel;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommercialRuleCriteria extends BasicRuleCriteria implements IRuleOutPut {

	@NotNull
	private List<CommercialComponent> components;

}