package com.tgs.services.cms.datamodel.commission;


import lombok.Getter;

@Getter
public enum CommercialType {

    TOURCODE("TC"),
    IATA("IATA"),
    PLB("PLB"),
    SEGMENT_MONEY("SM"),
    CREDIT_CASHBACK("CC"),
    MANAGEMENT_FEE("MF"),
    ADDITIONAL_INCENTIVE("AI"),
    CUT_AND_PAY("CAP"),
    MARKUP("MU"),
    DISCOUNT("DS");


    private String code;

    CommercialType(String code) {
        this.code = code;
    }


    public String getName() {
        return this.code;
    }

    public static CommercialType getEnumFromCode(String code) {
        return getCommercialType(code);
    }

    private static CommercialType getCommercialType(String code) {
        for (CommercialType action : CommercialType.values()) {
            if (action.getCode().equals(code))
                return action;
        }
        return null;
    }

}
