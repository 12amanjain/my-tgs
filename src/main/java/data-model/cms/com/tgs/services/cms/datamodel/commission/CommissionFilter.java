package com.tgs.services.cms.datamodel.commission;

import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.enums.AirType;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Setter
@Getter
@ApiModel(value = "This is used to fetch commission based on filter. You can pass either any one field or combination of fields depending upon your search criteria")
public class CommissionFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on priority ", example = "2")
	private Integer priority;

	@ApiModelProperty(notes = "To fetch based on code (sub product) ", example = "true")
	/* In case of flight commission it should be airlineCode */
	private List<String> codes;

	@ApiModelProperty(notes = "To fetch based on sourceid ", example = "5")
	private String sourceId;

	@ApiModelProperty(notes = "To fetch based on supplier Id ", example = "IndiGoCorp")
	private String supplierId;

	@ApiModelProperty(notes = "To fetch based on rule id  ", example = "2")
	private List<Integer> ruleIds;

	@ApiModelProperty(notes = "To fetch based on commission plan id  ", example = "2")
	private Integer commPlanId;

	@ApiModelProperty(notes = "To fetch based on plan description ", example = "Maharastra Plan")
	private String description;

	@ApiModelProperty(notes = "To fetch based on Order Type  ", example = "DOMAIRBOOKING")
	private String product;

	@ApiModelProperty(notes = "To fetch based on AirType ", example = "DOMESTIC")
	private AirType airType;

}
