package com.tgs.services.cms.datamodel.commission;

import java.time.LocalDateTime;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CommissionPlanMapper {

    @ApiModelProperty(notes = "Which is already created Unique ID for Commission Plan mapper,if need to update id is mandatory", example = "24")
    private Long id;

    @ApiModelProperty(notes = "Creation Date of Commission Plan Mapper", example = "2018-09-27")
    private LocalDateTime createdOn;
    
    @ApiModelProperty(notes= "ProcessedOn Date of CommissionRule" ,example = "2018-09-27")
	private LocalDateTime processedOn;

    @ApiModelProperty(notes = "commission rule which is created to be mapped with plan", example = "-1")
    @NotNull
    private Long commissionId;

    @ApiModelProperty(notes = "Pre Defined Commission Plan", example = "DEFAULT")
    @NotNull
    private Long commPlanId;

}