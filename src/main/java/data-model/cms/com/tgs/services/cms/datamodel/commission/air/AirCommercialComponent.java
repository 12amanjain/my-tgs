package com.tgs.services.cms.datamodel.commission.air;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.cms.datamodel.AirCommercialConditions;
import lombok.Getter;

import javax.validation.constraints.NotNull;

@Getter
public class AirCommercialComponent {

    @SerializedName("cond")
    private AirCommercialConditions condition;

    @NotNull
    @SerializedName("exp")
    private String expression;

    @SerializedName("ta")
    private Double thresholdAmount;
}
