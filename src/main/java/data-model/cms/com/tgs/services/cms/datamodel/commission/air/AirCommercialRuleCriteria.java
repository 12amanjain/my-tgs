package com.tgs.services.cms.datamodel.commission.air;

import java.util.Map;

import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirCommercialRuleCriteria implements IRuleOutPut {

	Map<AirType, AirCommercialComponent> commission;
}
