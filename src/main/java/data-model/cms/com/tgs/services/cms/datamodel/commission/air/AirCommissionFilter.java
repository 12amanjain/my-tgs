package com.tgs.services.cms.datamodel.commission.air;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch Air Commission based on filter. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class AirCommissionFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on supplierId ", example = "IndiGoCorp")
	private String supplierId;

	@ApiModelProperty(notes = "To fetch based on sourceid ", example = "5")
	private String sourceId;

	@ApiModelProperty(notes = "To fetch based on airline ", example = "G8")
	private String airline;

	@ApiModelProperty(notes = "To fetch based on priority ", example = "2")
	private Integer priority;

	@ApiModelProperty(notes = "To fetch based on deleted rule ", example = "true")
	private boolean isDeleted;

}
