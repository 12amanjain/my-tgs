package com.tgs.services.cms.datamodel.commission.air;

import java.time.LocalDateTime;
import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder (toBuilder = true)
public class AirCommissionRule extends BulkUploadQuery implements IRule {

	@ApiModelProperty(notes = "Which is already created Unique ID for Commission Rule,if need to update id is mandatory", example = "24")
	private Long id;

	@ApiModelProperty(notes = "Creation Date of Commission Rule", example = "2018-09-27")
	private LocalDateTime createdOn;

	@ApiModelProperty(notes = "To denote rule is enabled or disabled", example = "TRUE")
	private Boolean enabled;

	@ApiModelProperty(notes = "ProcessedOn Date of AirCommissionRule")
	private LocalDateTime processedOn;

	@ApiModelProperty(notes = "To denote the priority the rule", example = "1")
	private double priority;

	@ApiModelProperty(notes = "To denote which airline commission applied & it can be comma seperated airlines", example = "6E,SG")
	private String airline;

	@ApiModelProperty(notes = "To denote which supplier IATA Commission rule applied", example = "IndiGo")
	private String supplierId;

	@ApiModelProperty(notes = "denote the inclusproduction criteria of rule", example = "")
	private FlightBasicRuleCriteria inclusionCriteria;

	@ApiModelProperty(notes = "denote the exclusion criteria of rule", example = "")
	private FlightBasicRuleCriteria exclusionCriteria;

	@ApiModelProperty(notes = "denote the commission criteria of rule", example = "")
	private AirCommercialRuleCriteria commissionCriteria;

	@Exclude
	private boolean isDeleted;

	private String description;

	@Override
	public IRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public IRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public IRuleOutPut getOutput() {
		return commissionCriteria;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	@Override
	public boolean exitOnMatch() {
		return false;
	}

	@Override
	public double getPriority() {
		return priority;
	}

}
