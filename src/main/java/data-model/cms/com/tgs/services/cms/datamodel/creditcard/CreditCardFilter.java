package com.tgs.services.cms.datamodel.creditcard;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@ApiModel(value = "This is used to fetch credit card based on filter. You can pass either any one field or combination of fields depending upon your search criteria")
public class CreditCardFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on supplierId ", example = "IndiGoCorp")
	private String supplierId;

	@ApiModelProperty(notes = "To fetch based on subProduct comma separeted values", example = "9W,AI")
	private String subProduct;

	@ApiModelProperty(notes = "To fetch based on cardType ", example = "G8")
	private String cardType;

	@ApiModelProperty(notes = "To fetch based on bankName ", example = "ABCDE")
	private String bankName;

	@ApiModelProperty(notes = "To fetch based on priority ", example = "2")
	private Integer priority;

	@ApiModelProperty(notes = "To fetch based on accountCode ", example = "2")
	private Integer accountCode;

	@ApiModelProperty(notes = "To fetch based on deleted credit card", example = "true")
	private boolean isDeleted;
	
	@ApiModelProperty(notes = "To fetch based on Product Type ", example = "H For Hotel, A For AIR")
	private String product;

}
