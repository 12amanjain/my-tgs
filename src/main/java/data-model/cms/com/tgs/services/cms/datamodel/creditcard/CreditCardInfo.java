package com.tgs.services.cms.datamodel.creditcard;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.tgs.services.base.datamodel.CardType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.MaskedField;
import com.tgs.services.base.helper.UserId;
import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ApiModel(value = "This is used to save or update credit card info")
@ToString(onlyExplicitlyIncluded = true)
public class CreditCardInfo {

	@ToString.Include
	private Long id;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	@NotNull
	@ToString.Include
	private Boolean enabled;

	@NotNull
	@ToString.Include
	private Product product;

	@NotNull
	@ToString.Include
	private String supplierId;

	@ToString.Include
	private String subProduct;

	@NotNull
	@ToString.Include
	private CardType cardType;

	private String bankName;

	@NotNull
	@MaskedField(unmaskedEnd = 2)
	private String cardNumber;

	@NotNull
	private String expiry;

	@NotNull
	@MaskedField
	private String cvv;

	private String holderName;

	private String street;

	private String city;

	private String state;

	private String country;

	private String pinCode;

	@NotNull
	private String accountCode;

	@ToString.Include
	private Double priority;

	@ToString.Include
	private boolean isDeleted;

	private Double minPassthru;

	@ToString.Include
	private CreditCardAdditionalInfo additionalInfo;

	@UserId
	@ToString.Include
	private String userId;
}
