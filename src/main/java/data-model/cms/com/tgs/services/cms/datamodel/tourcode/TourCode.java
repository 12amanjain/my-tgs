package com.tgs.services.cms.datamodel.tourcode;

import java.time.LocalDateTime;

import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.cms.datamodel.commission.air.AirCommercialComponent;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TourCode implements IRule {

	private Long id;

	private Boolean enabled;

	private AirType airType;

	private SearchType searchType;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	private String supplierId;

	private String sourceId;

	private String airline;

	private String tourCode;

	@Exclude
	private boolean isDeleted;

	private String description;

	private double priority;

	private FlightBasicRuleCriteria inclusionCriteria;

	private FlightBasicRuleCriteria exclusionCriteria;

	private AirCommercialComponent tourCodeCriteria;

	@Override
	@ApiModelProperty(hidden = true)
	public String getName() {
		return null;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	@Override
	public double getPriority() {
		return priority;
	}

	@Override
	public IRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public IRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public IRuleOutPut getOutput() {
		return null;
	}

	@Override
	public boolean exitOnMatch() {
		return true;
	}

	public FlightBasicRuleCriteria getInclusionRuleCriteria(FlightBasicRuleCriteria defaultCriteria) {
		return inclusionCriteria != null ? inclusionCriteria : defaultCriteria;
	}
}
