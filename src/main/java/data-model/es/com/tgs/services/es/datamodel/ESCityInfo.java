package com.tgs.services.es.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ESCityInfo {
	
	private String code;
	private String name;
	private String country_code;
	private String country_name;

}
