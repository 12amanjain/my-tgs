package com.tgs.services.es.datamodel;

import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.ScoreSortBuilder;
import org.elasticsearch.search.sort.SortOrder;

import com.tgs.services.base.gson.GsonUtils;

import lombok.Getter;

@Getter
public enum ESMetaInfo {

    ALL("_all", "*") {
    },

    AIRPORT("airportinfo", "airport") {
        @Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
            List<QueryBuilder> queryBuilder = new ArrayList<>();
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("name", source));
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("cityCode", source));
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("code", source).boost(3));
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("city", source).boost(1));
            return queryBuilder;
        }

        @Override
        public void addSortingFields(SearchSourceBuilder sourceBuilder) {
            sourceBuilder.sort(new FieldSortBuilder("priority").order(SortOrder.DESC));
            sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
        }

        @Override
        public QueryBuilder postFilter() {
            QueryBuilder builder = QueryBuilders.boolQuery();
            ((BoolQueryBuilder) builder).filter().add(QueryBuilders.matchQuery("enabled", true));
            return builder;
        }

        @Override
        public int documentSize() {
            return 15;
        }
    },

    AIRLINE("airlineinfo", "airline") {
        @Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
            List<QueryBuilder> queryBuilder = new ArrayList<>();
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("name", source));
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("code", source).boost(2));
            return queryBuilder;
        }
    },
    SUPPLIERLOG("supplierlog", "supplierlog") {
        @Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
            List<QueryBuilder> queryBuilder = new ArrayList<>();
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("key", source));
            return queryBuilder;
        }

        public int documentSize() {
            return 25;
        }
    },
    
    ORDER("orderinfo", "order") {
    	@Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
            List<QueryBuilder> queryBuilder = new ArrayList<>();
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("pnr", source));
            return queryBuilder;
        }
    	
    	public int documentSize() {
            return 25;
        }
    },
    
    HOTEL("hotelinfo", "hotel") {
        @Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
            List<QueryBuilder> queryBuilder = new ArrayList<>();
//            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("city", source).boost(2));
//            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("nationality", source));
            return queryBuilder;
        }
    },
    LOG("loginfo", "loginfo") {
        @Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
            List<QueryBuilder> queryBuilder = new ArrayList<>();
            queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("key", source));
            return queryBuilder;
        }

        public int documentSize() {
            return 25;
        }
    },
    AIR("airinfo", "airinfo"),
    AIR_FARE_TRACK_INFO("airfaretrackinfo", "airfaretrackinfo"),
    NO_SHOW("no_show","no_show"),
    USER_INFO("userinfo","userinfo"),
    HOTELINV("hotelinv", "hotelinv"){
    	@Override
        public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
    		
    		ESHotel esHotel = GsonUtils.getGson().fromJson(source, ESHotel.class);
    		List<QueryBuilder> queryBuilder = new ArrayList<>();
    		BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
    		String hotelName = StringUtils.isBlank(esHotel.getName()) ? "" : esHotel.getName().toLowerCase();
    		boolQueryBuilder.must(QueryBuilders.wildcardQuery("name", "*"+hotelName+"*"));
    		boolQueryBuilder.filter(QueryBuilders.matchPhrasePrefixQuery("countryName", esHotel.getCountryName()));
    		boolQueryBuilder.filter(QueryBuilders.matchPhrasePrefixQuery("cityName", esHotel.getCityName()));
    		queryBuilder.add(boolQueryBuilder);
    		return queryBuilder;
    	}
    	
    	/*
    	 * For now 1000 hotels, will use pagination
    	 */
    	public int documentSize() {
            return 1000;
        }
    },
    HOTEL_CITY_MAPPING("cities-info","cities-info");
	

    private String index;

    private String type;

    ESMetaInfo(String index, String type) {
        this.index = index;
        this.type = type;
    }

    public String getIndex() {
        return this.index;
    }

    public String getType() {
        return this.type;
    }

    public static ESMetaInfo getESMetaInfo(String esMetaInfo) {
        for (ESMetaInfo metaInfo : ESMetaInfo.values()) {
            if (esMetaInfo.equalsIgnoreCase(metaInfo.getType())) {
                return metaInfo;
            }
        }
        return ESMetaInfo.ALL;
    }

    /**
     * Returns List of QueryBuilder
     *
     * @param source an {@link String}
     * @return an {@link QueryBuilder<List>}
     * @implSpec search based on name and code
     */
    public List<QueryBuilder> getQuerybuilder(@NotNull String source) {
        List<QueryBuilder> queryBuilder = new ArrayList<>();
        queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("name", source));
        queryBuilder.add(QueryBuilders.matchPhrasePrefixQuery("code", source).boost(2));
        return queryBuilder;
    }

    public int documentSize() {
        return 5;
    }

    public void addSortingFields(SearchSourceBuilder sourceBuilder) {
        sourceBuilder.sort(new ScoreSortBuilder().order(SortOrder.DESC));
    }

    public QueryBuilder postFilter() {
        return null;
    }
}
