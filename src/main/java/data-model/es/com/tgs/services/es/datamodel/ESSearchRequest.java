package com.tgs.services.es.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ESSearchRequest {

	// search string in elasticsearch index
	private String source;

	// elasticsearch index
	private String metaInfo;
}
