package com.tgs.services.fms.analytics;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class AirCancellationAnalyticsQuery  extends AnalyticsAirQuery {
	
	private String amendmentid;
	
	private Double airlinecancellationfee;
	
	private int selectedsegments;
	
	private String notallowedsegments;
	
	private Boolean autocancellation;
	
	private String priceinfo;
	
	private Double totalrefundamount;

}
