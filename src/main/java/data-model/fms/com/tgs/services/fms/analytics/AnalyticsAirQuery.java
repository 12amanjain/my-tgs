package com.tgs.services.fms.analytics;

import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class AnalyticsAirQuery extends BaseAnalyticsQuery {

	private String source;
	private String destination;
	private String routeinfo;
	private String onwarddate;
	private String returndate;
	private Integer onwarddaydiff;
	private Integer returndaydiff;
	private String airtype;
	private String searchtype;

	protected String bookingid;
	private String faretype;
	private String cabinclass;
	private String airlines;
	private String flightnumbers;
	private String bookingclass;
	private String farebasis;
	private String refundabletype;

	private Double totalairlinefare;
	private Double commission;
	private String commissionid;

	private Integer adults;
	private Integer children;
	private Integer infants;

	protected String searchid;
	private String requestid;

	private String sourcename;
	private String suppliername;
	private String supplierdesc;

	private String errormsg;

	private Integer searchresultcount;
	protected Integer searchtimeinsec;

	private String suppliersessionid;
	private String alerts;

	private String baggages;
	private String meals;
	private String seats;
	private String fflier;
	private String alternateclass;

	private String paymentmedium;
	private String itemstatus;
	private String flowtype;
	private Integer attempt;

	private String orderassigneduserid;

	private Double totalbookingfare;
	private String bookedfaretype;
	private String searchCabinClass;
	private Double farediff;

}
