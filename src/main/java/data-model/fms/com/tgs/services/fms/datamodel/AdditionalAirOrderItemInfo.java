package com.tgs.services.fms.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.helper.Exclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdditionalAirOrderItemInfo {

	private Integer stops;
	@SerializedName("dt")
	private String departureTerminal;
	@SerializedName("at")
	private String arrivalTerminal;
	@SerializedName("oA")
	private String operatingAirline;
	@SerializedName("so")
	private List<String> stopOverAirport;

	@SerializedName("du")
	private long duration;
	@SerializedName("isRs")
	private Boolean isReturnSegment;

	@SerializedName("sN")
	private int segmentNo;

	@SerializedName("tL")
	private LocalDateTime timeLimit;

	@Exclude
	@SerializedName("cRId")
	private Long commercialRuleId;

	@Exclude
	@SerializedName(value = "srid", alternate = {"sid"})
	private Long supplierRuleId;

	@Exclude
	@SerializedName("tc")
	private String tourCode;

	@SerializedName("type")
	private String type;

	@SerializedName("ac")
	private String accountCode;

	@Exclude
	@SerializedName("ccid")
	private Integer ccId;

	@SerializedName("ua")
	private Double updatedAmount;

	@SerializedName("iata")
	private Double iata;

	@SerializedName("pc")
	private String platingCarrier;

	@SerializedName("eT")
	private String equipType;

	@SerializedName("osn")
	private String orgSupplierName;

	/**
	 * @implSpec : this is used for supplier end which is needed in segmentInfo
	 */
	@SerializedName("sKey")
	private String segmentReference;

	@SerializedName("fi")
	private String fareIdentifier;

	@Exclude
	@SerializedName("st")
	private SearchType searchType;

	@SerializedName("iId")
	private String inventoryId;

	@SerializedName("scid")
	private Integer sourceId;

	@SerializedName("msg")
	private List<MessageInfo> messages;

	@SerializedName("isEtkt")
	private Boolean isEticket;

	@SerializedName("ln")
	private Integer legNum;

	@SerializedName("prc")
	private String providerCode;

	@SerializedName("isPf")
	private Boolean isPrivateFare;

	@SerializedName("csp")
	private String creditShellPNR;

	@SerializedName("iucc")
	private Boolean isUserCreditCard;

}
