package com.tgs.services.fms.datamodel;

import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.BookingConditions;
import com.tgs.services.base.helper.AirAPIExcludeV1;
import com.tgs.services.fms.datamodel.airconfigurator.DobOutput;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirBookingConditions extends BookingConditions {

	@SerializedName("pcs")
	private AirPassportConditions passportConditions;

	@SerializedName("ffas")
	private Set<String> frequentFlierAirlines;

	@SerializedName("isa")
	private Boolean isSeatApplicable;

	@SerializedName("dob")
	private DobOutput dobOutput;

	@SerializedName("iar")
	private Boolean isAddressRequired;

	@AirAPIExcludeV1
	@SerializedName("ipr")
	private Boolean isPANRequired;

}
