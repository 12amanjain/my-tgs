package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.helper.Exclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.apache.commons.collections.CollectionUtils;

@Getter
@Setter
@Builder
@ToString
public class AirImportPnrBooking {

	private List<TripInfo> tripInfos;

	private GstInfo gstInfo;

	private DeliveryInfo deliveryInfo;

	private AirItemStatus itemStatus;

	private String pnr;

	private String supplierId;

	@Exclude
	private PNRCreditInfo pnrCreditInfo;

	public List<TripInfo> getTripInfos() {
		if (tripInfos == null) {
			tripInfos = new ArrayList<>();
		}
		return tripInfos;
	}

	public String getEmailId() {
		if (deliveryInfo != null && CollectionUtils.isNotEmpty(deliveryInfo.getEmails())) {
			return deliveryInfo.getEmails().get(0);
		}
		return null;
	}

	public String getContactNo() {
		if (deliveryInfo != null && CollectionUtils.isNotEmpty(deliveryInfo.getContacts())) {
			return deliveryInfo.getContacts().get(0);
		}
		return null;
	}
}
