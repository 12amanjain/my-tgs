package com.tgs.services.fms.datamodel;

import java.util.List;
import java.util.Map;

import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.RouteInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirMessageAttributes extends EmailAttributes {

	private String paxCount;

	private String bookingTime;

	private List<ProcessedFlightTravellerInfo> travellerList;

	private List<TripInfo> tripInfos;

	private Map<TripInfo, String> tripDepartureTime;

	private Map<SegmentInfo, String> segmentDepartureTime;

	private Map<SegmentInfo, String> segmentArrivalTime;

	private Boolean isDomesticRetOrMult;

	private List<RouteInfo> sortedRouteInfos;

	private List<String> journey;

	private Map<RouteInfo, List<TripInfo>> tripInfoGroups;

	private Map<TripInfo, AirMessagePriceInfo> priceInfos;

	private Map<TripInfo, Boolean> isOddTrip;

	private Map<SegmentInfo, Boolean> isLastSegmentOfTrip;

	private Map<TripInfo, String> baggageInfos;

	private AirMessagePriceInfo priceInfo;

	private List<ProcessedPriceInfo> priceList;

	private String cabinClass;

	private String bookingId;

	private String status;
	
	private String agentName;
	
	private String agentContact;
	
	private String agentAddress;
	
	private String agentLogo;
	
	private GstInfo gstInfo;
	
	private String tripId;

	@Builder.Default
	private boolean isWithPrice = true;
}
