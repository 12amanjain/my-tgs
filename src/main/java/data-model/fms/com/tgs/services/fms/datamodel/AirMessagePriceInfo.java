package com.tgs.services.fms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirMessagePriceInfo {

    private String baseFare;

    private String totalTax;

    private String ssrFare;

    private String totalAmount;

    private String amountPaid;

    private String fareType;
    
    private String discount;
    
    private String netFare;
}
