package com.tgs.services.fms.datamodel;

import com.tgs.services.base.helper.APIUserExclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class AirSearchModifiers {

	private Boolean isDirectFlight;

	private Boolean isConnectingFlight;

	private Integer sourceId;

	@APIUserExclude
	private Boolean storeSearchLog;
	
	@APIUserExclude
	private PNRCreditInfo pnrCreditInfo;
}
