package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSearchResult {
    private Map<String, List<TripInfo>> tripInfos;

    public Map<String, List<TripInfo>> getTripInfos() {
        if (tripInfos == null) {
            tripInfos = new HashMap<>();
        }
        return tripInfos;
    }

    public void addTripInfo(String key, TripInfo tripInfo) {
    	if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
    		List<TripInfo> trips = getTripInfos().getOrDefault(key, new ArrayList<>());
    		trips.add(tripInfo);
    		getTripInfos().put(key, trips);
    	}
    }

    public int getTotalTrips() {
        int totalTrips = 0;
        for (String key : tripInfos.keySet()) {
            totalTrips += tripInfos.get(key).size();
        }
        return totalTrips;
    }
}
