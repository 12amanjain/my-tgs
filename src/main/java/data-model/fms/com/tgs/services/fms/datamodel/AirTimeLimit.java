package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import com.google.gson.annotations.SerializedName;
import lombok.Setter;

@Getter
@Setter
public class AirTimeLimit {

	@SerializedName("altd")
	private List<AirlineTimeLimitData> airlineTimeLimits;

	public List<AirlineTimeLimitData> getAirlineTimeLimits() {
		if (airlineTimeLimits == null) {
			airlineTimeLimits = new ArrayList<>();
		}
		return airlineTimeLimits;
	}

}
