package com.tgs.services.fms.datamodel;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirlineTimeLimitData {

	@SerializedName("al")
	List<String> airlines;
	
	@SerializedName("tf")
	String timeFormat;
	
	@SerializedName("rp")
	String regexPattern;
}
