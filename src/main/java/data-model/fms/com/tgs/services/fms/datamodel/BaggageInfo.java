package com.tgs.services.fms.datamodel;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BaggageInfo implements Validatable {
	/**
	 * {@link #allowance} . Purpose of keeping this as string variable is use to
	 * cover all cases , like upto 20Kg , 20 Kg, 1 Piece , 1 Piece but max 20 kg.
	 */
	@SerializedName("iB")
	private String allowance;

	/**
	 * {@link #cabinBaggage} , this is used to identify how much cabin-baggage is
	 * allowed
	 */
	@SerializedName("cB")
	private String cabinBaggage;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (StringUtils.isBlank(cabinBaggage)) {
			errors.put("cabinBaggage", SystemError.INVALID_BAGGAGE_INFO.getErrorDetail());
		}

		return errors;
	}
}
