package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.Map;
import java.util.HashMap;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class BookingSegments {

	@ToString.Include
	List<SegmentInfo> segmentInfos;

	SupplierSession supplierSession;

	String supplierBookingId;

	@ToString.Include
	String airlinePNR;

	AirSearchQuery searchQuery;

	Map<String, MessageInfo> messsages;

	@ToString.Include
	List<SegmentInfo> cancelSegments;


	public List<SegmentInfo> getSegmentInfos() {
		if (segmentInfos == null) {
			segmentInfos = new ArrayList<>();
		}
		return segmentInfos;
	}

	public List<SegmentInfo> getCancelSegments() {
		if (cancelSegments == null) {
			cancelSegments = new ArrayList<>();
		}
		return cancelSegments;
	}

	public Map<String, MessageInfo> getMessages() {
		if (messsages == null) {
			messsages = new HashMap<>();
		}
		return messsages;
	}

	public Set<String> getBookingSegmentsPNR() {
		Set<String> pnr = new LinkedHashSet<>();
		for (SegmentInfo segmentInfo : getSegmentInfos()) {
			pnr.add(segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0).getPnr());
		}
		return pnr;
	}

	public Set<String> getBookingTktNumber() {
		Set<String> ticketNumbers = new LinkedHashSet<>();
		for (SegmentInfo segmentInfo : getSegmentInfos()) {
			ticketNumbers.add(segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0).getTicketNumber());
		}
		return ticketNumbers;
	}
}
