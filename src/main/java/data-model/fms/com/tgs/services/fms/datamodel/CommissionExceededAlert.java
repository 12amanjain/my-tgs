package com.tgs.services.fms.datamodel;

import com.tgs.services.base.datamodel.Alert;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CommissionExceededAlert implements Alert {

	private String type;
	private String message;
	
	@Override
	public String getType() {
		return type;
	}

}
