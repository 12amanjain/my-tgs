package com.tgs.services.fms.datamodel;

import java.util.HashMap;
import java.util.Map;
import com.tgs.services.base.enums.FareComponent;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FareComponentCacheData {

	private Map<String, Map<FareComponent, Double>> segmentFare;

	public Map<String, Map<FareComponent, Double>> getSegmentFare() {
		if (segmentFare == null) {
			segmentFare = new HashMap<String, Map<FareComponent, Double>>();
		}
		return segmentFare;
	}

}
