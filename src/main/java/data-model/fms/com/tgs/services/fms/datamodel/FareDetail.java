package com.tgs.services.fms.datamodel;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.base.helper.SystemError;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class FareDetail implements Validatable {

	/**
	 * {@link #fareComponents} map consist of fare component wise value.
	 */
	@SerializedName("fC")
	@ToString.Include
	private Map<FareComponent, Double> fareComponents;

	@SerializedName("afC")
	private Map<FareComponent, Map<FareComponent, Double>> addlFareComponents;

	/**
	 * {@link #seatRemaining}, this is helpful to identify how many seats are left on a particular fare
	 */
	@DBExclude
	@SerializedName("sR")
	private Integer seatRemaining;

	@SerializedName("bI")
	private BaggageInfo baggageInfo;

	/**
	 * {@link #isHandBaggage}, In case {@link #baggageInfo} is null , it is not necessary that baggage is not allowed in
	 * that fare. It could be because that {@link #baggageInfo} is not coming from API.
	 */
	@SerializedName("isHB")
	private Boolean isHandBaggage;
	/**
	 * {@link #fareType} , 0 == Non-Refundable , 1 == Refundable , 2 == Partial
	 */
	@SerializedName("rT")
	@ToString.Include
	private Integer refundableType;

	@RestExclude
	private String fareType;

	/**
	 * {@link #cabinClass} , this is to identify cabinclass of this fare. It is possible that customer has requested for
	 * economy class but due to non-availability of econonmy class , system has returned business class
	 */
	@SerializedName("cc")
	@ToString.Include
	private CabinClass cabinClass;

	/**
	 * {@link #classOfBooking} , this is the useful parameter for airline revenue team so that they can upload different
	 * pricing against different bookingClass
	 */

	@SerializedName("cB")
	@ToString.Include
	private String classOfBooking;

	/**
	 * {@link #fareBasis}, FareBasis is the further extension of {@link #classOfBooking} , one class of Booking can have
	 * multiple {@link #fareBasis}. That's why both {@link #classOfBooking} and {@link #fareBasis} needs to be set to an
	 * appropriate value
	 */
	@SerializedName("fB")
	@ToString.Include
	private String fareBasis;

	@SerializedName("mI")
	private Boolean isMealIncluded;

	@DBExclude
	@Exclude
	private Double paxPreviousTF;
	
	@DBExclude
	@Exclude
	private Double paxPreviousDS;
	
	@DBExclude
	@Exclude
	private Double paxCancellationFee;
	
	@DBExclude
	@Exclude
	private Double maxRefundableAmount;
	
	@DBExclude
	@Exclude
	private Double previousAmendmentFee;

	@DBExclude
	@Exclude
	private Map<FareComponent, Double> orgManualRefundableFC;

	public Map<FareComponent, Double> getFareComponents() {
		if (fareComponents == null) {
			fareComponents = new HashMap<>();
		}
		return fareComponents;
	}

	public double getComponentsSum(Set<FareComponent> components) {
		if (this.fareComponents == null) {
			return 0d;
		}
		return components.stream().mapToDouble(f -> fareComponents.getOrDefault(f, 0d)).sum();
	}

	public Map<FareComponent, Double> getSubSet(Set<FareComponent> components) {
		Map<FareComponent, Double> subSet = new HashMap<>();
		components.forEach(component -> subSet.put(component, fareComponents.getOrDefault(component, 0d)));
		return subSet;
	}

	public Map<FareComponent, Double> getAddlFareComponents(FareComponent fareComponent) {
		if (addlFareComponents == null) {
			addlFareComponents = new HashMap<>();
		}
		if (addlFareComponents.get(fareComponent) == null) {
			addlFareComponents.put(fareComponent, new HashMap<>());
		}

		return addlFareComponents.get(fareComponent);
	}

	@ApiModelProperty(hidden = true)
	public boolean hasNonZeroFare() {
		Map<FareComponent, Double> nonNullFareComponentMap = getFareComponents();
		return nonNullFareComponentMap.getOrDefault(FareComponent.BF, 0.0) != 0
				|| nonNullFareComponentMap.getOrDefault(FareComponent.AT, 0.0) != 0;
	}

	@ApiModelProperty(hidden = true)
	public BaggageInfo getBaggageInfo() {
		if (baggageInfo == null) {
			baggageInfo = new BaggageInfo();
		}
		return baggageInfo;
	}

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (cabinClass == null) {
			errors.put("cabinClass", SystemError.INVALID_CABIN_CLASS.getErrorDetail());
		}

		if (StringUtils.isBlank(classOfBooking)) {
			errors.put("classOfBooking", SystemError.INVALID_CABIN_CLASS.getErrorDetail());
		}

		if (refundableType == null) {
			errors.put("refundableType", SystemError.INVALID_REFUNDABLE_TYPE.getErrorDetail());
		}

		return errors;
	}

	@ApiModelProperty(hidden = true)
	public Map<FareComponent, Double> populateMappedFareComponent(FareBreakUpConfigOutput fbConfig) {
		Map<FareComponent, Double> modifiedMap = new HashMap<>();
		for (Map.Entry<FareComponent, Double> entry : this.getFareComponents().entrySet()) {
			FareComponent fc = entry.getKey();
			Double value = entry.getValue();
			FareComponent mapComponent = fc.mapComponent(fbConfig, null);
			if (fc != null && mapComponent != null) {
				modifiedMap.put(mapComponent, value + modifiedMap.getOrDefault(mapComponent, 0.0));
			}
			if (!getParentMapComponents(fbConfig).contains(fc)) {
				modifiedMap.put(fc, value);
			}
		}
		return modifiedMap;
	}

	public Double getAirlineFare() {
		Double totalFare = 0.0;
		for (Map.Entry<FareComponent, Double> entry : this.getFareComponents().entrySet()) {
			// Not total fare & commission Components
			if (entry.getKey().airlineComponent())
				totalFare += entry.getValue();
		}
		return totalFare;
	}

	public Map<FareComponent, Double> getMatchedFareComponents(List<FareComponent> fareComponentList) {
		Map<FareComponent, Double> fareComponentMap = new HashMap<>();
		for (FareComponent component : fareComponentList) {
			if (fareComponents.get(component) != null) {
				fareComponentMap.put(component, fareComponents.get(component));
			}
		}
		return fareComponentMap;
	}

	private static Set<FareComponent> getParentMapComponents(FareBreakUpConfigOutput fbConfig) {
		Set<FareComponent> fareComponentSet = new HashSet<>();
		Arrays.asList(FareComponent.values()).forEach(fc -> {

			if (fc.mapComponent(fbConfig, null) != null) {
				fareComponentSet.add(fc.mapComponent(fbConfig, null));
			}
		});
		return fareComponentSet;
	}

}
