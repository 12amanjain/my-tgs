package com.tgs.services.fms.datamodel;

import com.google.gson.annotations.SerializedName;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString(onlyExplicitlyIncluded = true,includeFieldNames = false)
public class FlightDesignator {
    @SerializedName("aI")
    @ToString.Include
    private AirlineInfo airlineInfo;
    @SerializedName("fN")
    @ToString.Include
    private String flightNumber;
    @SerializedName("eT")
    private String equipType;

    @ApiModelProperty
    public String getAirlineCode() {
        if (this.airlineInfo != null) {
            return airlineInfo.getCode();
        }
        return null;
    }

}
