package com.tgs.services.fms.datamodel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.BookingConditions;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.datamodel.TravellerInfoValidatingData;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.airconfigurator.DobOutput;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class FlightTravellerInfo extends TravellerInfo {

	@DBExclude
	private List<SSRInformation> ssrBaggageInfos;
	@DBExclude
	private List<SSRInformation> ssrMealInfos;
	@DBExclude
	private List<SSRInformation> ssrSeatInfos;
	@DBExclude
	private List<SSRInformation> ssrExtraServiceInfos;

	@ApiModelProperty(hidden = true)
	@SerializedName("sbi")
	private SSRInformation ssrBaggageInfo;
	@ApiModelProperty(hidden = true)
	@SerializedName("smi")
	private SSRInformation ssrMealInfo;
	@ApiModelProperty(hidden = true)
	@SerializedName("ssi")
	private SSRInformation ssrSeatInfo;
	@ApiModelProperty(hidden = true)
	@SerializedName("es")
	private List<SSRInformation> extraServices;

	@SerializedName("ost")
	private String otherServicesText;

	private String pnr;

	@SerializedName("tknum")
	private String ticketNumber;

	@SerializedName("sBId")
	private String supplierBookingId;

	@SerializedName("fd")
	private FareDetail fareDetail;

	@SerializedName("st")
	private TravellerStatus status;

	@Exclude
	private TravellerMiscInfo miscInfo;

	@Exclude
	private String reservationPNR;

	public List<SSRInformation> getSsrBaggageInfos() {
		if (ssrBaggageInfos == null) {
			ssrBaggageInfos = new ArrayList<>();
		}
		return ssrBaggageInfos;
	}

	public List<SSRInformation> getSsrMealInfos() {
		if (ssrMealInfos == null) {
			ssrMealInfos = new ArrayList<>();
		}
		return ssrMealInfos;
	}

	public List<SSRInformation> getSsrSeatInfos() {
		if (ssrSeatInfos == null) {
			ssrSeatInfos = new ArrayList<>();
		}
		return ssrSeatInfos;
	}

	public List<SSRInformation> getSsrExtraServiceInfos() {
		if (ssrExtraServiceInfos == null) {
			ssrExtraServiceInfos = new ArrayList<>();
		}
		return ssrExtraServiceInfos;
	}

	@Override
	public FieldErrorMap validate(TravellerInfoValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (validatingData instanceof FlightTravellerInfoValidatingData) {
			FlightTravellerInfoValidatingData flightValidatingData = (FlightTravellerInfoValidatingData) validatingData;
			boolean dobRequired = false;
			DobOutput dobOutput = null;
			LocalDate firstTravelDate = null;
			LocalDate lastTravelDate = null;
			NameLengthLimit nameLengthLimit = null;

			BookingConditions bookingConditions = null;
			if (flightValidatingData != null) {
				firstTravelDate = flightValidatingData.getFirstTravelDate();
				lastTravelDate = flightValidatingData.getLastTravelDate();
				dobOutput = flightValidatingData.getDobOutput();
				if (dobOutput == null) {
					dobOutput = DobOutput.builder().build();
				}
				dobRequired = flightValidatingData.isDobRequired();
				bookingConditions = flightValidatingData.getBookingConditions();
				nameLengthLimit = flightValidatingData.getNameLengthLimit();
			}

			if (nameLengthLimit != null) {

				String firstName = getFirstName();
				String lastName = getLastName();
				String fullName = StringUtils.join(firstName, lastName);

				nameLengthLimit.validateFirstNameLength((StringUtils.isNotEmpty(firstName)) ? firstName.length() : 0,
						errors);

				nameLengthLimit.validateLastNameLength((StringUtils.isNotEmpty(lastName)) ? lastName.length() : 0,
						errors);

				nameLengthLimit.validateFullNameLength((StringUtils.isNotEmpty(fullName)) ? fullName.length() : 0,
						errors);
			}

			if (getPaxType() != null) {
				// validate DOB
				switch (getPaxType()) {
					case ADULT:
						if ((dobRequired || dobOutput.isAdultDobRequired()) && dob == null) {
							errors.put("dob", SystemError.REQUIRED_ADULT_DOB.getErrorDetail());
						} else if (dob != null && (dob.plusYears(12).isAfter(firstTravelDate)
								|| dob.plusYears(100).isBefore(lastTravelDate))) {
							errors.put("dob", SystemError.INVALID_ADULT_DOB.getErrorDetail());
						}
						break;
					case CHILD:
						if ((dobRequired || dobOutput.isChildDobRequired()) && dob == null) {
							errors.put("dob", SystemError.REQUIRED_CHILD_DOB.getErrorDetail());
						} else if (dob != null && (dob.plusYears(2).isAfter(firstTravelDate)
								|| dob.plusYears(12).isBefore(lastTravelDate))) {
							errors.put("dob", SystemError.INVALID_CHILD_DOB.getErrorDetail());
						}
						break;
					case INFANT:
						if ((dobRequired || dobOutput.isInfantDobRequired()) && dob == null) {
							errors.put("dob", SystemError.REQUIRED_INFANT_DOB.getErrorDetail());
						} else if (dob != null
								&& (dob.isAfter(firstTravelDate) || dob.plusYears(2).isBefore(lastTravelDate))) {
							errors.put("dob", SystemError.INVALID_INFANT_DOB.getErrorDetail());
						}

						if (CollectionUtils.isNotEmpty(getSsrMealInfos())) {
							errors.put("ssrMealInfos", SystemError.INFANT_SSR_NA.getErrorDetail());
						}
						if (CollectionUtils.isNotEmpty(getSsrBaggageInfos())) {
							errors.put("ssrBaggageInfos", SystemError.INFANT_SSR_NA.getErrorDetail());
						}
						if (CollectionUtils.isNotEmpty(getSsrSeatInfos())) {
							errors.put("ssrSeatInfos", SystemError.INFANT_SSR_NA.getErrorDetail());
						}
						if (CollectionUtils.isNotEmpty(getSsrExtraServiceInfos())) {
							errors.put("ssrExtraServiceInfos", SystemError.INFANT_SSR_NA.getErrorDetail());
						}
						break;
				}
			}

			AirBookingConditions airBookingConditions = null;
			if (bookingConditions instanceof AirBookingConditions) {
				airBookingConditions = (AirBookingConditions) bookingConditions;
			}

			if (airBookingConditions != null) {
				AirPassportConditions passportConditions = airBookingConditions.getPassportConditions();
				if (passportConditions != null) {
					flightValidatingData.setDobRequired(
							passportConditions.isPassportMandatory() || StringUtils.isNotBlank(getPassportNumber()));
					validatePassport(errors, passportConditions, flightValidatingData.getLastTravelDate(),
							flightValidatingData.getFirstTravelDate());
				}
			}

			validatePAN(errors);
			errors.putAll(super.validate(flightValidatingData));

			int i = 0;
			if (CollectionUtils.isNotEmpty(ssrBaggageInfos)) {
				for (SSRInformation ssrInformation : ssrBaggageInfos) {
					errors.putAll(
							ssrInformation.validate(null).withPrefixToFieldNames("ssrBaggageInfos[" + i++ + "]."));
				}
			}

			if (CollectionUtils.isNotEmpty(ssrMealInfos)) {
				i = 0;
				for (SSRInformation ssrInformation : ssrMealInfos) {
					errors.putAll(ssrInformation.validate(null).withPrefixToFieldNames("ssrMealInfos[" + i++ + "]."));
				}
			}

			if (CollectionUtils.isNotEmpty(ssrSeatInfos)) {
				i = 0;
				for (SSRInformation ssrInformation : ssrSeatInfos) {
					errors.putAll(ssrInformation.validate(null).withPrefixToFieldNames("ssrSeatInfos[" + i++ + "]."));
				}
			}

			if (fareDetail != null) {
				FieldErrorMap error = fareDetail.validate(null);
				if (error != null) {
					errors.putAll(error.withPrefixToFieldNames("fareDetail"));
				}

				if (getPaxType() != null) {
					if (!PaxType.INFANT.equals(getPaxType())) {
						Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
						if (fareComponents.get(FareComponent.BF) == null
								|| fareComponents.get(FareComponent.AT) == null) {
							errors.put("fareDetail.fareComponents",
									SystemError.INVALID_FARE_COMPONENT.getErrorDetail());
						}
					}
				}
			}

		}
		return errors;
	}

	private void validatePAN(FieldErrorMap errors) {
		if (StringUtils.isNotEmpty(getPanNumber())) {
			if (getPanNumber().length() != 10 || !getPanNumber().toUpperCase().matches("[A-Z]{5}[0-9]{4}[A-Z]{1}")) {
				errors.put("panNumber", SystemError.INVALID_PAN_NUMBER.getErrorDetail());
			}
		}

	}

	private void validatePassport(FieldErrorMap errors, AirPassportConditions passportConditions,
			LocalDate lastTravelDate, LocalDate firstTravelDate) {

		if ((passportConditions.isPassportMandatory() || StringUtils.isNotBlank(getPassportNumber()))) {
			validatePassportNationality(errors);
		}

		if (passportConditions.isPassportMandatory()) {
			validatePassportNumber(errors);
		}

		if (passportConditions.isPassportExpiryDate()
				&& (passportConditions.isPassportMandatory() || StringUtils.isNotBlank(getPassportNumber()))) {
			validatePassportExpiry(errors, lastTravelDate);
		}

		if (passportConditions.isPassportIssueDate()
				&& (passportConditions.isPassportMandatory() || StringUtils.isNotBlank(getPassportNumber()))) {
			vaidatePassportIssueDate(errors, lastTravelDate);
		}

	}

	private void validatePassportNationality(FieldErrorMap errors) {
		if (StringUtils.isBlank(getPassportNationality())) {
			errors.put("passportNationality", SystemError.EMPTY_PASSPORT_NATIONALITY.getErrorDetail());
		}
	}

	private void validatePassportNumber(FieldErrorMap errors) {
		if (StringUtils.isBlank(getPassportNumber())) {
			errors.put("passportNumber", SystemError.EMPTY_PASSPORT_NUMBER.getErrorDetail());
		}
	}

	private void validatePassportExpiry(FieldErrorMap errors, LocalDate lastTravelDate) {
		if (getExpiryDate() == null) {
			errors.put("expiryDate", SystemError.EMPTY_PASSPORT_EXPIRY_DATE.getErrorDetail());
		} else if (lastTravelDate.plusMonths(6).isAfter(getExpiryDate())) {
			errors.put("expiryDate", SystemError.PASSPORT_EXPIRY.getErrorDetail());
		}
	}

	private void vaidatePassportIssueDate(FieldErrorMap errors, LocalDate firstTravelDate) {
		if (getPassportIssueDate() == null) {
			errors.put("passportIssueDate", SystemError.EMPTY_PASSPORT_ISSUE_DATE.getErrorDetail());
		} else if (firstTravelDate.isBefore(getPassportIssueDate())) {
			errors.put("passportIssueDate", SystemError.PASSPORT_LATE_ISSUE.getErrorDetail());
		}
	}

	public void unsetDetails() {
		this.getFareDetail().setBaggageInfo(null);
		this.setFrequentFlierMap(null);
		this.setPassportIssueDate(null);
		this.setPassportNationality(null);
		this.setPassportNumber(null);
	}

	public String pnrKey() {
		return ObjectUtils.defaultIfNull(this.getPnr(), "").trim() + "-" + this.getId();
	}

}
