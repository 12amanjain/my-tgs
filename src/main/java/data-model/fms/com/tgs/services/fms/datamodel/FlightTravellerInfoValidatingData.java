package com.tgs.services.fms.datamodel;

import java.time.LocalDate;

import com.tgs.services.base.datamodel.BookingConditions;
import com.tgs.services.base.datamodel.TravellerInfoValidatingData;
import com.tgs.services.fms.datamodel.airconfigurator.DobOutput;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class FlightTravellerInfoValidatingData extends TravellerInfoValidatingData {

	private LocalDate firstTravelDate;
	private LocalDate lastTravelDate;
	@Builder.Default
	private boolean isSpaceAllowedInLastName = true;

	@Builder.Default
	private boolean isSpaceAllowedInFirstName = true;

	private BookingConditions bookingConditions;
	
	@Setter
	private boolean dobRequired;
	private NameLengthLimit nameLengthLimit;
	private DobOutput dobOutput;

}
