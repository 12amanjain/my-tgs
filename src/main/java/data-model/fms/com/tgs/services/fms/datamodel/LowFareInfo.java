package com.tgs.services.fms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LowFareInfo {
	private double fare;
	private String code;
}
