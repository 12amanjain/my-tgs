package com.tgs.services.fms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NonOperatingSectorInfo {

	private String source;
	
	private String destination;
	
	private Integer sourceId;
	
}
