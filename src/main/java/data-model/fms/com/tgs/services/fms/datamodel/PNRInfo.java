package com.tgs.services.fms.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PNRInfo {

	List<String> pnr;

	@SerializedName("st")
	String status;

	@SerializedName("tkey")
	String tripKey;

	@SerializedName("ua")
	Boolean unHoldAllowed;

}
