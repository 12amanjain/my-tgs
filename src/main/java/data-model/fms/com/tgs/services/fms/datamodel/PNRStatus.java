package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PNRStatus {

	List<PNRInfo> pnrInfo;

	public List<PNRInfo> getPnrInfo() {
		if (pnrInfo == null) {
			pnrInfo = new ArrayList<>();
		}
		return pnrInfo;
	}
}
