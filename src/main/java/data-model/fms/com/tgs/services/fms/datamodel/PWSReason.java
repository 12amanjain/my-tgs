package com.tgs.services.fms.datamodel;

import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PWSReason implements IRuleOutPut {

	private Short id;

	private String text;

	private Short holdTime;
}
