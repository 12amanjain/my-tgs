package com.tgs.services.fms.datamodel;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PathEdge implements VoidClean{
	private String src;
	private String dest;
	private List<Integer> sourceId;
	private List<String> prefferedAirline;
	
	@Override
	public void cleanData() {
		setSrc(StringUtils.defaultIfBlank(src, null));
		setDest(StringUtils.defaultIfBlank(dest, null));
		setSourceId(TgsCollectionUtils.getNonNullElements(getSourceId()));
		setPrefferedAirline(TgsCollectionUtils.getCleanStringList(getPrefferedAirline()));
	}

	@Override
	public boolean isVoid() {
		return src==null && dest==null && CollectionUtils.isEmpty(sourceId) && prefferedAirline==null;
	}
}
