package com.tgs.services.fms.datamodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.Builder;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.MessageType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
@Getter
@Setter
public class PriceInfo {

	/**
	 * This consists of paxWise : Fare Information, Baggage Information, Remaining seat,booking class , farebasis,
	 * refundable/non-refundable.
	 */
	@SerializedName("fd")
	@ToString.Include
	private Map<PaxType, FareDetail> fareDetails;

	/**
	 * It will be set post search, while parsing response from source , there is no need to populate this object. It is
	 * primarily used for UI
	 */
	private FareDetail totalFareDetail;

	@SerializedName("sb")
	@ToString.Include
	@RestExclude
	private SupplierBasicInfo supplierBasicInfo;

	@ToString.Include
	private FareType fareIdentifier;

	private String id;

	/**
	 * It will be useful to identify the pair of special return flight.
	 */
	@SerializedName("sri")
	private String specialReturnIdentifier;

	/**
	 * It will maintain the specialReturnIdentifier of matching special return fight, if any.
	 */
	@SerializedName("msri")
	private List<String> matchedSpecialReturnIdentifier;

	/**
	 * This is required to display special messages related to segment
	 */
	private List<MessageInfo> messages;

	@SerializedName("pc")
	private AirlineInfo platingCarrier;

	@APIUserExclude
	@SerializedName("mcc")
	private Boolean mixCabinClass;

	@ApiModelProperty(hidden = true)
	public List<String> getMatchedSpecialReturnIdentifier() {
		if (matchedSpecialReturnIdentifier == null) {
			matchedSpecialReturnIdentifier = new ArrayList<>();
		}
		return matchedSpecialReturnIdentifier;
	}

	@ApiModelProperty(hidden = true)
	public Map<PaxType, FareDetail> getFareDetails() {
		if (fareDetails == null) {
			fareDetails = new HashMap<>();
		}
		return fareDetails;
	}

	@ToString.Include
	@RestExclude
	private PriceMiscInfo miscInfo;

	@ApiModelProperty(hidden = true)
	public FareDetail getFareDetail(String type) {
		return getFareDetail(PaxType.getPaxType(type), null);
	}

	@ApiModelProperty(hidden = true)
	public FareDetail getFareDetail(String type, FareDetail defaultValue) {
		return getFareDetail(PaxType.getPaxType(type), defaultValue);
	}

	@ApiModelProperty(hidden = true)
	public FareDetail getFareDetail(PaxType paxType) {
		return getFareDetail(paxType, null);
	}

	@ApiModelProperty(hidden = true)
	public FareDetail getFareDetail(PaxType paxType, FareDetail defaultValue) {
		if (getFareDetails().get(paxType) == null && defaultValue != null) {
			getFareDetails().put(paxType, defaultValue);
			return defaultValue;
		}
		return getFareDetails().get(paxType);
	}
	
	//public PriceInfo(SupplierBasicInfo supplierBasicInfo) {
	//	this.supplierBasicInfo = supplierBasicInfo;
	//}

	public PriceMiscInfo getMiscInfo() {
		if (miscInfo == null) {
			miscInfo = PriceMiscInfo.builder().build();
		}
		return miscInfo;
	}

	@ApiModelProperty(hidden = true)
	public Integer getSourceId() {
		return this.getSupplierBasicInfo().getSourceId();
	}

	@ApiModelProperty(hidden = true)
	public String getSupplierId() {
		return this.getSupplierBasicInfo().getSupplierId();
	}

	@ApiModelProperty(hidden = true)
	public String getBookingClass(PaxType paxType) {
		// this.getFareDetails can be empty when fare detail in boookingRelated info on
		// segment
		if (MapUtils.isNotEmpty(this.getFareDetails())
				&& MapUtils.getObject(this.getFareDetails(), paxType, null) != null)
			return this.getFareDetail(paxType).getClassOfBooking();
		else if (MapUtils.getObject(this.getFareDetails(), PaxType.ADULT, null) != null) {
			return this.getFareDetail(PaxType.ADULT).getClassOfBooking();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getFareBasis(PaxType paxType) {
		// this.getFareDetails can be empty when fare detail in boookingRelated info on
		// segment
		if (MapUtils.isNotEmpty(this.getFareDetails())
				&& MapUtils.getObject(this.getFareDetails(), paxType, null) != null)
			return this.getFareDetail(paxType).getFareBasis();
		else if (MapUtils.getObject(this.getFareDetails(), PaxType.ADULT, null) != null) {
			return this.getFareDetail(PaxType.ADULT).getFareBasis();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public CabinClass getCabinClass(PaxType paxType) {
		// this.getFareDetails can be empty when fare detail in boookingRelated info on
		// segment
		if (paxType != null && MapUtils.isNotEmpty(this.getFareDetails())
				&& MapUtils.getObject(this.getFareDetails(), paxType, null) != null)
			return this.getFareDetail(paxType).getCabinClass();
		else if (MapUtils.getObject(this.getFareDetails(), PaxType.ADULT, null) != null) {
			return this.getFareDetail(PaxType.ADULT).getCabinClass();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public double getTotalAirlineFare() {
		AtomicDouble totalFare = new AtomicDouble(0.0);
		this.getFareDetails().forEach(((paxType, fareDetail) -> {
			totalFare.getAndAdd(fareDetail.getAirlineFare());
		}));
		return totalFare.doubleValue();
	}

	@ApiModelProperty(hidden = true)
	public Integer getTotalPaxCountOnPaxTypeWise(boolean isInfantRequired) {
		AtomicInteger count = new AtomicInteger(0);
		this.getFareDetails().forEach(((paxType, fareDetail) -> {
			if (!paxType.equals(PaxType.INFANT)) {
				count.getAndIncrement();
			} else if (paxType.equals(PaxType.INFANT) && isInfantRequired) {
				count.getAndIncrement();
			}
		}));
		return count.intValue();
	}

	@ApiModelProperty(hidden = true)
	public boolean isPrivateFare() {
		boolean isPrivateFare = false;
		if (this.getMiscInfo() != null && BooleanUtils.isTrue(this.getMiscInfo().getIsPrivateFare())) {
			isPrivateFare = true;
		}
		return isPrivateFare;
	}

	@ApiModelProperty(hidden = true)
	public String getFareType() {
		if (this.getFareIdentifier() != null) {
			return this.getFareIdentifier().getName();
		}
		return FareType.PUBLISHED.getName();
	}

	@ApiModelProperty(hidden = true)
	public String getAccountCode() {
		PriceMiscInfo miscInfo = this.getMiscInfo();
		if (miscInfo != null && StringUtils.isNotEmpty(miscInfo.getAccountCode())) {
			return miscInfo.getAccountCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getPlattingCarrier() {
		if (getMiscInfo().getPlatingCarrier() != null
				&& StringUtils.isNotEmpty(getMiscInfo().getPlatingCarrier().getCode())) {
			return getMiscInfo().getPlatingCarrier().getCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public List<MessageInfo> getMessages() {
		if (messages == null) {
			messages = new ArrayList<>();
		}
		return messages;
	}

	@ApiModelProperty(hidden = true)
	public void setMessageInfo(MessageType type, String message) {
		this.getMessages().add(MessageInfo.builder().type(type).message(message).build());
	}

	@ApiModelProperty(hidden = true)
	public double getTotalFare(Map<PaxType, Integer> paxTypeMap, FareComponent targetComponent) {
		double totalPrice = 0;
		for (PaxType paxType : paxTypeMap.keySet()) {
			if (getFareDetail(paxType) != null) {
				totalPrice += getFareDetail(paxType).getFareComponents().getOrDefault(targetComponent,
						Objects.nonNull(getFareDetail(paxType).getAddlFareComponents()) ? getFareDetail(paxType)
								.getAddlFareComponents().getOrDefault(FareComponent.TAF, new HashMap<>())
								.getOrDefault(targetComponent, 0.0) : 0.0)
						* paxTypeMap.get(paxType);
			}
		}
		return totalPrice;
	}

}
