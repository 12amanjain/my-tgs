package com.tgs.services.fms.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString(onlyExplicitlyIncluded = true)
public class PriceMiscInfo {

	/**
	 * This consists of Fare Key
	 */
	@APIUserExclude
	private String fareKey;

	@SerializedName("ac")
	@ToString.Include
	private String accountCode;

	@ToString.Include
	private Boolean isPrivateFare;

	@SerializedName("tc")
	@ToString.Include
	private String tourCode;

	/*
	 * To Identify Which Commission Applied EX: Refer CMS
	 */
	@SerializedName("cRId")
	@ToString.Include
	@APIUserExclude
	private Long commericialRuleId;

	@SerializedName("ccid")
	@APIUserExclude
	private Integer ccInfoId;

	@SerializedName("sBId")
	private String supplierBookingId;

	@SerializedName("tL")
	private LocalDateTime timeLimit;

	@SerializedName("iata")
	@APIUserExclude
	private Double iata;

	@SerializedName("pc")
	@ToString.Include
	private AirlineInfo platingCarrier;

	/**
	 * @implSpec : this key is used after booking also its required for supplier end 1. API used:
	 *           confirmFareBeforeTicket 2. Supplier : Amadeus
	 */
	@APIUserExclude
	private String segmentKey;

	@SerializedName("lfid")
	@APIUserExclude
	private Long logicalFlightId;

	private LocalDateTime reviewTimeLimit;

	@APIUserExclude
	private String sessionId;

	@APIUserExclude
	private String mealWayType, baggageWayType, weight;

	@APIUserExclude
	private String traceId;

	// This consists of Journey Sell Key Which Represents Journey Info
	@APIUserExclude
	private String journeyKey;

	/**
	 * This is used for Ifly to identify the fare
	 */
	@APIUserExclude
	private String fareLevel;

	@APIUserExclude
	private String tokenId;

	private String marriageGrp;

	@ForbidInAPIRequest
	@APIUserExclude
	private String inventoryId;

	@SerializedName("cs")
	private String classOfService;

	@SerializedName("fcs")
	private String fareClassOfService;

	@SerializedName("fs")
	private String fareSequence;

	@SerializedName("rn")
	private String ruleNumber;

	@SerializedName("fna")
	private String fareApplicationName;

	@SerializedName("frid")
	@APIUserExclude
	private Long fareRuleId;

	@SerializedName("fTid")
	@DBExclude
	@APIUserExclude
	private Integer fareTypeId;

	// used for supplier to confirm isEticket(confirm book::ticket number) eligible or not (Ex : MYSTIFLY,TBO)
	@SerializedName("isEtkt")
	@DBExclude
	private Boolean isEticket;

	@ToString.Include
	private Integer legNum;

	@APIUserExclude
	private String providerCode;

	@APIUserExclude
	private String effectiveDate;

	@APIUserExclude
	private String availablitySource;


	@APIUserExclude
	private String participationLevel;

	@APIUserExclude
	private Boolean linkavailablity;

	@APIUserExclude
	private String polledAvailabilityOption;

	@APIUserExclude
	private String availabilityDisplayType;

	@APIUserExclude
	private Map<PaxType, List<String>> paxPricingInfo;

	@APIUserExclude
	private String fareRuleInfoRef;

	@APIUserExclude
	private String fareRuleInfoValue;


	private String creditShellPNR;

	@APIUserExclude
	private Boolean isUserCreditCard;

}
