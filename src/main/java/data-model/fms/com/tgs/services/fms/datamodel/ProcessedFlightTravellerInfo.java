package com.tgs.services.fms.datamodel;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessedFlightTravellerInfo extends TravellerInfo {
	@DBExclude
	private Map<String, SSRInformation> ssrBaggageInfos;
	@DBExclude
	private Map<String, SSRInformation> ssrMealInfos;
	@DBExclude
	private Map<String, SSRInformation> ssrSeatInfos;
	@DBExclude
	private Map<String, List<SSRInformation>> ssrExtraServiceInfos;
	@DBExclude
	private Map<String, String> miscSsr;
	@DBExclude
	private Map<String, String> nbIdMap;
	@DBExclude
	private Map<String, String> obIdMap;
	@DBExclude
	private Map<String, TravellerStatus> statusMap;
	@DBExclude
	private Map<String, String> pnrDetails;
	@DBExclude
	private Map<String, String> ticketNumberDetails;
	@DBExclude
	private Map<String, String> supplierBookingIds;
	@DBExclude
	private Map<String, String> invoiceIds;

	public Map<String, String> getInvoiceIds() {
		if (invoiceIds == null) {
			invoiceIds = new LinkedHashMap<>();
		}
		return invoiceIds;
	}

	public Map<String, SSRInformation> getSsrBaggageInfos() {
		if (ssrBaggageInfos == null) {
			ssrBaggageInfos = new LinkedHashMap<>();
		}
		return ssrBaggageInfos;
	}

	public Map<String, SSRInformation> getSsrMealInfos() {
		if (ssrMealInfos == null) {
			ssrMealInfos = new LinkedHashMap<>();
		}
		return ssrMealInfos;
	}

	public Map<String, SSRInformation> getSsrSeatInfos() {
		if (ssrSeatInfos == null) {
			ssrSeatInfos = new LinkedHashMap<>();
		}
		return ssrSeatInfos;
	}

	public Map<String, List<SSRInformation>> getSsrExtraServiceInfos() {
		if (ssrExtraServiceInfos == null) {
			ssrExtraServiceInfos = new LinkedHashMap<>();
		}
		return ssrExtraServiceInfos;
	}

	public Map<String, String> getPnrDetails() {
		if (pnrDetails == null) {
			pnrDetails = new LinkedHashMap<>();
		}
		return pnrDetails;
	}

	public Map<String, String> getMiscSsr() {
		if (miscSsr == null) {
			miscSsr = new LinkedHashMap<>();
		}
		return miscSsr;
	}

	public Map<String, String> getNbIdMap() {
		if (nbIdMap == null) {
			nbIdMap = new LinkedHashMap<>();
		}
		return nbIdMap;
	}

	public Map<String, String> getObIdMap() {
		if (obIdMap == null) {
			obIdMap = new LinkedHashMap<>();
		}
		return obIdMap;
	}

	public Map<String, TravellerStatus> getStatusMap() {
		if (statusMap == null) {
			statusMap = new LinkedHashMap<>();
		}
		return statusMap;
	}

	public Map<String, String> getTicketNumberDetails() {
		if (ticketNumberDetails == null) {
			ticketNumberDetails = new LinkedHashMap<>();
		}
		return ticketNumberDetails;
	}

	public Map<String, String> getSupplierBookingIds() {
		if (supplierBookingIds == null) {
			supplierBookingIds = new HashMap<>();
		}
		return supplierBookingIds;
	}

	public String getPnrSet() {
		StringJoiner pnrs = new StringJoiner(",");
		if (pnrDetails != null) {
			pnrDetails.forEach((segmentKey, pnr) -> {
				if (!pnrs.toString().contains(pnr)) {
					pnrs.add(pnr);
				}
			});
		}
		return pnrs.toString();
	}

	public String getTicketNumberSet() {
		StringJoiner ticketNums = new StringJoiner(",");
		if (ticketNumberDetails != null) {
			ticketNumberDetails.forEach((segmentKey, tKnum) -> {
				if (!ticketNums.toString().contains(tKnum)) {
					ticketNums.add(tKnum.concat(" "));
				}
			});
		}
		return ticketNums.toString();
	}

	public String getSeatNoSet() {
		StringJoiner seatNos = new StringJoiner(",");
		getSsrSeatInfos().forEach((key, ssr) -> {
			seatNos.add(ssr.getCode());
		});
		return seatNos.toString();
	}

	public String getBaggageSet() {
		StringJoiner baggage = new StringJoiner(",");
		getSsrBaggageInfos().forEach((key, ssr) -> {
			baggage.add(ssr.getDesc());

		});
		return baggage.toString();
	}

	public String getOtherSSRSet() {
		StringJoiner otherSSRs = new StringJoiner(",");
		getSsrExtraServiceInfos().forEach((key, ssrList) -> {
			ssrList.forEach(ssr -> {
				otherSSRs.add(ssr.getDesc());
			});
		});
		return otherSSRs.toString();
	}

}
