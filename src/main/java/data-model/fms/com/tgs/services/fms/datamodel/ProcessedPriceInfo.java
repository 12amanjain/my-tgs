package com.tgs.services.fms.datamodel;

import java.util.List;
import java.util.Map;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessedPriceInfo {
	@SerializedName("tF")
	private Double totalPrice;
	@SerializedName("sl")
	private Integer seatLeft;
	@SerializedName("cm")
	private Double commission;
	@SerializedName("net")
	private Double netPrice;
	@SerializedName("rt")
	private Integer refundableType;
	@SerializedName("cc")
	private CabinClass cabinClass;
	@SerializedName("isMI")
	private boolean isMealIncluded;
	private String id;
	@SerializedName("ft")
	private String fareType;
	@SerializedName("isHB")
	private Boolean isHandBaggage;
	@SerializedName("mu")
	private Double markup;
	private Double tds;
	@SerializedName("sI")
	private SupplierBasicInfo supplierBasicInfo;

	/**
	 * It will be useful to identify the pair of special return flight.
	 */
	@SerializedName("sri")
	private String specialReturnIdentifier;

	/**
	 * It will maintain the specialReturnIdentifier of matching special return
	 * fight, if any.
	 */
	@SerializedName("msri")
	private List<String> matchedSpecialReturnIdentifier;

	@SerializedName("mI")
	private PriceMiscInfo miscInfo;

	@SerializedName("fcs")
	private Map<FareComponent, Double> fareComponents;

	@SerializedName("mcc")
	private Boolean mixCabinClass;
	
	@SerializedName("icca")
	private Boolean isChangeClassAllowed;

}
