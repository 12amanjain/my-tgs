package com.tgs.services.fms.datamodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessedTripInfo {

	@SerializedName("dt")
	private LocalDateTime departureTime;
	@SerializedName("at")
	private LocalDateTime finalArrivalTime;
	@SerializedName("aI")
	private AirlineInfo airlineInfo;

	@SerializedName("aIs")
	private List<AirlineInfo> airlineInfos;


	@SerializedName("fN")
	private List<String> flightNumbers = new ArrayList<>();
	@SerializedName("sids")
	private List<Integer> sourceIds = new ArrayList<>();
	
	/**
	 * This is the total duration of the trip
	 */
	@SerializedName("du")
	private Long duration;

	@SerializedName("pI")
	private List<ProcessedPriceInfo> priceInfo;

	@SerializedName("st")
	private int stops;

	@SerializedName("ima")
	private Boolean isMultiAirline;

	@SerializedName("da")
	private String departureAirport;
	@SerializedName("aa")
	private String arrivalAirport;

	@SerializedName("oda")
	private String orgDepartureAirport;
	@SerializedName("oaa")
	private String orgArrivalAirport;

	@SerializedName("dd")
	private Integer dayDiff;

	@SerializedName("cti")
	private List<ProcessedTripInfo> connectingTripInfo;

	public List<ProcessedTripInfo> getConnectingTripInfo() {
		if (connectingTripInfo == null) {
			connectingTripInfo = new ArrayList<>();
		}
		return connectingTripInfo;
	}

}
