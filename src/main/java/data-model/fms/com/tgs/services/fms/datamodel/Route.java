package com.tgs.services.fms.datamodel;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Route {

	@ApiModelProperty(example = "*-*;BOM-UAE;*-FRA-*;")
	private String key;

	public boolean isValidRoute(String airports, List<RouteInfo> routeInfos) {
		boolean isValid = false;
		airports = airports.trim();
		String[] airport = airports.split("-");
		if (airport.length == 2) {
			AirportInfo fromAirport = routeInfos.get(0).getFromCityOrAirport();
			AirportInfo toAirport = routeInfos.get(routeInfos.size() - 1).getToCityOrAirport();

			/**
			 * Only Source Country & Destination Country
			 * 
			 * @implNote Here Possibilities are *-* | DEL-* | *-DEL | BOM-UAE | BLR-Thailand
			 */
			if ((airport[0].equals("*") || fromAirport.getCountryCode().equals(airport[0])
					|| fromAirport.getCode().equals(airport[0]))
					&& (airport[1].equals("*") || toAirport.getCountryCode().equals(airport[1])
							|| toAirport.getCode().equals(airport[1]))) {
				isValid = true;
			}

		} else if (airport.length > 2) {
			/**
			 * @implNote Here Possibilities for Via points (connecting flights)DEL-BOM-BLR
			 *           or *-BOM-* or BOM-*-DEL or Thailand-BLR
			 *
			 * @implNote : 1 Check Source & Destination [Country,Code]
			 * @implNote : 2 In Between codes will be via points (connecting routes)
			 */

			AtomicBoolean isValidOnAllSegments = new AtomicBoolean();
			AirportInfo fromAirport = routeInfos.get(0).getFromCityOrAirport();
			AirportInfo toAirport = routeInfos.get(routeInfos.size() - 1).getToCityOrAirport();

			// Step : 1 Check Source & Destination
			if ((airport[0].equals("*") || fromAirport.getCountryCode().equals(airport[0])
					|| fromAirport.getCode().equals(airport[0]))
					&& (airport[airport.length - 1].equals("*")
							|| toAirport.getCountryCode().equals(airport[airport.length - 1])
							|| toAirport.getCode().equals(airport[airport.length - 1]))) {

				// Step : 2 Check Via Point Airport [airport[1] - will be via-points]
				routeInfos.forEach(routeInfo -> {
					if (routeInfo.getToCityOrAirport().getCode().equals(airport[1])
							|| routeInfo.getToCityOrAirport().getCountryCode().equals(airport[1])
							|| routeInfo.getFromCityOrAirport().getCountryCode().equals(airport[1])
							|| routeInfo.getFromCityOrAirport().getCode().equals(airport[1])) {
						isValidOnAllSegments.set(Boolean.TRUE);
					} else {
						if (!isValidOnAllSegments.get())
							isValidOnAllSegments.set(Boolean.FALSE);
					}
				});
			}
			isValid = isValidOnAllSegments.get();
		}
		return isValid;
	}
}
