package com.tgs.services.fms.datamodel;

import lombok.Getter;

@Getter
public class SectorTypeInfo {
	private String key;
	private Integer locationType;
}
