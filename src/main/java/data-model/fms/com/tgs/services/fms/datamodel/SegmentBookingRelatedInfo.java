package com.tgs.services.fms.datamodel;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.AirItemStatus;

import com.tgs.services.base.helper.APIUserExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SegmentBookingRelatedInfo {

	@SerializedName("tI")
	private List<FlightTravellerInfo> travellerInfo;

	@APIUserExclude
	@SerializedName("st")
	public AirItemStatus status;

}
