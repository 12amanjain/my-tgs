package com.tgs.services.fms.datamodel;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DurationFormatUtils;
import com.google.common.base.Joiner;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.AirAPIExcludeV1;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.alternateclass.AlternateClass;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true)
public class SegmentInfo {

	private String id;

	@APIUserExclude
	@RestExclude
	@DBExclude
	private Short pricingGroupId;

	/**
	 * It consist of information about aircraft , flight number and airline operating this aircraft.
	 */
	@SerializedName("fD")
	@ToString.Include
	private FlightDesignator flightDesignator;

	/**
	 * In case of code share flights ( flight which is being operated by two carrier) , {@link #operatedByAirlineInfo}
	 * information becomes necessary to identify operating airline.
	 */
	@SerializedName("oB")
	private AirlineInfo operatedByAirlineInfo;


	private Integer stops;

	@SerializedName("so")
	private List<AirportInfo> stopOverAirports;

	/**
	 * This is the total duration/flying time of the segment
	 */
	private long duration;

	/**
	 * {@link #connectingTime} consist of connecting time between next segment. This should be populated if there is any
	 * further connection.
	 */
	@SerializedName("cT")
	private Long connectingTime;

	@SerializedName("da")
	@ToString.Include
	private AirportInfo departAirportInfo;

	@SerializedName("aa")
	@ToString.Include
	private AirportInfo arrivalAirportInfo;

	/**
	 * {@link #departTime} in local timeZone of departureAirport
	 */
	@SerializedName("dt")
	@ToString.Include
	private LocalDateTime departTime;

	/**
	 * {@link #arrivalTime} in local timeZone of arrivalAirport
	 */
	@SerializedName("at")
	@ToString.Include
	private LocalDateTime arrivalTime;

	@SerializedName("d")
	private String departure;

	@SerializedName("a")
	private String arrival;

	@SerializedName("iand")
	private boolean isArrivalNextDay;

	public Amenities amenities;

	@SerializedName("isRs")
	@ToString.Include
	public Boolean isReturnSegment;

	@SerializedName("sN")
	@ToString.Include
	public Integer segmentNum;

	@APIUserExclude
	@RestExclude
	@SerializedName("iscfs")
	public Boolean isCombinationFirstSegment;

	/**
	 * PriceInfo is a list because one segmentInfo can have multiple fares
	 */
	@ToString.Include
	@AirAPIExcludeV1
	private List<PriceInfo> priceInfoList;

	@RestExclude
	private SegmentMiscInfo miscInfo;

	/**
	 * This field will be empty at the time of search and will be populated at the time of review . Also it will be
	 * depend upon Airline/Supplier wise . If any airline doesn't support SSR in API then it won't come in response as
	 * well
	 */
	private Map<SSRType, List<? extends SSRInformation>> ssrInfo;

	@SerializedName("bI")
	private SegmentBookingRelatedInfo bookingRelatedInfo;

	@SerializedName("ac")
	private List<AlternateClass> alternateClass;

	public List<PriceInfo> getPriceInfoList() {
		if (priceInfoList == null) {
			priceInfoList = new ArrayList<>();
		}
		return priceInfoList;
	}

	public PriceInfo getPriceInfo(int index, SupplierBasicInfo supplierInfo) {
		if (getPriceInfoList().size() <= index) {
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(supplierInfo);
			getPriceInfoList().add(priceInfo);
		}
		return getPriceInfoList().get(index);
	}

	public PriceInfo getPriceInfo(int index) {
		return getPriceInfoList().get(index);
	}

	public PriceInfo getPriceInfo(int index, FlightTravellerInfo travellerInfo) {
		PriceInfo priceInfo = getPriceInfoList().get(index);
		// To Handle Post Booking Fare Details of Traveller
		if ((priceInfo == null || MapUtils.isEmpty(priceInfo.getFareDetails())) && this.getBookingRelatedInfo() != null
				&& CollectionUtils.isNotEmpty(this.getBookingRelatedInfo().getTravellerInfo())) {
			// priceInfo is segmentinfo price (instance will affect) , thats why creating
			// new priceInfo1 (post booking pax has different fare detail)
			List<FlightTravellerInfo> travellerInfos = this.getBookingRelatedInfo().getTravellerInfo();
			PriceInfo priceInfo1 = PriceInfo.builder().build();
			priceInfo1.setSupplierBasicInfo(priceInfo.getSupplierBasicInfo());
			travellerInfos.forEach(travellerInfo1 -> {
				if (travellerInfo1.equals(travellerInfo) || (StringUtils.equalsIgnoreCase(travellerInfo1.getFirstName(),
						travellerInfo.getFirstName())
						&& StringUtils.equalsIgnoreCase(travellerInfo1.getLastName(), travellerInfo.getLastName()))) {
					priceInfo1.getFareDetails().put(travellerInfo.getPaxType(), travellerInfo.getFareDetail());
				}
			});
			return priceInfo1;
		}
		return priceInfo;
	}

	public SegmentMiscInfo getMiscInfo() {
		if (miscInfo == null) {
			miscInfo = SegmentMiscInfo.builder().build();
		}
		return miscInfo;
	}

	@ApiModelProperty(hidden = true)
	public String getDepartureAirportCode() {
		if (departAirportInfo != null) {
			return departAirportInfo.getCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getArrivalAirportCode() {
		if (arrivalAirportInfo != null) {
			return arrivalAirportInfo.getCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getAirlineCode(boolean isOperatingAirline) {
		if (isOperatingAirline) {
			if (this.getOperatedByAirlineInfo() != null)
				return this.getOperatedByAirlineInfo().getCode();
		}

		if (this.getFlightDesignator() != null)
			return this.getFlightDesignator().getAirlineInfo().getCode();

		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getFlightNumber() {
		if (this.getFlightDesignator() != null)
			return this.getFlightDesignator().getFlightNumber();
		return null;
	}

	@ApiModelProperty(hidden = true)
	public LocalDateTime getDepartTime() {
		return departTime;
	}

	@ApiModelProperty(hidden = true)
	public String getFormattedDepartTime() {
		return departTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy, HH:mm"));
	}

	@ApiModelProperty(hidden = true)
	public String getFormattedArrivalTime() {
		return arrivalTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy, HH:mm"));
	}

	@ApiModelProperty(hidden = true)
	public LocalDateTime getArrivalTime() {
		return arrivalTime;
	}

	@ApiModelProperty(hidden = true)
	public Integer getStops() {
		return ObjectUtils.defaultIfNull(stops, 0);
	}

	@ApiModelProperty(hidden = true)
	public void setTimeLimit(LocalDateTime holdTime) {
		if (this.getPriceInfo(0) != null) {
			this.getPriceInfo(0).getMiscInfo().setTimeLimit(holdTime);
		}
	}

	@ApiModelProperty(hidden = true)
	public boolean hasPricing() {
		FareDetail adultFareDetail;
		Map<PaxType, FareDetail> fareDetails;
		for (int i = 0; i < getPriceInfoList().size(); i++) {
			fareDetails = getFareDetailsForPriceInfo(i);
			if (fareDetails != null) {
				adultFareDetail = fareDetails.get(PaxType.ADULT);
				if (adultFareDetail != null && adultFareDetail.hasNonZeroFare())
					return true;
			}
		}
		return hasPricingInBookingInfo();
	}

	@ApiModelProperty(hidden = true)
	public boolean hasPricingInBookingInfo() {
		Map<PaxType, FareDetail> fareDetails = getFareDetailsFromBookingInfo();
		if (fareDetails != null) {
			FareDetail adultFareDetail = fareDetails.get(PaxType.ADULT);
			if (adultFareDetail != null && adultFareDetail.hasNonZeroFare())
				return true;
		}
		return false;
	}

	/**
	 * Returns non-empty {@code Map<PaxType, FareDetail>} or null.
	 *
	 * @return
	 */
	@ApiModelProperty(hidden = true)
	public Map<PaxType, FareDetail> getFareDetailsFromBookingInfo() {
		Map<PaxType, FareDetail> fareDetails = new HashMap<>();
		/**
		 * SegmentInfos converted from AirOrderItem will not have fareDetails in PriceInfos. However, they have
		 * fareDetail in BookingRelatedInfo.<br>
		 * <br>
		 * Note {@code MapUtils.isNotEmpty(getPriceInfoList().get(0).getFareDetails())}
		 */
		if (getBookingRelatedInfo() != null) {
			for (FlightTravellerInfo traveller : getBookingRelatedInfo().getTravellerInfo()) {
				if (fareDetails.get(traveller.getPaxType()) == null) {
					fareDetails.put(traveller.getPaxType(), traveller.getFareDetail());
				}
			}
		}
		return fareDetails.isEmpty() ? null : fareDetails;
	}

	/**
	 * For given priceInfoIndex, returns non-empty {@code Map<PaxType, FareDetail>} or null.
	 *
	 * @param priceInfoIndex
	 * @return
	 */
	@ApiModelProperty(hidden = true)
	public Map<PaxType, FareDetail> getFareDetailsForPriceInfo(int priceInfoIndex) {
		Map<PaxType, FareDetail> fareDetailsFromPriceInfo;
		if (CollectionUtils.isNotEmpty(getPriceInfoList()) && MapUtils
				.isNotEmpty(fareDetailsFromPriceInfo = getPriceInfoList().get(priceInfoIndex).getFareDetails())) {
			return fareDetailsFromPriceInfo;
		}
		// fareDetailsFromPriceInfo could be empty. Return null, instead.
		return null;
	}

	/*
	 * This property used to return Connecting Time in HH:mm format for email use
	 */
	@ApiModelProperty(hidden = true)
	public String getLayoverTime() {
		if (connectingTime != null) {
			if (connectingTime >= 24 * 60) {
				return DurationFormatUtils.formatDuration(connectingTime * 60000, "dd'd' HH'h' mm'm'");
			} else {
				return DurationFormatUtils.formatDuration(connectingTime * 60000, "HH'h' mm'm'");
			}
		}
		return null;
	}

	/*
	 * This property used to return Segment Duration in HH:mm format for email use
	 */
	@ApiModelProperty(hidden = true)
	public String getSegmentDuration() {
		if (duration >= 24 * 60) {
			return DurationFormatUtils.formatDuration(duration * 60000, "dd'd' HH'h' mm'm'");
		}
		return DurationFormatUtils.formatDuration(duration * 60000, "HH'h' mm'm'");

	}

	@ApiModelProperty(hidden = true)
	public long getDuration() {
		return duration;
	}

	@ApiModelProperty(hidden = true)
	public long calculateDuration() {
		if (this.getDepartTime() != null && this.getArrivalTime() != null) {
			TimeZone arrivalZone = TgsDateUtils.getTimeZoneOfCountry(getArrivalAirportInfo().getCountryCode());
			TimeZone departZone = TgsDateUtils.getTimeZoneOfCountry(getDepartAirportInfo().getCountryCode());
			ZonedDateTime onwardArrivalTime = getArrivalTime().atZone(arrivalZone.toZoneId());
			ZonedDateTime returnDepartureTime = getDepartTime().atZone(departZone.toZoneId());
			ZonedDateTime returnZonedDepartureTime = returnDepartureTime.withZoneSameInstant(arrivalZone.toZoneId());
			if (returnZonedDepartureTime.isBefore(onwardArrivalTime))
				return Duration.between(returnZonedDepartureTime, onwardArrivalTime).toMinutes();
			return Duration.between(onwardArrivalTime, returnZonedDepartureTime).toMinutes();

		}
		return 0;
	}

	public String getDepartureAndArrivalAirport() {
		return getDepartureAirportCode().concat("-").concat(getArrivalAirportCode());
	}

	public String getSegmentBookingKey() {
		return StringUtils.join(getDepartureAirportCode(), getArrivalAirportCode(), getDepartTime().toLocalDate())
				.trim();
	}

	@ApiModelProperty(hidden = true)
	public SupplierBasicInfo getSupplierInfo() {
		if (CollectionUtils.isNotEmpty(this.getPriceInfoList())) {
			return this.getPriceInfo(0).getSupplierBasicInfo();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getSegmentKey() {
		return StringUtils.join(getDepartureAirportCode(), "_", getArrivalAirportCode());
	}

	@ApiModelProperty(hidden = true)
	public void updateAirlinePnr(String pnr) {
		if (this.getBookingRelatedInfo() != null && StringUtils.isNotBlank(pnr)) {
			this.getBookingRelatedInfo().getTravellerInfo().forEach(travellerInfo -> {
				travellerInfo.setPnr(pnr);
			});
		}
	}

	@ApiModelProperty(hidden = true)
	public void updateReservationPnr(String reservationPnr) {
		if (this.getBookingRelatedInfo() != null) {
			this.getBookingRelatedInfo().getTravellerInfo().forEach(travellerInfo -> {
				travellerInfo.setReservationPNR(reservationPnr);
			});
		}
	}

	@ApiModelProperty(hidden = true)
	public boolean hasTicketNumberForAllPax() {
		AtomicBoolean hasTicketNumber = new AtomicBoolean();
		for (FlightTravellerInfo travellerInfo : this.getBookingRelatedInfo().getTravellerInfo()) {
			if (StringUtils.isBlank(travellerInfo.getTicketNumber())) {
				hasTicketNumber.set(Boolean.FALSE);
				// if any one pax doesn't has tikcet number then trip has not ticket number
				return hasTicketNumber.get();
			}
			hasTicketNumber.set(Boolean.TRUE);
		}
		return hasTicketNumber.get();
	}

	@ApiModelProperty(hidden = true)
	public boolean hasPNRForAllPax() {
		AtomicBoolean hasPNR = new AtomicBoolean();
		for (FlightTravellerInfo travellerInfo : this.getBookingRelatedInfo().getTravellerInfo()) {
			if (StringUtils.isBlank(travellerInfo.getPnr())) {
				hasPNR.set(Boolean.FALSE);
				// if any one pax doesn't has tikcet number then trip has not ticket number
				return hasPNR.get();
			}
			hasPNR.set(Boolean.TRUE);
		}
		return hasPNR.get();
	}

	@ApiModelProperty
	public String getPlatingCarrier(PriceInfo priceInfo) {
		if (priceInfo != null && priceInfo.getMiscInfo().getPlatingCarrier() != null) {
			return priceInfo.getMiscInfo().getPlatingCarrier().getCode();
		}
		if (CollectionUtils.isNotEmpty(priceInfoList)
				&& this.getPriceInfo(0).getMiscInfo().getPlatingCarrier() != null) {
			return this.getPriceInfo(0).getMiscInfo().getPlatingCarrier().getCode();
		}
		return getFlightDesignator().getAirlineInfo().getCode();
	}

	public List<AlternateClass> getAlternateClass() {
		if (alternateClass == null) {
			alternateClass = new ArrayList<>();
		}
		return alternateClass;
	}

	public List<FlightTravellerInfo> getTravellerInfo() {
		if (bookingRelatedInfo != null && CollectionUtils.isNotEmpty(bookingRelatedInfo.getTravellerInfo())) {
			return bookingRelatedInfo.getTravellerInfo();
		}
		return null;
	}

	public AirType getAirType(String clientCountry) {
		boolean isDomestic = false;
		if (StringUtils.isNotBlank(clientCountry)
				&& getArrivalAirportInfo().getCountry().equalsIgnoreCase(clientCountry)
				&& getDepartAirportInfo().getCountry().equalsIgnoreCase(clientCountry))
			isDomestic = true;
		return isDomestic ? AirType.DOMESTIC : AirType.INTERNATIONAL;
	}

	// Attributes to compare if 2 segments are equal.
	public String getUniqueAttributes() {
		return Joiner.on("_").join(this.getDepartureAirportCode(), this.getArrivalAirportCode(),
				this.getAirlineCode(false), this.getDeparture(), this.getArrival(), this.getFlightNumber());
	}

	public void addSSRInfo(SSRType ssrType, List<? extends SSRInformation> ssrInfos) {
		if (CollectionUtils.isNotEmpty(ssrInfos)) {
			if (MapUtils.isEmpty(getSsrInfo())) {
				ssrInfo = new HashMap<>();
			}
			getSsrInfo().put(ssrType, ssrInfos);
		}
	}
}
