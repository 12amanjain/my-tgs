package com.tgs.services.fms.datamodel;

import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.SearchType;

import com.tgs.services.base.helper.APIUserExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SegmentMiscInfo {

	@APIUserExclude
	private Boolean isHoldAllowed;

	/**
	 * This will be used to update Mark Up per trip at different steps.
	 */
	private Double updatedUserFee;
	private String updatedUserfeeType;

	// This will be used to identify the segment type ex: oneway, return, multicity.
	private SearchType searchType;

	// This will be used to identify the segment Air type ex: International,
	// domestic
	private AirType airType;

}
