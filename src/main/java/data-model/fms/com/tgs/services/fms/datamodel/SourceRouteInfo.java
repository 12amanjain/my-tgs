package com.tgs.services.fms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceRouteInfo {
	
	private Long id;
	
	private String src;
	
	private String dest;
	
	private Integer sourceId;

	private Boolean enabled;

}
