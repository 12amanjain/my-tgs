package com.tgs.services.fms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TravellerMiscInfo {
    
    private String passengerKey;
}