package com.tgs.services.fms.datamodel;

import lombok.Getter;

@Getter
public enum TravellerStatus {

	CANCELLED("CAN"),
	NO_SHOW("NS"),
	VOIDED("V"),
	WAIT_LISTED("W"),
	REISSUED("R"),
	AMENDMENT_IN_PROGRESS("AIP"),
	BOARDED("B") {
		@Override
		public boolean correctionAllowed() {
			return false;
		}
	},
	ORDER_PENDING("CRA") {
		@Override
		public boolean correctionAllowed() {
			return false;
		}
	},
	PENDING("P") {
		@Override
		public boolean correctionAllowed() {
			return false;
		}
	},
	ABORTED("A");

	private String code;

	TravellerStatus(String code) {
		this.code = code;
	}

	public static TravellerStatus getEnumFromCode(String code) {
		return getTravellerStatus(code);
	}

	public static TravellerStatus getTravellerStatus(String code) {
		for (TravellerStatus status : TravellerStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public boolean correctionAllowed() {
		return true;
	}

	public boolean isCancelled() {
		if (this.equals(CANCELLED) || this.equals(VOIDED) || this.equals(NO_SHOW) || this.equals(REISSUED)) {
			return true;
		}
		return false;
	}

}
