package com.tgs.services.fms.datamodel;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import com.tgs.services.base.enums.FareComponent;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@ToString(onlyExplicitlyIncluded = true)
public class TripInfo {

	/**
	 * This is used to uniquely identify TripInfo. There is no need to populate this information while parsing response
	 * from airline/supplier API
	 */
	@APIUserExclude
	private String id;

	@ToString.Include
	@SerializedName("sI")
	private List<SegmentInfo> segmentInfos;

	/**
	 * There is no need to populate this field while parsing response from airline/supplier API
	 */
	@APIUserExclude
	private ProcessedTripInfo processedTripInfo;

	@ToString.Include
	@Exclude
	private Map<PaxType, Integer> paxInfo;

	@SerializedName("totalPriceList")
	private List<PriceInfo> tripPriceInfos;

	@Exclude
	private AirBookingConditions bookingConditions;

	public List<SegmentInfo> getSegmentInfos() {
		if (segmentInfos == null) {
			segmentInfos = new ArrayList<>();
		}
		return segmentInfos;
	}

	public AirBookingConditions getBookingConditions() {
		if (bookingConditions == null) {
			bookingConditions = AirBookingConditions.builder().build();
		}
		return bookingConditions;
	}

	@ApiModelProperty(hidden = true)
	public SupplierBasicInfo getSupplierInfo() {
		if (isPriceInfosNotEmpty()) {
			return this.segmentInfos.get(0).getPriceInfo(0).getSupplierBasicInfo();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public Set<String> getFlightNumberSet() {
		Set<String> flightNumbers = new HashSet<>();
		if (isSegmentsNotEmpty()) {
			this.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getFlightDesignator() != null)
					flightNumbers.add(segmentInfo.getFlightDesignator().getFlightNumber());
			});
		}
		return flightNumbers != null ? flightNumbers : null;
	}

	@ApiModelProperty(hidden = true)
	public Set<String> getBookingClassSet(int priceIndex) {
		Set<String> bookingClasses = new HashSet<>();
		if (getSegmentInfos().get(0).getBookingRelatedInfo() != null
				&& getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail() != null) {
			getSegmentInfos().forEach(segmentInfo -> {
				bookingClasses.add(segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail()
						.getClassOfBooking());
			});
		} else if (isSegmentsNotEmpty() && isPriceInfosNotEmpty()
				&& getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			getSegmentInfos().forEach(segmentInfo -> {
				bookingClasses
						.add(segmentInfo.getPriceInfo(priceIndex).getFareDetail(PaxType.ADULT).getClassOfBooking());
			});
		}
		return bookingClasses;
	}

	@ApiModelProperty(hidden = true)
	public Set<String> getFareBasisSet(int priceIndex) {
		Set<String> fareBasisSet = new HashSet<>();
		if (getSegmentInfos().get(0).getBookingRelatedInfo() != null
				&& getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail() != null) {
			getSegmentInfos().forEach(segmentInfo -> {
				fareBasisSet.add(
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail().getFareBasis());
			});
		} else if (isSegmentsNotEmpty() && isPriceInfosNotEmpty()
				&& getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			getSegmentInfos().forEach(segmentInfo -> {
				fareBasisSet.add(segmentInfo.getPriceInfo(priceIndex).getFareDetail(PaxType.ADULT).getFareBasis());
			});
		}
		return fareBasisSet;
	}

	@ApiModelProperty(hidden = true)
	public Set<String> getFareType(int priceIndex) {
		Set<String> fareType = new HashSet<>();
		if (isSegmentsNotEmpty() && isPriceInfosNotEmpty()) {
			getSegmentInfos().forEach(segmentInfo -> {
				fareType.add(segmentInfo.getPriceInfo(priceIndex).getFareType());
			});
		}
		return fareType;
	}

	@ApiModelProperty(hidden = true)
	public Set<CabinClass> getCabinClassSet(int priceIndex) {
		Set<CabinClass> cabinClasses = new HashSet<>();
		if (this.getSegmentInfos().get(0).getBookingRelatedInfo() != null && this.getSegmentInfos().get(0)
				.getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail() != null) {
			getSegmentInfos().forEach(segmentInfo -> {
				cabinClasses.add(
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail().getCabinClass());
			});
		} else if (isSegmentsNotEmpty() && isPriceInfosNotEmpty()
				&& this.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			getSegmentInfos().forEach(segmentInfo -> {
				cabinClasses.add(segmentInfo.getPriceInfo(priceIndex).getFareDetail(PaxType.ADULT).getCabinClass());
			});
		}
		return cabinClasses;
	}

	@ApiModelProperty(hidden = true)
	public AirportInfo getArrivalAirport() {
		return this.getSegmentInfos().get(getNumOfSegments() - 1).getArrivalAirportInfo();
	}

	@ApiModelProperty(hidden = true)
	public String getArrivalAirportCode() {
		if (getArrivalAirport() != null) {
			return getArrivalAirport().getCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public AirportInfo getDepartureAirport() {
		return this.getSegmentInfos().get(0).getDepartAirportInfo();
	}

	@ApiModelProperty(hidden = true)
	public String getDepartureAirportCode() {
		if (getDepartureAirport() != null) {
			return getDepartureAirport().getCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public Set<String> getAirlineInfo(boolean isOperatingAirline) {
		Set<String> airlineInfos = new HashSet<>();
		if (isSegmentsNotEmpty()) {
			if (isOperatingAirline) {
				this.getSegmentInfos().forEach(segmentInfo -> {
					airlineInfos.add(segmentInfo.getAirlineCode(true));
				});
				return airlineInfos;
			}
			this.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getFlightDesignator() != null)
					airlineInfos.add(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
			});
		}
		return airlineInfos;
	}

	/*
	 * @return airlineCode based on segmentIndex
	 */
	@ApiModelProperty(hidden = true)
	public String getAirlineCode(boolean isOperatingAirline, int segmentIndex) {
		if (isSegmentsNotEmpty() && segmentIndex < getNumOfSegments()) {
			SegmentInfo segmentInfo = this.getSegmentInfos().get(segmentIndex);
			if (isOperatingAirline) {
				if (segmentInfo.getOperatedByAirlineInfo() != null)
					return segmentInfo.getOperatedByAirlineInfo().getCode();
			}
			if (segmentInfo.getFlightDesignator() != null)
				return segmentInfo.getFlightDesignator().getAirlineInfo().getCode();
		}
		return null;
	}

	@ApiModelProperty(hidden = true)
	public LocalDateTime getDepartureTime() {
		if (isSegmentsNotEmpty())
			return this.getSegmentInfos().get(0).getDepartTime();
		return null;
	}

	@ApiModelProperty(hidden = true)
	public String getFormattedDepartureTime() {
		LocalDateTime departTime = getDepartureTime();
		if (departTime != null)
			return departTime.format(DateTimeFormatter.ofPattern("MMM dd, yyyy, HH:mm"));
		return "";
	}

	@ApiModelProperty(hidden = true)
	public LocalDateTime getArrivalTime() {
		if (isSegmentsNotEmpty())
			return this.getSegmentInfos().get(getNumOfSegments() - 1).getArrivalTime();
		return null;
	}

	@ApiModelProperty(hidden = true)
	public TimePeriod getTravelPeriod() {
		TimePeriod period = new TimePeriod();
		period.setStartTime(getDepartureTime());
		period.setEndTime(getArrivalTime());
		return period;
	}

	@ApiModelProperty(hidden = true)
	public boolean isSegmentsNotEmpty() {
		return this != null && CollectionUtils.isNotEmpty(segmentInfos);
	}

	@ApiModelProperty(hidden = true)
	public boolean isPriceInfosNotEmpty() {
		boolean isPriceNotEmpty = true;
		if (isSegmentsNotEmpty()) {
			for (SegmentInfo segmentInfo : segmentInfos) {
				isPriceNotEmpty = CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList());
				if (!isPriceNotEmpty) {
					return false;
				}
			}
		}
		return isPriceNotEmpty;
	}

	@ApiModelProperty(hidden = true)
	public Integer getNumOfSegments() {
		if (isSegmentsNotEmpty()) {
			return this.getSegmentInfos().size();
		}
		return 0;
	}

	@ApiModelProperty(hidden = true)
	public CabinClass getCabinClass() {
		CabinClass cabinClass = null;
		if (isSegmentsNotEmpty() && isPriceInfosNotEmpty()
				&& this.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			cabinClass = this.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass();
		} else if (CollectionUtils.isNotEmpty(this.getTravllerInfos())
				&& this.getTravllerInfos().get(0).getFareDetail() != null) {
			cabinClass = this.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail()
					.getCabinClass();
		}
		return cabinClass;
	}

	@ApiModelProperty(hidden = true)
	public List<RouteInfo> getRouteInfos() {
		List<RouteInfo> routeInfos = new ArrayList<>();
		List<RouteInfo> finalRouteInfos = routeInfos;
		this.segmentInfos.forEach(segmentInfo -> {
			RouteInfo routeInfo = RouteInfo.builder().fromCityOrAirport(segmentInfo.getDepartAirportInfo())
					.toCityOrAirport(segmentInfo.getArrivalAirportInfo())
					.travelDate(segmentInfo.getDepartTime().toLocalDate()).build();
			finalRouteInfos.add(routeInfo);
		});
		return routeInfos;
	}

	@ApiModelProperty(hidden = true)
	public RouteInfo getRouteInfo() {
		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setFromCityOrAirport(getDepartureAirport());
		routeInfo.setToCityOrAirport(getArrivalAirport());
		routeInfo.setTravelDate(getDepartureTime().toLocalDate());
		return routeInfo;
	}

	@ApiModelProperty(hidden = true)
	public List<FlightTravellerInfo> getTravllerInfos() {
		if (isSegmentsNotEmpty() && this.getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
			return this.segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		}
		return null;
	}

	/**
	 * TripInfo to check whether it is domestic
	 *
	 * @return {@code false} if for any of the segment, departure and arrival differ in country. Otherwise,
	 *         {@code true}.
	 */
	@ApiModelProperty(hidden = true)
	public boolean isDomesticTrip(String clientCountry) {
		boolean isDomestic = true;
		for (SegmentInfo segmentInfo : this.getSegmentInfos()) {
			if (!segmentInfo.getDepartAirportInfo().getCountry().equalsIgnoreCase(clientCountry)
					|| !segmentInfo.getArrivalAirportInfo().getCountry().equalsIgnoreCase(clientCountry)) {
				isDomestic = false;
				break;
			}
		}
		return isDomestic;
	}

	/**
	 * @return {@code long } total duration time of trip (connecting time + duration)
	 */
	@ApiModelProperty(hidden = true)
	public Long getTripDuration() {
		long duration = 0;
		for (SegmentInfo segmentInfo : this.getSegmentInfos()) {
			duration += ObjectUtils.defaultIfNull(segmentInfo.getConnectingTime(), 0L)
					+ ObjectUtils.defaultIfNull(segmentInfo.getDuration(), 0L);
		}
		return duration;
	}

	@ApiModelProperty(hidden = true)
	public int getPaxCount() {
		int count = 0;
		if (paxInfo != null) {
			for (Integer c : paxInfo.values()) {
				count += ((c == null) ? 0 : c);
			}
			return count;
		}
		return getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().size();
	}

	@ApiModelProperty(hidden = true)
	public int getPaxCountWithoutInfant() {
		int count = 0;
		if (paxInfo != null) {
			for (PaxType paxType : paxInfo.keySet()) {
				if (!PaxType.INFANT.equals(paxType)) {
					count += paxInfo.getOrDefault(paxType, 0);
				}
			}
		}
		return count;
	}

	@ApiModelProperty(hidden = true)
	public List<String> getStopOverAirports() {
		List<String> stopOverAirports = new ArrayList<>();
		for (SegmentInfo segmentInfo : this.getSegmentInfos()) {
			if (CollectionUtils.isNotEmpty(segmentInfo.getStopOverAirports())) {
				segmentInfo.getStopOverAirports().forEach(stopOverAirport -> {
					stopOverAirports.add(stopOverAirport.getName());
				});
			}
		}
		return stopOverAirports;
	}

	@ApiModelProperty(hidden = true)
	public void setConnectionTime() {
		if (this.getSegmentInfos().size() > 1) {
			LocalDateTime lastArrivalTime = this.getSegmentInfos().get(0).getArrivalTime();
			for (int i = 1; i < this.getSegmentInfos().size(); i++) {
				LocalDateTime nextDeparture = this.getSegmentInfos().get(i).getDepartTime();
				long connectingTime = lastArrivalTime.until(nextDeparture, ChronoUnit.MINUTES);
				this.getSegmentInfos().get(i - 1).setConnectingTime(connectingTime);
				lastArrivalTime = this.getSegmentInfos().get(i).getArrivalTime();
			}
		}
	}

	public List<TripInfo> splitTripInfo(boolean shouldConsiderManualCombination) {
		return splitTripInfo(shouldConsiderManualCombination, true);
	}


	/*
	 * This will split tripInfos based on segmentNum or Manual combination or both depends on the @param
	 * shouldConsiderManualCombination and @param shouldConsiderSegmentNum.
	 */
	@ApiModelProperty(hidden = true)
	public List<TripInfo> splitTripInfo(boolean shouldConsiderManualCombination, boolean shouldConsiderSegmentNum) {
		List<TripInfo> trips = new ArrayList<>();
		TripInfo tempTripInfo = new TripInfo();
		for (SegmentInfo segmentInfo : this.getSegmentInfos()) {
			/**
			 * SegmentNum zero will denote start of new trip. For example in case of intl Return if there are 2 onward
			 * and 2 return segment then segment will be 0,1 for onward and 0,1 for return
			 */
			if ((shouldConsiderSegmentNum && segmentInfo.getSegmentNum() == 0) || (shouldConsiderManualCombination
					&& BooleanUtils.isTrue(segmentInfo.getIsCombinationFirstSegment()))) {
				if (CollectionUtils.isNotEmpty(tempTripInfo.getSegmentInfos())) {
					trips.add(tempTripInfo);
				}
				tempTripInfo = new TripInfo();
			}
			tempTripInfo.getSegmentInfos().add(segmentInfo);
			tempTripInfo.setPaxInfo(this.getPaxInfo());
		}
		trips.add(tempTripInfo);
		return trips;
	}

	@ApiModelProperty(hidden = true)
	public boolean hasTicketNumber() {
		AtomicBoolean hasTicketNumber = new AtomicBoolean();
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getBookingRelatedInfo() != null
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				hasTicketNumber.set(segmentInfo.hasTicketNumberForAllPax());
				if (!hasTicketNumber.get()) {
					return false;
				}
			}
		}
		return hasTicketNumber.get();
	}

	@ApiModelProperty(hidden = true)
	public boolean hasPNR() {
		AtomicBoolean hasPNR = new AtomicBoolean();
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getBookingRelatedInfo() != null
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				hasPNR.set(segmentInfo.hasPNRForAllPax());
				if (!hasPNR.get()) {
					return false;
				}
			}
		}
		return hasPNR.get();
	}

	@ApiModelProperty(hidden = true)
	public String getDepartureArrivalKey() {
		return StringUtils.join(this.getDepartureAirportCode(), "-", this.getArrivalAirportCode());
	}

	@ApiModelProperty(hidden = true)
	public String getAirlineCode() {
		return getAirlineInfo(false).stream().findFirst().get();
	}

	@ApiModelProperty(hidden = true)
	public boolean isMultipleSupplier() {
		Set<String> supplierList = new HashSet<String>();
		this.getSegmentInfos().forEach(segmentInfo -> {
			supplierList.add(segmentInfo.getPriceInfo(0).getSupplierId());
		});
		// If Size > 1 means multiple supplier involved on trip
		return supplierList.size() > 1;
	}

	@ApiModelProperty(hidden = true)
	public String getSegmentsBookingKey() {
		StringJoiner joiner = new StringJoiner(":");
		segmentInfos.forEach(segmentInfo -> {
			joiner.add(segmentInfo.getSegmentBookingKey());
		});
		return joiner.toString();
	}

	@ApiModelProperty
	public String getPlatingCarrier() {
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			SegmentInfo segmentInfo = this.getSegmentInfos().get(0);
			return segmentInfo.getPlatingCarrier(null);
		}
		return null;
	}

	public int daysDiff() {
		List<TripInfo> trips = this.splitTripInfo(false);
		int days = 0;
		for (TripInfo trip : trips) {
			days = Math.max(days, ((int) ChronoUnit.DAYS.between(trip.getDepartureTime().toLocalDate(),
					trip.getArrivalTime().toLocalDate())));
		}
		return days;
	}

	@ApiModelProperty(hidden = true)
	public Set<String> getEquipmentSet() {
		Set<String> equipments = new HashSet<>();
		if (isSegmentsNotEmpty()) {
			this.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getFlightDesignator() != null)
					equipments.add(segmentInfo.getFlightDesignator().getEquipType());
			});
		}
		return equipments;
	}

	@ApiModelProperty(hidden = true)
	public String getTripKey() {
		String key = "";
		for (SegmentInfo segmentInfo : this.getSegmentInfos()) {
			key = StringUtils.join(key, segmentInfo.getFlightDesignator().getFlightNumber(),
					segmentInfo.getAirlineCode(false), segmentInfo.getDepartTime(), segmentInfo.getArrivalTime());
		}
		return key;
	}

	@ApiModelProperty(hidden = true)
	public boolean isVoucherApplied() {
		for (SegmentInfo segmentInfo : getSegmentInfos()) {
			if (segmentInfo.hasPricingInBookingInfo()) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
					if (MapUtils.isNotEmpty(travellerInfo.getFareDetail()
							.getMatchedFareComponents(new ArrayList<>(FareComponent.voucherComponents())))) {
						return true;
					}
				}
			} else {
				PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
				for (PaxType paxType : priceInfo.getFareDetails().keySet()) {
					FareDetail fareDetail = priceInfo.getFareDetail(paxType);
					if (MapUtils.isNotEmpty(fareDetail.getMatchedFareComponents(new ArrayList<>(FareComponent.voucherComponents())))) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
