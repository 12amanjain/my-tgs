package com.tgs.services.fms.datamodel;

public enum TripInfoType {
	ONWARD("ONWARD", "OT"), RETURN("RETURN", "RT"), COMBO("COMBO", "CB");

	private String tripType;
	private String tripCode;

	TripInfoType(String tripType, String tripCode) {
		this.tripType = tripType;
		this.tripCode = tripCode;
	}

	public String getName() {
		return this.name();
	}

	public String getTripCode() {
		return this.tripCode;
	}

	public String getTripType() {
		return this.tripType;
	}

	public static TripInfoType getTripType(String tripCode) {
		for (TripInfoType tripInfoType : TripInfoType.values()) {
			if (tripInfoType.getTripCode().equals(tripCode)) {
				return tripInfoType;
			}
		}
		return null;
	}

}
