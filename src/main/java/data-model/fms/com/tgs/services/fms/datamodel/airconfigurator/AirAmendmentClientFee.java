package com.tgs.services.fms.datamodel.airconfigurator;


import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AirType;
import lombok.Getter;
import java.util.Map;

@Getter
public class AirAmendmentClientFee {

	@SerializedName("af")
	protected Map<AirType, Double> amendmentFee;
}