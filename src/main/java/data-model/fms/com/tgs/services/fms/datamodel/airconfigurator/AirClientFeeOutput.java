package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirAmendmentClientFee;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirClientFeeOutput implements IRuleOutPut {

	@SerializedName("mf")
	private Double managementFee;

	@SerializedName("ccf")
	private Double clientCancellationFee;

	@SerializedName("atccf")
	private Map<AirType, Double> airTypeClientCancellationFee;

	@SerializedName("crf")
	private Double clientRescheduleFee;

	@SerializedName("cnds")
	private ClientFeeConditions conditions;

	@SerializedName("acfee")
	private Map<String, AirAmendmentClientFee> amendmentFeeMap;
}