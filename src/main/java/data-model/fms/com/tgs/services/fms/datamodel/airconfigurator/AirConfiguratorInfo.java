package com.tgs.services.fms.datamodel.airconfigurator;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder(toBuilder = true)
@Getter
@Setter
public class AirConfiguratorInfo implements IRule {

	private Long id;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
	private Boolean enabled;

	private AirConfiguratorRuleType ruleType;

	private FlightBasicRuleCriteria inclusionCriteria;

	private FlightBasicRuleCriteria exclusionCriteria;

	@NotNull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	private Double priority;
	private Boolean exitOnMatch;

	@Exclude
	private boolean isDeleted;

	private String description;

	@Override
	public IRuleOutPut getOutput() {
		IRuleOutPut ruleOutput = GsonUtils.getGson().fromJson(output, ruleType.getOutPutTypeToken().getType());
		ruleOutput.cleanData();
		return ruleOutput;
	}


	public String getSerializedOutput() {
		return output;
	}

	@Override
	public IRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public IRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public double getPriority() {
		return priority == null ? 0.0 : priority;
	}

	@Override
	public boolean exitOnMatch() {
		if (AirConfiguratorRuleType.MESSAGE.equals(ruleType)) {
			if (exitOnMatch == null) {
				return false;
			}
		}
		return exitOnMatch == null ? true : exitOnMatch;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	public void cleanData() {
		if (getPriority() < 0.0) {
			setPriority(0.0);
		}
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
	}
}
