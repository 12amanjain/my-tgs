package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.Arrays;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.AirCustomPathOutput;
import com.tgs.services.fms.datamodel.AirPassportConditions;
import com.tgs.services.fms.datamodel.AirSearchGrpOutput;
import com.tgs.services.fms.datamodel.PWSReason;
import com.tgs.services.fms.ruleengine.FlightBasicAirConfigOutput;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import lombok.Getter;

@Getter
public enum AirConfiguratorRuleType {

	// ENUM Name Should not exceed 14 Character //

	PASSPORT(FlightBasicRuleCriteria.class, new TypeToken<AirPassportConditions>() {}),
	SEARCHGRPNG(FlightBasicRuleCriteria.class, new TypeToken<AirSearchGrpOutput>() {}),
	CUSTOM_PATH(FlightBasicRuleCriteria.class, new TypeToken<AirCustomPathOutput>() {}),
	GNPUPROSE(FlightBasicRuleCriteria.class, new TypeToken<AirGeneralPurposeOutput>() {}),
	APIPROMOTION(FlightBasicRuleCriteria.class, new TypeToken<FlightBasicAirConfigOutput>() {}),
	CLIENTFEE(FlightBasicRuleCriteria.class, new TypeToken<AirClientFeeOutput>() {}),
	MESSAGE(FlightBasicRuleCriteria.class, new TypeToken<ListOutput<MessageInfo>>() {}),
	SOURCECONFIG(FlightBasicRuleCriteria.class, new TypeToken<AirSourceConfigurationOutput>() {}),
	PWS_REASONS(FlightBasicRuleCriteria.class, new TypeToken<ListOutput<PWSReason>>() {}),
	SSR_INFO(FlightBasicRuleCriteria.class, new TypeToken<AirSSRInfoOutput>() {}),
	DOB(FlightBasicRuleCriteria.class, new TypeToken<DobOutput>() {}),
	CHANGE_CLASS(FlightBasicRuleCriteria.class, new TypeToken<ListOutput<String>>() {}),
	CANCELCONFIG(FlightBasicRuleCriteria.class, new TypeToken<AirSourceCancelConfiguration>() {}),
	SOURCETHREAD(FlightBasicRuleCriteria.class, new TypeToken<AirSourceThreadConfiguration>() {}),
	FILTER(FlightBasicRuleCriteria.class, new TypeToken<AirFilterConfiguration>() {}),
	NETREMITTANCE(FlightBasicRuleCriteria.class, new TypeToken<NetRemittanceOutput>() {});

	private Class<? extends IRuleCriteria> inputType;
	private TypeToken<? extends IRuleOutPut> outPutTypeToken;

	<T extends IRuleCriteria, RT extends IRuleOutPut> AirConfiguratorRuleType(Class<T> inputType,
			TypeToken<RT> outPutTypeToken) {
		this.inputType = inputType;
		this.outPutTypeToken = outPutTypeToken;
	}

	public List<AirConfiguratorRuleType> getAirConfigRuleType() {
		return Arrays.asList(AirConfiguratorRuleType.values());
	}

	public static AirConfiguratorRuleType getRuleType(String name) {
		for (AirConfiguratorRuleType ruleType : AirConfiguratorRuleType.values()) {
			if (ruleType.getName().equals(name)) {
				return ruleType;
			}
		}
		return null;
	}

	public String getName() {
		return this.name();
	}

}
