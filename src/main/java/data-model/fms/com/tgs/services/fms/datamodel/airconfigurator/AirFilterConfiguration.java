package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirFilterConfiguration implements IRuleOutPut {

	@SerializedName("isra")
	private boolean isSpecialReturnAllowed;


}
