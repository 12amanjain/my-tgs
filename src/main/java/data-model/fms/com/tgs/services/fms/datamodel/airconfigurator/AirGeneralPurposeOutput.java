package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import com.tgs.services.fms.datamodel.AirTimeLimit;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirGeneralPurposeOutput implements IRuleOutPut, Cleanable {

	@SerializedName("cds")
	private List<Integer> cachedDisabledSources;
	@SerializedName("sfd")
	private Boolean showFareDiff;
	@SerializedName("ffda")
	private List<String> ffDisabledAirlines;
	@SerializedName("dbe")
	private Boolean duplicateBookingEnabled;
	// Define Session time in minutes
	@SerializedName("st")
	private Integer sessionTime;

	@SerializedName("proxy")
	private String proxyAddress;

	@SerializedName("dpm")
	private Boolean disablePriceMerge;

	@SerializedName("pam")
	private Map<String, List<Integer>> prefferedAirlineMap;

	private Map<String, String> ttlMap;

	@SerializedName("tac")
	private List<String> termsAndConditions;

	@SerializedName("anlm")
	private Map<String, NameLengthLimit> airNameLengthMap;

	@SerializedName("sce")
	private Boolean sessionCachingEnabled;

	// flow type vs list of source id's
	@SerializedName("uhas")
	private Map<String, List<Integer>> unHoldAllowedSource;

	@SerializedName("frm")
	private Integer flightRestrictionMinutes;

	@SerializedName("cle")
	private List<String> criticalLoggerExclusions;

	@SerializedName("nbal")
	private List<KeyValue> nearByAirports;

	@SerializedName("nos")
	private List<Integer> nonOperatingNotAllowedSources;

	@SerializedName("atl")
	private AirTimeLimit airTimeLimit;

	// In seconds
	@SerializedName("tto")
	private Long threadTimeout;

	@SerializedName("iga")
	private Boolean isGstApplicable;

	@SerializedName("nsrbc")
	private Integer noSearchResultBufferCount;

	@SerializedName("nsrt")
	private Integer noSearchResultThreshold;

	@SerializedName("iccfta")
	private Boolean ccftAllowed;

	@SerializedName("cnrss")
	private List<Integer> noResultSectorDisabledSource;

	@SerializedName("itime")
	private List<Integer> tripTimingEnabledSources;

	@SerializedName("iacc")
	private Boolean isAutoCancelOnCron;

	@SerializedName("ucss")
	private List<Integer> userCreditCardSupportedSources;

	@SerializedName("isafr")
	private Boolean isAmedmentChargesfromFR;

	@Override
	public void cleanData() {
		setCachedDisabledSources(TgsCollectionUtils.getNonNullElements(getCachedDisabledSources()));
		setTermsAndConditions(TgsCollectionUtils.getCleanStringList(getTermsAndConditions()));
		setFfDisabledAirlines(TgsCollectionUtils.getCleanStringList(getFfDisabledAirlines()));
		setProxyAddress(StringUtils.defaultIfBlank(proxyAddress, null));
		setPrefferedAirlineMap(TgsMapUtils.getCleanMap(prefferedAirlineMap, TgsCollectionUtils::getNonNullElements,
				CollectionUtils::isEmpty));
		setNearByAirports(TgsCollectionUtils.getCleanList(getNearByAirports()));
		setTtlMap(TgsMapUtils.removeKeyIfEmpty(ttlMap));
		setAirNameLengthMap(TgsMapUtils.getCleanMap(airNameLengthMap));
		setNonOperatingNotAllowedSources(TgsCollectionUtils.getNonNullElements(getNonOperatingNotAllowedSources()));
	}

	public Integer getNoSearchResultBufferCount() {
		if (this.noSearchResultBufferCount == null) {
			this.noSearchResultBufferCount = 1;
		}
		return this.noSearchResultBufferCount;
	}

	public int getFlightRestrictionMinutes() {
		return ObjectUtils.firstNonNull(this.flightRestrictionMinutes, 0);
	}

	public List<Integer> getTripTimingEnabledSources() {
		if (tripTimingEnabledSources == null) {
			tripTimingEnabledSources = new ArrayList<>();
		}
		return tripTimingEnabledSources;
	}


}
