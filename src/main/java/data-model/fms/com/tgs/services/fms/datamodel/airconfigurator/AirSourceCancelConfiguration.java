package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import lombok.Getter;
import lombok.Setter;
import java.util.Map;

@Getter
@Setter
public class AirSourceCancelConfiguration implements IRuleOutPut, Cleanable {

	// fare type vs restricted fare type for cancellation
	@SerializedName("cbd")
	private Map<String, Boolean> cancellationAllowed;

	// fare type vs restricted for cancellation before departure
	@SerializedName("dcbd")
	private Map<AirType, Map<String, Integer>> cancellationRestrictions;

	@SerializedName("ssrr")
	private Boolean isSSRRefundable;

	@Override
	public void cleanData() {
		
	}

}
