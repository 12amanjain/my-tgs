package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import com.tgs.services.fms.datamodel.AirTimeLimit;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSourceConfigurationOutput implements IRuleOutPut, Cleanable {

	@SerializedName("ial")
	private List<String> includedAirlines;
	@SerializedName("eal")
	private List<String> excludedAirlines;
	@SerializedName("aal")
	private List<String> allowedAirlines;
	@SerializedName("foidal")
	private List<String> foidAirlines;
	@SerializedName("daft")
	private List<String> disAllowedFareTypes;

	@SerializedName("fc")
	private Integer flightsCount;

	@SerializedName("dfc")
	private Integer domflightsCount;

	// In minutes
	@SerializedName("st")
	private Integer sessionTime;

	@SerializedName("furls")
	private List<String> flightUrl;

	@SerializedName("cm")
	private Integer comboMinutes;

	@SerializedName("fr")
	private Boolean isFareRuleFetchFromApi;

	@SerializedName("afd")
	private Double allowedFareDiff;

	@SerializedName("nbal")
	private List<KeyValue> nearByAirports;

	@SerializedName("ffda")
	private List<String> ffDisabledAirlines;

	@SerializedName("sb")
	private Boolean isSupplierBlockAllowed;

	@SerializedName("tac")
	private List<String> termsAndConditions;

	@SerializedName("anlm")
	private Map<String, NameLengthLimit> airNameLengthMap;

	@SerializedName("sss")
	private Boolean isSingleSell;

	@SerializedName("isa")
	private Boolean isSeatApplicable;

	@SerializedName("ibo")
	private Boolean isBaggageOverride;

	@SerializedName("imo")
	private Boolean isMealOverride;

	@SerializedName("irto")
	private Boolean isRefundableTypeOverride;

	// In seconds
	@SerializedName("tto")
	private Integer threadTimeout;

	@SerializedName("icf")
	private Boolean isConfirmFareBeforeTicket;

	@SerializedName("sbm")
	private Integer blockMinutes;

	@SerializedName("cfc")
	private Boolean cacheFareComponents;

	@SerializedName("cf")
	private List<FareComponent> cacheableFareComponents;

	@SerializedName("icca")
	private Map<AirType, Boolean> isChangeClassAllowed;

	@SerializedName("mrm")
	private Integer mealSSRRestrictionMinutes;

	@SerializedName("brm")
	private Integer baggageSSRRestrictionMinutes;

	@SerializedName("smrm")
	private Integer seatMapRestrictionMinutes;

	@SerializedName("frm")
	private Integer flightRestrictionMinutes;


	@SerializedName("ccodes")
	private List<KeyValue> currencyCodes;

	@SerializedName("atl")
	private AirTimeLimit airTimeLimit;

	// In minutes
	@SerializedName("mct")
	private Integer minimumConnectingTime;

	@SerializedName("iffc")
	private Boolean isInfantFareFromCache;

	@SerializedName("iswt")
	private Boolean isSearchWithTax;

	@SerializedName("isaca")
	private Boolean isAutoCancellationAllowed;

	@SerializedName("iffi")
	private Boolean isInfantFareFromItineraryPrice;

	@SerializedName("isb")
	private Boolean isServiceBundle;

	@SerializedName("spc")
	private List<String> specialAccountCodes;

	@SerializedName("iar")
	private Boolean isAddressRequired;

    @SerializedName("isafr")
    private Boolean isAmedmentChargesfromFR;

	@Override
	public void cleanData() {
		setIncludedAirlines(TgsCollectionUtils.getCleanStringList(getIncludedAirlines()));
		setExcludedAirlines(TgsCollectionUtils.getCleanStringList(getExcludedAirlines()));
		setDisAllowedFareTypes(TgsCollectionUtils.getCleanStringList(getDisAllowedFareTypes()));
		setFlightUrl(TgsCollectionUtils.getCleanStringList(getFlightUrl()));
		setFfDisabledAirlines(TgsCollectionUtils.getCleanStringList(getFfDisabledAirlines()));
		setNearByAirports(TgsCollectionUtils.getCleanList(getNearByAirports()));
		setCacheableFareComponents(TgsCollectionUtils.getNonNullElements(getCacheableFareComponents()));
		setAirNameLengthMap(TgsMapUtils.getCleanMap(airNameLengthMap));
		setFoidAirlines(TgsCollectionUtils.getCleanStringList(getFoidAirlines()));
		setSpecialAccountCodes(TgsCollectionUtils.getCleanStringList(getSpecialAccountCodes()));
	}

}
