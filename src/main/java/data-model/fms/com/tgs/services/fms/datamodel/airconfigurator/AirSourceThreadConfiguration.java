package com.tgs.services.fms.datamodel.airconfigurator;

import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSourceThreadConfiguration implements IRuleOutPut {

	@SerializedName("swttoc")
	private Map<Integer, AirSourceThreadConfig> sourceWiseThreadConfig;


}
