package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClientFeeConditions {

	@SerializedName("naoi")
	private boolean notApplicableOnInfant;

	@SerializedName("aoas")
	private Boolean isApplicableOnAllSegment;

	private Boolean isPaxWise;

}
