package com.tgs.services.fms.datamodel.airconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class DobOutput implements IRuleOutPut {

	@SerializedName("adobr")
	private boolean isAdultDobRequired;

	@SerializedName("cdobr")
	private boolean isChildDobRequired;

	@SerializedName("idobr")
	@Builder.Default
	private boolean isInfantDobRequired = true;

}
