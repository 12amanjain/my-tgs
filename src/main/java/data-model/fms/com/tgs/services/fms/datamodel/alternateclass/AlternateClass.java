package com.tgs.services.fms.datamodel.alternateclass;

import org.apache.commons.lang3.StringUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AlternateClass {

	private String classOfBook;

	private Integer seatCount;

	@Override
	public String toString() {
		return StringUtils.join(classOfBook, "-", seatCount);
	}

}
