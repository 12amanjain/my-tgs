package com.tgs.services.fms.datamodel.alternateclass;


import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AlternateClassFareSearch {

    private String priceId;

    // segment No VS class selected
    private Map<Integer, String> classData;
}
