package com.tgs.services.fms.datamodel.cancel;

import java.util.*;

import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.MessageType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.RestExclude;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Getter
@Setter
@Builder
public class AirCancellationDetail {

	@RestExclude
	@SerializedName("cnas")
	private Map<String, MessageInfo> cancellationNotAllowedSegments;

	@RestExclude
	@SerializedName("tvs")
	private Map<Long, List<FlightTravellerInfo>> travellers;

	@SerializedName("tcf")
	private Double totalCancellationFare;

	@SerializedName("tmr")
	private Double totalAmountToRefund;

	@SerializedName("aca")
	@APIUserExclude
	private Boolean autoCancellationAllowed;

	@SerializedName("jcd")
	private Map<String, JourneyCancellationDetail> journeyCancellationDetails;

	private String errorMessage;


	public Map<String, MessageInfo> getNotAllowedSegments() {
		if (cancellationNotAllowedSegments == null) {
			cancellationNotAllowedSegments = new HashMap<>();
		}
		return cancellationNotAllowedSegments;
	}

	public Map<Long, List<FlightTravellerInfo>> getTravellers() {
		if (travellers == null) {
			travellers = new HashMap<Long, List<FlightTravellerInfo>>();
		}
		return travellers;
	}


	public void buildCancellationDetail(String bookingId, BookingSegments bookingSegments, String errorMessage) {
		if (bookingSegments != null && CollectionUtils.isNotEmpty(bookingSegments.getCancelSegments())) {
			if (StringUtils.isNotBlank(errorMessage)) {
				buildMessageInfo(bookingId, bookingSegments, errorMessage);
			} else {
				bookingSegments.getCancelSegments().forEach(segmentInfo -> {
					getTravellers().put(Long.valueOf(segmentInfo.getId()), segmentInfo.getTravellerInfo());
				});
			}
		}

		if (MapUtils.isNotEmpty(bookingSegments.getMessages())) {
			getNotAllowedSegments().putAll(bookingSegments.getMessages());
		}
	}

	public void buildMessageInfo(String bookingid, BookingSegments bookingSegments, String errorMessage) {
		if (StringUtils.isNotBlank(errorMessage) && CollectionUtils.isNotEmpty(bookingSegments.getCancelSegments())) {
			MessageInfo messageInfo =
					MessageInfo.builder().type(MessageType.CANCELLATION).message(errorMessage).build();
			log.info("Adding Error Message {} for bookingId {}", errorMessage, bookingid);
			bookingSegments.getCancelSegments().forEach(segmentInfo -> {
				this.getNotAllowedSegments().put(segmentInfo.getSegmentKey(), messageInfo);
			});
			bookingSegments.setSegmentInfos(null);
			this.setErrorMessage(errorMessage);
		}
	}

	public Double getTotalCancellationFare() {
		if (totalCancellationFare == null) {
			return 0.0;
		}
		return totalCancellationFare;
	}

	public void addMessageToSystemRemarks(String message) {
		MessageInfo messageInfo = MessageInfo.builder().message(message).type(MessageType.CANCELLATION).build();
		getNotAllowedSegments().put(RandomStringUtils.random(2), messageInfo);
		setJourneyCancellationDetails(null);
	}

	public Double getCancellationCharges() {
		AtomicDouble cancellationFee = new AtomicDouble(0);
		if (MapUtils.isNotEmpty(travellers)) {
			getTravellers().forEach((segmentId, travellers) -> {
				travellers.forEach(traveller -> {
					Map<FareComponent, Double> fareComponents = traveller.getFareDetail().getFareComponents();
					for (FareComponent fareComponent : fareComponents.keySet()) {
						if (FareComponent.getCancellationComponents().contains(fareComponent)) {
							cancellationFee
									.getAndAdd(fareComponent.getAmount(fareComponents.getOrDefault(fareComponent, 0d)));
						}
					}
				});
			});
		}
		return cancellationFee.doubleValue();
	}

	public Double getPaxTypeCancellationCharges() {
		AtomicDouble cancellationFee = new AtomicDouble(0);
		Set<PaxType> paxTypes = new HashSet<>();
		if (MapUtils.isNotEmpty(travellers)) {
			getTravellers().forEach((segmentId, travellers) -> {
				travellers.forEach(traveller -> {
					if (!paxTypes.contains(traveller.getPaxType())) {
						Map<FareComponent, Double> fareComponents = traveller.getFareDetail().getFareComponents();
						for (FareComponent fareComponent : fareComponents.keySet()) {
							if (FareComponent.getCancellationComponents().contains(fareComponent)) {
								cancellationFee.getAndAdd(
										fareComponent.getAmount(fareComponents.getOrDefault(fareComponent, 0d)));
								paxTypes.add(traveller.getPaxType());
							}
						}
					}
				});
			});
		}
		return cancellationFee.doubleValue();
	}
}

