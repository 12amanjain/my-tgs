package com.tgs.services.fms.datamodel.cancel;

import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail.AirCancellationDetailBuilder;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
public class JourneyCancellationDetail {

	
	@SerializedName("fd")
	private Map<PaxType, FareDetail> fareDetails;
	
	@SerializedName("pnr")
	private String pnr;
	
	@SerializedName("pc")
	private Map<PaxType, Integer> paxCount;
	
	@SerializedName("dd")
	private String departureDate;
	
	@SerializedName("dac")
	private String departureAirportCode;
	
	@SerializedName("aac")
	private String arrivalAirportCode;
	
	@SerializedName("fts")
	private List<FlightTravellerInfo> travellerInfos;
}
