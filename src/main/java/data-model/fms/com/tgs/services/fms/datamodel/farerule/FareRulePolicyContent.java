package com.tgs.services.fms.datamodel.farerule;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.FareComponent;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FareRulePolicyContent {

	private Double amount;
	private Double additionalFee;
	private String policyInfo;

	@SerializedName("fcs")
	private Map<FareComponent, Double> fareComponents;

	public Map<FareComponent, Double> getFareComponents() {
		if (fareComponents == null)
			fareComponents = new HashMap<>();
		return fareComponents;
	}

}