package com.tgs.services.fms.datamodel.farerule;

public enum FareRulePolicyType {

	CANCELLATION, DATECHANGE, SPECIALRETURN, NO_SHOW, SEAT_CHARGEABLE;

}