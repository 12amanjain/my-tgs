package com.tgs.services.fms.datamodel.ssr;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaggageSSRInformation extends SSRInformation {
	private Integer quantity;
	private String unit;
}
