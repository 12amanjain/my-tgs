package com.tgs.services.fms.datamodel.ssr;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.SystemError;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SSRInformation implements Validatable, VoidClean {
	@DBExclude
	private String key;
	private String code;
	private Double amount;
	private String desc;

	@DBExclude
	@Exclude
	private SSRMiscInfo miscInfo;

	public SSRMiscInfo getMiscInfo() {
		if (miscInfo == null) {
			miscInfo = SSRMiscInfo.builder().build();
		}
		return miscInfo;
	}

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (code == null && desc == null) {
			errors.put("code", SystemError.NULL_SSR_CODE_DESC.getErrorDetail());
			errors.put("desc", SystemError.NULL_SSR_CODE_DESC.getErrorDetail());
		}
		return errors;
	}

	@Override
	public void cleanData() {
		setKey(StringUtils.defaultIfBlank(key, null));
		setCode(StringUtils.defaultIfBlank(code, null));
		setDesc(StringUtils.defaultIfBlank(desc, null));
	}

	@Override
	public boolean isVoid() {
		return key==null && code==null && amount==null && desc==null;
	}
}
