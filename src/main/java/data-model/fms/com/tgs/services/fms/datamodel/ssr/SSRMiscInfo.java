package com.tgs.services.fms.datamodel.ssr;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SSRMiscInfo {
	// for TBO
	private int journeyType;

	private String seatCodeType;

	private List<String> paxSsrKeys;

}
