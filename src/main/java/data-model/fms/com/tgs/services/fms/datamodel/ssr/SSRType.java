package com.tgs.services.fms.datamodel.ssr;

public enum SSRType {

    BAGGAGE(BaggageSSRInformation.class),
    MEAL(MealSSRInformation.class),
    SEAT(SeatSSRInformation.class),
    EXTRASERVICES(ExtraServiceSSRInformation.class);

    public Class<? extends SSRInformation> classType;

    private <T extends SSRInformation> SSRType(Class<T> classType) {
        this.classType = classType;
    }
}
