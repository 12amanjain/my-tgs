package com.tgs.services.fms.datamodel.ssr;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SeatPosition {

    private int row;

    private int column;
}
