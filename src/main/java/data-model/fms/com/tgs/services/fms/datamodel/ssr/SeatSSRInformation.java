package com.tgs.services.fms.datamodel.ssr;

import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.Exclude;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SeatSSRInformation extends SSRInformation {

	private String seatNo;

	private SeatPosition seatPosition;

	private Boolean isBooked;
	private Boolean isLegroom;
	private Boolean isAisle;

	@DBExclude
	@Exclude
	private Integer seatGroup;

	@ApiModelProperty(hidden = true)
	public String getSeatColumn() {
		return getSeatNo().replaceAll("[0-9]", "");
	}
}
