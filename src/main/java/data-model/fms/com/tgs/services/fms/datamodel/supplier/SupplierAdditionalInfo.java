package com.tgs.services.fms.datamodel.supplier;

import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = false)
public class SupplierAdditionalInfo implements IRuleOutPut {
	/**
	 * Currently there are 4 types of keys are supported in the {@link #fareTypes} viz. ONEWAY,RETURN,INTL,DEFAULT.
	 * 
	 * If {@link #fareTypes} doesn't have any value based on type of journey then set in DEFAULT
	 */
	private Map<String, String> fareTypes;
	/**
	 * Currently there are 4 types of keys are supported in the {@link #productClasses} viz. ONEWAY,RETURN,INTL,DEFAULT
	 * 
	 * * If {@link #productClasses} doesn't have any value based on type of journey then set in DEFAULT
	 */
	private Map<String, String> productClasses;

	/**
	 * This will contain the list of Airline that we want to be searched from a supplier
	 * 
	 */
	@SerializedName("ias")
	private List<String> includedAirlines;

	/**
	 * 
	 * This will contain the list of airlines that we want to retain after search from a supplier
	 */
	@SerializedName("aal")
	private List<String> allowedAirlines;

	@SerializedName("eal")
	private List<String> excludedAirlines;

	/**
	 * {@link #accountCodes} are useful only in case of GDS. LCC this parameter doesn't have any existence
	 */

	private List<String> accountCodes;
	private String bookingSupplierId;

	private List<SupplierRule> ticketingSuppliers;
	private List<SupplierRule> bookingSuppliers;

	private List<KeyValue> ticketingSupplierIds;

	private List<KeyValue> bookingSupplierIds;

	// Navitaire && TravelPort
	private String fareClass;

	@SerializedName("baa")
	private List<String> bspAllowedAirlines;

	@SerializedName("ipr")
	private Boolean isPANRequired;

	public String getTicketingSupplierId(SegmentInfo segmentInfo, int priceIndex) {
		return getSupplierId(segmentInfo, priceIndex, ticketingSupplierIds);
	}

	public String getBookingSupplierId(SegmentInfo segmentInfo, int priceIndex) {
		return getSupplierId(segmentInfo, priceIndex, bookingSupplierIds);
	}

	public String getSupplierId(SegmentInfo segmentInfo, int priceIndex, List<KeyValue> supplierIdMap) {
		if (CollectionUtils.isNotEmpty(supplierIdMap)) {
			for (KeyValue supplierIdPair : supplierIdMap) {
				if (supplierIdPair.getKey().equals("*")) {
					return supplierIdPair.getValue();
				} else if (supplierIdPair.getKey().contains(segmentInfo.getAirlineCode(false))) {
					return supplierIdPair.getValue();
				} else if (segmentInfo.getPriceInfo(priceIndex) != null
						&& supplierIdPair.getKey().contains(segmentInfo.getAirlineCode(false) + "_"
								+ segmentInfo.getPriceInfo(priceIndex).getBookingClass(PaxType.ADULT))) {
					return supplierIdPair.getValue();
				} else if (segmentInfo.getBookingRelatedInfo() != null && supplierIdPair.getKey()
						.contains(segmentInfo.getAirlineCode(false) + "_" + segmentInfo.getBookingRelatedInfo()
								.getTravellerInfo().get(0).getFareDetail().getClassOfBooking())) {
					return supplierIdPair.getValue();
				}
			}
		}
		return null;
	}

	public String getFareTypes(AirSearchQuery searchQuery) {
		if (searchQuery.getSearchType().equals(SearchType.ONEWAY) && searchQuery.getIsDomestic()) {
			return fareTypes.getOrDefault(searchQuery.getSearchType().name(), fareTypes.get("DEFAULT"));
		} else if (searchQuery.getSearchType().equals(SearchType.RETURN) && searchQuery.getIsDomestic()) {
			return fareTypes.getOrDefault(searchQuery.getSearchType().name(), fareTypes.get("DEFAULT"));
		} else if (searchQuery.isIntl()) {
			return fareTypes.getOrDefault("INTL", fareTypes.get("DEFAULT"));
		}
		return null;
	}

	public String getProductClasses(AirSearchQuery searchQuery) {
		if (searchQuery.getSearchType().equals(SearchType.ONEWAY) && searchQuery.getIsDomestic()) {
			return productClasses.getOrDefault(searchQuery.getSearchType().name(), productClasses.get("DEFAULT"));
		} else if (searchQuery.getSearchType().equals(SearchType.RETURN) && searchQuery.getIsDomestic()) {
			return productClasses.getOrDefault(searchQuery.getSearchType().name(), productClasses.get("DEFAULT"));
		} else if (searchQuery.isIntl()) {
			return productClasses.getOrDefault("INTL", productClasses.get("DEFAULT"));
		}
		return null;
	}

	@Override
	public void cleanData() {
		if (MapUtils.isNotEmpty(fareTypes)) {
			fareTypes = TgsMapUtils.removeKeyIfEmpty(fareTypes);
		}
		if (MapUtils.isNotEmpty(productClasses)) {
			productClasses = TgsMapUtils.removeKeyIfEmpty(productClasses);
		}
		if (TgsCollectionUtils.isEmptyStringCollection(accountCodes)) {
			accountCodes = null;
		}

		if (TgsCollectionUtils.isEmpty(ticketingSupplierIds)) {
			ticketingSupplierIds = null;
		}

		if (TgsCollectionUtils.isEmpty(bookingSupplierIds)) {
			bookingSupplierIds = null;
		}

		if (TgsCollectionUtils.isEmpty(bookingSuppliers)) {
			bookingSuppliers = null;
		} else {
			for (SupplierRule bookingSupplier : bookingSuppliers) {
				bookingSupplier.getExclusionCriteria().cleanData();
				bookingSupplier.getInclusionCriteria().cleanData();
			}
		}

		if (TgsCollectionUtils.isEmpty(ticketingSuppliers)) {
			ticketingSuppliers = null;
		} else {
			for (SupplierRule ticketingSupplier : ticketingSuppliers) {
				ticketingSupplier.getExclusionCriteria().cleanData();
				ticketingSupplier.getInclusionCriteria().cleanData();
			}
		}

		if (TgsCollectionUtils.isEmptyStringCollection(includedAirlines)) {
			includedAirlines = null;
		}
		if (TgsCollectionUtils.isEmptyStringCollection(excludedAirlines)) {
			excludedAirlines = null;
		}
		if (TgsCollectionUtils.isEmptyStringCollection(allowedAirlines)) {
			allowedAirlines = null;
		}
		if (TgsCollectionUtils.isEmptyStringCollection(bspAllowedAirlines)) {
			bspAllowedAirlines = null;
		}
	}
}
