package com.tgs.services.fms.datamodel.supplier;

import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch supplier configuration. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class SupplierConfigurationFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on id ", example = "1")
	private Long id;

	@ApiModelProperty(notes = "To fetch based on sourceid ", example = "1")
	private List<Integer> sourceIds;

	@ApiModelProperty(notes = "To fetch based on supplierIds ", example = "Sabre")
	private List<String> supplierIds;

	@ApiModelProperty(notes = "To fetch based on deleted rule", example = "true")
	private boolean isDeleted;

}
