package com.tgs.services.fms.datamodel.supplier;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.base.helper.MaskedField;
import com.tgs.services.base.utils.TgsObjectUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SupplierCredential {

	private String userName;

	@MaskedField(unmaskedEnd = 2)
	private String password;
	private String url;

	@Deprecated
	@MaskedField(unmaskedEnd = 2)
	private String txnPassword;

	/**
	 * Applicable in case of GDS to identify queue no
	 */
	private String queueNo;
	/**
	 * Applicable in case of GDS to identify pcc7
	 */
	private String pcc;

	/**
	 * Used in case of LCC
	 */
	private String domain;

	/**
	 * For supplier level dealing currency
	 */
	private String currencyCode;
	/**
	 * Used in case of LCC
	 */
	private String organisationCode;

	/**
	 * This is required for airlines hosted on radixx
	 */
	private String iataNumber;
	/**
	 * This is required for airlines hosted on radixx
	 */
	private String agentUserName;
	/**
	 * This is required for airlines hosted on radixx
	 */
	@MaskedField(unmaskedEnd = 2)
	private String agentPassword;
	/**
	 * This is required for TBO & used for navitaire version
	 */
	private String clientId;

	private String accountingCode;

	/**
	 * This is very very crucial variable , Don't set this to true in your local
	 * until and unless you are sure that you are using test credentials .
	 * 
	 * In case you will set this variable true for any other credentials than test
	 * credentials then booking will start happening on production environment which
	 * will lead to losses.
	 * 
	 */
	private Boolean isTestCredential;

	/**
	 * This is Case of Some Suppliers which Default Required GST Mandatory Ex:
	 * IndiGo SME , IndiGo Corp
	 */
	private Boolean isGSTMandatory;

	private Map<String, String> fareTypes;

	private String providerCode;

	public void cleanData() {
		TgsObjectUtils.getNotNullFieldValueMap(this, false, false).forEach((k, v) -> {
			if (v instanceof String && StringUtils.isBlank((String) v)) {
				try {
					java.lang.reflect.Field prop = this.getClass().getDeclaredField(k);
					prop.setAccessible(true);
					prop.set(this, null);
				} catch (Exception e) {

				}
			}
		});
	}

}
