package com.tgs.services.fms.datamodel.supplier;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch based on supplier info. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class SupplierInfoFilter extends QueryFilter {

}
