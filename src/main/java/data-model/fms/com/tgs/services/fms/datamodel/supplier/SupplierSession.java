package com.tgs.services.fms.datamodel.supplier;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SupplierSession {

    private Long id;

    private String bookingId;

    private SupplierSessionInfo supplierSessionInfo;

    private Integer sourceId;

    private String supplierId;

    private LocalDateTime createdOn;
    
    private LocalDateTime expiryTime;

    private Long ttl;
    
}
