package com.tgs.services.fms.datamodel.supplier;

import lombok.Getter;

@Getter
public enum SupplierType {

    GDS("GDS"), LCC("LCC");

    private String code;

    SupplierType(String code) {
        this.code = code;
    }

    public String getName() {
        return this.code;
    }

    public static SupplierType getEnumFromCode(String code) {
        return getSupplierType(code);
    }

    private static SupplierType getSupplierType(String code) {
        for (SupplierType action : SupplierType.values()) {
            if (action.getCode().equals(code))
                return action;
        }
        return null;
    }
}
