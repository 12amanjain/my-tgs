package com.tgs.services.fms.ruleengine;

import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightAPIURLRuleCriteria implements IRuleOutPut {

	// 0
	private String securityURL;

	// 1
	private String pricingURL;

	// 2
	private String ssrURL;

	// 3
	private String reservationURL;

	// 4
	private String assesfeeURL;

	// 5
	private String pnrPaymentURL;

	// 6
	private String travelAgent;

	// 7
	private String host;

	// 8
	private String fareRuleURL;

	// 9
	private String logOutURL;

	// 10
	private String flightURL;

	// 11
	private String retrieveURL;

	// 12
	private String fareValidateURL;

	// 13
	private String fetchCreditShellURL;

	// 14
	private String validateCreditShellURL;

	// 15
	private String creditShellPaymentURL;

}
