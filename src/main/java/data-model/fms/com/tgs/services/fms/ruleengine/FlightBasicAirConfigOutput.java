package com.tgs.services.fms.ruleengine;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightBasicAirConfigOutput implements IRuleOutPut {

	private String value;

	public void cleanData () {
		setValue(StringUtils.defaultIfBlank(value, null));
	}
}
