package com.tgs.services.fms.ruleengine;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@ToString
public class FlightBasicFact extends GeneralBasicFact implements IFact {

	private String airline;
	private Set<String> bookingClasses;
	/**
	 * Rule Engine will check against all flight number , if any of them get satisfied then it will considered as valid
	 * rule
	 */
	private Set<String> flightNumbers;
	private Set<String> fareBasisSet;
	private TimePeriod travelPeriod;
	private List<RouteInfo> routeInfos;
	private List<RouteInfo> destinations;
	private AirType airType;
	private SupplierType supplierType;
	private SearchType searchType;
	private Integer sourceId;
	private Set<CabinClass> cabinClasses;
	private String supplierId;
	private Integer segmentNumber;
	private PaxType paxType;
	private Boolean isDirectFlight;
	private Boolean isConnectingFlight;
	private Boolean isCustomCombination;
	private String corporateCode;
	private String productClass;
	private Set<String> operatingAirline;
	private Map<FareComponent, Double> fareComponents;
	private Map<FareComponent, Double> tripFareComponents;
	private String accountCode;
	private String fareType;
	private Set<String> equipTypes;
	private List<Long> layovers;

	public static FlightBasicFact createFact() {
		return FlightBasicFact.builder().build();
	}

	public static FlightBasicFact getNonNull(FlightBasicFact fact) {
		if (fact == null) {
			return createFact();
		}
		return fact;
	}

	public FlightBasicFact generateFact(AirSearchQuery searchQuery) {
		TimePeriod travelPeriod = new TimePeriod();
		LocalDateTime travelStartTime =
				LocalDateTime.of(searchQuery.getRouteInfos().get(0).getTravelDate(), LocalTime.MIN);
		travelPeriod.setStartTime(travelStartTime);
		LocalDateTime travelEndTime = null;
		for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
			travelEndTime = LocalDateTime.of(routeInfo.getTravelDate(), LocalTime.of(23, 59));
			travelPeriod.setEndTime(travelEndTime);
		}
		setTravelPeriod(travelPeriod);

		setAirType(searchQuery.getAirType());
		setSearchType(searchQuery.getSearchType());

		cabinClasses = new HashSet<>();
		cabinClasses.add(searchQuery.getCabinClass());

		setRouteInfos(searchQuery.getRouteInfos());
		setDestinations(searchQuery.getRouteInfos());
		return this;
	}

	public FlightBasicFact generateFact(SupplierBasicInfo basicInfo) {
		setSupplierId(basicInfo.getSupplierId());
		setSourceId(basicInfo.getSourceId());
		return this;
	}

	public FlightBasicFact generateFact(SegmentInfo segmentInfo) {
		setSegmentNumber(segmentInfo.getSegmentNum());
		SupplierBasicInfo supplierBasicInfo = segmentInfo.getSupplierInfo();
		if (supplierBasicInfo != null) {
			setSourceId(supplierBasicInfo.getSourceId());
			setSupplierId(supplierBasicInfo.getSupplierId());
		}
		setOperatingAirline(new HashSet<>(Arrays.asList(segmentInfo.getAirlineCode(true))));
		setAirline(segmentInfo.getPlatingCarrier(null));
		// setFlightNumbers(new
		// HashSet<String>(Arrays.asList(segmentInfo.getFlightNumber()))); might be
		// required in manual order
		TimePeriod travelPeriod = new TimePeriod();
		travelPeriod.setStartTime(segmentInfo.getDepartTime());
		travelPeriod.setEndTime(segmentInfo.getArrivalTime());
		setTravelPeriod(travelPeriod);
		return this;
	}

	/**
	 * Populates fields in this {@code flightFact} using {@code tripInfo}
	 *
	 * @param tripInfo
	 * @return This {@code flightFact}.
	 */
	public FlightBasicFact generateFact(TripInfo tripInfo) {
		setAirline(tripInfo.getPlatingCarrier());
		setRouteInfos(tripInfo.getRouteInfos());
		setTravelPeriod(tripInfo.getTravelPeriod());
		setOperatingAirline(tripInfo.getAirlineInfo(true));
		if (tripInfo.isPriceInfosNotEmpty()) {
			setSourceId(tripInfo.getSupplierInfo().getSourceId());
			setSupplierId(tripInfo.getSupplierInfo().getSupplierId());
		}
		setFlightNumbers(tripInfo.getFlightNumberSet());
		setEquipTypes(tripInfo.getEquipmentSet());
		/**
		 * Generate fact for booking flow or first pricing.
		 */
		generateFact(tripInfo, 0);
		return this;
	}


	public FlightBasicFact generateFact(TripInfo tripInfo, int priceInfoIndex) {
		setBookingClasses(tripInfo.getBookingClassSet(priceInfoIndex));
		setFareBasisSet(tripInfo.getFareBasisSet(priceInfoIndex));
		setCabinClasses(tripInfo.getCabinClassSet(priceInfoIndex));
		setFareType(tripInfo.getFareType(priceInfoIndex).iterator().next());
		return this;
	}

	/**
	 * Populates fields in this {@code flightFact} using {@code priceInfo}.
	 *
	 * @param priceInfo {@code PriceInfo}
	 * @return This {@code flightFact}.
	 */
	public FlightBasicFact generateFact(PriceInfo priceInfo) {
		setSourceId(priceInfo.getSourceId());
		setSupplierId(priceInfo.getSupplierId());
		setAccountCode(priceInfo.getAccountCode());
		setFareType(priceInfo.getFareType());
		if (priceInfo.getPlattingCarrier() != null) {
			setAirline(priceInfo.getPlattingCarrier());
		}
		return this;
	}

	public FlightBasicFact generateFact(FareDetail fareDetail) {
		setFareComponents(fareDetail.getFareComponents());
		return this;
	}

	/**
	 * Populates fields in {@code flightFact} using {@code priceInfo} and {@code paxType}.
	 *
	 * @param priceInfo {@code PriceInfo}
	 * @param paxType {@code PaxType}
	 * @return This {@code flightFact}.
	 */
	public FlightBasicFact generateFact(PriceInfo priceInfo, PaxType paxType) {
		setPaxType(paxType);
		FareDetail fareDetail = priceInfo.getFareDetail(paxType, new FareDetail());
		generateFact(priceInfo).generateFact(fareDetail);
		return this;
	}

	public AirType getAirType(AirType defaultType) {
		return airType != null ? airType : defaultType;
	}

}
