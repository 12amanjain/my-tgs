package com.tgs.services.fms.ruleengine;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.fms.datamodel.Route;
import com.tgs.services.fms.datamodel.supplier.SupplierType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FlightBasicRuleCriteria extends GeneralBasicRuleCriteria {

	@SerializedName("ril")
	private List<Route> routeInfoList;

	private String destinations;

	/**
	 * Used Temporary, can be removed in future
	 */
	private String routes;

	@SerializedName("tp")
	private List<TimePeriod> travelPeriod;

	@SerializedName("bp")
	private List<TimePeriod> bookingPeriod;

	@SerializedName("al")
	private List<String> airlineList;

	@SerializedName("fbl")
	private List<String> fareBasisList;

	@SerializedName("bcl")
	private List<String> bookingClassList;

	@SerializedName("pcl")
	private List<String> productClassList;

	private List<PaxType> paxTypes;

	@SerializedName("ccl")
	private List<String> corporateCodesList;

	@SerializedName("sois")
	private List<Integer> sourceIds;

	@SerializedName("cc")
	private List<CabinClass> cabinClasses;

	@SerializedName("fnl")
	private String flightNumberList;

	private AirType airType;

	private SupplierType supplierType;

	private SearchType searchType;

	@SerializedName("sid")
	private List<String> supplierIds;

	@SerializedName("iaos")
	private Boolean isApplicableOnAllSegments;

	@SerializedName("icc")
	private Boolean isCustomCombination;

	@SerializedName("icf")
	private Boolean isConnectingFlight;

	@SerializedName("idf")
	private Boolean isDirectFlight;

	@SerializedName("oc")
	private List<String> operatingCarriers;

	@SerializedName("pac")
	private List<String> privateAccountCodes;

	@SerializedName("adw")
	private String allowedDaysOfWeek;

	@SerializedName("pfr")
	private PriceFareRange priceFareRange;

	@SerializedName("ft")
	private List<FareType> fareTypes;

	@SerializedName("etl")
	private List<String> equipTypeList;

	@SerializedName("lovr")
	private String layoverRangeInMin;

	@Override
	public void cleanData() {
		setUserIds(TgsCollectionUtils.getCleanStringList(getUserIds()));

		setSourceIds(TgsCollectionUtils.getNonNullElements(getSourceIds()));

		setFareBasisList(TgsCollectionUtils.getCleanStringList(getFareBasisList()));

		setBookingClassList(TgsCollectionUtils.getCleanStringList(getBookingClassList()));

		setProductClassList(TgsCollectionUtils.getCleanStringList(getProductClassList()));

		setPaxTypes(TgsCollectionUtils.getNonNullElements(getPaxTypes()));

		setSupplierIds(TgsCollectionUtils.getCleanStringList(getSupplierIds()));

		setAirlineList(TgsCollectionUtils.getCleanStringList(getAirlineList()));

		setCorporateCodesList(TgsCollectionUtils.getCleanStringList(getCorporateCodesList()));

		setOperatingCarriers(TgsCollectionUtils.getCleanStringList(getOperatingCarriers()));

		setPrivateAccountCodes(TgsCollectionUtils.getCleanStringList(getPrivateAccountCodes()));

		setTravelPeriod(TgsCollectionUtils.getNonNullElements(getTravelPeriod()));

		setBookingPeriod(TgsCollectionUtils.getNonNullElements(getBookingPeriod()));

		setCabinClasses(TgsCollectionUtils.getNonNullElements(getCabinClasses()));

		setFareTypes(TgsCollectionUtils.getNonNullElements(getFareTypes()));

		setEquipTypeList(TgsCollectionUtils.getCleanStringList(getEquipTypeList()));


		if (StringUtils.isBlank(this.getRoutes())) {
			this.setRoutes(null);
		} else {
			List<String> routeList = new ArrayList<>(Arrays.asList(this.getRoutes().split(",")));
			routeList = TgsCollectionUtils.getCleanStringList(routeList);
			String routes = routeList.toString();
			// it is length-safe
			setRoutes(routes.substring(1, routes.length() - 1));
		}

		if (StringUtils.isBlank(getDestinations())) {
			setDestinations(null);
		} else {
			List<String> routeList = new ArrayList<>(Arrays.asList(getDestinations().split(",")));
			routeList = TgsCollectionUtils.getCleanStringList(routeList);
			String routes = routeList.toString();
			// it is length-safe
			setDestinations(routes.substring(1, routes.length() - 1));
		}

		if (StringUtils.isBlank(this.getFlightNumberList())) {
			this.setFlightNumberList(null);
		} else {
			/*
			 * Arrays.asList(...) gives Arrays.ArrayList which doesn't support add/remove, so use java.util.ArrayList
			 */
			List<String> flightNumberList = new ArrayList<>(Arrays.asList(this.getFlightNumberList().split(",")));
			flightNumberList = TgsCollectionUtils.getCleanStringList(flightNumberList);
			String flightNumbers = flightNumberList.toString();
			// it is length-safe
			setFlightNumberList(flightNumbers.substring(1, flightNumbers.length() - 1));
		}

		if (priceFareRange != null && priceFareRange.isVoid()) {
			setPriceFareRange(null);
		}

		if (StringUtils.isBlank(allowedDaysOfWeek)) {
			setAllowedDaysOfWeek(null);
		}
		if (StringUtils.isBlank(layoverRangeInMin)) {
			setLayoverRangeInMin(null);
		}
	}
}
