package com.tgs.services.fms.ruleengine;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.BiPredicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.fms.datamodel.Route;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public enum FlightBasicRuleField implements IRuleField {

	AIRLINELIST {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getAirlineList()) || input.getAirlineList().contains("*")
					|| input.getAirlineList().contains(fact.getAirline());
		}
	},
	FAREBASISLIST {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (CollectionUtils.isEmpty(input.getFareBasisList())) {
				return true;
			}

			if (CollectionUtils.isEmpty(fact.getFareBasisSet()))
				return false;

			for (String fareBasis : fact.getFareBasisSet()) {
				if (input.getFareBasisList().contains(fareBasis)) {
					return true;
				}
			}

			return false;
		}

	},
	BOOKINGCLASSLIST {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (CollectionUtils.isEmpty(input.getBookingClassList())) {
				return true;
			}

			if (CollectionUtils.isEmpty(fact.getBookingClasses()))
				return false;

			for (String bookingClass : fact.getBookingClasses()) {
				if (input.getBookingClassList().contains(bookingClass)) {
					return true;
				}
			}

			return false;
		}
	},
	ROUTEINFOLIST {
		public List<Route> getRouteList(String routes) {
			List<Route> routeList = new ArrayList<>();
			if (StringUtils.isNotBlank(routes)) {
				try {
					String[] routeArry = routes.split(",");
					for (int i = 0; i < routeArry.length; i++) {
						routeList.add(new Route(routeArry[i]));
					}
				} catch (Exception e) {
					log.error("Unable to parse routes , Invalid routes {}", routes);
				}
			}
			return routeList;
		}

		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			BiPredicate<List<RouteInfo>, String> routesValidator = (routeInfos, routes) -> {
				AtomicBoolean isValid = new AtomicBoolean(true);
				for (Route routeInfo : this.getRouteList(routes)) {
					isValid.set(routeInfo.isValidRoute(routeInfo.getKey(), routeInfos));
					if (isValid.get()) {
						return true;
					}
				}
				return isValid.get();
			};

			/**
			 * If "destinations" are set in criteria validate destinations in fact. Otherwise, "routes" must be set.
			 */
			if (StringUtils.isNotBlank(input.getDestinations())) {
				return routesValidator.test(fact.getDestinations(), input.getDestinations());
			}
			return routesValidator.test(fact.getRouteInfos(), input.getRoutes());
		}
	},
	ROUTES {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return ROUTEINFOLIST.isInputValidAgainstFact(fact, input);
		}
	},
	DESTINATIONS {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return ROUTEINFOLIST.isInputValidAgainstFact(fact, input);
		}
	},
	TRAVELPERIOD {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			for (TimePeriod travelPeriod : input.getTravelPeriod()) {
				if (fact.getTravelPeriod() != null
						&& (travelPeriod.getStartTime().isEqual(fact.getTravelPeriod().getStartTime())
								|| travelPeriod.getStartTime().isBefore(fact.getTravelPeriod().getStartTime()))
						&& (travelPeriod.getEndTime().isEqual(fact.getTravelPeriod().getEndTime())
								|| travelPeriod.getEndTime().isAfter(fact.getTravelPeriod().getEndTime()))) {
					return true;
				}
			}
			return CollectionUtils.isEmpty(input.getTravelPeriod());
		}
	},
	BOOKINGPERIOD {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			LocalDateTime now = LocalDateTime.now();
			for (TimePeriod bookingPeriod : input.getBookingPeriod()) {
				if ((bookingPeriod.getStartTime().isEqual(now) || bookingPeriod.getStartTime().isBefore(now))
						&& (bookingPeriod.getEndTime().isEqual(now) || bookingPeriod.getEndTime().isAfter(now))) {
					return true;
				}
			}
			return CollectionUtils.isEmpty(input.getBookingPeriod());
		}
	},
	USERIDS {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getUserIds()) || input.getUserIds().contains(fact.getUserId());
		}
	},
	AIRTYPE {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return input.getAirType().equals(AirType.ALL) || input.getAirType().equals(fact.getAirType());
		}
	},
	SUPPLIERTYPE {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return input.getSupplierType().equals(fact.getSupplierType());
		}
	},
	SEARCHTYPE {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return input.getSearchType().equals(SearchType.ALL) || input.getSearchType().equals(fact.getSearchType());
		}
	},

	SOURCEIDS {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getSourceIds()) || input.getSourceIds().contains(fact.getSourceId());
		}
	},
	CABINCLASSES {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (CollectionUtils.isEmpty(input.getCabinClasses())) {
				return true;
			}

			if (CollectionUtils.isEmpty(fact.getCabinClasses()))
				return false;

			for (CabinClass cabinClass : fact.getCabinClasses()) {
				if (input.getCabinClasses().contains(cabinClass)) {
					return true;
				}
			}

			return false;
		}
	},
	SUPPLIERIDS {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getSupplierIds())
					|| input.getSupplierIds().contains(fact.getSupplierId());
		}
	},
	ISAPPLICABLEONALLSEGMENTS {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return BooleanUtils.isTrue(input.getIsApplicableOnAllSegments()) || fact.getSegmentNumber() == 0;
		}
	},
	PAXTYPES {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getPaxTypes()) || input.getPaxTypes().contains(fact.getPaxType());
		}
	},
	ISCUSTOMCOMBINATION {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return input.getIsCustomCombination() != null
					? input.getIsCustomCombination().equals(fact.getIsCustomCombination())
					: false;
		}
	},
	ISCONNECTINGFLIGHT {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return input.getIsConnectingFlight() != null
					? input.getIsConnectingFlight().equals(fact.getIsConnectingFlight())
					: false;
		}
	},
	ISDIRECTFLIGHT {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return input.getIsDirectFlight() != null ? input.getIsDirectFlight().equals(fact.getIsDirectFlight())
					: false;
		}
	},
	FLIGHTNUMBERLIST {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (StringUtils.isEmpty(input.getFlightNumberList()))
				return true;

			if (CollectionUtils.isEmpty(fact.getFlightNumbers()))
				return false;

			String[] allowedFlightNumbers = input.getFlightNumberList().split(",");

			for (String flightNumber : allowedFlightNumbers) {
				Pattern pattern = Pattern.compile(flightNumber.trim());
				for (String fn : fact.getFlightNumbers()) {
					Matcher matcher = pattern.matcher(fn);
					if (matcher.matches()) {
						return true;
					}
				}
			}
			return false;
		}
	},
	CORPORATECODESLIST {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getCorporateCodesList())
					|| input.getCorporateCodesList().contains(fact.getCorporateCode());
		}
	},
	PRODUCTCLASSLIST {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getProductClassList())
					|| input.getProductClassList().contains(fact.getProductClass());
		}
	},
	PRIVATEACCOUNTCODES {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (StringUtils.isEmpty(fact.getAccountCode())) {
				return false;
			}
			return CollectionUtils.isEmpty(input.getPrivateAccountCodes())
					|| input.getPrivateAccountCodes().contains(fact.getAccountCode());
		}
	},

	OPERATINGCARRIERS {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (CollectionUtils.isEmpty(fact.getOperatingAirline()))
				return false;

			for (String operatingAirline : fact.getOperatingAirline()) {
				if (!input.getOperatingCarriers().contains(operatingAirline)) {
					// if any one segment operating airline doesn't match then set entire trip is
					// not valid
					return false;
				}

			}
			return true;
		}

	},
	ALLOWEDDAYSOFWEEK {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			LocalDateTime currentDate = fact.getTravelPeriod().getStartTime();
			int dayOfWeek = currentDate.getDayOfWeek().getValue();
			/**
			 * the day-of-week, from 1 (Monday) to 7 (Sunday) input : example : 1,3,5,7
			 * 
			 * @see java.time.DayOfWeek
			 */
			if (input.getAllowedDaysOfWeek().contains(String.valueOf(dayOfWeek))) {
				return true;
			}
			return false;
		}
	},

	PRICEFARERANGE {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {

			boolean isValid = false;
			if (MapUtils.isNotEmpty(fact.getTripFareComponents())) {
				PriceFareRange priceFareRange = input.getPriceFareRange();
				AtomicDouble paxFare = new AtomicDouble(0);

				if (CollectionUtils.isEmpty(priceFareRange.getFareComponents())) {
					// Default if empty fare component , then consider BF
					priceFareRange.getFareComponents().add(FareComponent.BF);
				}
				priceFareRange.getFareComponents().forEach(fc -> {
					paxFare.getAndAdd(fact.getTripFareComponents().getOrDefault(fc, 0.0));
				});

				isValid = priceFareRange.isValidFare(paxFare);

			}
			return isValid;
		}
	},
	FARETYPES {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			try {
				return CollectionUtils.isEmpty(input.getFareTypes())
						|| input.getFareTypes().contains(FareType.valueOf(fact.getFareType()));
			} catch (Exception e) {
				log.error("Unable to find relevant FareType for " + fact.getFareType());
			}
			return false;
		}
	},
	EQUIPTYPELIST {

		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			if (CollectionUtils.isNotEmpty(input.getEquipTypeList())
					&& CollectionUtils.isNotEmpty(fact.getEquipTypes())) {
				return input.getEquipTypeList().containsAll(fact.getEquipTypes());
			}
			return true;
		}

	},

	LAYOVERRANGEINMIN {

		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			boolean isvalid = false;
			String layoverRange = input.getLayoverRangeInMin();
			if (StringUtils.isNotEmpty(layoverRange) && CollectionUtils.isNotEmpty(fact.getLayovers())) {
				List<String> range = Arrays.asList(layoverRange.split("-"));
				for (Long connectingTime : fact.getLayovers()) {
					if ((connectingTime > Integer.valueOf(range.get(0)))
							&& (connectingTime < Integer.valueOf(range.get(1)))) {
						isvalid = true;
						break;
					}
				}
			}
			return isvalid;
		}
	},

	ROLES {
		@Override
		public boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getRoles()) || input.getRoles().contains(fact.getRole());
		}

	};

	public abstract boolean isInputValidAgainstFact(FlightBasicFact fact, FlightBasicRuleCriteria rule);

	@Override
	public boolean isValidAgainstFact(IFact fact, IRuleCriteria rule) {
		return this.isInputValidAgainstFact((FlightBasicFact) fact, (FlightBasicRuleCriteria) rule);
	}

	// public static boolean isValidSector(AirportInfo rule, AirportInfo fact) {
	// if(rule.getCode())
	// }

}
