package com.tgs.services.fms.supplier.indigo;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Account {

	AccountCredits accountCredits;

}
