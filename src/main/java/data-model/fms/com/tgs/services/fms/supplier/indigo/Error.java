package com.tgs.services.fms.supplier.indigo;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Error {

	String errorCode;
	String errorType;
	String message;
	String severity;

}
