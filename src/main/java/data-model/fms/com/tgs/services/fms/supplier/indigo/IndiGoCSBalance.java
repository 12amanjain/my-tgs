package com.tgs.services.fms.supplier.indigo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndiGoCSBalance {

	private Account account;

	private String bookingAmount;

	private int bookingBalanceDue;

	private int cSAmountAvailable;

	private int cSBalanceAmount;

	private String recordLocator;

	private String strCSAmountAvailable;

	private String strTotalCsAmount;

	private String cSExpirationDateTime;
}
