package com.tgs.services.fms.supplier.indigo;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class IndiGoError {

	List<Error> errors;
}
