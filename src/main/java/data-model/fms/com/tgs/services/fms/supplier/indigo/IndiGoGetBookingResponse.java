package com.tgs.services.fms.supplier.indigo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IndiGoGetBookingResponse extends IndiGoBaseError {

	IndiGoCSBalance indigoCSBalance;

}
