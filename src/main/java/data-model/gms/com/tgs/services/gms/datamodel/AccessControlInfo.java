package com.tgs.services.gms.datamodel;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccessControlInfo {
    private String endPointUrl;

    private String methodType;

    @SerializedName("auids")
    private List<String> allowedUserIds;

    @SerializedName("aroles")
    private List<String> allowedRoles;

    @SerializedName("aips")
    private List<String> allowedIps;

    @SerializedName("ears")
    private List<String> emulatedAllowedRoles;
    
    @SerializedName("ekeys")
    private List<String> exclusionKeys;
    
    @SerializedName("ikeys")
    private List<String> inclusionKeys;
    
}
