package com.tgs.services.gms.datamodel;

import java.util.List;

import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;

@Getter
public class ClientBankConfiguration implements IRuleOutPut {

	List<BankAccountInfo> bankList;

}
