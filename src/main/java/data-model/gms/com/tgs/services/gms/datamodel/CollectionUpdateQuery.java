package com.tgs.services.gms.datamodel;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.base.helper.JsonStringSerializer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CollectionUpdateQuery extends BulkUploadQuery{
	
	private Boolean enabled;
	
	@JsonAdapter(JsonStringSerializer.class)
	private String data;
	
	private String key;
	
	private String type;

}
