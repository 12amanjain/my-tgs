package com.tgs.services.gms.datamodel;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryInfo {

    private String name;
    private String code;
    private String isoCode;
    private String mobileCode;


    @ApiModelProperty(hidden = true)
    public String getCountry() {
        return this.name;
    }
}
