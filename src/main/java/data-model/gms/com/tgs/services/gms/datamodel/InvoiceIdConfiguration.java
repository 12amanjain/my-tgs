package com.tgs.services.gms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class InvoiceIdConfiguration {

	@SerializedName("iimt")
	private InvoiceIdManagerType invoiceIdManagerType;

	@SerializedName("adip")
	private String airDomesticIdPrefix;

	@SerializedName("aiip")
	private String airInternationalIdPrefix;

	@SerializedName("cadip")
	private String corporateAirDomesticIdPrefix;

	@SerializedName("caiip")
	private String corporateAirInternationalIdPrefix;

	@SerializedName("chadip")
	private String chargedAirDomesticIdPrefix;

	@SerializedName("chaiip")
	private String chargedAirInternationalIdPrefix;

	@SerializedName("vadip")
	private String voidAirDomesticIdPrefix;

	@SerializedName("vaiip")
	private String voidAirInternationalIdPrefix;

	@SerializedName("radip")
	private String refundAirDomesticIdPrefix;

	@SerializedName("raiip")
	private String refundAirInternationalIdPrefix;

	@SerializedName("hip")
	private String hotelIdPrefix;

}
