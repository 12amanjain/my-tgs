package com.tgs.services.gms.datamodel;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class InvoiceIdData {

	private String prefix;
	private int numericInvoiceId;
	private String suffix;

}
