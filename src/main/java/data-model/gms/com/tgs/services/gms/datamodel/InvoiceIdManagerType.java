package com.tgs.services.gms.datamodel;

public enum InvoiceIdManagerType {

	DEFAULT;

	public static InvoiceIdManagerType getInvoiceIdManagerType(String name) {
		for (InvoiceIdManagerType type : InvoiceIdManagerType.values()) {
			if (type.name().equals(name))
				return type;
		}
		return null;
	}

}
