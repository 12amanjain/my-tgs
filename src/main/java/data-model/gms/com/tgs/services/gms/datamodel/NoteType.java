package com.tgs.services.gms.datamodel;

import lombok.Getter;

@Getter
public enum NoteType {
	FAREJUMP("F"),
	GENERAL("G"),
	BOOKING_FAILED("BF"),
	BOOKING_CANCELLATION_FAILED("BCF"),
	PNR_RELEASED("PR"),
	SMSFAILURE("S"),
	WHATSAPP("W"),
	AMENDMENT("A"),
	SUPPLIER_MESSAGE("SM"),
	MANUAL_CONFIRMATION("MC"),
	SPECIAL_SERVICE_REQUEST("SSR"),
	AUTO_ABORT("AA");

	private String code;

	private NoteType(String code) {
		this.code = code;
	}

	public static NoteType getEnumFromCode(String code) {
		return getNoteType(code);
	}

	public static NoteType getNoteType(String code) {
		for (NoteType action : NoteType.values()) {
			if (action.getCode().equals(code))
				return action;
		}
		return null;
	}
}