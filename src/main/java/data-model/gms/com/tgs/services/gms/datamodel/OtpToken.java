package com.tgs.services.gms.datamodel;

import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.UserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OtpToken {
	/*
	 * It should be userId in case of logged-in user and can be email or mobile for anonymous user
	 */
	private String key;

	@NotNull
	private String requestId;

	/* Type is used to find out for which type otp is required */
	@NotNull
	private String type;
	private Boolean smsreq;
	private Boolean emailreq;
	private String mobile;
	private UserRole role;
	private Boolean isConsumed;
	@SerializedName("maxattempt")
	private Integer maxAttemptCount;
	private String email;
	@SerializedName("et")
	private Integer expiryTimeInMinutes;
}
