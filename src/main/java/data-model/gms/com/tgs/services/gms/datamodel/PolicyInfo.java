package com.tgs.services.gms.datamodel;

import javax.validation.constraints.NotNull;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.helper.JsonStringSerializer;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class PolicyInfo {
	@NotNull
	@ApiModelProperty(notes = "Specify policy type. This is used to differentiate policy type", required = true, example = "ui")
	private String type;
	@NotNull
	@ApiModelProperty(notes = "Unique key to differentiate policy", required = true, example = "commission")
	private String key;

	private Boolean enabled;
	@ApiModelProperty(notes = "This is required at the time of updation", example = "12")
	private Integer id;
	@JsonAdapter(JsonStringSerializer.class)
	@ApiModelProperty(notes = "Data associated with policy. It will return value store in persitence storage", example = "{\"view\": \"accounts\", \"url\" :\"/accounts\", \"fields\":[ {\"name\":\"product_type\", \"value\" : \"AirOrder\"}]}")
	private String data;

	@ApiModelProperty(notes = "Policy Name", example = "Commission View")
	private String name;

	@ApiModelProperty(notes = "Policy description. To describe about policy", example = "Commission View Limited Access")
	private String description;

	@NotNull
	@ApiModelProperty(notes = "Every policy should be associated to group", required = true, example = "agent_group")
	private String groupName;
}
