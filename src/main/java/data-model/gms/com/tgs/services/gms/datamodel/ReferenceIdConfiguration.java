package com.tgs.services.gms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class ReferenceIdConfiguration {

	@SerializedName("rimb")
	private String referenceIdManagerBean = "defaultReferenceIdGenerator";

	private String prefix;

	private String amdIdPrefix;

	@SerializedName("random")
	private Integer randomStringLength;

	@SerializedName("msl")
	private Integer minSequenceLength;
}
