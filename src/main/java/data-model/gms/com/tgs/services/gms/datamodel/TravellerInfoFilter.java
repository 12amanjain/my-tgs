package com.tgs.services.gms.datamodel;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.enums.PaxType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class TravellerInfoFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch traveller by name")
	private String name;

	@ApiModelProperty(notes = "To fetch traveller by paxtype")
	private PaxType pt;

}
