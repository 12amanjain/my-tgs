package com.tgs.services.gms.datamodel;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class WhatsAppConsentInfo {
    private List<String> recipientNumbers;

    private String type;

}
