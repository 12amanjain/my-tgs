package com.tgs.services.gms.datamodel.abusecontrol;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ApiRateLimit {

	@SerializedName("url")
	private String endPointUrl;

	@Builder.Default
	private int priority = 0;

	@SerializedName("rlc")
	private List<RateLimitCombination> rateLimitCombinations;

	@SerializedName("apal")
	private List<ApplicationRateLimit> applicationRateLimit;

	@SerializedName("hal")
	private List<ApplicationRateLimit> hostWiseRateLimit;

	public List<RateLimitCombination> getRateLimitCombinations() {
		if (rateLimitCombinations == null)
			rateLimitCombinations = new ArrayList<>();
		return rateLimitCombinations;
	}

	public List<ApplicationRateLimit> getApplicationRateLimit() {
		if (applicationRateLimit == null)
			applicationRateLimit = new ArrayList<>();
		return applicationRateLimit;
	}

	public List<ApplicationRateLimit> getHostWiseRateLimit() {
		if (hostWiseRateLimit == null)
			hostWiseRateLimit = new ArrayList<>();
		return hostWiseRateLimit;
	}
}
