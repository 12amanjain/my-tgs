package com.tgs.services.gms.datamodel.abusecontrol;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RateLimitCombination {

	@SerializedName("cuid")
	private Boolean considerUserId;

	@SerializedName("cip")
	private Boolean considerIp;

	@SerializedName("cdid")
	private Boolean considerDeviceId;

	// applicable for certain combinations specified in the key
	@SerializedName("alfc")
	private Map<String, Map<TimeUnit, RateLimit>> allowedLimitForCombinations;

	// applicable for all combinations
	@SerializedName("al")
	private Map<TimeUnit, RateLimit> allowedApiLimit;

	// applicable for all combinations of api user
	@SerializedName("api")
	private Map<TimeUnit, RateLimit> allowedApiLimitForApiUser;

	// applicable for all combinations of partner user
	@SerializedName("ptr")
	private Map<TimeUnit, RateLimit> allowedApiLimitForPartnerUser;

	// applicable for all combinations of guest user on b2c platform
	@SerializedName("b2cg")
	private Map<TimeUnit, RateLimit> allowedApiLimitForGuest;

	// applicable for all combinations of customer user on b2c platform
	@SerializedName("b2cc")
	private Map<TimeUnit, RateLimit> allowedApiLimitForCustomer;

	public Map<String, Map<TimeUnit, RateLimit>> getAllowedLimitForCombinations() {
		if (allowedLimitForCombinations == null)
			allowedLimitForCombinations = new HashMap<>();
		return allowedLimitForCombinations;
	}

	public Map<TimeUnit, RateLimit> getAllowedApiLimit() {
		if (allowedApiLimit == null)
			allowedApiLimit = new HashMap<>();
		return allowedApiLimit;
	}

	public Map<TimeUnit, RateLimit> getAllowedApiLimitForApiUser() {
		if (allowedApiLimitForApiUser == null)
			allowedApiLimitForApiUser = new HashMap<>();
		return allowedApiLimitForApiUser;
	}

	public Map<TimeUnit, RateLimit> getAllowedApiLimitForPartnerUser() {
		if (allowedApiLimitForPartnerUser == null)
			allowedApiLimitForPartnerUser = new HashMap<>();
		return allowedApiLimitForPartnerUser;
	}
}
