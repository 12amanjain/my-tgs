package com.tgs.services.gms.datamodel.configurator;

import java.time.LocalDateTime;

import javax.validation.constraints.NotNull;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class ConfiguratorInfo implements IRule {

	private Long id;
	private LocalDateTime createdOn;
	private Boolean enabled;

	@NotNull
	private ConfiguratorRuleType ruleType;

	private GeneralBasicRuleCriteria inclusionCriteria;

	private GeneralBasicRuleCriteria exclusionCriteria;

	@NotNull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	private Double priority;
	private Boolean exitOnMatch;

	@Exclude
	private boolean isDeleted;

	@Override
	public IRuleOutPut getOutput() {
		return GsonUtils.getGson().fromJson(output, ruleType.getOutPutTypeToken().getType());
	}

	@Override
	public double getPriority() {
		return priority;
	}

	@Override
	public boolean exitOnMatch() {
		return exitOnMatch;
	}

	@Override
	public boolean getEnabled() {
		return enabled;
	}
}
