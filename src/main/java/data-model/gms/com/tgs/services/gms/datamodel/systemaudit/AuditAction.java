package com.tgs.services.gms.datamodel.systemaudit;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum AuditAction {

    LOGIN("L"),
    ORDER_CREATE("ON"),
    ORDER_CANCEL("OC"),
    ORDER_CONFIRM("CB"),
    ORDER_OPEN("OO"),
    USER_UPDATE("UU"),
    EMULATE("EM"),
    MANUAL_ORDER_CREATE("MC"),
    REISSUED("RE");

    private String code;

    AuditAction(String code) {
        this.code = code;
    }

    public static AuditAction getEnumFromCode(String code) {
        return getAuditAction(code);
    }

    public static AuditAction getAuditAction(String auditCode) {
        for (AuditAction action : AuditAction.values()) {
            if (action.getCode().equals(auditCode))
                return action;
        }
        return null;
    }

    public static List<String> getCodes(List<AuditAction> actions) {
        List<String> auditCodes = new ArrayList<>();
        actions.forEach(action -> {
            auditCodes.add(action.getCode());
        });
        return auditCodes;
    }

}