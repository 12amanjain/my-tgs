package com.tgs.services.gms.datamodel.systemaudit;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Getter
@Setter
@Builder
public class SystemAudit {

    private LocalDateTime createdOn;

    private String userId;

    private AuditAction auditType;

    private String ip;

    private String bookingId;

    private String loggedInUserId;
    
}