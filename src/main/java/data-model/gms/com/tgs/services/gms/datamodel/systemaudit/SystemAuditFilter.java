package com.tgs.services.gms.datamodel.systemaudit;

import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@ApiModel(value = "This is used to fetch system audit based on search criteria. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class SystemAuditFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch from date to particular date ", example = "")
	private LocalDateTime createdOn;

	@ApiModelProperty(notes = "To fetch to particular date ", example = "")
	private LocalDateTime endOn;

	@ApiModelProperty(notes = "To fetch based on audit Type ", example = "LOGIN")
	private List<AuditAction> auditTypes;

	@ApiModelProperty(notes = "To fetch based on booking id ", example = "123*****")
	private String bookingId;

	@ApiModelProperty(notes = "To fetch based on parent userid ", example = "60001")
	private String userId;

	@ApiModelProperty(notes = "To fetch based on action performed or emulated user id ", example = "50024")
	private String loggedInUserId;
}