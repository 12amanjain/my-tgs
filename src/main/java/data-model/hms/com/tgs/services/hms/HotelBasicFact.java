package com.tgs.services.hms;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

import com.tgs.services.base.ruleengine.GeneralBasicFact;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@SuperBuilder
public class HotelBasicFact extends GeneralBasicFact implements IFact {

	private Integer supplierId;
	private TimePeriod travelPeriod;
	private Integer sourceId;
	private List<Integer> sourceIds;
	private String userId;
	private LocalDate checkinDate;
	private HotelSearchType hotelSearchType;
	private Integer starRating;
	private String cityId;
	private String cityName;
	private String countryId;
	private String countryName;
	private String hotelId;
	private String giataId;
	private String hName;
	private AirType hotelType;
	private Boolean isCrossSellPreferred;
	private Double price;
	private String propertyType;

	public static HotelBasicFact createFact() {
		return HotelBasicFact.builder().build();
	}

	public HotelBasicFact generateFactFromSearchQuery(HotelSearchQuery searchQuery) {

		TimePeriod travelPeriod = new TimePeriod();
		if (!ObjectUtils.isEmpty(searchQuery.getCheckinDate()) && !ObjectUtils.isEmpty(searchQuery.getCheckoutDate())) {
			LocalDateTime travelStartTime = LocalDateTime.of(searchQuery.getCheckinDate(), LocalTime.MIN);
			travelPeriod.setStartTime(travelStartTime);

			LocalDateTime travelEndTime = LocalDateTime.of(searchQuery.getCheckinDate(), LocalTime.of(23, 59));
			travelPeriod.setEndTime(travelEndTime);
			setTravelPeriod(travelPeriod);
		}

		setSourceId(searchQuery.getSourceId());
		setSourceIds(searchQuery.getSourceIds());
		setSupplierId(!ObjectUtils.isEmpty(searchQuery.getMiscInfo())
				&& StringUtils.isNotBlank(searchQuery.getMiscInfo().getSupplierId())
				&& NumberUtils.isParsable(searchQuery.getMiscInfo().getSupplierId())
						? Integer.parseInt(searchQuery.getMiscInfo().getSupplierId())
						: null);

		if (!ObjectUtils.isEmpty(searchQuery.getSearchPreferences())) {
			isCrossSellPreferred =
					(!ObjectUtils.isEmpty(searchQuery.getSearchPreferences().getCrossSellParameter())) ? true : false;
		}

		if (!ObjectUtils.isEmpty(searchQuery.getSearchCriteria())) {
			setCityId(searchQuery.getSearchCriteria().getCityId());
			setCountryName(searchQuery.getSearchCriteria().getCountryName());
			setCountryId(searchQuery.getSearchCriteria().getCountryId());
			setCityName(searchQuery.getSearchCriteria().getCityName());
		}
		return this;
	}

	public HotelBasicFact generateFactFromUserId(String userId) {

		setUserId(userId);
		return this;
	}

	public HotelBasicFact generateFactFromHotelInfo(HotelInfo hotelInfo) {

		if (hotelInfo.isAddressPresent()) {
			if (hotelInfo.getAddress().isCityPresent())
				setCityName(hotelInfo.getAddress().getCity().getName());
			if (hotelInfo.getAddress().isCountryPresent())
				setCountryName(hotelInfo.getAddress().getCountry().getName());

		}
		setGiataId(hotelInfo.getGiataId());
		setStarRating(hotelInfo.getRating());
		if (!ObjectUtils.isEmpty(hotelInfo.getMiscInfo())) {
			setHotelId(hotelInfo.getMiscInfo().getSupplierStaticHotelId());
		}
		setPropertyType(hotelInfo.getPropertyType());
		setHName(StringUtils.upperCase(hotelInfo.getName()));
		if (CollectionUtils.isNotEmpty(hotelInfo.getProcessedOptions()))
			setPrice(hotelInfo.getProcessedOptions().get(0).getTotalPrice());
		return this;
	}

	public HotelBasicFact generateFactFromOption(Option option) {

		Double totalPrice = option.getRoomInfos().stream().mapToDouble(roomInfo -> {
			return roomInfo.getTotalPrice();
		}).sum();
		setPrice(totalPrice);
		return this;
	}


}
