package com.tgs.services.hms;

import java.time.LocalDate;
import java.util.List;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBasicRuleCriteria extends GeneralBasicRuleCriteria implements Validatable {

	@SerializedName("sid")
	private List<Integer> supplierIds;

	@SerializedName("srcids")
	private List<Integer> sourceIds;

	@SerializedName("bp")
	private List<TimePeriod> bookingPeriod;

	@SerializedName("tp")
	private List<TimePeriod> travelPeriod;

	@SerializedName("chkdt")
	private List<LocalDate> checkinDateList;

	@SerializedName("hst")
	private List<HotelSearchType> hotelSearchTypeList;

	@SerializedName("sr")
	private List<Integer> starRatingList;

	@SerializedName("cityIds")
	private List<String> cityIdList;

	@SerializedName("cityNames")
	private List<String> cityNameList;

	@SerializedName("countryIds")
	private List<String> countryIdList;

	@SerializedName("countryNames")
	private List<String> countryNameList;

	@SerializedName("gids")
	private List<String> giataIdList;

	@SerializedName("hids")
	private List<String> hotelIds;

	@SerializedName("hotelNames")
	private List<String> hotelNameList;

	private AirType hotelType;
	@SerializedName("icsf")
	private Boolean isCrossSellPreferred;

	@SerializedName("pt")
	private List<String> propertyTypeList;

	@SerializedName("pfr")
	private PriceFareRange priceFareRange;

	@SerializedName("adw")
	private String allowedDaysOfWeek;


	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (!CollectionUtils.isEmpty(bookingPeriod)) {
			int i = 0;
			for (TimePeriod bPeriod : bookingPeriod) {
				if (bPeriod != null) {
					if (bPeriod.getEndTime().isBefore(bPeriod.getStartTime())) {
						errors.put("bookingPeriod[" + i++ + "].", SystemError.INVALID_BOOKING_PERIOD.getErrorDetail());
					}
				}
			}
		}

		if (!CollectionUtils.isEmpty(checkinDateList)) {
			int i = 0;
			for (LocalDate checkinDate : checkinDateList) {
				if (checkinDate != null) {
					if (checkinDate.isBefore(LocalDate.now())) {
						errors.put("checkinDateList[" + i++ + "]",
								SystemError.HOTEL_CHECKINDATE_VALIDATION.getErrorDetail());
					}
				}
			}
		}

		if (!CollectionUtils.isEmpty(starRatingList)) {
			int i = 0;
			for (Integer starRating : starRatingList) {
				if (starRating != null) {
					if (starRating < 0 || starRating > 5) {
						errors.put("starRatingList[" + i++ + "]", SystemError.INVALID_STAR_RATING.getErrorDetail());
					}
				}
			}
		}
		return errors;
	}

	@Override
	public void cleanData() {
		setSourceIds(TgsCollectionUtils.getNonNullElements(getSourceIds()));
		setSupplierIds(TgsCollectionUtils.getNonNullElements(getSupplierIds()));
		setUserIds(TgsCollectionUtils.getCleanStringList(getUserIds()));
		setStarRatingList(TgsCollectionUtils.getNonNullElements(getStarRatingList()));
		setCityIdList(TgsCollectionUtils.getCleanStringList(getCityIdList()));
		setCityNameList(TgsCollectionUtils.getCleanStringList(getCityNameList()));
		setCountryIdList(TgsCollectionUtils.getCleanStringList(getCountryIdList()));
		setCountryNameList(TgsCollectionUtils.getCleanStringList(getCountryNameList()));
		setGiataIdList(TgsCollectionUtils.getCleanStringList(getGiataIdList()));
		setHotelIds(TgsCollectionUtils.getCleanStringList(getHotelIds()));
		setHotelNameList(TgsCollectionUtils.getCleanStringList(getHotelNameList()));
		setPropertyTypeList(TgsCollectionUtils.getCleanStringList(getPropertyTypeList()));
		setTravelPeriod(TgsCollectionUtils.getNonNullElements(getTravelPeriod()));
		setBookingPeriod(TgsCollectionUtils.getNonNullElements(getBookingPeriod()));

		if (StringUtils.isBlank(allowedDaysOfWeek)) {
			setAllowedDaysOfWeek(null);
		}

		if (StringUtils.isBlank(allowedDaysOfWeek)) {
			setAllowedDaysOfWeek(null);
		}

		if (priceFareRange != null && priceFareRange.isVoid()) {
			setPriceFareRange(null);
		}

		if (CollectionUtils.isEmpty(hotelSearchTypeList)) {
			setHotelSearchTypeList(null);
		}

		if (CollectionUtils.isEmpty(bookingPeriod)) {
			setBookingPeriod(null);
		}

		if (CollectionUtils.isEmpty(travelPeriod)) {
			setTravelPeriod(null);
		}
	}
}
