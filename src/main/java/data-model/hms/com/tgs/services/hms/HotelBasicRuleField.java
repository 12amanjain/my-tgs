package com.tgs.services.hms;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleField;

public enum HotelBasicRuleField implements IRuleField {

	SUPPLIERIDS {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getSupplierIds())
					|| input.getSupplierIds().contains(fact.getSupplierId());
		}
	},
	SOURCEIDS {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			if (!ObjectUtils.isEmpty(fact.getSourceId())) {
				return CollectionUtils.isEmpty(input.getSourceIds())
						|| input.getSourceIds().contains(fact.getSourceId());
			} else if (CollectionUtils.isNotEmpty(fact.getSourceIds())) {
				return CollectionUtils.isEmpty(input.getSourceIds())
						|| !Collections.disjoint(input.getSourceIds(), fact.getSourceIds());
			} else {
				return true;
			}
		}
	},
	USERIDS {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getUserIds()) || input.getUserIds().contains(fact.getUserId());
		}
	},
	TRAVELPERIOD {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			for (TimePeriod travelPeriod : input.getTravelPeriod()) {
				if ((travelPeriod.getStartTime().isEqual(fact.getTravelPeriod().getStartTime())
						|| travelPeriod.getStartTime().isBefore(fact.getTravelPeriod().getStartTime()))
						&& (travelPeriod.getEndTime().isEqual(fact.getTravelPeriod().getEndTime())
								|| travelPeriod.getEndTime().isAfter(fact.getTravelPeriod().getEndTime()))) {
					return true;
				}
			}
			return CollectionUtils.isEmpty(input.getTravelPeriod());
		}
	},
	BOOKINGPERIOD {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			LocalDateTime now = LocalDateTime.now();
			for (TimePeriod bookingPeriod : input.getBookingPeriod()) {
				if ((bookingPeriod.getStartTime().isEqual(now) || bookingPeriod.getStartTime().isBefore(now))
						&& (bookingPeriod.getEndTime().isEqual(now) || bookingPeriod.getEndTime().isAfter(now))) {
					return true;
				}
			}
			return CollectionUtils.isEmpty(input.getBookingPeriod());
		}
	},
	CHECKINDATELIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			Optional<LocalDate> result =
					input.getCheckinDateList().stream().filter(fact.getCheckinDate()::isEqual).findFirst();
			return result.isPresent() ? true : CollectionUtils.isEmpty(input.getCheckinDateList());
		}
	},
	HOTELSEARCHTYPELIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getHotelSearchTypeList())
					|| input.getHotelSearchTypeList().contains(fact.getHotelSearchType());
		}
	},
	STARRATINGLIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getStarRatingList())
					|| input.getStarRatingList().contains(fact.getStarRating());
		}
	},
	CITYIDLIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getCityIdList()) || input.getCityIdList().contains(fact.getCityId());

		}
	},
	CITYNAMELIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getCityNameList()) || ObjectUtils.isEmpty(fact.getCityName())
					|| input.getCityNameList().stream().anyMatch(fact.getCityName()::equalsIgnoreCase);

		}
	},
	COUNTRYIDLIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getCountryIdList())
					|| input.getCountryIdList().contains(fact.getCountryId());
		}
	},
	COUNTRYNAMELIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getCountryNameList()) || ObjectUtils.isEmpty(fact.getCountryName())
					|| input.getCountryNameList().stream().anyMatch(fact.getCountryName()::equalsIgnoreCase);
		}
	},
	HOTELIDS {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getHotelIds()) || input.getHotelIds().contains(fact.getHotelId());
		}
	},
	GIATAIDLIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getGiataIdList())
					|| input.getGiataIdList().contains(fact.getGiataId());
		}
	},
	HOTELNAMELIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getHotelNameList()) || ObjectUtils.isEmpty(fact.getHName())
					|| input.getHotelNameList().stream().anyMatch(fact.getHName()::equalsIgnoreCase);
		}
	},
	HOTELTYPE {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return input.getHotelType().equals(AirType.ALL) || input.getHotelType().equals(fact.getHotelType());
		}
	},
	ISCROSSSELLPREFERRED {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return input.getIsCrossSellPreferred() != null
					? input.getIsCrossSellPreferred().equals(fact.getIsCrossSellPreferred())
					: false;
		}
	},
	ALLOWEDDAYSOFWEEK {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			LocalDateTime currentDate = fact.getTravelPeriod().getStartTime();
			int dayOfWeek = currentDate.getDayOfWeek().getValue();
			/**
			 * the day-of-week, from 1 (Monday) to 7 (Sunday) input : example : 1,3,5,7
			 *
			 * @see java.time.DayOfWeek
			 */
			if (input.getAllowedDaysOfWeek().contains(String.valueOf(dayOfWeek))) {
				return true;
			}
			return false;
		}
	},
	PRICEFARERANGE {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {

			boolean isValid = false;
			if (!ObjectUtils.isEmpty(fact.getPrice())) {
				PriceFareRange priceFareRange = input.getPriceFareRange();
				AtomicDouble optionAmount = new AtomicDouble(0);
				optionAmount.getAndAdd(fact.getPrice());
				isValid = priceFareRange.isValidFare(optionAmount);
			}
			return isValid;
		}
	},
	PROPERTYTYPELIST {
		@Override
		public boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getPropertyTypeList())
					|| input.getPropertyTypeList().contains(fact.getPropertyType());
		}
	};

	public abstract boolean isInputValidAgainstFact(HotelBasicFact fact, HotelBasicRuleCriteria rule);

	@Override
	public boolean isValidAgainstFact(IFact fact, IRuleCriteria rule) {
		return this.isInputValidAgainstFact((HotelBasicFact) fact, (HotelBasicRuleCriteria) rule);
	}

}
