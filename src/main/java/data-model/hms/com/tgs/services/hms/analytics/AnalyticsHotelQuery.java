package com.tgs.services.hms.analytics;

import java.util.List;
import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AnalyticsHotelQuery extends BaseAnalyticsQuery {

	/**
	 * Common to all
	 */

	private String errormsg;
	private Integer errorcode;
	private String externalapiflow;

	/**
	 * Search Parameters
	 */
	private String checkindate;
	private String checkoutdate;
	private String city;
	private String locality;
	private List<Integer> ratings;
	private String searchid;
	private String sourcename;
	private String suppliername;

	private Integer noofroom;
	private String adultchildcombination;
	/**
	 * Populate this variable from firstroomInfo
	 */
	private Integer numberofadults;
	/**
	 * Populate this variable from firstroomInfo
	 */
	private Integer numberofchild;
	private Integer noofroomnights;
	private String country;
	private String nationality;

	private Integer searchresultcount;

	/**
	 * Hotel Detail Parameters
	 */

	private String hotelname;
	/**
	 * This should be database id of static table
	 */
	private String hotelid;
	private String giataid;
	private Integer optionscount;
	private Boolean hotelfacilityavail;
	private Boolean imagesavail;
	private String longitude;
	private String latitude;
	private String propertytype;
	private Boolean ispreferredhotel;
	private String tripadvisorid;
	private Double totalprice;
	
	/**
	 * Hotel Cancellation Parameters
	 */
	private Boolean iszerocancellationallowed;
	private Boolean isonrequest;
	private String expirationdate;
	
	/**
	 * Hotel Review Parameters
	 */
	private Boolean roomfacilityavail;
	private String mealbasis;
	private String roomcategory;
	private String bookingid;

	/**
	 * Booking Parameters
	 */
	private String paymentmedium;
	private String bookingstatus;
	private String paymentstatus;
	private Boolean holdbooking;
	private String flowtype;
	
	/**
	 * Quotation Prarameters
	 */
	private String quotationname;
	
}
