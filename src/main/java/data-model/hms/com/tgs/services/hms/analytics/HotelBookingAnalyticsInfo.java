package com.tgs.services.hms.analytics;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelBookingAnalyticsInfo {

	private String bookingId;
	private String paymentMedium;
	private String paymentStatus;
	private String orderStatus;
	private String flowType;
	private HotelInfo hInfo;
	private HotelSearchQuery searchQuery;
}
