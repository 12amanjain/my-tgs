package com.tgs.services.hms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AdditionalHotelOrderItemInfo {
	
	private String propertyType;
	
	@SerializedName("phi")
	private ProcessedHotelInfo hInfo;
	
	@SerializedName("sbid")
	private String supplierBookingReference;
	
	@SerializedName("sbr")
	private String supplierBookingId;
	
	@SerializedName("sbcn")
	private String supplierBookingConfirmationNo;
	
	@SerializedName("si")
	private String supplierId;
	
	@SerializedName("hbr")
	private String hotelBookingReference;
	
	@SerializedName("bcr")
	private String bookingCancellationReference;
	
	@SerializedName("sbu")
	private String supplierBookingUrl;
	
	@SerializedName("cci")
	private Long creditCardAppliedId;
	
	//Agoda Specific
	@SerializedName("ifbs")
	private Boolean isFailedFromSupplier;
	
	@SerializedName("tsb")
	private String tempSupplierBookingId;
	
}
