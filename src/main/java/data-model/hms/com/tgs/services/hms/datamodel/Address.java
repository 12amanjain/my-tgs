package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class Address {

	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS_LINE1)
	private String addressLine1;
	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS_LINE2)
	private String addressLine2;
	private String postalCode;
	private City city;
	private State state;
	private Country country;
	private String address;
	
	public boolean isAddressLinePresent() {
		return this.getAddressLine1() != null? true: false;
	}

	public boolean isCityPresent() {
		return this.getCity() != null ? true : false;
	}
	
	public boolean isCountryPresent() {
		return this.getCountry() != null ? true : false;
	}

}
