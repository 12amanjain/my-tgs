package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AgentBookingTransactionQuery {

	private String agentName;

	private Double grossSale;

	private String agentCurrency;

	private Double grossSaleInBaseCurrency;

	private String systemCurrency;

	private Integer noOfBookings;
	
}
