package com.tgs.services.hms.datamodel;

import org.apache.commons.lang3.BooleanUtils;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CancellationMiscInfo {

	@CustomSerializedName(key = FieldName.IS_BOOKING_ALLOWED)
	private Boolean isBookingAllowed;

	@CustomSerializedName(key = FieldName.IS_SOLD_OUT)
	private Boolean isSoldOut;
	
	private Boolean isCancellationPolicyBelongToRoom;
	
	public boolean cancellationPolicyBelongToRoom() {
		if(BooleanUtils.isTrue(isCancellationPolicyBelongToRoom)) 
			return true;
		return false;
	}
	
}
