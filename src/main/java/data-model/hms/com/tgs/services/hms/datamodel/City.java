package com.tgs.services.hms.datamodel;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class City {
	private String code;
	private String name;
}