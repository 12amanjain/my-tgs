package com.tgs.services.hms.datamodel;

import java.util.Objects;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CityInfo {
	
	private Long id;
	private String cityName;
	private String countryId;
	private String countryName;
	private String iataCode;
	
	@Override
	public boolean equals(Object obj) {
		if(this == obj) return true;
		if(obj == null) return false;
		if (getClass() != obj.getClass()) { return false; }
		CityInfo city = (CityInfo)obj;
		if(!Objects.equals(this.getCityName(), city.getCityName()))
			return false;
		if(!Objects.equals(this.getCountryName(), city.getCountryName()))
			return false;
		return true;
	}
}
