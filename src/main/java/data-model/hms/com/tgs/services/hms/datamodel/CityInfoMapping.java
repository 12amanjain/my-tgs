package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CityInfoMapping {

	private Long cityId;
	private String supplierName;
	private String supplierCity;
	private String supplierCountry;
	private String supplierCityName;
}
