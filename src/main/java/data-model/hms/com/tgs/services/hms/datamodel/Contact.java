package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Contact {

	@CustomSerializedName(key = FieldName.HOTEL_PHONE)
	private String phone;
	@CustomSerializedName(key = FieldName.HOTEL_EMAIL)
	private String email;
	private String fax;
	
}
