package com.tgs.services.hms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CrossSellParameter {

	@SerializedName("cc")
	private String cabinClass;
	
	@SerializedName("ic")
	private String iataCode;
	
	@SerializedName("cso")
	private Boolean crossSellOnly;
}
