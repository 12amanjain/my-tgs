package com.tgs.services.hms.datamodel;

import java.time.LocalDate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class DayWisePriceInfo {
	private LocalDate date;
	private String day;
	private Double price;
}