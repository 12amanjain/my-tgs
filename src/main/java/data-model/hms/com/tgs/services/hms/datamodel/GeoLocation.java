package com.tgs.services.hms.datamodel;



import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter

@Builder
public class GeoLocation {

	@CustomSerializedName(key = FieldName.HOTEL_LONGITUDE)
	private String longitude;
	@CustomSerializedName(key = FieldName.HOTEL_LATITUDE)
	private String latitude;
}