package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelAPIcredentialInfo {

	private String username;
	private String password;
}
