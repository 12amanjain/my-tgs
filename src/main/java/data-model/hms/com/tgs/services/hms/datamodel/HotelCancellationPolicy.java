package com.tgs.services.hms.datamodel;

import java.util.List;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.helper.RestExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelCancellationPolicy {

	private String id;
	
	@CustomSerializedName(key = FieldName.ROOM_CANCELLATION_POLICY_LIST)
	List<HotelCancellationPolicy> roomCancellationPolicyList;

	@CustomSerializedName(key = FieldName.IS_FULL_REFUND_ALLOWED)
	private Boolean isFullRefundAllowed;
	
	@CustomSerializedName(key = FieldName.IS_NO_REFUND_ALLOWED)
	private Boolean isNoRefundAllowed;

	@CustomSerializedName(key = FieldName.PENALTY_DETAILS)
	List<PenaltyDetails> penalyDetails;
	
	@CustomSerializedName(key = FieldName.CANCELLATION_POLICY_STRING)
	private String cancellationPolicy;

	@RestExclude
	@CustomSerializedName(key = FieldName.CANCELLATION_POLICY_MISC_INFO)
	private CancellationMiscInfo miscInfo;
	
}
