package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelCancellationPolicyResult implements HotelResult {

	private String id;
	private HotelInfo hotel; 
	
}
