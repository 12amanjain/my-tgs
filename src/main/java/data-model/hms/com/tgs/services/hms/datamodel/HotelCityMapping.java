package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelCityMapping {

	private String countryCode;
	private String countryId;
	private String cityName;
	private String countryName;
	private String supplierCityId;
	private String supplierCountryId;
}
