package com.tgs.services.hms.datamodel;

import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelCityMappingQuery extends BulkUploadQuery {

	private String cityName;
	private String countryName;
	private String supplierCityId;
	private String supplierCountryId;
	private String supplierName;
	private String supplierCityName;
}
