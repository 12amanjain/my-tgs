package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import javax.validation.constraints.NotNull;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder(toBuilder = true)
@Getter
@Setter
@ToString
public class HotelConfiguratorInfo extends DataModel implements IRule {

	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;
	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;

	@SearchPredicate(type = PredicateType.EQUAL, filterName = "enable")
	private Boolean enabled;

	@NotNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private HotelConfiguratorRuleType ruleType;

	private HotelBasicRuleCriteria inclusionCriteria;

	private HotelBasicRuleCriteria exclusionCriteria;

	@NotNull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	private Double priority;
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean exitOnMatch;

	@Exclude
	@SearchPredicate(type = PredicateType.EQUAL)
	private boolean isDeleted;

	private String description;

	@Override
	public IRuleOutPut getOutput() {
		return GsonUtils.getGson().fromJson(output, ruleType.getOutPutTypeToken().getType());
	}

	@Override
	public IRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public IRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public double getPriority() {
		return priority == null ? 0.0 : priority;
	}

	@Override
	public boolean exitOnMatch() {
		return exitOnMatch == null ? false : exitOnMatch;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}
	
	public void cleanData() {
		if (getPriority() < 0.0) {
			setPriority(0.0);
		}
		if (inclusionCriteria != null)
			inclusionCriteria.cleanData();
		if (exclusionCriteria != null)
			exclusionCriteria.cleanData();
	}
}
