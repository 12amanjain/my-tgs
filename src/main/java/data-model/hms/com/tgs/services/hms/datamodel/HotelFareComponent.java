

package com.tgs.services.hms.datamodel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import lombok.Getter;

@Getter
public enum HotelFareComponent {

	TF("TotalFare") {},
	BF("Base Fare") {},
	SP("Supplier Price") {},
	TAF("Taxes and Fee") {},
	NF("Net Fare") {},
	OT("Other") {
		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	GST("GST"),
	MF("Management Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public List<HotelFareComponent> dependentComponents() {
			return Arrays.asList(MFT);
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	MFT("Management Fee Taxes") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	PRF("Property Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	MU("Mark Up") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	MNT("Mandatory Tax") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	MNF("Mandatory Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	PF("Payment Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	ST("Sales Tax") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	EPF("Extra Person Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	RF("Resort Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	DS("Discount") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}

		@Override
		public double getAmount(double amount) {
			return -1 * amount;
		}
		
		@Override
		public boolean isPriceValidationRequired() {
			return true;
		}
	},
	CMU("Client Mark Up") {
		@Override
		public boolean keepCurrentComponent() {
			return false;
		}
		
		@Override
		public boolean isPriceValidationRequired() {
			return false;
		}
	},
	GDSF("GDS Fees") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	TSF("Tax And Service Fee") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	SDS("Supplier Discount") {

		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	SNP("Supplier Net Price") {

		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	SGP("Supplier Gross Price") {

		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	CGST("CGST") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	SGST("SGST") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	IGST("IGST") {
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	TMF("Supplier Total Marketing Fees") {
		
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}
		
		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	SRC("Supplier Retention Commission") {
		
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}
		
		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	SAC("Supplier Agent Comission") {
		
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}
		
		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	MUP("Supplier Client Markup") {
	
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}
		
		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	HAF("Hotel Amendment Fee"){
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	HAFT("Hotel Amendment Fee Tax"){
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	CAF("Client Amendment Fee"){
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	CAFT("Client Amendment Fee Tax"){
		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	RP("Redeemed Points") {

		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	},
	VD("Voucher Discount") {

		@Override
		public HotelFareComponent mapComponent() {
			return HotelFareComponent.TAF;
		}

		@Override
		public double getAmount(double amount) {
			return -1 * amount;
		}

		@Override
		public boolean keepCurrentComponent() {
			return true;
		}
	};

	private String desc;

	HotelFareComponent(String desc) {
		this.desc = desc;
	}

	public List<HotelFareComponent> dependentComponents() {
		return new ArrayList<>();
	}

	public HotelFareComponent mapComponent() {
		return null;
	}

	public boolean keepCurrentComponent() {
		return false;
	}
	
	public boolean isPriceValidationRequired() {
		return false;
	}

	public double getAmount(double amount) {
		return amount;
	}

	public static Set<HotelFareComponent> getNetFareComponents() {
		Set<HotelFareComponent> components = new HashSet<>();
		components.add(HotelFareComponent.MU);
		components.add(HotelFareComponent.DS);
		return components;
	}

	public static Set<HotelFareComponent> getTotalFareComponents() {
		Set<HotelFareComponent> components = new HashSet<>();
		components.add(HotelFareComponent.MU);
		components.add(HotelFareComponent.BF);
		components.add(HotelFareComponent.MF);
		components.add(HotelFareComponent.MFT);
		components.add(HotelFareComponent.TMF);
		components.add(HotelFareComponent.CMU);
		components.add(HotelFareComponent.TSF);
		components.add(HotelFareComponent.ST);
		return components;
	}

	public boolean isComputationRequired() {
		if(isGstComponent())
			return false;
		
		Set<HotelFareComponent> components = new HashSet<>();
		components.add(HotelFareComponent.PRF);
		components.add(HotelFareComponent.MNT);
		components.add(HotelFareComponent.ST);
		components.add(HotelFareComponent.EPF);
		components.add(HotelFareComponent.SRC);
		components.add(HotelFareComponent.TMF);
		components.add(HotelFareComponent.SAC);
		components.add(HotelFareComponent.MUP);
		components.add(HotelFareComponent.TSF);
		components.add(HotelFareComponent.SDS);
		components.add(HotelFareComponent.VD);
		components.add(HotelFareComponent.RP);
		components.add(HotelFareComponent.SNP);
		components.add(HotelFareComponent.SGP);
		return !components.contains(this);
	}
	
	public boolean isGstComponent() {
		Set<HotelFareComponent> components = getGstComponents();
		return !components.isEmpty() && components.contains(this);
	}

	public static Set<HotelFareComponent> getGstComponents() {
		Set<HotelFareComponent> components = new HashSet<>();
		components.add(HotelFareComponent.CGST);
		components.add(HotelFareComponent.SGST);
		components.add(HotelFareComponent.IGST);
		return components;
	}

	public static Set<HotelFareComponent> getGstDependendentComponents() {
		Set<HotelFareComponent> components = new HashSet<>();
		components.add(HotelFareComponent.MFT);
		return components;
	}

	public static List<HotelFareComponent> voucherComponents() {
		Set<HotelFareComponent> components = new HashSet<>();
		components.add(HotelFareComponent.VD);
		return new ArrayList<>(components);
	}
}