package com.tgs.services.hms.datamodel;

public enum HotelFlowType {

	SEARCH, DETAIL, REVIEW, BOOKING_DETAIL, BOOK, CANCELLATION, QUOTATION;
	
}
