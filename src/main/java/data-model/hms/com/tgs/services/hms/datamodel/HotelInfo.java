package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.helper.HotelAPIExcludeV1;
import com.tgs.services.base.helper.RestExclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;

@Getter
@Setter
@ToString
@Builder
@Accessors(chain = true)
public class HotelInfo {

	@CustomSerializedName(key = FieldName.HOTEL_ID)
	private String id;

	@CustomSerializedName(key = FieldName.GIATA_ID)
	private String giataId;

	private String name;
	
	@CustomSerializedName(key = FieldName.HOTEL_IMAGES)
	@ToString.Exclude
	private List<Image> images;
	
	@CustomSerializedName(key = FieldName.HOTEL_TERMS_AND_CONDITIONS)
	private TermsAndConditions termsAndConditions;

	@CustomSerializedName(key = FieldName.HOTEL_DESCRIPTION)
	private String description;
	
	@CustomSerializedName(key = FieldName.HOTEL_LONG_DESCRIPTION)
	private String longDescription;
	
	@CustomSerializedName(key = FieldName.HOTEL_RATING)
	private Integer rating;
	@CustomSerializedName(key = FieldName.HOTEL_GEOLOCATION)
	private GeoLocation geolocation;
	@CustomSerializedName(key = FieldName.HOTEL_ADDRESS)
	private Address address;
	@CustomSerializedName(key = FieldName.HOTEL_FACILITIES)
	private List<String> facilities;
	@CustomSerializedName(key = FieldName.HOTEL_PROPERTY_TYPE)
	private String propertyType;
	@CustomSerializedName(key = FieldName.HOTEL_CONTACT)
	private Contact contact;
	@CustomSerializedName(key = FieldName.HOTEL_WEBSITE)
	private String website;
	
	private Set<HotelTag> tags;
	
	@CustomSerializedName(key = FieldName.HOTEL_INSTRUCTIONS)
	private List<Instruction> instructions;
	
	@CustomSerializedName(key = FieldName.OPTIONS)
	private List<Option> options;
	
	/* 
	 * This is user primarily for search to
	 * compress size
	 */
	@CustomSerializedName(key = FieldName.PROCESSED_OPTIONS)
	private List<ProcessedOption> processedOptions;

	@RestExclude
	@CustomSerializedName(key = FieldName.HOTEL_MISC_INFO)
	private HotelMiscInfo miscInfo;

	@RestExclude
	@CustomSerializedName(key = FieldName.HOTEL_PROMOTION_INFO)
	private HotelPromotion promotionInfo;

	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.TRIP_ADVISOR_ID)
	private String userReviewSupplierId;
	
	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.SUPPLIER_SET)
	private Set<String> suppliers;
	
	private String cityName;

	private String countryName;

	private String localHotelCode;
	
	public HotelSearchType getSearchType() {
		if (this.getAddress() != null && this.getAddress().getCountry() != null
				&& this.getAddress().getCountry().getName() != null) {
			if (!this.getAddress().getCountry().getName().equalsIgnoreCase("India")) {
				return HotelSearchType.INTERNATIONAL;
			}
		}
		return HotelSearchType.DOMESTIC;
	}
	
	public boolean isOptionsNotEmpty() {
		return this != null && CollectionUtils.isNotEmpty(options);
	}

	public boolean isPriceInfosNotEmpty() {
		boolean isRoomsNotEmpty = true;
		boolean isPriceListNotEmpty = true;
		if (isOptionsNotEmpty()) {
			for (Option option : options) {
				isRoomsNotEmpty = CollectionUtils.isNotEmpty(option.getRoomInfos());
				if (!isRoomsNotEmpty) {
					return false;
				} else {
					for(RoomInfo roomInfo : option.getRoomInfos()) {
						isPriceListNotEmpty = CollectionUtils.isNotEmpty(roomInfo.getPerNightPriceInfos());
						if(!isPriceListNotEmpty) {
							return false;
						}
					}
				}
			}
		}
		return isPriceListNotEmpty;
	}
	
	public boolean isDomesticTrip(String clientCountry) {
		boolean isDomestic = true;
		if(!this.getAddress().getCountry().getName().equalsIgnoreCase(clientCountry)) {
			isDomestic = false;
		}
		return isDomestic;
	}

	public boolean isAddressPresent() {
		return this.getAddress() != null ? true : false;
	}


	public Set<String> getSuppliers() {
		if (suppliers == null)
			suppliers = new HashSet<>();
		return suppliers;

	}
	
	public List<Option> getOptions(){
		if(options == null) { options = new ArrayList<Option>(); }
		return options;
	}
	
	
	public boolean isExpired() {
		
		boolean isExpired = false;
		if(this.getMiscInfo() != null 
				&& !ObjectUtils.isEmpty(this.getMiscInfo().getSearchKeyExpiryTime()) && this.getMiscInfo().getSearchKeyExpiryTime().isBefore(LocalDateTime.now())){
			isExpired = true;
		}
		return isExpired;
		
	}
	
	public GeoLocation getGeolocation() {
		if(geolocation != null) return geolocation;
		return new GeoLocation("","");
	}
	
	public Set<HotelTag> getTags(){
		
		if(this.tags == null) {
			tags = new HashSet<>();
		}
		return tags;
	}
	
	public boolean isCrossSellHotel() {
		return this.getTags().contains(HotelTag.CROSS_SELL);
	}
	
	public boolean isVoucherApplied() {
		Option option = getOptions().get(0);
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				if (MapUtils.isNotEmpty(priceInfo.getMatchedFareComponents(HotelFareComponent.voucherComponents()))) {
					return true;
				}
			}
		}
		return false;
	}
}
