package com.tgs.services.hms.datamodel;

import lombok.Getter;

@Getter
public enum HotelMealBasisType {
	
	BREAKFAST("B"),
	BUFFET_BREAKFAST("BB"),
	BED_AND_BREAKFAST("BAB"),
	DINNER("D"),
	LUNCH("L"),
	HALF_BOARD("HB"),
	FULL_BOARD("FB"),
	ALL_INCLUSIVE("AI"),
	ONE_MEAL("OM"),
	ALL_MEALS("AM"),
	ROOM_ONLY("RO");

	public String getName() {
		return this.name();
	}

	private String code;

	private HotelMealBasisType(String code) {
		this.code = code;
	}
	
	public static HotelMealBasisType getMealBasisType(String code) {
	
		for(HotelMealBasisType mealBasisType: HotelMealBasisType.values()) {
			if(mealBasisType.getCode().equals(code))
				return mealBasisType;
		}
		return null;
	}
}

