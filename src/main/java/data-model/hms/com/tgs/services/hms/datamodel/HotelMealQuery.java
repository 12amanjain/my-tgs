package com.tgs.services.hms.datamodel;

import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelMealQuery extends BulkUploadQuery {
	
	private String sMealBasis;
	private String fMealBasis;
	private String supplierName;
}
