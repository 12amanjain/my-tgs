package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.helper.HotelAPIExcludeV1;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelMiscInfo {

	/*
	 * This is user to map search result with static data
	 */
	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.SUPPLIER_STATIC_HOTEL_ID)
	private String supplierStaticHotelId;
	
	@CustomSerializedName(key = FieldName.SEARCH_KEY_EXPIRY_TIME)
	private LocalDateTime searchKeyExpiryTime;

	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_REFERENCE)
	private String supplierBookingReference;
	
	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_ID)
	private String supplierBookingId;

	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_CONFIRMATION)
	private String supplierBookingConfirmationNo;

	@CustomSerializedName(key = FieldName.HOTEL_BOOKING_REFERENCE)
	private String hotelBookingReference;

	/*
	 * This is TGS searchId
	 */
	@HotelAPIExcludeV1
	private String searchId;

	@CustomSerializedName(key = FieldName.HOTEL_BOOKING_CANCELLATION_REFERENCE)
	private String hotelBookingCancellationReference;

	private Boolean isMealAlreadyMapped;

	/*
	 * DESIYA
	 */
	@HotelAPIExcludeV1
	private String correlationId;

	/*
	 * User Only For AGODA
	 */
	@CustomSerializedName(key = FieldName.SUPPLIER_BOOKING_URL)
	private String supplierBookingUrl;
	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.CREDIT_CARD_ID)
	private Long creditCardAppliedId;
	
	@SerializedName("ipr")
	private Boolean isPanRequired;
	
	//this field is currently used for agoda to check if the booking status needs to be checked 
	@HotelAPIExcludeV1
	@SerializedName("ifbs")
	private Boolean isFailedFromSupplier;
	
	@HotelAPIExcludeV1
	@SerializedName("tsb")
	private String tempSupplierBookingId;

}
