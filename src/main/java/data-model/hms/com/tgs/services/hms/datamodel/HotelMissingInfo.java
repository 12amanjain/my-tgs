package com.tgs.services.hms.datamodel;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class HotelMissingInfo {

	private String hotelName;
	private String hotelId;
	private String supplierName;
	private String cityName;
	private String countryName;
	private String searchId;
	private Boolean address;
	private Boolean hotelFacilities;
	private Boolean roomFacilities;
	private Boolean images;
	private Boolean geolocation;
	private Boolean rating;
	private Set<String> supplierNames;
	private Set<String> mealBasis;
	private OperationType operationType;
	
	public HotelMissingInfo(OperationType operationType) {
		this.operationType = operationType;
	}

	public Set<String> getMealBasis() {
		if (mealBasis == null) {
			mealBasis = new HashSet<>();
		}
		return mealBasis;
	}
}
