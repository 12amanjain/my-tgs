package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelNationalityInfo {
	
	private String supplierCountryId;
	private String countryId;

}
