package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelNationalityMappingInfo {

	private String supplierName;
	private String supplierCountryId;
	private String countryId;
	
}
