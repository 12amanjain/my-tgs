package com.tgs.services.hms.datamodel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelNationalitySupplierMapping {

	private String supplier;
	private List<HotelNationalityInfo> data;
	
	
}
