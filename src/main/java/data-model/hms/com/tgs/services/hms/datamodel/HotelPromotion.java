package com.tgs.services.hms.datamodel;

import com.google.gson.annotations.SerializedName;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelPromotion {

	@CustomSerializedName(key = FieldName.IS_LOWEST_PRICE_ALLOWED)
	private Boolean isLowestPriceAllowed;

	@CustomSerializedName(key = FieldName.IS_FLEXIBLE_TIMING_ALLOWED)
	private Boolean isFlexibleTimingAllowed;
	
	@SerializedName("ph")
	private Boolean preferredHotel;
	
	private String msg;
}
