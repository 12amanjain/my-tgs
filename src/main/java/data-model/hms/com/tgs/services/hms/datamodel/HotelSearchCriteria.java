package com.tgs.services.hms.datamodel;

import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchCriteria implements Validatable {
	
	/*
	 * Update Equals method also if any change in data model 
	 */

	private String hotelName;
	private String locality;
	@SerializedName("country")
	private String countryId;
	@SerializedName("city")
	private String cityId;
	private String countryName;
	private String cityName;
	private String nationality;
	/*
	 * DOTW has this requirement
	 */
	private String countryOfResidence;
	private Set<String> hotelIds;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (countryId == null && cityId == null) {
			errors.put("countryId", SystemError.INVALID_HOTEL_DESTINATION.getErrorDetail());
		}
		return errors;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cityId == null) ? 0 : cityId.hashCode());
		result = prime * result + ((cityName == null) ? 0 : cityName.hashCode());
		result = prime * result + ((countryId == null) ? 0 : countryId.hashCode());
		result = prime * result + ((countryName == null) ? 0 : countryName.hashCode());
		result = prime * result + ((hotelName == null) ? 0 : hotelName.hashCode());
		result = prime * result + ((locality == null) ? 0 : locality.hashCode());
		result = prime * result + ((nationality == null) ? 0 : nationality.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		HotelSearchCriteria other = (HotelSearchCriteria) obj;
		
		if(!StringUtils.equals(this.getCityId(), other.getCityId())) { return false; }
		if(!StringUtils.equals(this.countryId, other.getCountryId())) { return false; }
		if(!StringUtils.equals(this.getCityName(), other.getCityName())) { return false; }
		if(!StringUtils.equals(this.getCountryName(), other.getCountryName())) { return false; }
		if(!StringUtils.equals(this.getNationality(), other.getNationality())) { return false; }
		
		return true;
	}
	
	public boolean isDomesticTrip(String clientCountry) {
		boolean isDomestic = true;
		if (!this.getCountryName().equalsIgnoreCase(clientCountry)) {
			isDomestic = false;
		}
		return isDomestic;
	}
}
