package com.tgs.services.hms.datamodel;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchPreferences implements Validatable {

	/*
	 * Update Equals method also if any change in data model
	 */
	private Integer limitHotelRoomType;
	private Boolean availableonly;
	private List<Integer> ratings;
	private String currency;

	@SerializedName("lh")
	private Integer limitHotels;
	
	@SerializedName("fsc")
	private Boolean fetchSpecialCategory;
	
	@SerializedName("csp")
	private CrossSellParameter crossSellParameter;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		List<Integer> validRatingList = Arrays.asList(1, 2, 3, 4, 5);
		if (!CollectionUtils.isEmpty(ratings)) {
			if (Collections.disjoint(ratings, validRatingList)) {
				errors.put("ratings", SystemError.INVALID_HOTEL_RATINGS.getErrorDetail());
			}
		}

		return errors;
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((availableonly == null) ? 0 : availableonly.hashCode());
		result = prime * result + ((currency == null) ? 0 : currency.hashCode());
		result = prime * result + ((limitHotelRoomType == null) ? 0 : limitHotelRoomType.hashCode());
		result = prime * result + ((ratings == null) ? 0 : ratings.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {

		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}

		HotelSearchPreferences other = (HotelSearchPreferences) obj;

		if (ObjectUtils.compare(this.getLimitHotelRoomType(), other.getLimitHotelRoomType()) != 0) {
			return false;
		}
		if (ObjectUtils.compare(this.getAvailableonly(), other.getAvailableonly()) != 0) {
			return false;
		}
		if (!StringUtils.equals(this.getRatings().toString(), other.getRatings().toString())) {
			return false;
		}
		if (!StringUtils.equals(this.getCurrency(), other.getCurrency())) {
			return false;
		}

		return true;
	}
}
