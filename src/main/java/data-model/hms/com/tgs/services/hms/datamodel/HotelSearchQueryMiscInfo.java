package com.tgs.services.hms.datamodel;

import java.util.List;
import org.apache.commons.codec.binary.StringUtils;
import com.tgs.services.base.helper.HotelAPIExcludeV1;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class HotelSearchQueryMiscInfo {

	private String searchType;
	@HotelAPIExcludeV1
	private String sourceId;
	@HotelAPIExcludeV1
	private String supplierId;
	@HotelAPIExcludeV1
	private List<String> supplierIds;
	private String searchId;
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((searchId == null) ? 0 : searchId.hashCode());
		result = prime * result + ((searchType == null) ? 0 : searchType.hashCode());
		result = prime * result + ((sourceId == null) ? 0 : sourceId.hashCode());
		result = prime * result + ((supplierId == null) ? 0 : supplierId.hashCode());
		return result;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		
		if (this == obj) { return true; }
		if (obj == null) { return false; }
		if (getClass() != obj.getClass()) { return false; }
		
		HotelSearchQueryMiscInfo other = (HotelSearchQueryMiscInfo) obj;
		
		if(!StringUtils.equals(this.getSearchType(), other.getSearchType())) { return false; }
		if(!StringUtils.equals(this.getSourceId(), other.getSourceId())) { return false; }
		if(!StringUtils.equals(this.getSupplierId(), other.getSupplierId())) { return false; }
		if(!StringUtils.equals(this.getSearchId(), other.getSearchId())) { return false; }
		return true;
	}
}