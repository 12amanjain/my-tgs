package com.tgs.services.hms.datamodel;

import java.util.List;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelStaticCityInfo {

	@CustomSerializedName(key = FieldName.HOTEL_CITY_INFO_LIST)
	private List<CityInfo> cityInfoList;
	private String next;
	
}
