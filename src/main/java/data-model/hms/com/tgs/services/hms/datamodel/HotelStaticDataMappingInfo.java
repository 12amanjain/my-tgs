package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelStaticDataMappingInfo {

	private String hotelId;
	private String supplierhotelId;
	
}
