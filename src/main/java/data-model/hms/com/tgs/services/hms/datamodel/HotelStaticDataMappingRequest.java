package com.tgs.services.hms.datamodel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelStaticDataMappingRequest {

	private String supplierId;
	private String sourceId;
	private List<HotelStaticDataMappingInfo> mappingInfoList;
	
}
