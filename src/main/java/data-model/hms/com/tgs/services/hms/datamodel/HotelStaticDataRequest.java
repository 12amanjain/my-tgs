package com.tgs.services.hms.datamodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelStaticDataRequest {

	private String language;
	
	/*
	 * e.g TBO, DOTW, QTech, Expedia
	 */
	private String supplierId;
	
	private String outputPath;
	
	private HotelInfo hInfo;
	
	private List<HotelInfo> hotels;
	
	private Integer sourceId;
	
	private List<String> addlParams;

	private Boolean cacheStaticData;

	/*
	 * To fetch hotel based on cities
	 */
	private List<String> cityIds;

	/*
	 * To populate supplier cities in hotelsuppliercity
	 */
	private Boolean isMasterData;
}
