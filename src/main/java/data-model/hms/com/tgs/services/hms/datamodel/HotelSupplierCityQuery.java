package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSupplierCityQuery {

	private String cityName;
	private String cityId;
	private String supplierName;
}
