package com.tgs.services.hms.datamodel;

public enum HotelTag {
	
	PREFERRED,
	CROSS_SELL;

}
