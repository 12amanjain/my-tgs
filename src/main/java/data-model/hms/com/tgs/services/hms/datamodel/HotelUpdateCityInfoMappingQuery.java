package com.tgs.services.hms.datamodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelUpdateCityInfoMappingQuery {

	private String cityName;
	private String countryName;
	private String cityId;
	private String countryId;
	private List<TopSuggestion> topSuggestionList;
}
