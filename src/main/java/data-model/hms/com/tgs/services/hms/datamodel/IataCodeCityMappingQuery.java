package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class IataCodeCityMappingQuery {

	private String cityName;
	private String countryName;
	private String iataCode;
}
