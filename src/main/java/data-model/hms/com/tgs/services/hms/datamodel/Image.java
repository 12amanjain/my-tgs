package com.tgs.services.hms.datamodel;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Image {

	@CustomSerializedName(key = FieldName.HOTEL_THUMBNAILS)
	private String thumbnail;
	
	@CustomSerializedName(key = FieldName.HOTEL_IMAGE_URL)
	private String bigURL;
}
