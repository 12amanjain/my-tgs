package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Instruction {
	
	private InstructionType type;
	private String msg;
}
