package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.helper.HotelAPIExcludeV1;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class Option {


	@CustomSerializedName(key = FieldName.ROOMINFO_LIST)
	List<RoomInfo> roomInfos;

	private String id;

	@CustomSerializedName(key = FieldName.OPTION_PRICE)
	Double totalPrice;

	@CustomSerializedName(key = FieldName.OPTION_CANCELLATION_POLICY)
	private HotelCancellationPolicy cancellationPolicy;

	@CustomSerializedName(key = FieldName.OPTION_DEADLINEDATETIME)
	private LocalDateTime deadlineDateTime;

	@SerializedName("crd")
	private LocalDateTime cancellationRestrictedDateTime;

	@CustomSerializedName(key = FieldName.HOTEL_INSTRUCTIONS)
	private List<Instruction> instructions;

	@HotelAPIExcludeV1
	@CustomSerializedName(key = FieldName.OPTION_MISC_INFO)
	private OptionMiscInfo miscInfo;

	@DBExclude
	@CustomSerializedName(key = FieldName.IS_OPTION_ON_REQUEST)
	private Boolean isOptionOnRequest;

	/**
	 * This will be used post booking
	 */
	@CustomSerializedName(key = FieldName.ROOM_SSR_LIST)
	private List<RoomSSR> roomSSRs;
	
	@CustomSerializedName(key = FieldName.IS_PACKAGE_RATE)
	private Boolean isPackageRate;

	@SerializedName("ipr")
	private Boolean isPanRequired;
	
	public List<Instruction> getInstructions() {
		if (instructions == null)
			instructions = new ArrayList<>();
		return instructions;
	}


	public HotelCancellationPolicy getCancellationPolicy() {

		if (this.cancellationPolicy != null)
			return this.cancellationPolicy;

		List<HotelCancellationPolicy> cpList = new ArrayList<>();
		List<RoomInfo> roomInfos = this.getRoomInfos();

		boolean isFullRefundAllowed = true;
		for (RoomInfo roomInfo : roomInfos) {
			if (roomInfo.getCancellationPolicy() == null)
				return null;
			HotelCancellationPolicy roomCancellationPolicy = roomInfo.getCancellationPolicy();
			cpList.add(roomCancellationPolicy);
			if (roomCancellationPolicy.getIsFullRefundAllowed() != null) {
				isFullRefundAllowed &= roomInfo.getCancellationPolicy().getIsFullRefundAllowed();
			}

		}

		HotelCancellationPolicy cp = HotelCancellationPolicy.builder().roomCancellationPolicyList(cpList)
				.id(this.getId()).isFullRefundAllowed(isFullRefundAllowed).build();

		this.cancellationPolicy = cp;
		return cp;
	}

	public String getInstructionFromType(InstructionType type) {

		if (!CollectionUtils.isEmpty(this.getInstructions())) {
			return this.getInstructions().stream().filter((instruction) -> instruction.getType().equals(type))
					.map(Instruction::getMsg).findFirst().orElse(null);
		}

		return null;
	}

	public boolean getIsOptionOnRequest() {
		return BooleanUtils.isTrue(this.isOptionOnRequest);
	}

	public int getPaxCount() {

		int sum = 0;
		for (RoomInfo roomInfo : roomInfos) {
			sum += (roomInfo.getNumberOfAdults() + roomInfo.getNumberOfChild());
		}
		return sum;
	}

}
