package com.tgs.services.hms.datamodel;

import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OptionMiscInfo {

	@CustomSerializedName(key = FieldName.SUPPLIER_SEARCH_ID)
	private String supplierSearchId;

	@CustomSerializedName(key = FieldName.SECONDARY_SUPPLIER)
	private String secondarySupplier;

	@CustomSerializedName(key = FieldName.SUPPLIER_HOTEL_ID)
	private String supplierHotelId;

	/**
	 * This should be same as hotelsupplierInfotable#supplierId This is internal id
	 */
	@CustomSerializedName(key = FieldName.SUPPLIER_ID)
	private String supplierId;

	@CustomSerializedName(key = FieldName.SOURCE_ID)
	private Integer sourceId;

	@CustomSerializedName(key = FieldName.IS_MARKUP_UPDATED)
	private boolean isMarkupUpdatedByUser;
	
	@CustomSerializedName(key = FieldName.OPTION_BOOKING_PRICE)
	private String expectedOptionBookingPrice;
	
	@CustomSerializedName(key = FieldName.IS_COMMISSION_APPLIED)
	private Boolean isCommissionApplied;
	
	@CustomSerializedName(key = FieldName.IS_NOT_REQUIRED_ON_DETAIL)
	private Boolean isNotRequiredOnDetail;

	/**
	 * This will be used only for TBO
	 */
	private Integer resultIndex;
	private Boolean IsPackageFare;
	private Boolean IsPackageDetailsMandatory;
	
	/*
	 * Required for Expedia
	 */
	private Map<OptionLinkType, String> links;
	
	
	/*
	 * Required for Cleartrip
	 */
	private String roomTypeCode;
	private String bookingCode;

	public Map<OptionLinkType, String> getLinks() {
		if (MapUtils.isEmpty(links)) {
			links = new HashMap<>();
		}
		return links;
	}
	
	private Boolean isDetailHit;
	
	public boolean getIsDetailHit() {
		return BooleanUtils.isTrue(isDetailHit);
	}

}
