package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class PenaltyDetails {

	@CustomSerializedName(key = FieldName.FROM_DATE)
	private LocalDateTime fromDate;

	@CustomSerializedName(key = FieldName.TO_DATE)
	private LocalDateTime toDate;

	@CustomSerializedName(key = FieldName.PENALTY_AMOUNT)
	private Double penaltyAmount;

	@CustomSerializedName(key = FieldName.PENALTY_PERCENT)
	private Double penaltyPercent;

	@CustomSerializedName(key = FieldName.PENALTY_ROOM_NIGHTS)
	private Integer penaltyRoomNights;

	@CustomSerializedName(key = FieldName.CANCELLATION_TERMS_AND_CONDITIONS)
	private String termNConditions;

	@CustomSerializedName(key = FieldName.IS_CANCELLATION_RESTRICED)
	private Boolean isCancellationRestricted;
	
}
