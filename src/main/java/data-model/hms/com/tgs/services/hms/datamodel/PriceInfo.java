package com.tgs.services.hms.datamodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.DBExclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class PriceInfo {
	private Integer day;
	
	@SerializedName("tp")
	private Double totalPrice;

	/**
	 * This consists of paxWise : Fare Information, Baggage Information, Remaining
	 * seat,booking class , farebasis, refundable/non-refundable.
	 */
	@SerializedName("fc")
	@ToString.Include
	private Map<HotelFareComponent, Double> fareComponents;
	
	@DBExclude
	@SerializedName("afc")
	private Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponents;

	@SerializedName("cnp")
	private String cancellationPolicy;
	
	private PriceMiscInfo miscInfo;

	public Map<HotelFareComponent, Double> getFareComponents() {
		if (fareComponents == null) {
			fareComponents = new HashMap<>();
		}
		return this.fareComponents;
	}
	public Map<HotelFareComponent, Map<HotelFareComponent, Double>> getAddlFareComponents() {
		
		if(addlFareComponents == null) {
			addlFareComponents = new HashMap<>();
		}
		return addlFareComponents;

	}

	public PriceMiscInfo getMiscInfo() {
		if(miscInfo == null) {
			miscInfo = PriceMiscInfo.builder().build();
		}
		return miscInfo;
	}

	public Map<HotelFareComponent, Double> getMatchedFareComponents(List<HotelFareComponent> fareComponentList) {
		Map<HotelFareComponent, Double> fareComponentMap = new HashMap<>();
		for (HotelFareComponent component : fareComponentList) {
			if (fareComponents.get(component) != null) {
				fareComponentMap.put(component, fareComponents.get(component));
			}
		}
		return fareComponentMap;
	}
}
