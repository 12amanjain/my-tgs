package com.tgs.services.hms.datamodel;

import java.util.List;
import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ProcessedHotelInfo {

	@SerializedName("hid")
	private String hotelId;
	private String name;
	@SerializedName("des")
	private String description;
	@SerializedName("rt")
	private Integer rating;
	@SerializedName("gl")
	private GeoLocation geolocation;
	@SerializedName("ad")
	private Address address;
	@SerializedName("cnt")
	private Contact contact;
	@SerializedName("wb")
	private String website;
	private String type;
	
	@SerializedName("inst")
	private List<Instruction> instructions;
	
	@SerializedName("ss")
	private Set<String> suppliers;
	
	@CustomSerializedName(key = FieldName.OPTIONS)
	private List<ProcessedOptionInfo> options;
}
