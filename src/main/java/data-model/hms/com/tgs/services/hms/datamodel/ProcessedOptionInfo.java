package com.tgs.services.hms.datamodel;

import java.util.List;

import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ProcessedOptionInfo {
	
	@CustomSerializedName(key = FieldName.HOTEL_INSTRUCTIONS)
	private List<Instruction> instructions;
	
	
	@CustomSerializedName(key = FieldName.IS_PACKAGE_RATE)
	private Boolean isPackageRate;
}
