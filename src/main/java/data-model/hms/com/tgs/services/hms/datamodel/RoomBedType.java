package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomBedType {
	
	private Integer BedTypeCode;
	private String BedTypeDescription;

}