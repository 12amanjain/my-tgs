package com.tgs.services.hms.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.BooleanUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.HotelAPIExcludeV1;
import com.tgs.services.base.helper.RestExclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomInfo {

	private String id;

	@SerializedName("rc")
	private String roomCategory;
	@SerializedName("rt")
	private String roomType;
	@SerializedName("adt")
	private Integer numberOfAdults;
	@SerializedName("chd")
	private Integer numberOfChild;
	
	@SerializedName("mb")
	private String mealBasis;

	@SerializedName("tp")
	private Double totalPrice;

	@SerializedName("tfcs")
	private Map<HotelFareComponent, Double> totalFareComponents;
	
	@DBExclude
	@SerializedName("tafcs")
	private Map<HotelFareComponent, Map<HotelFareComponent, Double>> totalAddlFareComponents;

	@RestExclude
	@SerializedName("cnp")
	private HotelCancellationPolicy cancellationPolicy;

	@SerializedName("ddt")
	private LocalDateTime deadlineDateTime;

	@SerializedName("crd")
	private LocalDateTime cancellationRestrictedDateTime;

	@SerializedName("pis")
	private List<PriceInfo> perNightPriceInfos;

	@DBExclude
	@SerializedName("fcs")
	private List<String> roomAmenities;

	@SerializedName("imgs")
	@DBExclude
	private List<String> images;

	
	private LocalDate checkInDate;
	
	private LocalDate checkOutDate;

	@SerializedName("ti")
	private List<TravellerInfo> travellerInfo;


	@SerializedName("ssr")
	private List<RoomSSR> roomSSR;

	@RestExclude
	private Boolean isDeleted;

	@SerializedName("iopr")
	private Boolean isOptionOnRequest;
	
	@SerializedName("rmi")
	@HotelAPIExcludeV1
	private RoomMiscInfo miscInfo;
	
	@SerializedName("op")
	private String occupancyPattern;

	@SerializedName("iexb")
	private Boolean isextraBedIncluded;
	
	public Map<HotelFareComponent, Double> getTotalFareComponents() {
		if (totalFareComponents == null) {
			totalFareComponents = new HashMap<>();
		}
		return totalFareComponents;
	}
	
	public Map<HotelFareComponent, Map<HotelFareComponent, Double>> getTotalAddlFareComponents() {

		if (totalAddlFareComponents == null) {
			totalAddlFareComponents = new HashMap<>();
		}
		return this.totalAddlFareComponents;
	}
	
	public List<PriceInfo> getPerNightPriceInfos(){
		if(perNightPriceInfos == null) 
			perNightPriceInfos = new ArrayList<>();
		return perNightPriceInfos;
	}
	
	public boolean getIsOptionOnRequest() {
		return BooleanUtils.isTrue(this.isOptionOnRequest);
	}
}
