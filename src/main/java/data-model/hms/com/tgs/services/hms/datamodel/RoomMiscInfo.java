package com.tgs.services.hms.datamodel;

import java.math.BigDecimal;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RoomMiscInfo {

	private Integer roomIndex;
	private String ratePlanCode;
	private String roomTypeCode;
	private List<RoomBedType> bedTypes;
	private List<String> supplements;
	private String roomTypeName;
	private RoomPrice price;
	private boolean isUnderCancellationAllowedForAgent;
	private String allocationDetails;
	private String roomBookingId;
	private Integer numberOfPassengerNameRequiredForbooking;
	private String status;
	private Boolean isRoomBlocked;
	private String notes;
	
	/*
	 * Expedia Only
	 */
	private List<String> amenities;


	/*
	 * For Desiya Only
	 */

	private BigDecimal totalBaseAmount;
	private BigDecimal totalTaxes;

	/*
	 * Agoda
	 */
	private String roomBlockId;
	private Integer lineItemId;
	private String rateChannel;
	private Double exclusive;
	private Double tax;
	private Double fees;
	private Double inclusive;
	private String model;
	private String rateType;
	private String ratePlan;
	private List<RoomSurcharge> surcharges;
	
	/*
	 * Inventory
	 * 
	 */
	private List<Long> ratePlanIdList;
	private String secondarySupplier;
	public BigDecimal getTotalBaseAmount() {
		if(totalBaseAmount == null) return BigDecimal.ZERO;
		return totalBaseAmount;
	}
	
	public BigDecimal getTotalTaxes() {
		if(totalTaxes == null) return BigDecimal.ZERO;
		return totalTaxes;
	}


}
