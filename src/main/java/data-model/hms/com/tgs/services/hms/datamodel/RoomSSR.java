package com.tgs.services.hms.datamodel;

import java.time.LocalDateTime;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomSSR {

	@SerializedName("rm")
	private String requestMsg;
	@SerializedName("qrr")
	private Boolean isQuiteRoomRequired;
	@SerializedName("at")
	private LocalDateTime arrivalTime;
}
