package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RoomSurcharge {
	
	private String id;
	private String charge;
	private String name;
	private Rate rate;
	@Data
	public class Rate {
		    protected double exclusive;
		    protected double tax;
		    protected double fees;
		    protected double inclusive;
	}

}
