package com.tgs.services.hms.datamodel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SectionInfo {
	private String id;
	//private List<RoomInfo> roomInfoList;
}
