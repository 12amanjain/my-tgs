package com.tgs.services.hms.datamodel;



import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class State {

	private String code;
	private String name;

}
