package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SubReview {

	private String imageUrl;
	private String name;
	private Double value;
	
}
