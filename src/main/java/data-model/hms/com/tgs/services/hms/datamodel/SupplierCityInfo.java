package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SupplierCityInfo {

	private Long id;
	private String cityName;
	private String countryName;
	private String cityId;
	private String countryId;
	private String supplierName;
}
