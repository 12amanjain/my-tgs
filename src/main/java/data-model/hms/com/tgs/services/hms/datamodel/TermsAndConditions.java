package com.tgs.services.hms.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TermsAndConditions {

	@SerializedName("sc")
	private List<TermsAndConditionsFormat> supplierConditions;
}
