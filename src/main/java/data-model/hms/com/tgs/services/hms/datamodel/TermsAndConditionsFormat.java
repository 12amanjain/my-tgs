package com.tgs.services.hms.datamodel;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class TermsAndConditionsFormat {

	private TermsAndConditionsType type;
	private String info;
	private String label;
}
