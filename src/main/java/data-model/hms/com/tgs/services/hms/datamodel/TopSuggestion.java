package com.tgs.services.hms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TopSuggestion {

	private String cityName;
	private String countryName;
	private String cityId;
	private String countryId;
}
