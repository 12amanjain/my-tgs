package com.tgs.services.hms.datamodel;

import java.util.Map;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserReviewStaticData {

	
	private String hotelName;
	private String latitude;
	private String longitude;
	private Address address;
	private Map<String, Integer> tripTypes;
	private String category;
	
}
