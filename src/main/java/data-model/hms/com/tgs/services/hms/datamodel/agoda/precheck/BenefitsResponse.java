package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BenefitsResponse", propOrder = {"benefits"})
public class BenefitsResponse {

	@XmlElement(required = true)
	protected List<BenefitResponse> benefits;

	public List<BenefitResponse> getBenefits() {
		if (benefits == null) {
			benefits = new ArrayList<BenefitResponse>();
		}
		return this.benefits;
	}

}
