package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"bookingDetails", "customerDetail", "paymentDetails", "additionalInformation"})
@XmlRootElement(name = "BookingRequestV3")
@Data
public class BookingRequestV3 {

	@XmlElement(name = "BookingDetails", required = true)
	protected BookingRequestV3.BookingDetails bookingDetails;
	@XmlElement(name = "CustomerDetail", required = true)
	protected BookingRequestV3.CustomerDetail customerDetail;
	@XmlElement(name = "PaymentDetails", required = true)
	protected BookingRequestV3.PaymentDetails paymentDetails;
	@XmlElement(name = "AdditionalInformation")
	protected BookingRequestV3.AdditionalInformation additionalInformation;
	@XmlAttribute(name = "siteid", required = true)
	@XmlSchemaType(name = "unsignedInt")
	protected String siteid;
	@XmlAttribute(name = "apikey", required = true)
	protected String apikey;
	@XmlAttribute(name = "delaybooking")
	protected Boolean delaybooking;
	@XmlAttribute(name = "waittime")
	protected String waittime;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"originalUrl", "clickTimestamp", "others"})
	@Data
	public static class AdditionalInformation {

		@XmlElement(name = "OriginalUrl")
		protected String originalUrl;
		@XmlElement(name = "ClickTimestamp")
		protected Long clickTimestamp;
		@XmlElement(name = "Others")
		protected String others;

	}
	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"hotel"})
	@Data
	public static class BookingDetails {

		//@XmlElement(name = "Hotel", required = true)
		//protected BookingRequestV3.BookingDetails.Hotel hotel;
		@XmlElement(name = "Hotel", required = true)
		protected HotelRequest hotel;
		@XmlAttribute(name = "searchid", required = true)
		protected String searchid;
		@XmlAttribute(name = "tag", required = true)
		protected String tag;
		@XmlAttribute(name = "AllowDuplication")
		protected Boolean allowDuplication;
		@XmlAttribute(name = "CheckIn", required = true)
		@XmlSchemaType(name = "date")
		protected String checkIn;
		@XmlAttribute(name = "CheckOut", required = true)
		@XmlSchemaType(name = "date")
		protected String checkOut;
		@XmlAttribute(name = "ArrivalTimeLocal")
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar arrivalTimeLocal;
		@XmlAttribute(name = "DepartureTimeLocal")
		@XmlSchemaType(name = "dateTime")
		protected XMLGregorianCalendar departureTimeLocal;
		@XmlAttribute(name = "UserCountry")
		protected String userCountry;

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"rooms"})
		@Data
		public static class Hotel {

			@XmlElement(name = "Rooms", required = true)
			protected BookingRequestV3.BookingDetails.Hotel.Rooms rooms;
			@XmlAttribute(name = "id", required = true)
			@XmlSchemaType(name = "positiveInteger")
			protected String id;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"room"})
			@Data
			public static class Rooms {

				@XmlElement(name = "Room", required = true)
				protected List<BookingRequestV3.BookingDetails.Hotel.Rooms.Room> room;

				@XmlAccessorType(XmlAccessType.FIELD)
				@XmlType(name = "", propOrder = {"rate", "childrenAges", "surcharges", "guestDetails", "reuseHints",
						"specialRequest"})
				@Data
				public static class Room {
					@XmlElement(name = "Rate", required = true)
					protected RateRequest rate;
					@XmlElement(name = "ChildrenAges")
					protected ChildrenAgesRequest childrenAges;
					@XmlElement(name = "Surcharges")
					protected SurchargeRequest surcharges;
					@XmlElement(name = "GuestDetails", required = true)
					protected GuestDetails guestDetails;
					@XmlElement(name = "ReuseHints")
					protected BookingRequestV3.BookingDetails.Hotel.Rooms.Room.ReuseHints reuseHints;
					@XmlElement(name = "SpecialRequest")
					protected String specialRequest;
					@XmlAttribute(name = "id", required = true)
					@XmlSchemaType(name = "unsignedInt")
					protected String id;
					@XmlAttribute(name = "promotionid")
					@XmlSchemaType(name = "unsignedInt")
					protected String promotionid;
					@XmlAttribute(name = "name", required = true)
					protected String name;
					@XmlAttribute(name = "lineitemid", required = true)
					@XmlSchemaType(name = "positiveInteger")
					protected BigInteger lineitemid;
					@XmlAttribute(name = "rateplan")
					protected String rateplan;
					@XmlAttribute(name = "ratetype")
					protected String ratetype;
					@XmlAttribute(name = "ratechannel")
					protected String ratechannel;
					@XmlAttribute(name = "rateplanid")
					@XmlSchemaType(name = "unsignedInt")
					protected String rateplanid;
					@XmlAttribute(name = "currency", required = true)
					protected String currency;
					@XmlAttribute(name = "model", required = true)
					protected String model;
					@XmlAttribute(name = "ratecategoryid", required = true)
					@XmlSchemaType(name = "unsignedInt")
					protected String ratecategoryid;
					@XmlAttribute(name = "blockid", required = true)
					protected String blockid;
					@XmlAttribute(name = "booknowpaylaterdate")
					@XmlSchemaType(name = "date")
					protected XMLGregorianCalendar booknowpaylaterdate;
					@XmlAttribute(name = "roomtypenotguaranteed")
					protected String roomtypenotguaranteed;
					@XmlAttribute(name = "count", required = true)
					@XmlSchemaType(name = "positiveInteger")
					protected Integer count;
					@XmlAttribute(name = "adults", required = true)
					protected Integer adults;
					@XmlAttribute(name = "children", required = true)
					protected Integer children;

					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = {"guestDetail"})
					@Data
					public static class GuestDetails {

						@XmlElement(name = "GuestDetail", required = true)
						protected List<BookingRequestV3.BookingDetails.Hotel.Rooms.Room.GuestDetails.GuestDetail> guestDetail;


						@XmlAccessorType(XmlAccessType.FIELD)
						@XmlType(name = "", propOrder = {"title", "firstName", "lastName", "countryOfPassport",
								"gender", "age", "isChild"})
						@Data
						public static class GuestDetail {

							@XmlElement(name = "Title", required = true)
							protected String title;
							@XmlElement(name = "FirstName", required = true)
							protected String firstName;
							@XmlElement(name = "LastName", required = true)
							protected String lastName;
							@XmlElement(name = "CountryOfPassport")
							protected String countryOfPassport;
							@XmlElement(name = "Gender")
							protected String gender;
							@XmlElement(name = "Age")
							protected Integer age;
							@XmlElement(name = "IsChild")
							protected Boolean isChild;
							@XmlAttribute(name = "Primary")
							protected Boolean primary;


						}

					}


					@XmlAccessorType(XmlAccessType.FIELD)
					@XmlType(name = "", propOrder = {"reuseHint"})
					@Data
					public static class ReuseHints {

						@XmlElement(name = "ReuseHint", required = true)
						protected List<String> reuseHint;


					}

				}

			}

		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "",
			propOrder = {"language", "title", "firstName", "lastName", "email", "phone", "newsletter", "ipAddress"})
	@Data
	public static class CustomerDetail {

		@XmlElement(name = "Language", required = true)
		protected String language;
		@XmlElement(name = "Title", required = true)
		protected String title;
		@XmlElement(name = "FirstName", required = true)
		protected String firstName;
		@XmlElement(name = "LastName", required = true)
		protected String lastName;
		@XmlElement(name = "Email", required = true)
		protected String email;
		@XmlElement(name = "Phone", required = true)
		protected BookingRequestV3.CustomerDetail.Phone phone;
		@XmlElement(name = "Newsletter")
		protected boolean newsletter;
		@XmlElement(name = "IpAddress")
		protected String ipAddress;


		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"countryCode", "areaCode", "number"})
		@Data
		public static class Phone {

			@XmlElement(name = "CountryCode")
			protected String countryCode;
			@XmlElement(name = "AreaCode")
			protected String areaCode;
			@XmlElement(name = "Number", required = true)
			protected String number;


		}

	}

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"creditCardInfo"})
	@Data
	public static class PaymentDetails {

		@XmlElement(name = "CreditCardInfo")
		protected BookingRequestV3.PaymentDetails.CreditCardInfo creditCardInfo;


		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "",
				propOrder = {"cardtype", "number", "expiryDate", "cvc", "holderName", "countryOfIssue", "issuingBank"})
		@Data
		public static class CreditCardInfo {

			@XmlElement(name = "Cardtype", required = true)
			protected String cardtype;
			@XmlElement(name = "Number", required = true)
			protected String number;
			@XmlElement(name = "ExpiryDate", required = true)
			protected String expiryDate;
			@XmlElement(name = "Cvc", required = true)
			protected String cvc;
			@XmlElement(name = "HolderName", required = true)
			protected String holderName;
			@XmlElement(name = "CountryOfIssue", required = true)
			protected String countryOfIssue;
			@XmlElement(name = "IssuingBank", required = true)
			protected String issuingBank;

		}

	}

}
