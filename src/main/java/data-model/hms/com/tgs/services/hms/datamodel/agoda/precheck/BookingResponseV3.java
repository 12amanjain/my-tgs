package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"errorMessages", "bookingDetails"})
@XmlRootElement(name = "BookingResponseV3")
@Data
public class BookingResponseV3 {

	@XmlElement(name = "ErrorMessages")
	protected List<ErrorMessage> errorMessages;
	@XmlElement(name = "BookingDetails")
	protected BookingResponseV3.BookingDetails bookingDetails;
	@XmlAttribute(name = "status", required = true)
	protected String status;


	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"booking"})
	@Data
	public static class BookingDetails {

		@XmlElement(name = "Booking", required = true)
		protected List<BookingResponseV3.BookingDetails.Booking> booking;

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "")
		@Data
		public static class Booking {

			@XmlAttribute(name = "id", required = true)
			@XmlSchemaType(name = "unsignedInt")
			protected String id;
			@XmlAttribute(name = "ItineraryID", required = true)
			@XmlSchemaType(name = "unsignedInt")
			protected String itineraryID;
			@XmlAttribute(name = "selfservice", required = true)
			protected String selfservice;
			@XmlAttribute(name = "processing")
			protected Boolean processing;

		}

	}

}
