package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CancellationResponse", propOrder = {
    "policyText",
    "policyTranslated",
    "policyParameters",
    "policyDates"
})
@Data
public class CancellationResponse {

    protected PolicyTextResponse policyText;
    protected PolicyTranslatedResponse policyTranslated;
    protected PolicyParametersResponse policyParameters;
    protected PolicyDatesResponse policyDates;
}
