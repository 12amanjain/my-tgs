package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {"errorMessages", "cancellationSummary"})
@XmlRootElement(name = "CancellationResponseV2")
@Data
public class CancellationResponseV2 {

	@XmlElement(name = "ErrorMessages")
	protected List<ErrorMessage> errorMessages;
	@XmlElement(name = "CancellationSummary")
	protected CancellationResponseV2.CancellationSummary cancellationSummary;
	@XmlAttribute(name = "status", required = true)
	protected String status;

	@XmlAccessorType(XmlAccessType.FIELD)
	@XmlType(name = "", propOrder = {"bookingID", "reference", "cancellation", "payment", "refund"})
	@Data
	public static class CancellationSummary {

		@XmlElement(name = "BookingID")
		@XmlSchemaType(name = "unsignedInt")
		protected long bookingID;
		@XmlElement(name = "Reference")
		@XmlSchemaType(name = "unsignedInt")
		protected long reference;
		@XmlElement(name = "Cancellation", required = true)
		protected CancellationResponseV2.CancellationSummary.Cancellation cancellation;
		@XmlElement(name = "Payment", required = true)
		protected CancellationResponseV2.CancellationSummary.Payment payment;
		@XmlElement(name = "Refund", required = true)
		protected CancellationResponseV2.CancellationSummary.Refund refund;

		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"policyText"})
		@Data
		public static class Cancellation {

			@XmlElement(name = "PolicyText", required = true)
			protected List<CancellationResponseV2.CancellationSummary.Cancellation.PolicyText> policyText;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"value"})
			@Data
			public static class PolicyText {

				@XmlValue
				protected String value;
				@XmlAttribute(name = "language", required = true)
				protected String language;


			}

		}


		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"paymentRateInclusive"})
		@Data
		public static class Payment {

			@XmlElement(name = "PaymentRateInclusive", required = true)
			protected List<CancellationResponseV2.CancellationSummary.Payment.PaymentRateInclusive> paymentRateInclusive;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"value"})
			@Data
			public static class PaymentRateInclusive {

				@XmlValue
				protected double value;
				@XmlAttribute(name = "currency", required = true)
				protected String currency;


			}

		}
		@XmlAccessorType(XmlAccessType.FIELD)
		@XmlType(name = "", propOrder = {"refundRateInclusive"})
		@Data
		public static class Refund {

			@XmlElement(name = "RefundRateInclusive")
			protected List<CancellationResponseV2.CancellationSummary.Refund.RefundRateInclusive> refundRateInclusive;

			@XmlAccessorType(XmlAccessType.FIELD)
			@XmlType(name = "", propOrder = {"value"})
			@Data
			public static class RefundRateInclusive {

				@XmlValue
				protected double value;
				@XmlAttribute(name = "currency", required = true)
				protected String currency;

			}

		}

	}

}
