package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DailyRateDateResponse")
@Data
public class DailyRateDateResponse {

    @XmlAttribute(name = "date", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar date;
    @XmlAttribute(name = "exclusive", required = true)
    protected BigDecimal exclusive;
    @XmlAttribute(name = "tax", required = true)
    protected BigDecimal tax;
    @XmlAttribute(name = "fees", required = true)
    protected BigDecimal fees;
    @XmlAttribute(name = "inclusive", required = true)
    protected BigDecimal inclusive;

}
