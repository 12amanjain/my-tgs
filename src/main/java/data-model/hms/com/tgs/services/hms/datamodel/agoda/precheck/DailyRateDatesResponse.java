package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "DailyRateDatesResponse", propOrder = {
    "dailyRates"
})
@Data
public class DailyRateDatesResponse {

    @XmlElement(required = true)
    protected List<DailyRateDateResponse> dailyRates;

    public List<DailyRateDateResponse> getDailyRates() {
        if (dailyRates == null) {
            dailyRates = new ArrayList<DailyRateDateResponse>();
        }
        return this.dailyRates;
    }

}
