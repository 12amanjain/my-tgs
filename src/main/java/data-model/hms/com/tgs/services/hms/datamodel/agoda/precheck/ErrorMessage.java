package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorMessage")
@Data
public class ErrorMessage {
	
	@XmlValue
     protected String value;
	@XmlAttribute(name = "id", required = true)
	protected BigInteger id;
	@XmlAttribute(name = "subid")
	protected BigInteger subid;
}
