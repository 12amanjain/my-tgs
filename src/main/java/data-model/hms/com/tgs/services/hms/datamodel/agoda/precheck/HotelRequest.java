package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelRequest", propOrder = {
    "rooms"
})
@Data
public class HotelRequest {

    @XmlElement(name = "Rooms", required = true)
    protected RoomsRequest rooms;
    @XmlAttribute(name = "id", required = true)
    protected String id;
}
