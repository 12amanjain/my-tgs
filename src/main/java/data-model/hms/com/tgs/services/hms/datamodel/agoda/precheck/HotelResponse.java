package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "HotelResponse", propOrder = {
    "rooms",
    "cheapestRoom",
    "paxSettings"
})
@Data
public class HotelResponse {

    @XmlElement(name = "Rooms", required = true)
    protected List<RoomResponse> rooms;
    @XmlElement(name = "CheapestRoom", required = true)
    protected CheapestRoomResponse cheapestRoom;
    @XmlElement(name = "PaxSettings", required = true)
    protected PaxSettingsResponse paxSettings;
    @XmlAttribute(name = "id", required = true)
    protected BigInteger id;
}
