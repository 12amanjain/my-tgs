package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaxRoomOccupancyResponse")
@Data
public class MaxRoomOccupancyResponse {

    @XmlAttribute(name = "normalBedding", required = true)
    protected BigInteger normalBedding;
    @XmlAttribute(name = "extraBeds", required = true)
    protected BigInteger extraBeds;
}
