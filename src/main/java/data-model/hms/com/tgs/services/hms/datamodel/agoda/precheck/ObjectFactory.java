package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlRegistry;


@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tgs.services.hms.datamodel.agoda.precheck
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PrecheckResponse }
     * 
     */
    public PrecheckResponse createPrecheckResponse() {
        return new PrecheckResponse();
    }

    /**
     * Create an instance of {@link HotelsResponse }
     * 
     */
    public HotelsResponse createHotelsResponse() {
        return new HotelsResponse();
    }

    /**
     * Create an instance of {@link ErrorListResponse }
     * 
     */
    public ErrorListResponse createErrorListResponse() {
        return new ErrorListResponse();
    }

    /**
     * Create an instance of {@link DailyRateDateResponse }
     * 
     */
    public DailyRateDateResponse createDailyRateDateResponse() {
        return new DailyRateDateResponse();
    }

    /**
     * Create an instance of {@link RoomResponse }
     * 
     */
    public RoomResponse createRoomResponse() {
        return new RoomResponse();
    }

    /**
     * Create an instance of {@link PolicyParameterResponse }
     * 
     */
    public PolicyParameterResponse createPolicyParameterResponse() {
        return new PolicyParameterResponse();
    }

    /**
     * Create an instance of {@link RateResponse }
     * 
     */
    public RateResponse createRateResponse() {
        return new RateResponse();
    }

    /**
     * Create an instance of {@link PolicyDatesResponse }
     * 
     */
    public PolicyDatesResponse createPolicyDatesResponse() {
        return new PolicyDatesResponse();
    }

    /**
     * Create an instance of {@link RewardPointsResponse }
     * 
     */
    public RewardPointsResponse createRewardPointsResponse() {
        return new RewardPointsResponse();
    }

    /**
     * Create an instance of {@link PolicyParametersResponse }
     * 
     */
    public PolicyParametersResponse createPolicyParametersResponse() {
        return new PolicyParametersResponse();
    }

    /**
     * Create an instance of {@link SurchargesResponse }
     * 
     */
    public SurchargesResponse createSurchargesResponse() {
        return new SurchargesResponse();
    }

    /**
     * Create an instance of {@link RateInfoResponse }
     * 
     */
    public RateInfoResponse createRateInfoResponse() {
        return new RateInfoResponse();
    }

    /**
     * Create an instance of {@link TaxFeeResponse }
     * 
     */
    public TaxFeeResponse createTaxFeeResponse() {
        return new TaxFeeResponse();
    }

    /**
     * Create an instance of {@link PolicyTranslatedResponse }
     * 
     */
    public PolicyTranslatedResponse createPolicyTranslatedResponse() {
        return new PolicyTranslatedResponse();
    }

    /**
     * Create an instance of {@link HotelResponse }
     * 
     */
    public HotelResponse createHotelResponse() {
        return new HotelResponse();
    }

    /**
     * Create an instance of {@link BenefitResponse }
     * 
     */
    public BenefitResponse createBenefitResponse() {
        return new BenefitResponse();
    }

    /**
     * Create an instance of {@link ParentRoomResponse }
     * 
     */
    public ParentRoomResponse createParentRoomResponse() {
        return new ParentRoomResponse();
    }

    /**
     * Create an instance of {@link PolicyDateResponse }
     * 
     */
    public PolicyDateResponse createPolicyDateResponse() {
        return new PolicyDateResponse();
    }

    /**
     * Create an instance of {@link BenefitsResponse }
     * 
     */
    public BenefitsResponse createBenefitsResponse() {
        return new BenefitsResponse();
    }

    /**
     * Create an instance of {@link PromotionResponse }
     * 
     */
    public PromotionResponse createPromotionResponse() {
        return new PromotionResponse();
    }

    /**
     * Create an instance of {@link CheapestRoomResponse }
     * 
     */
    public CheapestRoomResponse createCheapestRoomResponse() {
        return new CheapestRoomResponse();
    }

    /**
     * Create an instance of {@link MaxRoomOccupancyResponse }
     * 
     */
    public MaxRoomOccupancyResponse createMaxRoomOccupancyResponse() {
        return new MaxRoomOccupancyResponse();
    }

    /**
     * Create an instance of {@link SurchargeResponse }
     * 
     */
    public SurchargeResponse createSurchargeResponse() {
        return new SurchargeResponse();
    }

    /**
     * Create an instance of {@link TaxFeesResponse }
     * 
     */
    public TaxFeesResponse createTaxFeesResponse() {
        return new TaxFeesResponse();
    }

    /**
     * Create an instance of {@link PaxSettingsResponse }
     * 
     */
    public PaxSettingsResponse createPaxSettingsResponse() {
        return new PaxSettingsResponse();
    }

    /**
     * Create an instance of {@link PolicyTextResponse }
     * 
     */
    public PolicyTextResponse createPolicyTextResponse() {
        return new PolicyTextResponse();
    }

    /**
     * Create an instance of {@link DailyRateDatesResponse }
     * 
     */
    public DailyRateDatesResponse createDailyRateDatesResponse() {
        return new DailyRateDatesResponse();
    }

    /**
     * Create an instance of {@link CancellationResponse }
     * 
     */
    public CancellationResponse createCancellationResponse() {
        return new CancellationResponse();
    }

}
