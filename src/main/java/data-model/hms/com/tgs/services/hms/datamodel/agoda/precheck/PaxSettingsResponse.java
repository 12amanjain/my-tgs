package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PaxSettingsResponse")
@Data
public class PaxSettingsResponse {

    @XmlAttribute(name = "submit", required = true)
    protected String submit;
    @XmlAttribute(name = "infantage", required = true)
    protected int infantage;
    @XmlAttribute(name = "childage", required = true)
    protected int childage;
}
