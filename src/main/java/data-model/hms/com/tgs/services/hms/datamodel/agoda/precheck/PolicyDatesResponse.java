package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyDatesResponse", propOrder = {
    "policyDates"
})
@Data
public class PolicyDatesResponse {

    @XmlElement(required = true)
    protected List<PolicyDateResponse> policyDates;

    public List<PolicyDateResponse> getPolicyDates() {
        if (policyDates == null) {
            policyDates = new ArrayList<PolicyDateResponse>();
        }
        return this.policyDates;
    }

}
