package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyParametersResponse", propOrder = {"policyParameters"})
@Data
public class PolicyParametersResponse {

	@XmlElement(required = true)
	protected List<PolicyParameterResponse> policyParameters;

	public List<PolicyParameterResponse> getPolicyParameters() {
		if (policyParameters == null) {
			policyParameters = new ArrayList<PolicyParameterResponse>();
		}
		return this.policyParameters;
	}

}
