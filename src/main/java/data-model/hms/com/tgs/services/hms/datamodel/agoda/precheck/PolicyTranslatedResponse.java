package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyTranslatedResponse")
@Data
public class PolicyTranslatedResponse {

    @XmlAttribute(name = "language", required = true)
    protected String language;
    @XmlAttribute(name = "text", required = true)
    protected String text;
}
