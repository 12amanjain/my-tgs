package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrecheckDetailsRequest", propOrder = {
    "hotel"
})
@Data
public class PrecheckDetailsRequest {

    @XmlElement(name = "Hotel", required = true)
    protected HotelRequest hotel;
    @XmlAttribute(name = "searchid", required = true)
    protected String searchid;
    @XmlAttribute(name = "tag", required = true)
    protected String tag;
    @XmlAttribute(name = "AllowDuplication")
    protected Boolean allowDuplication;
    @XmlAttribute(name = "CheckIn", required = true)
    @XmlSchemaType(name = "date")
    protected String checkIn;
    @XmlAttribute(name = "CheckOut", required = true)
    @XmlSchemaType(name = "date")
    protected String checkOut;
    @XmlAttribute(name = "arrivalTimeLocal")
    @XmlSchemaType(name = "date")
    protected String arrivalTimeLocal;
    @XmlAttribute(name = "departureTimeLocal")
    @XmlSchemaType(name = "date")
    protected String departureTimeLocal;
    @XmlAttribute(name = "UserCountry")
    protected String userCountry;
}
