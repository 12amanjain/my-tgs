package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "recommendation",
    "errorList"
})
@XmlRootElement(name = "PrecheckResponse")
@Data
public class PrecheckResponse {

    protected HotelsResponse recommendation;
    protected List<ErrorListResponse> errorList;
    @XmlAttribute(name = "status", required = true)
    protected Integer status;
    @XmlAttribute(name = "message")
    protected String message;


}
