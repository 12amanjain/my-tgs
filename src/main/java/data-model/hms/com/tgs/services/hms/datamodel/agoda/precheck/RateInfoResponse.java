package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateInfoResponse", propOrder = {
    "rate",
    "rewardPoints",
    "promotion",
    "dailyRates",
    "taxServiceBreakdown",
    "surcharges"
})
@Data
public class RateInfoResponse {

    @XmlElement(name = "Rate", required = true)
    protected RateResponse rate;
    @XmlElement(name = "RewardPoints")
    protected RewardPointsResponse rewardPoints;
    @XmlElement(name = "Promotion")
    protected PromotionResponse promotion;
    protected DailyRateDatesResponse dailyRates;
    protected TaxFeesResponse taxServiceBreakdown;
    @XmlElement(name = "Surcharges")
    protected SurchargesResponse surcharges;
    @XmlAttribute(name = "included")
    protected String included;
    @XmlAttribute(name = "excluded")
    protected String excluded;
}
