package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;

@XmlType(name = "ratePlanRequest")
@XmlEnum
public enum RatePlanRequest {

    @XmlEnumValue("Retail")
    RETAIL("Retail"),
    @XmlEnumValue("B2B")
    B_2_B("B2B"),
    APS("APS"),
    PCLN("PCLN"),
    NPCLN("NPCLN"),
    @XmlEnumValue("Domestic")
    DOMESTIC("Domestic"),
    @XmlEnumValue("Mobile")
    MOBILE("Mobile"),
    AO("AO"),
    @XmlEnumValue("China")
    CHINA("China"),
    @XmlEnumValue("RestrictedRetail")
    RESTRICTED_RETAIL("RestrictedRetail"),
    @XmlEnumValue("PricelineMerchant")
    PRICELINE_MERCHANT("PricelineMerchant"),
    @XmlEnumValue("Package")
    PACKAGE("Package"),
    @XmlEnumValue("PreferredOpaque")
    PREFERRED_OPAQUE("PreferredOpaque"),
    @XmlEnumValue("RestrictedPrivate")
    RESTRICTED_PRIVATE("RestrictedPrivate"),
    @XmlEnumValue("RestrictedOpaque")
    RESTRICTED_OPAQUE("RestrictedOpaque"),
    @XmlEnumValue("RestrictedMobile")
    RESTRICTED_MOBILE("RestrictedMobile"),
    @XmlEnumValue("RestrictedPackage")
    RESTRICTED_PACKAGE("RestrictedPackage");
    private final String value;

    RatePlanRequest(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RatePlanRequest fromValue(String v) {
        for (RatePlanRequest c: RatePlanRequest.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
