package com.tgs.services.hms.datamodel.agoda.precheck;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;


@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RateRequest")
public class RateRequest {

    @XmlAttribute(name = "exclusive", required = true)
    protected double exclusive;
    @XmlAttribute(name = "tax", required = true)
    protected double tax;
    @XmlAttribute(name = "fees", required = true)
    protected double fees;
    @XmlAttribute(name = "inclusive", required = true)
    protected double inclusive;
   // @XmlAttribute(name = "processingfee")
   // protected double processingfee;

}
