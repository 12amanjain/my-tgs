package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RewardPointsResponse")
@Data
public class RewardPointsResponse {

    @XmlAttribute(name = "point", required = true)
    protected short point;
    @XmlAttribute(name = "savings", required = true)
    protected BigDecimal savings;

}
