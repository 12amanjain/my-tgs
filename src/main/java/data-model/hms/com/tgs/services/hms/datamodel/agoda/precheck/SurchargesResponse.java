package com.tgs.services.hms.datamodel.agoda.precheck;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SurchargesResponse", propOrder = {
    "surcharges"
})
@Data
public class SurchargesResponse {

    @XmlElement(name = "Surcharges", required = true)
    protected List<SurchargeResponse> surcharges;

    public List<SurchargeResponse> getSurcharges() {
        if (surcharges == null) {
            surcharges = new ArrayList<SurchargeResponse>();
        }
        return this.surcharges;
    }

}
