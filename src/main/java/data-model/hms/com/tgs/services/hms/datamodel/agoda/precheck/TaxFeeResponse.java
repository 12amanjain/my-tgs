package com.tgs.services.hms.datamodel.agoda.precheck;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TaxFeeResponse")
@Data
public class TaxFeeResponse {

    @XmlAttribute(name = "id", required = true)
    protected BigInteger id;
    @XmlAttribute(name = "taxFeetype", required = true)
    protected String taxFeetype;
    @XmlAttribute(name = "description", required = true)
    protected String description;
    @XmlAttribute(name = "translation", required = true)
    protected String translation;
    @XmlAttribute(name = "method", required = true)
    protected String method;
    @XmlAttribute(name = "total", required = true)
    protected BigDecimal total;
    @XmlAttribute(name = "base")
    protected String base;
    @XmlAttribute(name = "taxable")
    protected String taxable;
    @XmlAttribute(name = "percent")
    protected BigDecimal percent;
    @XmlAttribute(name = "currency")
    protected String currency;
    @XmlAttribute(name = "amount")
    protected BigDecimal amount;
}
