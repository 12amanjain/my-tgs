package com.tgs.services.hms.datamodel.agoda.search;

import java.util.Date;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@XmlRootElement(name="AvailabilityRequestV2")
@XmlType(namespace="http://xml.agoda.com")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaSearchRequest extends AgodaBaseRequest {

	@XmlAttribute
	private String siteid;
	
	@XmlAttribute
	private String apikey;
	
	@XmlAttribute
	private String xmlns;
	
	@XmlElement
	private String Type;
	@XmlElement
	private String Id;
	@XmlElement
	private String CheckIn;
	@XmlElement
	private String CheckOut;
	@XmlElement
	private String Rooms;
	@XmlElement
	private String Adults;
	@XmlElement
	private String Children;
	//@XmlElement
	//private List<ChildrenAges> ChildrenAges;
	private ChildrenAges ChildrenAges;
	@XmlElement
	private String Language;
	@XmlElement
	private String Currency;
	@XmlElement
	private String UserCountry;
	
	
	
}