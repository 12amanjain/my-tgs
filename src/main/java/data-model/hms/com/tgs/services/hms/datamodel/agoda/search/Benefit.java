package com.tgs.services.hms.datamodel.agoda.search;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

@XmlRootElement(name="Benefit")
@XmlAccessorType(XmlAccessType.FIELD)
public class Benefit {
	 
	 @XmlElement 
	 private String Name;
	 
	 @XmlElement
	 private String Translation;
	 
	 @XmlAttribute
	 private String _id;
}
