package com.tgs.services.hms.datamodel.agoda.search;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class CheapestRoom {
	@XmlAttribute
	private String exclusive;
	
	@XmlAttribute
	private String tax;
	
	@XmlAttribute
	private String fees;
	
	@XmlAttribute
	private String inclusive;
}
