package com.tgs.services.hms.datamodel.agoda.search;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import lombok.Data;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ChildrenAges", propOrder = {
    "age"
})
@Data
public class ChildrenAges {

    @XmlElement(name = "Age", type = Integer.class)
    @XmlSchemaType(name = "unsignedInt")
    protected List<Integer> age;
    
    public List<Integer> getAge() {
        if (age == null) {
            age = new ArrayList<Integer>();
        }
        return this.age;
    }

}