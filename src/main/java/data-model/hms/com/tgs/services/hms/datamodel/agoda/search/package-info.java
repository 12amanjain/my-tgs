@XmlSchema(
        namespace="http://xml.agoda.com",
        elementFormDefault=XmlNsForm.QUALIFIED)
package com.tgs.services.hms.datamodel.agoda.search;

import javax.xml.bind.annotation.XmlNsForm;
import javax.xml.bind.annotation.XmlSchema;