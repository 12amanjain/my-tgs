package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="address")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaHotelAddress {

	@XmlElement(name = "hotel_id")
    protected Long hotelId;
    
	@XmlElement(name = "address_type")
    protected String addressType;
	
    @XmlElement(name = "address_line_1")
    protected String addressLine1;
    
    @XmlElement(name = "address_line_2")
    protected String addressLine2;
    
    @XmlElement(name = "postal_code")
    protected String postalCode;
    
    protected String state;
    protected String city;
    protected String country;
	
}
