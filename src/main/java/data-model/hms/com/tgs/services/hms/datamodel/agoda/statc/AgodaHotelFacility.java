package com.tgs.services.hms.datamodel.agoda.statc;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="facility")
@XmlAccessorType(XmlAccessType.FIELD)
public class AgodaHotelFacility {
	
	@XmlElement(name = "hotel_id")
    protected Long hotelId;
	
    @XmlElement(name = "property_group_description")
    protected String propertyGroupDescription;
    
    @XmlElement(name = "property_id")
    protected Long propertyId;
    
    @XmlElement(name = "property_name")
    protected String propertyName;

    
    @XmlElement(name = "property_translated_name")
    protected String propertyTranslatedName;

}
