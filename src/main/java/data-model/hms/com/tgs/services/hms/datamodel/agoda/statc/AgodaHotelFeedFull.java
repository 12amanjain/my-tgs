package com.tgs.services.hms.datamodel.agoda.statc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "Hotel_feed_full")
public class AgodaHotelFeedFull {
	
	@XmlElementWrapper(name = "hotels")
	@XmlElement(name="hotel")
	private List<AgodaHotel> hotels;
	
	@XmlElementWrapper(name = "addresses")
	@XmlElement(name="address")
	private List<AgodaHotelAddress> addressList;
	
	@XmlElementWrapper(name = "hotel_descriptions")
	@XmlElement(name="hotel_description")
	private List<AgodaHotelDescription> hotelDescriptionList;
	
	@XmlElementWrapper(name = "facilities")
	@XmlElement(name="facility")
	private List<AgodaHotelFacility> hotelFacilityList;
	
	@XmlElementWrapper(name = "pictures")
	@XmlElement(name="picture")
	private List<AgodaHotelImage> hotelImageList;
	
	@XmlElementWrapper(name = "roomtypes")
	@XmlElement(name="roomtype")
	private List<AgodaRoomType> hotelRoomTypeList;

}
