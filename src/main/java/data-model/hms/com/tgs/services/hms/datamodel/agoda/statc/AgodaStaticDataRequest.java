package com.tgs.services.hms.datamodel.agoda.statc;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AgodaStaticDataRequest {

	private String token;
	private String feed_id;
	private String site_id;
	private String ocountry_id;
	private String mcity_id;
	private String mhotel_id;
	private String olanguage_id;
}
