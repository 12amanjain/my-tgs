package com.tgs.services.hms.datamodel.agoda.statc;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "City_feed")
public class CityFeed {
	
	@XmlElementWrapper(name = "cities")
	@XmlElement(name="city")
	private List<AgodaCity> cities;

}
