package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingInfo {

	private String bookingStatus;
	private String voucherNumber;
}
