package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CityRequest extends CleartripBaseRequest{

	private String pageNo;
	private String size;
}
