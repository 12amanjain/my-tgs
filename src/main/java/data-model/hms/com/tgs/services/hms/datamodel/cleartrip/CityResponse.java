package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityResponse {

	private String code;
	private CleartripStaticResponseData data;
}
