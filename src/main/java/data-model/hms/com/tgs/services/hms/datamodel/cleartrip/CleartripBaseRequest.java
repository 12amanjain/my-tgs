package com.tgs.services.hms.datamodel.cleartrip;

import java.util.HashMap;
import java.util.Map;
import com.tgs.services.base.helper.Exclude;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CleartripBaseRequest {

	@Exclude
	private Map<String, String> headerParams;

	@Builder.Default
	private String sellingCurrency = "INR";
	@Builder.Default
	private String sellingCountry = "IN";

	@Exclude
	private String suffixOfURL;

	public Map<String, String> getHeaderParams() {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}

		headerParams.put("X-CT-SOURCETYPE", "API");
		headerParams.put("Cache-Control", "no-cache");
		headerParams.put("Content-Type", "application/json");
		return headerParams;
	}
}
