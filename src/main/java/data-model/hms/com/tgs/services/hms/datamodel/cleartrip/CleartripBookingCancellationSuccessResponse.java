package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripBookingCancellationSuccessResponse {

	private String cancelStatus;
}
