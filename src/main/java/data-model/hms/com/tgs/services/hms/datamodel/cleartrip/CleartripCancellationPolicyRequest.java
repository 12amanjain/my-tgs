package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CleartripCancellationPolicyRequest extends CleartripBaseRequest {

	private String nri;
	private String checkOutDate;
	private String hotelId;
	private String roomTypeCode;
	private String bookingCode;
	private String checkInDate;
	private List<Occupancy> occupancy; 
}
