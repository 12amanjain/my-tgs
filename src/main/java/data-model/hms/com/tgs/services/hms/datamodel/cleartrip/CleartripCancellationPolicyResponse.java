package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripCancellationPolicyResponse extends CleartripBaseResponse {

	private CleartripCancellationPolicySuccessResponse success;
}
