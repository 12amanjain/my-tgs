package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CleartripConfirmBookingRequest extends CleartripBaseRequest {

	private String itineraryId;
	private String affiliateTripRef;
	private CleartripPaymentInfo payment;
}
