package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripCreateBookingSuccessResponse {

	private String tripId;
	private String confirmationNumber;
	private String itineraryId;
}
