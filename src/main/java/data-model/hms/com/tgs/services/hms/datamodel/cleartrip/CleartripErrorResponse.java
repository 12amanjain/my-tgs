package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripErrorResponse {

	private Integer errorCode;
	private String errorMessage;
	private String detailedMessage;
}

