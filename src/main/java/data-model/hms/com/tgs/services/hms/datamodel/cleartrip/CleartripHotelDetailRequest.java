package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CleartripHotelDetailRequest extends CleartripSearchRequest {

	private String hotelId;
}
