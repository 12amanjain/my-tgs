package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripHotelInfo {

	private String hotelId;
	private Integer hotelRank;
	private HotelBasicInfo hotelBasicInfo;
	private HotelBasicInfo basicInfo;
	private List<CleartripRoomRate> roomRates; 
}