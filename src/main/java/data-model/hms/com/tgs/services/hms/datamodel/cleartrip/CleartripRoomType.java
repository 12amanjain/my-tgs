package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripRoomType {

	private Long roomTypeId;
	private String roomTypeCode;
	private String roomDescription;
	private String roomTypeName;
}
