package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class CleartripSearchRequest extends CleartripBaseRequest {

	private String country;
	private String hotelInfo;
	private String checkInDate;
	private String checkOutDate;
	private String occupancy;
	private String city;
}