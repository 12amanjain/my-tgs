package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CleartripSuccessSearchResponse {

	private String currency;
	private List<CleartripHotelInfo> hotels;
}
