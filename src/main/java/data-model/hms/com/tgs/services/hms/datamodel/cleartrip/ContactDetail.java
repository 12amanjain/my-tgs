package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactDetail {

	private String title;
	private String firstName;
	private String lastName;
	private String email;
	private String mobile;
}
