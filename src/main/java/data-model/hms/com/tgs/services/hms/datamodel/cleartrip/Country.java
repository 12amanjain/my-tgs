package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Country {

	private String id;
	private String isoCodeA2;
	private String countryName;
	private String countryCode;
}
