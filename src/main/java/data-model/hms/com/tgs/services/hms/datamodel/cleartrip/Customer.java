package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Customer {

	private String title;
	private String firstName;
	private String lastName;
	private String streetAddress1;
	private String city;
	private String state;
	private String country;
	private String postalCode;
	private String mobile;
	private String email;	
}
