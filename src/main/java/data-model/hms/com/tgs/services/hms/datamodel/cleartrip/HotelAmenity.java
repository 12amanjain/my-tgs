package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelAmenity {

	private String category;
	private List<String> amenities;
}
