package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBasicInfo {

	private String hotelName;
	private String chain;
	private String overview;
	private String thumbNailImage;
	private String starRating;
	private String slugId;
	private List<String> restrictions;
	private List<HotelAmenity> hotelAmenity;
	private String notice;
	private Locality locality;
}