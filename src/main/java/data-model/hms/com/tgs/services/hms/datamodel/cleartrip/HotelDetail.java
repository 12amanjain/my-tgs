package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelDetail {

	private String hotelId;
	private String address;
	private String city;
	private String checkInDate;
	private String checkOutDate;
}
