package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class HotelListRequest extends CleartripBaseRequest {

	private String cityId;
	private String pageNo;
	private String size;
}