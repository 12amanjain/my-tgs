package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfileBasicInfo {

	private String overview;
	private String description;
	private String exterior;
	private String lobby;
	private String restaurant;
	private String buildingNum;
	private Integer floors;
	private Integer numberOfRooms;
	private String childPolicy;
	private String cancelPolicy;
	private String ownerRecommendation;
	private Integer isVeg;
	private Integer hasAgreement;
}
