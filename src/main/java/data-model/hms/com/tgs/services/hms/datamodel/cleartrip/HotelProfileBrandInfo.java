package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfileBrandInfo {

	private String id;
	private String brand;
}
