package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfileData {

	private Integer id;
	private String thumbNailImage;
	private String hotelName;
	private String term;
	private String notice;
	private String parkingRemark;
	private String internetRemark;
	private String ctRecommendation;
	private Integer sellStatus;
	private HotelProfileLocationInfo locationInfo;
	private HotelProfileBasicInfo basicInfo;
	private HotelProfilePropertyInfo property;
	private HotelProfilePolicyInfo policyInfo;
	private HotelProfileBrandInfo brand;
	private HotelProfileRatingInfo ratings;
	private List<HotelProfileAmenityInfo> amenities;
	private List<HotelProfileImage> images;
}
