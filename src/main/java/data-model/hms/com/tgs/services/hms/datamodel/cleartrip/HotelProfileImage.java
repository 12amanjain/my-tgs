package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfileImage {

	private String originalImage;
	private String wideAngleImage;
	private String thumbNailImage;
	private String imageAlt;
	private String imageCategoryName;
	private String imageSubCategoryName;
	private String roomTypeId;
}
