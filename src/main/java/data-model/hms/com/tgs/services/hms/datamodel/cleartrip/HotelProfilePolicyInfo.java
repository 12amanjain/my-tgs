package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfilePolicyInfo {

	private String checkIn;
	private String checkOut;
	private String cancelPolicy;
	private String checkInInstructions;
	private String specialCheckInInstructions;
	private String mandatoryFees;
	private String optionalExtra;
	private String otherInformation;
}
