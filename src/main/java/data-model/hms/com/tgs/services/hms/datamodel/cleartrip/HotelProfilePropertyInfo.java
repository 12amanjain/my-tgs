package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelProfilePropertyInfo {

	private String propertyCode;
	private String propertyName;
	private String propertyType;
}
