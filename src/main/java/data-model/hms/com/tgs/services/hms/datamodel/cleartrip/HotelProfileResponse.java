package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class HotelProfileResponse {

	private String code;
	private HotelProfileData data;
}
