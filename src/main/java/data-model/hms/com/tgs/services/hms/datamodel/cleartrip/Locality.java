package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Locality {

	private String id;
	private String name;
	private String latitude;
	private String longitude;
	private String city;
	private String state;
	private String country;
	private String zip;
	private String localityName;
	private String localityId;
	private String localityLatitude;
	private String localityLongitude;
	private String address;
	private String stateCode;
	private String countryCode;
}
