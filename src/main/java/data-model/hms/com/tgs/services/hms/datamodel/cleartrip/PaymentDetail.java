package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentDetail {

	private String paymentType;
	private String amount;
	private String currency;
	private String status;
}
