package com.tgs.services.hms.datamodel.cleartrip;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RateBreakDown {

	private String date;
	private List<PricingElement> pricingElements;
}
