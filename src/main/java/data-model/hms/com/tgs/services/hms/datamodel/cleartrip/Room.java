package com.tgs.services.hms.datamodel.cleartrip;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Room {

	private String typeName;
	private String roomName;
	private Guest guests;
}