package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="customer")
@XmlAccessorType(XmlAccessType.FIELD)
public class Customer {
	
	@XmlElement
	private String username;
	@XmlElement
	private String password;
	@XmlElement
	private String id;
	@XmlElement
	private String source;
	@XmlElement
	private String product;
	@XmlElement
	private String language;
	
	@XmlElement
	private Request request;
	
}
