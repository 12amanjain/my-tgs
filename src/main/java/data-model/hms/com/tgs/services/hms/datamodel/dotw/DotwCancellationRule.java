package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwCancellationRule {
	
	@XmlElement
	private String fromDate;
	
	@XmlElement
	private String toDate;
	
	@XmlElement
	private Boolean amendRestricted;
	
	@XmlElement
	private Boolean cancelRestricted;
	
	@XmlElement
	private Boolean noShowPolicy;
	
	@XmlElement
	private Double amendCharge;
	
	@XmlElement
	private Double cancelCharge;
	
	@XmlElement
	private Double charge;

}
