package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="hotel")
@XmlAccessorType(XmlAccessType.FIELD)
public class DotwHotel {

	
	@XmlAttribute(name = "hotelid")
	private String hotelid;
	
	@XmlElement(name="fullAddress")
	private DotwAddress address;
	
	@XmlElement(name="description1")
	private DotwHotelDescription description1;
	
	@XmlElement(name="description2")
	private DotwHotelDescription description2;
	
	@XmlElement(name="hotelName")
	private String hotelName;
	
	@XmlElement(name="amenitie")
	private DotwHotelAmenity amenitie;
	
	@XmlElement(name="leisure")
	private DotwHotelAmenity leisure;
	
	@XmlElement(name="hotelPhone")
	private String hotelPhone;
	
	@XmlElement(name="rating")
	private String rating;
	
	@XmlElement(name="images")
	private DotwImages images;
	
	@XmlElement(name="geoPoint")
	private DotwHotelLocation geoPoint;
	
	@XmlElementWrapper(name = "rooms")
	@XmlElement(name="room")
	private List<DotwRoomResponse> rooms;
	
	
}
