package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
public class Filter {

	@XmlElement
	private String countryCode;
	@XmlElement
	private String countryName;
	@XmlElement
	private String city;
	@XmlElement
	private Boolean noPrice; 
	@XmlElement(namespace="http://us.dotwconnect.com/xsd/complexCondition")
	private DotwFilterComplexCriteria condition;
	
}
