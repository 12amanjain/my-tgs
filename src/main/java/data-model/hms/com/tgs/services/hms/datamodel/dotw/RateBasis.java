package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="rateBasis")
@XmlAccessorType(XmlAccessType.FIELD)
public class RateBasis {

	@XmlAttribute
	private String id;
	
	@XmlAttribute
	private String description;
	
	@XmlElement
	private Double total;
	
	@XmlElement
	private Double totalInRequestedCurrency; 
	

	@XmlElement
	private String status;
	
	@XmlElement
	private String passengerNamesRequiredForBooking;
	
	
	@XmlElementWrapper(name = "cancellationRules")
	@XmlElement(name="rule")
	private List<DotwCancellationRule> cancellationRules;
	
	@XmlElement
	private String withinCancellationDeadline;
	
	@XmlElement
	private String onRequest;
	
	@XmlElement
	private String tariffNotes;
	
	@XmlElementWrapper(name = "dates")
	@XmlElement(name="date")
	private List<DotwPerDayRoomPrice> date;
	
	
	private Integer leftToSell;
	
	@XmlElement
	private String allocationDetails;
	
	@XmlElement(name = "validForOccupancy")
	private ValidForOccupancy validForOccupancy;
	
	
	
}
