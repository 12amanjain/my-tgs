package com.tgs.services.hms.datamodel.dotw;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlRootElement(name="request")
@XmlAccessorType(XmlAccessType.FIELD)
public class Request {

	@XmlAttribute
	private String command;
	
	@XmlElement(name = "bookingDetails")
	private DotwSearchCriteria searchRequest;
	
	@XmlElement(name = "bookingDetails")
	private DotwBookingCriteria bookingRequest;
	
	@XmlElement(name = "bookingDetails")
	private DotwCancellationCriteria bookingCancellationRequest;
	
	@XmlElement
	private DotwBookingDetailRequest bookingDetails;
	
	@XmlElement(name = "return")
	private Return ret;
	
	
	public Return getRet() {
		if(ret == null) ret = new Return();
		return ret;
	}
	
	
}
