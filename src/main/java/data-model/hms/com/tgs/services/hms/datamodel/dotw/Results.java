package com.tgs.services.hms.datamodel.dotw;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@XmlRootElement(name="result")
@XmlAccessorType(XmlAccessType.FIELD)
public class Results {
	
	@XmlAttribute
	private String command;
	
	@XmlElement
	private String currencyShort;
	
	@XmlElementWrapper(name = "hotels")
	@XmlElement(name="hotel")
	private List<DotwHotel> hotels;
	
	@XmlElement(name="hotel")
	private DotwHotel hotel;
	
	@XmlElementWrapper(name = "countries")
	@XmlElement(name="country")
	private List<Country> countries;
	
	@XmlElementWrapper(name = "cities")
	@XmlElement(name="city")
	private List<DotwCity> cities;
	
	@XmlElement(name = "successful")
	private String successful;
	
	@XmlElement
	private String confirmationText;
	
	@XmlElement
	private String returnedCode;
	
	@XmlElement
	private String bookingCode;
	
	@XmlElementWrapper(name = "bookings")
	@XmlElement(name="booking")
	private List<DotwBooking> booking;
	

	@XmlElement(name = "itinerary")
	private DotwBookingDetailResponse bookingDetailResponse;
	
	@XmlElement
	private Integer productsLeftOnItinerary;
	
}
