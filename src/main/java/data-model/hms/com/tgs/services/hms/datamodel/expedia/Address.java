package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Address {

	private String line_1;
	private String line_2;
	private String city;
	private String state_province_name;
	private String postal_code;
	private String country_code;
	private Boolean obfuscated;
	private Localization localized;
}
