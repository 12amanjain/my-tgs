package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BedGroups {

	private String id;
	private String description;
	private PriceCheckLink links;
	private List<Configuration> configuration;
}

