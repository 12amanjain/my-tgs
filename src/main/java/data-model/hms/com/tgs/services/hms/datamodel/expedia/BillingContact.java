package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class BillingContact {

	private String given_name;
	private String family_name;
	private String email;
	private Phone phone;
	private Address address;
}

