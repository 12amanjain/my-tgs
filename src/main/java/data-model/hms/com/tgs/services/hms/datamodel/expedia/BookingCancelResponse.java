package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingCancelResponse {

	private String type;
	private String message;
}
