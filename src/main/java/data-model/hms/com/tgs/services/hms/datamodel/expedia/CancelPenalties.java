package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CancelPenalties {

	private String start;
	private String end;
	private String nights;
	private String currency;
	private String amount;
	private String percent;
}
