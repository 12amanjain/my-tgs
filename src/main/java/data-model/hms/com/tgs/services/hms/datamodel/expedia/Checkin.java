package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Checkin {

	private String begin_time;
	private String end_time;
	private String instructions;
	private String special_instructions;
	private String min_age;
}
