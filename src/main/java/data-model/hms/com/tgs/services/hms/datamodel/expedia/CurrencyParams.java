package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CurrencyParams {

	private ValueAndCurrency billable_currency;
	private ValueAndCurrency request_currency;
	private String scope;
	private String frequency;
}
