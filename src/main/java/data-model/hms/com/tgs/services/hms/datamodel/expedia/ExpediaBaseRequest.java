package com.tgs.services.hms.datamodel.expedia;

import java.util.HashMap;
import java.util.Map;

import com.tgs.services.base.helper.Exclude;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class ExpediaBaseRequest {

	@Exclude
	private Map<String, String> headerParams;
	
	@Exclude
	private String suffixOfURL;

	public Map<String, String> getHeaderParams() {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}
		headerParams.put("Accept", "application/json");
		return headerParams;
	}
	
	public void setAuthHeader(String authToken) {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}
		headerParams.put("Authorization", authToken);
	}
	
	public void setGzipHeader() {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}
		headerParams.put("Accept-Encoding", "gzip");
	}
}
