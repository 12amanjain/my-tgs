package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpediaBookingRequest {

	private String affiliate_reference_id;
	private boolean hold;
	private String email;
	private Phone phone;
	private List<ExpediaRoom> rooms;
	private List<Payment> payments;
}
