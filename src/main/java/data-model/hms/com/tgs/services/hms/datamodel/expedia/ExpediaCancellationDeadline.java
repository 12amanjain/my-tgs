package com.tgs.services.hms.datamodel.expedia;

import java.time.LocalDateTime;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpediaCancellationDeadline {

	private LocalDateTime deadlineDateTime;
}
