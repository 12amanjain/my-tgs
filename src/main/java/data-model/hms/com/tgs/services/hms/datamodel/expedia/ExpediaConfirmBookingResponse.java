package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpediaConfirmBookingResponse {

	private String type;
	private String message;
}
