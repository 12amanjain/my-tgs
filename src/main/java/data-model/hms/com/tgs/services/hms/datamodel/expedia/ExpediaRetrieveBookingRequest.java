package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class ExpediaRetrieveBookingRequest extends ExpediaBaseRequest {

	private String email;
}
