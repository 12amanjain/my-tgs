package com.tgs.services.hms.datamodel.expedia;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpediaRetrieveHoldBookingResponse {

	private String type;
	private String message;
	private String itinerary_id;
	private Map<String, URLSignature> links;
}
