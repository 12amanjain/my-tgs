package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class Field {

	private String name;
	private String type;
	private String value;
}
