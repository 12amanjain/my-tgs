package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ImportedRoomRate {

	private String id;
	private NightlyPrice cancel_refund;
	private Integer available_rooms;
	private Boolean refundable;
	private Boolean fenced_deal;
	private Boolean fenced_deal_available;
	private Boolean deposit_required;
	private String merchant_of_record;
	private Pricing pricing;
	private Map<String, BedGroups> bed_groups;
	private List<Long> amenities; 
	private PaymentOptionLink links;
	private List<CancelPenalties> cancel_penalties;
	private Promotions promotions;
	private Map<String, OccupancyPricing> occupancy_pricing;
}
