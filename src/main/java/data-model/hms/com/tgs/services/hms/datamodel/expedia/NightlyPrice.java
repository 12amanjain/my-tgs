package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class NightlyPrice {

	private String amount;
	private String type;
	private String value;
	private String currency;
}
