package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class OccupancyPricing {

	private List<List<NightlyPrice>> nightly;
	private List<NightlyPrice> stay;
	private OccupancyPricingTotal totals;
	private OccupancyFees fees;
}
