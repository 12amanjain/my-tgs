package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class OccupancyPricingTotal {

	private CurrencyParams strikethrough;
	private CurrencyParams exclusive;
	private CurrencyParams inclusive;
	private CurrencyParams marketing_fee;
	private CurrencyParams gross_profit;
}
