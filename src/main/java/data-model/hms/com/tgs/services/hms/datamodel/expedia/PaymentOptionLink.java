package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PaymentOptionLink {

	private URLSignature payment_options;
}
