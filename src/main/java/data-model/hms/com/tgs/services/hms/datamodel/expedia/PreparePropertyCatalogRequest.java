package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class PreparePropertyCatalogRequest extends ExpediaBaseRequest{

	private String language;
	private String property_id;
	private String country_code;
}
