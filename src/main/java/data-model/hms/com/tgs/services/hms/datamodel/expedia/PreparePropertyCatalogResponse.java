package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PreparePropertyCatalogResponse {

	private String method;
	private String href;
	private String expires;
}
