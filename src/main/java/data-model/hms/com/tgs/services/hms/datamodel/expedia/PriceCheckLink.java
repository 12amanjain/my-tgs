package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PriceCheckLink {

	private URLSignature price_check;
}
