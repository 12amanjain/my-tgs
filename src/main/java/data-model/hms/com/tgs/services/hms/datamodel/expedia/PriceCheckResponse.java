package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import java.util.Map;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceCheckResponse {
	
	private String status;
	private Map<String, OccupancyPricing> occupancy_pricing;
	private Map<String, URLSignature> links;
	private String type;
	private String message;
	private List<Field> fields;
}
