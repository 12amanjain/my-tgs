package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class PropertyCatalog {

	private String property_id;
	private String name;
	private Address address;
	private Location location;
	private List<Image> images;
	private String phone;
	private String rank;
	private Rating ratings;
	private BusinessModel business_model;
	private Date dates;
	private Statistics statistics;
	private Category category;
	private Descriptions descriptions;
	private Chain chain;
	private Brand brand;
	private Checkin checkin;
	private Policies policies;
	private Fees fees;
	private Map<String, Amenities> amenities;
	private String type;
	private String message;
}
