package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Rating {

	private Property property;
	private Guest guest;
}