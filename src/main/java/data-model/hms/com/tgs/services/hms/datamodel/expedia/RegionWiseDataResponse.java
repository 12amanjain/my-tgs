package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RegionWiseDataResponse {

	private String id;
	private String type;
	private String name;
	private String name_full;
	private String country_code;	
	private String message;
}