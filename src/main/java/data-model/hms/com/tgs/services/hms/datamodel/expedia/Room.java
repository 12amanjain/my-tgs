package com.tgs.services.hms.datamodel.expedia;

import java.util.List;
import java.util.Map;
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class Room {

	private String id;
	private String bed_group_id;
	private String checkin;
	private String checkout;
	private Integer number_of_adults;
	private List<Integer> child_ages;
	private String given_name;
	private String family_name;
	private Boolean smoking;
	private String status;
	private String room_name;
	private Map<String, URLSignature> links;
	private List<RoomRates> rates;
	private ImportedRoomRate rate;

}
