package com.tgs.services.hms.datamodel.expedia;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class ShoppingAPIRequest extends ExpediaBaseRequest {

	private String checkout;
	private String checkin;
	private String currency;
	private String language;
	private String country_code;
	private String sales_channel;
	private String sales_environment;
	private String sort_type;
	private String rate_plan_count;
	private String destination_iata_airport_code;
	private String cabin_class;
	private String rate_option;
	private String partner_point_of_sale;
	private String billing_terms;
	private String payment_terms;
	private String platform_name;
}
