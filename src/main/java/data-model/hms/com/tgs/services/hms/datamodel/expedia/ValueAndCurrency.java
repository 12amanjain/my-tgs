package com.tgs.services.hms.datamodel.expedia;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ValueAndCurrency {

	private String value;
	private String currency;
}
