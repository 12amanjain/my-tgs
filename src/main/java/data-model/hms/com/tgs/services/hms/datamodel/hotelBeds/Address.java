package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Address {

    private String languageCode;
    private String content;
    private String street;
    private String number;
}
