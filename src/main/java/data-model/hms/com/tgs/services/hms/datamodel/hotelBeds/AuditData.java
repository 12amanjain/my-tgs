package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class AuditData {

    private String processTime;
    private String timestamp;
    private String requestHost;
    private String serverId;
    private String environment;
    private String release;
    private String token;
    private String internal;
}
