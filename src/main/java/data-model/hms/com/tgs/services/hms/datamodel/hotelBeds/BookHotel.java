package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class BookHotel {

	public String checkOut;
	public String checkIn;
	public Integer code;
	public String name;
	public String categoryCode;
	public String categoryName;
	public String destinationCode;
	public String destinationName;
	public Integer zoneCode;
	public String zoneName;
	public String latitude;
	public String longitude;
	public List<BookingRoom> rooms = null;
	public String totalNet;
	public String currency;
	public Supplier supplier;
}
