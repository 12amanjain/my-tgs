package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class BookRequestRoom {

	private String rateKey;
	private List<Paxes> paxes = null;

}
