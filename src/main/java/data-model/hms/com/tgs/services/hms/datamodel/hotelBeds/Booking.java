package com.tgs.services.hms.datamodel.hotelBeds;

import java.time.LocalDate;
import lombok.Data;

@Data
public class Booking {

    public String reference;
    public String clientReference;
    public LocalDate creationDate;
    public String status;
    public ModificationPolicies modificationPolicies;
    public String creationUser;
    public Holder holder;
    public Hotel hotel;
    public String remark;
    public InvoiceCompany invoiceCompany;
    public Double totalNet;
    public Double pendingAmount;
    public String currency;

}
