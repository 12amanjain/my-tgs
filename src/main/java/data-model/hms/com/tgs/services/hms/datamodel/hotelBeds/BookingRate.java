package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class BookingRate {

    private String rateClass;
    private String net;
    private String sellingRate;
    private Boolean hotelMandatory;
    private String rateComments;
    private String paymentType;
    private Boolean packaging;
    private String boardCode;
    private String boardName;
    private List<CancellationPolicy> cancellationPolicies = null;
    private Integer rooms;
    private Integer adults;
    private Integer children;

}
