package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class BookingRoom {

	public String status;
	public Integer id;
	public String code;
	public String name;
	public List<Paxes> paxes = null;
	public List<BookingRate> rates = null;

}
