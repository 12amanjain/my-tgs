package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class CancellationPolicy {

    private double amount;
    private String from;
}
