package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class Country {

	private String code;
	private String isoCode;
	private Description description;
	private List<State> states = null;
}
