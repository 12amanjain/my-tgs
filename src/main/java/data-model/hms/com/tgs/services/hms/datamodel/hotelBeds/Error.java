package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Error {
	
	private String code;
	private String message;
}
