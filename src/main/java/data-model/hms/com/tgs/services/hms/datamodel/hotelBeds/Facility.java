package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Facility {

	private Integer facilityCode;
	private Integer facilityGroupCode;
	private Integer order;
	private Boolean indYesOrNo;
	private Boolean indFee;
	private Boolean indLogic;
	private double amount;
	private String currency;
	private String applicationType;
	private Integer number;
	private Boolean voucher;
}
