package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class FacilityDescription {

    private String languageCode;
    private String content;
}
