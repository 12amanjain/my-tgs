package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class FacilityDetail {

	private Integer code;
	private Integer facilityGroupCode;
	private Integer facilityTypologyCode;
	private FacilityDescription description;
}
