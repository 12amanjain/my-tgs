package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Getter;

@Getter
public enum HotelBedPaxType {

	ADULT("AD"), CHILD("CH");

	String code;

	HotelBedPaxType(String paxCode) {
		code = paxCode;
	}
}
