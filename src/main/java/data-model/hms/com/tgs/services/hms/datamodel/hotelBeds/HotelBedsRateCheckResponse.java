package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBedsRateCheckResponse extends HotelBedsBaseResponse{

	private AuditData auditData;
	private Hotel hotel;
	private Error error;
}
