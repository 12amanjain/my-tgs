package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
public class HotelBedsSearchRequest extends HotelBedsBaseRequest{

    private Stay stay;
    private List<Occupancy> occupancies;
    private searchRequestHotel hotels;
}
