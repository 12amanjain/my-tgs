package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBedsSearchResponse extends HotelBedsBaseResponse{

	private AuditData auditData;
	private Hotels hotels;
	private Error error;
}
