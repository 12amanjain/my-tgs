package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class HotelBedsStaticDataRequest {

	private String fields;
	private String language;
	private Integer from;
	private Integer to;
	private String useSecondaryLanguage;
}
