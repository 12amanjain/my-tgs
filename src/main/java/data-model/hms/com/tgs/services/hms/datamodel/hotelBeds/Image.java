package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Image {

    private String imageTypeCode;
    private String path;
    private Integer order;
    private Integer visualOrder;
    private String roomCode;
    private String roomType;
    private String characteristicCode;
}
