package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;
@Data
public class Issue {

    private String issueCode;
    private String issueType;
    private String dateFrom;
    private String dateTo;
    private Integer order;
    private Boolean alternative;
}
