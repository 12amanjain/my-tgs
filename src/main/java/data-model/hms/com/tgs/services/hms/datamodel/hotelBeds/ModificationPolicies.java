package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class ModificationPolicies {

	private Boolean cancellation;
	private Boolean modification;
}
