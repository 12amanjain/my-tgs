package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Occupancy {

	private Integer rooms;
	private Integer adults;
	private Integer children;
	private List<Paxes> paxes;
}
