package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Paxes {

    private Integer roomId;
    private String type;
    private Integer age;
    private String name;
    private String surname;
}
