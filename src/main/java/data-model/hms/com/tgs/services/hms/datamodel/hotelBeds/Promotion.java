package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Promotion {

	private String code;
	private String name;
}
