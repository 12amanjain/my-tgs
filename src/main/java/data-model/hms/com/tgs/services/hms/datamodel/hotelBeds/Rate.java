package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class Rate {

	private String rateKey;
	private String rateClass;
	private String rateType;
	private Double net;
	private double sellingRate;
	private Boolean hotelMandatory;
	private Integer allotment;
	private String paymentType;
	private Boolean packaging;
	private String boardCode;
	private String boardName;
	private List<CancellationPolicy> cancellationPolicies = null;
	private Taxes taxes;
	private Integer rooms;
	private Integer adults;
	private Integer children;
	private String childrenAges;
	private String rateCommentsId;
	private String rateComments;
	private List<Promotion> promotions = null;
	private String rateup;
}
