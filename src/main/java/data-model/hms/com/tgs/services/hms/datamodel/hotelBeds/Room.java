package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class Room {

	private String roomCode;
	private Integer minPax;
	private Integer maxPax;
	private Integer maxAdults;
	private Integer maxChildren;
	private Integer minAdults;
	private String roomType;
	private String characteristicCode;
	private List<RoomFacility> roomFacilities = null;
	private List<RoomStay> roomStays = null;
}
