package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class RoomFacility {

	private Integer facilityCode;
	private Integer facilityGroupCode;
	private Integer number;
	private Boolean indFee;
	private Boolean indYesOrNo;
	private Boolean voucher;
}
