package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class RoomResponse {

	public String code;
	public String name;
	public List<Paxes> paxes;
	public Integer id;
	public String status;
	public List<Rate> rates = null;
}
