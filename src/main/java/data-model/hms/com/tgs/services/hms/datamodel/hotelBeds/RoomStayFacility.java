package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class RoomStayFacility {

    private Integer facilityCode;
    private Integer facilityGroupCode;
    private Integer number;
}
