package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class StaticHotel {

	private Integer code;
	private Name name;
	private Description description;
	private String countryCode;
	private String stateCode;
	private String destinationCode;
	private Integer zoneCode;
	private Coordinates coordinates;
	private String categoryCode;
	private String categoryGroupCode;
	private String chainCode;
	private String accommodationTypeCode;
	private List<String> boardCodes = null;
	private List<Integer> segmentCodes = null;
	private Address address;
	private String postalCode;
	private City city;
	private String email;
	private String license;
	private List<Phone> phones = null;
	private List<Room> rooms = null;
	private List<Facility> facilities = null;
	private List<Terminal> terminals = null;
	private List<Issue> issues = null;
	private List<Image> images = null;
	private List<Wildcard> wildcards = null;
	private String web;
	private String lastUpdate;
	private String s2C;
	private Integer ranking;
}
