package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Stay {

	private String checkIn;
	private String checkOut;
}
