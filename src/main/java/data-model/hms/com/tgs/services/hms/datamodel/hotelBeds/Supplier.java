package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Supplier {

	private String name;
	private String vatNumber;
}
