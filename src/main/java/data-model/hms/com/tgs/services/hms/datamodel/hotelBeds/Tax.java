package com.tgs.services.hms.datamodel.hotelBeds;

import lombok.Data;

@Data
public class Tax {

	private Boolean included;
	private String amount;
	private String currency;
	private String clientAmount;
	private String clientCurrency;
}
