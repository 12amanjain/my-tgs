package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Data;

@Data
public class Taxes {

	private List<Tax> taxes = null;
	private Boolean allIncluded;
}
