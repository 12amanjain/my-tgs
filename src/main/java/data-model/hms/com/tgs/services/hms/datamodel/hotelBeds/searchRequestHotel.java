package com.tgs.services.hms.datamodel.hotelBeds;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class searchRequestHotel {

	private List<Integer> hotel;
}
