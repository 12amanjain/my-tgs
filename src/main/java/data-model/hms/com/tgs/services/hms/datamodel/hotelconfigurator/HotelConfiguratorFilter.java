package com.tgs.services.hms.datamodel.hotelconfigurator;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@ApiModel(value = "This is used to fetch airconfig rules based on search criteria. You can pass either any one field or combination of fields depending upon your search criteria")
@SuperBuilder
public class HotelConfiguratorFilter extends QueryFilter {

	@ApiModelProperty(notes = "To fetch based on ruleType ", example = "GNPUPROSE")
	private HotelConfiguratorRuleType ruleType;

	@ApiModelProperty(notes = "To fetch based on deleted config", example = "true")
	private boolean isDeleted;

	@ApiModelProperty(notes = "To fetch based on exit on match", example = "false")
	private Boolean exitOnMatch;
}