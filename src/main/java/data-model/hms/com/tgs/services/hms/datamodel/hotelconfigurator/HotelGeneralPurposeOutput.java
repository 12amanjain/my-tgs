package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelGeneralPurposeOutput implements IRuleOutPut {

	@SerializedName("proxy")
	private String proxyAddress;

	@SerializedName("dbe")
	private Boolean duplicateBookingEnabled;

	@SerializedName("hls")
	private Integer hotelListSizeLimit;
	
	@SerializedName("sfd")
	private Boolean showFareDiff;
	
	@SerializedName("src")
	private Integer searchRetryCount;

	@SerializedName("drc")
	private Integer detailRetryCount;
	
	@SerializedName("ccEmails")
	private Map<String, String> ccEmails;
	
	//List of suppliers which has no pan required 
	@SerializedName("spnr")
	private List<String> suppliersPanCardNotReqd;
}
