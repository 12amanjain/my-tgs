package com.tgs.services.hms.datamodel.hotelconfigurator;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelPromotionOutput implements IRuleOutPut {

	@SerializedName("lp")
	private Boolean lowestPrice;

	@SerializedName("ft")
	private Boolean flexibleTiming;

	@SerializedName("ph")
	private Boolean preferredHotel;

	private String msg;

}
