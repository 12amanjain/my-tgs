package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.hms.datamodel.TermsAndConditions;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSourceConfigOutput implements IRuleOutPut {

	@SerializedName("sket")
	private long searchKeyExpirationTime;

	@SerializedName("hurls")
	private List<String> hotelUrls;

	@SerializedName("igzip")
	private boolean isGzipEnabled;

	@SerializedName("rtll")
	private long roomTypeListLimit;

	@SerializedName("iao")
	private boolean isAvailableOnly;

	@SerializedName("isda")
	private boolean isStaticDataAllowed;

	@SerializedName("imgls")
	private long imageListSize;

	@SerializedName("srto")
	private long searchRequestTimeOut;

	@SerializedName("cur")
	private String currency;

	@SerializedName("tac")
	private TermsAndConditions termsAndConditions;

	@SerializedName("cig")
	private String cityInfoGranularity;

	@SerializedName("ma")
	private Integer maxAdultInRoom;


	@SerializedName("cpb")
	private Integer cancellationPolicyBuffer;

	@SerializedName("iora")
	private Boolean isOnRequestAllowed;

	@SerializedName("orh")
	private Integer onRequestHours;

	@SerializedName("hhc")
	private Integer hotelHitCount;

	@SerializedName("hbs")
	private Integer hotelBatchSize;

	@SerializedName("chc")
	private Integer cityHitCount;

	/*
	 * Expedia Specific Source Configuration Parameters
	 */

	@SerializedName("bt")
	private String billingTerms;

	@SerializedName("pt")
	private String paymentTerms;

	@SerializedName("pn")
	private String platformName;

	@SerializedName("ppos")
	private String partnerPointOfSale;

	@SerializedName("sc")
	private String salesChannel;

	@SerializedName("se")
	private String salesEnvironment;

	@SerializedName("st")
	private String sortType;

	@SerializedName("srpc")
	private String searchRatePlanCount;

	@SerializedName("drpc")
	private String detailRatePlanCount;

	@SerializedName("src")
	private Double sRetentionCommission;

	@SerializedName("sac")
	private Double sAgentCommssion;

	@SerializedName("smu")
	private Double supplierMarkup;

	@SerializedName("ihdrar")
	private Boolean isHotelDetailsRequiredAtReview;

	@SerializedName("phi")
	private Integer partitionHotelIdsLimit;

	/*
	 * Used only for TBO
	 */
	@SerializedName("token")
	private String supplierToken;

	/*
	 * Cleartrip Specific Source Configuration Parameters
	 */
	@SerializedName("offs")
	private String page;

	private String size;

	@SerializedName("dep")
	private Integer depositId;

	@SerializedName("hig")
	private String hotelInfoGranularity;

	// Desiya Specific
	@SerializedName("bn")
	private List<String> bookingNotes;

	@SerializedName("cl")
	private List<String> cityList;

	// HotelBeds Specific
	@SerializedName("tl")
	private double tolerance;
	@SerializedName("in")
	private Integer startIndex;
}
