package com.tgs.services.hms.datamodel.hotelconfigurator;

import java.util.List;
import java.util.regex.Pattern;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.HotelInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PropertyCriteria {

	@SerializedName("hnp")
	private String hotelNamePattern;
	@SerializedName("rt")
	private List<Integer> starRating;

	private boolean isValidStarRating(Integer starRating) {
		if (this.starRating == null || this.starRating.contains(starRating)) {
			return true;
		}
		return false;
	}

	private boolean isValidHotelNamePattern(String hotelName) {
		Pattern pattern = Pattern.compile(this.getHotelNamePattern(), Pattern.CASE_INSENSITIVE);
		return pattern.matcher(hotelName).find();
	}

	public boolean isPropertyCriteriaMatches(HotelInfo hInfo) {
		if (isValidHotelNamePattern(hInfo.getName()) && isValidStarRating(hInfo.getRating())) {
			return true;
		}
		return false;
	}
}
