package com.tgs.services.hms.datamodel.inventory;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExtraBedRate {

	@SerializedName("inf")
	List<Double> infant;
	
	@SerializedName("cr1")
	List<Double> childRange1;
	
	@SerializedName("cr2")
	List<Double> childRange2;
	
	@SerializedName("cr3")
	List<Double> childRange3;
	
	@SerializedName("ad")
	List<Double> adult;
}