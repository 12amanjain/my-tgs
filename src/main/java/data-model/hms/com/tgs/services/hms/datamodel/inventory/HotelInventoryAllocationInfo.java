package com.tgs.services.hms.datamodel.inventory;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class HotelInventoryAllocationInfo {

	@SerializedName("ti")
	public Integer totalInventory;
	
	@SerializedName("si")
	public Integer soldInventory;
}
