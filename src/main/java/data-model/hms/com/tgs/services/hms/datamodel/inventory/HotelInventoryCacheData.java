package com.tgs.services.hms.datamodel.inventory;

import java.time.LocalDate;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelInventoryCacheData {

	private String supplierHotelId;
	private LocalDate validOn;
	private String supplierId;
	private List<HotelRatePlan> ratePlanList;
	
}
