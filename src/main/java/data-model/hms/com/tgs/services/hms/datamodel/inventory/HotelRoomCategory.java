package com.tgs.services.hms.datamodel.inventory;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRoomCategory {
	
	private Long id;
	@SerializedName("rc")
	private String roomCategory;

}
