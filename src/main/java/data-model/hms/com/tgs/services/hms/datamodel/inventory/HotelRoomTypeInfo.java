package com.tgs.services.hms.datamodel.inventory;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelRoomTypeInfo {
	
	private Long id;
	private String roomTypeName;
	private Integer maxOccupancy;

}
