package com.tgs.services.hms.datamodel.qtech;

import java.util.List;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Amenities {

	List<HotelAmenities> HotelAmenities;
}
