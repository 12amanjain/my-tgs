package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookingDetail {

	private String Id;
	private String AgentId;
	private String BookingReference;
	private String BookingDate;
	private Double TotalCharges;
	private String LeaderTitle;
	private String LeaderFirstName;
	private String LeaderLastName;
	private String CurrencyCode;
	private String LocalHotelId;
	private String HotelName;
	private String CountryName;
	private String CityId;
	private String HotelAddress1;
	private String CurrentStatus;
	private String ExpirationDate;
	private String TotalAdults;
	private String TotalChildren;
	private String TotalRooms;
	private String CheckInDate;
	private String CheckOutDate;
	private String AgentRate;
	private String AgentRefNo;
	private String PassengerPhone;
	private String PassengerEmail;
	private String GrossAmount;
	private String HotelPhone;
	private String HotelRating;
	private String SelectedNights;
	private List<RoomDetail> RoomDetail;
	private String CommentContract;
	
}
