package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CancellationPolicyRequest extends QTechBaseRequest{

	private String hotel_id;
	private String unique_id;
	private String section_unique_id;		
}