package com.tgs.services.hms.datamodel.qtech;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Getter
@Setter
@ToString
public class CancellationPolicyResponse {
	
	private String Message;
	private String MessageInfo;
	private String CancellationCurrency;
	private String TotalBookingAmount;
	private String ContractComment;
	private String CancellationHours;
	private String AppliedAgentCharges;
	private BookingAllowedInfo BookingAllowedInfo;
	private String CancellationPolicy;
	private String StartTime;
	private String EndTime;

}
