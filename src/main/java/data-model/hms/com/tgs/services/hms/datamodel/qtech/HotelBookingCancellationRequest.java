package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingCancellationRequest extends QTechBaseRequest {

	private String booking_id;	
}
