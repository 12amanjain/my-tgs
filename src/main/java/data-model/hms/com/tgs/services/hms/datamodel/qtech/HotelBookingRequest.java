package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelBookingRequest extends QTechBaseRequest {
	
	private String unique_id;
	private String hotel_id;
	private String section_unique_id;
	private String expected_price;
	private String agent_ref_no;
	private String roomDetails;
}

