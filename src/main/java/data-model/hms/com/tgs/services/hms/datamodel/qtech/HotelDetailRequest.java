package com.tgs.services.hms.datamodel.qtech;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetailRequest extends QTechBaseRequest {

	private String hotel_id;
	private String unique_id;
}
