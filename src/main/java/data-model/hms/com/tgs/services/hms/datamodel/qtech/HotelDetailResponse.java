package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetailResponse {

	private String Message;
	private String HotelId;
	private String HotelName;
	private String Success;
	private String Description;
	private Amenities Amenities;
	private String email;
	private String website;
	private String longitude;
	private String latitude;
	private String HotelRating;
	private String HotelAddress;
	private List<HotelImages> HotelImages;
	private List<HotelProperty> SectionSelection;
	
}
