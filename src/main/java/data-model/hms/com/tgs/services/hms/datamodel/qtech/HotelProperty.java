package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelProperty {

	private Double DisplayRoomRate;
	private String Type;
	private String SectionUniqueId;
	private String[] RoomDetails;
	private String SupplierName;
	private List<RoomRates> RoomRates;

}
