package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchResponse {

	private Integer TotalCount;
	private String Success;
    private Double WebServiceVersion;
    private String Message;
    private String MessageInfo;
    private List<QTechHotelInfo> HotelList;
    private String SearchUniqueId;
    private String StartTime;
    private String EndTime;
	
	
}
