package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class QTechHotelInfo {

	private String HotelId;
	private String HotelName;
	private String LocalHotelId;
	private String PropertyRating;
	private Integer Available;
	private String ThumbNailUrl;
	private String RateCurrencyCode;
	private Double TotalCharges;
	private List<HotelProperty> HotelProperty;
	private String Address;
	private String ShortDescription;
	private String Latitude;
	private String Longitude;
}
