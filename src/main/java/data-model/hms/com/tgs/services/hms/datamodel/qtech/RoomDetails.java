package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter 
@ToString
@Builder
public class RoomDetails{
	
	private String numberOfChilds;
	private String roomClassId;
	private List<Travellers> passangers;

}