package com.tgs.services.hms.datamodel.qtech;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RoomRates {

	private Integer Available;
    private Integer NumberOfRooms;
    private Integer NumberOfAdults;
    private Integer NumberOfChild;
    private Double RoomRate;
    private String RoomType;
    private String RoomCategory; 
    private String MealBasis;
    private List<String> amenities;
    private String Note;
    private String ClassUniqueId;
    private List<PriceInfoQtech> RateBreakup;
	
}
