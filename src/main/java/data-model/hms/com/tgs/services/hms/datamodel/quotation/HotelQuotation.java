package com.tgs.services.hms.datamodel.quotation;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Image;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelQuotation {

	private String optionId;
	@Deprecated
	@SerializedName("chkin")
	private LocalDate checkinDate;
	@Deprecated
	@SerializedName("chkout")
	private LocalDate checkoutDate;
	@SerializedName("hn")
	private String hotelName;
	@SerializedName("add")
	private Address address;
	@SerializedName("pr")
	private Double price;
	@SerializedName("ds")
	private Double discount;
	@SerializedName("ia")
	private Boolean isAvailable;
	@SerializedName("fcs")
	private List<String> facilities;
	@SerializedName("rt")
	private Integer starRating;
	@SerializedName("img")
	private List<Image> images;
	@SerializedName("qri")
	private List<QuotationRoomInfo> quotationRoomInfoList;
	@SerializedName("sr")
	private HotelSearchQuery searchQuery;
	@SerializedName("cnp")
	private List<HotelCancellationPolicy> cancellationPolicy;

	public List<QuotationRoomInfo> getQuotationRoomInfoList() {
		if (quotationRoomInfoList == null) {
			quotationRoomInfoList = new ArrayList<>();
		}
		return quotationRoomInfoList;
	}

	public List<Image> getImages() {
		if (images == null) {
			images = new ArrayList<>();
		}
		return images;
	}

	public List<HotelCancellationPolicy> getCancellationPolicy() {
		if (cancellationPolicy == null) {
			cancellationPolicy = new ArrayList<>();
		}
		return cancellationPolicy;
	}
}
