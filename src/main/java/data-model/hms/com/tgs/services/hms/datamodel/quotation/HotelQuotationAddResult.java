package com.tgs.services.hms.datamodel.quotation;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelQuotationAddResult {

	private Long id;
	private String name;
	private String userId;
	private LocalDateTime createdOn;
	private List<HotelQuotation> quotationInfo;
}
