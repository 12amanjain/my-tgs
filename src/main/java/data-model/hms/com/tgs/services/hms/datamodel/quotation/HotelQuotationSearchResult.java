package com.tgs.services.hms.datamodel.quotation;

import java.time.LocalDateTime;
import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQuotationSearchResult {

	@SerializedName("id")
	private Long quotationId;
	@SerializedName("name")
	private String quotationName;
	private LocalDateTime createdOn;
	List<HotelQuotation> hotelQuotationList;
}
