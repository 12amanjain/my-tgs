package com.tgs.services.hms.datamodel.quotation;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class QuotationRoomInfo {

	@SerializedName("mb")
	private String mealBasis;
	@SerializedName("rc")
	private String roomCategory;
	@SerializedName("rt")
	private String roomType;
	@SerializedName("cnp")
	private List<HotelCancellationPolicy> cancellationPolicy;
	@SerializedName("rid")
	private String roomId;
	
	public List<HotelCancellationPolicy> getCancellationPolicy() {
		if (cancellationPolicy == null) {
			cancellationPolicy = new ArrayList<>();
		}
		return cancellationPolicy;
	}
}
