package com.tgs.services.hms.datamodel.supplier;

import com.tgs.services.base.helper.MaskedField;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CredentialsInfo {

	private String id;
	private String username;
	@MaskedField(unmaskedEnd = 2)
	private String password;
}