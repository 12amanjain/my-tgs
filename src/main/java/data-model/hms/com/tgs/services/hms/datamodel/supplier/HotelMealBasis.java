package com.tgs.services.hms.datamodel.supplier;

import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelMealBasis {

	private Integer id;
	private String fMealBasis;
	private String supplier;
	private String sMealBasis;
	private LocalDateTime createdOn;
	private String code;
	private String name;
}
