package com.tgs.services.hms.datamodel.supplier;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class HotelSourceInfo {

	private Integer id;
	private String name;
}
