package com.tgs.services.hms.datamodel.supplier;

import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.MaskedField;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public class HotelSupplierCredential {

	@ApiModelProperty(notes = "To provide hotel supplier user name", example = "abc")
	private String userName;

	@ApiModelProperty(notes = "To provide hotel supplier password", example = "xyz")
	@MaskedField(unmaskedEnd = 2)
	private String password;

	@ApiModelProperty(notes = "To provide hotel supplier endpoint", example = "https://abc.com")
	private String url;

	private Boolean isTestCredential;

	private String accountingCode;
	
	private String clientId;
	
	private String apiKey;
	
	private SupplierType type;
	
	@SerializedName("serv")
	private SupplierService service;
	
	@SerializedName("ecred")
	public List<CredentialsInfo> extranetCredentials;
	
	@SerializedName("re")
	private List<String> reservationEmail;
	
	@SerializedName("ce")
	private List<String> cancellationEmail;
	
	@SerializedName("cur")
	public String currency;
	
	@SerializedName("rm")
	public String remarks;
	
	@SerializedName("tz")
	public String timeZone;
	
	@SerializedName("add")
	public Address address;

	private Map<HotelUrlConstants, String> supplierUrl;
	
	@SerializedName("ipr")
	public Boolean isPanCardRequired;
	
	/*
	 * User Only For TBO
	 */
	private String token;

	public void cleanData() {
		TgsObjectUtils.getNotNullFieldValueMap(this, false, false).forEach((k, v) -> {
			if (v instanceof String && StringUtils.isBlank((String) v)) {
				try {
					java.lang.reflect.Field prop = this.getClass().getDeclaredField(k);
					prop.setAccessible(true);
					prop.set(this, null);
				} catch (Exception e) {
					log.error("Unable to clean data for {} due to ", GsonUtils.getGson().toJson(this), e);
				}
			}
		});
	}
}
