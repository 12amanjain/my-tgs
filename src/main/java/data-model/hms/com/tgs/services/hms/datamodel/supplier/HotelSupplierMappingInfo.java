package com.tgs.services.hms.datamodel.supplier;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelSupplierMappingInfo {	
	private Long hotelId;
	private String supplierHotelId;
	private String sourceName;
	private String supplierName;
}
