package com.tgs.services.hms.datamodel.supplier;

import lombok.Getter;

@Getter
public enum SupplierType {
	
	EXTERNAL("E"),
	INTERNAL("I"),
	SUBSTITUTE("S");
	
	private String code;

	SupplierType(String code) {
		this.code = code;
	}
}