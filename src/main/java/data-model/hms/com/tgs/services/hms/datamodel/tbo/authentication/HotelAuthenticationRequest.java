package com.tgs.services.hms.datamodel.tbo.authentication;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class HotelAuthenticationRequest extends TBOBaseRequest {

	private String ClientId;
	private String UserName;
	private String Password;
	private String EndUserIp;
	
}
