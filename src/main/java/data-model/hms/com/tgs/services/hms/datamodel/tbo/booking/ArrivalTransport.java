package com.tgs.services.hms.datamodel.tbo.booking;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ArrivalTransport {
	
	private Integer ArrivalTransportType;
	private String TransportInfoId;
	private LocalDateTime Time;
	

}
