package com.tgs.services.hms.datamodel.tbo.booking;

import java.util.List;
import com.tgs.services.hms.datamodel.tbo.search.Error;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BaseBookingResponse {

	private boolean isPriceChanged;
	private boolean isCancellationPolicyChanged;
	private List<HotelRoomDetail> HotelRoomDetails;
	private Integer ResponseStatus;
	private Error error;
	
}
