package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BlockRoomResult extends BaseBookingResponse{
	
	private String AvailabilityType;
	private String TraceId;
	private boolean IsPackageDetailsMandatory;
	private boolean IsPackageFare;
	private String HotelName;
	private String AddressLine1;
	
}
