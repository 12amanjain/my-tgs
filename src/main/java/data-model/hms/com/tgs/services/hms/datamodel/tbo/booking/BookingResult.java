package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookingResult extends BaseBookingResponse {

	private Boolean VoucherStatus;
	private Integer Status;
	private String HotelBookingStatus;
	private String ConfirmationNo;
	private String BookingRefNo;
	private String BookingId;
	
	
}
