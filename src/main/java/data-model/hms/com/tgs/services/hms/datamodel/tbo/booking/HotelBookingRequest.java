package com.tgs.services.hms.datamodel.tbo.booking;

import java.util.List;

import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelBookingRequest extends TBOBaseRequest {

	private String ResultIndex;
	private String HotelCode;
	private String HotelName;
	private String GuestNationality;
	private String NoOfRooms;
	private String ClientReferenceNo;
	private Boolean IsVoucherBooking;
	private String EndUserIp;
	private String TokenId;
	private String TraceId;
	private List<HotelRoomDetail> HotelRoomsDetails;
	private boolean IsPackageFare;
	private ArrivalTransport ArrivalTransport;
	
}
