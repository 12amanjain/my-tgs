package com.tgs.services.hms.datamodel.tbo.booking;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelBoutiqueBookingDetailRequest extends TBOBaseRequest {
	
	private String EndUserIp;
	private String TokenId;
	private String BookingId;
	private String TraceId;

}
