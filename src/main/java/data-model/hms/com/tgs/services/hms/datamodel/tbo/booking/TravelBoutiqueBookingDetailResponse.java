package com.tgs.services.hms.datamodel.tbo.booking;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class TravelBoutiqueBookingDetailResponse {

	private BookingDetailResult GetBookingDetailResult;
	
}
