package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelBookingCancellationRequest extends TBOBaseRequest{
	
	private Integer BookingMode;
	private Integer RequestType;
	private String Remarks;
	private String BookingId;
	private String EndUserIp;
	private String TokenId;
	

}
