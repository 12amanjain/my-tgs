package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelBookingCancellationStatusRequest extends TBOBaseRequest {

	private String EndUserIp;
	private String TokenId;
	private String ChangeRequestId;
	
}
