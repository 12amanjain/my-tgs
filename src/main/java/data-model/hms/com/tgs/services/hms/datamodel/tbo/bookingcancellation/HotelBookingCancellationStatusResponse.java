package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelBookingCancellationStatusResponse {

	private BookingCancellationStatus HotelChangeRequestStatusResult; 
	
	public boolean isSessionExpired() {
		
		if(this.getHotelChangeRequestStatusResult() != null
				&& this.getHotelChangeRequestStatusResult().getResponseStatus() == 4) {
			return true;
		}
		return false;
	}
	
}
