package com.tgs.services.hms.datamodel.tbo.bookingcancellation;

import java.util.List;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class RoomGuest {

	private Integer NoOfAdults;
	private Integer NoOfChild;
	private List<Integer> ChildAge;
}
