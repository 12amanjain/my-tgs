package com.tgs.services.hms.datamodel.tbo.mapping;

import javax.xml.bind.annotation.XmlElement;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class City {

	@XmlElement
	private String CityId;
	@XmlElement
	private String IsActive;
	@XmlElement
	private String CityName;
	@XmlElement
	private String CountryCode;
	
}
