package com.tgs.services.hms.datamodel.tbo.mapping;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CityInfoResponse {
	
	private String DestinationCityList;

}
