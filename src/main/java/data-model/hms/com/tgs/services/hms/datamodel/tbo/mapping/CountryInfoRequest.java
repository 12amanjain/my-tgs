package com.tgs.services.hms.datamodel.tbo.mapping;

import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryInfoRequest extends TBOBaseRequest {

	private String TokenId;
	private String ClientId;
	private String EndUserIp;
	
}
