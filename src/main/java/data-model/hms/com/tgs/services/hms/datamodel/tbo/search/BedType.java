package com.tgs.services.hms.datamodel.tbo.search;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BedType {
	
	private Integer BedTypeCode;
	private String BedTypeDescription;

}
