package com.tgs.services.hms.datamodel.tbo.search;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetail {

	private String HotelCode;
	private String HotelName;
	private String StarRating;
	private String HotelURL;
	private String Description;
	private List<String> HotelFacilities;
	private List<String> Images;
	private String Address;
	private String CountryName;
	private String Latitude;
	private String Longitude;
}
