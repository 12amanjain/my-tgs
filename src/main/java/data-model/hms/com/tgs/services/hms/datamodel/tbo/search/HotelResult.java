package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelResult {

	private Integer ResultIndex;
	private String HotelCode;
	private String HotelName;
	private String HotelCategory;
	private Integer StarRating;
	private String HotelDescription;
	private Price Price;
	private String HotelPicture;
	private String HotelAddress;
	private String Latitude;
	private String Longitude;
	private TripAdvisor TripAdvisor;
	
	
}
