package com.tgs.services.hms.datamodel.tbo.search;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.hms.datamodel.tbo.booking.Travellers;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelRoomDetail {

	private Integer RoomIndex;
	private String RoomTypeCode;
	private String RoomTypeName;
	private String RatePlanCode;
	private List<DayRate> DayRates; 
	private Price Price;
	private List<String> Amenities;
	private List<String> Amenity;
	private LocalDateTime LastCancellationDate;
	private List<CancellationPolicy> CancellationPolicies;
	private String CancellationPolicy;
	private String SmokingPreference;
	//private List<String> Supplements;
	private String Supplements;
	private List<BedType> BedTypes;
	private List<Travellers> HotelPassenger;
	private String InfoSource;
}
