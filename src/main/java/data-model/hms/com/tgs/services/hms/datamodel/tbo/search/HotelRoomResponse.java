package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelRoomResponse {

	private HotelRoomResult GetHotelRoomResult;
	
	public boolean isSessionExpired() {
		
		if(this.getGetHotelRoomResult() != null && this.getGetHotelRoomResult().getResponseStatus() != null){
			Integer responseStatus = this.getGetHotelRoomResult().getResponseStatus();
			if(responseStatus == 4) { return true; }
		}
		return false;
	}
	
	public boolean isUnderCancellationAllowedForAgent() {
		
		if(this.getGetHotelRoomResult().getIsUnderCancellationAllowed() != null) {
			return this.getGetHotelRoomResult().getIsUnderCancellationAllowed();
		}
		return true;
	}
	
}
