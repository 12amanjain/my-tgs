package com.tgs.services.hms.datamodel.tbo.search;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelRoomResult {

	private List<HotelRoomDetail> HotelRoomsDetails;
	private RoomCombinations RoomCombinations;
	private Boolean IsPolicyPerStay;
	private Boolean IsUnderCancellationAllowed;
	private Error error;
	private Integer ResponseStatus;
	
}
