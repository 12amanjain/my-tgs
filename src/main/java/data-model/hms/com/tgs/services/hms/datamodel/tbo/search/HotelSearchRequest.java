package com.tgs.services.hms.datamodel.tbo.search;

import java.util.Date;
import java.util.List;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.RoomGuest;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchRequest extends TBOBaseRequest {
	
	private String EndUserIp;
	private String TokenId;
	private String CheckInDate;
	private Integer NoOfNights;
	private String CountryCode;
	private Integer CityId;
	private String PreferredCurrency;
	private String GuestNationality;
	private Integer NoOfRooms;
	private List<RoomGuest> RoomGuests;
	private Integer MaxRating;
	private Integer MinRating;

}
