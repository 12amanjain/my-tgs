package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchResponse {
	
	private HotelSearchResult HotelSearchResult;
	
	public boolean isSessionExpired() {
		
		if(this.getHotelSearchResult() != null && this.getHotelSearchResult().getResponseStatus() != null 
				&& (this.getHotelSearchResult().getResponseStatus() == 3 
				|| this.getHotelSearchResult().getResponseStatus() == 4)) { return true; }
		return false;
	}

	
}
