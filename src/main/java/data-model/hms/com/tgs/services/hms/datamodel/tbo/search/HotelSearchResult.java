package com.tgs.services.hms.datamodel.tbo.search;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchResult {

	private String TraceId;
	private List<HotelResult> HotelResults;
	private Integer ResponseStatus;
	private Error Error;
	
}
