package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Price {

	private String CurrencyCode;
	private Double RoomPrice;
	private Double Tax;
	private Double ExtraGuestCharge;
	private Double ChildCharge;
	private Double OtherCharges;
	private Double Discount;
	private Double PublishedPrice;
	private Integer PublishedPriceRoundedOff;
	private Double OfferedPrice;
	private Integer OfferedPriceRoundedOff;
	private Double AgentCommission;
	private Double AgentMarkUp;
	private Double ServiceTax;
	private Double TDS;

}
