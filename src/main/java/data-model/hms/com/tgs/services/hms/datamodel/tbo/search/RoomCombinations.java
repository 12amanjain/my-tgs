package com.tgs.services.hms.datamodel.tbo.search;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoomCombinations {

	private String InfoSource;
	private List<RoomCombination> RoomCombination;
	
}
