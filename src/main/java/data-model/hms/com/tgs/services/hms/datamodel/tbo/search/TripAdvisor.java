package com.tgs.services.hms.datamodel.tbo.search;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripAdvisor {

	private Double Rating;
	private String ReviewURL;
	
}
