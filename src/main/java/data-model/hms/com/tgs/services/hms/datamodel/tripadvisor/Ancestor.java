package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Ancestor {
	private String abbrv;
	private String level;
	private String name;
	private String location_id;

}
