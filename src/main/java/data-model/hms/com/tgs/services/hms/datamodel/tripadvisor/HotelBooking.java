package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBooking {
	private boolean bookable;
	private String booking_url;

}
