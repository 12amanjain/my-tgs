package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRatingRequest {

	private String key;
	
}
