package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RankingData {

	private String ranking_string;
	private String ranking_out_of;
	private String geo_location_id; 
	private String ranking;
	private String geo_location_name;
	
}
