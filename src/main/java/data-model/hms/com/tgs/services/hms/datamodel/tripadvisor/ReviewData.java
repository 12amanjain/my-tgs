package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewData {

	private String location_id;
	private String name;
	
}
