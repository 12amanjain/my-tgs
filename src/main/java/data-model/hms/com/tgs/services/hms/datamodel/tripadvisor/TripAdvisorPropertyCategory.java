package com.tgs.services.hms.datamodel.tripadvisor;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TripAdvisorPropertyCategory {

	private String name;
	private String localized_name;
}
