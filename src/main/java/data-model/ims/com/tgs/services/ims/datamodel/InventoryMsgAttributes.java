package com.tgs.services.ims.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class InventoryMsgAttributes extends EmailAttributes {

	private String refId;
	
	private String inventoryName;
	
	private String inventoryId;
	
	private String travelDate;
	
	private String bookingDate;
	
	private String departure;
	
	private String departTime;
	
	private String arrival;

	private String arrivalTime;
	
	private String flightDetails;
	
	private String paxNames;
	
	private String airlinePnr;
	
	private String supplier;
	
	private String grossFare;
}
