package com.tgs.services.ims.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class InventoryOrder extends DataModel {

	private Long id;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private String inventoryId;

	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.IN)
	private String referenceId;

	@SearchPredicate(type = PredicateType.LTE)
	@SearchPredicate(type = PredicateType.GTE)
	private LocalDate validOn;

	private Product product;

	private List<FlightTravellerInfo> travellerInfo;

	@UserId
	private String bookingUserId;

	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = AirInventoryOrderInfo.class)})
	private InventoryOrderAdditionalInfo additionalInfo;

	@SearchPredicate(type = PredicateType.IN)
	@UserId
	private String supplierId;

}
