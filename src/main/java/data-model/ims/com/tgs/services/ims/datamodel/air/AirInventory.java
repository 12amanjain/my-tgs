package com.tgs.services.ims.datamodel.air;

import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.NotNull;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.fms.datamodel.SegmentInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AirInventory extends DataModel {

	private Long id;

	@ApiModelProperty(required = true)
	@NotNull
	private String name;

	@ForbidInAPIRequest
	private String fromAirport;

	@ForbidInAPIRequest
	private String toAirport;

	private List<SegmentInfo> segmentInfos;

	@ForbidInAPIRequest
	private Boolean isEnabled;

	private boolean isReturn;

	@ApiModelProperty(notes = "Time(in hrs) before departure to disable updating inventory", example = "48")
	private int disableBfrDept;

	@ForbidInAPIRequest
	private LocalDateTime createdOn;

	@UserId
	private String supplierId;

	private String carrier;

	@ApiModelProperty(required = true)
	private AirType tripType;

	@ForbidInAPIRequest
	private Boolean deleted;

}
