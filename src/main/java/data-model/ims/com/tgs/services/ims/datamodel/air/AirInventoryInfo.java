package com.tgs.services.ims.datamodel.air;

import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Builder
@Getter
@Setter
public class AirInventoryInfo {

	@SerializedName("d")
	private LocalDateTime departureTime;

	@SerializedName("a")
	private LocalDateTime arrivalTime;

}
