package com.tgs.services.ims.datamodel.air;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.ims.datamodel.InventoryOrderAdditionalInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class AirInventoryOrderInfo extends InventoryOrderAdditionalInfo {

	private String pnr;

	@SearchPredicate(type = PredicateType.IN, destinationEntity = "InventoryOrder", dbAttribute = "additionalInfo.source")
	private String source;

	@SearchPredicate(type = PredicateType.IN, destinationEntity = "InventoryOrder", dbAttribute = "additionalInfo.dest")
	private String dest;
	
	@SearchPredicate(type = PredicateType.IN, destinationEntity = "InventoryOrder", dbAttribute = "additionalInfo.airline")
	private String airline;
	
	private String flightNumbers;
	
}
