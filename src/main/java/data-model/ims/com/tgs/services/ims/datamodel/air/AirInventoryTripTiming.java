package com.tgs.services.ims.datamodel.air;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.Map;

@Builder
@Getter
@Setter
public class AirInventoryTripTiming {

	Map<String, AirInventoryInfo> tripTimings;
}
