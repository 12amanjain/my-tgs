package com.tgs.services.ims.datamodel.air;

import java.util.List;

import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.ims.datamodel.RatePlan;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class AirRatePlan extends RatePlan {

	private List<AirRatePlanInfo> ratePlanInfo;

	@ApiModelProperty(required = true)
	private CabinClass cabinClass;

	@ApiModelProperty(required = true)
	private String bookingClass;
}
