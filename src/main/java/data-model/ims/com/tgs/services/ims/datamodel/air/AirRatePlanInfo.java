package com.tgs.services.ims.datamodel.air;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ObjectUtils;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirRatePlanInfo {

	@SerializedName("pT")
	@NotNull
	private PaxType paxType;

	@SerializedName("bF")
	@NotNull
	private Double baseFare;

	@SerializedName("tR")
	private Double taxRate;

	@Exclude
	@SerializedName("cC")
	@ForbidInAPIRequest
	private String cabinClass;

	@Exclude
	@SerializedName("bC")
	@ForbidInAPIRequest
	private String bookingClass;

	private Double gst;

	public Double getBaseFare() {
		return ObjectUtils.defaultIfNull(baseFare, Double.valueOf(0));
	}

	public Double getTaxRate() {
		return ObjectUtils.defaultIfNull(taxRate, Double.valueOf(0));
	}

	public Double getGst() {
		return ObjectUtils.defaultIfNull(gst, Double.valueOf(0));
	}
}
