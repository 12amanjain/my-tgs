package com.tgs.services.ims.datamodel.air;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.ims.datamodel.SeatAllocationInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSeatAllocationAdditionalInfo extends SeatAllocationInfo {

	@SerializedName("aPnr")
	private String airlinePnr;
	
}
