package com.tgs.services.logging.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class LogMetaInfo {
	private String bookingId;
	private String userId;
	private String searchId;
	private Integer trips;
	private long timeInMs;
	private String endPoint;
	private String supplierId;
	private Integer sourceId;

}
