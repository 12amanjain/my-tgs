package com.tgs.services.messagingService.datamodel.sms;

import java.util.List;
import java.util.Map;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.enums.UserRole;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SmsAttributes {
	private List<String> recipientNumbers;

	/** It is unique key to identify which template to pick */
	private String key;

	private UserRole role;

	@Builder.Default
	private String partnerId = "0";

	/** Module/key specific attributes */
	private Map<String, String> attributes;

	@Builder.Default
	private MessageMedium medium = MessageMedium.SMS;

}
