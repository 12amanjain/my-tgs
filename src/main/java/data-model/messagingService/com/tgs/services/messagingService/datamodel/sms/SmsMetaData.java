package com.tgs.services.messagingService.datamodel.sms;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmsMetaData {

	private String body;
	
	private List<String> recipientNumbers;
}
