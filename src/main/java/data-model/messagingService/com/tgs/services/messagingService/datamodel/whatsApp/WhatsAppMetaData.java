package com.tgs.services.messagingService.datamodel.whatsApp;

import java.util.List;

import com.tgs.services.base.datamodel.AttachmentMetadata;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WhatsAppMetaData {

    private List<String> recipientNumbers;

    private String templateName;

    private List<String> attributes;
    
    private AttachmentMetadata attachmentData;
}
