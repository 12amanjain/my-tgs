package com.tgs.services.messagingService.datamodel.whatsApp;

public enum WhatsAppTemplateKey {

    HOTEL_VOUCHER,
    FLIGHT_TICKET
}
