package com.tgs.services.nss.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;


@Setter
@Getter
@Builder
public class NoShowInfo {

    private String userId;

    private String supplierId;

    private String bookingId;

    private LocalDateTime createdOn;

}