package com.tgs.services.oms.datamodel;

import java.time.LocalDateTime;
import com.tgs.services.base.datamodel.DataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirItemDetail extends DataModel {

    public AirItemDetail() {
        info = new AirItemDetailInfo();
    }

    private String bookingId;

    private AirItemDetailInfo info;

    private LocalDateTime createdOn;

    private Long id;

    public AirItemDetailInfo getInfo() {
        return info == null ? new AirItemDetailInfo() : info;
    }
}
