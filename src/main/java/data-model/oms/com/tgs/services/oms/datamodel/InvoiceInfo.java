package com.tgs.services.oms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.Builder;

@Getter
@Setter
@Builder
public class InvoiceInfo {

	private String name;

	private String email;

	private String phone;

	private String address;

	private String city;

	private String state;

	private String pincode;

	private String gstNumber;
}
