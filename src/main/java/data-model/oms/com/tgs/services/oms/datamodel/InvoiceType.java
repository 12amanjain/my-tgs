package com.tgs.services.oms.datamodel;

public enum InvoiceType {

	AGENCY, CUSTOMER, PARTNER;

}
