package com.tgs.services.oms.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.pms.datamodel.Payment;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {

	private String bookingId;
	@APIUserExclude
	@UserId
	private String bookingUserId;
	private String partnerId;
	/**
	 * Total Amount should be exclusive of markup Amount
	 */
	private double amount;
	private double markup;

	private OrderType orderType;
	@APIUserExclude
	private ChannelType channelType;
	@APIUserExclude
	private String loggedInUserId;

	private DeliveryInfo deliveryInfo;
	private OrderStatus status;
	private LocalDateTime createdOn;
	@APIUserExclude
	private LocalDateTime processedOn;
	@APIUserExclude
	private Long id;
	@APIUserExclude
	private OrderAdditionalInfo additionalInfo;
	private List<OrderItem> items;
	@APIUserExclude
	private String reason;
	private List<Payment> payments;
	@APIUserExclude
	private Set<OrderAction> actionList;

	public boolean isPaymentSuccessful() {
		if (this.getStatus().equals(OrderStatus.PAYMENT_SUCCESS)) {
			return true;
		}
		return false;
	}

	public OrderAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = OrderAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	public OrderAdditionalInfo getOrderAdditionalInfo(OrderAdditionalInfo defaultValue) {
		if (additionalInfo == null)
			return defaultValue;
		return additionalInfo;
	}
}
