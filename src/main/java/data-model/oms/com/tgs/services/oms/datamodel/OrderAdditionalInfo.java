package com.tgs.services.oms.datamodel;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.pms.datamodel.PaymentStatus;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OrderAdditionalInfo {
	private String reason;

	@SerializedName("auid")
	@UserId
	private String assignedUserId;

	@SerializedName("at")
	LocalDateTime assignedTime;

	@SerializedName("ft")
	private OrderFlowType flowType;

	@SerializedName("ps")
	private PaymentStatus paymentStatus;

	@SerializedName("iid")
	private String invoiceId;

	@SerializedName("cnf")
	private String confirmationNumber;

	@SerializedName("ips")
	private Boolean invoicePushStatus;

	@SerializedName("fut")
	private Map<String, LocalDateTime> firstUpdateTime;

	@SerializedName("irws")
	private Boolean isReconciledWithSupplier;

	@SerializedName("ifc")
	private Boolean isForceCancellation;

	private Map<String, String> invoiceMap;

	@SerializedName("rs")
	private ReconciliationStatus reconciliationStatus;

	@SerializedName("di")
	private List<OrderDisputeInfo> disputeInfo;

	@SerializedName("tid")
	private String tripId;

	@SerializedName("vc")
	private String voucherCode;

	public Map<String, LocalDateTime> getFirstUpdateTime() {
		if (firstUpdateTime == null)
			firstUpdateTime = new HashMap<>();
		return firstUpdateTime;
	}

}
