package com.tgs.services.oms.datamodel;

import lombok.Getter;

@Getter
public enum OrderDisputeStatus {

	IN_DISPUTE("ID"),
	RESOLVED("RS");
	
	public String getName() {
		return this.name();
	}

	private String code;

	private OrderDisputeStatus(String code) {
		this.code = code;
	}

	public static OrderDisputeStatus getEnumFromCode(String code) {
		return getOrderDisputeStatus(code);
	}
	
	public static OrderDisputeStatus getOrderDisputeStatus(String code) {
		for (OrderDisputeStatus disputeStatus : OrderDisputeStatus.values()) {
			if (disputeStatus.getCode().equals(code)) {
				return disputeStatus;
			}
		}
		return null;
	}
}
