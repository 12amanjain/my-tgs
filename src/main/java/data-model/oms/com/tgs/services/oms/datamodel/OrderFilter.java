package com.tgs.services.oms.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItemFilter;
import com.tgs.services.pms.datamodel.PaymentStatus;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@ToString
@NoArgsConstructor
@SuperBuilder
public class OrderFilter extends QueryFilter {

	@ApiModelProperty(
			notes = "To fetch records based on processed-on date. For example if you want to fetch users last processed after 25th May 2018",
			example = "2018-05-25")
	private LocalDate processedOnAfterDate;

	@ApiModelProperty(
			notes = "To fetch records based on processed-on date. For example if you want to fetch users last processed before 25th May 2018",
			example = "2018-05-25")
	private LocalDate processedOnBeforeDate;

	@ApiModelProperty(
			notes = "To fetch records based on processed-on datetime. For example if you want to fetch users last processed after 25th May 2018 12:50",
			example = "2018-05-25T12:50")
	private LocalDateTime processedOnAfterDateTime;

	@ApiModelProperty(
			notes = "To fetch records based on processed-on datetime. For example if you want to fetch users last processed before 25th May 2018 14:50",
			example = "2018-05-25T14:50")
	private LocalDateTime processedOnBeforeDateTime;

	@ApiModelProperty(notes = "To fetch booking for a particular bookingUserId ", allowableValues = "900024")
	private List<String> bookingUserIds;

	private List<String> loggedInUserIds;

	@ApiModelProperty(notes = "To fetch booking based on bookingId ", example = "123456")
	private String bookingId;
	@ApiModelProperty(notes = "To fetch booking based on bookingIds ", allowableValues = "123455")
	private List<String> bookingIds;
	@ApiModelProperty(notes = "To fetch booking according to statuses")
	private List<OrderStatus> statuses;
	@ApiModelProperty(notes = "To fetch booking according to channel types")
	private List<ChannelType> channels;
	@ApiModelProperty(notes = "To fetch booking according to product types")
	private List<OrderType> products;

	private List<String> assignedUserIdIn;

	private AirOrderItemFilter itemFilter;

	@ApiModelProperty(notes = "To fetch booking according to payment status")
	private List<PaymentStatus> paymentStatus;

	private HotelOrderItemFilter hotelItemFilter;

	private Boolean checkCartAssignment;

	private Boolean isPushedToAccounting;
}
