package com.tgs.services.oms.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class OrderMailAttributes extends EmailAttributes {

	private String bookingId;


}
