package com.tgs.services.oms.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public enum OrderStatus {

	IN_PROGRESS("IP") ,
	SUCCESS("S") ,
	ON_HOLD("OH") ,
	CANCELLED("C"),
	PAYMENT_SUCCESS("PS"),
	PAYMENT_PENDING("PP"),
	FAILED("F"),
	PENDING("P"),
	REJECTED("R"),
	ABORTED("AB"),
	UNCONFIRMED("UC"),
	CANCELLATION_PENDING("CP");

	public String getStatus() {
		return this.getCode();
	}

	private String code;

	OrderStatus(String code) {
		this.code = code;
	}

	public static OrderStatus getEnumFromCode(String code) {
		return getOrderStatus(code);
	}

	public static OrderStatus getOrderStatus(String code) {
		for (OrderStatus status : OrderStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<OrderStatus> statuses) {
		List<String> statusCodes = new ArrayList<>();
		statuses.forEach(status -> statusCodes.add(status.getCode()));
		return statusCodes;
	}

}
