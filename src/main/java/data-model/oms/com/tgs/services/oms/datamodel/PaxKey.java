package com.tgs.services.oms.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import java.util.Set;
import java.util.stream.Collectors;

@Getter
@Setter
public class PaxKey {

    private Long id;

    @SerializedName("pnr")
    private String airlinePnr;

    @SerializedName("tknum")
    private String ticketNumber;

    @SerializedName("pname")
    private String passengerName;


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof com.tgs.services.oms.datamodel.PaxKey) {
            return ((com.tgs.services.oms.datamodel.PaxKey) obj).getId().equals(this.getId());
        } else return false;
    }

    public static boolean equal(Set<com.tgs.services.oms.datamodel.PaxKey> paxSet1, Set<com.tgs.services.oms.datamodel.PaxKey> paxSet2) {
        if (paxSet1.size() != paxSet2.size()) return false;
        Set<Long> paxIdSet1 = paxSet1.stream().map(p -> p.getId()).collect(Collectors.toSet());
        Set<Long> paxIdSet2 = paxSet2.stream().map(p -> p.getId()).collect(Collectors.toSet());
        for (Long pId : paxIdSet1) {
            if (!paxIdSet2.contains(pId))
                return false;
        }
        return true;
    }
}
