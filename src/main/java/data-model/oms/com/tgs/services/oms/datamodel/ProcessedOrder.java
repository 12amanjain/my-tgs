package com.tgs.services.oms.datamodel;

import java.time.LocalDateTime;
import java.util.Set;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ProcessedOrder {

	private String bookingId;
	@UserId(userEmail = true, userMobile = true)
	private String bookingUserId;
	/**
	 * Total Amount should be exclusive of markup Amount
	 */
	private double amount;

	private Double discount;

	private Double netFare;


	private OrderType orderType;
	private ChannelType channelType;
	@UserId
	private String loggedInUserId;
	private OrderStatus status;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
	private String reason;
	private Set<OrderAction> actionList;
	private OrderAdditionalInfo additionalInfo;

}
