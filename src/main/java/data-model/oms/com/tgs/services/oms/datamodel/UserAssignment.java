package com.tgs.services.oms.datamodel;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class UserAssignment {

    private String userId;

    private LocalDateTime assignTime;

    private LocalDateTime releaseTime;

    public UserAssignment(String userId, LocalDateTime assignTime, LocalDateTime releaseTime) {
        this.userId = userId;
        this.assignTime = assignTime;
        this.releaseTime = releaseTime;
    }

    public UserAssignment() {
    }
}
