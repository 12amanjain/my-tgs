package com.tgs.services.oms.datamodel.air;

import java.util.List;

import com.tgs.services.base.enums.AirType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AdditionalInfoFilter {

	@ApiModelProperty(notes = "To fetch the booking based on trip type ")
	private List<AirType> tripTypes;
	@ApiModelProperty(notes = "To fetch booking according to the sources")
	private List<String> sourceIds;
}
