package com.tgs.services.oms.datamodel.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.helper.AirAPIExcludeV1;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PNRStatus;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.ItemDetails;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class AirOrderDetails implements ItemDetails {
	private List<TripInfo> tripInfos;
	@AirAPIExcludeV1
	private AirSearchQuery searchQuery;
	private List<ProcessedFlightTravellerInfo> travellerInfos;
	private PriceInfo totalPriceInfo;
	private LocalDateTime timeLimit;
	@AirAPIExcludeV1
	private List<SegmentInfo> segmentInfos;
	@AirAPIExcludeV1
	private List<String> termsConditions;
	private PNRStatus pnrStatus;

	public List<String> getTermsConditions() {
		if (termsConditions == null) {
			termsConditions = new ArrayList<>();
		}
		return termsConditions;
	}

}
