package com.tgs.services.oms.datamodel.air;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;

import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.TravellerStatus;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.DBExclude;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.datamodel.OrderItem;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
@AllArgsConstructor
public class AirOrderItem extends DataModel implements OrderItem, Validatable {
	private List<FlightTravellerInfo> travellerInfo;
	private String status;
	private String bookingId;
	private Long id;
	private String source;
	private String dest;
	private LocalDateTime departureTime;
	private LocalDateTime arrivalTime;
	private String supplierId;
	private Double amount;
	private Double markup;
	private String airlinecode;
	private String flightNumber;
	private AdditionalAirOrderItemInfo additionalInfo;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;

	@DBExclude
	@SerializedName("da")
	private AirportInfo departAirportInfo;

	@DBExclude
	@SerializedName("aa")
	private AirportInfo arrivalAirportInfo;

	@DBExclude
	@SerializedName("aI")
	private AirlineInfo airlineInfo;

	private OrderType type;

	@ApiModelProperty(hidden = true)
	public boolean hasTicketNumberForAllPax() {
		AtomicBoolean hasTicketNumber = new AtomicBoolean();
		for (FlightTravellerInfo travellerInfo : travellerInfo) {
			if (StringUtils.isBlank(travellerInfo.getTicketNumber())) {
				hasTicketNumber.set(Boolean.FALSE);
				// if any one pax doesn't has tikcet number then trip has not ticket number
				return hasTicketNumber.get();
			}
			hasTicketNumber.set(Boolean.TRUE);
		}
		return hasTicketNumber.get();
	}

	@ApiModelProperty(hidden = true)
	public boolean hasPNRForAllPNR() {
		AtomicBoolean hasPNR = new AtomicBoolean();
		for (FlightTravellerInfo travellerInfo : travellerInfo) {
			if (StringUtils.isBlank(travellerInfo.getPnr())) {
				hasPNR.set(Boolean.FALSE);
				// if any one pax doesn't has tikcet number then trip has not ticket number
				return hasPNR.get();
			}
			hasPNR.set(Boolean.TRUE);
		}
		return hasPNR.get();
	}

	@ApiModelProperty(hidden = true)
	public boolean isAllTravellerCancelled() {
		AtomicBoolean isCancelled = new AtomicBoolean();
		for (FlightTravellerInfo travellerInfo : travellerInfo) {
			if (!TravellerStatus.CANCELLED.equals(travellerInfo.getStatus())) {
				isCancelled.set(Boolean.FALSE);
				// if any one pax doesn't cancelled
				return isCancelled.get();
			}
			isCancelled.set(Boolean.TRUE);
		}
		return isCancelled.get();
	}

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		LocalDateTime arrivalTime = getArrivalTime(), departureTime = getDepartureTime();
		if (arrivalTime == null) {
			errors.put("arrivalTime", SystemError.INVALID_ARRIVAL_TIME.getErrorDetail());
		}

		if (departureTime == null) {
			errors.put("departureTime", SystemError.INVALID_DEPARTURE_TIME.getErrorDetail());
		}

		if (departureTime != null && arrivalTime != null && !arrivalTime.isAfter(departureTime)) {
			errors.put("departureTime", SystemError.INVALID_TRIP_TIMINGS.getErrorDetail());
			errors.put("arrivalTime", SystemError.INVALID_TRIP_TIMINGS.getErrorDetail());
		}

		// if (getType() == null) {
		// errors.put("type", SystemError.INVALID_ORDER_TYPE.getErrorDetail());
		// }

		if (additionalInfo != null && additionalInfo.getSegmentNo() < 0) {
			errors.put("additionalInfo.segmentNo", SystemError.INVALID_SEGMENT_NUMBER.getErrorDetail());
		}

		return errors;
	}

	/*
	 * returns formatted airline number and code.
	 * 
	 * @return 9W-123
	 */
	public String getFlightInfo() {
		return StringUtils.join(airlinecode, "-", flightNumber);
	}

	public AdditionalAirOrderItemInfo getAdditionalInfo() {
		if (additionalInfo == null) additionalInfo = AdditionalAirOrderItemInfo.builder().build();
		return additionalInfo;
	}

	public void setDuration() {
		long duration = 0;
		if (getDepartureTime() != null && getArrivalTime() != null) {
			TimeZone arrivalZone = TgsDateUtils.getTimeZoneOfCountry(getArrivalAirportInfo().getCountryCode());
			TimeZone departZone = TgsDateUtils.getTimeZoneOfCountry(getDepartAirportInfo().getCountryCode());
			ZonedDateTime onwardArrivalTime = getArrivalTime().atZone(arrivalZone.toZoneId());
			ZonedDateTime returnDepartureTime = getDepartureTime().atZone(departZone.toZoneId());
			ZonedDateTime returnZonedDepartureTime = returnDepartureTime.withZoneSameInstant(arrivalZone.toZoneId());
			if (returnZonedDepartureTime.isBefore(onwardArrivalTime))
				duration = Duration.between(returnZonedDepartureTime, onwardArrivalTime).toMinutes();
			else
				duration = Duration.between(onwardArrivalTime, returnZonedDepartureTime).toMinutes();

		}
		getAdditionalInfo().setDuration(duration);
	}
	
	public String getPlatingCarrier() {
		return ObjectUtils.firstNonNull(additionalInfo.getPlatingCarrier(), airlinecode);
	}
}
