package com.tgs.services.oms.datamodel.air;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.ProcessedTripInfo;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProcessedAirDetails implements ProcessedItemDetails {

	@SerializedName("tis")
	private List<ProcessedFlightTravellerInfo> travellerInfos;
	@SerializedName("ptis")
	private List<ProcessedTripInfo> processedTripInfos;
	
	private Double discount;

	private SearchType searchType;
	@SerializedName("at")
	private AirType airType;

	public List<ProcessedFlightTravellerInfo> getTravellerInfos() {
		if (travellerInfos == null) {
			travellerInfos = new ArrayList<>();
		}
		return travellerInfos;
	}
}
