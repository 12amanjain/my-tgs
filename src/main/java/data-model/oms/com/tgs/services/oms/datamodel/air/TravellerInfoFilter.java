package com.tgs.services.oms.datamodel.air;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class TravellerInfoFilter {

	@ApiModelProperty(notes = "To fetch booking based on passenger's first name", example = "Ashu")
	private String passengerFirstName;
	@ApiModelProperty(notes = "To fetch booking based on passenger's last name", example = "Gupta")
	private String passengerLastName;
	@ApiModelProperty(notes = "To fetch booking based on traveller's ticket number ", example = "3000578041")
	private String ticketNumber;
	@ApiModelProperty(notes = "To fetch booking based on PNR ", example = "MZ51KB")
	private String pnr;
	@ApiModelProperty(notes = "To fetch booking based on GDS PNR ", example = "YQ11LB")
	private String gdsPnr;
	
}
