package com.tgs.services.oms.datamodel.amendments;

import java.time.LocalDateTime;
import java.util.Set;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class Amendment {

	@Exclude
	private Long id;

	@SearchPredicate(type = PredicateType.IN)
	private String amendmentId;

	@SearchPredicate(type = PredicateType.IN)
	private String bookingId;

	@SearchPredicate(type = PredicateType.IN)
	@APIUserExclude
	private AmendmentType amendmentType;

	@SearchPredicate(type = PredicateType.IN)
	private AmendmentStatus status;

	@SearchPredicate(type = PredicateType.LTE)
	@SearchPredicate(type = PredicateType.GTE)
	private Double amount;

	@APIUserExclude
	private AmendmentAdditionalInfo additionalInfo;

	@APIUserExclude
	private ModifiedInfo modifiedInfo;

	@ForbidInAPIRequest
	@UserId
	@APIUserExclude
	private String loggedInUserId;

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	@ForbidInAPIRequest
	@UserId
	@APIUserExclude
	private String bookingUserId;
	
	@UserId
	@SearchPredicate(type = PredicateType.IN)
	private String partnerId;

	@SearchPredicate(type = PredicateType.IN)
	@UserId
	private String assignedUserId;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	@APIUserExclude
	private LocalDateTime assignedOn;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	@APIUserExclude
	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	@APIUserExclude
	private LocalDateTime processedOn;

	@APIUserExclude
	private Set<AmendmentAction> actionList;

	@SearchPredicate(type = PredicateType.EQUAL, skipCriteriaSearch = true)
	@APIUserExclude
	private Boolean intl;
	
	@SearchPredicate(type = PredicateType.IN)
	private OrderType orderType;
	
	@APIUserExclude
	@SearchPredicate(type = PredicateType.EQUAL)
	private ChannelType channelType;

	public void setAmendmentAmount() {
		amount = 0d;
		if (AmendmentType.REISSUE_QUOTATION.equals(amendmentType))
			return;
		amount += ObjectUtils.firstNonNull(this.getAdditionalInfo().getOrderDiffAmount(), 0d);
	}

	public ModifiedInfo getModifiedInfo() {
		if (modifiedInfo == null)
			modifiedInfo = new ModifiedInfo();
		return modifiedInfo;
	}

	public AirAdditionalInfo getAirAdditionalInfo() {
		return additionalInfo.getAirAdditionalInfo();
	}

	public HotelAdditionalInfo getHotelAdditionalInfo() {
		return additionalInfo.getHotelAdditionalInfo();
	}
}
