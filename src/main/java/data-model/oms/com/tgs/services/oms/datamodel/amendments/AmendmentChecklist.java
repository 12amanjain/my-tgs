package com.tgs.services.oms.datamodel.amendments;

import com.tgs.services.base.enums.AmendmentType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmendmentChecklist {

	private short id;

	private AmendmentType type;

	private String text;

	private boolean checked;
}
