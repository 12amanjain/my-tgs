package com.tgs.services.oms.datamodel.amendments;

import com.tgs.services.base.enums.UserRole;

import lombok.Getter;
import lombok.Setter;

@Getter
public enum AmendmentStatus {

	REQUESTED("REQ"),

	ASSIGNED("ASN"),

	PROCESSING("PRC"),

	PENDING_WITH_SUPPLIER("PEN"),

	PENDING_REASSIGN("PRASN"),

	REJECTED("RJ"),

	PAYMENT_FAILED("PF"),

	PAYMENT_SUCCESS("PS"),

	SUCCESS("S");


	private String code;

	@Setter
	private UserRole userRole;

	AmendmentStatus(String code) {
		this.code = code;
	}

	public static AmendmentStatus getEnumFromCode(String code) {
		return getAmendmentStatus(code);
	}

	public static AmendmentStatus getAmendmentStatus(String code) {
		for (AmendmentStatus status : AmendmentStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}
}
