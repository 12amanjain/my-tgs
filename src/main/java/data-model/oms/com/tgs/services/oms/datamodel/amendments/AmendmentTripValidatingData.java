package com.tgs.services.oms.datamodel.amendments;

import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.enums.AmendmentType;
import lombok.Builder;
import lombok.Getter;


@Getter
@Builder
public class AmendmentTripValidatingData implements ValidatingData {

	private AmendmentType amendmentType;
}
