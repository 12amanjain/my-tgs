package com.tgs.services.oms.datamodel.amendments;

import lombok.Getter;
import java.util.List;

@Getter
public class ChecklistData {

    private List<AmendmentChecklist> data;
}
