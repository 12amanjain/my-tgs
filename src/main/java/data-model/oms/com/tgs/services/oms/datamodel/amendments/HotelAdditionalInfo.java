package com.tgs.services.oms.datamodel.amendments;

import java.util.Set;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.HotelInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelAdditionalInfo {
	
	private Set<String> roomKeys;
	
	@SerializedName("pssh")
	private HotelInfo orderPreviousSnapshot;

	@SerializedName("nssh")
	private HotelInfo orderCurrentSnapshot;
	
}

