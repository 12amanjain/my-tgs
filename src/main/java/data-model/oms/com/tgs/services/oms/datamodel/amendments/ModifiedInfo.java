package com.tgs.services.oms.datamodel.amendments;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModifiedInfo {

    private List<AirOrderItem> airOrderItems;
    private HotelInfo  hotelInfo;
    @SerializedName("pwa")
    private Map<String,Double> paxWiseAmount;
    
    public Map<String,Double> getPaxWiseAmount() {
    	if(paxWiseAmount==null)
    		paxWiseAmount = new HashMap<String,Double>();
    	return paxWiseAmount;
    }
    
}
