package com.tgs.services.oms.datamodel.amendments;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.VoidClean;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PWSReason implements IRuleOutPut, VoidClean {

	private Short id;

	private String text;

    private Short holdTime;

	@Override
	public void cleanData() {
		setText(StringUtils.defaultIfBlank(text, null));
	}

	@Override
	public boolean isVoid() {
		return id ==null && text==null && holdTime==null;
	}
    
}
