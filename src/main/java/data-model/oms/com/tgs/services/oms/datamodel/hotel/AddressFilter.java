package com.tgs.services.oms.datamodel.hotel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressFilter {

	private List<String> cityList;
	private List<String> countryList;
}
