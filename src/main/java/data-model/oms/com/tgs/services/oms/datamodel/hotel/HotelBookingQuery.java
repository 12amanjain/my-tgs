package com.tgs.services.oms.datamodel.hotel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingQuery {

	private List<RoomTravellerInfo> roomTravellerInfo;
	
}
