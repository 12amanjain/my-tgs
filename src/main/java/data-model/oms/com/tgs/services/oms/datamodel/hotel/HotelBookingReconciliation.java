package com.tgs.services.oms.datamodel.hotel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.hms.datamodel.RoomInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingReconciliation {
	
	private String bookingId;

	private String bookingStatus;

	private String hotelName;

	private LocalDate checkInDate;

	private LocalDate checkOutDate;
	
	private LocalDate bookingDate;

	private Double amount;

	private String reconcilerUserId;
	
	private LocalDateTime cancellationDeadline;
	
	private HotelBookingReconciliationAdditionalInfo additionalInfo;
	
	private List<RoomInfo> roomInfos;
}