package com.tgs.services.oms.datamodel.hotel;

import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelBookingReconciliationAdditionalInfo {

	private String currency;
	
	private AddressInfo addressInfo;
	
	private DeliveryInfo deliveryInfo;
}
