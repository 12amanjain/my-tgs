package com.tgs.services.oms.datamodel.hotel;

import com.tgs.services.base.datamodel.DeliveryInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelImportBookingParams {

	private String supplierBookingId;
	private String bookingId;
	private String supplierId;
	private DeliveryInfo deliveryInfo;
	private Double marketingFees;
}
