package com.tgs.services.oms.datamodel.hotel;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.datamodel.OrderStatus;

import lombok.Getter;

@Getter
public enum HotelItemStatus {

	IN_PROGRESS("IP", OrderStatus.IN_PROGRESS) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(
					Arrays.asList(ON_HOLD, PAYMENT_FAILED, PAYMENT_SUCCESS, ABORTED, HOLD_PENDING, UNCONFIRMED));
		}
	},

	ON_HOLD("OH", OrderStatus.ON_HOLD) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, ABORTED, PAYMENT_SUCCESS, UNCONFIRMED, CANCELLATION_PENDING));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.values()));
		}
	},

	HOLD_PENDING("HP", OrderStatus.PENDING) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(
					Arrays.asList(SUCCESS, ABORTED, ON_HOLD, PAYMENT_FAILED, PAYMENT_SUCCESS, UNCONFIRMED));
		}
	},

	PAYMENT_SUCCESS("PS", OrderStatus.PAYMENT_SUCCESS) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, ABORTED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.values()));
		}

	},

	SUCCESS("S", OrderStatus.SUCCESS) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(CANCELLATION_PENDING, CANCELLED));
		}

		@Override
		public Set<AmendmentType> validAmdTypes() {
			return new HashSet<>(Arrays.asList(AmendmentType.values()));
		}
	},
	PENDING("P", OrderStatus.PENDING) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(SUCCESS, ON_HOLD));
		}
	},

	PAYMENT_FAILED("F", OrderStatus.FAILED) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(ABORTED));
		}
	},

	ABORTED("A", OrderStatus.ABORTED) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	CANCELLED("C", OrderStatus.CANCELLED) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	UNCONFIRMED("UC", OrderStatus.UNCONFIRMED) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>();
		}
	},
	CANCELLATION_PENDING("CP", OrderStatus.CANCELLATION_PENDING) {
		@Override
		public Set<HotelItemStatus> nextStatusSet() {
			return new HashSet<>(Arrays.asList(CANCELLED, UNCONFIRMED));
		}
	};

	public String getStatus() {
		return this.getCode();
	}

	private String code;
	private OrderStatus orderStatus;

	HotelItemStatus(String code, OrderStatus orderStatus) {
		this.code = code;
		this.orderStatus = orderStatus;
	}

	public static HotelItemStatus getHotelItemStatus(String code) {
		for (HotelItemStatus role : values()) {
			if (role.getCode().equals(code)) {
				return role;
			}
		}
		return null;
	}

	public abstract Set<HotelItemStatus> nextStatusSet();

	public Set<AmendmentType> validAmdTypes() {
		return new HashSet<>();
	}

	public static HotelItemStatus getStatus(String status) {
		for (HotelItemStatus role : values()) {
			if (role.getOrderStatus().getCode().equals(status)) {
				return role;
			}
		}
		return null;
	}

	public static HotelItemStatus getEnumFromCode(String code) {
		return getHotelItemStatus(code);
	}
}
