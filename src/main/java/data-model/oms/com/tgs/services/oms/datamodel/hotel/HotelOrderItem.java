package com.tgs.services.oms.datamodel.hotel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.RoomInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class HotelOrderItem extends DataModel{
	
	private Long id;
	private String bookingId;
	private String hotel;
	private LocalDateTime createdOn;
	private LocalDate checkInDate;
	private LocalDate checkOutDate;
	private String supplierId;
	private String roomName;
	private List<TravellerInfo> travellerInfo;
	private Double amount;
	private Double markup;
	private String status;
	private AdditionalHotelOrderItemInfo additionalInfo;
	private RoomInfo roomInfo;

}
