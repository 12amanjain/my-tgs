package com.tgs.services.oms.datamodel.hotel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.oms.datamodel.OrderItemFilter;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class HotelOrderItemFilter extends QueryFilter implements OrderItemFilter {
	
	private TravellerInfoFilter travellerInfoFilter;
	private AdditionalInfoFilter additionalInfoFilter;
	private LocalDate checkInAfterDate;
	private LocalDate checkInBeforeDate;
	private String hotelName;
	private AddressFilter addressFilter;
	private List<String> supplierIds;
	private LocalDateTime deadlineDateAfterDateTime;
	private LocalDateTime deadlineDateBeforeDateTime;
	private String hotelBookingReference;
}
