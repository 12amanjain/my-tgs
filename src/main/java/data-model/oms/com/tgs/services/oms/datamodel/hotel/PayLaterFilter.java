package com.tgs.services.oms.datamodel.hotel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayLaterFilter {

	private String scanHours;
	private String status;
	
}
