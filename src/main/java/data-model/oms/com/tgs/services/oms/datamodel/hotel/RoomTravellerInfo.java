package com.tgs.services.oms.datamodel.hotel;

import java.util.List;

import com.tgs.services.base.datamodel.TravellerInfo;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class RoomTravellerInfo {
	
	private List<TravellerInfo> travellerInfo;
	private String id;

}
