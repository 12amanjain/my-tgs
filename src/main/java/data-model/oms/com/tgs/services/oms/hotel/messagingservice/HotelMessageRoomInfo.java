package com.tgs.services.oms.hotel.messagingservice;

import com.tgs.services.hms.datamodel.RoomInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelMessageRoomInfo {
	
	private int roomNumber;
	private RoomInfo roomInfo;

}
