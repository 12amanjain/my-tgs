package com.tgs.services.gms.datamodel.Incidence;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillDueNotification implements IncidenceData {

    private Short notifyBeforeHours;
}
