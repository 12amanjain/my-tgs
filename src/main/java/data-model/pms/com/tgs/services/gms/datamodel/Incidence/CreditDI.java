package com.tgs.services.gms.datamodel.Incidence;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditDI implements IncidenceData {

    private Short payWithinHours;
}
