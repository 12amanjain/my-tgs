package com.tgs.services.gms.datamodel.Incidence;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PartialBillPayment implements IncidenceData {

    private Short billPayPercent;
}
