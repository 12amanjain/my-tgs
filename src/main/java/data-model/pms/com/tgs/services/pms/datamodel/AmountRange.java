package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import lombok.Data;

@Data
public class AmountRange {

	private BigDecimal minAmount;
	private BigDecimal maxAmount;

	/**
	 * Range contains a amount if the amount
	 * <li>is non-null,
	 * <li>given a non-null minAmount, is greater than or equal to minAmount
	 * <li>given a non-null maxAmount, is smaller than or equal to maxAmount
	 * 
	 * @param amount
	 * @return
	 */
	public boolean contains(BigDecimal amount) {
		return amount != null && (minAmount == null || minAmount.compareTo(amount) <= 0)
				&& (maxAmount == null || maxAmount.compareTo(amount) >= 0);
	}
}
