package com.tgs.services.pms.datamodel;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

import com.tgs.services.base.datamodel.CardType;

@Getter
@Setter
@Builder
public class CCInfo {
    private String cardNumber;
    private String cardCVV;
    private CardType cardType;
    private String expiryDate;
    private String holderName;
}
