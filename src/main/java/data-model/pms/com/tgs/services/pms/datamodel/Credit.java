package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import com.tgs.filters.CreditFilter;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.NRCreditAdditionalInfoFilter;
import com.tgs.filters.NRCreditFilter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.UserId;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

@Getter
@Setter
@Accessors(chain = true, fluent = true)
public class Credit {

	@UserId
	@SearchPredicate(type = PredicateType.IN)
	private String userId;
	
	@UserId
	@SearchPredicate(type = PredicateType.IN)
	private String partnerId;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String creditNumber;

	@SearchPredicate(type = PredicateType.IN)
	private String policy;

	private BigDecimal creditLimit;

	@SearchPredicate(type = PredicateType.GTE)
	@SearchPredicate(type = PredicateType.LTE)
	private BigDecimal utilized;

	@SearchPredicate(type = PredicateType.GTE)
	@SearchPredicate(type = PredicateType.LTE)
	private BigDecimal outStanding;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime periodStart;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime periodEnd;

	@UserId(userRelation = true)
	@SearchPredicate(type = PredicateType.IN)
	private String issuedByUserId;

	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.EQUAL)
	private CreditStatus status;

	private List<Product> products;

	private BigDecimal maxTempExt;
	private BigDecimal currentTempExt;

	private Double creditLimitExtension;

	private List<String> incidences;

	private CreditType creditType;

	public static CreditLineFilter creditFilterToCreditLineFilter(CreditFilter creditFilter) {
		return CreditLineFilter.builder().outstandingBalanceGreaterThanEqual(creditFilter.utilizedGreaterThanEqual)
				.outstandingBalanceLessThanEqual(creditFilter.utilizedLessThanEqual).userIdIn(creditFilter.userIdIn)
				.status(creditFilter.status).creditNumber(creditFilter.creditNumber).policyIdIn(creditFilter.policyIn)
				.issuedByUserIdIn(creditFilter.issuedByUserIdIn)
				.createdOnAfterDate(creditFilter.getCreatedOnAfterDate())
				.createdOnBeforeDate(creditFilter.getCreatedOnBeforeDate()).build();
	}

	public static NRCreditFilter creditFilterToNRCreditFilter(CreditFilter creditFilter) {
		NRCreditAdditionalInfoFilter additionalInfoFilter = NRCreditAdditionalInfoFilter.builder()
				.policyIn(creditFilter.policyIn).build();
		return NRCreditFilter.builder().utilizedGreaterThan(creditFilter.utilizedGreaterThanEqual)
				.utilizedLessThan(creditFilter.utilizedLessThanEqual).userIdIn(creditFilter.userIdIn)
				.status(creditFilter.status).creditId(creditFilter.creditNumber)
				.issuedByUserIdIn(creditFilter.issuedByUserIdIn)
				.createdOnAfterDate(creditFilter.getCreatedOnAfterDate())
				.createdOnBeforeDate(creditFilter.getCreatedOnBeforeDate()).additionalInfo(additionalInfoFilter)
				.partnerIdIn(creditFilter.partnerIdIn)
				.build();

	}
}
