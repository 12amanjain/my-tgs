package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.FilterMapping;
import com.tgs.services.base.annotations.Inject;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.Group;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.NotNull;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class CreditBill extends DataModel {

	@SearchPredicate(type = PredicateType.IN)
	private String billNumber;

	@UserId
	@Inject(dbEntity = "DbUser", field = "userId")
	@Inject(dbEntity = "DbUserRelation", field = "userId1")
	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;

	@Exclude
	@NotNull(groups = Group.class)
	@SearchPredicate(type = PredicateType.EQUAL)
	private Long creditId;

	private BillAdditionalInfo additionalInfo;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime paymentDueDate;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private LocalDateTime lockDate;

	@SearchPredicate(type = PredicateType.GTE)
	@SearchPredicate(type = PredicateType.LTE)
	private BigDecimal billAmount;

	private BigDecimal settledAmount;

	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isSettled;

	private Long daysSinceDueDate;

	private Integer extensionCount;

	@Exclude
	private Long id;

	public BillAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null)
			additionalInfo = BillAdditionalInfo.builder().build();
		return additionalInfo;
	}

	public BigDecimal getSettledAmount() {
		return ObjectUtils.defaultIfNull(settledAmount, BigDecimal.ZERO);
	}

	public Integer getExtensionCount() {
		return ObjectUtils.defaultIfNull(extensionCount, 0);
	}

	public BigDecimal getPendingAmount() {
		return billAmount.subtract(getSettledAmount());
	}

	public static BigDecimal pendingSum(List<CreditBill> creditBillList) {
		BigDecimal sum = BigDecimal.ZERO;
		for (CreditBill b : creditBillList) {
			sum = sum.add(b.getPendingAmount());
		}
		return sum;
	}

	@Exclude
	@SearchPredicate(type = PredicateType.IN)
	@FilterMapping(dbEntity = "DbUserRelation", filterField = "userId2In")
	public String salesUserId;

	@Exclude
	@SearchPredicate(type = PredicateType.EQUAL)
	@FilterMapping(dbEntity = "DbUser", filterField = "mobile")
	public String mobile;

}
