package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum CreditBillCycleType {

	FIXED_DAY_OF_WEEK("FDOW", CreditType.REVOLVING), FIXED_DAY_OF_MONTH("FDOM", CreditType.REVOLVING),
	FIXED_NUM_OF_DAYS("FNOD", CreditType.REVOLVING), DYNAMIC("DY", CreditType.REVOLVING),
	NON_REVOLVING("NR", CreditType.NON_REVOLVING), CALENDAR("CL", CreditType.REVOLVING);

	private String code;
	private CreditType creditType;

	CreditBillCycleType(String code, CreditType creditType) {
		this.code = code;
		this.creditType = creditType;
	}

	public static CreditBillCycleType getEnumFromCode(String code) {
		return getCreditBillCycleType(code);
	}

	public static CreditBillCycleType getCreditBillCycleType(String code) {
		for (CreditBillCycleType status : CreditBillCycleType.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}
}
