package com.tgs.services.pms.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class CreditBillEmailAttributes extends EmailAttributes {

	private String billNumber;

	private String outStandingAmount;

	private String billAmount;

	private String settledAmount;

	private String cycleStartDate;

	private String cycleEndDate;

	private String dueDate;

	private String lockDate;

	private String timeLeft;

	private short extendDays;

	private short extendHours;
}
