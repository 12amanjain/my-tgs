package com.tgs.services.pms.datamodel;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.FilterMapping;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.Relation;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;
import java.time.LocalDateTime;

@Getter
@Setter
public class CreditBillRelation implements Relation {

}
