package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.JwtExclude;
import com.tgs.services.base.helper.UserId;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CreditLine extends DataModel {

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	@NotEmpty
	@UserId
	private String userId;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String creditNumber;

	@SearchPredicate(type = PredicateType.IN)
	@NotNull
	private String policyId;

	@SearchPredicate(type = PredicateType.LTE)
	@SearchPredicate(type = PredicateType.GTE)
	@NotNull
	private BigDecimal creditLimit;

	@SearchPredicate(type = PredicateType.LTE)
	@SearchPredicate(type = PredicateType.GTE)
	@ForbidInAPIRequest
	private BigDecimal outstandingBalance;

	private BigDecimal balance;

	private BigDecimal extendedLimit;

	@JwtExclude
	private List<Product> products;

	@JwtExclude
	private CreditBillCycleType billCycleType;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	@ForbidInAPIRequest
	@JwtExclude
	private LocalDateTime billCycleStart;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	@ForbidInAPIRequest
	@JwtExclude
	private LocalDateTime billCycleEnd;

	@SearchPredicate(type = PredicateType.EQUAL)
	@SearchPredicate(type = PredicateType.IN)
	private CreditStatus status;

	@SearchPredicate(type = PredicateType.IN)
	@ForbidInAPIRequest
	@JwtExclude
	private String issuedByUserId;

	@JwtExclude
	@NotNull
	@SearchPredicate(type = PredicateType.OBJECT)
	private CreditAdditionalInfo additionalInfo;

	@ForbidInAPIRequest
	@JwtExclude
	private LocalDateTime createdOn;

	@Exclude
	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.IN)
	@JwtExclude
	private Long id;

	public BigDecimal getBalance() {
		return getExtendedLimit().subtract(outstandingBalance);
	}

	public BigDecimal getExtendedLimit() {
		return creditLimit.add(additionalInfo.getCurTemporaryExt());
	}

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}
		if (!(obj instanceof CreditLine)) {
			return false;
		}
		CreditLine otherRequest = (CreditLine) obj;
		return getKey().equals(otherRequest.getKey());
	}

	private String getKey() {
		StringBuilder sb = new StringBuilder();
		sb.append(userId).append(products).append(status);
		return sb.toString();
	}

	public CreditAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null)
			additionalInfo = CreditAdditionalInfo.builder().build();
		return additionalInfo;
	}

	public Credit toCredit() {
		return new Credit()
				.creditType(CreditType.REVOLVING)
				.creditNumber(this.getCreditNumber())
				.userId(this.getUserId())
				.policy(this.getPolicyId())
				.creditLimit(this.getCreditLimit())
				.utilized(this.getOutstandingBalance())
				.periodStart(this.getBillCycleStart())
				.periodEnd(this.getBillCycleEnd())
				.currentTempExt(this.getAdditionalInfo().getCurTemporaryExt())
				.maxTempExt(this.getAdditionalInfo().getMaxTemporaryExt())
				.issuedByUserId(this.getIssuedByUserId())
				.status(this.getStatus())
				.createdOn(this.getCreatedOn())
				.products(this.getProducts());
	}

	public BigDecimal getOutstandingBalance() {
		return outstandingBalance == null ? BigDecimal.ZERO : outstandingBalance;
	}

	public static BigDecimal outstandingSum(List<CreditLine> creditLines) {
		BigDecimal sum = BigDecimal.ZERO;
		for (CreditLine c : creditLines) {
			sum = sum.add(c.getOutstandingBalance());
		}
		return sum;
	}

}