package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.util.List;

import com.tgs.services.base.enums.PaymentMedium;

import lombok.Getter;

@Getter
public class CreditLinePaymentMode extends PaymentMode {

	private List<CreditLine> creditLines;

	public CreditLinePaymentMode(PaymentMedium mode, BigDecimal paymentFee, List<CreditLine> creditLines, Double priority,
								 Double balance) {
		super(mode, paymentFee, priority, balance);
		this.creditLines = creditLines;
	}
}
