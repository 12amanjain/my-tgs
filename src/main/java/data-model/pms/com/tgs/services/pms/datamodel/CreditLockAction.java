package com.tgs.services.pms.datamodel;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

@Getter
public enum CreditLockAction {

    LOCK_CREDITLINE("LCL"),
    LOCK_ACCOUNT("LA");

    private String code;

    CreditLockAction(String code) {
        this.code = code;
    }

    public static CreditLockAction getEnumFromCode(String code) {
        return getCreditLockAction(code);
    }

    public static CreditLockAction getCreditLockAction(String code) {
        for (CreditLockAction action : CreditLockAction.values()) {
            if (action.getCode().equals(code)) {
                return action;
            }
        }
        return null;
    }

    public static List<String> getCodes(List<CreditLockAction> creditLockActionList) {
        List<String> codes = new ArrayList<>();
        creditLockActionList.forEach(depReqStatus -> codes.add(depReqStatus.getCode()));
        return codes;
    }
}
