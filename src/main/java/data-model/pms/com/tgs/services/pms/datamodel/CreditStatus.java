package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum CreditStatus {

    ACTIVE("A"),
    BLOCKED("B"),
    EXPIRED("E");

    private String code;

    CreditStatus (String code) {
        this.code = code;
    }

    public static CreditStatus getEnumFromCode(String code) {
        return getCreditStatus(code);
    }

    private static CreditStatus getCreditStatus(String code) {
        for (CreditStatus status : CreditStatus.values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }
}
