package com.tgs.services.pms.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class CreditlineEmailAttributes extends EmailAttributes {

	private String creditNumber;

	private String creditLimit;

	private String outStandingAmount;

	private String unUsedCreditAmount;

	private String newExtensionLimit;

	private String oldLimit;

	private String cycleStartDate;

	private String cycleEndDate;

	private String dueDate;

	private short passedDueDays;

	private String paymentDueDate;
}
