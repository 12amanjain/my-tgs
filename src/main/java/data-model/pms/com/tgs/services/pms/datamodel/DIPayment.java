package com.tgs.services.pms.datamodel;

import com.tgs.services.base.helper.UserId;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DIPayment extends Payment {

    @UserId
    private String userId;

    private String diRcNumber;

    private String diDhNumber;

    public DIPayment(){}

    public DIPayment(Payment payment){

    }

}
