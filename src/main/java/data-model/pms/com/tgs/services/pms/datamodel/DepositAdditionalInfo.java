package com.tgs.services.pms.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DepositAdditionalInfo {

	@SerializedName("durls")
	private List<String> depositFilesUrl;

	@SerializedName("auid")
	@UserId
	@SearchPredicate(type = PredicateType.IN, dbAttribute = "auid")
	@SearchPredicate(type = PredicateType.IN, destinationEntity = "DepositRequest", dbAttribute = "additionalInfo.auid")
	private String assignedUserId;

	@SerializedName("at")
	private LocalDateTime assignedTime;

	@SerializedName("cm")
	private String comments;

	@SerializedName("pa")
	private Double processedAmount;

	private Double gst;

	@SerializedName("rcno")
	@SearchPredicate(type = PredicateType.IN, destinationEntity = "DepositRequest", dbAttribute = "additionalInfo.rcno")
	private String rcNo;

	@SerializedName("dhno")
	private String dhNo;

	@SerializedName("abuid")
	private String approvedByUserId;

	@SerializedName("pet")
	private LocalDateTime paymentExpectedTime;

	@SerializedName("prd")
	private LocalDate paymentReceivedDate;
}
