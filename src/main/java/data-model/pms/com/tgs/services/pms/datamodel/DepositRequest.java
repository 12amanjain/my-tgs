package com.tgs.services.pms.datamodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Set;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DepositRequest implements Validatable {

	@ForbidInAPIRequest
	private Long id;
	@SearchPredicate(type = PredicateType.IN)
	private String reqId;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;
	private String mobile;
	@UserId(userRole = true, userBalance = false)
	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;
	@UserId
	@SearchPredicate(type = PredicateType.IN)
	private String partnerId;
	private String updateUserId;

	@SearchPredicate(type = PredicateType.IN)
	private DepositRequestStatus status;
	@SearchPredicate(type = PredicateType.IN)
	private DepositType type;

	@SearchPredicate(type = PredicateType.EQUAL)
	private Double requestedAmount;

	private Double paymentFee;
	@SearchPredicate(type = PredicateType.IN)
	private String transactionId;
	private String depositBranch;
	private String depositBank;
	@SearchPredicate(type = PredicateType.IN)
	private String bank;
	private String accountNumber;
	@SearchPredicate(type = PredicateType.OBJECT)
	private DepositAdditionalInfo additionalInfo;

	private String chequeDrawOnBank;
	private LocalDate chequeIssueDate;
	private String chequeNumber; // can also be used as DDNumber

	private String comments;

	private Set<DepositAction> actionList;

	public DepositAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = DepositAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (reqId != null || (requestedAmount == null && StringUtils.isBlank(accountNumber) && StringUtils.isBlank(bank)
				&& type == null)) {
			return errors;
		}

		if (requestedAmount == null) {
			errors.put("requestedAmount", SystemError.INVALID_AMOUNT.getErrorDetail());
		}
		if (type == null) {
			errors.put("type", SystemError.INVALID_DEPOSIT_TYPE.getErrorDetail());
		}
		if (type != DepositType.CASH_DEPOSITED && type != DepositType.CREDIT_CARD && type != DepositType.CREDIT_MEMO
				&& type != DepositType.PAYMENT_GATEWAY) {
			if (accountNumber == null) {
				errors.put("accountNumber", SystemError.INVALID_ACCOUNT_NUMBER.getErrorDetail());
			}
			if (bank == null) {
				errors.put("bank", SystemError.INVALID_BANK.getErrorDetail());
			}
		}

		return errors;
	}

}
