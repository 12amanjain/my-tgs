package com.tgs.services.pms.datamodel;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Getter
public enum DepositRequestStatus {
	SUBMITTED("S"), ABORTED("AB"), PROCESSING("P"), PAYMENT_NOT_RECEIVED("PNR"), PAYMENT_RECEIVED("PR"), ACCEPTED("A"), REJECTED("R");

	private String code;

	private DepositRequestStatus(String code) {
		this.code = code;
	}

	public static DepositRequestStatus getEnumFromCode(String code) {
		return getDepositStatus(code);
	}

	private static DepositRequestStatus getDepositStatus(String code) {
		for (DepositRequestStatus status : DepositRequestStatus.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<DepositRequestStatus> depReqStatuses) {
		List<String> codes = new ArrayList<>();
		depReqStatuses.forEach(depReqStatus -> {
			codes.add(depReqStatus.getCode());
		});
		return codes;
	}

}
