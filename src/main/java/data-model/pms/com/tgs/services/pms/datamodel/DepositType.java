package com.tgs.services.pms.datamodel;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;

@Getter
public enum DepositType {
	CASH_IN_BANK("B"),
	CASH_DEPOSITED("C"),
	CHEQUE_DEPOSIT("D"),
	ONLINE_TRANS("O"),
	ONLINE_TOPUP("P"),
	GST_CLAIM("G"),
	CREDIT_CARD("R"),
	CREDIT_MEMO("CM"),
	DEPOSIT_INCENTIVE("DI"),
	PAYMENT_GATEWAY("PG");

	private String code;

	private DepositType(String code) {
		this.code = code;
	}

	public static DepositType getEnumFromCode(String code) {
		return getDepositType(code);
	}

	public static DepositType getDepositType(String code) {
		for (DepositType status : DepositType.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		return null;
	}

	public static List<String> getCodes(List<DepositType> depTypes) {
		List<String> codes = new ArrayList<>();
		depTypes.forEach(depType -> {
			codes.add(depType.getCode());
		});
		return codes;
	}
}
