package com.tgs.services.pms.datamodel;


import java.util.StringJoiner;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EasyPayMerchantInfo extends GatewayMerchantInfo {

	private String merchantid;
	private String submerchantid;
	private String returnurl;
	private String userId;

	private String trackingUrl;
	private String headerPrivateKey;

	// Key or Signature { AES Key}
	private String signature;
	private String Reference_No;
	private String pgamount;
	private String mobileno;
	private String email;
	private String mandatory_fields;
	private String optional_fields;
	private String paymode;
	private String transaction_amount;


	public String getMandatoryFields() {
		mandatory_fields = new String();
		StringJoiner joiner = new StringJoiner("|");
		joiner.add(Reference_No);
		joiner.add(submerchantid);
		joiner.add(pgamount);
		joiner.add(mobileno);
		joiner.add(email);
		mandatory_fields = joiner.toString();
		return mandatory_fields;
	}


}
