package com.tgs.services.pms.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ExternalPayment extends DataModel {

	@SearchPredicate(type = PredicateType.EQUAL)
	private Long id;

	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private String refId;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	@UserId(userRole = true)
	private String payUserId;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private PaymentStatus status;


	@SearchPredicate(type = PredicateType.OBJECT)
	private ExternalPaymentAdditionalInfo additionalInfo;

	private List<ExternalPaymentStatusInfo> timeline;

	public ExternalPaymentAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = ExternalPaymentAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

}
