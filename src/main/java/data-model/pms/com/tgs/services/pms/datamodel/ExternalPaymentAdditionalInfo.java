package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class ExternalPaymentAdditionalInfo {

	private BigDecimal amount;

	private String gatewayType;

	private Integer ruleId;

	private String merchantTxnId;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	@SerializedName("gs")
	private PaymentStatus gatewayStatus;

}
