package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum FundTransferOperation {

    FUND_ALLOCATE("FA", PaymentOpType.CREDIT),
    FUND_RECALL("FR", PaymentOpType.DEBIT),
    PAYMENT("P", PaymentOpType.CREDIT);

    public String getType() {
        return this.getCode();
    }

    private String code;
    private PaymentOpType paymentOpType;

    FundTransferOperation(String code, PaymentOpType paymentOpType) {
        this.code = code;
        this.paymentOpType = paymentOpType;
    }

    public static FundTransferOperation getEnumFromCode(String code) {
        return getOpType(code);
    }

    public static FundTransferOperation getOpType(String code) {
        for (FundTransferOperation pOType : FundTransferOperation.values()) {
            if (pOType.getCode().equals(code)) {
                return pOType;
            }
        }
        return null;
    }

}
