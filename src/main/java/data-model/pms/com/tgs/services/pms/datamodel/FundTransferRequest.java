package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.util.List;

import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.NotNull;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class FundTransferRequest {

	@NotNull
	private BigDecimal amount;

	private List<Product> products;

	@NotNull
	private String payUserId;

	private PaymentMedium targetPaymentMedium;

	@NotNull
	private FundTransferOperation transferOperation;

	private boolean allowDisabledModes;

	private PaymentAdditionalInfo additionalInfo;

	private String walletNumber;

	private String refId;

	private BigDecimal totalAmountSum;

	private String reason;

	public PaymentAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) additionalInfo = PaymentAdditionalInfo.builder().build();
		return additionalInfo;
	}

}
