package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GatewayMerchantInfo {

	private String gatewayURL;
	private String requestType;

	// @Exclude
	private String refundURL;
	private String gatewayType;

}
