package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IncentiveSlab {

	private Long minAmount;
	private Long maxAmount;
	private double incentive;
}
