package com.tgs.services.pms.datamodel;

import com.tgs.services.base.enums.PaymentMedium;

import lombok.Getter;

import java.math.BigDecimal;

@Getter
public class MediumTransferRequest {

	private String userId;

	private PaymentMedium source;

	private String sourceWalletNumber;

	private PaymentMedium target;

	private String targetWalletNumber;

	private BigDecimal amount;

	private String comments;
}
