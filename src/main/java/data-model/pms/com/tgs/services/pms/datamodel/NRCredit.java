package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.ObjectUtils;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.JwtExclude;
import com.tgs.services.base.helper.UserId;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NRCredit {

	@NotNull
	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	@UserId
	private String userId;
	
	@SearchPredicate(type = PredicateType.IN)
	@UserId
	private String partnerId;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private String creditId;

	@NotNull
	private BigDecimal creditAmount;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.EQUAL)
	private BigDecimal utilized;

	// is this used by UI?
	private BigDecimal balance;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	@NotNull
	private LocalDateTime creditExpiry;

	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private CreditStatus status;

	@JwtExclude
	@NotNull
	private List<Product> products;

	@JwtExclude
	@SearchPredicate(type = PredicateType.OBJECT)
	private NRCreditAdditionalInfo additionalInfo;

	@SearchPredicate(type = PredicateType.IN)
	@UserId
	@JwtExclude
	private String issuedByUserId;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	private Integer expiryExtCount;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	@JwtExclude
	private LocalDateTime createdOn;

	@JwtExclude
	private LocalDateTime processedOn;

	private Duration dueSince;

	@JwtExclude
	private Long id;

	public NRCreditAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) additionalInfo = NRCreditAdditionalInfo.builder().build();
		return additionalInfo;
	}

	public BigDecimal getBalance() {
		return creditAmount.subtract(utilized);
	}

	public BigDecimal getUtilized() {
		return utilized == null ? BigDecimal.ZERO : utilized;
	}

	public void incrementExtCount() {
		expiryExtCount = getExpiryExtCount() + 1;
	}

	public BigDecimal creditExtensionAmount() {
		return creditAmount.subtract(getAdditionalInfo().getOriginalCreditLimit());
	}

	private Integer getExpiryExtCount() {
		return ObjectUtils.defaultIfNull(expiryExtCount, 0);
	}

	private static Set<PaymentTransactionType> replenishLimitWithOA = new HashSet<>();

	static {
		replenishLimitWithOA.add(PaymentTransactionType.REFUND);
		replenishLimitWithOA.add(PaymentTransactionType.COMMISSION);
	}

	public Credit toCredit() {
		return new Credit().creditType(CreditType.NON_REVOLVING).creditNumber(this.getCreditId())
				.userId(this.getUserId()).policy(this.getAdditionalInfo().getPolicy())
				.creditLimit(this.getCreditAmount()).utilized(this.getUtilized()).periodStart(this.getCreatedOn())
				.periodEnd(this.getCreditExpiry()).issuedByUserId(this.getIssuedByUserId()).status(this.getStatus())
				.products(this.getProducts()).createdOn(this.getCreatedOn());
	}

	public static BigDecimal utilisedSum(List<NRCredit> nrCreditList) {
		BigDecimal sum = BigDecimal.ZERO;
		for (NRCredit c : nrCreditList) {
			sum = sum.add(c.getUtilized());
		}
		return sum;
	}
}
