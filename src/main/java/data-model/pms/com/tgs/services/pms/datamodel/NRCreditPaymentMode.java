package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.util.List;

import com.tgs.services.base.enums.PaymentMedium;

import lombok.Getter;

@Getter
public class NRCreditPaymentMode extends PaymentMode {

	private List<NRCredit> nrCreditList;

	public NRCreditPaymentMode(PaymentMedium mode, BigDecimal paymentFee, List<NRCredit> nrCredits, Double priority,
							   Double balance) {
		super(mode, paymentFee, priority, balance);
		this.nrCreditList = nrCredits;
	}
}
