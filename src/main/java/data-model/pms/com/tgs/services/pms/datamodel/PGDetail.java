package com.tgs.services.pms.datamodel;

import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class PGDetail {

	private Map<String, String> paymentFields;
}
