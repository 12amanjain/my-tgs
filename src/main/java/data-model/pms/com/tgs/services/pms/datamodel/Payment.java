package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.UserId;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Payment extends DataModel {

	@NotNull
	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	@UserId
	private String payUserId;

	@UserId
	@NotNull
	@SearchPredicate(type = PredicateType.IN)
	private String partnerId;

	@NotNull
	private BigDecimal amount;

	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.IN)
	private String paymentRefId;

	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.EQUAL)
	private PaymentStatus status;

	@SearchPredicate(type = PredicateType.EQUAL)
	@SearchPredicate(type = PredicateType.IN)
	private PaymentMedium paymentMedium;

	@SearchPredicate(type = PredicateType.IN, dbAttribute = "refId")
	@SearchPredicate(type = PredicateType.EQUAL, dbAttribute = "refId")
	@SerializedName(value = "bookingId", alternate = {"refId"})
	private String refId;

	private AmendmentType amendmentType;

	@SearchPredicate(type = PredicateType.EQUAL)
	@SearchPredicate(type = PredicateType.IN)
	private PaymentTransactionType type;

	@SearchPredicate(type = PredicateType.IN)
	private String amendmentId;

	@ForbidInAPIRequest
	private Product product;

	/**
	 * {@link PaymentOpType}
	 */
	@ForbidInAPIRequest
	private PaymentOpType opType;

	@SearchPredicate(type = PredicateType.OBJECT)
	private PaymentAdditionalInfo additionalInfo;

	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.IN)
	@UserId
	private String loggedInUserId;

	@SearchPredicate(type = PredicateType.GT)
	@SearchPredicate(type = PredicateType.LT)
	@ForbidInAPIRequest
	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	private BigDecimal paymentFee;

	private BigDecimal markup;

	private BigDecimal tds;

	private String reason;

	private Boolean considerNegativeBalance;

	@Exclude
	@ForbidInAPIRequest
	private String walletNumber;

	private Long walletId;

	private String currency;

	@ForbidInAPIRequest
	private BigDecimal currentBalance;
	@SearchPredicate(type = PredicateType.EQUAL)
	private String merchantTxnId;

	@Exclude
	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.EQUAL, skipCriteriaSearch = true)
	private Boolean merge;

	@Exclude
	@ForbidInAPIRequest
	@SearchPredicate(type = PredicateType.IN)
	private Long id;

	public BigDecimal getMaximumRefundableAmount() {
		return getAmount().subtract(getAdditionalInfo().getMerchantPaymentFee());
	}

	private transient PGDetail pgDetail;

	public PaymentAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = PaymentAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	public void setSubType(PaymentTransactionType subType) {
		if (!type.equals(subType)) {
			getAdditionalInfo().setActualTransactionType(subType);
		}
	}

	public static BigDecimal sum(List<Payment> paymentList) {
		BigDecimal sum = BigDecimal.ZERO;
		for (Payment p : paymentList) {
			sum = sum.add(p.getAmount());
		}
		return sum;
	}

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.ZERO : amount;
	}

	public BigDecimal getPaymentFee() {
		return paymentFee == null ? BigDecimal.ZERO : paymentFee;
	}

	public void setProduct() {
		if (product == null)
			product = Product.NA;
	}

	public void setRefId() {
		if (StringUtils.isEmpty(refId) && Product.NA.equals(product)) {
			refId = Product.NA.getPrefix() + RandomStringUtils.random(8, false, true);
		}
	}

	/*
	 * public Payment(@NotNull String payUserId, String partnerId, BigDecimal amount, String paymentRefId, PaymentStatus
	 * status, PaymentMedium paymentMedium, String refId, AmendmentType amendmentType, PaymentTransactionType type,
	 * String amendmentId, Product product, PaymentOpType opType, PaymentAdditionalInfo additionalInfo, String
	 * loggedInUserId, LocalDateTime createdOn, LocalDateTime processedOn, BigDecimal paymentFee, BigDecimal markup,
	 * BigDecimal tds, String reason, String walletNumber, Long walletId, String currency, BigDecimal currentBalance,
	 * String merchantTxnId, Boolean merge, Boolean considerNegativeBalance, Long id, PGDetail pgDetail) {
	 * this.payUserId = payUserId; this.partnerId = partnerId; this.amount = amount; this.paymentRefId = paymentRefId;
	 * this.status = status; this.paymentMedium = paymentMedium; this.refId = refId; this.amendmentType = amendmentType;
	 * this.type = type; this.amendmentId = amendmentId; this.product = product; this.opType = opType;
	 * this.additionalInfo = additionalInfo; this.loggedInUserId = loggedInUserId; this.createdOn = createdOn;
	 * this.processedOn = processedOn; this.paymentFee = paymentFee; this.markup = markup; this.tds = tds; this.reason =
	 * reason; this.walletNumber = walletNumber; this.walletId = walletId; this.currency = currency; this.currentBalance
	 * = currentBalance; this.merchantTxnId = merchantTxnId; this.id = id; this.pgDetail = pgDetail; this.merge = merge;
	 * this.considerNegativeBalance = considerNegativeBalance; }
	 */

	// public Payment() {}

	public boolean includeInBill() {
		return this.getAdditionalInfo().getActualTransactionType() != null
				? this.getAdditionalInfo().getActualTransactionType().includeInBill()
				: this.getType().includeInBill();
	}
}
