package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.helper.UserId;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class PaymentAdditionalInfo {

	@SerializedName("oPRId")
	private String originalPaymentRefId;

	@SerializedName("dRId")
	private String depositRequestId;

	@SerializedName("cm")
	private String comments;

	// in percentage
	@SerializedName("di")
	private BigDecimal depositIncentive;

	private BigDecimal exchangedAmount;

	private String exchangedCurrency;

	@SerializedName("gw")
	private String gateway;

	private Integer ruleId;

	@SerializedName("uBal")
	private BigDecimal utilizedBalance;

	@SerializedName("cbal")
	private BigDecimal closingBalance;

	@SerializedName("tcbal")
	private BigDecimal totalClosingBalance;

	@SerializedName("nop")
	private Double noOfPoints;

	@SerializedName("pt")
	private PointsType pointsType;

	@SerializedName("pe")
	@SearchPredicate(type = PredicateType.GTE, destinationEntity = "Payment", dbAttribute = "additionalInfo.pe")
	@SearchPredicate(type = PredicateType.LTE, destinationEntity = "Payment", dbAttribute = "additionalInfo.pe")
	private LocalDateTime pointsExpiry;

	private LocalDate paymentDate;

	@SerializedName("tda")
	private BigDecimal totalDepositAmount;

	@SerializedName("mpf")
	private BigDecimal merchantPaymentFee;

	@SerializedName("bn")
	private String billNumber;

	@SerializedName("apid")
	private String approverId;

	@UserId
	@SearchPredicate(type = PredicateType.IN)
	private String agentId;

	@SearchPredicate(type = PredicateType.IN, dbAttribute = "att")
	@SearchPredicate(type = PredicateType.IN, destinationEntity = "Payment", dbAttribute = "additionalInfo.att")
	@SerializedName(value = "att", alternate = "subType")
	private PaymentTransactionType actualTransactionType;

	@SerializedName("extPayId")
	private String externalPayId;

	@SerializedName("eSign")
	private String externalSignature;

	private boolean allowForDisabledMediums;

	private List<String> diRefId;

	private List<String> paymentRefId;

	@SerializedName("ic")
	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "Payment", dbAttribute = "additionalInfo.ic")
	private Boolean isChargedBySupplier;

	@SerializedName("ccid")
	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "Payment", dbAttribute = "additionalInfo.ccid")
	private Long creditCardId;

	private String voucherCode;


	public List<String> getDiRefId() {
		return diRefId == null ? new ArrayList<>() : diRefId;
	}

	public List<String> getPaymentRefId() {
		return paymentRefId == null ? new ArrayList<>() : paymentRefId;
	}

	public BigDecimal getMerchantPaymentFee() {
		return merchantPaymentFee == null ? BigDecimal.ZERO : merchantPaymentFee;
	}

	public BigDecimal getDepositIncentive() {
		return depositIncentive == null ? BigDecimal.ZERO : depositIncentive;
	}

}
