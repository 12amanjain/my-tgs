package com.tgs.services.pms.datamodel;

import java.time.LocalDateTime;
import java.util.Optional;
import javax.validation.constraints.NotNull;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleCriteria;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleExclusionCriteria;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleInclusionCriteria;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentConfigurationRule implements IRule {

	private Integer id;

	private LocalDateTime createdOn;

	private LocalDateTime processedOn;

	private PaymentMedium medium;

	private Boolean enabled;

	private PaymentRuleType ruleType;

	private PaymentBasicRuleInclusionCriteria inclusionCriteria;

	private PaymentBasicRuleExclusionCriteria exclusionCriteria;

	@NotNull
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	private Double priority;

	private String description;

	@Exclude
	private boolean isDeleted;

	@Override
	public PaymentBasicRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public PaymentBasicRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public boolean getEnabled() {
		return enabled;
	}

	@Override
	public double getPriority() {
		return priority;
	}

	@Override
	public IRuleOutPut getOutput() {
		GsonBuilder gsonBuilder = GsonUtils.getGsonBuilder();
		GsonUtils.registerAdapter(output, ruleType.getOutPutTypeToken().getRawType(), gsonBuilder, null,
				Optional.empty());
		return gsonBuilder.create().fromJson(output, ruleType.getOutPutTypeToken().getType());
	}
}
