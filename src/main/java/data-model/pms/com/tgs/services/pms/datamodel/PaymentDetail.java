package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentDetail extends Payment {

	private String leadPax;

	private String summary;

	private String invoiceId;

	public PaymentDetail() { }

	public void setPaymentDetail(Payment payment) {
		this.setAdditionalInfo(payment.getAdditionalInfo());
		this.setAmendmentId(payment.getAmendmentId());
		this.setAmendmentType(payment.getAmendmentType());
		this.setAmount(payment.getAmount());
		this.setCreatedOn(payment.getCreatedOn());
		this.setCurrency(payment.getCurrency());
		this.setCurrentBalance(payment.getCurrentBalance());
		this.setId(payment.getId());
		this.setLoggedInUserId(payment.getLoggedInUserId());
		this.setMarkup(payment.getMarkup());
		this.setMerchantTxnId(payment.getMerchantTxnId());
		this.setOpType(payment.getOpType());
		this.setPaymentFee(payment.getPaymentFee());
		this.setPaymentMedium(payment.getPaymentMedium());
		this.setPaymentRefId(payment.getPaymentRefId());
		this.setPayUserId(payment.getPayUserId());
		this.setPgDetail(payment.getPgDetail());
		this.setProcessedOn(payment.getProcessedOn());
		this.setProduct(payment.getProduct());
		this.setReason(payment.getReason());
		this.setRefId(payment.getRefId());
		this.setPartnerId(payment.getPartnerId());
		this.setStatus(payment.getStatus());
		this.setTds(payment.getTds());
		this.setType(payment.getType());
		this.setWalletId(payment.getWalletId());
		this.setWalletNumber(payment.getWalletNumber());
	}

}
