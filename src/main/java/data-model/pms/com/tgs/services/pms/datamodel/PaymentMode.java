package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.util.List;
import com.tgs.services.base.enums.PaymentMedium;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentMode {

	private PaymentMedium mode;

	private BigDecimal paymentFee;

	private BigDecimal partialpaymentFee;

	private Double priority;

	private Integer ruleId;

	private Double balance;
	
	private String displayName;
	
	private List<String> subMediums;

	public PaymentMode(PaymentMedium mode, BigDecimal paymentFee, Double priority, Double balance) {
		this.mode = mode;
		this.paymentFee = paymentFee;
		this.priority = priority;
		this.balance = balance;
	}
}