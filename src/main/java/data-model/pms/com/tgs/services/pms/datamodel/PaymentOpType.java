package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum PaymentOpType {

	CREDIT("C"), DEBIT("D");

	public String getType() {
		return this.getCode();
	}

	private String code;

	PaymentOpType(String code) {
		this.code = code;
	}

	public static PaymentOpType getEnumFromCode(String code) {
		return getOpType(code);
	}

	public static PaymentOpType getOpType(String code) {
		for (PaymentOpType pOType : PaymentOpType.values()) {
			if (pOType.getCode().equals(code)) {
				return pOType;
			}
		}
		return null;
	}

	public static PaymentOpType inverse(PaymentOpType opType) {
		return opType.equals(PaymentOpType.CREDIT) ? PaymentOpType.DEBIT : PaymentOpType.CREDIT;
	}

}
