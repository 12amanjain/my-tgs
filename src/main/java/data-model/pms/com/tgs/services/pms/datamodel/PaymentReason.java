package com.tgs.services.pms.datamodel;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import org.apache.commons.lang3.StringUtils;
import lombok.NoArgsConstructor;

public enum PaymentReason {

	PAID_USING("Paid using"),
	FAILURE_REASON("Failure reason"),
	BANK("Bank"),
	MERCHANT_TXN_ID("Merchant Txn Id"),
	PAYMENT_FEE("Payment Fee"),
	PROCESSED("Processed");

	final static public String REASON_SEPARATOR = "; ", KEY_VALUE_SEPARATOR = ": ";

	private String key;

	PaymentReason(String key) {
		this.key = key;
	}

	public String getReason(Object arg) {
		String val = (arg == null) ? null : arg.toString();
		StringBuilder sb = new StringBuilder(key).append(KEY_VALUE_SEPARATOR).append(val);
		return sb.toString();
	}

	public static String getValue(String reason) {
		if (StringUtils.isBlank(reason)) {
			return null;
		}
		return StringUtils.substringAfter(reason, KEY_VALUE_SEPARATOR);
	}

	public static PaymentReason getBy(String reason) {
		if (StringUtils.isBlank(reason)) {
			return null;
		}

		for (PaymentReason pr : values()) {
			if (reason.startsWith(pr.key)) {
				return pr;
			}
		}

		return null;
	}

	@NoArgsConstructor
	public static class PaymentReasonContainer {
		private Map<PaymentReason, Object> data = emptyReasonsData();

		private static <T> Map<PaymentReason, T> emptyReasonsData() {
			return new LinkedHashMap<>();
		}

		public PaymentReasonContainer(PaymentReasonContainer reasonContainer) {
			this.data.putAll(reasonContainer.data);
		}

		public void addReason(PaymentReason reason, Object arg) {
			if (reason != null) {
				data.put(reason, arg);
			}
		}

		public void removeReason(PaymentReason reason) {
			if (reason != null) {
				data.remove(reason);
			}
		}

		public String getValue(PaymentReason reason) {
			Object val = data.get(reason);
			String str = Objects.toString(val, null);
			return str;
		}

		public String appendReasonsTo(String reasons) {
			String newReasons = getCombinedReasons();
			if (StringUtils.isBlank(reasons)) {
				return newReasons;
			}
			return StringUtils.joinWith(REASON_SEPARATOR, reasons, newReasons);
		}

		public String getCombinedReasons() {
			StringJoiner sj = new StringJoiner(REASON_SEPARATOR);
			for (Map.Entry<PaymentReason, Object> entry : data.entrySet()) {
				PaymentReason pr = entry.getKey();
				sj.add(pr.getReason(entry.getValue()));
			}
			return sj.toString();
		}

		public static PaymentReasonContainer getContainer(String combinedReasons) {
			PaymentReasonContainer container = new PaymentReasonContainer();
			container.data.putAll(getValues(combinedReasons));
			return container;
		}

		public static Map<PaymentReason, String> getValues(String combinedReasons) {
			Map<PaymentReason, String> data = emptyReasonsData();
			if (StringUtils.isNotBlank(combinedReasons)) {
				String[] reasons = combinedReasons.split(REASON_SEPARATOR);
				for (String reason : reasons) {
					PaymentReason pr = PaymentReason.getBy(reason);
					data.put(pr, PaymentReason.getValue(reason));

				}
			}
			return data;
		}

		@Override
		public String toString() {
			return getCombinedReasons();
		}
	}

}
