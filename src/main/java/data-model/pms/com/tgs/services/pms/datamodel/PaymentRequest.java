package com.tgs.services.pms.datamodel;

import java.io.Serializable;
import java.math.BigDecimal;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@Accessors(chain = true)
@GsonRunTimeAdaptorRequired(dependOn = "paymentMedium")
@GsonPolymorphismMapping({
		@ClassType(keys = {"W", "CL", "WALLET", "CREDIT_LINE", "CREDIT", "NRC"}, value = WalletPaymentRequest.class)})
@SuperBuilder
@ToString
public class PaymentRequest implements Serializable {

	@ApiModelProperty(required = true, notes = "Amount value to be allocated/retracted")
	private BigDecimal amount;

	private BigDecimal markup;

	@ApiModelProperty(required = true, notes = "Id of the user to allocate amount to.")
	private String payUserId;

	private String partnerId;

	private String loggedInUserId;

	@ApiModelProperty(notes = "TransactionId received from the merchant or bank")
	private String merchantTxnId;

	private PaymentMedium paymentMedium;

	private String originalPaymentRefId;
	@SerializedName(value = "bookingId", alternate = {"refId"})
	private String refId;

	private String amendmentId;

	private PaymentOpType opType;

	@Builder.Default
	private Product product = Product.NA;

	private PaymentTransactionType transactionType;

	@SerializedName("aI")
	PaymentAdditionalInfo additionalInfo;

	/**
	 * Used for updating deposit request in fund handler.
	 */
	@SerializedName("dai")
	DepositAdditionalInfo depositAdditionalInfo;

	private Boolean process;

	private BigDecimal tds;

	private Integer ruleId;

	private String subMedium;

	private BigDecimal paymentFee;

	private PGDetail pgDetail;

	private boolean allowZeroAmount;

	private boolean isInternalQuery;

	private Boolean considerNegativeBalance;

	private String reason;

	@SerializedName("nop")
	private Double noOfPoints;

	public PaymentAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = PaymentAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	public DepositAdditionalInfo getDepositAdditionalInfo() {
		if (depositAdditionalInfo == null) {
			depositAdditionalInfo = DepositAdditionalInfo.builder().build();
		}
		return depositAdditionalInfo;
	}

	public PaymentRequest(PaymentRequest request) {
		setMerchantTxnId(request.getMerchantTxnId());
		setAmount(request.getAmount());
		setPayUserId(request.getPayUserId());
		setPaymentMedium(request.getPaymentMedium());
		setOriginalPaymentRefId(request.getOriginalPaymentRefId());
		setRefId(request.getRefId());
		setAmendmentId(request.getAmendmentId());
		setOpType(request.getOpType());
		setProduct(request.getProduct());
		setTransactionType(request.getTransactionType());
		setAdditionalInfo(request.getAdditionalInfo());
		setTds(request.getTds());
		setRuleId(request.getRuleId());
		setReason(request.getReason());
		setPaymentFee(request.getPaymentFee());
		setAllowZeroAmount(request.isAllowZeroAmount());
	}

	public void setSubType(PaymentTransactionType subType) {
		if (!transactionType.equals(subType)) {
			getAdditionalInfo().setActualTransactionType(subType);
		}
	}

	public BigDecimal getPaymentFee() {
		return paymentFee == null ? BigDecimal.ZERO : paymentFee;
	}

	public PaymentRequest setAmount(BigDecimal value) {
		amount = value;
		return this;
	}

	public PaymentRequest setAmount(double value) {
		amount = BigDecimal.valueOf(value);
		return this;
	}

}
