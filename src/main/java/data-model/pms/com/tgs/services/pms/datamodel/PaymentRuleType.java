package com.tgs.services.pms.datamodel;

import java.util.Arrays;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.pms.ruleengine.DepositIncentiveConfig;
import com.tgs.services.pms.ruleengine.DepositRequestConfig;
import com.tgs.services.pms.ruleengine.NRCreditExpiryOutput;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleCriteria;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.services.pms.ruleengine.RefundOutput;
import lombok.Getter;

@Getter
@SuppressWarnings("serial")
public enum PaymentRuleType {

	PAYMENT_MEDIUM(PaymentBasicRuleCriteria.class, new TypeToken<PaymentConfigOutput>() {
	}), DEPOSIT_INCENTIVE(PaymentBasicRuleCriteria.class, new TypeToken<DepositIncentiveConfig>() {
	}), REFUND(PaymentBasicRuleCriteria.class, new TypeToken<RefundOutput>() {
	}), NR_CREDIT_EXPIRY(PaymentBasicRuleCriteria.class, new TypeToken<NRCreditExpiryOutput>() {
	}), PARTIALPAYMENTMEDIUMS(PaymentBasicRuleCriteria.class, new TypeToken<PaymentConfigOutput>() {
	}), DEPOSIT_REQUEST(PaymentBasicRuleCriteria.class, new TypeToken<DepositRequestConfig>() {
	});

	private Class<? extends IRuleCriteria> inputType;
	private TypeToken<? extends IRuleOutPut> outPutTypeToken;

	<T extends IRuleCriteria, RT extends IRuleOutPut> PaymentRuleType(Class<T> inputType,
			TypeToken<RT> outPutTypeToken) {
		this.inputType = inputType;
		this.outPutTypeToken = outPutTypeToken;
	}

	public List<PaymentRuleType> getConfigRuleType() {
		return Arrays.asList(PaymentRuleType.values());
	}

	public static PaymentRuleType getRuleType(String name) {
		for (PaymentRuleType ruleType : PaymentRuleType.values()) {
			if (ruleType.getName().equals(name)) {
				return ruleType;
			}
		}
		return null;
	}

	public String getName() {
		return this.name();
	}
}
