package com.tgs.services.pms.datamodel;

import lombok.Getter;

@Getter
public enum PaymentTransactionType {

	PAID_FOR_ORDER("O", PaymentOpType.DEBIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	PAID_FOR_AMENDMENT("A", PaymentOpType.DEBIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	TOPUP("T", PaymentOpType.CREDIT),

	POINTS_RECALL("D", PaymentOpType.DEBIT),

	COMMISSION("C", PaymentOpType.CREDIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	PARTNER_MARKUP("PM", PaymentOpType.CREDIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	REFUND("RF", PaymentOpType.CREDIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	BILL_SETTLE("B", PaymentOpType.CREDIT),

	DEPOSIT_INCENTIVE("DI", PaymentOpType.CREDIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	REVERSE("RV", null) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	UTILISATION_ADJUSTMENT("OA", PaymentOpType.CREDIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},

	TEMPORARY_LIMIT_CHANGE("TLC", null) {
		@Override
		public boolean forLimitChange() {
			return true;
		}
	},
	CREDIT_ISSUED("CR_ISS", PaymentOpType.CREDIT) {
		@Override
		public boolean forLimitChange() {
			return true;
		}
	},
	CREDIT_EXPIRED("CR_EXP", PaymentOpType.DEBIT) {
		@Override
		public boolean forLimitChange() {
			return true;
		}
	},
	CREDIT_RECALLED("CR_RED", PaymentOpType.DEBIT) {
		@Override
		public boolean forLimitChange() {
			return true;
		}
	},
	FUND_TRANSFER("FT", null) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	},
	INTERNAL_PAYMENT("IT", null),
	PARTNER_COMMISSION("PC", PaymentOpType.CREDIT) {
		@Override
		public boolean includeInBill() {
			return true;
		}
	};

	private String code;
	private PaymentOpType opType;

	PaymentTransactionType(String code, PaymentOpType opType) {
		this.code = code;
		this.opType = opType;
	}

	public static PaymentTransactionType getEnumFromCode(String code) {
		return getTransactionType(code);
	}

	private static PaymentTransactionType getTransactionType(String code) {
		for (PaymentTransactionType pOType : PaymentTransactionType.values()) {
			if (pOType.getCode().equals(code)) {
				return pOType;
			}
		}
		return null;
	}

	public boolean includeInBill() {
		return false;
	}

	public boolean forLimitChange() {
		return false;
	}
}
