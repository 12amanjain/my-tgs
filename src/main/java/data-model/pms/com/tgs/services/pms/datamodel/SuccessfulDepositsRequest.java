package com.tgs.services.pms.datamodel;

import java.util.ArrayList;
import java.util.List;

public class SuccessfulDepositsRequest {
	
	private List<DepositRequest> depositRequests;
	
	public List<DepositRequest> getDepositRequests() {
		if (depositRequests == null) {
			depositRequests = new ArrayList<>();
		}
		return depositRequests;
	}
}
