package com.tgs.services.pms.datamodel;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserPointsCacheFilter {

	private List<String> userIds;

	private String type;
}
