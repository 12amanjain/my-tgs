package com.tgs.services.pms.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserWallet extends DataModel {

	private Long id;

	@SearchPredicate(type = PredicateType.IN, isUserIdentifier = true)
	private String userId;

	private BigDecimal balance;

	private LocalDateTime processedOn;

	public BigDecimal getBalance() {
		return balance == null ? BigDecimal.ZERO : balance;
	}
}
