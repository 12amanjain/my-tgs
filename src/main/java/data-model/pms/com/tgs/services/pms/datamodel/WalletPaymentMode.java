package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class WalletPaymentMode extends PaymentMode {

	private UserWallet wallet;
}
