package com.tgs.services.pms.datamodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.SuperBuilder;
import org.apache.http.annotation.Obsolete;

@Getter
@Setter
@Accessors(chain = true)
@SuperBuilder
@ToString
/**
 * Used when payment is done via a wallet or creditline
 */
public class WalletPaymentRequest extends PaymentRequest {

    private String walletNumber;

    @Deprecated
	private Long walletId;

	public WalletPaymentRequest(PaymentRequest request) {
		super(request);
    }

}
