package com.tgs.services.pms.datamodel.pg;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Base64;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AtomMerchantInfo extends GatewayMerchantInfo {

	@Exclude
	private String reqHashKey;

	@Exclude
	private String trackingUrl;

	@Exclude
	private boolean isClientCodeEncoded;

	private String ru;
	private String login;
	private String pass;
	private String prodid;
	private String clientcode;
	private String custacc;
	private String ttype;
	private String amt;
	private String txncurr;
	private String txnscamt;
	private String txnid;
	private String date;
	private String signature;
	private String mdd;
	private String udf1;
	private String reqEncrypKey;
	private String reqIVKey;
	private String respHashKey;
	private String respIVKey;
	private String respDecryptKey;
	private String encdata;
	// private String bankID;
	// private String merchantDiscretionaryData;

	public void encodeClientCode() throws UnsupportedEncodingException {
		if (isClientCodeEncoded) {
			return;
		}
		byte[] bytes = clientcode.getBytes(Charset.forName("UTF-8"));
		String base64 = Base64.getEncoder().encodeToString(bytes);
		clientcode = URLEncoder.encode(base64, "UTF-8");
		isClientCodeEncoded = true;
	}

	public String getPlainSignature() {
		return new StringBuilder(login).append(pass).append(ttype).append(prodid).append(txnid).append(amt)
				.append(txncurr).toString();
	}

	public String getEncodedPassword() throws UnsupportedEncodingException {
		byte[] bytes = pass.getBytes(Charset.forName("UTF-8"));
		String base64 = Base64.getEncoder().encodeToString(bytes);
		return URLEncoder.encode(base64, "UTF-8");
	}

	public void appendRefIdToCallBack(String refId) {
		ru = StringUtils.stripEnd(ru, "/");
		ru = ru + "/" + refId;
	}
}
