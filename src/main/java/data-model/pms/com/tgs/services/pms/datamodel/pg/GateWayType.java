package com.tgs.services.pms.datamodel.pg;

import lombok.Getter;

@Getter
public enum GateWayType {

	EASYPAY_ICICI,
	ATOM_PAY,
	HDFC_FSSNET,
	RAZOR_PAY_BASIC,
	RAZOR_PAY,
	PAYU,
	EASEBUZZ,
	CREDIMAX,
	ATOM_AES,
	HDFC_CCAVENUE;

	public static GateWayType getGateWayType(String gateWayName) {
		for (GateWayType gw : GateWayType.values()) {
			if (gw.name().equals(gateWayName)) {
				return gw;
			}
		}
		return null;
	}

}
