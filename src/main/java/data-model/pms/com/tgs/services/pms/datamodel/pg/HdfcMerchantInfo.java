package com.tgs.services.pms.datamodel.pg;

import com.tgs.services.pms.datamodel.GatewayMerchantInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HdfcMerchantInfo extends GatewayMerchantInfo {
	private String id;
	private String password;
	private String action;
	private String langid;
	private String currencycode;
	private String amt;
	private String responseURL;
	private String errorURL;
	private String trackid;
	private String PaymentID;
	private String udf1;

	public void clear() {
		id = password = action = langid = currencycode = amt = responseURL = errorURL = trackid = PaymentID = udf1 = null;
	}
}
