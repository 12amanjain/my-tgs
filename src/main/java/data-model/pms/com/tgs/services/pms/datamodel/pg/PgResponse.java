package com.tgs.services.pms.datamodel.pg;

import com.tgs.services.base.datamodel.ContextMetaInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public abstract class PgResponse extends ContextMetaInfo {

	protected Boolean success;
	
	protected GateWayType gateWayType;

	public abstract String getRefId();

}
