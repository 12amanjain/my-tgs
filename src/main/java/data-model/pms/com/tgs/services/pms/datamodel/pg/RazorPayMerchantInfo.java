package com.tgs.services.pms.datamodel.pg;

import com.tgs.services.base.helper.Exclude;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class RazorPayMerchantInfo extends GatewayMerchantInfo {

    private String key;
    private String amount;
    private String currency;
    private String order_id;
    private String callback_url;
    @Exclude
    private String secret_key;

    public void appendOrderIdToCallBack(String orderId) {
        callback_url = StringUtils.stripEnd(callback_url, "/");
        callback_url = callback_url + "/" + orderId;
    }
}
