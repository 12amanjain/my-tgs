package com.tgs.services.pms.datamodel.pg;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RazorpayPgResponse extends PgResponse {

    private String razorpay_payment_id;

    private String razorpay_order_id;

    private String refId;

    private String razorpay_signature;

    private String errorCode;

    private String errorDesc;

    @Override
    public String getRefId() {
        return refId;
    }
}
