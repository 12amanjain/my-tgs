package com.tgs.services.pms.datamodel.pg.credimax;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CredimaxMerchantInfo extends GatewayMerchantInfo {

	private String merchantId;
	private String apiKey;
	private String apiOperation;
	private String interactionOperation;
	private String merchantName;
	private String amount;
	private String currency;
	private String order_id;
	private String callback_url;
	private String description;
	private String session_id;
	private String baseUrl;
	private Boolean shouldCapture;

	@Exclude
	private String apiPassword;

	public void appendRefIdToCallBack(String refId) {
		callback_url = StringUtils.stripEnd(callback_url, "/");
		callback_url = callback_url + "/" + refId;
	}

}
