package com.tgs.services.pms.datamodel.pg.credimax;

import com.google.gson.JsonObject;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class CredimaxPgResponse extends PgResponse {

	private String status;

	private String result;

	private JsonObject error;

	private String refId;

	private String transactionId;

	@Override
	public String getRefId() {
		return refId;
	}

}
