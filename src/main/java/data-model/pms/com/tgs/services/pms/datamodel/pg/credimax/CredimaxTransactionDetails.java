package com.tgs.services.pms.datamodel.pg.credimax;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class CredimaxTransactionDetails {

	private String amount;

	private String currency;

	private String status;

}
