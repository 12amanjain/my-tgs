package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessage {

	private DBSBankMessageHeader header;
	private DBSBankMessageTxnInfo txnInfo;
}
