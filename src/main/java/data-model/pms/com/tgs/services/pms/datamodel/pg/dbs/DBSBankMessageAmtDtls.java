package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessageAmtDtls {
	private String txnCcy;
	private float txnAmt;
}
