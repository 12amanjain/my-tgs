package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessageHeader {
	
	private String msgId;
	private String orgId;
	private String timeStamp;
	private String ctry;
}
