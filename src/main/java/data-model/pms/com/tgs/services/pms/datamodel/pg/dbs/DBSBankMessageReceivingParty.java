package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DBSBankMessageReceivingParty {

	private String name;
	private String accountNo;
	private String virtualAccountNo;
}
