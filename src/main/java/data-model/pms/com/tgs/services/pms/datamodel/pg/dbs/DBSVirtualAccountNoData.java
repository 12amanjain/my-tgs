package com.tgs.services.pms.datamodel.pg.dbs;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class DBSVirtualAccountNoData {

	private String userId;
	private String prefix;
}
