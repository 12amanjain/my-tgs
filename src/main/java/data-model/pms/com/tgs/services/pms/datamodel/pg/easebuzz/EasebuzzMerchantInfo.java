package com.tgs.services.pms.datamodel.pg.easebuzz;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class EasebuzzMerchantInfo extends GatewayMerchantInfo {

	private String trackingUrl;
	
	@SerializedName("irurl")
	private String initiateRedirectionURL;

	private String key;
	private String salt;
	private String productinfo;
	private String firstname;
	private String phone;
	private String email;
	private String surl;
	private String furl;
	private String hash;

}
