package com.tgs.services.pms.datamodel.pg.easebuzz;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class EasebuzzVerificationResponse {

	@SerializedName("msg")
	private EasebuzzTransactionDetails transactionDetails;

}
