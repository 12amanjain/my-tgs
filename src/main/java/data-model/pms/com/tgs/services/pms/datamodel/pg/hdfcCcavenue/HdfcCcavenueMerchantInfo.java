package com.tgs.services.pms.datamodel.pg.hdfcCcavenue;

import java.math.BigDecimal;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HdfcCcavenueMerchantInfo extends GatewayMerchantInfo {
	
	private String redirect_url;
	
	private String cancel_url;
	
	private String currency;
	
	private BigDecimal amount;
	
	private String merchant_id;
	
	private String order_id;
	
	private String access_code;
	
	private String key;
	
	private String trackingUrl;
	
	private String command;
	
	private String encRequest;
	
	private String version;
	
	public void appendRefIdToCallBack(String refId) {
		redirect_url = StringUtils.stripEnd(redirect_url, "/");
		redirect_url = redirect_url + "/" + refId;
    }

}