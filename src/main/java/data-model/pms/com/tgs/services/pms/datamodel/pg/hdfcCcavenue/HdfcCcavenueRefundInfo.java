package com.tgs.services.pms.datamodel.pg.hdfcCcavenue;

import java.math.BigDecimal;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class HdfcCcavenueRefundInfo implements RefundInfo {

	private String reference_no;
	
	private BigDecimal refund_amount;
	
	private String refund_ref_no;
}
