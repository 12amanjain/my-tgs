package com.tgs.services.pms.datamodel.pg.hdfcCcavenue;

import lombok.Getter;

@Getter
public class HdfcCcavenueRefundResponse {
 
	private String status;
	
	private String enc_response;
	
	private String refund_status;
	
	private String reason;
	
	private String error_code;
	
}
