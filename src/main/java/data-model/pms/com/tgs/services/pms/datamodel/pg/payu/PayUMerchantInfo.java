package com.tgs.services.pms.datamodel.pg.payu;

import com.google.common.base.Joiner;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PayUMerchantInfo extends GatewayMerchantInfo {

	@Exclude
	private String trackingUrl;

	private String ru;

	/* ----------- Mandatory fields from Config ------------ */
	private String key;
	private String salt;
	private String surl;
	private String furl;

	/* -------------------- Mandatory ---------------------- */
	private String amount;
	private String txnid;
	private String productinfo;
	private String firstname;
	private String email;
	private String phone;
	private String hash;

	/* --------------------- Optional --------------------- */
	private String lastname;

	public String getPlainSignature() {
		return Joiner.on('|').useForNull("").join(key, txnid, amount, productinfo, firstname, email, "|||||||||", salt);
	}

}
