package com.tgs.services.pms.datamodel.pg.payu;

import com.google.common.base.Joiner;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class PayUPgResponse extends PgResponse {

	@SerializedName("txnid")
	private String refId;

	@SerializedName("mihpayid")
	private String payUTxnId;

	@SerializedName("status")
	private String status;

	@SerializedName("key")
	private String key;

	@SerializedName("productinfo")
	private String product;

	@SerializedName("firstname")
	private String firstname;

	@SerializedName("email")
	private String email;

	@SerializedName("amount")
	private String amount;

	@SerializedName("hash")
	private String hash;

	@SerializedName("error")
	private String error;

	@Override
	public String getRefId() {
		return refId;
	}

	public String getPlainSignature(String salt) {
		return Joiner.on('|').useForNull("").join(salt, status, "|||||||||", email, firstname, product, amount, refId,
				key);
	}
}
