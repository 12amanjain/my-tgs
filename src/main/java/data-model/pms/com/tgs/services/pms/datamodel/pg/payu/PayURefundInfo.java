package com.tgs.services.pms.datamodel.pg.payu;

import lombok.Builder;
import lombok.Getter;
import com.tgs.services.pms.datamodel.pg.RefundInfo;

@Builder
@Getter
public class PayURefundInfo implements RefundInfo {

	private String payUTxnId;
	private String tokenId;
	private String refundAmt;
	
}
