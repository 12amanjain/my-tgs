package com.tgs.services.pms.datamodel.pg.payu;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class PayURefundResponse {

	@SerializedName("mihpayid")
	private String payUTxnId;

	@SerializedName("transaction_amount")
	private String amount;
	
	@SerializedName("request_id")
	private String requestId;
	
	@SerializedName("msg")
	private String message;
	
	private String status;

}
