package com.tgs.services.pms.datamodel.pg.razorpay;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayMessage {

	private String entity;

	private String account_id;

	private String event;

	private List<String> contains;

	private RazorpayPayload payload;

	private Long created_at;
}
