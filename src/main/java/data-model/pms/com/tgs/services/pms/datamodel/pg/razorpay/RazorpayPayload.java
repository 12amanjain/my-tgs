package com.tgs.services.pms.datamodel.pg.razorpay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayPayload {

	private RazorpayPayment payment;

	private RazorpayVirtualAccount virtual_account;

	private RazorpayBankTransfer bank_transfer;
}
