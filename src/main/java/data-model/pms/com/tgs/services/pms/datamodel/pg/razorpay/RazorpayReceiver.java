package com.tgs.services.pms.datamodel.pg.razorpay;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayReceiver {

	private String id;

	private String entity;

	private String ifsc;

	private String bank_name;

	private String name;

	private String account_number;
}
