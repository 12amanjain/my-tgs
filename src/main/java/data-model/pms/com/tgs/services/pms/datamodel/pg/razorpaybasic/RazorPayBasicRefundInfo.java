package com.tgs.services.pms.datamodel.pg.razorpaybasic;
import com.tgs.services.pms.datamodel.pg.RefundInfo;

import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class RazorPayBasicRefundInfo implements RefundInfo {

	private String razorPayBasictxnId;
	private String refId;
	private String speedType;
	private String refundAmt;
}
