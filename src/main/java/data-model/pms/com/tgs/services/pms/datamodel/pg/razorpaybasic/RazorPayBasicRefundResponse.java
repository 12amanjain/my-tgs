package com.tgs.services.pms.datamodel.pg.razorpaybasic;

import java.util.Map;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;

@Getter
public class RazorPayBasicRefundResponse {
	
	@SerializedName("payment_id")
	private String razorPayBasicTxnId;

	@SerializedName("amount")
	private int amount;
	
	@SerializedName("id")
	private String refundId;
	
	@SerializedName("receipt")
	private String refId;
	
	@SerializedName("acquirer_data")
	private Map<String, String>  data;
	
	private String status;
}