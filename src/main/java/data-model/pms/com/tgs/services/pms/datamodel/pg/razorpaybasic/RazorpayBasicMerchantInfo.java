package com.tgs.services.pms.datamodel.pg.razorpaybasic;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RazorpayBasicMerchantInfo extends GatewayMerchantInfo {

	private String key_id;
    private String amount;
    private String currency;
    private String order_id;
    private String callback_url;
    private String name;
    private String description;
    
    @SerializedName("prefill[email]")
    private String email;
    
    @SerializedName("prefill[contact]")
    private String contact;
    
    @SerializedName("notes[transaction_id]")
    private String notes;
    
    @Exclude
    private String secret_key;
    
    public void appendRefIdToCallBack(String refId) {
        callback_url = StringUtils.stripEnd(callback_url, "/");
        callback_url = callback_url + "/" + refId;
    }
}
