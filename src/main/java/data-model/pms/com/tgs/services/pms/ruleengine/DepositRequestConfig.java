package com.tgs.services.pms.ruleengine;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class DepositRequestConfig extends PaymentConfigurationRuleOutput {

	@SerializedName("pfe")
	private String paymentFeeExpression;
}
