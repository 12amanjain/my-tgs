package com.tgs.services.pms.ruleengine;

import java.time.DayOfWeek;
import java.util.List;
import java.util.Set;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.pms.datamodel.AmountRange;
import com.tgs.services.pms.datamodel.DepositType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentBasicRuleCriteria extends BasicRuleCriteria {

	@SerializedName("pL")
	private List<PaymentMedium> paymentMediums;

	@Deprecated
	private Product product;

	private List<Product> products;

	private List<String> userIds;

	@SerializedName("ur")
	private List<UserRole> roles;

	@SerializedName("dow")
	private Set<DayOfWeek> dayOfWeeks;

	@SerializedName("wom")
	private Set<Short> weekOfMonths;

	@SerializedName("st")
	private String startTime;

	@SerializedName("et")
	private String endTime;

	private String city;

	private String state;

	@SerializedName("bn")
	private List<String> bankNames;

	@SerializedName("sm")
	private List<String> subMediums;
	
	@SerializedName("ipp")
	private Boolean isPaymentProcess;

	@SerializedName("an")
	private List<String> accountNumbers;
	
	@SerializedName("ana")
	private List<String> accountNames;

	@SerializedName("dt")
	private List<DepositType> depositTypes;
	
	@SerializedName("ipt")
	private Boolean isPartner;
	
	@SerializedName("ar")
	private AmountRange amountRange;
}
