package com.tgs.services.pms.ruleengine;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Calendar;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleField;

public enum PaymentBasicRuleField implements IRuleField {

	@Deprecated
	PRODUCT {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getProduct().equals(fact.getProduct());
		}
	},
	PRODUCTS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getProducts() == null || input.getProducts().contains(fact.getProduct());
		}
	},
	PAYMENTMEDIUMS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getPaymentMediums() == null || input.getPaymentMediums().contains(fact.getMedium());
		}
	},
	USERIDS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getUserIds()) || input.getUserIds().contains(fact.getUserId());
		}
	},
	ROLES {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return CollectionUtils.isEmpty(input.getRoles()) || input.getRoles().contains(fact.getRole());
		}
	},
	DAYOFWEEKS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getDayOfWeeks().contains(LocalDateTime.now().getDayOfWeek());
		}
	},
	WEEKOFMONTHS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getWeekOfMonths().contains((short) Calendar.getInstance().get(Calendar.WEEK_OF_MONTH));
		}
	},
	STARTTIME {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return LocalTime.now().isAfter(LocalTime.parse(input.getStartTime()));
		}
	},
	ENDTIME {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return LocalTime.now().isBefore(LocalTime.parse(input.getEndTime()));
		}
	},
	CITY {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return fact.getUserCity().equals(input.getCity());
		}
	},
	STATE {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return fact.getUserState().equals(input.getState());
		}
	},
	BANKNAMES {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getBankNames() == null || input.getBankNames().contains(fact.getBankName());
		}
	},
	ACCOUNTNUMBERS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getAccountNumbers() == null || input.getAccountNumbers().contains(fact.getAccountNumber());
		}
	},
	DEPOSITTYPES {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getDepositTypes() == null || input.getDepositTypes().contains(fact.getDepositType());
		}
	},
	AMOUNTRANGE {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getAmountRange() == null || input.getAmountRange().contains(fact.getAmount());
		}
	},
	ACCOUNTNAMES {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getAccountNames() == null || input.getAccountNames().contains(fact.getAccountName());
		}
	},
	SUBMEDIUMS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getSubMediums() == null || "all".equalsIgnoreCase(String.valueOf(fact.getSubMedium())) || input.getSubMediums().contains(fact.getSubMedium());
		}
	},
	ISPAYMENTPROCESS {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getIsPaymentProcess() == null || (input.getIsPaymentProcess() == BooleanUtils.isTrue(fact.getIsPaymentProcess()));
		}
	},
	ISPARTNER {
		@Override
		public boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria input) {
			return input.getIsPartner() == null || (input.getIsPartner() ==  BooleanUtils.isTrue(fact.getIsPartner()));
		}
	};

	public abstract boolean isInputValidAgainstFact(PaymentFact fact, PaymentBasicRuleCriteria rule);

	@Override
	public boolean isValidAgainstFact(IFact fact, IRuleCriteria ruleCriteria) {
		return this.isInputValidAgainstFact((PaymentFact) fact, (PaymentBasicRuleCriteria) ruleCriteria);
	}

}
