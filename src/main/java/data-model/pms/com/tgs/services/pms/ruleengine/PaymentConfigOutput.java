
package com.tgs.services.pms.ruleengine;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.pms.datamodel.EasyPayMerchantInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.AtomMerchantInfo;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.HdfcMerchantInfo;
import com.tgs.services.pms.datamodel.pg.RazorPayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxMerchantInfo;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzMerchantInfo;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueMerchantInfo;
import com.tgs.services.pms.datamodel.pg.payu.PayUMerchantInfo;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicMerchantInfo;
import lombok.Getter;

@Getter
public class PaymentConfigOutput extends PaymentConfigurationRuleOutput {

	@SerializedName("gt")
	private GateWayType gatewayType;

	@SerializedName("dn")
	private String displayName;

	@SerializedName("pfe")
	private String paymentFeeExpression;

	@SerializedName("ginfo")
	@GsonRunTimeAdaptorRequired(dependOn = "gt")
	@GsonPolymorphismMapping({ @ClassType(keys = { "EASYPAY_ICICI" }, value = EasyPayMerchantInfo.class),
			@ClassType(keys = { "ATOM_PAY" }, value = AtomMerchantInfo.class),
			@ClassType(keys = { "HDFC_FSSNET" }, value = HdfcMerchantInfo.class),
			@ClassType(keys = { "RAZOR_PAY" }, value = RazorPayMerchantInfo.class),
			@ClassType(keys = { "PAYU" }, value = PayUMerchantInfo.class),
			@ClassType(keys = { "EASEBUZZ" }, value = EasebuzzMerchantInfo.class),
			@ClassType(keys = { "RAZOR_PAY_BASIC" }, value = RazorpayBasicMerchantInfo.class),
			@ClassType(keys = { "CREDIMAX" }, value = CredimaxMerchantInfo.class),
			@ClassType(keys = {"ATOM_AES"}, value = AtomMerchantInfo.class),
			@ClassType(keys = { "HDFC_CCAVENUE"} , value = HdfcCcavenueMerchantInfo.class)})
	private GatewayMerchantInfo gateWayInfo;

	private List<PaymentMedium> paymentMediums;
}
