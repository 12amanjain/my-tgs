package com.tgs.services.pms.ruleengine;

import java.math.BigDecimal;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class PaymentFact implements IFact {

	private Product product;

	private PaymentMedium medium;

	private String userId;

	private UserRole role;

	private String userCity;

	private String userState;

	private String bankName;

	private String subMedium;

	private String accountNumber;
	
	private Boolean isPaymentProcess;
	
	/**
	 * Currently it is being used for payment modes
	 */
	private String accountName;

	private DepositType depositType;

	private BigDecimal amount;
	
	private Boolean isPartner;

	public static PaymentFact createFact() {
		return PaymentFact.builder().build();
	}

	public PaymentFact generateFact(Payment payment) {
		if (payment != null) {
			setMedium(payment.getPaymentMedium());
			setProduct(payment.getProduct());
			setAmount(payment.getAmount());
		}
		return this;
	}

	public PaymentFact generateFact(PaymentRequest paymentRequest) {
		if (paymentRequest != null) {
			setMedium(paymentRequest.getPaymentMedium());
			setProduct(paymentRequest.getProduct());
			setUserId(paymentRequest.getPayUserId());
			setAmount(paymentRequest.getAmount());
		}
		return this;
	}

	public PaymentFact generateFact(DepositRequest depositRequest) {
		if (depositRequest == null) {
			return this;
		}
		setUserId(depositRequest.getUserId());
		setAccountNumber(depositRequest.getAccountNumber());
		setBankName(depositRequest.getBank());
		setDepositType(depositRequest.getType());
		setAmount(BigDecimal.valueOf(depositRequest.getRequestedAmount()).setScale(2, BigDecimal.ROUND_HALF_EVEN));
		return this;
	}

	public PaymentFact setUserId(String userId) {
		this.userId = userId;
		return this;
	}

}
