package com.tgs.services.pms.ruleengine;

import com.tgs.services.base.enums.PaymentMedium;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefundOutput extends PaymentConfigurationRuleOutput {

    private PaymentMedium medium;

}
