package com.tgs.services.points.datamodel;

import com.tgs.services.fms.datamodel.TripInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import java.util.List;


@Getter
@Setter
@ToString(onlyExplicitlyIncluded = true, includeFieldNames = true)
public class AirRedeemPointsData extends ReedemPointsData {

	@ToString.Include
	List<TripInfo> tripInfos;

	public AirRedeemPointsData() {

	}

}
