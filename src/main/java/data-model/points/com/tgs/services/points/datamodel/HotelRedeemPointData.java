package com.tgs.services.points.datamodel;

import com.tgs.services.hms.datamodel.HotelInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRedeemPointData extends ReedemPointsData {

	HotelInfo hotelInfo;

	public HotelRedeemPointData() {
	}
}
