package com.tgs.services.points.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointsOutputCriteria extends BasicRuleCriteria implements IRuleOutPut {

	@SerializedName("mrp")
	private String maxRedeemebalePointsExpression;

	// Need to ensure its a pattern. Can do that in validation, for usage will use Duration.parse()
	@SerializedName("exp")
	private String expiry;


	public String getExpression(){
		return maxRedeemebalePointsExpression;
	}


}
