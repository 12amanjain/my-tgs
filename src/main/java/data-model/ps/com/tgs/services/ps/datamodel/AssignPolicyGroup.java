package com.tgs.services.ps.datamodel;

import java.util.List;
import com.tgs.services.base.datamodel.DataModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AssignPolicyGroup extends DataModel {

	String groupId; 
	
	List<String> userIds;
}
