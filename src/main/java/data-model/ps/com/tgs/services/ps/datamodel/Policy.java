package com.tgs.services.ps.datamodel;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.ps.datamodel.configurator.PolicyConfigurator;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Policy {

	@SerializedName("pid")
	private String policyId;
	
	private List<String> labels;

	@JsonAdapter(JsonStringSerializer.class)
	private String value;
	
	private String description;
	
	private boolean enabled;
	
	private String groupId;
	
	public <T> T getOutput() {
		if(StringUtils.isNotEmpty(value))
			return  GsonUtils.getGson().fromJson(value, PolicyConfigurator.valueOf(policyId).getOutPutTypeToken().getType());
		return null;		
	}
}
