package com.tgs.services.ps.datamodel;

import java.util.List;
import com.tgs.services.base.datamodel.QueryFilter;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class UserPolicyFilter extends QueryFilter{
	
	public List<String> userIds;
	
	public List<String> policyIds;
	
	public List<String> labels;
	
}
