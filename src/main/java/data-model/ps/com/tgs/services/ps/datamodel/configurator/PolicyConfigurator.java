package com.tgs.services.ps.datamodel.configurator;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.ruleengine.IPolicyOutput;
import lombok.Getter;

@Getter
@SuppressWarnings("serial")
public enum PolicyConfigurator {

	POLICY1(new TypeToken<BooleanOutput>() {}),
	CREDITLINE_OVERRULE_TEMP_EXT (new TypeToken<DoubleOutput>() {});

	private TypeToken<? extends IPolicyOutput> outPutTypeToken;

	<T extends IPolicyOutput> PolicyConfigurator(TypeToken<T> outPutTypeToken) {
		this.outPutTypeToken = outPutTypeToken;
	}

}
