package com.tgs.services.rms.datamodel;

import java.util.Map;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.AlwaysInclude;
import com.tgs.services.base.helper.DefaultReportField;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirLineTransactionResponse {

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Id")
	private String bookingId;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Type")
	private String bookingType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Date")
	private String bookingDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Time")
	private String bookingTime;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Payment Medium")
	private String paymentMedium;

	@DefaultReportField
	@SerializedName("Product Type")
	private String productType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Status")
	private String cartStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Cart Generation Date")
	private String cartGenerationDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Cart Generation Time")
	private String cartGenerationTime;

	@DefaultReportField
	@SerializedName("Payment Form")
	private String paymentForm;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Invoice Status")
	private String invoiceStatus;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Last modified User id")
	private String assignedUserId;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Last modified User name")
	private String assignedUserName;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Pass through")
	private Integer passThrough;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("System invoice id")
	private String systemInvoiceId;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Third party invoice id")
	private String thirdPartyInvoiceId;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Third party invoice status")
	private String thirdPartyInvoiceStatus;

	@DefaultReportField
	@SerializedName("Tour Code")
	private String tourCode;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Corporate Code")
	private String corporateCode;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Fare Basis")
	private String farebasis;

	@DefaultReportField
	@SerializedName("Reseller Id")
	private String bookingUserId;

	@DefaultReportField
	@SerializedName("Reseller Email")
	private String bookingUserEmail;

	@DefaultReportField
	@SerializedName("Reseller Name")
	private String bookingUserName;

	@DefaultReportField
	@SerializedName("Reseller Grade")
	private String bookingUserGrade;

	@DefaultReportField
	@SerializedName("Reseller City")
	private String bookingUserCity;

	@DefaultReportField
	@SerializedName("Reseller State")
	private String bookingUserState;

	@DefaultReportField
	@SerializedName("Reseller Country")
	private String bookingUserCountry;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Booking User Name")
	private String loggedInUserName;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Booking Employee Name")
	private String bookingEmployeeName;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Reseller Account Code")
	private String bookingUserAccountcode;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Product Name")
	private String airline;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Description")
	private String description;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Payment Status")
	private String paymentStatus;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Amendment Id")
	private String amendmentId;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Amendment Type")
	private String amendmentType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Amendment Date")
	private String amendmentDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Trip Type")
	private String tripType;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Departure")
	private String departureAirport;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Departure City")
	private String departureCity;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival")
	private String arrivalAirport;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival City")
	private String arrivalCity;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Destination")
	private String destCountry;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Departure Date")
	private String departureDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival Date")
	private String arrivalDate;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Departure Time")
	private String departureTime;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Arrival Time")
	private String arrivalTime;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Days to travel")
	private long days;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Supplier Name")
	private String supplier;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Supplier Code")
	private String supplierCode;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Predefined Recommended Source")
	private String predefinedSource;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Airline Code")
	private String airlineCode;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Flight Number")
	private String flightNumber;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Passenger Name")
	private String name;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Class")
	private String bookingClass;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Cabin Class")
	private String cabinClass;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Pax")
	private String paxType;

	@DefaultReportField
	@SerializedName("Gds Pnr")
	private String gdsPnr;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Pnr")
	private String pnr;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Ticket Number")
	private String ticketNumber;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Base Fare")
	private Double baseFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Total Fare")
	private Double totalFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Payment Fee")
	private Double paymentFee;

	@DefaultReportField
	@SerializedName("IATA Commission")
	private Double iata;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("TDS")
	private Double tds;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("YQ")
	private Double yQ;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("YR")
	private Double yR;

	// base+yq+yr
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Taxable")
	private Double taxable;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("TAF")
	private Double taf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("CGST")
	private Double cgst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("SGST")
	private Double sgst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("IGST")
	private Double igst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("UGST")
	private Double ugst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("RCF")
	private Double rcf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("PSF")
	private Double psf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("UDF")
	private Double udf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("JN")
	private Double jn;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("OB")
	private Double ob;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("OC")
	private Double oc;

	@DefaultReportField
	@SerializedName("Client Markup")
	private Double cmu;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Airline Gst")
	private Double airlineGst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Gross Fare")
	private Double grossFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("To Gst")
	private String toGst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("From Gst")
	private String fromGst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Management Fee")
	private Double managementFee;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Voucher Discount")
	private Double voucherDiscount;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Voucher Code")
	private String voucherCode;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Gross Discount")
	private Double grossDiscount;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Net Discount")
	private Double netDiscount;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Net Fare")
	private Double netFare;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Net Fare With Markup")
	private Double netFareWithMarkup;

	@DefaultReportField
	@SerializedName("Amendment Markup")
	private Double amendMentMarkup;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Supplier Amendment Fee")
	private Double acf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Reseller Amendment Fee")
	private Double ccf;

	@DefaultReportField
	@SerializedName("Client Amendment Fee")
	private Double caf;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Other Taxes")
	private Double otherTaxes;

	@DefaultReportField
	@SerializedName("Mark Up")
	private Double markup;

	@DefaultReportField
	@SerializedName("Points Discount")
	private Double pointsDiscount;

	@DefaultReportField
	@SerializedName("Management Fee Gst")
	private Double managementGst;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Credit Shell Amount")
	private Double creditShellAmount;

	@DefaultReportField
	@SerializedName("Sales Hierarchy")
	private String salesHierachy;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Employee Name")
	private String employeeName;

	@DefaultReportField
	@SerializedName("User Type")
	private String userType;

	@DefaultReportField
	@SerializedName("Personal Booking")
	private String personalBooking;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Billing Entity Reg Name")
	private String beCompanyName;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Billing Entity GST")
	private String beGstNumber;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Billing Entity Address")
	private String beAddress;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Billing Entity Phone")
	private String bePhone;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Billing Entity Email")
	private String beEmail;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Accounting Code")
	private String accountingCode;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 1 Fare")
	private Double lf1Fare;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 1 Number")
	private String lf1Number;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 1 Date")
	private String lf1Date;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 1 Time")
	private String lf1Time;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 2 Fare")
	private Double lf2Fare;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 2 Number")
	private String lf2Number;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 2 Date")
	private String lf2Date;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 2 Time")
	private String lf2Time;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 3 Fare")
	private Double lf3Fare;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 3 Number")
	private String lf3Number;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 3 Date")
	private String lf3Date;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Lowest Flight 3 Time")
	private String lf3Time;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Ticketed vs Lowest1 fare Difference")
	private Double lf1dev;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Ticketed vs Lowest2 fare Difference")
	private Double lf2dev;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Ticketed vs Lowest3 fare Difference")
	private Double lf3dev;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Wasted Amount")
	private Double wasted;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Reason For Higher Fare")
	private String reason;

	@DefaultReportField(includedRoles = {UserRole.CORPORATE})
	@SerializedName("Trip Id")
	private String tripId;

	@DefaultReportField(includedRoles = {UserRole.WHITELABEL_PARTNER})
	@SerializedName("Partner MarkUp")
	private Double partnerMarkup;

	@DefaultReportField(includedRoles = {UserRole.WHITELABEL_PARTNER})
	@SerializedName("Partner Commission")
	private Double partnerCommission;

	@DefaultReportField(includedRoles = {UserRole.WHITELABEL_PARTNER})
	@SerializedName("Partner TDS")
	private Double partnerTds;

	@DefaultReportField()
	@SerializedName("PAN Number")
	private String panNumber;

	@AlwaysInclude
	private Map<String, Object> cfMap;
}
