package com.tgs.services.rms.datamodel;

import com.tgs.services.base.helper.ReportParam;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AmexReportHeader {

	@ReportParam(beginIndex = 1, length = 4)
	private String recordType;
	
	@ReportParam(beginIndex = 5, length = 6)
	private String createDate;
	
	@ReportParam(beginIndex = 11, length = 4)
	private String agentRefCode;
	
	@ReportParam(beginIndex = 15, length = 9)
	private String fileSequenceNumber;
	
	@ReportParam(beginIndex = 24, length = 10)
	private String merchantNumber;
	
	@ReportParam(beginIndex = 34, length = 30)
	private String agentName;
	
	@ReportParam(beginIndex = 64, length = 3)
	private String currencyCode;
	
	@ReportParam(beginIndex = 67, length = 752)
	private String filler;
	
}
