package com.tgs.services.rms.datamodel;

import com.tgs.services.base.helper.ReportParam;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class AmexReportResponse {

	@ReportParam(beginIndex = 1, length = 4)
	@Builder.Default
	private String recordType = "5555";

	@ReportParam(beginIndex = 5, length = 6)
	private String transactionDate;

	@ReportParam(beginIndex = 11, length = 6, isFiller = true)
	private String clientReferenceNo;

	@ReportParam(beginIndex = 17, length = 4, isFiller = true)
	private String filler;

	@ReportParam(beginIndex = 21, length = 15, isNumeric = true)
	@Builder.Default
	private String accountNumber = "376919738691002";

	@ReportParam(beginIndex = 36, length = 3)
	private String airlineCode;

	@ReportParam(beginIndex = 39, length = 11)
	private String ticketNumber;

	@ReportParam(beginIndex = 50, length = 2, isFiller = true)
	private String travellerInitName;

	@ReportParam(beginIndex = 52, length = 20)
	private String travellerName;

	@ReportParam(beginIndex = 72, length = 20)
	private String destinationDescription;

	@ReportParam(beginIndex = 92, length = 6)
	private String departureDate;

	@ReportParam(beginIndex = 98, length = 15, isFiller = true)
	private String obseleteFiller;

	@ReportParam(beginIndex = 113, length = 1)
	private String creditOrDebit;

	@ReportParam(beginIndex = 114, length = 6, isFiller = true)
	private String approvalCode;

	@ReportParam(beginIndex = 120, length = 1)
	@Builder.Default
	private String travelTypeCode = "A";

	@ReportParam(beginIndex = 121, length = 9, isNumeric = true)
	@Builder.Default
	private String travelInvoiceNo = "13609093";

	@ReportParam(beginIndex = 130, length = 2)
	private String airCarrierCode;

	@ReportParam(beginIndex = 132, length = 14, isNumeric = true)
	private String travelAmount;

	@ReportParam(beginIndex = 146, length = 45)
	private String department;

	@ReportParam(beginIndex = 191, length = 20)
	private String tripId;

	@ReportParam(beginIndex = 211, length = 20)
	private String costCenter;

	@ReportParam(beginIndex = 231, length = 14, isNumeric = true, isFiller = true)
	private String gstAmount;

	@ReportParam(beginIndex = 245, length = 1)
	@Builder.Default
	private String gstInd = "N";

	// Can fetch from client info
	@ReportParam(beginIndex = 246, length = 3)
	@Builder.Default
	private String currCode = "INR";

	@ReportParam(beginIndex = 249, length = 8)
	private String transDate;

	@ReportParam(beginIndex = 257, length = 2)
	private String transactionCode;

	@ReportParam(beginIndex = 259, length = 9)
	private String fillerField;

	@ReportParam(beginIndex = 268, length = 36, isNumeric = true)
	private String fillerField2;

	@ReportParam(beginIndex = 304, length = 3)
	@Builder.Default
	private String airProdCode = "307";

	@ReportParam(beginIndex = 307, length = 20)
	private String employeeCode;

	@ReportParam(beginIndex = 327, length = 8)
	private String fillerField3;

	@ReportParam(beginIndex = 335, length = 6)
	private String fillerField4;

	@ReportParam(beginIndex = 341, length = 31)
	private String fillerField5;

	@ReportParam(beginIndex = 372, length = 14, isFiller = true, isNumeric = true)
	private String airTaxAmount;

	@ReportParam(beginIndex = 386, length = 11, isFiller = true)
	private String airPnrNumber;

	@ReportParam(beginIndex = 397, length = 1)
	private String domOrIntl;

	@ReportParam(beginIndex = 398, length = 1)
	private String merchantAgentOrAirline;

	@ReportParam(beginIndex = 399, length = 96)
	private String fillerField6;

	// Repeated 4 times
	@ReportParam(beginIndex = 495, length = 324)
	private String airSectorData;

	@ReportParam(beginIndex = 576, length = 324)
	private String airSectorData1;

	@ReportParam(beginIndex = 657, length = 324)
	private String airSectorData2;

	@ReportParam(beginIndex = 738, length = 324)
	private String airSectorData3;

	@Setter
	@Getter
	public class AirSectorData {
		@ReportParam(beginIndex = 1, length = 14)
		private String airPrimaryTicketNumber;

		@ReportParam(beginIndex = 15, length = 2, isNumeric = true)
		private int airSectorNumber;

		@ReportParam(beginIndex = 17, length = 2)
		private String airCarrierCode;

		@ReportParam(beginIndex = 19, length = 4, isFiller = true)
		private String airFlightNumber;

		@ReportParam(beginIndex = 23, length = 14, isFiller = true)
		private String filler5;

		@ReportParam(beginIndex = 37, length = 6, isFiller = true)
		private String departureDate;

		@ReportParam(beginIndex = 43, length = 3)
		private String departurePort;

		@ReportParam(beginIndex = 46, length = 3)
		private String arrivalPort;

		@ReportParam(beginIndex = 49, length = 2)
		private String airClass;

		@ReportParam(beginIndex = 51, length = 31, isFiller = true)
		private String filler6;


	}

}
