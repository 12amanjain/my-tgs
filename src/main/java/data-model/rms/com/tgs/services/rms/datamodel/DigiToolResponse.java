
package com.tgs.services.rms.datamodel;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.DefaultReportField;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DigiToolResponse {
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("ATO")
	private String bookingId;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Agency Id")
	private String bookingUserId;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Airline Code")
	private String airlineCode;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Supplier Name")
	private String supplier;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Airlines")
	private String airline;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Sector")
	private String tripType;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Refund Type")
	private String amendmentType;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Ticket No")
	private String ticketNumber;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Passenger Name")
	private String name;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Pnr No")
	private String pnr;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Reference Id")
	private String amendmentId;

	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Request Date")
	private String amendmentDate;
	
	@DefaultReportField(includedRoles = {UserRole.CALLCENTER, UserRole.ADMIN, UserRole.SUPERVISOR})
	@SerializedName("Amendment Status")
	private String amendmentStatus;
	
	
	
}
