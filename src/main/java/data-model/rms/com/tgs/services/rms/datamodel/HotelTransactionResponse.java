package com.tgs.services.rms.datamodel;

import java.time.LocalDate;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.DefaultReportField;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelTransactionResponse {

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Sr")
	private Long sr;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Id")
	private String bookingId;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Supplier")
	private String supplier;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Supplier Booking ID")
	private String supplierBookingId;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Agent ID")
	private String agentId;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Agency State")
	private String agencyState;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Agency City")
	private String agencyCity;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Sales in Charge")
	private String salesInCharge;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Hotel Confirmation No")
	private String supplierRefNo;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Agent")
	private String agent;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Status")
	private String status;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Amount")
	private Double bookingAmount;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Currency")
	private String bookingCurrency;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Leader Name")
	private String leaderName;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Service Date")
	private LocalDate serviceDate;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Checkout Date")
	private LocalDate checkoutDate;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Deadline")
	private LocalDate deadline;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("City")
	private String city;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Country")
	private String country;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Booking Date")
	private LocalDate bookingDate;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Hotel Name")
	private String hotelName;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Hotel Category")
	private String hotelCategory;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("No. of Nights")
	private Integer noOfNights;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Total No. of Rooms")
	private Integer totalNoOfRooms;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Total Room Nights")
	private Integer totalRoomNights;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Agent Markup")
	private Double agentMarkup;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Flow Type")
	private String flowType;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("PassportNumber")
	private String pNum;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("PanNumber")
	private String pan;
	
	@DefaultReportField
	@SerializedName("Points Discount")
	private Double pointsDiscount;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Voucher Discount")
	private Double voucherDiscount;

	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Voucher Code")
	private String voucherCode;

	@SerializedName("Client Markup")
	private Double cmu;
	
	@DefaultReportField(nonMidOfficeRole = true)
	@SerializedName("Discount")
	private Double discount;
}
