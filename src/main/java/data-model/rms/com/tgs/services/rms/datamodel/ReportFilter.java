package com.tgs.services.rms.datamodel;

import java.util.List;
import java.util.Map;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
@GsonRunTimeAdaptorRequired(dependOn = "reportType")
@GsonPolymorphismMapping({
        @ClassType(keys = {"AIRLINE_TRANSACTION", "AT"}, value = AirLineTransactionReportFilter.class),
        @ClassType(keys = {"BOOKING_TRANSACTION", "BT"}, value = AirBookingTransactionReportFilter.class),
        @ClassType(keys = {"HOTEL_TRANSACTION", "BT"}, value = HotelTransactionReportFilter.class),
        @ClassType(keys = {"DSR"}, value = DSRFilter.class),
        @ClassType(keys = {"DIGI_TOOL"}, value = DigiToolReportFilter.class)
})
public class ReportFilter extends QueryFilter {

    @ApiModelProperty(value = "To fetch reports based on reports type")
    private ReportType reportType;

    @ApiModelProperty(value = "Fields to be shown in response")
    private List<String> outputFields;

    @ApiModelProperty(value = "Id of database table to fetch the query")
    private Long queryId;

    @ApiModelProperty(value = "Fields to be replaced in dynamic query fetched from database")
    private Map<String, Object> queryFields;

    @ApiModelProperty(value = "To fetch reports in a particular format")
    private ReportFormat outputFormat;

    @ApiModelProperty(value = "Email ids of recepient's who will be receiving the reports")
    private List<String> emailIds;

}
