package com.tgs.services.rms.datamodel;

import lombok.Getter;

@Getter
public enum ReportFormat {
    CSV("csv"), EXCEL("xls"), PDF("pdf"), REST("R");

    private String code;

    private ReportFormat(String code) {
        this.code = code;
    }

}
