package com.tgs.services.rms.datamodel;

import lombok.Getter;

@Getter
public enum ReportType {

    DSR("DSR"),
    DATABASEQUERY("DB"),
    AIRLINE_TRANSACTION("AT"),
    BOOKING_TRANSACTION("BT"),
    HOTEL_TRANSACTION("HT"),
    DIGI_TOOL("DT"),
    AMEX_REPORT("AR");

    private String code;

    ReportType(String code) {
        this.code = code;
    }
}
