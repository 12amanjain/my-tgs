package com.tgs.services.rms.datamodel;

import java.time.LocalDateTime;

import com.tgs.services.base.datamodel.DataModel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ReportsQuery extends DataModel {

	private Long id;

	private String query;

	private String columns;

	private String description;

	private LocalDateTime createdOn;

}
