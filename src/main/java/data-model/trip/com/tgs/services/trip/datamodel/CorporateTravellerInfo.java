package com.tgs.services.trip.datamodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.enums.PaxType;
import lombok.Getter;
import lombok.Setter;

/**
 * This data model is actually similar to TravellerInfo. But here we need to save custom fields in userProfile,
 * but in normal flow we need not save custom fields.
 * For now i have created new data model but need to find a solution for such things to remove duplicacy
 *
 */
@Getter
@Setter
public class CorporateTravellerInfo {

	@SerializedName("ti")
	private String title;
	@SerializedName("pt")
	private PaxType paxType;
	@SerializedName("fN")
	private String firstName;
	@SerializedName("lN")
	private String lastName;
	private String userId;
	private UserProfile userProfile;
}
