package com.tgs.services.ums.datamodel;


import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AadhaarAddress {
	
	
	private String loc;
	
	private String country;
	
	private String house;
	
	private String subdist;
	
	private String vtc;
	
	private String po;
	
	private String state;
	
	private String street;
	
	private String dist;
	
	
}
