package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AadhaarInfo {
	
	@SerializedName("an")
	private String aadhaarNumber;
	
	@SerializedName("gn")
	private String gender;
	
	private String dob;
	
	private String zip;
	
	private AadhaarAddress aadhaarAddress;
	
	@SerializedName("fn")
	private String fullName;

	
	@SerializedName("zd")
	private String zipData;

	
	@SerializedName("cf")
	private String careOf;

	
	@SerializedName("pi")
	private String profileImage;

	
	@SerializedName("rx")
	private String rawXml;

	
	@SerializedName("sc")
	private String shareCode;

	
	@SerializedName("cid")
	private String clientId;

	

    
}
