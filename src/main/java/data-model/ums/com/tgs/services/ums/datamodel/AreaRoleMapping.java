package com.tgs.services.ums.datamodel;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.util.CollectionUtils;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AreaRoleMapping extends DataModel {

	private Long id;

	@SearchPredicate(type = PredicateType.IN, filterName = "areaRoles")
	private AreaRole areaRole;

	private AreaRoleMappingInfo mappingInfo;

	public static Map<UserRole, Set<String>> getMapOfFieldsByUserRole(List<AreaRoleMapping> areaRoleMappings) {
		Map<UserRole, Set<String>> map = new HashMap<>();
		areaRoleMappings.forEach(entry -> {
			if (entry.getMappingInfo() != null) {
				entry.getMappingInfo().getUserRoleSet().forEach(role -> {
					map.put(role, entry.getMappingInfo().getFields());
				});
			}
		});
		return map;
	}

	public static Map<UserRole, Set<AreaRole>> getMapByUserRole(List<AreaRoleMapping> areaRoleMappings) {
		Map<UserRole, Set<AreaRole>> map = new HashMap<>();
		areaRoleMappings.forEach(entry -> {
			if (entry.getMappingInfo() != null) {
				if (!CollectionUtils.isEmpty(entry.getMappingInfo().getUserRoleSet())) {
					entry.getMappingInfo().getUserRoleSet().forEach(ur -> {
						if (!map.containsKey(ur))
							map.put(ur, new HashSet<>());
						map.get(ur).add(entry.getAreaRole());
					});
				}
				if (entry.getMappingInfo().isMidOfficeRoles()) {
					for (UserRole userRole : UserRole.getMidOfficeRoles()) {
						if (!map.containsKey(userRole))
							map.put(userRole, new HashSet<>());
						map.get(userRole).add(entry.getAreaRole());
					}
				}

				if (entry.getMappingInfo().isAllRoles()) {
					for (UserRole userRole : UserRole.values()) {
						if (!map.containsKey(userRole))
							map.put(userRole, new HashSet<>());
						map.get(userRole).add(entry.getAreaRole());
					}
				}
			}
		});
		return map;
	}

	public static Map<UserRole, Set<AreaRole>> getMapByEmulatedRoleSet(List<AreaRoleMapping> areaRoleMappings) {
		Map<UserRole, Set<AreaRole>> map = new HashMap<>();
		areaRoleMappings.forEach(entry -> {
			if (entry.getMappingInfo() != null) {
				if (!CollectionUtils.isEmpty(entry.getMappingInfo().getEmulatedRolesSet())) {
					entry.getMappingInfo().getEmulatedRolesSet().forEach(ur -> {
						if (!map.containsKey(ur))
							map.put(ur, new HashSet<>());
						map.get(ur).add(entry.getAreaRole());
					});
				}
			}
		});
		return map;
	}

	public static Map<String, Set<AreaRole>> getMapByUserRoleGroups(List<AreaRoleMapping> areaRoleMappings) {
		Map<String, Set<AreaRole>> map = new HashMap<>();
		areaRoleMappings.forEach(entry -> {
			if (entry.getMappingInfo() != null) {
				if (!CollectionUtils.isEmpty(entry.getMappingInfo().getUserRoleGroups())) {
					entry.getMappingInfo().getUserRoleGroups().forEach(group -> {
						if (!map.containsKey(group))
							map.put(group, new HashSet<>());
						map.get(group).add(entry.getAreaRole());
					});
				}
			}
		});
		return map;
	}

	public static Set<AreaRole> allowedAreaRoles(List<AreaRoleMapping> areaRoleMappings, User user) {
		Map<String, Set<AreaRole>> userRoleGroupMap = getMapByUserRoleGroups(areaRoleMappings);
		Map<UserRole, Set<AreaRole>> userRoleMap = getMapByUserRole(areaRoleMappings);
		Set<AreaRole> allowedRoles = new HashSet<>();
		if (!CollectionUtils.isEmpty(user.getAdditionalInfo().getGroups())) {
			for (String userGroup : user.getAdditionalInfo().getGroups()) {
				Set<AreaRole> areaRoles =
						userRoleGroupMap.containsKey(userGroup) ? userRoleGroupMap.get(userGroup) : null;
				if (areaRoles != null && !CollectionUtils.isEmpty(areaRoles)) {
					allowedRoles.addAll(userRoleGroupMap.get(userGroup));
				}
			}
		}
		if (userRoleMap.containsKey(user.getRole()))
			allowedRoles.addAll(userRoleMap.get(user.getRole()));

		return allowedRoles;
	}
}
