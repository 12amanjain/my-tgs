package com.tgs.services.ums.datamodel;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CinInfo {

	@SerializedName("cc")
	private String companyClass;
	
	@SerializedName("cid")
	private String clientId;
	
	private String zip;
	
	@SerializedName("cin")
	private String cinNumber;
	
	@SerializedName("ca")
	private String companyAddress;
	
	@SerializedName("email")
	private String email;

	@SerializedName("id")
    private String incorporationDate;

	@SerializedName("cn")
	private String companyName;

	@SerializedName("pn")
	private String phoneNumber;
	
	private List<Directors> directors;
}
