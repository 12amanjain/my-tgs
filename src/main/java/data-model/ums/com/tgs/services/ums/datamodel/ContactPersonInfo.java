package com.tgs.services.ums.datamodel;

import javax.validation.constraints.NotNull;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
public class ContactPersonInfo {

	@NotNull
	@ApiModelProperty(required = true)
	private String name;
	@NotNull
	@ApiModelProperty(required = true)
	private String email;
	@NotNull
	@ApiModelProperty(required = true)
	private String mobile;
	private String phone;
}
