package com.tgs.services.ums.datamodel;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DailyUsageInfo {

	private Double limit;
	private Double balance;
	private String userId;
}
