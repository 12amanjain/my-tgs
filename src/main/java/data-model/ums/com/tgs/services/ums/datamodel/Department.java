package com.tgs.services.ums.datamodel;

public enum Department {
	ACCOUNTS, SALES;
}
