package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;

@Getter
public class Directors {

	@SerializedName("pa")
	private String present_address;

	@SerializedName("na")
	private String nationality;

	@SerializedName("cid")
	private String client_id;

	@SerializedName("fn")
	private String father_name;

	@SerializedName("email")
	private String email;

	@SerializedName("pma")
	private String permanent_address;

	@SerializedName("name")
	private String full_name;

	@SerializedName("dob")
	private String dob;

	@SerializedName("dinno")
	private String din_number;

}
