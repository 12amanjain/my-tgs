package com.tgs.services.ums.datamodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class DueInfo {
	private BigDecimal amount;
	private Integer dueDays;
	private LocalDateTime dueDate;

	public BigDecimal getAmount() {
		return amount == null ? BigDecimal.ZERO : amount;
	}
}
