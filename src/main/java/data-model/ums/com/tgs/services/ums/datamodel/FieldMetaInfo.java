package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FieldMetaInfo {

    @SerializedName("displayLabel")
    private String displayLabel;

    @SerializedName("name")
    private String name;

    @SerializedName("required")
    private boolean required;

    @SerializedName("vpax")
    private boolean vpax;

    @SerializedName("vReports")
    private boolean vReports;

    @SerializedName("vInvoice")
    private boolean vInvoice;

    @SerializedName("isEditable")
    private boolean isEditable;
}
