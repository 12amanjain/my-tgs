package com.tgs.services.ums.datamodel;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class GSTInfo {

	@NotNull
	@ApiModelProperty(required = true)
	private String gstNumber;
	@ApiModelProperty(notes = "All characters should be in lower case", example = "ashu.gupta@technogramsolutions.com")
	private String gstEmail;
	@ApiModelProperty(notes = "Phone number should be including local area code", example = "01126479985")
	private String gstPhone;
	@NotNull
	@ApiModelProperty(required = true)
	private String gstName;
	@Size(min = 0, max = 500)
	@ApiModelProperty(notes = "Size shouldn't be greater than 500 characters")
	private String gstAddress;
}
