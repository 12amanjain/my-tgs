package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class LicenseInfo {

	@SerializedName("ta")
	private String TemporaryAddress;
	
	@SerializedName("fohn")
	private String fatherOrHusbandName;
	
	@SerializedName("doe")
	private String doe;
	
	@SerializedName("tz")
	private String TemporaryZip;
	
	@SerializedName("pa")
	private String PermanentAddress;
	
	@SerializedName("doi")
	private String doi;
	
	@SerializedName("ci")
	private String ClientId;
	
	@SerializedName("ctzn")
	private String citizenship;
	
	@SerializedName("dob")
	private String dob;
	
	@SerializedName("pz")
	private String PermanentZip;
	
	@SerializedName("gn")
	private String gender;
	
	@SerializedName("ln")
	private String LicenseNumber;
	
	@SerializedName("name")
	private String name;
	
	@SerializedName("state")
	private String state;
	
	@SerializedName("on")
	private String OlaName;
	
	@SerializedName("oc")
	private String OlaCode;
}
