package com.tgs.services.ums.datamodel;

import javax.validation.constraints.NotNull;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PanInfo {
	@NotNull
	@ApiModelProperty(required = true)
	private String number;
	/* Name on pan card */
	@NotNull
	@ApiModelProperty(required = true)
	private String panName;
}
