package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;



public class PassportInfo {
	
	private String doe;
	
	private String dob;
	
	private String father;
	
	@SerializedName("gn")
	private String givenName;
	
	@SerializedName("ml1")
	private String mrzLine1;

	
	@SerializedName("opn")
	private String oldPassportNum;

	
	@SerializedName("fn")
	private String fileNum;

	
	@SerializedName("ci")
	private String clientId;

	
	@SerializedName("pi")
	private String placeOfIssue;

	
	@SerializedName("sp")
    private String spouse;

	
	@SerializedName("cc")
	private String countryCode;

	
	@SerializedName("ad")
	private String address;

	
	@SerializedName("sn")
	private String surname;

	
	@SerializedName("ml2")
	private String mrzLine2;

	
	@SerializedName("pn")
	private String passportNum;

	
	@SerializedName("")
	private String doi;

	
	@SerializedName("od")
    private String oldDoi;

	
	private String gender;

	
	@SerializedName("nn")
	private String nationality;

	
	@SerializedName("pb")
	private String placeOfBirth;

	
	private String mother;


	@SerializedName("opi")
	private String oldPlaceOfIssue;

	private String pin;

	
	@SerializedName("vf")
	private String verified;
	
	
}
