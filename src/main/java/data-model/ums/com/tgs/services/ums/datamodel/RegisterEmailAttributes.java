package com.tgs.services.ums.datamodel;

import com.tgs.services.base.datamodel.EmailAttributes;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class RegisterEmailAttributes extends EmailAttributes{

	private String name;
	
	private String email;
	
	private String mobile;
	
	private String status;
	
	private String userId;
}
