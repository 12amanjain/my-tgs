package com.tgs.services.ums.datamodel;

import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.FilterMapping;
import com.tgs.services.base.annotations.Inject;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.ForbidInAPIRequest;
import com.tgs.services.base.helper.JwtExclude;
import com.tgs.services.base.helper.UserId;
import io.swagger.annotations.ApiParam;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class User extends DataModel {

	@ForbidInAPIRequest
	private Long id;

	@NotNull
	@SearchPredicate(type = PredicateType.LIKE)
	private String name;

	@NotNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private String email;

	@NotNull
	@SearchPredicate(type = PredicateType.EQUAL)
	private String mobile;

	private String phone;

	@NotNull
	@SearchPredicate(type = PredicateType.IN, filterName = "roles")
	@SearchPredicate(type = PredicateType.IN, isNegated = true)
	private UserRole role;

	private User emulateUser;

	@UserId(creditLine = true, userRelation = true, walletBalance = true)
	@SearchPredicate(type = PredicateType.EQUAL, isUserIdentifier = true)
	@SearchPredicate(type = PredicateType.IN, filterName = "userIds", isUserIdentifier = true)
	@Inject(dbEntity = "DbUserRelation", field = "userId1")
	private String userId;

	@SearchPredicate(type = PredicateType.IN)
	private String partnerId;

	private Double balance;

	private Double walletBalance;

	@SearchPredicate(type = PredicateType.IN, filterName = "statuses")
	private UserStatus status;

	private LocalDateTime createdOn;

	@SearchPredicate(type = PredicateType.LT)
	@SearchPredicate(type = PredicateType.GT)
	private LocalDateTime processedOn;

	private GSTInfo gstInfo;
	@JwtExclude
	private PanInfo panInfo;

	private AddressInfo addressInfo;

	private ContactPersonInfo contactPersonInfo;
	@APIUserExclude
	private UserAdditionalInfo additionalInfo;

	@SearchPredicate(type = PredicateType.EQUAL)
	private String parentUserId;

	/**
	 * Use {@link #parentUser} which consist of all parent user details
	 */
	@Deprecated
	private UserConfiguration parentConf;

	@SerializedName("pu")
	private User parentUser;

	private Boolean canBeEmulated;

	@APIUserExclude

	private UserConfiguration userConf;

	@JwtExclude
	private UserProfile userProfile;

	@SearchPredicate(type = PredicateType.EQUAL)
	@SearchPredicate(type = PredicateType.LIKE, filterName = "employeeLike")
	private String employeeId;

	@JwtExclude
	private Set<User> userRelations;

	private Double totalBalance;

	@ApiParam(name = "du", value = "Used for Daily Usage Info")
	@SerializedName("du")
	private DailyUsageInfo dailyUsageInfo;

	@SerializedName("bs")
	private UserBalanceSummary balanceSummary;

	@SerializedName("ds")
	private UserDuesSummary dueSummary;

	public boolean paxWiseInvoice() {
		if (getUserConf().getPaxInvoice() == null) {
			return UserRole.corporate(getRole());
		}
		return BooleanUtils.isTrue(getUserConf().getPaxInvoice());
	}

	/**
	 * In case of emulate, person who has emulated into user account , his/her userId will be treated as loggedInUserId
	 * , otherwise in case of staff and normal user it will be userId of the person who has loggedIn into our system.
	 *
	 * @return String
	 */
	public String getLoggedInUserId() {
		return userId;
	}

	public String getEmulateOrLoggedInUserId() {
		return ObjectUtils.firstNonNull(emulateUser != null ? emulateUser.getUserId() : null, userId);
	}

	public UserAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = UserAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	public UserConfiguration getUserConf() {
		if (userConf == null) {
			userConf = UserConfiguration.builder().build();
		}
		return userConf;
	}

	public UserProfile getUserProfile() {
		return userProfile == null ? UserProfile.builder().build() : userProfile;
	}

	/**
	 * In case of staff we consider staff's parent as actual user
	 * 
	 * @return
	 */
	public String getParentUserId() {
		return ObjectUtils.firstNonNull(parentUserId, userId);
	}

	@Exclude
	@SearchPredicate(type = PredicateType.IN)
	@FilterMapping(dbEntity = "DbUserRelation", filterField = "userId2In")
	public String salesUserId;

}
