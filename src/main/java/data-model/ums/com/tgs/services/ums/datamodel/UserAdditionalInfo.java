package com.tgs.services.ums.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.datamodel.Cleanable;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.DocumentInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.JwtExclude;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Builder
@Getter
@Setter
public class UserAdditionalInfo implements Cleanable {

	@SerializedName("lurl")
	private String logoURL;

	@SerializedName("gps")
	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "User", dbAttribute = "additionalInfo.gps")
	private List<String> groups;

	@SerializedName("tr")
	private Double tdsRate;

	@SerializedName("ac")
	private String accountingCode;

	@SerializedName("rfb")
	private String referredBy;

	@SerializedName("ll")
	private LocalDateTime lastLoginTime;

	@SerializedName("lt")
	private Map<Product, LocalDateTime> lastTxnTime;

	// Daily Usage limit per wallet/creditLine for Staff users
	@SerializedName("wu")
	private Map<String, Double> walletUsageLimit;

	// Daily Total wallet usage limit for staff users
	@SerializedName("du")
	private Double dailyUsageLimit;

	@SerializedName("dts")
	private List<DocumentInfo> documents;

	@SerializedName("pfexp")
	private Map<PaymentMedium, String> paymentFees;

	private String grade;

	@SerializedName("ft")
	private String firmType;

	private Map<Department, DeliveryInfo> deptInfo;

	@SerializedName("dm")
	private Boolean disableMarkup;

	@SerializedName("dbas")
	private List<BankAccountInfo> depositBankAccounts;

	@JwtExclude
	public AadhaarInfo aadhaarInfo;

	@JwtExclude
	public LicenseInfo licenseInfo;

	@JwtExclude
	public VoterInfo voterInfo;

	@JwtExclude
	public List<PartnerPanInfo> partnerPanInfo;

	@JwtExclude
	public CinInfo cinInfo;

	@JwtExclude
	public PassportInfo passportInfo;

	@SerializedName("ps")
	public Object policies;

	@SerializedName("eids")
	public String ccEmailIds;

	@SerializedName("iao")
	public Boolean isAutoOnBoarding;

	@SerializedName("ips")
	public Boolean isPushedToAccounting;

	@Override
	public void cleanData() {
		groups = TgsCollectionUtils.getCleanStringList(groups);
		if (StringUtils.isBlank(accountingCode))
			accountingCode = null;
		setDeptInfo(TgsMapUtils.getCleanMap(deptInfo));
	}

	public boolean containsGroup(String groupName) {
		if (CollectionUtils.isNotEmpty(groups)) {
			return groups.contains(groupName);
		}
		return false;
	}
}
