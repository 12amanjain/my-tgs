package com.tgs.services.ums.datamodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserApiConfiguration {

	@SerializedName("apikey")
	private String apiKey;

	@SerializedName("isnkr")
	private Boolean isNewKeyRequired;

	@SerializedName("av")
	private Integer airVersion;
	
	@SerializedName("hv")
	private Integer hotelVersion;
	
	@SerializedName("alips")
	private List<String> whitelistedIps;
	
	
}
