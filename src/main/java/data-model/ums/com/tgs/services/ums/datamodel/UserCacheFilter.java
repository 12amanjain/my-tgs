package com.tgs.services.ums.datamodel;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class UserCacheFilter {
	private String role;
	private String name;
	private String email;
	private String mobile;
	private String status;
	private String apiKey;
	private List<String> userIds;
	private Boolean fetchUserDues;
}
