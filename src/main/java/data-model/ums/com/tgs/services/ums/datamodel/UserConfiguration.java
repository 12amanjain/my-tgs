package com.tgs.services.ums.datamodel;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.helper.JwtExclude;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.CommercialPlanId;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserConfiguration {
	@SerializedName("sic")
	private List<Integer> saleInCharge;
	@UserId(creditLine = true, userRelation = true)
	@SerializedName("dId")
	@SearchPredicate(type = PredicateType.EQUAL, destinationEntity = "User", dbAttribute = "parentConf.dId", filterName = "distributorId")
	private String distributorId;
	@SerializedName("md")
	private LocalDateTime migrationDate;
	@SerializedName("pconf")
	private UserPaymentConfiguration paymentConf;
	@SerializedName("apf")
	private Boolean allowPrivateFare;
	@SerializedName("isDI")
	private Boolean isDIAllowed;

	@SerializedName("cpolicy")
	private Map<Product, String> creditPolicyMap;

	private Map<String, Integer> commPlanMap;

	@JwtExclude
	@SerializedName("apiconf")
	private UserApiConfiguration apiConfiguration;

	@SerializedName("paxinv")
	private Boolean paxInvoice;

	public Integer getPlanId(String key) {
		if (commPlanMap == null) {
			return null;
		}
		return commPlanMap.getOrDefault(key, null);
	}

	@CommercialPlanId
	public Collection<Integer> getPlanIds() {
		if (commPlanMap == null) {
			return Collections.EMPTY_SET;
		}
		return commPlanMap.values();
	}

	public void cleanData() {
		if (TgsCollectionUtils.isEmpty(saleInCharge))
			saleInCharge = null;
		if (StringUtils.isBlank(distributorId))
			distributorId = null;
		if (TgsMapUtils.isEmpty(commPlanMap))
			commPlanMap = null;
	}

	public Map<Product, String> getCreditPolicyMap() {
		if (creditPolicyMap == null)
			creditPolicyMap = new HashMap<>();
		return creditPolicyMap;
	}


}
