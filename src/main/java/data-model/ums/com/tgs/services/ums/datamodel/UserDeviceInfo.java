package com.tgs.services.ums.datamodel;

import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.datamodel.DeviceInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserDeviceInfo extends DataModel {

	@SearchPredicate(type = PredicateType.IN)
	private String userId;

	private DeviceInfo deviceInfo;
}
