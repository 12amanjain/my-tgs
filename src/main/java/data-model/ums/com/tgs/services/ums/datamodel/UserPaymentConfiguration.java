package com.tgs.services.ums.datamodel;

import java.util.List;

import lombok.Data;

@Data
public class UserPaymentConfiguration {

	private Boolean isPaymentFeeExempted;
	private List<String> enabledPaymentMode;
}
