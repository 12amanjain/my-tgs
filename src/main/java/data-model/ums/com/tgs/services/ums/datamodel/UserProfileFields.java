package com.tgs.services.ums.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Getter
@Setter
public class UserProfileFields {

    @SerializedName("corporateOnboardData")
    private List<FieldMetaInfo> fields;

    private Map<String, FieldMetaInfo> fieldMap;

    public void InitMap() {
        if (CollectionUtils.isNotEmpty(fields)) {
            fieldMap = fields.stream().collect(Collectors.toMap(FieldMetaInfo::getName, x -> x));
        }
    }
}
