package com.tgs.services.ums.datamodel.bulkUpload;

import java.util.Map;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserConfiguration;

public class CommissionPlanUpdateQuery extends UserUpdateQuery{

	Map<String, Integer> commPlanMap;
	
	public User getUserFromRequest() {		
		return User.builder().userId(getUserId())
				.userConf(UserConfiguration.builder().commPlanMap(commPlanMap).build())
				.build();
	}
}
