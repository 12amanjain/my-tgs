package com.tgs.services.ums.datamodel.bulkUpload;

import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class UserUpdateQuery extends BulkUploadQuery {

	private String userId;

	public abstract User getUserFromRequest();
	
	
}
