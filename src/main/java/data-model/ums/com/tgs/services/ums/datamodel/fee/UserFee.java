package com.tgs.services.ums.datamodel.fee;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.ruleengine.UserAirFeeRuleCriteria;
import com.tgs.services.base.ruleengine.UserFeeRuleCriteria;
import com.tgs.services.base.ruleengine.UserHotelFeeRuleCriteria;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder(toBuilder = true)
public class UserFee implements Validatable, IRule {

	private Long id;
	private LocalDateTime createdOn;
	private LocalDateTime processedOn;

	@UserId
	private String userId;

	@NotNull
	private Product product;

	@NotNull
	private UserFeeType feeType;

	@NotNull
	private Boolean enabled;

	private Boolean isDeleted;

	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = UserAirFeeRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = UserHotelFeeRuleCriteria.class)})
	private UserFeeRuleCriteria inclusionCriteria;

	@NotNull
	private UserFeeAmount userFee;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (product == null) {
			errors.put("product", SystemError.INVALID_PRODUCT.getErrorDetail());
		}
		if (feeType == null) {
			errors.put("feeType", SystemError.INVALID_USERFEE_TYPE.getErrorDetail());
		}
		if (userFee == null) {
			errors.put("userFee", SystemError.INVALID_USERFEE.getErrorDetail());
		} else {
			errors.putAll(userFee.validate(validatingData).withPrefixToFieldNames("userFee."));
		}

		return errors;
	}

	@Override
	public IRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public IRuleCriteria getExclusionCriteria() {
		return null;
	}

	@Override
	public IRuleOutPut getOutput() {
		return userFee;
	}

	@Override
	public double getPriority() {
		return 0.0;
	}

	@Override
	public boolean exitOnMatch() {
		return false;
	}

	@Override
	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	@ApiModelProperty(hidden = true)
	public static UserFee getFareTypeBasedUserFee(List<UserFee> userFees, Product product, String fareIdentifier) {
		List<UserFee> matchFees = new ArrayList<>();
		for (Iterator<UserFee> iter = userFees.iterator(); iter.hasNext();) {
			UserFee userFee = iter.next();
			UserAirFeeRuleCriteria ruleCriteria = (UserAirFeeRuleCriteria) userFee.getInclusionCriteria();
			if (Product.AIR.equals(product)) {
				if (ruleCriteria != null && (CollectionUtils.isNotEmpty(ruleCriteria.getFareTypeList())
						&& ruleCriteria.getFareTypeList().contains(fareIdentifier))) {
					matchFees.add(userFee);
				}
			}
		}
		return CollectionUtils.isNotEmpty(matchFees) ? matchFees.get(0) : null;
	}

	@ApiModelProperty(hidden = true)
	public static UserFee getUserFee(List<UserFee> userFees, Product product) {
		List<UserFee> matchFees = new ArrayList<>();
		for (Iterator<UserFee> iter = userFees.iterator(); iter.hasNext();) {
			UserFee userFee = iter.next();
			UserAirFeeRuleCriteria ruleCriteria = (UserAirFeeRuleCriteria) userFee.getInclusionCriteria();
			if (Product.AIR.equals(product)) {
				if (ruleCriteria != null && CollectionUtils.isEmpty(ruleCriteria.getFareTypeList())) {
					matchFees.add(userFee);
				}
			}
		}
		return CollectionUtils.isNotEmpty(matchFees) ? matchFees.get(0) : null;
	}


}
