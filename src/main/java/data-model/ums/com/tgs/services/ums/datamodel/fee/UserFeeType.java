package com.tgs.services.ums.datamodel.fee;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.FareComponent;
import lombok.Getter;

@Getter
public enum UserFeeType {
	MARKUP("M", FareComponent.MU),
	CLIENTMARKUP("CM", FareComponent.CMU) {
		@Override
		public boolean updateTotalFare() {
			return true;
		}

		@Override
		public List<AirFlowType> allowedFlowTypes() {
			List<AirFlowType> allowedFlowTypes = new ArrayList<>();
			allowedFlowTypes.add(AirFlowType.SEARCH);
			allowedFlowTypes.add(AirFlowType.REVIEW);
			return allowedFlowTypes;
		}
	},
	PARTNER_MARKUP("PM", FareComponent.PMU) {

		@Override
		public boolean updateTotalFare() {
			return true;
		}

		@Override
		public List<AirFlowType> allowedFlowTypes() {
			// this component is not allowed to update from any of screens
			return new ArrayList<>();
		}
	},
	PARTNER_COMMISSION("PC", null) {

		@Override
		public List<AirFlowType> allowedFlowTypes() {
			// this component is not allowed to update from any of screens
			return new ArrayList<>();
		}
	};

	private String code;

	private FareComponent targetComponent;

	private UserFeeType(String code, FareComponent targetComponent) {
		this.code = code;
		this.targetComponent = targetComponent;
	}

	public static UserFeeType getEnumFromCode(String code) {
		return getUserFeeType(code);

	}

	public static UserFeeType getUserFeeType(String code) {
		for (UserFeeType status : UserFeeType.values()) {
			if (status.getCode().equals(code)) {
				return status;
			}
		}
		throw new IllegalArgumentException("No enum constant for code " + code);
	}

	public boolean updateTotalFare() {
		return false;
	}

	public List<AirFlowType> allowedFlowTypes() {
		List<AirFlowType> allowedFlowTypes = new ArrayList<>();
		allowedFlowTypes.add(AirFlowType.SEARCH);
		allowedFlowTypes.add(AirFlowType.REVIEW);
		allowedFlowTypes.add(AirFlowType.BOOKING_DETAIL);
		return allowedFlowTypes;
	}

}
