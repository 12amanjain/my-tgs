package com.tgs.services.voucher.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;


@Getter
public class AirVoucherRuleCriteriaOutput extends VoucherRuleCriteriaOutput {

	@SerializedName("aoi")
	private Boolean isApplicableOnInfant;

	@SerializedName("aoas")
	private Boolean isApplicableOnAllSegment;

	@SerializedName("aob")
	private Boolean isApplicableOnBooking;

}
