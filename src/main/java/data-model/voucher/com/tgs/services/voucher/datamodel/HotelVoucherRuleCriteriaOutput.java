package com.tgs.services.voucher.datamodel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;


@Getter
public class HotelVoucherRuleCriteriaOutput extends VoucherRuleCriteriaOutput {

	@SerializedName("aob")
	private Boolean isApplicableOnBooking;

}
