package com.tgs.services.voucher.datamodel;

import java.time.LocalDateTime;
import java.util.List;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.PredicateType;
import com.tgs.services.base.annotations.SearchPredicate;
import com.tgs.services.base.datamodel.DataModel;
import com.tgs.services.base.gson.UpperCaseTypeAdaptor;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class VoucherConfiguration extends DataModel {


	@ApiModelProperty(notes = "Already created Unique ID for Voucher Rule,if need to update id is mandatory",
			example = "10")
	@SearchPredicate(type = PredicateType.IN)
	@SearchPredicate(type = PredicateType.EQUAL)
	private Integer id;

	@ApiModelProperty(notes = "Alphanumeric voucher/promo code", example = "NEW500")
	@SearchPredicate(type = PredicateType.IN)
	@JsonAdapter(UpperCaseTypeAdaptor.class)
	private String voucherCode;

	@ApiModelProperty(notes = "To denote the description of the rule",
			example = "Voucher applicable on Domestic bookings")
	private String description;

	@ApiModelProperty(notes = "To denote rule is enabled or disabled", example = "TRUE")
	private Boolean enabled;

	@ApiModelProperty(notes = "To denote rule is deleted", example = "TRUE")
	@SearchPredicate(type = PredicateType.EQUAL)
	private Boolean isDeleted;

	@ApiModelProperty(notes = "To denote the priority the rule", example = "1")
	@SearchPredicate(type = PredicateType.EQUAL)
	private Double priority;

	@ApiModelProperty(notes = "List of all voucher rules to which this voucher code will be applied ")
	private List<VoucherConfigurationRule> voucherRules;

	@ApiModelProperty(notes = "Creation Date of Voucher Rule", example = "2020-09-15")
	private LocalDateTime createdOn;

	@ApiModelProperty(notes = "Expiry Date of Voucher Rule", example = "2020-09-15")
	private LocalDateTime expiry;

	@ApiModelProperty(notes = "ProcessedOn Date of Voucher Rule", example = "2020-09-15")
	private LocalDateTime processedOn;

	public double getPriority() {
		return priority != null ? priority.doubleValue() : 0.0;
	}

	public boolean getEnabled() {
		return enabled == null ? false : enabled;
	}

	public Boolean getIsDeleted() {
		return isDeleted == null ? false : isDeleted;
	}

}

