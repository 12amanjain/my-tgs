package com.tgs.services.voucher.datamodel;

import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.base.ruleengine.GeneralBasicRuleCriteria;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;


@Setter
@Getter
public class VoucherConfigurationRule implements IRule {

	@ApiModelProperty(
			notes = "To denote the Product Type, Will be NA for General Criterias, applicable to all products",
			example = "AIR")
	private Product product;

	@ApiModelProperty(notes = "To denote the priority the rule", example = "1")
	private Double priority;

	@ApiModelProperty(notes = "denote the quantity consumed of this voucher", example = "1")
	private Integer quantityConsumed;

	@ApiModelProperty(notes = "denote the inclusion criteria of rule", example = "")
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = FlightBasicRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelBasicRuleCriteria.class)})
	private GeneralBasicRuleCriteria inclusionCriteria;

	@ApiModelProperty(notes = "denote the exclusion criteria of rule", example = "")
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = FlightBasicRuleCriteria.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelBasicRuleCriteria.class)})
	private GeneralBasicRuleCriteria exclusionCriteria;

	@ApiModelProperty(notes = "denote the voucher criteria of rule", example = "")
	@GsonRunTimeAdaptorRequired(dependOn = "product")
	@GsonPolymorphismMapping({@ClassType(keys = {"A", "AIR"}, value = AirVoucherRuleCriteriaOutput.class),
			@ClassType(keys = {"H", "HOTEL"}, value = HotelVoucherRuleCriteriaOutput.class)})
	private VoucherRuleCriteriaOutput voucherCriteria;

	private Boolean canBeConsideredExclusively;

	@NotNull
	@SerializedName("qty")
	// Fixed total quantity
	private Integer totalQuantity;

	@Override
	public double getPriority() {
		return priority != null ? priority.doubleValue() : 0.0;
	}

	@Override
	public GeneralBasicRuleCriteria getInclusionCriteria() {
		return inclusionCriteria;
	}

	@Override
	public GeneralBasicRuleCriteria getExclusionCriteria() {
		return exclusionCriteria;
	}

	@Override
	public VoucherRuleCriteriaOutput getOutput() {
		return getVoucherCriteria();
	}

	public VoucherRuleCriteriaOutput getVoucherCriteria() {
		if (voucherCriteria == null) {
			voucherCriteria = new VoucherRuleCriteriaOutput();
		}
		return voucherCriteria;
	}

	@Override
	public boolean canBeConsideredExclusively() {
		return canBeConsideredExclusively;
	}

	public Integer getQuantityConsumed() {
		if (quantityConsumed != null)
			return quantityConsumed;
		return 0;
	}

	public Integer getTotalQuantity() {
		return totalQuantity != null ? totalQuantity : 0;
	}

	public Integer getRemainingQty() {
		return getTotalQuantity() - getQuantityConsumed();
	}
}
