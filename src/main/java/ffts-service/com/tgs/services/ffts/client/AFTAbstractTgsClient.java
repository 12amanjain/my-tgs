package com.tgs.services.ffts.client;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.common.HttpUtils;

public abstract class AFTAbstractTgsClient {

	@Value("${domain}")
	private String domain;

	@Value("${commProtocol}")
	private String protocol;

	private static String serverUrl;

	final protected String getServerUrl() {
		if (serverUrl == null) {
			serverUrl = new StringBuilder(protocol).append("://").append(domain).toString();
		}
		return serverUrl;
	}

	final protected <V> V doPost(String url, Object postData, Class<? extends V> responseType) throws IOException {
		return doPost(url, postData, responseType, null);
	}

	final protected <V> V doPost(String url, Object postData, Class<? extends V> responseType,
			Map<String, String> headerParams) throws IOException {
		if (headerParams == null) {
			headerParams = new HashMap<>();
		}
		headerParams.put("Content-Type", "application/json");
		String strPostData = GsonUtils.getGson().toJson(postData);
		HttpUtils httpUtils =
				HttpUtils.builder().urlString(url).headerParams(headerParams).postData(strPostData).build();
		return doRequest(httpUtils, responseType);
	}

	private <V> V doRequest(HttpUtils httpUtils, Class<? extends V> responseType) throws IOException {
		populateHeaderParams(httpUtils.getHeaderParams());
		return httpUtils.getResponse(responseType).get();
	}

	private void populateHeaderParams(Map<String, String> headerParams) {
		headerParams.put("deviceid", "System");
	}
}
