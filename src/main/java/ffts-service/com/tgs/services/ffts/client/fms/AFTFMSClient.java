package com.tgs.services.ffts.client.fms;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.ffts.client.AFTAbstractTgsClient;

@Service
public class AFTFMSClient extends AFTAbstractTgsClient {

	final private static String RELATIVE_FMS_PATH = "/fms/v1";

	private static String flightServiceUrl;

	/**
	 * Mapping of relative paths to absolute paths
	 */
	private static Map<FMSService, String> urlMapping;

	static {
		urlMapping = new HashMap<>();
	}

	private String getFlightServiceUrl() {
		if (flightServiceUrl == null) {
			String serverUrl = getServerUrl();
			flightServiceUrl = new StringBuilder(serverUrl).append(RELATIVE_FMS_PATH).toString();
		}
		return flightServiceUrl;
	}

	private String getAbsolutePath(FMSService service) {
		String absolutePath = urlMapping.get(service);
		if (absolutePath == null) {
			absolutePath = getFlightServiceUrl() + service.relativePath;
			urlMapping.put(service, absolutePath);
		}
		return absolutePath;
	}

	final public <V> V doPostForFms(FMSService fmsService, Object postData, Class<? extends V> responseType, String jwt)
			throws IOException {
		String url = getAbsolutePath(fmsService);
		Map<String, String> headerParams = new HashMap<>();
		String jwtTokenHeader = SecurityConstants.TOKEN_PREFIX + jwt;
		headerParams.put(SecurityConstants.HEADER_STRING, jwtTokenHeader);
		return doPost(url, postData, responseType, headerParams);
	}

	public static enum FMSService {
		SEARCHQUERY_LIST("/air-searchquery-list"), SEARCH("/air-search");

		private String relativePath;

		private FMSService(String relativePath) {
			this.relativePath = relativePath;
		}
	}
}
