package com.tgs.services.ffts.datamodel.analytics;

import com.tgs.services.base.BaseAnalyticsQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Setter
@Getter
@SuperBuilder
public class AnalyticsAirFareTracker extends BaseAnalyticsQuery {

	private String pnr;
	private String supplier;

	// comma-separated
	private String searchsuppliers;

	private String toemailid;

	private String bccemailid;

	private String fromemailid;

	private Boolean successstatus;

	private Boolean notified;

	private Boolean faredropped;

	private Double faredrop;

	private String comment;

	// comma-separated
	private String airlines;

	private Double totalfare;

	private String importtime;

	// departure time
	private String deptime;

	// minutes from import
	private Long mfi;

	// minutes to departure
	private Long mtd;
}
