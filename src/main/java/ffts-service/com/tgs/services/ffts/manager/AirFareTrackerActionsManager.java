package com.tgs.services.ffts.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.ffts.datamodel.ActionResult;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.ffts.datamodel.notification.AirFareUpdateEmailData;
import com.tgs.services.ffts.datamodel.notification.AirFareUpdateMessageAttributes;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
class AirFareTrackerActionsManager {

	private final static double DEFAULT_ACTION_THRESHOLD = 500d;

	@Autowired
	private MsgServiceCommunicator msgServiceCommunicator;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	/**
	 * 
	 * @param airRetrievedBookingInfo
	 * @param tripInfos
	 * @param emailData
	 * @return {@code true} if mail was sent
	 */
	ActionResult sendMail(AirRetrievedBookingInfo airRetrievedBookingInfo, Map<String, List<TripInfo>> tripInfos) {
		boolean notified = false;
		FareTrackingInfo fareTrackingInfo = null;
		Double fareDrop = null;
		try {
			fareTrackingInfo = getFareTrackingInfo(airRetrievedBookingInfo, tripInfos);
			fareDrop = fareTrackingInfo.getFareDifference().doubleValue();
			log.debug("Fare tracking info {}", fareTrackingInfo);
			double threshold = ObjectUtils.firstNonNull(
					airRetrievedBookingInfo.getAirFareTrackerRequest().getActionThreshold(), DEFAULT_ACTION_THRESHOLD);
			if (fareDrop >= threshold) {
				msgServiceCommunicator.sendMail(getAirMessageAttributesSupplier(fareTrackingInfo,
						airRetrievedBookingInfo.getAirFareTrackerRequest().getEmailData()));
				notified = true;
			}
		} catch (Exception e) {
			log.error("Failed to send mail for {} due to ", airRetrievedBookingInfo, e);
		}
		return ActionResult.builder().actionPerformed(notified).notified(notified).fareDrop(fareDrop).build();
	}


	private FareTrackingInfo getFareTrackingInfo(AirRetrievedBookingInfo airRetrievedBooking,
			Map<String, List<TripInfo>> tripInfos) {
		List<TripInfo> allTrips = new ArrayList<>();
		tripInfos.values().forEach(trips -> allTrips.addAll(trips));
		double newFare = 0.0;
		for (TripInfo trip : allTrips) {
			newFare += getTotalFare(trip.getPaxInfo(), trip.getTripPriceInfos().get(0), FareComponent.TF);
		}

		double oldFare = 0.0;
		for (TripInfo trip : airRetrievedBooking.getAirImportPnrBooking().getTripInfos()) {
			oldFare += fmsCommunicator.getTotalFareComponentAmount(trip, FareComponent.TF);
		}

		BigDecimal newFareBD = BigDecimal.valueOf(newFare).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		BigDecimal oldFareBD = BigDecimal.valueOf(oldFare).setScale(2, BigDecimal.ROUND_HALF_EVEN);
		BigDecimal fareDiffBD = oldFareBD.subtract(newFareBD);
		return FareTrackingInfo.builder().newFare(newFareBD).oldFare(oldFareBD).fareDifference(fareDiffBD)
				.pnr(airRetrievedBooking.getAirImportPnrBooking().getPnr()).build();
	}

	/**
	 * {@code FMSCommunicator#getTotalFareComponentAmount} is giving zero for retrieved trips.
	 * 
	 * @param paxTypeMap
	 * @param priceInfo
	 * @param targetComponent
	 * @return
	 */
	private double getTotalFare(Map<PaxType, Integer> paxTypeMap, PriceInfo priceInfo, FareComponent targetComponent) {
		double totalPrice = 0;
		for (PaxType paxType : paxTypeMap.keySet()) {
			if (priceInfo.getFareDetail(paxType) != null) {
				totalPrice += priceInfo.getFareDetail(paxType).getFareComponents().getOrDefault(targetComponent,
						Objects.nonNull(priceInfo.getFareDetail(paxType).getAddlFareComponents()) ? priceInfo
								.getFareDetail(paxType).getAddlFareComponents()
								.getOrDefault(FareComponent.TAF, new HashMap<>()).getOrDefault(targetComponent, 0.0)
								: 0.0)
						* paxTypeMap.get(paxType);
			}
		}
		return totalPrice;
	}

	private AbstractMessageSupplier<AirFareUpdateMessageAttributes> getAirMessageAttributesSupplier(
			FareTrackingInfo fareTrackingInfo, AirFareUpdateEmailData emailData) {
		return new AbstractMessageSupplier<AirFareUpdateMessageAttributes>() {
			@Override
			public AirFareUpdateMessageAttributes get() {
				AirFareUpdateMessageAttributes messageAttributes = AirFareUpdateMessageAttributes.builder().build();
				messageAttributes.setKey(EmailTemplateKey.AIR_FARE_UPDATE_EMAIL.name());
				messageAttributes.setToEmailId(emailData.getToEmailId());
				messageAttributes.setBccEmailId(emailData.getBccEmailId());
				messageAttributes.setFromEmail(emailData.getFromEmailId());
				messageAttributes.setNewFare(TgsStringUtils.formatCurrency(fareTrackingInfo.getNewFare()));
				messageAttributes.setOldFare(TgsStringUtils.formatCurrency(fareTrackingInfo.getOldFare()));
				messageAttributes.setDifference(TgsStringUtils.formatCurrency(fareTrackingInfo.getFareDifference()));
				messageAttributes.setPnr(fareTrackingInfo.getPnr());
				return messageAttributes;
			}
		};
	}

	@Getter
	@Setter
	@Builder
	@ToString
	private static class FareTrackingInfo {
		private BigDecimal newFare;
		private BigDecimal oldFare;
		private BigDecimal fareDifference;
		private String pnr;
	}

}
