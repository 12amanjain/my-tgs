package com.tgs.services.ffts.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.fms.restmodel.AirSearchResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.JWTHelper;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
class AirFareTrackerEngine {

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private AirFareTrackerSearchManager fmsSearchManager;

	@Autowired
	private UserServiceCommunicator userService;


	/**
	 * Return filtered flight - one tripInfo per tripInfoType - with new (cheapest) fares.
	 * 
	 * @param airRetrievedBookingInfo
	 * @return
	 * @throws Exception
	 */
	Map<String, List<TripInfo>> findNewFares(AirRetrievedBookingInfo airRetrievedBookingInfo) throws Exception {
		AirImportPnrBooking airImportPnrBooking = airRetrievedBookingInfo.getAirImportPnrBooking();
		AirSearchQuery airSearchQuery = createSearchQuery(airImportPnrBooking,
				airRetrievedBookingInfo.getAirFareTrackerRequest().getSearchSupplierIds());
		log.info("Search Query created for {}: \n{}", airImportPnrBooking, airSearchQuery);
		Map<String, List<TripInfo>> tripInfos =
				searchFlights(airSearchQuery, airRetrievedBookingInfo.getAirFareTrackerRequest());
		return filterResults(tripInfos, airImportPnrBooking.getTripInfos());
	}

	private AirSearchQuery createSearchQuery(AirImportPnrBooking airImportPnrBooking, List<String> supplierIds) {
		AirSearchQuery airSearchQuery = fmsCommunicator.getSearchQueryFromTripInfos(airImportPnrBooking.getTripInfos());

		/**
		 * Temporary fix
		 */
		airSearchQuery.setSourceIds(null);

		airSearchQuery.setSupplierIds(supplierIds);
		return airSearchQuery;
	}

	private Map<String, List<TripInfo>> searchFlights(AirSearchQuery airSearchQuery,
			AirFareTrackerRequest airFareTrackerRequest) throws Exception {
		AirSearchRequest airSearchRequest = new AirSearchRequest();
		airSearchRequest.setSearchQuery(airSearchQuery);
		String loggedinUserId = airFareTrackerRequest.getLoggedinUser();
		User loggedinUser = loggedInUser(loggedinUserId);
		String jwt = null;
		try {
			jwt = JWTHelper.generateAndStoreAccessToken(loggedinUser);
		} catch (Exception e) {
			log.error("Failed to generate jwt for {} due to ", loggedinUser, e);
			throw new CustomGeneralException("Failed to generate jwt");
		}
		AirSearchResponse airSearchResponse = fmsSearchManager.searchFlights(airSearchRequest, jwt);
		Map<String, List<TripInfo>> tripInfos = airSearchResponse.getSearchResult().getTripInfos();
		return tripInfos;
	}

	private User loggedInUser(String loggedinUserId) {
		User loggedinUser = null;
		if (StringUtils.isNotBlank(loggedinUserId)) {
			loggedinUser = userService.getUserFromCache(loggedinUserId);
		}
		/**
		 * if loggedinUserId is invalid/empty, see if some user is actually logged in
		 */
		if (loggedinUser == null) {
			loggedinUser = SystemContextHolder.getContextData().getUser();
		}
		/**
		 * otherwise, try to find a valid user to use search service
		 */
		if (loggedinUser == null) {
			loggedinUser = tryToFindValidUser();
		}
		return loggedinUser;
	}

	/**
	 * To be removed
	 * 
	 * @return
	 */
	private User tryToFindValidUser() {
		User user = userService.getUserFromCache("5402");
		if (user == null) {
			user = userService.getUserFromCache("91");
		}
		if (user == null) {
			user = userService.getUserFromCache("2000282");
		}
		return user;
	}

	private Map<String, List<TripInfo>> filterResults(Map<String, List<TripInfo>> tripInfos,
			List<TripInfo> bookedTrips) {
		// TODO: handle all cases
		Map<String, List<TripInfo>> filteredTripInfos = filterTripsByFlight(tripInfos, bookedTrips);

		Map<PaxType, Integer> paxInfo = BaseUtils.getPaxInfo(bookedTrips.get(0));
		setPaxInfos(filteredTripInfos, paxInfo);
		CabinClass cabinClass = bookedTrips.get(0).getCabinClass();
		Map<String, List<TripInfo>> cheapestFilteredTripInfos =
				filterAndFindCheapestPrice(filteredTripInfos, cabinClass);
		return cheapestFilteredTripInfos;
	}

	private void setPaxInfos(Map<String, List<TripInfo>> tripInfos, Map<PaxType, Integer> paxInfo) {
		for (List<TripInfo> trips : tripInfos.values()) {
			for (TripInfo trip : trips) {
				trip.setPaxInfo(paxInfo);
			}
		}
	}

	/**
	 * 
	 * @param tripInfos
	 * @param bookedTrips could be split route-wise
	 * @return
	 */
	private Map<String, List<TripInfo>> filterTripsByFlight(Map<String, List<TripInfo>> tripInfos,
			List<TripInfo> bookedTrips) {
		Map<String, List<TripInfo>> filteredTripInfos = new HashMap<>();
		tripInfos.keySet().forEach(key -> {
			filteredTripInfos.put(key, new ArrayList<TripInfo>());
		});

		String bookedTripsKey = getKeyForTripsCombined(bookedTrips);
		if (tripInfos.keySet().size() == 2) {
			for (TripInfo onwardTrip : tripInfos.get(TripInfoType.ONWARD.getTripType())) {
				for (TripInfo returnTrip : tripInfos.get(TripInfoType.RETURN.getTripType())) {
					String tripsKey = getKeyForTripsCombined(Lists.newArrayList(onwardTrip, returnTrip));
					if (bookedTripsKey.equals(tripsKey)) {
						filteredTripInfos.get(TripInfoType.ONWARD.getTripType()).add(onwardTrip);
						filteredTripInfos.get(TripInfoType.RETURN.getTripType()).add(returnTrip);
					}
				}
			}
		} else {
			String tripInfoType = tripInfos.keySet().iterator().next();
			for (TripInfo trip : tripInfos.get(tripInfoType)) {
				if (bookedTripsKey.equals(getKey(trip))) {
					filteredTripInfos.get(tripInfoType).add(trip);
				}
			}
		}

		return filteredTripInfos;
	}

	private String getKeyForTripsCombined(List<TripInfo> trips) {
		if (trips == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		for (TripInfo trip : trips) {
			sb.append(getKey(trip));
		}
		return sb.toString();
	}

	private String getKey(TripInfo tripInfo) {
		final StringBuilder sb = new StringBuilder();
		tripInfo.getSegmentInfos()
				.forEach(segment -> sb.append(segment.getAirlineCode(true)).append(segment.getFlightNumber()));
		return sb.toString();
	}

	private void filterPriceInfo(TripInfo tripInfo, CabinClass cabinClass) {
		int priceIndex =
				indexOfPriceInfoWithCabinClass(tripInfo.getSegmentInfos().get(0).getPriceInfoList(), cabinClass);

		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			List<PriceInfo> priceInfos = segmentInfo.getPriceInfoList();
			segmentInfo.setPriceInfoList(Lists.newArrayList(priceInfos.get(priceIndex)));
		}
	}

	private int indexOfPriceInfoWithCabinClass(List<PriceInfo> priceInfos, CabinClass cabinClass) {
		int priceIndex = -1;
		for (int i = 0; i < priceInfos.size(); i++) {
			if (cabinClass.equals(priceInfos.get(i).getCabinClass(PaxType.ADULT))) {
				priceIndex = i;
			}
		}
		return priceIndex;
	}

	private Map<String, List<TripInfo>> filterAndFindCheapestPrice(Map<String, List<TripInfo>> tripInfos,
			CabinClass cabinClass) {
		Map<String, List<TripInfo>> cheapestFilteredTripInfos = new HashMap<>();
		for (Map.Entry<String, List<TripInfo>> tripEntry : tripInfos.entrySet()) {
			List<TripInfo> filteredTrips = tripEntry.getValue();
			double cheapestFare = 0;
			TripInfo cheapestTrip = null;
			for (TripInfo trip : filteredTrips) {
				filterPriceInfo(trip, cabinClass);
				double tripFare =
						getTotalFare(BaseUtils.getPaxInfo(trip), trip.getTripPriceInfos().get(0), FareComponent.TF);
				if (cheapestFare == 0 || tripFare < cheapestFare) {
					cheapestFare = tripFare;
					cheapestTrip = trip;
				}
			}
			if (cheapestTrip != null) {
				cheapestFilteredTripInfos.put(tripEntry.getKey(), Lists.newArrayList(cheapestTrip));
			}
		}
		return cheapestFilteredTripInfos;
	}

	private double getTotalFare(Map<PaxType, Integer> paxTypeMap, PriceInfo priceInfo, FareComponent targetComponent) {
		double totalPrice = 0;
		for (PaxType paxType : paxTypeMap.keySet()) {
			if (priceInfo.getFareDetail(paxType) != null) {
				totalPrice += priceInfo.getFareDetail(paxType).getFareComponents().getOrDefault(targetComponent,
						Objects.nonNull(priceInfo.getFareDetail(paxType).getAddlFareComponents()) ? priceInfo
								.getFareDetail(paxType).getAddlFareComponents()
								.getOrDefault(FareComponent.TAF, new HashMap<>()).getOrDefault(targetComponent, 0.0)
								: 0.0)
						* paxTypeMap.get(paxType);
			}
		}
		return totalPrice;
	}
}
