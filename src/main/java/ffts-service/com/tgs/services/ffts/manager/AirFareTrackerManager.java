package com.tgs.services.ffts.manager;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.function.Function;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.ffts.datamodel.ActionResult;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.ffts.manager.analytics.AirFareTrackerAnalyticsManager;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

/**
 * AirFareTrackerEngine + Request Manager + Import PNR Manager + Actions Manager
 * 
 * @author Abhineet Kumar
 *
 */

@Slf4j
@Service
public class AirFareTrackerManager {

	private final static int POOL_SIZE = 100, MAX_TASKS_COUNT_FOR_SEARCH = 200,
			/**
			 * Do not send more than BATCH_SIZE_FOR_SEARCH search requests to thread-pool.
			 */
			BATCH_SIZE_FOR_SEARCH = 20,
			/**
			 * 30 seconds threshold
			 */
			POOL_EXECUTION_TIME_THRESHOLD = 30 * 1000, MAX_TASKS_COUNT_FOR_IMPORT_PNR = 250,
			/**
			 * Do not send more than BATCH_SIZE_FOR_IMPORT_PNR requests to thread-pool.
			 */
			BATCH_SIZE_FOR_IMPORT_PNR = 25, CONSECUTIVE_UNHEALTHY_EXECUTIONS_THRESHOLD = 2;

	private static ThreadPoolExecutor poolExecutor =
			(ThreadPoolExecutor) ExecutorUtils.getFareTrackerThreadPool(POOL_SIZE, 1000);

	@Autowired
	private AirFareTrackerRequestManager airFareTrackerRequestManager;

	@Autowired
	private AirImportPnrManager airImportPnrManager;

	@Autowired
	private AirFareTrackerEngine airFareTrackerEngine;

	@Autowired
	private AirFareTrackerActionsManager actionsManager;

	@Autowired
	private AirFareTrackerAnalyticsManager airFareTrackerAnalyticsManager;

	public Map<String, List<TripInfo>> findNewFares(AirFareTrackerRequest airFareTrackerRequest) throws Exception {
		AirImportPnrBooking airImportPnrBooking = airImportPnrManager.retrieveBooking(airFareTrackerRequest);
		AirRetrievedBookingInfo retrievedBookingInfo = AirRetrievedBookingInfo.builder()
				.airImportPnrBooking(airImportPnrBooking).airFareTrackerRequest(airFareTrackerRequest).build();
		return airFareTrackerEngine.findNewFares(retrievedBookingInfo);
	}

	public void addRequestsToQueue(List<AirFareTrackerRequest> fareTrackerRequests) {
		if (CollectionUtils.isNotEmpty(fareTrackerRequests)) {
			for (AirFareTrackerRequest airFareTrackerRequest : fareTrackerRequests) {
				if (StringUtils.isNotEmpty(airFareTrackerRequest.getPnr())) {
					try {
						airFareTrackerRequestManager.addRequestToQueue(airFareTrackerRequest);
					} catch (Exception e) {
						log.error("Failed to store AirFareTrackerRequest {} to the queue due to ",
								airFareTrackerRequest, e);
						airFareTrackerAnalyticsManager.pushRequestAnalytics(fareTrackerRequests, false);
						throw new CustomGeneralException(SystemError.STORE_FARE_TRACKER_REQUEST_FAILURE);
					}
				}
			}
		}
	}

	public void retrieveBookings() {
		log.info("Started Retrieving booking from queue");
		final Map<String, AirFareTrackerRequest> pnrRequestMap = new HashMap<>();
		int consecutiveUnhealthyExecutions = 0;

		String firstKey = null;
		for (int count = 0; count < MAX_TASKS_COUNT_FOR_IMPORT_PNR; count++) {
			AirFareTrackerRequest airFareTrackerRequest = null;
			try {
				airFareTrackerRequest = airFareTrackerRequestManager.popRequest();
				log.debug("Fetched data from queue {}", airFareTrackerRequest);

				if (airFareTrackerRequest == null) {
					/**
					 * Request Queue is apparently empty. Wait for next cycle.
					 */
					break;
				}

				String requestKey = getKey(airFareTrackerRequest);
				if (requestKey.equals(firstKey)) {
					/**
					 * End of cycle. Wait for next cycle.
					 */
					airFareTrackerRequestManager.addRequestToQueue(airFareTrackerRequest);
					break;
				}

				String pnr = airFareTrackerRequest.getPnr();
				if (airImportPnrManager.wasRequestCompleted(pnr)) {
					count--;
					airImportPnrManager.storeRequestData(airFareTrackerRequest);
					continue;
				}

				/**
				 * Store request for backup
				 */
				airFareTrackerRequestManager.addRequestToQueue(airFareTrackerRequest);
				if (firstKey == null) {
					/**
					 * Set firstKey only if request's added back to queue
					 */
					firstKey = requestKey;
				}

				/**
				 * Consume the last request in the queue for a given PNR
				 */
				pnrRequestMap.put(pnr, airFareTrackerRequest);
				if (pnrRequestMap.size() == BATCH_SIZE_FOR_IMPORT_PNR) {
					boolean unhealthyExecution = !retrieveBookingsAndWait(pnrRequestMap.values());
					pnrRequestMap.clear();
					if (unhealthyExecution) {
						if (++consecutiveUnhealthyExecutions == CONSECUTIVE_UNHEALTHY_EXECUTIONS_THRESHOLD) {
							break;
						}
					} else {
						/**
						 * Reset counter if healthy execution occurs
						 */
						consecutiveUnhealthyExecutions = 0;
					}
				}
			} catch (Exception e) {
				log.error("Failed to create task for AirFareTrackerRequest {} due to ", airFareTrackerRequest, e);
				if (e instanceof CachingLayerException) {
					try {
						airFareTrackerRequestManager.addRequestToQueue(airFareTrackerRequest);
					} catch (Exception exception2) {
						log.error("Failed to add request {} to queue due to", airFareTrackerRequest, exception2);
					}
					/**
					 * Possibly cache is overloaded, avoid further fetch/store.
					 */
					break;
				}
			}
		}

		pnrRequestMap.values().forEach(
				airFareTrackerRequest -> poolExecutor.submit(() -> serveFareTrackerRequest(airFareTrackerRequest)));

		log.info("Finished Retrieving booking from queue");
	}

	/**
	 * 
	 * @param requests
	 * @return {@code true} if next pool should be executed
	 */
	private boolean retrieveBookingsAndWait(Collection<AirFareTrackerRequest> requests) {
		return executePoolAndWait(getTasks(requests, request -> () -> serveFareTrackerRequest(request)),
				POOL_EXECUTION_TIME_THRESHOLD, new ExceptionHandler() {
					@Override
					public void handleException(Exception e) {
						log.error("Failed to retrieve bookings for {} due to ", requests, e);
					}
				});
	}

	/**
	 * 
	 * @param tasks
	 * @param threshold in seconds
	 * @return {@code true} if execution completed within threshold and without error
	 */
	private boolean executePoolAndWait(Collection<Runnable> tasks, int threshold, ExceptionHandler exceptionHandler) {
		long start = System.currentTimeMillis();
		try {
			List<Future<?>> futures = new ArrayList<>();
			tasks.forEach(task -> futures.add(poolExecutor.submit(task)));
			for (Future<?> future : futures) {
				future.get(POOL_EXECUTION_TIME_THRESHOLD, TimeUnit.MILLISECONDS);
			}
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			exceptionHandler.handleException(e);
			return false;
		}
		return (System.currentTimeMillis() - start) <= threshold;
	}

	private <T> List<Runnable> getTasks(Collection<T> objs, Function<T, Runnable> createRunnableFunction) {
		List<Runnable> runnables = new ArrayList<>();
		for (T t : objs) {
			runnables.add(createRunnableFunction.apply(t));
		}
		return runnables;
	}

	private void serveFareTrackerRequest(AirFareTrackerRequest airFareTrackerRequest) {
		AirRetrievedBookingInfo airRetrievedBookingInfo = null;
		StringBuilder comment = null;
		try {
			AirImportPnrBooking airImportPnrBooking = airImportPnrManager.retrieveBooking(airFareTrackerRequest);
			airRetrievedBookingInfo = airImportPnrManager.storeRetrievedBookingForTracking(airImportPnrBooking);
		} catch (Exception e) {
			log.error("Failed to import pnr for {} due to ", airFareTrackerRequest, e);
			comment = new StringBuilder().append(e.getMessage());
			try {
				airRetrievedBookingInfo = airImportPnrManager.storeFailedBooking(airFareTrackerRequest.getPnr());
			} catch (Exception exceptionWhileStoringFailedBooking) {
				log.error("Failed to store failed booking with pnr {}, whose retrieval failed, due to ",
						airFareTrackerRequest.getPnr(), exceptionWhileStoringFailedBooking);
				comment.append(" Failed to store retrieved booking in cache");
			}
		}

		airFareTrackerAnalyticsManager.pushBookingRetrievalAnalytics(airFareTrackerRequest, airRetrievedBookingInfo,
				comment == null ? null : comment.toString());
	}

	private String getKey(AirFareTrackerRequest airFareTrackerRequest) {
		String pnr = airFareTrackerRequest.getPnr();
		int index = airFareTrackerRequest.getIndexInQueue();
		return pnr + index;
	}

	public void findNewFares() {
		final List<AirRetrievedBookingInfo> airRetrievedBookingInfos = new ArrayList<>();
		int consecutiveUnhealthyExecutions = 0;
		for (int i = 0; i < MAX_TASKS_COUNT_FOR_SEARCH; i++) {
			AirRetrievedBookingInfo airRetrievedBookingInfo = airImportPnrManager.popNextRetrievedBookingToTrack();
			if (airRetrievedBookingInfo == null) {
				/**
				 * No more booking available for tracking. All done. Wait for next cycle.
				 */
				break;
			}
			airRetrievedBookingInfos.add(airRetrievedBookingInfo);
			if (airRetrievedBookingInfos.size() == BATCH_SIZE_FOR_SEARCH) {
				boolean unhealthyExecution = !findNewFaresAndWait(airRetrievedBookingInfos);
				airRetrievedBookingInfos.clear();
				if (unhealthyExecution) {
					if (++consecutiveUnhealthyExecutions == CONSECUTIVE_UNHEALTHY_EXECUTIONS_THRESHOLD) {
						return;
					}
				} else {
					consecutiveUnhealthyExecutions = 0;
				}
			}
		}
		airRetrievedBookingInfos.forEach(
				airRetrievedBookingInfo -> poolExecutor.submit(() -> findNewFaresFor(airRetrievedBookingInfo)));
	}

	/**
	 * 
	 * @param requests
	 * @return {@code true} if next pool should be executed
	 */
	private boolean findNewFaresAndWait(Collection<AirRetrievedBookingInfo> requests) {
		return executePoolAndWait(getTasks(requests, request -> () -> findNewFaresFor(request)),
				POOL_EXECUTION_TIME_THRESHOLD, new ExceptionHandler() {
					@Override
					public void handleException(Exception e) {
						log.error("Failed to find new fares for {} due to ", requests, e);
					}
				});
	}

	private void findNewFaresFor(AirRetrievedBookingInfo airRetrievedBookingInfo) {
		ActionResult actionResult = null;
		String comment = null;
		boolean searchStatus = false;
		try {
			Map<String, List<TripInfo>> newTripInfos = airFareTrackerEngine.findNewFares(airRetrievedBookingInfo);
			if (searchStatus = !MapUtils.isEmpty(newTripInfos)) {
				actionResult = performAction(airRetrievedBookingInfo, newTripInfos);
				if (actionResult.isActionPerformed()) {
					/**
					 * Perform action only once.
					 */
					airImportPnrManager
							.deleteRetrievedBooking(airRetrievedBookingInfo.getAirImportPnrBooking().getPnr());
				}
			}
		} catch (Exception e) {
			log.error("Failed to find new fares for {} due to ", airRetrievedBookingInfo, e);
		}
		if (!searchStatus) {
			comment = "Failed to search flight(s)";
		} else if (actionResult == null) {
			comment = "Failed to perform action";
		}
		airFareTrackerAnalyticsManager.pushSearchAndActionAnalytics(airRetrievedBookingInfo, actionResult, comment);
	}

	/**
	 * 
	 * @param airRetrievedBookingInfo
	 * @param newTripInfos
	 * @param emailData
	 * @return {@code true} if action was performed
	 */
	private ActionResult performAction(AirRetrievedBookingInfo airRetrievedBookingInfo,
			Map<String, List<TripInfo>> newTripInfos) {
		try {
			return actionsManager.sendMail(airRetrievedBookingInfo, newTripInfos);
		} catch (Exception e) {
			log.error("Failed to perform action for {} due to ", airRetrievedBookingInfo, e);
			return null;
		}
	}

	private static interface ExceptionHandler {
		void handleException(Exception e);
	}
}
