package com.tgs.services.ffts.manager;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.AirFareTrackerMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ffts.datamodel.notification.AirFareUpdateEmailData;

@Service
class AirFareTrackerRequestManager {

	private static int counter = 1;

	private final static String PNR_QUEUE_KEY = "pnr_queue";

	@Autowired
	private GeneralCachingCommunicator cachingService;

	void addRequestToQueue(AirFareTrackerRequest fareTrackerRequest) {
		if (fareTrackerRequest.getIndexInQueue() == 0) {
			fareTrackerRequest.setIndexInQueue(counter++);
		}
		cachingService.storeInQueue(requestToMetaInfo(fareTrackerRequest));
	}

	/**
	 * 
	 * @return request at index 0. Returns {@code null} if no record found or if the pop operation failed
	 */
	AirFareTrackerRequest popRequest() {
		AirFareTrackerMetaInfo fareTrackerMetaInfo = cachingService.fetchFromQueue(
				AirFareTrackerMetaInfo.builder().key(PNR_QUEUE_KEY).namespace(CacheNameSpace.FLIGHT.getName())
						.set(CacheSetName.FARE_TRACKER.getName()).index(0).compress(false).build());

		if (fareTrackerMetaInfo == null) {
			return null;
		}
		return metaInfoToRequest(fareTrackerMetaInfo);
	}

	private AirFareTrackerMetaInfo requestToMetaInfo(AirFareTrackerRequest fareTrackerRequest) {
		return AirFareTrackerMetaInfo.builder().key(PNR_QUEUE_KEY).pnr(fareTrackerRequest.getPnr())
				.supplierId(fareTrackerRequest.getSupplierId())
				.searchSupplierIds(fareTrackerRequest.getSearchSupplierIds())
				.loggedinUser(fareTrackerRequest.getLoggedinUser())
				.actionThreshold(fareTrackerRequest.getActionThreshold())
				.indexInQueue(fareTrackerRequest.getIndexInQueue())
				.toEmailId(fareTrackerRequest.getEmailData().getToEmailId())
				.fromEmailId(fareTrackerRequest.getEmailData().getFromEmailId())
				.bccEmailId(fareTrackerRequest.getEmailData().getBccEmailId())
				.namespace(CacheNameSpace.FLIGHT.getName())
				.isRequestDataUpdated(fareTrackerRequest.isRequestDataUpdated())
				.set(CacheSetName.FARE_TRACKER.getName()).compress(false).build();
	}

	private AirFareTrackerRequest metaInfoToRequest(AirFareTrackerMetaInfo fareTrackerMetaInfo) {
		AirFareUpdateEmailData emailData = AirFareUpdateEmailData.builder()
				.toEmailId(fareTrackerMetaInfo.getToEmailId()).bccEmailId(fareTrackerMetaInfo.getBccEmailId())
				.fromEmailId(fareTrackerMetaInfo.getFromEmailId()).build();

		return AirFareTrackerRequest.builder().indexInQueue(fareTrackerMetaInfo.getIndexInQueue())
				.searchSupplierIds(fareTrackerMetaInfo.getSearchSupplierIds())
				.loggedinUser(fareTrackerMetaInfo.getLoggedinUser())
				.actionThreshold(fareTrackerMetaInfo.getActionThreshold()).pnr(fareTrackerMetaInfo.getPnr())
				.supplierId(fareTrackerMetaInfo.getSupplierId()).emailData(emailData)
				.isRequestDataUpdated(fareTrackerMetaInfo.isRequestDataUpdated()).build();
	}

}
