package com.tgs.services.ffts.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import com.tgs.services.fms.restmodel.AirSearchResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.ffts.client.fms.AFTFMSClient;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirSearchQueryListResponse;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
class AirFareTrackerSearchManager {

	final static private int MAX_RETRIES = 10;

	@Autowired
	private AFTFMSClient aftfmsClient;

	AirSearchResponse searchFlights(AirSearchRequest airSearchRequest, String jwt) throws Exception {
		AirSearchQueryListResponse queryListResponse = aftfmsClient.doPostForFms(
				AFTFMSClient.FMSService.SEARCHQUERY_LIST, airSearchRequest, AirSearchQueryListResponse.class, jwt);
		log.info("List of searchIds received for airSearchQuery {} are {}", airSearchRequest.getSearchQuery(),
				queryListResponse.getSearchIds());
		return getSearchedFlights(queryListResponse, jwt);
	}

	private AirSearchResponse getSearchedFlights(AirSearchQueryListResponse queryListResponse, String jwt) {
		List<AirSearchResponse> searchResponses = new ArrayList<>();
		List<Future<AirSearchResponse>> futures = new ArrayList<>();
		for (String searchId : queryListResponse.getSearchIds()) {
			try {
				AirSearchRequest airSearchRequest = new AirSearchRequest();
				airSearchRequest.setSearchId(searchId);
				futures.add(getAirSearchFuture(airSearchRequest, jwt));
			} catch (Exception e) {
				log.error("Failed to add air search future for searchId {} due to ", searchId, e);
			}
		}

		for (int i = 0; i < futures.size(); i++) {
			try {
				AirSearchResponse airSearchResponse = futures.get(i).get();
				searchResponses.add(airSearchResponse);
			} catch (Exception e) {
				log.error("Failed to process flight search response for searchId {} due to ",
						queryListResponse.getSearchIds().get(i), e);
			}
		}

		return mergeSearchedFlights(searchResponses);
	}

	private Future<AirSearchResponse> getAirSearchFuture(final AirSearchRequest airSearchRequest, String jwt) {
		return ExecutorUtils.getFareTrackerThreadPool().submit(new Callable<AirSearchResponse>() {

			@Override
			public AirSearchResponse call() throws Exception {
				int retriesLeft = MAX_RETRIES;
				Integer retryInSeconds = null;
				AirSearchResponse airSearchResponse = null;
				do {
					airSearchResponse = aftfmsClient.doPostForFms(AFTFMSClient.FMSService.SEARCH, airSearchRequest,
							AirSearchResponse.class, jwt);
					retryInSeconds = airSearchResponse.getRetryInSecond();
					if (retryInSeconds != null) {
						Thread.sleep(retryInSeconds * 1000);
					}
					retriesLeft--;
				} while (retryInSeconds != null && retriesLeft > 0);

				return airSearchResponse;
			}
		});
	}

	private AirSearchResponse mergeSearchedFlights(List<AirSearchResponse> searchResponses) {
		AirSearchResponse mergedSearchResponse = new AirSearchResponse();
		if (CollectionUtils.isEmpty(searchResponses)) {
			return mergedSearchResponse;
		}
		mergedSearchResponse.setSearchResult(new AirSearchResult());
		Map<String, List<TripInfo>> mergedTripInfosMap = mergedSearchResponse.getSearchResult().getTripInfos();
		for (AirSearchResponse searchResponse : searchResponses) {
			if (searchResponse.getSearchResult() != null) {
				Map<String, List<TripInfo>> tripInfosMap = searchResponse.getSearchResult().getTripInfos();
				if (MapUtils.isNotEmpty(tripInfosMap)) {
					for (String key : tripInfosMap.keySet()) {
						if (tripInfosMap.get(key) != null) {
							if (!mergedTripInfosMap.containsKey(key)) {
								mergedTripInfosMap.put(key, new ArrayList<>());
							}
							mergedTripInfosMap.get(key).addAll(tripInfosMap.get(key));
						}
					}
				}
			}
		}
		return mergedSearchResponse;
	}

}
