package com.tgs.services.ffts.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequestList;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.ffts.manager.analytics.AirFareTrackerAnalyticsManager;
import com.tgs.services.ffts.servicehandler.AirFareTrackerQueueServiceHandler;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RegisterPnrManager {

	@Autowired
	AirFareTrackerAnalyticsManager aftAnalyticsManager;

	@Autowired
	private AirFareTrackerQueueServiceHandler aftRequestQueueHandler;

	public BulkUploadResponse registerPnrs(AirFareTrackerRequestList fareTrackerRequest) throws Exception {
		if (fareTrackerRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(fareTrackerRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable registerPnrTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(1);
			for (AirFareTrackerRequest pnrRequest : fareTrackerRequest.getAirFareTrackerRequests()) {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(pnrRequest.getRowId());
					try {
						registerPnr(pnrRequest);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error(
								"Unable to register pnr {}, supplier id {} , ssids {} , to email {}, bcc email {}, from email {} ",
								pnrRequest.getPnr(), pnrRequest.getSupplierId(), pnrRequest.getSearchSupplierIds(),
								pnrRequest.getEmailData().getToEmailId(), pnrRequest.getEmailData().getBccEmailId(),
								pnrRequest.getEmailData().getFromEmailId(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while registering pnrs for {} ", jobId, e);
			}
		};
		aftAnalyticsManager.pushRequestAnalytics(fareTrackerRequest.getAirFareTrackerRequests(), null);
		return BulkUploadUtils.processBulkUploadRequest(registerPnrTask, fareTrackerRequest.getUploadType(), jobId);
	}

	public void registerPnr(AirFareTrackerRequest request) throws Exception {
		try {
			AirFareTrackerRequestList requestList = new AirFareTrackerRequestList();
			requestList.getAirFareTrackerRequests().add(request);
			aftRequestQueueHandler.initData(requestList, new BaseResponse());
			aftRequestQueueHandler.getResponse();
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Failed to add request {} due to ", request, e);
			throw new CustomGeneralException(SystemError.STORE_FARE_TRACKER_REQUEST_FAILURE);
		}
	}
}
