package com.tgs.services.ffts.mapper.analytics;

import java.time.Duration;
import java.time.LocalDateTime;
import com.google.common.base.Joiner;
import com.tgs.services.ffts.datamodel.ActionResult;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.ffts.datamodel.notification.AirFareUpdateEmailData;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import lombok.Builder;
import lombok.Setter;

@Setter
@Builder
public class AirSearchToAFTAnalyticsMapper extends BaseAFTAnalyticsMapper {

	private ActionResult actionResult;

	@Override
	protected void execute() {
		super.execute();
		AirFareTrackerRequest airFareTrackerRequest = getAirFareTrackerRequest();
		AirFareUpdateEmailData emailData = null;
		if (airFareTrackerRequest != null) {
			output.setSearchsuppliers(Joiner.on(", ").join(airFareTrackerRequest.getSearchSupplierIds().toArray()));
			emailData = airFareTrackerRequest.getEmailData();
		}
		AirRetrievedBookingInfo airRetrievedBookingInfo = getAirRetrievedBookingInfo();
		LocalDateTime departureDateTime = null;
		LocalDateTime importTime = null;
		if (airRetrievedBookingInfo != null) {
			importTime = airRetrievedBookingInfo.getImportTime();
			if (importTime != null) {
				LocalDateTime now = LocalDateTime.now();
				Long minutesFromImport = Duration.between(importTime, now).toMinutes();
				output.setImporttime(importTime.toString());
				output.setMfi(minutesFromImport);
				if (airRetrievedBookingInfo.getAirImportPnrBooking() != null) {
					departureDateTime =
							airRetrievedBookingInfo.getAirImportPnrBooking().getTripInfos().get(0).getDepartureTime();
					Long minutesToDeparture = Duration.between(now, departureDateTime).toMinutes();
					output.setDeptime(departureDateTime.toString());
					output.setMtd(minutesToDeparture);
				}
			}
		}
		boolean fareDropped = false, notified = false;
		if (actionResult != null) {
			Double fareDrop = actionResult.getFareDrop();
			output.setFaredrop(fareDrop);
			fareDropped = fareDrop != null && fareDrop > 0;
			notified = actionResult.isNotified();
			if (actionResult.isNotified() && emailData != null) {
				output.setToemailid(emailData.getToEmailId());
				output.setFromemailid(emailData.getFromEmailId());
				output.setBccemailid(emailData.getBccEmailId());
			}
		}
		output.setFaredropped(fareDropped);
		output.setNotified(notified);
	}
}
