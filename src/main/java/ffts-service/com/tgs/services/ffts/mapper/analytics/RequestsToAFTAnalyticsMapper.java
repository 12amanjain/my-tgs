package com.tgs.services.ffts.mapper.analytics;

import java.util.List;
import java.util.StringJoiner;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import lombok.Builder;

@Builder
public class RequestsToAFTAnalyticsMapper extends BaseAFTAnalyticsMapper {

	private List<AirFareTrackerRequest> requests;

	@Override
	protected void execute() throws CustomGeneralException {
		super.execute();
		output.setPnr(getPnrs());
	}

	/**
	 * 
	 * @return comma-separated PNRs
	 */
	private String getPnrs() {
		if (CollectionUtils.isEmpty(requests)) {
			return "No PNRs";
		}
		StringJoiner pnrs = new StringJoiner(", ");
		requests.forEach(request -> pnrs.add(request.getPnr()));
		return pnrs.toString();
	}
}
