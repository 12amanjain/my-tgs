package com.tgs.services.ffts.mapper.analytics;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.ffts.datamodel.AirRetrievedBookingInfo;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.Builder;
import lombok.Setter;

@Setter
@Builder
public class RetrievedBookingToAFTAnalyticsMapper extends BaseAFTAnalyticsMapper {

	private FMSCommunicator fmsCommunicator;

	@Override
	protected void execute() throws CustomGeneralException {
		super.execute();
		AirRetrievedBookingInfo airRetrievedBookingInfo = getAirRetrievedBookingInfo();
		if (airRetrievedBookingInfo != null) {
			AirImportPnrBooking airImportPnrBooking = airRetrievedBookingInfo.getAirImportPnrBooking();
			if (fmsCommunicator != null && airImportPnrBooking != null && airImportPnrBooking.getTripInfos() != null) {
				double totalFare = 0;
				for (TripInfo tripInfo : airImportPnrBooking.getTripInfos()) {
					totalFare += fmsCommunicator.getTotalFareComponentAmount(tripInfo, FareComponent.TF);
				}
				output.setTotalfare(totalFare);
			}
			output.setSuccessstatus(airRetrievedBookingInfo.getSuccessStatus());
		}
	}

}
