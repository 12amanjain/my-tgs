package com.tgs.services.ffts.restcontroller.validator;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequestList;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class AirFareTrackerRequestListValidator implements Validator {

	@Autowired
	private AirFareTrackerRequestValidator aftRequestValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return AirFareTrackerRequestList.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (!(target instanceof AirFareTrackerRequestList)) {
			return;
		}

		AirFareTrackerRequestList airFareTrackerRequestList = (AirFareTrackerRequestList) target;
		if (CollectionUtils.isEmpty(airFareTrackerRequestList.getAirFareTrackerRequests())) {
			return;
		}

		for (int i = 0; i < airFareTrackerRequestList.getAirFareTrackerRequests().size(); i++) {
			AirFareTrackerRequest airFareTrackerRequest = airFareTrackerRequestList.getAirFareTrackerRequests().get(i);
			String fieldName = "airFareTrackerRequests[" + i + "]";
			aftRequestValidator.validate(errors, fieldName, airFareTrackerRequest);
		}
	}
}
