package com.tgs.services.ffts.restcontroller.validator;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.validator.ListValidator;

@Service
public class AirFareTrackerRequestValidator implements Validator {

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private UserServiceCommunicator usCommunicator;

	@Autowired
	private ListValidator listValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return AirFareTrackerRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (!(target instanceof AirFareTrackerRequest)) {
			return;
		}

		AirFareTrackerRequest airFareTrackerRequest = (AirFareTrackerRequest) target;
		validate(errors, null, airFareTrackerRequest);
	}

	public void validate(Errors errors, String fieldName, AirFareTrackerRequest airFareTrackerRequest) {
		if (airFareTrackerRequest == null) {
			return;
		}

		String childFieldNamePrefix = fieldName == null ? "" : fieldName + ".";

		String bookingSupplierId = airFareTrackerRequest.getSupplierId();
		if (StringUtils.isBlank(bookingSupplierId) || fmsCommunicator.getSupplierInfo(bookingSupplierId) == null) {
			rejectValue(errors, childFieldNamePrefix + "supplierId", SystemError.INVALID_FBRC_SUPPLIERID,
					bookingSupplierId);
		}

		if (airFareTrackerRequest.getSearchSupplierIds() != null ) {
			if (TgsCollectionUtils.isEmptyStringCollection(airFareTrackerRequest.getSearchSupplierIds())) {
				rejectValue(errors, childFieldNamePrefix + "searchSupplierIds", SystemError.INVALID_FBRC_SUPPLIERID,
						"");
			} else {
				listValidator.validateSupplierIds(airFareTrackerRequest.getSearchSupplierIds(),
						childFieldNamePrefix + "searchSupplierIds", errors, SystemError.INVALID_FBRC_SUPPLIERID);
			}
		}

		String loggedInUser = airFareTrackerRequest.getLoggedinUser();
		if (loggedInUser != null) {
			if (StringUtils.isBlank(loggedInUser) || usCommunicator.getUserFromCache(loggedInUser) == null) {
				rejectValue(errors, childFieldNamePrefix + "loggedinUser", SystemError.INVALID_FBRC_USERID,
						loggedInUser);
			}
		}

		if (StringUtils.isBlank(airFareTrackerRequest.getPnr())) {
			rejectValue(errors, childFieldNamePrefix + "pnr", SystemError.BLANK_PNR);
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

}
