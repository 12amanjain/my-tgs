package com.tgs.services.ffts.servicehandler;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequestList;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.ffts.manager.AirFareTrackerManager;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirFareTrackerQueueServiceHandler extends ServiceHandler<AirFareTrackerRequestList, BaseResponse> {

	@Autowired
	private AirFareTrackerManager airFareTrackerManager;

	@Override
	public void beforeProcess() throws Exception {
		/**
		 * Use booking supplier id for search if not explicitly provided
		 */
		if (!CollectionUtils.isEmpty(request.getAirFareTrackerRequests())) {
			for (AirFareTrackerRequest airFareTrackerRequest : request.getAirFareTrackerRequests()) {
				if (CollectionUtils.isEmpty(airFareTrackerRequest.getSearchSupplierIds())) {
					airFareTrackerRequest
							.setSearchSupplierIds(Lists.newArrayList(airFareTrackerRequest.getSupplierId()));
				}
			}
		}
	}

	@Override
	public void process() throws Exception {
		try {
			airFareTrackerManager.addRequestsToQueue(request.getAirFareTrackerRequests());
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Failed to add requests {} due to ", request.getAirFareTrackerRequests(), e);
			throw new CustomGeneralException(SystemError.STORE_FARE_TRACKER_REQUEST_FAILURE);
		}
	}

	@Override
	public void afterProcess() throws Exception {}

}
