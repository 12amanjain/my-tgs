package com.tgs.services.ffts.servicehandler;

import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;
import com.tgs.services.ffts.restmodel.AirFareTrackerResponse;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.ffts.manager.AirFareTrackerManager;

@Service
public class AirFareTrackerServiceHandler extends ServiceHandler<AirFareTrackerRequest, AirFareTrackerResponse> {

	@Autowired
	private AirFareTrackerManager airFareTrackerManager;

	@Override
	public void beforeProcess() throws Exception {
		if (CollectionUtils.isEmpty(request.getSearchSupplierIds())) {
			request.setSearchSupplierIds(Lists.newArrayList(request.getSupplierId()));
		}
	}

	@Override
	public void process() throws Exception {
		response.setTripInfos(airFareTrackerManager.findNewFares(request));
	}

	@Override
	public void afterProcess() throws Exception {}

}
