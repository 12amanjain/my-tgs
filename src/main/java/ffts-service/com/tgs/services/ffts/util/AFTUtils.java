package com.tgs.services.ffts.util;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AFTUtils {

	/**
	 * Copied from BookingUtils
	 * 
	 * @param segmentList
	 * @return
	 */
	public static List<TripInfo> createTripListFromSegmentList(List<SegmentInfo> segmentList) {
		List<TripInfo> tripList = new ArrayList<>();
		TripInfo tripInfo = null;
		SegmentInfo previousSegmentInfo = null;
		for (SegmentInfo segmentInfo : segmentList) {
			if (segmentInfo.getSegmentNum() == 0) {
				tripInfo = new TripInfo();
				tripList.add(tripInfo);
			}
			if (segmentInfo.getSegmentNum() > 0) {
				if (previousSegmentInfo == null) {
					log.error("Segment number is " + segmentInfo.getSegmentNum() + "segmentInfo is"
							+ segmentInfo.toString());
				}
				// TODO Have To confirm
				if (segmentInfo.getDepartTime() != null && previousSegmentInfo.getArrivalTime() != null)
					previousSegmentInfo.setConnectingTime(Duration
							.between(previousSegmentInfo.getArrivalTime(), segmentInfo.getDepartTime()).toMinutes());
			}
			tripInfo.getSegmentInfos().add(segmentInfo);
			previousSegmentInfo = segmentInfo;
		}
		return tripList;
	}
}
