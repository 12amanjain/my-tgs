package com.tgs.services;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import com.tgs.services.base.HealthCheckController;
import com.tgs.services.fms.servicehandler.AirStubsInitializeHandler;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.annotations.ApiIgnore;

@RequestMapping("/fms/v1")
@Service
@ApiIgnore
@Slf4j
public class AirStaticReloadScheduler {


	@Autowired
	AirStubsInitializeHandler stubsInitializeHandler;

	@Value("${env}")
	private String env;

	/*
	 * @SuppressWarnings("unchecked")
	 *
	 * @RequestMapping(value = "/reload/stubs", method = RequestMethod.POST) protected BaseResponse
	 * reloadType(HttpServletRequest request, HttpServletResponse response,
	 *
	 * @RequestBody ReloadRequest reloadRequestType) throws Exception { log.info("Doing manual reload by user {}",
	 * SystemContextHolder.getContextData().getUser()); stubsInitializeHandler.initData(reloadRequestType, new
	 * BaseResponse()); BaseResponse response1 = stubsInitializeHandler.getResponse(); HealthCheckController.isAppReady
	 * = true; return response1; }
	 */

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 7 * 24 * 60 * 60 * 1000)
	public void reloadStubs() throws Exception {
		try {
			if ("prod".equals(env)) {
				log.info("Initializing Air Stubs Started");
				stubsInitializeHandler.initData(null, new BaseResponse());
				stubsInitializeHandler.getResponse();
			}
		} finally {
			log.info("Initializing Air Stubs Done ");
			HealthCheckController.isAppReady = true;
		}
	}
}
