package com.tgs.services.fms.communicator.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.ProcessedTripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceCancelConfiguration;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.jparepository.AirportInfoService;
import com.tgs.services.fms.manager.AirAnalyticsCancellationEngine;
import com.tgs.services.fms.manager.AirBookingEngine;
import com.tgs.services.fms.manager.AirCancellationEngine;
import com.tgs.services.fms.manager.AirPointsManager;
import com.tgs.services.fms.manager.AirVoucherManager;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.fms.mapper.AirBookToAnalyticsAirBookMapper;
import com.tgs.services.fms.mapper.TripInfoToProccessedTripInfoMapper;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import com.tgs.services.fms.restmodel.AirSearchResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.servicehandler.AirCancellationFareHandler;
import com.tgs.services.fms.servicehandler.AirCancellationHandler;
import com.tgs.services.fms.servicehandler.AirSearchHandler;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.fms.utils.AirAmendmentUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.points.datamodel.AirRedeemPointsData;
import com.tgs.services.points.restmodel.AirRedeemPointResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.vms.restmodel.FlightVoucherValidateRequest;
import com.tgs.services.vms.restmodel.FlightVoucherValidateResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FMSCommunicatorImpl implements FMSCommunicator {

	@Autowired
	@Lazy
	private AirBookingEngine bookingEngine;

	@Autowired
	private FareRuleManager fareRuleManager;

	@Autowired
	private AirAnalyticsHelper analyticsHelper;

	@Autowired
	private AirCancellationEngine cancellationEngine;

	@Autowired
	AirAnalyticsCancellationEngine airAnalyticsCancellationEngine;

	@Autowired
	@Lazy
	AirVoucherManager voucherManager;

	@Autowired
	AirportInfoService airportInfoService;

	@Autowired
	private AirPointsManager pointsManager;

	@Override
	public List<TripInfo> getTripInfos(AirBookingRequest bookingRequest) {
		return bookingEngine.getTripInfos(bookingRequest);
	}

	@Override
	public AirReviewResponse getAirReviewResponse(String bookingId, String oldBookingId) {
		return bookingEngine.getAirReviewResponse(bookingId, oldBookingId);
	}

	@Override
	public AirlineInfo getAirlineInfo(String airlineCode) {
		return AirlineHelper.getAirlineInfo(airlineCode);
	}

	@Override
	public AirportInfo getAirportInfo(String airportCode) {
		return AirportHelper.getAirportInfo(airportCode);
	}

	@Override
	public SupplierInfo getSupplierInfo(String supplierId) {
		return SupplierConfigurationHelper.getSupplierInfo(supplierId);
	}

	@Override
	public SupplierRule getSupplierRule(SupplierInfo supplierInfo) {
		return SupplierConfigurationHelper.getSupplierRule(supplierInfo.getSourceId(), supplierInfo.getSupplierId());
	}

	@Override
	public AirConfiguratorInfo getAirConfigRule(AirConfiguratorRuleType ruleType, User user) {
		FlightBasicFact flightFact = BaseUtils.createFactOnUser(FlightBasicFact.createFact(), user);
		return AirConfiguratorHelper.getAirConfigRuleInfo(flightFact, ruleType);
	}

	@Override
	public AirConfiguratorInfo getAirConfigRuleInfo(AirConfiguratorRuleType ruleType, FlightBasicFact fact) {
		return AirConfiguratorHelper.getAirConfigRuleInfo(fact, ruleType);
	}

	@Override
	public void doBooking(List<TripInfo> tripInfos, Order order) {
		bookingEngine.doBooking(tripInfos, order);
	}

	@Override
	public boolean doConfirmFare(Order order) {
		return bookingEngine.confirmTicketFare(order);
	}

	@Override
	public void doConfirmBooking(List<TripInfo> tripInfos, Order order) {
		bookingEngine.doConfirmBooking(tripInfos, order);
	}

	@Override
	public AirImportPnrBooking retrieveBooking(AirImportPnrBookingRequest pnrRequest) throws Exception {
		return bookingEngine.retrieveBooking(pnrRequest);
	}

	@Override
	public void updateCancellationAndRescheduleFees(TripInfo tripInfo, User bookingUser) {
		fareRuleManager.updateCancellationAndReschedulingFees(tripInfo, bookingUser);
	}

	@Override
	public void updateCancellationAndRescheduleFees(SegmentInfo segmentInfo, User bookingUser) {
		fareRuleManager.updateCancellationAndReschedulingFees(segmentInfo, bookingUser);
	}

	@Override
	public boolean isValidSource(Integer sourceId) {
		return AirSourceType.getAirSourceType(sourceId) != null;
	}

	@Override
	public NameLengthLimit getAirNameLength(FlightBasicFact fact) {
		return AirUtils.getAirNameLengthLimit(fact);
	}

	@Override
	public List<String> getTermsAndConditions(List<TripInfo> tripInfos, User bookingUser) {
		List<String> termsConditions = new ArrayList<>();
		tripInfos.forEach(tripInfo -> {
			FlightBasicFact flightFact = FlightBasicFact.createFact();
			flightFact.generateFact(tripInfo);
			flightFact.setAirType(AirUtils.getAirType(tripInfo));
			BaseUtils.createFactOnUser(flightFact, bookingUser);
			AirSourceConfigurationOutput configuratorInfo =
					AirConfiguratorHelper.getAirConfigRule(flightFact, AirConfiguratorRuleType.SOURCECONFIG);
			if (configuratorInfo != null && CollectionUtils.isNotEmpty(configuratorInfo.getTermsAndConditions())) {
				termsConditions.addAll(configuratorInfo.getTermsAndConditions());
			} else {
				AirGeneralPurposeOutput output =
						AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
				if (output != null) {
					termsConditions.addAll(output.getTermsAndConditions());
				}
			}
		});
		return termsConditions.stream().distinct().collect(Collectors.toList());
	}

	@Override
	public FareRuleInfo getFareRule(long id) {
		return fareRuleManager.getFareRuleById(id);
	}

	@Override
	public double getTotalFareComponentAmount(String bookingId, String oldBookingId, FareComponent fareComponent) {
		AirReviewResponse airReviewResponse = getAirReviewResponse(bookingId, oldBookingId);
		double totalAmount = 0;
		for (TripInfo tripInfo : airReviewResponse.getTripInfos()) {
			totalAmount += AirUtils.getTotalFareComponentAmount(tripInfo, fareComponent);
		}

		return totalAmount;
	}

	public double getTotalFareComponentAmount(TripInfo tripInfo, FareComponent fareComponent) {
		return AirUtils.getTotalFareComponentAmount(tripInfo, fareComponent);
	}

	@Override
	public void addBookToAnalytics(Order order, List<TripInfo> tripInfos, List<PaymentRequest> paymentRequests,
			String errorMsg, AirAnalyticsType analyticsType, AirSearchQuery searchQuery) {
		String bookingId = StringUtils.EMPTY;
		try {
			if (order != null) {
				bookingId = order.getBookingId();
				if (searchQuery == null)
					searchQuery = AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(tripInfos));
				AirBookToAnalyticsAirBookMapper queryMapper = AirBookToAnalyticsAirBookMapper.builder()
						.searchQuery(searchQuery).user(SystemContextHolder.getContextData().getUser())
						.bookingId(order.getBookingId()).contextData(SystemContextHolder.getContextData()).order(order)
						.tripInfos(tripInfos).build();
				queryMapper.setSearchQuery(searchQuery);
				queryMapper.setPaymentRequests(paymentRequests);
				queryMapper.setErrorMessage(errorMsg);
				analyticsHelper.pushToAnalytics(queryMapper, analyticsType);
			}
		} catch (Exception e) {
			log.error("Failed to Push Analytics Book bookingid {} ", bookingId, e);
		}
	}

	@Override
	public List<SupplierInfo> getSupplierForSourceId(String sourceId) {
		return SupplierConfigurationHelper.getSupplierForSourceId(sourceId);
	}

	@Override
	public AirSearchQuery getSearchQueryFromReviewResponse(AirBookingRequest request) {
		return bookingEngine.getSearchQueryFromReviewResponse(request);
	}

	@Override
	public AirSearchQuery getSearchQueryFromTripInfos(List<TripInfo> tripInfos) {
		return AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(tripInfos));
	}

	@Override
	public AirSearchResponse searchFlights(AirSearchRequest airSearchRequest) throws Exception {
		AirSearchHandler searchHandler = SpringContext.getApplicationContext().getBean(AirSearchHandler.class);
		searchHandler.initData(airSearchRequest, new AirSearchResponse());
		return searchHandler.getResponse();
	}

	@Override
	public ProcessedTripInfo getProcessTripInfo(TripInfo tripInfo) {
		return TripInfoToProccessedTripInfoMapper.builder().tripInfo(tripInfo).build().convert();
	}

	@Override
	public void releasePnr(Map<String, BookingSegments> supplierWiseBookingSegments, Order order) {
		cancellationEngine.releasePnr(supplierWiseBookingSegments, order);
	}

	@Override
	public Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingPNR(List<TripInfo> tripInfos) {
		return TripBookingUtils.getSupplierWiseBookingSegmentsUsingPNR(tripInfos);
	}

	@Override
	public SearchType getSearchTypeFromTrips(List<TripInfo> tripInfoList) {
		return AirUtils.getSearchTypeFromTrips(tripInfoList);
	}


	@Override
	public AirCancellationResponse getCancellationFare(AirCancellationRequest cancellationRequest, Order order)
			throws Exception {
		AirCancellationResponse cancellationResponse = AirCancellationResponse.builder().build();
		try {
			AirCancellationFareHandler cancellationFareHandler =
					SpringContext.getApplicationContext().getBean(AirCancellationFareHandler.class);
			cancellationFareHandler.setRequest(cancellationRequest);
			cancellationFareHandler.setOrder(order);
			cancellationFareHandler.setResponse(cancellationResponse);
			cancellationResponse = cancellationFareHandler.getResponse();
		} catch (Exception e) {
			log.error("AutoCancellation Failed for booking Id {} and exception is ", cancellationRequest.getBookingId(),
					e);
			AirCancellationDetail airCancellationDetail = AirCancellationDetail.builder().build();
			airCancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
			airCancellationDetail.setErrorMessage(e.getMessage());
			cancellationResponse.setCancellationDetail(airCancellationDetail);
		}
		return cancellationResponse;
	}

	@Override
	public AirCancellationResponse confirmCancellation(AirCancellationRequest cancellationRequest,
			AirCancellationResponse airCancellationResponse, Order order) {
		AirCancellationResponse cancellationResponse = AirCancellationResponse.builder().build();
		try {
			AirCancellationHandler cancellationHandler =
					SpringContext.getApplicationContext().getBean(AirCancellationHandler.class);
			cancellationHandler.setRequest(cancellationRequest);
			cancellationHandler.setOrder(order);
			cancellationHandler.setAirCancellationReviewResponse(airCancellationResponse);
			cancellationHandler.setResponse(cancellationResponse);
			cancellationResponse = cancellationHandler.getResponse();
		} catch (Exception e) {
			log.error("Error Occured on cancellation amendment id {} bookingid {} cause ",
					cancellationRequest.getAmendmentId(), cancellationRequest.getBookingId(), e);
		}
		return cancellationResponse;
	}

	@Override
	public boolean isSSRRefundable(AirOrderItem airOrderItem, User bookingUser) {
		return isRefundable(airOrderItem.getSupplierId(), airOrderItem.getAdditionalInfo().getSourceId(), bookingUser);
	}

	public void sendCancellationDataToAnalytics(AirCancellationRequest cancellationRequest, Order order,
			AirCancellationResponse cancellationReviewResponse, Double amountToBeRefunded, boolean isReview) {
		try {
			airAnalyticsCancellationEngine.sendDataToAnalytics(cancellationRequest, order, cancellationReviewResponse,
					amountToBeRefunded, isReview);
		} catch (Exception e) {
			log.error("Error Occured on cancellation analytics amendment id {} bookingid {} cause ",
					cancellationRequest.getAmendmentId(), cancellationRequest.getBookingId(), e);
		}
	}

	@Override
	public void updateAmendmentClientFee(AmendmentType amendmentType, SegmentInfo segmentInfo, User userFromCache,
			FareComponent fc) {
		AirAmendmentUtils.updateClientFee(segmentInfo, userFromCache, amendmentType, fc);
	}

	@Override
	public SupplierConfiguration getSupplierConfiguration(String supplierId) {
		return SupplierConfigurationHelper.getSupplierConfiguration(supplierId);
	}

	@Override
	public AirCreditShellResponse fetchCreditBalance(AirCreditShellRequest creditShellRequest) {
		AirSourceType sourceType = AirSourceType.getAirSourceType(creditShellRequest.getSupplierConfig().getSourceId());
		AbstractAirBookingRetrieveFactory factory = sourceType
				.getBookingRetrieveFactoryInstance(creditShellRequest.getSupplierConfig(), creditShellRequest.getPnr());
		return factory.fetchCreditShellBalance(creditShellRequest);
	}

	@Override
	public Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingSession(List<TripInfo> trips,
			String bookingId, User bookingUser) {
		return TripBookingUtils.getSupplierWiseBookingSegmentsUsingSession(trips, bookingId, bookingUser);
	}

	@Override
	public List<TripInfo> getTripInfosFromReviewResponse(String bookingId) {
		List<TripInfo> trips = new ArrayList<>();
		try {
			AirReviewResponse reviewResponse = getAirReviewResponse(bookingId, null);
			if (reviewResponse != null && CollectionUtils.isNotEmpty(reviewResponse.getTripInfos())) {
				trips = reviewResponse.getTripInfos();
			}
		} catch (Exception e) {
			log.info("AirReviewResponse expired for the booking id {} ", bookingId, e);
		}
		return trips;
	}

	public boolean isGeneralAndSourceApplicable(List<TripInfo> tripInfoList, User bookingUser) {
		AirGeneralPurposeOutput gnOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (!AirUtils.isInfantPresentInBooking(tripInfoList)) {
			for (TripInfo tripInfo : tripInfoList) {
				FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(tripInfo);
				AirSourceConfigurationOutput configurationOutput =
						AirConfiguratorHelper.getAirConfigRule(flightFact, AirConfiguratorRuleType.SOURCECONFIG);
				if (BooleanUtils.isTrue(gnOutput.getIsAmedmentChargesfromFR())
						&& BooleanUtils.isTrue(configurationOutput.getIsAmedmentChargesfromFR())) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public List<AirportInfo> fetchByCity(String city) {
		List<AirportInfo> airportInfoList = new ArrayList<>();
		List<com.tgs.services.fms.dbmodel.AirportInfo> airports = airportInfoService.fetchByCity(city);
		if (CollectionUtils.isNotEmpty(airports)) {
			airports.forEach(airportInfo -> {
				if (airportInfo != null)
					airportInfoList.add(new GsonMapper<>(airportInfo, AirportInfo.class).convert());
			});
		}
		return airportInfoList;
	}

	@Override
	public AirRedeemPointResponse calcOrApplyPoints(AirRedeemPointsData request, boolean modifyfareDetail) {
		return pointsManager.validatePoints(request, modifyfareDetail);
	}


	@Override
	public FlightVoucherValidateResponse applyVoucher(FlightVoucherValidateRequest request, boolean modifyFareDetail) {
		return voucherManager.calculateVoucherDiscount(request, modifyFareDetail);
	}

	@Override
	public boolean isSSRRefundable(TripInfo tripInfo, User bookingUser) {
		return isRefundable(tripInfo.getSupplierInfo().getSupplierId(), tripInfo.getSupplierInfo().getSourceId(),
				bookingUser);
	}

	public boolean isRefundable(String supplierId, Integer sourceId, User bookingUser) {
		FlightBasicFact basicFact = FlightBasicFact.createFact();
		basicFact.setSupplierId(supplierId);
		basicFact.setSourceId(sourceId);
		BaseUtils.createFactOnUser(basicFact, bookingUser);
		AirSourceCancelConfiguration cancelConfiguration =
				AirConfiguratorHelper.getAirConfigRule(basicFact, AirConfiguratorRuleType.CANCELCONFIG);
		if (cancelConfiguration != null) {
			return BooleanUtils.isTrue(cancelConfiguration.getIsSSRRefundable());
		}
		return false;
	}

}

