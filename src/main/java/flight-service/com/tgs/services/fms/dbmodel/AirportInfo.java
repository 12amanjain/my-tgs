package com.tgs.services.fms.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.RestExclude;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
public class AirportInfo extends BaseModel<AirportInfo, com.tgs.services.base.datamodel.AirportInfo> {

	@Column(unique = true)
	private String code;
	@Column
	private String name;
	@Column
	private String city;
	@Column
	private String country;
	@Column
	private String countryCode;
	@Column
	private String cityCode;

	@RestExclude
	@Column
	private Boolean enabled;

	@RestExclude
	@Column
	private Double priority;

	@Override
	public AirportInfo from(com.tgs.services.base.datamodel.AirportInfo dataModel) {
		return new GsonMapper<>(dataModel, this, AirportInfo.class).convert();
	}

}
