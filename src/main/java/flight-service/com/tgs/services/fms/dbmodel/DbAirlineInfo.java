package com.tgs.services.fms.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.fms.datamodel.AirlineInfo;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "airlineinfo")
public class DbAirlineInfo extends BaseModel<DbAirlineInfo, AirlineInfo> {

	@Column(unique = true)
	private String code;
	@Column
	private String name;
	@Column
	private boolean isLcc;
	@Column
	private boolean isTkRequired;

	private String accountingCode;

	@Override
	public AirlineInfo toDomain() {
		return new GsonMapper<>(this, AirlineInfo.class).convert();
	}

	@Override
	public DbAirlineInfo from(AirlineInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbAirlineInfo.class).convert();
	}
}
