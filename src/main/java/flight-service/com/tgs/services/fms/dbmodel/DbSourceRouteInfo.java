package com.tgs.services.fms.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "sourcerouteinfo")
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"src", "dest", "sourceId"})})
public class DbSourceRouteInfo extends BaseModel<DbSourceRouteInfo, SourceRouteInfo> {

	@Column
	private String src;
	@Column
	private String dest;
	@Column
	private Integer sourceId;

	@Column
	private Boolean enabled;

	@Override
	public DbSourceRouteInfo from(SourceRouteInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbSourceRouteInfo.class).convert();
	}

	@Override
	public SourceRouteInfo toDomain() {
		return new GsonMapper<>(this, SourceRouteInfo.class).convert();
	}
}
