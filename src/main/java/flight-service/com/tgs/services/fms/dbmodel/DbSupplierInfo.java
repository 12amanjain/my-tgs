package com.tgs.services.fms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.utils.TgsSecurityUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Audited
@TypeDefs({@TypeDef(name = "SupplierCredentialType", typeClass = SupplierCredentialType.class)})
@Table(name = "supplierinfo", uniqueConstraints = {@UniqueConstraint(columnNames = {"id", "name"})})
public class DbSupplierInfo extends BaseModel<DbSupplierInfo, SupplierInfo> {

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Type(type = "SupplierCredentialType")
	private SupplierCredential credentialInfo;

	@Column
	private boolean enabled;

	@Column
	private Integer sourceId;

	@Column
	private String name;

	@Column
	private boolean isDeleted;

	@Override
	public SupplierInfo toDomain() {
		return toDomain(true);
	}

	public SupplierInfo toDomain(boolean decrypt) {
		SupplierInfo si = new GsonMapper<>(this, SupplierInfo.class).convert();
		if (decrypt) {
			SupplierCredential creds = si.getCredentialInfo();
			if (creds != null) {
				try {
					creds.setPassword(TgsSecurityUtils.decryptData(creds.getPassword()));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return si;
	}

	@Override
	public DbSupplierInfo from(SupplierInfo dataModel) {
		return fromDomain(dataModel, true);
	}

	public DbSupplierInfo fromDomain(SupplierInfo dataModel, boolean encrypt) {
		SupplierInfo storedDataModel = this.toDomain();
		SupplierInfo merged = new GsonMapper<>(dataModel, storedDataModel, SupplierInfo.class).convert();
		DbSupplierInfo dbModel = new GsonMapper<>(merged, this, DbSupplierInfo.class).convert();
		if (encrypt) {
			SupplierCredential creds = dbModel.getCredentialInfo();
			if (creds != null) {
				try {
					creds.setPassword(TgsSecurityUtils.encryptData(creds.getPassword()));
				} catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
		}
		return dbModel;
	}
}
