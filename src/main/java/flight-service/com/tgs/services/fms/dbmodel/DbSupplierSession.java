package com.tgs.services.fms.dbmodel;

import java.time.LocalDateTime;
import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@TypeDefs({@TypeDef(name = "SupplierSessionInfoType", typeClass = SupplierSessionInfoType.class)})
@Table(name = "suppliersession")
public class DbSupplierSession extends BaseModel<DbSupplierSession, SupplierSession> {

	@Column
	@Nonnull
	private String bookingId;

	@Column
	@Nonnull
	@Type(type = "SupplierSessionInfoType")
	private SupplierSessionInfo supplierSessionInfo;

	@Column
	@Nonnull
	private Integer sourceId;

	@Column
	@Nonnull
	private String supplierId;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column(name = "expiryon")
	private LocalDateTime expiryTime;

	@Override
	public SupplierSession toDomain() {
		return new GsonMapper<>(this, SupplierSession.class).convert();
	}

	@Override
	public DbSupplierSession from(SupplierSession dataModel) {
		return new GsonMapper<>(dataModel, this, DbSupplierSession.class).convert();
	}
}
