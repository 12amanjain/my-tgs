package com.tgs.services.fms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;

public class SupplierSessionInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return SupplierSessionInfo.class;
	}

}
