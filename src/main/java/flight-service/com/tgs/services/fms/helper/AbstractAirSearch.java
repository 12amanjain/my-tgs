package com.tgs.services.fms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.mapper.AnalyticsAirQueryMapper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public abstract class AbstractAirSearch {

	@Autowired
	AirSearchResultProcessingManager processingManager;

	@Autowired
	AirAnalyticsHelper analyticsHelper;

	@Autowired
	AirSourceThreadHelper airSourceThreadhelper;


	public AirSearchResult search(AirSearchQuery searchQuery, ContextData contextData) {

		List<Future<AirSearchResult>> futureTaskList = new ArrayList<>();
		List<AbstractAirInfoFactory> factories = new ArrayList<>();
		AirSearchResult finalSearchResult = null;
		for (Integer sourceId : searchQuery.getSourceIds()) {
			AirSourceType airSourceType = AirSourceType.getAirSourceType(sourceId);
			List<SupplierConfiguration> supplierList =
					SupplierConfigurationHelper.getValidSearchSuppliers(searchQuery, sourceId, contextData.getUser());
			log.debug("Supplier count {} for sourceId {}, for searchQuery {}", supplierList.size(), sourceId,
					searchQuery);
			List<String> searchForAirline = new ArrayList<>();
			if (StringUtils.isNotEmpty(searchQuery.getPrefferedAirline())) {
				searchForAirline = Arrays.asList(searchQuery.getPrefferedAirline().split(","));
			}
			for (SupplierConfiguration supplierConf : supplierList) {
				AirSourceConfigurationOutput airSourceOutput = AirUtils.getAirSourceConfiguration(searchQuery,
						supplierConf.getBasicInfo(), contextData.getUser());
				List<String> preferredAirline = getPreferredAirline(airSourceOutput, supplierConf, searchForAirline);
				// To check Preferred airline from search is in supplierconf
				if (preferredAirline == null || preferredAirline.size() > 0) {
					AirSearchQuery copyQuery = new GsonMapper<>(searchQuery, AirSearchQuery.class).convert();
					if (CollectionUtils.isNotEmpty(preferredAirline)) {
						List<AirlineInfo> airlineList = new ArrayList<>();
						for (String airline : preferredAirline) {
							airlineList.add(AirlineHelper.getAirlineInfo(airline));
						}
						copyQuery.setPreferredAirline(new HashSet<>(airlineList));
					}
					if (airSourceThreadhelper.isSearchAllowed(airSourceType.getSourceId(), copyQuery,
							contextData.getUser())) {
						AbstractAirInfoFactory factory = airSourceType.getFactoryInstance(copyQuery, supplierConf);
						factories.add(factory);
						Future<AirSearchResult> searchResponse = ExecutorUtils.getFlightSearchThreadPool()
								.submit(() -> factory.getAvailableSchedule(contextData));
						futureTaskList.add(searchResponse);
					}
				}
			}
		}

		long startTime = System.currentTimeMillis();
		for (int i = 0; i < futureTaskList.size(); i++) {
			AirSearchQuery tempQuery = factories.get(i).getSearchQuery();
			AirSearchResult searchResult = null;
			long searchTime = 0;
			boolean isTimeOut = false;
			try {
				/**
				 * This is to ensure that cumulative max time of all the threads are 15 sec
				 */
				long timeout = startTime
						+ AirUtils.getTimeOutInMilliSecond(contextData.getUser(),
								factories.get(i).getSupplierConf().getBasicInfo(), tempQuery)
						- System.currentTimeMillis();
				if (timeout < 0) {
					timeout = 1000;
				}

				searchResult = futureTaskList.get(i).get(timeout, TimeUnit.MILLISECONDS);
				searchTime = factories.get(i).getSearchTime();

				Integer sourceId = factories.get(i).getSupplierConf().getSourceId();

				if (searchResult == null || (searchResult != null && MapUtils.isEmpty(searchResult.getTripInfos()))) {
					log.debug("No result found for supplier {} searchQuery {}",
							factories.get(i).getSupplierConf().getBasicInfo().getSupplierId(), tempQuery);
					continue;
				} else if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())
						&& searchResult.getTotalTrips() > 0) {
					// On Successful search response this will remove the Source from NoResultSector.
					SourceRouteInfoHelper.deleteNoResultKey(tempQuery.getRouteInfos(), sourceId);
				}
				if (finalSearchResult == null) {
					finalSearchResult = searchResult;
				} else {
					/**
					 * This will merge search Result from different supplier. For example in case of GoAir , we are
					 * hitting three suppliers(TBO,GoAirDirectAPI,Mystifly) to fetch best rates.
					 *
					 * Different Trips(having same segmentInfo) will be be merged into one Trip in
					 * AirSearchResultProcessingManager.
					 */

					for (TripInfoType tripType : TripInfoType.values()) {
						String key = tripType.name();
						if (MapUtils.isNotEmpty(searchResult.getTripInfos())
								&& CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(key))) {
							if (CollectionUtils.isEmpty(finalSearchResult.getTripInfos().get(key))) {
								finalSearchResult.getTripInfos().put(key, searchResult.getTripInfos().get(key));
							} else {
								finalSearchResult.getTripInfos().get(key).addAll(searchResult.getTripInfos().get(key));
							}
						}
					}
				}
			} catch (TimeoutException te) {
				isTimeOut = true;
				log.info("Timeout occured after {} sec, searchQuery {}",
						(System.currentTimeMillis() - startTime) / 1000, tempQuery);
				futureTaskList.get(i).cancel(true);
				factories.get(i).getCriticalMessageLogger().add(AirSourceConstants.TIMEOUT);
			} catch (Exception e) {
				log.error("Unable to fetch search Response for searchQuery {}", tempQuery, e);
			} finally {
				AnalyticsAirQueryMapper queryMapper = AnalyticsAirQueryMapper.builder().searchQuery(tempQuery)
						.user(SystemContextHolder.getContextData().getUser())
						.contextData(SystemContextHolder.getContextData()).searchResult(searchResult)
						.basicInfo(factories.get(i).getSupplierConf().getBasicInfo())
						.threadTime(TimeUnit.MILLISECONDS.toSeconds(searchTime))
						.errorMessages(factories.get(i).getCriticalMessageLogger()).build();
				sendToAnalytics(queryMapper);
				airSourceThreadhelper.searchFinished(factories.get(i).getSupplierConf().getSourceId(), isTimeOut,
						contextData.getUser());
			}
		}
		processingManager.processAirSearchResult(finalSearchResult, searchQuery, contextData.getUser());
		return finalSearchResult;
	}

	private void sendToAnalytics(AnalyticsAirQueryMapper queryMapper) {
		try {
			analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.SEARCH);
		} catch (Exception e) {
			log.error("Failed to Push Analytics SEARCH ", e);
		}
	}

	private List<String> getPreferredAirline(AirSourceConfigurationOutput airSourceOutput,
			SupplierConfiguration supplierConf, List<String> searchForAirline) {
		List<String> prefferedAirline = null;
		List<String> includedAirline = null;

		if (Objects.nonNull(supplierConf.getSupplierAdditionalInfo())
				&& (CollectionUtils.isNotEmpty(supplierConf.getSupplierAdditionalInfo().getIncludedAirlines()))) {
			includedAirline = new ArrayList<>();
			includedAirline = supplierConf.getSupplierAdditionalInfo().getIncludedAirlines();
		}

		if (CollectionUtils.isNotEmpty(searchForAirline)) {
			includedAirline = retainSameAirline(includedAirline, searchForAirline);
		}

		if (Objects.nonNull(airSourceOutput) && CollectionUtils.isNotEmpty(airSourceOutput.getIncludedAirlines())) {
			includedAirline = retainSameAirline(includedAirline, airSourceOutput.getIncludedAirlines());
		}

		if (Objects.nonNull(airSourceOutput) && CollectionUtils.isNotEmpty(includedAirline)
				&& CollectionUtils.isNotEmpty(airSourceOutput.getExcludedAirlines())) {
			for (String airline : airSourceOutput.getExcludedAirlines()) {
				includedAirline.remove(airline);
			}
		}
		prefferedAirline = includedAirline;
		if (includedAirline != null && includedAirline.size() == 0) {
			prefferedAirline = new ArrayList<>();
		}
		return prefferedAirline;
	}

	private List<String> retainSameAirline(List<String> includedAirline, List<String> applicableAirline) {
		if (includedAirline == null) {
			includedAirline = new ArrayList<>();
			includedAirline.addAll(applicableAirline);
		} else {
			includedAirline.retainAll(applicableAirline);
		}
		return includedAirline;
	}

}
