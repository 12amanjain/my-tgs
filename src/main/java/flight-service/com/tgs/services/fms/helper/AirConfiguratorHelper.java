package com.tgs.services.fms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.ruleengine.GeneralRuleField;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorFilter;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;
import com.tgs.services.fms.jparepository.AirConfiguratorService;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.logging.datamodel.LogMetaInfo;

@Service
public class AirConfiguratorHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap airConfigRules;

	private static Map<AirConfiguratorRuleType, List<AirConfiguratorInfo>> inMemoryAirConfigRules = new HashMap<>();

	private static final String FIELD = "air_config";

	private static Map<String, ? extends IRuleField> gnResolverMap = EnumUtils.getEnumMap(GeneralRuleField.class);
	private static Map<String, ? extends IRuleField> airResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

	private static Map<String, IRuleField> fieldResolverMap = new HashMap<>();;

	static {
		fieldResolverMap.putAll(gnResolverMap);
		fieldResolverMap.putAll(airResolverMap);
	}

	private static AirConfiguratorHelper SINGLETON;

	@Autowired
	AirConfiguratorService airConfiguratorService;

	public AirConfiguratorHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap airConfigRules) {
		super(configurationHashMap);
		AirConfiguratorHelper.airConfigRules = airConfigRules;
	}

	@PostConstruct
	void init() {
		SINGLETON = this;
	}

	@Override
	public void process() {
		processInMemory();

		if (MapUtils.isEmpty(inMemoryAirConfigRules)) {
			return;
		}

		inMemoryAirConfigRules.forEach((ruleType, rules) -> {
			airConfigRules.put(ruleType.getName(), FIELD, rules,
					CacheMetaInfo.builder().set(CacheSetName.AIR_CONFIGURATOR.getName())
							.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		});
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<DbAirConfiguratorRule> configRules =
				airConfiguratorService.findAll(AirConfiguratorFilter.builder().build());
		Map<AirConfiguratorRuleType, List<AirConfiguratorInfo>> airConfigRuleMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(configRules)) {
			List<AirConfiguratorInfo> configuratorInfos = DbAirConfiguratorRule.toDomainList(configRules);
			airConfigRuleMap =
					configuratorInfos.stream().collect(Collectors.groupingBy(AirConfiguratorInfo::getRuleType));
		}
		inMemoryAirConfigRules = airConfigRuleMap;
	}


	@Override
	public void deleteExistingInitializer() {
		if (inMemoryAirConfigRules != null) {
			inMemoryAirConfigRules.clear();
		}
		airConfigRules.truncate(CacheSetName.AIR_CONFIGURATOR.getName());
	}

	@SuppressWarnings("unchecked")
	public static <T> T getAirConfigRule(IFact flightFact, AirConfiguratorRuleType ruleType) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return (T) matchingRules.get(0).getOutput();
		}
		return null;
	}

	public static AirConfiguratorInfo getAirConfigRuleInfo(IFact flightFact, AirConfiguratorRuleType ruleType) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules.get(0);
		}
		return null;
	}

	public static AirGeneralPurposeOutput getGeneralPurposeOutput(FlightBasicFact basicFact) {
		AirGeneralPurposeOutput output = null;
		List<AirConfiguratorInfo> configuratorInfos = getApplicableRules(AirConfiguratorRuleType.GNPUPROSE, basicFact);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			output = (AirGeneralPurposeOutput) configuratorInfos.get(0).getOutput();
		}
		return output;
	}


	public static List<AirConfiguratorInfo> getAllAirConfigRule(AirConfiguratorRuleType ruleType, IFact flightFact) {
		List<AirConfiguratorInfo> matchingRules = getApplicableRules(ruleType, flightFact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static List<AirConfiguratorInfo> getApplicableRules(AirConfiguratorRuleType ruleType, IFact flightFact) {
		List<AirConfiguratorInfo> airconfigRules = getAirConfiguratorRules(ruleType);

		if (CollectionUtils.isEmpty(airconfigRules)) {
			return null;
		}

		LogUtils.log("configurator#ruleexecute", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				null);
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(airconfigRules, flightFact, fieldResolverMap);
		List<AirConfiguratorInfo> rules = (List<AirConfiguratorInfo>) ruleEngine.fireAllRules();
		if (UserUtils.getUserId(SystemContextHolder.getContextData().getUser()).equals("511")) {
			LogUtils.log("singletrip", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
					"configurator#ruleexecute", 2L);
		}
		return rules;
	}

	public static List<AirConfiguratorInfo> getAirConfiguratorRules(AirConfiguratorRuleType ruleType) {
		if (inMemoryAirConfigRules == null)
			SINGLETON.processInMemory();
		List<AirConfiguratorInfo> rules = new ArrayList<>();
		List<AirConfiguratorInfo> configuratorInfos = inMemoryAirConfigRules.get(ruleType);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			inMemoryAirConfigRules.get(ruleType).forEach(rule -> {
				rules.add(rule.toBuilder().build());
			});
		}
		return rules;
		// Gson gson = GsonUtils.getGson();
		// String json = gson.toJson(inMemoryAirConfigRules.get(ruleType));
		// return gson.fromJson(json, new TypeToken<List<AirConfiguratorInfo>>() {}.getType());
		/*
		 * List<AirConfiguratorInfo> ruleList = new ArrayList<>(); return airConfigRules.get(ruleType.getName(), FIELD,
		 * ruleList.getClass(), CacheMetaInfo.builder().set(CacheSetName.AIR_CONFIGURATOR.getName()) .typeOfT(new
		 * TypeToken<List<AirConfiguratorInfo>>() {}.getType()).build());
		 */
	}
}
