package com.tgs.services.fms.helper;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceThreadConfig;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceThreadConfiguration;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.mapper.AirSearchRestrictionQueryMapper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirSourceThreadHelper {

	@Autowired
	AirAnalyticsHelper airanalyticsHelper;

	public AirSourceThreadHelper() {
		initializeSourceThreadMap();
		initializeSourceTimeOutMap();
		initializeSourceResetTimeMap();
	}

	static Map<Integer, AtomicInteger> sourceWiseThreadCount = null;

	static Map<Integer, AtomicInteger> sourceWiseTimeOutCount = null;

	static Map<Integer, AtomicLong> sourceWiseResetTime = null;

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 60 * 1000)
	public static void logSourceThreadInfo() {
		log.info("Current State of SourceWise Thread Map for Host : {}, is : {}", ServiceUtils.getHostName(),
				sourceWiseThreadCount);
		log.info("Current State of SourceWise ThreadTimeOut Map for Host : {}, is : {}", ServiceUtils.getHostName(),
				sourceWiseTimeOutCount);
	}

	private static void initializeSourceThreadMap() {
		sourceWiseThreadCount = new HashMap<>();
		for (AirSourceType airSourceType : AirSourceType.values()) {
			sourceWiseThreadCount.put(airSourceType.getSourceId(), new AtomicInteger());
		}
	}

	private static void initializeSourceTimeOutMap() {
		sourceWiseTimeOutCount = new HashMap<>();
		for (AirSourceType airSourceType : AirSourceType.values()) {
			sourceWiseTimeOutCount.put(airSourceType.getSourceId(), new AtomicInteger());
		}
	}

	private static void initializeSourceResetTimeMap() {
		sourceWiseResetTime = new HashMap<>();
		long currentTimeInSec = System.currentTimeMillis();
		for (AirSourceType airSourceType : AirSourceType.values()) {
			sourceWiseResetTime.put(airSourceType.getSourceId(), new AtomicLong(currentTimeInSec));
		}
	}

	public static void resetSourceWiseTimeOutMap(int sourceId) {
		sourceWiseResetTime.put(sourceId, new AtomicLong(System.currentTimeMillis()));
		sourceWiseTimeOutCount.put(sourceId, new AtomicInteger());
	}

	public void incrementSourceWiseThread(int sourceId) {
		sourceWiseThreadCount.get(sourceId).getAndIncrement();
	}

	/**
	 * 
	 * This method will decrement the source thread count , and increment the timeout count for the source, if we are
	 * considering timeout for that source.
	 * 
	 * @param sourceId
	 * @param isTimeOut
	 */
	public void searchFinished(int sourceId, boolean isTimeOut, User user) {
		AirSourceThreadConfig sourceThreadConfig = getSourceThreadConfig(sourceId, user);
		if (sourceThreadConfig != null) {
			if (sourceWiseThreadCount.get(sourceId).get() > 0) {
				sourceWiseThreadCount.get(sourceId).getAndDecrement();
			}
			if (isTimeOut && BooleanUtils.isTrue(sourceThreadConfig.getConsiderThreadTimeOut())) {
				sourceWiseTimeOutCount.get(sourceId).getAndIncrement();
			}
		}
	}

	public boolean isSearchAllowed(int sourceId, AirSearchQuery searchQuery, User bookingUser) {
		boolean isAllowed = true;
		FlightBasicFact fact = FlightBasicFact.builder().build();
		BaseUtils.createFactOnUser(fact, bookingUser);
		AirSourceThreadConfiguration threadConfig =
				AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.SOURCETHREAD);
		if (threadConfig != null && MapUtils.isNotEmpty(threadConfig.getSourceWiseThreadConfig())) {
			AirSourceThreadConfig sourceThreadConfig = threadConfig.getSourceWiseThreadConfig().get(sourceId);
			if (Objects.nonNull(sourceThreadConfig)) {
				try {
					/**
					 * 
					 * There will be two checks; <br>
					 * 1. To check total number of threads for the source, should be less than the specified
					 * threshold<br>
					 * 2. To check total number of timeout counts, should be less than the threshold limit After passing
					 * these checks, search requests will be allowed for the source.
					 */
					Long lastResetTime = sourceWiseResetTime.get(sourceId).get();
					boolean isExceededThreadCount =
							sourceThreadConfig.isExceededThreadCount(sourceWiseThreadCount.get(sourceId).get());
					boolean isTimeOutExceeded = sourceThreadConfig
							.isTimeOutThreadExceeded(sourceWiseTimeOutCount.get(sourceId).get(), lastResetTime);
					if (isExceededThreadCount || isTimeOutExceeded) {
						isAllowed = false;
						log.info(
								"Max Allowed Thread limit Crossed for host {} max limit for source {} is : {}, Current Thread Count {}, Current TimeOut Count {} ",
								ServiceUtils.getHostName(), AirSourceType.getAirSourceType(sourceId).name(),
								sourceThreadConfig.getMaxLimit(), sourceWiseThreadCount.get(sourceId),
								sourceWiseTimeOutCount.get(sourceId));
						AirSearchRestrictionQueryMapper mapper =
								getAirSearchRestyrictionMapper(searchQuery, sourceId, sourceThreadConfig);
						airanalyticsHelper.pushToAnalytics(mapper, AirAnalyticsType.SOURCE_THREAD);
					}
					boolean isTimeOutTimeFrameCompleted = sourceThreadConfig.isTimeOutTimeFrameCompleted(lastResetTime);
					if (isTimeOutTimeFrameCompleted) {
						resetSourceWiseTimeOutMap(sourceId);
					}
				} finally {
					if (isAllowed) {
						incrementSourceWiseThread(sourceId);
					}
				}
			}
		}
		return isAllowed;
	}


	private AirSearchRestrictionQueryMapper getAirSearchRestyrictionMapper(AirSearchQuery searchQuery, int sourceId,
			AirSourceThreadConfig sourceThreadConfig) {
		AirSearchRestrictionQueryMapper mapper = AirSearchRestrictionQueryMapper.builder().build();
		mapper.setThreadcount(sourceWiseThreadCount.get(sourceId).get());
		mapper.setMaxallowedthread(sourceThreadConfig.getMaxLimit());
		mapper.setMaxtimeoutallowed(sourceThreadConfig.getMaxTimeOutAllowed());
		mapper.setTimeoutcount(sourceWiseTimeOutCount.get(sourceId).get());
		mapper.setSourceid(sourceId);
		mapper.setSearchid(searchQuery.getSearchId());
		mapper.setAnalyticstype(AirAnalyticsType.SOURCE_THREAD.name());
		mapper.setUser(SystemContextHolder.getContextData().getEmulateOrLoggedInUser());
		mapper.setContextData(SystemContextHolder.getContextData());
		return mapper;

	}

	public AirSourceThreadConfig getSourceThreadConfig(Integer sourceId, User user) {
		AirSourceThreadConfiguration threadConfig = AirConfiguratorHelper
				.getAirConfigRule(BaseUtils.createFactOnUser(null, user), AirConfiguratorRuleType.SOURCETHREAD);
		return threadConfig != null && MapUtils.isNotEmpty(threadConfig.getSourceWiseThreadConfig())
				? threadConfig.getSourceWiseThreadConfig().get(sourceId)
				: null;
	}

}
