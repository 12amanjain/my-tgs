package com.tgs.services.fms.helper;

import java.util.Arrays;
import java.util.List;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.BeanType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.airarabia.AirArabiaBookingFactory;
import com.tgs.services.fms.sources.airarabia.AirArabiaBookingRetrieveFactory;
import com.tgs.services.fms.sources.airarabia.AirArabiaInfoFactory;
import com.tgs.services.fms.sources.airasia.AirAsiaBookingFactory;
import com.tgs.services.fms.sources.airasia.AirAsiaBookingRetrieveFactory;
import com.tgs.services.fms.sources.airasia.AirAsiaCancellationFactory;
import com.tgs.services.fms.sources.airasia.AirAsiaInfoFactory;
import com.tgs.services.fms.sources.airasiadotrez.AirAsiaDotRezAirCancellationFactory;
import com.tgs.services.fms.sources.airasiadotrez.AirAsiaDotRezAirInfoFactory;
import com.tgs.services.fms.sources.airasiadotrez.AirAsiaDotRezBookingFactory;
import com.tgs.services.fms.sources.airasiadotrez.AirAsiaDotRezBookingRetrieveFactory;
import com.tgs.services.fms.sources.amadeus.AmadeusAirBookingFactory;
import com.tgs.services.fms.sources.amadeus.AmadeusAirCancellationFactory;
import com.tgs.services.fms.sources.amadeus.AmadeusAirInfoFactory;
import com.tgs.services.fms.sources.amadeus.AmadeusRetrieveBookingFactory;
import com.tgs.services.fms.sources.dealinventory.InventoryAirBookingFactory;
import com.tgs.services.fms.sources.dealinventory.InventoryAirInfoFactory;
import com.tgs.services.fms.sources.flightroutes24.FlightRoutes24AirBookingFactory;
import com.tgs.services.fms.sources.flightroutes24.FlightRoutes24InfoFactory;
import com.tgs.services.fms.sources.ifly.IflyAirBookingFactory;
import com.tgs.services.fms.sources.ifly.IflyAirBookingRetrieveFactroy;
import com.tgs.services.fms.sources.ifly.IflyAirInfoFactory;
import com.tgs.services.fms.sources.mystifly.MystiflyAirBookingFactory;
import com.tgs.services.fms.sources.mystifly.MystiflyAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.mystifly.MystiflyAirCancellationFactory;
import com.tgs.services.fms.sources.mystifly.MystiflyAirInfoFactory;
import com.tgs.services.fms.sources.navitaireV4_2.NavitaireAirBookingFactory;
import com.tgs.services.fms.sources.navitaireV4_2.NavitaireAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.navitaireV4_2.NavitaireAirCancellationFactory;
import com.tgs.services.fms.sources.navitaireV4_2.NavitaireAirInfoFactory;
import com.tgs.services.fms.sources.otaradixx.RadixxAirBookingFactory;
import com.tgs.services.fms.sources.otaradixx.RadixxAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.otaradixx.RadixxAirCancellationFactory;
import com.tgs.services.fms.sources.otaradixx.RadixxAirInfoFactory;
import com.tgs.services.fms.sources.sabre.SabreAirBookingFactory;
import com.tgs.services.fms.sources.sabre.SabreAirCancellationFactory;
import com.tgs.services.fms.sources.sabre.SabreAirInfoFactory;
import com.tgs.services.fms.sources.sabre.SabreAirRetrieveBookingFactory;
import com.tgs.services.fms.sources.tbo.TravelBoutiqueAirBookingFactory;
import com.tgs.services.fms.sources.tbo.TravelBoutiqueAirCancellationFactory;
import com.tgs.services.fms.sources.tbo.TravelBoutiqueAirInfoFactory;
import com.tgs.services.fms.sources.technogram.TechnoGramAirBookingFactory;
import com.tgs.services.fms.sources.technogram.TechnoGramAirInfoFactory;
import com.tgs.services.fms.sources.travelfusion.TravelFusionAirBookingFactory;
import com.tgs.services.fms.sources.travelfusion.TravelFusionAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.travelfusion.TravelFusionAirInfoFactory;
import com.tgs.services.fms.sources.travelport.TravelPortAirBookingFactory;
import com.tgs.services.fms.sources.travelport.TravelPortAirBookingRetrieveFactory;
import com.tgs.services.fms.sources.travelport.TravelPortAirInfoFactory;
import com.tgs.services.fms.sources.travelport.TravelPortCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.Getter;

@Getter
public enum AirSourceType {

	GOAIR(1) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, NavitaireAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, NavitaireAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(NavitaireAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					NavitaireAirCancellationFactory.class);
		}

	},
	SABRE(2) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, SabreAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, SabreAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(SabreAirRetrieveBookingFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public PaxType getTaxType(String type) {
			if (type.equals("INF")) {
				return PaxType.INFANT;
			}
			return PaxType.getPaxType(type);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					SabreAirCancellationFactory.class);
		}

	},
	FLYDUBAI(3) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, RadixxAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, RadixxAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(RadixxAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					RadixxAirCancellationFactory.class);
		}

	},
	SPICEJET(4) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, NavitaireAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, NavitaireAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(NavitaireAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY, CabinClass.BUSINESS);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					NavitaireAirCancellationFactory.class);
		}

	},
	INDIGO(5) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, NavitaireAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, NavitaireAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(NavitaireAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					NavitaireAirCancellationFactory.class);
		}

	},
	SCOOT(6) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, NavitaireAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, NavitaireAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(NavitaireAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					NavitaireAirCancellationFactory.class);
		}

	},
	AIRARABIA(7) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, AirArabiaInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, AirArabiaBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(AirArabiaBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}

	},
	AIRINDIAEXPRESS(8) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, RadixxAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, RadixxAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(RadixxAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					RadixxAirCancellationFactory.class);
		}

	},
	TBO(9) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, TravelBoutiqueAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order,
					TravelBoutiqueAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return null;
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					TravelBoutiqueAirCancellationFactory.class);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY);
		}

		@Override
		public boolean isValidSearchQuery(AirSearchQuery searchQuery) {
			if ((searchQuery.getPaxInfo().get(PaxType.ADULT) + searchQuery.getPaxInfo().get(PaxType.CHILD)
					+ searchQuery.getPaxInfo().get(PaxType.INFANT)) > 9) {
				return false;
			}
			return true;
		}

	},
	AMADEUS(10) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, AmadeusAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, AmadeusAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(AmadeusRetrieveBookingFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					AmadeusAirCancellationFactory.class);
		}

	},
	DEALINVENTORY(11) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, InventoryAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, InventoryAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return null;
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY,
					CabinClass.PREMIUM_BUSINESS, CabinClass.FIRST, CabinClass.PREMIUMFIRST);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}

	},
	MYSTIFLY(12) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, MystiflyAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, MystiflyAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(MystiflyAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					MystiflyAirCancellationFactory.class);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY);
		}

	},
	TRUJET(13) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, IflyAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, IflyAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(IflyAirBookingRetrieveFactroy.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}

	},
	JAZEERA(14) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, NavitaireAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, NavitaireAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(NavitaireAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY, CabinClass.BUSINESS);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					NavitaireAirCancellationFactory.class);
		}

	},
	TRAVELPORT(15) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, TravelPortAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, TravelPortAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(TravelPortAirBookingRetrieveFactory.class,
					supplierConf, pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					TravelPortCancellationFactory.class);
		}

	},
	AIRASIA(16) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, AirAsiaInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, AirAsiaBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(AirAsiaBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					AirAsiaCancellationFactory.class);
		}

	},

	TRIPJACK(17) {

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, TechnoGramAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, TechnoGramAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return null;
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}

	},
	TRAVELIMPRESSION(18) {

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, TechnoGramAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, TechnoGramAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return null;
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;

		}
	},
	FKTOURS(19) {

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, TechnoGramAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, TechnoGramAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return null;
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}

	},
	FLIGHTROUTES24(20) {

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.BUSINESS, CabinClass.ECONOMY, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, FlightRoutes24InfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order,
					FlightRoutes24AirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return null;
		}

		@Override
		public boolean isValidSearchQuery(AirSearchQuery searchQuery) {
			if (searchQuery != null && searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0) > 0) {
				return false;
			}
			return true;
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}
	},
	TRAVELFUSION(21) {
		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY, CabinClass.BUSINESS, CabinClass.PREMIUM_ECONOMY, CabinClass.FIRST);
		}

		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, TravelFusionAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, TravelFusionAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(TravelFusionAirBookingRetrieveFactory.class,
					supplierConf, pnr);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return null;
		}
	},

	AIRASIADOTREZ(22) {

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, AirAsiaDotRezAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, AirAsiaDotRezBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(AirAsiaDotRezBookingRetrieveFactory.class,
					supplierConf, pnr);
		}
		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					AirAsiaDotRezAirCancellationFactory.class);
		}

	},
	SALAMAIR(23) {
		@Override
		public AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
				SupplierConfiguration supplierConf) {
			return getAirInfoFactoryBean(searchQuery, supplierConf, RadixxAirInfoFactory.class);
		}

		@Override
		public AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
				BookingSegments bookingSegments, Order order) {
			return getAirBookingFactoryBean(supplierConf, bookingSegments, order, RadixxAirBookingFactory.class);
		}

		@Override
		public AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(SupplierConfiguration supplierConf,
				String pnr) {
			return SpringContext.getApplicationContext().getBean(RadixxAirBookingRetrieveFactory.class, supplierConf,
					pnr);
		}

		@Override
		public List<CabinClass> allowedCabinClasses() {
			return Arrays.asList(CabinClass.ECONOMY);
		}

		@Override
		public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
				SupplierConfiguration supplierConf) {
			return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
					RadixxAirCancellationFactory.class);
		}

	};

	private int sourceId;

	private AirSourceType(int sourceId) {
		this.sourceId = sourceId;
	}

	public PaxType getTaxType(String type) {
		return PaxType.getPaxType(type);
	}

	public static AirSourceType getAirSourceType(int sourceId) {
		for (AirSourceType sourceType : AirSourceType.values()) {
			if (sourceType.getSourceId() == sourceId) {
				return sourceType;
			}
		}
		return null;
	}

	public abstract List<CabinClass> allowedCabinClasses();

	public abstract AbstractAirInfoFactory getFactoryInstance(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConf);

	public abstract AbstractAirBookingFactory getBookingFactoryInstance(SupplierConfiguration supplierConf,
			BookingSegments bookingSegments, Order order);

	public abstract AbstractAirBookingRetrieveFactory getBookingRetrieveFactoryInstance(
			SupplierConfiguration supplierConf, String pnr);

	public AbstractAirCancellationFactory getAbortBookingFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		return getAbstractAirAbortBookingFactory(bookingSegments, order, supplierConf,
				AbstractAirCancellationFactory.class);
	}


	public <T extends AbstractAirInfoFactory> AbstractAirInfoFactory getAirInfoFactoryBean(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConf, Class<T> defaultClass) {

		return (AbstractAirInfoFactory) SpringContext.getApplicationContext().getBean(
				ServiceUtils.getDependencyBeanName(BeanType.AIRINFOFACTORY, String.valueOf(sourceId), defaultClass),
				searchQuery, supplierConf);

	}

	public <T extends AbstractAirBookingFactory> AbstractAirBookingFactory getAirBookingFactoryBean(
			SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order, Class<T> defaultClass) {

		return (AbstractAirBookingFactory) SpringContext.getApplicationContext().getBean(
				ServiceUtils.getDependencyBeanName(BeanType.AIRBOOKINGFACTORY, String.valueOf(sourceId), defaultClass),
				supplierConf, bookingSegments, order);

	}

	public <T extends AbstractAirCancellationFactory> AbstractAirCancellationFactory getAbstractAirAbortBookingFactory(
			BookingSegments bookingSegments, Order order, SupplierConfiguration supplierConf, Class<T> defaultClass) {

		return (AbstractAirCancellationFactory) SpringContext.getApplicationContext().getBean(ServiceUtils
				.getDependencyBeanName(BeanType.AIRCANCELLATIONFACTORY, String.valueOf(sourceId), defaultClass),
				bookingSegments, order, supplierConf);

	}

	public boolean isValidSearchQuery(AirSearchQuery searchQuery) {
		return true;
	}

}
