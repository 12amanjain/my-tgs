package com.tgs.services.fms.helper;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import com.tgs.services.base.GsonMapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.jparepository.AirlineInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirlineHelper extends InMemoryInitializer {

	private static Map<String, AirlineInfo> airlineHashMap;

	@Autowired
	AirlineInfoService service;

	@Autowired
	public AirlineHelper(CustomInMemoryHashMap configurationHashMap) {
		super(configurationHashMap);
	}

	public static AirlineInfo getAirlineInfo(String code) {
		if (StringUtils.isBlank(code)) {
			return null;
		}

		if (airlineHashMap.get(code) == null) {
			try {
				log.info("Unable to find airline code {} in database", code);
				throw new Exception("unable to find airlinecode " + code);
			} catch (Exception e) {
				log.info("unable to find airlinecode {}" ,code, e);
			}

		}
		return new GsonMapper<>(airlineHashMap.get(code), AirlineInfo.class).convert();
	}

	@Override
	@PostConstruct
	public void process() {
		airlineHashMap = new HashMap<>();
		service.findAll().forEach(airlineInfo -> airlineHashMap.put(airlineInfo.getCode(), airlineInfo.toDomain()));

	}

	@Override
	public void deleteExistingInitializer() {
		airlineHashMap = new HashMap<>();
	}
}
