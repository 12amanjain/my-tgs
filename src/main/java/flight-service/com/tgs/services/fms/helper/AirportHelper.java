package com.tgs.services.fms.helper;

import java.util.HashMap;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.fms.jparepository.AirportInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirportHelper extends InMemoryInitializer {

	private static HashMap<String, AirportInfo> airportHashMap;

	@Autowired
	AirportInfoService service;

	@Autowired
	public AirportHelper(CustomInMemoryHashMap configurationHashMap) {
		super(configurationHashMap);
	}

	@Override
	@PostConstruct
	public void process() {
		airportHashMap = new HashMap<>();
		service.findAll().forEach(airportInfo -> airportHashMap.put(airportInfo.getCode(),
				new GsonMapper<>(airportInfo, AirportInfo.class).convert()));
	}

	public static AirportInfo getAirportInfo(String code) {
		AirportInfo info = airportHashMap.get(code);
		if (info == null) {
			log.info("There is no airport mapping found in our database for code {}", code);
		}

		return new GsonMapper<>(info, AirportInfo.class).convert();
	}

	/**
	 * @implSpec : getAirport should be used for supplier <br>
	 *           factory level to throw Exception <br>
	 *           if airport not available in system, <br>
	 *           corresponding segment/trip will be ignored
	 */
	public static AirportInfo getAirport(String code) {
		AirportInfo info = airportHashMap.get(code);
		if (info == null) {
			throw new RuntimeException("There is no airport mapping found in our database for code " + code);
		}
		return new GsonMapper<>(info, AirportInfo.class).convert();
	}

	@Override
	public void deleteExistingInitializer() {
		airportHashMap = new HashMap<>();
	}
}
