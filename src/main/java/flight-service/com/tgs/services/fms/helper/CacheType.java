package com.tgs.services.fms.helper;

import java.util.Map;
import org.apache.commons.lang3.math.NumberUtils;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;

@Getter
public enum CacheType {
	SEARCHIDWAITTIME(300),
	RETRYSECOND(10),
	SEARCHIDEXPIRATION(600),
	FLIGHTSEARCHCACHING(3600),
	PRICEIDKEYATSEARCH(3600),
	LOWAIRFARE(25200),
	REVIEWKEYEXPIRATION(7200),
	FARECOMPONENT(604800),
	SUPPLIERSESSIONEXPIRATION(3600),
	NON_OPERATIONAL_SECTORS(259200);

	private int ttl;

	private CacheType(int ttl) {
		this.ttl = ttl;
	}

	/*
	 * contains dead code
	 */
	public int getTtl(User user) {
		AirConfiguratorInfo rule = null; // AirConfiguratorHelper.getValidAirConfigRule(null,
											// AirConfiguratorRuleType.GNPUPROSE);
		Map<String, String> ttlMap = null;
		if (rule != null && rule.getOutput() != null) {
			AirGeneralPurposeOutput output = (AirGeneralPurposeOutput) rule.getOutput();
			if (output.getTtlMap() != null) {
				ttlMap = output.getTtlMap();
				return NumberUtils.toInt(ttlMap.get(this.name()), ttl);
			}
		}
		return ttl;
	}

	public int getTtl() {
		return getTtl(null);
	}
}
