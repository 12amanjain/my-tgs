package com.tgs.services.fms.helper;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReference;
import com.tgs.services.fms.utils.AirUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirPath;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.PathEdge;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CombinationAirSearch extends AbstractAirSearch {

	@Autowired
	FlightCacheHandler cacheHandler;

	public static AirSearchQuery createSearchQueryFromEdge(PathEdge edge, AirSearchQuery searchQuery) {
		AirSearchQuery copyQuery = new GsonMapper<>(searchQuery, AirSearchQuery.class).convert();
		copyQuery.getRouteInfos().get(0).setFromCityOrAirport(AirportHelper.getAirportInfo(edge.getSrc()));
		copyQuery.getRouteInfos().get(0).setToCityOrAirport(AirportHelper.getAirportInfo(edge.getDest()));
		if (edge.getPrefferedAirline() != null) {
			edge.getPrefferedAirline().forEach(preferredAirline -> {
				copyQuery.addPreferredAirline(AirlineHelper.getAirlineInfo(preferredAirline));
			});
		}
		copyQuery.setSourceIds(edge.getSourceId());
		return copyQuery;
	}

	@Override
	public AirSearchResult search(AirSearchQuery airSearchQuery, ContextData contextData) {
		Map<AirPath, Map<AirSearchQuery, Future<AirSearchResult>>> futureTaskMap = new LinkedHashMap<>();
		List<AirSearchQuery> searchQueries = AirUtils.splitSearchQueryRoutesIntoSearchQueryList(airSearchQuery);
		AirSearchResult finalSearchResult = new AirSearchResult();
		searchQueries.forEach(searchQuery -> {
			for (AirPath airPath : AirUtils.getCustomPaths(searchQuery, null)) {
				Map<AirSearchQuery, Future<AirSearchResult>> innerMap =
						futureTaskMap.getOrDefault(airPath, new LinkedHashMap<>());
				for (PathEdge edge : airPath.getEdges()) {
					AirSearchQuery copyQuery = createSearchQueryFromEdge(edge, searchQuery);
					Future<AirSearchResult> searchResponse = ExecutorUtils.getFlightSearchThreadPool()
							.submit(() -> super.search(copyQuery, contextData));
					innerMap.put(copyQuery, searchResponse);
				}
				futureTaskMap.put(airPath, innerMap);
			}
		});

		for (AirPath airPath : futureTaskMap.keySet()) {
			AirSearchResult airPathSearchResult = null;
			boolean isAllEdgesSuccesfull = true;
			for (Map.Entry<AirSearchQuery, Future<AirSearchResult>> mapEntry : futureTaskMap.get(airPath).entrySet()) {
				try {
					AirSearchResult edgeResult = mapEntry.getValue().get();
					if (edgeResult == null) {
						isAllEdgesSuccesfull = false;
					}
					if (airPathSearchResult == null) {
						airPathSearchResult = edgeResult;
					} else {
						airPathSearchResult = consolidateResult(airPathSearchResult, edgeResult, mapEntry.getKey());
					}
					if (edgeResult != null) {
						edgeResult.getTripInfos().forEach((type, trips) -> {
							trips.forEach(trip -> {
								cacheHandler.storeSearchQueryForCombiSearch(mapEntry.getKey(), trip);
							});
						});
					}
				} catch (Exception e) {
					isAllEdgesSuccesfull = false;
					log.error("Unable to fetch search Response for searchQuery {}", airSearchQuery, e);
				}
			}
			if (isAllEdgesSuccesfull) {
				TripInfoType tripInfoType = getTripType(airSearchQuery, airPath);
				for (String key : airPathSearchResult.getTripInfos().keySet()) {
					finalSearchResult.getTripInfos().put(tripInfoType.getTripType(),
							airPathSearchResult.getTripInfos().get(key));
				}
			}
		}
		return finalSearchResult;
	}

	private TripInfoType getTripType(AirSearchQuery airSearchQuery, AirPath airPath) {
		AtomicReference<TripInfoType> tripInfoType = new AtomicReference<>();
		tripInfoType.set(TripInfoType.ONWARD);
		if (airSearchQuery.isDomesticReturn()) {
			RouteInfo routeInfo = airSearchQuery.getRouteInfos().get(1);
			airPath.getEdges().forEach(pathEdge -> {
				if (routeInfo.getFromCityAirportCode().equals(pathEdge.getSrc())) {
					tripInfoType.set(TripInfoType.RETURN);
				}
			});
		}
		return tripInfoType.get();
	}

	@SuppressWarnings("serial")
	public AirSearchResult consolidateResult(AirSearchResult oldSearchResult, AirSearchResult newSearchResult,
			AirSearchQuery searchQuery) {
		AirSearchResult combineResults = new AirSearchResult();
		for (String key : oldSearchResult.getTripInfos().keySet()) {
			for (int i = 0; i < oldSearchResult.getTripInfos().get(key).size(); i++) {
				for (TripInfo newTripInfo : newSearchResult.getTripInfos().get(key)) {
					TripInfo copyOldTripInfo =
							new GsonMapper<>(oldSearchResult.getTripInfos().get(key).get(i), TripInfo.class).convert();
					if (TripPriceEngine.removeRedundantAndCheckMergeApplicable(copyOldTripInfo, newTripInfo)) {
						List<SegmentInfo> copyNewSegmentInfo =
								GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(newTripInfo.getSegmentInfos()),
										new TypeToken<List<SegmentInfo>>() {}.getType());
						LocalDateTime newSegmentDepartureTime = copyNewSegmentInfo.get(0).getDepartTime();
						LocalDateTime oldTripArrivalTime = copyOldTripInfo.getArrivalTime();

						// As minimum Connecting two Trips Hours should be greater than 3 Hours
						if (Duration.between(oldTripArrivalTime, newSegmentDepartureTime).toHours() >= 3) {
							copyOldTripInfo.getSegmentInfos().get(0).setIsCombinationFirstSegment(true);
							copyNewSegmentInfo.get(0).setIsCombinationFirstSegment(true);
							TripPriceEngine.sortPriceOnFareTypeOrdinal(copyOldTripInfo);
							TripPriceEngine.sortPriceOnFareTypeOrdinal(newTripInfo);
							int segmentNum = 0;
							copyOldTripInfo.getSegmentInfos().addAll(copyNewSegmentInfo);
							for (SegmentInfo segmentInfo : copyOldTripInfo.getSegmentInfos()) {
								segmentInfo.setSegmentNum(segmentNum++);
							}
							combineResults.addTripInfo(key, copyOldTripInfo);
						}
					}
				}
			}
		}
		return combineResults;
	}
}
