package com.tgs.services.fms.helper;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.fms.jparepository.SupplierSessionService;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.fms.sources.technogram.TechnoGramAirUtils;
import com.tgs.services.fms.sources.travelport.TravelPortMarshallerWrapper;

@Service
public class FMSStaticContextInitializer {

	@Autowired
	SupplierSessionService sessionService;

	@Autowired
	SupplierSessionHelper sessionHelper;


	@Autowired
	Jaxb2Marshaller marshaller;

	@Autowired
	MoneyExchangeCommunicator moneyExchangeComm;

	@PostConstruct
	public void init() {
		TripBookingUtils.init(sessionService, sessionHelper);
		TravelPortMarshallerWrapper.init(marshaller);
		TechnoGramAirUtils.init(moneyExchangeComm);
	}
}
