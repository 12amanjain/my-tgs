package com.tgs.services.fms.helper;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.farerule.FareRuleFilter;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.fms.dbmodel.DbFareRuleInfo;
import com.tgs.services.fms.jparepository.FareRuleService;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FareRuleHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap fareRules;

	static Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

	private static final String FIELD = "fare_rule";

	private static final String FR_KEY = "FR_GEN";

	private static Map<String, List<FareRuleInfo>> inMemoryFareRuleMap = new HashMap<>();

	private static FareRuleHelper SINGLETON;

	@Autowired
	FareRuleService fareRuleService;

	public FareRuleHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap fareRules) {
		super(configurationHashMap);
		FareRuleHelper.fareRules = fareRules;
	}

	@PostConstruct
	void init() {
		SINGLETON = this;
	}

	@Override
	public void process() {
		processInMemory();
		if (MapUtils.isNotEmpty(inMemoryFareRuleMap)) {
			inMemoryFareRuleMap.forEach((ruleType, rules) -> {
				fareRules.put(ruleType, FIELD, rules, CacheMetaInfo.builder().set(CacheSetName.FARE_RULE.getName())
						.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
			});
		}
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	public void processInMemory() {
		List<DbFareRuleInfo> rules =
				fareRuleService.findAll(FareRuleFilter.builder().enabled(true).deleted(false).build());
		List<FareRuleInfo> fareRuleInfos = new ArrayList<>();
		rules.forEach(rule -> {
			FareRuleInfo fareRule = rule.toDomain();
			fareRule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()).setAirType(fareRule.getAirType());
			fareRuleInfos.add(new GsonMapper<>(fareRule, FareRuleInfo.class).convert());
		});
		Map<String, List<FareRuleInfo>> convertedRules =
				fareRuleInfos.stream().collect(Collectors.groupingBy(fareRuleInfo -> getKey(fareRuleInfo)));
		inMemoryFareRuleMap =
				Optional.<Map<String, List<FareRuleInfo>>>ofNullable(convertedRules).orElseGet(HashMap::new);
	}

	public static String getKey(FareRuleInfo ruleInfo) {

		/**
		 * In case of GDS, airline will empty. To handle this case we are joining airType with FR_GEN as key in caching.
		 * ALL will be default airType
		 */
		return StringUtils.join(StringUtils.join(ruleInfo.getAirType().name(), ObjectUtils.firstNonNull(
				StringUtils.isNotBlank(ruleInfo.getAirline()) ? ruleInfo.getAirline() : null, FareRuleHelper.FR_KEY)));
	}

	@Override
	public void deleteExistingInitializer() {
		if (MapUtils.isNotEmpty(inMemoryFareRuleMap))
			inMemoryFareRuleMap.clear();
		fareRules.truncate(CacheSetName.FARE_RULE.getName());
	}

	/**
	 * @implNote : "isCustomInstance" <br>
	 *           1. Its used from Inmemory if any population over fields of FareRuleInfo then isCustomInstance: true
	 *           Case : 1: In Case of whitelabel : Partner User Fee is added to Cancellation/DateChange Fee<br>
	 *           (in case of same instance always amount is getting multipled as much request count).
	 * 
	 */
	@SuppressWarnings("unchecked")
	public static FareRuleInfo getFareRule(FlightBasicFact flightFact, boolean isCustomInstance) {
		List<FareRuleInfo> fareRules = getFareRules(flightFact);
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(fareRules, flightFact, fieldResolverMap);
		List<FareRuleInfo> fareRuleInfoList = (List<FareRuleInfo>) ruleEngine.fireAllRules();
		if (CollectionUtils.isNotEmpty(fareRuleInfoList)) {
			log.debug("Fare rule found  {}", fareRuleInfoList.get(0).getId());
			if (isCustomInstance) {
				return new GsonMapper<>(fareRuleInfoList.get(0), FareRuleInfo.class).convert();
			}
			return fareRuleInfoList.get(0);
		}

		return null;
	}

	private static List<FareRuleInfo> getFareRules(FlightBasicFact flightFact) {
		List<FareRuleInfo> rules = new ArrayList<>();
		if (inMemoryFareRuleMap == null)
			SINGLETON.processInMemory();
		if (MapUtils.isNotEmpty(inMemoryFareRuleMap)) {

			// fact airType + fact airline.
			String key = StringUtils.join(flightFact.getAirType().name(), flightFact.getAirline());
			log.debug("Fare rule key  {}", key);
			List<FareRuleInfo> cachedRules = new ArrayList<>(inMemoryFareRuleMap.getOrDefault(key, new ArrayList<>()));

			// AirType.ALL + fact airline.
			if (CollectionUtils.isEmpty(cachedRules)) {
				key = StringUtils.join(AirType.ALL.name(), flightFact.getAirline());
				cachedRules = new ArrayList<>(inMemoryFareRuleMap.getOrDefault(key, new ArrayList<>()));
			}

			// AirType.ALL + FR_GEN.
			key = StringUtils.join(AirType.ALL.name(), FareRuleHelper.FR_KEY);
			if (CollectionUtils.isNotEmpty(inMemoryFareRuleMap.get(key))) {
				cachedRules.addAll(new ArrayList<>(inMemoryFareRuleMap.get(key)));
			}

			if (CollectionUtils.isNotEmpty(cachedRules)) {
				Iterator<FareRuleInfo> itr = cachedRules.iterator();
				while (itr.hasNext()) {
					FareRuleInfo fRule = itr.next();
					rules.add(fRule.toBuilder().build());
				}
			}
			/*
			 * if (CollectionUtils.isNotEmpty(fareRules)) { Gson gson = GsonUtils.getGson(); String json =
			 * gson.toJson(fareRules); log.debug("Fare rule found {} for key  {}", json, key); return
			 * gson.fromJson(json, new TypeToken<List<FareRuleInfo>>() {}.getType()); }
			 */
		}
		return rules;
	}

	public FareRuleInfo getFareRuleInfo(Long id) {
		DbFareRuleInfo fareRuleInfo = fareRuleService.findById(id);
		if (fareRuleInfo != null) {
			return fareRuleInfo.toDomain();
		}
		return null;
	}

}
