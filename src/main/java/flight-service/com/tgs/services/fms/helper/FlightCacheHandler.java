package com.tgs.services.fms.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.LowFareInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FlightCacheHandler {

	public static final String DELIM = "_";

	@Autowired
	FMSCachingServiceCommunicator cachingService;

	@Value("${env}")
	private String env;

	public Integer getFlightListingCachingExpiration(List<TripInfo> tripInfos, AirSearchQuery searchQuery,
			SupplierConfiguration supplierConf) {
		// if ("prod".equals(env)) {
		// return 0;
		// }
		return CacheType.FLIGHTSEARCHCACHING.getTtl();
	}

	public void cacheAirSearchResults(AirSearchResult searchResult, AirSearchQuery searchQuery,
			SupplierConfiguration supplierConf, User bookingUser) {
		if (searchResult != null && !AirUtils.isCacheDisabled(searchQuery, supplierConf.getBasicInfo(), bookingUser)) {
			log.debug("Storing searchResult in cache for searchQuery {}", searchQuery);
			searchResult.getTripInfos().forEach((type, trips) -> {
				log.debug("Total no of trips for type {} is {} for searchQuery {}", type, trips.size(), searchQuery);
				if (CollectionUtils.isNotEmpty(trips)) {
					try {
						int expiration = getFlightListingCachingExpiration(trips, searchQuery, supplierConf);
						CacheMetaInfo metaInfo = CacheMetaInfo.builder().compress(true).plainData(false)
								.expiration(expiration).set(CacheSetName.FLIGHT_SEARCH.getName()).build();

						cachingService.cacheFlightListing(trips, generateKey(type, searchQuery, supplierConf),
								metaInfo);
					} catch (Exception e) {
						log.error("Unable to store flight listing into cache for searchQuery {} ", searchQuery, e);
					}
					if (searchQuery.getIsDomestic() || (searchQuery.isIntl() && searchQuery.isOneWay())) {
						storeLowFare(trips, searchQuery, type);
					}
				}
			});
		}
	}

	public String generateKey(String type, AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		String sectorKey = "";
		if (searchQuery.getSearchType() == SearchType.ONEWAY && !type.equals(TripInfoType.ONWARD.name())) {
			return sectorKey;
		}
		if (searchQuery.isDomesticReturn()) {
			if (type.equals(TripInfoType.ONWARD.getName())) {
				sectorKey =
						StringUtils.join(sectorKey, searchQuery.getRouteInfos().get(0).getFromCityOrAirport().getCode(),
								searchQuery.getRouteInfos().get(0).getToCityOrAirport().getCode(),
								searchQuery.getRouteInfos().get(0).getTravelDate().toString());
			} else {
				sectorKey =
						StringUtils.join(sectorKey, searchQuery.getRouteInfos().get(1).getFromCityOrAirport().getCode(),
								searchQuery.getRouteInfos().get(1).getToCityOrAirport().getCode(),
								searchQuery.getRouteInfos().get(1).getTravelDate().toString());
			}
		} else {
			for (RouteInfo routInfo : searchQuery.getRouteInfos()) {
				sectorKey = StringUtils.join(sectorKey, routInfo.getFromCityOrAirport().getCode(),
						routInfo.getToCityOrAirport().getCode(), routInfo.getTravelDate().toString());
			}
		}

		String paxKey = StringUtils.join(searchQuery.getPaxInfo().get(PaxType.ADULT).intValue(),
				searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0).intValue(),
				searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0).intValue(),
				searchQuery.getCabinClass().getCode());

		String miscKey = StringUtils.join(supplierConf.getBasicInfo().getRuleId(), searchQuery.getPrefferedAirline());
		return type + sectorKey + paxKey + miscKey;
	}

	public AirSearchResult getCacheResult(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		AirSearchResult searchResult = null;
		try {
			for (TripInfoType type : TripInfoType.values()) {
				String key = generateKey(type.name(), searchQuery, supplierConf);
				if (StringUtils.isNotBlank(key)) {
					List<TripInfo> trips = cachingService.fetchValue(key, TripInfo.class,
							CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.getName()).build());
					if (CollectionUtils.isNotEmpty(trips)) {
						if (searchResult == null) {
							searchResult = new AirSearchResult();
						}

						/**
						 * To remove special fare from the cached special return.
						 */
						if (((searchQuery.isOneWay() || searchQuery.isMultiCity()) && searchQuery.getIsDomestic())
								&& searchResult != null) {
							trips = processSpecialFare(trips);
						}
						searchResult.getTripInfos().put(type.name(), trips);
					}
				}
			}
			if (searchQuery.isDomesticReturn() && searchResult != null) {
				if (Collections.isEmpty(searchResult.getTripInfos().get(TripInfoType.ONWARD.getName()))
						|| Collections.isEmpty(searchResult.getTripInfos().get(TripInfoType.RETURN.getName()))) {
					return null;
				}
			}
			return searchResult;
		} catch (Exception e) {
			log.error("Unable to store flight listing into cache for searchQuery {} ", searchQuery, e);
		} finally {
			if (searchResult == null) {
				log.debug("Unable to find searchresult in cache for searchQuery{}", searchQuery);
			}
		}
		return searchResult;
	}

	/*
	 * This functionality will remove special fare trip from the entire search result.
	 */
	private List<TripInfo> processSpecialFare(List<TripInfo> trips) {
		List<TripInfo> newTrips = new ArrayList<>();
		for (TripInfo tripInfo : trips) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return priceInfo.getFareIdentifier() == null
							|| !priceInfo.getFareIdentifier().equals(FareType.SPECIAL_RETURN);
				}).collect(Collectors.toList());
				segmentInfo.setPriceInfoList(priceInfoList);
			}
			if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos().get(0).getPriceInfoList())) {
				newTrips.add(tripInfo);
			}
		}
		return newTrips;
	}

	public boolean deleteCache(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		String key = null;
		try {
			for (TripInfoType type : TripInfoType.values()) {
				key = generateKey(type.getName(), searchQuery, supplierConf);
				cachingService.deleteRecord(
						CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.getName()).key(key).build());
			}
		} catch (Exception e) {
			log.error("Unable to delete cache for key {}, searchQuery {}", key, searchQuery, e);
		}
		return true;
	}

	public static Integer getSearchPriceIdExpiration() {
		return CacheType.PRICEIDKEYATSEARCH.getTtl();
	}

	public void storeTripKey(AirSearchResult searchResult, AirSearchQuery searchQuery) {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			searchResult.getTripInfos().forEach((type, trips) -> {
				if (CollectionUtils.isNotEmpty(trips)) {
					try {
						String searchKey = searchQuery.getSearchId();
						cachingService.store(searchKey, BinName.SEARCHQUERYBIN.getName(), searchQuery,
								CacheMetaInfo.builder().set(CacheSetName.SEARCHQUERY.name()).compress(true)
										.plainData(false).expiration(getSearchPriceIdExpiration()).build());
						processAndStore(trips, searchKey, type);
					} catch (Exception e) {
						log.error("Unable to store tripInfo List for searchQuery {} ", searchQuery, e);
					}
				}
			});
		}
	}

	public void storeLowFare(List<TripInfo> tripInfoList, AirSearchQuery searchQuery, String tripType) {
		try {
			int routeInfos = 0;
			if (tripType.equals(TripInfoType.RETURN.getTripType())) {
				routeInfos = 1;
			}

			String binName = searchQuery.getRouteInfos().get(routeInfos).getTravelDate().toString();
			String fromAirportCode = searchQuery.getRouteInfos().get(routeInfos).getFromCityOrAirport().getCode();
			String toAirportCode = searchQuery.getRouteInfos().get(routeInfos).getToCityOrAirport().getCode();
			String key = fromAirportCode + "-" + toAirportCode;
			LowFareInfo existingLowFare =
					cachingService.fetchValue(key, LowFareInfo.class, CacheSetName.LOW_FARE.getName(), binName);
			LowFareInfo newLowFare = null;
			for (TripInfo trip : tripInfoList) {
				LowFareInfo tempLowFare = getLowestFareFromSegments(trip.getSegmentInfos());
				if (newLowFare == null || newLowFare.getFare() > tempLowFare.getFare()) {
					newLowFare = tempLowFare;
				}
			}
			if (Objects.isNull(existingLowFare) || newLowFare.getCode().equals(existingLowFare.getCode())
					|| newLowFare.getFare() < existingLowFare.getFare()) {
				cachingService.store(key, binName, newLowFare, CacheMetaInfo.builder().set(CacheSetName.LOW_FARE.name())
						.compress(true).plainData(false).expiration(CacheType.LOWAIRFARE.getTtl()).build());
			}
		} catch (Exception e) {
			log.error("Unable to store lowFare for searchQuery {}", searchQuery, e);
		}
	}

	private LowFareInfo getLowestFareFromSegments(List<SegmentInfo> segmentInfoList) {
		LowFareInfo lowFareInfo = new LowFareInfo();
		Double lowestFare = new Double(0);
		int priceInfos = segmentInfoList.get(0).getPriceInfoList().size();
		for (int priceIndex = 0; priceIndex < priceInfos; priceIndex++) {
			Double segmentPrice = new Double(0);
			for (SegmentInfo segment : segmentInfoList) {
				PriceInfo priceInfo = segment.getPriceInfo(priceIndex);
				if (!FareType.SPECIAL_RETURN.equals(priceInfo.getFareIdentifier())
						&& MapUtils.isNotEmpty(priceInfo.getFareDetail(PaxType.ADULT).getFareComponents())) {
					FareDetail fareDetail = priceInfo.getFareDetail(PaxType.ADULT);
					segmentPrice += fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0);
				}
			}
			if (lowestFare.doubleValue() == 0d || lowestFare > segmentPrice) {
				lowestFare = segmentPrice;
			}
		}
		lowFareInfo.setFare(lowestFare);
		lowFareInfo.setCode(segmentInfoList.get(0).getFlightDesignator().getAirlineInfo().getCode());
		return lowFareInfo;
	}

	public String getSegmentKey(List<SegmentInfo> segmentInfos) {
		String key = "";
		for (SegmentInfo segmentInfo : segmentInfos) {
			key = StringUtils.join(key, segmentInfo.getDepartAirportInfo().getCode().trim(),
					segmentInfo.getArrivalAirportInfo().getCode().trim(),
					segmentInfo.getFlightDesignator().getAirlineInfo().getCode().trim(),
					segmentInfo.getFlightDesignator().getFlightNumber().trim());
		}
		return key;
	}

	public String generateUniqueId(TripInfo tripInfo, String searchId, int tripIndex) {
		return searchId + DELIM + tripIndex + getSegmentKey(tripInfo.getSegmentInfos());
	}

	public void storeSearchQueryForCombiSearch(AirSearchQuery searchQuery, TripInfo tripInfo) {
		cachingService.store(getSegmentKey(tripInfo.getSegmentInfos()), BinName.SEARCHQUERYBIN.getName(), searchQuery,
				CacheMetaInfo.builder().set(CacheSetName.SEARCHQUERY.name()).compress(true).plainData(false)
						.expiration(getSearchPriceIdExpiration()).build());
	}

	public AirSearchQuery fetchSearchQueryForCombiSearch(TripInfo tripInfo) {
		return cachingService.fetchValue(getSegmentKey(tripInfo.getSegmentInfos()), AirSearchQuery.class,
				CacheSetName.SEARCHQUERY.getName(), BinName.SEARCHQUERYBIN.getName());
	}

	public void processAndStore(List<TripInfo> trips, String searchId, String tripType) {
		int tripIndex = 0;
		for (TripInfo tripInfo : trips) {
			try {
				// it should be unique even if same flight numbers/airline in trips available
				// this uniqueTripId is used in frontend to remove duplicates for Dealinventory and normal fare after
				// merging at frontend
				String uniqueTripId = generateUniqueId(tripInfo, searchId, tripIndex++);
				tripInfo.setId(uniqueTripId);
				for (int i = 0; i < tripInfo.getSegmentInfos().size(); i++) {
					ListIterator<PriceInfo> iter = tripInfo.getSegmentInfos().get(i).getPriceInfoList().listIterator();
					int j = 0;
					while (iter.hasNext()) {
						iter.next();
						String id = uniqueTripId + DELIM + System.nanoTime();
						if (j == tripInfo.getSegmentInfos().get(0).getPriceInfoList().size()) {
							iter.remove();
							continue;
						}

						if (i >= 1) {
							id = tripInfo.getSegmentInfos().get(0).getPriceInfoList().get(j).getId();
						}
						tripInfo.getSegmentInfos().get(i).getPriceInfoList().get(j).setId(id);
						j++;
					}

				}
				tripInfo.getSegmentInfos().get(0).getPriceInfoList().forEach(priceInfo -> {
					cachingService.store(priceInfo.getId(), BinName.PRICEINFOBIN.getName(), tripInfo,
							CacheMetaInfo.builder().set(CacheSetName.PRICE_INFO.name()).compress(true).plainData(false)
									.expiration(getSearchPriceIdExpiration()).build());
				});
			} catch (Exception e) {
				log.error("Unable to persist tripInfo {}", tripInfo, e);
			}
		}
	}

	public static String getSearchQueryKey(String key) {
		return key.substring(0, key.indexOf(DELIM));
	}
}
