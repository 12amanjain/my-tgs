package com.tgs.services.fms.helper;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;
import com.tgs.services.base.enums.FareComponent;
import lombok.Getter;

@Getter
public enum GDSFareComponentMapper {

	// The Gds fares should have unique names in this enum
	YQ(FareComponent.YQ),
	YQI(FareComponent.YQ),
	TX(FareComponent.AT),
	WO(FareComponent.WO),
	K3(FareComponent.AGST),
	K38(FareComponent.AGST),
	PSF(FareComponent.PSF),
	IN(FareComponent.UDF),
	IN4(FareComponent.UDF),
	YM(FareComponent.YM),
	YR(FareComponent.YR),
	YRI(FareComponent.YR),
	SG(FareComponent.PSF),
	OB(FareComponent.OB),
	YQF(FareComponent.YQ);

	private FareComponent fareComponent;

	private static Map<String, GDSFareComponentMapper> gdsFareComponents;

	public static Map<String, GDSFareComponentMapper> getGdsFareComponents() {
		return gdsFareComponents;
	}

	static {
		gdsFareComponents = Arrays.stream(GDSFareComponentMapper.values())
				.collect(Collectors.toMap(f -> f.toString().trim(), f -> f));
	}

	GDSFareComponentMapper(FareComponent fareComponent) {
		this.fareComponent = fareComponent;
	}
}
