package com.tgs.services.fms.helper;

import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;

@Service
public class ServiceCommunicatorHelper {

	@Autowired
	private static GeneralServiceCommunicator gnService;

	private static ConfiguratorInfo configuratorInfo;

	@Autowired
	public ServiceCommunicatorHelper(GeneralServiceCommunicator gnService) {
		ServiceCommunicatorHelper.gnService = gnService;
	}

	public static ClientGeneralInfo getClientInfo() {

		if (Objects.isNull(configuratorInfo)) {
			configuratorInfo = gnService.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO);
			return (ClientGeneralInfo) configuratorInfo.getOutput();
		}

		return (ClientGeneralInfo) configuratorInfo.getOutput();
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 300 * 1000)
	public void setConfiguratorInfo() {
		configuratorInfo = gnService.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO);
	}
}
