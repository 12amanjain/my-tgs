package com.tgs.services.fms.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.aerospike.client.Bin;
import com.aerospike.client.Operation;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.NonOperatingSectorInfo;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SourceRouteInfoHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	SourceRouteInfoService service;

	@Autowired
	public SourceRouteInfoHelper(CustomInMemoryHashMap airportHashMap, GeneralCachingCommunicator cachingCommunicator) {
		super(configurationHashMap);
		SourceRouteInfoHelper.cachingCommunicator = cachingCommunicator;
	}

	private static final String FIELD = "route_list";

	@Override
	public void process() {
		Runnable fetchSourceRouteInfoTask = () -> {
			log.info("Fetching source route data from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<SourceRouteInfo> routeList = DbSourceRouteInfo.toDomainList(service.findAll(page));
				if (CollectionUtils.isEmpty(routeList))
					break;
				log.debug("Fetched source route info from database, info list size is {}", routeList.size());
				Map<String, List<SourceRouteInfo>> sectorWiseMapping = routeList.stream()
						.collect(Collectors.groupingBy(routeInfo -> getKey(routeInfo.getSrc(), routeInfo.getDest())));
				sectorWiseMapping.forEach((key, value) -> {
					cachingCommunicator.store(
							CacheMetaInfo.builder().set(CacheSetName.ROUTE_INFO.getName())
									.namespace(CacheNameSpace.STATIC_MAP.getName()).key(key).build(),
							FIELD, value, false, false, InMemoryInitializer.NEVER_EXPIRE);
				});
			}
		};

		Thread fetchRouteInfoThread = new Thread(fetchSourceRouteInfoTask);
		fetchRouteInfoThread.start();
	}

	public static String getKey(String src, String dest) {
		return String.join("-", src, dest);
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.ROUTE_INFO.getName())
				.namespace(CacheNameSpace.STATIC_MAP.getName()).build());
	}

	@SuppressWarnings("serial")
	public static boolean isValidRoute(NonOperatingSectorInfo sourceRouteInfo) {

		/**
		 * To check whether the sourceId has faced any NOT_OPERATING_SECTOR in any previous searches.
		 */
		/*
		 * if (!SourceRouteInfoHelper.isOperatingSector(sourceRouteInfo)) {
		 * log.info("Not Operating sector {} src {} dest {} ", sourceRouteInfo.getSourceId(),
		 * sourceRouteInfo.getSource(), sourceRouteInfo.getDestination()); return false; }
		 */

		List<String> keys =
				Arrays.asList(getKey(sourceRouteInfo.getSource(), sourceRouteInfo.getDestination()), getKey("*", "*"),
						getKey(sourceRouteInfo.getSource(), "*"), getKey("*", sourceRouteInfo.getDestination()));

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.STATIC_MAP.getName())
				.set(CacheSetName.ROUTE_INFO.getName()).keys(keys.toArray(new String[0]))
				.typeOfT(new TypeToken<List<SourceRouteInfo>>() {}.getType()).build();
		Map<String, Map<String, List<SourceRouteInfo>>> routeMap = cachingCommunicator.get(metaInfo, null);

		if (MapUtils.isNotEmpty(routeMap)) {
			for (Entry<String, Map<String, List<SourceRouteInfo>>> entry : routeMap.entrySet()) {
				if (keys.contains(entry.getKey())) {
					for (SourceRouteInfo info : entry.getValue().get(FIELD)) {
						if (info.getSourceId().equals(sourceRouteInfo.getSourceId())
								&& BooleanUtils.isTrue(info.getEnabled()))
							return true;
					}
				}
			}
		}
		return false;
	}

	/**
	 * @implSpec :Caching NonOperating sectors, So we can avoid same sector search for the particular sourceId for
	 *           particular days (be default specidfied to two days)
	 */
	public static void addNonOperatingSector(NonOperatingSectorInfo sectorInfo) {

		String key = getNonOperatingSectorKey(sectorInfo);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.NON_OPERATIONAL_SECTOR.getName())
				.namespace(CacheNameSpace.STATIC_MAP.getName()).expiration(CacheType.NON_OPERATIONAL_SECTORS.getTtl())
				.key(key).build();
		Map<String, String> binMap = new HashMap<>();

		/**
		 * @param isOperational used to determine operating status for the searching sectors.
		 */

		binMap.put(BinName.ROUTEINFO.getName(), GsonUtils.getGson().toJson(sectorInfo));
		cachingCommunicator.store(metaInfo, binMap, false, true, CacheType.NON_OPERATIONAL_SECTORS.getTtl());
	}

	/**
	 * @implSpec : generate the key from sourceRouteInfo (vice versa for onward, return)
	 */
	public static boolean isOperatingSector(NonOperatingSectorInfo sourceRouteInfo) {
		String key = getNonOperatingSectorKey(sourceRouteInfo);
		NonOperatingSectorInfo returnRouteInfo =
				new GsonMapper<>(sourceRouteInfo, NonOperatingSectorInfo.class).convert();
		returnRouteInfo.setSource(sourceRouteInfo.getDestination());
		returnRouteInfo.setDestination(sourceRouteInfo.getSource());
		String returnKey = getNonOperatingSectorKey(returnRouteInfo);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.STATIC_MAP.getName())
				.set(CacheSetName.NON_OPERATIONAL_SECTOR.getName())
				.keys(Arrays.asList(key, returnKey).toArray(new String[0])).build();
		Map<String, Map<String, String>> resultSet = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(resultSet)
				&& ((resultSet.get(key) != null && resultSet.get(key).get(BinName.ROUTEINFO.getName()) != null)
						|| (resultSet.get(returnKey) != null
								&& resultSet.get(returnKey).get(BinName.ROUTEINFO.getName()) != null))) {
			return false;
		}
		return true;
	}

	public static String getNonOperatingSectorKey(NonOperatingSectorInfo sourceRouteInfo) {
		if (sourceRouteInfo != null && ObjectUtils.allNotNull(sourceRouteInfo.getSource(),
				sourceRouteInfo.getDestination(), sourceRouteInfo.getSourceId())) {
			StringJoiner joiner = new StringJoiner("_");
			joiner.add(sourceRouteInfo.getSource());
			joiner.add(sourceRouteInfo.getDestination());
			joiner.add(sourceRouteInfo.getSourceId().toString());
			return StringUtils.join(joiner.toString());
		}
		return null;
	}

	/**
	 * 1.This function will increment @param noResultCount for every search request. <br>
	 * 2.If the count is greater than @param noSearchResultTreshold it will be treated as NoResultSector. <br>
	 * 3.The next search will be triggered only if @param noResultCount is greater than @param noSearchResultTreshold
	 * and (@param noResultCount % @param noSearchResultBufferCount) is equal to 0.
	 * 
	 * @param routes
	 * @param sourceId
	 * @return
	 */
	public static boolean isNoResultSector(List<RouteInfo> routes, Integer sourceId, User bookingUser) {
		String key = SourceRouteInfoHelper.getNoResultSectorKey(routes, sourceId);
		boolean isNoResultSector = false;
		AirGeneralPurposeOutput gnPurpose =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));

		// In case of deal inventory no result sector check is not required.
		if (sourceId == null || (gnPurpose.getNoResultSectorDisabledSource() != null
				&& gnPurpose.getNoResultSectorDisabledSource().contains(sourceId))) {
			return isNoResultSector;
		}

		Long noResultCount = SourceRouteInfoHelper.addAndFetcNoResultSearchCount(key);

		Integer noSearchResultThreshold = gnPurpose.getNoSearchResultThreshold();
		Integer noSearchResultBufferCount = gnPurpose.getNoSearchResultBufferCount();
		if (noSearchResultThreshold != null && noResultCount > noSearchResultThreshold) {
			if (noResultCount % noSearchResultBufferCount != 0) {
				isNoResultSector = true;
			}
		}

		return isNoResultSector;
	}

	/*
	 * This will do atomic increment operation on count bin and return the value.
	 */
	public static Long addAndFetcNoResultSearchCount(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(key).namespace(CacheNameSpace.STATIC_MAP.name())
				.plainData(true).set(CacheSetName.NO_RESULTS_SECTOR.getName()).build();
		Bin bin = new Bin(BinName.COUNT.getName(), 1);
		Long noResultCount = cachingCommunicator.performOperationsAndFetchBinValue(metaInfo, BinName.COUNT.getName(),
				Long.class, -1, Operation.add(bin), Operation.get(BinName.COUNT.getName()));
		return noResultCount == null ? 0 : noResultCount;
	}

	public static void deleteNoResultKey(List<RouteInfo> routes, Integer sourceId) {
		String key = SourceRouteInfoHelper.getNoResultSectorKey(routes, sourceId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(key).namespace(CacheNameSpace.STATIC_MAP.name())
				.plainData(true).set(CacheSetName.NO_RESULTS_SECTOR.getName()).build();
		cachingCommunicator.delete(metaInfo);
	}

	private static String getNoResultSectorKey(List<RouteInfo> routes, Integer sourceId) {
		StringJoiner joiner = new StringJoiner("_");
		for (RouteInfo route : routes) {
			joiner.add(route.getFromCityAirportCode());
			joiner.add(route.getToCityAirportCode());
		}
		joiner.add(sourceId.toString());
		return joiner.toString();
	}

}
