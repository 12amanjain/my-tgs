package com.tgs.services.fms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.TgsSecurityUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.datamodel.supplier.SupplierRuleFilter;
import com.tgs.services.fms.dbmodel.DbSupplierRule;
import com.tgs.services.fms.jparepository.SupplierInfoService;
import com.tgs.services.fms.jparepository.SupplierRuleService;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SupplierConfigurationHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap supplierInfoHashMap;

	private static CustomInMemoryHashMap supplierRules;

	private static CustomInMemoryHashMap suppliers;

	private static Map<String, List<SupplierInfo>> inMemorysourceIdWiseSuppliers = new HashMap<>();

	private static Map<String, SupplierInfo> inMemorySupplierInfo = new HashMap<>();

	private static Map<Integer, List<SupplierRule>> inMemorySupplierRuleMap = new HashMap<>();

	@Autowired
	SupplierInfoService supplierInfoService;

	@Autowired
	SupplierRuleService ruleService;

	@Autowired
	public SupplierConfigurationHelper(CustomInMemoryHashMap supplierCredentialMap,
			CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap supplierRules,
			CustomInMemoryHashMap suppliers) {
		super(null);
		SupplierConfigurationHelper.supplierInfoHashMap = supplierCredentialMap;
		SupplierConfigurationHelper.supplierRules = supplierRules;
		SupplierConfigurationHelper.suppliers = suppliers;

	}

	private static final String FIELD = "air_supplier";
	private static final String FIELD1 = "air_supp_rules";
	private static final String FIELD2 = "air_sourceid";

	private static String getKey(String supplierId) {
		return supplierId;
	}

	@Override
	public void process() {
		processInMemory();

		if (MapUtils.isEmpty(inMemorysourceIdWiseSuppliers) || MapUtils.isEmpty(inMemorySupplierRuleMap)
				|| MapUtils.isEmpty(inMemorySupplierInfo)) {
			return;
		}

		// modifyTTLOfSet();

		inMemorySupplierInfo.forEach((supplierId, info) -> {
			supplierInfoHashMap.put(supplierId, FIELD, info, CacheMetaInfo.builder().compress(false)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.AIR_SUPPLIER.getName()).build());
		});

		inMemorysourceIdWiseSuppliers.forEach((key, value) -> {
			suppliers.put(key, FIELD2, value, CacheMetaInfo.builder().compress(true)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.AIR_SUPPLIER.getName()).build());
		});

		inMemorySupplierRuleMap.forEach((key, value) -> {
			supplierRules.put(key.toString(), FIELD1, value, CacheMetaInfo.builder().compress(true)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.AIR_SUPPLIER.getName()).build());
		});
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		Map<String, List<SupplierInfo>> sourceIdWiseSuppliers = new HashMap<>();
		Map<String, SupplierInfo> supplierInfoMap = new HashMap<>();
		supplierInfoService.findAll().forEach(supplierInfo -> {
			SupplierInfo info = supplierInfo.toDomain(false);
			supplierInfoMap.put(getKey(info.getSupplierId()), info);
			List<SupplierInfo> supplierInfos =
					sourceIdWiseSuppliers.getOrDefault(String.valueOf(info.getSourceId()), new ArrayList<>());
			supplierInfos.add(info);
			sourceIdWiseSuppliers.put(String.valueOf(info.getSourceId()), supplierInfos);
		});
		inMemorySupplierInfo = supplierInfoMap;
		inMemorysourceIdWiseSuppliers = sourceIdWiseSuppliers;

		List<DbSupplierRule> dbSupplierRules = ruleService.findAll(SupplierRuleFilter.builder().build());

		List<SupplierRule> dMSupplierRules = new ArrayList<>();
		dbSupplierRules.forEach(dbSupplierRule -> {
			SupplierRule rule = dbSupplierRule.toDomain();
			rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()).setAirType(rule.getAirType());
			rule.getInclusionRuleCriteria(new FlightBasicRuleCriteria()).setSearchType(rule.getSearchType());
			dMSupplierRules.add(rule);
		});

		inMemorySupplierRuleMap = dMSupplierRules.stream().collect(Collectors.groupingBy(SupplierRule::getSourceId));

	}

	public static SupplierInfo getSupplierInfo(String supplierId) {
		SupplierInfo supplierInfo = inMemorySupplierInfo.get(getKey(supplierId));
		supplierInfo = new GsonMapper<SupplierInfo>(supplierInfo, SupplierInfo.class).convert();
		log.debug("Supplier info fetched for supplierId {} from cache: {}", supplierId, supplierInfo);
		if (supplierInfo != null) {
			SupplierCredential creds = supplierInfo.getCredentialInfo();
			try {
				creds.setPassword(TgsSecurityUtils.decryptData(creds.getPassword()));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return supplierInfo;
	}

	public static List<SupplierInfo> getSupplierForSourceId(String sourceId) {
		log.debug("In getSupplierForSourceId ");
		Gson gson = GsonUtils.getGson();
		String json = gson.toJson(inMemorysourceIdWiseSuppliers.get(sourceId));
		List<SupplierInfo> supplierInfos = gson.fromJson(json, new TypeToken<List<SupplierInfo>>() {}.getType());
		log.debug("Fetched supplierInfos");

		if (supplierInfos == null)
			supplierInfos = new ArrayList<>();

		if ("11".equals(sourceId))
			return supplierInfos;

		supplierInfos.forEach(si -> {
			SupplierCredential creds = si.getCredentialInfo();
			try {
				creds.setPassword(TgsSecurityUtils.decryptData(creds.getPassword()));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		});
		log.debug("SupplierInfos decrypted");
		return supplierInfos;
	}

	public List<String> getSupplierUserNames(String sourceId, Boolean status) {
		List<SupplierInfo> supplierInfos = getSupplierForSourceId(sourceId);
		return supplierInfos.stream().filter(supp -> supp.getEnabled() == status)
				.map(supp -> supp.getCredentialInfo().getUserName()).collect(Collectors.toList());
	}

	public static List<SupplierRule> getSupplierRules(Integer sourceId) {
		Gson gson = GsonUtils.getGson();
		String json = gson.toJson(inMemorySupplierRuleMap.get(sourceId));
		List<SupplierRule> rules = gson.fromJson(json, new TypeToken<List<SupplierRule>>() {}.getType());
		if (CollectionUtils.isEmpty(rules)) {
			log.info("Supplier Rules is empty for sourceId {}", sourceId);
		}
		return rules;
	}

	public static List<SupplierRule> getSupplierRules(Integer sourceId, Boolean status) {
		List<SupplierRule> suppliers = getSupplierRules(sourceId);
		if (status != null) {
			suppliers =
					suppliers.stream().filter(supplier -> supplier.getEnabled() == status).collect(Collectors.toList());
		}
		return suppliers;
	}

	public static SupplierRule getSupplierRule(Integer sourceId, Long ruleId) {
		log.debug("searching for supplierrules for sourceId {},ruleId {} ", sourceId, ruleId);
		getSupplierRules(sourceId).forEach(supplierRule -> {
			log.debug("Got Supplier Rules {} ", supplierRule);
		});

		return getSupplierRules(sourceId).stream().filter(supplierRule -> {
			return supplierRule.getId() == ruleId.longValue();
		}).findFirst().orElse(null);
	}

	public static SupplierRule getSupplierRule(Integer sourceId, String supplierId) {
		return getSupplierRules(sourceId).stream().filter(supplierRule -> {
			return supplierRule.getSupplierId().equals(supplierId);
		}).findFirst().orElse(null);
	}

	public static SupplierConfiguration getSupplierConfiguration(String supplierId) {
		SupplierInfo si = getSupplierInfo(supplierId);
		SupplierRule supplierRule = getSupplierRule(si.getSourceId(), supplierId);
		return buildSupplierConfiguration(supplierRule, si.getSourceId(), supplierId, false);
	}

	public static SupplierConfiguration getSupplierConfiguration(Integer sourceId, Long ruleId) {
		SupplierRule supplierRule = getSupplierRule(sourceId, ruleId);
		return buildSupplierConfiguration(getSupplierRule(sourceId, ruleId), sourceId, supplierRule.getSupplierId(),
				false);
	}

	public static SupplierConfiguration getSupplierConfiguration(Integer sourceId, Long ruleId, String supplierId) {
		return buildSupplierConfiguration(getSupplierRule(sourceId, ruleId), sourceId, supplierId, false);
	}

	public static SupplierConfiguration buildSupplierConfiguration(SupplierRule supplierRule, Integer sourceId,
			String supplierId, boolean isConsiderDisable) {

		SupplierInfo supplierInfo = getSupplierInfo(supplierId);

		if (supplierInfo != null && ((isConsiderDisable && BooleanUtils.isFalse(supplierInfo.getIsDeleted()))
				|| BooleanUtils.isTrue(supplierInfo.getEnabled()))) {
			// SupplierConfiguration while search should be done only for enabled
			SupplierBasicInfo basicInfo = SupplierBasicInfo.builder().ruleId(supplierRule.getId())
					.supplierId(supplierInfo.getSupplierId()).supplierName(supplierInfo.getName()).sourceId(sourceId)
					.description(supplierRule.getDescription()).priority(supplierRule.getPriority()).build();
			return SupplierConfiguration.builder().supplierAdditionalInfo(supplierRule.getSupplierAdditionalInfo())
					.supplierCredential(supplierInfo.getCredentialInfo()).basicInfo(basicInfo).build();
		}
		return null;
	}

	@Override
	public void deleteExistingInitializer() {
		if (inMemorySupplierRuleMap != null) {
			inMemorySupplierRuleMap.clear();
		}
		if (inMemorysourceIdWiseSuppliers != null) {
			inMemorysourceIdWiseSuppliers.clear();
		}
		if (inMemorySupplierInfo != null) {
			inMemorySupplierInfo.clear();
		}
		supplierRules.truncate(CacheSetName.AIR_SUPPLIER.getName());
	}

	public void modifyTTLOfSet() {
		try {
			supplierRules.modifyTtl(CacheSetName.AIR_SUPPLIER.getName(), 10);
		} catch (Exception e) {
			deleteExistingInitializer();
		}
	}

	/*
	 * public static SupplierConfiguration getSupplierConfiguration(AirSearchQuery searchQuery, int sourceId) { return
	 * SupplierConfigurationHelper.getSupplierConfiguration(sourceId, sourceId); }
	 */

	public static List<SupplierConfiguration> getValidSearchSuppliers(AirSearchQuery searchQuery, int sourceId,
			User user) {
		List<SupplierConfiguration> supplierConfigurations = new ArrayList<>();
		List<SupplierRule> supplierRules = getSupplierRules(sourceId);
		List<SupplierRule> matchingRules = null;

		if (!searchQuery.isPNRCreditSearch()) {
			log.debug("Total supplier rule for sourceId {} is size {}", sourceId,
					TgsCollectionUtils.size(supplierRules));
			if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())
					&& CollectionUtils.isNotEmpty(supplierRules)) {
				Iterator<SupplierRule> iterator = supplierRules.iterator();
				while (iterator.hasNext()) {
					SupplierRule supplierRule = iterator.next();
					if (Objects.nonNull(supplierRule.getSupplierAdditionalInfo()) && CollectionUtils
							.isNotEmpty(supplierRule.getSupplierAdditionalInfo().getIncludedAirlines())) {
						boolean isRuleApplicable = false;
						for (AirlineInfo airline : searchQuery.getPreferredAirline()) {
							if (supplierRule.getSupplierAdditionalInfo().getIncludedAirlines()
									.contains(airline.getCode())) {
								isRuleApplicable = true;
							}
						}
						// remove here
						if (!isRuleApplicable) {
							iterator.remove();
						}

					}
				}
			}
			Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

			FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(searchQuery);
			BaseUtils.createFactOnUser(flightFact, user);
			CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(supplierRules, flightFact, fieldResolverMap);
			matchingRules = (List<SupplierRule>) ruleEngine.fireAllRules();
		} else if (CollectionUtils.isNotEmpty(supplierRules)) {
			List<String> suppliers = searchQuery.getSupplierIds();
			Iterator<SupplierRule> iterator = supplierRules.iterator();
			while (iterator.hasNext()) {
				SupplierRule supplierRule = iterator.next();
				if (!suppliers.contains(supplierRule.getSupplierId())) {
					iterator.remove();
				}
			}
			matchingRules = supplierRules;
		}

		log.debug("Total matchingRules rule for sourceId {} is size {}", sourceId,
				TgsCollectionUtils.size(matchingRules));
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			matchingRules.forEach(supplierRule -> {
				SupplierConfiguration supplierConf = buildSupplierConfiguration(supplierRule, sourceId,
						supplierRule.getSupplierId(), isConsiderDisableRule(searchQuery));

				if (supplierConf != null) {
					/**
					 * If supplierIds is already set then only relevant supplier should return
					 */
					if (searchQuery.getSupplierIds() != null) {
						if (searchQuery.getSupplierIds().contains(supplierConf.getSupplierId()))
							supplierConfigurations.add(supplierConf);
					} else {
						supplierConfigurations.add(supplierConf);
					}
				}
			});
		} else {
			log.debug("No valid supplier rule is configured for sourceId {}, searchQuery {}", sourceId, searchQuery);
		}

		return supplierConfigurations;
	}

	private static boolean isConsiderDisableRule(AirSearchQuery searchQuery) {
		return searchQuery.isPNRCreditSearch() && CollectionUtils.isNotEmpty(searchQuery.getSupplierIds());
	}

}
