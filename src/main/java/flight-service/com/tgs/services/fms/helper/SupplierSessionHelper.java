package com.tgs.services.fms.helper;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Map.Entry;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.aerospike.client.query.Filter;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.jparepository.SupplierSessionService;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;

@Service
public class SupplierSessionHelper {

	@Autowired
	protected GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private SupplierSessionService sessionService;

	public void save(SupplierSession session) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.BOOKINGID.getName(), session.getBookingId());
		binMap.put(BinName.SESSIONINFO.getName(), GsonUtils.getGson().toJson(session));
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.SUPPLIER_SESSION.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).key(getSessionKey(session)).build();
		cachingCommunicator.store(metaInfo, binMap, false, true, session.getTtl().intValue());
	}

	public List<SupplierSession> getBookingSession(String bookingId) {
		List<SupplierSession> supplierSessionList = new ArrayList<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.SUPPLIER_SESSION.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).build();
		Map<String, Map<String, String>> supplierSessionMap = cachingCommunicator.getResultSet(metaInfo, String.class,
				Filter.equal(BinName.BOOKINGID.getName(), bookingId));
		for (Entry<String, Map<String, String>> entryMap : supplierSessionMap.entrySet()) {
			supplierSessionList.add(GsonUtils.getGson().fromJson(entryMap.getValue().get(BinName.SESSIONINFO.getName()),
					SupplierSession.class));
		}
		return supplierSessionList;
	}

	private String getSessionKey(SupplierSession session) {
		String sessionKey = StringUtils.EMPTY;
		if (StringUtils.isNotEmpty(session.getBookingId()) && StringUtils.isNotEmpty(session.getSupplierId())) {
			sessionKey = StringUtils.join(session.getSupplierId(), "_", session.getBookingId());
			if (session.getSupplierSessionInfo() != null
					&& StringUtils.isNotEmpty(session.getSupplierSessionInfo().getTripKey())) {
				sessionKey = StringUtils.join(sessionKey, "_", session.getSupplierSessionInfo().getTripKey());
			}
		}
		return sessionKey;
	}

	/**
	 * @implSpec <br>
	 *           this can be used to get session based on the trip. with tripkey it will return session for that trip,
	 *           without tripkey it will return first session for the bookingId.
	 */
	public SupplierSession getSessionByTrip(String bookingId, String tripKey) {
		List<SupplierSession> sessionList = getBookingSession(bookingId);
		List<SupplierSession> validSessionList = removeExpiredSessionsFromCache(sessionList);
		SupplierSession supplierSession = null;
		if (CollectionUtils.isNotEmpty(validSessionList)) {
			if (StringUtils.isBlank(tripKey)) {
				return validSessionList.get(0);
			} else {
				for (SupplierSession session : validSessionList) {
					if (session.getSupplierSessionInfo() != null
							&& session.getSupplierSessionInfo().getTripKey() != null
							&& session.getSupplierSessionInfo().getTripKey().contains(tripKey)) {
						supplierSession = session;
						break;
					}
				}
			}
		}
		return supplierSession;
	}

	public List<SupplierSession> removeExpiredSessionsFromCache(List<SupplierSession> supplierSessionList) {
		List<SupplierSession> expiredSessions = new ArrayList<>();
		for (int index = 0; index < supplierSessionList.size(); index++) {
			SupplierSession supplierSession = supplierSessionList.get(index);
			if (supplierSession.getExpiryTime().compareTo(LocalDateTime.now()) < 0) {
				expiredSessions.add(supplierSession);
				supplierSessionList.remove(supplierSession);
			}
		}
		removeSessions(expiredSessions);
		return supplierSessionList;
	}

	public void removeSessions(List<SupplierSession> supplierSessionList) {
		if (CollectionUtils.isNotEmpty(supplierSessionList)) {
			for (SupplierSession supplierSession : supplierSessionList) {
				CacheMetaInfo expiredCacheMetaInfo =
						CacheMetaInfo.builder().set(CacheSetName.SUPPLIER_SESSION.getName())
								.namespace(CacheNameSpace.FLIGHT.getName()).key(getSessionKey(supplierSession)).build();
				cachingCommunicator.delete(expiredCacheMetaInfo);
			}
		}
	}

	public void saveToDB(SupplierSession session) {
		sessionService.save(session);
	}

	public SupplierSession getBookingSessionFromDB(String bookingId, SupplierSessionInfo sessionInfo,
			SupplierConfiguration supplierConf) {
		return sessionService.getBookingSession(bookingId, supplierConf.getBasicInfo().getSourceId(),
				supplierConf.getBasicInfo().getSupplierId(), sessionInfo);
	}


	public void cleanSessions(List<SupplierSession> sessions, User user) {
		if (AirUtils.isSupplierSessionCachingEnabled(user)) {
			removeSessions(sessions);
		} else {
			sessionService.removeSessions(sessions);
		}
	}
}
