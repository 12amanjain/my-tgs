package com.tgs.services.fms.helper.esstackInitializer;

import java.util.List;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.fms.dbmodel.DbAirlineInfo;
import com.tgs.services.fms.jparepository.AirlineInfoService;

@Service
public class ESStackAirlineInfoInitializer extends ESStackInitializer {

	@Autowired
	AirlineInfoService service;

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	@Override
	public void process() {
		List<DbAirlineInfo> airlineHashMap = service.findAll();
		esCommunicator.addBulkDocuments(airlineHashMap, ESMetaInfo.AIRLINE);
	}

	@Override
	public void deleteExistingData() {
		esCommunicator.deleteIndex(ESMetaInfo.AIRLINE);
	}

}
