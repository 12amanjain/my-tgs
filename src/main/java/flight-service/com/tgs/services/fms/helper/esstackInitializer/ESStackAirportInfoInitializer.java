package com.tgs.services.fms.helper.esstackInitializer;

import java.util.List;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ESStackInitializer;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.fms.dbmodel.AirportInfo;
import com.tgs.services.fms.jparepository.AirportInfoService;

@Service
public class ESStackAirportInfoInitializer extends ESStackInitializer {

	@Autowired
	AirportInfoService service;

	@Autowired
	ElasticSearchCommunicator esCommunicator;


	@Override
	public void process() {
		List<AirportInfo> airportHaskMap = service.findAll();
		esCommunicator.addBulkDocuments(airportHaskMap, ESMetaInfo.AIRPORT);
	}

	@Override
	public void deleteExistingData() {
		esCommunicator.deleteIndex(ESMetaInfo.AIRPORT);
	}
}
