package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;

@Repository
public interface AirConfiguratorRepository
		extends JpaRepository<DbAirConfiguratorRule, Long>, JpaSpecificationExecutor<DbAirConfiguratorRule> {

	List<DbAirConfiguratorRule> findByRuleTypeOrderByProcessedOnDesc(String ruleType);
}
