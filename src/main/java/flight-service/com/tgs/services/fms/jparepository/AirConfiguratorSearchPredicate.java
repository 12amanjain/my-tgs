package com.tgs.services.fms.jparepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections4.CollectionUtils;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorFilter;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum AirConfiguratorSearchPredicate {

	CREATED_ON {
		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	ENABLED {
		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getEnabled()))
				predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
		}
	},
	RULETYPE {
		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getRuleType()))
				predicates.add(criteriaBuilder.equal(root.get("ruleType"), filter.getRuleType().getName()));
		}
	},
	RULETYPES {

		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getRuleTypes())) {
				List<String> ruleTypes = new ArrayList<>();
				for (AirConfiguratorRuleType type : filter.getRuleTypes()) {
					ruleTypes.add(type.getName());
				}
				Expression<String> expr = root.get("ruleType");
				predicates.add(expr.in(ruleTypes));
			}

		}

	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.isDeleted()));
		}
	},
	EXITONMATCH {
		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getExitOnMatch()))
				predicates.add(criteriaBuilder.equal(root.get("exitOnMatch"), filter.getExitOnMatch()));
		}
	},
	ID {
		@Override
		public void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates) {
			if (filter.getId() != null)
				predicates.add(criteriaBuilder.equal(root.get("id"), filter.getId()));

		}
	};

	public abstract void addPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnAirConfigFilter(Root<DbAirConfiguratorRule> root,
			CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, AirConfiguratorFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (AirConfiguratorSearchPredicate configuratorFilter : AirConfiguratorSearchPredicate.values()) {
			configuratorFilter.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
