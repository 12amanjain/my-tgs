package com.tgs.services.fms.jparepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorFilter;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class AirConfiguratorService {

	@Autowired
	AirConfiguratorRepository airConfiguratorRepository;

	@Autowired
	AirConfiguratorHelper acHelper;

	public DbAirConfiguratorRule save(DbAirConfiguratorRule airConfigRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			airConfigRule.setProcessedOn(LocalDateTime.now());
			airConfigRule = airConfiguratorRepository.saveAndFlush(airConfigRule);
		}
		acHelper.process();
		return airConfigRule;
	}

	public List<DbAirConfiguratorRule> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return airConfiguratorRepository.findAll(sort);
	}

	public List<DbAirConfiguratorRule> findByRuleType(AirConfiguratorRuleType ruleType) {
		return airConfiguratorRepository.findByRuleTypeOrderByProcessedOnDesc(ruleType.getName());
	}

	public List<DbAirConfiguratorRule> findAll(AirConfiguratorFilter queryFilter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(queryFilter);
		Specification<DbAirConfiguratorRule> specification = new Specification<DbAirConfiguratorRule>() {
			@Override
			public Predicate toPredicate(Root<DbAirConfiguratorRule> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates = (AirConfiguratorSearchPredicate.getPredicateListBasedOnAirConfigFilter(root, query, cb,
						queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};
		return airConfiguratorRepository.findAll(specification, request).getContent();
	}

	public DbAirConfiguratorRule findById(long id) {
		return airConfiguratorRepository.findOne(id);
	}
}
