package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.dbmodel.DbAirlineInfo;

@Service
public class AirlineInfoService {

	@Autowired
	AirlineInfoRepository airlineInfoRepo;

	public List<DbAirlineInfo> fetchAirlines(@Param("searchQuery") String searchQuery) {
		return airlineInfoRepo.fetchAirlines(searchQuery);
	}

	public List<DbAirlineInfo> findAll() {
		return airlineInfoRepo.findAll();
	}

	public DbAirlineInfo findByCode(String code) {
		return airlineInfoRepo.findByCode(code);
	}

	public DbAirlineInfo save(DbAirlineInfo airlineInfo) {
		return airlineInfoRepo.saveAndFlush(airlineInfo);
	}
}
