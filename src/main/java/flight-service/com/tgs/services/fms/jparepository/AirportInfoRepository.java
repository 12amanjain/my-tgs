package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.fms.dbmodel.AirportInfo;

public interface AirportInfoRepository extends JpaRepository<AirportInfo, Long>, JpaSpecificationExecutor<AirportInfo> {

	@Query(value = "SELECT a.* FROM airportinfo a WHERE a.code ilike %:searchQuery% or a.name ilike %:searchQuery%",
			nativeQuery = true)
	public List<AirportInfo> fetchAirports(@Param("searchQuery") String searchQuery);

	@Query(value = "SELECT a.* FROM airportinfo a WHERE a.city ilike %:searchQuery%", nativeQuery = true)
	public List<AirportInfo> fetchAirportsByCity(@Param("searchQuery") String searchQuery);

	public AirportInfo findByCode(String code);

	public AirportInfo findByCityContainingIgnoreCase(String city);

}
