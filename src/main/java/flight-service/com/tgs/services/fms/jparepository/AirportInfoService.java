package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.dbmodel.AirportInfo;

@Service
public class AirportInfoService {

	@Autowired
	AirportInfoRepository airportInfoRepo;

	public List<AirportInfo> fetchAirports(@Param("searchQuery") String searchQuery) {
		return airportInfoRepo.fetchAirports(searchQuery);
	}

	public List<AirportInfo> fetchByCity(@Param("searchQuery") String searchQuery) {
		return airportInfoRepo.fetchAirportsByCity(searchQuery);
	}

	public List<AirportInfo> findAll() {
		return airportInfoRepo.findAll();
	}

	public void addAll(List<AirportInfo> airPortInfos) {
		airPortInfos.forEach(airportInfo -> {
			airportInfoRepo.saveAndFlush(airportInfo);
		});
	}

	public AirportInfo findById(Long id) {
		return airportInfoRepo.findOne(id);
	}

	public AirportInfo save(AirportInfo dbAirportInfo) {
		return airportInfoRepo.saveAndFlush(dbAirportInfo);
	}

	public AirportInfo findByCode(String code) {
		return airportInfoRepo.findByCode(code);
	}

	public void deleteById(Long id) {
		airportInfoRepo.delete(id);
	}
}
