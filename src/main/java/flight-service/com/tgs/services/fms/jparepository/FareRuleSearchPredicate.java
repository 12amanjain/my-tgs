package com.tgs.services.fms.jparepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.runtime.database.PsqlFunction;
import com.tgs.services.fms.datamodel.farerule.FareRuleFilter;
import com.tgs.services.fms.dbmodel.DbFareRuleInfo;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum FareRuleSearchPredicate {

	CREATED_ON {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	ENABLED {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getEnabled()))
				predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
		}
	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getDeleted())) {
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.getDeleted()));
			}
		}
	},
	AIRTYPE {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getAirType()))
				predicates.add(criteriaBuilder.equal(root.<String>get("airType"), filter.getAirType().getCode()));
		}
	},
	AIRLINE {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getAirline()))
				predicates.add(criteriaBuilder.equal(root.<String>get("airline"), filter.getAirline()));

		}
	},
	SOURCEID {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getSourceIds())) {
				// inclusioncriteria#>>'{sois}' like '%Y%';
				Expression<String> sidExpression = PsqlFunction.JSONB_EXTRACT_PATH_TEXT.getExpression(criteriaBuilder,
						String.class, root.get("inclusionCriteria"), criteriaBuilder.literal("sois"));
				List<Predicate> soisPredicates = new ArrayList<>();
				for (String sourceId : filter.getSourceIds()) {
					soisPredicates.add(criteriaBuilder.like(sidExpression,
							criteriaBuilder.literal("%" + sourceId + "%").as(String.class)));
				}
				predicates.add(criteriaBuilder.or(soisPredicates.toArray(new Predicate[0])));

			}
		}

	},
	ID {
		@Override
		public void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				FareRuleFilter filter, List<Predicate> predicates) {
			if (filter.getId() != null)
				predicates.add(criteriaBuilder.equal(root.get("id"), filter.getId()));

		}
	};

	public abstract void addPredicate(Root<DbFareRuleInfo> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, FareRuleFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnFareRuleFilter(Root<DbFareRuleInfo> root,
			CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, FareRuleFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (FareRuleSearchPredicate ruleSearchPredicate : FareRuleSearchPredicate.values()) {
			ruleSearchPredicate.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
