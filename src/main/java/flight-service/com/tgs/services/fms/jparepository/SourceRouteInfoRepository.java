package com.tgs.services.fms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;

public interface SourceRouteInfoRepository
		extends JpaRepository<DbSourceRouteInfo, Long>, JpaSpecificationExecutor<DbSourceRouteInfo> {
	
	public List<DbSourceRouteInfo> findBySourceId(int sourceId);

}
