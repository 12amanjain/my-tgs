package com.tgs.services.fms.jparepository;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.fms.helper.SourceRouteInfoHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;

@Service
public class SourceRouteInfoService {

	@Autowired
	SourceRouteInfoRepository sourceRouteRepo;

	@Autowired
	SourceRouteInfoHelper srHelper;

	public List<DbSourceRouteInfo> findAll(Pageable pageable) {
		List<DbSourceRouteInfo> sourceRouteInfoList = new ArrayList<>();
		sourceRouteRepo.findAll(pageable).forEach(sourceRouteInfo -> sourceRouteInfoList.add(sourceRouteInfo));
		return sourceRouteInfoList;
	}

	public void addAll(List<DbSourceRouteInfo> sourceRouteInfos) {
		sourceRouteInfos.forEach(sourceRouteInfo -> {
			sourceRouteRepo.saveAndFlush(sourceRouteInfo);
		});
	}

	public DbSourceRouteInfo findOne(Long id) {
		return sourceRouteRepo.findOne(id);
	}

	public DbSourceRouteInfo save(DbSourceRouteInfo dbSourceRouteInfo) {
		dbSourceRouteInfo = sourceRouteRepo.saveAndFlush(dbSourceRouteInfo);
		srHelper.process();
		return dbSourceRouteInfo;
	}

	public void deleteById(Long id) {
		sourceRouteRepo.delete(id);
	}

	public List<DbSourceRouteInfo> findAllBySourceId(int sourceId) {
		return sourceRouteRepo.findBySourceId(sourceId);
	}

}
