package com.tgs.services.fms.jparepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.supplier.SupplierConfigurationFilter;
import com.tgs.services.fms.dbmodel.DbSupplierInfo;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;

@Service
public class SupplierInfoService {

	@Autowired
	SupplierInfoRepository supplierInfoRepo;

	@Autowired
	private SupplierConfigurationHelper confHelper;

	public List<DbSupplierInfo> findAll() {
		return supplierInfoRepo.findAllByOrderBySourceIdAscProcessedOnDesc();
	}

	public List<DbSupplierInfo> findAll(@Valid SupplierConfigurationFilter filter) {
		Specification<DbSupplierInfo> specification = new Specification<DbSupplierInfo>() {
			@Override
			public Predicate toPredicate(Root<DbSupplierInfo> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter != null && CollectionUtils.isNotEmpty(filter.getSupplierIds())) {
					Expression<String> exp = root.get("name");
					predicates.add(exp.in(filter.getSupplierIds()));
				}
				if (filter != null && filter.getId() != null) {
					predicates.add(cb.equal(root.<Long>get("id"), filter.getId()));
				}
				if (filter != null && filter.getEnabled() != null) {
					predicates.add(cb.equal(root.<Boolean>get("enabled"), filter.getEnabled()));
				}
				if (filter != null && CollectionUtils.isNotEmpty(filter.getSourceIds())) {
					Expression<String> exp = root.get("sourceId");
					predicates.add(exp.in(filter.getSourceIds()));
				}
				if (filter != null && (filter.getCreatedOnAfterDateTime() != null)
						&& (filter.getCreatedOnBeforeDateTime() != null)) {
					predicates.add(cb.between(root.get("createdOn"), filter.getCreatedOnAfterDateTime(),
							filter.getCreatedOnBeforeDateTime()));
				}
				predicates.add(cb.equal(root.get("isDeleted"), filter.isDeleted()));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("sourceId", "name", "processedOn"));
		}
		return supplierInfoRepo.findAll(specification, sort);
	}

	public DbSupplierInfo save(DbSupplierInfo supplierInfo) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			supplierInfo.getCredentialInfo().cleanData();
			supplierInfo.setProcessedOn(LocalDateTime.now());
			supplierInfo = supplierInfoRepo.saveAndFlush(supplierInfo);
		}
		confHelper.process();
		return supplierInfo;
	}

	public DbSupplierInfo findById(Long id) {
		return supplierInfoRepo.findOne(id);
	}

	public Long getNextSeriesId() {
		return supplierInfoRepo.getNextSeriesId();
	}

}
