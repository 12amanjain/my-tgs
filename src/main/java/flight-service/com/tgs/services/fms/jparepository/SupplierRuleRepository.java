package com.tgs.services.fms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.fms.dbmodel.DbSupplierRule;

public interface SupplierRuleRepository
		extends JpaRepository<DbSupplierRule, Long>, JpaSpecificationExecutor<DbSupplierRule> {
}
