package com.tgs.services.fms.jparepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.supplier.SupplierRuleFilter;
import com.tgs.services.fms.dbmodel.DbSupplierRule;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;

@Service
public class SupplierRuleService {

	@Autowired
	SupplierRuleRepository supplierRuleRepo;

	@Autowired
	private SupplierConfigurationHelper confHelper;

	public List<DbSupplierRule> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return supplierRuleRepo.findAll(sort);
	}

	public List<DbSupplierRule> findAll(@Valid SupplierRuleFilter filter) {
		Specification<DbSupplierRule> specification = new Specification<DbSupplierRule>() {
			@Override
			public Predicate toPredicate(Root<DbSupplierRule> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter != null && CollectionUtils.isNotEmpty(filter.getSupplierIds())) {
					Expression<String> exp = root.get("supplierId");
					predicates.add(exp.in(filter.getSupplierIds()));
				}
				if (filter != null && filter.getId() != null) {
					predicates.add(cb.equal(root.<Long>get("id"), filter.getId()));
				}
				if (filter != null && filter.getEnabled() != null) {
					predicates.add(cb.equal(root.<Boolean>get("enabled"), filter.getEnabled()));
				}
				if (filter != null && CollectionUtils.isNotEmpty(filter.getSourceIds())) {
					Expression<String> exp = root.get("sourceId");
					predicates.add(exp.in(filter.getSourceIds()));
				}
				if (filter != null && (filter.getCreatedOnAfterDateTime() != null)
						&& (filter.getCreatedOnBeforeDateTime() != null)) {
					predicates.add(cb.between(root.get("createdOn"), filter.getCreatedOnAfterDateTime(),
							filter.getCreatedOnBeforeDateTime()));
				}
				if (filter.getDeleted() != null) {
					predicates.add(cb.equal(root.get("isDeleted"), filter.getDeleted()));
				}
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("sourceId", "priority", "supplierId", "processedOn"));
		}
		return supplierRuleRepo.findAll(specification, sort);
	}

	public DbSupplierRule save(DbSupplierRule supplierRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			supplierRule.cleanData();
			supplierRule.setProcessedOn(LocalDateTime.now());
			supplierRule = supplierRuleRepo.saveAndFlush(supplierRule);
		}
		confHelper.process();
		return supplierRule;
	}

	public DbSupplierRule findById(Long id) {
		return supplierRuleRepo.findOne(id);
	}
}
