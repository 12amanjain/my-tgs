package com.tgs.services.fms.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.fms.dbmodel.DbSupplierSession;

@Repository
public interface SupplierSessionRepository
		extends JpaRepository<DbSupplierSession, Long>, JpaSpecificationExecutor<DbSupplierSession> {

	public List<DbSupplierSession> findByBookingIdAndSourceIdAndSupplierIdAndExpiryTimeGreaterThan(String bookingId,
			Integer sourceId, String supplierId, LocalDateTime currentTime);

	public List<DbSupplierSession> findByBookingIdAndSourceIdAndSupplierId(String bookingId, Integer sourceId,
			String supplierId);

	public List<DbSupplierSession> findByBookingId(String bookingId);
}
