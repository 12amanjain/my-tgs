package com.tgs.services.fms.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.dbmodel.DbSupplierSession;

@Slf4j
@Service
public class SupplierSessionService {

	@Autowired
	SupplierSessionRepository supplierSessionRepo;

	public DbSupplierSession save(SupplierSession supplierSession) {
		DbSupplierSession sessionManagement = DbSupplierSession.builder()
				.supplierSessionInfo(supplierSession.getSupplierSessionInfo()).bookingId(supplierSession.getBookingId())
				.supplierId(supplierSession.getSupplierId()).sourceId(supplierSession.getSourceId())
				.createdOn(supplierSession.getCreatedOn()).expiryTime(supplierSession.getExpiryTime()).build();
		SupplierSession session = getExistsBookingSession(sessionManagement);
		if (session != null) {
			// To Avoid If Review Failed and Again Review Success case
			supplierSessionRepo.delete(session.getId());
		}
		if (BooleanUtils.isTrue(sessionManagement.getSupplierSessionInfo().getIsSameForAll())) {
			List<DbSupplierSession> oldVal =
					supplierSessionRepo.findByBookingIdAndSourceIdAndSupplierId(sessionManagement.getBookingId(),
							sessionManagement.getSourceId(), sessionManagement.getSupplierId());
			if (CollectionUtils.isNotEmpty(oldVal)) {
				log.debug("Deleting old session info for booking {}", oldVal.get(0).getBookingId());
				supplierSessionRepo.delete(oldVal.get(0).getId());
			}
		}
		return supplierSessionRepo.saveAndFlush(sessionManagement);
	}

	public SupplierSession getBookingSession(String bookingId, Integer sourceId, String supplierId,
			SupplierSessionInfo sessionInfo) {
		List<DbSupplierSession> supplierSessions =
				supplierSessionRepo.findByBookingIdAndSourceIdAndSupplierIdAndExpiryTimeGreaterThan(bookingId, sourceId,
						supplierId, LocalDateTime.now());
		AtomicReference<DbSupplierSession> existSession = new AtomicReference<>();
		if (CollectionUtils.isNotEmpty(supplierSessions)) {
			if (StringUtils.isNotBlank(sessionInfo.getTripKey())) {
				existSession.set(supplierSessions.stream().filter(
						eachSess -> eachSess.getSupplierSessionInfo().getTripKey().contains(sessionInfo.getTripKey()))
						.findFirst().orElse(null));
			} else {
				existSession.set(supplierSessions.get(0));
			}
			if (existSession.get() != null) {
				return existSession.get().toDomain();
			}
		}
		return null;
	}

	public List<SupplierSession> getBookingSession(String bookingId) {
		return DbSupplierSession.toDomainList(supplierSessionRepo.findByBookingId(bookingId));
	}

	/**
	 * @implSpec : TO Avoid Multiple Session On Review (if first review success & second trip failed) In this case again
	 *           review happen , let remove the old one from system
	 */
	private SupplierSession getExistsBookingSession(DbSupplierSession oldSS) {
		List<DbSupplierSession> supplierSessions =
				supplierSessionRepo.findByBookingIdAndSourceIdAndSupplierIdAndExpiryTimeGreaterThan(
						oldSS.getBookingId(), oldSS.getSourceId(), oldSS.getSupplierId(), LocalDateTime.now());
		AtomicReference<DbSupplierSession> existSession = new AtomicReference<>();
		if (CollectionUtils.isNotEmpty(supplierSessions)) {
			if (StringUtils.isNotBlank(oldSS.getSupplierSessionInfo().getTripKey())) {
				existSession.set(supplierSessions.stream().filter(eachSess -> eachSess.getSupplierSessionInfo()
						.getTripKey().equals(oldSS.getSupplierSessionInfo().getTripKey())).findFirst().orElse(null));
			}
		}
		if (existSession.get() != null) {
			return existSession.get().toDomain();
		}
		return null;
	}

	
	public List<SupplierSession> removeExpiredSessionsFromDb(List<SupplierSession> supplierSessionList) {
		for (int index = 0; index < supplierSessionList.size(); index++) {
			SupplierSession supplierSession = supplierSessionList.get(index);
			if (supplierSession.getExpiryTime().compareTo(LocalDateTime.now()) < 0) {
				supplierSessionRepo.delete(supplierSession.getId());
				supplierSessionList.remove(supplierSession);
			}
		}
		return supplierSessionList;
	}


	public void removeSessions(List<SupplierSession> supplierSessionList) {
		if (CollectionUtils.isNotEmpty(supplierSessionList)) {
			for (SupplierSession supplierSession : supplierSessionList) {
				supplierSessionRepo.delete(supplierSession.getId());
			}
		}
	}

}
