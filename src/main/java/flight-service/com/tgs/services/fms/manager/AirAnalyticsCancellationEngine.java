package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.Map;
import java.util.Map.Entry;

import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.mapper.AirCancellationAnalyticsMapper;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirAnalyticsCancellationEngine {

	@Autowired
	AirAnalyticsHelper analyticsHelper;

	public void sendDataToAnalytics(AirCancellationRequest cancellationRequest, Order order,
			AirCancellationResponse cancellationReviewResponse, Double amountToBeRefunded, boolean isReview) {
		Map<String, BookingSegments> cancellationSegments = TripBookingUtils
				.filterCancellingTravellers(cancellationRequest.getPaxKeys(), cancellationRequest.getSegmentInfos());
		ArrayList<SegmentInfo> cancelledSegments = new ArrayList<SegmentInfo>();
		for (Entry<String, BookingSegments> entry : cancellationSegments.entrySet()) {
			cancelledSegments.addAll(entry.getValue().getCancelSegments());
		}
		AirCancellationAnalyticsMapper queryMapper =
				AirCancellationAnalyticsMapper.builder().amendmentId(cancellationRequest.getAmendmentId())
						.segmentInfos(cancelledSegments).bookingId(order.getBookingId())
						.bookingCancellationDetail(cancellationReviewResponse.getCancellationDetail())
						.cancellationPriceInfo(cancellationReviewResponse.getPriceInfo())
						.totalAmountToRefund(amountToBeRefunded).build();
		if (isReview
				&& cancellationRequest.getAmendment().getAmendmentType().equals(AmendmentType.CANCELLATION_QUOTATION))
			analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.QUOTATION);
		else if (isReview && cancellationRequest.getAmendment().getAmendmentType().equals(AmendmentType.CANCELLATION))
			analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.CANCELLATION);
		else
			analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.CONFIRMCANCELLATION);
	}
}
