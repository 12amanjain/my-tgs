package com.tgs.services.fms.manager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.reflect.TypeToken;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.CorporateTripServiceCommunicator;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierAdditionalInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.HoldLimitException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirBookingEngine {

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	private AirOrderItemCommunicator itemComm;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	private CorporateTripServiceCommunicator corpTripCommunicator;

	@Autowired
	private UserServiceCommunicator umsComm;

	public void doBooking(List<TripInfo> tripInfos, Order order) {
		log.info("Inside AirBookingEngine#doBooking for booking Id {}", order.getBookingId());
		log.info("Order additional ifon booking Id {} is {} ", order.getBookingId(),
				GsonUtils.getGson().toJson(order.getAdditionalInfo()));
		List<Future<Boolean>> futureTaskList = new ArrayList<>();
		AirSearchQuery searchQuery = AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(tripInfos));
		User bookingUser = umsComm.getUserFromCache(order.getBookingUserId());
		ExecutorUtils.getFlightSearchThreadPool().submit(() -> {
			Map<String, BookingSegments> sourceWiseTripMap = TripBookingUtils
					.getSupplierWiseBookingSegmentsUsingSession(tripInfos, order.getBookingId(), bookingUser);
			for (Map.Entry<String, BookingSegments> entry : sourceWiseTripMap.entrySet()) {
				BookingSegments bookingSegments = entry.getValue();
				boolean putToPending =
						putToPending(bookingSegments, order, "BOOKING", gmsCommunicator, itemComm, bookingUser);
				if (!putToPending) {
					bookingSegments.setSearchQuery(searchQuery);
					SupplierBasicInfo basicInfo =
							bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getSupplierBasicInfo();
					AirSourceType sourceType = AirSourceType.getAirSourceType(basicInfo.getSourceId());
					SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
							basicInfo.getSourceId(), basicInfo.getRuleId(), basicInfo.getSupplierId());
					AbstractAirBookingFactory factory =
							sourceType.getBookingFactoryInstance(supplierConf, bookingSegments, order);
					futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
							.submit(() -> factory.bookTrip(SystemContextHolder.getContextData())));
				}
			}

			for (Future<Boolean> task : futureTaskList) {
				try {
					task.get(240, TimeUnit.SECONDS);
				} catch (TimeoutException e) {
					log.error("Cancelling booking thread due to timeout for bookingId {}", order.getBookingId(), e);
					task.cancel(true);
				} catch (Exception e) {
					log.error("Unable to book for bookingId {}", order.getBookingId(), e);
				}
			}
			itemComm.updateOrderAndItem(AirUtils.getSegmentInfos(tripInfos), order, null);
			itemComm.checkAndUpdateOrderStatus(AirUtils.getSegmentInfos(tripInfos), order);
			updateTripStatus(order);
		});
	}

	private void updateTripStatus(Order order) {
		if (StringUtils.isNotEmpty(order.getAdditionalInfo().getTripId())) {
			String status = OrderStatus.SUCCESS.equals(order.getStatus()) ? "Booked" : "In_Progress";
			corpTripCommunicator.linkBookingIdWithTrip(order.getAdditionalInfo().getTripId(), order.getBookingId(),
					status);
		}
	}

	public static boolean putToPending(BookingSegments bookingSegments, Order order, String flow,
			GeneralServiceCommunicator gmsCommunicator, AirOrderItemCommunicator itemComm, User bookingUser) {
		boolean putToManual = false;
		AirItemStatus status = null;
		if (isHoldBook(order)) {
			status = AirItemStatus.HOLD_PENDING;
		} else {
			status = AirItemStatus.TICKET_PENDING;
		}
		SupplierRule rule = SupplierConfigurationHelper.getSupplierRule(
				bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getSupplierBasicInfo().getSourceId(),
				bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getSupplierBasicInfo().getRuleId());
		String supplierId = "";
		if (flow.equals("BOOKING")) {
			supplierId = getBookingSupplierId(rule.getSupplierAdditionalInfo(), bookingSegments.getSegmentInfos(),
					bookingUser);
		} else {
			supplierId = getTicketingSupplierId(rule.getSupplierAdditionalInfo(), bookingSegments.getSegmentInfos(),
					bookingUser);
		}

		// To avoid null key search of supplierInfo.
		if (StringUtils.isNotEmpty(supplierId)) {
			SupplierInfo supplierInfo = SupplierConfigurationHelper.getSupplierInfo(supplierId);
			if (supplierInfo != null && supplierInfo.getSourceId() == -1) {
				putToManual = true;
				String noteMessage = Note.getNoteMessage(null, StringUtils.join(
						"Booking is going in pending status as per supplier Rule configuration, Supplier Rule Id is ",
						rule.getId()));
				Note note = Note.builder().bookingId(order.getBookingId()).noteType(NoteType.MANUAL_CONFIRMATION)
						.noteMessage(noteMessage)
						.userId(SystemContextHolder.getContextData().getUser().getLoggedInUserId()).build();
				gmsCommunicator.addNote(note);
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, status);
			}
		}
		return putToManual;

	}

	public void doConfirmBooking(List<TripInfo> tripInfos, Order order) {
		order.setReason("");
		log.info("Inside AirBookingEngine#doConfirmBooking for booking Id {}", order.getBookingId());
		ExecutorUtils.getFlightSearchThreadPool().submit(() -> {
			try {
				if (isValidOrderStatus(order)) {
					Thread.sleep(10 * 1000);
					List<Future<Boolean>> futureTaskList = new ArrayList<>();
					AirSearchQuery searchQuery =
							AirUtils.combineSearchQuery(AirUtils.getSearchQueryFromTripInfos(tripInfos));
					Map<String, BookingSegments> sourceWiseTripMap =
							TripBookingUtils.getSupplierWiseBookingSegmentsUsingPNR(tripInfos, order.getBookingId());
					User bookingUser = umsComm.getUserFromCache(order.getBookingUserId());
					for (Map.Entry<String, BookingSegments> entry : sourceWiseTripMap.entrySet()) {
						BookingSegments bookingSegments = entry.getValue();
						boolean putToManual = putToPending(bookingSegments, order, "TICKETING", gmsCommunicator,
								itemComm, bookingUser);
						if (!putToManual) {
							SupplierBasicInfo basicInfo =
									bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getSupplierBasicInfo();
							AirSourceType sourceType = AirSourceType.getAirSourceType(basicInfo.getSourceId());
							bookingSegments.setSearchQuery(searchQuery);
							SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
									basicInfo.getSourceId(), basicInfo.getRuleId(), basicInfo.getSupplierId());
							AbstractAirBookingFactory factory =
									sourceType.getBookingFactoryInstance(supplierConf, bookingSegments, order);
							futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
									.submit(() -> factory.confirmBook(SystemContextHolder.getContextData())));
						}
						for (Future<Boolean> task : futureTaskList) {
							try {
								task.get(240, TimeUnit.SECONDS);
							} catch (TimeoutException e) {
								log.error("Cancelling booking thread due to timeout for bookingId {}",
										order.getBookingId(), e);
								task.cancel(true);
							} catch (Exception e) {
								log.error("Unable to confirm booking for bookingId {}", order.getBookingId(), e);
							}
						}
						itemComm.updateOrderAndItem(AirUtils.getSegmentInfos(tripInfos), order, null);
						itemComm.checkAndUpdateOrderStatus(AirUtils.getSegmentInfos(tripInfos), order);
						updateTripStatus(order);
					}
				}
			} catch (InterruptedException ie) {
				log.error("Thread interrupted for confirm Book {} excep ", order.getBookingId(), ie);
			}
		});

	}

	private boolean isValidOrderStatus(Order order) {
		order = itemComm.getOrderByBookingId(order.getBookingId());
		if (order.getStatus() != null && order.getStatus().equals(OrderStatus.ABORTED)) {
			log.info("Unable to do confirmBooking due to order status aborted for booking {}", order.getBookingId());
			return false;
		}
		if (!order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
			return false;
		}
		return true;
	}

	public boolean confirmTicketFare(Order order) {
		log.info("Inside AirBookingEngine#confirmTicketFare for booking Id {}", order.getBookingId());
		AtomicBoolean isFareDiff = new AtomicBoolean(Boolean.FALSE);
		List<TripInfo> trips = itemComm.findTripByBookingId(order.getBookingId());
		boolean withInHoldTimeLimit = isWithinHoldTimeLimit(trips);
		if (!withInHoldTimeLimit) {
			throw new HoldLimitException(SystemError.HOLD_TIME_LIMIT_EXCEED);
		}

		List<Future<Boolean>> futureTaskList = new ArrayList<>();
		Map<String, BookingSegments> sourceWiseTripMap =
				TripBookingUtils.getSupplierWiseBookingSegmentsUsingPNR(trips, order.getBookingId());
		for (Map.Entry<String, BookingSegments> entry : sourceWiseTripMap.entrySet()) {
			BookingSegments bookingSegments = entry.getValue();
			SupplierBasicInfo basicInfo =
					bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getSupplierBasicInfo();
			AirSourceType sourceType = AirSourceType.getAirSourceType(basicInfo.getSourceId());
			SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
					basicInfo.getSourceId(), basicInfo.getRuleId(), basicInfo.getSupplierId());
			AbstractAirBookingFactory factory =
					sourceType.getBookingFactoryInstance(supplierConf, bookingSegments, order);
			futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
					.submit(() -> factory.confirmFareBeforeTicket(SystemContextHolder.getContextData())));

			for (Future<Boolean> task : futureTaskList) {
				try {
					boolean isFareChange = task.get(120, TimeUnit.SECONDS);
					if (isFareChange) {
						isFareDiff.set(Boolean.TRUE);
					}
				} catch (TimeoutException e) {
					log.error("Cancelling confirm ticket thread due to timeout for bookingId {}", order.getBookingId(),
							e);
					task.cancel(true);
				} catch (InterruptedException | ExecutionException e) {
					log.error("Unable to validate fare for bookingId {}", order.getBookingId(), e);
				}
			}
		}
		return isFareDiff.get();
	}

	public boolean isWithinHoldTimeLimit(List<TripInfo> trips) {
		AtomicBoolean withInHoldTimeLimit = new AtomicBoolean(true);
		trips.forEach(trip -> {
			trip.getSegmentInfos().forEach(segment -> {
				if (Objects.nonNull(segment.getPriceInfo(0).getMiscInfo().getTimeLimit())
						&& segment.getPriceInfo(0).getMiscInfo().getTimeLimit().isBefore(LocalDateTime.now())) {
					// If Time Limit is Expired than current time
					withInHoldTimeLimit.set(false);
				}
			});
		});
		return withInHoldTimeLimit.get();
	}

	public AirImportPnrBooking retrieveBooking(AirImportPnrBookingRequest pnrRequest) throws Exception {
		SupplierInfo supplierInfo = SupplierConfigurationHelper.getSupplierInfo(pnrRequest.getSupplierId());
		AirImportPnrBooking pnrBooking = AirImportPnrBooking.builder().build();
		SupplierRule supplierRule = null;
		SupplierConfiguration supplierConf = null;
		if (supplierInfo != null) {
			supplierRule = SupplierConfigurationHelper.getSupplierRule(supplierInfo.getSourceId(),
					supplierInfo.getSupplierId());
			AirSourceType sourceType = AirSourceType.getAirSourceType(supplierRule.getSourceId());
			supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(supplierRule.getSourceId(),
					supplierRule.getId());
			AbstractAirBookingRetrieveFactory bookingFactory =
					sourceType.getBookingRetrieveFactoryInstance(supplierConf, pnrRequest.getPnr());
			pnrBooking = bookingFactory.retrieveBooking(SystemContextHolder.getContextData(), pnrRequest);
		} else {
			log.error("No Supplier Found for PNR {} & Supplier id {}", pnrRequest.getPnr(), pnrRequest.getSupplierId());
			throw new CustomGeneralException(SystemError.NO_SUPPLIER_FOUND);
		}
		return pnrBooking;
	}

	public AirReviewResponse getAirReviewResponse(String bookingId, String oldBookingId) {
		AirReviewResponse reviewResponse = cachingService.fetchValue(ObjectUtils.firstNonNull(oldBookingId, bookingId),
				AirReviewResponse.class, CacheSetName.AIR_REVIEW.getName(), BinName.AIRREVIEW.getName());
		if (reviewResponse == null) {
			throw new CustomGeneralException(SystemError.EXPIRED_BOOKING_ID);
		}
		return reviewResponse;
	}

	public List<TripInfo> getTripInfos(AirBookingRequest bookingRequest) {
		AirReviewResponse reviewResponse =
				getAirReviewResponse(bookingRequest.getBookingId(), bookingRequest.getOldBookingId());

		bookingRequest.setType(AirUtils.getOrderType(reviewResponse.getSearchQuery()));
		for (TripInfo tripInfo : reviewResponse.getTripInfos()) {
			SegmentInfo previousSegmentInfo = null;
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				@SuppressWarnings("serial")
				List<FlightTravellerInfo> travellerInfoList =
						GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(bookingRequest.getTravellerInfo()),
								new TypeToken<List<FlightTravellerInfo>>() {}.getType());
				setSegmentSSRInformation(travellerInfoList, segmentInfo, previousSegmentInfo,
						bookingRequest.getBookingId());
				segmentInfo.setBookingRelatedInfo(
						SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfoList).build());
				previousSegmentInfo = segmentInfo;
			}
		}

		return reviewResponse.getTripInfos();
	}

	private void setBaggageInfo(FlightTravellerInfo travellerInfo, SegmentInfo segmentInfo,
			FlightTravellerInfo previousSegmentTravellerInfo, String bookingId) {
		try {
			if (CollectionUtils.isNotEmpty(travellerInfo.getSsrBaggageInfos())
					&& MapUtils.isNotEmpty(segmentInfo.getSsrInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE))) {
				for (SSRInformation baggageInfo : travellerInfo.getSsrBaggageInfos()) {
					if (baggageInfo != null) {
						for (SSRInformation ssrInfo : segmentInfo.getSsrInfo().get(SSRType.BAGGAGE)) {
							/**
							 * This condition is required for connecting segments . Because you can purchase baggage for
							 * one journey/trip . SegmentWise baggage is not allowed. That's why if a passenger has
							 * selected baggage in first segment then same is applicable on other segments as well.
							 */
							if ((segmentInfo.getSegmentNum() != 0
									&& previousSegmentTravellerInfo.getSsrBaggageInfo() != null
									&& ssrInfo.getCode()
											.equals(previousSegmentTravellerInfo.getSsrBaggageInfo().getCode()))
									|| (ssrInfo.getCode().equals(baggageInfo.getCode())
											&& baggageInfo.getKey().equals(segmentInfo.getId()))) {
								travellerInfo.setSsrBaggageInfo(ssrInfo);
							}
						}
					}
				}

			}

			/*
			 * previousSegmentTravellerInfo check repeated because we are setting segment baggage ssr from
			 * travellerInfo.getSsrBaggageInfos() which will be empty from UI or API.
			 */
			if ((segmentInfo.getSegmentNum() == 0 || (previousSegmentTravellerInfo != null
					&& previousSegmentTravellerInfo.getSsrBaggageInfo() != null))
					&& !PaxType.INFANT.equals(travellerInfo.getPaxType())
					&& travellerInfo.getSsrBaggageInfo() == null) {
				SSRInformation ssrInfo = getFreeBaggage(segmentInfo);
				if (ssrInfo != null) {
					travellerInfo.setSsrBaggageInfo(ssrInfo);
					log.info("SSR selected from free baggage for bookingId {}", bookingId);
				} else if (segmentInfo.getSegmentNum() != 0) {
					travellerInfo.setSsrBaggageInfo(previousSegmentTravellerInfo.getSsrBaggageInfo());
					log.info("SSR selected from free baggage for bookingId {}", bookingId);
				}
			}

		} catch (Exception e) {
			log.error("Unable to set BaggageInfo for segmentInfo {}", segmentInfo, e);
			throw e;
		}
	}

	private SSRInformation getFreeBaggage(SegmentInfo segmentInfo) {
		if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())
				&& CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE))) {
			for (SSRInformation baggageSSR : segmentInfo.getSsrInfo().get(SSRType.BAGGAGE)) {
				if (baggageSSR != null && baggageSSR.getAmount() != null && baggageSSR.getAmount() == 0) {
					return baggageSSR;
				}

			}

		}
		return null;
	}

	private void setMealInfo(FlightTravellerInfo travellerInfo, SegmentInfo segmentInfo) {
		if (CollectionUtils.isNotEmpty(travellerInfo.getSsrMealInfos()) && MapUtils.isNotEmpty(segmentInfo.getSsrInfo())
				&& CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.MEAL))) {
			for (SSRInformation mealInfo : travellerInfo.getSsrMealInfos()) {
				if (mealInfo != null && mealInfo.getKey() != null && mealInfo.getKey().equals(segmentInfo.getId())) {
					for (SSRInformation ssrInfo : segmentInfo.getSsrInfo().get(SSRType.MEAL)) {
						if (ssrInfo.getCode().equals(mealInfo.getCode())) {
							travellerInfo.setSsrMealInfo(ssrInfo);
						}
					}
				}
			}
		}
	}

	private void setSeatInfo(FlightTravellerInfo travellerInfo, SegmentInfo segmentInfo) {
		if (CollectionUtils.isNotEmpty(travellerInfo.getSsrSeatInfos())) {
			for (SSRInformation seatInfo : travellerInfo.getSsrSeatInfos()) {
				if (seatInfo != null && seatInfo.getKey() != null && seatInfo.getKey().equals(segmentInfo.getId())) {
					for (SSRInformation ssrInformation : segmentInfo.getSsrInfo().get(SSRType.SEAT)) {
						if (ssrInformation.getCode().equals(seatInfo.getCode())) {
							travellerInfo.setSsrSeatInfo(ssrInformation);
						}
					}
				}
			}
		}
	}

	private void setExtraServices(FlightTravellerInfo travellerInfo, SegmentInfo segmentInfo) {
		if (CollectionUtils.isNotEmpty(travellerInfo.getSsrExtraServiceInfos())) {
			for (SSRInformation extraService : travellerInfo.getSsrExtraServiceInfos()) {
				if (extraService.getKey() != null && extraService.getKey().equals(segmentInfo.getId())) {
					for (SSRInformation ssrInformation : segmentInfo.getSsrInfo().get(SSRType.EXTRASERVICES)) {
						if (ssrInformation.getCode().equals(extraService.getCode())) {
							if (CollectionUtils.isEmpty(travellerInfo.getExtraServices())) {
								travellerInfo.setExtraServices(new ArrayList<>());
							}
							travellerInfo.getExtraServices().add(ssrInformation);
						}
					}
				}
			}
		}
	}

	private void setSegmentSSRInformation(List<FlightTravellerInfo> travellerInfoList, SegmentInfo segmentInfo,
			SegmentInfo previousSegmentInfo, String bookingId) {
		int travellerIndex = 0;
		for (FlightTravellerInfo travellerInfo : travellerInfoList) {
			setBaggageInfo(travellerInfo, segmentInfo,
					previousSegmentInfo == null ? null
							: previousSegmentInfo.getBookingRelatedInfo().getTravellerInfo().get(travellerIndex),
					bookingId);
			setMealInfo(travellerInfo, segmentInfo);
			setSeatInfo(travellerInfo, segmentInfo);
			setExtraServices(travellerInfo, segmentInfo);
			emptyRedudantParameters(travellerInfo);
			// System Should GenerateId,from no where we should accept Id.
			travellerInfo.setId(null);
			travellerIndex++;
		}
		segmentInfo.setSsrInfo(null);
	}

	private void emptyRedudantParameters(FlightTravellerInfo travellerInfo) {
		travellerInfo.setSsrBaggageInfos(null);
		travellerInfo.setSsrMealInfos(null);
		travellerInfo.setSsrSeatInfos(null);

	}

	public AirSearchQuery getSearchQueryFromReviewResponse(AirBookingRequest request) {
		AirReviewResponse reviewResponse = getAirReviewResponse(request.getBookingId(), request.getOldBookingId());
		return reviewResponse.getSearchQuery();
	}

	public static String getBookingSupplierId(SupplierAdditionalInfo additionalInfo, List<SegmentInfo> segments,
			User user) {
		String supplierId = "";
		if (CollectionUtils.isNotEmpty(additionalInfo.getBookingSuppliers())) {
			SupplierRule bookingSupplierRule =
					getMatchedSupplierRule(additionalInfo.getBookingSuppliers(), segments, user);
			if (bookingSupplierRule != null) {
				supplierId = bookingSupplierRule.getSupplierId();
			}
			return supplierId;
		}

		if (CollectionUtils.isNotEmpty(additionalInfo.getBookingSupplierIds())) {
			supplierId = additionalInfo.getBookingSupplierId(segments.get(0), 0);
		}
		return supplierId;
	}

	public static String getTicketingSupplierId(SupplierAdditionalInfo additionalInfo, List<SegmentInfo> segments,
			User user) {
		String supplierId = "";
		if (CollectionUtils.isNotEmpty(additionalInfo.getTicketingSuppliers())) {
			SupplierRule ticketingSupplierRule =
					getMatchedSupplierRule(additionalInfo.getTicketingSuppliers(), segments, user);
			if (ticketingSupplierRule != null) {
				supplierId = ticketingSupplierRule.getSupplierId();
			}
			return supplierId;
		}
		if (CollectionUtils.isNotEmpty(additionalInfo.getTicketingSupplierIds())) {
			supplierId = additionalInfo.getTicketingSupplierId(segments.get(0), 0);
		}
		return supplierId;
	}

	private static SupplierRule getMatchedSupplierRule(List<SupplierRule> supplierRules, List<SegmentInfo> segments,
			User user) {
		List<SupplierRule> enabledRules = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(supplierRules)) {
			enabledRules = supplierRules.stream().filter(rule -> BooleanUtils.isNotTrue(rule.getIsDeleted()))
					.collect(Collectors.toList());
		}
		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(segments);
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(tripInfo);
		flightFact.setFareType(segments.get(0).getPriceInfo(0).getFareType());
		BaseUtils.createFactOnUser(flightFact, user);
		flightFact.setAirType(AirUtils.getAirType(tripInfo));
		flightFact.setAccountCode(segments.get(0).getPriceInfo(0).getAccountCode());
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(enabledRules, flightFact, fieldResolverMap);
		@SuppressWarnings("unchecked")
		List<SupplierRule> matchingRules = (List<SupplierRule>) ruleEngine.fireAllRules();
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules.get(0);
		}
		return null;
	}

	private static boolean isHoldBook(Order order) {
		boolean isHoldBook = true;
		if (order.getAdditionalInfo() != null && order.getAdditionalInfo().getPaymentStatus() != null
				&& PaymentStatus.SUCCESS.equals(order.getAdditionalInfo().getPaymentStatus())) {
			isHoldBook = false;
		}
		return isHoldBook;
	}

}
