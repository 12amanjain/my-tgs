package com.tgs.services.fms.manager;

import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirCancellationEngine {

	@Autowired
	AirCancellationManager cancellationManager;

	// Release the Hold PNR
	public void releasePnr(Map<String, BookingSegments> supplierWiseBookingSegments, Order order) {
		log.info("Inside AirCancellationEngine#cancelBooking for bookingId {}", order.getBookingId());
		for (Entry<String, BookingSegments> entry : supplierWiseBookingSegments.entrySet()) {
			AbstractAirCancellationFactory factory = getCancellationFactory(entry.getValue(), order, true, null);
			factory.releasePNR();
		}
	}


	public List<AirCancellationDetail> reviewCancellation(Map<String, BookingSegments> cancellationSegments,
			Order order) {
		List<AirCancellationDetail> cancellationDetails = new ArrayList<>();
		log.info("Inside AirCancellationEngine#reviewCancellation for bookingId {}", order.getBookingId());
		boolean isAutoCancellationAllowed =
				checkAutoCancellationAllowed(order.getBookingId(), cancellationSegments, cancellationDetails, order);
		if (isAutoCancellationAllowed) {
			log.info("Review AutoCancellation is applicable for bookingId {} ", order.getBookingId());
			for (Entry<String, BookingSegments> entry : cancellationSegments.entrySet()) {
				if (CollectionUtils.isNotEmpty(entry.getValue().getCancelSegments())) {
					AirCancellationDetail airCancellationDetail = AirCancellationDetail.builder().build();
					entry.getValue().setAirlinePNR(entry.getKey());
					AbstractAirCancellationFactory factory =
							getCancellationFactory(entry.getValue(), order, false, airCancellationDetail);
					factory.setCancellationReview(Boolean.TRUE);
					airCancellationDetail = factory.reviewCancellation();
					cancellationDetails.add(airCancellationDetail);
				}
			}
		}
		return cancellationDetails;
	}

	private boolean checkAutoCancellationAllowed(String bookingid, Map<String, BookingSegments> cancellationSegments,
			List<AirCancellationDetail> cancellationDetails, Order order) {
		boolean isAutoCancellationAllowed = true;
		log.info("Review AutoCancellation#checkAutoCancellationAllowed is applicable for bookingId {} ",
				order.getBookingId());
		for (Entry<String, BookingSegments> entry : cancellationSegments.entrySet()) {
			AirCancellationDetail airCancellationDetail = AirCancellationDetail.builder().build();
			AbstractAirCancellationFactory factory =
					getCancellationFactory(entry.getValue(), order, false, airCancellationDetail);
			if (factory == null || CollectionUtils.isNotEmpty(entry.getValue().getCancelSegments())
					&& !factory.isSupplierAutoCancellationAllowed(factory.getSupplierConf().getSourceId())) {
				log.info("Auto Cancellation not allowed for booking {}", bookingid);
				isAutoCancellationAllowed = false;
				airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
				if (factory != null && CollectionUtils.isNotEmpty(factory.getCriticalMessageLogger())) {
					airCancellationDetail.buildMessageInfo(order.getBookingId(), entry.getValue(),
							AirSourceType
									.getAirSourceType(
											entry.getValue().getCancelSegments().get(0).getSupplierInfo().getSourceId())
									.name() + String.join(",", factory.getCriticalMessageLogger()));
				} else if (factory != null) {
					airCancellationDetail.buildMessageInfo(order.getBookingId(), entry.getValue(),
							"AutoCancellation is not enabled for supplier " + factory.getSupplierConf().getSupplierId()
									+ " PNR " + entry.getValue().getAirlinePNR());
				}
				cancellationDetails.add(airCancellationDetail);
				break;
			}
		}
		return isAutoCancellationAllowed;
	}


	public List<AirCancellationDetail> confirmCancellation(Map<String, BookingSegments> cancellationSegments,
			AirCancellationResponse airCancellationReviewResponse, Order order) {
		List<AirCancellationDetail> cancellationDetails = new ArrayList<>();
		log.info("Inside AirCancellationEngine#confirmCancellation for bookingId {}", order.getBookingId());
		boolean isAutoCancellationAllowed =
				checkAutoCancellationAllowed(order.getBookingId(), cancellationSegments, cancellationDetails, order);
		if (isAutoCancellationAllowed) {
			for (Entry<String, BookingSegments> entry : cancellationSegments.entrySet()) {
				if (CollectionUtils.isNotEmpty(entry.getValue().getCancelSegments())) {
					AirCancellationDetail airCancellationDetail = AirCancellationDetail.builder().build();
					entry.getValue().setAirlinePNR(entry.getKey());
					AbstractAirCancellationFactory factory =
							getCancellationFactory(entry.getValue(), order, false, airCancellationDetail);
					factory.setCancellationReview(Boolean.FALSE);
					factory.setAirCancellationReviewResponse(airCancellationReviewResponse);
					airCancellationDetail = factory.confirmCancellation();
					cancellationDetails.add(airCancellationDetail);
				}
			}
		}
		return cancellationDetails;
	}


	private AbstractAirCancellationFactory getCancellationFactory(BookingSegments bookingSegments, Order order,
			boolean isSupplierConfReq, AirCancellationDetail cancellationDetail) {
		SupplierBasicInfo basicInfo = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getSupplierBasicInfo();
		SupplierInfo si = SupplierConfigurationHelper.getSupplierInfo(basicInfo.getSupplierId());
		SupplierRule supplierRule = null;

		// Search with null key is not allowed.
		if (basicInfo.getRuleId() != null) {
			// Rule Id can be empty or may have value 0, -1. In those cases supplier rule will be empty and will be
			// considered as reissued order.
			supplierRule = SupplierConfigurationHelper.getSupplierRule(si.getSourceId(), basicInfo.getRuleId());
		}

		if (supplierRule == null) {
			log.info("Supplier Rule id empty {},unable to proceed cancellation ", order.getBookingId());
			if (cancellationDetail != null) {
				cancellationDetail.buildMessageInfo(order.getBookingId(), bookingSegments,
						"Reissue Order Can't be auto cancelled");
			}
			return null;
		}
		AbstractAirCancellationFactory factory = null;
		SupplierConfiguration supplierConf = SupplierConfigurationHelper.buildSupplierConfiguration(supplierRule,
				si.getSourceId(), basicInfo.getSupplierId(), true);
		AirSourceType sourceType = AirSourceType.getAirSourceType(basicInfo.getSourceId());
		factory = sourceType.getAbortBookingFactory(bookingSegments, order, supplierConf);
		return factory;
	}

}
