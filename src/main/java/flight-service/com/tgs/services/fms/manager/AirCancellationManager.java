package com.tgs.services.fms.manager;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.MessageType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.cancel.JourneyCancellationDetail;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.*;
import java.util.Map.Entry;

@Slf4j
@Service
public class AirCancellationManager {

	@Autowired
	private FareRuleManager fareRuleManager;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	public AirCancellationDetail mergeCancellationDetails(List<AirCancellationDetail> cancellationDetails, Order order,
			Map<String, BookingSegments> cancellationSegments) {

		AirCancellationDetail cancellationDetail = AirCancellationDetail.builder().build();

		if (CollectionUtils.isNotEmpty(cancellationDetails)) {

			cancellationDetail.setAutoCancellationAllowed(
					BooleanUtils.isTrue(cancellationDetails.get(0).getAutoCancellationAllowed()));

			cancellationDetails.forEach(detail -> {
				Map<String, MessageInfo> notAllowedSegments = detail.getCancellationNotAllowedSegments();
				if (MapUtils.isNotEmpty(notAllowedSegments)) {
					cancellationDetail.getNotAllowedSegments().putAll(notAllowedSegments);
				}
				mergeTravellerInfos(detail.getTravellers(), cancellationDetail);
				cancellationDetail
						.setAutoCancellationAllowed(BooleanUtils.isTrue(cancellationDetail.getAutoCancellationAllowed())
								&& BooleanUtils.isTrue(detail.getAutoCancellationAllowed()));
				if (StringUtils.isNotEmpty(detail.getErrorMessage())) {
					cancellationDetail.setErrorMessage(detail.getErrorMessage());
				}
			});

			if (BooleanUtils.isFalse(cancellationDetail.getAutoCancellationAllowed())
					&& cancellationDetails.size() > 1) {
				for (String key : cancellationSegments.keySet()) {
					cancellationDetail.buildMessageInfo(order.getBookingId(), cancellationSegments.get(key),
							"Multiple Supplier Cancellation Not Allowed");
				}
			}

		}
		return cancellationDetail;
	}

	private void mergeTravellerInfos(Map<Long, List<FlightTravellerInfo>> travellers,
			AirCancellationDetail cancellationDetail) {
		for (Map.Entry<Long, List<FlightTravellerInfo>> entry : travellers.entrySet()) {
			if (cancellationDetail.getTravellers().containsKey(entry.getKey())) {
				List<FlightTravellerInfo> flightTravellers = cancellationDetail.getTravellers().get(entry.getKey());
				List<FlightTravellerInfo> flightTravellers2 = entry.getValue();
				List<FlightTravellerInfo> maxTravellerSegments = new ArrayList<>(
						flightTravellers.size() > flightTravellers2.size() ? flightTravellers : flightTravellers2);
				List<FlightTravellerInfo> minTravellerSegments = new ArrayList<>(
						flightTravellers.size() < flightTravellers2.size() ? flightTravellers2 : flightTravellers);
				for (FlightTravellerInfo firstSegmentTraveller : maxTravellerSegments) {
					for (FlightTravellerInfo nextSegmentTraveller : minTravellerSegments) {
						if (firstSegmentTraveller.getId().equals(nextSegmentTraveller.getId())) {
							mergeTravellerCancellationFare(firstSegmentTraveller, nextSegmentTraveller);
						}
					}
				}
				cancellationDetail.getTravellers().put(entry.getKey(), maxTravellerSegments);
			} else {
				cancellationDetail.getTravellers().put(entry.getKey(), entry.getValue());
			}
		}
	}


	public void mergeTravellerCancellationFare(FlightTravellerInfo previousTravellerInfo,
			FlightTravellerInfo newTravellerInfo) {
		FareDetail previousFareDetail = previousTravellerInfo.getFareDetail();
		FareDetail currentFareDetail = newTravellerInfo.getFareDetail();
		previousFareDetail.getFareComponents().put(FareComponent.ACF,
				previousFareDetail.getFareComponents().getOrDefault(FareComponent.ACF, 0.0)
						+ (currentFareDetail.getFareComponents().getOrDefault(FareComponent.ACF, 0.0)));
	}

	public PriceInfo populateCancellationFare(AirCancellationDetail cancellationDetail) {
		PriceInfo cancellationPrice = null;
		// Calculate Pax Wise Cancellation Fare
		Map<PaxType, FareDetail> paxWiseFare = getCancellationPrices(cancellationDetail);
		if (MapUtils.isNotEmpty(paxWiseFare)) {
			cancellationPrice = PriceInfo.builder().build();
			cancellationPrice.setFareDetails(paxWiseFare);
			// Total Cancellation Fare
			FareDetail fareDetail = getTotalCancellationFare(paxWiseFare);
			cancellationPrice.setTotalFareDetail(fareDetail);
		}
		return cancellationPrice;
	}

	private Map<PaxType, FareDetail> getCancellationPrices(AirCancellationDetail cancellationDetail) {
		Map<PaxType, FareDetail> paxDetail = new HashMap<>();
		for (Map.Entry<Long, List<FlightTravellerInfo>> entry : cancellationDetail.getTravellers().entrySet()) {
			List<FlightTravellerInfo> travellerInfos = entry.getValue();
			travellerInfos.forEach(travellerInfo -> {
				FareDetail previousFareDetailFare =
						paxDetail.getOrDefault(travellerInfo.getPaxType(), new FareDetail());
				FareDetail updatedDetail = new GsonMapper<>(travellerInfo.getFareDetail(), FareDetail.class).convert();
				updatedDetail.getFareComponents().forEach(((fareComponent, amount) -> {
					Double initialAmount =
							previousFareDetailFare.getFareComponents().getOrDefault(fareComponent, new Double(0));
					updatedDetail.getFareComponents().put(fareComponent, initialAmount + amount);
				}));
				paxDetail.put(travellerInfo.getPaxType(), updatedDetail);
			});
		}
		return paxDetail;
	}

	private FareDetail getTotalCancellationFare(Map<PaxType, FareDetail> paxWiseFare) {
		FareDetail totalFare = new FareDetail();
		Map<FareComponent, Double> originalComponents = totalFare.getFareComponents();
		paxWiseFare.forEach((paxType, fareDetail) -> {
			fareDetail.getFareComponents().forEach((fareComponent, amount) -> {
				Double previousFare = originalComponents.getOrDefault(fareComponent, 0.0);
				originalComponents.put(fareComponent, previousFare + amount);
			});
		});
		return totalFare;
	}

	public void createJourneyCancellationDetails(List<TripInfo> tripInfos,
			AirCancellationDetail bookingCancellationDetail) {
		for (TripInfo tripInfo : tripInfos) {
			String tripInfoKey = tripInfo.getDepartureAirportCode() + "_" + tripInfo.getArrivalAirportCode() + "_"
					+ tripInfo.getDepartureTime().toString();
			if (!bookingCancellationDetail.getJourneyCancellationDetails().containsKey(tripInfoKey)) {
				for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
					for (Entry<Long, List<FlightTravellerInfo>> entry : bookingCancellationDetail.getTravellers()
							.entrySet()) {
						if (segmentInfo.getId().equals(entry.getKey().toString())) {
							JourneyCancellationDetail journeyCancellationDetail = null;
							Map<PaxType, FareDetail> paxDetail = null;
							List<FlightTravellerInfo> travellerInfos = entry.getValue();
							if (bookingCancellationDetail.getJourneyCancellationDetails().containsKey(tripInfoKey)) {
								journeyCancellationDetail =
										bookingCancellationDetail.getJourneyCancellationDetails().get(tripInfoKey);
								paxDetail = journeyCancellationDetail.getFareDetails();
							} else {
								journeyCancellationDetail = JourneyCancellationDetail.builder().build();
								journeyCancellationDetail.setTravellerInfos(new ArrayList<FlightTravellerInfo>());
								journeyCancellationDetail
										.setPnr(segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0).getPnr());
								journeyCancellationDetail.setDepartureDate(tripInfo.getDepartureTime().toString());
								journeyCancellationDetail.setDepartureAirportCode(tripInfo.getDepartureAirportCode());
								journeyCancellationDetail.setArrivalAirportCode(tripInfo.getArrivalAirportCode());
								paxDetail = new HashMap<PaxType, FareDetail>();
								journeyCancellationDetail.setPaxCount(new HashMap<PaxType, Integer>());
								journeyCancellationDetail.getPaxCount().put(PaxType.ADULT, CollectionUtils
										.size(AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT)));
								journeyCancellationDetail.getPaxCount().put(PaxType.CHILD, CollectionUtils
										.size(AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD)));
								journeyCancellationDetail.getPaxCount().put(PaxType.INFANT, CollectionUtils
										.size(AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT)));
							}
							for (FlightTravellerInfo travellerInfo : travellerInfos) {
								FareDetail previousFareDetailFare =
										paxDetail.getOrDefault(travellerInfo.getPaxType(), new FareDetail());
								FareDetail updatedDetail =
										new GsonMapper<>(travellerInfo.getFareDetail(), FareDetail.class).convert();
								updatedDetail.getFareComponents().forEach(((fareComponent, amount) -> {
									Double initialAmount = previousFareDetailFare.getFareComponents()
											.getOrDefault(fareComponent, new Double(0));
									updatedDetail.getFareComponents().put(fareComponent, amount + initialAmount);
								}));
								previousFareDetailFare.getFareComponents().forEach(((fareComponent, amount) -> {
									if (!updatedDetail.getFareComponents().containsKey(fareComponent)) {
										updatedDetail.getFareComponents().put(fareComponent, amount);
									}
								}));
								// createPassengerWiseCancellationDetails(journeyCancellationDetail, travellerInfo,
								// paxDetail);
								paxDetail.put(travellerInfo.getPaxType(), updatedDetail);

							}
							journeyCancellationDetail.setFareDetails(paxDetail);
							bookingCancellationDetail.getJourneyCancellationDetails().put(tripInfoKey,
									journeyCancellationDetail);
						}
					}
				}
				if (bookingCancellationDetail.getJourneyCancellationDetails().containsKey(tripInfoKey)) {
					Map<PaxType, FareDetail> fareDetails =
							bookingCancellationDetail.getJourneyCancellationDetails().get(tripInfoKey).getFareDetails();
					for (Entry<PaxType, FareDetail> entry : fareDetails.entrySet()) {
						entry.getValue().getFareComponents().forEach(((fareComponent, amount) -> {
							Integer paxTypeCount = bookingCancellationDetail.getJourneyCancellationDetails()
									.get(tripInfoKey).getPaxCount().get(entry.getKey());
							entry.getValue().getFareComponents().put(fareComponent, amount / paxTypeCount);
						}));
					}
				}
			}
		}
	}

	// Note : Not In Use : Phase : I - Support "N" Cancellation of Same Booking
	private void createPassengerWiseCancellationDetails(JourneyCancellationDetail journeyCancellationDetail,
			FlightTravellerInfo travellerInfo, Map<PaxType, FareDetail> paxDetail) {

		if (journeyCancellationDetail.getTravellerInfos().isEmpty()) {
			journeyCancellationDetail.getTravellerInfos()
					.add(new GsonMapper<>(travellerInfo, FlightTravellerInfo.class).convert());
		} else {
			boolean isFlightTravellerInfoExist = false;
			for (FlightTravellerInfo cancelledTravellerInfo : journeyCancellationDetail.getTravellerInfos()) {
				if (cancelledTravellerInfo.getId().equals(travellerInfo.getId())) {
					isFlightTravellerInfoExist = true;
					FareDetail cancelledPreviousFareDetailFare = cancelledTravellerInfo.getFareDetail();
					FareDetail cancelledUpdatedDetail =
							new GsonMapper<>(cancelledTravellerInfo.getFareDetail(), FareDetail.class).convert();
					cancelledUpdatedDetail.getFareComponents().forEach(((fareComponent, amount) -> {
						Double initialAmount = cancelledPreviousFareDetailFare.getFareComponents()
								.getOrDefault(fareComponent, new Double(0));
						cancelledUpdatedDetail.getFareComponents().put(fareComponent, initialAmount + amount);
					}));
					cancelledPreviousFareDetailFare.getFareComponents().forEach(((fareComponent, amount) -> {
						if (!cancelledUpdatedDetail.getFareComponents().containsKey(fareComponent)) {
							cancelledUpdatedDetail.getFareComponents().put(fareComponent, amount);
						}
					}));
					// cancelledTravellerInfo.setFareDetail(cancelledPreviousFareDetailFare);
					break;
				}
			}
			if (!isFlightTravellerInfoExist) {
				journeyCancellationDetail.getTravellerInfos()
						.add(new GsonMapper<>(travellerInfo, FlightTravellerInfo.class).convert());
			}
		}
	}

	public void checkZeroCancellationFees(AirCancellationDetail cancellationDetail, String bookingId) {
		for (Entry<String, JourneyCancellationDetail> entry : cancellationDetail.getJourneyCancellationDetails()
				.entrySet()) {
			for (Entry<PaxType, FareDetail> fareDetail : entry.getValue().getFareDetails().entrySet()) {
				if (!fareDetail.getKey().equals(PaxType.INFANT)
						&& fareDetail.getValue().getFareComponents().getOrDefault(FareComponent.ACF, 0.0) <= 0) {
					log.error("Cancellation fee is zero for bookingId {}, fare details are {}", bookingId,
							entry.getValue().getFareDetails());
					cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
					cancellationDetail.addMessageToSystemRemarks("Cancellation fee is zero");
					break;
				}
			}
		}
	}

	public void isCancellationFareGreater(AirCancellationDetail cancellationDetail, String bookingId) {
		if (MapUtils.isNotEmpty(cancellationDetail.getJourneyCancellationDetails())) {
			Double cancellationCharges = cancellationDetail.getPaxTypeCancellationCharges();
			Double totalFare = 0.0;
			for (Entry<String, JourneyCancellationDetail> entry : cancellationDetail.getJourneyCancellationDetails()
					.entrySet()) {
				for (Entry<PaxType, FareDetail> passenger : entry.getValue().getFareDetails().entrySet()) {
					totalFare += passenger.getValue().getFareComponents().get(FareComponent.TF);
				}
			}
			log.info("Cancellation Charges for bookingId {}, cancellationFare {}, TotalFare {}", bookingId,
					cancellationCharges, totalFare);
			if (cancellationCharges >= totalFare) {
				cancellationDetail.setAutoCancellationAllowed(Boolean.FALSE);
				String message = "Cancellation fee is greater than total fare";
				cancellationDetail.addMessageToSystemRemarks(message);
				log.info("{} for bookingId {}, cancellationFare {}, TotalFare {}", message, bookingId,
						cancellationCharges, totalFare);
			}
		}
	}
}
