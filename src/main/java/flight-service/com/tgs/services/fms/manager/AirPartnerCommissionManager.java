package com.tgs.services.fms.manager;

import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.ruleengine.UserFeeAirRuleField;
import com.tgs.services.base.ruleengine.UserFeeFact;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.communicator.impl.AirUserFeeServiceCommunicator;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Iterator;
import java.util.Map;

@Slf4j
@Service
public class AirPartnerCommissionManager {

	static Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(UserFeeAirRuleField.class);

	@Autowired
	AirUserFeeServiceCommunicator userFeeService;

	@Autowired
	UserServiceCommunicator userComm;

	static String DELIMITER = "_";

	static String CONTEXTDATA_KEY = "P_COMM";

	public void processPartnerCommission(User user, TripInfo tripInfo, AirSearchQuery searchQuery) {

		if (user == null)
			return;

		try {
			String partnerId =
					!UserRole.WHITELABEL_STAFF.equals(user.getRole()) ? UserUtils.getPartnerUserId(user) : null;
			// In case of B2CWL - partner has access to set top markup on top of it
			String resellerId = user != null ? user.getParentUserId() : null;

			List<UserFee> partnerRules = userComm.getUserFee(partnerId, Product.AIR, UserFeeType.PARTNER_COMMISSION);
			if (CollectionUtils.isNotEmpty(partnerRules)) {
				// filter based on reseller id
				UserFee commissionRule = getResellerRules(partnerRules, tripInfo, resellerId);
				log.debug("Applying Partner Commission Plan for {} , userfeeid {} ", resellerId,
						commissionRule.getId());
				if (commissionRule != null) {
					Double percentage = commissionRule.getUserFee().getValue();
					for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos())
						for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
							if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
								priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
									if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
										Map<FareComponent, Double> fareComps = fareDetail.getFareComponents();
										Map<FareComponent, Double> tempMap = new HashMap<>();
										for (FareComponent resellerComp : fareComps.keySet()) {
											Double amount = fareComps.get(resellerComp);
											if (!FareComponent.TDS.equals(resellerComp)
													&& !FareComponent.PCTDS.equals(resellerComp)) {
												tempMap.put(resellerComp, amount);
											}
											if (amount > 0 && resellerComp.getPartnerComponent() != null
													&& FareComponent.getCommissionComponents().contains(resellerComp)) {
												FareComponent partnerComp = resellerComp.getPartnerComponent();
												Double totalComm = resellerComp.getAmount(fareComps.get(resellerComp));
												Double resellerComm = totalComm * percentage / 100;
												Double partnerComm = totalComm - resellerComm;
												tempMap.put(partnerComp, partnerComm);
												tempMap.put(resellerComp, resellerComm);

												// Reseller TDS
												updateTDS(resellerComp, resellerComm, user, tempMap);
												// Partner TDS
												updateTDS(partnerComp, partnerComm, user, tempMap);
											}
										}
										fareDetail.setFareComponents(tempMap);
									}
								});
							}
						}
				}
			}
		} catch (Exception e) {
			log.error("Partner Commission Couldn't be applied for {} for trip {}", tripInfo.getTripKey(),
					searchQuery.getSearchId(), e);
		}
	}

	private void updateTDS(FareComponent targetComp, Double amount, User user, Map<FareComponent, Double> tempMap) {
		for (FareComponent dependentComponent : targetComp.dependentComponents()) {
			if (dependentComponent != null) {
				double dependentComponentAmount =
						BaseUtils.calculateDependentFareComponent(dependentComponent, amount, user);
				tempMap.put(dependentComponent,
						tempMap.getOrDefault(dependentComponent, 0.0) + dependentComponentAmount);
				if (dependentComponent.isUpdateTotalFare()) {
					tempMap.put(FareComponent.TF,
							tempMap.getOrDefault(FareComponent.TF, 0.0) + dependentComponentAmount);
				}
			}
		}
	}

	private UserFee getResellerRules(List<UserFee> partnerRules, TripInfo tripInfo, String resellerId) {
		ContextData contextData = SystemContextHolder.getContextData();
		AirType airType = AirUtils.getAirType(tripInfo);
		String platingCarrier = tripInfo.getPlatingCarrier();
		String key = getContextKey(resellerId, platingCarrier, airType);
		if (contextData.getValue(key) != null) {
			return (UserFee) contextData.getValue(key).get();
		} else {
			UserFeeFact userFeeFact = UserFeeFact.builder().build();
			userFeeFact.setAirline(platingCarrier);
			userFeeFact.setAirType(airType);
			userFeeFact.setUserId(resellerId);
			CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(partnerRules, userFeeFact, fieldResolverMap);
			// this will filter based on airtype,airline,sub userid
			List<UserFee> userMatchedRules = (List<UserFee>) ruleEngine.fireAllRules();
			if (CollectionUtils.isNotEmpty(userMatchedRules)) {
				contextData.setValue(key, userMatchedRules.get(0));
				return userMatchedRules.get(0);
			}
		}
		return null;
	}


	private String getContextKey(String resellerId, String platingCarrier, AirType airType) {
		String key = StringUtils.join(CONTEXTDATA_KEY, DELIMITER);
		if (StringUtils.isNotBlank(resellerId)) {
			key = StringUtils.join(key, resellerId, DELIMITER, platingCarrier, DELIMITER, airType);
		} else {
			key = StringUtils.join(key, platingCarrier, DELIMITER, airType);
		}
		return key;
	}


}
