package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.tgs.services.base.enums.UserRole;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.ruleengine.UserAirFeeRuleCriteria;
import com.tgs.services.base.ruleengine.UserFeeAirRuleField;
import com.tgs.services.base.ruleengine.UserFeeFact;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.communicator.impl.AirUserFeeServiceCommunicator;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.AmountType;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirPartnerUserFeeManager {

	static Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(UserFeeAirRuleField.class);

	@Autowired
	AirUserFeeServiceCommunicator userFeeService;

	@Autowired
	UserServiceCommunicator userComm;

	static String DELIMITER = "_";

	static String CONTEXTDATA_KEY = "P_MARKUP";

	public void processPartnerMarkUp(User user, TripInfo tripInfo, UserFeeType feeType, String searchOrBookingId) {
		try {
			String partnerUserId = (user != null && !UserRole.WHITELABEL_STAFF.equals(user.getRole()))
					? UserUtils.getPartnerUserId(user)
					: null;
			User partnerUser = userComm.getUserFromCache(partnerUserId);
			// In case of B2CWL - partner has access to set top markup on top of it
			String resellerId = user != null ? UserUtils.getParentUserId(user) : null;
			List<UserFee> partnerRules =
					!UserUtils.isPartnerUser(user) ? null : userComm.getUserFee(partnerUserId, Product.AIR, feeType);
			if (CollectionUtils.isNotEmpty(partnerRules)) {
				// filter based on reseller id
				List<UserFee> userFeeRules = getResellerRules(partnerRules, tripInfo, resellerId);
				if (CollectionUtils.isNotEmpty(userFeeRules)) {
					for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos())
						for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
							if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
								String fareType = priceInfo.getFareType();
								priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
									UserFee userFee = getUserFee(userFeeRules, paxType, fareType);
									if (userFee != null) {
										setUserFeeOnFareDetail(user, partnerUser, userFee.getUserFee(), segmentInfo,
												fareDetail, UserFeeType.PARTNER_MARKUP);
									}
								});
							}
						}
				}
			}
		} catch (Exception e) {
			log.error("Partner Markup Couldn't be applied {} for trip {}", searchOrBookingId, tripInfo.getTripKey(), e);
		}
	}

	public List<UserFee> getResellerRules(List<UserFee> partnerRules, TripInfo tripInfo, String resellerId) {
		ContextData contextData = SystemContextHolder.getContextData();
		String platingCarrier = tripInfo.getPlatingCarrier();
		AirType airType = AirUtils.getAirType(tripInfo);
		String key = getContextKey(resellerId, platingCarrier, airType);

		if (contextData.getValue(key) != null) {
			return (List<UserFee>) contextData.getValue(key).get();
		} else {
			UserFeeFact userFeeFact = UserFeeFact.builder().build();
			userFeeFact.setAirline(platingCarrier);
			userFeeFact.setAirType(airType);
			userFeeFact.setUserId(resellerId);
			CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(partnerRules, userFeeFact, fieldResolverMap);
			// this will filter based on airtype,airline,sub userid
			List<UserFee> userMatchedRules = (List<UserFee>) ruleEngine.fireAllRules();
			if (CollectionUtils.isNotEmpty(userMatchedRules)) {
				contextData.setValue(key, userMatchedRules);
				return userMatchedRules;
			}
		}
		return null;
	}

	private String getContextKey(String resellerId, String platingCarrier, AirType airType) {
		String key = StringUtils.join(CONTEXTDATA_KEY, DELIMITER);
		if (StringUtils.isNotBlank(resellerId)) {
			key = StringUtils.join(key, resellerId, DELIMITER, platingCarrier, DELIMITER, airType);
		} else {
			key = StringUtils.join(key, platingCarrier, DELIMITER, airType);
		}
		return key;
	}

	private UserFee getUserFee(List<UserFee> userFeeRules, PaxType paxType, String fareType) {
		if (CollectionUtils.isEmpty(userFeeRules)) {
			return null;
		}
		Map<PaxType, List<UserFee>> paxTypeListMap = new HashMap<>();

		for (PaxType pType : PaxType.values()) {
			List<UserFee> tempList = new ArrayList<>();
			for (UserFee userFee : userFeeRules) {
				UserAirFeeRuleCriteria ruleCriteria = (UserAirFeeRuleCriteria) userFee.getInclusionCriteria();
				if (ruleCriteria != null) {
					Boolean matchesPaxType = null;
					if (CollectionUtils.isNotEmpty(ruleCriteria.getPaxTypes())) {
						matchesPaxType = ruleCriteria.getPaxTypes().contains(paxType.getCode());
					}

					if (Objects.isNull(matchesPaxType)) {
						tempList.add(userFee);
					} else if (!BooleanUtils.isFalse(matchesPaxType)) {
						tempList.add(userFee);
					}
				}
			}
			paxTypeListMap.put(pType, tempList);
		}

		List<UserFee> paxUseeFees = paxTypeListMap.get(paxType);

		UserFee userFee = null;
		if (CollectionUtils.isNotEmpty(paxUseeFees)) {
			userFee = UserFee.getFareTypeBasedUserFee(paxUseeFees, Product.AIR, fareType);
		}
		return userFee != null ? userFee : UserFee.getUserFee(paxUseeFees, Product.AIR);
	}


	private void setUserFeeOnFareDetail(User user, User partner, UserFeeAmount feeAmount, SegmentInfo segmentInfo,
			FareDetail fareDetail, UserFeeType feeType) {

		AmountType amountType = AmountType.valueOf((feeAmount.getAmountType()));
		if (amountType.equals(AmountType.FIXED) && segmentInfo.getSegmentNum().intValue() == 0) {
			// Fixed User Fee
			userFeeService.setUserFeeAndUpdateTotalFare(user, partner, fareDetail, feeAmount.getValue(), feeType);
		} else if (amountType.equals(AmountType.PERCENTAGE)) {
			// Percentage User Fee - Calculate Percentage on Total fare
			double userFee =
					fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0) * (feeAmount.getValue() / 100);
			userFeeService.setUserFeeAndUpdateTotalFare(user, partner, fareDetail, userFee, feeType);
		}
	}

}
