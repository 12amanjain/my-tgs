package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.datamodel.PointsConfiguration;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import com.tgs.services.points.datamodel.AirRedeemPointsData;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.datamodel.PointsOutputCriteria;
import com.tgs.services.points.restmodel.AirRedeemPointResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirPointsManager {

	@Autowired
	private AirBookingEngine bookingEngine;

	@Autowired
	private AirOrderItemCommunicator orderItemCommunicator;

	static Map<String, ? extends IRuleField> resolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

	private List<TripInfo> fetchTripInfosByBookingId(String bookingId) {
		List<TripInfo> tripInfos = new ArrayList<>();
		try {
			AirReviewResponse reviewResponse = bookingEngine.getAirReviewResponse(bookingId, null);
			if (reviewResponse != null && CollectionUtils.isNotEmpty(reviewResponse.getTripInfos())) {
				tripInfos = reviewResponse.getTripInfos();
			}
		} catch (Exception e) {
			log.info("AirReviewResponse not available for {} ", bookingId);
		}
		if (CollectionUtils.isEmpty(tripInfos)) {
			tripInfos = orderItemCommunicator.findTripByBookingId(bookingId);
		}
		if (CollectionUtils.isEmpty(tripInfos)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
		return tripInfos;
	}

	public boolean calculatePointsOnTrip(TripInfo tripInfo, RedeemPointsResponse response, boolean modifyFareDetail,
			PointsOutputCriteria outputCriteria, AirRedeemPointsData request) {
		boolean isPointsApplied = false;
		PointsConfiguration clientPointConfig = request.getPointsConfiguration();
		List<TripInfo> tripInfos = tripInfo.splitTripInfo(false);
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			TripInfo trip = tripInfos.get(0);
			Integer totalPaxCount = trip.getPaxCountWithoutInfant();
			Double perPaxPoint = request.getPoints() / totalPaxCount;
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
				if (segmentInfo.getSegmentNum().equals(0)) {
					Map<PaxType, FareDetail> fareDetails = segmentInfo.getPriceInfo(0).getFareDetails();
					for (Map.Entry<PaxType, FareDetail> entry : fareDetails.entrySet()) {
						if (!PaxType.INFANT.equals(entry.getKey())) {
							Integer paxCount = tripInfo.getPaxInfo().getOrDefault(entry.getKey(), 0);
							double perPaxPointsToAmount = 0;
							if (outputCriteria != null && StringUtils.isNotBlank(outputCriteria.getExpression())) {
								Double tripPoints = BaseUtils.evaluateExpression(outputCriteria.getExpression(),
										entry.getValue(), null, true);
								if (isMaxAllowed(perPaxPoint, tripPoints)) {
									perPaxPointsToAmount = perPaxPoint * clientPointConfig.getAmountOfOnePoint();
								}
							} else if (outputCriteria == null) {
								perPaxPointsToAmount = perPaxPoint * clientPointConfig.getAmountOfOnePoint();
							}
							if (modifyFareDetail) {
								calcOrValidateRedeemPoint(entry.getValue(), perPaxPointsToAmount);
							}
							if (perPaxPointsToAmount > 0) {
								response.setAmount(perPaxPointsToAmount * paxCount);
								isPointsApplied = true;
							}
						}
					}
				}
			}
		}
		return isPointsApplied;
	}

	private void calcOrValidateRedeemPoint(FareDetail fareDetail, Double perPaxPointAmount) {
		fareDetail.getFareComponents().put(FareComponent.RP, perPaxPointAmount);
	}

	public AirRedeemPointResponse validatePoints(AirRedeemPointsData request, boolean modifyFareDetail) {
		String bookingId = request.getBookingId();
		ContextData contextData = SystemContextHolder.getContextData();
		AirRedeemPointResponse response = new AirRedeemPointResponse();
		if (StringUtils.isNotBlank(request.getBookingId())) {
			List<TripInfo> tripInfos = CollectionUtils.isNotEmpty(request.getTripInfos()) ? request.getTripInfos()
					: fetchTripInfosByBookingId(request.getBookingId());
			PointsConfiguration clientPointConfig = request.getPointsConfiguration();
			boolean isPointsApplied = false;
			// always applies to first trip
			for (TripInfo tripInfo : tripInfos) {
				if (!isPointsApplied) {
					AirType airType = AirUtils.getAirType(tripInfo);
					Integer paxCount = tripInfo.getPaxCountWithoutInfant();
					FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(tripInfo);
					flightFact.setUserId(UserUtils.getUserId(contextData.getUser()));
					flightFact.setRole(contextData.getUser().getRole());
					flightFact.setAirType(airType);
					List<PointsConfigurationRule> requestedRules = request.getPointRules();
					if (CollectionUtils.isNotEmpty(requestedRules))
						requestedRules.forEach(rule -> rule.setProduct(Product.AIR));
					CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(requestedRules, flightFact, resolverMap);
					List<PointsConfigurationRule> rules = (List<PointsConfigurationRule>) ruleEngine.fireAllRules();
					if (CollectionUtils.isNotEmpty(rules)) {
						PointsConfigurationRule pointRule = rules.get(0);
						log.info("Redeem Points Applied for {} PointConfig Rule {} ", bookingId, pointRule);
						PointsOutputCriteria outputCriteria = pointRule.getOutput();
						isPointsApplied =
								calculatePointsOnTrip(tripInfo, response, modifyFareDetail, outputCriteria, request);
						log.info("Reward Point Converted to Amount for {} amount {}", bookingId, response.getAmount());
					}
					if (!isPointsApplied && isMaxAllowed(request.getPoints(),
							clientPointConfig.getAirMaxRedeemablePoints(airType) * paxCount)) {
						log.info("Redeem Points Applied for {} ClientConfig Rule", bookingId);
						// response.setAmount(request.getPoints() * clientPointConfig.getAmountOfOnePoint());
						isPointsApplied = calculatePointsOnTrip(tripInfo, response, modifyFareDetail, null, request);
						log.info("Reward Point Converted to Amount for {} amount {}", bookingId, response.getAmount());
					}
					log.info("Redeem Point Applied to Trip for {} amount {} ", bookingId, response.getAmount());
				}
			}
			if (CollectionUtils.isNotEmpty(tripInfos)) {
				response.setTripInfos(tripInfos);
			}
		}
		return response;
	}

	public boolean isMaxAllowed(Double userReqPoint, Double maxPoints) {
		if (userReqPoint > maxPoints) {
			String message = StringUtils.join(SystemError.MAX_ALLOWED_POINTS.getMessage(), maxPoints);
			throw new CustomGeneralException(SystemError.MAX_ALLOWED_POINTS, message);
		}
		return true;
	}
}
