package com.tgs.services.fms.manager;

import java.util.Arrays;
import java.util.List;

import lombok.Setter;
import org.springframework.stereotype.Service;

import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;

@Setter
@Service
public class AirReviewEngine extends AbstractAirReviewEngine {

	@Override
	public List<TripInfo> reviewTrips(List<TripInfo> tripInfos, List<AirSearchQuery> searchQueries, ContextData contextData, String bookingId) {
		return Arrays.asList(reviewTrip(tripInfos.get(0), searchQueries.get(0), false,contextData,bookingId));
	}

}
