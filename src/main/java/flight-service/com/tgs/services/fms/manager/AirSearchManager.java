package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirPath;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.NonOperatingSectorInfo;
import com.tgs.services.fms.helper.AirSearch;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.helper.CombinationAirSearch;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.helper.SourceRouteInfoHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirSearchManager {

	@Autowired
	FMSCachingServiceCommunicator cachingService;

	@Autowired
	FlightCacheHandler cacheHandler;

	@Autowired
	UserServiceCommunicator userService;

	@Autowired
	AirSearch airSearch;

	@Autowired
	CombinationAirSearch combiSearch;

	@Autowired
	AirSearchResultProcessingManager searchResultProcessingManager;

	public static String getSearchQueryStatusString(String searchId) {
		return StringUtils.join(searchId, "STATUS");
	}

	public List<AirSearchQuery> getSearchQueryList(AirSearchQuery airSearchQuery, ContextData contextData) {
		LogUtils.log("searchlist#1", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"system#filter");
		String randomId = airSearchQuery.getRequestId();
		List<AirSearchQuery> searchQueryList = new ArrayList<>();
		List<Integer[]> sourceIdList = AirUtils.getSearchGroupingList(airSearchQuery, contextData.getUser());

		Map<String, List<Integer>> prefSourceIdMap = null;
		if (CollectionUtils.isNotEmpty(airSearchQuery.getPreferredAirline())) {
			prefSourceIdMap = AirUtils.getPreferredAirlineSourceList(contextData.getUser());
		}
		for (AirSearchQuery searchQuery : generateSearchQueryBasedOnMultiCitySearch(airSearchQuery, randomId)) {
			for (Integer[] sourceIds : sourceIdList) {
				String sourceIdStr = "";
				AirSearchQuery copyQuery = new GsonMapper<>(searchQuery, AirSearchQuery.class).convert();
				for (Integer sourceId : sourceIds) {
					/**
					 * Search should have valid route in routeInfo or valid route using NearBy. Along with it if
					 * sourceId is passed at the time of search then only that sourceId is applicable for search
					 */

					boolean isValidSourceId = isValidPrefferedSourceId(searchQuery, sourceId, prefSourceIdMap);
					AirSourceType sourceType = AirSourceType.getAirSourceType(sourceId);
					if (sourceType.isValidSearchQuery(copyQuery)
							&& sourceType.allowedCabinClasses().contains(searchQuery.getCabinClass()) && isValidSourceId
							&& ((validateRouteUsingNearByAirport(copyQuery, sourceId, contextData.getUser())
									|| validateRoute(copyQuery, sourceId, contextData.getUser()))
									&& (copyQuery.getSearchModifiers().getSourceId() == null
											|| copyQuery.getSearchModifiers().getSourceId() == sourceId))) {
						sourceIdStr = getSourceIdString(sourceType, sourceIdStr);
						copyQuery.addSourceId(sourceId);
					}
				}
				if (copyQuery.getSourceIds() != null) {
					copyQuery.setSearchId(sourceIdStr + copyQuery.getSearchId());
					searchQueryList.add(copyQuery);
				}
			}
		}

		LogUtils.log("searchlist#2", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"system#filter");

		if (isAnyCustomCombinationDefined(airSearchQuery, contextData)) {
			AirSearchQuery copyQuery = new GsonMapper<>(airSearchQuery, AirSearchQuery.class).convert();
			copyQuery.setIsCustomCombination(true);
			copyQuery.setSearchId("COMB-" + copyQuery.getSearchId());
			searchQueryList.add(copyQuery);
		}
		return searchQueryList;
	}

	private String getSourceIdString(AirSourceType sourceType, String sourceIdStr) {
		if (UserUtils.isMidOfficeRole(
				UserUtils.getEmulatedUserRoleOrUserRoleCode(SystemContextHolder.getContextData().getUser()))) {
			sourceIdStr = String.join("-", sourceType.name().toLowerCase() + "", sourceIdStr);
		} else {
			sourceIdStr = String.join("-", sourceType.getSourceId() + "", sourceIdStr);
		}
		return sourceIdStr;
	}

	/**
	 * This will give valid source that needs to be hit for search, Based on airline code , we will get list of sources
	 * that are allowed for that airline, However We need to remove those source which contains the airline in their
	 * excluded airline list.
	 * 
	 * @param searchQuery
	 * @param sourceId
	 * @return
	 */

	private boolean isValidPrefferedSourceId(AirSearchQuery searchQuery, Integer sourceId,
			Map<String, List<Integer>> prefSourceIdMap) {
		boolean isValidSourceId = true;
		if (searchQuery.getPreferredAirline() != null && MapUtils.isNotEmpty(prefSourceIdMap)) {
			isValidSourceId = false;
			for (AirlineInfo airlineInfo : searchQuery.getPreferredAirline()) {
				isValidSourceId = isValidSourceId || prefSourceIdMap
						.getOrDefault(airlineInfo.getCode(), prefSourceIdMap.get("ALL")).contains(sourceId);
			}
		}
		return isValidSourceId;
	}

	public boolean isAnyCustomCombinationDefined(AirSearchQuery airSearchQuery, ContextData contextData) {
		List<AirPath> pathList = AirUtils.getCustomPaths(airSearchQuery, contextData.getUser());
		return CollectionUtils.isNotEmpty(pathList) ? true : false;
	}

	public List<AirSearchQuery> generateSearchQueryBasedOnMultiCitySearch(AirSearchQuery airSearchQuery,
			String randomId) {
		List<AirSearchQuery> searchQueryList = new ArrayList<>();
		if (airSearchQuery.isDomesticMultiCity()) {
			for (int i = 0; i < airSearchQuery.getRouteInfos().size(); i++) {
				AirSearchQuery copyQuery = new GsonMapper<>(airSearchQuery, AirSearchQuery.class).convert();
				copyQuery.setOrigSearchType(SearchType.MULTICITY);
				copyQuery.setRouteInfos(new ArrayList<>());
				copyQuery.setSearchId(StringUtils.join(randomId, i));
				copyQuery.getRouteInfos().add(airSearchQuery.getRouteInfos().get(i));
				copyQuery.setOrigRouteInfoSize(airSearchQuery.getRouteInfos().size());
				// In case of Dom multicity PNR credit balance should be divided equally to all the search query.
				if (airSearchQuery.isPNRCreditSearch()
						&& airSearchQuery.getSearchModifiers().getPnrCreditInfo().getCreditBalance() > 0) {
					copyQuery.getSearchModifiers().getPnrCreditInfo()
							.setCreditBalance(airSearchQuery.getSearchModifiers().getPnrCreditInfo().getCreditBalance()
									/ airSearchQuery.getRouteInfos().size());
				}

				searchQueryList.add(copyQuery);
			}
		} else {
			airSearchQuery.setSearchId(randomId);
			airSearchQuery.setOrigRouteInfoSize(airSearchQuery.getRouteInfos().size());
			searchQueryList.add(airSearchQuery);
		}
		return searchQueryList;
	}

	public boolean validateRoute(AirSearchQuery airSearchQuery, Integer sourceId, User bookingUser) {
		RouteInfo routeInfo = airSearchQuery.getRouteInfos().iterator().next();
		NonOperatingSectorInfo sourceRouteInfo =
				NonOperatingSectorInfo.builder().source(routeInfo.getFromCityAirportCode())
						.destination(routeInfo.getToCityAirportCode()).sourceId(sourceId).build();
		return SourceRouteInfoHelper.isValidRoute(sourceRouteInfo)
				&& !SourceRouteInfoHelper.isNoResultSector(airSearchQuery.getRouteInfos(), sourceId, bookingUser);
	}

	public boolean validateRouteUsingNearByAirport(AirSearchQuery airSearchQuery, Integer sourceId, User bookingUser) {
		RouteInfo routeInfo = airSearchQuery.getRouteInfos().iterator().next();
		boolean isRouteExist = false;
		String nearByDepartureAirport = AirUtils.getNearByAirport(routeInfo.getFromCityOrAirport().getCode(), sourceId,
				airSearchQuery, bookingUser);
		if (StringUtils.isNotBlank(nearByDepartureAirport)
				&& SourceRouteInfoHelper.isValidRoute(NonOperatingSectorInfo.builder().source(nearByDepartureAirport)
						.destination(routeInfo.getToCityOrAirport().getCode()).sourceId(sourceId).build())) {
			airSearchQuery.getRouteInfos().get(0)
					.setNearByFromCityOrAirport(AirportHelper.getAirportInfo(nearByDepartureAirport));
			if (airSearchQuery.getRouteInfos().size() == 2) {
				airSearchQuery.getRouteInfos().get(1)
						.setNearByToCityOrAirport(AirportHelper.getAirportInfo(nearByDepartureAirport));
			}
			isRouteExist =
					!SourceRouteInfoHelper.isNoResultSector(airSearchQuery.getRouteInfos(), sourceId, bookingUser);
		} else {
			String nearByArrivalAirport = AirUtils.getNearByAirport(routeInfo.getToCityOrAirport().getCode(), sourceId,
					airSearchQuery, bookingUser);
			if (StringUtils.isNotBlank(nearByArrivalAirport) && SourceRouteInfoHelper
					.isValidRoute(NonOperatingSectorInfo.builder().source(routeInfo.getFromCityOrAirport().getCode())
							.destination(nearByArrivalAirport).sourceId(sourceId).build())) {
				airSearchQuery.getRouteInfos().get(0)
						.setNearByToCityOrAirport(AirportHelper.getAirportInfo(nearByArrivalAirport));
				if (airSearchQuery.getRouteInfos().size() == 2) {
					airSearchQuery.getRouteInfos().get(1)
							.setNearByFromCityOrAirport(AirportHelper.getAirportInfo(nearByArrivalAirport));
				}
				isRouteExist =
						!SourceRouteInfoHelper.isNoResultSector(airSearchQuery.getRouteInfos(), sourceId, bookingUser);
			}
		}
		return isRouteExist;
	}

	public AirSearchResult doSearch(AirSearchQuery airSearchQuery, ContextData contextData) {
		AirSearchResult searchResult = new AirSearchResult();
		try {
			LogUtils.log(LogTypes.AIRSEARCH_DOSEARCH, LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
					.searchId(airSearchQuery.getSearchId()).build(), "searchcontroller");
			// contextData.setUser(userService.getUserFromCache(UserUtils.getUserId(contextData.getUser())));
			if (BooleanUtils.isTrue(airSearchQuery.getIsCustomCombination())) {
				searchResult = combiSearch.search(airSearchQuery, contextData);
			} else {
				searchResult = airSearch.search(airSearchQuery, contextData);
			}

			if (searchResult != null) {
				LogUtils.log(LogTypes.AIRSEARCH, LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
						.searchId(airSearchQuery.getSearchId()).build(), null);
				searchResultProcessingManager.process(searchResult, airSearchQuery, null, contextData.getUser());
				LogUtils.log(LogTypes.AIRSEARCH_PROCESS,
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
								.searchId(airSearchQuery.getSearchId()).trips(searchResult.getTotalTrips()).build(),
						LogTypes.AIRSEARCH);

				for (String key : searchResult.getTripInfos().keySet()) {
					cacheHandler.storeTripKey(searchResult, airSearchQuery);
					cachingService.cacheFlightListing(searchResult.getTripInfos().get(key),
							airSearchQuery.getSearchId() + key,
							CacheMetaInfo.builder()
									.expiration(CacheType.SEARCHIDEXPIRATION.getTtl(contextData.getUser()))
									.set(CacheSetName.FLIGHT_SEARCH.getName()).build());
				}
				LogUtils.log(LogTypes.AIRSEARCH_STORE,
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
								.searchId(airSearchQuery.getSearchId()).trips(searchResult.getTotalTrips()).build(),
						LogTypes.AIRSEARCH_PROCESS);
			}
		} finally {
			cachingService
					.deleteRecord(CacheMetaInfo.builder().key(getSearchQueryStatusString(airSearchQuery.getSearchId()))
							.set(CacheSetName.FLIGHT_SEARCH.getName()).build());
		}
		return searchResult;
	}

}
