package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import com.tgs.services.base.ruleengine.UserAirFeeRuleCriteria;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.communicator.impl.AirUserFeeServiceCommunicator;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.AmountType;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirUserFeeManager {

	@Autowired
	AirUserFeeServiceCommunicator userFeeService;

	@Autowired
	UserServiceCommunicator userComm;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	/**
	 * @param tripInfo
	 * 
	 * @param user - For That User - User Fee to Applied {Air Product User Fee to be applied} Applicable on First
	 *        Segment on each Trip if Fixed & percentage applicable on all segments
	 */
	protected void processUserFee(User user, TripInfo tripInfo, UserFeeType feeType) {
		if (user == null) {
			return;
		}

		List<UserFee> userFeeRequest = userComm.getUserFee(user.getUserId(), Product.AIR, feeType);
		if (CollectionUtils.isEmpty(userFeeRequest) && StringUtils.isNotBlank(user.getParentUserId())) {
			userFeeRequest = userComm.getUserFee(user.getParentUserId(), Product.AIR, feeType);
		}
		if (CollectionUtils.isNotEmpty(userFeeRequest)) {
			Map<String, List<UserFee>> userFeeMap =
					userFeeRequest.stream().collect(Collectors.groupingBy(userRequest -> getKey(userRequest)));
			Map<PaxType, UserFee> paxWiseUserFeeMap = getUserFee(tripInfo, userFeeMap);
			if (MapUtils.isNotEmpty(paxWiseUserFeeMap)) {
				tripInfo.getSegmentInfos().forEach(segmentInfo -> {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
							priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
								if (paxWiseUserFeeMap.get(paxType) != null)
									setUserFeeOnFareDetail(user, paxWiseUserFeeMap.get(paxType).getUserFee(),
											segmentInfo, fareDetail, feeType);
							});
						}
					});
				});
			}
		}
	}

	private String getKey(UserFee userRequest) {
		UserAirFeeRuleCriteria ruleCriteria = ((UserAirFeeRuleCriteria) userRequest.getInclusionCriteria());
		if (ruleCriteria != null) {
			String airType = ruleCriteria.getIsDomestic() != null
					? AirUtils.getAirTypeCode(BooleanUtils.isTrue(ruleCriteria.getIsDomestic()))
					: AirType.ALL.getCode();
			return StringUtils.join(airType);
		}
		return StringUtils.EMPTY;
	}

	/**
	 * @implSpec - Applicable on First Segment on each Trip if Fixed & percentage applicable on all segments
	 */
	protected void setUserFeeOnFareDetail(User user, UserFeeAmount feeAmount, SegmentInfo segmentInfo,
			FareDetail fareDetail, UserFeeType feeType) {

		AmountType amountType = AmountType.valueOf((feeAmount.getAmountType()));
		if (amountType.equals(AmountType.FIXED) && segmentInfo.getSegmentNum().intValue() == 0) {
			// Fixed User Fee
			userFeeService.setUserFeeAndUpdateTotalFare(user, null, fareDetail, feeAmount.getValue(), feeType);
		} else if (amountType.equals(AmountType.PERCENTAGE)) {
			// Percentage User Fee - Calculate Percentage on Total fare
			double userFee =
					fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0) * (feeAmount.getValue() / 100);
			/**
			 * This is to apply max value ,in case when calculated percentage value is greater than maxValue
			 */
			/*
			 * if (userFee > feeAmount.getMaxValue()) { userFee = feeAmount.getMaxValue(); }
			 */
			userFeeService.setUserFeeAndUpdateTotalFare(user, null, fareDetail, userFee, feeType);
		}
	}

	// find all possibilities for key from trip and try to fetch the value from map.
	public Map<PaxType, UserFee> getUserFee(TripInfo tripInfo, Map<String, List<UserFee>> userFeeMap) {
		Map<PaxType, UserFee> paxWiseUserFeeMap = new HashMap<>();
		if (MapUtils.isNotEmpty(userFeeMap)) {
			List<String> tripKeys = new ArrayList<>();
			String airline = tripInfo.getAirlineCode(false, 0);
			tripKeys.add(AirUtils.getAirType(tripInfo).getCode());
			tripKeys.add(AirType.ALL.getName());
			Set<PaxType> paxTypes = tripInfo.getPaxInfo().keySet();

			List<UserFee> feeRequests = new ArrayList<>();
			for (PaxType paxType : paxTypes) {
				for (String key : tripKeys) {
					feeRequests = (List<UserFee>) MapUtils.getObject(userFeeMap, key, null);
					List<UserFee> tempList = new ArrayList<>();
					if (CollectionUtils.isNotEmpty(feeRequests)) {
						for (UserFee userFee : feeRequests) {
							UserAirFeeRuleCriteria ruleCriteria =
									(UserAirFeeRuleCriteria) userFee.getInclusionCriteria();
							if (ruleCriteria != null) {
								Boolean matchesAirline = null;
								Boolean matchesPaxType = null;
								if (CollectionUtils.isNotEmpty(ruleCriteria.getAirLineList())) {
									matchesAirline = ruleCriteria.getAirLineList().contains(airline);
								}
								if (CollectionUtils.isNotEmpty(ruleCriteria.getPaxTypes())) {
									matchesPaxType = ruleCriteria.getPaxTypes().contains(paxType.getCode());
								}
								if (Objects.isNull(matchesPaxType) && Objects.isNull(matchesAirline)) {
									tempList.add(userFee);
								} else if (!BooleanUtils.isFalse(matchesAirline)
										&& !BooleanUtils.isFalse(matchesPaxType)) {
									// Select the first applicable userfee
									paxWiseUserFeeMap.put(paxType, userFee);
									break;
								}
							}
						}
					}
					if (CollectionUtils.isNotEmpty(tempList) && paxWiseUserFeeMap.get(paxType) == null)
						// If no userfee matches the exact criteria, select the default one with highest processedon.
						paxWiseUserFeeMap.put(paxType, tempList.get(0));
				}
			}
		}
		return paxWiseUserFeeMap;
	}

	// Setting User Fee in all priceInfos to {@code userFee} on {@code fareComponent}.
	public void setUserFeeOnAllFareDetails(User bookingUser, TripInfo tripInfo, UserFeeAmount userFee,
			UserFeeType feeType) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())
					&& segmentInfo.getPriceInfoList().get(0).getFareDetail(PaxType.ADULT) != null) {
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
						setUserFeeOnFareDetail(bookingUser, userFee, segmentInfo, fareDetail, feeType);

					});
				});
			} else if (segmentInfo.getBookingRelatedInfo() != null) {
				segmentInfo.getBookingRelatedInfo().getTravellerInfo().forEach(traveller -> {
					setUserFeeOnFareDetail(bookingUser, userFee, segmentInfo, traveller.getFareDetail(), feeType);
				});
			}
		});
	}

	/**
	 * Updates each trip's User Fee (in trip's first segment).<br>
	 * User Fee per trip is calculated which is later used to calculate User Fee per pax (per trip) while updating each
	 * trip's User Fee.
	 * 
	 * @param userFee Fixed User Fee
	 * @param tripInfos
	 */
	public void updateUserFee(User bookingUser, Double userFee, UserFeeType feeType, List<TripInfo> tripInfos) {
		if (userFee == null || CollectionUtils.isEmpty(tripInfos)) {
			throw new IllegalArgumentException("Either userFee or tripInfos is null/empty");

		}

		/**
		 * Markup gets updated in trips not having pricing also.
		 */
		int tripsCount = tripInfos.size();// BaseUtils.countTripsWithPricing(tripInfos);

		/**
		 * User Fee has to be updated per pax per trip.<br>
		 * This line calculates User Fee per trip.
		 */
		Double userFeePerTrip = userFee / tripsCount;
		tripInfos.forEach(tripInfo -> {
			// it will calculate User Fee per Adult
			updateUserFee(bookingUser, userFeePerTrip, feeType, tripInfo);

			User user = SystemContextHolder.getContextData().getUser();
			GeneralBasicFact fact = GeneralBasicFact.builder().build();
			fact.generateFact(user.getRole());
			FareBreakUpConfigOutput configOutput = gsCommunicator.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);

			AirUtils.setTripPriceInfoFromSegmentPriceInfo(tripInfo, user, configOutput);
		});
	}

	/**
	 * Updates User Fee in trip's first segment.<br>
	 * User Fee per pax is calculated.
	 * 
	 * @param userFee Fixed User Fee
	 * @param tripInfo
	 */
	public void updateUserFee(User bookingUser, Double userFee, UserFeeType feeType, TripInfo tripInfo) {
		if (userFee == null || tripInfo == null) {
			throw new IllegalArgumentException("Either userFee or tripInfos is null/empty");
		}

		// AmountType.PERCENTAGE updates user fee in fareDetails to zero (0.0) for all the segments.
		setUserFeeOnAllFareDetails(bookingUser, tripInfo, new UserFeeAmount(AmountType.PERCENTAGE.name(), 0.0, 0.0),
				feeType);

		/**
		 * User Fee has to be updated per Adult per trip.<br>
		 * This line calculates User Fee per Adult (per trip).
		 */
		Double userFeePerAdult = userFee / BaseUtils.getPaxInfo(tripInfo).get(PaxType.ADULT);

		SegmentInfo firstSegment = tripInfo.getSegmentInfos().get(0);
		if (firstSegment.getBookingRelatedInfo() != null) {
			firstSegment.getBookingRelatedInfo().getTravellerInfo().forEach(traveller -> {
				if (traveller.getPaxType().equals(PaxType.ADULT)) {
					setUserFeeOnFareDetail(bookingUser,
							new UserFeeAmount(AmountType.FIXED.name(), userFeePerAdult, userFeePerAdult), firstSegment,
							traveller.getFareDetail(), feeType);
				}
			});
		} else if (CollectionUtils.isNotEmpty(firstSegment.getPriceInfoList())
				&& firstSegment.getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			firstSegment.getPriceInfoList().forEach(priceInfo -> {
				FareDetail adultPaxFareDetail = priceInfo.getFareDetails().get(PaxType.ADULT);
				setUserFeeOnFareDetail(bookingUser,
						new UserFeeAmount(AmountType.FIXED.name(), userFeePerAdult, userFeePerAdult), firstSegment,
						adultPaxFareDetail, feeType);
			});
		}

		// User Fee per trip is stored in SegmentMiscInfo.
		tripInfo.getSegmentInfos().get(0).getMiscInfo().setUpdatedUserFee(userFee);
		tripInfo.getSegmentInfos().get(0).getMiscInfo().setUpdatedUserfeeType(feeType.getCode());
	}

	public void resetMarkUp(TripInfo tripInfo, User user) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			// Its Applicable for All Pax - Mostly from Pre Booking
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
					if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
						fareDetail.getFareComponents().remove(FareComponent.MU);
					}
				});
			});
		});
	}
}
