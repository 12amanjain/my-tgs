package com.tgs.services.fms.manager;

import java.util.*;
import java.util.Map.Entry;
import java.util.stream.Collectors;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.communicator.VoucherServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.ruleengine.GeneralRuleField;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.vms.restmodel.FlightVoucherValidateRequest;
import com.tgs.services.vms.restmodel.FlightVoucherValidateResponse;
import com.tgs.services.voucher.datamodel.AirVoucherRuleCriteriaOutput;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.datamodel.VoucherConfigurationRule;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirVoucherManager {

	@Autowired
	AirOrderItemCommunicator orderItemCommunicator;

	@Autowired
	VoucherServiceCommunicator voucherService;

	@Autowired
	@Lazy
	private AirBookingEngine bookingEngine;

	private static Map<String, ? extends IRuleField> gnResolverMap = EnumUtils.getEnumMap(GeneralRuleField.class);
	private static Map<String, ? extends IRuleField> airResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);

	private static Map<String, IRuleField> resolverMap = new HashMap<>();;

	static {
		resolverMap.putAll(gnResolverMap);
		resolverMap.putAll(airResolverMap);
	}

	private List<TripInfo> fetchTripInfosByBookingId(String bookingId) {
		List<TripInfo> tripInfos = new ArrayList<>();
		try {
			AirReviewResponse reviewResponse = bookingEngine.getAirReviewResponse(bookingId, null);
			if (reviewResponse != null && CollectionUtils.isNotEmpty(reviewResponse.getTripInfos())) {
				tripInfos = reviewResponse.getTripInfos();
			}
		} catch (Exception e) {
			log.info("AirReviewResponse not available for {} ", bookingId);
		}
		if (CollectionUtils.isEmpty(tripInfos)) {
			tripInfos = orderItemCommunicator.findTripByBookingId(bookingId);
		}
		if (CollectionUtils.isEmpty(tripInfos)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
		return tripInfos;
	}

	public FlightVoucherValidateResponse applyDiscountOnTrips(AirVoucherRuleCriteriaOutput voucherCriteria,
			List<TripInfo> tripInfos, boolean isModifyFD) {
		FlightVoucherValidateResponse voucherResponse = new FlightVoucherValidateResponse();
		AtomicDouble totalDiscount = new AtomicDouble();
		for (TripInfo trip : tripInfos) {
			for (SegmentInfo segment : trip.getSegmentInfos()) {
				Map<PaxType, FareDetail> fareDetails = segment.getPriceInfo(0).getFareDetails();
				for (Entry<PaxType, FareDetail> entry : fareDetails.entrySet()) {
					Integer paxTypeCount = trip.getPaxInfo().getOrDefault(entry.getKey(), 0);
					if (isVoucherRuleApplicable(voucherCriteria, segment, entry.getKey())) {
						double voucherDiscount = BaseUtils.evaluateExpression(voucherCriteria.getExpression(),
								entry.getValue(), null, true);
						Double discount = voucherCriteria.getDiscountAmount(voucherDiscount);
						calcOrApplyVoucherDiscount(entry.getValue(), isModifyFD, discount);
						totalDiscount.getAndAdd(discount * paxTypeCount);
					}
				}
			}
		}
		voucherResponse.setDiscountedAmount(totalDiscount.get());
		return voucherResponse;
	}

	private void calcOrApplyVoucherDiscount(FareDetail fareDetail, boolean isModify, Double totalDiscount) {
		if (isModify) {
			fareDetail.getFareComponents().put(FareComponent.VD, totalDiscount);
		}
	}

	public FlightVoucherValidateResponse calculateVoucherDiscount(FlightVoucherValidateRequest request,
			boolean isApplyDiscountOnTrips) {
		ContextData contextData = SystemContextHolder.getContextData();
		FlightVoucherValidateResponse response = new FlightVoucherValidateResponse();
		if (StringUtils.isNotBlank(request.getBookingId()) && CollectionUtils.isNotEmpty(request.getConfigurations())) {
			List<TripInfo> tripInfos = CollectionUtils.isNotEmpty(request.getTripInfos()) ? request.getTripInfos()
					: fetchTripInfosByBookingId(request.getBookingId());
			VoucherConfiguration configuration = request.getConfigurations().get(0);
			// configuration.getVoucherRules().sort(Comparator.comparing(VoucherConfigurationRule::getPriority));
			for (TripInfo tripInfo : tripInfos) {
				AirVoucherRuleCriteriaOutput voucherCriteria = null;
				if (!tripInfo.isVoucherApplied()) {
					VoucherConfigurationRule voucherRule =
							getVoucherRuleOnTrip(request, contextData, tripInfo, configuration);
					if (voucherRule != null) {
						voucherCriteria =
								new GsonMapper<>(voucherRule.getOutput(), AirVoucherRuleCriteriaOutput.class).convert();
						FlightVoucherValidateResponse validateResponse =
								applyDiscountOnTrips(voucherCriteria, Arrays.asList(tripInfo), isApplyDiscountOnTrips);
						response.setDiscountedAmount(
								response.getDiscountedAmount() + validateResponse.getDiscountedAmount());
						if (isApplyDiscountOnTrips) {
							voucherRule.setQuantityConsumed(voucherRule.getQuantityConsumed() + 1);
						}
					}
				}
				if (voucherCriteria != null && BooleanUtils.isTrue(voucherCriteria.getIsApplicableOnBooking())) {
					// applicable on booking means only on satisfied journey its applicable and wont applu on next
					break;
				}
			}
			if (isApplyDiscountOnTrips) {
				voucherService.saveVoucherConfiguration(configuration);
			}
			response.setTripInfos(tripInfos);
		}
		return response;
	}

	public static boolean isVoucherRuleApplicable(AirVoucherRuleCriteriaOutput condition, SegmentInfo segmentInfo,
			PaxType paxType) {
		boolean isApplicable = true;
		if (condition == null && !PaxType.INFANT.equals(paxType)) {
			isApplicable = true;
		}
		if (condition != null) {
			if (BooleanUtils.isTrue(condition.getIsApplicableOnBooking())) {
				isApplicable = segmentInfo.getSegmentNum().equals(0)
						&& BooleanUtils.isNotTrue(segmentInfo.getIsReturnSegment());
			} else if (Objects.nonNull(condition.getIsApplicableOnAllSegment())) {
				isApplicable = segmentInfo.getSegmentNum().equals(0) ? true : condition.getIsApplicableOnAllSegment();
			}
		}
		if (paxType.equals(PaxType.INFANT)
				&& (condition == null || Objects.nonNull(condition.getIsApplicableOnInfant()))) {
			isApplicable = condition == null ? false : BooleanUtils.isTrue(condition.getIsApplicableOnInfant());
			if (isApplicable) {
				isApplicable = BooleanUtils.isTrue(condition.getIsApplicableOnAllSegment())
						|| segmentInfo.getSegmentNum().equals(0);
			}
		}
		return isApplicable;
	}

	private VoucherConfigurationRule getVoucherRuleOnTrip(FlightVoucherValidateRequest request, ContextData contextData,
			TripInfo tripInfo, VoucherConfiguration configuration) {
		List<VoucherConfigurationRule> configurationRules = configuration.getVoucherRules().stream()
				.filter(rule -> (Product.AIR.equals(rule.getProduct()) || Product.NA.equals(rule.getProduct()))
						&& rule.getRemainingQty() > 0)
				.collect(Collectors.toList());
		User user = contextData.getUser();
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(tripInfo);
		flightFact.setUserId(UserUtils.getUserId(contextData.getUser()));
		flightFact.setRole(contextData.getUser().getRole());
		flightFact.setPaymentMediums(request.getMediums());
		flightFact.setChannelType(contextData.getHttpHeaders().getChannelType());
		flightFact.setRole(user.getRole());
		flightFact.setPartnerId(UserUtils.getPartnerUserId(user));
		flightFact.setPaxCount(tripInfo.getPaxCountWithoutInfant());
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(configurationRules, flightFact, resolverMap);
		List<VoucherConfigurationRule> rules = (List<VoucherConfigurationRule>) ruleEngine.fireAllRules();
		if (CollectionUtils.isNotEmpty(rules)) {
			return rules.get(0);
		}
		return null;
	}
}
