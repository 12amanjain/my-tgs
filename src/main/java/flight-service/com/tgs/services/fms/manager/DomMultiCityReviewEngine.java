package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.List;
import lombok.Setter;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;

@Setter
@Service
public class DomMultiCityReviewEngine extends AbstractAirReviewEngine {

	@Override
	public List<TripInfo> reviewTrips(List<TripInfo> tripInfos, List<AirSearchQuery> searchQueries,
			ContextData contextData, String booking) {
		List<TripInfo> newTripInfos = new ArrayList<>();
		for (int i = 0; i < tripInfos.size(); i++) {

			/**
			 * This code be improvise using separate thread for each review but need to handle sample supplier case due
			 * to one pnr for one supplier
			 */
			newTripInfos.add(this.reviewTrip(tripInfos.get(i), searchQueries.get(i), false, contextData, booking));
		}
		return newTripInfos;
	}
}
