package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Setter
@Service
@Slf4j
public class DomReturnReviewEngine extends AbstractAirReviewEngine {

	@Override
	public List<TripInfo> reviewTrips(List<TripInfo> tripInfos, List<AirSearchQuery> searchQueries,
			ContextData contextData, String bookingId) {
		List<TripInfo> newTripInfos = new ArrayList<>();
		List<Future<TripInfo>> futureTaskList = new ArrayList<>();

		PriceInfo onwardPriceInfo = tripInfos.get(0).getSegmentInfos().get(0).getPriceInfo(0);
		PriceInfo returnPriceInfo = tripInfos.get(1).getSegmentInfos().get(0).getPriceInfo(0);

		SupplierBasicInfo onwardBasicInfo = onwardPriceInfo.getSupplierBasicInfo();
		SupplierBasicInfo returnBasicInfo = returnPriceInfo.getSupplierBasicInfo();

		AirlineInfo onwardAirline = AirlineHelper.getAirlineInfo(tripInfos.get(0).getAirlineCode());
		AirlineInfo returnAirline = AirlineHelper.getAirlineInfo(tripInfos.get(1).getAirlineCode());

		CabinClass onwardCabinClass = tripInfos.get(0).getCabinClass();
		CabinClass returnCabinClass = tripInfos.get(1).getCabinClass();

		String onwardFareIdentifier = onwardPriceInfo.getFareType();
		String returnFareIdentifier = returnPriceInfo.getFareType();


		/**
		 * @implNote 1. Incaseof same supplier <br>
		 *           1A. Same airline & supplier support Domestic Return in Single Sell <br>
		 *           1B. Incaseof same airline & supplier not support Domestic Return in Single Sell <br>
		 *           1C. Incaseof different airline & supplier not support Domestic Return in Single Sell<br>
		 *           2. Incaseof different supplier
		 */
		if (AirUtils.isSameSupplier(onwardBasicInfo, returnBasicInfo)) {
			if (AirUtils.isSameAirline(onwardAirline, returnAirline)
					&& AirUtils.isSameCabinClass(onwardCabinClass, returnCabinClass)
					|| isSingleSellSource(onwardBasicInfo, returnBasicInfo)) {
				/**
				 * @implSpec <br>
				 *           :Ex 1: InCase of same supplier & same Airline G8 from GoAir Onward & G8 from GoAir Return
				 *           :Ex 2: InCase of same supplier & same Airline 9W from Sabre Onward & 9W from Sabre Return
				 *           (on same session supplier support single sell)
				 */
				if (isSingleSellForBothTrip(onwardBasicInfo, returnBasicInfo, onwardAirline, returnAirline,
						onwardFareIdentifier, returnFareIdentifier, contextData.getUser())
						|| isSingleSellSource(onwardBasicInfo, returnBasicInfo)) {
					TripInfo reviewTrip = this.reviewTrip(AirUtils.mergeTripInfo(tripInfos),
							AirUtils.combineSearchQuery(searchQueries), true, contextData, bookingId);
					newTripInfos.addAll(AirUtils.splitTripInfo(reviewTrip, false));
				} else if (!isSpecialReturn(onwardFareIdentifier, returnFareIdentifier)
						&& isSellSourceTripWiseIfSameAirline(onwardBasicInfo)) {
					/**
					 * @implSpec : InCase of same supplier & same Airline 9W from Sabre then Two Sell for Two PNR
					 */
					futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(() -> this
							.reviewTrip(tripInfos.get(0), searchQueries.get(0), false, contextData, bookingId)));
					futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(() -> this
							.reviewTrip(tripInfos.get(1), searchQueries.get(0), false, contextData, bookingId)));
				} else {
					/**
					 * @implSpec : If airline doesn't support single sell or any other use case. Example : TBO
					 */
					newTripInfos.add(
							this.reviewTrip(tripInfos.get(0), searchQueries.get(0), false, contextData, bookingId));
					newTripInfos.add(
							this.reviewTrip(tripInfos.get(1), searchQueries.get(0), false, contextData, bookingId));
				}
			} else {
				/**
				 * @implSpec : InCase of same supplier & different Airline 9W from Sabre & AI from AirIndia
				 */
				futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(
						() -> this.reviewTrip(tripInfos.get(0), searchQueries.get(0), false, contextData, bookingId)));
				futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(
						() -> this.reviewTrip(tripInfos.get(1), searchQueries.get(0), false, contextData, bookingId)));
			}
		} else {
			/**
			 * In case of different supplier , searchQuery should be updated in such a manner that it will be considered
			 * as 2 one-ways
			 */
			/**
			 * Removing second routeInfo from first searchQuery
			 */
			searchQueries.get(0).getRouteInfos().remove(1);
			AirUtils.populateMissingParametersInAirSearchQuery(searchQueries.get(0));
			/**
			 * Removing second routeInfo from second searchQuery and updating in firstSearchQuery
			 */
			RouteInfo routeInfo = searchQueries.get(1).getRouteInfos().get(1);
			searchQueries.get(1).setRouteInfos(new ArrayList<>()).getRouteInfos().add(routeInfo);
			AirUtils.populateMissingParametersInAirSearchQuery(searchQueries.get(1));

			futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(
					() -> this.reviewTrip(tripInfos.get(0), searchQueries.get(0), false, contextData, bookingId)));
			futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool().submit(
					() -> this.reviewTrip(tripInfos.get(1), searchQueries.get(1), false, contextData, bookingId)));
		}
		for (int i = 0; i < futureTaskList.size(); i++) {
			try {
				newTripInfos.add(futureTaskList.get(i).get(120, TimeUnit.SECONDS));
			} catch (ExecutionException | InterruptedException | TimeoutException e) {
				log.error("Unable to reprice trip for tripInfo {}", tripInfos.get(i), e);
			}
		}
		return newTripInfos;
	}

	private boolean isSingleSellSource(SupplierBasicInfo onwardBasicInfo, SupplierBasicInfo returnBasicInfo) {
		List<Integer> singlSellSources = Arrays.asList(AirSourceType.TRIPJACK.getSourceId());
		if (singlSellSources.contains(onwardBasicInfo.getSourceId())
				&& singlSellSources.contains(returnBasicInfo.getSourceId())) {
			return true;
		}
		return false;
	}

	private boolean isSingleSellForBothTrip(SupplierBasicInfo onwardInfo, SupplierBasicInfo returnInfo,
			AirlineInfo onwardAirline, AirlineInfo returnAirline, String onwardFareType, String returnFareType,
			User bookingUser) {
		boolean isSingleSell = false;
		FlightBasicFact fact = FlightBasicFact.builder().build().generateFact(onwardInfo);
		BaseUtils.createFactOnUser(fact, bookingUser);
		AirSourceConfigurationOutput sourceInfo =
				AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.SOURCECONFIG);

		if (sourceInfo != null && BooleanUtils.isTrue(sourceInfo.getIsSingleSell())) {
			return true;
		}
		/**
		 * @implSpec <br>
		 *           : As Sabre for Domestic Return Only in case of SPECIAL RETURN Will be Single Sell else will be not
		 *           supporting Single Sell
		 */
		if (AirUtils.isSameAirline(onwardAirline, returnAirline) && isSpecialReturn(onwardFareType, returnFareType)
				&& (onwardInfo.getSourceId().intValue() == AirSourceType.SABRE.getSourceId()
						|| onwardInfo.getSourceId().intValue() == AirSourceType.AMADEUS.getSourceId()
						|| onwardInfo.getSourceId().intValue() == AirSourceType.TRAVELPORT.getSourceId())) {
			isSingleSell = true;
		} else if (onwardInfo.getSourceId() == AirSourceType.TBO.getSourceId()
				&& returnInfo.getSourceId() == AirSourceType.TBO.getSourceId()) {
			isSingleSell = false;
		} else if (isSameFareType(onwardFareType, returnFareType) && !isSellSourceTripWiseIfSameAirline(onwardInfo)) {
			isSingleSell = true;
		}
		return isSingleSell;
	}

	private boolean isSpecialReturn(String onwardFareType, String returnFareType) {
		return isSameFareType(onwardFareType, returnFareType)
				&& FareType.SPECIAL_RETURN.getName().equals(onwardFareType);
	}

	private boolean isSameFareType(String onwardFareType, String returnFareType) {
		return onwardFareType.equals(returnFareType);
	}

	private boolean isSellSourceTripWiseIfSameAirline(SupplierBasicInfo onwardBasicInfo) {
		boolean isSellSourceTripWise = false;
		if (AirSourceType.SABRE.getSourceId() == onwardBasicInfo.getSourceId().intValue()
				|| AirSourceType.AMADEUS.getSourceId() == onwardBasicInfo.getSourceId().intValue()
				|| AirSourceType.DEALINVENTORY.getSourceId() == onwardBasicInfo.getSourceId().intValue()
				|| AirSourceType.TRAVELPORT.getSourceId() == onwardBasicInfo.getSourceId().intValue()) {
			isSellSourceTripWise = true;
		}
		return isSellSourceTripWise;
	}

}
