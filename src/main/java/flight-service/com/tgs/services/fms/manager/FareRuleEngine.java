package com.tgs.services.fms.manager;

import java.util.List;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.ums.datamodel.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.FareRuleTripResponse;
import com.tgs.services.fms.utils.AirUtils;

@Service
public class FareRuleEngine {

	@Autowired
	private FareRuleManager ruleManager;


	public FareRuleTripResponse getFareRule(List<TripInfo> tripInfos, String bookingId, AirFlowType flowType,
			boolean isDetailed, User bookingUser) {
		SearchType searchType = AirUtils.getSearchTypeFromTrips(tripInfos);
		boolean isDomesticBooking = AirUtils.isDomesticTrips(tripInfos);
		FareRuleTripResponse fareRules = new FareRuleTripResponse();

		// For return and multicity cases.
		if (searchType.equals(SearchType.RETURN) || searchType.equals(SearchType.MULTICITY)) {
			String onwardFareType = tripInfos.get(1).getSegmentInfos().get(0).getPriceInfo(0).getFareType();
			String returnFareType = tripInfos.get(1).getSegmentInfos().get(0).getPriceInfo(0).getFareType();
			if ((onwardFareType.equals(FareType.SPECIAL_RETURN.getName())
					&& returnFareType.equals(FareType.SPECIAL_RETURN.getName())) || !isDomesticBooking) {
				/**
				 * Domestic special return and international multicity & international return trips should be combined
				 * for the fare rule as they are from single supplier. Reason not to use multiple threads is to keep the
				 * Segment order while forming fare rule.
				 */
				TripInfo newTripInfo = new TripInfo();
				for (TripInfo trip : tripInfos) {
					newTripInfo.getSegmentInfos().addAll(trip.getSegmentInfos());
				}
				FareRuleTripResponse response =
						ruleManager.getFareRule(newTripInfo, bookingId, flowType, isDetailed, bookingUser);
				fareRules.getFareRule().putAll(response.getFareRule());

			} else {
				/**
				 * Domestic return and domestic multicity should be called separately for the fare rule as trips would
				 * be from different supplier. Reason not to use multiple threads is to keep the Segment order while
				 * forming fare rule.
				 */
				for (TripInfo trip : tripInfos) {
					FareRuleTripResponse response =
							ruleManager.getFareRule(trip, bookingId, flowType, isDetailed, bookingUser);
					fareRules.getFareRule().putAll(response.getFareRule());
				}
			}
		} else {
			// For Intl oneway and dom oneway.
			FareRuleTripResponse response =
					ruleManager.getFareRule(tripInfos.get(0), bookingId, flowType, isDetailed, bookingUser);
			fareRules.getFareRule().putAll(response.getFareRule());
		}

		return fareRules;
	}


}
