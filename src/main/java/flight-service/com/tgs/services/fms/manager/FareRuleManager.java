package com.tgs.services.fms.manager;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientFeeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.FareRuleHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.restmodel.FareRuleTripResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FareRuleManager {

	@Autowired
	private FareRuleHelper fareRuleHelper;

	@Autowired
	private AirPartnerUserFeeManager feeManager;


	public FareRuleTripResponse getFareRule(TripInfo trip, String bookingId, AirFlowType flowType, boolean isDetailed,
			User bookingUser) {
		FareRuleTripResponse fareRuleResponse = new FareRuleTripResponse();
		try {
			User user = SystemContextHolder.getContextData().getUser();
			List<TripInfo> tripInfos = trip.splitTripInfo(true, false);
			tripInfos.forEach(tripInfo -> {
				FlightBasicFact flightFact = FlightBasicFact.builder().build();
				flightFact.generateFact(tripInfo);
				flightFact.setAirType(AirUtils.getAirType(tripInfo));
				BaseUtils.createFactOnUser(flightFact, bookingUser);
				PriceInfo fareRulePriceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
				flightFact.generateFact(fareRulePriceInfo).generateFact(fareRulePriceInfo.getFareDetail(PaxType.ADULT));

				boolean fetchFromApi = false;
				Map<FareRuleTimeWindow, FareRulePolicyContent> policyContent = new HashMap<>();

				if (!isDetailed) {
					FareRuleInfo ruleInfo = FareRuleHelper.getFareRule(flightFact, true);

					/**
					 * Additional check of checking cancellation cancellation is due to cases where we want to override
					 * baggage, meal parameters but cancellation policy should come from API. In such scenario fare rule
					 * policy will not be picked from our database but baggage information will be picked
					 */
					policyContent =
							ruleInfo != null
									? ruleInfo.getFareRuleInformation().getFareRuleInfo()
											.getOrDefault(FareRulePolicyType.CANCELLATION, null)
									: null;
					if (MapUtils.isNotEmpty(policyContent)
							&& (policyContent.get(FareRuleTimeWindow.DEFAULT).getAmount() != null || StringUtils
									.isNotBlank(policyContent.get(FareRuleTimeWindow.DEFAULT).getPolicyInfo()))) {
						fillClientFee(Arrays.asList(ruleInfo.getFareRuleInformation()), flightFact);
						applyPartnerClientFee(ruleInfo.getFareRuleInformation(), tripInfo, user, flowType, bookingId);
						fareRuleResponse.getFareRule().put(getRuleKey(tripInfo), ruleInfo.getFareRuleInformation());
					} else {
						fetchFromApi = isFetchFromAPI(tripInfo, bookingUser);
					}
				}
				if (isDetailed || fetchFromApi) {
					PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
					AirSourceType airSourceType =
							AirSourceType.getAirSourceType(priceInfo.getSupplierBasicInfo().getSourceId());
					SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
							priceInfo.getSupplierBasicInfo().getSourceId(),
							priceInfo.getSupplierBasicInfo().getRuleId());
					AbstractAirInfoFactory factory = airSourceType.getFactoryInstance(null, supplierConf);
					factory.setFlowType(flowType);
					factory.setContextData(SystemContextHolder.getContextData());
					TripFareRule tripFareRule = factory.fareRule(tripInfo, bookingId, isDetailed);
					fillClientFee(tripFareRule.getFareRule().values(), flightFact);
					if (tripFareRule != null && MapUtils.isNotEmpty(tripFareRule.getFareRule())) {
						for (String key : tripFareRule.getFareRule().keySet()) {
							applyPartnerClientFee(tripFareRule.getFareRule().get(key), tripInfo, user, flowType,
									bookingId);
						}
					}
					fareRuleResponse.getFareRule().putAll(tripFareRule.getFareRule());

				}
			});
		} catch (Exception e) {
			log.error("Error Occured while fetching fare rule for trip {} bookingid {} flow {} cause {}",
					trip.toString(), bookingId, flowType.name(), e.getMessage(), e);
		}
		return fareRuleResponse;
	}

	private void fillClientFee(Collection<FareRuleInformation> fareRuleInfos, FlightBasicFact flightBasicFact) {
		for (FareRuleInformation fareRuleInfoOutput : fareRuleInfos) {
			if (fareRuleInfoOutput != null) {
				Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo =
						fareRuleInfoOutput.getFareRuleInfo();
				for (FareRulePolicyType fareRulePolicyType : fareRuleInfo.keySet()) {
					Map<FareRuleTimeWindow, FareRulePolicyContent> fareRuleInfoContents =
							fareRuleInfo.get(fareRulePolicyType);
					if (fareRuleInfoContents == null) {
						continue;
					}
					switch (fareRulePolicyType) {
						case CANCELLATION:
							fillClientCancellationFeeInFareRulePolicyContents(fareRuleInfoContents.values(),
									flightBasicFact);
							break;
						case DATECHANGE:
							fillClientRescheduleFeeInFareRulePolicyContents(fareRuleInfoContents.values(),
									flightBasicFact);
							break;
						default:
							break;
					}
				}
			}
		}
	}

	private void fillClientCancellationFeeInFareRulePolicyContents(
			Collection<FareRulePolicyContent> fareRuleInfoContents, FlightBasicFact flightBasicFact) {
		for (FareRulePolicyContent content : fareRuleInfoContents) {
			BaseUtils.updateFareComponent(content.getFareComponents(), FareComponent.ACF, content.getAmount(), null);
			BaseUtils.updateFareComponent(content.getFareComponents(), FareComponent.CCF, content.getAdditionalFee(),
					null);
			if (content.getAdditionalFee() == null) {
				Double additionalFee = getClientCancellationFeeFromConfigurator(flightBasicFact);
				content.setAdditionalFee(additionalFee);
				BaseUtils.updateFareComponent(content.getFareComponents(), FareComponent.CCF, additionalFee, null);
			}
		}
	}

	private void fillClientRescheduleFeeInFareRulePolicyContents(Collection<FareRulePolicyContent> fareRuleInfoContents,
			FlightBasicFact flightBasicFact) {
		for (FareRulePolicyContent content : fareRuleInfoContents) {
			BaseUtils.updateFareComponent(content.getFareComponents(), FareComponent.ARF, content.getAmount(), null);
			BaseUtils.updateFareComponent(content.getFareComponents(), FareComponent.CRF, content.getAdditionalFee(),
					null);
			if (content.getAdditionalFee() == null) {
				Double additionalFee = getClientRescheduleFeeFromConfigurator(flightBasicFact);
				content.setAdditionalFee(additionalFee);
				BaseUtils.updateFareComponent(content.getFareComponents(), FareComponent.CRF, additionalFee, null);
			}
		}
	}

	public static boolean isFetchFromAPI(TripInfo tripInfo, User bookingUser) {
		AirSourceConfigurationOutput airSourceOutput = AirUtils.getAirSourceConfigOnTripInfo(tripInfo, bookingUser);

		if (airSourceOutput != null && Objects.nonNull(airSourceOutput.getIsFareRuleFetchFromApi())) {
			return Boolean.valueOf(airSourceOutput.getIsFareRuleFetchFromApi());
		}
		return false;
	}

	// must be accessible to AbstractAirInfoFactories also
	public static String getRuleKey(TripInfo trip) {
		return StringUtils.join(trip.getDepartureAirportCode().trim(), "-", trip.getArrivalAirportCode());
	}


	protected void setInclusions(TripInfo trip, FlightBasicFact flightFact, AirSearchQuery searchQuery,
			AtomicReference<AirSourceConfigurationOutput> airSourceOutput, User bookingUser) {

		trip.getSegmentInfos().forEach(segmentInfo -> {

			if (trip.isMultipleSupplier() || airSourceOutput == null || airSourceOutput.get() == null) {
				airSourceOutput.set(
						AirUtils.getAirSourceConfiguration(searchQuery, segmentInfo.getSupplierInfo(), bookingUser));
			}

			if (segmentInfo.getBookingRelatedInfo() != null) {
				// Post Booking
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
				FlightTravellerInfo adultTraveller =
						AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).get(0);
				flightFact.generateFact(adultTraveller.getFareDetail());
				flightFact.generateFact(priceInfo);
				Set<String> bookingClass = new HashSet<>();
				bookingClass.add(adultTraveller.getFareDetail().getClassOfBooking());
				flightFact.setBookingClasses(bookingClass);
				FareRuleInfo fareRule = FareRuleHelper.getFareRule(flightFact, false);
				if (fareRule != null) {
					FareRuleInformation ruleInformation = fareRule.getFareRuleInformation();
					travellerInfos.forEach(travellerInfo -> {
						BaggageInfo baggageInfo = getBaggageInclusions(ruleInformation, travellerInfo.getPaxType(),
								travellerInfo.getFareDetail(), airSourceOutput.get());
						travellerInfo.getFareDetail().setBaggageInfo(baggageInfo);
						if (airSourceOutput != null && airSourceOutput.get() != null
								&& BooleanUtils.isFalse(airSourceOutput.get().getIsRefundableTypeOverride())) {
							if (travellerInfo.getFareDetail().getRefundableType() == null
									&& ruleInformation.getRefundableType() != null) {
								travellerInfo.getFareDetail()
										.setRefundableType(ruleInformation.getRefundableType().getRefundableType());
							}
						} else if (ruleInformation.getRefundableType() != null) {
							travellerInfo.getFareDetail()
									.setRefundableType(ruleInformation.getRefundableType().getRefundableType());
						}
					});
				}
			} else {
				// Pre Booking
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
						Set<String> bookingClass = new HashSet<>();
						bookingClass.add(priceInfo.getBookingClass(PaxType.ADULT));
						flightFact.setBookingClasses(bookingClass);
						flightFact
								.setCabinClasses(new HashSet<>(Arrays.asList(priceInfo.getCabinClass(PaxType.ADULT))));
						FareRuleInfo fareRule = FareRuleHelper.getFareRule(
								FlightBasicFact.getNonNull(flightFact).generateFact(priceInfo, PaxType.ADULT), false);
						if (Objects.nonNull(fareRule) && fareRule.getFareRuleInformation() != null) {
							FareRuleInformation ruleInfo = fareRule.getFareRuleInformation();
							priceInfo.getMiscInfo().setFareRuleId(fareRule.getId());
							priceInfo.getFareDetails().forEach((paxType, fareDetails) -> {
								fareDetails.setBaggageInfo(
										getBaggageInclusions(ruleInfo, paxType, fareDetails, airSourceOutput.get()));

								if (airSourceOutput != null && airSourceOutput.get() != null
										&& BooleanUtils.isFalse(airSourceOutput.get().getIsRefundableTypeOverride())) {
									if (fareDetails.getRefundableType() == null
											&& ruleInfo.getRefundableType() != null) {
										fareDetails.setRefundableType(ruleInfo.getRefundableType().getRefundableType());
									}
								} else if (ruleInfo.getRefundableType() != null) {
									fareDetails.setRefundableType(ruleInfo.getRefundableType().getRefundableType());
								}
								if (airSourceOutput != null && airSourceOutput.get() != null
										&& BooleanUtils.isFalse(airSourceOutput.get().getIsMealOverride())) {
									if (fareDetails.getIsMealIncluded() == null
											&& ruleInfo.getIsMealIndicator() != null) {
										fareDetails
												.setIsMealIncluded(BooleanUtils.isTrue(ruleInfo.getIsMealIndicator()));
									}
								} else if (ruleInfo.getIsMealIndicator() != null) {
									fareDetails.setIsMealIncluded(BooleanUtils.isTrue(ruleInfo.getIsMealIndicator()));
								}
							});
						}
					}
				});
			}
		});
	}

	private BaggageInfo getBaggageInclusions(FareRuleInformation ruleInfo, PaxType paxType, FareDetail fareDetail,
			AirSourceConfigurationOutput airSourceOutput) {
		BaggageInfo baggageInfo = fareDetail.getBaggageInfo();

		if (airSourceOutput != null && BooleanUtils.isFalse(airSourceOutput.getIsBaggageOverride())) {

			// Dont overide the existing value if present
			if (baggageInfo.getCabinBaggage() == null && ruleInfo.getHandBaggage(paxType) != null) {
				baggageInfo.setCabinBaggage(ruleInfo.getHandBaggage(paxType));
			}

			if (baggageInfo.getAllowance() == null && ruleInfo.getCheckedInBagagge(paxType) != null) {
				baggageInfo.setAllowance(ruleInfo.getCheckedInBagagge(paxType));
			}

			if (BooleanUtils.isTrue(ruleInfo.getIsHandBaggageIndicator())) {
				fareDetail.setIsHandBaggage(true);
			}
		} else {
			// default behaviour {override}
			if (ruleInfo.getHandBaggage(paxType) != null) {
				baggageInfo.setCabinBaggage(ruleInfo.getHandBaggage(paxType));
			}

			if (ruleInfo.getCheckedInBagagge(paxType) != null) {
				baggageInfo.setAllowance(ruleInfo.getCheckedInBagagge(paxType));
			}

			if (BooleanUtils.isTrue(ruleInfo.getIsHandBaggageIndicator())) {
				fareDetail.setIsHandBaggage(true);
			}
		}
		return baggageInfo;
	}

	public FareRuleInfo getFareRuleById(long id) {
		return fareRuleHelper.getFareRuleInfo(id);
	}

	/**
	 * For given {@code TripInfo}, {@code FareComponent}s - ACF, ARF, CCF, RCF of all the {@code FareDetail}s
	 * ({@code PaxType}-wise) available in all {@code PriceInfo}s in the <i>first</i> {@code SegmentInfo} are updated.
	 * These components are fetched from applicable fare-rule and air-configurator ({@code AirClientFeeOutput}). <br>
	 * <u>Note:</u> Only one instance of {@code FareDetail} is affected. Users accessing this function (via
	 * {@code FMSCommunicator}) from other services are advised that if the tripInfo has been converted from other type,
	 * update same information in other instances of {@code FareDetail} explicitly.
	 *
	 * @param tripInfo
	 * @param bookingUser
	 */
	public void updateCancellationAndReschedulingFees(TripInfo tripInfo, User bookingUser) {
		SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
		segmentInfo.getPriceInfoList().forEach(priceInfo -> {
			for (FareDetail fareDetail : priceInfo.getFareDetails().values()) {
				FlightBasicFact flightBasicFact = FlightBasicFact.createFact();
				flightBasicFact.generateFact(tripInfo);
				flightBasicFact.setAirType(AirUtils.getAirType(tripInfo));
				flightBasicFact.generateFact(priceInfo).generateFact(fareDetail);
				BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
				FareRuleInfo fareRuleInfo = FareRuleHelper.getFareRule(flightBasicFact, false);
				FareRuleInformation fareRuleInformation =
						(fareRuleInfo == null ? null : fareRuleInfo.getFareRuleInformation());

				updateCancellationAndReschedulingFees(fareDetail, fareRuleInformation, flightBasicFact, bookingUser);
			}
		});
	}

	/**
	 * For given {@code SegmentInfo}, {@code FareComponent}s - ACF, ARF, CCF, RCF of all the {@code FareDetail}s
	 * ({@code PaxType}-wise) available in all {@code PriceInfo}s are updated. These components are fetched from
	 * applicable fare-rule and air-configurator ({@code AirClientFeeOutput}). <br>
	 * <u>Note:</u> Only one instance of {@code FareDetail} is affected. Users accessing this function (via
	 * {@code FMSCommunicator}) from other services are advised that if the segmentInfo has been converted from other
	 * type, update same information in other instances of {@code FareDetail} explicitly.
	 *
	 * @param segmentInfo
	 * @param bookingUser
	 */
	public void updateCancellationAndReschedulingFees(SegmentInfo segmentInfo, User bookingUser) {
		/*
		 * segmentInfo.getPriceInfoList().forEach(priceInfo -> { for (FareDetail fareDetail :
		 * priceInfo.getFareDetails().values()) { FlightBasicFact flightBasicFact =
		 * FlightBasicFact.createFact().generateFact(segmentInfo)
		 * .generateFact(bookingUser).generateFact(priceInfo).generateFact(fareDetail);
		 *
		 * FareRuleInfo fareRuleInfo = FareRuleHelper.getFareRule(flightBasicFact); FareRuleInformation
		 * fareRuleInformation = (fareRuleInfo == null ? null : fareRuleInfo.getFareRuleInformation());
		 *
		 * updateCancellationAndReschedulingFees(fareDetail, fareRuleInformation, flightBasicFact, bookingUser); } });
		 */

		segmentInfo.getBookingRelatedInfo().getTravellerInfo().forEach(pax -> {
			if (pax.getStatus() == null) {
				FareDetail fareDetail = pax.getFareDetail();
				FlightBasicFact flightBasicFact = FlightBasicFact.createFact().generateFact(segmentInfo)
						.generateFact(segmentInfo.getPriceInfo(0)).generateFact(fareDetail);
				ClientGeneralInfo clientGeneralInfo = ServiceCommunicatorHelper.getClientInfo();
				flightBasicFact.setAirType(
						segmentInfo.getAirType(clientGeneralInfo == null ? null : clientGeneralInfo.getCountry()));
				flightBasicFact.setRouteInfos(getRouteInfo(segmentInfo));
				flightBasicFact
						.setTravelPeriod(getTravelPeriod(segmentInfo.getDepartTime(), segmentInfo.getArrivalTime()));
				BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
				FareRuleInfo fareRuleInfo = FareRuleHelper.getFareRule(flightBasicFact, false);
				FareRuleInformation fareRuleInformation =
						(fareRuleInfo == null ? null : fareRuleInfo.getFareRuleInformation());
				updateCancellationAndReschedulingFees(fareDetail, fareRuleInformation, flightBasicFact, bookingUser);
			}
		});
	}

	// temp fix
	private TimePeriod getTravelPeriod(LocalDateTime departTime, LocalDateTime arrivalTime) {
		TimePeriod travelPeriod = new TimePeriod();
		travelPeriod.setStartTime(departTime);
		travelPeriod.setEndTime(arrivalTime);
		return travelPeriod;
	}

	public List<RouteInfo> getRouteInfo(SegmentInfo segmentInfo) {
		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setFromCityOrAirport(segmentInfo.getDepartAirportInfo());
		routeInfo.setToCityOrAirport(segmentInfo.getArrivalAirportInfo());
		routeInfo.setTravelDate(segmentInfo.getDepartTime().toLocalDate());
		return Arrays.asList(routeInfo);
	}

	/**
	 * @param fareRuleInformation FeeOutput
	 *
	 * @implSpec Fetches cancellation and reschedule fees from {@code fareRuleInformation}, and client-cancellation and
	 *           client-reschedule fees from {@code clientFeeOutput}. It puts these fees in fare-details of all
	 *           priceInfos, for all paxTypes.
	 */
	private void updateCancellationAndReschedulingFees(FareDetail fareDetail, FareRuleInformation fareRuleInformation,
			final FlightBasicFact flightBasicFact, User bookingUser) {
		// non-null map
		Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo =
				Optional.ofNullable(fareRuleInformation).orElseGet(() -> new FareRuleInformation()).getFareRuleInfo();

		if (MapUtils.isEmpty(fareRuleInfo)) {
			return;
		}

		AirGeneralPurposeOutput output =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		boolean isClientFeeTaxAllowed = BooleanUtils.isTrue(output.getCcftAllowed());

		Map<FareRuleTimeWindow, FareRulePolicyContent> cancellationPolicy, reschedulePolicy;
		cancellationPolicy = fareRuleInfo.get(FareRulePolicyType.CANCELLATION);
		reschedulePolicy = fareRuleInfo.get(FareRulePolicyType.DATECHANGE);

		FareRulePolicyContent cancelPolicyContent, reschedulePolicyContent;
		cancelPolicyContent =
				Optional.ofNullable(cancellationPolicy).orElse(new HashMap<>()).get(FareRuleTimeWindow.DEFAULT);
		reschedulePolicyContent =
				Optional.ofNullable(reschedulePolicy).orElse(new HashMap<>()).get(FareRuleTimeWindow.DEFAULT);

		Double cancellationFee, rescheduleFee, clientCF, clientRF;
		cancellationFee = Optional.ofNullable(cancelPolicyContent).orElse(new FareRulePolicyContent()).getAmount();
		rescheduleFee = Optional.ofNullable(reschedulePolicyContent).orElse(new FareRulePolicyContent()).getAmount();
		clientCF = getClientCancellationFee(cancelPolicyContent, flightBasicFact);
		clientRF = getClientRescheduleFee(reschedulePolicyContent, flightBasicFact);

		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
		BaseUtils.updateFareComponent(fareComponents, FareComponent.ACF, cancellationFee, bookingUser);
		BaseUtils.updateFareComponent(fareComponents, FareComponent.ARF, rescheduleFee, bookingUser);
		BaseUtils.updateFareComponent(fareComponents, FareComponent.CCF, clientCF, bookingUser, isClientFeeTaxAllowed);
		BaseUtils.updateFareComponent(fareComponents, FareComponent.CRF, clientRF, bookingUser, isClientFeeTaxAllowed);
	}


	public void updateCancellationCCF(FlightTravellerInfo pax, SegmentInfo segmentInfo, User bookingUser,
			TripInfo tripInfo) {
		FareDetail fareDetail = pax.getFareDetail();
		FlightBasicFact flightBasicFact = FlightBasicFact.createFact().generateFact(segmentInfo)
				.generateFact(segmentInfo.getPriceInfo(0)).generateFact(fareDetail);
		BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
		flightBasicFact.setAirType(AirUtils.getAirType(tripInfo));
		flightBasicFact.setRouteInfos(getRouteInfo(segmentInfo));
		flightBasicFact.setTravelPeriod(getTravelPeriod(segmentInfo.getDepartTime(), segmentInfo.getArrivalTime()));
		FareRuleInfo fareRuleInfo = FareRuleHelper.getFareRule(flightBasicFact, false);
		FareRuleInformation fareRuleInformation = (fareRuleInfo == null ? null : fareRuleInfo.getFareRuleInformation());
		updateCancellationFee(fareDetail, fareRuleInformation, flightBasicFact, bookingUser,
				AirUtils.getAirType(tripInfo));
	}

	private void updateCancellationFee(FareDetail fareDetail, FareRuleInformation fareRuleInformation,
			FlightBasicFact flightBasicFact, User bookingUser, AirType airType) {

		Map<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo =
				Optional.ofNullable(fareRuleInformation).orElseGet(() -> new FareRuleInformation()).getFareRuleInfo();

		AirGeneralPurposeOutput output =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		boolean isClientFeeTaxAllowed = BooleanUtils.isTrue(output.getCcftAllowed());
		Map<FareRuleTimeWindow, FareRulePolicyContent> cancellationPolicy = null;
		if (!MapUtils.isEmpty(fareRuleInfo)) {
			cancellationPolicy = fareRuleInfo.get(FareRulePolicyType.CANCELLATION);
		}

		FareRulePolicyContent cancelPolicyContent;
		cancelPolicyContent =
				Optional.ofNullable(cancellationPolicy).orElse(new HashMap<>()).get(FareRuleTimeWindow.DEFAULT);


		Double clientCF;
		clientCF = getAirTypeClientCancellationFee(cancelPolicyContent, flightBasicFact, airType);
		if (clientCF != null) {
			Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
			BaseUtils.updateFareComponent(fareComponents, FareComponent.CCF, clientCF, bookingUser,
					isClientFeeTaxAllowed);
		}
		// clientRF = getClientRescheduleFee(reschedulePolicyContent, flightBasicFact);

	}

	private Double getAirTypeClientCancellationFee(FareRulePolicyContent cancelPolicyContent,
			FlightBasicFact flightBasicFact, AirType airType) {
		Double fee;
		if (cancelPolicyContent == null || (fee = cancelPolicyContent.getAdditionalFee()) == null) {
			return getAirTypeClientCancellationFeeFromConfigurator(flightBasicFact, airType);
		}
		return fee;
	}

	private Double getAirTypeClientCancellationFeeFromConfigurator(FlightBasicFact flightBasicFact, AirType airType) {
		AirClientFeeOutput clientFeeOutput = AirUtils.getClientFeeOutput(flightBasicFact);
		AirType val = null;
		if (clientFeeOutput != null && clientFeeOutput.getAirTypeClientCancellationFee() != null
				&& (val = getAirType(clientFeeOutput, airType)) != null) {
			return clientFeeOutput.getAirTypeClientCancellationFee().get(val);
		}
		return null;
	}

	private AirType getAirType(AirClientFeeOutput clientFeeOutput, AirType airType) {
		AirType val = clientFeeOutput.getAirTypeClientCancellationFee().get(airType) != null ? airType
				: clientFeeOutput.getAirTypeClientCancellationFee().get(AirType.ALL) != null ? AirType.ALL : null;
		return val;
	}

	private Double getClientCancellationFee(FareRulePolicyContent cancellationFareRule,
			final FlightBasicFact flightBasicFact) {
		Double fee;
		if (cancellationFareRule == null || (fee = cancellationFareRule.getAdditionalFee()) == null) {
			return getClientCancellationFeeFromConfigurator(flightBasicFact);
		}
		return fee;
	}

	private Double getClientRescheduleFee(FareRulePolicyContent rescheduleFareRule,
			final FlightBasicFact flightBasicFact) {
		Double fee;
		if (rescheduleFareRule == null || (fee = rescheduleFareRule.getAdditionalFee()) == null) {
			return getClientRescheduleFeeFromConfigurator(flightBasicFact);
		}
		return fee;
	}

	private Double getClientCancellationFeeFromConfigurator(final FlightBasicFact flightBasicFact) {
		Double cancellationFee =
				getAirTypeClientCancellationFeeFromConfigurator(flightBasicFact, flightBasicFact.getAirType());
		if (Objects.nonNull(cancellationFee)) {
			return cancellationFee;
		}
		AirClientFeeOutput clientFeeOutput = AirUtils.getClientFeeOutput(flightBasicFact);
		return Optional.ofNullable(clientFeeOutput).orElseGet(() -> new AirClientFeeOutput())
				.getClientCancellationFee();
	}

	private Double getClientRescheduleFeeFromConfigurator(final FlightBasicFact flightBasicFact) {
		AirClientFeeOutput clientFeeOutput = AirUtils.getClientFeeOutput(flightBasicFact);
		return Optional.ofNullable(clientFeeOutput).orElseGet(() -> new AirClientFeeOutput()).getClientRescheduleFee();
	}

	private void applyPartnerClientFee(FareRuleInformation fareRuleInfo, TripInfo tripInfo, User user,
			AirFlowType flowType, String bookingId) {
		// add partner cancellation/reschedule fee
		if (MapUtils.isNotEmpty(fareRuleInfo.getFareRuleInfo())) {
			if (AirFlowType.REVIEW.equals(flowType)) {
				// this will refresh if new updated by partner admin
				feeManager.processPartnerMarkUp(user, tripInfo, UserFeeType.PARTNER_MARKUP, bookingId);
			}
			Double partnerFee = getPartnerFee(tripInfo, FareComponent.PMU);
			// partnerfeetax also goes into Parent component, not to tax component
			Double partnerFeeTax = getPartnerFee(tripInfo, FareComponent.PMTDS);
			Map<FareRuleTimeWindow, FareRulePolicyContent> content = null;
			for (FareRulePolicyType policy : fareRuleInfo.getFareRuleInfo().keySet()) {
				switch (policy) {
					case CANCELLATION:
						content = fareRuleInfo.getFareRuleInfo().get(policy);
						applyPartnerCharges(content, FareComponent.CCF, FareComponent.CCFT, partnerFee, partnerFeeTax);
						break;
					case DATECHANGE:
						content = fareRuleInfo.getFareRuleInfo().get(policy);
						applyPartnerCharges(content, FareComponent.CRF, FareComponent.CRFT, partnerFee, partnerFeeTax);
						break;
					default:
						break;

				}
			}
		}
	}

	private void applyPartnerCharges(Map<FareRuleTimeWindow, FareRulePolicyContent> content, FareComponent parentComp,
			FareComponent taxComp, Double partnerFee, Double partnerFeeTax) {
		if (MapUtils.isNotEmpty(content)) {
			for (FareRuleTimeWindow window : content.keySet()) {
				FareRulePolicyContent policyContent = content.get(window);
				if (policyContent != null && MapUtils.isNotEmpty(policyContent.getFareComponents())) {
					for (FareComponent component : policyContent.getFareComponents().keySet()) {
						if (parentComp.equals(component)) {
							Double initialAmount = policyContent.getFareComponents().get(component);
							policyContent.getFareComponents().put(component, initialAmount + partnerFee);
							if (policyContent.getAdditionalFee() != null) {
								policyContent.setAdditionalFee(
										policyContent.getAdditionalFee() + partnerFee + partnerFeeTax);
							}
						}
					}
				}
			}
		}
	}


	private Double getPartnerFee(TripInfo tripInfo, FareComponent partfeeComp) {
		Double partnerFee = new Double(0);
		if (tripInfo != null && tripInfo.isPriceInfosNotEmpty()) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
				// In Fare rule we always consider Adult fare type
				FareDetail fareDetail = priceInfo.getFareDetail(PaxType.ADULT, new FareDetail());
				if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
					partnerFee += fareDetail.getFareComponents().getOrDefault(partfeeComp, 0.0);
				}
			}
		}
		return partnerFee;
	}

}
