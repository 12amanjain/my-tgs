package com.tgs.services.fms.manager;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.helper.AirlineHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ShowPublicBookPrivateEngine {

	/**
	 * @param tripInfo
	 * @param user
	 * @implNote <br>
	 *           1.if user not allowed for private book then public fare (difference on private fare will be added to
	 *           private price info) <br>
	 *           2. First trip should have public fare trip <br>
	 *           3. LCC fare Never should be private -> pricemiscinfo - isprivatefare indicator should be false ,
	 *           otherwise in Review it will break (due to Flow not able to identify)
	 */
	public void isUserShowPublicBookPrivate(TripInfo tripInfo, User user, AirSourceConfigurationOutput sourceConfig) {

		if (BaseUtils.isUserShowPublicBookPrivate(user)
				|| (sourceConfig != null && CollectionUtils.isNotEmpty(sourceConfig.getSpecialAccountCodes()))) {


			boolean isBasedOnFareType =
					sourceConfig != null && CollectionUtils.isNotEmpty(sourceConfig.getSpecialAccountCodes());
			boolean isLCCAirline = AirlineHelper.getAirlineInfo(tripInfo.getAirlineCode()).getIsLcc();

			List<PriceInfo> tripPrices = tripInfo.getTripPriceInfos();
			String userId = UserUtils.getParentUserId(user);

			List<PriceInfo> privateFares = tripPrices.stream().filter(priceInfo -> {
				return isValidPrivateFare(priceInfo)
						&& !priceInfo.getFareDetail(PaxType.ADULT).getFareComponents().containsKey(FareComponent.XT);
			}).collect(Collectors.toList());

			List<PriceInfo> publicFares = tripPrices.stream().filter(priceInfo -> {
				return !priceInfo.isPrivateFare();
			}).collect(Collectors.toList());

			privateFares.forEach(privateFarePriceInfo -> {


				log.debug(
						"Private fares count {}, public fare count {} for user before Show public Book Private Logic {} trip flight key {}",
						privateFares.size(), publicFares.size(), userId, tripInfo.getFlightNumberSet());

				CabinClass cabinClass = privateFarePriceInfo.getCabinClass(PaxType.ADULT);

				/**
				 * 1.Find the cheapest public fare from trip <br>
				 * 1A . If No Cheapest fare applicable for corresponding private fare <br>
				 * (Remove That private Fare entire trip) As user strictly restrict for Private fare (even if
				 * expensive)<br>
				 * 2.Filter Private Fare List from Trip<br>
				 * 3.Show Public Book Private Logic (if private fare < public fare) apply logic 1 to all private fare
				 * list
				 */

				PriceInfo cheapestPublicFareInfo =
						findCheapestPublicFare(cabinClass, publicFares, privateFarePriceInfo, isBasedOnFareType);

				if (cheapestPublicFareInfo != null && MapUtils.isNotEmpty(cheapestPublicFareInfo.getFareDetails())) {
					Double diff =
							privateFarePriceInfo.getTotalAirlineFare() - cheapestPublicFareInfo.getTotalAirlineFare();

					/**
					 * @implNote : <br>
					 *           1. <b>GDS- Sabre,Amadeus,Travelport </b> - Applicable on Zero and difference means <br>
					 *           a. Even if Public and private fare are same - Difference is zero , to get benefits we
					 *           will book on Private fare <br>
					 *           b. private fare is cheaper - Difference of Public fare will added as XT and book on
					 *           Private fare <br>
					 *           2. <b>SG LCC</b> - Applicable only on difference of public and private above zero fare
					 * 
					 */
					if ((!isLCCAirline && privateFarePriceInfo.getTotalAirlineFare() <= cheapestPublicFareInfo
							.getTotalAirlineFare())
							|| (isLCCAirline && privateFarePriceInfo.getTotalAirlineFare() < cheapestPublicFareInfo
									.getTotalAirlineFare())) {
						Double diffOnPublicfare = Math.abs(cheapestPublicFareInfo.getTotalAirlineFare()
								- privateFarePriceInfo.getTotalAirlineFare());
						Integer segmentCount = tripInfo.getNumOfSegments();
						updatePublicFareOnPrivateSegment(tripInfo, privateFarePriceInfo, diffOnPublicfare, segmentCount,
								user);
						// After applying Public fare to Private Fare , Remove Public Fare
						log.debug(
								"SPBP:Filtering Cheapest Fare user cause SPBP logic applied {} from trip {} flightkey {}",
								userId, cheapestPublicFareInfo.getFareIdentifier(), tripInfo.getFlightNumberSet());
						removeRestrictedFare(cheapestPublicFareInfo, tripInfo);
					} else {
						log.debug(
								"In:Filtering Private Fare for user {} from trip {} flightkey {} due to Private fare is higher than public fare {}",
								userId, privateFarePriceInfo.getFareIdentifier(), tripInfo.getFlightNumberSet(), diff);
						removeRestrictedFare(privateFarePriceInfo, tripInfo);
					}
				} else {
					log.debug(
							"Ex:Filtering Private Fare for user as forcefully not allowed to search {} from trip {} flightkey {}",
							userId, privateFarePriceInfo.getFareIdentifier(), tripInfo.getFlightNumberSet());
					removeRestrictedFare(privateFarePriceInfo, tripInfo);
				}
			});
		}
	}

	private void removeRestrictedFare(PriceInfo priceInfo, TripInfo tripInfo) {
		int indexToBeRemoved = getPriceIndex(tripInfo, priceInfo);
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			if (indexToBeRemoved < segmentInfo.getPriceInfoList().size())
				segmentInfo.getPriceInfoList().remove(indexToBeRemoved);
		});
	}

	private int getPriceIndex(TripInfo tripInfo, PriceInfo existPriceInfo) {
		int priceIndex = 0;
		List<PriceInfo> priceList = tripInfo.getSegmentInfos().get(0).getPriceInfoList();
		for (priceIndex = 0; priceIndex < priceList.size(); priceIndex++) {
			if (AirUtils.isSamePriceInfo(priceList.get(priceIndex), existPriceInfo) && StringUtils
					.equalsIgnoreCase(priceList.get(priceIndex).getAccountCode(), existPriceInfo.getAccountCode())) {
				break;
			}
		}
		return priceIndex;
	}

	// Filter according to Cabin Class & based on fare type as well
	private PriceInfo findCheapestPublicFare(CabinClass privateFarecabinClass, List<PriceInfo> publicFares,
			PriceInfo pfPriceInfo, boolean isBasedOnFareType) {

		AtomicReference<PriceInfo> cheapestPrice = new AtomicReference<>();

		String privfFareType = pfPriceInfo.getFareType();

		if (CollectionUtils.isNotEmpty(publicFares)) {

			Double publicTotalfare = publicFares.get(0).getTotalAirlineFare();

			publicFares.forEach(publicFare -> {
				String pubfFareType = publicFare.getFareType();
				Double currentPrice = publicFare.getTotalAirlineFare();
				CabinClass publicCabinClass = publicFare.getCabinClass(PaxType.ADULT);
				if (isBasedOnFareType && publicCabinClass.equals(privateFarecabinClass)
						&& currentPrice <= publicTotalfare && privfFareType.equalsIgnoreCase(pubfFareType)) {
					cheapestPrice.set(publicFare);
				} else if (!isBasedOnFareType && publicCabinClass.equals(privateFarecabinClass)
						&& currentPrice <= publicTotalfare) {
					cheapestPrice.set(publicFare);
				}

			});
		}

		return cheapestPrice.get();
	}

	public void updatePublicFareOnPrivateSegment(TripInfo tripInfo, PriceInfo priceInfo, Double diffOnPublicFare,
			Integer numOfSegments, User user) {
		Integer paxCount = priceInfo.getTotalPaxCountOnPaxTypeWise(true);
		double amountOnSegment = Math.abs(diffOnPublicFare);
		double xtFare = amountOnSegment / paxCount;
		SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
		segmentInfo.getPriceInfoList().forEach(priceInfo1 -> {
			if (MapUtils.isNotEmpty(priceInfo1.getFareDetails()) && AirUtils.isSamePriceInfo(priceInfo, priceInfo1)
					&& BooleanUtils.isTrue(priceInfo1.getMiscInfo().getIsPrivateFare())) {
				for (PaxType paxType : priceInfo1.getFareDetails().keySet()) {
					FareDetail fareDetail = priceInfo1.getFareDetail(paxType);
					if (!isXTExists(fareDetail)) {
						BaseUtils.updateFareComponent(fareDetail.getFareComponents(), FareComponent.XT, xtFare, user);
					}
				}
			}
		});
	}

	/**
	 * @param oldTripInfo
	 * @param newTripInfo
	 * @implSpec : This Method Blindly Consider that oldTrip has all details
	 * @implNote : This Will add XT Fare Component to new TripInfo (which is copied from oldTrip) before applying will
	 *           check new trip is private fare
	 */
	public void addXtraFareOnTrip(TripInfo oldTripInfo, TripInfo newTripInfo, User user, String bookingId) {

		List<TripInfo> oldTripInfos = oldTripInfo.splitTripInfo(false);
		List<TripInfo> newTripInfos = newTripInfo.splitTripInfo(false);

		int tripIndex = 0;
		for (TripInfo oldTrip : oldTripInfos) {
			TripInfo newTrip = newTripInfos.get(tripIndex);
			if (isValidPrivateFare(newTrip)) {
				if (CollectionUtils.isNotEmpty(newTrip.getSegmentInfos())
						&& CollectionUtils.isNotEmpty(newTrip.getSegmentInfos().get(0).getPriceInfoList())) {
					SegmentInfo oldSegmentInfo = oldTrip.getSegmentInfos().get(0);
					SegmentInfo newSegmentInfo = newTrip.getSegmentInfos().get(0);
					addXtraFare(oldSegmentInfo.getPriceInfo(0), newSegmentInfo.getPriceInfo(0), user);
				}
				log.info("Added Xtra Fare to trip oldtrip {},newtrip {} for user {} booking {}", oldTripInfo,
						newTripInfo, UserUtils.getParentUserId(user), bookingId);
			}
			tripIndex++;
		}
	}

	/**
	 * @param oldPriceInfo
	 * @param newPriceInfo
	 * @implNote : This Method will copy oldPriceinfo XT fare component to newPriceInfo
	 */
	public void addXtraFare(PriceInfo oldPriceInfo, PriceInfo newPriceInfo, User user) {
		FareDetail oldFareInfo = oldPriceInfo.getFareDetail(PaxType.ADULT);
		if (oldFareInfo.getFareComponents().containsKey(FareComponent.XT)) {
			newPriceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
				if (!isXTExists(fareDetail)) {
					FareDetail oldFareDetail = oldPriceInfo.getFareDetail(paxType);
					double xtraFare = oldFareDetail.getFareComponents().getOrDefault(FareComponent.XT, 0.0);
					BaseUtils.updateFareComponent(fareDetail.getFareComponents(), FareComponent.XT, xtraFare, user);
					AirUtils.copyStaticInfo(fareDetail, oldFareDetail);
				}
			}));
		}
	}

	public boolean isValidPrivateFare(TripInfo newTripInfo) {
		PriceMiscInfo miscInfo = newTripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo();
		return newTripInfo != null && BooleanUtils.isTrue(miscInfo.getIsPrivateFare())
				&& StringUtils.isNotBlank(miscInfo.getAccountCode());
	}

	public boolean isValidPrivateFare(PriceInfo priceInfo) {
		PriceMiscInfo miscInfo = priceInfo.getMiscInfo();
		return BooleanUtils.isTrue(miscInfo.getIsPrivateFare()) && StringUtils.isNotBlank(miscInfo.getAccountCode());
	}

	public boolean isXTExists(FareDetail fareDetail) {
		return fareDetail.getFareComponents().containsKey(FareComponent.XT);
	}

}
