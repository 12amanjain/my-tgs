package com.tgs.services.fms.manager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.helper.SupplierSessionHelper;
import com.tgs.services.fms.jparepository.SupplierSessionService;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.PaxKey;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TripBookingUtils {

	static SupplierSessionService sessionService;

	static SupplierSessionHelper sessionHelper;

	public static void init(SupplierSessionService sessionService, SupplierSessionHelper sessionHelper) {
		TripBookingUtils.sessionHelper = sessionHelper;
		TripBookingUtils.sessionService = sessionService;
	}

	public static Map<String, List<TripInfo>> getSourceWiseTripInfo(List<TripInfo> trips, String bookingId) {
		return getSourceWiseTrip(trips, bookingId);
	}

	public static Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingSession(List<TripInfo> trips,
			String bookingId, User bookingUserId) {
		return getSupplierWiseBookingSegments(trips, bookingId, bookingUserId);
	}

	public static Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingPNR(List<TripInfo> trips,
			String bookingId) {
		return getSupplierWiseBookingSegmentsUsingPNR(trips);
	}

	public static Map<String, BookingSegments> getSupplierWiseBookingSegments(List<TripInfo> trips, String bookingId,
			User bookingUser) {

		Map<String, BookingSegments> supplierBookingSegments = new HashMap<>();
		List<SupplierSession> supplierSessions = getSupplierSession(bookingId, bookingUser);
		List<SegmentInfo> segmentInfos = AirUtils.getSegmentInfos(trips);
		BookingSegments bookingSegments = null;
		if (CollectionUtils.isNotEmpty(supplierSessions)) {
			for (int i = 0; i < supplierSessions.size(); i++) {
				SupplierSession supplierSession = supplierSessions.get(i);
				String segmentBookingKey = supplierSession.getSupplierSessionInfo().getTripKey();

				List<String> sessionKey = Arrays.asList(segmentBookingKey.split(":"));
				int segmentIndex = 0;
				for (segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
					bookingSegments = BookingSegments.builder().build();
					SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
					String segmentKey = segmentInfo.getSegmentBookingKey();
					if (sessionKey.contains(segmentKey)) {
						// If Session Contains current segment then add to corresponding segment Key
						BookingSegments tempSegments =
								supplierBookingSegments.getOrDefault(segmentBookingKey, bookingSegments);
						tempSegments.getSegmentInfos().add(segmentInfo);
						tempSegments.setSupplierSession(supplierSession);
						supplierBookingSegments.put(segmentBookingKey, tempSegments);
					}
				}
			}
		}
		log.info("Inside TripBookingUtils#getSupplierWiseBookingSegments size {} for bookingid {}",
				supplierBookingSegments.size(), bookingId);
		return supplierBookingSegments;
	}

	public static List<SupplierSession> getSupplierSession(String bookingId, User user) {
		if (AirUtils.isSupplierSessionCachingEnabled(user)) {
			return sessionHelper.getBookingSession(bookingId);
		}
		return sessionService.getBookingSession(bookingId);
	}

	public static Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingPNR(List<TripInfo> trips) {
		Map<String, BookingSegments> supplierBookingSegments = new HashMap<>();
		List<SegmentInfo> segmentInfos = AirUtils.getSegmentInfos(trips);
		segmentInfos.forEach(segmentInfo -> {
			BookingSegments tempSegments = BookingSegments.builder().build();
			FlightTravellerInfo travellerInfo = segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(0);
			String key = ObjectUtils.firstNonNull(travellerInfo.getSupplierBookingId(), travellerInfo.getPnr());
			tempSegments = supplierBookingSegments.getOrDefault(key, tempSegments);
			tempSegments.getSegmentInfos().add(segmentInfo);
			tempSegments.setAirlinePNR(travellerInfo.getPnr());
			tempSegments.setSupplierBookingId(travellerInfo.getSupplierBookingId());
			supplierBookingSegments.put(key, tempSegments);
		});

		return supplierBookingSegments;
	}


	public static Map<String, BookingSegments> getSupplierWiseBookingSegments(List<SegmentInfo> segmentInfos,
			Map<String, Set<PaxKey>> paxKeys) {
		Map<String, BookingSegments> supplierBookingSegments = new LinkedHashMap<>();
		segmentInfos.forEach(segmentInfo -> {
			BookingSegments tempSegments = BookingSegments.builder().build();
			if (CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				FlightTravellerInfo travellerInfo = null;
				for (FlightTravellerInfo traveller : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					for (Map.Entry<String, Set<PaxKey>> entry : paxKeys.entrySet()) {
						for (PaxKey paxkey : entry.getValue()) {
							if (paxkey.getId().equals(traveller.getId())) {
								travellerInfo = traveller;
								String key = ObjectUtils.firstNonNull(travellerInfo.getPnr(),
										travellerInfo.getSupplierBookingId());
								tempSegments = supplierBookingSegments.getOrDefault(key, tempSegments);
								boolean isSegmentExist = false;
								if (!tempSegments.getSegmentInfos().isEmpty()) {
									for (SegmentInfo tempSegmentInfo : tempSegments.getSegmentInfos()) {
										if (tempSegmentInfo.getId().equals(segmentInfo.getId())) {
											isSegmentExist = true;
											break;
										}
									}
								}
								if (!isSegmentExist)
									tempSegments.getSegmentInfos().add(segmentInfo);
								tempSegments.setAirlinePNR(travellerInfo.getPnr());
								tempSegments.setSupplierBookingId(travellerInfo.getSupplierBookingId());
								supplierBookingSegments.put(key, tempSegments);
							}
						}
					}

				}
			}
		});
		return supplierBookingSegments;
	}

	public static Map<String, BookingSegments> filterCancellingTravellers(Map<String, Set<PaxKey>> paxKeys,
			List<SegmentInfo> segmentInfoList) {
		Map<String, BookingSegments> cancelBookingSegments = getSupplierWiseBookingSegments(segmentInfoList, paxKeys);
		for (Map.Entry<String, Set<PaxKey>> entry : paxKeys.entrySet()) {
			for (SegmentInfo segmentInfo : segmentInfoList) {
				SegmentInfo copySegementInfo = new GsonMapper<>(segmentInfo, SegmentInfo.class).convert();
				BookingSegments tempSegments = BookingSegments.builder().build();
				if (entry.getKey().equals(segmentInfo.getId())) {
					List<FlightTravellerInfo> travellerInfos = new ArrayList<FlightTravellerInfo>();
					for (PaxKey paxkey : entry.getValue()) {
						for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
							if (paxkey.getId() == travellerInfo.getId()) {
								FlightTravellerInfo traveller =
										new GsonMapper<>(travellerInfo, FlightTravellerInfo.class).convert();
								traveller.unsetDetails();
								travellerInfos.add(traveller);
							}
						}
					}
					// airlineRef - > LCC airline pnr , if GDS GDS PNR
					for (FlightTravellerInfo flightTravellerInfo : travellerInfos) {
						String supplierRef = ObjectUtils.firstNonNull(flightTravellerInfo.getPnr(),
								flightTravellerInfo.getSupplierBookingId());
						tempSegments = new GsonMapper<>(cancelBookingSegments.get(supplierRef), BookingSegments.class)
								.convert();
						copySegementInfo.getBookingRelatedInfo().setTravellerInfo(travellerInfos);
						boolean isSegmentExist = false;
						if (!tempSegments.getCancelSegments().isEmpty()) {
							for (SegmentInfo tempSegmentInfo : tempSegments.getCancelSegments()) {
								if (tempSegmentInfo.getId().equals(segmentInfo.getId())) {
									isSegmentExist = true;
									break;
								}
							}
						}
						if (!isSegmentExist)
							tempSegments.getCancelSegments().add(copySegementInfo);
						cancelBookingSegments.put(supplierRef, tempSegments);
					}
				}
			}
		}
		return cancelBookingSegments;
	}

	public static Map<String, List<TripInfo>> getSourceWiseTrip(List<TripInfo> trips, String bookingId) {

		Map<String, List<TripInfo>> sourceWiseTripMap = new HashMap<>();
		Map<String, List<SegmentInfo>> sourceWiseSegmentMap = new HashMap<>();
		trips.forEach(tripInfo -> {
			tripInfo.setId(RandomStringUtils.random(2, false, true));
			if (tripInfo.isMultipleSupplier()) {
				// Should Consider Manual Combination will be Null Due To Not Persisting in
				// DB,split trip on Segment Number
				// Demerge TripInfo when Different Suppliers on Segment
				tripInfo.getSegmentInfos().forEach(segmentInfo -> {
					/**
					 * if combo airline - which means multi ticket will be generated : key with trip id,supplierid else
					 * - which across all segment same supplier - single ticket : key with supplierid
					 */
					String supplierId = StringUtils.join(segmentInfo.getSupplierInfo().getSupplierId(),
							segmentInfo.getSegmentNum());
					if (sourceWiseSegmentMap.get(supplierId) == null) {
						sourceWiseSegmentMap.put(supplierId, new ArrayList<>());
					}
					sourceWiseSegmentMap.get(supplierId).add(segmentInfo);
				});
			} else {
				String supplierId = tripInfo.getSupplierInfo().getSupplierId();
				if (sourceWiseTripMap.get(supplierId) == null) {
					sourceWiseTripMap.put(supplierId, new ArrayList<>());
				}
				sourceWiseTripMap.get(supplierId).addAll(Arrays.asList(tripInfo));
			}

			// supplierid will be key - has corresponding segments will treated as trip
			sourceWiseSegmentMap.forEach((supplierId, segments) -> {
				TripInfo trip = new TripInfo();
				trip.getSegmentInfos().addAll(segments);
				sourceWiseTripMap.put(supplierId, Arrays.asList(trip));
			});
		});
		log.info("Inside TripBookingUtils#sourcewisetrip size {} for booking id {}", sourceWiseTripMap.size(),
				bookingId);
		return sourceWiseTripMap;
	}


}
