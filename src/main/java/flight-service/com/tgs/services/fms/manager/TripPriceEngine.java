package com.tgs.services.fms.manager;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirFilterConfiguration;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TripPriceEngine {

	public static void filterAirlinesAndFareTypes(SupplierConfiguration supplierConfiguration,
			AirSearchResult searchResult, AirSearchQuery searchQuery) {
		filterAirlines(supplierConfiguration, searchResult, searchQuery);
	}


	public static void setSpecialReturnIdentifier(SegmentInfo onwardSegment, PriceInfo onwardPriceInfo,
			SegmentInfo returnSegment, PriceInfo returnPriceInfo) {
		String onwardIdentifier;
		String returnIdentifier;
		if (StringUtils.isEmpty(onwardPriceInfo.getSpecialReturnIdentifier())) {
			onwardIdentifier = StringUtils.join(TgsStringUtils.generateRandomNumber(4, ""),
					onwardSegment.getAirlineCode(false), onwardSegment.getFlightNumber());
		} else {
			onwardIdentifier = onwardPriceInfo.getSpecialReturnIdentifier();
		}
		if (StringUtils.isEmpty(returnPriceInfo.getSpecialReturnIdentifier())) {
			returnIdentifier = StringUtils.join(TgsStringUtils.generateRandomNumber(4, ""),
					returnSegment.getAirlineCode(false), returnSegment.getFlightNumber());
		} else {
			returnIdentifier = returnPriceInfo.getSpecialReturnIdentifier();
		}
		onwardPriceInfo.setSpecialReturnIdentifier(onwardIdentifier);
		onwardPriceInfo.getMatchedSpecialReturnIdentifier().add(returnIdentifier);
		returnPriceInfo.setSpecialReturnIdentifier(returnIdentifier);
		returnPriceInfo.getMatchedSpecialReturnIdentifier().add(onwardIdentifier);
	}

	protected static void filterAndMergePriceInfo(TripInfo tripInfo, Map<String, TripInfo> keyMap, String key,
			boolean isMergeRequired) {
		try {
			int priceIndex = 0;
			List<Integer> listOfPriceIndex = new ArrayList<>();

			/**
			 * Finding listofPriceIndex based on farebasis , if any of farebasis already exist in old segmentInfo then
			 * there is no need to add it again. This typically happen in case of multiple suppliers and it can happen
			 * in domestic return as well.
			 */
			for (PriceInfo priceInfo : tripInfo.getSegmentInfos().get(0).getPriceInfoList()) {
				boolean fareBasisAlreadyExist = false;
				for (PriceInfo oldPriceInfo : keyMap.get(key).getSegmentInfos().get(0).getPriceInfoList()) {
					boolean isSamePriceType = AirUtils.isSamePriceInfo(oldPriceInfo, priceInfo);
					boolean isSameBaseFare = AirUtils.isSameBaseFare(oldPriceInfo, priceInfo);
					boolean isSpecialFare = BooleanUtils.isTrue(oldPriceInfo.isPrivateFare())
							|| BooleanUtils.isTrue(priceInfo.isPrivateFare());
					if (isSpecialFare && priceInfo.getSourceId() == AirSourceType.SPICEJET.getSourceId()
							&& isSameBaseFare && isSamePriceType && !StringUtils
									.equalsIgnoreCase(priceInfo.getAccountCode(), oldPriceInfo.getAccountCode())) {
						/**
						 * @implNote :<br>
						 *           1. Even Base Fare of Current PriceInfo and Account CodePrice Info are same,<br>
						 *           it will get merged to single tripinfo <br>
						 *           2. Those special private fare will get removed or booked as special fare by using
						 *           ShowPublicBookPrivateEngine Engine logic <br>
						 * @see ShowPublicBookPrivateEngine#isUserShowPublicBookPrivate logic
						 */
						continue;
					} else if (isMergeRequired && isSamePriceType && isSameBaseFare) {
						// log.info("Merging Price info for type {}", priceInfo.getFareIdentifier());
						fareBasisAlreadyExist = true;
						break;
					}
				}
				if (!fareBasisAlreadyExist) {
					listOfPriceIndex.add(priceIndex);
				}
				priceIndex++;
			}

			// Adding those priceIndexes which are new in TripInfo will be clubbed in existing segmentInfo
			int segmentIndex = 0;
			for (SegmentInfo segmentInfo : keyMap.get(key).getSegmentInfos()) {
				for (Integer prIndex : listOfPriceIndex) {
					SegmentInfo sourceSegment = tripInfo.getSegmentInfos().get(segmentIndex);
					if (sourceSegment.getPriceInfoList().size() > prIndex) {
						segmentInfo.getPriceInfoList().add(sourceSegment.getPriceInfo(prIndex));
					}
				}
				segmentIndex++;
			}
		} catch (Exception e) {
			log.error("Error Occured on filtering price for trip {}", tripInfo, e);
		}

	}

	public static void filterComboPriceInfos(TripInfo copyOnwardTrip, TripInfo copyReturnTrip) {
		Set<String> onwardFareTypes = getFareIdentifier(copyOnwardTrip);
		Set<String> returnFareTypes = getFareIdentifier(copyReturnTrip);
		Set<CabinClass> onwardCabinClass = getCabinClass(copyOnwardTrip);
		Set<CabinClass> returnCabinClass = getCabinClass(copyReturnTrip);
		TripPriceEngine.filterBasedOnCabinClass(copyOnwardTrip, returnCabinClass);
		TripPriceEngine.filterBasedOnCabinClass(copyReturnTrip, onwardCabinClass);
		TripPriceEngine.filterBasedOnFareIdentifier(copyOnwardTrip, returnFareTypes);
		TripPriceEngine.filterBasedOnFareIdentifier(copyReturnTrip, onwardFareTypes);
	}

	public static Set<String> getFareIdentifier(TripInfo tripInfo) {
		Set<String> fareIdentifier = new HashSet<>();
		if (tripInfo.isPriceInfosNotEmpty()) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					fareIdentifier.add(priceInfo.getFareType());
				});
			});
		}
		return fareIdentifier;
	}

	public static void filterBasedOnFareIdentifier(TripInfo tripInfo, Set<String> fareTypes) {
		if (CollectionUtils.isNotEmpty(fareTypes)) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> filteredPriceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return fareTypes.contains(priceInfo.getFareType());
				}).collect(Collectors.toList());
				segmentInfo.setPriceInfoList(filteredPriceInfoList);
			});
		}
	}

	public static void filterBasedOnCabinClass(TripInfo tripInfo, Set<CabinClass> cabinClasses) {
		if (CollectionUtils.isNotEmpty(cabinClasses)) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> filteredPriceInfoList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return cabinClasses.contains(priceInfo.getCabinClass(PaxType.ADULT));
				}).collect(Collectors.toList());
				segmentInfo.setPriceInfoList(filteredPriceInfoList);
			});
		}
	}

	public static Set<CabinClass> getCabinClass(TripInfo tripInfo) {
		Set<CabinClass> cabinClasses = new HashSet<>();
		if (tripInfo.isPriceInfosNotEmpty()) {
			tripInfo.getSegmentInfos().get(0).getPriceInfoList().forEach(priceInfo -> {
				cabinClasses.add(priceInfo.getCabinClass(PaxType.ADULT));
			});
		}
		return cabinClasses;
	}


	public static void filterTripOnLayOverTime(AirSearchResult searchResult, AirSearchQuery searchQuery,
			AirSourceConfigurationOutput sourceConfigurationOutput) {
		try {
			if (searchQuery.isIntlReturn() && searchResult != null
					&& MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				for (String key : searchResult.getTripInfos().keySet()) {
					List<TripInfo> tripInfos = searchResult.getTripInfos().get(key);
					if (CollectionUtils.isNotEmpty(tripInfos)) {
						for (Iterator<TripInfo> tripIter = tripInfos.iterator(); tripIter.hasNext();) {
							TripInfo tripInfo = tripIter.next();
							tripInfo.setConnectionTime();
							AtomicBoolean isValidTrip = new AtomicBoolean(true);
							tripInfo.getSegmentInfos().forEach(segmentInfo -> {
								// For Now Will fixed more than 6 hours filtered
								if (segmentInfo.getConnectingTime() != null
										&& segmentInfo.getConnectingTime().intValue() > 360) {
									isValidTrip.set(Boolean.FALSE);
								}
							});
							if (!isValidTrip.get()) {
								tripIter.remove();
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to filter TripOnLayOverTime for searchQuery {]", searchQuery);
		}
	}

	public static void shiftFareDetailsToFlightTraveller(List<TripInfo> tripInfos,
			List<FlightTravellerInfo> flightTravellerInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				SegmentBookingRelatedInfo bookingRelatedInfo = SegmentBookingRelatedInfo.builder().build();
				segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
				segmentInfo.getBookingRelatedInfo().setTravellerInfo(new ArrayList<>());
				segmentInfo.getPriceInfo(0).getFareDetails().forEach((paxType, fareDetail) -> {
					List<FlightTravellerInfo> travellerInfos1 =
							GsonUtils.getGson()
									.fromJson(
											GsonUtils.getGson()
													.toJson(AirUtils.getParticularPaxTravellerInfo(flightTravellerInfos,
															paxType)),
											new TypeToken<List<FlightTravellerInfo>>() {}.getType());
					travellerInfos1.forEach(travellerInfo -> {
						travellerInfo.setFareDetail(
								new GsonMapper<>(fareDetail, travellerInfo.getFareDetail(), FareDetail.class)
										.convert());
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().add(travellerInfo);
					});
				});
				// Finally Make Price Info Fare Detail to empty
				segmentInfo.getPriceInfo(0).setFareDetails(null);
			});
		});
	}

	/**
	 * This method will filter the searchResults airlines and FareTypes based on the Supplier rule configuration
	 *
	 * @param supplierConfiguration
	 * @param searchResult
	 * @param searchQuery
	 * @return
	 */
	public static void filterAirlines(SupplierConfiguration supplierConfiguration, AirSearchResult searchResult,
			AirSearchQuery searchQuery) {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			AirFilterConfiguration airFilterConfig = getAirFilterConfig(supplierConfiguration);
			List<String> allowedAirlines = getAllowedAirlines(searchQuery, supplierConfiguration);
			List<String> disAllowedAirlines = getExcludeAirlines(searchQuery, supplierConfiguration);
			List<String> fareTypes = getDisallowedFareTypes(searchQuery, supplierConfiguration);
			searchResult.getTripInfos().forEach((key, trips) -> {
				for (Iterator<TripInfo> iter = trips.iterator(); iter.hasNext();) {
					TripInfo tripInfo = iter.next();
					boolean isRemoveTrip = false;

					if (CollectionUtils.isNotEmpty(allowedAirlines)
							&& !isValidTripInfo(tripInfo, allowedAirlines, false)) {
						// If Not Allowed Airline for Supplier or Source Config
						isRemoveTrip = true;
					}

					if (CollectionUtils.isNotEmpty(disAllowedAirlines) && !isRemoveTrip
							&& isValidTripInfo(tripInfo, disAllowedAirlines, false)) {
						// Excluded Airlines for Supplier or Source Config
						isRemoveTrip = true;
					}

					if (!isRemoveTrip) {
						filterDisallowedFareType(tripInfo, fareTypes);
					}

					if (!isRemoveTrip && airFilterConfig != null
							&& BooleanUtils.isFalse((airFilterConfig.isSpecialReturnAllowed()))) {
						filterDisallowedFareType(tripInfo, Arrays.asList(FareType.SPECIAL_RETURN.getName()));
					}

					/**
					 * @implSpec : <br>
					 *           After Filtering DisAllowed FareTypes , there is chance of Only Skeleton of Trip is
					 *           Present but no price Object.
					 * 
					 *           if(isRemoveTrip || !tripInfo.isPriceInfosNotEmpty()) remore trip;
					 * 
					 */
					if (isRemoveTrip || !tripInfo.isPriceInfosNotEmpty()) {
						iter.remove();
					}

				}
			});
		}
	}


	private static AirFilterConfiguration getAirFilterConfig(SupplierConfiguration supplierConfiguration) {
		User user = SystemContextHolder.getContextData().getUser();
		FlightBasicFact flightfact = FlightBasicFact.builder().build();
		
		SupplierBasicInfo basicInfo = supplierConfiguration.getBasicInfo();
		if (basicInfo != null) {
			flightfact.setSourceId(basicInfo.getSourceId());
			flightfact.setSupplierId(basicInfo.getSupplierId());
		}
		flightfact.setChannelType(SystemContextHolder.getContextData().getHttpHeaders().getChannelType());
		AirFilterConfiguration airFilterConfig = AirConfiguratorHelper
				.getAirConfigRule(BaseUtils.createFactOnUser(flightfact, user), AirConfiguratorRuleType.FILTER);
		return airFilterConfig;
	}


	private static List<String> getDisallowedFareTypes(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfiguration) {
		List<String> fareTypes = new ArrayList<>();
		if (MapUtils.isNotEmpty(SystemContextHolder.getContextData().getValueMap())) {
			AirSourceConfigurationOutput airSourceOutput = (AirSourceConfigurationOutput) SystemContextHolder
					.getContextData().getValueMap().get(supplierConfiguration.getSupplierId());
			if ((airSourceOutput != null) && CollectionUtils.isNotEmpty(airSourceOutput.getDisAllowedFareTypes())) {
				fareTypes.addAll(airSourceOutput.getDisAllowedFareTypes());
			}
		}
		return fareTypes;
	}

	private static List<String> getAllowedAirlines(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfiguration) {
		List<String> airlines = new ArrayList<>();
		ContextData contextData = SystemContextHolder.getContextData();
		if (supplierConfiguration != null && supplierConfiguration.getSupplierAdditionalInfo() != null
				&& CollectionUtils.isNotEmpty(supplierConfiguration.getSupplierAdditionalInfo().getAllowedAirlines())) {
			airlines.addAll(supplierConfiguration.getSupplierAdditionalInfo().getAllowedAirlines());
		} else {
			AirSourceConfigurationOutput airSourceOutput =
					(AirSourceConfigurationOutput) contextData.getValueMap().get(supplierConfiguration.getSupplierId());
			if ((airSourceOutput != null) && CollectionUtils.isNotEmpty(airSourceOutput.getAllowedAirlines())) {
				airlines.addAll(airSourceOutput.getAllowedAirlines());
			}

		}
		return airlines;
	}

	private static List<String> getExcludeAirlines(AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfiguration) {
		List<String> airlines = new ArrayList<>();
		ContextData contextData = SystemContextHolder.getContextData();
		if (supplierConfiguration != null && supplierConfiguration.getSupplierAdditionalInfo() != null
				&& CollectionUtils
						.isNotEmpty(supplierConfiguration.getSupplierAdditionalInfo().getExcludedAirlines())) {
			airlines.addAll(supplierConfiguration.getSupplierAdditionalInfo().getExcludedAirlines());
		} else {
			AirSourceConfigurationOutput airSourceOutput =
					(AirSourceConfigurationOutput) contextData.getValueMap().get(supplierConfiguration.getSupplierId());
			if ((airSourceOutput != null) && CollectionUtils.isNotEmpty(airSourceOutput.getExcludedAirlines())) {
				airlines.addAll(airSourceOutput.getExcludedAirlines());
			}
		}
		return airlines;

	}

	/**
	 * This method will check whether the trip is valid, as per the allowed airline List
	 * 
	 * @param tripInfo
	 * @param allowedAirlines
	 * @param isOperatingAirline
	 * @return
	 */

	private static boolean isValidTripInfo(TripInfo tripInfo, List<String> allowedAirlines,
			boolean isOperatingAirline) {
		boolean isValid = true;
		if (CollectionUtils.isNotEmpty(allowedAirlines)) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (!allowedAirlines.contains(segmentInfo.getPlatingCarrier(null))) {
					isValid = false;
					break;
				}
			}
		}
		return isValid;
	}

	private static void filterDisallowedFareType(TripInfo tripInfo, List<String> fareTypes) {
		if (CollectionUtils.isNotEmpty(fareTypes)) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (Iterator<PriceInfo> iter = segmentInfo.getPriceInfoList().iterator(); iter.hasNext();) {
					PriceInfo pInfo = iter.next();
					boolean isRemovePriceInfo = false;
					if (fareTypes.contains(pInfo.getFareType().toUpperCase())) {
						isRemovePriceInfo = true;
					}
					if (isRemovePriceInfo) {
						iter.remove();
					}
				}
			}
		}
	}

	public static boolean removeRedundantAndCheckMergeApplicable(TripInfo oldTripInfo, TripInfo newTripInfo) {
		boolean isMergeable = false;
		// key is fareIdentifier : PUBLISHED, SME,FLEXI,CORP
		Set<String> onwardFareTypes = TripPriceEngine.getFareIdentifier(oldTripInfo);
		Set<String> returnFareTypes = TripPriceEngine.getFareIdentifier(newTripInfo);
		TripPriceEngine.filterBasedOnFareIdentifier(oldTripInfo, returnFareTypes);
		TripPriceEngine.filterBasedOnFareIdentifier(newTripInfo, onwardFareTypes);
		if (CollectionUtils.isNotEmpty(oldTripInfo.getSegmentInfos().get(0).getPriceInfoList())
				&& CollectionUtils.isNotEmpty(newTripInfo.getSegmentInfos().get(0).getPriceInfoList())) {
			isMergeable = true;
		}
		return isMergeable;
	}

	public static void sortPriceOnFareTypeOrdinal(TripInfo tripInfo) {
		if (tripInfo != null && CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> priceInfos = segmentInfo.getPriceInfoList();
				if (CollectionUtils.isNotEmpty(priceInfos)) {
					Collections.sort(priceInfos, new Comparator<PriceInfo>() {
						@Override
						public int compare(PriceInfo o1, PriceInfo o2) {
							if (o1.getFareIdentifier() != null && o2.getFareIdentifier() != null) {
								return Integer.compare(o1.getFareIdentifier().ordinal(),
										o2.getFareIdentifier().ordinal());
							}
							return 0;
						}
					});
				}
				segmentInfo.setPriceInfoList(priceInfos);
			});
		}
	}

	public static void filterInValidPriceInfoFromTrip(List<SegmentInfo> segmentInfos, AirSearchQuery searchQuery) {

		for (SegmentInfo segmentInfo : segmentInfos) {
			for (Iterator<PriceInfo> iter = segmentInfo.getPriceInfoList().iterator(); iter.hasNext();) {
				PriceInfo priceInfo = iter.next();
				if (priceInfo.getCabinClass(PaxType.ADULT) == null
						|| !searchQuery.getCabinClass().isAllowedCabinClass(priceInfo.getCabinClass(PaxType.ADULT))) {
					iter.remove();
				}
			}
		}
	}

}
