package com.tgs.services.fms.mapper;


import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.analytics.AirCancellationAnalyticsQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.fms.utils.AirCancelUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirCancellationAnalyticsMapper extends Mapper<AirCancellationAnalyticsQuery> {

	private String bookingId;

	private String amendmentId;

	private AirCancellationDetail bookingCancellationDetail;

	private List<SegmentInfo> segmentInfos;

	private PriceInfo cancellationPriceInfo;

	private Double totalAmountToRefund;

	@Override
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : AirCancellationAnalyticsQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().build().setOutput(output).convert();
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(segmentInfos);
		TripInfoToAnalyticsTripMapper.builder().tripInfos(Arrays.asList(tripInfo)).bookingId(bookingId).build()
				.setOutput(output).convert();
		output.setRouteinfo(AirAnalyticsUtils.getSerializedRouteInfoFromSegmentList(segmentInfos));
		output.setSelectedsegments(CollectionUtils.size(segmentInfos));

		Map<String, MessageInfo> cancellationNotAllowedSegments = new LinkedHashMap<>();

		if (StringUtils.isNotBlank(amendmentId)) {
			output.setAmendmentid(amendmentId);
		}

		if (bookingCancellationDetail != null
				&& bookingCancellationDetail.getCancellationNotAllowedSegments() != null) {
			bookingCancellationDetail.getCancellationNotAllowedSegments().forEach((k, v) -> {
				cancellationNotAllowedSegments.put(k, new GsonMapper<>(v, MessageInfo.class).convert());
			});
			output.setNotallowedsegments((new com.google.gson.Gson().toJson(cancellationNotAllowedSegments)));
		}


		if (bookingCancellationDetail != null) {
			output.setAutocancellation(BooleanUtils.isTrue(bookingCancellationDetail.getAutoCancellationAllowed()));
		}

		if (cancellationPriceInfo != null) {
			output.setPriceinfo(new com.google.gson.Gson()
					.toJson(new GsonMapper<>(cancellationPriceInfo, PriceInfo.class).convert()));
		}

		if (totalAmountToRefund != null) {
			output.setTotalrefundamount(totalAmountToRefund);
		}

		if (bookingCancellationDetail != null) {
			output.setAirlinecancellationfee(bookingCancellationDetail.getTotalCancellationFare());
		}

		if (StringUtils.isNotEmpty(bookingCancellationDetail.getErrorMessage())) {
			output.setErrormsg(bookingCancellationDetail.getErrorMessage());
		}

	}

}
