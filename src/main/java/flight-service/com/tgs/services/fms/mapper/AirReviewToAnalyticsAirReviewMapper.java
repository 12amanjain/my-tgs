package com.tgs.services.fms.mapper;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirReviewToAnalyticsAirReviewMapper extends Mapper<AnalyticsAirQuery> {

	private User user;
	private ContextData contextData;
	private String bookingId;
	private List<TripInfo> tripInfos;
	private AirSearchQuery searchQuery;
	private List<Alert> alerts;
	private List<String> errorMessages;
	private Double totalbookingfare;
	private Double farediff;
	private String bookedfaretype;

	@Override
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : AnalyticsAirQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output = AnalyticsAirQueryMapper.builder().searchQuery(searchQuery).build().setOutput(output).convert();

		output.setBookingid(bookingId);
		output.setTotalbookingfare(totalbookingfare);
		output.setBookedfaretype(bookedfaretype);
		output.setSearchCabinClass(AirAnalyticsUtils.getCabinClass(searchQuery));
		output.setFarediff(farediff);

		if (CollectionUtils.isNotEmpty(tripInfos)) {
			List<SegmentInfo> segmentInfos = AirUtils.getSegmentInfos(tripInfos);
			output.setAirlines(AirAnalyticsUtils.getAirlines(segmentInfos));
			output.setBookingclass(AirAnalyticsUtils.getBookingClass(segmentInfos));
			output.setFarebasis(AirAnalyticsUtils.getFareBasis(segmentInfos));
			output.setFaretype(AirAnalyticsUtils.getFareType(segmentInfos));
			output.setFlightnumbers(AirAnalyticsUtils.getFlightNumbers(segmentInfos));
			output.setCabinclass(AirAnalyticsUtils.getCabinClass(segmentInfos));
			output.setSourcename(AirAnalyticsUtils.getSourceName(segmentInfos));
			output.setSuppliername(AirAnalyticsUtils.getSupplierName(segmentInfos));
			output.setSupplierdesc(AirAnalyticsUtils.getSupplierDesc(segmentInfos));
			output.setTotalairlinefare(AirAnalyticsUtils.getTotalAirlineFare(segmentInfos));
			output.setCommission(AirAnalyticsUtils.getTotalCommission(segmentInfos));
			output.setCommissionid(AirAnalyticsUtils.getCommissionId(segmentInfos));
			output.setRefundabletype(AirAnalyticsUtils.getRefundableType(segmentInfos));
			output.setSuppliersessionid(AirAnalyticsUtils.getSupplierSession(bookingId, user));
		}

		if (CollectionUtils.isNotEmpty(alerts)) {
			output.setAlerts(getAlerts());
		}

		if (CollectionUtils.isNotEmpty(errorMessages)) {
			output.setErrormsg(errorMessages.toString());
		}
	}

	private String getAlerts() {
		List<String> alert = new ArrayList<>();
		alerts.forEach(alert1 -> {
			alert.add(alert1.getType());
		});
		return alert.toString();
	}
}
