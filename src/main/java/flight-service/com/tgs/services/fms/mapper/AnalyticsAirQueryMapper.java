package com.tgs.services.fms.mapper;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AnalyticsAirQueryMapper extends Mapper<AnalyticsAirQuery> {

	private AirSearchQuery searchQuery;
	private SupplierBasicInfo basicInfo;
	private User user;
	private ContextData contextData;
	private AirSearchResult searchResult;
	private long threadTime;
	private List<String> errorMessages;

	@Override
	protected void execute() throws CustomGeneralException {

		output = output != null ? output : AnalyticsAirQuery.builder().build();
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();

		if (searchQuery != null) {
			output.setSource(searchQuery.getRouteInfos().get(0).getFromCityAirportCode());
			output.setDestination(searchQuery.getRouteInfos().get(0).getToCityAirportCode());
			output.setRequestid(searchQuery.getRequestId());
			output.setRouteinfo(AirAnalyticsUtils.getSerializedRouteInfo(searchQuery));
			output.setOnwarddate(searchQuery.getRouteInfos().get(0).getTravelDate().toString());
			output.setOnwarddaydiff(AirAnalyticsUtils.getDayDiff(searchQuery.getRouteInfos().get(0).getTravelDate()));

			if (searchQuery.getRouteInfos().size() > 1) {
				output.setReturndate(searchQuery.getRouteInfos().get(1).getTravelDate().toString());
				output.setReturndaydiff(
						AirAnalyticsUtils.getDayDiff(searchQuery.getRouteInfos().get(1).getTravelDate()));
			}

			output.setSearchtype(searchQuery.getOrigSearchType().name());
			output.setSearchid(searchQuery.getSearchId());
			output.setCabinclass(AirAnalyticsUtils.getCabinClass(searchQuery));

			output.setAirtype(searchQuery.getAirType().name());
			output.setAdults(searchQuery.getPaxInfo().getOrDefault(PaxType.ADULT, 0));
			output.setChildren(searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0));
			output.setInfants(searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0));
			output.setAirlines(AirAnalyticsUtils.getAirlines(searchQuery));
			output.setSourcename(getSourceName());
			output.setSuppliername(getSupplierName());
			output.setSupplierdesc(getSupplierDesc());
		}
		output.setSearchresultcount(AirAnalyticsUtils.getSearchResultCount(searchResult, searchQuery));
		if (output.getSearchtimeinsec() == null || output.getSearchtimeinsec() == 0) {
			output.setSearchtimeinsec(Long.valueOf(threadTime).intValue());
		}
		output.setErrormsg(getErrorMessages());

	}

	private String getSourceName() {
		if (basicInfo != null) {
			return AirSourceType.getAirSourceType(basicInfo.getSourceId()).name();
		}
		return null;
	}

	private String getSupplierName() {
		if (basicInfo != null) {
			return basicInfo.getSupplierId();
		}
		return null;
	}

	private String getSupplierDesc() {
		if (basicInfo != null) {
			return basicInfo.getDescription();
		}
		return null;
	}

	private String getErrorMessages() {
		if (CollectionUtils.isNotEmpty(errorMessages)) {
			return errorMessages.toString();
		}
		return null;
	}

}
