package com.tgs.services.fms.mapper;

import java.util.Map;
import org.apache.commons.lang.BooleanUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.ProcessedPriceInfo;
import com.tgs.services.fms.utils.AirUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class PriceInfoToProcessedPriceInfo extends Mapper<ProcessedPriceInfo> {

	private PriceInfo priceInfo;

	private Map<PaxType, Integer> paxInfo;

	@Override
	protected void execute() throws CustomGeneralException {

		output = new ProcessedPriceInfo();
		FareDetail fareDetail = priceInfo.getFareDetail(PaxType.ADULT);
		output.setSupplierBasicInfo(priceInfo.getSupplierBasicInfo());
		output.setId(priceInfo.getId());
		output.setTotalPrice(priceInfo.getTotalFare(paxInfo, FareComponent.TF));
		output.setSeatLeft(fareDetail.getSeatRemaining());
		output.setRefundableType(fareDetail.getRefundableType());
		output.setCabinClass(fareDetail.getCabinClass() != null ? fareDetail.getCabinClass() : CabinClass.ECONOMY);
		output.setId(priceInfo.getId());
		output.setCommission(priceInfo.getTotalFare(paxInfo, FareComponent.NCM));
		output.setTds(Math
				.abs(AirUtils.getTotalMappedComponentAmount(paxInfo, priceInfo, FareComponent.NCM, FareComponent.TDS)));
		output.setMarkup(priceInfo.getTotalFare(paxInfo, FareComponent.MU));
		output.setNetPrice(getNetFare());
		output.setMealIncluded(BooleanUtils.isTrue(priceInfo.getFareDetail(PaxType.ADULT).getIsMealIncluded()));
		output.setFareType(priceInfo.getFareType());
		output.setSpecialReturnIdentifier(priceInfo.getSpecialReturnIdentifier());
		output.setMatchedSpecialReturnIdentifier(priceInfo.getMatchedSpecialReturnIdentifier());
		output.setMiscInfo(priceInfo.getMiscInfo());
		output.setIsHandBaggage(fareDetail.getIsHandBaggage());
		output.setMixCabinClass(priceInfo.getMixCabinClass());

	}

	public Double getNetFare() {
		return priceInfo.getTotalFare(paxInfo, FareComponent.TF) - output.getCommission()
				- priceInfo.getTotalFare(paxInfo, FareComponent.DS) - priceInfo.getTotalFare(paxInfo, FareComponent.MU);
	}
}
