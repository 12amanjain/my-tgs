package com.tgs.services.fms.mapper;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import java.util.ArrayList;
import java.util.List;

@Builder
@Getter
@Setter
public class SegmentPriceInfoToTripPriceInfo extends Mapper<List<PriceInfo>> {

	TripInfo tripInfo;

	User user;

	@Override
	protected void execute() throws CustomGeneralException {

		if (output == null) {
			output = new ArrayList<PriceInfo>();
		}

		List<PriceInfo> tripPriceInfo = new ArrayList<>();

		/**
		 * Creating copy of PriceInfo list so that it won't impact anything on segmentInfo priceList
		 */
		tripPriceInfo = GsonUtils.getGson().fromJson(
				GsonUtils.getGson().toJson(tripInfo.getSegmentInfos().get(0).getPriceInfoList()),
				new TypeToken<List<PriceInfo>>() {}.getType());
		/**
		 * Merging segment wise pricing into total pricing because on UI we can't display segment wise pricing
		 */


		for (int i = 1; i < tripInfo.getSegmentInfos().size(); i++) {
			SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(i);
			for (int j = 0; j < segmentInfo.getPriceInfoList().size(); j++) {

				for (PaxType paxType : segmentInfo.getPriceInfo(j).getFareDetails().keySet()) {

					FareDetail fareDetail = segmentInfo.getPriceInfo(j).getFareDetails().get(paxType);

					for (FareComponent fareComponent : fareDetail.getFareComponents().keySet()) {
						double totalPrice = tripPriceInfo.get(j).getFareDetail(paxType).getFareComponents()
								.getOrDefault(fareComponent, 0.0);
						double segmentPrice = fareDetail.getFareComponents().getOrDefault(fareComponent, 0.0);
						totalPrice += segmentPrice;
						tripPriceInfo.get(j).getFareDetail(paxType).getFareComponents().put(fareComponent, totalPrice);
					}

					if (tripPriceInfo.get(j).getFareDetail(paxType).getCabinClass() != fareDetail.getCabinClass()) {
						tripPriceInfo.get(j).setMixCabinClass(true);
					}

					if (ObjectUtils.defaultIfNull(segmentInfo.getPriceInfo(j).getFareDetail(paxType).getSeatRemaining(),
							0) < ObjectUtils
									.defaultIfNull(tripPriceInfo.get(j).getFareDetail(paxType).getSeatRemaining(), 0)) {
						tripPriceInfo.get(j).getFareDetail(paxType).setSeatRemaining(
								segmentInfo.getPriceInfo(j).getFareDetail(paxType).getSeatRemaining());
					}

					if (BooleanUtils.isTrue(segmentInfo.getPriceInfo(j).getFareDetail(paxType).getIsHandBaggage())) {
						tripPriceInfo.get(j).getFareDetail(paxType).setIsHandBaggage(true);
					}
				}
			}
		}
		output = tripPriceInfo;

	}
}
