package com.tgs.services.fms.mapper;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.analytics.BaseAnalyticsQueryMapper;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class TripInfoToAnalyticsTripMapper extends Mapper<AnalyticsAirQuery> {

	private User user;
	private ContextData contextData;

	private String bookingId;
	private List<TripInfo> tripInfos;
	private AirSearchQuery searchQuery;
	private List<String> errorMessages;
	private Integer attempt;

	@Override
	protected void execute() throws CustomGeneralException {

		if (output == null) {
			output = AnalyticsAirQuery.builder().build();
		}

		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		AirReviewToAnalyticsAirReviewMapper.builder().searchQuery(searchQuery).tripInfos(tripInfos).bookingId(bookingId)
				.build().setOutput(output).convert();
		if (CollectionUtils.isNotEmpty(errorMessages))
			output.setErrormsg(errorMessages.toString());
		output.setAttempt(attempt);
		output.setBaggages(AirAnalyticsUtils.getBaggages(AirUtils.getSegmentInfos(tripInfos)));
		output.setMeals(AirAnalyticsUtils.getMeal(AirUtils.getSegmentInfos(tripInfos)));
		output.setSeats(AirAnalyticsUtils.getSeat(AirUtils.getSegmentInfos(tripInfos)));
		output.setAlternateclass(AirAnalyticsUtils.getAlternateClass(AirUtils.getSegmentInfos(tripInfos)));

	}
}
