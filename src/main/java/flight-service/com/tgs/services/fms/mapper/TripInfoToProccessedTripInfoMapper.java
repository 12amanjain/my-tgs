package com.tgs.services.fms.mapper;

import java.util.ArrayList;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.ProcessedTripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class TripInfoToProccessedTripInfoMapper extends Mapper<ProcessedTripInfo> {

	private TripInfo tripInfo;

	@Override
	protected void execute() throws CustomGeneralException {

		output = new ProcessedTripInfo();
		output.setDepartureTime(tripInfo.getDepartureTime());
		output.setFinalArrivalTime(tripInfo.getArrivalTime());
		output.setDuration(tripInfo.getTripDuration());
		int daysDiff = tripInfo.daysDiff();
		if (daysDiff > 0) {
			output.setDayDiff(daysDiff);
		}
		int stops = -1;
		AirlineInfo airlineInfo = tripInfo.getSegmentInfos().get(0).getFlightDesignator().getAirlineInfo();

		output.setAirlineInfos(new ArrayList<AirlineInfo>());
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			output.getFlightNumbers().add(segmentInfo.getFlightDesignator().getAirlineInfo().getCode() + "-"
					+ segmentInfo.getFlightDesignator().getFlightNumber().trim());
			output.getSourceIds().add(segmentInfo.getSupplierInfo().getSourceId());
			stops += segmentInfo.getStopOverAirports() != null ? segmentInfo.getStopOverAirports().size() : 0;
			output.getAirlineInfos().add(segmentInfo.getFlightDesignator().getAirlineInfo());
			if (!airlineInfo.getCode().equals(segmentInfo.getFlightDesignator().getAirlineInfo().getCode())) {
				output.setIsMultiAirline(true);
			}
			stops++;
		}
		output.setDepartureAirport(tripInfo.getSegmentInfos().get(0).getDepartAirportInfo().getCode());
		output.setArrivalAirport(tripInfo.getSegmentInfos().get(tripInfo.getSegmentInfos().size() - 1)
				.getArrivalAirportInfo().getCode());
		if (BooleanUtils.isNotTrue(output.getIsMultiAirline())) {
			output.setAirlineInfo(airlineInfo);
			output.setAirlineInfos(null);
		}
		output.setStops(stops);
	}
}
