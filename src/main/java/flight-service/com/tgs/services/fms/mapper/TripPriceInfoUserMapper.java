package com.tgs.services.fms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.MapUtils;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

@Builder
@Getter
@Setter
public class TripPriceInfoUserMapper extends Mapper<Void> {


	TripInfo tripInfo;
	User user;
	FareBreakUpConfigOutput configOutput;


	@Override
	protected void execute() throws CustomGeneralException {

		SegmentPriceInfoToTripPriceInfo toTripPriceInfo =
				SegmentPriceInfoToTripPriceInfo.builder().tripInfo(tripInfo).user(user).build();

		List<PriceInfo> tripPriceInfo = toTripPriceInfo.convert();

		String endPoint = SystemContextHolder.getContextData().getRequestContextHolder().getEndPoint();

		/**
		 * Merging Tax Breakup into its corresponding map component
		 */
		for (PriceInfo priceInfo : tripPriceInfo) {
			Iterator<Map.Entry<PaxType, FareDetail>> iter = priceInfo.getFareDetails().entrySet().iterator();
			while (iter.hasNext()) {
				Map.Entry<PaxType, FareDetail> entry = iter.next();
				Map<FareComponent, Double> tempMap = new HashMap<>();
				Iterator<Map.Entry<FareComponent, Double>> fareComponentIter =
						entry.getValue().getFareComponents().entrySet().iterator();
				double totalMarkup = 0;

				while (fareComponentIter.hasNext()) {
					FareComponent fareComponent = fareComponentIter.next().getKey();
					if (fareComponent.equals(FareComponent.MU)) {
						totalMarkup += entry.getValue().getFareComponents().getOrDefault(fareComponent, 0.0d);
					}
					FareComponent mapComponent = fareComponent.mapComponent(configOutput, endPoint);
					if (entry.getValue().getFareComponents().get(fareComponent) != null && mapComponent != null) {
						double value = fareComponent.getAmount(entry.getValue().getFareComponents().get(fareComponent));
						double mapComponentValue = tempMap.getOrDefault(mapComponent, 0.0);
						tempMap.put(mapComponent, value + mapComponentValue);
						if (fareComponent.keepCurrentComponent()) {
							entry.getValue().getAddlFareComponents(mapComponent).put(fareComponent, value);
						} else {
							entry.getValue().getAddlFareComponents(mapComponent).put(FareComponent.OT, entry.getValue()
									.getAddlFareComponents(mapComponent).getOrDefault(FareComponent.OT, 0.0) + value);
						}
					} else {
						tempMap.put(fareComponent, entry.getValue().getFareComponents().get(fareComponent));
					}
				}
				if (MapUtils.isNotEmpty(tempMap)) {
					Double netFare =
							tempMap.getOrDefault(FareComponent.TF, 0.0) - tempMap.getOrDefault(FareComponent.DS, 0.0)
									- tempMap.getOrDefault(FareComponent.NCM, 0.0);
					tempMap.put(FareComponent.NF, netFare);
					tempMap.put(FareComponent.TF, tempMap.getOrDefault(FareComponent.TF, 0.0) + totalMarkup);
					entry.getValue().setFareComponents(tempMap);
				}
			}
		}
		tripInfo.setTripPriceInfos(tripPriceInfo);
	}
}
