package com.tgs.services.fms.restcontroller;

import com.tgs.services.fms.servicehandler.AirCancellationFareHandler;
import com.tgs.services.fms.servicehandler.AirCancellationHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@Slf4j
@RestController
@RequestMapping("/fms/v1/cancel")
public class AirCancellationController {

	@Autowired
	AirCancellationFareHandler fareReviewHandler;

	@Autowired
	AirCancellationHandler cancellationHandler;


	/*
	 * @RequestMapping(value = "/review", method = RequestMethod.POST) protected AirCancellationResponse
	 * reviewTrip(HttpServletRequest request, HttpServletResponse response,
	 * 
	 * @Valid @RequestBody AirCancellationRequest cancellationRequest) throws Exception {
	 * SystemContextHolder.getContextData().getReqIds().add(cancellationRequest.getBookingId());
	 * SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
	 * fareReviewHandler.initData(cancellationRequest, new AirCancellationResponse()); return
	 * fareReviewHandler.getResponse(); }
	 * 
	 * @RequestMapping(value = "/order", method = RequestMethod.POST) protected AirCancellationResponse
	 * cancelTrip(HttpServletRequest request, HttpServletResponse response,
	 * 
	 * @Valid @RequestBody AirCancellationRequest cancellationRequest) throws Exception {
	 * SystemContextHolder.getContextData().getReqIds().add(cancellationRequest.getBookingId());
	 * SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
	 * cancellationHandler.initData(cancellationRequest, new AirCancellationResponse()); return
	 * cancellationHandler.getResponse(); }
	 */

}
