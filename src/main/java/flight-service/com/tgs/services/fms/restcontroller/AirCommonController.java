package com.tgs.services.fms.restcontroller;

import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.services.base.fms.restmodel.AirlineInfoResponse;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.restmodel.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.jparepository.AirportInfoService;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.servicehandler.AirlineInfoHandler;
import com.tgs.services.fms.servicehandler.AirportInfoHandler;
import com.tgs.services.fms.servicehandler.PriceDetailsHandler;
import com.tgs.services.fms.servicehandler.SourceRouteHandler;
import com.tgs.services.fms.servicehandler.SourceRouteInfoHandler;


@RestController
@RequestMapping("/fms/v1")
public class AirCommonController {

	@Autowired
	private PriceDetailsHandler priceDetailHandler;

	@Autowired
	AirportInfoHandler airportHandler;

	@Autowired
	AirportInfoService airportService;

	@Autowired
	SourceRouteInfoHandler sourceRouteInfoHandler;

	@Autowired
	SourceRouteInfoService routeInfoService;

	@Autowired
	AirlineInfoHandler airlineInfoHandler;

	@Autowired
	SourceRouteHandler sourceRouteHandler;


	@RequestMapping(value = "/fetch-tripinfo", method = RequestMethod.POST)
	protected PriceDetailsResponse fetchPriceDetails(HttpServletRequest request, HttpServletResponse response,
			@RequestBody PriceDetailsRequest priceDetailRequest) throws Exception {
		priceDetailHandler.initData(priceDetailRequest, new PriceDetailsResponse());
		return priceDetailHandler.getResponse();
	}


	@RequestMapping(value = "/airports/save", method = RequestMethod.POST)
	protected AirportInfoResponse saveAirports(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirportInfo airportInfo) throws Exception {
		airportHandler.initData(airportInfo, new AirportInfoResponse());
		return airportHandler.getResponse();
	}

	@RequestMapping(value = "/airports/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteAirport(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		com.tgs.services.fms.dbmodel.AirportInfo dbAirportInfo = airportService.findById(id);

		if (Objects.nonNull(dbAirportInfo)) {
			airportService.deleteById(dbAirportInfo.getId());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	@RequestMapping(value = "/airlines/save", method = RequestMethod.POST)
	protected AirlineInfoResponse saveAirlines(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirlineInfo airlineInfo) throws Exception {
		airlineInfoHandler.initData(airlineInfo, new AirlineInfoResponse());
		return airlineInfoHandler.getResponse();
	}

	@RequestMapping(value = "/source-route/save", method = RequestMethod.POST)
	protected SourceRouteInfoResponse saveSourceRouteInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SourceRouteInfo sourceRouteInfo) throws Exception {
		sourceRouteInfoHandler.initData(sourceRouteInfo, new SourceRouteInfoResponse());
		return sourceRouteInfoHandler.getResponse();
	}


	@RequestMapping(value = "/source-route/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteSourceRoute(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		DbSourceRouteInfo dbSourceRouteInfo = routeInfoService.findOne(id);
		if (Objects.nonNull(dbSourceRouteInfo)) {
			routeInfoService.deleteById(dbSourceRouteInfo.getId());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}


	@RequestMapping(value = "/add/newroutes", method = RequestMethod.POST)
	protected BaseResponse addNewRoutes(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SourceRouteRequest sourceRouteRequest) throws Exception {
		sourceRouteHandler.initData(sourceRouteRequest, new BaseResponse());
		return sourceRouteHandler.getResponse();
	}


}
