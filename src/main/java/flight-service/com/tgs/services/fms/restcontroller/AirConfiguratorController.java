package com.tgs.services.fms.restcontroller;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorFilter;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;
import com.tgs.services.fms.jparepository.AirConfiguratorService;
import com.tgs.services.fms.restmodel.AirConfigRuleTypeResponse;
import com.tgs.services.fms.servicehandler.AirConfiguratorHandler;
import com.tgs.services.fms.validator.AirConfiguratorRuleValidator;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("fms/v1/config")
public class AirConfiguratorController {

	@Autowired
	private AirConfiguratorHandler airConfigHandler;

	@Autowired
	private AirConfiguratorService airConfigService;

	@Autowired
	private AuditsHandler auditHandler;

	@Autowired
	private AirConfiguratorRuleValidator airConfigValidator;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(airConfigValidator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected AirConfigRuleTypeResponse saveOrUpdateAirConfigRule(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody AirConfiguratorInfo airConfiguratorInfo)
			throws Exception {
		airConfigHandler.initData(airConfiguratorInfo, new AirConfigRuleTypeResponse());
		return airConfigHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected AirConfigRuleTypeResponse getAirConfigRuleType(HttpServletRequest request,
			HttpServletResponse httpResoponse, @RequestBody AirConfiguratorFilter configuratorFilter) throws Exception {
		AirConfigRuleTypeResponse ruleTypeResponse = new AirConfigRuleTypeResponse();
		airConfigService.findAll(configuratorFilter).forEach(airConfig -> {
			ruleTypeResponse.getAirConfiguratorInfos()
					.add(new GsonMapper<>(airConfig, AirConfiguratorInfo.class).convert());
		});
		return ruleTypeResponse;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse deleteConfigRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		DbAirConfiguratorRule airConfiguratorRule = airConfigService.findById(id);
		if (Objects.nonNull(airConfiguratorRule)) {
			airConfiguratorRule.setDeleted(Boolean.TRUE);
			airConfiguratorRule.setEnabled(Boolean.FALSE);
			airConfigService.save(airConfiguratorRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	@RequestMapping(value = "/airconfig-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbAirConfiguratorRule.class, ""));
		return auditResponse;
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateConfigStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return airConfigHandler.updateConfigStatus(id, status);
	}

}
