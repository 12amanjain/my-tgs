package com.tgs.services.fms.restcontroller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.tgs.services.base.SpringContext;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import com.tgs.services.fms.restmodel.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SearchQueryValidator;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.gson.RestExcludeStrategy;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.LowFareInfo;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.jparepository.AirlineInfoService;
import com.tgs.services.fms.jparepository.AirportInfoService;
import com.tgs.services.fms.manager.AirApiSearchManager;
import com.tgs.services.fms.manager.AirSearchManager;
import com.tgs.services.fms.servicehandler.AirExternalServiceHandler;
import com.tgs.services.fms.servicehandler.AirSearchHandler;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/fms/v1")
@RestController
@Slf4j
public class AirSearchController {

	@Autowired
	AirportInfoService airportInfoService;

	@Autowired
	AirSearchHandler searchHandler;

	@Autowired
	AirlineInfoService airlineInfoService;

	@Autowired
	SearchQueryValidator validator;

	@Autowired
	FMSCachingServiceCommunicator cachingService;

	@Autowired
	AirSearchManager searchManager;

	@Autowired
	AirExternalServiceHandler externalServiceHandler;

	@Autowired
	AirApiSearchManager airApiSearchManager;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@RequestMapping(value = "/air-searchquery-list", method = RequestMethod.POST)
	protected AirSearchQueryListResponse getSearchList(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirSearchRequest searchRequest) throws Exception {
		LogUtils.log("searchcontroller", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"system#filter");
		AirSearchQueryListResponse searchQueryListResponse = new AirSearchQueryListResponse();
		AirUtils.populateMissingParametersInAirSearchQuery(searchRequest.getSearchQuery());
		log.debug("Got search request for searchQuery {}", searchRequest.getSearchQuery());
		ContextData contextData = SystemContextHolder.getContextData();
		contextData.getExclusionStrategys().add(new RestExcludeStrategy());
		searchRequest.getSearchQuery().setRequestId(RandomStringUtils.random(10, false, true));
		for (AirSearchQuery query : searchManager.getSearchQueryList(searchRequest.getSearchQuery(), contextData)) {
			if (BooleanUtils.isNotTrue(searchRequest.getIsNewFlow())) {
				log.debug("Storing flight cache list in cache with key {} ", query.getSearchId());
				cachingService.store(AirSearchManager.getSearchQueryStatusString(query.getSearchId()),
						BinName.STOREAT.getName(), LocalDateTime.now().toString(),
						CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.name()).compress(true).plainData(false)
								.expiration(CacheType.SEARCHIDWAITTIME.getTtl(contextData.getUser())).build());
				log.debug("Storage successful for key {} ", query.getSearchId());
				ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager.doSearch(query, contextData));
			} else {
				cachingService.store(query.getSearchId() + "_NewFlow", BinName.SEARCHQUERYBIN.getName(), query,
						CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.name()).compress(true).plainData(false)
								.expiration(100).build());
			}
			searchQueryListResponse.addSearchId(query.getSearchId());
		}
		searchQueryListResponse.setSearchQuery(searchRequest.getSearchQuery());
		return searchQueryListResponse;
	}

	@RequestMapping(value = "/air-search", method = RequestMethod.POST)
	protected AirSearchResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirSearchRequest searchRequest) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		searchHandler.initData(searchRequest, new AirSearchResponse());
		return searchHandler.getResponse();
	}

	@RequestMapping(value = "/air-search/{searchid}", method = RequestMethod.POST)
	protected AirSearchResponse searchWithSearchId(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirSearchRequest searchRequest) throws Exception {
		ContextData contextData = SystemContextHolder.getContextData();
		AirSearchQuery query = cachingService.fetchValue(searchRequest.getSearchId() + "_NewFlow", AirSearchQuery.class,
				CacheSetName.FLIGHT_SEARCH.name(), BinName.SEARCHQUERYBIN.getName());
		if (query != null) {
			cachingService.store(AirSearchManager.getSearchQueryStatusString(query.getSearchId()),
					BinName.STOREAT.getName(), LocalDateTime.now().toString(),
					CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.name()).compress(true).plainData(false)
							.expiration(CacheType.SEARCHIDWAITTIME.getTtl(contextData.getUser())).build());
			ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager.doSearch(query, contextData));
			cachingService.store(query.getSearchId() + "_NewFlow", BinName.SEARCHQUERYBIN.getName(), query,
					CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.name()).compress(true).plainData(false)
							.expiration(1).build());
		}

		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		searchHandler.initData(searchRequest, new AirSearchResponse());
		return searchHandler.getResponse();
	}

	@RequestMapping(value = "/airports/search/{query}", method = RequestMethod.GET)
	protected AirportSearchResponse getAirportList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("query") String searchQuery) {
		List<AirportInfo> airportInfoList = new ArrayList<>();
		airportInfoService.fetchAirports(searchQuery).forEach(
				airportInfo -> airportInfoList.add(new GsonMapper<>(airportInfo, AirportInfo.class).convert()));
		AirportSearchResponse searchResponse = new AirportSearchResponse();
		searchResponse.setAirportList(airportInfoList);
		return searchResponse;

	}

	@RequestMapping(value = "/airports/search", method = RequestMethod.POST)
	protected AirportSearchResponse getAirportsList(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirportSearchRequest serachRequest) {
		List<AirportInfo> airportInfoList = new ArrayList<>();
		serachRequest.getAirportCodes().forEach(code -> {
			AirportInfo airportInfo = AirportHelper.getAirportInfo(code);
			if(airportInfo!=null)
				airportInfoList.add(airportInfo);
		});
		AirportSearchResponse searchResponse = new AirportSearchResponse();
		searchResponse.setAirportList(airportInfoList);
		return searchResponse;

	}

	@RequestMapping(value = "/airlines/search/{query}", method = RequestMethod.GET)
	protected AirlineSearchResponse getAirlineList(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("query") String searchQuery) {
		List<AirlineInfo> airlineInfoList = new ArrayList<>();
		airlineInfoService.fetchAirlines(searchQuery).forEach(
				airlineInfo -> airlineInfoList.add(new GsonMapper<>(airlineInfo, AirlineInfo.class).convert()));
		AirlineSearchResponse searchResponse = new AirlineSearchResponse();
		searchResponse.setAirlineList(airlineInfoList);
		return searchResponse;
	}

	@RequestMapping(value = "/air-lowfare/search", method = RequestMethod.POST)
	protected LowFareSearchResponse getLowFare(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid LowFareSearchRequest searchRequest) {
		LowFareSearchResponse lowFareResponse = new LowFareSearchResponse();
		LocalDate currentDate = searchRequest.getFromDate();
		String key = searchRequest.getSource() + "-" + searchRequest.getDestination();
		String[] bin =
				new String[(int) ChronoUnit.DAYS.between(searchRequest.getFromDate(), searchRequest.getToDate()) + 1];
		for (int i = 0; currentDate.compareTo(searchRequest.getToDate()) <= 0; i++) {
			bin[i] = currentDate.toString();
			currentDate = currentDate.plusDays(1);
		}
		Map<String, LowFareInfo> lowFareMap =
				cachingService.fetchValue(key, LowFareInfo.class, CacheSetName.LOW_FARE.getName(), bin);
		lowFareResponse.setFareResult(lowFareMap);
		return lowFareResponse;
	}

	@RequestMapping(value = "/{metaInfo}/{source}", method = RequestMethod.GET)
	protected ESAutoSuggestionResponse getSearchResponse(HttpServletRequest request, HttpServletResponse httpResoponse,
			@PathVariable("metaInfo") String metaInfo, @PathVariable("source") String source) {
		ESAutoSuggestionResponse autoSuggestionResponse = new ESAutoSuggestionResponse();
		autoSuggestionResponse = externalServiceHandler.getSuggestions(source, metaInfo);
		return autoSuggestionResponse;
	}

	@RequestMapping(value = "/air-search-all", method = RequestMethod.POST)
	protected AirSearchResponse searchAll(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirSearchRequest searchQueryRequest) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		AirSearchResponse finalSearchResponse = new AirSearchResponse();
		AirSearchQueryListResponse searchListResponse = getSearchList(request, response, searchQueryRequest);
		AirSearchQuery searchQuery = searchListResponse.getSearchQuery();
		List<String> searchIds = searchListResponse.getSearchIds();
		List<Future<AirSearchResponse>> futureTaskList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(searchIds)) {
			for (String searchId : searchIds) {
				AirSearchRequest searchIdRequest = new AirSearchRequest();
				searchIdRequest.setSearchId(searchId);
				AirApiSearchManager searchHandler =
						SpringContext.getApplicationContext().getBean(AirApiSearchManager.class);
				Future<AirSearchResponse> sourceWiseSearchResponse = ExecutorUtils.getFlightSearchThreadPool()
						.submit(() -> searchHandler.fetchSearchResponse(searchIdRequest));
				futureTaskList.add(sourceWiseSearchResponse);
			}
			AirSearchResult searchResult = new AirSearchResult();
			for (int index = 0; index < futureTaskList.size(); index++) {
				AirSearchResponse searchResponse = futureTaskList.get(index).get();
				airApiSearchManager.processSearchResponse(searchQuery, searchResult, searchResponse);
			}
			finalSearchResponse.setSearchResult(searchResult);
		}
		return finalSearchResponse;
	}

}
