package com.tgs.services.fms.restcontroller;

import com.tgs.services.base.gson.RestExcludeStrategy;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassFareSearchRequest;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassRequest;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassResponse;
import com.tgs.services.fms.servicehandler.AlternateClassFareSearchHandler;
import com.tgs.services.fms.servicehandler.AlternateClassSearchHandler;
import com.tgs.services.fms.validator.AlternateClassFareSearchValidator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.InitBinder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Arrays;

@RequestMapping("/fms/v1")
@RestController
@Slf4j
public class AlternateClassController {

	@Autowired
	AlternateClassSearchHandler classSearchHandler;

	@Autowired
	AlternateClassFareSearchHandler fareSearchHandler;

	@Autowired
	AlternateClassFareSearchValidator fareSearchValidator;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(fareSearchValidator);
	}

	@RequestMapping(value = "/change-class", method = RequestMethod.POST)
	protected AlternateClassResponse changeClassSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AlternateClassRequest classSearchRequest) throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(Arrays.asList(new RestExcludeStrategy()));
		classSearchHandler.initData(classSearchRequest, new AlternateClassResponse());
		return classSearchHandler.getResponse();
	}


	@RequestMapping(value = "/class-fare", method = RequestMethod.POST)
	protected AlternateClassResponse classFareSearch(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AlternateClassFareSearchRequest fareSearchRequest) throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(Arrays.asList(new RestExcludeStrategy()));
		fareSearchHandler.initData(fareSearchRequest, new AlternateClassResponse());
		return fareSearchHandler.getResponse();
	}
}
