package com.tgs.services.fms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.fms.datamodel.farerule.FareRuleFilter;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.fms.jparepository.FareRuleService;
import com.tgs.services.fms.restmodel.FareRuleRequest;
import com.tgs.services.fms.restmodel.FareRuleResponse;
import com.tgs.services.fms.restmodel.FareRuleTripResponse;
import com.tgs.services.fms.servicehandler.FareRuleHandler;
import com.tgs.services.fms.validator.FareRuleValidator;

@RestController
@RequestMapping("/fms/v1")
public class FareRuleController {

	@Autowired
	private FareRuleHandler fareRuleHandler;

	@Autowired
	private FareRuleService ruleService;

	@Autowired
	private FareRuleValidator ruleValidator;

	@Autowired
	private AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ruleValidator);
	}

	@RequestMapping(value = "/farerule/save", method = RequestMethod.POST)
	protected FareRuleResponse addFareRule(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid FareRuleInfo fareRuleInfo) throws Exception {
		fareRuleHandler.initData(fareRuleInfo, new FareRuleResponse());
		return fareRuleHandler.getResponse();
	}

	@RequestMapping(value = "/farerule/list", method = RequestMethod.POST)
	protected FareRuleResponse getFareRule(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid FareRuleFilter fareRuleFilter) throws Exception {
		FareRuleResponse ruleResponse = new FareRuleResponse();
		ruleService.findAll(fareRuleFilter).forEach(fareRuleInfo -> {
			ruleResponse.getFareRuleInfos().add(new GsonMapper<>(fareRuleInfo, FareRuleInfo.class).convert());
		});
		return ruleResponse;
	}

	@RequestMapping(value = "/farerule", method = RequestMethod.POST)
	protected FareRuleTripResponse getTripFareRule(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid FareRuleRequest fareRuleRequest) throws Exception {
		return fareRuleHandler.getFareRule(fareRuleRequest);
	}

	@RequestMapping(value = "/farerule/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteFareRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return fareRuleHandler.deleteById(id);
	}

	@RequestMapping(value = "/farerule/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateFareRuleStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return fareRuleHandler.changeStatus(id, status);
	}

	@RequestMapping(value = "/farerule/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchFareRuleAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(
				auditHandler.fetchAudits(auditsRequestData, com.tgs.services.fms.dbmodel.DbFareRuleInfo.class, ""));
		return auditResponse;
	}
}
