package com.tgs.services.fms.restcontroller;

import java.util.Iterator;
import java.util.List;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.AsyncExecutionExceptionHandler;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.base.utils.thread.UnboundedAsynchronousExecutor;
import com.tgs.services.fms.restmodel.UserFeeUpdateRequest;
import com.tgs.services.fms.servicehandler.UserFeeUpdateHandler;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/fms/v1")
public class UserFeeUpdateController {

	@Autowired
	private UserFeeUpdateHandler userFeeUpdateHandler;

	/**
	 * Deprecated. However, still used in UI for updating user fee for one pricing.
	 * 
	 * @param userFeeUpdateRequest
	 * @return
	 * @throws Exception
	 */
	@Deprecated
	@RequestMapping(value = "/update-user-fee", method = RequestMethod.POST)
	protected BaseResponse updateUserFee(@RequestBody UserFeeUpdateRequest userFeeUpdateRequest) throws Exception {
		if (BooleanUtils
				.isTrue(SystemContextHolder.getContextData().getUser().getAdditionalInfo().getDisableMarkup())) {
			throw new CustomGeneralException(SystemError.PERMISSION_DENIED);
		}
		userFeeUpdateHandler.initData(userFeeUpdateRequest, new BaseResponse());
		return userFeeUpdateHandler.getResponse();
	}


	@RequestMapping(value = "/user-fee/update", method = RequestMethod.POST)
	protected BaseResponse updateUserFee(@RequestBody List<UserFeeUpdateRequest> userFeeUpdateRequests)
			throws Exception {
		AsyncExecutionExceptionHandler exceptionHandler = (e, taskIndex) -> log
				.error("Failed to update user fee for {} due to ", userFeeUpdateRequests.get(taskIndex), e);
		UnboundedAsynchronousExecutor executor = new UnboundedAsynchronousExecutor(
				ExecutorUtils.getFlightSearchThreadPool(), 20, 10, 2, exceptionHandler);
		Iterator<UserFeeUpdateRequest> requestIterator = userFeeUpdateRequests.iterator();
		final ContextData contextData = SystemContextHolder.getContextData();
		executor.executeAsynchronously(() -> {
			if (requestIterator.hasNext()) {
				final UserFeeUpdateRequest userFeeUpdateRequest = requestIterator.next();
				return () -> {
					SystemContextHolder.setContextData(contextData);
					UserFeeUpdateHandler userFeeUpdateHandler =
							SpringContext.getApplicationContext().getBean(UserFeeUpdateHandler.class);
					userFeeUpdateHandler.initData(userFeeUpdateRequest, new BaseResponse());
					userFeeUpdateHandler.getResponse();
					return null;
				};
			}
			return null;
		});
		/**
		 * wait for threads to finish to avoid concurrency issues while logging
		 */
		synchronized (executor) {
			executor.wait();
		}
		return new BaseResponse();
	}

	@RequestMapping(value = "/update-client-fee", method = RequestMethod.POST)
	protected BaseResponse updatelientFee(@RequestBody List<UserFeeUpdateRequest> clientFeeUpdateRequests)
			throws Exception {
		AsyncExecutionExceptionHandler exceptionHandler = (e, taskIndex) -> log
				.error("Failed to update client fee for {} due to ", clientFeeUpdateRequests.get(taskIndex), e);
		UnboundedAsynchronousExecutor executor = new UnboundedAsynchronousExecutor(
				ExecutorUtils.getFlightSearchThreadPool(), 20, 10, 2, exceptionHandler);
		Iterator<UserFeeUpdateRequest> requestIterator = clientFeeUpdateRequests.iterator();
		final ContextData contextData = SystemContextHolder.getContextData();
		executor.executeAsynchronously(() -> {
			if (requestIterator.hasNext()) {
				final UserFeeUpdateRequest clientFeeUpdateRequest = requestIterator.next();
				return () -> {
					SystemContextHolder.setContextData(contextData);
					UserFeeUpdateHandler userFeeUpdateHandler =
							SpringContext.getApplicationContext().getBean(UserFeeUpdateHandler.class);
					userFeeUpdateHandler.initData(clientFeeUpdateRequest, new BaseResponse());
					userFeeUpdateHandler.getResponse();
					return null;
				};
			}
			return null;
		});
		/**
		 * wait for threads to finish to avoid concurrency issues while logging
		 */
		synchronized (executor) {
			executor.wait();
		}
		return new BaseResponse();
	}

}
