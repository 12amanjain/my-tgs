package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.cancel.JourneyCancellationDetail;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.AirCancellationEngine;
import com.tgs.services.fms.manager.AirCancellationManager;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
@Service
public class AirCancellationHandler extends ServiceHandler<AirCancellationRequest, AirCancellationResponse> {

	private Order order;

	@Autowired
	private AirCancellationEngine cancellationEngine;

	@Autowired
	private AirCancellationManager cancellationManager;

	private AirCancellationDetail bookingCancellationDetail;

	private PriceInfo cancellationPriceInfo;

	private Boolean isCancelled;

	private AirCancellationResponse airCancellationReviewResponse;

	@Autowired
	private AirCancellationValidator cancellationValidator;

	@Autowired
	AirAnalyticsHelper analyticsHelper;


	@Override
	public void beforeProcess() throws Exception {
		if (request == null || StringUtils.isBlank(request.getBookingId())) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
		if (response == null) {
			response = AirCancellationResponse.builder().build();
		}
	}

	@Override
	public void process() throws Exception {
		Map<String, BookingSegments> cancellationSegments =
				TripBookingUtils.filterCancellingTravellers(request.getPaxKeys(), request.getSegmentInfos());
		AirCancellationDetail validate = cancellationValidator.validate(order.getBookingId(), cancellationSegments);
		List<TripInfo> tripInfos = new ArrayList<TripInfo>();
		for (Entry<String, BookingSegments> entry : cancellationSegments.entrySet()) {
			tripInfos.addAll(
					AirCancelUtils.createTripListFromSegmentListCancellation(entry.getValue().getSegmentInfos()));
		}
		List<AirCancellationDetail> cancellationDetails = new ArrayList<AirCancellationDetail>();
		if (validate.getAutoCancellationAllowed()) {
			cancellationDetails =
					cancellationEngine.confirmCancellation(cancellationSegments, airCancellationReviewResponse, order);
		} else {
			cancellationDetails.add(validate);
		}
		bookingCancellationDetail = cancellationManager.mergeCancellationDetails(cancellationDetails, order,cancellationSegments);
		bookingCancellationDetail.setJourneyCancellationDetails(new LinkedHashMap<String, JourneyCancellationDetail>());
		cancellationManager.createJourneyCancellationDetails(tripInfos, bookingCancellationDetail);
		cancellationPriceInfo = cancellationManager.populateCancellationFare(bookingCancellationDetail);
	}

	@Override
	public void afterProcess() throws Exception {
		response.setPriceInfo(cancellationPriceInfo);
		response.setCancellationDetail(bookingCancellationDetail);

	}
}
