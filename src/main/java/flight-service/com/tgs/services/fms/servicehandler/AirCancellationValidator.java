package com.tgs.services.fms.servicehandler;


import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerMiscInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class AirCancellationValidator extends TgsValidator {


	public AirCancellationDetail validate(String bookingId, Map<String, BookingSegments> cancellationSegments) {

		boolean isAutoCancellationAllowed = false;

		AirCancellationDetail airCancellationDetail = AirCancellationDetail.builder().build();

		for (String supplierRef : cancellationSegments.keySet()) {

			BookingSegments bookedSegments = cancellationSegments.get(supplierRef);

			List<TripInfo> tripInfos =
					AirCancelUtils.createTripListFromSegmentListCancellation(bookedSegments.getSegmentInfos());
			List<TripInfo> cancelledTripInfos = AirCancelUtils
					.createTripListFromCancelledSegmentList(bookedSegments.getCancelSegments(), tripInfos);

			FareType fareType = tripInfos.get(0).getSegmentInfos().get(0).getPriceInfo(0).getFareIdentifier();

			if (FareType.SPECIAL_RETURN.equals(fareType) && tripInfos != cancelledTripInfos
					&& CollectionUtils.size(tripInfos) != CollectionUtils.size(cancelledTripInfos)) {
				airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
				airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
						SystemError.INVALID_SPECIAL_RETURN_SELECTION.getMessage());
				airCancellationDetail.setErrorMessage(SystemError.INVALID_SPECIAL_RETURN_SELECTION.getMessage());
				// throw new CustomGeneralException(SystemError.INVALID_SPECIAL_RETURN_SELECTION);
				return airCancellationDetail;
			}

			for (SegmentInfo segmentInfo : bookedSegments.getSegmentInfos()) {
				for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getTravellerInfo()) {
					if (flightTravellerInfo.getStatus() != null
							&& flightTravellerInfo.getStatus().equals(TravellerStatus.CANCELLED)) {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.setErrorMessage("Cancellation Already Done");
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments, "Cancellation Already Done");
						return airCancellationDetail;
					}
				}
			}

			// Pax Validation
			AtomicInteger tripIndex = new AtomicInteger(0);

			TripInfo firstCancelledTripInfo = null;
			if (CollectionUtils.isNotEmpty(cancelledTripInfos)) {
				firstCancelledTripInfo = cancelledTripInfos.get(0);
			}

			for (TripInfo cancelledTripInfo : cancelledTripInfos) {

				if (firstCancelledTripInfo.getSegmentInfos().get(0).getTravellerInfo().size() == cancelledTripInfo
						.getSegmentInfos().get(0).getTravellerInfo().size()) {
					for (int index = 0; index < cancelledTripInfo.getSegmentInfos().size(); index++) {
						if (firstCancelledTripInfo.getSegmentInfos().get(0).getTravellerInfo()
								.size() == cancelledTripInfo.getSegmentInfos().get(index).getTravellerInfo().size()) {
							for (int flightTravellerIndex = 0; flightTravellerIndex < firstCancelledTripInfo
									.getSegmentInfos().get(0).getTravellerInfo().size()
									&& flightTravellerIndex < cancelledTripInfo.getSegmentInfos().get(index)
											.getTravellerInfo().size(); flightTravellerIndex++) {
								boolean isPresent = false;
								for (FlightTravellerInfo currentCancelledTravellerInfo : cancelledTripInfo
										.getSegmentInfos().get(index).getTravellerInfo()) {
									if (firstCancelledTripInfo.getSegmentInfos().get(0).getTravellerInfo()
											.get(flightTravellerIndex).getId()
											.equals(currentCancelledTravellerInfo.getId())) {
										isPresent = true;
									}
								}
								if (!isPresent) {
									airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
									airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
											"All the tripInfos must Have Same Passengers");
									return airCancellationDetail;
								}
							}
						} else {
							airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
							airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
									"All the tripInfos must Have Same Passengers");
							return airCancellationDetail;
						}
					}
				} else {
					airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
					airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
							"All the tripInfos must Have Same Passengers");
					return airCancellationDetail;
				}

				TripInfo originalTrip = tripInfos.get(tripIndex.get());
				int travellerSize = cancelledTripInfo.getSegmentInfos().get(0).getTravellerInfo().size();

				List<FlightTravellerInfo> cancelledTravellerList =
						cancelledTripInfo.getSegmentInfos().get(0).getTravellerInfo();

				AtomicInteger segmentIndex = new AtomicInteger(0);
				for (SegmentInfo segmentInfo : cancelledTripInfo.getSegmentInfos()) {

					SegmentInfo originalSegment = originalTrip.getSegmentInfos().get(segmentIndex.get());
					List<FlightTravellerInfo> orgAdultTravellers =
							AirUtils.getParticularPaxTravellerInfo(originalSegment.getTravellerInfo(), PaxType.ADULT);
					List<FlightTravellerInfo> orgChildTravellers =
							AirUtils.getParticularPaxTravellerInfo(originalSegment.getTravellerInfo(), PaxType.CHILD);
					List<FlightTravellerInfo> orgInfantTravellers =
							AirUtils.getParticularPaxTravellerInfo(originalSegment.getTravellerInfo(), PaxType.INFANT);

					List<FlightTravellerInfo> adultTravellers =
							AirUtils.getParticularPaxTravellerInfo(segmentInfo.getTravellerInfo(), PaxType.ADULT);
					List<FlightTravellerInfo> childTravellers =
							AirUtils.getParticularPaxTravellerInfo(segmentInfo.getTravellerInfo(), PaxType.CHILD);
					List<FlightTravellerInfo> infantTravellers =
							AirUtils.getParticularPaxTravellerInfo(segmentInfo.getTravellerInfo(), PaxType.INFANT);


					int cancelledAdults = 0;
					int cancelledChild = 0;

					for (FlightTravellerInfo travellerInfo : orgAdultTravellers) {
						if (travellerInfo.getStatus() != null
								&& travellerInfo.getStatus().equals(TravellerStatus.CANCELLED)) {
							cancelledAdults++;
						}
					}


					int remainingAdult = orgAdultTravellers.size() - cancelledAdults;

					for (FlightTravellerInfo travellerInfo : orgChildTravellers) {
						if (travellerInfo.getStatus() != null
								&& travellerInfo.getStatus().equals(TravellerStatus.CANCELLED)) {
							cancelledChild++;
						}
					}
					cancelledChild = cancelledChild + childTravellers.size();

					// Equal number of infant,adult should be selected
					if (CollectionUtils.isNotEmpty(infantTravellers)
							&& CollectionUtils.size(infantTravellers) > CollectionUtils.size(adultTravellers)) {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
								SystemError.INVALID_INFANT_ADULT_ASSOCIATION.getMessage());
						airCancellationDetail
								.setErrorMessage(SystemError.INVALID_INFANT_ADULT_ASSOCIATION.getMessage());
						return airCancellationDetail;
					}

					if (remainingAdult == adultTravellers.size() && CollectionUtils.isNotEmpty(orgChildTravellers)
							&& orgChildTravellers.size() != childTravellers.size()
							&& orgChildTravellers.size() != cancelledChild) {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
								SystemError.INVALID_PAX_CANCELLATION_SELECTION.getMessage());
						airCancellationDetail
								.setErrorMessage(SystemError.INVALID_PAX_CANCELLATION_SELECTION.getMessage());
						return airCancellationDetail;
					}

					if (travellerSize != segmentInfo.getBookingRelatedInfo().getTravellerInfo().size()) {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
								SystemError.INVALID_CANCELLATION_SEGMENT_PAX_SELECTION.getMessage());
						airCancellationDetail
								.setErrorMessage(SystemError.INVALID_CANCELLATION_SEGMENT_PAX_SELECTION.getMessage());
						return airCancellationDetail;
					}
					// If Cancelling all adults then entire pax must be selected for cancelling
					if (CollectionUtils.isNotEmpty(adultTravellers)
							&& CollectionUtils.size(orgAdultTravellers) == CollectionUtils.size(adultTravellers)
							&& (CollectionUtils.isNotEmpty(orgChildTravellers)
									&& CollectionUtils.isEmpty(childTravellers)
									&& CollectionUtils.isNotEmpty(orgInfantTravellers)
									&& CollectionUtils.isEmpty(infantTravellers))) {
						throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PAX_SELECTION);
					}

					if (CollectionUtils.isNotEmpty(adultTravellers)
							&& CollectionUtils.size(orgAdultTravellers) == CollectionUtils.size(adultTravellers)
							&& CollectionUtils.isNotEmpty(orgChildTravellers)
							&& CollectionUtils.isNotEmpty(childTravellers)
							&& CollectionUtils.size(orgChildTravellers) != cancelledChild) {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
								SystemError.INVALID_CANCELLATION_PAX_SELECTION.getMessage());
						airCancellationDetail
								.setErrorMessage(SystemError.INVALID_CANCELLATION_SEGMENT_PAX_SELECTION.getMessage());
						return airCancellationDetail;
					}

					if (CollectionUtils.isNotEmpty(adultTravellers)
							&& CollectionUtils.size(orgAdultTravellers) == CollectionUtils.size(adultTravellers)
							&& (CollectionUtils.isNotEmpty(orgChildTravellers)
									&& CollectionUtils.isEmpty(childTravellers))
							&& orgChildTravellers.size() != cancelledChild) {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
								SystemError.INVALID_PAX_CANCELLATION_SELECTION.getMessage());
						airCancellationDetail
								.setErrorMessage(SystemError.INVALID_PAX_CANCELLATION_SELECTION.getMessage());
						return airCancellationDetail;
					}

					if (travellerSize == segmentInfo.getBookingRelatedInfo().getTravellerInfo().size()) {
						// deep matching of travellers name, incase 1 Adult and 1 Infant Cancellation
						if (!hasPax(cancelledTravellerList, segmentInfo.getTravellerInfo())) {
							airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
							airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
									SystemError.INVALID_CANCELLATION_SEGMENT_PAX_SELECTION.getMessage());
							airCancellationDetail.setErrorMessage(
									SystemError.INVALID_CANCELLATION_SEGMENT_PAX_SELECTION.getMessage());
							return airCancellationDetail;
						}
					} else {
						airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
						airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
								SystemError.INVALID_PAX_CANCELLATION_SELECTION.getMessage());
						airCancellationDetail
								.setErrorMessage(SystemError.INVALID_PAX_CANCELLATION_SELECTION.getMessage());
						return airCancellationDetail;
					}

					// If More number of adults but mapping of infant selection is mismatch
					if (CollectionUtils.isNotEmpty(orgInfantTravellers)) {
						buildAdultInfantMapping(orgAdultTravellers, orgInfantTravellers);
						buidCancellationAdultInfantMapping(adultTravellers, infantTravellers, orgAdultTravellers,
								orgInfantTravellers);
						for (FlightTravellerInfo travellerInfo : adultTravellers) {
							FlightTravellerInfo adultMappedTraveller =
									getTravellerIndex(orgAdultTravellers, travellerInfo);
							if (adultMappedTraveller != null
									&& StringUtils.isNotEmpty(adultMappedTraveller.getMiscInfo().getPassengerKey())) {
								Integer infantIndex =
										Integer.valueOf(adultMappedTraveller.getMiscInfo().getPassengerKey());
								if (!containsInfantIndex(infantIndex, infantTravellers)) {
									airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
									airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
											SystemError.INVALID_INFANT_ADULT_ASSOCIATION.getMessage());
									airCancellationDetail
											.setErrorMessage(SystemError.INVALID_INFANT_ADULT_ASSOCIATION.getMessage());
									return airCancellationDetail;
								}
							}
						}

						for (FlightTravellerInfo travellerInfo : infantTravellers) {
							FlightTravellerInfo orgInfant = getTravellerIndex(orgInfantTravellers, travellerInfo);
							FlightTravellerInfo adultMappedInfant =
									orgAdultTravellers.get(Integer.valueOf(orgInfant.getMiscInfo().getPassengerKey()));
							boolean orgAdultExist = false;
							for (FlightTravellerInfo cancelledAdult : adultTravellers) {
								if (cancelledAdult.getId().equals(adultMappedInfant.getId())) {
									orgAdultExist = true;
									break;
								}
							}
							if (!orgAdultExist) {
								airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
								airCancellationDetail.buildMessageInfo(bookingId, bookedSegments,
										SystemError.INVALID_INFANT_ADULT_ASSOCIATION.getMessage());
								airCancellationDetail
										.setErrorMessage(SystemError.INVALID_INFANT_ADULT_ASSOCIATION.getMessage());
								return airCancellationDetail;
							}
						}

					}

					// If there is no adult remaining to handle the remaining child.
					if (CollectionUtils.isNotEmpty(orgInfantTravellers)
							&& childTravellers.size() < orgChildTravellers.size()
							&& orgAdultTravellers.size() == adultTravellers.size()) {
						throw new CustomGeneralException(SystemError.INVALID_CHILD);
					}


					segmentIndex.getAndIncrement();
				}
				tripIndex.getAndIncrement();
			}
		}
		isAutoCancellationAllowed = true;
		airCancellationDetail.setAutoCancellationAllowed(isAutoCancellationAllowed);
		return airCancellationDetail;
	}

	private boolean containsInfantIndex(int infantIndex, List<FlightTravellerInfo> infantTravellers) {
		for (FlightTravellerInfo infantTraveller : infantTravellers) {
			if (infantTraveller.getMiscInfo() == null) {
				return false;
			}
			if (infantTraveller.getMiscInfo() != null
					&& infantTraveller.getMiscInfo().getPassengerKey().equals(String.valueOf(infantIndex)))
				return true;
		}
		return false;
	}

	private void buildAdultInfantMapping(List<FlightTravellerInfo> adults, List<FlightTravellerInfo> infants) {
		AtomicInteger adultPaxIndex = new AtomicInteger(0);
		adults.forEach(adult -> {
			adult.setMiscInfo(new TravellerMiscInfo());
			// adult.getMiscInfo().setPassengerKey(String.valueOf(adultPaxIndex.get()));
			FlightTravellerInfo infantTraveller =
					adultPaxIndex.get() < infants.size() ? infants.get(adultPaxIndex.get()) : null;
			if (infantTraveller != null) {
				infantTraveller.setMiscInfo(new TravellerMiscInfo());
				infantTraveller.getMiscInfo().setPassengerKey(String.valueOf(adultPaxIndex.get()));
				for (int infantIndex = 0; infantIndex < infants.size(); infantIndex++) {
					if (infantTraveller == infants.get(infantIndex)) {
						adult.getMiscInfo().setPassengerKey(String.valueOf(infantIndex));
						break;
					}
				}

			}
			adultPaxIndex.getAndIncrement();
		});
	}

	private void buidCancellationAdultInfantMapping(List<FlightTravellerInfo> cancelledAdults,
			List<FlightTravellerInfo> cancelledInfants, List<FlightTravellerInfo> orgAdults,
			List<FlightTravellerInfo> orgInfants) {
		cancelledAdults.forEach(cancelledAdult -> {
			FlightTravellerInfo orgInfant = getInfantAssociatedWithAdult(orgAdults, orgInfants, cancelledAdult);
			FlightTravellerInfo orgAdult = getTravellerIndex(orgAdults, cancelledAdult);
			if (orgInfant != null) {
				for (FlightTravellerInfo cancelledInfant : cancelledInfants) {
					if (cancelledInfant.getId().equals(orgInfant.getId())) {
						cancelledAdult.setMiscInfo(new TravellerMiscInfo());
						cancelledAdult.getMiscInfo().setPassengerKey(orgAdult.getMiscInfo().getPassengerKey());
						cancelledInfant.setMiscInfo(new TravellerMiscInfo());
						cancelledInfant.getMiscInfo().setPassengerKey(orgAdult.getMiscInfo().getPassengerKey());
					}
				}
			}
		});

	}

	private FlightTravellerInfo getTravellerIndex(List<FlightTravellerInfo> orgInfantTravellers,
			FlightTravellerInfo travellerInfo) {
		if (CollectionUtils.isNotEmpty(orgInfantTravellers)) {
			Optional<FlightTravellerInfo> traveller = AirUtils.findTraveller(orgInfantTravellers, travellerInfo);
			if (traveller.isPresent()) {
				return traveller.get();
			}
		}
		return null;
	}

	private FlightTravellerInfo getInfantAssociatedWithAdult(List<FlightTravellerInfo> orgAdultravellers,
			List<FlightTravellerInfo> orgInfantravellers, FlightTravellerInfo travellerInfo) {
		for (FlightTravellerInfo adulTraveller : orgAdultravellers) {
			if (adulTraveller.getId().equals(travellerInfo.getId())) {
				if (StringUtils.isNotEmpty(adulTraveller.getMiscInfo().getPassengerKey())) {
					return orgInfantravellers.get(Integer.valueOf(adulTraveller.getMiscInfo().getPassengerKey()));
				}
			}
		}
		return null;
	}

	public boolean hasPax(List<FlightTravellerInfo> firstSegmentCancelledTravellers,
			List<FlightTravellerInfo> restSegmentCancelledTravellers) {
		for (FlightTravellerInfo firstSegmentTraveller : firstSegmentCancelledTravellers) {
			boolean isTravellerExist = false;
			for (FlightTravellerInfo restSegmentTravellerInfo : restSegmentCancelledTravellers) {
				if (firstSegmentTraveller.getId().equals(restSegmentTravellerInfo.getId())) {
					isTravellerExist = true;
					break;
				}
			}
			if (!isTravellerExist) {
				return false;
			}
		}
		return true;
	}

}
