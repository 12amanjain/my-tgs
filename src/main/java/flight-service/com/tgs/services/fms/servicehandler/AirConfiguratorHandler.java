package com.tgs.services.fms.servicehandler;

import java.util.Objects;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.dbmodel.DbAirConfiguratorRule;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.jparepository.AirConfiguratorService;
import com.tgs.services.fms.restmodel.AirConfigRuleTypeResponse;

@Service
public class AirConfiguratorHandler extends ServiceHandler<AirConfiguratorInfo, AirConfigRuleTypeResponse> {

	@Autowired
	AirConfiguratorService airConfigService;

	@Autowired
	AirConfiguratorHelper helper;

	@Autowired
	SyncService syncService;


	private DbAirConfiguratorRule rule;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		rule = null;

		if (Objects.nonNull(request.getId())) {
			rule = airConfigService.findById(request.getId());
		}

		if (rule != null) {
			AirConfiguratorInfo configuratorInfo = rule.toDomain();
			patchUpdate(configuratorInfo, request);
		}
		if (request.getExitOnMatch() == null) {
			if (AirConfiguratorRuleType.MESSAGE.equals(request.getRuleType()))
				request.setExitOnMatch(false);
			else {
				request.setExitOnMatch(true);

			}
		}
		request.cleanData();
		rule = new DbAirConfiguratorRule().from(request);

		if (rule.getId() != null) {
			DbAirConfiguratorRule dbAirConfigRule = airConfigService.findById(rule.getId());

			JSONObject inputRule = new JSONObject(request.getSerializedOutput());

			JSONObject dbJsonRule = new JSONObject(dbAirConfigRule.getOutput());
			rule.setOutput(mergeTwoJson(inputRule, dbJsonRule).toString());
			rule.setOutput(rule.toDomain().getSerializedOutput());
			rule.setCreatedOn(dbAirConfigRule.getCreatedOn());
		}
		rule = airConfigService.save(rule);
		rule.setId(rule.getId());
		request.setId(rule.getId());
		syncService.sync("fms", rule.toDomain());
	}

	private JSONObject mergeTwoJson(JSONObject source, JSONObject target) {
		for (String key : JSONObject.getNames(source)) {
			Object value = source.get(key);
			if (!target.has(key) && !value.equals(null)) {
				target.put(key, value);
			} else {
				if (source.get(key).equals(null)) {
					target.remove(key);
				} else {
					target.put(key, value);
				}
			}
		}
		return target;
	}

	@Override
	public void afterProcess() throws Exception {
		response.getAirConfiguratorInfos().add(rule.toDomain());
	}

	public BaseResponse updateConfigStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbAirConfiguratorRule dbConfigRule = airConfigService.findById(id);
		if (Objects.nonNull(dbConfigRule)) {
			dbConfigRule.setEnabled(status);
			airConfigService.save(dbConfigRule);
			syncService.sync("fms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public void patchUpdate(AirConfiguratorInfo oldConfig, AirConfiguratorInfo newConfig) {
		if (oldConfig == null) {
			return;
		}
		/**
		 * System will override Inclusion and exclusion criteria coming in the request
		 */
		// newConfig.setInclusionCriteria(new GsonMapper<>(newConfig.getInclusionCriteria(),
		// (FlightBasicRuleCriteria) oldConfig.getInclusionCriteria(), FlightBasicRuleCriteria.class).convert());
		// newConfig.setExclusionCriteria(new GsonMapper<>(newConfig.getExclusionCriteria(),
		// (FlightBasicRuleCriteria) oldConfig.getExclusionCriteria(), FlightBasicRuleCriteria.class).convert());

		newConfig.cleanData();
	}
}
