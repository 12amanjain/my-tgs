package com.tgs.services.fms.servicehandler;

import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Slf4j
@Service
public class AirExternalServiceHandler {

	@Autowired
	ElasticSearchCommunicator esCommunicator;

	public ESAutoSuggestionResponse getSuggestions(String searchKey, String metaIndex) {
		ESSearchRequest searchRequest = ESSearchRequest.builder().source(searchKey).metaInfo(metaIndex).build();
		return esCommunicator.getSuggestions(searchRequest);
	}
}
