
package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.WordUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.fms.datamodel.AirMessageAttributes;
import com.tgs.services.fms.datamodel.AirMessagePriceInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.restmodel.AirMessageRequest;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.SimpleCache;
import com.tgs.utils.exception.ResourceNotFoundException;

@Service
public class AirMessageHandler extends ServiceHandler<AirMessageRequest, BaseResponse> {

	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	protected FMSCachingServiceCommunicator cachingService;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Override
	public void beforeProcess() throws Exception {

		this.validateRequest();

	}

	@Override
	public void process() throws Exception {
		if (request.getMode().equals(MessageMedium.EMAIL)) {
			processEmailContent(request.getPriceIds(), EmailTemplateKey.AIR_SEARCH_EMAIL);
		} else {
			// SMS Futue Scope
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public void processEmailContent(List<String> priceIds, EmailTemplateKey templateKey) {
		if (CollectionUtils.isEmpty(priceIds)) {
			return;
		}
		User user = SystemContextHolder.getContextData().getUser();
		AirSearchQuery searchQuery =
				cachingService.getSearchQuery(FlightCacheHandler.getSearchQueryKey(priceIds.get(0)));
		AirMessageAttributes messageAttributes = getMessageAttributes(templateKey, request.getToIds(), user);
		messageAttributes.setCabinClass(searchQuery.getCabinClass().getDisplayName());
		List<TripInfo> allTrips = new ArrayList<>();
		Set<RouteInfo> allRoutes = new HashSet<>();
		Map<TripInfo, AirMessagePriceInfo> priceInfos = new HashMap<>();
		Map<TripInfo, String> baggageInfos = new HashMap<>();

		populateTripsInformation(priceIds, allTrips, priceInfos, baggageInfos);

		if (request.isWithoutPrice()) {
			messageAttributes.setWithPrice(false);
		}

		if (CollectionUtils.isNotEmpty(allTrips)) {
			populateMessageAttributes(messageAttributes, allTrips, priceInfos, baggageInfos, allRoutes);
			messageAttributes.setJourney(getJourney(searchQuery.getRouteInfos()));
			sendEmail(messageAttributes);
		}
	}

	private void populateMessageAttributes(AirMessageAttributes messageAttributes, List<TripInfo> allTrips,
			Map<TripInfo, AirMessagePriceInfo> priceInfos, Map<TripInfo, String> baggageInfos,
			Set<RouteInfo> allRoutes) {
		messageAttributes.setPriceInfos(priceInfos);
		messageAttributes.setBaggageInfos(baggageInfos);
		messageAttributes.setPaxCount(getPaxCount(allTrips.get(0).getPaxInfo()));
		/**
		 * If it is an international combo, there will be only one tripInfo which is international.
		 */
		boolean isInternational = !AirUtils.isDomesticTrip(allTrips.get(0));
		SimpleCache<String, RouteInfo> routeInfoCache = new SimpleCache<String, RouteInfo>() {
			@Override
			protected String getKey(RouteInfo value) {
				return value.toString();
			}
		};
		Map<RouteInfo, Boolean> isTripForRouteOdd = new HashMap<>();
		Map<TripInfo, Boolean> isOddTrip = new HashMap<>();
		Map<SegmentInfo, Boolean> isLastSegmentOfTrip = new HashMap<>();
		Map<RouteInfo, List<TripInfo>> tripInfoGroups = allTrips.stream().collect(Collectors.groupingBy(trip -> {
			RouteInfo routeInfo = null;
			/**
			 * Sorted {@code allRoutes} is used for displaying journey, travel dates and trip-lists in correct sequence.
			 * <p>
			 * Trips are grouped by routes in {@code allRoutes}. In case of international (and domestic one-way) search,
			 * all trips will be mapped to one single (last) route in {@code allRoutes}.
			 * <p>
			 * Trip-list-banners with route-infos will be shown ONLY in case of domestic multicity and return. In all
			 * other cases, trip-list-banner is not required.
			 * <p>
			 * see {@link AirMessageAttributes#isDomesticRetOrMult};
			 */
			for (TripInfo t : trip.splitTripInfo(false)) {
				RouteInfo r = t.getRouteInfo();
				routeInfo = r = routeInfoCache.getCached(r);
				allRoutes.add(r);
			}
			Boolean isOdd = isTripForRouteOdd.get(routeInfo);
			if (isOdd == null) {
				isOdd = true;
			} else {
				isOdd = !isOdd;
			}
			isTripForRouteOdd.put(routeInfo, isOdd);
			isOddTrip.put(trip, isOdd);
			int lastSegmentIndex = trip.getSegmentInfos().size() - 1;
			isLastSegmentOfTrip.put(trip.getSegmentInfos().get(lastSegmentIndex), true);
			return routeInfo;
		}));
		messageAttributes.setIsOddTrip(isOddTrip);
		messageAttributes.setIsLastSegmentOfTrip(isLastSegmentOfTrip);
		messageAttributes.setIsDomesticRetOrMult(!isInternational && allRoutes.size() > 1);
		messageAttributes.setSortedRouteInfos(AirUtils.sortRouteList(new ArrayList<>(allRoutes)));
		messageAttributes.setTripInfoGroups(tripInfoGroups);
	}

	private void populateTripsInformation(List<String> priceIds, List<TripInfo> allTrips,
			Map<TripInfo, AirMessagePriceInfo> priceInfos, Map<TripInfo, String> baggageInfos) {

		priceIds.forEach(priceId -> {
			try {
				TripInfo tripInfo = cachingService.getTripInfo(priceId);
				allTrips.add(tripInfo);
				AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, priceId);
				Double totalPrice = AirUtils.getTotalFare(tripInfo);
				totalPrice += AirUtils.getTotalFareComponentAmount(tripInfo, FareComponent.MU);
				Double netFare = totalPrice;
				BaggageInfo baggageInfo =
						tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT).getBaggageInfo();
				baggageInfos.put(tripInfo, baggageInfo == null ? null : baggageInfo.getAllowance());

				AirMessagePriceInfo airMessagePriceInfo =
						AirMessagePriceInfo.builder().totalAmount(TgsStringUtils.formatCurrency(totalPrice)).build();
				if (UserRole.corporate(SystemContextHolder.getContextData().getUser().getRole())) {
					double discount = BaseUtils.getGrossCommission(tripInfo, 0, null, true);
					netFare = netFare - discount;
					if (discount > 0) {
						airMessagePriceInfo.setDiscount(TgsStringUtils.formatCurrency(discount));
					}
				}
				airMessagePriceInfo.setNetFare(TgsStringUtils.formatCurrency(netFare));
				priceInfos.put(tripInfo, airMessagePriceInfo);
			} catch (Exception e) {
				throw new ResourceNotFoundException(SystemError.KEYS_EXPIRED);
			}
		});
	}

	private List<String> getJourney(List<RouteInfo> routes) {
		List<String> journey = new ArrayList<>();
		int index = 0;
		for (; index < routes.size(); index++) {
			journey.add(routes.get(index).getFromCityOrAirport().getCity());
		}
		journey.add(routes.get(index - 1).getToCityOrAirport().getCity());
		return journey;
	}

	private String getPaxCount(Map<PaxType, Integer> paxInfo) {
		StringJoiner stringJoiner = new StringJoiner(", ");
		for (PaxType paxType : paxInfo.keySet()) {
			String type = paxType.name();
			Integer count = paxInfo.get(paxType);
			if (count != null && count > 0) {
				stringJoiner.add(paxInfo.get(paxType) + " " + type);
			}
		}
		return WordUtils.capitalizeFully(stringJoiner.toString());
	}

	public void sendEmail(AirMessageAttributes messageAttributes) {
		msgSrvCommunicator.sendMail(messageAttributes);
	}

	public AirMessageAttributes getMessageAttributes(EmailTemplateKey templateKey, String toEmail, User user) {
		AbstractMessageSupplier<AirMessageAttributes> messageAttributes =
				new AbstractMessageSupplier<AirMessageAttributes>() {
					@Override
					public AirMessageAttributes get() {
						AirMessageAttributes messageAttributes = AirMessageAttributes.builder().build();
						messageAttributes.setToEmailId(toEmail);
						messageAttributes.setKey(templateKey.name());
						if (user != null) {
							messageAttributes.setFromEmail(user.getEmail());
							messageAttributes.setRole(user.getRole());
						}
						return messageAttributes;
					}
				};
		return messageAttributes.get();
	}

	public void validateRequest() {

		if (CollectionUtils.isEmpty(request.getPriceIds())) {
			throw new CustomGeneralException(SystemError.EMPTY_PRICEID);
		}

	}
}
