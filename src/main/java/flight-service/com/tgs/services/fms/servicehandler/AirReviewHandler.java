package com.tgs.services.fms.servicehandler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.GstConditions;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.fms.datamodel.AirPassportConditions;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightSoldOut;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SegmentMiscInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.DobOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirReviewManager;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.AirUserFeeManager;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.fms.mapper.AirReviewToAnalyticsAirReviewMapper;
import com.tgs.services.fms.mapper.PriceInfoToTotalFareDetailMapper;
import com.tgs.services.fms.restmodel.AirReviewRequest;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirAnalyticsUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import com.tgs.utils.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirReviewHandler extends ServiceHandler<AirReviewRequest, AirReviewResponse> {

	@Autowired
	private FlightCacheHandler cacheHandler;

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	AirReviewManager reviewManager;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	@Autowired
	AirUserFeeManager userFeeManager;

	@Autowired
	AirReviewSessionHandler sessionHandler;

	@Autowired
	AirAnalyticsHelper analyticsHelper;

	@Autowired
	HMSCommunicator hmsCommunicator;

	@Autowired
	OrderServiceCommunicator omsCommunicator;

	@Override
	public void beforeProcess() throws Exception {

		if (request.getPriceValidation() == null && StringUtils.isBlank(request.getBookingId())) {
			// Previous API Login to make all times price validation true
			request.setPriceValidation(true);
		}

		if (StringUtils.isNotBlank(request.getBookingId())) {
			List<SupplierSession> sessions = TripBookingUtils.getSupplierSession(request.getBookingId(),
					SystemContextHolder.getContextData().getUser());
			if (CollectionUtils.isNotEmpty(sessions)) {
				if (checkIsValidReviewTrips(sessions)) {
					AirReviewResponse reviewResponse = cachingService.fetchValue(request.getBookingId(),
							AirReviewResponse.class, CacheSetName.AIR_REVIEW.getName(), BinName.AIRREVIEW.getName());
					if (reviewResponse != null) {
						response = reviewResponse;
						response.setAlerts(null);
						updateSessionTime(response, sessions);
						request.setPriceValidation(Boolean.TRUE);
					}
				} else {
					// It should be sequence to get the same class again
					sessionHandler.closeExistsSession(sessions, request.getBookingId());
					request.setPriceValidation(Boolean.TRUE);
				}
			}
		}
	}

	@Override
	public void process() throws Exception {
		StringJoiner exceptions = new StringJoiner("-");
		if (CollectionUtils.isEmpty(response.getTripInfos())) {
			String bookingId = null;
			List<AirSearchQuery> searchQueries = new ArrayList<>();
			/**
			 * segmentMiscInfos (in order of tripInfos) to be used for updating Mark Up
			 */
			List<TripInfo> tripInfosCopy = new ArrayList<>();
			boolean isToSendFareDifference = false;
			try {
				List<TripInfo> tripInfos = new ArrayList<>();
				try {
					for (String priceId : request.getPriceIds()) {
						log.info("Reviewing priceId {}", priceId);
						searchQueries.add(
								cachingService.fetchValue(cacheHandler.getSearchQueryKey(priceId), AirSearchQuery.class,
										CacheSetName.SEARCHQUERY.getName(), BinName.SEARCHQUERYBIN.getName()));
						TripInfo tripInfo = cachingService.fetchValue(priceId, TripInfo.class,
								CacheSetName.PRICE_INFO.getName(), BinName.PRICEINFOBIN.getName());
						AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, priceId);
						tripInfos.add(tripInfo);
						tripInfosCopy.addAll(AirUtils.splitTripInfo(tripInfo, false));
					}
				} catch (Exception e) {
					throw new ResourceNotFoundException(SystemError.KEYS_EXPIRED);
				}
				/**
				 * when number of priceId passed in the review request dosen't match the number of routeInfo requested
				 * in the original searchQuery
				 */
				if (BooleanUtils.isFalse(searchQueries.get(0).isIntl())
						&& searchQueries.get(0).getOrigRouteInfoSize() != tripInfos.size()) {
					throw new CustomGeneralException(SystemError.PRICEID_MISMATCH,
							StringUtils.join("Please pass correct number of priceIds."));
				}

				isToSendFareDifference =
						isToShowFareDiff(searchQueries.get(0), SystemContextHolder.getContextData().getUser());

				@SuppressWarnings("serial")
				List<AirSearchQuery> copySearchQueries = GsonUtils.getGson().fromJson(
						GsonUtils.getGson().toJson(searchQueries), new TypeToken<List<AirSearchQuery>>() {}.getType());
				if (BooleanUtils.isNotFalse(request.getPriceValidation())) {
					bookingId = getBookingId(searchQueries);
				}
				AirUtils.validateTrips(tripInfos, searchQueries, SystemContextHolder.getContextData().getUser());
				List<TripInfo> newTripInfos = new ArrayList<>();
				if (BooleanUtils.isNotFalse(request.getPriceValidation())) {
					setFlowTypeInSearchQuery(copySearchQueries);
					newTripInfos = reviewManager.reviewTrips(copySearchQueries, tripInfos, bookingId);
					response.setBookingId(bookingId);
				} else {
					newTripInfos.addAll(tripInfos);
				}

				response.setBookingId(bookingId);
				response.setTripId(request.getTripId());

				if (searchQueries.get(0).isIntl()
						&& (searchQueries.get(0).isReturn() || searchQueries.get(0).isMultiCity())) {
					response.setTripInfos(AirUtils.splitTripInfo(newTripInfos.get(0), false));
				} else {
					response.setTripInfos(newTripInfos);
				}
				response.setSearchQuery(AirUtils.combineSearchQuery(searchQueries));
				int i = 0;
				User user = SystemContextHolder.getContextData().getUser();
				for (TripInfo trip : response.getTripInfos()) {
					SegmentMiscInfo miscInfo = tripInfosCopy.get(i++).getSegmentInfos().get(0).getMiscInfo();

					/**
					 * Update MarkUp if it has been updated in previous step.<br>
					 * Fetch Mark Up for a trip from SegmentMiscInfo and use it to update Mark Up for that trip.
					 */
					Double updatedUserFee = miscInfo.getUpdatedUserFee();
					if (updatedUserFee != null) {
						userFeeManager.updateUserFee(user, updatedUserFee,
								UserFeeType.getEnumFromCode(miscInfo.getUpdatedUserfeeType()), trip);
					}

					GeneralBasicFact fact = GeneralBasicFact.builder().build();
					fact.generateFact(user.getRole());
					FareBreakUpConfigOutput configOutput =
							gsCommunicator.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);

					AirUtils.setTripPriceInfoFromSegmentPriceInfo(trip, user, configOutput);
					trip.setProcessedTripInfo(AirUtils.getProcessedTripInfo(trip));
					if (UserUtils.isApiUserRequest(user)) {
						AirUtils.unsetExtraServicesForApiUser(trip);
					}
				}

				response.addAlert(
						AirUtils.getFareChangeAlert(tripInfos, newTripInfos, bookingId, isToSendFareDifference));

				AtomicInteger routeTripIndex = new AtomicInteger(0);
				response.getSearchQuery().getRouteInfos().forEach(routeInfo -> {
					AirUtils.updateNearByAirport(routeInfo,
							response.getTripInfos().get(routeTripIndex.get()).getProcessedTripInfo());
					routeTripIndex.getAndIncrement();
				});

				// response.setSearchQueries(searchQueries);
				PriceInfoToTotalFareDetailMapper toTotalFareDetailMapper = PriceInfoToTotalFareDetailMapper.builder()
						.addMarkupInTF(false).paxInfo(response.getSearchQuery().getPaxInfo())
						.priceInfos(AirUtils.getPriceInfos(response.getTripInfos())).build();
				response.setTotalPriceInfo(toTotalFareDetailMapper.convert());

				AirUtils.setPlatingCarrier(response.getTripInfos());
				response.setConditions(getBookingCondition(newTripInfos, response.getSearchQuery(), user));
				if (response.getSearchQuery().isPNRCreditSearch()) {
					List<TravellerInfo> travellers = omsCommunicator.getFlightTravellers(
							response.getSearchQuery().getSearchModifiers().getPnrCreditInfo().getPnr());
					response.setTravellers(travellers);
				}
			} catch (ResourceNotFoundException e) {
				log.error("Keys are already expired {}", request.getPriceIds(), e);
				throw e;
			} catch (CustomGeneralException e) {
				LogUtils.log(bookingId, "AirReview", e);
				exceptions.add(e.getMessage());
				String message = StringUtils.join(" You can use booking Id ", bookingId, " for reference");
				FlightSoldOut soldOut = FlightSoldOut.builder().type(AlertType.SOLDOUT.name()).message(message).build();
				response.addAlert(soldOut);
				throw new CustomGeneralException(e.getError(),
						StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
			} catch (Exception e) {
				LogUtils.log(bookingId, "AirReview", e);
				exceptions.add(e.getMessage());
				log.error("Not able to successfully review flights for bookingId {}", bookingId, e);
				FlightSoldOut soldOut = FlightSoldOut.builder().type(AlertType.SOLDOUT.name())
						.message(SystemError.FLIGHT_SOLD_OUT.getMessage()).build();
				response.addAlert(soldOut);
				throw new CustomGeneralException(SystemError.FLIGHT_SOLD_OUT,
						StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
			} finally {
				if (StringUtils.isNotBlank(bookingId)) {
					if (CollectionUtils.isNotEmpty(response.getTripInfos())) {
						cacheHandler.processAndStore(response.getTripInfos(), searchQueries.get(0).getSearchId(),
								searchQueries.get(0).getSearchType().toString());
						cachingService.store(bookingId, BinName.AIRREVIEW.getName(), response,
								CacheMetaInfo.builder().set(CacheSetName.AIR_REVIEW.name()).compress(true)
										.plainData(false).expiration(CacheType.REVIEWKEYEXPIRATION.getTtl()).build());

						if (UserRole.corporate(SystemContextHolder.getContextData().getUser().getRole())) {
							cachingService.store(bookingId, BinName.LPIDS.getName(), request,
									CacheMetaInfo.builder().set(CacheSetName.LOWEST_PRICE_IDS.name()).compress(true)
											.plainData(false).expiration(CacheType.REVIEWKEYEXPIRATION.getTtl())
											.build());
						}
						generateAndCacheHotelSearchQuery(bookingId, response.getTripInfos());
					}
					reviewManager.storeSearchLogs(searchQueries, bookingId);
				}
				sendDataToAnalytics();
				LogUtils.clearLogList();
			}
		}

	}

	public void sendDataToAnalytics() {
		try {
			if (StringUtils.isNotBlank(response.getBookingId())) {
				AirReviewToAnalyticsAirReviewMapper queryMapper = AirReviewToAnalyticsAirReviewMapper.builder()
						.user(SystemContextHolder.getContextData().getUser())
						.contextData(SystemContextHolder.getContextData()).tripInfos(response.getTripInfos())
						.searchQuery(response.getSearchQuery()).alerts(response.getAlerts()).build();
				queryMapper.setBookingId(response.getBookingId());
				analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.REVIEW);
			}
			// sendSimilarPriceOptions();

		} catch (Exception e) {
			log.error("Failed to Push Analytics REVIEW ", e);
		}
	}

	private void sendSimilarPriceOptions() {
		if (BooleanUtils.isTrue(request.getPriceValidation())) {
			List<TripInfo> tripList = new ArrayList<>();
			for (String priceId : request.getPriceIds()) {
				tripList.add(cachingService.fetchValue(priceId, TripInfo.class, CacheSetName.PRICE_INFO.getName(),
						BinName.PRICEINFOBIN.getName()));
			}
			List<List<String>> priceList = new ArrayList<>();
			for (int priceIndex = 0; priceIndex < tripList.get(0).getSegmentInfos().get(0).getPriceInfoList()
					.size(); priceIndex++) {
				priceList.set(priceIndex, new ArrayList<>());
				for (int tripIndex = 0; tripIndex < tripList.size(); tripIndex++) {
					if (tripList.get(tripIndex).getSegmentInfos().get(0).getPriceInfoList().size() < priceIndex) {
						String priceId =
								tripList.get(tripIndex).getSegmentInfos().get(0).getPriceInfo(priceIndex).getId();
						if (request.getPriceIds().contains(priceId)) {
							priceList.remove(priceIndex);
							break;
						}
						priceList.get(priceIndex).add(priceId);
					}
				}
			}

			if (request.getPriceIds().size() == 1) {
				// merge priceList
				for (int i = 0; i < priceList.size(); i++) {
					for (int j = 0; j < request.getSimilarPriceIds().size(); j++) {
						if (!priceList.get(i).get(0).contains(request.getSimilarPriceIds().get(j))) {
							priceList.add(Arrays.asList(request.getSimilarPriceIds().get(j)));
						}
					}
				}
			}

			List<TripInfo> availableTripList = new ArrayList<>();
			for (int priceOptionIdex = 0; priceOptionIdex < priceList.size(); priceOptionIdex++) {
				for (int tripIndex = 0; tripIndex < priceList.get(priceOptionIdex).size(); tripIndex++) {
					String priceId = priceList.get(priceOptionIdex).get(tripIndex);
					log.info("priceId {}, requestPriceId {}", priceId, request.getPriceIds().get(0));
					TripInfo tripInfo = cachingService.fetchValue(priceId, TripInfo.class,
							CacheSetName.PRICE_INFO.getName(), BinName.PRICEINFOBIN.getName());
					AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, priceId);
					availableTripList.add(tripInfo);
				}

				List<SegmentInfo> segmentInfos = AirUtils.getSegmentInfos(response.getTripInfos());
				double farediff = AirAnalyticsUtils.getTotalAirlineFare(segmentInfos)
						- AirAnalyticsUtils.getTotalAirlineFare(AirUtils.getSegmentInfos(tripList));
				AirReviewToAnalyticsAirReviewMapper queryMapper = AirReviewToAnalyticsAirReviewMapper.builder()
						.user(SystemContextHolder.getContextData().getUser()).farediff(farediff)
						.totalbookingfare(AirAnalyticsUtils.getTotalAirlineFare(segmentInfos))
						.bookedfaretype(AirAnalyticsUtils.getFareType(segmentInfos))
						.contextData(SystemContextHolder.getContextData()).tripInfos(tripList)
						.bookingId(response.getBookingId()).searchQuery(response.getSearchQuery()).build();
				analyticsHelper.pushToAnalytics(queryMapper, AirAnalyticsType.SIMILAR_PRICE);
			}

		}
	}

	private void setFlowTypeInSearchQuery(List<AirSearchQuery> copySearchQueries) {
		if (StringUtils.isNotBlank(request.getFlowType())) {
			AirFlowType flowType = AirFlowType.getAirFlowType(request.getFlowType());
			if (flowType != null) {
				copySearchQueries.forEach(searchQuery -> {
					searchQuery.setFlowType(flowType);
				});
			}
		}
	}

	private String getBookingId(List<AirSearchQuery> searchQueries) {
		String bookingId = request != null ? request.getBookingId() : null;
		if (StringUtils.isBlank(bookingId)) {
			bookingId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AIR).subProduct(
					searchQueries.get(0).getIsDomestic() ? AirType.DOMESTIC.getName() : AirType.INTERNATIONAL.getName())
					.build());
		}
		return bookingId;
	}

	@Override
	public void afterProcess() throws Exception {
		response.setPriceValidation(BooleanUtils.isTrue(request.getPriceValidation()));
	}

	public AirBookingConditions getBookingCondition(List<TripInfo> tripInfos, AirSearchQuery searchQuery,
			User bookingUser) {
		AtomicInteger sessionTime = new AtomicInteger(20);
		Set<String> frequentFlierAirlines = new HashSet<>();
		AtomicBoolean gstApplicable = new AtomicBoolean();
		AtomicBoolean isBlockingAllowed = new AtomicBoolean(true);
		AtomicBoolean isSeatApplicable = new AtomicBoolean();
		AtomicBoolean isAddressRequired = new AtomicBoolean();
		AtomicReference<AirPassportConditions> passportCond =
				new AtomicReference<AirPassportConditions>(AirPassportConditions.builder().build());
		AtomicReference<DobOutput> dobOutput = new AtomicReference<DobOutput>(DobOutput.builder().build());
		AtomicBoolean isGSTMandatory = new AtomicBoolean();
		AtomicBoolean isPANRequired = new AtomicBoolean(false);

		tripInfos.forEach(tripInfo -> tripInfo.splitTripInfo(true).forEach(trip -> {
			trip.setBookingConditions(tripInfo.getBookingConditions());
			dobOutput.set(AirUtils.getDobConditions(trip, dobOutput.get(), bookingUser));
			isBlockingAllowed.set(AirUtils.isBlockingAllowed(trip, searchQuery, isBlockingAllowed));
			sessionTime.set(Math.min(AirUtils.getSessionTime(trip, null, bookingUser), sessionTime.get()));
			isSeatApplicable.set(AirUtils.getSeatApplicable(trip, isSeatApplicable));
			isPANRequired.set(AirUtils.getIsPANRequired(trip, isPANRequired));
			passportCond.set(AirUtils.getPassportConditions(tripInfo, passportCond.get(), bookingUser, isPANRequired));
			gstApplicable.set(AirUtils.isGSTApplicable(trip, gstApplicable, bookingUser));
			isAddressRequired.set(AirUtils.isAddressRequired(trip, isAddressRequired, bookingUser));
			isGSTMandatory.set(AirUtils.isGSTMandatory(trip, isGSTMandatory, gstApplicable));
		}));

		AirBookingConditions bookingConditions = AirBookingConditions.builder()
				.sessionTimeInSecond(sessionTime.get() * 60).sessionCreationTime(LocalDateTime.now())
				.gstInfo(ServiceUtils.isGstApplicable() != null ? GstConditions.builder()
						.gstApplicable(gstApplicable.get()).isGSTMandatory(isGSTMandatory.get()).build() : null)
				.isBlockingAllowed(isBlockingAllowed.get()).isSeatApplicable(isSeatApplicable.get())
				.isAddressRequired(isAddressRequired.get()).dobOutput(dobOutput.get())
				.isPANRequired(isPANRequired.get()).build();

		if (passportCond.get().isPassportEligible()) {
			bookingConditions.setPassportConditions(passportCond.get());
		}

		for (TripInfo tripInfo : tripInfos) {
			List<String> disabledffAirlines = AirUtils.getFfDisabledAirlines(tripInfo, bookingUser);
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				String airlineCode = segmentInfo.getPlatingCarrier(segmentInfo.getPriceInfo(0));
				if (!disabledffAirlines.contains(airlineCode)) {
					frequentFlierAirlines.add(airlineCode);
				}
			}
		}

		if (!BooleanUtils.isNotFalse(request.getPriceValidation())) {
			bookingConditions.setSessionTimeInSecond(null);
		}
		bookingConditions.setFrequentFlierAirlines(frequentFlierAirlines);
		return bookingConditions;
	}

	/**
	 * This is primarily used for testing purpose. In order to test fare jump case we can define value in DB.
	 *
	 * @param searchQuery
	 * @return
	 */
	public boolean isToShowFareDiff(AirSearchQuery searchQuery, User bookingUser) {
		FlightBasicFact fact = FlightBasicFact.createFact().generateFact(searchQuery);
		BaseUtils.createFactOnUser(fact, bookingUser);
		AirGeneralPurposeOutput rule = AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.GNPUPROSE);
		if (rule != null) {
			return BooleanUtils.toBoolean(rule.getShowFareDiff());
		}
		return false;
	}

	private boolean checkIsValidReviewTrips(List<SupplierSession> sessions) {
		LocalDateTime minimumExpiryTime =
				sessions.stream().map(SupplierSession::getExpiryTime).min(LocalDateTime::compareTo).get();
		Integer reviewCacheExpiry = Long
				.valueOf(Duration.between(sessions.get(0).getCreatedOn(), minimumExpiryTime).toMinutes()).intValue();
		return Duration.between(sessions.get(0).getCreatedOn(), LocalDateTime.now())
				.toMinutes() < (reviewCacheExpiry - 5);
	}

	private void updateSessionTime(AirReviewResponse response, List<SupplierSession> sessions) {
		AirBookingConditions conditions = (AirBookingConditions) response.getConditions();
		Long minutes = Duration.between(sessions.get(0).getCreatedOn(), LocalDateTime.now()).toMinutes();
		conditions.setSessionTimeInSecond(conditions.getSessionTimeInSecond() - (minutes.intValue() * 60));
		response.setConditions(conditions);
	}

	private void generateAndCacheHotelSearchQuery(String bookingId, List<TripInfo> tripInfoList) {
		ExecutorService executor = ExecutorUtils.getFlightSearchThreadPool();
		executor.submit(() -> {
			try {
				if (CollectionUtils.isNotEmpty(tripInfoList)) {
					HotelSearchQuery searchQuery = BaseUtils.getHotelSearchQuery(tripInfoList, bookingId);
					hmsCommunicator.getCrossSellHotelSearchResult(searchQuery);
				}
			} catch (Exception e) {
				log.error("Unable to generate search query for bookingId {}", bookingId, e);
			}
		});
	}
}
