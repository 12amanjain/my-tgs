package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.helper.SupplierSessionHelper;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.oms.restmodel.BookingRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirReviewSessionHandler extends ServiceHandler<BookingRequest, BaseResponse> {

	@Autowired
	private SupplierSessionHelper sessionHelper;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		String bookingId = request.getBookingId();
		List<SupplierSession> supplierSessions =
				TripBookingUtils.getSupplierSession(bookingId, SystemContextHolder.getContextData().getUser());
		try {
			List<Future<?>> futureTaskList = new ArrayList<>();
			Map<String, List<SupplierSession>> supplierWiseSession = getSourceWiseSession(supplierSessions);
			for (Map.Entry<String, List<SupplierSession>> entry : supplierWiseSession.entrySet()) {
				List<SupplierSession> sessionList = entry.getValue();
				Integer sourceId = sessionList.get(0).getSourceId();
				AirSourceType sourceType = AirSourceType.getAirSourceType(sourceId);
				SupplierRule supplierRule =
						SupplierConfigurationHelper.getSupplierRule(sourceId, sessionList.get(0).getSupplierId());
				SupplierConfiguration supplierConf =
						SupplierConfigurationHelper.getSupplierConfiguration(sourceId, supplierRule.getId());
				AbstractAirInfoFactory factory = sourceType.getFactoryInstance(null, supplierConf);
				futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
						.submit(() -> factory.closeReviewSession(sessionList, bookingId)));
				futureTaskList.forEach(task -> {
					try {
						task.get(120, TimeUnit.SECONDS);
					} catch (TimeoutException e) {
						log.error("Cancelling review thread due to timeout for bookingId {}", bookingId, e);
						task.cancel(true);
					} catch (Exception e) {
						log.error("Close Session Execution failed for BookingId {} ", bookingId, e);
						throw new CustomGeneralException(SystemError.EXPIRED_BOOKING_ID);
					}
				});
			}
		} catch (Exception e) {
			throw new CustomGeneralException(SystemError.EXPIRED_BOOKING_ID);
		} finally {
			sessionHelper.cleanSessions(supplierSessions, SystemContextHolder.getContextData().getUser());
		}
	}

	private Map<String, List<SupplierSession>> getSourceWiseSession(List<SupplierSession> sessions) {
		Map<String, List<SupplierSession>> sessionMap = new HashMap<>();
		sessions.forEach(session -> {
			if (StringUtils.isNotBlank(session.getSupplierSessionInfo().getSessionToken())) {
				if (!sessionMap.containsKey(session.getSupplierId())) {
					sessionMap.put(session.getSupplierId(), new ArrayList<>());
				}
				sessionMap.get(session.getSupplierId()).add(session);
			}
		});
		return sessionMap;
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public void closeExistsSession(List<SupplierSession> sessions, String bookingId) {
		request = new BookingRequest();
		request.setBookingId(bookingId);
		response = new BaseResponse();
		try {
			getResponse();
		} catch (Exception e) {
			log.error(AirSourceConstants.SESSION_CLOSE, bookingId);
		}
	}
}
