package com.tgs.services.fms.servicehandler;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.fms.restmodel.AirSearchResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.manager.AirSearchManager;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirSearchHandler extends ServiceHandler<AirSearchRequest, AirSearchResponse> {

	@Autowired
	FlightCacheHandler cacheHandler;

	@Autowired
	FMSCachingServiceCommunicator cachingService;

	@Autowired
	AirSearchManager searchManager;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		AirSearchResult searchResult = null;
		if (request.getSearchQuery() != null) {
			AirUtils.populateMissingParametersInAirSearchQuery(request.getSearchQuery());
			searchResult = searchManager.doSearch(request.getSearchQuery(), SystemContextHolder.getContextData());
		} else {
			SystemContextHolder.getContextData().getReqIds().add(request.getSearchId());
			if (isSearchCompleted()) {
				for (TripInfoType type : TripInfoType.values()) {
					log.debug("Retreiving Trip Info for key {}", (request.getSearchId() + type.getName()));
					List<TripInfo> trips = cachingService.fetchValue(request.getSearchId() + type.getName(),
							TripInfo.class, CacheMetaInfo.builder().set(CacheSetName.FLIGHT_SEARCH.name()).build());
					log.debug("Total no of trips fetched for type {} is {} for searchId {}", type, trips.size(),
							request.getSearchId());
					if (CollectionUtils.isNotEmpty(trips)) {
						if (searchResult == null) {
							searchResult = new AirSearchResult();
						}
						AirSearchQuery searchQuery =
								cachingService.fetchValue(request.getSearchId(), AirSearchQuery.class,
										CacheSetName.SEARCHQUERY.getName(), BinName.SEARCHQUERYBIN.getName());

						LogUtils.log(LogTypes.AIRSEARCH_RESPONSE_START, LogMetaInfo.builder()
								.timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId()).build(),
								null);

						GeneralBasicFact fact = GeneralBasicFact.builder().build();
						User user = SystemContextHolder.getContextData().getUser();
						fact.generateFact(user.getRole());
						FareBreakUpConfigOutput configOutput =
								gmsComm.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);

						trips.forEach(trip -> {

							AirUtils.setTripPriceInfoFromSegmentPriceInfo(trip, user, configOutput);

							AirUtils.setProcessedTripInfo(trip, searchQuery, user);

							/**
							 * There is no need to send fare component segment wise to UI, But we need to send fare
							 * components for API User
							 */
							if (!UserUtils.isApiUserRequest(SystemContextHolder.getContextData().getUser())) {
								for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
									for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
										for (Iterator<PaxType> iter =
												priceInfo.getFareDetails().keySet().iterator(); iter.hasNext();) {
											FareDetail fd = priceInfo.getFareDetails().get(iter.next());
											fd.setAddlFareComponents(null);
											fd.setFareComponents(null);
										}
									}
								}
							}
						});
						searchResult.getTripInfos().put(type.name(), trips);
					}
				}
			}
		}
		response.setSearchId(request.getSearchId());
		response.setSearchResult(searchResult);
	}

	@Override
	public void afterProcess() throws Exception {}

	public boolean isSearchCompleted() {
		Map<String, String> binMap =
				cachingService.fetchValue(AirSearchManager.getSearchQueryStatusString(request.getSearchId()),
						String.class, CacheSetName.FLIGHT_SEARCH.getName(), new String[] {BinName.STOREAT.name()});
		if (MapUtils.isNotEmpty(binMap)) {
			LocalDateTime searchTime = LocalDateTime.parse(binMap.get(BinName.STOREAT.name()));
			long timeInSec = ChronoUnit.SECONDS.between(searchTime, LocalDateTime.now());
			if (timeInSec > 120) {
				log.info("Unable to perform search in {} sec for searchId {}", timeInSec, request.getSearchId());
				return true;
			}
			if (timeInSec < 1) {
				timeInSec = 1;
			}
			if (timeInSec < 12) {
				timeInSec = 2;
			} else {
				log.info("Unable to perform search in {} sec for searchId {}", timeInSec, request.getSearchId());
				timeInSec = 5;
			}
			response.setRetryInSecond((int) timeInSec);
			return false;
		}
		return true;
	}

}
