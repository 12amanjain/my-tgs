package com.tgs.services.fms.servicehandler;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirReviewManager;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.fms.mapper.TripInfoToAnalyticsTripMapper;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.AirReviewSeatResponse;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.restmodel.BookingRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Slf4j
@Service
public class AirSeatMapHandler extends ServiceHandler<BookingRequest, AirReviewSeatResponse> {

	@Autowired
	AirReviewManager reviewManager;

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	protected AirAnalyticsHelper analyticsHelper;

	@Override
	public void beforeProcess() {}

	@Override
	public void process() {
		AirReviewResponse reviewResponse = getAirReviewResponse(request.getBookingId());
		List<TripInfo> tripInfos = reviewResponse.getTripInfos();
		List<String> errorMessages = new ArrayList<>();
		AirBookingConditions condition = (AirBookingConditions) reviewResponse.getConditions();
		if (condition != null && BooleanUtils.isTrue(condition.getIsSeatApplicable())) {
			try {
				Map<String, List<TripInfo>> sourceWiseTripMap =
						TripBookingUtils.getSourceWiseTripInfo(tripInfos, request.getBookingId());
				for (Map.Entry<String, List<TripInfo>> entry : sourceWiseTripMap.entrySet()) {
					List<TripInfo> trips = entry.getValue();
					for (TripInfo tripInfo : trips) {
						AirSourceConfigurationOutput sourceConfiguration = AirUtils
								.getAirSourceConfigOnTripInfo(tripInfo, SystemContextHolder.getContextData().getUser());
						if (BooleanUtils.isTrue(sourceConfiguration.getIsSeatApplicable())
								&& AirUtils.isSSRValidForTrip(sourceConfiguration.getSeatMapRestrictionMinutes(),
										tripInfo.getDepartureTime(), LocalDateTime.now())) {
							SupplierBasicInfo basicInfo = tripInfo.getSupplierInfo();
							AirSearchQuery searchQuery = AirUtils.getSearchQueryFromTripInfo(tripInfo);
							AirSourceType airSourceType = AirSourceType.getAirSourceType(basicInfo.getSourceId());
							SupplierConfiguration supplierConf = SupplierConfigurationHelper
									.getSupplierConfiguration(basicInfo.getSourceId(), basicInfo.getRuleId());
							AbstractAirInfoFactory factory =
									airSourceType.getFactoryInstance(searchQuery, supplierConf);
							TripSeatMap tripSeatMap = factory.getSeatInfo(tripInfo, request.getBookingId());
							if (tripSeatMap != null && MapUtils.isNotEmpty(tripSeatMap.getTripSeat())) {
								addSeatMapToTripInfo(tripSeatMap, tripInfo);
								tripSeatMap.prepareStructure();
								response.addSeatMap(tripSeatMap);
							} else if (CollectionUtils.isNotEmpty(factory.getCriticalMessageLogger())) {
								errorMessages.addAll(factory.getCriticalMessageLogger());
							}
						}
					}
				}
			} finally {
				updateReviewResponseOnCache(request.getBookingId(), reviewResponse);
				addSeatInfoToAnalytics(reviewResponse, errorMessages);
			}
		}
	}

	private void addSeatMapToTripInfo(TripSeatMap tripSeatMap, TripInfo tripInfo) {
		if (MapUtils.isNotEmpty(tripSeatMap.getTripSeat())) {
			for (Map.Entry<String, SeatInformation> entry : tripSeatMap.getTripSeat().entrySet()) {
				String segmentId = entry.getKey();
				List<SeatSSRInformation> seatInfos = entry.getValue().getSeatsInfo();
				SegmentInfo segmentInfo = tripInfo.getSegmentInfos().stream()
						.filter(segment -> segment.getId().equals(segmentId)).findFirst().orElse(null);
				if (segmentInfo != null) {
					if (MapUtils.isEmpty(segmentInfo.getSsrInfo())) {
						segmentInfo.setSsrInfo(new HashMap<>());
					}
					segmentInfo.getSsrInfo().put(SSRType.SEAT, seatInfos);
				}
			}
		}
	}

	@Override
	public void afterProcess() {
		response.setBookingId(request.getBookingId());
		if (response.getTripSeatMap() == null || MapUtils.isEmpty(response.getTripSeatMap().getTripSeat())) {
			throw new CustomGeneralException(SystemError.SEAT_SELECTION_NOT_AVAILABLE);
		}
	}

	private void addSeatInfoToAnalytics(AirReviewResponse reviewResponse, List<String> errorMessages) {
		try {
			TripInfoToAnalyticsTripMapper tripMapper = TripInfoToAnalyticsTripMapper.builder()
					.contextData(SystemContextHolder.getContextData()).bookingId(request.getBookingId())
					.user(SystemContextHolder.getContextData().getUser()).tripInfos(reviewResponse.getTripInfos())
					.errorMessages(errorMessages).searchQuery(reviewResponse.getSearchQuery()).build();
			analyticsHelper.pushToAnalytics(tripMapper, AirAnalyticsType.REVIEW_SEAT);
		} catch (Exception e) {
			log.error("Failed to Push Analytics REVIEW_SEAT {}", request.getBookingId(), e);
		}
	}

	public AirReviewResponse getAirReviewResponse(String bookingId) {
		AirReviewResponse reviewResponse = cachingService.fetchValue(bookingId, AirReviewResponse.class,
				CacheSetName.AIR_REVIEW.getName(), BinName.AIRREVIEW.getName());
		if (reviewResponse == null) {
			throw new CustomGeneralException(SystemError.EXPIRED_BOOKING_ID);
		}
		return reviewResponse;
	}

	public void updateReviewResponseOnCache(String bookingId, AirReviewResponse reviewResponse) {
		cachingService.store(bookingId, BinName.AIRREVIEW.getName(), reviewResponse,
				CacheMetaInfo.builder().set(CacheSetName.AIR_REVIEW.name()).compress(true).plainData(false)
						.expiration(CacheType.REVIEWKEYEXPIRATION.getTtl()).build());
	}
}
