package com.tgs.services.fms.servicehandler;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.ReloadRequest;
import com.tgs.services.fms.datamodel.AirSearchModifiers;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.jparepository.SupplierInfoService;
import com.tgs.services.fms.jparepository.SupplierRuleService;
import com.tgs.services.fms.manager.AirSearchManager;
import com.tgs.services.fms.utils.AirUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirStubsInitializeHandler extends ServiceHandler<ReloadRequest, BaseResponse> {

	@Autowired
	SupplierInfoService supplierInfoService;

	@Autowired
	SupplierRuleService supplierRuleService;

	@Autowired
	AirSearchManager searchManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		log.info("Doing domestic search for oneway");
		doSearch(SearchType.ONEWAY, getRouteInfos());
		log.info("Doing domestic search for return");
		doSearch(SearchType.RETURN, getRouteInfosRT());
		log.info("Doing international search for oneway");
		doSearch(SearchType.ONEWAY, getRouteInfosIntl());
		log.info("Doing international search for return");
		doSearch(SearchType.ONEWAY, getRouteInfosRTIntl());
	}


	public void doSearch(SearchType type, List<RouteInfo> routeInfos) {
		List<Future<AirSearchResult>> futureTaskList = new ArrayList<>();
		try {
			int num = 0;
			do {
				num++;
				Future future = ExecutorUtils.getFlightSearchThreadPool().submit(() -> {
					AirSearchQuery searchQuery = new AirSearchQuery();
					searchQuery.setRouteInfos(routeInfos);
					searchQuery.setTimeout(60L);
					searchQuery.setIsLiveSearch(true);
					searchQuery.setSearchType(type);
					searchQuery.setPaxInfo(getPaxInfo());
					searchQuery.setRequestId("SYSTEM_SEARCH");
					searchQuery.setSearchId(StringUtils.join("SYSTEM_SEARCH", RandomStringUtils.randomNumeric(3)));
					searchQuery.setSearchModifiers(new AirSearchModifiers());
					searchQuery.getSearchModifiers().setStoreSearchLog(true);
					searchQuery.setSourceIds(getSourceIds());
					AirUtils.populateMissingParametersInAirSearchQuery(searchQuery);
					searchManager.doSearch(searchQuery, contextData);
				});
				futureTaskList.add(future);
			} while (num < 5);
		} catch (Exception e) {
			log.error("Error occured in search {}", e);
		}

		long startTime = System.currentTimeMillis();

		for (int index = 0; index < futureTaskList.size(); index++) {
			try {
				long timeout = startTime + 60 * 1000 - System.currentTimeMillis();
				if (timeout < 0) {
					timeout = 2000;
				}
				log.info("SystemSearch Thread running for {}", index);
				futureTaskList.get(index).get(timeout, TimeUnit.MILLISECONDS);
			} catch (Exception e) {
				log.error("Error occured in search thread  {}", e);
			} finally {
				log.info("SystemSearch Thread completed for {}", index);
			}
		}
	}

	private List<Integer> getSourceIds() {
		List<Integer> sourceIds = new ArrayList<>();
		sourceIds.add(AirSourceType.SABRE.getSourceId());
		sourceIds.add(AirSourceType.MYSTIFLY.getSourceId());
		sourceIds.add(AirSourceType.TRUJET.getSourceId());
		sourceIds.add(AirSourceType.GOAIR.getSourceId());
		sourceIds.add(AirSourceType.AMADEUS.getSourceId());
		sourceIds.add(AirSourceType.SPICEJET.getSourceId());
		sourceIds.add(AirSourceType.INDIGO.getSourceId());
		sourceIds.add(AirSourceType.AIRASIA.getSourceId());
		sourceIds.add(AirSourceType.TRAVELPORT.getSourceId());
		sourceIds.add(AirSourceType.FLYDUBAI.getSourceId());
		return sourceIds;
	}

	private Map<PaxType, Integer> getPaxInfo() {
		Map<PaxType, Integer> paxInfo = new HashMap<>();
		paxInfo.put(PaxType.ADULT, 1);
		return paxInfo;
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public List<RouteInfo> getRouteInfos() {
		List<RouteInfo> routeInfos = new ArrayList<>();
		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setFromCityOrAirport(new AirportInfo().setCode("BLR"));
		routeInfo.setToCityOrAirport(new AirportInfo().setCode("DEL"));
		routeInfo.setTravelDate(LocalDate.now().plusMonths(2));
		routeInfos.add(routeInfo);
		return routeInfos;
	}

	public List<RouteInfo> getRouteInfosRT() {
		List<RouteInfo> routeInfos = new ArrayList<>();
		RouteInfo routeInfo1 = new RouteInfo();
		routeInfo1.setFromCityOrAirport(new AirportInfo().setCode("BOM"));
		routeInfo1.setToCityOrAirport(new AirportInfo().setCode("DEL"));
		routeInfo1.setTravelDate(LocalDate.now().plusMonths(2));


		RouteInfo routeInfo2 = new RouteInfo();
		routeInfo2.setFromCityOrAirport(new AirportInfo().setCode("DEL"));
		routeInfo2.setToCityOrAirport(new AirportInfo().setCode("BOM"));
		routeInfo2.setTravelDate(LocalDate.now().plusMonths(3));

		routeInfos.add(routeInfo1);
		routeInfos.add(routeInfo2);
		return routeInfos;
	}

	public List<RouteInfo> getRouteInfosIntl() {
		List<RouteInfo> routeInfos = new ArrayList<>();
		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setFromCityOrAirport(new AirportInfo().setCode("BOM"));
		routeInfo.setToCityOrAirport(new AirportInfo().setCode("DXB"));
		routeInfo.setTravelDate(LocalDate.now().plusMonths(2));
		routeInfos.add(routeInfo);
		return routeInfos;
	}

	public List<RouteInfo> getRouteInfosRTIntl() {
		List<RouteInfo> routeInfos = new ArrayList<>();
		RouteInfo routeInfo1 = new RouteInfo();
		routeInfo1.setFromCityOrAirport(new AirportInfo().setCode("BOM"));
		routeInfo1.setToCityOrAirport(new AirportInfo().setCode("LHR"));
		routeInfo1.setTravelDate(LocalDate.now().plusMonths(2));

		RouteInfo routeInfo2 = new RouteInfo();
		routeInfo2.setFromCityOrAirport(new AirportInfo().setCode("LHR"));
		routeInfo2.setToCityOrAirport(new AirportInfo().setCode("BOM"));
		routeInfo2.setTravelDate(LocalDate.now().plusMonths(3));

		routeInfos.add(routeInfo1);
		routeInfos.add(routeInfo2);
		return routeInfos;
	}
}

