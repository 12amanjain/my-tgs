package com.tgs.services.fms.servicehandler;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.fms.restmodel.AirlineInfoResponse;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.jparepository.AirlineInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Objects;
import java.util.Optional;

@Service
public class AirlineInfoHandler extends ServiceHandler<AirlineInfo, AirlineInfoResponse> {

	@Autowired
	AirlineInfoService airlineInfoService;


	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request.getCode()) || StringUtils.isBlank(request.getName())) {
			throw new CustomGeneralException("Code or Name cannot be blank");
		}
	}

	@Override
	public void process() throws Exception {
		com.tgs.services.fms.dbmodel.DbAirlineInfo dbAirlineInfo = null;

		if (Objects.nonNull(request.getCode())) {
			dbAirlineInfo = airlineInfoService.findByCode(request.getCode());
		}

		dbAirlineInfo = Optional.ofNullable(dbAirlineInfo).orElse(new com.tgs.services.fms.dbmodel.DbAirlineInfo())
				.from(request);
		dbAirlineInfo = airlineInfoService.save(dbAirlineInfo);
	}

	@Override
	public void afterProcess() throws Exception {
		response.getAirportInfos().add(request);
	}
}
