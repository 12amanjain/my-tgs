package com.tgs.services.fms.servicehandler;

import java.util.Objects;
import java.util.Optional;
import com.tgs.services.base.CustomGeneralException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.fms.jparepository.AirportInfoService;
import com.tgs.services.fms.restmodel.AirportInfoResponse;

@Service
public class AirportInfoHandler extends ServiceHandler<AirportInfo, AirportInfoResponse> {

	@Autowired
	AirportInfoService airportService;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request.getCode()) || StringUtils.isBlank(request.getName())) {
			throw new CustomGeneralException("Code or Name cannot be blank");
		}
	}

	@Override
	public void process() throws Exception {
		com.tgs.services.fms.dbmodel.AirportInfo dbAirportInfo = null;
		if (Objects.nonNull(request.getId())) {
			dbAirportInfo = airportService.findById(request.getId());
		}
		if (Objects.nonNull(request.getCode())) {
			dbAirportInfo = airportService.findByCode(request.getCode());
		}

		dbAirportInfo =
				Optional.ofNullable(dbAirportInfo).orElse(new com.tgs.services.fms.dbmodel.AirportInfo()).from(request);
		dbAirportInfo = airportService.save(dbAirportInfo);
		request.setId(dbAirportInfo.getId());
	}

	@Override
	public void afterProcess() throws Exception {
		response.getAirportInfos().add(request);
	}


}
