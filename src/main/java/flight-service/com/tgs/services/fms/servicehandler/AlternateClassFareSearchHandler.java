package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.mapper.TripInfoToAnalyticsTripMapper;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassFareSearchRequest;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassResponse;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class AlternateClassFareSearchHandler
		extends ServiceHandler<AlternateClassFareSearchRequest, AlternateClassResponse> {

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	private AlternateClassManager classManager;

	@Autowired
	private FlightCacheHandler cacheHandler;

	@Autowired
	private AirSearchResultProcessingManager processingManager;

	@Autowired
	private GeneralServiceCommunicator gmsComm;

	@Autowired
	private AirAnalyticsHelper analyticsHelper;

	private List<TripInfo> tripInfos;

	@Override
	public void beforeProcess() {
		tripInfos = new ArrayList<>();
	}

	@Override
	public void process() {

		User user = SystemContextHolder.getContextData().getUser();
		List<Future<TripInfo>> futureTaskList = new ArrayList<>();
		log.info("Change Class Search for {} priceIds {}", request.getFareClasses());

		try {
			request.getFareClasses().forEach(trip -> {
				TripInfo tripInfo = cachingService.fetchValue(trip.getPriceId(), TripInfo.class,
						CacheSetName.PRICE_INFO.getName(), BinName.PRICEINFOBIN.getName());
				if (tripInfo != null) {
					AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, trip.getPriceId());
					String searchId = cacheHandler.getSearchQueryKey(trip.getPriceId());
					tripInfo.setId(searchId);
					classManager.updateNewClassToSearch(tripInfo, trip.getClassData());
					futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
							.submit(() -> classManager.getAlternateClassFare(tripInfo, searchId, trip.getClassData())));
				}
			});

			for (Future<TripInfo> task : futureTaskList) {
				try {
					TripInfo tripInfo = task.get(60, TimeUnit.SECONDS);
					AirSearchQuery searchQuery = AirUtils.getSearchQueryFromTripInfo(tripInfo);
					AirSourceConfigurationOutput supplierConfig = AirUtils.getAirSourceConfigOnTripInfo(tripInfo, user);
					processingManager.processTripInfo(tripInfo, searchQuery, user, supplierConfig);
					cacheHandler.processAndStore(Arrays.asList(tripInfo), tripInfo.getId(), null);

					GeneralBasicFact fact = GeneralBasicFact.builder().build();
					fact.generateFact(user.getRole());
					FareBreakUpConfigOutput configOutput =
							gmsComm.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);
					AirUtils.setTripPriceInfoFromSegmentPriceInfo(tripInfo, user, configOutput);

					AirUtils.setProcessedTripInfo(tripInfo, searchQuery,
							SystemContextHolder.getContextData().getUser());
					removeSegmentFareDetail(tripInfo);
					tripInfos.add(tripInfo);
				} catch (NoSeatAvailableException | ExecutionException | InterruptedException e) {
					log.error(AirSourceConstants.AIR_SUPPLIER_CHANGE_CLASS_THREAD, e);
					throw e;
				}
			}
		} catch (Exception e) {
			throw new CustomGeneralException(SystemError.ALTERNATE_CLASS_PAX_MISMATCH);
		}

	}

	@Override
	public void afterProcess() {
		response.setTripInfos(tripInfos);
		addTripsToAnalytics(tripInfos);
	}

	public void removeSegmentFareDetail(TripInfo tripInfo) {
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
				for (Iterator<PaxType> iter = priceInfo.getFareDetails().keySet().iterator(); iter.hasNext();) {
					FareDetail fd = priceInfo.getFareDetails().get(iter.next());
					fd.setAddlFareComponents(null);
					fd.setFareComponents(null);
				}
			}
		}
	}

	protected void addTripsToAnalytics(List<TripInfo> tripInfos) {
		try {
			TripInfoToAnalyticsTripMapper tripMapper =
					TripInfoToAnalyticsTripMapper.builder().contextData(SystemContextHolder.getContextData())
							.user(SystemContextHolder.getContextData().getUser()).tripInfos(tripInfos)
							.errorMessages(SystemContextHolder.getMessageLogger().getMessages()).build();
			analyticsHelper.pushToAnalytics(tripMapper, AirAnalyticsType.CHANGE_CLASS_FARE_SEARCH);
		} catch (Exception e) {
			log.error("Failed to Push Analytics CHANGE_CLASS_FARE_SEARCH {}", request.getFareClasses(), e);
		}
	}
}
