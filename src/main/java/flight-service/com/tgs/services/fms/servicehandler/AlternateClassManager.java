package com.tgs.services.fms.servicehandler;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import java.util.Map;

@Slf4j
@Service
public class AlternateClassManager {

	public TripInfo getAvailableAlternateClass(TripInfo tripInfo, String searchId) {
		tripInfo.splitTripInfo(false).forEach(trip -> {
			AirSourceConfigurationOutput sourceConfiguration =
					AirUtils.getAirSourceConfigOnTripInfo(trip, SystemContextHolder.getContextData().getUser());
			if (MapUtils.isNotEmpty(sourceConfiguration.getIsChangeClassAllowed())
					&& BooleanUtils.isTrue((Boolean) MapUtils.getObject(sourceConfiguration.getIsChangeClassAllowed(),
							AirUtils.getAirType(tripInfo)))) {
				PriceInfo priceInfo = trip.getSegmentInfos().get(0).getPriceInfo(0);
				AirSourceType airSourceType =
						AirSourceType.getAirSourceType(priceInfo.getSupplierBasicInfo().getSourceId());
				SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
						priceInfo.getSupplierBasicInfo().getSourceId(), priceInfo.getSupplierBasicInfo().getRuleId());
				AirSearchQuery searchQuery = AirUtils.getSearchQueryFromTripInfo(trip);
				AbstractAirInfoFactory factory = airSourceType.getFactoryInstance(searchQuery, supplierConf);
				factory.getAlternateClass(trip, searchId);
			}
		});
		return tripInfo;
	}

	public void updateNewClassToSearch(TripInfo tripInfo, Map<Integer, String> classData) {
		if (MapUtils.isNotEmpty(classData)) {
			classData.forEach((segmentIndex, alterNateClass) -> {
				if (segmentIndex < tripInfo.getSegmentInfos().size()) {
					SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
					PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
					priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
						fareDetail.setClassOfBooking(alterNateClass);
					}));
				}
			});
		}
	}

	public TripInfo getAlternateClassFare(TripInfo tripInfo, String searchId, Map<Integer, String> classData) {
		PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
		try {
			if (MapUtils.isNotEmpty(classData)) {
				AirSourceConfigurationOutput sourceConfiguration =
						AirUtils.getAirSourceConfigOnTripInfo(tripInfo, SystemContextHolder.getContextData().getUser());
				if (MapUtils.isNotEmpty(sourceConfiguration.getIsChangeClassAllowed()) && BooleanUtils
						.isTrue((Boolean) MapUtils.getObject(sourceConfiguration.getIsChangeClassAllowed(),
								AirUtils.getAirType(tripInfo)))) {
					AirSourceType airSourceType =
							AirSourceType.getAirSourceType(priceInfo.getSupplierBasicInfo().getSourceId());
					SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(
							priceInfo.getSupplierBasicInfo().getSourceId(),
							priceInfo.getSupplierBasicInfo().getRuleId());
					AbstractAirInfoFactory factory = airSourceType.getFactoryInstance(null, supplierConf);
					return factory.getAlternateClassFare(tripInfo, searchId);
				}
			}
		} catch (NoSeatAvailableException nos) {
			throw nos;
		}
		return tripInfo;
	}
}
