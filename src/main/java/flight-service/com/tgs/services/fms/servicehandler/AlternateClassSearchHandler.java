package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.mapper.TripInfoToAnalyticsTripMapper;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassRequest;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassResponse;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class AlternateClassSearchHandler extends ServiceHandler<AlternateClassRequest, AlternateClassResponse> {

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	private FlightCacheHandler cacheHandler;

	@Autowired
	private AlternateClassManager classManager;

	@Autowired
	private AirAnalyticsHelper analyticsHelper;

	private List<TripInfo> tripInfos;

	@Override
	public void beforeProcess() {
		tripInfos = new ArrayList<>();
	}

	@Override
	public void process() {


		List<Future<TripInfo>> futureTaskList = new ArrayList<>();
		log.info("Change Class Search for priceIds {}", request.getPriceIds());

		request.getPriceIds().forEach(priceId -> {
			TripInfo tripInfo = cachingService.fetchValue(priceId, TripInfo.class, CacheSetName.PRICE_INFO.getName(),
					BinName.PRICEINFOBIN.getName());
			String searchId = cacheHandler.getSearchQueryKey(priceId);
			AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, priceId);
			futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
					.submit(() -> classManager.getAvailableAlternateClass(tripInfo, searchId)));
		});

		for (Future<TripInfo> task : futureTaskList) {
			try {
				tripInfos.add(task.get(60, TimeUnit.SECONDS));
			} catch (Exception e) {
				log.error(AirSourceConstants.AIR_SUPPLIER_CHANGE_CLASS_THREAD, e);
			}
		}
	}


	@Override
	public void afterProcess() {
		response.setTripInfos(tripInfos);
		addTripsToAnalytics(tripInfos);
	}

	protected void addTripsToAnalytics(List<TripInfo> tripInfos) {
		try {
			TripInfoToAnalyticsTripMapper tripMapper =
					TripInfoToAnalyticsTripMapper.builder().contextData(SystemContextHolder.getContextData())
							.user(SystemContextHolder.getContextData().getUser()).tripInfos(tripInfos)
							.errorMessages(SystemContextHolder.getMessageLogger().getMessages()).build();
			analyticsHelper.pushToAnalytics(tripMapper, AirAnalyticsType.CHANGE_CLASS_SEARCH);
		} catch (Exception e) {
			log.error("Failed to Push Analytics CHANGE_CLASS_SEARCH {}", request.getPriceIds(), e);
		}
	}
}
