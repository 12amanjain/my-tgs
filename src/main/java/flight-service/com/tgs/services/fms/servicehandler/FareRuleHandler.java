package com.tgs.services.fms.servicehandler;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.dbmodel.DbFareRuleInfo;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.jparepository.FareRuleService;
import com.tgs.services.fms.manager.FareRuleEngine;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.services.fms.restmodel.FareRuleRequest;
import com.tgs.services.fms.restmodel.FareRuleResponse;
import com.tgs.services.fms.restmodel.FareRuleTripResponse;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.ResourceNotFoundException;

@Service
public class FareRuleHandler
		extends ServiceHandler<com.tgs.services.fms.datamodel.farerule.FareRuleInfo, FareRuleResponse> {

	@Autowired
	FareRuleService fareRuleService;

	@Autowired
	FareRuleEngine ruleEngine;

	@Autowired
	FareRuleManager ruleManager;

	@Autowired
	AirOrderItemCommunicator itemComm;

	@Autowired
	SyncService syncService;

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	private UserServiceCommunicator umsComm;

	@Override
	public void beforeProcess() throws Exception {
		DbFareRuleInfo rule = null;
		if (Objects.nonNull(request.getId())) {
			rule = fareRuleService.findById(request.getId());
		}
		rule = Optional.ofNullable(rule).orElse(new DbFareRuleInfo()).from(request);
		rule.setProcessedOn(LocalDateTime.now());
		rule = fareRuleService.save(rule);
		request.setId(rule.getId());
		syncService.sync("fms", rule.toDomain());
	}

	@Override
	public void process() throws Exception {}

	@Override
	public void afterProcess() throws Exception {
		response.getFareRuleInfos().add(request);
	}

	public BaseResponse deleteById(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbFareRuleInfo fareRuleInfo = fareRuleService.findById(id);
		if (Objects.nonNull(fareRuleInfo)) {
			fareRuleInfo.setDeleted(Boolean.TRUE);
			fareRuleInfo.setEnabled(Boolean.FALSE);
			fareRuleService.save(fareRuleInfo);
			syncService.sync("fms", null, "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse changeStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbFareRuleInfo fareRuleInfo = fareRuleService.findById(id);
		if (Objects.nonNull(fareRuleInfo)) {
			fareRuleInfo.setEnabled(status);
			fareRuleService.save(fareRuleInfo);
			syncService.sync("fms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	/**
	 * @implSpec : This Method will handle both pre booking & post booking based on flowtype
	 * @see FareRuleRequest
	 */
	public FareRuleTripResponse getFareRule(FareRuleRequest ruleRequest) {
		FareRuleTripResponse fareRule = new FareRuleTripResponse();
		boolean isDetailed = BooleanUtils.isTrue(ruleRequest.getIsDetailed());
		if (Objects.isNull(ruleRequest.getFlowType()))
			return fareRule;
		if (ruleRequest.getFlowType().equals(AirFlowType.SEARCH)
				|| ruleRequest.getFlowType().equals(AirFlowType.REVIEW)) {
			try {
				TripInfo tripInfo = cachingService.fetchValue(ruleRequest.getId(), TripInfo.class,
						CacheSetName.PRICE_INFO.getName(), BinName.PRICEINFOBIN.getName());
				AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, ruleRequest.getId());
				return ruleManager.getFareRule(tripInfo, ruleRequest.getId().split(FlightCacheHandler.DELIM)[0],
						ruleRequest.getFlowType(), isDetailed, SystemContextHolder.getContextData().getUser());
			} catch (Exception e) {
				throw new ResourceNotFoundException(SystemError.KEYS_EXPIRED);
			}
		} else if (ruleRequest.getFlowType().equals(AirFlowType.BOOKING_DETAIL)) {
			List<TripInfo> tripInfos = itemComm.findTripByBookingId(ruleRequest.getId());
			if (CollectionUtils.isEmpty(tripInfos)) {
				throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
			} else {
				Order order = itemComm.getOrderByBookingId(ruleRequest.getId());
				User bookingUser = umsComm.getUserFromCache(order.getBookingUserId());
				return ruleEngine.getFareRule(tripInfos, ruleRequest.getId(), ruleRequest.getFlowType(), isDetailed,
						bookingUser);
			}
		}

		return fareRule;
	}

}
