package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.PriceDetailsRequest;
import com.tgs.services.fms.restmodel.PriceDetailsResponse;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PriceDetailsHandler extends ServiceHandler<PriceDetailsRequest, PriceDetailsResponse> {

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		try {
			List<TripInfo> tripInfos = new ArrayList<>();
			for (String priceId : request.getPriceIds()) {
				TripInfo tripInfo = cachingService.fetchValue(priceId, TripInfo.class, CacheSetName.PRICE_INFO.name(),
						BinName.PRICEINFOBIN.getName());
				if (Objects.nonNull(tripInfo)) {
					AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, priceId);
					tripInfo.setProcessedTripInfo(AirUtils.getProcessedTripInfo(tripInfo));
					tripInfo.setTripPriceInfos(AirUtils.getTripTotalPriceInfoList(tripInfo, null));
					tripInfos.add(tripInfo);
				}
			}
			SystemContextHolder.getContextData().setExclusionStrategys(Arrays.asList(
					new FieldExclusionStrategy(null, Arrays.asList("messages", "id", "processedTripInfo", "sI"))));
			response.setTripInfos(tripInfos);
		} catch (Exception e) {
			throw new ResourceNotFoundException(SystemError.KEYS_EXPIRED);
		}

	}

	@Override
	public void afterProcess() throws Exception {

	}

}
