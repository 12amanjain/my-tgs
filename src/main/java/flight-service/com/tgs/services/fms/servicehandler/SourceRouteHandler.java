package com.tgs.services.fms.servicehandler;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;

import com.tgs.services.fms.restmodel.SourceRouteRequest;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SourceRouteHandler extends ServiceHandler<SourceRouteRequest, BaseResponse> {

	@Autowired
	SourceRouteInfoService sourceRouteInfoService;

	private static final String SEARCH_KEY = "ROUTES";


	@Override
	public void beforeProcess() throws Exception {
		if (request == null || CollectionUtils.isEmpty(request.getSourceIds())) {
			throw new CustomGeneralException(SystemError.INVALID_REQUEST);
		}
	}

	@Override
	public void process() throws Exception {
		for (Integer sourceId : request.getSourceIds()) {
			List<SourceRouteInfo> routes = new ArrayList<>();
			AirSourceType airSourceType = AirSourceType.getAirSourceType(sourceId);
			List<SupplierInfo> supplierInfos = SupplierConfigurationHelper.getSupplierForSourceId(sourceId.toString());
			if (CollectionUtils.isNotEmpty(supplierInfos)) {
				String supplierId = supplierInfos.get(0).getName();
				SupplierConfiguration supplierConf = SupplierConfigurationHelper.getSupplierConfiguration(supplierId);
				if (supplierConf != null) {
					AbstractAirInfoFactory factory = airSourceType.getFactoryInstance(null, supplierConf);
		            routes = factory.addNewRoutesToSystem(routes, SEARCH_KEY);
					log.info("Adding Routes {} routes size {} ", sourceId, CollectionUtils.size(routes));
					if (CollectionUtils.isNotEmpty(routes)) {
						List<DbSourceRouteInfo> dbSourceList = new ArrayList<>();
						for (SourceRouteInfo routeInfo : routes) {
							dbSourceList.add(new DbSourceRouteInfo().from(routeInfo));
						}
					    sourceRouteInfoService.addAll(dbSourceList);
					}
				}
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
