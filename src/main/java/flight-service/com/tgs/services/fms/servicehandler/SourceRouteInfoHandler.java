package com.tgs.services.fms.servicehandler;

import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.dbmodel.DbSourceRouteInfo;
import com.tgs.services.fms.jparepository.SourceRouteInfoService;
import com.tgs.services.fms.restmodel.SourceRouteInfoResponse;

@Service
public class SourceRouteInfoHandler extends ServiceHandler<SourceRouteInfo, SourceRouteInfoResponse> {


	@Autowired
	SourceRouteInfoService routeInfoService;

	@Override
	public void beforeProcess() throws Exception {
		DbSourceRouteInfo dbSourceRouteInfo = null;
		if (Objects.nonNull(request.getId())) {
			dbSourceRouteInfo = routeInfoService.findOne(request.getId());
		}
		dbSourceRouteInfo = Optional.ofNullable(dbSourceRouteInfo).orElse(new DbSourceRouteInfo()).from(request);
		dbSourceRouteInfo = routeInfoService.save(dbSourceRouteInfo);
		request.setId(dbSourceRouteInfo.getId());

	}

	@Override
	public void process() throws Exception {
		response.getSourceRouteInfos().add(request);

	}

	@Override
	public void afterProcess() throws Exception {

	}


}
