package com.tgs.services.fms.servicehandler;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.base.utils.FieldTransform;
import com.tgs.services.fms.datamodel.supplier.SupplierConfigurationFilter;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.datamodel.supplier.SupplierRuleFilter;
import com.tgs.services.fms.dbmodel.DbSupplierInfo;
import com.tgs.services.fms.dbmodel.DbSupplierRule;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.jparepository.SupplierInfoService;
import com.tgs.services.fms.jparepository.SupplierRuleService;
import com.tgs.services.fms.restmodel.SupplierInfoResponse;

import com.tgs.utils.common.UpdateMaskedField;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SupplierCredentialHandler extends ServiceHandler<SupplierInfo, SupplierInfoResponse> {

	@Autowired
	SupplierInfoService supplierService;

	@Autowired
	SupplierRuleService supplierRuleService;

	@Autowired
	SyncService syncService;
	
	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		DbSupplierInfo supplierDbModel = null;
		if (request.getId() != null) {
			supplierDbModel = supplierService.findById(request.getId());
			UpdateMaskedField.update(request);
		}
		supplierDbModel = Optional.ofNullable(supplierDbModel).orElseGet(() -> new DbSupplierInfo()).from(request);
		supplierDbModel = supplierService.save(supplierDbModel);
		request = supplierDbModel.toDomain();
		syncService.sync("fms", supplierDbModel.toDomain());
	}

	@Override
	public void afterProcess() throws Exception {
		FieldTransform.mask(request);
		response.getSupplierInfos().add(request);
	}

	public List<SupplierRule> getSupplierRules(SupplierRuleFilter ruleFilter) {
		log.info("Fetching supplier rule from database ");
		List<DbSupplierRule> supplierRules = supplierRuleService.findAll(ruleFilter);
		log.info("fetched supplier rule from database ");
		List<SupplierRule> rules = DbSupplierRule.toDomainList(supplierRules);
		log.info("converted from dbmodel to datamodel");
		return rules;
	}

	public SupplierRule saveSupplierRule(SupplierRule supplierRule) {
		DbSupplierRule supplierRuleDbModel = null;
		supplierRule.setExitOnMatch(false);
		if (Objects.nonNull(supplierRule.getId())) {
			supplierRuleDbModel = supplierRuleService.findById(supplierRule.getId());
		}
		supplierRuleDbModel =
				Optional.ofNullable(supplierRuleDbModel).orElseGet(() -> new DbSupplierRule()).from(supplierRule);

		supplierRuleDbModel.setSourceId(
				SupplierConfigurationHelper.getSupplierInfo(supplierRuleDbModel.getSupplierId()).getSourceId());
		supplierRule = supplierRuleService.save(supplierRuleDbModel).toDomain();
		syncService.sync("fms", supplierRule);
		return supplierRule;

	}

	public BaseResponse deleteSupplierRule(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierRule supplierRule = supplierRuleService.findById(id);
		if (Objects.nonNull(supplierRule)) {
			supplierRule.setDeleted(Boolean.TRUE);
			supplierRule.setEnabled(Boolean.FALSE);
			supplierRuleService.save(supplierRule);
			syncService.sync("fms", null, "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateSupplierRule(Long id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierRule supplierRule = supplierRuleService.findById(id);
		if (Objects.nonNull(supplierRule)) {
			supplierRule.setEnabled(status);
			supplierRuleService.save(supplierRule);
			syncService.sync("fms", supplierRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public SupplierInfoResponse getSupplierInfo(SupplierConfigurationFilter configurationFilter) {
		SupplierInfoResponse supplierInfoResponse = new SupplierInfoResponse();
		supplierService.findAll(configurationFilter).forEach(supplier -> {
			supplierInfoResponse.getSupplierInfos().add(supplier.toDomain());
		});
		return FieldTransform.mask(supplierInfoResponse);
	}

	public BaseResponse deleteSupplierInfo(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierInfo supplierInfo = supplierService.findById(id);
		if (Objects.nonNull(supplierInfo)) {
			supplierInfo.setEnabled(Boolean.FALSE);
			supplierInfo.setDeleted(Boolean.TRUE);
			supplierService.save(supplierInfo);
			syncService.sync("fms", null, "DELETE");
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateStatusSupplierInfo(Long id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbSupplierInfo supplierInfo = supplierService.findById(id);
		if (Objects.nonNull(supplierInfo)) {
			supplierInfo.setEnabled(status);
			supplierService.save(supplierInfo);
			syncService.sync("fms", null);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
