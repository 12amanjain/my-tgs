package com.tgs.services.fms.servicehandler;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.manager.AirUserFeeManager;
import com.tgs.services.fms.mapper.PriceInfoToTotalFareDetailMapper;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.UserFeeUpdateRequest;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import com.tgs.utils.exception.CachingLayerException;
import com.tgs.utils.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserFeeUpdateHandler extends ServiceHandler<UserFeeUpdateRequest, BaseResponse> {

	@Autowired
	private AirUserFeeManager userFeeManager;

	@Autowired
	private UserServiceCommunicator userServiceComm;

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	private AirOrderItemCommunicator orderItemComm;

	@Autowired
	private OrderServiceCommunicator orderComm;

	@Override
	public void beforeProcess() throws Exception {

	}

	/**
	 * If the request is valid, flow-type is used to recognize from where this service is being called and received User
	 * Fee information is used to update User Fee in appropriate places using given id.<br>
	 * {@code AirFlowType} is used to recognize
	 * <ul>
	 * <li>id as priceId or bookingId,</li>
	 * <li>appropriate place (TripInfo, AirOrderitem, etc) for update.</li>
	 * </ul>
	 * <br>
	 * 
	 * @throws Exception if any of data (id, userFee or flow-type) in request is null or if flow-type doen't exist.
	 */
	@Override
	public void process() throws Exception {
		String id = request.getId();
		Double userFee = request.getValue();
		AirFlowType flowType = request.getFlowType();
		UserFeeType feeType = UserFeeType.getEnumFromCode(request.getFeeType());

		if (StringUtils.isBlank(id) || userFee == null || flowType == null || feeType == null
				|| !feeType.allowedFlowTypes().contains(flowType)) {
			throw new CustomGeneralException(SystemError.BAD_REQUEST);
		}

		if (userFee < 0) {
			throw new CustomGeneralException(SystemError.BAD_REQUEST, "User Fee must be non-negative");
		}

		try {
			User user = null;
			switch (flowType) {
				case SEARCH:
					SystemContextHolder.getContextData().getReqIds().add(id.split(FlightCacheHandler.DELIM)[0]);
					user = SystemContextHolder.getContextData().getUser();
					updateUserFeeForTrip(user, userFee, feeType, id);
					break;

				case REVIEW:
					SystemContextHolder.getContextData().getReqIds().add(id);
					user = SystemContextHolder.getContextData().getUser();
					updateUserFeeForAirReview(user, userFee, feeType, id);
					break;

				case BOOKING_DETAIL:
					SystemContextHolder.getContextData().getReqIds().add(id);
					updateUserFeeForAirOrderItems(userFee, feeType, id);
					break;
			}
		} catch (Exception e) {
			log.error("Updating User Fee failed for id {} {}", id, e);
			throw e;
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	/**
	 * Update User Fee for {@code TripInfo} corresponding to given priceId.
	 * 
	 * @param userFee
	 * @param priceId
	 * @param feeType
	 */
	private void updateUserFeeForTrip(User user, Double userFee, UserFeeType feeType, String priceId) {
		TripInfo tripInfo = getSearchTripInfo(priceId);
		userFeeManager.updateUserFee(user, userFee, feeType, tripInfo.splitTripInfo(true));
		cachingService.store(priceId, BinName.PRICEINFOBIN.getName(), tripInfo,
				CacheMetaInfo.builder().set(CacheSetName.PRICE_INFO.name()).compress(true).plainData(false)
						.expiration(CacheType.PRICEIDKEYATSEARCH.getTtl()).build());
	}

	private TripInfo getSearchTripInfo(String priceId) {
		TripInfo tripInfo = null;
		try {
			tripInfo = cachingService.getTripInfo(priceId);
		} catch (CachingLayerException e) {
			throw new ResourceNotFoundException(SystemError.KEYS_EXPIRED);
		}
		return tripInfo;
	}

	/**
	 * Update User Fee for tripInfos in {@code AirReviewResponse} corresponding to given bookingId.
	 * 
	 * @param userFee
	 * @param feeType
	 * @param bookingId
	 */
	private void updateUserFeeForAirReview(User user, Double userFee, UserFeeType feeType, String bookingId) {
		try {
			AirReviewResponse airReviewResponse = cachingService.getAirReviewResponse(bookingId);
			List<TripInfo> tripInfos = airReviewResponse.getTripInfos();
			userFeeManager.updateUserFee(user, userFee, feeType, tripInfos);

			PriceInfoToTotalFareDetailMapper toTotalFareDetailMapper = PriceInfoToTotalFareDetailMapper.builder()
					.addMarkupInTF(false).paxInfo(airReviewResponse.getSearchQuery().getPaxInfo())
					.priceInfos(AirUtils.getPriceInfos(tripInfos)).build();
			airReviewResponse.setTotalPriceInfo(toTotalFareDetailMapper.convert());

			cachingService.store(bookingId, BinName.AIRREVIEW.getName(), airReviewResponse,
					CacheMetaInfo.builder().set(CacheSetName.AIR_REVIEW.name()).compress(true).plainData(false)
							.expiration(CacheType.REVIEWKEYEXPIRATION.getTtl()).build());
		} catch (CachingLayerException e) {
			throw new ResourceNotFoundException(SystemError.KEYS_EXPIRED);
		}
	}

	/**
	 * Update User Fee for airOrderItems corresponding to given bookingId.
	 * 
	 * @param userFee
	 * @param feeType
	 * @param bookingId
	 */
	private void updateUserFeeForAirOrderItems(Double userFee, UserFeeType feeType, String bookingId) {
		// List<AirOrderItem> orderItems = orderItemManager.findTripByBookingId(bookingId);
		// Order order = orderManager.findByBookingId(bookingId, new Order());
		// List<SegmentInfo> segmentInfos = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(orderItems, null);
		Order order = orderComm.findByBookingId(bookingId);
		List<TripInfo> tripInfos = orderItemComm.findTripByBookingId(bookingId);
		User bookingUser = userServiceComm.getUserFromCache(order.getBookingUserId());
		userFeeManager.updateUserFee(bookingUser, userFee, feeType, tripInfos);
		orderItemComm.updateOrderAndItem(AirUtils.getSegmentInfos(tripInfos), orderComm.findByBookingId(bookingId),
				null);
	}
}
