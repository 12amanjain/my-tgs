package com.tgs.services.fms.sources;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.analytics.AnalyticsAirQuery;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.AirBookingEngine;
import com.tgs.services.fms.mapper.AirBookToAnalyticsAirBookMapper;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.ImportPNRNotAllowedException;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierPaymentException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Service
@NoArgsConstructor
@Slf4j
public abstract class AbstractAirBookingFactory {

	protected SupplierConfiguration supplierConf;

	protected String bookingId;

	protected BookingSegments bookingSegments;

	protected GstInfo gstInfo;

	protected boolean isHoldBooking;

	protected String pnr;

	protected Order order;

	protected DeliveryInfo deliveryInfo;

	protected User bookingUser;

	@Value("${env}")
	private String env;

	@Value("${pbuid}")
	private String prodBookingUserId;

	protected boolean isTkRequired;

	protected SupplierSession supplierSession;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchangeComm;

	@Autowired
	protected CommercialCommunicator commercialCommunicator;

	@Autowired
	protected AirOrderItemCommunicator itemComm;

	@Autowired
	protected OrderServiceCommunicator orderComm;

	@Autowired
	protected GeneralCachingCommunicator cachingComm;

	@Autowired
	protected UserServiceCommunicator userComm;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected List<String> criticalMessageLogger;

	protected boolean isThreadInterrupted;

	@Autowired
	protected AirAnalyticsHelper analyticsHelper;

	protected List<AnalyticsAirQuery> analyticInfos;

	public AbstractAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		this.supplierConf = supplierConf;
		this.bookingId = order.getBookingId();
		this.bookingSegments = bookingSegments;
		this.order = order;
		deliveryInfo = AirSupplierUtils.getDeliveryDetails(order.getDeliveryInfo());
		orderComm = SpringContext.getApplicationContext().getBean(OrderServiceCommunicator.class);
		GstInfo gstInfo = orderComm.getGstInfo(order.getBookingId());
		this.gstInfo = removeExtraCharsFromGstInfo(gstInfo);
		userComm = SpringContext.getApplicationContext().getBean(UserServiceCommunicator.class);
		bookingUser = userComm.getUserFromCache(order.getBookingUserId());
		sourceConfiguration = AirUtils.getAirSourceConfiguration(null, supplierConf.getBasicInfo(), bookingUser);
		isTkRequired =
				bookingSegments.getSegmentInfos().get(0).getFlightDesignator().getAirlineInfo().getIsTkRequired();
		supplierSession = bookingSegments.getSupplierSession();
		criticalMessageLogger = new ArrayList<>();
		userComm = SpringContext.getApplicationContext().getBean(UserServiceCommunicator.class);
		analyticInfos = new ArrayList<>();
	}

	private GstInfo removeExtraCharsFromGstInfo(GstInfo gstInfo) {
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getAddress())) {
			if (gstInfo.getAddress().length() > 70) {
				gstInfo.setAddress(gstInfo.getAddress().substring(0, 70));
			}
		}
		return gstInfo;
	}

	public boolean bookTrip(ContextData contextData) {
		log.info("Inside AbstractAirBookingFactory#bookTrip for bookingId {} Supplier {}", bookingId,
				supplierConf.getBasicInfo().getSupplierId());
		if (Thread.currentThread().isInterrupted()) {
			throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
		}
		this.isHoldBooking = getIsHoldBook(order);
		boolean isBooked = false;
		try {
			if (isBookingAllowed()) {
				isBooked = this.doBooking();
			} else {
				isBooked = true;
				pnr = getTestPnr();
			}
		} catch (CustomGeneralException e) {
			if (e.getError() == SystemError.INTERRUPRT_EXPECTION) {
				log.error("Thread received interrupted exception for booking {}", bookingId);
				isThreadInterrupted = true;
				if (StringUtils.isNotBlank(bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo()
						.getTravellerInfo().get(0).getPnr())) {
					String message = StringUtils.join("TimeOut Failed,CRS PNR ", bookingSegments.getSupplierBookingId(),
							"Airline PNR ", bookingSegments.getBookingSegmentsPNR(), ":SYSTEM GENERATED");
					log.error("Thread Interrupted for Booking PNR {} Airlinr PNR {}", bookingId,
							bookingSegments.getBookingSegmentsPNR());
					criticalMessageLogger.add(message);
				}
			}
		} catch (SupplierUnHandledFaultException se) {
			LogUtils.log(bookingId, "AirBook", se);
			criticalMessageLogger.add(se.getMessage());
			log.error(AirSourceConstants.BOOK_FAILED, bookingId, supplierConf.getSupplierId(), se);
		} catch (SOAPFaultException se) {
			LogUtils.log(bookingId, "AirBook", se);
			criticalMessageLogger.add(se.getMessage());
			log.error(AirSourceConstants.BOOK_FAILED, bookingId, supplierConf.getSupplierId(), se);
		} catch (SupplierRemoteException | SupplierPaymentException | ImportPNRNotAllowedException sE) {
			LogUtils.log(bookingId, "AirBook", sE);
			criticalMessageLogger.add(sE.getMessage());
			log.error(AirSourceConstants.BOOK_FAILED, bookingId, supplierConf.getSupplierId(), sE);
		} catch (Exception e) {
			LogUtils.log(bookingId, "AirBook", e);
			log.error(AirSourceConstants.BOOK_FAILED, bookingId, supplierConf.getSupplierId(), e);
			pnr = null;
		} finally {
			LogUtils.clearLogList();
			if (!isThreadInterrupted) {
				if (!isBooked && AirSourceType.DEALINVENTORY.getSourceId() != supplierConf.getSourceId()) {
					log.error("Booking failed for BookingID {} Block ,supplier {} airline {}, failure reason is {}",
							bookingId, supplierConf.getSupplierId(),
							bookingSegments.getSegmentInfos().get(0).getAirlineCode(false),
							String.join(",", criticalMessageLogger));
				}
				updatePNRAndStatus(isBooked);
			}
			if (isHoldBooking || !isBooked || CollectionUtils.isNotEmpty(criticalMessageLogger)) {
				addSupplierMessageToNotes(isBooked);
			}
			pushTimeLimitInfoToAnalytics();
		}

		if (requireConfirmBooking(isBooked)) {
			isBooked = confirmBook(contextData);
		}

		return isBooked;
	}

	private void updatePNRAndStatus(boolean isBooked) {
		if (BookingUtils.isValidPnr(pnr) && isBooked) {
			if (bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0)
					.getPnr() == null) {
				BookingUtils.updateAirlinePnr(bookingSegments.getSegmentInfos(), pnr);
			}
			if (isHoldBooking) {
				log.info("Blocked PNR {} for Booking ID {}", pnr, bookingId);
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, AirItemStatus.ON_HOLD);
			} else {
				log.info("Confirmation PNR {} for Booking ID {}, isTkRequired {}", pnr, bookingId, isTkRequired);
				AirItemStatus status = AirItemStatus.SUCCESS;
				if (isTkRequired) {
					status = AirItemStatus.TICKET_PENDING;
				}
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, status);
			}
		} else {
			if (isHoldBooking) {
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, AirItemStatus.HOLD_PENDING);
			} else {
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, AirItemStatus.PNR_PENDING);
			}
			criticalMessageLogger.add("Booking Confirmation Failed :SYSTEM GENERATED");
		}

		if (BookingUtils.isValidPnr(pnr) && !isBooked) {
			criticalMessageLogger.add(StringUtils.join("PNR Generated ", pnr, " But Failed to Confirm"));
		}
	}

	public boolean requireConfirmBooking(boolean isBooked) {
		if (isBooked && !isHoldBooking && isTkRequired) {
			return true;
		}
		return false;
	}

	public boolean confirmBook(ContextData contextData) {
		log.info("Inside AbstractAirBookingFactory#confirmBook for bookingId {}", bookingId);
		if (Thread.currentThread().isInterrupted()) {
			throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
		}
		this.isHoldBooking = getIsHoldBook(order);
		criticalMessageLogger = new ArrayList<>();
		boolean putToPending = AirBookingEngine.putToPending(bookingSegments, order, "TICKETING", gmsCommunicator,
				itemComm, bookingUser);
		if (putToPending) {
			return true;
		}

		boolean isBooked = false;
		if (StringUtils.isBlank(pnr))
			pnr = bookingSegments.getAirlinePNR();
		try {
			if (isBookingAllowed()) {
				isBooked = this.doConfirmBooking();
			} else {
				/**
				 * This is require for testing purpose where we don't have test test credentials from supplier
				 */
				isBooked = true;
				if (isTkRequired) {
					for (FlightTravellerInfo travellerInfo : bookingSegments.getSegmentInfos().get(0)
							.getBookingRelatedInfo().getTravellerInfo()) {
						BookingUtils.updateTicketNumber(bookingSegments.getSegmentInfos(), "11111111111",
								travellerInfo);
					}
				}
			}
		} catch (CustomGeneralException e) {
			if (e.getError() == SystemError.INTERRUPRT_EXPECTION) {
				log.error("Thread received interrupted exception for booking {}", bookingId);
				isThreadInterrupted = true;
				if (StringUtils.isNotBlank(
						bookingSegments.getSegmentInfos().get(0).getTravellerInfo().get(0).getTicketNumber())) {
					String message = StringUtils.join("TimeOut Failed,CRS PNR ", bookingSegments.getSupplierBookingId(),
							"TicketNumber PNR ", bookingSegments.getBookingTktNumber(), ":SYSTEM GENERATED");
					log.error("Thread Intrepputed Interrupted for Booking PNR {} tkt number {} ", bookingId,
							bookingSegments.getBookingTktNumber());
					criticalMessageLogger.add(message);
				}
			}
		} catch (SupplierUnHandledFaultException se) {
			criticalMessageLogger.add(se.getMessage());
			log.error(AirSourceConstants.BOOK_TICKETING_FAILED, bookingId, supplierConf.getSupplierId(), se);
		} catch (SOAPFaultException se) {
			criticalMessageLogger.add(se.getMessage());
			log.error(AirSourceConstants.BOOK_TICKETING_FAILED, bookingId, supplierConf.getSupplierId(), se);
		} catch (SupplierRemoteException | SupplierPaymentException sE) {
			criticalMessageLogger.add(sE.getMessage());
			log.error(AirSourceConstants.BOOK_TICKETING_FAILED, bookingId, supplierConf.getSupplierId(), sE);
		} finally {
			LogUtils.clearLogList();
			if (!isThreadInterrupted) {
				updateTktNumberAndStatus(isBooked);
			}
			addSupplierMessageToNotes(isBooked);
		}
		return isBooked;
	}

	protected void updateTktNumberAndStatus(boolean isBooked) {
		if (isBooked
				&& (!isTkRequired || (BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos())))) {
			itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, AirItemStatus.SUCCESS);
		} else {
			itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, AirItemStatus.CONFIRM_PENDING);
			criticalMessageLogger.add("Ticket Number Confirmation Failed :SYSTEM GENERATED");
			log.error("Booking failed for BookingID {} Ticketing ,supplier {}, airline {},failure reason is {}",
					bookingId, supplierConf.getSupplierId(),
					bookingSegments.getSegmentInfos().get(0).getAirlineCode(false),
					String.join(",", criticalMessageLogger));
		}
	}

	public boolean confirmFareBeforeTicket(ContextData contextData) {
		log.info("Inside AbstractAirBookingFactory#confirmFareBeforeTicket for bookingId {}", bookingId);
		boolean isFareChanged = false;
		boolean isFareValidateAllowed = true;
		try {
			if (sourceConfiguration != null && sourceConfiguration.getIsConfirmFareBeforeTicket() != null) {
				isFareValidateAllowed = BooleanUtils.isTrue(sourceConfiguration.getIsConfirmFareBeforeTicket());
			}
			if (isBookingAllowed() && isFareValidateAllowed) {
				isFareChanged = this.confirmBeforeTicket();
			}
		} catch (NoPNRFoundException | ImportPNRNotAllowedException | SupplierUnHandledFaultException
				| SupplierRemoteException e) {
			criticalMessageLogger.add(e.getMessage());
			log.error(AirSourceConstants.FARE_CHANGE_FAILED, bookingId, e);
		} catch (Exception e) {
			log.error(AirSourceConstants.FARE_CHANGE_FAILED, bookingId, e);
		} finally {
			LogUtils.clearLogList();
		}
		return isFareChanged;
	}

	protected boolean isSameDayTicketing() {
		LocalDate ticketingDate = LocalDate.now();
		LocalDate orderCreationDate = order.getCreatedOn().toLocalDate();
		return ticketingDate.isEqual(orderCreationDate);
	}

	public abstract boolean doConfirmBooking();

	public boolean confirmBeforeTicket() {
		return false;
	}

	public abstract boolean doBooking();

	public String getTestPnr() {
		return "TESTPNR";
	}

	public boolean isFareDiff(double amountToBePaidToAirline) {
		if (Thread.currentThread().isInterrupted()) {
			throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
		}
		double amountChargedFromCustomer =
				BookingUtils.getAmountChargedByAirline(getBookingSegments().getSegmentInfos());
		boolean isFareDiff = true;
		double fareDiff = BookingUtils.isFareDiff(amountChargedFromCustomer, amountToBePaidToAirline);
		double allowedDifference =
				getAllowedDifference(supplierConf, getBookingSegments().getSegmentInfos(), bookingUser);
		log.info("Total AmountToPay Supplier {} - amountCharged {} for booking {} ", amountToBePaidToAirline,
				amountChargedFromCustomer, bookingId);
		if (fareDiff < allowedDifference) {
			isFareDiff = false;
			log.info("BookingId {}, allowed fare diff : {}, fare diff : {}  ", bookingId, allowedDifference, fareDiff);
		} else {
			log.info("Fare Difference on Booking {} supplier {} amountCharged {} - amountTobePaid {} ", bookingId,
					supplierConf.getSupplierId(), amountChargedFromCustomer, amountToBePaidToAirline);
			String noteMessage = Note.getNoteMessage(null, StringUtils.join("amount to be paid - ",
					amountToBePaidToAirline, ". amount charged - ", amountChargedFromCustomer));
			Note note =
					Note.builder().bookingId(bookingId).noteType(NoteType.FAREJUMP).noteMessage(noteMessage).build();
			gmsCommunicator.addNote(note);
		}
		return isFareDiff;
	}

	public void updateTimeLimit(Calendar holdTimeLimit) {
		if (Objects.nonNull(holdTimeLimit)) {
			LocalDateTime holdTime = TgsDateUtils.calenderToLocalDateTime(holdTimeLimit);
			// hold time should not be previous then current date
			if (holdTime.isAfter(LocalDateTime.now())) {
				log.info("Hold Time limit for Booking id {} timelimit {} ", bookingId, holdTime);
				getBookingSegments().getSegmentInfos().forEach(segmentInfo -> {
					segmentInfo.setTimeLimit(holdTime);
				});
			} else {
				log.info("Wrong Time limit Received for Booking id {} timelimit {} ", bookingId, holdTimeLimit);
			}
		} else {
			log.info("No Time limit Received for Booking id {} timelimit {} ", bookingId, holdTimeLimit);
			List<SegmentInfo> segments = bookingSegments.getSegmentInfos();
			/*
			 * AnalyticsAirQuery query = AnalyticsAirQuery.builder().source(segments.get(0).getDepartureAirportCode())
			 * .destination(segments.get(segments.size() - 1).getArrivalAirportCode())
			 * .airlines(segments.get(0).getPlatingCarrier(null))
			 * .suppliername(supplierConf.getBasicInfo().getSupplierName())
			 * .supplierdesc(supplierConf.getBasicInfo().getDescription())
			 * .sourcename(supplierConf.getBasicInfo().getSupplierId())
			 * .analyticsType(AirAnalyticsType.TIME_LIMIT.name()).errormsg("No Time limit Received").build();
			 * analyticInfos.add(query);
			 */
		}
	}

	public static double getAllowedDifference(SupplierConfiguration supplierConf, List<SegmentInfo> segmentInfos,
			User bookingUser) {
		int paxCount = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().size();
		int tripCount = segmentInfos.size();
		double allowedDiff = 10 * paxCount * tripCount;
		AirSourceConfigurationOutput airSourceOutput =
				AirUtils.getAirSourceConfiguration(null, supplierConf.getBasicInfo(), bookingUser);
		if (airSourceOutput != null && Objects.nonNull(airSourceOutput.getAllowedFareDiff())) {
			allowedDiff = paxCount * tripCount * airSourceOutput.getAllowedFareDiff();
		}
		return allowedDiff;
	}

	public boolean isBookingAllowed() {
		return BooleanUtils.isTrue(this.getSupplierConf().getSupplierCredential().getIsTestCredential())
				|| "prod".equalsIgnoreCase(env)
				|| (prodBookingUserId != null && prodBookingUserId.contains(order.getBookingUserId() + ","));
	}

	protected void updateSupplierId(List<SegmentInfo> segmentInfos) {
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				priceInfo.setSupplierBasicInfo(supplierConf.getBasicInfo());
			});
		});

	}

	protected CreditCardInfo getSupplierBookingCreditCard() {
		PriceMiscInfo miscInfo = getBookingSegments().getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo();
		if (miscInfo.getCcInfoId() != null) {
			return commercialCommunicator.getCreditCardById(Long.valueOf(miscInfo.getCcInfoId()));
		}
		return null;
	}

	private boolean getIsHoldBook(Order order) {
		boolean isHoldBook = false;
		if (order.getAdditionalInfo() == null || order.getAdditionalInfo().getPaymentStatus() == null) {
			isHoldBook = true;
		} else if (order.getAdditionalInfo() != null && order.getAdditionalInfo().getPaymentStatus() != null
				&& !order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
			throw new CustomGeneralException("Booking Not Allowed, Due To Payment is Not Done!");
		}
		return isHoldBook;
	}

	protected void isLCCTkRequiredfromGDS() {
		isTkRequired = true;
	}

	public FlightAPIURLRuleCriteria getEndPointURL() {
		FlightAPIURLRuleCriteria flightUrls = AirUtils.getAirEndPointURL(sourceConfiguration);
		return flightUrls;
	}

	private void addSupplierMessageToNotes(boolean isBooked) {
		List<String> filteredCriticalMessages =
				AirSupplierUtils.filterCriticalMessage(criticalMessageLogger, isBooked, bookingUser);
		if (CollectionUtils.isNotEmpty(filteredCriticalMessages)) {
			String supplierMessage = String.join(",", filteredCriticalMessages);
			if (StringUtils.isNotBlank(supplierMessage)) {
				Note note = Note.builder().bookingId(bookingId).noteType(NoteType.SUPPLIER_MESSAGE)
						.noteMessage(supplierMessage).build();
				gmsCommunicator.addNote(note);
			}
		}
	}

	private void pushTimeLimitInfoToAnalytics() {
		try {
			if (CollectionUtils.isNotEmpty(analyticInfos)) {
				for (AnalyticsAirQuery query : analyticInfos) {
					AirBookToAnalyticsAirBookMapper analyticsMapper = AirBookToAnalyticsAirBookMapper.builder().build();
					analyticsMapper.setOutput(query);
					analyticsMapper.setBookingId(bookingId);
					analyticsMapper.setTripInfos(new ArrayList<>());
					analyticsMapper.setErrorMessage(query.getErrormsg());
					analyticsMapper.setOrder(order);
					analyticsHelper.pushToAnalytics(analyticsMapper,
							AirAnalyticsType.valueOf(query.getAnalyticstype()));
				}
			}
		} catch (Exception e) {
			log.error("Failed to Push Analytics Book {} cause ", bookingId, e);
		}
	}

}
