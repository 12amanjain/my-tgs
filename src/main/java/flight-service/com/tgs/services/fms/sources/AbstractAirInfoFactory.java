package com.tgs.services.fms.sources;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.RejectedExecutionException;
import javax.xml.ws.soap.SOAPFaultException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.NonOperatingSectorInfo;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.CacheType;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.helper.SourceRouteInfoHelper;
import com.tgs.services.fms.helper.SupplierSessionHelper;
import com.tgs.services.fms.helper.analytics.AirAnalyticsHelper;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.mapper.SupplierInfoAnalyticsMapper;
import com.tgs.services.fms.mapper.TripInfoToAnalyticsTripMapper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.FareRuleUnHandledFaultException;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSeatMapFaultException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public abstract class AbstractAirInfoFactory {

	protected AirSearchQuery searchQuery;
	protected SupplierConfiguration supplierConf;
	protected AirSearchResult searchResult;
	@Autowired
	protected FlightCacheHandler cacheHandler;
	protected ContextData contextData;
	protected AirFlowType flowType;
	protected User user;

	@Autowired
	AirSearchResultProcessingManager processingManager;

	@Autowired
	protected GeneralCachingCommunicator cachingComm;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected AirSourceConfigurationOutput sourceConfiguration;

	@Autowired
	protected SupplierSessionHelper sessionHelper;

	protected List<String> criticalMessageLogger;

	protected long searchTime;

	@Autowired
	protected AirAnalyticsHelper analyticsHelper;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	public AbstractAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		this.searchQuery = searchQuery;
		this.supplierConf = supplierConf;
		cacheHandler = new FlightCacheHandler();
		user = SystemContextHolder.getContextData().getUser();
		sourceConfiguration = AirUtils.getAirSourceConfiguration(searchQuery, supplierConf.getBasicInfo(), user);
		criticalMessageLogger = new ArrayList<>();
		supplierAnalyticsInfos = new ArrayList<>();
	}

	public AirSearchResult getAvailableSchedule(ContextData contextData) {
		long startTime = 0;
		long endTime = 0;
		try {
			LogUtils.log(LogTypes.AIR_SUPPLIER_SEARCH_START, LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
					.searchId(searchQuery.getSearchId()).build(), LogTypes.AIRSEARCH_DOSEARCH);
			this.setContextData(contextData);
			SystemContextHolder.setContextData(contextData);
			contextData.setValue(supplierConf.getSupplierId(), sourceConfiguration);
			startTime = System.currentTimeMillis();
			if (!BooleanUtils.isTrue(searchQuery.getIsLiveSearch())) {
				searchResult = this.fetchFromCache();
			}
			if (searchResult == null) {
				log.debug("Searching available schedules for supplier {} searchQuery {}",
						supplierConf.getBasicInfo().getDescription(), searchQuery);
				this.searchAvailableSchedules();
				searchResult = combineResults(searchResult);
				cacheHandler.cacheAirSearchResults(searchResult, searchQuery, supplierConf, contextData.getUser());
				LogUtils.log(LogTypes.AIR_SUPPLIER_SEARCH_END, LogMetaInfo.builder()
						.timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId()).build(),
						LogTypes.AIR_SUPPLIER_SEARCH_START);
				processingManager.storeFlightTimings(searchResult, supplierConf, user);
			}

		} catch (SupplierSessionException se) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger
						.add(ObjectUtils.firstNonNull(se.getMessage(), AirSourceConstants.AUHTENTICATION_ERROR));
			}
			log.error(StringUtils.join(AirSourceConstants.AUHTENTICATION_ERROR, searchQuery.getSearchId(), " ",
					supplierConf.getBasicInfo(), se.getMessage()));
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", se);
		} catch (NotOperatingSectorException nos) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger
						.add(ObjectUtils.firstNonNull(nos.getMessage(), AirSourceConstants.NOT_OPERATING_SECTOR));
			}
			log.debug("Not Operating Sector {} sq {} and erroMsg {}", supplierConf.getSourceId(), searchQuery,
					nos.getMessage());
			if (isNonOperatingAllowedToCache(supplierConf)) {
				NonOperatingSectorInfo nonOperatingSectorInfo = NonOperatingSectorInfo.builder()
						.source(searchQuery.getRouteInfos().get(0).getFromCityAirportCode())
						.destination(searchQuery.getRouteInfos().get(0).getToCityAirportCode())
						.sourceId(supplierConf.getSourceId()).build();
				SourceRouteInfoHelper.addNonOperatingSector(nonOperatingSectorInfo);
			}
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", nos);
		} catch (NoSearchResultException nos) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger
						.add(ObjectUtils.firstNonNull(nos.getMessage(), AirSourceConstants.NO_SEARCH_RESULT));
			}
			// Schedule for particular date is not available
			// Ashu : Moving it to info logs because system is generating false alarms.
			log.debug(AirSourceConstants.LOG_NO_SEARCH_RESULT, supplierConf.getBasicInfo().getDescription(),
					searchQuery, nos.getMessage());
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", nos);
		} catch (SupplierRemoteException re) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(ObjectUtils.firstNonNull(re.getMessage(), AirSourceConstants.REMOTE_FAILED));
			}
			log.info(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(),
					searchQuery.getSearchId(), re.getMessage());
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", re);
		} catch (SOAPFaultException se) {
			if (StringUtils.isNotBlank(se.getMessage())) {
				criticalMessageLogger.add(se.getMessage());
				log.info(AirSourceConstants.LOG_SUPPLIER_SOAP_FAILED, supplierConf.getSourceId(),
						searchQuery.getSearchId(), se.getMessage());
			} else {
				criticalMessageLogger.add("Soap Fault occured");
				log.info(AirSourceConstants.LOG_SUPPLIER_SOAP_FAILED, supplierConf.getSourceId(),
						searchQuery.getSearchId(), se);
			}
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", se);
		} catch (RejectedExecutionException re) {
			criticalMessageLogger.add(re.getMessage());
			log.error("Flight Search Thread pool tasks exceeded {} cause", searchQuery.getSearchId(), re);
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", re);
		} catch (Exception e) {
			if (StringUtils.isNotBlank(e.getMessage()))
				criticalMessageLogger.add(e.getMessage());
			log.error(AirSourceConstants.AIR_SUPPLIER_SEARCH, supplierConf.getBasicInfo().getDescription(), searchQuery,
					e);
			LogUtils.log(searchQuery.getSearchId(), "AirSearch", e);
		} finally {
			endTime = System.currentTimeMillis();
			searchTime = endTime - startTime;
			TripPriceEngine.filterAirlinesAndFareTypes(supplierConf, searchResult, searchQuery);
			LogUtils.clearLogList();
			sendToAnalytics();
		}
		return searchResult;
	}

	private void sendToAnalytics() {
		if (CollectionUtils.isNotEmpty(supplierAnalyticsInfos)) {
			for (SupplierAnalyticsQuery query : supplierAnalyticsInfos) {
				SupplierInfoAnalyticsMapper analyticsMapper = SupplierInfoAnalyticsMapper.builder().build();
				analyticsMapper.setOutput(query);
				analyticsMapper.setSearchQuery(searchQuery);
				analyticsMapper.setBasicInfo(supplierConf.getBasicInfo());
				analyticsHelper.pushToAnalytics(analyticsMapper, AirAnalyticsType.SUPPLIER_INFO);
			}
		}
	}

	protected abstract void searchAvailableSchedules();

	private AirSearchResult fetchFromCache() {
		return cacheHandler.getCacheResult(searchQuery, supplierConf);
	}

	public TripInfo reviewTrip(TripInfo selectedTrip, String bookingId, Integer attemptCount) {
		TripInfo tripInfo = null;
		TripInfo oldTripInfo = new GsonMapper<>(selectedTrip, TripInfo.class).convert();
		LogUtils.log(
				LogTypes.AIR_REVIEW_SUPPLIERBEGIN, LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
						.sourceId(supplierConf.getSourceId()).bookingId(bookingId).build(),
				LogTypes.AIR_REVIEW_REVIEWTRIP);
		SystemContextHolder.setContextData(contextData);
		contextData.setValue(supplierConf.getSupplierId(), sourceConfiguration);
		try {
			if (selectedTrip == null) {
				return selectedTrip;
			}
			log.info("AbstractAirInfoFactory#reviewTrip for bookingId {}", bookingId);
			// Reset commercial and markup components before sending trip for review.
			selectedTrip = new GsonMapper<>(selectedTrip, TripInfo.class).convert();
			processingManager.resetCommercial(selectedTrip, contextData.getUser());
			processingManager.resetCreditShellBalance(selectedTrip, searchQuery);
			tripInfo = this.review(selectedTrip, bookingId);
			if (tripInfo != null) {
				processingManager.addXtraFare(oldTripInfo, tripInfo, contextData.getUser(), bookingId);
				processingManager.processTripInfo(tripInfo, searchQuery, contextData.getUser(), sourceConfiguration);
				processingManager.processTripInfoProperties(tripInfo, searchQuery, supplierConf);
				processingManager.processSSRInfoOnReview(tripInfo, sourceConfiguration);
				createSegmentId(tripInfo);
			}
		} catch (SupplierSessionException se) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add("Authentication Failed");
			}
			log.error(StringUtils.join(AirSourceConstants.AUHTENTICATION_ERROR, bookingId, supplierConf.getBasicInfo(),
					se.getMessage()));
			tripInfo = null;
			LogUtils.log(bookingId, "AirReview", se);
		} catch (NoSeatAvailableException nos) {
			// selected is not available, supplier should automatically jump to next class
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(nos.getMessage());
			}
			log.info(AirSourceConstants.NO_SEAT_AVAILABLE, nos.getMessage(), bookingId, selectedTrip.toString());
			tripInfo = null;
			LogUtils.log(bookingId, "AirReview", nos);
		} catch (Exception e) {
			if (StringUtils.isNotBlank(e.getMessage()))
				criticalMessageLogger.add(e.getMessage());
			LogUtils.log(bookingId, "AirReview", e);
			throw e;
		} finally {
			LogUtils.clearLogList();
			reviewTripsToAnalytics(selectedTrip, searchQuery, bookingId, attemptCount);
		}
		LogUtils.log(
				LogTypes.AIR_REVIEW_SUPPLIEREND, LogMetaInfo.builder().timeInMs(System.currentTimeMillis())
						.sourceId(supplierConf.getSourceId()).bookingId(bookingId).build(),
				LogTypes.AIR_REVIEW_SUPPLIERBEGIN);
		return tripInfo;
	}

	public TripSeatMap getSeatInfo(TripInfo tripInfo, String bookingId) {
		TripSeatMap seatMap = null;
		try {
			log.info("AbstractAirInfoFactory#getSeatInfo for bookingId {}", bookingId);
			seatMap = this.getSeatMap(tripInfo, bookingId);
		} catch (SupplierSeatMapFaultException | SupplierSessionException s) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(s.getMessage());
			}
			log.error("Unable to fetch seat for {} trip {} error {}", bookingId, tripInfo.toString(), s.getMessage());
		} catch (SupplierRemoteException sr) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(ObjectUtils.firstNonNull(sr.getMessage(), AirSourceConstants.REMOTE_FAILED));
			}
			log.error(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), bookingId,
					sr.getMessage());
		} catch (Exception e) {
			if (StringUtils.isNotBlank(e.getMessage()))
				criticalMessageLogger.add(e.getMessage());
			log.error("Unable to fetch seat for {} trip {}", bookingId, tripInfo.toString(), e);
		} finally {
			LogUtils.clearLogList();
		}
		return seatMap;
	}

	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		return null;
	}

	protected abstract TripInfo review(TripInfo selectedTrip, String bookingId);

	public TripFareRule fareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		TripFareRule tripFareRule = null;
		log.info("AbstractAirInfoFactory#fareRule for bookingId {}", bookingId);
		SystemContextHolder.setContextData(contextData);
		contextData.setValue(supplierConf.getSupplierId(), sourceConfiguration);
		try {
			if (MapUtils.isEmpty(selectedTrip.getPaxInfo())) {
				selectedTrip.setPaxInfo(BaseUtils.getPaxInfo(selectedTrip));
			}
			tripFareRule = this.getFareRule(selectedTrip, bookingId, isDetailed);
		} catch (FareRuleUnHandledFaultException | SupplierSessionException | NoSeatAvailableException fe) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger
						.add(ObjectUtils.firstNonNull(fe.getMessage(), AirSourceConstants.FARERULE_FAILED));
			}
			log.error(AirSourceConstants.LOG_FARERULE_FAILED, supplierConf.getSourceId(), bookingId, selectedTrip, fe);
		} catch (SupplierRemoteException sr) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(ObjectUtils.firstNonNull(sr.getMessage(), AirSourceConstants.REMOTE_FAILED));
			}
			log.error(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), bookingId,
					sr.getMessage());
		} catch (Exception e) {
			log.error(AirSourceConstants.LOG_FARERULE_FAILED, supplierConf.getSourceId(), bookingId, selectedTrip, e);
		} finally {
			LogUtils.clearLogList();
		}
		return tripFareRule;
	}

	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		return null;
	}

	private void createSegmentId(TripInfo tripInfo) {
		if (tripInfo == null) {
			return;
		}
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.setId(RandomStringUtils.random(3, false, true));
		});
	}

	public void storeBookingSession(String bookingId, SupplierSessionInfo sessionInfo, SupplierSession previousSession,
			TripInfo reviewedTrip) {
		if (reviewedTrip != null) {
			StringJoiner tripKey = new StringJoiner(":");

			if (previousSession != null
					&& StringUtils.isNotBlank(previousSession.getSupplierSessionInfo().getTripKey())) {
				tripKey.add(previousSession.getSupplierSessionInfo().getTripKey());
			}

			if (sessionInfo == null) {
				sessionInfo = SupplierSessionInfo.builder().build();
			}
			tripKey.add(reviewedTrip.getSegmentsBookingKey());
			sessionInfo.setTripKey(tripKey.toString());
			SupplierSession session = SupplierSession.builder().supplierSessionInfo(sessionInfo).bookingId(bookingId)
					.supplierId(supplierConf.getBasicInfo().getSupplierId())
					.sourceId(supplierConf.getBasicInfo().getSourceId()).build();
			LocalDateTime expiryTime = LocalDateTime.now().plusMinutes(getExpiryMinutes());
			session.setCreatedOn(LocalDateTime.now());
			session.setExpiryTime(expiryTime);
			if (AirUtils.isSupplierSessionCachingEnabled(user)) {
				session.setTtl(Long.valueOf(CacheType.SUPPLIERSESSIONEXPIRATION.getTtl()));
				sessionHelper.save(session);
			} else {
				sessionHelper.saveToDB(session);
			}
		}
	}

	private long getExpiryMinutes() {
		if (sourceConfiguration != null && Objects.nonNull(sourceConfiguration.getSessionTime()))
			return Long.parseLong(String.valueOf(sourceConfiguration.getSessionTime()));
		return 15;
	}

	private SupplierSession getExistSessionForReview(String bookingId, SupplierSessionInfo sessionInfo) {
		return sessionHelper.getBookingSessionFromDB(bookingId, sessionInfo, supplierConf);
	}

	public SupplierSession getSessionIfExists(String bookingId, SupplierSessionInfo sessionInfo) {
		if (sessionInfo == null) {
			sessionInfo = SupplierSessionInfo.builder().build();
		}
		if (AirUtils.isSupplierSessionCachingEnabled(user)) {
			return getReviewSession(bookingId, sessionInfo);
		} else {
			return getExistSessionForReview(bookingId, sessionInfo);
		}
	}

	private SupplierSession getReviewSession(String bookingId, SupplierSessionInfo sessionInfo) {
		SupplierSession supplierSession = sessionHelper.getSessionByTrip(bookingId, sessionInfo.getTripKey());
		return supplierSession;
	}

	public TripInfo getChangeClass(TripInfo tripInfo, String searchId) {
		return tripInfo;
	}

	public TripInfo getAlternateClass(TripInfo tripInfo, String searchId) {
		try {
			LogUtils.log(LogTypes.AIR_SUPPLIER_CHANGE_CLASS_SEARCH_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchId).build(), null);
			log.info("AbstractAirInfoFactory#getAlternateClass for bookingId {}", searchId);
			tripInfo = this.getChangeClass(tripInfo, searchId);
			LogUtils.log(LogTypes.AIR_SUPPLIER_CHANGE_CLASS_END_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchId).build(),
					LogTypes.AIR_SUPPLIER_CHANGE_CLASS_SEARCH_START);
		} catch (SupplierSessionException se) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add("Authentication Failed");
			}
			log.error(StringUtils.join(AirSourceConstants.AUHTENTICATION_ERROR, searchId, supplierConf.getBasicInfo(),
					se.getMessage()));
			tripInfo = null;
		} catch (SupplierRemoteException re) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(ObjectUtils.firstNonNull(re.getMessage(), AirSourceConstants.REMOTE_FAILED));
			}
			log.info(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(),
					searchQuery.getSearchId(), re.getMessage());
		} catch (Exception e) {
			if (StringUtils.isNotBlank(e.getMessage()))
				criticalMessageLogger.add(e.getMessage());
			throw e;
		} finally {
			LogUtils.clearLogList();
		}
		return tripInfo;
	}

	public TripInfo getChangeClassFare(TripInfo tripInfo, String searchId) {
		return tripInfo;
	}

	public TripInfo getAlternateClassFare(TripInfo tripInfo, String searchId) {
		try {
			log.info("AbstractAirInfoFactory#getAlternateClassFare for bookingId {}", searchId);
			LogUtils.log(LogTypes.AIR_SUPPLIER_CHANGE_CLASS_FARE_SEARCH_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchId).build(), null);
			this.searchQuery = AirUtils.getSearchQueryFromTripInfo(tripInfo);
			// setFlowType -> This is help suppliers not to shift to another lower class available
			this.searchQuery.setFlowType(AirFlowType.ALTERNATE_CLASS);
			tripInfo = this.getChangeClassFare(tripInfo, searchId);
			LogUtils.log(LogTypes.AIR_SUPPLIER_CHANGE_CLASS_FARE_END_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchId).build(),
					LogTypes.AIR_SUPPLIER_CHANGE_CLASS_FARE_SEARCH_START);
		} catch (SupplierSessionException se) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add("Authentication Failed");
			}
			log.error(StringUtils.join(AirSourceConstants.AUHTENTICATION_ERROR, searchId, supplierConf.getBasicInfo(),
					se.getMessage()));
		} catch (NoSeatAvailableException nos) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(nos.getMessage());
			}
			log.info("Error Occured On Alternate class fare {}", tripInfo, nos);
			throw nos;
		} catch (Exception e) {
			if (StringUtils.isNotBlank(e.getMessage()) && CollectionUtils.isEmpty(criticalMessageLogger))
				criticalMessageLogger.add(e.getMessage());
			throw e;
		} finally {
			LogUtils.clearLogList();
		}
		return tripInfo;
	}

	public void closeSession(List<SupplierSession> sessionList, String bookingId) {}

	public void closeReviewSession(List<SupplierSession> sessionList, String bookingId) {
		try {
			this.closeSession(sessionList, bookingId);
		} catch (SupplierRemoteException re) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(ObjectUtils.firstNonNull(re.getMessage(), AirSourceConstants.REMOTE_FAILED));
			}
			log.info(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(),
					searchQuery.getSearchId(), re.getMessage());
		} finally {
			LogUtils.clearLogList();
		}
	}

	protected void reviewTripsToAnalytics(TripInfo tripInfo, AirSearchQuery searchQuery, String bookingId,
			Integer attemptCount) {
		try {
			TripInfoToAnalyticsTripMapper tripMapper = TripInfoToAnalyticsTripMapper.builder()
					.contextData(SystemContextHolder.getContextData()).bookingId(bookingId).attempt(attemptCount)
					.user(SystemContextHolder.getContextData().getUser())
					.tripInfos((tripInfo != null) ? Arrays.asList(tripInfo) : null).errorMessages(criticalMessageLogger)
					.searchQuery(searchQuery).build();
			analyticsHelper.pushToAnalytics(tripMapper, AirAnalyticsType.REVIEW_TRIP);
		} catch (Exception e) {
			log.error("Failed to Push Analytics REVIEW_TRIP {}", tripInfo, e);
		}
	}

	public Boolean initializeStubs() {
		return null;
	}

	/**
	 * This method can be overridden in supplierinfo factory to create customized combination logic.
	 * 
	 * @param searchResult
	 * @return
	 */
	public AirSearchResult combineResults(AirSearchResult searchResult) {
		searchResult = AirSearchResultProcessingManager.processSearchTypeResult(searchResult,
				supplierConf.getSourceId(), searchQuery.isIntlReturn(), user);
		return searchResult;
	}

	protected boolean isNonOperatingAllowedToCache(SupplierConfiguration configuration) {
		AirGeneralPurposeOutput output =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		if (output != null && CollectionUtils.isNotEmpty(output.getNonOperatingNotAllowedSources())) {
			return !output.getNonOperatingNotAllowedSources().contains(configuration.getSourceId());
		}
		return true;
	}

	public List<SourceRouteInfo> addNewRoutesToSystem(List<SourceRouteInfo> routes, String searchKey) {
		try {
			log.info("AbstractAirInfoFactory#addNewRoutesToSystem");
			routes = this.addRoutesToSystem(routes, searchKey);
		} catch (SupplierSessionException se) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger
						.add(ObjectUtils.firstNonNull(se.getMessage(), AirSourceConstants.AUHTENTICATION_ERROR));
			}
			log.error(StringUtils.join(AirSourceConstants.AUHTENTICATION_ERROR, " ", supplierConf.getBasicInfo(),
					se.getMessage()));

		} catch (SupplierRemoteException re) {
			if (CollectionUtils.isEmpty(criticalMessageLogger)) {
				criticalMessageLogger.add(ObjectUtils.firstNonNull(re.getMessage(), AirSourceConstants.REMOTE_FAILED));
			}
			log.info(AirSourceConstants.LOG_SUPPLIER_REMOTE_FAILED, supplierConf.getSourceId(), re.getMessage());


		} catch (Exception e) {
			log.error("Error Occured while adding new Routes to System due to {}", e);
		} finally {
			LogUtils.clearLogList();
		}
		return routes;
	}

	protected List<SourceRouteInfo> addRoutesToSystem(List<SourceRouteInfo> routes, String searchKey) {
		return routes;

	}

}
