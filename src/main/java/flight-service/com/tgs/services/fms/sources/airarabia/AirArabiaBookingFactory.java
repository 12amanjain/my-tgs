package com.tgs.services.fms.sources.airarabia;

import java.util.List;
import com.tgs.services.base.AxisRequestResponseListener;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import org.apache.commons.lang3.StringUtils;
import org.opentravel.www.OTA._2003._05.OTA_AirBookRS;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirArabiaBookingFactory extends AbstractAirBookingFactory {

	protected AxisRequestResponseListener listner = null;

	protected AirArabiaBindingService bindingService;

	public AirArabiaBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		listner = new AxisRequestResponseListener(bookingId, supplierConf.getBasicInfo().getSupplierName());
		bindingService = AirArabiaBindingService.builder().cacheCommunicator(cachingComm).build();
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		initialize();
		try {
			if (supplierSession != null && supplierSession.getSupplierSessionInfo() != null
					&& StringUtils.isNotEmpty(supplierSession.getSupplierSessionInfo().getSessionToken())) {
				List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
				String jSessionID = supplierSession.getSupplierSessionInfo().getSessionToken().split("#")[0];
				String transactionIdentifier = supplierSession.getSupplierSessionInfo().getSessionToken().split("#")[1];
				List<FlightTravellerInfo> travellerInfos =
						segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
				AirArabiaBookingManager bookingManager =
						AirArabiaBookingManager.builder().configuration(supplierConf).bindingService(bindingService)
								.segmentInfos(segmentInfos).travellerInfos(travellerInfos).deliveryInfo(deliveryInfo)
								.bookingId(bookingId).listener(listner).criticalMessageLogger(criticalMessageLogger)
								.gstInfo(gstInfo).moneyExchnageComm(moneyExchangeComm).bookingUser(bookingUser).build();
				bookingManager.getSessionBinding();
				bookingManager.setTransactionIdentifier(transactionIdentifier);
				bookingManager.setJSessionId(jSessionID);
				bookingManager.setGmsCommunicator(gmsCommunicator);
				double amountToPaid = bookingManager.doPriceQuote();
				if (!isFareDiff(amountToPaid)) {
					bookingManager.purchaseItinerary(isHoldBooking, amountToPaid, order.getBookingUserId());
					pnr = bookingManager.getPnr();
					if (isHoldBooking) {
						updateTimeLimit(bookingManager.getTimeLimit());
						isSuccess = true;
					} else if (BookingUtils.hasTicketNumberInAllSegments(segmentInfos)) {
						isSuccess = true;
					}
				}
			}
		} finally {
			if (!isHoldBooking && isSuccess) {
				// were G9 isTkrequired is true (even LCC) but not required to call confirmation again
				// cause already purchaseItinerary had done payment
				this.isTkRequired = false;
			}
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		initialize();
		List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
		List<FlightTravellerInfo> travellerInfos = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		AirArabiaRetrieveManager retrieveManager = AirArabiaRetrieveManager.builder().configuration(supplierConf)
				.bindingService(bindingService).listener(listner).bookingUser(bookingUser).build();
		OTA_AirBookRS bookRS = retrieveManager.retrieveByPNR(pnr);
		if (bookRS != null && StringUtils.isNotBlank(bookRS.getEchoToken())) {
			AirArabiaBookingManager bookingManager =
					AirArabiaBookingManager.builder().configuration(supplierConf).bindingService(bindingService)
							.criticalMessageLogger(criticalMessageLogger).segmentInfos(segmentInfos)
							.travellerInfos(travellerInfos).deliveryInfo(deliveryInfo).bookingId(bookingId)
							.listener(listner).moneyExchnageComm(moneyExchangeComm).bookingUser(bookingUser).build();
			bookingManager.setPnr(pnr);
			bookingManager.setGmsCommunicator(gmsCommunicator);
			double amountToPay = bookingManager.getAmountToPay(bookRS);
			if (!isFareDiff(amountToPay)) {
				bookingManager.confirmItinerary(amountToPay);
				if (BookingUtils.hasTicketNumberInAllSegments(segmentInfos)) {
					isSuccess = true;
				}
			}
		}
		return isSuccess;
	}
}
