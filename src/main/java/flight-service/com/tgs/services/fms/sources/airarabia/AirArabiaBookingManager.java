package com.tgs.services.fms.sources.airarabia;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import com.tgs.services.base.utils.BookingUtils;
import org.apache.axis.types.Language;
import org.apache.axis.types.NonNegativeInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.opentravel.www.OTA._2003._05.AirItineraryType;
import org.opentravel.www.OTA._2003._05.AirItineraryTypeOriginDestinationOptions;
import org.opentravel.www.OTA._2003._05.AirTripType;
import org.opentravel.www.OTA._2003._05.BookFlightSegmentType;
import org.opentravel.www.OTA._2003._05.Code;
import org.opentravel.www.OTA._2003._05.FlightNumberType;
import org.opentravel.www.OTA._2003._05.FlightSegmentBaseTypeArrivalAirport;
import org.opentravel.www.OTA._2003._05.FlightSegmentBaseTypeDepartureAirport;
import org.opentravel.www.OTA._2003._05.OTA_AirBookRS;
import org.opentravel.www.OTA._2003._05.OTA_AirPriceRQ;
import org.opentravel.www.OTA._2003._05.OTA_AirPriceRS;
import org.opentravel.www.OTA._2003._05.PassengerTypeQuantityType;
import org.opentravel.www.OTA._2003._05.SpecialReqDetailsType;
import org.opentravel.www.OTA._2003._05.SpecialReqDetailsTypeBaggageRequestsBaggageRequest;
import org.opentravel.www.OTA._2003._05.SpecialReqDetailsTypeMealRequestsMealRequest;
import org.opentravel.www.OTA._2003._05.Target;
import org.opentravel.www.OTA._2003._05.TravelerInfoSummaryType;
import org.opentravel.www.OTA._2003._05.TravelerInformationType;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.utils.common.XMLUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@SuperBuilder
@Slf4j
final class AirArabiaBookingManager extends AirArabiaServiceManager {

	protected List<SegmentInfo> segmentInfos;

	protected String pnr;

	protected double airlineAmount;

	protected GstInfo gstInfo;

	private static DecimalFormat df2 = new DecimalFormat(".##");

	protected GeneralServiceCommunicator gmsCommunicator;


	public OTA_AirBookRS purchaseItinerary(boolean isHold, Double amountToPay, String bookingUserId) {
		OTA_AirBookRS bookRS = null;
		amountToPay = Double.parseDouble(df2.format(amountToPay));
		String methodCall = isHold ? "holdItinerary" : "confirmItinerary";
		String bookingReqXML = prepareBookingRequest(isHold, amountToPay, bookingUserId);
		String response = getResponseByRequest(bookingReqXML, methodCall, "book");
		parseBookingResponse(response, isHold);
		return bookRS;
	}

	private void parseBookingResponse(String bookingResponse, boolean isHold) {
		Document doc = preprocessBookingResponse(bookingResponse);
		if (doc != null && !checkIfAnyConfirmError(doc)) {
			pnr = XMLUtils.getAttrributeValue(
					XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/BookingReferenceID"), "ID");
			log.info("Booking Reference {} for booking Id {}", pnr, bookingId);
			BookingUtils.updateAirlinePnr(segmentInfos, pnr);
			if (isHold) {
				updateTicketingTimeLimit(doc);
			} else if (!isHold) {
				updateTicketDetails(doc);
			}
		}
	}

	private boolean checkIfAnyConfirmError(Document doc) {
		Node errorNode = XMLUtils.getNode(doc, "Body/OTA_AirBookRS/Errors/Error");
		boolean error = false;
		if (errorNode != null && errorNode.getAttributes() != null) {
			// not supported -> Hold Booking not allowed for confirming
			// Booking Success
			NamedNodeMap namedNodeMap = errorNode.getAttributes();
			if (namedNodeMap.getLength() > 2) {
				error = true;
				String errorMsg = namedNodeMap.item(1).getNodeValue();
				criticalMessageLogger.add(errorMsg);
			}
		}
		return error;
	}

	private Document preprocessBookingResponse(String bookingResponse) {
		Document doc = null;
		try {
			doc = XMLUtils.createDocumentFromString(bookingResponse);
			String bookingStatus =
					XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/Ticketing").getTextContent();
			log.info("Booking status for  {} status {}", bookingId, bookingStatus);
		} catch (Exception e) {
			doc = XMLUtils.createDocumentFromString(bookingResponse);
		}
		return doc;
	}

	private void updateTicketDetails(Document doc) {
		NodeList nodeList = XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/TravelerInfo").getChildNodes();
		if (nodeList != null) {
			Node node = null;
			for (int i = 0; i < nodeList.getLength(); i++) {
				/**
				 * @implSpec : Blindly considering First Child will be PersonName(First Name,Last Name)
				 */
				node = nodeList.item(i);
				NodeList travellerDetails = node.getFirstChild().getChildNodes();
				int paxNameLength = travellerDetails.getLength();
				String ticketNumber;
				String firstName = null;
				String lastName = null;
				for (int nameIndex = 0; nameIndex < paxNameLength; nameIndex++) {
					Node node1 = travellerDetails.item(nameIndex);
					if (node1.getNodeName().contains("GivenName")) {
						firstName = node1.getFirstChild().getNodeValue().trim();
					}
					if (node1.getNodeName().contains("Surname")) {
						lastName = node1.getFirstChild().getNodeValue().trim();
					}
				}
				ticketNumber = (XMLUtils.getAttrributeValue(XMLUtils.getNode(node, "ETicketInfo/ETicketInfomation"),
						"eTicketNo"));
				if (StringUtils.isNotBlank(ticketNumber)) {
					/**
					 * @implSpec : Corresponding ticket number has to be updated for pax
					 */
					for (SegmentInfo segmentInfo : segmentInfos) {
						List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
						for (FlightTravellerInfo travellerInfo : travellerInfos) {
							if (StringUtils.equalsIgnoreCase(lastName, travellerInfo.getLastName())
									&& StringUtils.equalsIgnoreCase(firstName, travellerInfo.getFirstName())) {
								travellerInfo.setTicketNumber(ticketNumber);
							}
						}
					}
				}
			}
		} else {
			log.error("Booking failed for bookingid {} due to some exception on booking response", bookingId);
		}
	}

	public String getPaxName(FlightTravellerInfo travellerInfo) {
		return travellerInfo.getFullName();
	}


	private String prepareBookingRequest(boolean isHold, double amountToPay, String bookingUserId) {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(getPreRequestData());
		sbuf.append(getAirBookRQData(isHold, amountToPay));
		sbuf.append(getContactPostRequest(travellerInfos.get(0), deliveryInfo, bookingUserId));
		sbuf.append(getPostRequestData());
		return sbuf.toString();
	}

	private String getContactPostRequest(FlightTravellerInfo travellerInfo, DeliveryInfo deliveryInfo,
			String bookingUserId) {
		String nat = travellerInfo.getPassportNationality();
		nat = StringUtils.isNotBlank(nat) ? nat : "IN";
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(bookingUserId, clientInfo);
		String gstNumber = StringUtils.EMPTY;
		if (gstInfo != null && StringUtils.isNotEmpty(gstInfo.getGstNumber())) {
			gstNumber = gstInfo.getGstNumber();
		}
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.BOOK_REQUEST_PAX);
		Object[] args = {AirArabiaUtils.getTitle(travellerInfo), AirArabiaUtils.getFirstName(travellerInfo),
				AirArabiaUtils.getLastName(travellerInfo), deliveryInfo.getContacts().get(0),
				deliveryInfo.getEmails().get(0), contactAddress.getAddress(), contactAddress.getCityInfo().getName(),
				nat, nat, clientInfo.getNationality(), gstNumber};
		return new StringBuffer(msgFormat.format(args)).toString();
	}


	private StringBuffer getAirBookRQData(boolean isHold, double amountToPay) {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(getAirBookRQPreData());
		sbuf.append(getAirItineraryData());
		sbuf.append(getTravelerInfoData());
		if (!isHold) {
			sbuf.append(getFulfillmentData(amountToPay, false));
		}
		sbuf.append(getAirBookRQPostData());
		return sbuf;
	}


	private StringBuffer getAirBookRQPreData() {
		Calendar nowCal = Calendar.getInstance();
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.BOOK_PRE_REQUEST);
		Object[] args = {echoToken, getXMLTimeStamp(nowCal), getTransactionId(), "20061", TERMINALID,
				configuration.getSupplierCredential().getUserName()};
		return new StringBuffer(msgFormat.format(args));
	}

	/**
	 * @implSpec : TransactionId is unique identifier for entire trip Trace Id will be same for all segments
	 */
	public String getTransactionId() {
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			transactionIdentifier = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getTraceId();
			return transactionIdentifier;
		}
		return null;
	}


	private String getAirBookRQPostData() {
		return "</ns2:OTA_AirBookRQ>";
	}

	private StringBuffer getFulfillmentData(double amountToPay, boolean isPostBook) {
		if (!isPostBook) {
			MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.BOOK_PAYMENT);
			Object[] args =
					{configuration.getSupplierCredential().getOrganisationCode(), "" + amountToPay, getCurrencyCode()};
			return new StringBuffer(msgFormat.format(args));
		}
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.BOOK_POST_PAYMENT);
		Object[] args = {configuration.getSupplierCredential().getOrganisationCode(), "" + airlineAmount, "AED"};
		return new StringBuffer(msgFormat.format(args));
	}

	private StringBuffer getAirItineraryData() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(AirArabiaXMLConstantsInfo.OD_OPEN);
		sbuf.append(getOriginDestinationOption());
		sbuf.append(AirArabiaXMLConstantsInfo.OD_CLOSE);
		return sbuf;
	}

	private StringBuffer getOriginDestinationOption() {
		StringBuffer sbuf = new StringBuffer();
		segmentInfos.forEach(segmentInfo -> {
			MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.FLIGHT_SEGMENT);
			Object[] args = {getXMLTimeStamp(TgsDateUtils.getCalendar(segmentInfo.getArrivalTime())),
					getXMLTimeStamp(TgsDateUtils.getCalendar(segmentInfo.getDepartTime())),
					getFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber()),
					segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey(), segmentInfo.getDepartureAirportCode(),
					segmentInfo.getArrivalAirportCode()};
			sbuf.append(msgFormat.format(args));
		});
		return sbuf;
	}

	private StringBuffer getTravelerInfoData() {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(AirArabiaXMLConstantsInfo.TRAVELER_OPEN);

		int adultCounter = 0;
		int adultPositions[] = new int[AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).size()];
		int infantCounter = 0;
		boolean isInfant = false;

		for (int i = 0; i < travellerInfos.size(); i++) {
			FlightTravellerInfo travellerInfo = travellerInfos.get(i);
			String nat = travellerInfo.getPassportNationality();
			nat = StringUtils.isNotBlank(nat) ? nat : "IN";
			nat = "IN";
			String passportNumber = travellerInfo.getPassportNumber();
			if (StringUtils.isEmpty(passportNumber)) {
				passportNumber = StringUtils.EMPTY;
			}

			isInfant = false;

			if (travellerInfo.getPaxType() == PaxType.ADULT) {
				adultPositions[adultCounter] = (i + 1);
				adultCounter++;
			} else if (travellerInfo.getPaxType() == PaxType.INFANT) {
				isInfant = true;
				infantCounter++;
			}

			if (!isInfant) {
				MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.AIRTRAVELER);
				Object[] args =
						{getXMLTimeStamp(TgsDateUtils.getCalendar(travellerInfo.getDob())), getPaxType(travellerInfo),
								AirArabiaUtils.getFirstName(travellerInfo), AirArabiaUtils.getLastName(travellerInfo),
								AirArabiaUtils.getTitle(travellerInfo), deliveryInfo.getContacts().get(0), nat,
								passportNumber, nat, ("" + travellerInfo.getPaxType().getType().charAt(0)) + (i + 1)};
				sbuf.append(msgFormat.format(args));
			} else {
				sbuf.append(getInfantAirTravellerData(getXMLTimeStamp(TgsDateUtils.getCalendar(travellerInfo.getDob())),
						AirArabiaUtils.getFirstName(travellerInfo), AirArabiaUtils.getLastName(travellerInfo),
						AirArabiaUtils.getTitle(travellerInfo),
						("I" + (i + 1) + "/A" + adultPositions[infantCounter - 1]), nat, passportNumber));
			}

		}

		sbuf.append(AirArabiaXMLConstantsInfo.TRAVELER_CLOSE);
		return sbuf;
	}


	private String getPaxType(FlightTravellerInfo travellerInfo) {
		String paxType = travellerInfo.getPaxType().getType();
		if (travellerInfo.getPaxType().equals(PaxType.CHILD)) {
			paxType = "CHD";
		}
		return paxType;
	}

	private StringBuffer getInfantAirTravellerData(String dob, String firstName, String lastName, String title,
			String paxTypeRPH, String nationality, String passportNumber) {
		StringBuffer sbuf = new StringBuffer();
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.INFANTTRAVELER);
		Object[] args = {dob, firstName, lastName, title, paxTypeRPH, passportNumber, nationality};
		sbuf.append(msgFormat.format(args));
		return sbuf;
	}

	public OTA_AirBookRS confirmItinerary(double amountToPay) {
		OTA_AirBookRS bookRS = null;
		try {
			String bookingReqXML = prepareConfirmBookingRequest(amountToPay);
			String response = getResponseByRequest(bookingReqXML, "ConfirmBooking", "modifyReservation");
			if (StringUtils.isNotBlank(response)) {
				parseBookingResponse(response, false);
			}
		} catch (Exception e) {
			log.error("Error Occured for bookingId {} cause {}", bookingId, e);
		}
		return bookRS;
	}

	private String prepareConfirmBookingRequest(double amountToPay) {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(getPreRequestData());
		sbuf.append(getAirBookRQData());
		sbuf.append(getFulfillmentData(amountToPay, true));
		sbuf.append(getBookingReference());
		sbuf.append(getAAAirBookModifyRQExtData());
		sbuf.append(getPostRequestData());
		return sbuf.toString();
	}

	private StringBuffer getAAAirBookModifyRQExtData() {
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.MODIFYREQUEST);
		return new StringBuffer(msgFormat.format(null));
	}

	private StringBuffer getBookingReference() {
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.REFERENCE);
		Object[] args = {pnr};
		return new StringBuffer(msgFormat.format(args));
	}

	private StringBuffer getAirBookRQData() {
		Calendar nowCal = Calendar.getInstance();
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.BOOK_MODIFY);
		Object[] args = {echoToken, getXMLTimeStamp(nowCal), transactionIdentifier, "20061", TERMINALID,
				configuration.getSupplierCredential().getUserName()};
		return new StringBuffer(msgFormat.format(args));
	}


	public double getAmountToPay(OTA_AirBookRS bookRS) {
		double amountToPay = 0;
		String clientCurrency = getCurrencyCode();
		try {
			if (bookRS != null) {
				Document doc = XMLUtils.createDocumentFromString(bookRS.getEchoToken());
				String currencyCode = XMLUtils.getAttrributeValue(
						XMLUtils.getNode(doc,
								"Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/TotalEquivFare"),
						"CurrencyCode");
				if (currencyCode.toUpperCase().equalsIgnoreCase(clientCurrency)) {
					airlineAmount =
							Double.parseDouble(XMLUtils.getAttrributeValue(
									XMLUtils.getNode(doc,
											"Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/TotalFare"),
									"Amount"));
					amountToPay = Double.parseDouble(XMLUtils.getAttrributeValue(
							XMLUtils.getNode(doc,
									"Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/TotalEquivFare"),
							"Amount"));
				} else {
					airlineAmount = Double.parseDouble(XMLUtils.getAttrributeValue(
							XMLUtils.getNode(doc,
									"Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/TotalEquivFare"),
							"Amount"));
					amountToPay =
							Double.parseDouble(XMLUtils.getAttrributeValue(
									XMLUtils.getNode(doc,
											"Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/TotalFare"),
									"Amount"));
				}
				transactionIdentifier = XMLUtils.getAttrributeValue(XMLUtils.getNode(doc, "Body/OTA_AirBookRS"),
						"TransactionIdentifier");
			}
		} catch (Exception e) {
			log.error("Error occured while retrieving pnr {} cause {}", bookingId, e);
		}
		return Double.valueOf(df2.format(amountToPay));
	}

	public double doPriceQuote() {
		double amountToPaid = 0;
		try {
			if (stub == null) {
				this.stub = getSessionBinding();
			}
			OTA_AirPriceRS priceRS = stub.getPrice(getAirPriceQuoteRQ());
			String requestXML = priceRS.getEchoToken().replaceAll("FlexiOperations", "FlightSegment");
			String priceResp = getResponseByRequest(requestXML, "BookPrice", "getPrice");
			amountToPaid = parsePriceQuoteResponse(priceResp);
		} catch (Exception e) {
			log.error("unable to parse price info for booking {}", bookingId, e);
		}
		return amountToPaid;
	}

	private OTA_AirPriceRQ getAirPriceQuoteRQ() {
		OTA_AirPriceRQ priceRQ = new OTA_AirPriceRQ();
		priceRQ.setPOS(getPointOfSale());
		priceRQ.setVersion(version);
		priceRQ.setTarget(Target.Test);
		priceRQ.setPrimaryLangID(new Language(LOCALECODE));
		priceRQ.setEchoToken(echoToken);
		priceRQ.setSequenceNmbr(new NonNegativeInteger(SEQUENCE));
		priceRQ.setTimeStamp(Calendar.getInstance());
		priceRQ.setTransactionIdentifier(transactionIdentifier);
		priceRQ.setAirItinerary(getBookAirItinerary());
		priceRQ.setTravelerInfoSummary(getTravellerInfoSummary());
		return priceRQ;
	}

	public AirItineraryType getBookAirItinerary() {
		AirItineraryType itineraryType = new AirItineraryType();
		itineraryType.setDirectionInd(getTripType());
		itineraryType.setOriginDestinationOptions(getOriginDestinationInfo());
		return itineraryType;
	}

	@Override
	public AirTripType getTripType() {
		AirTripType tripType = AirTripType.value1;
		AtomicInteger tripCount = new AtomicInteger(0);
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			segmentInfos.forEach(segmentInfo -> {
				if (segmentInfo.getSegmentNum() == 0 || BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
					tripCount.getAndIncrement();
				}
			});
		}
		if (tripCount.intValue() == 1) {
			tripType = AirTripType.value1;
		} else if (tripCount.intValue() > 1) {
			tripType = AirTripType.value3;
		}
		return tripType;
	}

	@Override
	public org.opentravel.www.OTA._2003._05.TravelerInfoSummaryType getTravellerInfoSummary() {
		TravelerInfoSummaryType summary = new TravelerInfoSummaryType();
		summary.setAirTravelerAvail(getAirAvailPaxType());
		summary.setSpecialReqDetails(getSpecialRequestDetails(segmentInfos));
		return summary;
	}

	private SpecialReqDetailsType[] getSpecialRequestDetails(List<SegmentInfo> segmentInfos) {
		SpecialReqDetailsType[] ssrRequest = new SpecialReqDetailsType[1];
		ssrRequest[0] = new SpecialReqDetailsType();
		setBaggageInfo(ssrRequest);
		setMealInfo(ssrRequest);
		return ssrRequest;
	}

	private void setMealInfo(SpecialReqDetailsType[] ssrRequest) {
		List<SpecialReqDetailsTypeMealRequestsMealRequest> mealRequestList =
				new ArrayList<SpecialReqDetailsTypeMealRequestsMealRequest>();
		for (int i = 0; i < segmentInfos.size(); i++) {
			SegmentInfo segmentInfo = segmentInfos.get(i);
			if (segmentInfo.getSegmentNum() == 0 || BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				AtomicInteger paxIndex = new AtomicInteger(1);
				travellerInfos.forEach(travellerInfo -> {
					if (travellerInfo.getPaxType() != PaxType.INFANT && travellerInfo.getSsrMealInfo() != null) {
						SpecialReqDetailsTypeMealRequestsMealRequest mealRequest =
								new SpecialReqDetailsTypeMealRequestsMealRequest();
						mealRequest.setDepartureDate(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
						FlightNumberType flightNumberType = new FlightNumberType();
						flightNumberType.setFlightNumber(getFlightNumber(segmentInfo.getFlightNumber()));
						mealRequest.setFlightNumber(flightNumberType);
						String[] travelerReferenceNumber =
								{travellerInfo.getPaxType().getCode().charAt(0) + "" + paxIndex.intValue()};
						String[] flightRefNumberRPH = new String[1];
						flightRefNumberRPH[0] = segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey();
						flightRefNumberRPH[0] = flightRefNumberRPH[0].replaceAll(" DOL ", "\\$");
						mealRequest.setTravelerRefNumberRPHList(travelerReferenceNumber);
						mealRequest.setMealCode(travellerInfo.getSsrMealInfo().getCode());
						mealRequest.setFlightRefNumberRPHList(flightRefNumberRPH);
						mealRequestList.add(mealRequest);
						paxIndex.getAndIncrement();
					}
				});
			}
		}
		// Set Meal info
		if (CollectionUtils.isNotEmpty(mealRequestList)) {
			ssrRequest[0].setMealRequests(
					mealRequestList.toArray(new SpecialReqDetailsTypeMealRequestsMealRequest[mealRequestList.size()]));

		}
	}

	private void setBaggageInfo(SpecialReqDetailsType[] ssrRequest) {
		List<SpecialReqDetailsTypeBaggageRequestsBaggageRequest> baggageRequestList =
				new ArrayList<SpecialReqDetailsTypeBaggageRequestsBaggageRequest>();
		SegmentInfo previousSegmentInfo = null;
		for (int i = 0; i < segmentInfos.size(); i++) {
			SegmentInfo segmentInfo = segmentInfos.get(i);
			List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
			AtomicInteger paxIndex = new AtomicInteger(1);
			copyConnectingFlightBaggage(segmentInfo, previousSegmentInfo);
			travellerInfos.forEach(travellerInfo -> {
				if (travellerInfo.getPaxType() != PaxType.INFANT) {
					SpecialReqDetailsTypeBaggageRequestsBaggageRequest baggageRequest =
							new SpecialReqDetailsTypeBaggageRequestsBaggageRequest();
					baggageRequest.setDepartureDate(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
					FlightNumberType flightNumberType = new FlightNumberType();
					flightNumberType.setFlightNumber(getFlightNumber(segmentInfo.getFlightNumber()));
					baggageRequest.setFlightNumber(flightNumberType);
					String[] travelerReferenceNumber =
							{travellerInfo.getPaxType().getCode().charAt(0) + "" + paxIndex.intValue()};
					baggageRequest.setTravelerRefNumberRPHList(travelerReferenceNumber);
					String[] flightRefNumberRPH = new String[1];
					flightRefNumberRPH[0] = segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey();
					flightRefNumberRPH[0] = flightRefNumberRPH[0].replaceAll(" DOL ", "\\$");
					baggageRequest.setFlightRefNumberRPHList(flightRefNumberRPH);
					if (travellerInfo.getSsrBaggageInfo() != null) {
						baggageRequest.setBaggageCode(travellerInfo.getSsrBaggageInfo().getCode());
					} else {
						/**
						 * @implSpec : In case of customer not opted baggage have to send No Bag Code
						 */
						baggageRequest.setBaggageCode(AirArabiaXMLConstantsInfo.NO_BAG);
					}
					baggageRequestList.add(baggageRequest);
					paxIndex.getAndIncrement();
				}
			});
			if (segmentInfo.getSegmentNum().intValue() == 0) {
				previousSegmentInfo = segmentInfo;
			}
		}
		// Set baggage info
		ssrRequest[0].setBaggageRequests(baggageRequestList
				.toArray(new SpecialReqDetailsTypeBaggageRequestsBaggageRequest[baggageRequestList.size()]));
	}

	private void copyConnectingFlightBaggage(SegmentInfo previousSegmentInfo, SegmentInfo currentSegmentInfo) {
		if (previousSegmentInfo != null && currentSegmentInfo != null
				&& currentSegmentInfo.getSegmentNum().intValue() > 0) {
			List<FlightTravellerInfo> travellerInfos = currentSegmentInfo.getBookingRelatedInfo().getTravellerInfo();
			List<FlightTravellerInfo> previousTravellerInfos =
					previousSegmentInfo.getBookingRelatedInfo().getTravellerInfo();
			travellerInfos.forEach(travellerInfo -> {
				FlightTravellerInfo previousSameTraveller =
						getFilterTraveller(previousTravellerInfos, getPaxName(travellerInfo));
				if (previousSameTraveller != null && travellerInfo.getSsrBaggageInfo() == null) {
					travellerInfo.setSsrBaggageInfo(previousSameTraveller.getSsrBaggageInfo());
				}
			});
		}
	}


	public FlightTravellerInfo getFilterTraveller(List<FlightTravellerInfo> previousTravellerInfo, String paxName) {
		return previousTravellerInfo.stream().filter(travellerInfo -> getPaxName(travellerInfo).equals(paxName))
				.findFirst().orElse(null);
	}

	private TravelerInformationType[] getAirAvailPaxType() {
		TravelerInformationType[] travelerInformationTypes = new TravelerInformationType[1];
		travelerInformationTypes[0] = new TravelerInformationType();
		travelerInformationTypes[0].setPassengerTypeQuantity(getPaxInfo());
		return travelerInformationTypes;
	}


	@Override
	public PassengerTypeQuantityType[] getPaxInfo() {
		PassengerTypeQuantityType[] passengerTypeQuantityTypes = new PassengerTypeQuantityType[3];
		AtomicInteger paxIndex = new AtomicInteger(0);
		if (searchQuery != null) {
			searchQuery.getPaxInfo().forEach((paxType, count) -> {
				Code code = new Code();
				String paxCode = getPaxType(paxType.getType());
				code.setOTA_CodeTypeValue(paxCode);
				BigInteger paxCount = BigInteger.valueOf(AirUtils.getParticularPaxCount(searchQuery, paxType));
				passengerTypeQuantityTypes[paxIndex.intValue()] = new PassengerTypeQuantityType();
				passengerTypeQuantityTypes[paxIndex.intValue()].setCode(code);
				passengerTypeQuantityTypes[paxIndex.intValue()].setQuantity(paxCount);
				paxIndex.getAndIncrement();
			});
		} else if (CollectionUtils.isNotEmpty(travellerInfos)) {
			for (PaxType paxType : PaxType.values()) {
				Integer paxCount = AirUtils.getParticularPaxTravellerInfo(travellerInfos, paxType).size();
				Code code = new Code();
				String paxCode = getPaxType(paxType.getType());
				code.setOTA_CodeTypeValue(paxCode);
				BigInteger pCount = new BigInteger(String.valueOf(paxCount));
				passengerTypeQuantityTypes[paxIndex.intValue()] = new PassengerTypeQuantityType();
				passengerTypeQuantityTypes[paxIndex.intValue()].setCode(code);
				passengerTypeQuantityTypes[paxIndex.intValue()].setQuantity(pCount);
				paxIndex.getAndIncrement();
			}

		}
		return passengerTypeQuantityTypes;
	}

	private String getPaxType(String type) {
		String code = type;
		if (type.equals(PaxType.CHILD.getType())) {
			code = "CHD";
		}
		return code;
	}

	public AirItineraryTypeOriginDestinationOptions getOriginDestinationInfo() {
		AirItineraryTypeOriginDestinationOptions originDestInfo = new AirItineraryTypeOriginDestinationOptions();
		BookFlightSegmentType[][] bookFlightSegmentType = new BookFlightSegmentType[segmentInfos.size()][1];
		AtomicInteger segmentSize = new AtomicInteger(0);
		segmentInfos.forEach(segmentInfo -> {
			bookFlightSegmentType[segmentSize.intValue()] = new BookFlightSegmentType[1];
			bookFlightSegmentType[segmentSize.intValue()][0] = new BookFlightSegmentType();
			FlightSegmentBaseTypeDepartureAirport departureAirport = new FlightSegmentBaseTypeDepartureAirport();
			departureAirport.setLocationCode(segmentInfo.getDepartAirportInfo().getCode());
			departureAirport.setTerminal(TERMINALX);
			bookFlightSegmentType[segmentSize.intValue()][0].setDepartureAirport(departureAirport);
			FlightSegmentBaseTypeArrivalAirport arrivalAirport = new FlightSegmentBaseTypeArrivalAirport();
			arrivalAirport.setLocationCode(segmentInfo.getArrivalAirportInfo().getCode());
			arrivalAirport.setTerminal(TERMINALX);
			bookFlightSegmentType[segmentSize.intValue()][0].setArrivalAirport(arrivalAirport);
			bookFlightSegmentType[segmentSize.intValue()][0]
					.setDepartureDateTime(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
			bookFlightSegmentType[segmentSize.intValue()][0]
					.setArrivalDateTime(TgsDateUtils.getCalendar(segmentInfo.getArrivalTime()));
			FlightNumberType flightNumber = new FlightNumberType();
			flightNumber.setFlightNumber(getFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber()));
			bookFlightSegmentType[segmentSize.intValue()][0].setFlightNumber(flightNumber);
			bookFlightSegmentType[segmentSize.intValue()][0]
					.setRPH(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
			segmentSize.getAndIncrement();
		});
		originDestInfo.setOriginDestinationOption(bookFlightSegmentType);
		return originDestInfo;
	}

	private double parsePriceQuoteResponse(String priceQuoteResponseXML) {
		Document doc;
		double currencyFare = 0;
		try {
			doc = XMLUtils.createDocumentFromString(priceQuoteResponseXML);
			double amountToPaid = Double.parseDouble(XMLUtils.getAttrributeValue(XMLUtils.getNode(doc,
					"Body/OTA_AirPriceRS/PricedItineraries/PricedItinerary/AirItineraryPricingInfo/ItinTotalFare/TotalFare"),
					"Amount"));
			String currencyCode = XMLUtils.getAttrributeValue(XMLUtils.getNode(doc,
					"Body/OTA_AirPriceRS/PricedItineraries/PricedItinerary/AirItineraryPricingInfo/ItinTotalFare/TotalFare"),
					"CurrencyCode");
			currencyFare = getAmountBasedOnCurrency(amountToPaid, currencyCode);
		} catch (Exception e) {
			log.error("Unable to parse price response for booking {}", bookingId, e);
		}
		return currencyFare;
	}

}
