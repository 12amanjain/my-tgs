package com.tgs.services.fms.sources.airarabia;

import com.tgs.services.base.AxisRequestResponseListener;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AirArabiaBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	protected AxisRequestResponseListener listner = null;

	protected AirArabiaBindingService bindingService;

	AirArabiaBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
		listner = new AxisRequestResponseListener(pnr, supplierConf.getBasicInfo().getSupplierName());
	}


	public void initialize() {
		listner = new AxisRequestResponseListener(pnr, supplierConf.getBasicInfo().getSupplierName());
		bindingService = AirArabiaBindingService.builder().cacheCommunicator(cachingCommunicator).build();
	}


	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		AirArabiaRetrieveManager retrieveManager =
				AirArabiaRetrieveManager.builder().configuration(supplierConf).bindingService(bindingService)
						.moneyExchnageComm(moneyExchangeComm).listener(listner).bookingUser(bookingUser).build();
		pnrBooking = retrieveManager.retrieveBooking(pnr);
		updateTimeLimit(retrieveManager.getTimeLimit());
		return pnrBooking;
	}
}
