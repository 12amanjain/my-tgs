package com.tgs.services.fms.sources.airarabia;

import com.tgs.services.base.AxisRequestResponseListener;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.lang3.StringUtils;
import org.opentravel.www.OTA._2003._05.AAResWebServicesHttpBindingStub;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

@Slf4j
@Service
public class AirArabiaInfoFactory extends AbstractAirInfoFactory {

	protected AxisRequestResponseListener listner = null;

	protected AirArabiaBindingService bindingService;

	protected boolean isStorePriceSearch = true;

	protected String jSessionId;

	public AirArabiaInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize() {
		if (StringUtils.isEmpty(contextData.getBookingId())) {
			listner = new AxisRequestResponseListener(searchQuery.getSearchId(),
					supplierConf.getBasicInfo().getSupplierName());
		} else {
			listner = new AxisRequestResponseListener(contextData.getBookingId(),
					supplierConf.getBasicInfo().getSupplierName());
		}
		bindingService = AirArabiaBindingService.builder().cacheCommunicator(cachingComm).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
	}

	@Override
	protected void searchAvailableSchedules() {
		AirArabiaSearchManager searchManager = null;
		try {
			initialize();
			searchManager = AirArabiaSearchManager.builder().listener(listner).configuration(supplierConf)
					.bindingService(bindingService).jSessionId(jSessionId).searchQuery(searchQuery)
					.bookingId(contextData.getBookingId()).moneyExchnageComm(moneyExchnageComm).bookingUser(user)
					.build();
			searchManager.getSessionBinding();
			searchResult = searchManager.getAvailability(isStorePriceSearch);
		} finally {
			bindingService.storeSessionMetaInfo(supplierConf, AAResWebServicesHttpBindingStub.class,
					searchManager.stub);
		}
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		String jSessionId = null;
		String transactionId = null;
		boolean isReviewSuccess = false;
		AirArabiaReviewManager reviewManager = null;
		try {
			initialize();
			listner.setKey(bookingId);
			isStorePriceSearch = false;
			this.searchAvailableSchedules();
			selectedTrip = AirUtils.filterTripFromAirSearchResult(searchResult, selectedTrip);
			if (selectedTrip != null) {
				listner.setKey(bookingId);
				this.jSessionId = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
				reviewManager =
						AirArabiaReviewManager.builder().configuration(supplierConf).bindingService(bindingService)
								.searchQuery(searchQuery).listener(listner).jSessionId(jSessionId).bookingId(bookingId)
								.moneyExchnageComm(moneyExchnageComm).bookingUser(user).build();
				reviewManager.getSessionBinding();
				selectedTrip = reviewManager.doPriceQuote(selectedTrip);
				if (selectedTrip != null) {
					List<TripInfo> ssrTripInfos = AirUtils.splitTripInfo(selectedTrip, false);
					for (TripInfo ssrTripInfo : ssrTripInfos) {
						reviewManager.setJSessionId(reviewManager.getTripTokenId(ssrTripInfo));
						reviewManager.doBaggageSellService(ssrTripInfo);
						// Commented Meal Service Due Currency Conversion Not Supporting
						// reviewManager.doMealSellService(ssrTripInfo);
					}
				}
				jSessionId = reviewManager.getJSessionId();
				transactionId = reviewManager.getTransactionIdentifier();
				isReviewSuccess = true;
			}
		} finally {
			if (isReviewSuccess && StringUtils.isNotBlank(jSessionId) && StringUtils.isNotEmpty(transactionId)) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().build();
				String reference = StringUtils.join(jSessionId, "#", transactionId);
				sessionInfo.setSessionToken(reference);
				storeBookingSession(bookingId, sessionInfo, null, selectedTrip);
			}
			bindingService.storeSessionMetaInfo(supplierConf, AAResWebServicesHttpBindingStub.class,
					reviewManager.stub);
		}
		return selectedTrip;
	}

	@Override
	public Boolean initializeStubs() {
		// bindingService = AirArabiaBindingService.builder().cacheCommunicator(cachingComm).build();
		// AirArabiaServiceManager serviceManager =
		// AirArabiaServiceManager.builder().configuration(supplierConf).build();
		// serviceManager.getSessionBinding();
		// bindingService.storeSessionMetaInfo(supplierConf, AAResWebServicesHttpBindingStub.class,
		// serviceManager.stub);
		return true;
	}
}
