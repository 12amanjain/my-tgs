package com.tgs.services.fms.sources.airarabia;

import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.Amenities;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.XMLUtils;
import com.tgs.utils.exception.air.NoPNRFoundException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.opentravel.www.OTA._2003._05.OTA_AirBookRS;
import org.opentravel.www.OTA._2003._05.OTA_ReadRQReadRequests;
import org.opentravel.www.OTA._2003._05.OTA_ReadRQReadRequestsReadRequest;
import org.opentravel.www.OTA._2003._05.UniqueID_Type;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;

@Slf4j
@SuperBuilder
final class AirArabiaRetrieveManager extends AirArabiaServiceManager {


	public AirImportPnrBooking retrieveBooking(String pnr) {
		AirImportPnrBooking pnrBooking = null;
		setBookingId(pnr);
		OTA_AirBookRS bookRS = retrieveByPNR(pnr);
		if (bookRS != null && StringUtils.isNotEmpty(bookRS.getEchoToken())) {
			Document doc = XMLUtils.createDocumentFromString(bookRS.getEchoToken());
			List<TripInfo> tripInfos = parseBookingResponse(doc, pnr);
			GstInfo gstInfo = getGSTInfo(doc);
			DeliveryInfo deliveryInfo = getDeliveryInfo(doc);
			pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).gstInfo(gstInfo)
					.build();
		} else {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + pnr);
		}
		return pnrBooking;
	}

	private Map<PaxType, Integer> getPaxInfo(List<TripInfo> tripInfos) {
		Map<PaxType, Integer> paxInfo = new HashMap<>();
		List<FlightTravellerInfo> travellerInfos =
				tripInfos.get(0).getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
		for (FlightTravellerInfo traveller : travellerInfos) {
			if (paxInfo.get(traveller.getPaxType()) == null)
				paxInfo.put(traveller.getPaxType(), 0);
			paxInfo.put(traveller.getPaxType(), paxInfo.get(traveller.getPaxType()) + 1);
		}
		return paxInfo;
	}

	public OTA_AirBookRS retrieveByPNR(String pnr) {
		OTA_AirBookRS airBookRS = new OTA_AirBookRS();
		try {
			String bookingReqXML = prepareBookingRequest(pnr);
			String response = getResponseByRequest(bookingReqXML, "GetPNRStatus", "getReservationbyPNR");
			airBookRS.setEchoToken(response);
		} catch (Exception e) {
			log.error("PNR Retrieve Failed PNR{} cause {} ", pnr, e);
		}
		return airBookRS;
	}

	private String prepareBookingRequest(String pnr) {
		StringBuffer sbuf = new StringBuffer();
		sbuf.append(getPreRequestData());
		sbuf.append(getAirBookRQData());
		sbuf.append(getPNRRequest(pnr));
		sbuf.append(getPostPNRRequest(pnr));
		sbuf.append(getPostRequestData());
		return sbuf.toString();
	}

	private StringBuffer getAirBookRQData() {
		Calendar nowCal = Calendar.getInstance();
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.PNRRBOOKDATA);
		Object[] args = {echoToken, getXMLTimeStamp(nowCal), "20061", TERMINALID,
				configuration.getSupplierCredential().getUserName()};
		return new StringBuffer(msgFormat.format(args));
	}

	private StringBuffer getPostPNRRequest(String pnr) {
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.POSTPNRREQUEST);
		Object[] args = {pnr};
		return new StringBuffer(msgFormat.format(args));
	}

	private StringBuffer getPNRRequest(String pnr) {
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.PNRREQUEST);
		Object[] args = {pnr};
		return new StringBuffer(msgFormat.format(args));
	}


	private OTA_ReadRQReadRequests getReadRequest(String pnr) {
		OTA_ReadRQReadRequests readRequest = new OTA_ReadRQReadRequests();
		OTA_ReadRQReadRequestsReadRequest[] pnrRQ = new OTA_ReadRQReadRequestsReadRequest[1];
		pnrRQ[0] = new OTA_ReadRQReadRequestsReadRequest();
		UniqueID_Type uniqueID = new UniqueID_Type();
		uniqueID.setType("14");
		uniqueID.setID(pnr);
		pnrRQ[0].setUniqueID(uniqueID);
		readRequest.setReadRequest(pnrRQ);
		return readRequest;
	}

	private GstInfo getGSTInfo(Document doc) {
		GstInfo gstInfo = new GstInfo();
		return gstInfo;
	}

	private DeliveryInfo getDeliveryInfo(Document doc) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		Node contactInfoNode = XMLUtils.getNode(doc,
				"Body/OTA_AirBookRS/AirReservation/TPA_Extensions/AAAirReservationExt/ContactInfo");
		String email = XMLUtils.getElementValue(XMLUtils.getNode(contactInfoNode, "Email"));
		List<String> emails = new ArrayList<>();
		emails.add(email);
		deliveryInfo.setEmails(emails);
		String contact = XMLUtils.getElementValue(XMLUtils.getNode(contactInfoNode, "Telephone/PhoneNumber"));
		List<String> contacts = new ArrayList<>();
		contacts.add(contact);
		deliveryInfo.setContacts(contacts);
		return deliveryInfo;
	}

	private List<TripInfo> parseBookingResponse(Document doc, String pnr) {
		List<TripInfo> tripInfos = new ArrayList<>();
		String cabinClassCode = "";
		NodeList originDestinationNodes = XMLUtils.getNodeList(doc,
				"Body/OTA_AirBookRS/AirReservation/AirItinerary/OriginDestinationOptions/OriginDestinationOption");
		if (originDestinationNodes != null && originDestinationNodes.getLength() > 0) {
			for (int tripIndex = 0; tripIndex < originDestinationNodes.getLength(); tripIndex++) {
				Node originDestination = originDestinationNodes.item(tripIndex);
				NodeList flightSegmentList = originDestination.getChildNodes();
				List<SegmentInfo> segmentInfos = new ArrayList<>();
				try {
					for (int segmentIndex = 0; segmentIndex < flightSegmentList.getLength(); segmentIndex++) {
						Node flightSegment = flightSegmentList.item(segmentIndex);
						cabinClassCode = XMLUtils.getAttrributeValue(flightSegment, "ResCabinClass");
						SegmentInfo segmentInfo = getSegmentInfo(flightSegment);
						String segmentReference = XMLUtils.getAttrributeValue(flightSegment, "FlightNumber");
						parseTravellersInfo(doc, pnr, segmentReference, segmentInfo, cabinClassCode);
						segmentInfos.add(segmentInfo);
					}
				} catch (Exception e) {
					segmentInfos = null;
				}
				if (CollectionUtils.isNotEmpty(segmentInfos)) {
					TripInfo tripInfo = new TripInfo();
					tripInfo.setSegmentInfos(segmentInfos);
					sortSegmentInfosOnDepatureTime(tripInfo.getSegmentInfos());
					tripInfos.add(tripInfo);
				}
			}
		}
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			setPricingInfo(tripInfos, doc, cabinClassCode);
			updateTicketingTimeLimit(doc);
		}
		return tripInfos;
	}

	private SegmentInfo getSegmentInfo(Node flightSegment) {
		NamedNodeMap segmentAttributes = flightSegment.getAttributes();
		NodeList flightSegmentChildren = flightSegment.getChildNodes();
		Node departureAirportNode = flightSegmentChildren.item(0);
		Node arrivalAirportNode = flightSegmentChildren.item(1);
		SegmentInfo segmentInfo = new SegmentInfo();
		String airportCode = departureAirportNode.getAttributes().getNamedItem("LocationCode").getNodeValue();
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(airportCode));
		airportCode = arrivalAirportNode.getAttributes().getNamedItem("LocationCode").getNodeValue();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(airportCode));
		segmentInfo
				.setDepartTime(getLocalTimeFromUTC(segmentAttributes.getNamedItem("DepartureDateTime").getNodeValue()));
		segmentInfo
				.setArrivalTime(getLocalTimeFromUTC(segmentAttributes.getNamedItem("ArrivalDateTime").getNodeValue()));
		Node amentiesNode = segmentAttributes.getNamedItem("SmokingAllowed");
		if (amentiesNode != null) {
			Amenities amenities = new Amenities();
			amenities.setIsSmokingAllowed(Boolean.getBoolean(amentiesNode.getNodeValue()));
			segmentInfo.setAmenities(amenities);
		}
		FlightDesignator designator = FlightDesignator.builder().build();
		designator.setAirlineInfo(AirlineHelper.getAirlineInfo(airlineCode));
		String flightNumber = segmentAttributes.getNamedItem("FlightNumber").getNodeValue().trim();
		designator.setFlightNumber(getFlightNumberWithCode(flightNumber));
		segmentInfo.setFlightDesignator(designator);
		String journeyKey = segmentAttributes.getNamedItem("RPH").getNodeValue();
		List<PriceInfo> priceInfoList = new ArrayList<>();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		priceInfo.getMiscInfo().setJourneyKey(journeyKey);
		priceInfoList.add(priceInfo);
		segmentInfo.setPriceInfoList(priceInfoList);
		boolean isReturnSegment = false;
		Node returnFlagNode = segmentAttributes.getNamedItem("returnFlag");
		if (returnFlagNode != null) {
			isReturnSegment = Boolean.getBoolean(returnFlagNode.getNodeValue());
		}
		segmentInfo.setIsReturnSegment(isReturnSegment);
		segmentInfo.setStops(0);
		// segmentInfo.setOperatedByAirlineInfo(AirlineHelper.getAirlineInfo(airlineCode));
		segmentInfo.setDuration(segmentInfo.calculateDuration());
		return segmentInfo;
	}

	private void sortSegmentInfosOnDepatureTime(List<SegmentInfo> segmentInfos) {
		Collections.sort(segmentInfos, new Comparator<SegmentInfo>() {
			public int compare(SegmentInfo s1, SegmentInfo s2) {
				return s1.getDepartTime().compareTo(s2.getDepartTime());
			}
		});
	}

	private void setPricingInfo(List<TripInfo> tripInfos, Document doc, String cabinClassCode) {
		Map<PaxType, Integer> paxInfo = getPaxInfo(tripInfos);
		tripInfos.forEach(trip -> {
			trip.setPaxInfo(paxInfo);
		});
		setZeroPricing(tripInfos, cabinClassCode);
		PriceInfo priceInfo = tripInfos.get(0).getSegmentInfos().get(0).getPriceInfo(0);
		int paxCount = AirUtils.getPaxCountFromPaxInfo(paxInfo, true);
		Map<PaxType, FareDetail> fareDetails = new HashMap<>();
		for (Entry<PaxType, Integer> entry : paxInfo.entrySet()) {
			FareDetail fareDetail = new FareDetail();
			double totalTax = 0.0, yQFee = 0.0;
			double baseFare = getAmountFromNode(
					XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/BaseFare"));
			double totalFare = getAmountFromNode(
					XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/TotalFare"));
			NodeList taxNodeList =
					XMLUtils.getNodeList(doc, "Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/Taxes/Tax");
			for (int taxIndex = 0; taxIndex < taxNodeList.getLength(); taxIndex++) {
				Node taxNode = taxNodeList.item(taxIndex);
				totalTax += getAmountFromNode(taxNode);
			}
			NodeList feeNodeList =
					XMLUtils.getNodeList(doc, "Body/OTA_AirBookRS/AirReservation/PriceInfo/ItinTotalFare/Fees/Fee");
			for (int feeIndex = 0; feeIndex < feeNodeList.getLength(); feeIndex++) {
				Node feeNode = feeNodeList.item(feeIndex);
				yQFee += getAmountFromNode(feeNode);
			}
			Map<FareComponent, Double> fareComponents = new HashMap<>();

			addNewFareComponent(fareComponents, FareComponent.BF, baseFare, paxCount);
			addNewFareComponent(fareComponents, FareComponent.TF, totalFare, paxCount);
			addNewFareComponent(fareComponents, FareComponent.AT, totalTax, paxCount);
			addNewFareComponent(fareComponents, FareComponent.YQ, yQFee, paxCount);

			fareDetail.setFareComponents(fareComponents);
			fareDetail.setCabinClass(CabinClass.getEnumFromCode(cabinClassCode));
			fareDetails.put(entry.getKey(), fareDetail);
		}
		priceInfo.setFareDetails(fareDetails);
		List<PriceInfo> priceInfoList = new ArrayList<>();
		priceInfoList.add(priceInfo);
		tripInfos.get(0).getSegmentInfos().get(0).setPriceInfoList(priceInfoList);
	}

	// We are getting total trip fare from supplier. So splitting fare on paxtypewise.
	private Map<FareComponent, Double> addNewFareComponent(Map<FareComponent, Double> fareComponents,
			FareComponent newComponent, double fareTotal, int paxCount) {
		if (fareTotal > 0) {
			fareComponents.put(newComponent, fareTotal / paxCount);
		}
		return fareComponents;
	}

	private void setZeroPricing(List<TripInfo> tripInfos, String cabinClassCode) {
		tripInfos.forEach(trip -> {
			AtomicInteger segmentNum = new AtomicInteger(0);
			trip.getSegmentInfos().forEach(segment -> {
				segment.setSegmentNum(segmentNum.get());
				Map<PaxType, FareDetail> fareDetails = new HashMap<>();
				for (Entry<PaxType, Integer> entry : trip.getPaxInfo().entrySet()) {
					PaxType paxType = entry.getKey();
					int paxCount = entry.getValue();
					if (paxCount > 0) {
						FareDetail fareDetail = new FareDetail();
						fareDetail.setCabinClass(CabinClass.getEnumFromCode(cabinClassCode));
						Map<FareComponent, Double> fareComponents = new HashMap<>();
						fareComponents.put(FareComponent.TF, 0.0);
						fareComponents.put(FareComponent.BF, 0.0);
						fareDetail.setFareComponents(fareComponents);
						fareDetails.put(paxType, fareDetail);
					}
				}
				segment.getPriceInfo(0).setFareDetails(fareDetails);
				segmentNum.incrementAndGet();
			});
		});
	}

	private double getAmountFromNode(Node node) {
		double amount = 0.0;
		String amountNode = XMLUtils.getAttrributeValue(node, "Amount");
		String currencyCode = XMLUtils.getAttrributeValue(node, "CurrencyCode");
		if (StringUtils.isNotEmpty(amountNode)) {
			amount = getAmountBasedOnCurrency(Double.valueOf(XMLUtils.getAttrributeValue(node, "Amount")),
					currencyCode);
		}
		return amount;
	}

	private void parseTravellersInfo(Document doc, String pnr, String segmentReference, SegmentInfo segmentInfo,
			String cabinClassCode) {
		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		Node travellersInformationNode = XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/TravelerInfo");
		NodeList airTravellerNodeList = XMLUtils.getNodeList(travellersInformationNode, "AirTraveler");
		for (int index = 0; index < airTravellerNodeList.getLength(); index++) {
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			Node airTravellerNode = airTravellerNodeList.item(index);
			String firstName = XMLUtils.getElementValue(XMLUtils.getNode(airTravellerNode, "PersonName/GivenName"));
			String lastName = XMLUtils.getElementValue(XMLUtils.getNode(airTravellerNode, "PersonName/Surname"));
			String title = XMLUtils.getElementValue(XMLUtils.getNode(airTravellerNode, "PersonName/NameTitle"));
			travellerInfo.setFirstName(firstName);
			travellerInfo.setLastName(lastName);
			travellerInfo.setTitle(title);
			travellerInfo.setPnr(pnr);
			FareDetail fareDetail = new FareDetail();
			fareDetail.setFareComponents(new HashMap<>());
			fareDetail.setCabinClass(CabinClass.getEnumFromCode(cabinClassCode));
			travellerInfo.setFareDetail(fareDetail);
			travellerInfo.setPaxType(getPaxType(XMLUtils.getAttrributeValue(airTravellerNode, "PassengerTypeCode")));
			Node travelerRefNumberNode = XMLUtils.getNode(airTravellerNode, "TravelerRefNumber");
			Node documentNode = XMLUtils.getNode(airTravellerNode, "Document");
			if (documentNode != null) {
				String docType = XMLUtils.getAttrributeValue(documentNode, "DocType");
				if (docType.equalsIgnoreCase("PSPT")) {
					travellerInfo
							.setPassportNationality(XMLUtils.getAttrributeValue(documentNode, "DocHolderNationality"));
					travellerInfo.setPassportNumber(XMLUtils.getAttrributeValue(documentNode, "DocID"));
				}
			}
			NodeList eTicketInfoNodList = XMLUtils.getNodeList(airTravellerNode, "ETicketInfo/ETicketInfomation");
			for (int ticketIndex = 0; ticketIndex < eTicketInfoNodList.getLength(); ticketIndex++) {
				Node ticketNode = eTicketInfoNodList.item(ticketIndex);
				String flightSegmentCode = XMLUtils.getAttrributeValue(ticketNode, "flightSegmentCode");
				if (flightSegmentCode.equals(StringUtils.join(segmentInfo.getDepartureAirportCode(), "/",
						segmentInfo.getArrivalAirportCode()))) {
					travellerInfo.setTicketNumber(XMLUtils.getAttrributeValue(ticketNode, "eTicketNo"));
					break;
				}
			}
			setTravellerSpecialService(travellersInformationNode, travellerInfo,
					XMLUtils.getAttrributeValue(travelerRefNumberNode, "RPH"), segmentReference);
			travellerInfos.add(travellerInfo);
		}
		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfos).build();
		segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
	}

	private PaxType getPaxType(String paxType) {
		if (paxType.equalsIgnoreCase("CHD")) {
			return PaxType.CHILD;
		}
		return PaxType.getPaxType(paxType);
	}

	// Special services matches with pax ref and segment Ref.
	private void setTravellerSpecialService(Node travellersInformationNode, FlightTravellerInfo travellerInfo,
			String travellerReference, String segmentReference) {
		Node specialServiceNode = XMLUtils.getNode(travellersInformationNode, "SpecialReqDetails");
		if (specialServiceNode != null) {

			NodeList baggageServiceList = XMLUtils.getNodeList(specialServiceNode, "BaggageRequests/BaggageRequest");
			for (int index = 0; index < baggageServiceList.getLength(); index++) {
				Node baggageNode = baggageServiceList.item(index);
				String specialServiceTravellerReference =
						XMLUtils.getAttrributeValue(baggageNode, "TravelerRefNumberRPHList");
				String specialServiceSegmentReference = XMLUtils.getAttrributeValue(baggageNode, "FlightNumber");
				if (travellerReference.equalsIgnoreCase(specialServiceTravellerReference)
						&& segmentReference.equalsIgnoreCase(specialServiceSegmentReference)) {
					String baggageCode = XMLUtils.getAttrributeValue(baggageNode, "baggageCode");
					if (StringUtils.isNotEmpty(baggageCode)
							&& !baggageCode.contains(AirArabiaXMLConstantsInfo.NO_BAG)) {
						SSRInformation ssrBaggageInfo = new SSRInformation();
						// Code and desc are same.
						ssrBaggageInfo.setDesc(baggageCode);
						ssrBaggageInfo.setCode(baggageCode);
						travellerInfo.setSsrBaggageInfo(ssrBaggageInfo);
					}
				}
			}
		}
	}

}
