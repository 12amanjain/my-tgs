package com.tgs.services.fms.sources.airarabia;


import com.google.gson.reflect.TypeToken;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.utils.common.XMLUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis.types.Language;
import org.apache.axis.types.NonNegativeInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.opentravel.www.OTA._2003._05.AA_OTA_AirBaggageDetailsRQ;
import org.opentravel.www.OTA._2003._05.OTA_AirPriceRQ;
import org.opentravel.www.OTA._2003._05.OTA_AirPriceRS;
import org.opentravel.www.OTA._2003._05.Target;
import org.opentravel.www.OTA._2003._05.AA_OTA_AirBaggageDetailsRQBaggageDetailsRequestsBaggageDetailsRequest;
import org.opentravel.www.OTA._2003._05.AA_OTA_AirMealDetailsRQMealDetailsRequestsMealDetailsRequest;
import org.opentravel.www.OTA._2003._05.OTA_AirPriceRQServiceTaxCriteriaOptions;
import org.opentravel.www.OTA._2003._05.AA_OTA_AirMealDetailsRQ;
import org.opentravel.www.OTA._2003._05.AA_OTA_AirBaggageDetailsRS;
import org.opentravel.www.OTA._2003._05.AA_OTA_AirMealDetailsRS;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import java.rmi.RemoteException;
import java.util.List;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


@Getter
@Setter
@SuperBuilder
@Slf4j
final class AirArabiaReviewManager extends AirArabiaServiceManager {

	public TripInfo doPriceQuote(TripInfo tripInfo) throws NoSeatAvailableException, SupplierRemoteException {
		try {
			if (stub == null) {
				this.stub = getSessionBinding();
			}
			jSessionId = StringUtils.isNotEmpty(jSessionId) ? jSessionId : getTripTokenId(tripInfo);
			OTA_AirPriceRS priceRS = stub.getPrice(getAirPriceQuoteRQ(tripInfo));
			String requestXML = priceRS.getEchoToken().replaceAll("FlexiOperations", "FlightSegment");
			String priceResp = getResponseByRequest(requestXML, "getPrice", "getPrice");
			if (!checkIfAnyPriceError(priceResp)) {
				tripInfo = parsePriceInfo(priceResp, tripInfo);
			}
			if (tripInfo != null) {
				updateSegmentMisc(tripInfo);
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		}
		return tripInfo;
	}

	public String getTripTokenId(TripInfo tripInfo) {
		if (tripInfo != null && CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			return tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
		}
		return jSessionId;
	}

	private OTA_AirPriceRQ getAirPriceQuoteRQ(TripInfo tripInfo) {
		OTA_AirPriceRQ priceRQ = new OTA_AirPriceRQ();
		priceRQ.setPOS(getPointOfSale());
		priceRQ.setVersion(version);
		priceRQ.setTarget(Target.Test);
		priceRQ.setPrimaryLangID(new Language(LOCALECODE));
		priceRQ.setEchoToken(echoToken);
		priceRQ.setSequenceNmbr(new NonNegativeInteger(SEQUENCE));
		priceRQ.setTimeStamp(Calendar.getInstance());
		priceRQ.setTransactionIdentifier(getTransactionId(tripInfo));
		priceRQ.setAirItinerary(getAirItinerary(tripInfo));
		priceRQ.setTravelerInfoSummary(getTravellerInfoSummary());
		priceRQ.setServiceTaxCriteriaOptions(getServieTaxOption());
		return priceRQ;
	}

	private OTA_AirPriceRQServiceTaxCriteriaOptions getServieTaxOption() {
		ClientGeneralInfo generalInfo = ServiceCommunicatorHelper.getClientInfo();
		OTA_AirPriceRQServiceTaxCriteriaOptions taxOptions = new OTA_AirPriceRQServiceTaxCriteriaOptions();
		AtomicBoolean isTaxApplicable = new AtomicBoolean();
		RouteInfo routeInfo = searchQuery.getRouteInfos().get(0);
		if (routeInfo.getFromCityOrAirport().getCountry().equals(generalInfo.getCountry())
				|| routeInfo.getToCityOrAirport().getCountry().equals(generalInfo.getCountry())) {
			isTaxApplicable.set(true);
		}
		if (isTaxApplicable.get() && generalInfo != null) {
			taxOptions.setTaxRegistrationNo(StringUtils.EMPTY);
			taxOptions.setCountryCode(generalInfo.getNationality());
			taxOptions.setStateCode("27");
		}
		return taxOptions;
	}


	private TripInfo parsePriceInfo(String priceRS, TripInfo tripInfo) throws NoSeatAvailableException {

		Document doc = XMLUtils.createDocumentFromString(priceRS);
		Node node1 = XMLUtils.getNode(doc,
				"Body/OTA_AirPriceRS/PricedItineraries/PricedItinerary/AirItineraryPricingInfo/PTC_FareBreakdowns");
		if (node1 != null && node1.hasChildNodes()) {
			NodeList nodeList = node1.getChildNodes();
			Node node = null;
			List<PriceInfo> priceInfos = new ArrayList<>();
			if (Objects.nonNull(nodeList)) {

				// PriceInfo will be initialized with some misc infos while forming segmentInfo's.
				PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
				priceInfo.getMiscInfo().setTraceId(transactionIdentifier);
				NodeList taxNodes = null;
				NodeList feeNodes = null;
				Map<PaxType, FareDetail> paxFareDetails =
						parseFareDetail(nodeList, node, taxNodes, feeNodes, priceInfo);
				if (MapUtils.isNotEmpty(paxFareDetails)) {
					priceInfo.setFareDetails(paxFareDetails);
					priceInfos.add(priceInfo);
				} else {
					throw new NoSeatAvailableException();
				}
			}
			// Set priceInfo in Trip
			if (tripInfo != null && tripInfo.isSegmentsNotEmpty()) {
				tripInfo.getSegmentInfos().get(0).setPriceInfoList(priceInfos);
				copyStaticInfoSegments(priceInfos, tripInfo);
			}
		} else {
			throw new NoSeatAvailableException();
		}
		return tripInfo;
	}

	private Map<PaxType, FareDetail> parseFareDetail(NodeList nodeList, Node node, NodeList taxNodes, NodeList feeNodes,
			PriceInfo priceInfo) {
		Map<PaxType, FareDetail> paxFareDetails = new HashMap<>();
		for (int i = 0; i < nodeList.getLength(); i++) {
			// List of Pax PriceInfo
			node = nodeList.item(i);
			// priceInfo.setFareIdentifier(node.getAttributes().getNamedItem("PricingSource").getNodeValue());
			PaxType paxType = getPaxCode(node);
			if (paxFareDetails.get(paxType) == null) {
				priceInfo.setFareIdentifier(FareType.PUBLISHED);
				FareDetail fareDetail = priceInfo.getFareDetail(paxType, new FareDetail());
				String bookingClass =
						XMLUtils.getNode(node, "FareBasisCodes/FareBasisCode").getFirstChild().getNodeValue();
				String currencyCode = getCurrencyFromNode(XMLUtils.getNode(node, "PassengerFare/BaseFare"));
				// Base Fare break Up
				double baseFare = getAmountFromNode(XMLUtils.getNode(node, "PassengerFare/BaseFare"), currencyCode);

				// Tax Break Up (Which includes GST)
				double tax = 0.0;
				taxNodes = XMLUtils.getNode(node, "PassengerFare/Taxes").getChildNodes();
				for (int j = 0; j < taxNodes.getLength(); j++) {
					tax += getAmountFromNode(taxNodes.item(j), currencyCode);
				}

				double gst = 0.0;
				for (int j = 0; j < taxNodes.getLength(); j++) {
					gst += getGSTAmountFromNode(taxNodes.item(j), currencyCode);
				}

				// Fuel Surcharge Fare
				double yQFare = 0.0;
				feeNodes = XMLUtils.getNode(node, "PassengerFare/Fees").getChildNodes();
				for (int j = 0; j < feeNodes.getLength(); j++) {
					yQFare += getAmountFromNode(feeNodes.item(j), currencyCode);
				}

				double totalFare = getAmountFromNode(XMLUtils.getNode(node, "PassengerFare/TotalFare"), currencyCode);
				fareDetail.setClassOfBooking(bookingClass);
				fareDetail.setCabinClass(searchQuery.getCabinClass());
				fareDetail.setFareType(com.tgs.services.base.enums.FareType.PUBLISHED.getName());
				fareDetail.setCabinClass(searchQuery.getCabinClass());
				fareDetail.setSeatRemaining(9);
				fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
				setFareComponents(fareDetail, baseFare, tax, yQFare, gst, totalFare);
				fareDetail.getFareComponents().put(FareComponent.TF, getTotalFare(fareDetail.getFareComponents()));
				paxFareDetails.put(paxType, fareDetail);
			}
		}
		return paxFareDetails;
	}

	private Double getTotalFare(Map<FareComponent, Double> fareComponents) {
		Double totalAmount = 0d;
		if (MapUtils.isNotEmpty(fareComponents)) {
			for (FareComponent component : fareComponents.keySet()) {
				if (!FareComponent.TF.equals(component) && component.airlineComponent()) {
					totalAmount += fareComponents.get(component);
				}
			}
		}
		return totalAmount;
	}

	private void copyStaticInfoSegments(List<PriceInfo> priceInfos, TripInfo tripInfo) {
		for (int index = 1; index < tripInfo.getSegmentInfos().size(); index++) {
			List<PriceInfo> newPriceInfos = new ArrayList<>();
			SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(index);
			List<PriceInfo> finalPriceInfos = tripInfo.getSegmentInfos().get(index).getPriceInfoList();
			AtomicInteger priceIndex = new AtomicInteger(0);
			priceInfos.forEach(priceInfo -> {
				PriceInfo priceInfo1 = finalPriceInfos.get(priceIndex.get());
				priceInfo1.setFareIdentifier(priceInfo.getFareIdentifier());
				Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
				priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
					FareDetail copyFareDetail1 = new FareDetail();
					copyFareDetail1.setBaggageInfo(fareDetail.getBaggageInfo());
					copyFareDetail1.setRefundableType(fareDetail.getRefundableType());
					copyFareDetail1.setCabinClass(fareDetail.getCabinClass());
					copyFareDetail1.setSeatRemaining(fareDetail.getSeatRemaining());
					copyFareDetail1.setIsHandBaggage(BooleanUtils.isTrue(fareDetail.getIsHandBaggage()));
					copyFareDetail1.setIsMealIncluded(BooleanUtils.isTrue(fareDetail.getIsMealIncluded()));
					fareDetailMap.put(paxType, copyFareDetail1);
				}));
				priceInfo1.setFareDetails(fareDetailMap);
				newPriceInfos.add(priceInfo1);
				priceIndex.getAndIncrement();
			});
			segmentInfo.setPriceInfoList(newPriceInfos);
		}
	}

	public PaxType getPaxCode(Node node) {
		String ptcPaxCode = XMLUtils.getAttrributeValue(node.getFirstChild(), "Code");
		if (ptcPaxCode.equalsIgnoreCase("CHD")) {
			ptcPaxCode = PaxType.CHILD.getType();
		}
		return PaxType.getPaxType(ptcPaxCode);
	}

	private double getAmountFromNode(Node node, String currencyCode) {
		String taxNodeName = XMLUtils.getAttrributeValue(node, "TaxCode");
		if (taxNodeName != null && !taxNodeName.contains("GST")) {
			Double amount = Double.parseDouble(XMLUtils.getAttrributeValue(node, "Amount"));
			return getAmountBasedOnCurrency(amount, currencyCode);
		}
		return 0;
	}

	private double getGSTAmountFromNode(Node node, String currencyCode) {
		String taxNodeName = XMLUtils.getAttrributeValue(node, "TaxCode");
		if (taxNodeName != null && taxNodeName.contains("GST")) {
			Double amount = Double.parseDouble(XMLUtils.getAttrributeValue(node, "Amount"));
			return getAmountBasedOnCurrency(amount, currencyCode);
		}
		return 0;
	}

	private String getCurrencyFromNode(Node node) {
		return XMLUtils.getAttrributeValue(node, "CurrencyCode");
	}

	private static FareDetail setFareComponents(FareDetail fareDetail, Double baseFare, Double tax, Double fuelFare,
			Double gst, Double totalFare) {
		Map<FareComponent, Double> fareComponent = new HashMap<>();
		fareComponent.put(FareComponent.BF, baseFare);
		fareComponent.put(FareComponent.AT, tax);
		fareComponent.put(FareComponent.YQ, fuelFare);
		fareComponent.put(FareComponent.AGST, gst);
		fareComponent.put(FareComponent.TF, totalFare);
		fareDetail.setFareComponents(fareComponent);
		return fareDetail;
	}

	public void doBaggageSellService(TripInfo ssrTripInfo) {
		AA_OTA_AirBaggageDetailsRQ baggageDetailsRQ = new AA_OTA_AirBaggageDetailsRQ();
		try {
			baggageDetailsRQ.setPOS(getPointOfSale());
			baggageDetailsRQ.setEchoToken(echoToken);
			baggageDetailsRQ.setPrimaryLangID(new Language(LOCALECODE));
			baggageDetailsRQ.setSequenceNmbr(new NonNegativeInteger(SEQUENCE));
			baggageDetailsRQ.setVersion(version);
			baggageDetailsRQ.setTransactionIdentifier(transactionIdentifier);
			baggageDetailsRQ.setBaggageDetailsRequests(getBaggageDetailRQ(ssrTripInfo));
			AA_OTA_AirBaggageDetailsRS baggageDetailsRS = stub.getBaggageDetails(baggageDetailsRQ);
			String response = getResponseByRequest(baggageDetailsRS.getEchoToken(), "BAGGAGE", "getBaggageDetails");
			setBaggageInfoToTrip(response, ssrTripInfo);
			copySameBagaggeInfoToConnecting(ssrTripInfo);
		} catch (Exception e) {
			log.error("Unable to get Baggage info for booking {} for trip {} ", bookingId, ssrTripInfo, e);
		}
	}

	private void copySameBagaggeInfoToConnecting(TripInfo ssrTripInfo) {
		AtomicReference<SegmentInfo> previousSegment = new AtomicReference<>();
		ssrTripInfo.getSegmentInfos().forEach(segmentInfo -> {
			copySSRBaggageWithAmountZero(segmentInfo, previousSegment);
			if (segmentInfo.getSegmentNum().intValue() == 0) {
				previousSegment.set(segmentInfo);
			}
		});
	}

	private void copySSRBaggageWithAmountZero(SegmentInfo currentSegmentInfo,
			AtomicReference<SegmentInfo> previousSegment) {
		if (previousSegment.get() != null && currentSegmentInfo != null
				&& currentSegmentInfo.getSegmentNum().intValue() > 0) {
			if (currentSegmentInfo.getSsrInfo() == null || MapUtils.isEmpty(currentSegmentInfo.getSsrInfo())
					|| CollectionUtils.isEmpty(currentSegmentInfo.getSsrInfo().get(SSRType.BAGGAGE))) {
				currentSegmentInfo.setSsrInfo(new HashMap<>());
			}
			if (previousSegment.get().getSsrInfo() != null
					&& CollectionUtils.isNotEmpty(previousSegment.get().getSsrInfo().get(SSRType.BAGGAGE))) {
				List<BaggageSSRInformation> baggageSSRInfo = GsonUtils.getGson().fromJson(
						GsonUtils.getGson().toJson(previousSegment.get().getSsrInfo().get(SSRType.BAGGAGE)),
						new TypeToken<List<BaggageSSRInformation>>() {}.getType());
				baggageSSRInfo.forEach(ssrInfo -> {
					ssrInfo.setAmount(null);
				});
				currentSegmentInfo.getSsrInfo().put(SSRType.BAGGAGE, baggageSSRInfo);
			}
		}
	}


	public void doMealSellService(TripInfo ssrTripInfo) {
		AA_OTA_AirMealDetailsRQ mealRequest = new AA_OTA_AirMealDetailsRQ();
		try {
			mealRequest.setEchoToken(echoToken);
			mealRequest.setPrimaryLangID(new Language(LOCALECODE));
			mealRequest.setSequenceNmbr(new NonNegativeInteger(SEQUENCE));
			mealRequest.setTransactionIdentifier(transactionIdentifier);
			mealRequest.setVersion(version);
			mealRequest.setPOS(getPointOfSale());
			mealRequest.setMealDetailsRequests(getMealDetailRequest(ssrTripInfo));
			AA_OTA_AirMealDetailsRS mealResponse = stub.getMealDetails(mealRequest);
			String mealRs = getResponseByRequest(mealResponse.getEchoToken(), "MEAL", "getMealDetails");
			setMealInfoToTrip(mealRs, ssrTripInfo);
		} catch (Exception e) {
			log.error("Unable to get meal info for booking {} for trip {}", bookingId, ssrTripInfo, e);
		}
	}

	private AA_OTA_AirMealDetailsRQMealDetailsRequestsMealDetailsRequest[] getMealDetailRequest(TripInfo ssrTripInfo) {
		AA_OTA_AirMealDetailsRQMealDetailsRequestsMealDetailsRequest[] mealrequest =
				new AA_OTA_AirMealDetailsRQMealDetailsRequestsMealDetailsRequest[ssrTripInfo.getSegmentInfos().size()];
		AtomicInteger segmentIndex = new AtomicInteger(0);
		ssrTripInfo.getSegmentInfos().forEach(segmentInfo -> {
			mealrequest[segmentIndex.intValue()] = new AA_OTA_AirMealDetailsRQMealDetailsRequestsMealDetailsRequest();
			mealrequest[segmentIndex.intValue()].setFlightSegmentInfo(getFlightSegmentType(segmentInfo));
			segmentIndex.getAndIncrement();
		});
		return mealrequest;
	}

	private AA_OTA_AirBaggageDetailsRQBaggageDetailsRequestsBaggageDetailsRequest[] getBaggageDetailRQ(
			TripInfo ssrTripInfo) {
		AA_OTA_AirBaggageDetailsRQBaggageDetailsRequestsBaggageDetailsRequest[] request =
				new AA_OTA_AirBaggageDetailsRQBaggageDetailsRequestsBaggageDetailsRequest[ssrTripInfo.getSegmentInfos()
						.size()];
		AtomicInteger segmentIndex = new AtomicInteger(0);
		ssrTripInfo.getSegmentInfos().forEach(segmentInfo -> {
			request[segmentIndex.intValue()] =
					new AA_OTA_AirBaggageDetailsRQBaggageDetailsRequestsBaggageDetailsRequest();
			request[segmentIndex.intValue()].setFlightSegmentInfo(getFlightSegmentType(segmentInfo));
			segmentIndex.getAndIncrement();
		});
		return request;
	}


	private void setBaggageInfoToTrip(String baggageResponse, TripInfo ssrTripInfo) {
		try {
			Document document = XMLUtils.createDocumentFromString(baggageResponse);
			NodeList baggageDetailsResponses = XMLUtils.getNodeList(document,
					"//Envelope//Body//AA_OTA_AirBaggageDetailsRS//BaggageDetailsResponses");
			SegmentInfo segmentInfo = null;
			if (baggageDetailsResponses != null && baggageDetailsResponses.getLength() > 0) {
				for (int i = 0; i < baggageDetailsResponses.getLength(); ++i) {
					segmentInfo = ssrTripInfo.getSegmentInfos().get(i);
					List<BaggageSSRInformation> baggageSSRs = new ArrayList<>();
					Node baggageDetailsResponse = baggageDetailsResponses.item(i);
					NodeList onDBaggageDetailsResponses = baggageDetailsResponse.getChildNodes();
					if (onDBaggageDetailsResponses != null && onDBaggageDetailsResponses.getLength() > 0) {
						Node onDBaggageDetailsResponse = onDBaggageDetailsResponses.item(i);
						NodeList onDBaggageDetailsResponseChildren = onDBaggageDetailsResponse.getChildNodes();
						for (int index = 0; index < onDBaggageDetailsResponseChildren.getLength(); ++index) {
							Node onDBaggageDetailChild = onDBaggageDetailsResponseChildren.item(index);
							if (onDBaggageDetailChild.getNodeName().equals("ns1:Baggage")) {
								NodeList baggageDetailsList = onDBaggageDetailChild.getChildNodes();
								String baggageCode = baggageDetailsList.item(0).getFirstChild().getNodeValue();
								String baggageDescription = baggageDetailsList.item(1).getFirstChild().getNodeValue();
								String baggageCharge = baggageDetailsList.item(2).getFirstChild().getNodeValue();
								String currencyCode = baggageDetailsList.item(3).getFirstChild().getNodeValue();
								baggageCharge = String.valueOf(
										getAmountBasedOnCurrency(Double.parseDouble(baggageCharge), currencyCode));
								if (StringUtils.isNotEmpty(baggageCode) && !baggageCode.toLowerCase()
										.equalsIgnoreCase(AirArabiaXMLConstantsInfo.NO_BAG)) {
									BaggageSSRInformation ssrInformation = new BaggageSSRInformation();
									ssrInformation.setCode(baggageCode);
									Double charge = Double.parseDouble(baggageCharge);
									charge = charge + getIfGSTChargeApplies(charge, ssrTripInfo);
									ssrInformation.setAmount(charge);
									ssrInformation.setDesc(baggageDescription);
									baggageSSRs.add(ssrInformation);
								}
							}
						}
					}
					Map<SSRType, List<? extends SSRInformation>> baggageSSR = new HashMap<>();
					baggageSSR.put(SSRType.BAGGAGE, baggageSSRs);
					segmentInfo.setSsrInfo(baggageSSR);
				}
			}
		} catch (Exception e) {
			log.error("Unable to set baggage info for booking id {} trip {} ", bookingId, ssrTripInfo.toString(), e);
		}
	}

	// Manually applying 5% GST for baggage.
	private Double getIfGSTChargeApplies(Double ssrCharge, TripInfo ssrTripInfo) {
		Double gstCharges = new Double(0);
		if (ssrCharge != null) {
			String clientCountry = ServiceCommunicatorHelper.getClientInfo().getCountry();
			if (clientCountry.equalsIgnoreCase(ssrTripInfo.getDepartureAirport().getCountry())) {
				gstCharges = (ssrCharge * 0.05);
			}
		}
		return gstCharges;
	}

	private void setMealInfoToTrip(String mealResponse, TripInfo ssrTrip) {
		int totalPax = AirUtils.getPaxCount(searchQuery, true);
		try {
			Document document = XMLUtils.createDocumentFromString(mealResponse);
			NodeList mealDetailsResponses =
					XMLUtils.getNodeList(document, "//Envelope//Body//AA_OTA_AirMealDetailsRS//MealDetailsResponse");
			SegmentInfo segmentInfo = null;
			for (int mealIndx = 0; mealIndx < mealDetailsResponses.getLength() && ssrTrip != null; mealIndx++) {
				segmentInfo = ssrTrip.getSegmentInfos().get(mealIndx);
				List<MealSSRInformation> mealSSRs = new ArrayList<>();
				Node mealNode = mealDetailsResponses.item(mealIndx);
				NodeList allMeals = mealNode.getChildNodes();
				for (int mealPer = 0; mealPer < allMeals.getLength(); mealPer++) {
					if (allMeals.item(mealPer).getNodeName().equals("ns1:Meal")) {
						NodeList mealDetailsList = allMeals.item(mealPer).getChildNodes();
						String mealCode = mealDetailsList.item(0).getFirstChild().getNodeValue();
						String mealDescription = mealDetailsList.item(1).getFirstChild().getNodeValue();
						String mealCharge = mealDetailsList.item(2).getFirstChild().getNodeValue();
						String currencyCode = configuration.getSupplierCredential().getCurrencyCode();
						double mealAmount = getAmountBasedOnCurrency(Double.parseDouble(mealCharge), currencyCode);
						int mealQuantity = Integer.parseInt(mealDetailsList.item(4).getFirstChild().getNodeValue());
						if (totalPax <= mealQuantity && StringUtils.isNotEmpty(mealCode)) {
							MealSSRInformation ssrInformation = new MealSSRInformation();
							ssrInformation.setCode(mealCode);
							mealAmount = mealAmount + getIfGSTChargeApplies(mealAmount, ssrTrip);
							ssrInformation.setAmount(mealAmount);
							ssrInformation.setDesc(mealDescription);
							mealSSRs.add(ssrInformation);
						}
					}
				}
				Map<SSRType, List<? extends SSRInformation>> ssr = segmentInfo.getSsrInfo();
				if (ssr == null) {
					ssr = new HashMap<>();
				}
				ssr.put(SSRType.MEAL, mealSSRs);
				segmentInfo.setSsrInfo(ssr);
			}
		} catch (Exception e) {
			log.error("Unable to parse meal response {} trip {}", bookingId, ssrTrip, e);
		}

	}


	private boolean checkIfAnyPriceError(String responseString) {
		boolean isError = false;
		Document document = XMLUtils.createDocumentFromString(responseString);
		Node errorNode = XMLUtils.getNode(document, "//Envelope//Body//OTA_AirPriceRS//Errors");
		if (errorNode != null && errorNode.getChildNodes() != null && errorNode.getChildNodes().item(0) != null
				&& errorNode.getChildNodes().item(0).getAttributes() != null) {
			isError = true;
			String errorMsg = errorNode.getChildNodes().item(0).getAttributes().item(1).getNodeValue();
			throw new NoSeatAvailableException(errorMsg);
		}
		return isError;
	}

}
