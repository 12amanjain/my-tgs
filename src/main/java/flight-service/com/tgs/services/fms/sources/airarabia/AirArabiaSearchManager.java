package com.tgs.services.fms.sources.airarabia;

import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.Amenities;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.common.XMLUtils;
import com.tgs.utils.exception.air.AirSearchException;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.axis.types.Language;
import org.apache.axis.types.NonNegativeInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.opentravel.www.OTA._2003._05.OTA_AirAvailRQ;
import org.opentravel.www.OTA._2003._05.OTA_AirAvailRQOriginDestinationInformation;
import org.opentravel.www.OTA._2003._05.OTA_AirAvailRS;
import org.opentravel.www.OTA._2003._05.OTA_PingRQ;
import org.opentravel.www.OTA._2003._05.OTA_PingRS;
import org.opentravel.www.OTA._2003._05.OriginDestinationInformationTypeDestinationLocation;
import org.opentravel.www.OTA._2003._05.OriginDestinationInformationTypeOriginLocation;
import org.opentravel.www.OTA._2003._05.Target;
import org.opentravel.www.OTA._2003._05.TimeInstantType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.opentravel.www.OTA._2003._05.CabinType;
import org.opentravel.www.OTA._2003._05.AirSearchPrefsType;
import org.opentravel.www.OTA._2003._05.AirSearchPrefsTypeCabinPref;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

@Getter
@Setter
@SuperBuilder
@Slf4j
final class AirArabiaSearchManager extends AirArabiaServiceManager {


	protected static String NO_FLIGHT_FOUND = "no matching flight found";

	protected static String NO_AVAILABILITY = "no availability";

	protected static String NO_SEARCH_AVAILABLE = "no flight available on search date";

	protected static String INVALID_SECTORS = "invalid from and/or to airport";

	public boolean ping() throws Exception {
		boolean isError = false;
		try {
			OTA_PingRS ping = stub.ping(getPingRQ());
			isError = isAnyError(ping.getErrors());
		} catch (Exception e) {
			log.error("AirArabia Service is not working {} cause ", searchQuery.toString(), e);
		} finally {
			captureRQRS(searchQuery.getSearchId());
		}
		return isError;
	}

	private OTA_PingRQ getPingRQ() {
		OTA_PingRQ pingRQ = new OTA_PingRQ();
		pingRQ.setTarget(Target.Test);
		pingRQ.setSequenceNmbr(new NonNegativeInteger(SEQUENCE));
		pingRQ.setTimeStamp(Calendar.getInstance());
		pingRQ.setPrimaryLangID(new Language(LOCALECODE));
		pingRQ.setEchoData(configuration.getSupplierCredential().getUserName());
		pingRQ.setEchoToken(StringUtils.EMPTY);
		return pingRQ;
	}


	public AirSearchResult getAvailability(boolean isStorePriceSearch) throws NoSearchResultException {
		OTA_AirAvailRS availability = null;
		searchResult = new AirSearchResult();
		try {
			availability = stub.getAvailability(getAirAvailibilityRQ());
			String availabilityRs =
					getResponseByRequest(availability.getEchoToken(), "getAvailability", "getAvailability");
			checkIfFlightAvailable(availabilityRs);
			Document document = XMLUtils.createDocumentFromString(availabilityRs);
			if (document != null) {
				Node txnNode = XMLUtils.getNode(document, "//Envelope//Body//OTA_AirAvailRS");
				if (txnNode != null && txnNode.getAttributes() != null) {
					String txnIdentifier = txnNode.getAttributes().getNamedItem("TransactionIdentifier").getNodeValue();
					setTransactionIdentifier(txnIdentifier);
					List<TripInfo> tripInfos = parseGetAvailability(availabilityRs);
					if (CollectionUtils.isNotEmpty(tripInfos)) {

						if (tripInfos.size() > 10) {
							log.info("Removing Trips for {} trips size {} sq {}", configuration.getSourceId(),
									tripInfos.size(), searchQuery);
							tripInfos.removeAll(tripInfos.subList(10, tripInfos.size()));
						}

						tripInfos.forEach(tripInfo -> {
							updateSegmentMisc(tripInfo);
						});

						tripInfos = priceQuoteAvailabilities(tripInfos, isStorePriceSearch);

						if (searchQuery.isIntlReturn()) {
							updateReturnSegmentFlag(tripInfos, searchQuery.getRouteInfos().get(1));
							searchResult.getTripInfos().put(TripInfoType.COMBO.getTripType(), tripInfos);
						} else if (searchQuery.isOneWay()) {
							searchResult.getTripInfos().put(TripInfoType.ONWARD.getTripType(), tripInfos);
						} else if (searchQuery.isDomesticReturn()) {
							searchResult.getTripInfos().put(TripInfoType.RETURN.getTripType(), tripInfos);
						}
					}
				}
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} catch (Exception e) {
			throw e;
		}
		return searchResult;
	}

	private void checkIfFlightAvailable(String availabilityRs) {
		if (StringUtils.isNotBlank(availabilityRs)) {
			Document document = XMLUtils.createDocumentFromString(availabilityRs);
			Node errorNode = XMLUtils.getNode(document, "//Envelope//Body//OTA_AirAvailRS//Errors");
			if (errorNode != null && errorNode.getChildNodes() != null && errorNode.getChildNodes().item(0) != null
					&& errorNode.getChildNodes().item(0).getAttributes() != null) {
				NodeList errorList = errorNode.getChildNodes();
				String errorText =
						errorList.item(0).getAttributes().getNamedItem("ShortText").getNodeValue().toLowerCase();
				if (StringUtils.isNotBlank(errorText)) {
					if (errorText.contains(INVALID_SECTORS)) {
						throw new NotOperatingSectorException(errorText);
					} else {
						throw new NoSearchResultException(errorText);
					}
				}
			}
		}
	}

	private void updateReturnSegmentFlag(List<TripInfo> tripInfos, RouteInfo routeInfo) {
		tripInfos.forEach(tripInfo -> {
			AtomicInteger segmentNum = new AtomicInteger(0);
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				String fromCityAirportCode = getNearByAirport(routeInfo.getFromCityOrAirport().getCode(), bookingUser);
				if (segmentInfo.getDepartAirportInfo().getCode().equals(fromCityAirportCode)) {
					segmentInfo.setIsReturnSegment(true);
					segmentNum.set(0);
				}
				segmentInfo.setSegmentNum(segmentNum.intValue());
				segmentNum.getAndIncrement();
			});
		});
	}

	private List<TripInfo> priceQuoteAvailabilities(List<TripInfo> tripInfos, boolean isStorePriceSearch) {
		List<TripInfo> newTripInfos = new ArrayList<>();
		tripInfos.forEach(tripInfo -> {
			try {
				AirArabiaReviewManager reviewManager = AirArabiaReviewManager.builder().configuration(configuration)
						.searchQuery(searchQuery).stub(stub).listener(listener).moneyExchnageComm(moneyExchnageComm)
						.bookingUser(bookingUser).build();
				reviewManager.setTransactionIdentifier(this.transactionIdentifier);
				reviewManager.setJSessionId(jSessionId);
				reviewManager.setStorePriceSearch(isStorePriceSearch);
				tripInfo = reviewManager.doPriceQuote(tripInfo);
				newTripInfos.add(tripInfo);
			} catch (NoSeatAvailableException nos) {
				log.info("Unable to get pricing for Search Id {} TripInfo {}", searchQuery.getSearchId(), tripInfo,
						nos);
			} catch (Exception e) {
				log.error("Price Quote failed for trip {} cause {}", tripInfo.toString(), e);
			}
		});
		return newTripInfos;
	}

	private List<TripInfo> parseGetAvailability(String availabilityRs) {
		List<TripInfo> tripInfos = new ArrayList<>();
		try {
			Document document = XMLUtils.createDocumentFromString(availabilityRs);
			NodeList originDestinationInformation =
					XMLUtils.getNodeList(document, "//Envelope//Body//OTA_AirAvailRS//OriginDestinationInformation");
			if (originDestinationInformation != null && originDestinationInformation.getLength() > 0) {
				for (int tripIndex = 0; tripIndex < originDestinationInformation.getLength(); ++tripIndex) {
					Node originDestinationInformationNode = originDestinationInformation.item(tripIndex);
					NodeList journeyDetails = originDestinationInformationNode.getChildNodes();
					NodeList originDestinationOptionList = journeyDetails.item(4).getChildNodes();
					List<SegmentInfo> segmentInfos = new ArrayList<>();
					try {
						for (int segmentIndex = 0; segmentIndex < originDestinationOptionList
								.getLength(); ++segmentIndex) {
							NodeList originDestinationOptionChildren =
									originDestinationOptionList.item(segmentIndex).getChildNodes();
							Node flightSegment = originDestinationOptionChildren.item(0);
							SegmentInfo segmentInfo = getSegmentInfo(segmentIndex, flightSegment);
							segmentInfos.add(segmentInfo);
						}
						TripInfo tripInfo = new TripInfo();
						tripInfo.setSegmentInfos(segmentInfos);
						tripInfos.add(tripInfo);
					} catch (Exception e) {
						log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
					}
				}
			}
		} catch (Exception e) {
			throw new AirSearchException(AirSourceConstants.NO_SEARCH_RESULT);
		}
		return tripInfos;
	}

	private SegmentInfo getSegmentInfo(Integer segmentIndex, Node flightSegment) {
		NamedNodeMap segmentAttributes = flightSegment.getAttributes();
		NodeList flightSegmentChildren = flightSegment.getChildNodes();
		Node departureAirportNode = flightSegmentChildren.item(0);
		Node arrivalAirportNode = flightSegmentChildren.item(1);
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setSegmentNum(segmentIndex);
		String airportCode = departureAirportNode.getAttributes().getNamedItem("LocationCode").getNodeValue();
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(airportCode));
		airportCode = arrivalAirportNode.getAttributes().getNamedItem("LocationCode").getNodeValue();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(airportCode));
		segmentInfo
				.setDepartTime(getLocalTimeFromUTC(segmentAttributes.getNamedItem("DepartureDateTime").getNodeValue()));
		segmentInfo
				.setArrivalTime(getLocalTimeFromUTC(segmentAttributes.getNamedItem("ArrivalDateTime").getNodeValue()));
		Node amentiesNode = segmentAttributes.getNamedItem("SmokingAllowed");
		if (amentiesNode != null) {
			Amenities amenities = new Amenities();
			amenities.setIsSmokingAllowed(Boolean.getBoolean(amentiesNode.getNodeValue()));
			segmentInfo.setAmenities(amenities);
		}
		FlightDesignator designator = FlightDesignator.builder().build();
		designator.setAirlineInfo(AirlineHelper.getAirlineInfo(airlineCode));
		String flightNumber = segmentAttributes.getNamedItem("FlightNumber").getNodeValue().trim();
		designator.setFlightNumber(getFlightNumberWithCode(flightNumber));
		segmentInfo.setFlightDesignator(designator);
		String journeyKey = segmentAttributes.getNamedItem("RPH").getNodeValue();
		List<PriceInfo> priceInfoList = new ArrayList<>();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		priceInfo.getMiscInfo().setJourneyKey(journeyKey);
		priceInfoList.add(priceInfo);
		segmentInfo.setPriceInfoList(priceInfoList);
		boolean isReturnSegment = false;
		Node returnFlagNode = segmentAttributes.getNamedItem("returnFlag");
		if (returnFlagNode != null) {
			isReturnSegment = Boolean.getBoolean(returnFlagNode.getNodeValue());
		}
		segmentInfo.setIsReturnSegment(isReturnSegment);
		segmentInfo.setStops(0);
		// segmentInfo.setOperatedByAirlineInfo(AirlineHelper.getAirlineInfo(airlineCode));
		segmentInfo.setDuration(segmentInfo.calculateDuration());
		return segmentInfo;
	}

	private OTA_AirAvailRQ getAirAvailibilityRQ() {
		OTA_AirAvailRQ airAvailRQ = new OTA_AirAvailRQ();
		airAvailRQ.setPOS(getPointOfSale());
		airAvailRQ.setTarget(Target.Test);
		airAvailRQ.setPrimaryLangID(new Language(LOCALECODE));
		airAvailRQ.setTimeStamp(Calendar.getInstance());
		airAvailRQ.setEchoToken(echoToken);
		airAvailRQ.setTravelPreferences(getAirSearchPreference());
		airAvailRQ.setVersion(version);
		airAvailRQ.setRetransmissionIndicator(Boolean.TRUE);
		airAvailRQ.setSequenceNmbr(new org.apache.axis.types.NonNegativeInteger(SEQUENCE));
		airAvailRQ.setOriginDestinationInformation(getOriginDestionation());
		airAvailRQ.setTravelerInfoSummary(getTravellerInfoSummary());
		return airAvailRQ;
	}

	private AirSearchPrefsType getAirSearchPreference() {
		AirSearchPrefsType searchPrefsType = new AirSearchPrefsType();
		AirSearchPrefsTypeCabinPref[] cabinPref = new AirSearchPrefsTypeCabinPref[1];
		cabinPref[0] = new AirSearchPrefsTypeCabinPref();
		cabinPref[0].setCabin(getCabinPreference());
		searchPrefsType.setCabinPref(cabinPref);
		return searchPrefsType;
	}

	/**
	 * By Default ECONOMY of "Y"
	 */
	private CabinType getCabinPreference() {
		CabinType cabinPref = CabinType.Y;
		if (searchQuery.getCabinClass().equals(CabinClass.BUSINESS)) {
			cabinPref = CabinType.C;
		} else if (searchQuery.getCabinClass().equals(CabinClass.FIRST)) {
			cabinPref = CabinType.F;
		}
		return cabinPref;
	}

	private OTA_AirAvailRQOriginDestinationInformation[] getOriginDestionation() {
		OTA_AirAvailRQOriginDestinationInformation[] originDestinationInformation =
				new OTA_AirAvailRQOriginDestinationInformation[searchQuery.getRouteInfos().size()];
		originDestinationInformation[0] = getOriginDestination(0);
		if (searchQuery.isReturn()) {
			originDestinationInformation[1] = getOriginDestination(1);
		}
		return originDestinationInformation;
	}

	private OTA_AirAvailRQOriginDestinationInformation getOriginDestination(int index) {
		OTA_AirAvailRQOriginDestinationInformation originDestinationInformation =
				new OTA_AirAvailRQOriginDestinationInformation();
		OriginDestinationInformationTypeOriginLocation location = new OriginDestinationInformationTypeOriginLocation();
		OriginDestinationInformationTypeDestinationLocation destination =
				new OriginDestinationInformationTypeDestinationLocation();
		location.setLocationCode(getNearByAirport(
				searchQuery.getRouteInfos().get(index).getNearByFromCityOrAirport().getCode(), bookingUser));
		destination.setLocationCode(getNearByAirport(
				searchQuery.getRouteInfos().get(index).getNearByToCityOrAirport().getCode(), bookingUser));
		originDestinationInformation.setOriginLocation(location);
		originDestinationInformation.setDestinationLocation(destination);
		TimeInstantType originDate = new TimeInstantType();
		originDate.setDateTimeValue(TgsDateUtils.getCalendar(searchQuery.getRouteInfos().get(index).getTravelDate()));
		originDestinationInformation.setDepartureDateTime(originDate);
		return originDestinationInformation;
	}

	public static LocalDateTime getLocalTimeFromUTC(String tempTime) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Calendar retCal = Calendar.getInstance();
		try {
			retCal.setTime(dateFormat.parse(tempTime));
		} catch (ParseException e) {
		}
		return TgsDateUtils.calenderToLocalDateTime(retCal);
	}

}
