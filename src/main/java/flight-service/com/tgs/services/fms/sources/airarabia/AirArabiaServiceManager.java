package com.tgs.services.fms.sources.airarabia;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.URL;
import java.net.URLConnection;
import java.text.DateFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPElement;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.XMLUtils;
import lombok.Builder;
import org.apache.axis.Message;
import org.apache.axis.message.SOAPHeaderElement;
import org.apache.axis.transport.http.HTTPConstants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.opentravel.www.OTA._2003._05.AAResWebServicesHttpBindingStub;
import org.opentravel.www.OTA._2003._05.AirItineraryType;
import org.opentravel.www.OTA._2003._05.AirItineraryTypeOriginDestinationOptions;
import org.opentravel.www.OTA._2003._05.AirTripType;
import org.opentravel.www.OTA._2003._05.BookFlightSegmentType;
import org.opentravel.www.OTA._2003._05.Code;
import org.opentravel.www.OTA._2003._05.ErrorType;
import org.opentravel.www.OTA._2003._05.FlightNumberType;
import org.opentravel.www.OTA._2003._05.FlightSegmentBaseTypeArrivalAirport;
import org.opentravel.www.OTA._2003._05.FlightSegmentBaseTypeDepartureAirport;
import org.opentravel.www.OTA._2003._05.PassengerTypeQuantityType;
import org.opentravel.www.OTA._2003._05.SourceType;
import org.opentravel.www.OTA._2003._05.SourceTypeBookingChannel;
import org.opentravel.www.OTA._2003._05.SourceTypeRequestorID;
import org.opentravel.www.OTA._2003._05.TravelerInfoSummaryType;
import org.opentravel.www.OTA._2003._05.TravelerInformationType;
import org.w3c.dom.Document;
import org.opentravel.www.OTA._2003._05.FlightSegmentType;
import org.opentravel.www.OTA._2003._05.OperatingAirlineType;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.AxisRequestResponseListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.utils.AirUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Setter
@Getter
@Slf4j
@SuperBuilder
abstract class AirArabiaServiceManager {

	protected SupplierConfiguration configuration;

	protected AAResWebServicesHttpBindingStub stub;

	protected String sessionSignature;

	protected String bookingId;

	protected String jSessionId;

	protected String transactionIdentifier;

	protected AirSearchQuery searchQuery;

	protected Client client;

	protected AirSearchResult searchResult;

	protected ClientGeneralInfo clientInfo;

	protected DeliveryInfo deliveryInfo;

	protected static final String airlineCode = "G9";
	protected final String echoToken = "12662148060105253838426";
	protected static final String SEQUENCE = "1";
	protected static final String TERMINALID = "TestUser/Test Runner";
	protected static final String TERMINALX = "TerminalX";
	protected static final BigDecimal version = new BigDecimal(20061);
	protected static final String LOCALECODE = "en-us";

	protected static final String HEADER_ACCEPT = "application/xml, application/dime, multipart/related, text/*";

	protected List<FlightTravellerInfo> travellerInfos;

	protected static SimpleDateFormat xmlSDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

	protected AxisRequestResponseListener listener;

	protected AirArabiaBindingService bindingService;

	protected static String SYSTEM_GEN = ": SYSTEM GENERATED";

	protected List<String> criticalMessageLogger;

	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected Calendar timeLimit;

	protected User bookingUser;

	@Builder.Default
	protected boolean isStorePriceSearch = true;

	public AAResWebServicesHttpBindingStub getSessionBinding() {
		AAResWebServicesHttpBindingStub binding = null;
		try {
			binding = bindingService.getAAResWebServiceStub(configuration);
			if (ArrayUtils.isEmpty(binding.getHeaders())) {
				setHeader(binding);
			}
		} catch (Exception e) {
			log.error("AirArabia Create Session Header Failed {}", e);
		}
		binding.setTimeout(120000);
		binding.setMaintainSession(true);
		stub = binding;
		return stub;
	}

	public void setHeader(AAResWebServicesHttpBindingStub binding) throws Exception {
		// Create the top-level WS-Security SOAP header XML name.
		QName headerName = new QName(
				"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd", "Security");
		SOAPHeaderElement header = new SOAPHeaderElement(headerName);
		// no intermediate actors are involved.
		header.setActor(null);
		// not important, "wsse" is standard
		header.setPrefix("wsse");
		header.setMustUnderstand(true);
		// Add the UsernameToken element to the WS-Security header
		SOAPElement utElem = header.addChildElement("UsernameToken");
		utElem.setAttribute("wsu:Id", "UsernameToken-17855236");
		utElem.setAttribute("xmlns:wsu",
				"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd");
		SOAPElement userNameElem = utElem.addChildElement("Username");
		userNameElem.setValue(configuration.getSupplierCredential().getUserName());
		SOAPElement passwordElem = utElem.addChildElement("Password");
		passwordElem.setAttribute("Type",
				"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText");
		passwordElem.setValue(configuration.getSupplierCredential().getPassword());
		// Finally, attach the header to the binding.
		binding.setHeader(header);
	}

	public boolean isAnyError(ErrorType[] errorTypes) {
		AtomicBoolean isError = new AtomicBoolean(false);
		if (ArrayUtils.isNotEmpty(errorTypes)) {
			Arrays.stream(errorTypes).forEach(errorType -> {
				String message = StringUtils.join(errorType.getType(), errorType.getShortText(), errorType.getCode());
				isError.set(Boolean.TRUE);
				log.error("AirArabia Search Failed {} SQ {} ", message, searchQuery);
			});
		}
		return isError.get();
	}


	public void captureRQRS(String referenceId) {
		try {
			String type = stub._getCall().getOperationName().toString();
			Message rqMessage = getStub()._getCall().getMessageContext().getRequestMessage();
			Message rsMessage = getStub()._getCall().getMessageContext().getResponseMessage();
			AxisRequestResponseListener listner =
					new AxisRequestResponseListener(referenceId, type, configuration.getBasicInfo().getSupplierName());
			if (StringUtils.isNotBlank(bookingId)) {
				type = StringUtils.join(type, "B");
			}
			listner.extractMessage(rqMessage, StringUtils.join(type, " Request"));
			listner.extractMessage(rsMessage, StringUtils.join(type, " Response"));
		} catch (Exception e) {
			log.error("Error Occured while capturing logs on bookingid {} cause  {}", referenceId, e);
		}
	}

	public SourceType[] getPointOfSale() {
		SourceType[] sourceTypes = new SourceType[1];
		SourceTypeRequestorID sourceTypeRequestorID = new SourceTypeRequestorID();
		sourceTypeRequestorID.setID(configuration.getSupplierCredential().getUserName());
		sourceTypeRequestorID.setType("4");
		SourceTypeBookingChannel bookingChannel = new SourceTypeBookingChannel();
		bookingChannel.setType("12");
		sourceTypes[0] = new SourceType();
		sourceTypes[0].setRequestorID(sourceTypeRequestorID);
		sourceTypes[0].setBookingChannel(bookingChannel);
		sourceTypes[0].setTerminalID(TERMINALID);
		return sourceTypes;
	}

	public AirTripType getTripType() {
		AirTripType tripType = AirTripType.value1;
		if (searchQuery != null) {
			if (searchQuery.isIntl() && searchQuery.isOneWay()) {
				tripType = AirTripType.value1;
			} else if (searchQuery.isIntlReturn()) {
				tripType = AirTripType.value3;
			}
		}
		return tripType;
	}

	public org.opentravel.www.OTA._2003._05.TravelerInfoSummaryType getTravellerInfoSummary() {
		TravelerInfoSummaryType summary = new TravelerInfoSummaryType();
		summary.setAirTravelerAvail(getAirAvailPaxType());
		return summary;
	}

	private TravelerInformationType[] getAirAvailPaxType() {
		TravelerInformationType[] travelerInformationTypes = new TravelerInformationType[1];
		travelerInformationTypes[0] = new TravelerInformationType();
		travelerInformationTypes[0].setPassengerTypeQuantity(getPaxInfo());
		return travelerInformationTypes;
	}

	public PassengerTypeQuantityType[] getPaxInfo() {
		PassengerTypeQuantityType[] passengerTypeQuantityTypes = new PassengerTypeQuantityType[3];
		AtomicInteger paxIndex = new AtomicInteger(0);
		if (searchQuery != null) {
			searchQuery.getPaxInfo().forEach((paxType, count) -> {
				Code code = new Code();
				String paxCode = getPaxType(paxType.getType());
				code.setOTA_CodeTypeValue(paxCode);
				BigInteger paxCount = BigInteger.valueOf(AirUtils.getParticularPaxCount(searchQuery, paxType));
				passengerTypeQuantityTypes[paxIndex.intValue()] = new PassengerTypeQuantityType();
				passengerTypeQuantityTypes[paxIndex.intValue()].setCode(code);
				passengerTypeQuantityTypes[paxIndex.intValue()].setQuantity(paxCount);
				paxIndex.getAndIncrement();
			});
		} else if (CollectionUtils.isNotEmpty(travellerInfos)) {
			for (PaxType paxType : PaxType.values()) {
				Integer paxCount = AirUtils.getParticularPaxTravellerInfo(travellerInfos, paxType).size();
				Code code = new Code();
				String paxCode = getPaxType(paxType.getType());
				code.setOTA_CodeTypeValue(paxCode);
				BigInteger pCount = new BigInteger(String.valueOf(paxCount));
				passengerTypeQuantityTypes[paxIndex.intValue()] = new PassengerTypeQuantityType();
				passengerTypeQuantityTypes[paxIndex.intValue()].setCode(code);
				passengerTypeQuantityTypes[paxIndex.intValue()].setQuantity(pCount);
				paxIndex.getAndIncrement();
			}

		}
		return passengerTypeQuantityTypes;
	}

	private String getPaxType(String type) {
		String code = type;
		if (type.equals(PaxType.CHILD.getType())) {
			code = "CHD";
		}
		return code;
	}

	public AirItineraryType getAirItinerary(TripInfo tripInfo) {
		AirItineraryType itineraryType = new AirItineraryType();
		itineraryType.setDirectionInd(getTripType());
		itineraryType.setOriginDestinationOptions(getOriginDestinationInfo(tripInfo));
		return itineraryType;
	}

	public AirItineraryTypeOriginDestinationOptions getOriginDestinationInfo(TripInfo tripInfo) {
		AirItineraryTypeOriginDestinationOptions originDestInfo = new AirItineraryTypeOriginDestinationOptions();
		BookFlightSegmentType[][] bookFlightSegmentType =
				new BookFlightSegmentType[tripInfo.getSegmentInfos().size()][1];
		AtomicInteger segmentSize = new AtomicInteger(0);
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			bookFlightSegmentType[segmentSize.intValue()] = new BookFlightSegmentType[1];
			bookFlightSegmentType[segmentSize.intValue()][0] = new BookFlightSegmentType();
			FlightSegmentBaseTypeDepartureAirport departureAirport = new FlightSegmentBaseTypeDepartureAirport();
			departureAirport.setLocationCode(segmentInfo.getDepartAirportInfo().getCode());
			departureAirport.setTerminal(TERMINALX);
			bookFlightSegmentType[segmentSize.intValue()][0].setDepartureAirport(departureAirport);
			FlightSegmentBaseTypeArrivalAirport arrivalAirport = new FlightSegmentBaseTypeArrivalAirport();
			arrivalAirport.setLocationCode(segmentInfo.getArrivalAirportInfo().getCode());
			arrivalAirport.setTerminal(TERMINALX);
			bookFlightSegmentType[segmentSize.intValue()][0].setArrivalAirport(arrivalAirport);
			bookFlightSegmentType[segmentSize.intValue()][0]
					.setDepartureDateTime(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
			bookFlightSegmentType[segmentSize.intValue()][0]
					.setArrivalDateTime(TgsDateUtils.getCalendar(segmentInfo.getArrivalTime()));
			FlightNumberType flightNumber = new FlightNumberType();
			flightNumber.setFlightNumber(getFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber()));
			bookFlightSegmentType[segmentSize.intValue()][0].setFlightNumber(flightNumber);
			bookFlightSegmentType[segmentSize.intValue()][0]
					.setRPH(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
			segmentSize.getAndIncrement();
		});
		originDestInfo.setOriginDestinationOption(bookFlightSegmentType);
		return originDestInfo;
	}

	public String getResponseByRequest(String requestXML, String methodName, String soapAction) {
		String sbStr = null;
		int numTry = 0;
		boolean doRetry = false;
		if (StringUtils.isNotEmpty(requestXML)) {
			do {
				try {
					URL url = new URL(configuration.getSupplierCredential().getUrl());
					URLConnection urlCon = null;
					urlCon = url.openConnection();
					urlCon.setDoInput(true);
					urlCon.setDoOutput(true);
					urlCon.setUseCaches(false);
					urlCon.setRequestProperty(HTTPConstants.HEADER_CONTENT_TYPE, "text/xml; charset=utf-8");
					urlCon.setRequestProperty(HTTPConstants.HEADER_ACCEPT, HEADER_ACCEPT);
					urlCon.setRequestProperty(HTTPConstants.HEADER_ACCEPT_ENCODING, "gzip, deflate");
					urlCon.setRequestProperty(HTTPConstants.HEADER_AUTHORIZATION, "Basic VklBOnZpYXZpYQ==");
					// NOT in GZIP Format
					// urlCon.setRequestProperty(HTTPConstants.HEADER_ACCEPT_ENCODING, "deflate, gzip");
					// urlCon.setRequestProperty(HTTPConstants.HEADER_CONTENT_ENCODING, "gzip");
					if (StringUtils.isNotBlank(soapAction))
						urlCon.setRequestProperty(HTTPConstants.HEADER_SOAP_ACTION, soapAction);
					if (StringUtils.isNotBlank(jSessionId)) {
						urlCon.setRequestProperty(HTTPConstants.HEADER_COOKIE, jSessionId);
						log.debug("Existing Session Used for booking {} for session {} ", bookingId, jSessionId);
					}

					DataOutputStream printout = new DataOutputStream(urlCon.getOutputStream());
					printout.writeBytes(requestXML);
					printout.flush();
					printout.close();

					InputStream rawInStream = urlCon.getInputStream();
					InputStream dataInput = null;
					log.debug("Request Method Name {} for booking {}", methodName, bookingId);
					// logRequestResponseHeader(urlCon);

					List<String> headerElements = getHeaderElements(urlCon, HTTPConstants.HEADER_SET_COOKIE);
					if (CollectionUtils.isNotEmpty(headerElements)) {
						for (String cookieElement : headerElements) {
							if (cookieElement.contains("JSESSIONID=")) {
								jSessionId = cookieElement;
								if (jSessionId != null && StringUtils.isNotBlank(jSessionId)) {
									jSessionId = jSessionId.split(";")[0];
									log.debug("Created Session for booking {} for session {} ", bookingId, jSessionId);
								}
								break;
							}
						}
					}

					String encoding = urlCon.getContentEncoding();
					if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
						dataInput = new GZIPInputStream(new BufferedInputStream(rawInStream));
					} else {
						dataInput = new BufferedInputStream(rawInStream);
					}
					StringBuffer sb = new StringBuffer();
					int rc;
					while ((rc = dataInput.read()) != -1) {
						sb.append((char) rc);
					}
					dataInput.close();
					sbStr = sb.toString();

				} catch (IOException e) {
					String key = StringUtils.isNotBlank(bookingId) ? bookingId
							: (searchQuery != null) ? searchQuery.getSearchId() : "";
					log.error("Unable to connect service {} for Id {} and msg {} ", methodName, key, e.getMessage());
					doRetry = false;
				} catch (Exception e) {
					log.error("Exception Occured while trying  methodName {}", methodName, e);
					doRetry = false;
				} finally {
					if (isStorePriceSearch) {
						captureRequestResponse(bookingId, requestXML, sbStr, methodName);
					}
				}
			} while (numTry++ < 3 && doRetry);
		}
		return sbStr;
	}

	private List<String> getHeaderElements(URLConnection urlCon, String messageHeader) {
		if (MapUtils.isNotEmpty(urlCon.getHeaderFields())) {
			for (String key : urlCon.getHeaderFields().keySet()) {
				if (messageHeader.equalsIgnoreCase(key)
						&& CollectionUtils.isNotEmpty(urlCon.getHeaderFields().get(key))) {
					return urlCon.getHeaderFields().get(key);
				}
			}
		}
		return null;
	}

	protected void captureRequestResponse(String bookingId, String requestXML, String response, String methodName) {
		String requestHead = " Request";
		String responseHead = " Response";
		if (StringUtils.isNotEmpty(bookingId)) {
			requestHead = StringUtils.join(requestHead, " -B");
			responseHead = StringUtils.join(responseHead, " -B");
		}
		listener.extractMessage(requestXML,
				AirUtils.getLogType(StringUtils.join(methodName, requestHead), configuration));
		listener.extractMessage(response,
				AirUtils.getLogType(StringUtils.join(methodName, responseHead), configuration));
	}


	/**
	 * @implSpec : TransactionId is unique identifier for entire trip Trace Id will be same for all segments
	 */
	public String getTransactionId(TripInfo tripInfo) {
		if (StringUtils.isBlank(transactionIdentifier)) {
			transactionIdentifier = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
			return transactionIdentifier;
		}
		return transactionIdentifier;
	}


	public String getPostRequestData() {
		return AirArabiaXMLConstantsInfo.POSTREQUESTDATA;
	}

	public StringBuffer getPreRequestData() {
		MessageFormat msgFormat = new MessageFormat(AirArabiaXMLConstantsInfo.PREREQUESTDATA);
		Object[] args = {"17855236", configuration.getSupplierCredential().getUserName(),
				configuration.getSupplierCredential().getPassword()};
		return new StringBuffer(msgFormat.format(args));
	}

	public String getXMLTimeStamp(Calendar utilCal) {
		return xmlSDF.format(utilCal.getTime());
	}


	public FlightSegmentType getFlightSegmentType(SegmentInfo segmentInfo) {
		FlightSegmentType flightSegmentInfo = new FlightSegmentType();
		flightSegmentInfo.setDepartureDateTime(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
		flightSegmentInfo.setArrivalDateTime(TgsDateUtils.getCalendar(segmentInfo.getArrivalTime()));
		FlightNumberType flightNumber = new FlightNumberType();
		flightNumber.setFlightNumber(getFlightNumber(segmentInfo.getFlightNumber()));
		flightSegmentInfo.setFlightNumber(flightNumber);
		flightSegmentInfo.setRPH(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
		FlightSegmentBaseTypeDepartureAirport departureAirport = new FlightSegmentBaseTypeDepartureAirport();
		departureAirport.setLocationCode(segmentInfo.getDepartureAirportCode());
		departureAirport.setTerminal(TERMINALX);
		flightSegmentInfo.setDepartureAirport(departureAirport);
		FlightSegmentBaseTypeArrivalAirport arrivalAirport = new FlightSegmentBaseTypeArrivalAirport();
		arrivalAirport.setLocationCode(segmentInfo.getArrivalAirportCode());
		arrivalAirport.setTerminal(TERMINALX);
		flightSegmentInfo.setArrivalAirport(arrivalAirport);

		OperatingAirlineType operatingAirline = new OperatingAirlineType(airlineCode);
		operatingAirline.setCode(airlineCode);
		flightSegmentInfo.setOperatingAirline(operatingAirline);
		return flightSegmentInfo;
	}

	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {
		// Currently we are not using currency but in future we might need to set amount
		// based on current , so keeping scope for future
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(AirSourceType.AIRARABIA.name().toUpperCase()).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	public void updateSegmentMisc(TripInfo tripInfo) {
		AirUtils.splitTripInfo(tripInfo, false).forEach(tripInfo1 -> {
			tripInfo1.getSegmentInfos().forEach(segmentInfo -> {
				segmentInfo.getPriceInfo(0).getMiscInfo().setTokenId(getJSessionId());
			});
		});
	}

	public String getFlightNumber(String flightNum) {
		if (flightNum.contains(airlineCode)) {
			return flightNum;
		}
		return StringUtils.join(airlineCode, flightNum);
	}

	public String getFlightNumberWithCode(String flightNum) {
		if (flightNum.contains(airlineCode)) {
			return flightNum.replaceAll(airlineCode, "");
		}
		return flightNum;
	}


	public void logRequestResponseHeader(URLConnection urlCon) {
		if (log.isDebugEnabled()) {
			urlCon.getHeaderFields().forEach((key, value) -> {
				List<String> header = value;
				header.forEach(v -> {
					// This is just to confirm all headers are fine if testing required
					log.debug(" {}", key + " : " + v);
				});
			});
		}
	}

	public Note getNote(NoteType noteType, String message) {
		Note note = Note.builder().bookingId(bookingId).build();
		if (noteType != null) {
			note.setNoteType(noteType);
		} else {
			note.setNoteType(NoteType.SUPPLIER_MESSAGE);
		}
		if (StringUtils.isNotBlank(message)) {
			note.setNoteMessage(StringUtils.join(message, SYSTEM_GEN));
		}
		return note;
	}

	public String getNearByAirport(String airportCode, User bookingUser) {
		String nearByCode =
				AirUtils.getNearByAirport(airportCode, AirSourceType.AIRARABIA.getSourceId(), searchQuery, bookingUser);
		if (StringUtils.isNotBlank(nearByCode)) {
			return nearByCode;
		}
		return airportCode;
	}

	public String getCurrencyCode() {
		if (configuration != null && configuration.getSupplierCredential() != null
				&& StringUtils.isNotBlank(configuration.getSupplierCredential().getCurrencyCode())) {
			return configuration.getSupplierCredential().getCurrencyCode();
		}
		clientInfo = ServiceCommunicatorHelper.getClientInfo();
		return clientInfo.getCurrencyCode();
	}

	protected static LocalDateTime getLocalTimeFromUTC(String tempTime) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
		Calendar retCal = Calendar.getInstance();
		try {
			retCal.setTime(dateFormat.parse(tempTime));
		} catch (ParseException e) {
		}
		return TgsDateUtils.calenderToLocalDateTime(retCal);
	}

	protected void updateTicketingTimeLimit(Document doc) {
		if (doc != null) {
			String holdTime = XMLUtils.getAttrributeValue(
					XMLUtils.getNode(doc, "Body/OTA_AirBookRS/AirReservation/Ticketing"), "TicketTimeLimit");
			if (StringUtils.isNotBlank(holdTime)) {
				Calendar airlineTimeLimitCal = Calendar.getInstance();
				try {
					DateFormat utcFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
					utcFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
					// utcFormat will convert zulu time to Normal Timezone Date
					Date airlineTimelimitDate = utcFormat.parse(holdTime);
					airlineTimeLimitCal.setTime(airlineTimelimitDate);
					timeLimit = airlineTimeLimitCal;
				} catch (Exception e) {
					log.info("Error Occured while parsing timelimit for -{} -{}", bookingId, e);
				}
			}
		}
	}

}
