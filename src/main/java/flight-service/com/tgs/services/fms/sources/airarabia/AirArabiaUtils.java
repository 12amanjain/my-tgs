package com.tgs.services.fms.sources.airarabia;

import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import org.apache.commons.lang3.StringUtils;

public class AirArabiaUtils {

	public static String getTitle(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			return travellerInfo.getTitle().toUpperCase();
		} else if ((travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())
				|| travellerInfo.getPaxType().getType().equals(PaxType.INFANT.getType())) && isMaster(travellerInfo)) {
			return "MSTR";
		} else if ((travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())
				|| travellerInfo.getPaxType().getType().equals(PaxType.INFANT.getType())) && isMiss(travellerInfo)) {
			return "MISS";
		}
		return travellerInfo.getTitle().toUpperCase();
	}


	private static boolean isMaster(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MASTER")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MSTR");
		}
		return false;
	}

	private static boolean isMiss(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MISS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	public static String getFirstName(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isNotBlank(travellerInfo.getFirstName())) {
			return travellerInfo.getFirstName();
		}
		return StringUtils.EMPTY;
	}

	public static String getLastName(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isNotBlank(travellerInfo.getLastName())) {
			return travellerInfo.getLastName();
		}
		return StringUtils.EMPTY;
	}


}
