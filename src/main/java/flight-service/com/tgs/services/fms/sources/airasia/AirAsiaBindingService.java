package com.tgs.services.fms.sources.airasia;


import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.CacheType;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.SessionServiceBasicHttpBinding_ISessionServiceStub;
import org.tempuri.LookupServiceBasicHttpBinding_ILookupServiceStub;
import org.tempuri.FareServiceBasicHttpBinding_IFareServiceStub;
import org.tempuri.BookingServiceBasicHttpBinding_IBookingServiceStub;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Slf4j
@Builder
public class AirAsiaBindingService {

	private GeneralCachingCommunicator cacheCommunicator;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	protected User user;

	public SessionServiceBasicHttpBinding_ISessionServiceStub getSessionStub(SupplierConfiguration configuration,
			AirSearchQuery query) {
		SessionServiceBasicHttpBinding_ISessionServiceStub sessionStub = null;
		try {
			SessionMetaInfo metaInfo = fetchFromQueue(
					getSessionMetaInfo(configuration, SessionServiceBasicHttpBinding_ISessionServiceStub.class, null));
			if (metaInfo != null) {
				sessionStub = (SessionServiceBasicHttpBinding_ISessionServiceStub) metaInfo.getValue();
			}
			if (sessionStub == null) {
				log.info("Creating new Session Stub for searchQuery {}", query);
				sessionStub = new SessionServiceBasicHttpBinding_ISessionServiceStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "SessionService.svc"));
			}
		} catch (Exception e) {
			log.error("Session Binding Initilaized failed for  cause", e);
		}
		setProxy(sessionStub);
		return sessionStub;
	}

	public FareServiceBasicHttpBinding_IFareServiceStub getFareServiceStub(SupplierConfiguration configuration,
			AirSearchQuery query) {
		FareServiceBasicHttpBinding_IFareServiceStub fareServiceStub = null;
		try {
			SessionMetaInfo metaInfo = fetchFromQueue(
					getSessionMetaInfo(configuration, FareServiceBasicHttpBinding_IFareServiceStub.class, null));
			if (metaInfo != null) {
				fareServiceStub = (FareServiceBasicHttpBinding_IFareServiceStub) metaInfo.getValue();
			}
			if (fareServiceStub == null) {
				log.info("Creating new Fare Service Stub for searchQuery {}", query);
				fareServiceStub = new FareServiceBasicHttpBinding_IFareServiceStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "FareService.svc"));
			}
		} catch (Exception e) {
			log.error("Fare Service Binding Initilaized failed for  cause", e);
		}
		setProxy(fareServiceStub);
		return fareServiceStub;
	}

	public LookupServiceBasicHttpBinding_ILookupServiceStub getLookUpStub(SupplierConfiguration configuration,
			AirSearchQuery query) {
		LookupServiceBasicHttpBinding_ILookupServiceStub lookupServiceStub = null;
		try {
			SessionMetaInfo metaInfo = fetchFromQueue(
					getSessionMetaInfo(configuration, LookupServiceBasicHttpBinding_ILookupServiceStub.class, null));
			if (metaInfo != null) {
				lookupServiceStub = (LookupServiceBasicHttpBinding_ILookupServiceStub) metaInfo.getValue();
			}
			if (lookupServiceStub == null) {
				log.info("Creating new LookUp Service Stub for searchQuery {}", query);
				lookupServiceStub = new LookupServiceBasicHttpBinding_ILookupServiceStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "LookupService.svc"));
			}
		} catch (Exception e) {
			log.error("LookUp Binding Initilaized failed for  cause", e);
		}
		setProxy(lookupServiceStub);
		return lookupServiceStub;
	}

	public BookingServiceBasicHttpBinding_IBookingServiceStub getBookingStub(SupplierConfiguration configuration,
			AirSearchQuery query) {
		BookingServiceBasicHttpBinding_IBookingServiceStub bookingServiceStub = null;
		try {
			SessionMetaInfo metaInfo = fetchFromQueue(
					getSessionMetaInfo(configuration, BookingServiceBasicHttpBinding_IBookingServiceStub.class, null));
			if (metaInfo != null) {
				bookingServiceStub = (BookingServiceBasicHttpBinding_IBookingServiceStub) metaInfo.getValue();
			}
			if (bookingServiceStub == null) {
				log.info("Creating new LookUp Service Stub for searchQuery {}", query);
				bookingServiceStub = new BookingServiceBasicHttpBinding_IBookingServiceStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "BookingService.svc"));
			}
		} catch (Exception e) {
			log.error("LookUp Binding Initilaized failed for  cause", e);
		}
		setProxy(bookingServiceStub);
		return bookingServiceStub;
	}


	public void setProxy(Stub stub) {
		String proxyAddress = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		String proxy = null;
		if (configuratorInfo != null) {
			proxy = configuratorInfo.getProxyAddress();
		}
		if (StringUtils.isNotBlank(proxy)) {
			int proxyPort = 3128;
			if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
				ServiceClient client = stub._getServiceClient();
				Options options = client.getOptions();
				options.setProperties(null);
				proxyAddress = proxy.split(":")[0];
				proxyPort = Integer.valueOf(proxy.split(":")[1]);
				if (StringUtils.isNotBlank(proxyAddress)) {
					HttpTransportProperties.ProxyProperties proxyProperties =
							new HttpTransportProperties.ProxyProperties();
					proxyProperties.setProxyName(proxyAddress); // 10.10.16.165
					proxyProperties.setProxyPort(proxyPort); // 3128
					options.setProperty(org.apache.axis2.transport.http.HTTPConstants.PROXY, proxyProperties);
					options.setProperty(HTTPConstants.CHUNKED, false);
				}
				client.setOptions(options);
			}
		}
		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions()
					.setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			setConnectionManager(stub);
		}
	}

	@SuppressWarnings("rawtypes")
	public static SessionMetaInfo getSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder()
				.key(String.join("-", configuration.getBasicInfo().getRuleId().toString(), classx.toString()))
				.value(value).expiryTime(LocalDateTime.now().plusMinutes(600)).cacheType(CacheType.INMEMORY).build();
		return metaInfo;
	}

	@SuppressWarnings("rawtypes")
	public void storeSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		if (value != null) {
			SessionMetaInfo metaInfo = getSessionMetaInfo(configuration, classx, value);
			cacheCommunicator.storeInQueue(metaInfo);
		}
	}

	public SessionMetaInfo fetchFromQueue(SessionMetaInfo metaInfo) {
		metaInfo.setIndex(0);
		SessionMetaInfo cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
		if (cacheInfo == null) {
			metaInfo.setIndex(-1);
			cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
		}
		return cacheInfo;
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(20);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}
}

