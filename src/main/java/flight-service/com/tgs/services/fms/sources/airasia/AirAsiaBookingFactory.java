package com.tgs.services.fms.sources.airasia;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.tempuri.Booking;
import org.tempuri.BookingServiceBasicHttpBinding_IBookingServiceStub;
import org.tempuri.SessionServiceBasicHttpBinding_ISessionServiceStub;
import java.rmi.RemoteException;
import java.util.Calendar;
import java.util.List;


@Slf4j
@Service
@NoArgsConstructor
public class AirAsiaBookingFactory extends AbstractAirBookingFactory {

	protected AirAsiaBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	protected SessionServiceBasicHttpBinding_ISessionServiceStub sessionStub;

	protected BookingServiceBasicHttpBinding_IBookingServiceStub bookingStub;


	public AirAsiaBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}


	public void initialize() {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(bookingUser).build();
		sessionStub = bindingService.getSessionStub(supplierConf, null);
		bookingStub = bindingService.getBookingStub(supplierConf, null);
	}

	@Override
	public boolean doBooking() {
		boolean isBookingSuccess = false;
		List<SegmentInfo> segmentInfos = null;
		AirAsiaSessionManager sessionManager = null;
		double amountToBePaidToAirline = 0;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			segmentInfos = bookingSegments.getSegmentInfos();
			sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionStub(sessionStub).bookingId(bookingId).bookingUser(bookingUser).build();
			if (supplierSession != null
					&& StringUtils.isNotBlank(supplierSession.getSupplierSessionInfo().getSessionToken())) {
				sessionManager.setSessionSignature(supplierSession.getSupplierSessionInfo().getSessionToken());
				AirAsiaBookingManager bookingManager = AirAsiaBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionManager.sessionSignature).deliveryInfo(deliveryInfo)
						.sourceConfiguration(sourceConfiguration).bookingId(bookingId).listener(listener)
						.bookingUser(bookingUser).bookingStub(bookingStub).criticalMessageLogger(criticalMessageLogger)
						.build();
				bookingManager.initialize(segmentInfos);
				bookingManager.setGmsCommunicator(gmsCommunicator);
				bookingManager.setMoneyExchnageComm(moneyExchangeComm);
				bookingManager.setSearchQuery(bookingSegments.getSearchQuery());
				bookingManager.initializeTravellerInfo(segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo());
				if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
					amountToBePaidToAirline = bookingManager.sendGSTAndContactInfo(gstInfo);
				}
				amountToBePaidToAirline = bookingManager.getBookingFromState();
				if (bookingManager.airAsiaAirline.isAdditionalSSRRequired(segmentInfos)
						|| AirUtils.isSSRAddedInTrip(segmentInfos)
						|| AirUtils.getParticularPaxCount(bookingSegments.getSearchQuery(), PaxType.INFANT) > 0) {
					bookingManager.sendSellSSRRequest(segmentInfos, true);
				}
				if (AirAsiaUtils.isSeatAddedInTrip(segmentInfos)) {
					amountToBePaidToAirline = bookingManager.sendSeatSellRequest(segmentInfos);
				}
				if (!isHoldBooking && amountToBePaidToAirline != 0) {
					amountToBePaidToAirline = bookingManager.addPaymentToBooking();
				}
				if (!isFareDiff(amountToBePaidToAirline)) {
					pnr = bookingManager.commitBooking(isHoldBooking, StringUtils.EMPTY, segmentInfos);
					isBookingSuccess = true;
					AirAsiaBookingRetrieveManager retrieveManager = AirAsiaBookingRetrieveManager.builder()
							.configuration(supplierConf).sessionStub(sessionManager.sessionStub)
							.sessionSignature(sessionManager.sessionSignature).bookingUser(bookingUser)
							.criticalMessageLogger(criticalMessageLogger).bookingStub(bookingStub).listener(listener)
							.build();
					retrieveManager.init();
					Booking booking = retrieveManager.getBookingResponse(pnr);
					if (isHoldBooking) {
						Calendar holdTime = Calendar.getInstance();
						holdTime.setTimeInMillis(booking.getBookingHold().getHoldDateTime().getTimeInMillis());
						updateTimeLimit(holdTime);
					}
				}
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			sessionManager.closeSession();
			storeSessionInfo();
		}
		return isBookingSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isBookingSuccess = false;
		AirAsiaSessionManager sessionManager = null;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionStub(sessionStub).bookingId(bookingId).bookingUser(bookingUser).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
				AirAsiaBookingRetrieveManager retrieveManager = AirAsiaBookingRetrieveManager.builder()
						.configuration(supplierConf).sessionStub(sessionManager.sessionStub)
						.sessionSignature(sessionManager.sessionSignature).criticalMessageLogger(criticalMessageLogger)
						.bookingStub(bookingStub).listener(listener).bookingUser(bookingUser).build();
				retrieveManager.init();
				Booking booking = retrieveManager.getBookingResponse(pnr);
				AirAsiaBookingManager bookingManager = AirAsiaBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionManager.sessionSignature).deliveryInfo(deliveryInfo)
						.bookingId(bookingId).bookingStub(bookingStub).bookingUser(bookingUser).listener(listener)
						.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger).build();
				bookingManager.init();
				bookingManager.setMoneyExchnageComm(moneyExchangeComm);
				bookingManager.setSearchQuery(bookingSegments.getSearchQuery());
				bookingManager.initializeTravellerInfo(segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo());
				bookingManager.totalFare = booking.getBookingSum().getTotalCost().doubleValue();
				double amountToBePaidToAirline = bookingManager.addPaymentToBooking();
				if (!isFareDiff(amountToBePaidToAirline)) {
					pnr = bookingManager.commitBooking(false, pnr, bookingSegments.getSegmentInfos());
					isBookingSuccess = true;
				}
			}
		} finally {
			sessionManager.closeSession();
			storeSessionInfo();

		}
		return isBookingSuccess;
	}

	private void storeSessionInfo() {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(bookingUser).build();
		bindingService.storeSessionMetaInfo(supplierConf, SessionServiceBasicHttpBinding_ISessionServiceStub.class,
				sessionStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingServiceBasicHttpBinding_IBookingServiceStub.class,
				bookingStub);
	}

}
