package com.tgs.services.fms.sources.airasia;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.AddPaymentToBooking;
import org.tempuri.AddPaymentToBookingRequestData;
import org.tempuri.AddPaymentToBookingResponse;
import org.tempuri.ArrayOfBookingContact;
import org.tempuri.ArrayOfInt16;
import org.tempuri.ArrayOfInt64;
import org.tempuri.ArrayOfPassenger;
import org.tempuri.ArrayOfPassengerAddress;
import org.tempuri.ArrayOfPassengerBag;
import org.tempuri.ArrayOfPassengerFee;
import org.tempuri.ArrayOfPassengerInfant;
import org.tempuri.ArrayOfPassengerInfo;
import org.tempuri.ArrayOfPassengerTypeInfo;
import org.tempuri.ArrayOfPaxSSR;
import org.tempuri.ArrayOfSegmentSSRRequest;
import org.tempuri.ArrayOfSegmentSeatRequest;
import org.tempuri.ArrayOfSellJourney;
import org.tempuri.ArrayOfSellSegment;
import org.tempuri.AssignSeats;
import org.tempuri.AssignSeatsResponse;
import org.tempuri.Booking;
import org.tempuri.BookingContact;
import org.tempuri.BookingHold;
import org.tempuri.BookingPaymentStatus;
import org.tempuri.Commit;
import org.tempuri.CommitRequestData;
import org.tempuri.CommitResponse;
import org.tempuri.DistributionOption;
import org.tempuri.FlightDesignator;
import org.tempuri.Gender;
import org.tempuri.GetBookingFromState;
import org.tempuri.GetBookingFromStateResponse;
import org.tempuri.MessageState;
import org.tempuri.NotificationPreference;
import org.tempuri.Passenger;
import org.tempuri.PassengerInfant;
import org.tempuri.PassengerInfo;
import org.tempuri.PassengerProgram;
import org.tempuri.PassengerTypeInfo;
import org.tempuri.PaxSSR;
import org.tempuri.Payment;
import org.tempuri.PaymentReferenceType;
import org.tempuri.ReceivedByInfo;
import org.tempuri.RequestPaymentMethodType;
import org.tempuri.SSRRequest;
import org.tempuri.SeatAssignmentMode;
import org.tempuri.SeatSellRequest;
import org.tempuri.SegmentSSRRequest;
import org.tempuri.SegmentSeatRequest;
import org.tempuri.Sell;
import org.tempuri.SellBy;
import org.tempuri.SellFee;
import org.tempuri.SellJourney;
import org.tempuri.SellJourneyRequest;
import org.tempuri.SellJourneyRequestData;
import org.tempuri.SellRequestData;
import org.tempuri.SellResponse;
import org.tempuri.SellSSR;
import org.tempuri.SellSegment;
import org.tempuri.Success;
import org.tempuri.TypeOfSale;
import org.tempuri.UnitHoldType;
import org.tempuri.UpdateContacts;
import org.tempuri.UpdateContactsRequestData;
import org.tempuri.UpdateContactsResponse;
import org.tempuri.Warning;
import org.tempuri.WeightCategory;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierPaymentException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Getter
@Setter
@Slf4j
@SuperBuilder
final class AirAsiaBookingManager extends AirAsiaServiceManager {

	protected AirSearchResult searchResult;
	protected double totalFare;
	protected double paymentToBeCollected;

	protected GeneralServiceCommunicator gmsCommunicator;

	protected String carrierCode;

	protected boolean isTicketNumberRequired;

	public void initialize(List<SegmentInfo> segmentInfos) {
		init();
		carrierCode = segmentInfos.get(0).getAirlineCode(false);
	}

	public void initializeTravellerInfo(List<FlightTravellerInfo> travellerInfos) {
		adults = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT);
		child = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD);
		infants = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT);
		this.adultCount = adults.size();
		this.childCount = child.size();
		this.infantCount = infants.size();
	}

	public boolean sendSellJourneyRequest(List<TripInfo> tripInfos) throws Exception {
		boolean isSellSuccess = false;
		try {
			Sell sell = new Sell();
			sell.setStrSessionID(sessionSignature);
			SellRequestData sellRqData = new SellRequestData();
			sellRqData.setSellBy(SellBy.Journey);
			sellRqData.setSellJourneyRequest(new SellJourneyRequest());
			sellRqData.setSellSSR(null);
			sellRqData.setSellFee(new SellFee());
			sellRqData.setSellJourneyRequest(getSellJourneyRequest(tripInfos));
			sell.setObjSellRequestData(sellRqData);
			listener.setType("4_Sell");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			SellResponse sellResponse = bookingStub.sell(sell);
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			boolean isError = isAnyError(sellResponse.getSellResult());
			isSellSuccess = getIsSellSuccess(sellResponse);
			if (!isError && isSellSuccess) {
				// Sell Success - Trip Itinerary
				double newPrice = getTotalSellfare(sellResponse.getSellResult().getSuccess());
				log.info("Total Supplier Price {} for Review Trip Booking id on Sell Request {} ", newPrice, bookingId);
			} else {
				throw new NoSeatAvailableException();
			}
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSellSuccess;
	}

	public SellJourneyRequest getSellJourneyRequest(List<TripInfo> tripInfos) {
		SellJourneyRequest journeyRequest = new SellJourneyRequest();
		SellJourneyRequestData requestData = new SellJourneyRequestData();
		requestData.setCurrencyCode(getCurrencyCode());
		requestData.setJourneys(getSellJourneys(tripInfos.get(0)));
		requestData.setPassengers(getPassengers());
		requestData.setPaxCount((short) AirUtils.getPaxCount(searchQuery, false));
		requestData.setTypeOfSale(getTypeOfSale(AirUtils.getSegmentInfos(tripInfos)));
		requestData.setSourceBookingPOS(getSourceBookingPOS());
		journeyRequest.setSellJourneyRequestData(requestData);
		return journeyRequest;
	}

	private TypeOfSale getTypeOfSale(List<SegmentInfo> segmentInfos) {
		TypeOfSale typeOfSale = null;
		if (segmentInfos != null && CollectionUtils.isNotEmpty(segmentInfos)) {
			typeOfSale = new TypeOfSale();
			if (StringUtils.isNotBlank(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getAccountCode())) {
				typeOfSale.setState(MessageState.New);
				typeOfSale.setPromotionCode(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getAccountCode());
			}
		}
		return typeOfSale;
	}

	private ArrayOfSellJourney getSellJourneys(TripInfo tripInfo) {
		ArrayOfSellJourney sellJourney = new ArrayOfSellJourney();
		AirUtils.splitTripInfo(tripInfo, false).forEach(trip -> {
			SellJourney sellJourney1 = new SellJourney();
			sellJourney1.setState(MessageState.New);
			sellJourney1.setNotForGeneralUse(false);
			sellJourney1.setSegments(getSegmentsSellJourneyRequest(trip));
			sellJourney.addSellJourney(sellJourney1);
		});
		return sellJourney;
	}

	private ArrayOfSellSegment getSegmentsSellJourneyRequest(TripInfo tripInfo) {
		ArrayOfSellSegment sellSegment = new ArrayOfSellSegment();
		SellSegment sellSegment1 = null;
		for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
			SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = (segmentIndex + 1 < tripInfo.getSegmentInfos().size())
					? tripInfo.getSegmentInfos().get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			if (legNum == 0) {
				sellSegment1 = new SellSegment();
				sellSegment1.setState(MessageState.New);
				sellSegment1.setDepartureStation(segmentInfo.getDepartureAirportCode());
				sellSegment1.setFlightDesignator(getFlightDesignator(segmentInfo));
				sellSegment1.setActionStatusCode("NN");
				sellSegment1.setFare(getSellFare(segmentInfo, 0));
				sellSegment1.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
			} else {
				sellSegment1.setActionStatusCode("NU");
			}
			sellSegment1.setArrivalStation(segmentInfo.getArrivalAirportCode());
			sellSegment1.setSTA(TgsDateUtils.getCalendar(segmentInfo.getArrivalTime().toLocalDate()));
			if (nextSegmentLegNum == 0) {
				sellSegment.addSellSegment(sellSegment1);
			}
		}
		return sellSegment;
	}

	public boolean getIsSellSuccess(SellResponse sellResponse) {
		boolean isSellSuccess = false;
		if (sellResponse != null && sellResponse.getSellResult() != null) {
			Success success = sellResponse.getSellResult().getSuccess();
			if (success != null && success.getPNRAmount() != null) {
				isSellSuccess = true;
			}
		}
		if (sellResponse.getSellResult().getWarning() != null) {
			Warning warning = sellResponse.getSellResult().getWarning();
			log.info("Warning Occured on booking id {} message {} ", bookingId, warning.getWarningText());
		}
		return isSellSuccess;
	}

	public double sendGSTAndContactInfo(GstInfo gstInfo) throws RemoteException {
		double newFare = 0d;
		try {
			UpdateContacts gstContacts = new UpdateContacts();
			gstContacts.setStrSessionID(sessionSignature);
			gstContacts.setObjUpdateContactsRequestData(getUpdateContactAndGSTData(gstInfo));
			listener.setType("6_UpdateContacts");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			UpdateContactsResponse contactsResponse = bookingStub.updateContacts(gstContacts);
			boolean isError = isAnyError(contactsResponse.getUpdateContactsResult());
			if (contactsResponse.isUpdateContactsResultSpecified() && !isError) {
				newFare = getTotalSellfare(contactsResponse.getUpdateContactsResult().getSuccess());
				totalFare = newFare;
				newFare = getAmountBasedOnCurrency(new BigDecimal(newFare), getCurrencyCode());
				log.info("Sell GST new fare {} for booking id {}", newFare, bookingId);
			} else {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return newFare;
	}

	private UpdateContactsRequestData getUpdateContactAndGSTData(GstInfo gstInfo) {
		UpdateContactsRequestData requestData = new UpdateContactsRequestData();
		ArrayOfBookingContact bookingContact = new ArrayOfBookingContact();
		int index = 0;
		BookingContact[] contact = null;
		contact = new BookingContact[1];
		contact[index] = new BookingContact();
		contact[index] = getGSTContactInfo(gstInfo);
		bookingContact.setBookingContact(contact);
		requestData.setBookingContactList(bookingContact);
		return requestData;
	}

	public BookingContact getGSTContactInfo(GstInfo gstInfo) {
		BookingContact contact = new BookingContact();
		ClientGeneralInfo generalInfo = ServiceCommunicatorHelper.getClientInfo();
		String gstCompanyName =
				AirAsiaUtils.getValidCompanyName(BaseUtils.getCleanGstName(gstInfo.getRegisteredName()));
		contact.setState(MessageState.New);
		contact.setTypeCode(airAsiaAirline.getGSTTypeCode());
		contact.setNames(null);
		contact.setEmailAddress(gstInfo.getEmail());
		contact.setSourceOrganization(StringUtils.EMPTY);
		contact.setNotificationPreference(NotificationPreference.None);
		contact.setCountryCode(StringUtils.EMPTY);
		contact.setFax(StringUtils.EMPTY);
		contact.setCompanyName(gstCompanyName);
		contact.setProvinceState(gstInfo.getState());
		contact.setPostalCode(gstInfo.getPincode());
		contact.setCustomerNumber(gstInfo.getGstNumber());
		contact.setDistributionOption(DistributionOption.None);
		contact.setWorkPhone(AirAsiaUtils.getContact(generalInfo.getCountryCode(), gstInfo.getMobile()));
		contact.setHomePhone(AirAsiaUtils.getContact(generalInfo.getCountryCode(), gstInfo.getMobile()));
		return contact;
	}

	private ArrayOfBookingContact getBookingContacts(List<FlightTravellerInfo> travellerInfos) {
		ArrayOfBookingContact bookingContact = new ArrayOfBookingContact();
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(bookingUser.getUserId(), clientInfo);
		FlightTravellerInfo travellerInfo =
				AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).get(0);
		BookingContact contact = new BookingContact();
		contact.setState(MessageState.New);
		contact.setTypeCode(airAsiaAirline.getContactTypeCode());
		contact.setEmailAddress(AirSupplierUtils.getEmailId(deliveryInfo));
		contact.setHomePhone(AirAsiaUtils.getContact(clientInfo.getCountryCode(), clientInfo.getWorkPhone()));
		contact.setSourceOrganization(configuration.getSupplierCredential().getOrganisationCode());
		contact.setNotificationPreference(NotificationPreference.None);
		contact.setCountryCode(clientInfo.getNationality());
		contact.setFax(clientInfo.getFax());
		contact.setWorkPhone(AirAsiaUtils.getContact(clientInfo.getCountryCode(), clientInfo.getWorkPhone()));
		contact.setCompanyName(AirAsiaUtils.getCompanyName(bookingUser, clientInfo));
		// contact.setCompanyName(clientInfo.getCompanyName().toUpperCase());
		contact.setOtherPhone(AirSupplierUtils.getContactNumberWithCountryCode(deliveryInfo));
		contact.setPostalCode(contactAddress.getPincode());
		contact.setCustomerNumber(StringUtils.EMPTY);
		contact.setNames(airAsiaAirline.getBookingPaxName(travellerInfo));
		contact.setAddressLine1(AirAsiaUtils.getAddress(contactAddress.getAddress()));
		contact.setCity(AirAsiaUtils.getCity(contactAddress.getCityInfo().getName()));
		contact.setDistributionOption(DistributionOption.Email);
		bookingContact.addBookingContact(contact);
		return bookingContact;
	}

	public double addPaymentToBooking() {
		try {
			AddPaymentToBooking paymentToBooking = new AddPaymentToBooking();
			paymentToBooking.setStrSessionID(sessionSignature);
			paymentToBooking.setObjAddPaymentToBookingRequestData(buildPaymentRequest());
			listener.setType("7_AddPaymentToBooking");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			AddPaymentToBookingResponse paymentResponse = bookingStub.addPaymentToBooking(paymentToBooking);
			if (!isAnyPaymentError(paymentResponse)) {
				Payment payment = paymentResponse.getAddPaymentToBookingResult().getValidationPayment().getPayment();
				paymentToBeCollected = getAmountBasedOnCurrency(payment.getPaymentAmount(), payment.getCurrencyCode());
			} else {
				throw new SupplierPaymentException(StringUtils.join(",", getCriticalMessageLogger()));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return paymentToBeCollected;
	}

	private AddPaymentToBookingRequestData buildPaymentRequest() {
		AddPaymentToBookingRequestData paymentRequestData = new AddPaymentToBookingRequestData();
		paymentRequestData.setMessageState(MessageState.New);
		paymentRequestData.setWaiveFee(false);
		paymentRequestData.setReferenceType(PaymentReferenceType.Default);
		paymentRequestData.setPaymentMethodType(RequestPaymentMethodType.AgencyAccount);
		paymentRequestData.setPaymentMethodCode("AG");
		BigDecimal amountToPay = new BigDecimal(totalFare).setScale(7, BigDecimal.ROUND_HALF_UP);
		paymentRequestData.setQuotedAmount(amountToPay);
		paymentRequestData.setStatus(BookingPaymentStatus.New);
		paymentRequestData.setAccountNumberID(0);
		paymentRequestData.setInstallments((short) 1);
		paymentRequestData.setQuotedCurrencyCode(getCurrencyCode());
		paymentRequestData.setAccountNumber(configuration.getSupplierCredential().getOrganisationCode().toUpperCase());
		paymentRequestData.setExpiration(getExpirationCalendar());
		paymentRequestData.setParentPaymentID(0);
		paymentRequestData.setPaymentText("PAY BY AG");
		paymentRequestData.setDeposit(false);
		paymentRequestData.setAuthorizationCode(StringUtils.EMPTY);
		return paymentRequestData;
	}

	private Calendar getExpirationCalendar() {
		LocalDate localDate = LocalDate.now();
		localDate = localDate.withYear(0001).withMonth(01).withDayOfYear(01);
		Calendar calendar = TgsDateUtils.getCalendar(localDate);
		return calendar;
	}

	public String commitBooking(boolean isHoldBooking, String pnr, List<SegmentInfo> segmentInfos) {
		String recordLocator = null;
		try {
			Commit commit = new Commit();
			commit.setStrSessionID(sessionSignature);
			commit.setObjCommitRequestData(buildCommitRequest(isHoldBooking, pnr, segmentInfos));
			listener.setType("8_Commit");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			CommitResponse commitResponse = bookingStub.commit(commit);
			boolean isError = isAnyError(commitResponse.getCommitResult());
			if (commitResponse.isCommitResultSpecified() && !isError) {
				recordLocator = commitResponse.getCommitResult().getSuccess().getRecordLocator();
			} else {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return recordLocator;
	}

	private CommitRequestData buildCommitRequest(boolean isHoldBooking, String pnr, List<SegmentInfo> segmentInfos) {
		CommitRequestData requestData = new CommitRequestData();
		requestData.setBooking(getBooking(isHoldBooking, segmentInfos));
		return requestData;
	}

	private Booking getBooking(boolean isHoldBooking, List<SegmentInfo> segmentInfos) {
		Booking booking = new Booking();
		booking.setState(MessageState.New);
		booking.setPaxCount((short) (adultCount + childCount));
		booking.setBookingParentID(0);
		booking.setBookingID(0);
		booking.setBookingHold(getHoldBooking(isHoldBooking));
		booking.setPassengers(buildPassengerInfos());
		booking.setTypeOfSale(getTypeOfSale(segmentInfos));
		booking.setSourceBookingPOS(getSourceBookingPOS());
		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		travellerInfos.addAll(adults);
		ArrayOfBookingContact bookingContact = null;
		bookingContact = getBookingContacts(travellerInfos);
		booking.setBookingContacts(bookingContact);
		return booking;
	}

	private ArrayOfPassenger buildPassengerInfos() {
		ArrayOfPassenger passengers = new ArrayOfPassenger();
		int totalPaxIndex = 0;
		for (int paxIndex = 0; paxIndex < adults.size(); paxIndex++) {
			passengers.addPassenger(getPassengerDetails(adults.get(paxIndex), paxIndex, totalPaxIndex));
			totalPaxIndex++;
		}
		for (int paxIndex = 0; paxIndex < child.size(); paxIndex++) {
			passengers.addPassenger(getPassengerDetails(child.get(paxIndex), paxIndex, totalPaxIndex));
			totalPaxIndex++;
		}
		return passengers;
	}

	private Passenger getPassengerDetails(FlightTravellerInfo travellerInfo, int paxIndex, int totalPaxIndex) {
		Passenger passenger = new Passenger();
		passenger.setPassengerNumber((short) totalPaxIndex);
		passenger.setState(MessageState.Clean);
		passenger.setNames(airAsiaAirline.getBookingPaxName(travellerInfo));
		PassengerInfant infant = null;
		if (travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())
				&& CollectionUtils.isNotEmpty(infants)) {
			infant = getInfantPassenger(travellerInfo, paxIndex);
			if (infant != null) {
				passenger.setInfant(infant);
			}
		}
		passenger.setPassengerInfo(getPassenegerInfo(travellerInfo));
		passenger.setPassengerProgram(new PassengerProgram());
		passenger.setPassengerFees(new ArrayOfPassengerFee());
		passenger.setPassengerAddresses(new ArrayOfPassengerAddress());
		FlightTravellerInfo infantTravellerInfo = getInfantTravellerInfo(travellerInfo, paxIndex);
		passenger.setPassengerTravelDocuments(
				airAsiaAirline.getPassengerTravelDocument(travellerInfo, infantTravellerInfo, carrierCode));
		passenger.setPassengerBags(new ArrayOfPassengerBag());
		passenger.setPassengerID(totalPaxIndex);
		passenger.setPassengerTypeInfos(getPassengerTypeInfo(travellerInfo));
		passenger.setPseudoPassenger(false);
		passenger.setPassengerInfos(new ArrayOfPassengerInfo());
		passenger.setPassengerInfants(new ArrayOfPassengerInfant());
		passenger.setPassengerTypeInfo(null);
		return passenger;
	}


	private PassengerInfo getPassenegerInfo(FlightTravellerInfo flightTravellerInfo) {
		PassengerInfo passengerInfo = new PassengerInfo();
		passengerInfo.setGender(AirAsiaUtils.getGender(flightTravellerInfo.getTitle(), flightTravellerInfo));
		passengerInfo.setNationality(AirAsiaUtils.getNationality(flightTravellerInfo));
		passengerInfo.setWeightCategory(getWeightCategory(flightTravellerInfo));
		passengerInfo.setResidentCountry(AirAsiaUtils.getNationality(flightTravellerInfo));
		passengerInfo.setState(MessageState.New);
		return passengerInfo;
	}

	private WeightCategory getWeightCategory(FlightTravellerInfo travellerInfo) {
		WeightCategory weightCategory = null;
		Gender gender = AirAsiaUtils.getGender(travellerInfo.getTitle(), travellerInfo);
		if (travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())) {
			weightCategory = WeightCategory.Child;
		} else if (Gender._Male.equalsIgnoreCase(gender.getValue())
				&& travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			weightCategory = WeightCategory.Male;
		} else if (Gender._Female.equalsIgnoreCase(gender.getValue())
				&& travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			weightCategory = WeightCategory.Female;
		}
		return weightCategory;
	}


	private PassengerInfant getInfantPassenger(FlightTravellerInfo flightTravellerInfo, int paxIndex) {
		PassengerInfant infant = null;
		FlightTravellerInfo infantTraveller = getInfantTravellerInfo(flightTravellerInfo, paxIndex);
		if (infantTraveller != null) {
			infant = new PassengerInfant();
			if (infantTraveller.getDob() != null) {
				infant.setDOB(TgsDateUtils.getCalendar(infantTraveller.getDob()));
			} else {
				infant.setDOB(TgsDateUtils.getDefaultDOB());
			}
			infant.setNames(airAsiaAirline.getBookingPaxName(infantTraveller));
			infant.setGender(AirAsiaUtils.getGender(infantTraveller.getTitle(), infantTraveller));
			infant.setNationality(AirAsiaUtils.getNationality(infantTraveller));
			infant.setResidentCountry(AirAsiaUtils.getNationality(infantTraveller));
			infant.setState(MessageState.New);
		}
		return infant;
	}

	private ArrayOfPassengerTypeInfo getPassengerTypeInfo(FlightTravellerInfo flightTravellerInfo) {
		ArrayOfPassengerTypeInfo passengerTypeInfo = new ArrayOfPassengerTypeInfo();
		PassengerTypeInfo[] typeInfo = new PassengerTypeInfo[1];
		typeInfo[0] = new PassengerTypeInfo();
		typeInfo[0] = getPaxTypeInfo(flightTravellerInfo);
		passengerTypeInfo.setPassengerTypeInfo(typeInfo);
		return passengerTypeInfo;
	}

	private PassengerTypeInfo getPaxTypeInfo(FlightTravellerInfo flightTravellerInfo) {
		PassengerTypeInfo typeInfo = new PassengerTypeInfo();
		if (flightTravellerInfo.getDob() != null) {
			typeInfo.setDOB(TgsDateUtils.getCalendar(flightTravellerInfo.getDob()));
		} else {
			typeInfo.setDOB(TgsDateUtils.getDefaultDOB());
		}
		typeInfo.setState(MessageState.New);
		if (flightTravellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			typeInfo.setPaxType(PAX_TYPE_ADULT);
		} else if (flightTravellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())) {
			typeInfo.setPaxType(PAX_TYPE_CHILD);
		}
		return typeInfo;
	}

	private FlightTravellerInfo getInfantTravellerInfo(FlightTravellerInfo flightTravellerInfo, int paxIndex) {
		FlightTravellerInfo infantTraveller = null;
		if (flightTravellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())
				&& infants.size() >= (paxIndex + 1)) {
			infantTraveller = infants.get(paxIndex);
		}
		return infantTraveller;
	}

	private BookingHold getHoldBooking(boolean isHoldBook) {
		BookingHold bookingHold = null;
		if (isHoldBook) {
			bookingHold = new BookingHold();
			Calendar holdDate = TgsDateUtils.getCalendar(TgsDateUtils.getCurrentDate());
			holdDate.add(Calendar.DAY_OF_YEAR, 1);
			bookingHold.setHoldDateTime(holdDate);
			bookingHold.setState(MessageState.New);
		}
		return bookingHold;
	}

	private ReceivedByInfo getReceivedByInfo() {
		ReceivedByInfo receivedByInfo = new ReceivedByInfo();
		receivedByInfo.setReceivedBy(configuration.getSupplierCredential().getUserName());
		receivedByInfo.setReceivedReference(StringUtils.EMPTY);
		receivedByInfo.setReferralCode(StringUtils.EMPTY);
		receivedByInfo.setLatestReceivedBy(StringUtils.EMPTY);
		receivedByInfo.setLatestReceivedReference(StringUtils.EMPTY);
		receivedByInfo.setState(MessageState.New);
		return receivedByInfo;
	}

	public double sendSeatSellRequest(List<SegmentInfo> segmentInfos) {
		AssignSeats assignSeats = new AssignSeats();
		double totalSellFare = 0;
		AssignSeatsResponse seatResp = null;
		try {
			assignSeats.setStrSessionID(sessionSignature);
			assignSeats.setObjSeatSellRequest(buildSeatRequest(segmentInfos));
			listener.setType("R-ASSIGNSEAT");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			seatResp = bookingStub.assignSeats(assignSeats);
			if (isAnyError(seatResp.getAssignSeatsResult())) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		if (seatResp != null) {
			totalSellFare = getTotalSellfare(seatResp.getAssignSeatsResult().getSuccess());
			totalFare = totalSellFare;
			totalSellFare = getAmountBasedOnCurrency(new BigDecimal(totalSellFare), getCurrencyCode());
			log.info("Sell Seat new fare {} for booking id {}", totalSellFare, bookingId);
		}
		return totalSellFare;
	}

	private SeatSellRequest buildSeatRequest(List<SegmentInfo> segmentInfos) {
		SeatSellRequest seatSellRequest = new SeatSellRequest();
		seatSellRequest.setSeatAssignmentMode(SeatAssignmentMode.PreSeatAssignment);
		seatSellRequest.setSegmentSeatRequests(getSegmentsSeatRequest(segmentInfos));
		seatSellRequest.setBlockType(UnitHoldType.Session);
		seatSellRequest.setSameSeatRequiredOnThruLegs(true);
		seatSellRequest.setAssignNoSeatIfAlreadyTaken(false);
		seatSellRequest.setWaiveFee(false);
		seatSellRequest.setIgnoreSeatSSRs(false);
		return seatSellRequest;
	}

	public ArrayOfSegmentSeatRequest getSegmentsSeatRequest(List<SegmentInfo> segmentInfos) {
		ArrayOfSegmentSeatRequest segmentSeatRequest = new ArrayOfSegmentSeatRequest();
		for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
			Integer legNum = segmentInfos.get(segmentIndex).getPriceInfo(0).getMiscInfo().getLegNum();
			if (legNum == 0) {
				List<SegmentInfo> subLegSegments = this.getSubLegSegments(segmentInfos, segmentIndex);
				addPaxSeatSSR(subLegSegments, segmentSeatRequest);
			}
		}
		return segmentSeatRequest;
	}

	public void addPaxSeatSSR(List<SegmentInfo> segmentInfos, ArrayOfSegmentSeatRequest segmentSeatRequest) {
		// Add Seat if Any to travaller
		SegmentInfo firstSegment = segmentInfos.get(0);
		SegmentInfo lastSegment = segmentInfos.get(segmentInfos.size() - 1);
		SegmentBookingRelatedInfo relatedInfo = firstSegment.getBookingRelatedInfo();
		List<FlightTravellerInfo> flightTravellerInfos = relatedInfo.getTravellerInfo();
		for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
			SSRInformation seatInformation = flightTravellerInfos.get(paxIndex).getSsrSeatInfo();
			if (Objects.nonNull(seatInformation)) {
				long[] paxIDs = new long[1];
				short[] paxNums = new short[1];
				paxIDs[0] = 0;
				paxNums[0] = (short) (paxIndex);
				ArrayOfInt64 longPax = new ArrayOfInt64();
				ArrayOfInt16 paxShort = new ArrayOfInt16();
				longPax.set_long(paxIDs);
				paxShort.set_short(paxNums);
				SegmentSeatRequest seatRequest = new SegmentSeatRequest();
				seatRequest.setCompartmentDesignator(StringUtils.EMPTY);
				seatRequest.setPassengerSeatPreferences(null);
				seatRequest.setArrivalStation(lastSegment.getArrivalAirportInfo().getCode());
				seatRequest.setDepartureStation(firstSegment.getDepartAirportInfo().getCode());
				seatRequest.setSTD(TgsDateUtils.getCalendar(firstSegment.getDepartTime().toLocalDate()));
				FlightDesignator designator = new FlightDesignator();
				designator.setCarrierCode(firstSegment.getFlightDesignator().getAirlineInfo().getCode());
				designator.setFlightNumber(StringUtils.leftPad(firstSegment.getFlightNumber(), 4, " "));
				seatRequest.setFlightDesignator(designator);
				seatRequest.setPassengerIDs(longPax);
				seatRequest.setPassengerNumbers(paxShort);
				seatRequest.setUnitDesignator(seatInformation.getCode());
				segmentSeatRequest.addSegmentSeatRequest(seatRequest);
			}
		}
	}

	public List<SegmentInfo> sendSellSSRRequest(List<SegmentInfo> segmentInfos, boolean isInfantSell) {
		searchResult = new AirSearchResult();
		try {
			Sell sellRequest = new Sell();
			SellRequestData sellRequestData = new SellRequestData();
			sellRequest.setStrSessionID(sessionSignature);
			sellRequestData.setSellBy(SellBy.SSR);
			sellRequestData.setSellJourneyRequest(new SellJourneyRequest());
			sellRequestData.setSellFee(new SellFee());
			sellRequestData.setSellJourneyByKeyRequest(null);
			sellRequestData.setSellSSR(getSSRTrip(segmentInfos, isInfantSell));
			sellRequest.setObjSellRequestData(sellRequestData);
			listener.setType("SellSsr");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			SellResponse sellResponse = bookingStub.sell(sellRequest);
			double newPrice = 0;
			if (!isAnyError(sellResponse.getSellResult())) {
				newPrice = getTotalSellfare(sellResponse.getSellResult().getSuccess());
				totalFare = newPrice;
				newPrice = getAmountBasedOnCurrency(new BigDecimal(newPrice), getCurrencyCode());
				log.info("Total Supplier Price {} for Booking id {} on Sell SSR Request", newPrice, bookingId);
			} else {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return segmentInfos;
	}

	private SellSSR getSSRTrip(List<SegmentInfo> segmentInfos, boolean isInfantSell) {
		SellSSR sellSSR = new SellSSR();
		if (airAsiaAirline.isAdditionalSSRRequired(segmentInfos) || AirUtils.isSSRAddedInTrip(segmentInfos)
				|| infantCount > 0) {
			SSRRequest ssrRequest = new SSRRequest();
			ArrayOfSegmentSSRRequest segmentSSRRequest = new ArrayOfSegmentSSRRequest();
			SegmentSSRRequest ssrSegmentRequest = null;
			List<SegmentInfo> subLegSegments = null;
			for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
				SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
				Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
				Integer nextSegmentLegNum = AirAsiaUtils.getNextLegNum(segmentInfos, segmentIndex);
				if (legNum == 0) {
					ssrSegmentRequest = new SegmentSSRRequest();
					ssrSegmentRequest.setDepartureStation(segmentInfo.getDepartAirportInfo().getCode());
					ssrSegmentRequest.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
					FlightDesignator designator = new FlightDesignator();
					designator.setCarrierCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
					designator.setFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber());
					ssrSegmentRequest.setFlightDesignator(designator);
					subLegSegments = getSubLegSegments(segmentInfos, segmentIndex);
				}
				if (nextSegmentLegNum == 0) {
					ssrSegmentRequest.setArrivalStation(segmentInfo.getArrivalAirportInfo().getCode());
					ArrayOfPaxSSR paxSSR = getPaxSSR(subLegSegments);
					if (paxSSR != null && paxSSR.getPaxSSR() != null) {
						ssrSegmentRequest.setPaxSSRs(paxSSR);
						segmentSSRRequest.addSegmentSSRRequest(ssrSegmentRequest);
					}
					subLegSegments = null;
				}
			}
			ssrRequest.setSegmentSSRRequests(segmentSSRRequest);
			ssrRequest.setCurrencyCode(getCurrencyCode());
			sellSSR.setSSRRequest(ssrRequest);
		}
		return sellSSR;
	}

	private boolean hasMealSSR(SegmentInfo segmentInfo) {
		boolean isMealIncluded = false;
		if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
				&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
			for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
				if (Objects.nonNull(travellerInfo.getSsrMealInfo())) {
					isMealIncluded = true;
					break;
				}
			}
		}
		return isMealIncluded;
	}

	private ArrayOfPaxSSR getPaxSSR(List<SegmentInfo> segmentInfos) {
		ArrayOfPaxSSR paxSSR = new ArrayOfPaxSSR();

		for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
			if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
					&& Objects.nonNull(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				if (segmentIndex == 0) {

					for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
						SSRInformation baggageInformation = segmentInfo.getBookingRelatedInfo().getTravellerInfo()
								.get(paxIndex).getSsrBaggageInfo();
						if (Objects.nonNull(baggageInformation)
								&& !airAsiaAirline.passThruLegCodes().contains(baggageInformation.getCode())) {
							PaxSSR ssr = new PaxSSR();
							ssr.setActionStatusCode("NN");
							ssr.setDepartureStation(segmentInfos.get(0).getDepartureAirportCode());
							ssr.setArrivalStation(segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportCode());
							ssr.setPassengerNumber((short) paxIndex);
							ssr.setSSRCode(baggageInformation.getCode());
							paxSSR.addPaxSSR(ssr);
						}

						if (airAsiaAirline.isAdditionalSSRRequired(Arrays.asList(segmentInfos.get(0)))) {
							List<String> additionalSSRCodes = airAsiaAirline.getAdditionalSSRCodes(segmentInfos);
							for (String ssrCode : additionalSSRCodes) {
								if (StringUtils.isNotEmpty(ssrCode)) {
									PaxSSR ssr = new PaxSSR();
									ssr.setActionStatusCode("NN");
									ssr.setDepartureStation(segmentInfos.get(0).getDepartureAirportCode());
									ssr.setArrivalStation(
											segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportCode());
									ssr.setPassengerNumber((short) paxIndex);
									ssr.setSSRCode(ssrCode);
									paxSSR.addPaxSSR(ssr);
								}
							}
						}
					}

					if (infantCount > 0) {
						for (int infantIndex = 0; infantIndex < infantCount; infantIndex++) {
							PaxSSR ssr = new PaxSSR();
							ssr.setActionStatusCode("NN");
							ssr.setDepartureStation(segmentInfos.get(0).getDepartureAirportCode());
							ssr.setArrivalStation(segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportCode());
							ssr.setPassengerNumber((short) infantIndex);
							ssr.setSSRCode(PAX_TYPE_INFANT);
							paxSSR.addPaxSSR(ssr);
						}
					}
				}

				if (hasMealSSR(segmentInfo)) {
					for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
						SSRInformation mealInformation =
								segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(paxIndex).getSsrMealInfo();
						if (Objects.nonNull(mealInformation)) {
							PaxSSR ssr = new PaxSSR();
							ssr.setActionStatusCode("NN");
							ssr.setDepartureStation(segmentInfo.getDepartureAirportCode());
							ssr.setArrivalStation(segmentInfo.getArrivalAirportCode());
							ssr.setPassengerNumber((short) paxIndex);
							ssr.setSSRCode(mealInformation.getCode());
							paxSSR.addPaxSSR(ssr);
						}
					}
				}
			}
		}

		return paxSSR;
	}

	public double getBookingFromState() {
		Double totalPrice = 0.0;
		try {
			GetBookingFromState bookingState = new GetBookingFromState();
			bookingState.setStrSessionID(sessionSignature);
			listener.setType("GetBookingFromState");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetBookingFromStateResponse response = bookingStub.getBookingFromState(bookingState);
			if (Objects.nonNull(response) && Objects.nonNull(response.getGetBookingFromStateResult())
					&& Objects.nonNull(response.getGetBookingFromStateResult().getBookingSum())) {
				totalPrice = getAmountBasedOnCurrency(
						response.getGetBookingFromStateResult().getBookingSum().getBalanceDue(), getCurrencyCode());

			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return totalPrice;
	}

}
