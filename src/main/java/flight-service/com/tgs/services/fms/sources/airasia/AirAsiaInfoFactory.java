package com.tgs.services.fms.sources.airasia;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.tempuri.BookingServiceBasicHttpBinding_IBookingServiceStub;
import org.tempuri.FareServiceBasicHttpBinding_IFareServiceStub;
import org.tempuri.SessionServiceBasicHttpBinding_ISessionServiceStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirAsiaInfoFactory extends AbstractAirInfoFactory {

	protected SoapRequestResponseListner listener = null;

	protected SessionServiceBasicHttpBinding_ISessionServiceStub sessionStub;

	protected BookingServiceBasicHttpBinding_IBookingServiceStub bookingStub;

	protected FareServiceBasicHttpBinding_IFareServiceStub searchStub;

	protected String sessionToken;

	protected AirAsiaBindingService bindingService;

	public AirAsiaInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize(boolean isSearch) {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(user).build();
		sessionToken = fetchSessionToken(isSearch);
		sessionStub = bindingService.getSessionStub(supplierConf, searchQuery);
		bookingStub = bindingService.getBookingStub(supplierConf, searchQuery);
		searchStub = bindingService.getFareServiceStub(supplierConf, searchQuery);
	}

	public AirAsiaInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize(true);
		log.debug("Intialization completed  for search Query {} ", searchQuery);
		searchResult = new AirSearchResult();
		AirAsiaSessionManager sessionManager = null;
		int numTry = 0;
		boolean doRetry = false;
		do {
			try {
				listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
						supplierConf.getBasicInfo().getSupplierName());
				sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf).listener(listener)
						.sessionStub(sessionStub).criticalMessageLogger(criticalMessageLogger).bookingUser(user)
						.build();
				listener.setStoreLogs(sessionManager.isStoreLog(searchQuery));
				sessionManager.isStoreLog(searchQuery);
				if (StringUtils.isBlank(sessionToken)) {
					sessionManager.openSession();
					sessionToken = sessionManager.getSessionSignature();
				}
				AirAsiaSearchManager searchManager = AirAsiaSearchManager.builder().configuration(supplierConf)
						.searchQuery(searchQuery).sessionSignature(sessionToken).listener(listener)
						.moneyExchnageComm(moneyExchnageComm).searchStub(searchStub).bookingStub(bookingStub)
						.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
						.supplierAnalyticInfos(supplierAnalyticsInfos).bookingUser(user).build();
				searchManager.init();
				searchResult = searchManager.doSearch(true);
				if (!searchManager.isAvailabilityWithTax()) {
					searchResult = searchManager.processSearchResult(searchResult);
					searchResult = searchManager.doItineraryPrice(searchResult,
							StringUtils.isEmpty(contextData.getBookingId()));
				}
				searchResult = searchManager.setInfantPrice(searchResult, sessionManager);
				AirAsiaUtils.filterTripAndPriceOptions(searchResult, searchQuery);
			} catch (SupplierSessionException se) {
				setEmptySearchResult();
				doRetry = false;
				if (sessionManager.isRetry(se) && numTry == 0) {
					if (StringUtils.isNotBlank(sessionToken)) {
						sessionManager.setSessionSignature(sessionToken);
						sessionManager.closeSession();
					}
					sessionToken = null;
					doRetry = true;
				} else {
					throw se;
				}
			} finally {
				if (StringUtils.isNotBlank(sessionToken) && !doRetry) {
					storeSession(sessionToken, sessionManager);
				}
				storeSessionStubs();
			}
		} while (++numTry < 2 && doRetry);
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize(false);
		AirAsiaSessionManager sessionManager = null;
		SupplierSession session = null;
		boolean isReviewSuccess = true;
		try {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf)
					.criticalMessageLogger(criticalMessageLogger).listener(listener).sessionStub(sessionStub)
					.bookingUser(user).build();
			sessionManager.init();
			sessionManager.setSessionToken(session);
			AirAsiaSearchManager searchManager = AirAsiaSearchManager.builder().configuration(supplierConf)
					.searchStub(searchStub).searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature)
					.listener(listener).moneyExchnageComm(moneyExchnageComm).bookingStub(bookingStub)
					.bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
					.sourceConfiguration(sourceConfiguration).bookingUser(user).build();
			searchManager.init();
			searchManager.getItineraryPrice(Arrays.asList(Pair.of(selectedTrip, 0)), false).get(0);
			if (Objects.isNull(selectedTrip)) {
				throw new NoSeatAvailableException();
			}
			AirAsiaBookingManager reviewManager = AirAsiaBookingManager.builder().configuration(supplierConf)
					.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature).bookingId(bookingId)
					.listener(listener).bookingStub(bookingStub).sourceConfiguration(sourceConfiguration)
					.moneyExchnageComm(moneyExchnageComm).criticalMessageLogger(criticalMessageLogger)
					.searchStub(searchStub).bookingUser(user).build();
			AirAsiaSSRManager ssrManager = AirAsiaSSRManager.builder().configuration(supplierConf)
					.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature).bookingId(bookingId)
					.listener(listener).bookingStub(bookingStub).sourceConfiguration(sourceConfiguration)
					.moneyExchnageComm(moneyExchnageComm).criticalMessageLogger(criticalMessageLogger)
					.searchStub(searchStub).bookingUser(user).build();
			reviewManager.init();
			ssrManager.init();
			reviewManager.sendSellJourneyRequest(Arrays.asList(selectedTrip));
			ssrManager.doSSRSearch(selectedTrip);
			searchManager.setInfantPriceOnReview(Arrays.asList(selectedTrip));
		} catch (Exception e) {
			sessionManager.closeSession();
			isReviewSuccess = false;
			throw new NoSeatAvailableException();
		} finally {
			if (isReviewSuccess) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().build();
				sessionInfo.setSessionToken(sessionManager.getSessionSignature());
				storeBookingSession(bookingId, sessionInfo, session, selectedTrip);
			}
			storeSessionStubs();
		}
		return selectedTrip;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		AirAsiaSessionManager sessionManager = null;
		SupplierSession session = null;
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		initialize(false);
		try {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(tripInfo.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = AirAsiaSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionStub(sessionStub).criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			sessionManager.init();
			sessionManager.setSessionSignature(session.getSupplierSessionInfo().getSessionToken());
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				AirAsiaSSRManager seatMapManager = AirAsiaSSRManager.builder().configuration(supplierConf)
						.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature).bookingId(bookingId)
						.moneyExchnageComm(moneyExchnageComm).sourceConfiguration(sourceConfiguration)
						.listener(listener).bookingStub(bookingStub).criticalMessageLogger(criticalMessageLogger)
						.bookingUser(user).build();
				seatMapManager.setPaxCount();
				seatMapManager.init();
				for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
					int legNum = tripInfo.getSegmentInfos().get(segmentIndex).getPriceInfo(0).getMiscInfo().getLegNum();
					if (legNum == 0) {
						List<SegmentInfo> subLegSegments =
								seatMapManager.getSubLegSegments(tripInfo.getSegmentInfos(), segmentIndex);
						seatMapManager.setSeatMap(subLegSegments, tripSeatMap);
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to fetch seat map for trip {}", tripInfo.toString(), e);
		} finally {
			storeSessionStubs();
		}
		return tripSeatMap;
	}

	private void storeSessionStubs() {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(user).build();
		bindingService.storeSessionMetaInfo(supplierConf, SessionServiceBasicHttpBinding_ISessionServiceStub.class,
				sessionStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingServiceBasicHttpBinding_IBookingServiceStub.class,
				bookingStub);
		bindingService.storeSessionMetaInfo(supplierConf, FareServiceBasicHttpBinding_IFareServiceStub.class,
				searchStub);
	}

	public String fetchSessionToken(boolean isSearch) {
		if (isSearch) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
					.index(0).compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
			if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
				metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString()).index(-1)
						.compress(false).build();
				metaInfo = cachingComm.fetchFromQueue(metaInfo);
			}

			if (metaInfo != null && metaInfo.getValue() != null) {
				return (String) metaInfo.getValue();
			}
		}
		return null;
	}

	public void storeSession(String sessionToken, AirAsiaSessionManager sessionManager) {
		if (StringUtils.isNotBlank(sessionToken)) {
			try {
				long sessionValidMin = sessionManager.getSessionValidTime();
				SessionMetaInfo metaInfo = SessionMetaInfo.builder()
						.key(supplierConf.getBasicInfo().getRuleId().toString()).compress(false).build();
				metaInfo.setValue(sessionToken);
				metaInfo.setExpiryTime(LocalDateTime.now().plusMinutes(sessionValidMin));
				cachingComm.storeInQueue(metaInfo);
			} catch (Exception e) {
				log.error(AirSourceConstants.SEARCH_SESSION_FAILURE, searchQuery.getSearchId(), e);
				sessionManager = AirAsiaSessionManager.builder().searchQuery(searchQuery).configuration(supplierConf)
						.sessionSignature(sessionToken).build();
				sessionManager.closeSession();
			}
		}
	}

	private void setEmptySearchResult() {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			searchResult = null;
		}
	}

	@Override
	public Boolean initializeStubs() {
		bindingService = AirAsiaBindingService.builder().cacheCommunicator(cachingComm).user(user).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
		sessionStub = bindingService.getSessionStub(supplierConf, null);
		searchStub = bindingService.getFareServiceStub(supplierConf, null);
		storeSessionStubs();
		return true;
	}
}
