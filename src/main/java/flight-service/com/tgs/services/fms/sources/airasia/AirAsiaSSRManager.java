package com.tgs.services.fms.sources.airasia;

import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import com.tgs.services.fms.utils.AirUtils;
import java.rmi.RemoteException;
import java.util.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.*;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.PaxType;

@Getter
@Setter
@Slf4j
@SuperBuilder
final class AirAsiaSSRManager extends AirAsiaServiceManager {

	public void doSSRSearch(TripInfo selectedTrip) {
		// Always We will be doing Review for Only one trip as Journey wise
		// Case : Onward will be one trip & return will be one trip with different key
		// so SSR should also on journey wise
		AirUtils.splitTripInfo(selectedTrip, false).forEach(tripInfo -> {
			getAllSSRDetailsforTrip(tripInfo);
		});
	}

	private void getAllSSRDetailsforTrip(TripInfo tripInfo) {
		try {
			GetSSRAvailabilityForBooking ssrRequest = new GetSSRAvailabilityForBooking();
			ssrRequest.setStrSessionID(sessionSignature);
			ssrRequest.setObjSSRAvailabilityRequest(getSSRAvailabilityRequest(tripInfo));
			listener.setType("5_GetSSR");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetSSRAvailabilityForBookingResponse ssrResponse = bookingStub.getSSRAvailabilityForBooking(ssrRequest);
			if (!checkIfSSRSuccess(ssrResponse)) {
				parseSSRtoTrip(ssrResponse, tripInfo);
				mergeBaggagePriceToFirstSegment(tripInfo);
			}

		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (Exception e) {
			log.error("Error Occured while fetching SSR for Trip {} ", tripInfo, e);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	// combine baggage fare for all segments in first segment
	private void mergeBaggagePriceToFirstSegment(TripInfo tripInfo) {
		try {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())) {
					if (segmentInfo.getSegmentNum() != 0
							&& segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
						AirAsiaUtils.mergeBaggageinFirstSegment(tripInfo.getSegmentInfos().get(0), segmentInfo);
					}
				}
			});
		} catch (Exception e) {
			log.error("SSR Merging failed for booking {} for trip {}  cause {}", bookingId, tripInfo.toString(), e);
		}
	}

	private SSRAvailabilityForBookingRequest getSSRAvailabilityRequest(TripInfo tripInfo) {
		SSRAvailabilityForBookingRequest ssrRequest = new SSRAvailabilityForBookingRequest();
		setCommonSSRRequest(ssrRequest);
		ssrRequest.setSegmentKeyList(getSegmentSSRKey(tripInfo));
		return ssrRequest;
	}

	private ArrayOfLegKey getSegmentSSRKey(TripInfo tripInfo) {
		ArrayOfLegKey legKey = new ArrayOfLegKey();
		LegKey segmentKey = null;
		for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
			SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = (segmentIndex + 1 < tripInfo.getSegmentInfos().size())
					? tripInfo.getSegmentInfos().get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			if (legNum == 0) {
				segmentKey = new LegKey();
				segmentKey.setState(MessageState.New);
				segmentKey.setDepartureStation(segmentInfo.getDepartAirportInfo().getCode());
				segmentKey.setDepartureDate(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
				segmentKey.setCarrierCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
				segmentKey.setFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber());
			}
			segmentKey.setArrivalStation(segmentInfo.getArrivalAirportInfo().getCode());
			if (nextSegmentLegNum == 0) {
				legKey.addLegKey(segmentKey);
			}
		}
		return legKey;
	}

	private boolean checkIfSSRSuccess(GetSSRAvailabilityForBookingResponse ssrResponse) {
		if (ssrResponse != null && ssrResponse.getGetSSRAvailabilityForBookingResult() != null
				&& StringUtils.isNotBlank(ssrResponse.getGetSSRAvailabilityForBookingResult().getExceptionMessage())) {
			String errorMsg = ssrResponse.getGetSSRAvailabilityForBookingResult().getExceptionMessage();
			log.error("SSR Search failed for bookingId {} and error {}", bookingId, errorMsg);
			return true;
		}
		return false;
	}

	private void parseSSRtoTrip(GetSSRAvailabilityForBookingResponse ssrResponse, TripInfo tripInfo) {
		try {
			if (Objects.nonNull(ssrResponse)
					&& Objects.nonNull(ssrResponse.getGetSSRAvailabilityForBookingResult().getSSRSegmentList())) {
				ArrayOfSSRSegment ssrSegmentList =
						ssrResponse.getGetSSRAvailabilityForBookingResult().getSSRSegmentList();
				for (SSRSegment ssrSegment : ssrSegmentList.getSSRSegment()) {
					LegKey legKey = ssrSegment.getLegKey();
					List<SegmentInfo> matchedSegmentInfos = AirAsiaUtils.getSegmentInfo(tripInfo.getSegmentInfos(),
							legKey.getDepartureStation(), legKey.getArrivalStation(), legKey.getCarrierCode(),
							legKey.getFlightNumber(), legKey.getDepartureDate());
					if (CollectionUtils.isNotEmpty(matchedSegmentInfos)) {
						for (AvailablePaxSSR paxSSR : ssrSegment.getAvailablePaxSSRList().getAvailablePaxSSR()) {
							if (isLegWiseSSR(paxSSR)) {
								for (SSRLeg ssrLeg : paxSSR.getSSRLegList().getSSRLeg()) {
									LegKey segmentLegKey = ssrLeg.getLegKey();
									List<SegmentInfo> segmentInfos = AirAsiaUtils.getSegmentInfo(matchedSegmentInfos,
											segmentLegKey.getDepartureStation(), segmentLegKey.getArrivalStation(),
											segmentLegKey.getCarrierCode(), segmentLegKey.getFlightNumber(),
											segmentLegKey.getDepartureDate());
									if (CollectionUtils.isNotEmpty(segmentInfos)) {
										SegmentInfo segmentInfo = segmentInfos.get(0);
										FlightBasicFact flightFact =
												AirAsiaUtils.getSSRFlightFact(segmentInfo, true, bookingUser);
										TimePeriod travelPeriod = new TimePeriod();
										travelPeriod
												.setEndTime(segmentInfos.get(segmentInfos.size() - 1).getArrivalTime());
										flightFact.setTravelPeriod(travelPeriod);
										BaseUtils.createFactOnUser(flightFact, bookingUser);
										Map<SSRType, List<? extends SSRInformation>> preSSR =
												AirUtils.getPreSSR(flightFact);
										setSSRInfoToSegmentInfo(segmentInfo, paxSSR, preSSR);
									}
								}
							} else {
								for (SegmentInfo segment : matchedSegmentInfos) {
									Map<SSRType, List<? extends SSRInformation>> preSSR = AirUtils
											.getPreSSR(AirAsiaUtils.getSSRFlightFact(segment, true, bookingUser));
									setSSRInfoToSegmentInfo(segment, paxSSR, preSSR);
								}
							}
						}
					}
				}
			}

		} catch (Exception e) {
			log.error("SSR Parsing failed for booking {} for trip {}  cause {}", bookingId, tripInfo.toString(), e);
		}
	}

	private void setSSRInfoToSegmentInfo(SegmentInfo segmentInfo, AvailablePaxSSR paxSSR,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {
		Map<SSRType, List<? extends SSRInformation>> ssrInfo = segmentInfo.getSsrInfo();
		Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
		if (MapUtils.isEmpty(ssrInfo)) {
			ssrInfo = new HashMap<SSRType, List<? extends SSRInformation>>();
		}
		SSRInformation baggageSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE), paxSSR.getSSRCode());
		SSRInformation mealSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.MEAL), paxSSR.getSSRCode());
		if (Objects.nonNull(baggageSSRInfo) && !airAsiaAirline.passThruLegCodes().contains(paxSSR.getSSRCode())) {
			// Baggage SSR
			BaggageSSRInformation ssrInformation = new BaggageSSRInformation();
			ssrInformation.setCode(paxSSR.getSSRCode());
			ssrInformation.setDesc(baggageSSRInfo.getDesc());
			Double amount = getTotalSSRAmount(paxSSR);
			Double passThruAmount = airAsiaAirline.getSSRAmount(segmentInfo, paxSSR.getSSRCode(), amount);
			ssrInformation.setAmount(passThruAmount);
			List<BaggageSSRInformation> baggageSSRList = (List<BaggageSSRInformation>) ssrInfo.get(SSRType.BAGGAGE);
			if (CollectionUtils.isEmpty(baggageSSRList)) {
				baggageSSRList = new ArrayList<BaggageSSRInformation>();
			}
			baggageSSRList.add(ssrInformation);
			ssrInfo.put(SSRType.BAGGAGE, baggageSSRList);
		} else if (Objects.nonNull(mealSSRInfo)) {
			// Meal SSR
			MealSSRInformation ssrInformation = new MealSSRInformation();
			ssrInformation.setAmount(getTotalSSRAmount(paxSSR));
			ssrInformation.setCode(paxSSR.getSSRCode());
			ssrInformation.setDesc(mealSSRInfo.getDesc());
			List<MealSSRInformation> mealSSRList = (List<MealSSRInformation>) ssrInfo.get(SSRType.MEAL);
			if (CollectionUtils.isEmpty(mealSSRList)) {
				mealSSRList = new ArrayList<MealSSRInformation>();
			}
			mealSSRList.add(ssrInformation);
			ssrInfo.put(SSRType.MEAL, mealSSRList);
		} else if (infantCount > 0 && paxSSR.getSSRCode().equalsIgnoreCase(PAX_TYPE_INFANT) && legNum == 0) {
			// Infant Fare
			PaxFare paxFare = new PaxFare();
			FareDetail infantFareDetail = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT, new FareDetail());
			if (MapUtils.isEmpty(infantFareDetail.getFareComponents())) {
				infantFare = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT, new FareDetail());
				paxFare.setPaxType(PAX_TYPE_INFANT);
				for (PaxSSRPrice ssrPrice : paxSSR.getPaxSSRPriceList().getPaxSSRPrice()) {
					infantFare = setPaxWiseFareBreakUp(infantFare, paxFare, 1,
							ssrPrice.getPaxFee().getServiceCharges().getBookingServiceCharge());
				}
				infantFareDetail.setFareComponents(infantFare.getFareComponents());
			}
		}
		segmentInfo.setSsrInfo(ssrInfo);
	}


	private boolean isLegWiseSSR(AvailablePaxSSR paxSSR) {
		if (paxSSR.getSSRLegList() != null && ArrayUtils.isNotEmpty(paxSSR.getSSRLegList().getSSRLeg())
				&& paxSSR.getSSRLegList().getSSRLeg().length > 1) {
			return true;
		}
		return false;
	}

	public Double getTotalSSRAmount(AvailablePaxSSR paxSSR) {
		double amount = 0;
		if (paxSSR != null && paxSSR.getPaxSSRPriceList() != null
				&& Objects.nonNull(paxSSR.getPaxSSRPriceList().getPaxSSRPrice())) {
			for (PaxSSRPrice price : paxSSR.getPaxSSRPriceList().getPaxSSRPrice()) {
				for (BookingServiceCharge serviceCharge : price.getPaxFee().getServiceCharges()
						.getBookingServiceCharge()) {
					if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
						if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
								&& serviceCharge.getAmount().doubleValue() < 0) {
							serviceCharge.setAmount(serviceCharge.getAmount().abs());
						}
						amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
					}
				}
			}
		}
		return amount;
	}

	public void setSeatMap(List<SegmentInfo> segmentInfos, TripSeatMap tripSeatMap) {
		try {
			GetSeatAvailability seatAvailabilityRequest = new GetSeatAvailability();
			seatAvailabilityRequest.setStrSessionID(sessionSignature);
			seatAvailabilityRequest.setObjSeatAvailabilityRequest(getSeatAvailabilityParam(segmentInfos));
			listener.setType("R-SEATMAP");
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetSeatAvailabilityResponse response = bookingStub.getSeatAvailability(seatAvailabilityRequest);
			parseSeatMapInfo(response, tripSeatMap, segmentInfos);
		} catch (Exception e) {
			log.error("Seat Segment Failed for booking id {} and segment {}", bookingId, segmentInfos.toString(), e);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private SeatAvailabilityRequest getSeatAvailabilityParam(List<SegmentInfo> segmentInfos) {
		SeatAvailabilityRequest request = new SeatAvailabilityRequest();
		request.setDepartureStation(segmentInfos.get(0).getDepartureAirportCode());
		request.setArrivalStation(segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportCode());
		request.setCarrierCode(segmentInfos.get(0).getAirlineCode(false));
		request.setCollectedCurrencyCode(getCurrencyCode());
		request.setIncludeSeatFees(true);
		request.setSeatAssignmentMode(SeatAssignmentMode.PreSeatAssignment);
		request.setSeatGroup((short) 0);
		request.setSTD(TgsDateUtils.getCalendar(segmentInfos.get(0).getDepartTime().toLocalDate()));
		request.setFlightNumber(StringUtils.leftPad(segmentInfos.get(0).getFlightNumber(), 4, " "));
		request.setCompressProperties(false);
		request.setOpSuffix(StringUtils.EMPTY);
		request.setExcludeEquipmentConfiguration(false);
		request.setEnforceSeatGroupRestrictions(false);
		return request;
	}

	private void parseSeatMapInfo(GetSeatAvailabilityResponse response, TripSeatMap tripSeatMap,
			List<SegmentInfo> segmentInfos) {
		// get Total Seats
		ArrayOfEquipmentInfo equipmentInfo = response.getGetSeatAvailabilityResult().getEquipmentInfos();
		List<SeatInfo> seatsBySegments = new ArrayList<>();
		if (equipmentInfo != null && ArrayUtils.isNotEmpty(equipmentInfo.getEquipmentInfo())) {
			Arrays.stream(equipmentInfo.getEquipmentInfo()).forEach(equipmentInfo1 -> {
				Arrays.stream(equipmentInfo1.getCompartments().getCompartmentInfo()).forEach(compartmentInfo -> {
					if (compartmentInfo.getSeats() != null && compartmentInfo.getSeats().getSeatInfo() != null) {
						seatsBySegments
								.addAll(new ArrayList<>(Arrays.asList(compartmentInfo.getSeats().getSeatInfo())));
					}
				});
			});
		}
		// get Seat Group fare
		Map<String, SeatGroupPassengerFee> seatGroupPassengerFee = new HashMap<>();
		ArrayOfSeatGroupPassengerFee seatFeeList = response.getGetSeatAvailabilityResult().getSeatGroupPassengerFees();
		if (seatFeeList != null && ArrayUtils.isNotEmpty(seatFeeList.getSeatGroupPassengerFee())) {
			Arrays.stream(seatFeeList.getSeatGroupPassengerFee()).forEach(seatGroupFee -> {
				seatGroupPassengerFee.put(String.valueOf(seatGroupFee.getSeatGroup()), seatGroupFee);
			});
		}

		ArrayOfSeatAvailabilityLeg legs = response.getGetSeatAvailabilityResult().getLegs();
		if (legs != null && ArrayUtils.isNotEmpty(legs.getSeatAvailabilityLeg())) {
			for (int legIndex = 0; legIndex < legs.getSeatAvailabilityLeg().length; legIndex++) {
				List<SeatSSRInformation> seatInfos = new ArrayList<>();
				SeatAvailabilityLeg leg = legs.getSeatAvailabilityLeg()[legIndex];
				List<SegmentInfo> matchedSegments = AirAsiaUtils.getSegmentInfo(segmentInfos, leg.getDepartureStation(),
						leg.getArrivalStation(), leg.getFlightDesignator().getCarrierCode(),
						leg.getFlightDesignator().getFlightNumber(), leg.getSTD());
				if (CollectionUtils.isNotEmpty(matchedSegments)) {
					SegmentInfo matchedSegment = matchedSegments.get(0);
					if (legIndex < seatsBySegments.size()) {
						List<SeatInfo> seatSegment = seatsBySegments;
						seatSegment.forEach(seat -> {
							// map Seat with Seat group to get fare
							try {
								if (!seat.getSeatDesignator().contains("$")) {
									SeatSSRInformation seatSSRInformation = new SeatSSRInformation();
									seatSSRInformation.setSeatNo(seat.getSeatDesignator().trim());
									ArrayOfEquipmentProperty seatProperty = seat.getPropertyList();
									if (seatGroupPassengerFee.getOrDefault(Short.toString(seat.getSeatGroup()),
											null) != null) {
										BookingServiceCharge[] bookingCharge = seatGroupPassengerFee
												.get(Short.toString(seat.getSeatGroup())).getPassengerFee()
												.getServiceCharges().getBookingServiceCharge();
										seatSSRInformation.setAmount(getSeatSSRAmount(bookingCharge));
										seatSSRInformation.setCode(seat.getSeatDesignator().trim());
										seatSSRInformation.setIsBooked(!getSeatAvailable(seat));
										seatInfos.add(seatSSRInformation);
									}
									if (seatProperty != null) {
										EquipmentProperty[] properties = seatProperty.getEquipmentProperty();
										Arrays.stream(properties).forEach(property -> {
											String seatType = property.getTypeCode();
											String seatTypeValue = property.getValue();
											if ("AISLE".equalsIgnoreCase(seatType)) {
												seatSSRInformation.setIsAisle(Boolean.valueOf(seatTypeValue));
											} else if ("LEGROOM".equalsIgnoreCase(seatType)) {
												seatSSRInformation.setIsLegroom(Boolean.valueOf(seatTypeValue));
											}
										});
									}
								}
							} catch (Exception e) {
								log.error("Error in Seat map response for bookingid {} and seat {} ", bookingId, seat);
							}
						});
					}

					if (CollectionUtils.isNotEmpty(seatInfos)) {
						SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatInfos).build();
						tripSeatMap.getTripSeat().put(matchedSegment.getId(), seatInformation);
					}
				}
			}
		}
	}

	public double getSeatSSRAmount(BookingServiceCharge[] bookingServiceCharges) {
		double amount = 0;
		for (BookingServiceCharge serviceCharge : bookingServiceCharges) {
			if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
				if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
						&& serviceCharge.getAmount().doubleValue() < 0) {
					serviceCharge.setAmount(serviceCharge.getAmount().abs());
				}
				amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
			}
		}
		return amount;
	}

	private Boolean getSeatAvailable(SeatInfo seat) {
		if (seat.getSeatAvailability().equals(SeatAvailability.Open)) {
			return true;
		}
		return false;
	}
}

