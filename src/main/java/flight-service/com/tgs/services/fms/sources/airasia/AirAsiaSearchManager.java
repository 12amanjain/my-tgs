package com.tgs.services.fms.sources.airasia;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import org.tempuri.ArrayOfAvailabilityRequest;
import org.tempuri.ArrayOfFare;
import org.tempuri.ArrayOfJourney;
import org.tempuri.ArrayOfJourneySortKey;
import org.tempuri.ArrayOfPaxSSR;
import org.tempuri.ArrayOfPriceJourney;
import org.tempuri.ArrayOfPriceSegment;
import org.tempuri.ArrayOfSegment;
import org.tempuri.ArrayOfSegmentSSRRequest;
import org.tempuri.AvailabilityFilter;
import org.tempuri.AvailabilityRequest;
import org.tempuri.AvailabilityResponse;
import org.tempuri.AvailabilityType;
import org.tempuri.Booking;
import org.tempuri.BookingServiceCharge;
import org.tempuri.DOW;
import org.tempuri.Fare;
import org.tempuri.FareClassControl;
import org.tempuri.FlightDesignator;
import org.tempuri.FlightType;
import org.tempuri.GetAvailability;
import org.tempuri.GetAvailabilityResponse;
import org.tempuri.GetAvailabilityWithTaxes;
import org.tempuri.GetAvailabilityWithTaxesResponse;
import org.tempuri.GetItineraryPrice;
import org.tempuri.GetItineraryPriceResponse;
import org.tempuri.ItineraryPriceRequest;
import org.tempuri.Journey;
import org.tempuri.JourneyDateMarket;
import org.tempuri.JourneySortKey;
import org.tempuri.Leg;
import org.tempuri.MessageState;
import org.tempuri.PassengerFee;
import org.tempuri.PaxFare;
import org.tempuri.PaxSSR;
import org.tempuri.PointOfSale;
import org.tempuri.PriceItineraryBy;
import org.tempuri.PriceJourney;
import org.tempuri.PriceJourneyRequestData;
import org.tempuri.PriceSegment;
import org.tempuri.SSRRequest;
import org.tempuri.Segment;
import org.tempuri.SegmentSSRRequest;
import org.tempuri.TripAvailabilityRequest;
import org.tempuri.TypeOfSale;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfString;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SupplierFlowType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class AirAsiaSearchManager extends AirAsiaServiceManager {

	protected AirSearchResult searchResult;

	protected static final String INVALID_ROUTE = "invalid route";

	public AirSearchResult doSearch(boolean isSearch) {
		try {
			searchResult = new AirSearchResult();
			if (isAvailabilityWithTax()) {
				searchResult = new AirSearchResult();
				GetAvailabilityWithTaxes availabilityRq = buildAvailabilityRequest();
				listener.setType(AirUtils.getLogType("2_GetAvailabilityWithTax", configuration));
				searchStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				GetAvailabilityWithTaxesResponse availabilityResponse =
						searchStub.getAvailabilityWithTaxes(availabilityRq);
				if (!checkIfExpections(availabilityResponse)) {
					searchResult = parseTripResponseWithTax(availabilityResponse.getGetAvailabilityWithTaxesResult());
				}
			} else {
				GetAvailability availabilityRq = buildAvailabilityRequest(isSearch);
				listener.setType(AirUtils.getLogType("2_GetAvailability", configuration));
				searchStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				GetAvailabilityResponse availabilityResponse = searchStub.getAvailability(availabilityRq);
				if (!checkIfExpections(availabilityResponse)) {
					searchResult = parseTripResponse(availabilityResponse);
				}
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			searchStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return searchResult;
	}

	public boolean isAvailabilityWithTax() {
		return sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsSearchWithTax());
	}

	private boolean checkIfExpections(GetAvailabilityResponse availabilityResponse) {
		if (availabilityResponse != null && availabilityResponse.getGetAvailabilityResult() != null
				&& StringUtils.isNotBlank(availabilityResponse.getGetAvailabilityResult().getExceptionMessage())) {
			String errorMessage = availabilityResponse.getGetAvailabilityResult().getExceptionMessage();
			if (errorMessage.toLowerCase().contains(SESSION_NOT_FOUND)) {
				throw new SupplierSessionException(errorMessage);
			}
			if (errorMessage.toLowerCase().contains(INVALID_ROUTE)) {
				throw new NotOperatingSectorException(errorMessage);
			}
			throw new NoSearchResultException(errorMessage);
		}
		return false;
	}

	private GetAvailability buildAvailabilityRequest(boolean isSearch) {
		GetAvailability availability = new GetAvailability();
		availability.setStrSessionID(sessionSignature);
		availability.setObjTripAvailabilityRequest(buildTripAvailabilityRq());
		return availability;
	}

	private TripAvailabilityRequest buildTripAvailabilityRq() {
		TripAvailabilityRequest availabilityRequest = new TripAvailabilityRequest();
		ArrayOfAvailabilityRequest availabilityRequests = new ArrayOfAvailabilityRequest();
		searchQuery.getRouteInfos().forEach(routeInfo -> {
			AvailabilityRequest request = new AvailabilityRequest();
			request.setDepartureStation(routeInfo.getFromCityAirportCode());
			request.setArrivalStation(routeInfo.getToCityAirportCode());
			request.setBeginDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
			request.setEndDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
			request.setFlightType(FlightType.All);
			request.setPaxCount((short) AirUtils.getPaxCount(searchQuery, false));
			request.setDow(DOW.Daily);
			request.setCurrencyCode(getCurrencyCode());
			request.setAvailabilityType(AvailabilityType.Default);
			request.setMaximumConnectingFlights((short) 20);
			request.setAvailabilityFilter(AvailabilityFilter.ExcludeUnavailable);
			request.setFareClassControl(getFareClassControl());
			request.setNightsStay(0);
			request.setIncludeAllotments(true);
			request.setFareTypes(getFareTypes());
			request.setProductClasses(getProductClasses());
			request.setJourneySortKeys(getFareFilter());
			request.setIncludeTaxesAndFees(true);
			request.setPaxPriceTypes(getPaxPriceType(true));
			request.setPromotionCode(getPromoCode(searchQuery));
			request.setSourceOrganization(configuration.getSupplierCredential().getOrganisationCode());
			availabilityRequests.addAvailabilityRequest(request);
		});
		availabilityRequest.setAvailabilityRequests(availabilityRequests);
		return availabilityRequest;
	}

	public FareClassControl getFareClassControl() {
		FareClassControl classControl = FareClassControl.LowestFareClass;
		if (configuration != null && configuration.getSupplierAdditionalInfo() != null
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareClass())) {
			String fareClass = configuration.getSupplierAdditionalInfo().getFareClass().toUpperCase();
			if (FareClassControl._CompressByProductClass.toUpperCase().equals(fareClass)) {
				classControl = FareClassControl.CompressByProductClass;
			} else if (FareClassControl._Default.equals(fareClass)) {
				classControl = FareClassControl.Default;
			} else {
				classControl = FareClassControl.LowestFareClass;
			}
		}
		return classControl;
	}

	private ArrayOfJourneySortKey getFareFilter() {
		ArrayOfJourneySortKey jKey = new ArrayOfJourneySortKey();
		jKey.addJourneySortKey(JourneySortKey.LowestFare);
		return jKey;
	}

	private ArrayOfString getFareTypes() {
		ArrayOfString arrString = new ArrayOfString();
		if (Objects.nonNull(configuration.getSupplierAdditionalInfo().getFareTypes())
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareTypes(searchQuery)))
			arrString.setString(configuration.getSupplierAdditionalInfo().getFareTypes(searchQuery).split(","));
		return arrString;
	}

	private ArrayOfString getProductClasses() {
		ArrayOfString arrString = new ArrayOfString();
		if (Objects.nonNull(configuration.getSupplierAdditionalInfo().getProductClasses())
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getProductClasses(searchQuery)))
			arrString.setString(configuration.getSupplierAdditionalInfo().getProductClasses(searchQuery).split(","));
		return arrString;
	}

	private AirSearchResult parseTripResponse(GetAvailabilityResponse availabilityResponse) {
		AvailabilityResponse tripAvailabilityResponse = availabilityResponse.getGetAvailabilityResult();
		if (Objects.nonNull(tripAvailabilityResponse.getSchedule())
				&& ArrayUtils.isNotEmpty(tripAvailabilityResponse.getSchedule().getJourneyDateMarket())) {
			JourneyDateMarket[] journeyMarket = tripAvailabilityResponse.getSchedule().getJourneyDateMarket();
			AtomicInteger journeyCount = new AtomicInteger(0);
			for (JourneyDateMarket journeyDateMarket : journeyMarket) {
				List<TripInfo> tripInfos = new ArrayList<>();
				if (journeyDateMarket.getJourneys() != null) {
					ArrayOfJourney arrayOfJourney = journeyDateMarket.getJourneys();
					Arrays.stream(arrayOfJourney.getJourney()).forEach(journey -> {
						String journeySellKey = journey.getJourneySellKey();
						TripInfo tripInfo = new TripInfo();
						List<SegmentInfo> segmentInfos =
								getSegmentInfos(journey.getSegments(), journeySellKey, journeyCount.intValue());
						if (CollectionUtils.isNotEmpty(segmentInfos) && checkIfPriceInfoSizeEquals(segmentInfos)) {
							tripInfo.setSegmentInfos(segmentInfos);
							tripInfo.setConnectionTime();
							tripInfos.add(tripInfo);
						}
					});
				}
				if (ArrayUtils.isNotEmpty(journeyMarket) && journeyMarket[journeyCount.intValue()] != null
						&& Objects.nonNull(journeyMarket[journeyCount.intValue()].getJourneys())
						&& ArrayUtils.isNotEmpty(journeyMarket[journeyCount.intValue()].getJourneys().getJourney())) {
					searchResult = AirAsiaUtils.segregateResult(searchQuery, journeyMarket[journeyCount.intValue()],
							tripInfos, searchResult, bookingUser);
				}
				journeyCount.getAndIncrement();
			}
		} else {
			throw new NoSearchResultException(AirSourceConstants.NO_SCHEDULE_FOUND);
		}
		return searchResult;
	}

	private AirSearchResult parseTripResponseWithTax(AvailabilityResponse tripAvailabilityResponse) {
		if (Objects.nonNull(tripAvailabilityResponse.getSchedule())
				&& ArrayUtils.isNotEmpty(tripAvailabilityResponse.getSchedule().getJourneyDateMarket())) {
			JourneyDateMarket[] journeyMarket = tripAvailabilityResponse.getSchedule().getJourneyDateMarket();
			AtomicInteger journeyCount = new AtomicInteger(0);
			for (JourneyDateMarket journeyDateMarket : journeyMarket) {
				List<TripInfo> tripInfos = new ArrayList<>();
				if (journeyDateMarket.getJourneys() != null) {
					ArrayOfJourney arrayOfJourney = journeyDateMarket.getJourneys();
					Arrays.stream(arrayOfJourney.getJourney()).forEach(journey -> {
						String journeySellKey = journey.getJourneySellKey();
						TripInfo tripInfo = new TripInfo();
						List<SegmentInfo> segmentInfos =
								getSegmentInfos(journey.getSegments(), journeySellKey, journeyCount.intValue());

						if (CollectionUtils.isNotEmpty(segmentInfos)) {
							tripInfo.setSegmentInfos(segmentInfos);
							tripInfo.setConnectionTime();
							tripInfos.add(tripInfo);
						}
					});
				}
				if (ArrayUtils.isNotEmpty(journeyMarket) && journeyMarket[journeyCount.intValue()] != null
						&& Objects.nonNull(journeyMarket[journeyCount.intValue()].getJourneys())
						&& ArrayUtils.isNotEmpty(journeyMarket[journeyCount.intValue()].getJourneys().getJourney())) {
					searchResult = AirAsiaUtils.segregateResult(searchQuery, journeyMarket[journeyCount.intValue()],
							tripInfos, searchResult, bookingUser);
				}
				journeyCount.getAndIncrement();
			}
		} else {
			throw new NoSearchResultException(AirSourceConstants.NO_SCHEDULE_FOUND);
		}
		return searchResult;
	}


	private boolean checkIfPriceInfoSizeEquals(List<SegmentInfo> segmentInfos) {
		int priceInfosize = segmentInfos.get(0).getPriceInfoList().size();
		if (priceInfosize == 0) {
			return false;
		}
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getPriceInfoList().size() != priceInfosize) {
				return false;
			}
			for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
				int fdSize = priceInfo.getFareDetails().size();
				if (priceInfo.getFareDetails().containsKey(PaxType.INFANT)) {
					// due to default fare detial map , infant can be added
					fdSize = fdSize - 1;
				}
				if (AirAsiaUtils.getPaxTypeCount(searchQuery) != fdSize) {
					return false;
				}
			}
		}
		return true;
	}

	private List<SegmentInfo> getSegmentInfos(ArrayOfSegment journey, String journeyKey, Integer journeyCount) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		try {
			if (journey != null && ArrayUtils.isNotEmpty(journey.getSegment())) {
				AtomicInteger segmentIndex = new AtomicInteger(0);

				for (Segment segment : journey.getSegment()) {
					Leg[] legs = segment.getLegs().getLeg();
					ArrayOfFare arrayOfFare = segment.getFares();
					List<SegmentInfo> segmentInfos1 =
							parseSegmentInfoFromLeg(legs, journeyCount, arrayOfFare, journeyKey, segmentIndex);
					segmentInfos.addAll(segmentInfos1);
				}
			}
		} catch (Exception e) {
			segmentInfos = null;
			log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
		}
		return segmentInfos;
	}

	public List<PriceInfo> getPriceInfos(ArrayOfFare arrayOfFare, String journeyKey, Integer legIndex) {
		List<PriceInfo> priceInfos = new ArrayList<>();
		if (arrayOfFare != null && ArrayUtils.isNotEmpty(arrayOfFare.getFare())) {
			for (Fare fare : arrayOfFare.getFare()) {
				int seatCount = fare.getAvailableCount();
				if (seatCount > 0 && AirUtils.getPaxCount(searchQuery, false) <= seatCount) {
					PriceInfo priceInfo = PriceInfo.builder().build();
					priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
					airAsiaAirline.processPriceInfo(fare.getProductClass(), priceInfo, configuration);
					PriceMiscInfo miscInfo = PriceMiscInfo.builder().build();
					miscInfo.setFareKey(fare.getFareSellKey());
					miscInfo.setFareClassOfService(fare.getFareClassOfService());
					miscInfo.setClassOfService(fare.getClassOfService());
					miscInfo.setRuleNumber(fare.getRuleNumber());
					short sequence = fare.getFareSequence();
					miscInfo.setFareSequence(Integer.valueOf(sequence).toString());
					miscInfo.setFareApplicationName(fare.getFareApplicationType().getValue());
					miscInfo.setPlatingCarrier(AirlineHelper.getAirlineInfo(fare.getCarrierCode()));
					miscInfo.setLegNum(legIndex);
					priceInfo.setMiscInfo(PriceMiscInfo.builder().fareKey(fare.getFareSellKey()).build());
					Map<PaxType, FareDetail> fareDetailMap = getPaxWiseFareDetail(fare, fare.getAvailableCount());
					if (legIndex.intValue() == 0) {
						// Set price in First Leg
						priceInfo.setFareDetails(fareDetailMap);
					} else {
						fareDetailMap.forEach(((paxType, fareDetail) -> {
							fareDetail.setFareComponents(new HashMap<>());
						}));
						priceInfo.setFareDetails(fareDetailMap);
					}
					miscInfo.setJourneyKey(journeyKey);
					priceInfo.setMiscInfo(miscInfo);
					priceInfos.add(priceInfo);
				}
			}
		}
		return priceInfos;
	}

	private int getPaxArraySize(PaxFare[] paxFareArray) {
		Set<String> paxTypeSet = new HashSet<String>();
		for (PaxFare paxFare : paxFareArray) {
			paxTypeSet.add(paxFare.getPaxType());
		}
		return paxTypeSet.size();
	}

	private List<SegmentInfo> parseSegmentInfoFromLeg(Leg[] legs, Integer journeyCount, ArrayOfFare arrayOfFare,
			String journeyKey, AtomicInteger segmentIndex) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		Integer legIndex = 0;
		for (Leg leg : legs) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentIndex.intValue());
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(leg.getDepartureStation()));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(leg.getArrivalStation()));
			segmentInfo.setDepartTime(TgsDateUtils.toLocalDateTime(leg.getSTD()));
			segmentInfo.setArrivalTime(TgsDateUtils.toLocalDateTime(leg.getSTA()));
			segmentInfo.setFlightDesignator(getFlightDesignator(leg));
			segmentInfo.setDuration(segmentInfo.calculateDuration());
			segmentInfo.setIsReturnSegment(Boolean.FALSE);
			if (leg.getLegInfo() != null && StringUtils.isNotBlank(leg.getLegInfo().getOperatingCarrier())) {
				AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(leg.getLegInfo().getOperatingCarrier());
				segmentInfo.setOperatedByAirlineInfo(operatingAirline);
			}
			if (journeyCount.intValue() == 1) {
				segmentInfo.setIsReturnSegment(Boolean.TRUE);
			}
			AirAsiaUtils.setTerminal(leg, segmentInfo);
			List<PriceInfo> priceInfos = getPriceInfos(arrayOfFare, journeyKey, legIndex);
			segmentInfo.setPriceInfoList(priceInfos);
			segmentInfos.add(segmentInfo);
			segmentIndex.getAndIncrement();
			legIndex++;
		}
		return segmentInfos;
	}

	private Map<PaxType, FareDetail> getPaxWiseFareDetail(Fare fare, int seatCount) {
		Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
		if (fare != null && fare.getPaxFares() != null && ArrayUtils.isNotEmpty(fare.getPaxFares().getPaxFare())) {
			PaxFare[] paxFares = fare.getPaxFares().getPaxFare();
			if (ArrayUtils.isNotEmpty(paxFares)) {
				Arrays.stream(paxFares).forEach(paxFare -> {
					FareDetail fareDetail = new FareDetail();
					String paxType = paxFare.getPaxType();
					if (paxType.equalsIgnoreCase(PAX_TYPE_CHILD)) {
						paxType = PaxType.CHILD.getType();
					}
					fareDetail.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
					fareDetail.setClassOfBooking(fare.getProductClass());
					fareDetail.setSeatRemaining(seatCount);
					fareDetail.setFareBasis(fare.getFareBasisCode());
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					BookingServiceCharge[] serviceCharges = paxFare.getServiceCharges().getBookingServiceCharge();
					setPaxWiseFareBreakUpV4(fareDetail, paxFare,
							searchQuery.getPaxInfo().getOrDefault(PaxType.getPaxType(paxType), 1), serviceCharges);
					fareDetailMap.put(PaxType.getPaxType(paxType), fareDetail);
				});
			} else {
				fareDetailMap.putAll(getDefaultFareDetail(fare, seatCount));
			}
		} else {
			fareDetailMap.putAll(getDefaultFareDetail(fare, seatCount));
		}
		return fareDetailMap;
	}

	public List<TripInfo> getItineraryPrice(List<Pair<TripInfo, Integer>> flightList, boolean isSearch) {
		GetItineraryPrice priceItineraryRq = null;
		GetItineraryPriceResponse priceResponse = null;
		List<TripInfo> tripList = new ArrayList<>();
		try {
			priceItineraryRq = new GetItineraryPrice();
			ItineraryPriceRequest itineraryPriceRequest = new ItineraryPriceRequest();
			itineraryPriceRequest.setPriceItineraryBy(PriceItineraryBy.JourneyWithLegs);
			TypeOfSale typeOfSale = getTypeOfSale();
			itineraryPriceRequest.setTypeOfSale(typeOfSale);
			itineraryPriceRequest.setSSRRequest(getSSRRequest(flightList));
			itineraryPriceRequest.setPriceJourneyWithLegsRequest(getPriceJourneyLegs(flightList, true, isSearch));
			priceItineraryRq.setObjItineraryPriceRequest(itineraryPriceRequest);
			priceItineraryRq.setStrSessionID(sessionSignature);
			listener.setType("3_GetItineraryPrice");
			if (!searchStub._getServiceClient().getAxisService()
					.hasMessageContextListener(SoapRequestResponseListner.class)) {
				searchStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			}
			priceResponse = searchStub.getItineraryPrice(priceItineraryRq);
			if (!checkIfPriceResponseSuccess(priceResponse)) {
				tripList = parseTripPriceResponse(priceResponse, flightList, searchQuery, bookingId, typeOfSale);
			}
		} catch (Exception e) {
			log.error("Error occured on Itinerary Price for search {} supplier {} booking {} ",
					searchQuery.getSearchId(), getSupplierDesc(), bookingId, e);
		} finally {
			searchStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return tripList;
	}

	private SSRRequest getSSRRequest(List<Pair<TripInfo, Integer>> flightList) {
		SSRRequest ssrRequest = null;
		if (BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromItineraryPrice())
				&& searchQuery.getPaxInfo().get(PaxType.INFANT) > 0) {
			ssrRequest = new SSRRequest();
			ArrayOfSegmentSSRRequest segmentSSRRequests = new ArrayOfSegmentSSRRequest();
			for (Pair<TripInfo, Integer> flight : flightList) {
				TripInfo tripInfo = flight.getFirst();
				for (int i = 0; i < tripInfo.getSegmentInfos().size();) {
					List<SegmentInfo> subSegments = getSubLegSegments(tripInfo.getSegmentInfos(), i);
					SegmentSSRRequest segmentSSRRequest = new SegmentSSRRequest();
					FlightDesignator flightDesig = new FlightDesignator();
					flightDesig.setState(MessageState.New);
					flightDesig.setFlightNumber(subSegments.get(0).getFlightNumber());
					flightDesig.setCarrierCode(subSegments.get(0).getFlightDesignator().getAirlineInfo().getCode());
					segmentSSRRequest.setFlightDesignator(flightDesig);
					segmentSSRRequest.setDepartureStation(subSegments.get(0).getDepartureAirportCode());
					segmentSSRRequest
							.setArrivalStation(subSegments.get(subSegments.size() - 1).getArrivalAirportCode());
					segmentSSRRequest
							.setSTD(TgsDateUtils.getCalendarWithoutTimeZone(subSegments.get(0).getDepartTime()));
					ArrayOfPaxSSR paxSSRs = new ArrayOfPaxSSR();
					for (Integer paxIndex = 0; paxIndex < searchQuery.getPaxInfo().get(PaxType.INFANT); paxIndex++) {
						PaxSSR paxSSR = new PaxSSR();
						paxSSR.setState(MessageState.New);
						paxSSR.setActionStatusCode("NN");
						paxSSR.setDepartureStation(subSegments.get(0).getDepartureAirportCode());
						paxSSR.setArrivalStation(subSegments.get(subSegments.size() - 1).getArrivalAirportCode());
						paxSSR.setPassengerNumber(paxIndex.shortValue());
						paxSSR.setSSRCode(PAX_TYPE_INFANT);
						paxSSRs.addPaxSSR(paxSSR);
					}
					segmentSSRRequest.setPaxSSRs(paxSSRs);
					segmentSSRRequests.addSegmentSSRRequest(segmentSSRRequest);
					i += subSegments.size();
				}
			}
			ssrRequest.setSegmentSSRRequests(segmentSSRRequests);
		}
		return ssrRequest;
	}

	private boolean checkIfPriceResponseSuccess(GetItineraryPriceResponse priceResponse) {
		if (priceResponse != null && priceResponse.getGetItineraryPriceResult() != null
				&& StringUtils.isNotBlank(priceResponse.getGetItineraryPriceResult().getExceptionMessage())) {
			String errorMsg = priceResponse.getGetItineraryPriceResult().getExceptionMessage();
			throw new NoSeatAvailableException(errorMsg);
		}
		return false;
	}

	private PriceJourneyRequestData getPriceJourneyLegs(List<Pair<TripInfo, Integer>> flightList, boolean isPaxPrice,
			boolean isSearch) {
		PriceJourneyRequestData journeyRequestData = new PriceJourneyRequestData();
		journeyRequestData.setCurrencyCode(getCurrencyCode());
		journeyRequestData.setPaxCount((short) AirUtils.getPaxCount(searchQuery, false));
		journeyRequestData.setPassengers(getPassengers());
		journeyRequestData.setPriceJourneys(getPriceJourneys(flightList));
		// journeyRequestData.setSourceBookingPOS(getSourceBookingPOS());
		return journeyRequestData;
	}

	private ArrayOfPriceJourney getPriceJourneys(List<Pair<TripInfo, Integer>> flightList) {
		ArrayOfPriceJourney priceJourney = new ArrayOfPriceJourney();
		flightList.forEach(pair -> {
			TripInfo trip = pair.getFirst();
			Integer priceIndex = pair.getSecond();
			trip.splitTripInfo(false).forEach(tripJourney -> {
				PriceJourney journey = new PriceJourney();
				journey.setState(MessageState.New);
				journey.setSegments(getPriceSegments(tripJourney, priceIndex));
				priceJourney.addPriceJourney(journey);
			});
		});
		return priceJourney;
	}

	private ArrayOfPriceSegment getPriceSegments(TripInfo tripInfo, Integer priceIndex) {
		ArrayOfPriceSegment priceSegment = new ArrayOfPriceSegment();
		PriceSegment segment = null;
		for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
			SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = AirAsiaUtils.getNextLegNum(tripInfo.getSegmentInfos(), segmentIndex);
			if (legNum == 0) {
				segment = new PriceSegment();
				segment.setDepartureStation(segmentInfo.getDepartureAirportCode());
				segment.setSTD(TgsDateUtils.getCalendarWithoutTimeZone(segmentInfo.getDepartTime()));
				segment.setActionStatusCode("NN");
				segment.setFlightDesignator(getFlightDesignator(segmentInfo));
				segment.setFare(getSellFare(segmentInfo, priceIndex));
			}
			segment.setArrivalStation(segmentInfo.getArrivalAirportCode());
			segment.setSTA(TgsDateUtils.getCalendarWithoutTimeZone(segmentInfo.getArrivalTime()));
			if (nextSegmentLegNum == 0) {
				priceSegment.addPriceSegment(segment);
			}
		}
		return priceSegment;
	}

	private List<TripInfo> parseTripPriceResponse(GetItineraryPriceResponse priceItineraryResponse,
			List<Pair<TripInfo, Integer>> flightList, AirSearchQuery searchQuery, String bookingId,
			TypeOfSale typeOfSale) {

		Booking booking = priceItineraryResponse.getGetItineraryPriceResult().getBooking();
		ArrayOfJourney journey = booking.getJourneys();

		if (journey != null && ArrayUtils.isNotEmpty(journey.getJourney())) {
			for (Journey journeys : booking.getJourneys().getJourney()) {
				Pair<TripInfo, Integer> tripPair = AirAsiaUtils.findTripInfo(flightList, journeys);
				if (tripPair != null) {
					TripInfo tripInfo = tripPair.getFirst();
					Integer priceIndex = tripPair.getSecond();
					for (Segment segment : journeys.getSegments().getSegment()) {
						try {
							List<SegmentInfo> matchedSegments = AirAsiaUtils.getSegmentInfo(tripInfo.getSegmentInfos(),
									segment.getDepartureStation(), segment.getArrivalStation(),
									segment.getFlightDesignator().getCarrierCode(),
									segment.getFlightDesignator().getFlightNumber(), segment.getSTD());
							PassengerFee infantFee = AirAsiaUtils.getInfantFee(booking.getPassengers(),
									StringUtils.join(segment.getFlightDesignator().getCarrierCode(),
											segment.getFlightDesignator().getFlightNumber()));
							if (CollectionUtils.isNotEmpty(matchedSegments)) {
								AirAsiaUtils.initializeInfantFareDetail(matchedSegments, searchQuery);
								SegmentInfo segmentInfo = matchedSegments.get(0);
								for (Fare fare : segment.getFares().getFare()) {
									PriceInfo priceInfo = segmentInfo.getPriceInfo(priceIndex);
									if (Objects.nonNull(priceInfo) && Objects.nonNull(fare)
											&& Objects.nonNull(fare.getPaxFares())) {
										setNewFareComponent(priceInfo, fare.getPaxFares().getPaxFare(), searchQuery,
												infantFee);
										short sequence = fare.getFareSequence();
										priceInfo.getMiscInfo().setFareSequence(Integer.valueOf(sequence).toString());
										priceInfo.getMiscInfo().setFareClassOfService(fare.getFareClassOfService());
										priceInfo.getMiscInfo().setRuleNumber(fare.getRuleNumber());
									}
									if (isPromoFare.get() && typeOfSale != null
											&& StringUtils.isNotEmpty(typeOfSale.getPromotionCode())) {
										priceInfo.getMiscInfo().setAccountCode(typeOfSale.getPromotionCode());
									}
								}
							}
						} catch (Exception e) {
							log.error("Fare Parsing failed for booking Id {} trip {} excep ", bookingId,
									tripInfo.toString(), e);
						}
					}
				}
			}
		}
		List<TripInfo> tripList = AirAsiaUtils.collectTripInfos(flightList);
		AirAsiaUtils.setTotalFareOnTripInfo(tripList);
		return tripList;
	}

	private PriceInfo setNewFareComponent(PriceInfo priceInfo, PaxFare[] paxFares, AirSearchQuery searchQuery,
			PassengerFee infantFee) {
		Map<PaxType, FareDetail> fareDetails = new ConcurrentHashMap<>();
		FareDetail fareDetail = new FareDetail();
		int paxCount = 0;
		// Adult Fare
		paxCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
		fareDetail =
				parsePaxWiseFareBreakUp(fareDetail, getPaxFare(paxFares, PAX_TYPE_ADULT), PAX_TYPE_ADULT, paxCount);
		fareDetail = copyStaticInfo(fareDetail, priceInfo.getFareDetail(PaxType.ADULT));
		fareDetails.put(PaxType.ADULT, fareDetail);
		// Child Fare
		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD) > 0) {
			fareDetail = new FareDetail();
			paxCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			fareDetail =
					parsePaxWiseFareBreakUp(fareDetail, getPaxFare(paxFares, PAX_TYPE_CHILD), PAX_TYPE_CHILD, paxCount);
			fareDetail = copyStaticInfo(fareDetail, priceInfo.getFareDetail(PaxType.CHILD));
			fareDetails.put(PaxType.CHILD, fareDetail);
		}

		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT) > 0) {
			// Infant Fare
			fareDetail = new FareDetail();
			fareDetail.setFareComponents(new HashMap<>());
			paxCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT);
			if (infantFee != null && infantFee.getServiceCharges() != null
					&& ArrayUtils.isNotEmpty(infantFee.getServiceCharges().getBookingServiceCharge())) {
				PaxFare paxFare = new PaxFare();
				paxFare.setPaxType(PAX_TYPE_INFANT);
				fareDetail = setPaxWiseFareBreakUp(fareDetail, paxFare, paxCount,
						infantFee.getServiceCharges().getBookingServiceCharge());
			}
			fareDetails.put(PaxType.INFANT, fareDetail);
		}
		priceInfo.setFareDetails(fareDetails);
		return priceInfo;
	}

	public static PaxFare getPaxFare(PaxFare[] paxFares, String paxType) {
		PaxFare fare = null;
		if (ArrayUtils.isNotEmpty(paxFares)) {
			for (PaxFare paxFare : paxFares) {
				if (paxType.equalsIgnoreCase(paxFare.getPaxType())) {
					fare = paxFare;
					break;
				}
			}
		}
		return fare;
	}

	protected TypeOfSale getTypeOfSale() {
		TypeOfSale typeOfSale = new TypeOfSale();
		typeOfSale.setPromotionCode(getPromoCode(searchQuery));
		return typeOfSale;
	}

	private FareDetail parsePaxWiseFareBreakUp(FareDetail fareDetail, PaxFare paxFare, String paxType, int paxCount) {
		if (Objects.nonNull(paxFare) && paxFare.getPaxType().equalsIgnoreCase(paxType)) {
			return setPaxWiseFareBreakUp(fareDetail, paxFare, paxCount,
					paxFare.getServiceCharges().getBookingServiceCharge());
		}
		return fareDetail;
	}

	private static FareDetail copyStaticInfo(FareDetail newFareDetail, FareDetail oldFareDetail) {
		if (newFareDetail != null && oldFareDetail != null) {
			newFareDetail.setCabinClass(oldFareDetail.getCabinClass());
			newFareDetail.setClassOfBooking(oldFareDetail.getClassOfBooking());
			newFareDetail.setFareBasis(oldFareDetail.getFareBasis());
			newFareDetail.setFareType(oldFareDetail.getFareType());
			newFareDetail.setIsHandBaggage(oldFareDetail.getIsHandBaggage());
			newFareDetail.setIsMealIncluded(oldFareDetail.getIsMealIncluded());
			newFareDetail.setRefundableType(oldFareDetail.getRefundableType());
			newFareDetail.setSeatRemaining(oldFareDetail.getSeatRemaining());
		}
		return newFareDetail;
	}

	public AirSearchResult setInfantPrice(AirSearchResult searchResult, AirAsiaSessionManager sessionManager) {
		try {
			AtomicBoolean isInfantFarePresentInTrip = new AtomicBoolean();
			if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos()) && infantCount > 0) {
				if (sourceConfiguration != null
						&& BooleanUtils.isNotTrue(sourceConfiguration.getIsInfantFareFromItineraryPrice())
						&& BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromCache())
						&& StringUtils.isBlank(getPromoCode(searchQuery)) && searchQuery.getIsDomestic()) {
					fetchAndSetInfantFareFromCache(searchResult, isInfantFarePresentInTrip);
				}
				if (BooleanUtils.isNotTrue(sourceConfiguration.getIsInfantFareFromItineraryPrice())
						&& !isInfantFarePresentInTrip.get()) {
					AirAsiaBookingManager reviewManager = AirAsiaBookingManager.builder().configuration(configuration)
							.searchQuery(searchQuery).sessionSignature(sessionManager.getSessionSignature())
							.moneyExchnageComm(moneyExchnageComm).listener(listener).bookingStub(bookingStub)
							.sourceConfiguration(sourceConfiguration).bookingUser(bookingUser).build();
					AirAsiaSSRManager ssrManager = AirAsiaSSRManager.builder().configuration(configuration)
							.searchQuery(searchQuery).sessionSignature(sessionManager.getSessionSignature())
							.moneyExchnageComm(moneyExchnageComm).listener(listener).bookingStub(bookingStub)
							.sourceConfiguration(sourceConfiguration).bookingUser(bookingUser).build();
					reviewManager.init();
					ssrManager.init();
					try {
						searchResult.getTripInfos().forEach((tripType, trips) -> {
							if (CollectionUtils.isNotEmpty(trips)) {
								infantFare = new FareDetail();
								sessionManager.openSession();
								sellInfantFare(reviewManager, sessionManager, ssrManager, trips);
							}
						});
					} catch (NoSearchResultException nos) {
						addExceptionToLogger(nos);
						searchResult = null;
					}
				}
			}
		} catch (Exception e) {
			addExceptionToLogger(e);
			throw new NoSearchResultException(StringUtils.join(",", criticalMessageLogger));
		} finally {

		}
		return searchResult;
	}

	private void fetchAndSetInfantFareFromCache(AirSearchResult searchResult, AtomicBoolean isInfantFarePresent) {
		searchResult.getTripInfos().forEach((tripType, trips) -> {
			infantFare = new FareDetail();
			infantFare = FareComponentHelper.getInfantfareFromCache(searchQuery, AirSourceType.AIRASIA.getSourceId());
			if (CollectionUtils.isNotEmpty(trips) && infantFare != null
					&& MapUtils.isNotEmpty(infantFare.getFareComponents())) {
				isInfantFarePresent.set(Boolean.TRUE.booleanValue());
				for (TripInfo tripInfo : trips) {
					boolean isFlyThru = isFlyThruFLight(tripInfo.getSegmentInfos());
					tripInfo.getSegmentInfos().forEach(segmentInfo -> {
						copyInfantFareToSegment(segmentInfo, infantFare, isFlyThru);
					});
				}
				AirAsiaUtils.setTotalFareOnTripInfo(trips);
			}
		});

	}

	private void copyInfantFareToSegment(SegmentInfo segmentInfo, FareDetail infantFareDetail, boolean isFlyThru) {
		segmentInfo.getPriceInfoList().forEach(priceInfo -> {
			FareDetail fareDetail = new FareDetail();
			fareDetail.setFareComponents(new HashMap<>());
			if (infantFareDetail != null && MapUtils.isNotEmpty(infantFareDetail.getFareComponents())) {
				if (airAsiaAirline.isInfantFareOnSegmentWise(segmentInfo)
						&& (!isFlyThru || segmentInfo.getSegmentNum() == 0)) {
					infantFareDetail.getFareComponents().forEach((fc, amount) -> {
						fareDetail.getFareComponents().put(fc, amount);
					});
				}
			}
			fareDetail.setCabinClass(searchQuery.getCabinClass());
			priceInfo.getFareDetail(PaxType.INFANT, new FareDetail()).setFareComponents(fareDetail.getFareComponents());
		});

	}

	private void sellInfantFare(AirAsiaBookingManager reviewManager, AirAsiaSessionManager sessionManager,
			AirAsiaSSRManager ssrManager, List<TripInfo> copyTripInfos) {
		int index = 0;
		TripInfo tripInfo = null;
		boolean isSuccess = false;
		do {
			try {
				sessionManager.openSession();
				reviewManager.setSessionSignature(sessionManager.getSessionSignature());
				ssrManager.setSessionSignature(sessionManager.getSessionSignature());
				tripInfo = copyTripInfos.get(index);
				reviewManager.sendSellJourneyRequest(Arrays.asList(tripInfo));
				ssrManager.doSSRSearch(tripInfo);
				removeSSRAndSetZeroFareDetailOnSearch(tripInfo);
				reviewManager.setInfantFareToTrip(copyTripInfos, tripInfo);
				isSuccess = true;
			} catch (Exception e) {
				index++;
				log.info("Infant Fare is Not available checking for another trip {}", tripInfo, e);
			} finally {
				sessionManager.closeSession();
			}
		} while (!isSuccess && index - 1 < copyTripInfos.size() - 1);

		if (!isSuccess && index == copyTripInfos.size()) {
			throw new NoSearchResultException("Infant Fare Failed for all trips");
		}
	}

	// Resets baggage and meal on search and sets zero fare for the infant fare unset segments(i.e segments having
	// legNum > 0)
	private void removeSSRAndSetZeroFareDetailOnSearch(TripInfo tripInfo) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.setSsrInfo(null);
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				if (priceInfo.getMiscInfo().getLegNum() != 0) {
					FareDetail infantFare = priceInfo.getFareDetail(PaxType.INFANT);
					if (infantFare == null || MapUtils.isEmpty(infantFare.getFareComponents())) {
						FareDetail infantFareDetail = new FareDetail();
						Map<FareComponent, Double> fareComponents = new HashMap<>();
						fareComponents.put(FareComponent.BF, 0.0);
						infantFareDetail.setFareComponents(fareComponents);
						priceInfo.getFareDetails().put(PaxType.INFANT, infantFareDetail);
					}
				}
			});
		});
	}

	public AirSearchResult doItineraryPrice(AirSearchResult searchResult, boolean isItineraryPrice) {
		AtomicLong startTime = new AtomicLong(0);
		AtomicLong endTime = new AtomicLong(0);
		AtomicInteger totalTripSize = new AtomicInteger(0);
		AtomicInteger batchTripsCount = new AtomicInteger(0);
		AtomicInteger itinPriceRqCount = new AtomicInteger(0);
		try {
			listener.setStoreLogs(true);
			if (isItineraryPrice && searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				List<Future<List<TripInfo>>> futureTaskList = new ArrayList<>();
				startTime.set(System.currentTimeMillis());
				searchResult.getTripInfos().forEach((key, trips) -> {
					totalTripSize.set(totalTripSize.get() + trips.size());
					List<List<Pair<TripInfo, Integer>>> batchTrips = new ArrayList<>();
					batchTrips = AirAsiaUtils.getBatchTrips(trips, 6);
					batchTrips.forEach(batch -> {
						batchTripsCount.set(batchTripsCount.get() + batch.size());
						futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
								.submit(() -> this.getItineraryPrice(batch, true)));
					});
				});
				log.debug("Size of Itinerary Price batch size {} for search {}", futureTaskList.size(),
						searchQuery.getSearchId());
				futureTaskList.forEach(task -> {
					try {
						task.get(30, TimeUnit.SECONDS);
					} catch (InterruptedException | ExecutionException | TimeoutException e) {
						log.info(AirSourceConstants.AIR_SUPPLIER_SEARCH_PRICE_THREAD, searchQuery, e);
						throw new NoSearchResultException("ItineraryPrice Thread Interrupted ");
					}
				});
				endTime.set(System.currentTimeMillis());
			}
		} finally {
			searchResult = this.splitAndProcessPricedItinerary(searchResult);
			long searchtime = Long.valueOf(endTime.longValue() - startTime.longValue());
			if (totalTripSize.get() > 0) {
				SupplierAnalyticsQuery supplierAnalyticsQuery = SupplierAnalyticsQuery.builder()
						.tripsize(totalTripSize.get()).batchsize(itinPriceRqCount.get())
						.cacheapplied(totalTripSize.get() - batchTripsCount.get()).supplierhit(batchTripsCount.get())
						.flowtype(SupplierFlowType.ITINERARY_PRICE.name())
						.searchtimeinsec(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(searchtime)).intValue()).build();
				supplierAnalyticInfos.add(supplierAnalyticsQuery);
			}
		}
		return searchResult;
	}

	public void setInfantPriceOnReview(List<TripInfo> newTrips) {
		if (CollectionUtils.isNotEmpty(newTrips)) {
			TripInfo selectedTrip = newTrips.get(0);
			AirUtils.splitTripInfo(selectedTrip, false).forEach(trip -> {
				if (infantCount > 0) {
					// As Selling Infant while booking copy infant from Old Trip
					trip.getSegmentInfos().forEach(segmentInfo -> {
						FareDetail infantFare = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT);
						setInfantFareToTrip(segmentInfo, infantFare);
					});
				}
				AirAsiaUtils.setTotalFareOnTripInfo(Arrays.asList(trip));
			});
		}
	}

	public AirSearchResult processSearchResult(AirSearchResult searchResult) {
		if (searchQuery.isIntlReturn() && searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			searchResult = AirAsiaUtils.processSearchResultForItineraryPrice(searchResult, configuration.getSourceId(),
					searchQuery.isIntlReturn(), bookingUser);
			if (searchResult.getTripInfos().get(TripInfoType.COMBO.name()) != null) {
				log.info("Number of combination made for itinerary pricing is {} for searchId {}",
						searchResult.getTripInfos().get(TripInfoType.COMBO.name()).size(), searchQuery.getSearchId());
			}
		}
		return searchResult;
	}

	public AirSearchResult splitAndProcessPricedItinerary(AirSearchResult searchResult) {
		if (searchQuery.isIntlReturn() && searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())
				&& CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(TripInfoType.COMBO.name()))) {
			searchResult = AirAsiaUtils.splitItineraryPricedTrips(searchResult);
			searchResult = AirSearchResultProcessingManager.processSearchTypeResult(searchResult,
					configuration.getSourceId(), searchQuery.isIntlReturn(), bookingUser);
		}
		return searchResult;
	}

	private boolean checkIfExpections(GetAvailabilityWithTaxesResponse availabilityResponse) {
		if (availabilityResponse != null && availabilityResponse.getGetAvailabilityWithTaxesResult() != null
				&& StringUtils
						.isNotBlank(availabilityResponse.getGetAvailabilityWithTaxesResult().getExceptionMessage())) {
			String errorMessage = availabilityResponse.getGetAvailabilityWithTaxesResult().getExceptionMessage();
			if (errorMessage.toLowerCase().contains(SESSION_NOT_FOUND)) {
				throw new SupplierSessionException(errorMessage);
			}
			if (errorMessage.toLowerCase().contains(INVALID_ROUTE)) {
				throw new NotOperatingSectorException(errorMessage);
			}
			throw new NoSearchResultException(errorMessage);
		}
		return false;
	}

	private GetAvailabilityWithTaxes buildAvailabilityRequest() {
		GetAvailabilityWithTaxes availabilityRQ = new GetAvailabilityWithTaxes();
		availabilityRQ.setStrSessionID(sessionSignature);
		availabilityRQ.setObjTripAvailabilityRequest(buildTripAvailabilityRq());
		return availabilityRQ;
	}

}
