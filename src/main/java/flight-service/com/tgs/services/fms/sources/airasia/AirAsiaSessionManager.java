package com.tgs.services.fms.sources.airasia;

import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.Logon;
import org.tempuri.LogonRequest;
import org.tempuri.LogonResponseE;
import org.tempuri.Logout;
import java.rmi.RemoteException;
import java.util.Objects;


@Getter
@Setter
@Slf4j
@SuperBuilder
final class AirAsiaSessionManager extends AirAsiaServiceManager {

	protected long sessionValidTime;

	// Session token authentication failure. : No such session
	protected static final String SESSION_FAILURE = "session token authentication failure";
	// Session Timeout
	protected static final String NO_SUCH_SESSION = "no such session";
	// Invalid Session
	protected static final String INVALID_SESSION = "invalid session";

	public void openSession() {
		try {
			Logon logon = new Logon();
			logon.setLogonRequest(getLogOnRequest());
			listener.setType("1_LOGIN");
			listener.getVisibilityGroups().addAll(AirSupplierUtils.getLogVisibility());
			sessionStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			LogonResponseE responseE = sessionStub.logon(logon);
			if (responseE.getLogonResult() != null) {
				if (StringUtils.isNotBlank(responseE.getLogonResult().getExceptionMessage())) {
					throw new SupplierRemoteException(responseE.getLogonResult().getExceptionMessage());
				} else {
					sessionSignature = responseE.getLogonResult().getSessionID();
					sessionValidTime = responseE.getLogonResult().getSessionTimeoutInterval();
				}
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			sessionStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private LogonRequest getLogOnRequest() {
		LogonRequest logonRequest = new LogonRequest();
		logonRequest.setUsername(configuration.getSupplierCredential().getUserName());
		logonRequest.setPassword(configuration.getSupplierCredential().getPassword());
		return logonRequest;
	}

	public void closeSession() {
		try {
			if (StringUtils.isNotBlank(sessionSignature) && sessionStub != null) {
				Logout logout = new Logout();
				logout.setSessionID(sessionSignature);
				listener.setType("9_Logout");
				sessionStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				sessionStub.logout(logout);
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			sessionStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	public void setSessionToken(SupplierSession session) throws Exception {
		if (Objects.isNull(session)) {
			openSession();
		} else {
			setSessionSignature(session.getSupplierSessionInfo().getSessionToken());
		}
	}

	public boolean isRetry(Exception e) {
		return e != null && StringUtils.isNotBlank(e.getMessage())
				&& (e.getMessage().toLowerCase().contains(SESSION_NOT_FOUND)
						|| e.getMessage().toLowerCase().contains(SESSION_FAILURE)
						|| e.getMessage().toLowerCase().contains(NO_SUCH_SESSION)
						|| e.getMessage().toLowerCase().contains(INVALID_SESSION));
	}
}
