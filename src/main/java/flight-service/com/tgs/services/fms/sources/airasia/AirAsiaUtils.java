package com.tgs.services.fms.sources.airasia;


import com.google.common.collect.Lists;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import org.tempuri.JourneyDateMarket;
import org.tempuri.Journey;
import org.tempuri.Segment;
import org.tempuri.ArrayOfPassenger;
import org.tempuri.BookingName;
import org.tempuri.Passenger;
import org.tempuri.PassengerFee;
import org.tempuri.Leg;
import org.tempuri.Gender;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.SortedSet;
import java.util.Iterator;
import java.util.ArrayList;
import java.util.StringJoiner;
import java.util.TreeSet;
import java.util.Map;
import java.util.HashMap;
import java.util.Calendar;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Slf4j
public class AirAsiaUtils {

	public static AirSearchResult segregateResult(AirSearchQuery searchQuery, JourneyDateMarket journeyDateMarket,
			List<TripInfo> tripInfos, AirSearchResult searchResult, User bookingUser) {
		log.debug("Got {} trips for searchQuery {}", tripInfos.size(), searchQuery);
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			setTotalFareOnTripInfo(tripInfos);
			AtomicInteger journeyType = new AtomicInteger(0);
			searchQuery.getRouteInfos().forEach(routeInfo -> {
				if ((journeyDateMarket.getDepartureStation().equals(routeInfo.getFromCityOrAirport().getCode())
						|| journeyDateMarket.getDepartureStation()
								.equals(getNearByAirport(routeInfo.getFromCityOrAirport().getCode(), searchQuery,
										AirSourceType.AIRASIA.getSourceId(), bookingUser)))
						&& (journeyDateMarket.getArrivalStation().equals(routeInfo.getToCityOrAirport().getCode())
								|| journeyDateMarket.getArrivalStation()
										.equals(getNearByAirport(routeInfo.getToCityOrAirport().getCode(), searchQuery,
												AirSourceType.AIRASIA.getSourceId(), bookingUser)))) {
					if (journeyType.get() == 0) {
						searchResult.getTripInfos().put(TripInfoType.ONWARD.name(), tripInfos);
					} else if (journeyType.get() == 1) {
						searchResult.getTripInfos().put(TripInfoType.RETURN.name(), tripInfos);
					}
				}
				journeyType.getAndIncrement();
			});
		}
		return searchResult;
	}

	public static void setTotalFareOnTripInfo(List<TripInfo> tripList) {
		tripList.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getBookingRelatedInfo() != null) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(travellerInfo -> {
						if (travellerInfo.getFareDetail() != null) {
							travellerInfo.getFareDetail().getFareComponents().put(FareComponent.TF,
									getTotalFare(travellerInfo.getFareDetail()));
						}
					});
				} else if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						priceInfo.getFareDetails().forEach((paxType, fare) -> {
							Double totalFare = getTotalFare(fare);
							if (totalFare != 0) {
								priceInfo.getFareDetail(paxType).getFareComponents().put(FareComponent.TF,
										getTotalFare(fare));
							}
						});
					});
				}
			});
		});
	}

	public static Double getTotalFare(FareDetail fareDetail) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			fareDetail.getFareComponents().forEach((fareComponent, amount) -> {
				if (!fareComponent.equals(FareComponent.TF))
					totalAmount.addAndGet(amount);
			});
		}
		return totalAmount.doubleValue();
	}

	public static String getNearByAirport(String airportCode, AirSearchQuery searchQuery, Integer sourceId,
			User bookingUser) {
		String nearByCode = AirUtils.getNearByAirport(airportCode, sourceId, searchQuery, bookingUser);
		if (StringUtils.isNotBlank(nearByCode)) {
			return nearByCode;
		}
		return airportCode;
	}

	public static Pair<TripInfo, Integer> findTripInfo(List<Pair<TripInfo, Integer>> flightList, Journey journeys) {
		for (Pair<TripInfo, Integer> tripPair : flightList) {
			TripInfo tripInfo = tripPair.getFirst();
			boolean hasAllSegment = true;
			Segment[] journeySegments = journeys.getSegments().getSegment();
			for (int journeySegmentIndex = 0; journeySegmentIndex < journeySegments.length; journeySegmentIndex++) {
				Segment segment = journeySegments[journeySegmentIndex];
				if (CollectionUtils
						.isEmpty(AirAsiaUtils.getSegmentInfo(tripInfo.getSegmentInfos(), segment.getDepartureStation(),
								segment.getArrivalStation(), segment.getFlightDesignator().getCarrierCode(),
								segment.getFlightDesignator().getFlightNumber(), segment.getSTD()))) {
					hasAllSegment = false;
					break;
				}
			}
			if (hasAllSegment) {
				return tripPair;
			}
		}
		return null;
	}

	public static boolean isMaster(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MASTER")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MSTR");
		}
		return false;
	}

	public static boolean isMiss(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MISS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	public static boolean isMrsMs(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MRS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	public static Gender getGender(String title, FlightTravellerInfo travellerInfo) {
		if (title.toUpperCase().equalsIgnoreCase("MR") || isMaster(travellerInfo)) {
			return Gender.Male;
		} else if (title.toUpperCase().equalsIgnoreCase("MRS") || isMiss(travellerInfo)) {
			return Gender.Female;
		}
		return null;
	}

	/**
	 * @return true or false - if any SSR Added to atleast one Pax
	 */
	public static boolean isSeatAddedInTrip(List<SegmentInfo> segmentInfos) {
		boolean isSSRAdded = false;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					if (Objects.nonNull(travellerInfo.getSsrSeatInfo())) {
						isSSRAdded = true;
						break;
					}
				}
			}
		}
		return isSSRAdded;
	}

	public static String getNationality(FlightTravellerInfo flightTravellerInfo) {
		if (StringUtils.isNotBlank(flightTravellerInfo.getPassportNationality())) {
			return flightTravellerInfo.getPassportNationality();
		}
		ClientGeneralInfo generalInfo = ServiceCommunicatorHelper.getClientInfo();
		return generalInfo.getNationality();
	}

	public static String getMobile(boolean isSpecialCharacterNotRequired, String mobile) {
		if (isSpecialCharacterNotRequired) {
			mobile = mobile.replaceAll("[^0-9]", "");
			return mobile;
		}
		return mobile;
	}

	public static String getContact(String prefix, String mobile) {
		return StringUtils.join(prefix, mobile).replaceAll("[^0-9]", "");
	}

	/**
	 * @return true or false - if any SSR Added to atleast one Pax
	 */
	public static boolean isSSRAddedInTrip(List<SegmentInfo> segmentInfos) {
		boolean isSSRAdded = false;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					if (Objects.nonNull(travellerInfo.getSsrBaggageInfo())
							|| Objects.nonNull(travellerInfo.getSsrMealInfo())) {
						isSSRAdded = true;
						break;
					}
				}
			}
		}
		return isSSRAdded;
	}

	public static boolean isProductClassRequireSSR(SegmentInfo segmentInfo, String productClass) {
		AtomicBoolean isAllowed = new AtomicBoolean();
		segmentInfo.getPriceInfo(0).getFareDetails().forEach(((paxType, fareDetail) -> {
			if (StringUtils.isNotEmpty(fareDetail.getClassOfBooking())
					&& fareDetail.getClassOfBooking().equalsIgnoreCase(productClass)) {
				isAllowed.set(true);
			}

		}));
		return isAllowed.get();
	}

	/*
	 * public static List<List<TripInfo>> getTripsInBucket(List<TripInfo> tripInfos, Integer bucketSize,
	 * AirSourceConfigurationOutput sourceConfiguration) { List<List<TripInfo>> batchTrips = new
	 * ArrayList<List<TripInfo>>(); tripInfos.forEach(trip -> { AirAsiaUtils.addToBucket(trip, batchTrips, bucketSize);
	 * });
	 * 
	 * return batchTrips; }
	 */

	private static boolean addToBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets,
			TripInfo currentTripInfo, AtomicInteger index, Integer batchSize, Integer priceIndex) {
		boolean isAdded = false;

		if (!checkIfTripExistsInBucket(tripBuckets, index.get(), currentTripInfo)
				&& !isShiftBucket(tripBuckets, index.get(), batchSize)) {
			tripBuckets.get(index.get()).add(Pair.of(currentTripInfo, priceIndex));
			isAdded = true;
		} else {
			index.getAndIncrement();
			initializeBucket(tripBuckets, index.get());
			isAdded = addToBucket(tripBuckets, currentTripInfo, index, batchSize, priceIndex);
		}

		return isAdded;
	}

	private static boolean isShiftBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets, Integer index,
			Integer batchSize) {
		return tripBuckets.get(index).size() >= batchSize;
	}

	private static boolean checkIfTripExistsInBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets,
			Integer index, TripInfo currentTripInfo) {
		List<String> flightKey = currentTripInfo.getFlightNumberSet().stream().collect(Collectors.toList());
		AtomicBoolean isExist = new AtomicBoolean();
		if (MapUtils.isNotEmpty(tripBuckets)) {
			AtomicInteger bucketIndex = new AtomicInteger(0);
			tripBuckets.forEach((bucket, tripPairs) -> {
				if (index == bucketIndex.intValue()) {
					tripPairs.forEach(pair -> {
						List<String> bucketKey =
								pair.getFirst().getFlightNumberSet().stream().collect(Collectors.toList());
						if (!isExist.get()) {
							isExist.set(isTwoFlightInSameBucket(bucketKey, flightKey));
						}
					});
				}
				bucketIndex.getAndIncrement();
			});
		}
		return isExist.get();
	}

	public static boolean isTwoFlightInSameBucket(List<String> bucketFlightKey, List<String> flightKey) {
		AtomicBoolean isExist = new AtomicBoolean();
		flightKey.forEach(key -> {
			if (!isExist.get() && bucketFlightKey.contains(key)) {
				isExist.set(true);
			}
		});
		return isExist.get();
	}

	public static List<SegmentInfo> getSegmentInfo(List<SegmentInfo> segmentInfos, String departureStation,
			String arrivalStation, String airline, String flightNumber, Calendar departureDate) {
		int fromIndex = 0;
		int toIndex = segmentInfos.size();
		for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
			if (segmentInfo.getDepartureAirportCode().equalsIgnoreCase(departureStation.trim())
					&& segmentInfo.getAirlineCode(false).equals(airline)
					&& segmentInfo.getFlightNumber().equalsIgnoreCase(flightNumber.trim())) {
				int innerSegmentIndex = segmentIndex;
				for (; innerSegmentIndex < segmentInfos.size(); innerSegmentIndex++) {
					if (segmentInfos.get(innerSegmentIndex).getArrivalAirportCode()
							.equalsIgnoreCase(arrivalStation.trim())) {
						fromIndex = segmentIndex;
						toIndex = innerSegmentIndex + 1;
						return segmentInfos.subList(fromIndex, toIndex);
					}
				}
			}
		}
		return null;
	}

	public static int getNextLegNum(List<SegmentInfo> segmentInfos, int segmentIndex) {
		return (segmentIndex + 1 < segmentInfos.size())
				? segmentInfos.get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
				: 0;
	}

	public static AirSearchResult processSearchResultForItineraryPrice(AirSearchResult searchResult, Integer sourceId,
			boolean isIntlReturn, User bookingUser) {
		if (isIntlReturn) {
			List<TripInfo> combinedTrips = AirAsiaUtils.buildCombinationWithoutRedundantTrip(
					searchResult.getTripInfos().get(TripInfoType.ONWARD.name()),
					searchResult.getTripInfos().get(TripInfoType.RETURN.name()), sourceId, bookingUser);
			searchResult = new AirSearchResult();
			if (CollectionUtils.isNotEmpty(combinedTrips)) {
				searchResult.getTripInfos().put(TripInfoType.COMBO.name(), combinedTrips);
			}
			// searchResult.getTripInfos().remove(TripInfoType.ONWARD.name());
			// searchResult.getTripInfos().remove(TripInfoType.RETURN.name());
		}
		return searchResult;
	}

	public static List<TripInfo> buildCombinationWithoutRedundantTrip(List<TripInfo> onwardTrips,
			List<TripInfo> returnTrips, int sourceId, User bookingUser) {
		if (CollectionUtils.isEmpty(onwardTrips) || CollectionUtils.isEmpty(returnTrips)) {
			return null;
		}
		List<TripInfo> result = new ArrayList<TripInfo>();
		int returnIndex = 0;
		Map<String, Boolean> combinedTripMap = new HashMap<>();
		for (int onwardIndex = 0; onwardIndex < onwardTrips.size(); onwardIndex++) {
			TripInfo onwardTrip = onwardTrips.get(onwardIndex);
			boolean breakValue = false;
			int returnIndexCopy = returnIndex;
			do {
				TripInfo returnTrip = returnTrips.get(returnIndexCopy);
				TripInfo combinedTrip = getCombinedTrip(onwardTrip, returnTrip, sourceId, bookingUser);
				if (combinedTrip != null && combinedTripMap.get(AirAsiaUtils.getTripKey(combinedTrip)) == null) {
					result.add(combinedTrip);
					combinedTripMap.put(AirAsiaUtils.getTripKey(combinedTrip), true);
					breakValue = true;
				}
				returnIndexCopy = (returnIndexCopy + 1 == returnTrips.size()) ? 0 : returnIndexCopy + 1;
				if (returnIndexCopy == returnIndex) {
					breakValue = true;
				}
			} while (!breakValue);
			returnIndex = returnIndexCopy;
		}

		int onwardIndex = 0;
		for (returnIndex = 0; returnIndex < returnTrips.size(); returnIndex++) {
			TripInfo returnTrip = returnTrips.get(returnIndex);
			boolean breakValue = false;
			int onwardIndexCopy = onwardIndex;
			do {
				TripInfo onwardTrip = onwardTrips.get(onwardIndexCopy);
				TripInfo combinedTrip = getCombinedTrip(onwardTrip, returnTrip, sourceId, bookingUser);
				if (combinedTrip != null && combinedTripMap.get(AirAsiaUtils.getTripKey(combinedTrip)) == null) {
					result.add(combinedTrip);
					combinedTripMap.put(AirAsiaUtils.getTripKey(combinedTrip), true);
					breakValue = true;
				}
				onwardIndexCopy = (onwardIndexCopy + 1 == onwardTrips.size()) ? 0 : onwardIndexCopy + 1;
				if (onwardIndexCopy == onwardIndex) {
					breakValue = true;
				}
			} while (!breakValue);
			onwardIndex = onwardIndexCopy;
		}
		return result;
	}

	private static String getTripKey(TripInfo combinedTrip) {
		StringJoiner joiner = new StringJoiner("_");
		combinedTrip.getSegmentInfos().forEach(segment -> {
			joiner.add(segment.getDepartureAirportCode());
			joiner.add(segment.getArrivalAirportCode());
			joiner.add(segment.getAirlineCode(false));
			joiner.add(segment.getFlightNumber());
		});
		return joiner.toString();
	}

	public static TripInfo getCombinedTrip(TripInfo onwardTrip, TripInfo returnTrip, int sourceId, User bookingUser) {
		LocalDateTime onwardArrivalTime = onwardTrip.getArrivalTime();
		LocalDateTime returnDepartureTime = returnTrip.getDepartureTime();
		if (Duration.between(onwardArrivalTime, returnDepartureTime).toMinutes() > AirUtils
				.getCombinationAllowedMinutes(sourceId, bookingUser)) {
			TripInfo onwardTripCopy = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
			TripInfo returnTripCopy = new GsonMapper<>(returnTrip, TripInfo.class).convert();
			if (onwardTripCopy.isPriceInfosNotEmpty() && returnTripCopy.isPriceInfosNotEmpty()) {
				returnTripCopy.getSegmentInfos().get(0).setIsReturnSegment(true);
				onwardTripCopy.getSegmentInfos().addAll(returnTripCopy.getSegmentInfos());
				return onwardTripCopy;
			}
		}
		return null;
	}

	public static AirSearchResult splitItineraryPricedTrips(AirSearchResult searchResult) {
		List<TripInfo> onwardTrips = new ArrayList<TripInfo>();
		List<TripInfo> returnTrips = new ArrayList<TripInfo>();
		Map<String, Boolean> onwardTripMap = new HashMap<String, Boolean>();
		Map<String, Boolean> returnTripMap = new HashMap<String, Boolean>();
		if (CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(TripInfoType.COMBO.name()))) {
			searchResult.getTripInfos().get(TripInfoType.COMBO.name()).forEach(tripInfo -> {
				List<TripInfo> combinedTrips = AirUtils.splitTripInfo(tripInfo, false);
				if (combinedTrips.size() == 2) {
					TripInfo onwardTrip = combinedTrips.get(0);
					TripInfo returnTrip = combinedTrips.get(1);
					if (onwardTripMap.get(AirAsiaUtils.getTripKey(onwardTrip)) == null) {
						onwardTrips.add(onwardTrip);
						onwardTripMap.put(AirAsiaUtils.getTripKey(onwardTrip), true);
					}
					if (returnTripMap.get(AirAsiaUtils.getTripKey(returnTrip)) == null) {
						returnTrips.add(returnTrip);
						returnTripMap.put(AirAsiaUtils.getTripKey(returnTrip), true);
					}
				}
			});
			searchResult = new AirSearchResult();
			searchResult.getTripInfos().put(TripInfoType.ONWARD.name(), onwardTrips);
			searchResult.getTripInfos().put(TripInfoType.RETURN.name(), returnTrips);
		}
		return searchResult;
	}

	public static String getCompanyName(User bookingUser, ClientGeneralInfo clientInfo) {
		String companyName = AirSupplierUtils.getCompanyName(bookingUser, clientInfo);
		if (companyName.length() > 64) {
			return companyName.substring(0, 63);
		}
		return companyName;
	}

	public static String getPassengerName(BookingName bookingName) {
		return StringUtils.join(bookingName.getFirstName(), bookingName.getLastName());
	}

	public static LocalDate getRetreieveDOB(Passenger pax) {
		LocalDate localDate = TgsDateUtils.calenderToLocalDate(pax.getPassengerTypeInfo().getDOB());
		LocalDate defaultDOB = LocalDate.of(9999, 12, 31);
		if (localDate.equals(defaultDOB)) {
			return null;
		}
		return localDate;
	}

	public static String getCity(String name) {
		if (name.length() > 32) {
			return name.substring(0, 31);
		}
		return name;
	}

	public static void setTerminal(Leg leg, SegmentInfo segmentInfo) {
		if (leg.getLegInfo() != null && segmentInfo.getDepartAirportInfo() != null
				&& segmentInfo.getArrivalAirportInfo() != null) {
			if (StringUtils.isNotBlank(leg.getLegInfo().getDepartureTerminal())) {
				segmentInfo.getDepartAirportInfo()
						.setTerminal(AirUtils.getTerminalInfo(leg.getLegInfo().getDepartureTerminal()));
			}
			if (StringUtils.isNotBlank(leg.getLegInfo().getArrivalTerminal())) {
				segmentInfo.getArrivalAirportInfo()
						.setTerminal(AirUtils.getTerminalInfo(leg.getLegInfo().getArrivalTerminal()));
			}
		}
	}

	public static String getAddress(String address) {
		if (address.length() > 51) {
			// address cannot be More than 51 Characters
			address = address.substring(0, 51);
		}
		return address;
	}

	public static String getValidCompanyName(String companyName) {
		if (companyName.length() > 64) {
			// Company Name cannot be More than 64 Characters
			return companyName.substring(0, 63);
		}
		return companyName;
	}

	@SuppressWarnings("unchecked")
	public static void mergeBaggageinFirstSegment(SegmentInfo firstSegmentInfo, SegmentInfo otherSegmentInfo) {
		List<BaggageSSRInformation> baggageSSRFirstSegment =
				(List<BaggageSSRInformation>) firstSegmentInfo.getSsrInfo().get(SSRType.BAGGAGE);
		List<BaggageSSRInformation> baggageSSROtherSegment =
				(List<BaggageSSRInformation>) otherSegmentInfo.getSsrInfo().get(SSRType.BAGGAGE);
		for (Iterator<BaggageSSRInformation> iter = baggageSSRFirstSegment.iterator(); iter.hasNext();) {
			BaggageSSRInformation baggage = iter.next();
			if (!parseAmountToFirstSegment(baggage, baggageSSROtherSegment)) {
				iter.remove();
			}
		}
		setSSRAmountToZero(baggageSSROtherSegment);
	}

	private static boolean parseAmountToFirstSegment(BaggageSSRInformation firstbaggage,
			List<BaggageSSRInformation> baggageSSROtherSegment) {
		for (BaggageSSRInformation baggage : baggageSSROtherSegment) {
			if (firstbaggage.getCode().equals(baggage.getCode()) && Objects.nonNull(baggage.getAmount())) {
				firstbaggage.setAmount(firstbaggage.getAmount() + baggage.getAmount());
				return true;
			}
		}
		return false;
	}

	private static void setSSRAmountToZero(List<BaggageSSRInformation> baggageSSROtherSegment) {
		baggageSSROtherSegment.forEach(ssrInfo -> {
			ssrInfo.setAmount(null);
		});
	}

	public static void filterTripAndPriceOptions(AirSearchResult searchResult, AirSearchQuery searchQuery) {
		try {
			if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				for (String key : searchResult.getTripInfos().keySet()) {
					List<TripInfo> tripInfos = searchResult.getTripInfos().get(key);
					if (CollectionUtils.isNotEmpty(tripInfos)) {
						for (Iterator<TripInfo> tripIter = tripInfos.iterator(); tripIter.hasNext();) {
							filterPriceInTrip(tripIter.next());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to filter tripAndPriceoptions for searchQuery {}", searchQuery);
		}
	}

	public static void filterPriceInTrip(TripInfo tripInfo) {
		if (tripInfo != null && CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			AtomicInteger minimumPriceOptionCount =
					new AtomicInteger(tripInfo.getSegmentInfos().get(0).getPriceInfoList().size());
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					if (minimumPriceOptionCount.get() > segmentInfo.getPriceInfoList().size()) {
						minimumPriceOptionCount.set(segmentInfo.getPriceInfoList().size());
					}
				}
			});
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (minimumPriceOptionCount.get() < segmentInfo.getPriceInfoList().size()) {
					List<PriceInfo> priceList = new ArrayList<>();
					for (int index = 0; index < minimumPriceOptionCount.get(); index++) {
						priceList.add(segmentInfo.getPriceInfo(index));
					}
					segmentInfo.setPriceInfoList(priceList);
				}
			});
		}
	}

	public static SegmentInfo findSegmentFromTripInfos(List<TripInfo> tripInfos, Segment segment) {
		String segmentKey = StringUtils.join(segment.getDepartureStation(), "_", segment.getArrivalStation());
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (segmentInfo.getSegmentKey().equalsIgnoreCase(segmentKey)) {
					return segmentInfo;
				}
			}
		}
		return null;
	}

	public static FlightBasicFact getSSRFlightFact(SegmentInfo segmentInfo, boolean isReviewFlow, User user) {
		Integer sourceId = segmentInfo.getSupplierInfo().getSourceId();
		FareDetail fareDetail = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT);
		String productClass = fareDetail.getClassOfBooking();
		String airline = segmentInfo.getAirlineCode(false);
		TimePeriod travelPeriod = null;
		if (isReviewFlow) {
			travelPeriod = new TimePeriod();
			travelPeriod.setStartTime(segmentInfo.getDepartTime());
			travelPeriod.setEndTime(segmentInfo.getArrivalTime());
		}
		FlightBasicFact flightFact = FlightBasicFact.builder().airline(airline).productClass(productClass)
				.sourceId(sourceId).travelPeriod(travelPeriod).build();
		BaseUtils.createFactOnUser(flightFact, user);
		return flightFact;
	}

	public static List<List<Pair<TripInfo, Integer>>> getBatchTrips(List<TripInfo> tripInfos, Integer batchSize) {
		List<List<Pair<TripInfo, Integer>>> batchTrips = new ArrayList<>();
		Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets = new HashMap<>();

		AtomicInteger index = new AtomicInteger(0);

		initializeBucket(tripBuckets, index.get());

		for (TripInfo tripInfo : tripInfos) {
			int validPriceCount = AirAsiaUtils.getValidPriceCount(tripInfo);
			for (int priceIndex = 0; priceIndex < validPriceCount; priceIndex++) {
				addToBucket(tripBuckets, tripInfo, index, batchSize, priceIndex);
			}
			index.set(0);
		}

		tripBuckets.forEach((ind, trips) -> {
			batchTrips.addAll(Lists.partition(trips, batchSize));
		});

		return batchTrips;
	}

	private static void initializeBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets, Integer index) {
		if (MapUtils.isEmpty(tripBuckets) || MapUtils.getObject(tripBuckets, index) == null) {
			tripBuckets.put(index, new ArrayList<>());
			return;
		}
	}

	private static int getValidPriceCount(TripInfo tripInfo) {
		AtomicInteger validPriceCount = new AtomicInteger(tripInfo.getSegmentInfos().get(0).getPriceInfoList().size());
		tripInfo.getSegmentInfos().forEach(segment -> {
			if (segment.getPriceInfoList().size() < validPriceCount.get()) {
				validPriceCount.set(segment.getPriceInfoList().size());
			}
		});
		return validPriceCount.get();
	}

	public static List<TripInfo> collectTripInfos(List<Pair<TripInfo, Integer>> tripPair) {
		List<TripInfo> flightList = new ArrayList<>();
		for (Pair<TripInfo, Integer> pair : tripPair) {
			flightList.add(pair.getFirst());
		}
		return flightList;
	}

	public static PassengerFee getInfantFee(ArrayOfPassenger passengers, String flightReference) {
		if (passengers != null && ArrayUtils.isNotEmpty(passengers.getPassenger())) {
			for (Passenger passenger : passengers.getPassenger()) {
				if (passenger.getPassengerFees() != null
						&& ArrayUtils.isNotEmpty(passenger.getPassengerFees().getPassengerFee())) {
					for (PassengerFee passengerFee : passenger.getPassengerFees().getPassengerFee()) {
						if (passengerFee.getFlightReference() != null
								&& passengerFee.getFlightReference().contains(flightReference)
								&& AirAsiaServiceManager.PAX_TYPE_INFANT.equals(passengerFee.getFeeCode())) {
							return passengerFee;
						}
					}
				}
			}
		}
		return null;
	}

	public static void initializeInfantFareDetail(List<SegmentInfo> segments, AirSearchQuery searchQuery) {
		if (CollectionUtils.isNotEmpty(segments) && searchQuery.getPaxInfo().get(PaxType.INFANT) != null
				&& searchQuery.getPaxInfo().get(PaxType.INFANT) > 0) {
			for (SegmentInfo segment : segments) {
				for (PriceInfo priceInfo : segment.getPriceInfoList()) {
					if (priceInfo.getFareDetail(PaxType.INFANT) == null) {
						FareDetail fareDetail = new FareDetail();
						Map<FareComponent, Double> fc = new HashMap<>();
						fc.put(FareComponent.BF, 0d);
						fareDetail.setFareComponents(fc);
						priceInfo.getFareDetails().put(PaxType.INFANT, fareDetail);
					}
				}
			}
		}
	}

	public static Integer getPaxTypeCount(AirSearchQuery searchQuery) {
		AtomicInteger paxTypeCount = new AtomicInteger(0);
		if (MapUtils.isNotEmpty(searchQuery.getPaxInfo())) {
			searchQuery.getPaxInfo().forEach(((paxType, count) -> {
				if (count > 0 && !PaxType.INFANT.equals(paxType)) {
					paxTypeCount.getAndIncrement();
				}
			}));
		}
		return paxTypeCount.get();
	}

}
