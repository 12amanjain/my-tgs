package com.tgs.services.fms.sources.airasiadotrez;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.airasia.datamodel.GetBookingInStateRS;
import com.airasia.datamodel.SellTripRS;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirAsiaDotRezAirCancellationFactory extends AbstractAirCancellationFactory {

	protected RestAPIListener listener = null;

	protected FlightAPIURLRuleCriteria apiUrls;

	protected String tokenId;

	protected AirAsiaURLBindingService bindingService;

	public AirAsiaDotRezAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {

		super(bookingSegments, order, supplierConf);

	}

	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener(bookingId);
		}
		bindingService = AirAsiaURLBindingService.builder().configuration(supplierConf)
				.apiUrls(AirUtils.getAirEndPointURL(sourceConfiguration)).cachingComm(cachingComm).build();
	}

	@Override
	public AirCancellationDetail reviewBookingForCancellation(AirCancellationDetail cancellationDetail) {
		initialize();
		AirAsiaDotRezTokenManager tokenManager = AirAsiaDotRezTokenManager.builder().listener(listener)
				.bindingService(bindingService).bookingId(bookingId).supplierConfig(supplierConf).build();
		tokenId = tokenManager.createToken();
		if (StringUtils.isNotEmpty(tokenId)) {
			AirAsiaDotRezBookingRetrieveManager bookingRetrieveManager =
					AirAsiaDotRezBookingRetrieveManager.builder().listener(listener).supplierConfig(supplierConf)
							.bindingService(bindingService).listenerKey(bookingId).bookingId(bookingId).sessionId(tokenId).pnr(pnr).build();
			SellTripRS bookingDetailsResponse = bookingRetrieveManager.getBookingDetailsResponse();
			if (bookingDetailsResponse != null && bookingDetailsResponse.getData() != null) {
				AirAsiaDotRezCancellationManager cancellationManager = AirAsiaDotRezCancellationManager.builder()
						.listener(listener).sessionId(tokenId).supplierConfig(supplierConf)
						.bindingService(bindingService).bookingSegments(bookingSegments).bookingId(bookingId).build();
				cancellationManager.init();
				boolean isCancelled =
						cancellationManager.isCancelled(bookingDetailsResponse.getData().getInfo().getStatus());
				if (!isCancelled) {
					cancellationManager
							.setPassengerKeys(cancellationManager.createPassengerKeys(bookingDetailsResponse));
					GetBookingInStateRS bookingResponse =
							cancellationManager.submitCancelRequest(bookingDetailsResponse.getData());
					cancellationManager.setAirlineCancellationFee(bookingResponse, bookingSegments, false);
					cancellationDetail.setAutoCancellationAllowed(Boolean.TRUE);
				}
			}
		}
		return cancellationDetail;
	}

	@Override
	public boolean confirmBookingForCancellation() {
		boolean isCancelled = false;
		Optional<String> newPNR = Optional.empty();
		initialize();
		try {

			if (isPassengerCancellation()) {
				newPNR = Optional.of(dividePNR());
			}
			String childPNR = newPNR.orElse(pnr);
			AirAsiaDotRezTokenManager tokenManager = AirAsiaDotRezTokenManager.builder().bindingService(bindingService)
					.listener(listener).bookingId(bookingId).supplierConfig(supplierConf).build();
			tokenId = tokenManager.createToken();
			if (StringUtils.isNotEmpty(tokenId)) {
				AirAsiaDotRezBookingRetrieveManager bookingRetrieveManager = AirAsiaDotRezBookingRetrieveManager
						.builder().listener(listener).supplierConfig(supplierConf).sessionId(tokenId)
						.bookingId(bookingId).listenerKey(bookingId).pnr(childPNR).bindingService(bindingService).build();
				SellTripRS bookingDetailsResponse = bookingRetrieveManager.getBookingDetailsResponse();
				if (bookingDetailsResponse != null && bookingDetailsResponse.getData() != null) {
					AirAsiaDotRezCancellationManager cancellationManager = AirAsiaDotRezCancellationManager.builder()
							.listener(listener).bindingService(bindingService).sessionId(tokenId)
							.supplierConfig(supplierConf).bookingId(bookingId).bookingSegments(bookingSegments).build();
					cancellationManager.init();
					boolean isCancel =
							cancellationManager.isCancelled(bookingDetailsResponse.getData().getInfo().getStatus());
					if (!isCancel) {
						cancellationManager
								.setPassengerKeys(cancellationManager.createPassengerKeys(bookingDetailsResponse));
						GetBookingInStateRS bookingResponse =
								cancellationManager.submitCancelRequest(bookingDetailsResponse.getData());
						cancellationManager.setAirlineCancellationFee(bookingResponse, bookingSegments, false);
						double airlineCancellationFee = AirCancelUtils.getAirCancellationFees(bookingSegments);
						if (!isFareDiff(airlineCancellationFee)) {
							cancellationManager.commitBooking(AirAsiaDotRezConstants.PUT);
							isCancelled = true;
						}
					}
				}
			}
		} finally {
			if (!isCancelled) {
				String message = StringUtils
						.join("Cancellation Failed for " + getSupplierConf().getBasicInfo().getSupplierName());
				criticalMessageLogger.add(message);
				log.error("{} for bookingid {}", message, bookingId);
			}
		}
		return isCancelled;
	}

	private String dividePNR() {
		AirAsiaDotRezTokenManager tokenManager = AirAsiaDotRezTokenManager.builder().bindingService(bindingService)
				.listener(listener).bookingId(bookingId).supplierConfig(supplierConf).build();
		tokenId = tokenManager.createToken();
		if (StringUtils.isNotEmpty(tokenId)) {
			AirAsiaDotRezBookingRetrieveManager bookingRetrieveManager = AirAsiaDotRezBookingRetrieveManager.builder()
					.listener(listener).bindingService(bindingService).supplierConfig(supplierConf)
					.listenerKey(bookingId).bookingId(bookingId).sessionId(tokenId).pnr(pnr).build();
			SellTripRS bookingDetailsResponse = bookingRetrieveManager.getBookingDetailsResponse();
			if (bookingDetailsResponse != null && bookingDetailsResponse.getData() != null) {
				AirAsiaDotRezCancellationManager cancellationManager = AirAsiaDotRezCancellationManager.builder()
						.listener(listener).bindingService(bindingService).bookingId(bookingId).sessionId(tokenId)
						.pnr(pnr).supplierConfig(supplierConf).bookingSegments(bookingSegments).build();
				cancellationManager.init();
				boolean isCancel =
						cancellationManager.isCancelled(bookingDetailsResponse.getData().getInfo().getStatus());
				if (!isCancel) {
					cancellationManager
							.setPassengerKeys(cancellationManager.createPassengerKeys(bookingDetailsResponse));
					return cancellationManager.dividePNR(bookingDetailsResponse);
				}
			}
		}
		return null;
	}

	private boolean isSegmentCancellation() {
		List<TripInfo> cancellationTrips =
				AirCancelUtils.createTripListFromSegmentListCancellation(bookingSegments.getCancelSegments());
		List<TripInfo> originalTripsInBooking =
				AirCancelUtils.createTripListFromSegmentListCancellation(bookingSegments.getSegmentInfos());
		for (TripInfo cancelTrip : cancellationTrips) {
			String tripKey = cancelTrip.getTripKey();
			boolean isTripKeyFound = false;
			for (TripInfo originalTrip : originalTripsInBooking) {
				if (tripKey.equals(originalTrip.getTripKey())) {
					isTripKeyFound = true;
				}
			}
			if (!isTripKeyFound) {
				log.error("Error - SegmentWise cancellation not allowed for booking id {}", bookingId);
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isSupplierCancellationAllowed(Integer sourceId) {
		boolean isCancellationAllowed = false;
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(bookingSegments.getSegmentInfos());
		LocalDateTime departureTime = this.bookingSegments.getCancelSegments().get(0).getDepartTime();
		String fareType = this.bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getFareType();
		AirType airType = AirUtils.getAirType(tripInfo);
		if (cancelConfiguration != null) {
			isCancellationAllowed =
					AirCancelUtils.isBasedOnFareTypeAllowed(cancelConfiguration, fareType, criticalMessageLogger)
							&& AirCancelUtils.isDependsOnDepartureTime(cancelConfiguration, airType, departureTime,
									fareType, criticalMessageLogger);
		}
		boolean isPassengerWiseCancellation = isPassengerCancellation();
		boolean isSegmentCancellation = isSegmentCancellation();
		if (isPassengerWiseCancellation || isSegmentCancellation) {
			isCancellationAllowed = isPassengerWiseCancellationAllowed(sourceId) && !isSegmentCancellation;
			if (!isCancellationAllowed && !isPassengerWiseCancellationAllowed(sourceId)) {
				criticalMessageLogger.add("PassengerWise Cancellation not allowed");
			}
			if (isSegmentCancellation) {
				criticalMessageLogger.add("SegmentWise Cancellation not allowed");
			}
		}
		return isCancellationAllowed;
	}

	private boolean isPassengerCancellation() {
		boolean isPassengerWiseCancellation = false;
		for (int index = 0; index < bookingSegments.getCancelSegments().size(); index++) {
			SegmentInfo cancelledSegment = bookingSegments.getCancelSegments().get(index);
			SegmentInfo originalSegment = bookingSegments.getSegmentInfos().get(index);
			if (cancelledSegment.getTravellerInfo().size() != originalSegment.getTravellerInfo().size()) {
				isPassengerWiseCancellation = true;
				break;
			}
		}
		return isPassengerWiseCancellation;
	}

	@Override
	protected boolean isPassengerWiseCancellationAllowed(Integer sourceId) {
		return true;
	}

}
