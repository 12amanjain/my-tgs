package com.tgs.services.fms.sources.airasiadotrez;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.airasia.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirAsiaDotRezAirInfoFactory extends AbstractAirInfoFactory {

	protected RestAPIListener listener;

	protected AirAsiaURLBindingService bindingService;

	public AirAsiaDotRezAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener(StringUtils.EMPTY);
		}
		bindingService = AirAsiaURLBindingService.builder().configuration(supplierConf)
				.apiUrls(AirUtils.getAirEndPointURL(sourceConfiguration)).cachingComm(cachingComm).build();
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		AirAsiaDotRezTokenManager tokenManager = null;
		String token = null;
		try {
			listener.setKey(searchQuery.getSearchId());
			tokenManager = AirAsiaDotRezTokenManager.builder().listener(listener).searchQuery(searchQuery)
					.bindingService(bindingService).criticalMessageLogger(criticalMessageLogger).bookingUser(user)
					.supplierConfig(supplierConf).build();
			token = bindingService.getAuthToken();
			if (StringUtils.isBlank(token)) {
				token = tokenManager.createToken();
			}
			AirAsiaDotRezSearchManager searchManager = AirAsiaDotRezSearchManager.builder().listener(listener)
					.searchQuery(searchQuery).bindingService(bindingService).bookingUser(user)
					.moneyExchnageComm(moneyExchnageComm).supplierConfig(supplierConf).sessionId(token)
					.criticalMessageLogger(criticalMessageLogger).sourceConfiguration(sourceConfiguration).build();
			searchManager.init();
			searchResult = searchManager.doSearch();
			searchResult = searchManager.setInfantPrice(searchResult);
		} catch (SupplierRemoteException e) {
			token = null;
			throw e;
		} finally {
			if (StringUtils.isNotEmpty(token)) {
				bindingService.storeToken(token, tokenManager.sessionValidMinutes);
			}
		}
	}


	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize();
		TripInfo tripInfo = null;
		String sessionId = "";
		try {
			listener.setKey(bookingId);
			AirAsiaDotRezTokenManager tokenManager = AirAsiaDotRezTokenManager.builder().searchQuery(searchQuery)
					.listener(listener).bookingUser(user).supplierConfig(supplierConf)
					.criticalMessageLogger(criticalMessageLogger).bindingService(bindingService).build();
			sessionId = tokenManager.createToken();
			AirAsiaDotRezReviewManager reviewManager = AirAsiaDotRezReviewManager.builder().sessionId(sessionId)
					.searchQuery(searchQuery).bookingUser(user).bindingService(bindingService)
					.moneyExchnageComm(moneyExchnageComm).supplierConfig(supplierConf)
					.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
					.bookingId(bookingId).listener(listener).build();
			AirAsiaDotRezSSRManager ssrManager = AirAsiaDotRezSSRManager.builder().sessionId(sessionId)
					.listener(listener).bookingUser(user).searchQuery(searchQuery)
					.infantFare(selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.INFANT))
					.moneyExchnageComm(moneyExchnageComm).bindingService(bindingService)
					.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
					.bookingId(bookingId).supplierConfig(supplierConf).build();
			reviewManager.init();
			tripInfo = reviewManager.reviewTrip(selectedTrip);
			ssrManager.init();
			if (CollectionUtils.isNotEmpty(reviewManager.passengerKeys)) {
				ssrManager.getSSRAvailability(selectedTrip, reviewManager.passengerKeys);
			}
			ssrManager.setInfantPriceOnReview(Arrays.asList(tripInfo));
		} finally {
			if (tripInfo != null && StringUtils.isNotBlank(sessionId)) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(sessionId).build();
				storeBookingSession(bookingId, sessionInfo, null, tripInfo);
			}
		}
		return tripInfo;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		initialize();
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		SupplierSession session = null;
		try {
			listener.setKey(bookingId);
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(tripInfo.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			AirAsiaDotRezSSRManager seatMapManager =
					AirAsiaDotRezSSRManager.builder().criticalMessageLogger(criticalMessageLogger)
							.sessionId(session.getSupplierSessionInfo().getSessionToken()).listener(listener)
							.searchQuery(searchQuery).sourceConfiguration(sourceConfiguration)
							.moneyExchnageComm(moneyExchnageComm).bindingService(bindingService).bookingId(bookingId)
							.bookingUser(user).supplierConfig(supplierConf).build();
			List<TripInfo> tripInfos = tripInfo.splitTripInfo(false);
			for (TripInfo trip : tripInfos) {
				seatMapManager.buildSeatMap(trip, tripSeatMap);
			}
		} catch (Exception e) {
			log.error("Unable to fetch seat map for trip {}", tripInfo.toString(), e);
		}
		return tripSeatMap;
	}

}
