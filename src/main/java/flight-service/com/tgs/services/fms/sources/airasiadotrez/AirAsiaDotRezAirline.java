package com.tgs.services.fms.sources.airasiadotrez;

import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.airasia.AirAsiaUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

@Slf4j
@Getter
enum AirAsiaDotRezAirline {

	AIRASIA();


	AirAsiaDotRezAirline() {

	}

	public static final String PASS_DOC_TYPE_CODE = "P";

	public static final String FF_DOC_TYPE_CODE = "OAFF";
	
	public String getGSTTypeCode() {
		return "G";
	}

	public String getContactTypeCode() {
		return "P";
	}

	public String getPaymentMethod() {
		return "AG";
	}

	public String getTitle(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())) {
			return "CHD";
		} else if (travellerInfo.getPaxType().equals(PaxType.INFANT)) {
			return StringUtils.EMPTY;
		}
		return travellerInfo.getTitle().toUpperCase();
	}

	/**
	 * @implSpec : In case first name is blank we can use last name same for first name
	 * @implSpec : last name cannot be empty, for more contact ashu sir before making changes
	 */
	public String getFirstName(FlightTravellerInfo flightTravellerInfo) {
		if (StringUtils.isBlank(flightTravellerInfo.getFirstName())) {
			return flightTravellerInfo.getLastName();
		}
		return flightTravellerInfo.getFirstName();
	}

	/* SSR Applicable on All segment */
	protected boolean isSSRApplicableOnAllSegment(SegmentInfo segmentInfo) {
		return true;
	}

	public boolean isAdditionalSSRRequired(List<SegmentInfo> segments) {
		return AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "HF")
				|| AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "CL")
				|| AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "FS");
	}

	public List<String> getAdditionalSSRCodes(List<SegmentInfo> segments) {
		List<String> additionalSSRList = new ArrayList<>();
		if (AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "HF"))
			additionalSSRList.add("HFL");
		if (AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "CL"))
			additionalSSRList.add("GCPL");
		if (AirAsiaUtils.isProductClassRequireSSR(segments.get(0), "FS"))
			additionalSSRList.add("GCP");
		return additionalSSRList;
	}

	public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration) {
		if (productClass.equalsIgnoreCase("EC")) {
			priceInfo.setFareIdentifier(FareType.PUBLISHED);
		} else if (productClass.equalsIgnoreCase("HF")) {
			priceInfo.setFareIdentifier(FareType.PREMIUM_FLEX);
		} else if (productClass.equalsIgnoreCase("PM")) {
			priceInfo.setFareIdentifier(FareType.PREMIUM_FLATBED);
		} else if (productClass.equalsIgnoreCase("EP")) {
			priceInfo.setFareIdentifier(FareType.SALE);
		} else if (productClass.equalsIgnoreCase("CL")) {
			priceInfo.setFareIdentifier(FareType.CORPORATE_LITE);
		} else if (productClass.equalsIgnoreCase("FS")) {
			priceInfo.setFareIdentifier(FareType.CORPORATE_FLEX);
		}
		if (configuration.isTestCredential() && StringUtils.isNotBlank(priceInfo.getAccountCode())) {
			priceInfo.setFareIdentifier(FareType.PROMO);
		}
	}

	public boolean isInfantFareOnSegmentWise(SegmentInfo segmentInfo) {
		return true;
	}

	public RouteInfo getCurrencyCode(AirSearchQuery searchQuery) {
		if (searchQuery != null) {
			return searchQuery.getRouteInfos().get(0);
		}
		return null;
	}

	public List<String> passThruLegCodes() {
		return Arrays.asList("PBPB");
	}

	public Double getSSRAmount(SegmentInfo segmentInfo, String code, Double amount) {
		if (passThruLegCodes().contains(code) && segmentInfo.getSegmentNum() > 0) {
			return new Double(0);
		}
		if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
			return amount;
		}
		return null;
	}

	public boolean isPassThruBaggageAdded(List<SegmentInfo> segmentInfos) {
		AtomicBoolean isAdded = new AtomicBoolean();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getTravellerInfo() != null) {
				segmentInfo.getTravellerInfo().forEach(travellerInfo -> {
					if (!isAdded.get() && travellerInfo.getSsrBaggageInfo() != null
							&& StringUtils.isNotBlank(travellerInfo.getSsrBaggageInfo().getCode())
							&& passThruLegCodes().contains(travellerInfo.getSsrBaggageInfo().getCode())) {
						isAdded.set(true);
					}
				});
			}
		});
		return isAdded.get();
	}
}
