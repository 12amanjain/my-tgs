package com.tgs.services.fms.sources.airasiadotrez;

import java.util.HashMap;
import java.util.Map;
import com.airasia.datamodel.SellTripData;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirAsiaDotRezBookingFactory extends AbstractAirBookingFactory {

	protected RestAPIListener listener = null;

	protected AirAsiaURLBindingService bindingService;

	public AirAsiaDotRezBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}


	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener(bookingId);
		}
		bindingService = AirAsiaURLBindingService.builder().configuration(supplierConf)
				.apiUrls(AirUtils.getAirEndPointURL(sourceConfiguration)).cachingComm(cachingComm).build();
	}

	@Override
	public boolean doBooking() {
		Map<String, String> passengersKeyMap = new HashMap<String, String>();;
		boolean isSuccess = false;
		initialize();
		String sessionId = supplierSession.getSupplierSessionInfo().getSessionToken();
		AirAsiaDotRezBookingManager bookingManager = AirAsiaDotRezBookingManager.builder().sessionId(sessionId)
				.passengersKeyMap(passengersKeyMap).criticalMessageLogger(criticalMessageLogger)
				.moneyExchnageComm(moneyExchangeComm).bookingUser(bookingUser).order(order).bookingId(bookingId)
				.passengersKeyMap(passengersKeyMap).deliveryInfo(deliveryInfo).sourceConfiguration(sourceConfiguration)
				.segmentInfos(bookingSegments.getSegmentInfos()).bindingService(bindingService)
				.supplierConfig(supplierConf).listener(listener).pnr(pnr).build();
		bookingManager.init();
		bookingManager.initializeTravellerInfo(bookingSegments.getSegmentInfos().get(0).getTravellerInfo());
		bookingManager.addContactInfo();
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			bookingManager.addGSTInfo(gstInfo);
		}
		bookingManager.setPassengerKeys();
		bookingManager.addPassengers();
		bookingManager.sellSsrs();
		bookingManager.getBookingInState();
		if (!isFareDiff(bookingManager.totalAmount)) {
			if (!isHoldBooking) {
				bookingManager.addPaymentToBooking();
			}
			bookingManager.commitBooking(AirAsiaDotRezConstants.POST);
			SellTripData tripData = bookingManager.getBookingPnr();
			pnr = tripData.getRecordLocator();
			// status 2 denotes booking success
			if (StringUtils.isNotEmpty(tripData.getRecordLocator()) && tripData.getInfo().getStatus() == 2) {
				isSuccess = true;
			}
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		initialize();
		String sessionId = "";
		listener.setKey(bookingId);
		AirAsiaDotRezTokenManager tokenManager = AirAsiaDotRezTokenManager.builder().bookingId(bookingId)
				.listener(listener).supplierConfig(supplierConf).bindingService(bindingService).build();
		sessionId = tokenManager.createToken();
		AirAsiaDotRezBookingManager bookingManager = AirAsiaDotRezBookingManager.builder().sessionId(sessionId)
				.bindingService(bindingService).criticalMessageLogger(criticalMessageLogger).order(order)
				.bookingId(bookingId).deliveryInfo(deliveryInfo).moneyExchnageComm(moneyExchangeComm)
				.sourceConfiguration(sourceConfiguration).segmentInfos(bookingSegments.getSegmentInfos())
				.listener(listener).pnr(pnr).build();
		bookingManager.getBookingInState();
		if (!isFareDiff(bookingManager.totalAmount)) {
			bookingManager.addPaymentToBooking();
			bookingManager.commitBooking(AirAsiaDotRezConstants.POST);
			SellTripData tripData = bookingManager.getBookingPnr();
			pnr = tripData.getRecordLocator();
			// status 2 denotes booking success
			if (StringUtils.isNotEmpty(tripData.getRecordLocator()) && tripData.getInfo().getStatus() == 2) {
				isSuccess = true;
			}
		}
		return isSuccess;
	}


}
