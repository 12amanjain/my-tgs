package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import com.airasia.datamodel.*;
import com.tgs.services.fms.datamodel.TravellerMiscInfo;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.sources.airasia.AirAsiaUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierPaymentException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class AirAsiaDotRezBookingManager extends AirAsiaDotRezServiceManager {

	protected DeliveryInfo deliveryInfo;
	private Order order;
	private List<SegmentInfo> segmentInfos;

	private LocalDateTime ticketTimeLimit;

	public void addContactInfo() {
		HttpUtils httpUtils = null;
		try {
			AddContactRQ contactRequest = getContactRequestByType(AirAsiaDotRezConstants.PHONE_CONTACT_TYPE, null);
			httpUtils = HttpUtils.builder().headerParams(headerParams())
					.postData(GsonUtils.getGson().toJson(contactRequest)).proxy(proxy())
					.urlString(bindingService.contactUrl()).build();
			AddContactRS response = httpUtils.getResponse(AddContactRS.class).orElse(null);
			if (isAnyError(response)) {
				throw new SupplierUnHandledFaultException("Contact Info Failed");
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "AddContactRQ"),
					formatRQRS(httpUtils.getResponseString(), "AddContactRS"), "SessionId : ", sessionId);
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("5-AddContact").build());
		}
	}

	private AddContactRQ getContactRequestByType(String contactType, GstInfo gstInfo) {
		AddContactRQ addContactRequest = AddContactRQ.builder().build();
		addContactRequest.setContactTypeCode(contactType);
		addContactRequest.setPhoneNumbers(getPhoneNumbers(contactType, gstInfo));
		addContactRequest.setAddress(getContactAddress());
		addContactRequest.setEmailAddress(AirSupplierUtils.getEmailId(deliveryInfo));
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			addContactRequest.setCustomerNumber(gstInfo.getGstNumber());
			addContactRequest.setCompanyName(gstInfo.getRegisteredName());
			if (StringUtils.isNotBlank(gstInfo.getEmail())) {
				addContactRequest.setEmailAddress(gstInfo.getEmail());
			}
		}
		addContactRequest.setName(getPassengerName(segmentInfos.get(0).getTravellerInfo().get(0)));
		return addContactRequest;
	}

	private PassengerName getPassengerName(FlightTravellerInfo travellerInfo) {
		PassengerName passengername = PassengerName.builder().build();
		passengername.setFirst(dotRezAirline.getFirstName(travellerInfo));
		passengername.setLast(travellerInfo.getLastName());
		passengername.setTitle(dotRezAirline.getTitle(travellerInfo));
		return passengername;
	}

	private ContactAddress getContactAddress() {
		ContactAddress contactAddress = ContactAddress.builder().build();
		String countryCode, postalCode;
		ClientGeneralInfo clientInfoCriteria = ServiceCommunicatorHelper.getClientInfo();
		postalCode = clientInfoCriteria.getPostalCode();
		countryCode = clientInfoCriteria.getNationality();
		AddressInfo contactAdd = ServiceUtils.getContactAddressInfo(order.getBookingUserId(), clientInfoCriteria);
		String addressLine1 = contactAdd.getAddress();
		String city = contactAdd.getCityInfo().getName();
		contactAddress.setCountryCode(countryCode);
		contactAddress.setPostalCode(postalCode);
		contactAddress.setLineOne(addressLine1);
		contactAddress.setCity(city);
		return contactAddress;
	}

	private List<PhoneNumber> getPhoneNumbers(String contactType, GstInfo gstInfo) {
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		List<PhoneNumber> phoneNumbers = new ArrayList<>();
		if (AirAsiaDotRezConstants.PHONE_CONTACT_TYPE.equals(contactType)) {
			phoneNumbers.add(getPhoneNumber("HOME",
					AirAsiaUtils.getContact(clientInfo.getCountryCode(), clientInfo.getWorkPhone())));
			phoneNumbers.add(getPhoneNumber(AirAsiaDotRezConstants.OTHER_PHONE,
					AirSupplierUtils.getContactNumberWithCountryCode(deliveryInfo)));
		} else if (AirAsiaDotRezConstants.GST_CONTACT_TYPE.equals(contactType)) {
			phoneNumbers.add(getPhoneNumber(AirAsiaDotRezConstants.OTHER_PHONE,
					AirSupplierUtils.getContactNumberWithCountryCode(gstInfo.getMobile())));
			phoneNumbers.add(getPhoneNumber(AirAsiaDotRezConstants.HOME_PHONE,
					AirSupplierUtils.getContactNumberWithCountryCode(gstInfo.getMobile())));
		}
		return phoneNumbers;
	}

	private PhoneNumber getPhoneNumber(String type, String number) {
		PhoneNumber phonenumber = PhoneNumber.builder().build();
		phonenumber.setType(type);
		phonenumber.setNumber(number);
		return phonenumber;
	}

	public void addPassengers() {
		HttpUtils httpUtils = null;
		// Url will be appended with the passenger key, and in one request 1 passenger need to be addded
		for (FlightTravellerInfo travellerInfo : segmentInfos.get(0).getTravellerInfo()) {
			String paxKey = travellerInfo.getMiscInfo().getPassengerKey();
			try {
				AddPassengerRQ passengerRequest = getPassengerRequest(travellerInfo);
				httpUtils = HttpUtils.builder().headerParams(headerParams())
						.postData(GsonUtils.getGson().toJson(passengerRequest))
						.requestMethod(AirAsiaDotRezUtils.getRequestMethod(travellerInfo.getPaxType())).proxy(proxy())
						.urlString(
								bindingService.passengerUrl(paxKey, travellerInfo.getPaxType().equals(PaxType.INFANT)))
						.build();
				AddPassengerRS response = httpUtils.getResponse(AddPassengerRS.class).orElse(null);
				if (isAnyError(response)) {
					throw new SupplierUnHandledFaultException("Passenger Adding Failed");
				}
			} catch (IOException e) {
				throw new SupplierRemoteException(e);
			} finally {
				String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "AddPassengerRQ"),
						formatRQRS(httpUtils.getResponseString(), "AddPassengerRS"));
				listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
						.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("6-AddPassenger").build());
			}
		}
	}


	private AddPassengerRQ getPassengerRequest(FlightTravellerInfo travellerInfo) {
		AddPassengerRQ request = AddPassengerRQ.builder().build();
		if (!PaxType.INFANT.equals(travellerInfo.getPaxType())) {
			request.setInfo(getPassengerInfo(travellerInfo));
		} else {
			if (travellerInfo.getDob() != null) {
				request.setDateOfBirth(travellerInfo.getDob().toString());
			}
			request.setGender(AirAsiaDotRezUtils.getGender(travellerInfo.getTitle(), travellerInfo));
			request.setNationality(AirAsiaDotRezUtils.getNationality(travellerInfo));
			request.setResidentCountry(AirAsiaDotRezUtils.getNationality(travellerInfo));

		}
		request.setName(getPassengerName(travellerInfo));
		return request;
	}

	private PassengerInfo getPassengerInfo(FlightTravellerInfo travellerInfo) {
		PassengerInfo passengerInfo = PassengerInfo.builder().build();
		if (travellerInfo.getDob() != null) {
			passengerInfo.setDateOfBirth(travellerInfo.getDob().toString());
		}
		passengerInfo.setGender(AirAsiaDotRezUtils.getGender(travellerInfo.getTitle(), travellerInfo));
		passengerInfo.setNationality(AirAsiaDotRezUtils.getNationality(travellerInfo));
		passengerInfo.setResidentCountry(AirAsiaDotRezUtils.getNationality(travellerInfo));
		return passengerInfo;
	}

	public void sendSellSSRRequest(String ssrkey) {
		HttpUtils httpUtils = null;
		try {
			SellSsrRQ sellSsrRequests = getSsrRequest();
			httpUtils = HttpUtils.builder().headerParams(headerParams())
					.postData(GsonUtils.getGson().toJson(sellSsrRequests)).proxy(proxy())
					.urlString(bindingService.sellSSRUrl(ssrkey)).build();
			SellSsrRS response = httpUtils.getResponse(SellSsrRS.class).orElse(null);
		} catch (IOException e) {
			log.error("Error Occured due to {}", e.getMessage());

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SellSsrRQ"),
					formatRQRS(httpUtils.getResponseString(), "SellSsrRS"), "URL: ", bindingService.sellSSRUrl(ssrkey),
					" SsrKey :", ssrkey);
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("7-SellSsr").build());
		}
	}

	private SellSsrRQ getSsrRequest() {
		SellSsrRQ ssrRq = SellSsrRQ.builder().build();
		ssrRq.setCurrencyCode(AirSourceConstants.getSupplierInfoCurrencyCode(supplierConfig));
		ssrRq.setForceWaveOnSell(false);
		ssrRq.setCount(1);
		ssrRq.setNote("DevTest");
		return ssrRq;
	}


	public void setPassengerKeys() {
		GetBookingInStateRS bookingInStateResponse = getBookingInState();
		Map<String, PassengerDetail> passengersMap = bookingInStateResponse.getData().getPassengers();
		AtomicInteger adultIndex = new AtomicInteger();
		AtomicInteger childIndex = new AtomicInteger();
		passengersMap.forEach((passengerKey, passengerDetail) -> {
			PaxType paxType = getPaxType(passengerDetail.getPassengerTypeCode());
			if (StringUtils.equalsIgnoreCase(paxType.getType(), "ADT")) {
				passengersKeyMap.put(StringUtils.join("ADULT", adultIndex.get()), passengerKey);
				adultIndex.getAndIncrement();
			} else {
				passengersKeyMap.put(StringUtils.join("CHILD", childIndex.get()), passengerKey);
				childIndex.getAndIncrement();
			}
		});
		int adult_Index = 0;
		int child_Index = 0;
		int infant_Index = 0;
		for (String key : passengersMap.keySet()) {
			PassengerDetail detail = passengersMap.get(key);
			PaxType paxType = getPaxType(detail.getPassengerTypeCode());
			if (PaxType.ADULT.equals(paxType)) {
				FlightTravellerInfo travellerInfo = adults.get(adult_Index);
				TravellerMiscInfo miscInfo = travellerInfo.getMiscInfo();
				if (miscInfo == null) {
					miscInfo = new TravellerMiscInfo();
				}
				miscInfo.setPassengerKey(key);
				travellerInfo.setMiscInfo(miscInfo);
				if (CollectionUtils.isNotEmpty(infants) && infant_Index < infantCount) {
					FlightTravellerInfo infantTravellerInfo = infants.get(infant_Index);
					infantTravellerInfo.setMiscInfo(miscInfo);
					this.setPassengerKey(infantTravellerInfo);
					infant_Index++;
				}
				this.setPassengerKey(travellerInfo);
				adult_Index++;
			} else if (PaxType.CHILD.equals(paxType)) {
				FlightTravellerInfo travellerInfo = children.get(child_Index);
				TravellerMiscInfo miscInfo = travellerInfo.getMiscInfo();
				if (miscInfo == null) {
					miscInfo = new TravellerMiscInfo();
				}
				miscInfo.setPassengerKey(key);
				travellerInfo.setMiscInfo(miscInfo);
				this.setPassengerKey(travellerInfo);
				child_Index++;
			}
		}
	}

	private void setPassengerKey(FlightTravellerInfo travellerInfo) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			TravellerMiscInfo miscInfo = travellerInfo.getMiscInfo();
			Optional<FlightTravellerInfo> passenger =
					AirUtils.findTraveller(segmentInfo.getTravellerInfo(), travellerInfo);
			if (passenger.isPresent()) {
				passenger.get().setMiscInfo(miscInfo);
			}
		}
	}

	public void addPaymentToBooking() {
		HttpUtils httpUtils = null;
		try {
			AddPaymentRQ paymentRequest = buildPaymentRequest();
			httpUtils = HttpUtils.builder().headerParams(headerParams()).proxy(proxy())
					.postData(GsonUtils.getGson().toJson(paymentRequest)).urlString(bindingService.paymentUrl())
					.build();
			BaseResponse response = httpUtils.getResponse(BaseResponse.class).orElse(null);
			if (isAnyError(response)) {
				throw new SupplierUnHandledFaultException("Supplier Payment Failed");
			}
		} catch (IOException e) {
			throw new SupplierPaymentException(e.getMessage());
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "AddPaymentRQ"),
					formatRQRS(httpUtils.getResponseString(), "AddPaymentRQ"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("8-AddPayment").build());
		}
	}

	private AddPaymentRQ buildPaymentRequest() {
		AddPaymentRQ paymentRequest = AddPaymentRQ.builder().build();
		paymentRequest.setAmount(totalAmount);
		paymentRequest.setCurrencyCode(getCurrencyCode());
		paymentRequest.setPaymentMethodCode(dotRezAirline.getPaymentMethod());
		paymentRequest.setInstallments(1);
		paymentRequest.setPaymentFields(getPaymentFields());
		return paymentRequest;
	}

	private PaymentFields getPaymentFields() {
		PaymentFields paymentField = PaymentFields.builder().build();
		paymentField.setACCTNO(supplierConfig.getSupplierCredential().getOrganisationCode());
		paymentField.setAMT(String.valueOf(totalAmount));
		return paymentField;
	}

	public void sellSsrs() {
		for (SegmentInfo segmentInfo : segmentInfos) {
			String journeyRef = segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey();
			int travIndex = 0;
			for (FlightTravellerInfo traveller : segmentInfo.getTravellerInfo()) {
				String ssrKey = "";
				// amount will be present only in the first segment of a journey, and that is for complete journey
				if (traveller.getSsrBaggageInfo() != null && traveller.getSsrBaggageInfo().getAmount() != null) {
					ssrKey = traveller.getSsrBaggageInfo().getMiscInfo().getPaxSsrKeys().get(travIndex);
					sendSellSSRRequest(ssrKey);
				}
				if (traveller.getSsrMealInfo() != null) {
					ssrKey = traveller.getSsrMealInfo().getMiscInfo().getPaxSsrKeys().get(travIndex);
					sendSellSSRRequest(ssrKey);
				}
				if (traveller.getSsrSeatInfo() != null) {
					ssrKey = traveller.getSsrSeatInfo().getMiscInfo().getSeatCodeType();
					sendSeatSSRRequest(journeyRef, traveller.getMiscInfo().getPassengerKey(), ssrKey);
				}
				travIndex++;
			}
		}
	}

	private void sendSeatSSRRequest(String tripRef, String paxKey, String ssrKey) {
		HttpUtils httpUtils = null;
		try {
			SeatAssignementRQ seatAssignementRQ = buildSeatAssignmentRequest(tripRef);
			httpUtils = HttpUtils.builder().headerParams(headerParams())
					.postData(GsonUtils.getGson().toJson(seatAssignementRQ)).proxy(proxy())
					.urlString(bindingService.sellSeatUrl(paxKey, ssrKey)).build();
			SellSsrRS response = httpUtils.getResponse(SellSsrRS.class).orElse(null);
			if (isAnyError(response)) {
				throw new SupplierUnHandledFaultException("Seat Assignment Failed");
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SellSeatRS"),
					formatRQRS(httpUtils.getResponseString(), "SellSeatRS"), "URL : ",
					bindingService.sellSeatUrl(paxKey, ssrKey), " SsrKey :", ssrKey);
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("7-SeatSellSsr").build());
		}
	}

	private SeatAssignementRQ buildSeatAssignmentRequest(String tripRef) {
		SeatAssignementRQ seatAssignementRQ = SeatAssignementRQ.builder().build();
		seatAssignementRQ.setJourneyKey(tripRef);
		seatAssignementRQ.setIgnoreSeatSsrs(true);
		seatAssignementRQ.setWaiveFee(false);
		seatAssignementRQ.setInventoryControl(AirAsiaDotRezConstants.Seat_Inventory_Control);
		seatAssignementRQ.setSeatAssignmentMode(AirAsiaDotRezConstants.SeatAssignment_Mode);
		seatAssignementRQ.setCollectedCurrencyCode(AirSourceConstants.getSupplierInfoCurrencyCode(supplierConfig));
		return seatAssignementRQ;
	}

	public void addGSTInfo(GstInfo gstInfo) {
		HttpUtils httpUtils = null;
		AddContactRS response = null;
		AddContactRQ gstRequest = null;
		try {
			gstRequest = getContactRequestByType(AirAsiaDotRezConstants.GST_CONTACT_TYPE, gstInfo);
			httpUtils =
					HttpUtils.builder().headerParams(headerParams()).postData(GsonUtils.getGson().toJson(gstRequest))
							.proxy(proxy()).urlString(bindingService.contactUrl()).build();
			response = httpUtils.getResponse(AddContactRS.class).orElse(null);
			if (isAnyError(response)) {
				throw new SupplierUnHandledFaultException("GST Info Failed");
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "AddGSTRQ"),
					formatRQRS(httpUtils.getResponseString(), "AddGSTRS"), "SessionId : ", sessionId);
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("GSTInfo").build());
		}
	}
}
