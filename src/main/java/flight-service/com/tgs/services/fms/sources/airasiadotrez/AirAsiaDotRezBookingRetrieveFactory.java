package com.tgs.services.fms.sources.airasiadotrez;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;

@Service
public class AirAsiaDotRezBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	protected RestAPIListener listener = null;

	protected FlightAPIURLRuleCriteria apiUrls;

	protected String tokenId;

	protected AirAsiaURLBindingService bindingService;

	public AirAsiaDotRezBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener(pnr);
		}
		bindingService = AirAsiaURLBindingService.builder().configuration(supplierConf)
				.apiUrls(AirUtils.getAirEndPointURL(sourceConfiguration)).build();
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		AirAsiaDotRezTokenManager tokenManager = AirAsiaDotRezTokenManager.builder().listener(listener)
				.bindingService(bindingService).bookingId(pnr).supplierConfig(supplierConf).build();
		tokenId = tokenManager.createToken();
		if (StringUtils.isNotEmpty(tokenId)) {
			AirAsiaDotRezBookingRetrieveManager bookingRetrieveManager = AirAsiaDotRezBookingRetrieveManager.builder()
					.listener(listener).listenerKey(pnr).supplierConfig(supplierConf).bindingService(bindingService)
					.sessionId(tokenId).pnr(pnr).build();
			pnrBooking = bookingRetrieveManager.getPnrDetails(bookingRetrieveManager.getBookingDetailsResponse());

		} else {
			throw new SupplierRemoteException("Error: Token generation failure");
		}
		return pnrBooking;
	}


}
