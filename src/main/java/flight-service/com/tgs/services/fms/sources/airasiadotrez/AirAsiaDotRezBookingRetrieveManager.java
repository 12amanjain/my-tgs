package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.airasia.datamodel.AvailableJourney;
import com.airasia.datamodel.ContactDetails;
import com.airasia.datamodel.Infant;
import com.airasia.datamodel.Leg;
import com.airasia.datamodel.PassengerDetail;
import com.airasia.datamodel.PassengerFare;
import com.airasia.datamodel.PassengerSsr;
import com.airasia.datamodel.PassengerSsrFee;
import com.airasia.datamodel.Seats;
import com.airasia.datamodel.Segment;
import com.airasia.datamodel.SellTripRS;
import com.airasia.datamodel.ServiceCharge;
import com.airasia.datamodel.TypeOfSale;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class AirAsiaDotRezBookingRetrieveManager extends AirAsiaDotRezServiceManager {

	protected Map<String, PassengerDetail> passengerDetails;
	protected String supplierBookingId;
	protected String listenerKey;

	public AirImportPnrBooking getPnrDetails(SellTripRS responseBody) {
		AirImportPnrBooking pnrBooking = null;
		init();
		if (responseBody != null && responseBody.getData() != null) {
			pnrBooking = AirImportPnrBooking.builder()
					.deliveryInfo(getDeliveryInfo(responseBody.getData().getContacts()))
					.gstInfo(getGstInfo(responseBody.getData().getContacts())).tripInfos(getTripInfos(responseBody))
					.pnr(pnr).supplierId(supplierConfig.getSupplierId()).build();
		}
		return pnrBooking;
	}

	public SellTripRS getBookingDetailsResponse() {
		HttpUtils httpUtils = null;
		SellTripRS responseBody = null;
		try {
			httpUtils = HttpUtils.builder().headerParams(headerParams()).urlString(bindingService.retrievePNRUrl(pnr))
					.proxy(proxy()).build();
			responseBody = httpUtils.getResponse(SellTripRS.class).orElse(null);
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "RecordLocatorRQ"),
					formatRQRS(httpUtils.getResponseString(), "RecordLocatorRS"));
			listener.addLog(LogData.builder().key(listenerKey).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("RetrieveBooking").build());
		}

		return responseBody;
	}

	private DeliveryInfo getDeliveryInfo(Map<String, ContactDetails> contact) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		ContactDetails personalContact = contact.get(AirAsiaDotRezConstants.PHONE_CONTACT_TYPE);
		if (MapUtils.isNotEmpty(contact) && Objects.nonNull(personalContact)) {
			deliveryInfo.setContacts(getContacts(personalContact));
			deliveryInfo.setEmails(getEmail(contact.get("P")));
		}
		return deliveryInfo;
	}

	private List<String> getEmail(ContactDetails contactDetails) {
		ArrayList<String> email = new ArrayList<>();
		email.add(contactDetails.getEmailAddress());
		return email;
	}

	private List<String> getContacts(ContactDetails contactDetails) {
		ArrayList<String> contacts = new ArrayList<>();
		contactDetails.getPhoneNumbers().forEach(phone -> {
			contacts.add(phone.number);
		});
		return contacts;
	}


	private GstInfo getGstInfo(Map<String, ContactDetails> contacts) {
		GstInfo gstInfo = new GstInfo();
		ContactDetails gstDetail = contacts.get(AirAsiaDotRezConstants.GST_CONTACT_TYPE);
		if (Objects.nonNull(gstDetail)) {
			gstInfo.setGstNumber(gstDetail.getCustomerNumber());
			gstInfo.setEmail(gstDetail.getEmailAddress());
			if (CollectionUtils.isNotEmpty(gstDetail.getPhoneNumbers())) {
				gstInfo.setPhone(gstDetail.getPhoneNumbers().get(0).getNumber());
			}
			if (gstDetail.getAddress() != null) {
				gstInfo.setAddress(StringUtils.join(gstDetail.getAddress().getLineOne(),
						gstDetail.getAddress().getLineTwo(), gstDetail.getAddress().getLineThree()));
				gstInfo.setPincode(gstDetail.getAddress().getPostalCode());
				gstInfo.setCityName(gstDetail.getAddress().getCity());
			}
			if (gstDetail.getName() != null) {
				gstInfo.setRegisteredName(
						StringUtils.join(gstDetail.getName().getFirst(), " ", gstDetail.getName().getLast()));
			}
			gstInfo.setBillingCompanyName(gstDetail.getCompanyName());
		}
		return gstInfo;
	}

	private List<TripInfo> getTripInfos(SellTripRS responseBody) {
		ArrayList<TripInfo> trips = null;
		passengerDetails = responseBody.getData().getPassengers();
		if (Objects.nonNull(responseBody.getData().getJourneys())) {
			trips = new ArrayList<TripInfo>();
			for (int journeyIndex = 0; journeyIndex < responseBody.getData().getJourneys().size(); journeyIndex++) {
				AvailableJourney journey = responseBody.getData().getJourneys().get(journeyIndex);
				trips.add(createTripInfo(journey, responseBody));
			}
		}
		if (trips.size() == 2 && AirAsiaDotRezUtils.setIsReturnSegment(trips, bookingUser)) {
			trips.get(1).getSegmentInfos().forEach(segment -> {
				segment.setIsReturnSegment(true);
			});
		}
		return trips;
	}

	private TripInfo createTripInfo(AvailableJourney journey, SellTripRS responseBody) {
		TripInfo trip = new TripInfo();
		List<SegmentInfo> segments = new ArrayList<SegmentInfo>();
		for (int segNum = 0; segNum < journey.getSegments().size(); segNum++) {
			Segment segment = journey.getSegments().get(segNum);
			for (int legNum = 0; legNum < segment.getLegs().size(); legNum++) {
				Leg leg = segment.getLegs().get(legNum);
				segments.add(createSegmentInfo(segment, leg, segNum, journey, responseBody.getData().getTypeOfSale(),
						legNum));
			}

		}
		trip.setSegmentInfos(segments);
		return trip;
	}

	private SegmentInfo createSegmentInfo(Segment segment, Leg leg, int segNum, AvailableJourney journey,
			TypeOfSale typeOfSale, int legNum) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(leg.getDesignator().getDestination()));
		segmentInfo.getArrivalAirportInfo()
				.setTerminal(AirUtils.getTerminalInfo(leg.getLegInfo().getArrivalTerminal()));
		segmentInfo.setArrivalTime(leg.getDesignator().getArrival());
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(leg.getDesignator().getOrigin()));
		segmentInfo.getDepartAirportInfo()
				.setTerminal(AirUtils.getTerminalInfo(leg.getLegInfo().getDepartureTerminal()));
		segmentInfo.setDepartTime(leg.getDesignator().getDeparture());
		segmentInfo.setIsReturnSegment(Boolean.FALSE);
		segmentInfo.setSegmentNum(segNum);
		segmentInfo.setDuration(segmentInfo.calculateDuration());
		segmentInfo.setFlightDesignator(getFlightDesignator(segment, leg));
		List<PriceInfo> priceInfos = new ArrayList<>();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(supplierConfig.getBasicInfo());
		priceInfo.getMiscInfo().setPlatingCarrier(segmentInfo.getFlightDesignator().getAirlineInfo());
		if (leg.getLegInfo() != null && StringUtils.isNotBlank(leg.getLegInfo().getOperatingCarrier())
				&& !(leg.getLegInfo().getOperatingCarrier()
						.equals(segmentInfo.getFlightDesignator().getAirlineInfo().getCode()))) {
			AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(leg.getLegInfo().getOperatingCarrier());
			segmentInfo.setOperatedByAirlineInfo(operatingAirline);
			priceInfo.getMiscInfo().setPlatingCarrier(operatingAirline);
		}
		if (Objects.nonNull(typeOfSale) && StringUtils.isNotBlank(typeOfSale.getPromotionCode())) {
			priceInfo.getMiscInfo().setAccountCode(typeOfSale.getPromotionCode());
		}
		priceInfo.setFareIdentifier(FareType.PUBLISHED);
		if (CollectionUtils.isNotEmpty(segment.getFares())
				&& StringUtils.isNotBlank(segment.getFares().get(0).getProductClass())) {
			dotRezAirline.processPriceInfo(segment.getFares().get(0).getProductClass(), priceInfo, supplierConfig);
		}
		priceInfos.add(priceInfo);
		segmentInfo.setPriceInfoList(priceInfos);
		segmentInfo.setBookingRelatedInfo(getBookingRelatedInfo(segment, segmentInfo, legNum));
		segmentInfo.updateAirlinePnr(pnr);
		return segmentInfo;
	}

	private SegmentBookingRelatedInfo getBookingRelatedInfo(Segment segment, SegmentInfo segmentInfo, int legNum) {
		List<FlightTravellerInfo> travellerInfoList = new ArrayList<FlightTravellerInfo>();
		int passengerCount = 0;
		for (Map.Entry<String, PassengerDetail> entry : segment.getPassengerSegment().entrySet()) {
			FlightTravellerInfo traveller = new FlightTravellerInfo();
			PassengerDetail passenger = entry.getValue();
			PassengerDetail passengerDetail = passengerDetails.get(entry.getKey());
			traveller.setFirstName(passengerDetail.getName().getFirst());
			traveller.setLastName(passengerDetail.getName().getLast());
			traveller.setPaxType(PaxType.getPaxType(getType(passengerDetail.getPassengerTypeCode())));
			String title = passengerDetail.getName().getTitle();
			traveller.setTitle(getTitle(traveller.getPaxType(), title));
			if (Objects.nonNull(passengerDetail.getInfo().getDateOfBirth()))
				traveller.setDob(AirAsiaDotRezUtils.getDOB(passengerDetail.getInfo().getDateOfBirth().toString()));
			traveller.setFareDetail(
					getFareDetails(segment.getFares().get(0), passengerDetail.getPassengerTypeCode(), null, legNum));
			String segKey = segment.getFlightReference();
			if (CollectionUtils.isNotEmpty(passenger.getSeats())) {
				traveller.setSsrSeatInfo(
						getSeatSSRInfo(segKey, passenger.getSeats(), passengerDetail.getFees(), legNum));
			}
			if (CollectionUtils.isNotEmpty(passenger.getSsrs())) {
				getSSR(segmentInfo, traveller, getSSRFeesList(passenger.getSsrs(), passengerDetail.getFees()), legNum);
			}
			travellerInfoList.add(traveller);
			if (passenger.hasInfant) {
				travellerInfoList.add(
						addInfantTraveller(segKey, passengerDetail.getInfant(), ++passengerCount, segment, legNum));
			}
			passengerCount++;

		}
		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfoList).build();
		return bookingRelatedInfo;
	}

	private List<PassengerSsrFee> getSSRFeesList(List<PassengerSsr> ssrs, List<PassengerSsrFee> fees) {
		List<PassengerSsrFee> ssrList = new ArrayList<PassengerSsrFee>();
		for (PassengerSsr ssr : ssrs) {
			for (PassengerSsrFee ssrfee : fees) {
				if (ssr.getSsrCode().equalsIgnoreCase(ssrfee.getSsrCode())) {
					ssrList.add(ssrfee);
					break;
				}
			}
		}
		return ssrList;
	}

	private void getSSR(SegmentInfo segmentInfo, FlightTravellerInfo traveller, List<PassengerSsrFee> ssrFeeList,
			int legNum) {
		Map<SSRType, List<? extends SSRInformation>> preSSR =
				AirUtils.getPreSSR(getSSRFlightFact(segmentInfo, traveller.getFareDetail()));
		for (PassengerSsrFee ssrFee : ssrFeeList) {
			SSRInformation baggageSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE), ssrFee.getSsrCode());
			SSRInformation mealSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.MEAL), ssrFee.getSsrCode());
			BaggageSSRInformation baggInformation = null;
			MealSSRInformation mealInformation = null;
			String reference = ssrFee.getFlightReference();
			if (baggageSSRInfo != null) {
				baggInformation = new BaggageSSRInformation();
				if (legNum == 0)
					baggInformation.setAmount(getCharges(ssrFee.getServiceCharges()));
				baggInformation.setDesc(baggageSSRInfo.getDesc());
				baggInformation.setCode(ssrFee.getSsrCode());
				baggInformation.setKey(reference);
				traveller.setSsrBaggageInfo(baggInformation);
			} else if (mealSSRInfo != null) {
				mealInformation = new MealSSRInformation();
				mealInformation.setCode(ssrFee.getSsrCode());
				mealInformation.setDesc(mealSSRInfo.getDesc());
				mealInformation.setAmount(getCharges(ssrFee.getServiceCharges()));
				mealInformation.setKey(reference);
				traveller.setSsrMealInfo(mealInformation);
			}
		}

	}

	public static FlightBasicFact getSSRFlightFact(SegmentInfo segmentInfo, FareDetail fareDetail) {
		Integer sourceId = segmentInfo.getSupplierInfo().getSourceId();
		String productClass = fareDetail.getClassOfBooking();
		String airline = segmentInfo.getAirlineCode(false);
		FlightBasicFact flightFact =
				FlightBasicFact.builder().airline(airline).productClass(productClass).sourceId(sourceId).build();
		return flightFact;
	}

	private SSRInformation getSeatSSRInfo(String segKey, List<Seats> seats, List<PassengerSsrFee> fees, int legNum) {
		SeatSSRInformation seatSSR = null;
		PassengerSsrFee seatFee = getFee(fees, segKey, true, false);
		for (Seats seat : seats) {
			seatSSR = new SeatSSRInformation();
			seatSSR.setCode(seat.getUnitDesignator());
			if (legNum == 0)
				seatSSR.setAmount(getCharges(seatFee.getServiceCharges()));
		}
		return seatSSR;
	}

	private PassengerSsrFee getFee(List<PassengerSsrFee> fees, String segKey, boolean isSeatFeeRequired,
			boolean isInfantFeeRequired) {
		for (PassengerSsrFee fee : fees) {
			String reference = fee.getFlightReference();
			if (StringUtils.isNotBlank(reference) && reference.equals(segKey)) {
				if (isSeatFeeRequired && fee.getCode().equalsIgnoreCase(AirAsiaDotRezConstants.SEAT)) {
					return fee;
				}
				if (isInfantFeeRequired) {
					return fee;
				}
			}
		}
		return null;
	}

	private FlightTravellerInfo addInfantTraveller(String segKey, Infant infant, int passengerCount, Segment segment,
			int legNum) {
		FlightTravellerInfo traveller = new FlightTravellerInfo();
		traveller.setPaxType(PaxType.INFANT);
		traveller.setFirstName(infant.getName().getFirst());
		traveller.setLastName(infant.getName().getLast());
		traveller.setTitle(getTitle(traveller.getPaxType(), infant.getName().getTitle()));
		traveller.setDob(AirAsiaDotRezUtils.getDOB(infant.getDateOfBirth()));
		traveller.setPnr(pnr);
		PassengerSsrFee infantFee = getFee(infant.getFees(), segKey, false, true);
		traveller.setFareDetail(getFareDetails(segment.getFares().get(0), PaxType.INFANT.name(), infantFee, legNum));
		return traveller;

	}

	private String getTitle(PaxType paxType, String title) {
		if (!paxType.equals(PaxType.ADULT) && AirAsiaDotRezConstants.MR.equalsIgnoreCase(title)) {
			return AirAsiaDotRezConstants.MASTER;
		}
		return title;
	}

	private String getType(String passengerTypeCode) {
		if (AirAsiaDotRezConstants.CHILD_SSRCODE.equals(passengerTypeCode))
			return PaxType.CHILD.getType();
		if (AirAsiaDotRezConstants.INFANT_SSRCODE.equals(passengerTypeCode))
			return PaxType.INFANT.getType();
		return passengerTypeCode;
	}

	private FareDetail getFareDetails(com.airasia.datamodel.FareDetail fD, String paxType, PassengerSsrFee infantFee,
			int legNum) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.setCabinClass(CabinClass.ECONOMY);
		fareDetail.setClassOfBooking(fD.getProductClass());
		fareDetail.setFareBasis(fD.getFareBasisCode());
		if (AirAsiaDotRezConstants.INFANT.equalsIgnoreCase(paxType) && legNum == 0) {
			getFC(infantFee.getServiceCharges(), fareDetail);
			fareDetail.getFareComponents().put(FareComponent.TF, getTotalFare(fareDetail));

		} else if (legNum == 0) {
			getFareComponents(fD.getPassengerFares(), paxType, fareDetail);
			fareDetail.getFareComponents().put(FareComponent.TF, getTotalFare(fareDetail));

		}
		return fareDetail;
	}

	private void getFareComponents(List<PassengerFare> passengerFares, String paxType, FareDetail fareDetail) {
		for (PassengerFare fare : passengerFares) {
			if (fare.getPassengerType().equals(paxType)) {
				getFC(fare.getServiceCharges(), fareDetail);
			}
		}
	}

	private void getFC(List<ServiceCharge> serviceCharges, FareDetail fareDetail) {
		for (ServiceCharge charge : serviceCharges) {
			AirAsiaDotRezCharges dotRezCharge = AirAsiaDotRezCharges.getCharges(charge);
			if (dotRezCharge != null) {
				dotRezCharge.setFareComponent(fareDetail, null, charge.getAmount(), charge.getCurrencyCode());
			}
		}
	}

	public FlightDesignator getFlightDesignator(Segment segment, Leg leg) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(segment.getIdentifier().getCarrierCode()));
		flightDesignator.setEquipType(leg.getLegInfo().getEquipmentType());
		flightDesignator.setFlightNumber(segment.getIdentifier().getIdentifier());
		return flightDesignator;
	}
}
