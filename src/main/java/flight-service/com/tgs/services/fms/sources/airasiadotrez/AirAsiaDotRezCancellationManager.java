package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.airasia.datamodel.AvailableJourney;
import com.airasia.datamodel.BaseRequest;
import com.airasia.datamodel.BaseResponse;
import com.airasia.datamodel.BookingPaymentTransfer;
import com.airasia.datamodel.CrsRecordLocator;
import com.airasia.datamodel.DividePNRRQ;
import com.airasia.datamodel.GetBookingInStateRS;
import com.airasia.datamodel.PassengerDetail;
import com.airasia.datamodel.PassengerSsrFee;
import com.airasia.datamodel.SellSsrRS;
import com.airasia.datamodel.SellTripData;
import com.airasia.datamodel.SellTripRS;
import com.airasia.datamodel.ServiceCharge;
import com.tgs.services.base.LogData;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
@Getter
@Setter
public class AirAsiaDotRezCancellationManager extends AirAsiaDotRezServiceManager {

	protected BookingSegments bookingSegments;

	protected Map<String, String> passengerKeys;

	protected List<TripInfo> cancelTrips;

	protected List<TripInfo> actualTrips;

	public void init() {
		cancelTrips = AirCancelUtils.createTripListFromSegmentListCancellation(bookingSegments.getCancelSegments());
		actualTrips = AirCancelUtils.createTripListFromSegmentListCancellation(bookingSegments.getSegmentInfos());
	}

	public List<String> getJourneyKeysFromCancelSegments(List<TripInfo> trips, SellTripData data) {
		List<String> journeyKeys = new ArrayList<String>();
		if (CollectionUtils.isNotEmpty(data.getJourneys())) {
			for (TripInfo trip : trips) {
				for (AvailableJourney journey : data.getJourneys()) {
					if (cancellationRaisedForTrip(trip, journey)) {
						journeyKeys.add(journey.getJourneyKey());
					}
				}
			}
		}
		return journeyKeys;
	}

	private boolean cancellationRaisedForTrip(TripInfo trip, AvailableJourney journey) {
		if (trip.getDepartureAirportCode().equalsIgnoreCase(journey.getDesignator().getOrigin())
				&& trip.getArrivalAirportCode().equalsIgnoreCase(journey.getDesignator().getDestination())) {
			return true;
		}
		return false;
	}

	public boolean isCancelled(int status) {
		// cancelled code = 3
		if (status == 3) {
			return true;
		}
		return false;
	}

	public GetBookingInStateRS submitCancelRequest(SellTripData sellTripData) {
		List<String> journeyKeys = getJourneyKeysFromCancelSegments(cancelTrips, sellTripData);
		if (actualTrips.size() == journeyKeys.size()) {
			submitRequest(bindingService.journeyURL("", true));
		} else {
			raiseCancellationForJourney(journeyKeys);
		}
		return getBookingInState();
	}

	private void raiseCancellationForJourney(List<String> journeyKeys) {
		for (String key : journeyKeys) {
			submitRequest(bindingService.journeyURL(key, false));
		}

	}

	private void submitRequest(String journeyURL) {
		HttpUtils httpUtils = null;
		try {
			httpUtils = HttpUtils.builder().headerParams(headerParams()).urlString(journeyURL).proxy(proxy())
					.requestMethod(AirAsiaDotRezConstants.DELETE).build();
			SellSsrRS responseBody = httpUtils.getResponse(SellSsrRS.class).orElse(null);
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "Submit cancellation RQ"),
					formatRQRS(httpUtils.getResponseString(), "Submit cancellation RS"));
			listener.addLog(LogData.builder().key(getListnerKey()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("Submit cancellation").build());
		}
	}

	public GetBookingInStateRS getBookingInState() {
		HttpUtils httpUtils = null;
		GetBookingInStateRS bookingResponse = null;
		try {
			httpUtils = HttpUtils.builder().headerParams(headerParams()).proxy(proxy())
					.urlString(bindingService.bookingStateUrl()).build();
			bookingResponse = httpUtils.getResponse(GetBookingInStateRS.class).orElse(null);
			return bookingResponse;
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getResponseString(), "GetBookingInStateRS"));
			listener.addLog(LogData.builder().key(getListnerKey()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("GetBookingInState").build());
		}
	}

	public void setAirlineCancellationFee(GetBookingInStateRS bookingResponse, BookingSegments bookingSegments,
			boolean isConfirmFlow) {
		Map<String, PassengerDetail> details = bookingResponse.getData().getPassengers();
		for (TripInfo trip : cancelTrips) {
			for (int segIdx = 0; segIdx < trip.getSegmentInfos().size(); segIdx++) {
				SegmentInfo segment = trip.getSegmentInfos().get(segIdx);
				for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
					PassengerDetail passengerDetail = details.get(passengerKeys.get(getTravellerKey(trip, traveller)));
					if (passengerDetail != null) {
						for (PassengerSsrFee fee : passengerDetail.getFees()) {
							String reference = fee.getFlightReference();
							if (fee.getCode().equalsIgnoreCase("CXL2")
									&& MapUtils.isNotEmpty(traveller.getFareDetail().getFareComponents())
									&& isMatchedSegment(segment, reference)) {
								setTravellerCancelFee(traveller.getFareDetail(), getCharges(fee.getServiceCharges()));
							}
						}
					} else {
						setTravellerCancelFee(traveller.getFareDetail(), 0.0);
					}
				}
			}
		}
	}

	private void setTravellerCancelFee(FareDetail fareDetail, Double amount) {
		double initialAmount = getInitialAmount(fareDetail, FareComponent.ACF);
		double cancelAmount = initialAmount + amount;
		if (cancelAmount >= 0.0)
			fareDetail.getFareComponents().put(FareComponent.ACF, amount);
	}


	private static double getInitialAmount(FareDetail fareDetail, FareComponent fareComponent) {
		double initialAmount = 0.0;
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
				&& Objects.nonNull(fareDetail.getFareComponents().get(fareComponent))
				&& fareDetail.getFareComponents().get(fareComponent).doubleValue() > 0) {
			initialAmount = fareDetail.getFareComponents().get(fareComponent).doubleValue();
		}
		return initialAmount;
	}

	public String dividePNR(SellTripRS bookingDetailsResponse) {
		String newPNR = null;
		HttpUtils httpUtils = null;
		try {
			DividePNRRQ request = buildDividePnrRequest();
			httpUtils = HttpUtils.builder().headerParams(headerParams()).proxy(proxy())
					.postData(GsonUtils.getGson().toJson(request)).requestMethod(AirAsiaDotRezConstants.POST)
					.urlString(bindingService.dividePNRURL()).build();
			SellTripRS response = httpUtils.getResponse(SellTripRS.class).orElse(null);
			if (CollectionUtils.isEmpty(response.getErrors())) {
				newPNR = response.getData().getRecordLocator();
				updatePNR(newPNR, bookingSegments.getCancelSegments());
				updateSegmentPnr(bookingSegments.getCancelSegments(), bookingSegments.getSegmentInfos());
				commitBooking(AirAsiaDotRezConstants.PUT);
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "DividePNR RQ"),
					formatRQRS(httpUtils.getResponseString(), "DividePNR RS"));
			listener.addLog(LogData.builder().key(getListnerKey()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("Divide PNR").build());
		}
		return newPNR;
	}

	private void updateSegmentPnr(List<SegmentInfo> cancelSegments, List<SegmentInfo> segmentInfos) {
		segmentInfos.forEach(segment -> {
			cancelSegments.get(0).getTravellerInfo().forEach(cancelTraveller -> {
				segment.getTravellerInfo().forEach(traveller -> {
					if (traveller.getPnr().equals(pnr)
							&& BookingUtils.isSameTravellerInfo(cancelTraveller, traveller)) {
						traveller.setPnr(cancelTraveller.getPnr());
					}
				});
			});
		});
	}

	private DividePNRRQ buildDividePnrRequest() {
		DividePNRRQ request = DividePNRRQ.builder().crsRecordLocators(getRecordLocator()).autoDividePayments(true)
				.overrideRestrictions(false).passengerKeys(getPassengerKeys()).bookingPaymentTransfers(getPayment())
				.cancelSourceBooking(false).receivedBy("").childEmail("").build();
		return request;
	}

	private List<BookingPaymentTransfer> getPayment() {
		List<BookingPaymentTransfer> paymentTransfers = new ArrayList<BookingPaymentTransfer>();
		BookingPaymentTransfer paymentTransfer =
				BookingPaymentTransfer.builder().bookingPaymentId(0).transferAmount(0).build();
		paymentTransfers.add(paymentTransfer);
		return paymentTransfers;
	}

	private List<String> getPassengerKeys() {
		List<String> keys = new ArrayList<String>();
		for (Map.Entry<String, String> set : passengerKeys.entrySet()) {
			String key = set.getValue();
			if (!keys.contains(key) && key != null) {
				keys.add(key);
			}
		}
		return keys;
	}

	private List<CrsRecordLocator> getRecordLocator() {
		CrsRecordLocator locator = CrsRecordLocator.builder().recordLocatorKey(pnr).build();
		List<CrsRecordLocator> locators = new ArrayList<CrsRecordLocator>();
		locators.add(locator);
		return locators;
	}

	/**
	 * After dividing the pnr the new pnr which is to be cancelled must be updated in the cancel segments
	 **/
	private void updatePNR(String recordLocator, List<SegmentInfo> segments) {
		segments.forEach(cancelSeg -> {
			cancelSeg.getTravellerInfo().forEach(traveller -> {
				traveller.setPnr(recordLocator);
			});
		});

	}

	protected Map<String, String> createPassengerKeys(SellTripRS bookingDetailsResponse) {
		Map<String, String> keysMap = new HashMap<String, String>();
		Map<String, PassengerDetail> details = bookingDetailsResponse.getData().getPassengers();
		for (TripInfo cancelTrip : cancelTrips) {
			for (FlightTravellerInfo traveller : cancelTrip.getTravllerInfos()) {
				keysMap.put(getTravellerKey(cancelTrip, traveller), getPassengerKeyValue(details, traveller));
			}
		}
		return keysMap;
	}

	private String getPassengerKeyValue(Map<String, PassengerDetail> details, FlightTravellerInfo traveller) {
		for (Map.Entry<String, PassengerDetail> entry : details.entrySet()) {
			PassengerDetail passenger = entry.getValue();
			if (passenger.getName().getFirst().equals(traveller.getFirstName())
					&& passenger.getName().getLast().equals(traveller.getLastName())) {
				return passenger.getPassengerKey();
			}
		}
		return null;
	}

	private String getTravellerKey(TripInfo cancelTrip, FlightTravellerInfo traveller) {
		return StringUtils.join(cancelTrip.getDepartureAirportCode(), "_", cancelTrip.getArrivalAirportCode(), "_",
				traveller.getFirstName(), "_", traveller.getLastName());
	}


}
