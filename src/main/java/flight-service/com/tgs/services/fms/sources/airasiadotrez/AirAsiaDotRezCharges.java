package com.tgs.services.fms.sources.airasiadotrez;

import com.airasia.datamodel.ServiceCharge;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FareDetail;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import java.util.Objects;

@Getter
@Slf4j
enum AirAsiaDotRezCharges {

	OTHER_CHARGES(0, "Other Charges", -1) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.OC);
			fareDetail.getFareComponents().put(FareComponent.OC, initialAmount + amount);
		}
	},
	BASE_FARE(1, null, 0) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.BF);
			fareDetail.getFareComponents().put(FareComponent.BF, amount + initialAmount);
		}
	},
	AIRLINE_TAX(2, "TravelFee", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	TRAVEL_FEE(3, "Travel Fee", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	GST(4, "GST", 5) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	INFT(5, "INFT", -1) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.BF);
			fareDetail.getFareComponents().put(FareComponent.BF, initialAmount + amount);
		}
	},
	PROMOTION_DISCOUNT(6, "PRM", 7) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = 0.0;
			if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.AT))
					&& fareDetail.getFareComponents().get(FareComponent.AT).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.AT).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.AT, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Airline Tax", amount, paxType);
			} else if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.BF))
					&& fareDetail.getFareComponents().get(FareComponent.BF).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.BF).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.BF, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Base Fare", amount, paxType);
			}
		}
	},
	CHANGE_FEE(7, "Penalty- Change Fee", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	CANCEL_FEE(8, "Penalty- Cancel Fee", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	SEAT_FEE(9, "Seat Fee", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	SPOILAGE_FEE(10, "SpoilageFee", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	PRIORITY_FEE(11, "Priority Checkin And Baggage", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	UNACCOMPANIED_MINOR_FEE(12, "UMNR", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	WEAPON_CARRIAGE_FEE(13, "WPNN", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	AIRPORT_DEVELOPMENT_FEE(14, "ADF", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.WO);
			fareDetail.getFareComponents().put(FareComponent.WO, initialAmount + amount);
		}
	},
	COMMISSION(15, "COMM", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	COMMISSIONBF(16, "COMMBF", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	CUTE_CHARGE(17, "CUTE", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	PASSENGER_SERVICE_FEE(18, "PSF", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.PSF);
			fareDetail.getFareComponents().put(FareComponent.PSF, initialAmount + amount);
		}
	},
	RCS_FEE(19, "RCS", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.RCF);
			fareDetail.getFareComponents().put(FareComponent.RCF, initialAmount + amount);
		}
	},
	USER_DEVELOPMENT_FEE(20, "UDF", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.UDF);
			fareDetail.getFareComponents().put(FareComponent.UDF, initialAmount + amount);
		}
	},
	AIRLINE_FUEL_CHARGE(21, "YQ", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.YQ);
			fareDetail.getFareComponents().put(FareComponent.YQ, initialAmount + amount);
		}
	},
	AGENCY_COMMISSION(22, "TTF", 4) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			/**
			 * @implSpec : As this Agency Commission No need to add in Fare detail
			 */
			// log.error("Agency Commission {}", amount);
		}
	},
	CT_GST(23, "CGST", 5) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	ST_GST(24, "SGST", 5) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	IT_GST(25, "IGST", 5) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	DISCOUNT(26, "Discount", 7) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = 0.0;
			if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.AT))
					&& fareDetail.getFareComponents().get(FareComponent.AT).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.AT).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.AT, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Airline Tax", amount, paxType);
			} else if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.BF))
					&& fareDetail.getFareComponents().get(FareComponent.BF).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.BF).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.BF, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Base Fare", amount, paxType);
			}
		}
	},
	AVIATION_SECURITY_FEE(20, "ASF", 7) {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	OTA_COMMISSION(21, "OTA", 7) {

		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = 0.0;
			if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.AT))
					&& fareDetail.getFareComponents().get(FareComponent.AT).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.AT).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.AT, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Airline Tax", amount, paxType);
			} else if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.BF))
					&& fareDetail.getFareComponents().get(FareComponent.BF).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.BF).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.BF, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Base Fare", amount, paxType);
			}
		}
	},
	DISCOUNT_CHARGE(22, "Discount", 1) {

	};

	private int id = -1;

	private String type;

	private Integer typeOrdinal;

	private AirAsiaDotRezCharges(int value, String type, Integer typeOrdinal) {
		id = value;
		this.type = type;
		this.typeOrdinal = typeOrdinal;
	}

	// total tax sum
	public static final String TAX_SUM = "TaxSum";

	public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {}

	public double getInitialAmount(FareDetail fareDetail, FareComponent fareComponent) {
		double initialAmount = 0.0;
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
				&& Objects.nonNull(fareDetail.getFareComponents().get(fareComponent))
				&& fareDetail.getFareComponents().get(fareComponent).doubleValue() > 0) {
			initialAmount = fareDetail.getFareComponents().get(fareComponent).doubleValue();
		}
		return initialAmount;
	}

	public static AirAsiaDotRezCharges getCharges(ServiceCharge serviceCharge) {
		if (StringUtils.equalsIgnoreCase(TAX_SUM, serviceCharge.getDetail())) {
			// as we using break up of each component, Tax Sum is Not required
			return AirAsiaDotRezCharges.GST;
		}
		for (AirAsiaDotRezCharges charge : AirAsiaDotRezCharges.values()) {
			if (StringUtils.equalsIgnoreCase(charge.getType(), serviceCharge.getCode())) {
				return charge;
			}
		}
		return AirAsiaDotRezCharges.OTHER_CHARGES;
	}


	/**
	 * FarePrice <br>
	 * Discount <br>
	 * IncludedTravelFee<br>
	 * IncludedTax <br>
	 * TravelFee <br>
	 * Tax <br>
	 * ServiceCharge<br>
	 * PromotionDiscount<br>
	 * ConnectionAdjustmentAmount<br>
	 * AddOnServicePrice<br>
	 * IncludedAddOnServiceFee<br>
	 * AddOnServiceFee <br>
	 * Calculated Note<br>
	 * AddOnServiceMarkup <br>
	 * FareSurcharge<br>
	 * Loyalty <br>
	 * FarePoints<br>
	 * DiscountPoints<br>
	 * AddOnServiceCancelFee<br>
	 * Unmapped<br>
	 * 
	 */
}
