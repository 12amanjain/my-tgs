package com.tgs.services.fms.sources.airasiadotrez;

public class AirAsiaDotRezConstants {

	public static final Integer MAX_CONNECTIONS = 20;

	public static final String TAXES_AND_FEES = "Taxes";

	public static final String INVENTORY_CONTROL = "HoldSpace";

	public static final String PHONE_CONTACT_TYPE = "P";

	public static final String GST_CONTACT_TYPE = "G";

	public static final String CURRENCY_CODE = "INR";

	public static final String INFANT_SSRCODE = "INFT";

	public static final String HOME_PHONE = "Home";

	public static final String OTHER_PHONE = "Other";

	public static final String CHILD_SSRCODE = "CHD";

	public static final String MASTER = "MASTER";

	public static final String MR = "MR";

	public static final String INFANT = "INFANT";

	public static final String SEAT = "SEAT";

	public static final String POST = "POST";

	public static final String DELETE = "DELETE";

	public static final String PUT = "PUT";

	public static final String LOWEST_FARECLASS = "LowestFareClass";

	public static final String COMPRESS_BY_PRODUCTCLASS = "CompressByProductClass";

	public static final String DEFAULT_CLASS = "Default";

	public static final String FARE_SORT_OPTION = "LowestFare";

	public static final String Loyalty = "MonetaryOnly";

	public static final String Seat_Inventory_Control = "Session";

	public static final String SeatAssignment_Mode = "PreSeatAssignment";


}
