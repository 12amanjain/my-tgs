package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import com.airasia.datamodel.*;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@SuperBuilder
final class AirAsiaDotRezReviewManager extends AirAsiaDotRezServiceManager {

	protected Set<String> passengerKeys;

	public TripInfo reviewTrip(TripInfo selectedTrip) {
		HttpUtils httpUtils = null;
		SellTripRQ sellTripRq = null;
		SellTripRS sellTripResponse = null;
		try {
			sellTripRq = getSellTripRequest(selectedTrip);
			passengerKeys = new HashSet<>();
			httpUtils = HttpUtils.builder().urlString(bindingService.sellUrl()).headerParams(headerParams())
					.proxy(proxy()).postData(GsonUtils.getGson().toJson(sellTripRq)).build();
			sellTripResponse = httpUtils.getResponse(SellTripRS.class).orElse(null);
			if (isAnyError(sellTripResponse)) {
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
			if (!parseReviewResponse(sellTripResponse, selectedTrip)) {
				selectedTrip = null;
				throw new NoSeatAvailableException("Unable to Sell[HoldSpace] selected flight");
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SellTripRQ"),
					formatRQRS(httpUtils.getResponseString(), "SellTripRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("3-SellTrip").build());
		}
		return selectedTrip;
	}

	private boolean parseReviewResponse(SellTripRS sellTripResponse, TripInfo selectedTrip) {
		boolean isSuccess = false;
		List<AvailableJourney> availableJourneys = sellTripResponse.getData().getJourneys();
		AtomicInteger segmentNum = new AtomicInteger();
		for (AvailableJourney journey : availableJourneys) {
			for (Segment segment : journey.getSegments()) {
				int legNumber = 0;
				for (Leg leg : segment.getLegs()) {
					populatePriceInfo(segment, selectedTrip, journey, segmentNum, legNumber, leg);
					legNumber++;
				}
				isSuccess = true;
			}
		}
		passengerKeys.addAll(sellTripResponse.getData().getPassengers().keySet());
		Map<PaxType, List<String>> paxKeys = new HashMap();
		for (String paxKey : sellTripResponse.getData().getPassengers().keySet()) {
			String paxTypeCode = sellTripResponse.getData().getPassengers().get(paxKey).getPassengerTypeCode();
			PaxType paxType = getPaxType(paxTypeCode);
			if (!paxKeys.containsKey(paxType)) {
				paxKeys.put(paxType, new ArrayList<>());
			}
			paxKeys.get(paxType).add(paxKey);
		}
		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			priceInfo.getMiscInfo().setPaxPricingInfo(paxKeys);
		}
		return isSuccess;
	}

	private void populatePriceInfo(Segment segment, TripInfo selectedTrip, AvailableJourney journey,
			AtomicInteger segmentNum, int legNumber, Leg leg) {
		SegmentInfo segmentInfo = selectedTrip.getSegmentInfos().get(segmentNum.get());
		PriceInfo oldPriceInfo = segmentInfo.getPriceInfo(0);
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(oldPriceInfo.getSupplierBasicInfo());
		priceInfo.setMiscInfo(oldPriceInfo.getMiscInfo());
		priceInfo.getMiscInfo().setJourneyKey(journey.getJourneyKey());
		priceInfo.getMiscInfo().setLegNum(legNumber);
		priceInfo.setFareIdentifier(FareType.PUBLISHED);
		dotRezAirline.processPriceInfo(oldPriceInfo.getBookingClass(PaxType.ADULT), priceInfo, supplierConfig);
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		com.airasia.datamodel.FareDetail fareDetailSupplier = segment.getFares().get(0);
		if (CollectionUtils.isNotEmpty(fareDetailSupplier.getPassengerFares()) && legNumber == 0) {
			for (PassengerFare passengerFare : fareDetailSupplier.getPassengerFares()) {
				FareDetail fareDetail = new FareDetail();
				fareDetail.setCabinClass(CabinClass.ECONOMY);
				fareDetail.setFareBasis(fareDetailSupplier.getFareBasisCode());
				fareDetail.setClassOfBooking(fareDetailSupplier.getProductClass());
				fareDetail.setSeatRemaining(5);
				Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
				PaxType paxType = getPaxType(passengerFare.getPassengerType());
				for (ServiceCharge serviceCharge : passengerFare.getServiceCharges()) {
					AirAsiaDotRezCharges dotRezCharge = AirAsiaDotRezCharges.getCharges(serviceCharge);
					if (dotRezCharge != null) {
						dotRezCharge.setFareComponent(fareDetail, paxType.getType(), serviceCharge.getAmount(),
								serviceCharge.getCurrencyCode());
					}
				}
				fareComponents.put(FareComponent.TF, getTotalFare(fareDetail));
				fareDetails.put(paxType, fareDetail);
			}
		} else {
			FareDetail fareDetail = new FareDetail();
			fareDetail.setCabinClass(CabinClass.ECONOMY);
			fareDetail.setFareBasis(fareDetailSupplier.getFareBasisCode());
			fareDetail.setClassOfBooking(fareDetailSupplier.getProductClass());
			fareDetail.setSeatRemaining(5);
			searchQuery.getPaxInfo().forEach((paxType, count) -> {
				if (!paxType.equals(PaxType.INFANT) && count > 0) {
					fareDetails.put(paxType, fareDetail);
				}
			});
		}
		segmentInfo.setPriceInfoList(Arrays.asList(priceInfo));
		segmentNum.getAndIncrement();
	}


	private SellTripRQ getSellTripRequest(TripInfo selectedTrip) {
		SellTripRQ request = SellTripRQ.builder().build();
		request.setCurrencyCode(getCurrencyCode());
		request.setSuppressPassengerAgeValidation(true);
		request.setPreventOverlap(true);
		request.setKeys(getTripKeys(selectedTrip));
		request.setPassengers(getPassengers());
		String promoCode = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getAccountCode();
		if (StringUtils.isNotBlank(promoCode)) {
			request.setPromotionCode(promoCode);
		}
		return request;
	}


	private List<TripKey> getTripKeys(TripInfo selectedTrip) {
		List<TripKey> tripKeys = new ArrayList<TripKey>();
		List<TripInfo> splitTripInfo = AirUtils.splitTripInfo(selectedTrip, false);
		for (TripInfo tripInfo : splitTripInfo) {
			tripKeys.add(getSingleTripKey(tripInfo));
		}
		return tripKeys;
	}

	private TripKey getSingleTripKey(TripInfo tripInfo) {
		TripKey tripKey = TripKey.builder().build();
		tripKey.setFareAvailabilityKey(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareKey());
		tripKey.setInventoryControl(AirAsiaDotRezConstants.INVENTORY_CONTROL);
		tripKey.setJourneyKey(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey());
		return tripKey;
	}

}
