package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.airasia.datamodel.AvailableJourneySsr;
import com.airasia.datamodel.AvailableLegSsr;
import com.airasia.datamodel.AvailableSegmentSsr;
import com.airasia.datamodel.CompartmentData;
import com.airasia.datamodel.CompartmentUnits;
import com.airasia.datamodel.DeckData;
import com.airasia.datamodel.FlightIdentifier;
import com.airasia.datamodel.GetBookingQuoteRQ;
import com.airasia.datamodel.GetBookingQuoteRS;
import com.airasia.datamodel.GetSsrAvailabilityRQ;
import com.airasia.datamodel.GetSsrAvailabilityRS;
import com.airasia.datamodel.InfantFare;
import com.airasia.datamodel.Market;
import com.airasia.datamodel.PassengerSSRAvalibilityDetail;
import com.airasia.datamodel.PassengerSsrItem;
import com.airasia.datamodel.PassengerType;
import com.airasia.datamodel.Passengers;
import com.airasia.datamodel.SeatAvailability;
import com.airasia.datamodel.SeatAvailabilityRS;
import com.airasia.datamodel.SeatCharges;
import com.airasia.datamodel.SeatFeeGroup;
import com.airasia.datamodel.SeatProperty;
import com.airasia.datamodel.ServiceCharge;
import com.airasia.datamodel.SsrDetails;
import com.airasia.datamodel.SsrItem;
import com.airasia.datamodel.SsrRequest;
import com.airasia.datamodel.SsrTrip;
import com.airasia.datamodel.TripKey;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.LogData;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRMiscInfo;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.sources.airasia.AirAsiaUtils;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class AirAsiaDotRezSSRManager extends AirAsiaDotRezServiceManager {

	private static final String AISLE = "AISLE";
	private static final String LEGROOM = "LEGROOM";
	private static final String WINDOW = "WINDOW";

	public void getSSRAvailability(TripInfo selectedTrip, Set<String> passengerKeys) {
		HttpUtils httpUtils = null;
		try {
			GetSsrAvailabilityRQ getSsrRequest = getSSRRequest(selectedTrip, passengerKeys);
			httpUtils = HttpUtils.builder().headerParams(headerParams())
					.postData(GsonUtils.getGson().toJson(getSsrRequest)).requestMethod("POST").proxy(proxy())
					.urlString(bindingService.ssrAvailabilityUrl()).build();
			GetSsrAvailabilityRS ssrResponse = httpUtils.getResponse(GetSsrAvailabilityRS.class).orElse(null);
			parseSSRResponse(selectedTrip, ssrResponse);
		} catch (Exception e) {
			log.error("Error Occured for SSR Availability for {} cause ", bookingId, e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "GetSSRAvailabilityRQ"),
					formatRQRS(httpUtils.getResponseString(), "GetSSRAvailabilityRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("4-GetSSRAvailability").build());
		}
	}

	public void getInfantFare(TripInfo tripInfo) {
		HttpUtils httpUtils = null;
		try {
			GetBookingQuoteRQ bookingQuoteRQ = getBookingQuoteRequest(tripInfo);
			httpUtils = HttpUtils.builder().headerParams(headerParams())
					.postData(GsonUtils.getGson().toJson(bookingQuoteRQ)).requestMethod("POST").proxy(proxy())
					.urlString(bindingService.bookingQuoteUrl()).build();
			GetBookingQuoteRS bookingQuoteRS = httpUtils.getResponse(GetBookingQuoteRS.class).orElse(null);
			parseBookingQuoteResponse(bookingQuoteRS, tripInfo.getSegmentInfos().get(0));
		} catch (IOException e) {
			log.error("Error Occured while fetching Infant Fare from booking Quote for {} cause ",
					searchQuery.getSearchId(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "GetBookingQuoteRQ"),
					formatRQRS(httpUtils.getResponseString(), "GetBookingQuoteRS"));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("3-GetBookingQuote").build());
		}
	}

	private void parseBookingQuoteResponse(GetBookingQuoteRS bookingQuoteRS, SegmentInfo segmentInfo) {
		InfantFare infantSupplierFare = bookingQuoteRS.getData().getBreakdown().getPassengerTotals().getInfant();
		FareDetail infantFareDetail = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT, new FareDetail());
		if (MapUtils.isEmpty(infantFareDetail.getFareComponents())) {
			infantFare = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT, new FareDetail());
			infantFare = setFareBreakUp(infantFare, infantSupplierFare);
			infantFareDetail.setFareComponents(infantFare.getFareComponents());
		}
	}

	private FareDetail setFareBreakUp(FareDetail fareDetail, InfantFare infantSupplierFare) {
		for (ServiceCharge serviceCharge : infantSupplierFare.getCharges()) {
			AirAsiaDotRezCharges dotRezCharge = AirAsiaDotRezCharges.getCharges(serviceCharge);
			if (dotRezCharge != null) {
				dotRezCharge.setFareComponent(fareDetail, AirAsiaDotRezConstants.INFANT_SSRCODE,
						serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
			}
		}
		return fareDetail;
	}

	private GetBookingQuoteRQ getBookingQuoteRequest(TripInfo selectedTrip) {
		GetBookingQuoteRQ request = GetBookingQuoteRQ.builder().build();
		request.setCurrencyCode(AirAsiaDotRezConstants.CURRENCY_CODE);
		request.setKeys(getTripKeys(selectedTrip));
		request.setPassengers(getPassengersForBookingQuote());
		request.setSsrs(getSsrList(selectedTrip));
		return request;
	}

	private List<SsrRequest> getSsrList(TripInfo selectedTrip) {
		List<SsrRequest> ssrList = new ArrayList<SsrRequest>();
		SegmentInfo segmentInfo = selectedTrip.getSegmentInfos().get(0);
		SsrRequest ssr = SsrRequest.builder().build();
		ssr.setMarket(getSsrMarket(segmentInfo));
		ssr.setItems(getPassengerSsrItem(segmentInfo));
		ssrList.add(ssr);
		return ssrList;
	}

	private List<PassengerSsrItem> getPassengerSsrItem(SegmentInfo segmentInfo) {
		List<PassengerSsrItem> passengerItemList = new ArrayList<PassengerSsrItem>();
		PassengerSsrItem passengerItem = PassengerSsrItem.builder().build();
		passengerItem.setPassengerType(PaxType.ADULT.getType());
		passengerItem.setSsrs(getInfantSsr(segmentInfo));
		passengerItemList.add(passengerItem);
		return passengerItemList;
	}

	private List<SsrItem> getInfantSsr(SegmentInfo segmentInfo) {
		List<SsrItem> ssrList = new ArrayList<SsrItem>();
		SsrItem ssrItem = SsrItem.builder().build();
		// getching fare for 1 infant only
		ssrItem.setCount(1);
		ssrItem.setSsrCode(AirAsiaDotRezConstants.INFANT_SSRCODE);
		ssrItem.setDesignator(getFlightDesignator(segmentInfo));
		ssrList.add(ssrItem);
		return ssrList;
	}

	private com.airasia.datamodel.FlightDesignator getFlightDesignator(SegmentInfo segmentInfo) {
		com.airasia.datamodel.FlightDesignator flightDesignator =
				com.airasia.datamodel.FlightDesignator.builder().build();
		flightDesignator.setOrigin(segmentInfo.getDepartureAirportCode());
		flightDesignator.setDestination(segmentInfo.getArrivalAirportCode());
		flightDesignator.setDeparture(segmentInfo.getDepartTime());
		return flightDesignator;
	}

	private Market getSsrMarket(SegmentInfo segmentInfo) {
		Market market = Market.builder().build();
		market.setOrigin(segmentInfo.getDepartureAirportCode());
		market.setDestination(segmentInfo.getArrivalAirportCode());
		market.setDepartureDate(segmentInfo.getDepartTime().toString());
		market.setIdentifier(getFlightIdentifier(segmentInfo.getFlightDesignator()));
		return market;
	}

	private Passengers getPassengersForBookingQuote() {
		Passengers passengers = Passengers.builder().build();
		passengers.setTypes(getPassengerTypesForBookingQuote());
		return passengers;
	}


	private List<PassengerType> getPassengerTypesForBookingQuote() {
		List<PassengerType> passengerTypes = new ArrayList<PassengerType>();
		PassengerType passengerType = PassengerType.builder().build();
		// fetching infant fare associated with 1 adult only.
		passengerType.setCount(1);
		passengerType.setType(PaxType.ADULT.getType());
		passengerTypes.add(passengerType);
		return passengerTypes;
	}

	private List<TripKey> getTripKeys(TripInfo selectedTrip) {
		List<TripKey> tripKeys = new ArrayList<TripKey>();
		List<TripInfo> splitTripInfo = AirUtils.splitTripInfo(selectedTrip, false);
		for (TripInfo tripInfo : splitTripInfo) {
			tripKeys.add(getSingleTripKey(tripInfo));
		}
		return tripKeys;
	}

	private TripKey getSingleTripKey(TripInfo tripInfo) {
		TripKey tripKey = TripKey.builder().build();
		tripKey.setFareAvailabilityKey(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareKey());
		tripKey.setInventoryControl(AirAsiaDotRezConstants.INVENTORY_CONTROL);
		tripKey.setJourneyKey(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey());
		return tripKey;
	}

	private void parseSSRResponse(TripInfo tripInfo, GetSsrAvailabilityRS ssrResponse) {
		int segmentIndex = 0;
		int journeyIndex = 0;
		Map<SSRType, List<? extends SSRInformation>> preSSR =
				AirUtils.getPreSSR(AirAsiaDotRezUtils.getSSRFlightFact(tripInfo));
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			if (Objects.nonNull(preSSR)) {
				Map<SSRType, List<? extends SSRInformation>> ssrInfo = new HashMap<>();
				if (segmentInfo.getSegmentNum() == 0) {
					List<BaggageSSRInformation> baggageSSRInformations = getBaggageInfos(preSSR, segmentInfo,
							ssrResponse.getData().getJourneySsrs().get(journeyIndex), true);
					if (baggageSSRInformations != null)
						ssrInfo.put(SSRType.BAGGAGE, baggageSSRInformations);
					journeyIndex++;
				} else {
					List<BaggageSSRInformation> baggageSSRInformations = getBaggageInfos(preSSR, segmentInfo,
							ssrResponse.getData().getJourneySsrs().get(journeyIndex - 1), false);
					if (baggageSSRInformations != null) {
						ssrInfo.put(SSRType.BAGGAGE, baggageSSRInformations);
					}
				}
				List<MealSSRInformation> mealSSRInformations =
						getMealInfos(segmentInfo, preSSR, ssrResponse.getData().getLegSsrs().get(segmentIndex));
				if (mealSSRInformations != null)
					ssrInfo.put(SSRType.MEAL, mealSSRInformations);
				segmentInfo.setSsrInfo(ssrInfo);

				segmentIndex++;
			}
		}
	}


	private List<MealSSRInformation> getMealInfos(SegmentInfo segmentInfo,
			Map<SSRType, List<? extends SSRInformation>> preSSR, AvailableLegSsr availableLegSsr) {
		List<MealSSRInformation> mealSSRs = new ArrayList<MealSSRInformation>();
		for (SsrDetails ssrDetail : availableLegSsr.getSsrs()) {
			if (ssrDetail.getAvailable() != null
					&& ssrDetail.getAvailable() >= AirUtils.getPaxCount(searchQuery, false)) {
				// iterate only for meal code;
				SSRInformation mealSSRInfo = AirUtils.getSSRInfo(preSSR.get(SSRType.MEAL), ssrDetail.getSsrCode());
				if (Objects.nonNull(mealSSRInfo)) {
					mealSSRs.add(getMealSsrInfo(ssrDetail));
				}
			}
		}
		return mealSSRs;

	}

	private MealSSRInformation getMealSsrInfo(SsrDetails ssrDetail) {
		MealSSRInformation mealSSRInformation = new MealSSRInformation();
		AtomicDouble ssrAmount = new AtomicDouble();
		mealSSRInformation.setCode(ssrDetail.getSsrCode());
		mealSSRInformation.setDesc(ssrDetail.getName());
		mealSSRInformation.setMiscInfo(getSSRMiscInfo(ssrDetail.getPassengersAvailability(), ssrAmount));
		mealSSRInformation.setAmount(ssrAmount.get());
		return mealSSRInformation;
	}

	private List<BaggageSSRInformation> getBaggageInfos(Map<SSRType, List<? extends SSRInformation>> preSSR,
			SegmentInfo segmentInfo, AvailableJourneySsr availableJourneySsr, boolean isFirstSegment) {
		List<BaggageSSRInformation> baggageSSRs = new ArrayList<>();
		for (SsrDetails ssrDetail : availableJourneySsr.getSsrs()) {
			if (ssrDetail.getAvailable() != null
					&& ssrDetail.getAvailable() >= AirUtils.getPaxCount(searchQuery, false)) {
				SSRInformation baggageSSRInfo =
						AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE), ssrDetail.getSsrCode());
				if (Objects.nonNull(baggageSSRInfo)) {
					baggageSSRs.add(getBaggageSsrInfo(ssrDetail, isFirstSegment));
				}
			}
		}
		return baggageSSRs;
	}

	private BaggageSSRInformation getBaggageSsrInfo(SsrDetails ssrDetail, boolean isFirstSegment) {
		BaggageSSRInformation baggageSsrInfo = new BaggageSSRInformation();
		AtomicDouble ssrAmount = new AtomicDouble();
		baggageSsrInfo.setCode(ssrDetail.getSsrCode());
		baggageSsrInfo.setDesc(ssrDetail.getName());
		baggageSsrInfo.setMiscInfo(getSSRMiscInfo(ssrDetail.getPassengersAvailability(), ssrAmount));
		if (isFirstSegment) {
			baggageSsrInfo.setAmount(ssrAmount.get());
		} else {
			baggageSsrInfo.setAmount(null);
		}
		return baggageSsrInfo;
	}

	private SSRMiscInfo getSSRMiscInfo(Map<String, PassengerSSRAvalibilityDetail> passengersAvailability,
			AtomicDouble ssrAmount) {
		SSRMiscInfo miscInfo = SSRMiscInfo.builder().build();
		List<String> paxKeys = new ArrayList<String>();
		passengersAvailability.forEach((paxKey, paxDetails) -> {
			paxKeys.add(paxDetails.getSsrKey());
			ssrAmount.set(paxDetails.getPrice());
		});
		miscInfo.setPaxSsrKeys(paxKeys);
		return miscInfo;
	}

	private GetSsrAvailabilityRQ getSSRRequest(TripInfo selectedTrip, Set<String> passengerKeys) {
		GetSsrAvailabilityRQ request = GetSsrAvailabilityRQ.builder().build();
		request.setCurrencyCode(getCurrencyCode());
		request.setPassengerKeys(new ArrayList<>(passengerKeys));
		request.setTrips(getTripsForSSR(selectedTrip));
		return request;
	}

	private List<SsrTrip> getTripsForSSR(TripInfo selectedTrip) {
		List<SsrTrip> ssrTrips = new ArrayList<SsrTrip>();
		int index = 0;
		SsrTrip prevSegSsr = null;
		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			SsrTrip ssrTrip = SsrTrip.builder().build();
			ssrTrip.setOrigin(segmentInfo.getDepartureAirportCode());
			ssrTrip.setDestination(segmentInfo.getArrivalAirportCode());
			ssrTrip.setIdentifier(getFlightIdentifier(segmentInfo.getFlightDesignator()));
			ssrTrip.setDepartureDate(segmentInfo.getDepartTime().toString());
			if (index != 0) {
				// this means thru flight , no need to send 2 ssr for that, only for that complete journey is required.
				if (prevSegSsr.getIdentifier().getIdentifier().equals(ssrTrip.getIdentifier().getIdentifier())) {
					prevSegSsr.setDestination(segmentInfo.getArrivalAirportCode());
				} else {
					ssrTrips.add(ssrTrip);
				}
			} else {
				ssrTrips.add(ssrTrip);
			}
			prevSegSsr = ssrTrip;
			index++;
		}
		return ssrTrips;
	}

	public FlightIdentifier getFlightIdentifier(FlightDesignator flightDesignator) {
		FlightIdentifier identifier = FlightIdentifier.builder().build();
		identifier.setCarrierCode(flightDesignator.getAirlineCode());
		identifier.setIdentifier(flightDesignator.getFlightNumber());
		return identifier;
	}


	public void setInfantFareToTrip(List<TripInfo> tripInfos, TripInfo reviewedTrip) {
		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromCache())
				&& searchQuery.getIsDomestic()) {
			FareComponentHelper.storeInfantFare(searchQuery, AirSourceType.AIRASIADOTREZ.getSourceId(),
					reviewedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.INFANT));
		}
		for (TripInfo newTripInfo : tripInfos) {
			AtomicInteger segmentNum = new AtomicInteger(0);
			boolean isFlyThru = isFlyThruFLight(newTripInfo.getSegmentInfos());
			newTripInfo.getSegmentInfos().forEach(segmentInfo -> {
				FareDetail infantFareDetail = getInfantFareBasedOnSourceType(reviewedTrip, segmentNum, segmentInfo);
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					FareDetail fareDetail = new FareDetail();
					fareDetail.setFareComponents(new HashMap<>());
					if (MapUtils.isNotEmpty(infantFareDetail.getFareComponents())) {
						if (isFlyThru) {
							if (priceInfo.getMiscInfo().getLegNum() == 0) {
								infantFareDetail.getFareComponents().forEach((fc, amount) -> {
									fareDetail.getFareComponents().put(fc, amount);
								});
							}
						} else if (isInfantFareOnSegmentWise(segmentInfo)) {
							infantFareDetail.getFareComponents().forEach((fc, amount) -> {
								fareDetail.getFareComponents().put(fc, amount);
							});
						}
					}
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					priceInfo.getFareDetail(PaxType.INFANT, new FareDetail())
							.setFareComponents(fareDetail.getFareComponents());
				});
				segmentNum.getAndIncrement();
			});
			AirAsiaDotRezUtils.setTotalFareOnTripInfo(Arrays.asList(newTripInfo));
		}
	}

	public void setInfantPriceOnReview(List<TripInfo> newTrips) {
		if (CollectionUtils.isNotEmpty(newTrips)) {
			TripInfo selectedTrip = newTrips.get(0);
			for (TripInfo tripInfo : AirUtils.splitTripInfo(selectedTrip, false)) {
				if (infantCount > 0) {
					// As Selling Infant while booking copy infant from Old Trip
					boolean isFlyThru = isFlyThruFLight(tripInfo.getSegmentInfos());
					tripInfo.getSegmentInfos().forEach(segmentInfo -> {
						copyInfantFareToSegment(segmentInfo, infantFare, isFlyThru);
					});
				}
				AirAsiaDotRezUtils.setTotalFareOnTripInfo(Arrays.asList(tripInfo));
			}
		}
	}


	public void buildSeatMap(TripInfo trip, TripSeatMap tripSeatMap) {
		HttpUtils httpUtils = null;
		try {
			String journeyKey = trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey();
			httpUtils = HttpUtils.builder().headerParams(headerParams()).requestMethod("GET").proxy(proxy())
					.urlString(bindingService.seatURL(journeyKey)).build();
			SeatAvailabilityRS availabilityRS = httpUtils.getResponse(SeatAvailabilityRS.class).orElse(null);
			if (!isAnyError(availabilityRS)) {
				buildSeatMapResponse(availabilityRS, trip, tripSeatMap);
			}
		} catch (IOException e) {
			log.error("Error Occured for SSR Availability for {} cause ", bookingId, e);
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SeatAvailablilityRQ"),
					formatRQRS(httpUtils.getResponseString(), "SeatAvailablilityRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("SeatAvailablility").build());
		}
	}

	private void buildSeatMapResponse(SeatAvailabilityRS availabilityRS, TripInfo trip, TripSeatMap tripSeatMap) {
		List<SeatAvailability> seatAvailabilities = availabilityRS.getData();
		for (SeatAvailability availability : seatAvailabilities) {
			Optional<SegmentInfo> segmentInfo = matchedSeatSegment(availability, trip);
			Map<PaxType, List<String>> paxKeys =
					trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getPaxPricingInfo();
			List<String> adultKeys = paxKeys.get(PaxType.ADULT);
			List<String> childKeys = paxKeys.get(PaxType.CHILD);
			Map<String, DeckData> deckDataMap = availability.getSeatMap().getDecks();
			Map<String, SeatFeeGroup> seatFeesMap = availability.getFees().get(adultKeys.get(0)).getGroups();
			List<CompartmentUnits> compartmentUnits = new ArrayList<>();
			for (String deckKey : deckDataMap.keySet()) {
				Map<String, CompartmentData> compartmentDataMap = deckDataMap.get(deckKey).getCompartments();
				if (MapUtils.isNotEmpty(compartmentDataMap)) {
					for (String key : compartmentDataMap.keySet()) {
						compartmentUnits.addAll(compartmentDataMap.get(key).getUnits());
					}
				}
			}
			if (segmentInfo.isPresent()) {
				List<SeatSSRInformation> ssrInformations = new ArrayList<>();
				for (CompartmentUnits unit : compartmentUnits) {
					if (unit.isAssignable()) {
						SeatSSRInformation ssrInformation = new SeatSSRInformation();
						ssrInformation.setSeatNo(unit.getDesignator());
						ssrInformation.setCode(unit.getDesignator());
						ssrInformation.setIsAisle(isPropertyAvailable(unit.getProperties(), AISLE));
						ssrInformation.setIsLegroom(isPropertyAvailable(unit.getProperties(), WINDOW));
						ssrInformation.setAmount(seatAmount(unit, seatFeesMap));
						ssrInformation.setIsBooked(!isSeatAvailable(unit));
						ssrInformation.getMiscInfo().setSeatCodeType(unit.getUnitKey());
						ssrInformations.add(ssrInformation);
					}
				}
				SeatInformation seatInformation = SeatInformation.builder().seatsInfo(ssrInformations).build();
				tripSeatMap.getTripSeat().put(segmentInfo.get().getId(), seatInformation);
			}
		}
	}

	private Boolean isSeatAvailable(CompartmentUnits unit) {
		return unit.getAvailability() == 5;
	}

	private Double seatAmount(CompartmentUnits unit, Map<String, SeatFeeGroup> seatFeesMap) {
		Double amount = 0d;
		if (seatFeesMap.get(unit.getGroup().toString()) != null
				&& CollectionUtils.isNotEmpty(seatFeesMap.get(unit.getGroup().toString()).getFees())) {
			SeatCharges seatCharge = seatFeesMap.get(unit.getGroup().toString()).getFees().get(0);
			for (ServiceCharge serviceCharge : seatCharge.getServiceCharges()) {
				amount += serviceCharge.getAmount();
			}
		}
		return amount;
	}

	private Boolean isPropertyAvailable(List<SeatProperty> properties, String propertyKey) {
		if (CollectionUtils.isNotEmpty(properties)) {
			for (SeatProperty property : properties) {
				if (StringUtils.equalsIgnoreCase(propertyKey, property.getCode())
						&& Boolean.valueOf(property.getValue().toLowerCase())) {
					return true;
				}
			}
		}
		return false;
	}

	private Optional<SegmentInfo> matchedSeatSegment(SeatAvailability availability, TripInfo trip) {
		if (isFlyThruFLight(trip.getSegmentInfos())) {
			return Optional.of(trip.getSegmentInfos().get(0));
		} else {
			for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
				if (segmentInfo.getArrivalAirportCode().equalsIgnoreCase(availability.getSeatMap().getArrivalStation())
						&& segmentInfo.getDepartureAirportCode()
								.equalsIgnoreCase(availability.getSeatMap().getDepartureStation())) {
					return Optional.of(segmentInfo);
				}
			}
		}
		return null;
	}
}
