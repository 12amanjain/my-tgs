package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.lang.Exception;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.FareClassControl;
import com.airasia.datamodel.AdditionalInfo;
import com.airasia.datamodel.AvailableJourney;
import com.airasia.datamodel.FareReference;
import com.airasia.datamodel.FlightIdentifier;
import com.airasia.datamodel.GetAvailabilityRQ;
import com.airasia.datamodel.GetAvailabilityRS;
import com.airasia.datamodel.JourneyFare;
import com.airasia.datamodel.Leg;
import com.airasia.datamodel.PassengerFare;
import com.airasia.datamodel.PassengerType;
import com.airasia.datamodel.Passengers;
import com.airasia.datamodel.SearchFilters;
import com.airasia.datamodel.Segment;
import com.airasia.datamodel.ServiceCharge;
import com.airasia.datamodel.Trip;
import com.tgs.services.base.LogData;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class AirAsiaDotRezSearchManager extends AirAsiaDotRezServiceManager {

	AirSearchResult airSearchResult;

	public AirSearchResult doSearch() {
		GetAvailabilityRQ searchRequest = null;
		GetAvailabilityRS availabilityRS = null;
		HttpUtils httpUtils = null;
		try {
			airSearchResult = new AirSearchResult();
			searchRequest = getAvailabiltyRequest();
			httpUtils =
					HttpUtils.builder().headerParams(headerParams()).urlString(bindingService.simpleAvailabilityUrl())
							.postData(GsonUtils.getGson().toJson(searchRequest)).proxy(proxy()).build();
			availabilityRS = httpUtils.getResponse(GetAvailabilityRS.class).orElse(null);
			if (isAnyError(availabilityRS)) {
				throw new NoSearchResultException(String.join(",", criticalMessageLogger));
			}
			parseSearchResponse(availabilityRS);
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "GetAvailabilityRQ"),
					formatRQRS(httpUtils.getResponseString(), "GetAvailabilityRS"));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("2-L-GetAvailability").build());
		}
		return airSearchResult;
	}

	private void parseSearchResponse(GetAvailabilityRS response) {
		if (response.getData() == null || CollectionUtils.isEmpty(response.getData().getResults())) {
			throw new NoSearchResultException("No SearchResult Available");
		}
		Map<String, JourneyFare> availableFaresMap = response.getData().getFaresAvailable();
		int size = response.getData().getResults().size();
		if (size == 2) {
			populateTripInfo(response.getData().getResults().get(0).getTrips().get(0), TripInfoType.ONWARD,
					availableFaresMap);
			populateTripInfo(response.getData().getResults().get(1).getTrips().get(0), TripInfoType.RETURN,
					availableFaresMap);
		} else {
			populateTripInfo(response.getData().getResults().get(0).getTrips().get(0), TripInfoType.ONWARD,
					availableFaresMap);
		}
	}

	private void populateTripInfo(Trip trip, TripInfoType tripType, Map<String, JourneyFare> availablefaresMap) {
		String tripKey = getTripKey(tripType, searchQuery);
		List<AvailableJourney> availableJourneys = trip.getJourneysAvailableByMarket().get(tripKey);
		for (AvailableJourney journey : availableJourneys) {
			TripInfo tripInfo = new TripInfo();
			List<SegmentInfo> segmentInfos = getSegmentInfos(journey, availablefaresMap);
			tripInfo.setSegmentInfos(segmentInfos);
			airSearchResult.addTripInfo(tripType.getTripType(), tripInfo);
		}
	}


	private String getTripKey(TripInfoType tripType, AirSearchQuery searchQuery) {
		String key = "";
		key = StringUtils.join(searchQuery.getRouteInfos().get(0).getFromCityAirportCode(), "|",
				searchQuery.getRouteInfos().get(0).getToCityAirportCode());
		if (TripInfoType.RETURN.equals(tripType)) {
			key = StringUtils.join(searchQuery.getRouteInfos().get(1).getFromCityAirportCode(), "|",
					searchQuery.getRouteInfos().get(1).getToCityAirportCode());
		}
		return key;
	}

	private List<SegmentInfo> getSegmentInfos(AvailableJourney journey, Map<String, JourneyFare> availablefaresMap) {
		List<SegmentInfo> segmentInfos = new ArrayList<SegmentInfo>();
		AtomicInteger segmentNum = new AtomicInteger();
		Integer supplierSegmentNumber = 0;
		AtomicBoolean isPromoFare = new AtomicBoolean();
		for (Segment segment : journey.getSegments()) {
			List<SegmentInfo> segmentInfosFromLegs = getSegmentInfoFromLegs(segment, segmentNum, availablefaresMap,
					journey, supplierSegmentNumber, isPromoFare);
			segmentInfos.addAll(segmentInfosFromLegs);
			supplierSegmentNumber++;
		}
		return segmentInfos;
	}

	private List<SegmentInfo> getSegmentInfoFromLegs(Segment segment, AtomicInteger segmentNum,
			Map<String, JourneyFare> availablefaresMap, AvailableJourney journey, Integer supplierSegmentNumber,
			AtomicBoolean isPromoFare) {
		List<SegmentInfo> segmentInfos = new ArrayList<SegmentInfo>();
		Integer legIndex = 0;
		for (Leg leg : segment.getLegs()) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(leg.getDesignator().getOrigin()));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(leg.getDesignator().getDestination()));
			segmentInfo.setDepartTime(leg.getDesignator().getDeparture());
			segmentInfo.setArrivalTime(leg.getDesignator().getArrival());
			segmentInfo.setSegmentNum(segmentNum.get());
			segmentInfo.setFlightDesignator(getFlightDesignator(segment, leg));
			segmentInfo.setDuration(segmentInfo.calculateDuration());
			segmentInfo.setIsReturnSegment(Boolean.FALSE);
			if (leg.getLegInfo() != null && StringUtils.isNotBlank(leg.getLegInfo().getOperatingCarrier())) {
				AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(leg.getLegInfo().getOperatingCarrier());
				segmentInfo.setOperatedByAirlineInfo(operatingAirline);
			}
			AirAsiaDotRezUtils.setTerminal(leg, segmentInfo);
			populatePriceInfos(segmentInfo, availablefaresMap, journey, segmentNum.get(), legIndex,
					supplierSegmentNumber, isPromoFare);
			segmentNum.getAndIncrement();
			legIndex++;
			segmentInfos.add(segmentInfo);
		}
		return segmentInfos;
	}

	private void populatePriceInfos(SegmentInfo segmentInfo, Map<String, JourneyFare> availablefaresMap,
			AvailableJourney journey, Integer segmentNum, Integer legIndex, Integer supplierSegmentNumber,
			AtomicBoolean isPromoFare) {
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		List<FareReference> arrayOfFares = journey.getFares();
		if (CollectionUtils.isNotEmpty(arrayOfFares)) {
			for (FareReference fareReference : arrayOfFares) {
				PriceInfo priceInfo = PriceInfo.builder().build();
				JourneyFare journeyFare = availablefaresMap.get(fareReference.getFareAvailabilityKey());
				Integer seatCount = fareReference.getDetails().get(0).getAvailableCount();
				priceInfo.setSupplierBasicInfo(supplierConfig.getBasicInfo());
				PriceMiscInfo miscInfo = PriceMiscInfo.builder().build();
				miscInfo.setJourneyKey(journey.getJourneyKey());
				miscInfo.setFareKey(fareReference.getFareAvailabilityKey());
				miscInfo.setLegNum(legIndex);
				priceInfo.setMiscInfo(miscInfo);
				priceInfo.setFareIdentifier(FareType.PUBLISHED);
				int fareIndex = getFareIndexForLeg(journeyFare, supplierSegmentNumber);
				com.airasia.datamodel.FareDetail fareInfo = journeyFare.getFares().get(fareIndex);
				updateFareDetails(priceInfo, seatCount, journeyFare, segmentNum, legIndex, fareInfo, isPromoFare);
				dotRezAirline.processPriceInfo(fareInfo.getProductClass(), priceInfo, supplierConfig);
				priceInfoList.add(priceInfo);
			}
		}
	}

	private void updateFareDetails(PriceInfo priceInfo, Integer seatCount, JourneyFare journeyFare, Integer segmentNum,
			Integer legIndex, com.airasia.datamodel.FareDetail fareInfo, AtomicBoolean isPromoFare) {
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		for (PassengerFare passengerFare : fareInfo.getPassengerFares()) {
			PaxType paxType = getPaxType(passengerFare.getPassengerType());
			FareDetail fareDetail = new FareDetail();
			fareDetail.setCabinClass(CabinClass.ECONOMY);
			fareDetail.setFareBasis(fareInfo.getFareBasisCode());
			fareDetail.setClassOfBooking(fareInfo.getProductClass());
			fareDetail.setSeatRemaining(seatCount);
			Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
			if (segmentNum == 0 && BooleanUtils.isFalse(journeyFare.getIsSumOfSector())
					|| (BooleanUtils.isTrue(journeyFare.getIsSumOfSector()) && legIndex == 0)) {
				for (ServiceCharge serviceCharge : passengerFare.getServiceCharges()) {
					double amount =
							getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
					AirAsiaDotRezCharges dotRezCharge = AirAsiaDotRezCharges.getCharges(serviceCharge);
					if (dotRezCharge != null) {
						dotRezCharge.setFareComponent(fareDetail, paxType.getType(), amount,
								serviceCharge.getCurrencyCode());
					}
					if (AirAsiaDotRezCharges.OTA_COMMISSION.equals(dotRezCharge)
							&& StringUtils.isBlank(priceInfo.getMiscInfo().getAccountCode())) {
						isPromoFare.set(true);
					}
				}
				fareComponents.put(FareComponent.TF, getTotalFare(fareDetail));
			}
			fareDetails.put(paxType, fareDetail);
		}
		if (isPromoFare.get())
			priceInfo.getMiscInfo().setAccountCode(supplierConfig.getSupplierAdditionalInfo().getAccountCodes().get(0));
	}

	private SegmentInfo populateSegmentInfo(Segment segment) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(segment.getDesignator().getDestination()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(segment.getDesignator().getOrigin()));
		segmentInfo.setFlightDesignator(createFlightDesignator(segment.getIdentifier()));
		return segmentInfo;
	}

	private int getFareIndexForLeg(JourneyFare journeyFare, Integer supplierSegmentNumber) {
		int index = 0;
		int fareArraySize = journeyFare.getFares().size();
		if (BooleanUtils.isTrue(journeyFare.getIsSumOfSector())) {
			if (supplierSegmentNumber < fareArraySize) {
				index = supplierSegmentNumber;
			}
		}
		return index;
	}

	private FlightDesignator getFlightDesignator(Segment segment, Leg leg) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(segment.getIdentifier().getCarrierCode()));
		flightDesignator.setEquipType(leg.getLegInfo().getEquipmentType());
		flightDesignator.setFlightNumber(segment.getIdentifier().getIdentifier());
		return flightDesignator;
	}


	private FlightDesignator createFlightDesignator(FlightIdentifier identifier) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(identifier.getCarrierCode()));
		flightDesignator.setEquipType(null);
		flightDesignator.setFlightNumber(identifier.getIdentifier());
		return flightDesignator;
	}

	private GetAvailabilityRQ getAvailabiltyRequest() {
		GetAvailabilityRQ request = GetAvailabilityRQ.builder().build();
		setOriginDestinations(request);
		request.setPassengers(getPassengers());
		request.setFilters(getSearchFilters());
		request.setTaxesAndFees(AirAsiaDotRezConstants.TAXES_AND_FEES);
		request.setSearchDestinationMacs(true);
		request.setSearchOriginMacs(true);
		request.setCodes(buildAdditionalInfo());
		if (CollectionUtils.isNotEmpty(supplierConfig.getSupplierAdditionalInfo().getAccountCodes())) {
			request.setPromotionCode(supplierConfig.getSupplierAdditionalInfo().getAccountCodes().get(0));
		}
		return request;
	}

	private AdditionalInfo buildAdditionalInfo() {
		AdditionalInfo additionalInfo = AdditionalInfo.builder().build();
		additionalInfo.setCurrencyCode(AirSourceConstants.getSupplierInfoCurrencyCode(supplierConfig));
		return additionalInfo;
	}

	private void setOriginDestinations(GetAvailabilityRQ request) {
		request.setOrigin(searchQuery.getRouteInfos().get(0).getFromCityAirportCode());
		request.setDestination(searchQuery.getRouteInfos().get(0).getToCityAirportCode());
		request.setBeginDate(searchQuery.getRouteInfos().get(0).getTravelDate().toString());
		if (searchQuery.isReturn()) {
			request.setEndDate(searchQuery.getRouteInfos().get(1).getTravelDate().toString());
		}
	}

	private SearchFilters getSearchFilters() {
		SearchFilters searchFilters = SearchFilters.builder().build();
		searchFilters.setMaxConnections(AirAsiaDotRezConstants.MAX_CONNECTIONS);
		searchFilters.setCompressionType(getCompressionType());
		searchFilters.setProductClasses(getProductClasses());
		searchFilters.setFareTypes(getFareTypes());
		searchFilters.setSortOptions(getSortOptions());
		searchFilters.setIncludeAllotments(true);
		searchFilters.setFareInclusionType("Default");
		searchFilters.setLoyalty(AirAsiaDotRezConstants.Loyalty);
		return searchFilters;
	}

	private List<String> getFareTypes() {
		List<String> fareTypes = new ArrayList<String>();
		if (Objects.nonNull(supplierConfig.getSupplierAdditionalInfo().getFareTypes())
				&& StringUtils.isNotBlank(supplierConfig.getSupplierAdditionalInfo().getFareTypes(searchQuery)))
			for (String fareType : supplierConfig.getSupplierAdditionalInfo().getFareTypes(searchQuery).split(",")) {
				fareTypes.add(fareType);
			}
		return fareTypes;
	}

	private List<String> getSortOptions() {
		List<String> sortOptions = new ArrayList<String>();
		sortOptions.add(AirAsiaDotRezConstants.FARE_SORT_OPTION);
		return sortOptions;
	}

	private List<String> getProductClasses() {
		List<String> productClasses = new ArrayList<String>();
		if (Objects.nonNull(supplierConfig.getSupplierAdditionalInfo().getProductClasses())
				&& StringUtils.isNotBlank(supplierConfig.getSupplierAdditionalInfo().getProductClasses(searchQuery))) {
			for (String productClass : supplierConfig.getSupplierAdditionalInfo().getProductClasses(searchQuery)
					.split(",")) {
				productClasses.add(productClass);
			}
		}
		return productClasses;
	}

	private String getCompressionType() {
		String compressionType = AirAsiaDotRezConstants.LOWEST_FARECLASS;
		if (supplierConfig != null && supplierConfig.getSupplierAdditionalInfo() != null
				&& StringUtils.isNotBlank(supplierConfig.getSupplierAdditionalInfo().getFareClass())) {
			String fareClass = supplierConfig.getSupplierAdditionalInfo().getFareClass().toUpperCase();
			if (AirAsiaDotRezConstants.COMPRESS_BY_PRODUCTCLASS.toUpperCase().equals(fareClass)) {
				compressionType = AirAsiaDotRezConstants.COMPRESS_BY_PRODUCTCLASS;
			} else if (AirAsiaDotRezConstants.DEFAULT_CLASS.equals(fareClass)) {
				compressionType = AirAsiaDotRezConstants.DEFAULT_CLASS;
			} else {
				compressionType = AirAsiaDotRezConstants.LOWEST_FARECLASS;
			}
		}
		return compressionType;
	}

	public AirSearchResult setInfantPrice(AirSearchResult searchResult) {
		try {
			AtomicBoolean isInfantFarePresentInTrip = new AtomicBoolean();
			if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos()) && infantCount > 0) {
				if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromCache())
						&& searchQuery.getIsDomestic()) {
					fetchAndSetInfantFareFromCache(searchResult, isInfantFarePresentInTrip);
				}
				if (BooleanUtils.isNotTrue(sourceConfiguration.getIsInfantFareFromItineraryPrice())
						&& !isInfantFarePresentInTrip.get()) {
					AirAsiaDotRezSSRManager ssrManager = AirAsiaDotRezSSRManager.builder().sessionId(sessionId)
							.listener(listener).bindingService(bindingService).bookingId(bookingId)
							.sourceConfiguration(sourceConfiguration).searchQuery(searchQuery)
							.supplierConfig(supplierConfig).build();
					ssrManager.init();
					try {
						searchResult.getTripInfos().forEach((tripType, trips) -> {
							if (CollectionUtils.isNotEmpty(trips)) {
								infantFare = new FareDetail();
								sellInfantFare(ssrManager, trips);
							}
						});
					} catch (NoSearchResultException nos) {
						searchResult = null;
					}
				}
			}
		} catch (Exception e) {
			throw new NoSearchResultException(StringUtils.join(","));
		}
		return searchResult;
	}

	private void sellInfantFare(AirAsiaDotRezSSRManager ssrManager, List<TripInfo> copyTripInfos) {
		int index = 0;
		TripInfo tripInfo = null;
		boolean isSuccess = false;
		do {
			try {
				tripInfo = copyTripInfos.get(index);
				ssrManager.getInfantFare(tripInfo);
				ssrManager.setInfantFareToTrip(copyTripInfos, tripInfo);
				isSuccess = true;
			} catch (Exception e) {
				index++;
				log.info("Infant Fare is Not available checking for another trip {}", tripInfo, e);
			}
		} while (!isSuccess && index - 1 < copyTripInfos.size() - 1);

		if (!isSuccess && index == copyTripInfos.size()) {
			throw new NoSearchResultException("Infant Fare Failed for all trips");
		}

	}

	private void fetchAndSetInfantFareFromCache(AirSearchResult searchResult, AtomicBoolean isInfantFarePresent) {
		searchResult.getTripInfos().forEach((tripType, trips) -> {
			infantFare = new FareDetail();
			infantFare =
					FareComponentHelper.getInfantfareFromCache(searchQuery, AirSourceType.AIRASIADOTREZ.getSourceId());
			if (CollectionUtils.isNotEmpty(trips) && infantFare != null
					&& MapUtils.isNotEmpty(infantFare.getFareComponents())) {
				isInfantFarePresent.set(Boolean.TRUE.booleanValue());
				for (TripInfo tripInfo : trips) {
					boolean isFlyThru = isFlyThruFLight(tripInfo.getSegmentInfos());
					tripInfo.getSegmentInfos().forEach(segmentInfo -> {
						copyInfantFareToSegment(segmentInfo, infantFare, isFlyThru);
					});
				}
				AirAsiaDotRezUtils.setTotalFareOnTripInfo(trips);
			}
		});

	}


}
