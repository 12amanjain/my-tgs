package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import java.net.Proxy;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.airasia.datamodel.BaseError;
import com.airasia.datamodel.BaseRequest;
import com.airasia.datamodel.BaseResponse;
import com.airasia.datamodel.GetBookingInStateRS;
import com.airasia.datamodel.PassengerType;
import com.airasia.datamodel.Passengers;
import com.airasia.datamodel.SellTripData;
import com.airasia.datamodel.ServiceCharge;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@NoArgsConstructor
class AirAsiaDotRezServiceManager {

	protected String sessionId;
	protected AirSourceConfigurationOutput sourceConfiguration;
	protected String pnr;
	protected RestAPIListener listener;
	protected String bookingId;
	protected Map<String, String> passengersKeyMap;
	protected FareDetail infantFare;
	protected int adultCount;
	protected int childCount;
	protected int infantCount;
	protected AirAsiaURLBindingService bindingService;
	protected List<FlightTravellerInfo> travellerInfos;
	protected List<String> criticalMessageLogger;
	protected SupplierConfiguration supplierConfig;
	protected AirSearchQuery searchQuery;
	protected User bookingUser;
	protected ClientGeneralInfo clientInfo;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected double totalAmount;
	protected List<FlightTravellerInfo> infants;
	protected List<FlightTravellerInfo> adults;
	protected List<FlightTravellerInfo> children;

	protected AirAsiaDotRezAirline dotRezAirline;

	public String getListnerKey() {
		if (StringUtils.isNotBlank(bookingId)) {
			return bookingId;
		}
		if (searchQuery != null) {
			return searchQuery.getSearchId();
		}
		return null;
	}

	public Proxy proxy() {
		return AirSupplierUtils.getProxy(bookingUser);
	}

	public void init() {
		setPaxCount();
		dotRezAirline = AirAsiaDotRezAirline.AIRASIA;
	}

	public boolean isAnyError(BaseResponse response) {
		boolean isError = false;
		if (CollectionUtils.isNotEmpty(response.getErrors())) {
			String errorMessage = null;
			for (BaseError error : response.getErrors()) {
				if (StringUtils.equalsIgnoreCase("type", error.getType().toLowerCase())) {
					errorMessage = StringUtils.join(error.getCode(), " ", error.getType(), " ", error.getMessage());
					if (StringUtils.isNotBlank(error.getRawMessage())) {
						errorMessage = StringUtils.join(error.getRawMessage());
					}
					isError = true;
				}
			}
			if (criticalMessageLogger == null) {
				criticalMessageLogger = new ArrayList<>();
			}
			criticalMessageLogger.add(errorMessage);
		}
		return isError;
	}

	public SupplierCredential getCredential() {
		return supplierConfig.getSupplierCredential();
	}

	public Map<String, String> headerParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Content-Type", "application/json");
		if (StringUtils.isNotEmpty(sessionId)) {
			headerParams.put("Authorization", sessionId);
			headerParams.put("Accept-Encoding", "application/gzip");
		}
		return headerParams;
	}

	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}


	public static Double getTotalFare(FareDetail fareDetail) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			fareDetail.getFareComponents().forEach((fareComponent, amount) -> {
				if (!fareComponent.equals(FareComponent.TF))
					totalAmount.addAndGet(amount);
			});
		}
		return totalAmount.doubleValue();
	}


	public boolean isFlyThruFLight(List<SegmentInfo> segmentInfos) {
		boolean isFlyThru = false;
		for (SegmentInfo segment : segmentInfos) {
			if (segment.getPriceInfo(0).getMiscInfo().getLegNum() > 0) {
				isFlyThru = true;
				break;
			}
		}
		return isFlyThru;
	}

	public void setPaxCount() {
		if (Objects.nonNull(searchQuery)) {
			this.adultCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
			this.childCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			this.infantCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT);
		}
	}

	public void initializeTravellerInfo(List<FlightTravellerInfo> travellerInfos) {
		adults = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT);
		children = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD);
		infants = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT);
		this.adultCount = adults.size();
		this.childCount = children.size();
		this.infantCount = infants.size();
	}

	public FareDetail getInfantFareBasedOnSourceType(TripInfo reviewedTrip, AtomicInteger segmentNum,
			SegmentInfo segmentInfo) {
		// Considering infant fare will be same for all the segments.
		if (isInfantFareOnSegmentWise(segmentInfo)) {
			return reviewedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.INFANT);
		}
		return infantFare;
	}

	public boolean isInfantFareOnSegmentWise(SegmentInfo segmentInfo) {
		return true;
	}

	public SegmentInfo setInfantFareToTrip(SegmentInfo segmentInfo, FareDetail oldfareDetail) {
		if (oldfareDetail != null && MapUtils.isNotEmpty(oldfareDetail.getFareComponents())) {
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				FareDetail fareDetail = priceInfo.getFareDetail(PaxType.INFANT, new FareDetail());
				if (MapUtils.isEmpty(fareDetail.getFareComponents())) {
					fareDetail.setFareComponents(new HashMap<>());
					oldfareDetail.getFareComponents().forEach((fc, amount) -> {
						fareDetail.getFareComponents().put(fc, amount);
					});
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					priceInfo.getFareDetail(PaxType.INFANT, fareDetail)
							.setFareComponents(fareDetail.getFareComponents());
				}
			});
		}
		return segmentInfo;
	}

	protected Double getCharges(List<ServiceCharge> serviceCharges) {
		double charges = 0.0;
		double discount = 0.0;
		for (ServiceCharge charge : serviceCharges) {
			AirAsiaDotRezCharges chargeType = AirAsiaDotRezCharges.getCharges(charge);
			// type 1 is discount
			if (chargeType != null && chargeType.getTypeOrdinal() == 1) {
				discount += charge.getAmount();
			} else {
				if (chargeType != null)
					charges = charges + charge.getAmount();
			}
		}
		double finalCharge = charges - discount;
		return finalCharge >= 0.0 ? finalCharge : 0.0;
	}

	protected boolean isMatchedSegment(SegmentInfo segment, String reference) {
		if (reference.contains(getDeptDate(segment.getDepartTime().toLocalDate()))
				&& reference.contains(segment.getFlightDesignator().getAirlineCode())
				&& reference.contains(segment.getFlightNumber())) {
			return true;
		}
		return false;
	}

	protected String getDeptDate(LocalDate deptDate) {
		return deptDate.toString().replace("-", "");
	}

	public PaxType getPaxType(String paxTypeCode) {
		if (StringUtils.equalsIgnoreCase("ADT", paxTypeCode)) {
			return PaxType.ADULT;
		} else if (StringUtils.equalsIgnoreCase("CHD", paxTypeCode)) {
			return PaxType.CHILD;
		} else {
			return PaxType.INFANT;
		}
	}

	public String getPaxTypeCode(PaxType paxType) {
		String paxTypeCode = paxType.getType();
		if (paxType.equals(PaxType.CHILD)) {
			paxTypeCode = "CHD";
		}
		return paxTypeCode;

	}

	public String getCurrencyCode() {
		String currencyCode = getCurrencyCodeFromAirPortInfo();
		if (StringUtils.isBlank(currencyCode)) {
			if (supplierConfig != null
					&& StringUtils.isNotBlank(supplierConfig.getSupplierCredential().getCurrencyCode())) {
				return supplierConfig.getSupplierCredential().getCurrencyCode();
			}
			clientInfo = ServiceCommunicatorHelper.getClientInfo();
			if (clientInfo != null && StringUtils.isNotBlank(clientInfo.getCurrencyCode())) {
				return clientInfo.getCurrencyCode();
			}
		}
		return currencyCode;
	}

	private String getCurrencyCodeFromAirPortInfo() {
		RouteInfo sourceAirport = dotRezAirline.getCurrencyCode(searchQuery);
		if (sourceAirport != null) {
			if (CollectionUtils.isNotEmpty(sourceConfiguration.getCurrencyCodes())) {
				List<KeyValue> currencyList = sourceConfiguration.getCurrencyCodes();
				Optional<KeyValue> currencyCode = currencyList.stream()
						.filter(currencyKey -> currencyKey.getKey()
								.equalsIgnoreCase(sourceAirport.getFromCityOrAirport().getCountryCode())
								|| currencyKey.getKey().contains(sourceAirport.getFromCityAirportCode()))
						.findFirst();
				if (currencyCode.isPresent()) {
					return currencyCode.get().getValue();
				}
			}
		}
		return null;
	}

	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (fromCurrency.equalsIgnoreCase(toCurrency)) {
			return amount;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(AirSourceType.AIRASIADOTREZ.name().toUpperCase()).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	public GetBookingInStateRS getBookingInState() {
		HttpUtils httpUtils = null;
		GetBookingInStateRS bookingResponse = null;
		try {
			httpUtils = HttpUtils.builder().headerParams(headerParams()).proxy(proxy())
					.urlString(bindingService.bookingStateUrl()).build();
			bookingResponse = httpUtils.getResponse(GetBookingInStateRS.class).orElse(null);
			totalAmount = bookingResponse.getData().getBreakdown().getBalanceDue();
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getResponseString(), "GetBookingInStateRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("5-GetBookingInState").build());
		}
		return bookingResponse;
	}

	public SellTripData getBookingPnr() {
		GetBookingInStateRS bookingresponse = getBookingInState();
		return bookingresponse.getData();
	}

	public void copyInfantFareToSegment(SegmentInfo segmentInfo, FareDetail infantFareDetail, boolean isFlyThru) {
		segmentInfo.getPriceInfoList().forEach(priceInfo -> {
			FareDetail fareDetail = new FareDetail();
			fareDetail.setFareComponents(new HashMap<>());
			if (infantFareDetail != null && MapUtils.isNotEmpty(infantFareDetail.getFareComponents())) {
				if (isInfantFareOnSegmentWise(segmentInfo)
						&& (!isFlyThru || priceInfo.getMiscInfo().getLegNum() == 0)) {
					infantFareDetail.getFareComponents().forEach((fc, amount) -> {
						fareDetail.getFareComponents().put(fc, amount);
					});
				}
			}
			fareDetail.setCabinClass(searchQuery.getCabinClass());
			priceInfo.getFareDetail(PaxType.INFANT, new FareDetail()).setFareComponents(fareDetail.getFareComponents());
		});

	}

	public void commitBooking(String methodType) {
		HttpUtils httpUtils = null;
		try {
			BaseRequest request = BaseRequest.builder().build();
			httpUtils = HttpUtils.builder().headerParams(headerParams()).proxy(proxy())
					.postData(GsonUtils.getGson().toJson(request)).requestMethod(methodType)
					.urlString(bindingService.commitUrl()).build();
			BaseResponse response = httpUtils.getResponse(BaseResponse.class).orElse(null);
			if (isAnyError(response)) {
				throw new SupplierUnHandledFaultException("Supplier Payment Failed");
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getResponseString(), "CommitBookingRQ"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("8-CommitBooking").build());
		}
	}

	public Passengers getPassengers() {
		Passengers passengers = Passengers.builder().build();
		passengers.setTypes(getPassengerTypes());
		return passengers;
	}

	public List<PassengerType> getPassengerTypes() {
		List<PassengerType> passengerTypes = new ArrayList<PassengerType>();
		searchQuery.getPaxInfo().forEach((paxType, count) -> {
			if (count > 0 && !paxType.equals(PaxType.INFANT)) {
				String paxTypeCode = getPaxTypeCode(paxType);
				PassengerType passengerType = PassengerType.builder().build();
				passengerType.setCount(count);
				passengerType.setType(paxTypeCode);
				passengerTypes.add(passengerType);
			}
		});
		return passengerTypes;
	}

}
