package com.tgs.services.fms.sources.airasiadotrez;

import java.io.IOException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.airasia.datamodel.Credentials;
import com.airasia.datamodel.TokenCreateRQ;
import com.airasia.datamodel.TokenCreateRS;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class AirAsiaDotRezTokenManager extends AirAsiaDotRezServiceManager {

	protected int sessionValidMinutes;

	public String createToken() {
		TokenCreateRS tokenCreateRS = null;
		HttpUtils httpUtils = null;
		try {
			TokenCreateRQ tokenRQ = TokenCreateRQ.builder().credentials(getCredentials()).build();
			httpUtils = HttpUtils.builder().urlString(bindingService.loginUrl())
					.postData(GsonUtils.getGson().toJson(tokenRQ)).headerParams(headerParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			tokenRQ.setApplicationName(StringUtils.EMPTY);
			tokenCreateRS = httpUtils.getResponse(TokenCreateRS.class).orElse(null);
			if (isAnyError(tokenCreateRS)) {
				throw new SupplierSessionException("Token Creation Failed");
			}
			sessionValidMinutes = tokenCreateRS.getData().getIdleTimeoutInMinutes();
			return tokenCreateRS.getData().getToken();
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "TokenCreateRQ"),
					formatRQRS(httpUtils.getResponseString(), "TokenCreateRS"));
			listener.addLog(LogData.builder().key(getListnerKey()).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("1-L-TokenCreate").build());
		}
	}

	public Credentials getCredentials() {
		Credentials cred = Credentials.builder().username(getCredential().getUserName())
				.password(getCredential().getPassword()).domain(getCredential().getDomain()).location(null).build();
		return cred;
	}

}
