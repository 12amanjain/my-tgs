package com.tgs.services.fms.sources.airasiadotrez;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.airasia.datamodel.Leg;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirAsiaDotRezUtils {

	public static String getNationality(FlightTravellerInfo flightTravellerInfo) {
		if (StringUtils.isNotBlank(flightTravellerInfo.getPassportNationality())) {
			return flightTravellerInfo.getPassportNationality();
		}
		ClientGeneralInfo generalInfo = ServiceCommunicatorHelper.getClientInfo();
		return generalInfo.getNationality();
	}

	public static void setTerminal(Leg leg, SegmentInfo segmentInfo) {
		if (leg.getLegInfo() != null && segmentInfo.getDepartAirportInfo() != null
				&& segmentInfo.getArrivalAirportInfo() != null) {
			if (StringUtils.isNotBlank(leg.getLegInfo().getDepartureTerminal())) {
				segmentInfo.getDepartAirportInfo()
						.setTerminal(AirUtils.getTerminalInfo(leg.getLegInfo().getDepartureTerminal()));
			}
			if (StringUtils.isNotBlank(leg.getLegInfo().getArrivalTerminal())) {
				segmentInfo.getArrivalAirportInfo()
						.setTerminal(AirUtils.getTerminalInfo(leg.getLegInfo().getArrivalTerminal()));
			}
		}
	}


	public static String getTitle(FlightTravellerInfo travellerInfo) {
		if (PaxType.CHILD.getType().equals(travellerInfo.getPaxType().getType())) {
			return "CHD";
		} else if (PaxType.INFANT.equals(travellerInfo.getPaxType())) {
			return StringUtils.EMPTY;
		}
		return travellerInfo.getTitle().toUpperCase();
	}

	public static void setTotalFareOnTripInfo(List<TripInfo> tripList) {
		tripList.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getBookingRelatedInfo() != null) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(travellerInfo -> {
						if (travellerInfo.getFareDetail() != null) {
							travellerInfo.getFareDetail().getFareComponents().put(FareComponent.TF,
									getTotalFare(travellerInfo.getFareDetail()));
						}
					});
				} else if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						priceInfo.getFareDetails().forEach((paxType, fare) -> {
							Double totalFare = getTotalFare(fare);
							if (totalFare != 0) {
								priceInfo.getFareDetail(paxType).getFareComponents().put(FareComponent.TF,
										getTotalFare(fare));
							}
						});
					});
				}
			});
		});
	}

	public static Double getTotalFare(FareDetail fareDetail) {
		return fareDetail.getAirlineFare();
	}


	public static String getGender(String title, FlightTravellerInfo travellerInfo) {
		if (title.toUpperCase().equalsIgnoreCase("MR") || isMaster(travellerInfo)) {
			return "Male";
		} else if (title.toUpperCase().equalsIgnoreCase("MRS") || isMiss(travellerInfo)) {
			return "Female";
		}
		return null;
	}

	public static boolean isMaster(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MASTER")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MSTR");
		}
		return false;
	}

	public static boolean isMiss(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MISS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	public static boolean isMrsMs(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MRS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	public static LocalDate getDOB(String dob) {
		return LocalDateTime.parse(dob).toLocalDate();
	}

	public static boolean setIsReturnSegment(List<TripInfo> tripInfos, User bookingUser) {
		if (CollectionUtils.isNotEmpty(tripInfos) && tripInfos.size() == 2) {
			TripInfo onwardTrip = tripInfos.get(0);
			TripInfo returnTrip = tripInfos.get(1);
			String onwardDepartureAirport = onwardTrip.getDepartureAirportCode();
			String onwardArrivalAirport = onwardTrip.getArrivalAirportCode();
			String returnDepartureAirport = returnTrip.getDepartureAirportCode();
			String retunrArrivalAirport = returnTrip.getArrivalAirportCode();
			if ((onwardDepartureAirport.equals(retunrArrivalAirport)
					|| AirUtils.isGNNearByAirport(onwardDepartureAirport, onwardTrip.getSupplierInfo().getSourceId(),
							null, bookingUser)
					|| AirUtils.isGNNearByAirport(retunrArrivalAirport, onwardTrip.getSupplierInfo().getSourceId(),
							null, bookingUser))
					&& (onwardArrivalAirport.equals(returnDepartureAirport)
							|| AirUtils.isGNNearByAirport(onwardArrivalAirport,
									onwardTrip.getSupplierInfo().getSourceId(), null, bookingUser)
							|| AirUtils.isGNNearByAirport(returnDepartureAirport,
									onwardTrip.getSupplierInfo().getSourceId(), null, bookingUser))) {
				return true;
			}
		}
		return false;
	}

	public static FlightBasicFact getSSRFlightFact(TripInfo tripInfo) {
		FlightBasicFact flightFact = FlightBasicFact.builder().build();
		SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
		Integer sourceId = tripInfo.getSegmentInfos().get(0).getSupplierInfo().getSourceId();
		String airline = segmentInfo.getAirlineCode(false);
		flightFact.setAirline(airline);
		flightFact.setSourceId(sourceId);
		return flightFact;
	}

	/*
	 * While adding adult or child , the request method is PUT and while adding infant to existing adult, the request is
	 * post.
	 */
	public static String getRequestMethod(PaxType paxType) {
		return (PaxType.INFANT.equals(paxType) ? "POST" : "PUT");
	}

}
