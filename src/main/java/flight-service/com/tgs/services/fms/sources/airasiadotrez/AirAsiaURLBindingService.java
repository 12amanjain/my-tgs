package com.tgs.services.fms.sources.airasiadotrez;


import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import java.time.LocalDateTime;

@Builder
@Setter
public class AirAsiaURLBindingService {

	public static final String LOGIN_ENDPOINT = "/api/nsk/v1/token";
	public static final String SEARCH_ENDPOINT = "/api/nsk/v4/availability/search/simple";
	public static final String SELL_ENDPOINT = "/api/nsk/v4/trip/sell";
	public static final String SSR_ENDPOINT = "/api/nsk/v2/booking/ssrs/availability";
	public static final String CONTACT_ENDPOINT = "/api/nsk/v1/booking/contacts";
	public static final String SELLSSR_ENDPOINT = "/api/nsk/v2/booking/ssrs/";
	public static final String GST_ENDPOINT = "/api/nsk/v1/token";
	public static final String PAX_ENDPOINT = "/api/nsk/v3/booking/passengers/";
	public static final String BOOKING_STATE_ENDPOINT = "/api/nsk/v1/booking";
	public static final String PAYMENT_ENDPOINT = "/api/nsk/v2/booking/payments";
	public static final String COMMIT_ENDPOINT = "/api/nsk/v3/booking";
	public static final String BOOKING_QUOTE = "/api/nsk/v2/bookings/quote";
	public static final String SEAT_ENDPOINT = "/api/nsk/v3/booking/seatmaps/journey/";
	public static final String RETRIEVE_PNR = "/api/nsk/v1/booking/retrieve/byRecordLocator/";
	public static final String CANCEL_JOURNEY = "/api/nsk/v1/booking/journeys";
	public static final String DIVIDE_PNR = "/api/nsk/v2/booking/divide";
	public static final String SEAT_ASSIGNMENT = "/api/nsk/v2/booking/passengers/";

	private final static String SLASH = "/";
	private final static String SEATS = "seats";
	private final static String PROPERTY_LOOKUP = "?IncludePropertyLookup=true";

	private SupplierConfiguration configuration;

	@Getter
	protected FlightAPIURLRuleCriteria apiUrls;

	private GeneralCachingCommunicator cachingComm;


	public String loginUrl() {
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getUrl())) {
			boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
			String prefixUrl = configuration.getSupplierCredential().getUrl();
			if (isEndWithSlash) {
				prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
			}
			return StringUtils.join(prefixUrl, LOGIN_ENDPOINT);
		}
		return apiUrls.getSecurityURL();
	}

	public String simpleAvailabilityUrl() {
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getUrl())) {
			boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
			String prefixUrl = configuration.getSupplierCredential().getUrl();
			if (isEndWithSlash) {
				prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
			}
			return StringUtils.join(prefixUrl, SEARCH_ENDPOINT);
		}
		return apiUrls.getTravelAgent();
	}

	public String bookingQuoteUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, BOOKING_QUOTE);
	}

	public String sellUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, SELL_ENDPOINT);
	}

	public String sellSSRUrl(String ssrkey) {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, SELLSSR_ENDPOINT, ssrkey);
	}

	public String ssrAvailabilityUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, SSR_ENDPOINT);
	}

	public String contactUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, CONTACT_ENDPOINT);
	}

	public String passengerUrl(String passenegerKey) {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, PAX_ENDPOINT, passenegerKey);
	}

	public String bookingStateUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, BOOKING_STATE_ENDPOINT);
	}

	public String paymentUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, PAYMENT_ENDPOINT);
	}

	public String commitUrl() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, COMMIT_ENDPOINT);
	}

	public String seatURL(String journeyKey) {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, SEAT_ENDPOINT, journeyKey, SLASH, PROPERTY_LOOKUP);
	}


	public String sellSeatUrl(String paxKey, String ssrKey) {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, SEAT_ASSIGNMENT, paxKey, SLASH, SEATS, SLASH, ssrKey);
	}


	public String getAuthToken() {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(configuration.getBasicInfo().getRuleId().toString())
				.compress(false).index(0).build();
		metaInfo = cachingComm.fetchFromQueue(metaInfo);
		if (metaInfo == null) {
			metaInfo = SessionMetaInfo.builder().key(configuration.getBasicInfo().getRuleId().toString()).index(-1)
					.compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
		}
		if (metaInfo != null && metaInfo.getValue() != null) {
			return (String) metaInfo.getValue();
		}
		return null;
	}

	public void storeToken(String token, int validMinutes) {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(configuration.getBasicInfo().getRuleId().toString())
				.compress(false).build();
		metaInfo.setValue(token);
		metaInfo.setExpiryTime(LocalDateTime.now().plusMinutes(validMinutes));
		cachingComm.storeInQueue(metaInfo);
	}

	public String retrievePNRUrl(String pnr) {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, RETRIEVE_PNR, pnr);
	}

	public String journeyURL(String key, boolean isCompleteCancellation) {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		if (!isCompleteCancellation) {
			return StringUtils.join(prefixUrl, CANCEL_JOURNEY, "/", key);
		}
		return StringUtils.join(prefixUrl, CANCEL_JOURNEY);
	}

	public String dividePNRURL() {
		boolean isEndWithSlash = configuration.getSupplierCredential().getUrl().endsWith(SLASH);
		String prefixUrl = configuration.getSupplierCredential().getUrl();
		if (isEndWithSlash) {
			prefixUrl = prefixUrl.subSequence(0, prefixUrl.length() - 1).toString();
		}
		return StringUtils.join(prefixUrl, DIVIDE_PNR);
	}

	public String passengerUrl(String paxKey, boolean isInfant) {
		String url = passengerUrl(paxKey);
		if (isInfant) {
			url = StringUtils.join(url, "/infant");
		}
		return url;
	}

}
