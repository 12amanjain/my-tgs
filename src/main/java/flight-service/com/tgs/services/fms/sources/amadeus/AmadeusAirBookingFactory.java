package com.tgs.services.fms.sources.amadeus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.amadeus.xml.AmadeusBookingWebServicesStub;
import com.amadeus.xml.AmadeusWebServicesStub;
import com.amadeus.xml.pnradd_15_1_1a.PNR_AddMultiElements;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
@Qualifier("amadeusAirBookingFactory")
public class AmadeusAirBookingFactory extends AbstractAirBookingFactory {

	protected AmadeusBookingManager bookingManager;

	protected AmadeusRetrieveBookingManager retrieveManager;

	protected SoapRequestResponseListner listener;

	protected AmadeusWebServicesStub webServicesStub;

	protected AmadeusBookingWebServicesStub bookingStub;

	protected AmadeusBindingService bindingService;
	protected PNR_AddMultiElements ssrElements;

	public AmadeusAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		if (listener == null) {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		}
		bindingService = AmadeusBindingService.builder().cacheCommunicator(cachingComm).configuration(supplierConf)
				.user(bookingUser).build();
		webServicesStub = bindingService.getWebServiceStub(bookingSegments.getSearchQuery());
		bookingStub = bindingService.getBookingWebServiceStub(bookingSegments.getSearchQuery());
		if (bookingManager == null) {
			bookingManager = AmadeusBookingManager.builder().listener(listener).deliveryInfo(deliveryInfo)
					.supplierConfiguration(supplierConf).bookingUser(bookingUser).build();
			List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
			List<SegmentInfo> segmentInfosCopy =
					AirUtils.checkAndApplyPrivateFareLogic(bookingSegments.getSegmentInfos());
			bookingManager.init(bookingSegments.getSearchQuery(), isHoldBooking, segmentInfos, segmentInfosCopy, order);
			bookingManager.setCriticalMessageLogger(criticalMessageLogger);
			bookingManager.setBookingStub(bookingStub);
			bookingManager.setSourceConfig(sourceConfiguration);
			bookingManager.setBookingId(bookingId);
			bookingManager.setWebServicesStub(webServicesStub);
			ssrElements = new PNR_AddMultiElements();
		}
		if (retrieveManager == null) {
			retrieveManager = AmadeusRetrieveBookingManager.builder().listener(listener)
					.supplierConfiguration(supplierConf).bookingUser(bookingUser).build();
			retrieveManager.setBookingStub(bookingStub);
			retrieveManager.setWebServicesStub(webServicesStub);
			retrieveManager.setBookingId(bookingId);
			retrieveManager.setCriticalMessageLogger(criticalMessageLogger);
		}
	}


	@Override
	public boolean doBooking() {
		boolean isBookingSuccess = false;
		isTkRequired = true;
		String sessionToken = bookingSegments.getSupplierSession().getSupplierSessionInfo().getSessionToken();
		AmadeusSessionManager sessionManager = null;
		ssrElements = new PNR_AddMultiElements();
		initialize();
		sessionManager = AmadeusSessionManager.builder().listener(listener).supplierConfiguration(supplierConf).build();
		try {
			sessionManager.setCriticalMessageLogger(criticalMessageLogger);
			sessionManager.init(sessionToken);
			sessionManager.setWebServicesStub(webServicesStub);
			bookingManager.init(sessionToken);
			bookingManager.setMoneyExchnageComm(moneyExchangeComm);
			retrieveManager.init(sessionToken);
			bookingManager.setRetrieveManager(retrieveManager);
			bookingManager.setGmsComm(gmsCommunicator);
			bookingManager.setBookingStub(bookingStub);
			retrieveManager.setBookingStub(bookingStub);
			// overriding pax count
			bookingManager.setPaxCount();
			bookingManager.sendPNRAddMultiElements();
			bookingManager.pricePNRwithBookingClass();
			if (!isFareDiff(bookingManager.getSupplierAmount())) {
				bookingManager.sendTicketCreateTST();
				bookingManager.commitPNR();
				String crsPNR = bookingManager.getCrsPNR();
				if (StringUtils.isNotBlank(crsPNR)) {
					bookingSegments.setSupplierBookingId(crsPNR);
					BookingUtils.updateSupplierBookingId(bookingSegments.getSegmentInfos(), crsPNR);
					bookingManager.retrieveAirlinePNR(crsPNR);
					isBookingSuccess = true;
					this.addAdditionalCommand();
					bookingManager.commitSSRs(ssrElements, gstInfo);
					bookingManager.commitPNR();
					bookingManager.isPNRStatusActive(5, false);
					pnr = crsPNR;
				}
			}
		} finally {
			sessionManager.closeSession();
			if (isHoldBooking && isBookingSuccess) {
				Calendar timeLimit = TgsDateUtils.getCalendar(bookingManager.getTicketingTimeLimit());
				updateTimeLimit(timeLimit);
			} else {
				storeStubs();
			}
		}
		return isBookingSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isBookingSuccess = false;
		AmadeusSessionManager sessionManager = null;
		try {
			initialize();
			sessionManager =
					AmadeusSessionManager.builder().listener(listener).supplierConfiguration(supplierConf).build();
			sessionManager.setWebServicesStub(webServicesStub);
			sessionManager.setCriticalMessageLogger(criticalMessageLogger);
			sessionManager.openSession();
			retrieveManager.init(sessionManager.getSession());
			bookingManager.init(sessionManager.getSession());
			bookingManager.setMoneyExchnageComm(moneyExchangeComm);
			bookingManager.setRetrieveManager(retrieveManager);
			bookingManager.setCrsPNR(bookingSegments.getSupplierBookingId());
			if (bookingManager.isPNRStatusActive(5, false)) {
				boolean isSameFare = true;
				if (!isSameDayTicketing()) {
					isSameFare = false;
					bookingManager.pricePNRwithBookingClass();
					if (!isFareDiff(bookingManager.getSupplierAmount())) {
						isSameFare = true;
						bookingManager.sendTicketCreateTST();
						bookingManager.commitPNR();
					}
				}
				if (isSameFare) {
					bookingManager.sendFOP(getSupplierBookingCreditCard());
					bookingManager.commitPNR();
					bookingManager.isPNRStatusActive(5, true);
					bookingManager.issueTicket();
					bookingManager.commitTicketNumber();
					isBookingSuccess = BookingUtils.hasTicketNumberInAllSegments(bookingSegments.getSegmentInfos());
				}
			}
		} finally {
			sessionManager.closeSession();
			storeStubs();
		}
		return isBookingSuccess;
	}

	@Override
	public boolean confirmBeforeTicket() {
		boolean isFareChange = false;
		AmadeusSessionManager sessionManager = null;
		try {
			initialize();
			sessionManager =
					AmadeusSessionManager.builder().listener(listener).supplierConfiguration(supplierConf).build();
			sessionManager.setWebServicesStub(webServicesStub);
			sessionManager.setCriticalMessageLogger(criticalMessageLogger);
			sessionManager.openSession();
			retrieveManager.init(sessionManager.getSession());
			bookingManager.init(sessionManager.getSession());
			bookingManager.setMoneyExchnageComm(moneyExchangeComm);
			SegmentBookingRelatedInfo bookingRelatedInfo =
					bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo();
			String crsPNR = bookingRelatedInfo.getTravellerInfo().get(0).getSupplierBookingId();
			bookingManager.setRetrieveManager(retrieveManager);
			bookingManager.setCrsPNR(crsPNR);
			if (bookingManager.isPNRStatusActive(5, false) && !isSameDayTicketing()) {
				bookingManager.pricePNRwithBookingClass();
				isFareChange = isFareDiff(bookingManager.getSupplierAmount());
			}
		} finally {
			sessionManager.closeSession();
			storeStubs();
		}
		return isFareChange;
	}

	protected void addAdditionalCommand() {
		bookingManager.dataElementDiv = new ArrayList<>();
		if (ssrElements == null) {
			ssrElements = new PNR_AddMultiElements();
		}
		bookingManager.dataElementDiv = new ArrayList<>();
		bookingManager.buildMarker(ssrElements);
		bookingManager.buildPNRAction(ssrElements, AmadeusConstants.PNR_ADD_DETAILS_CODE);
		bookingManager.buildReceivedFrom(bookingId);
		return;
	}

	private void storeStubs() {
		bindingService = AmadeusBindingService.builder().cacheCommunicator(cachingComm).configuration(supplierConf)
				.user(bookingUser).build();
		bindingService.cleanUp(bookingStub);
		bindingService.storeSessionMetaInfo(supplierConf, AmadeusBookingWebServicesStub.class, bookingStub);
		bindingService.cleanUp(webServicesStub);
		bindingService.storeSessionMetaInfo(supplierConf, AmadeusWebServicesStub.class, webServicesStub);
	}
}
