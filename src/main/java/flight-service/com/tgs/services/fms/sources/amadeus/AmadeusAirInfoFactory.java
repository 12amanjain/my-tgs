package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import com.amadeus.xml.AmadeusReviewWebServicesStub;
import com.amadeus.xml.AmadeusWebServicesStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AmadeusAirInfoFactory extends AbstractAirInfoFactory {

	protected SoapRequestResponseListner listener;

	protected AmadeusBindingService bindingService;

	protected AmadeusWebServicesStub webServicesStub;

	protected AmadeusReviewWebServicesStub reviewServicesStub;

	protected AmadeusMasterPriceSearchManager searchManager;

	public AmadeusAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public AmadeusAirInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	public void initialize(boolean isSearch, String bookingId) {
		bindingService = AmadeusBindingService.builder().cacheCommunicator(cachingComm).configuration(supplierConf)
				.user(user).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
		if (isSearch) {
			listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
					supplierConf.getBasicInfo().getSupplierName());
		} else {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		}
	}


	@Override
	public void searchAvailableSchedules() {
		initialize(true, null);
		searchManager = AmadeusMasterPriceSearchManager.builder().listener(listener).supplierConfiguration(supplierConf)
				.searchQuery(searchQuery).build();
		searchManager.init(null);
		searchManager.setCriticalMessageLogger(criticalMessageLogger);
		searchManager.setMoneyExchnageComm(moneyExchnageComm);
		this.initNoOfRecommendations();
		searchResult = new AirSearchResult();
		Map<AirSearchQuery, Future<AirSearchResult>> futureTaskMap = new LinkedHashMap<>();
		if (searchQuery.isDomesticReturn()) {
			// DomReturn-SpecialReturn results
			AmadeusMasterPriceSearchManager searchManager1 = buildSearchManager(searchManager, searchQuery);
			Future<AirSearchResult> searchResultFuture1 =
					ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager1.doMPTBSearch());
			futureTaskMap.put(searchQuery, searchResultFuture1);
			// OneWay-Oneway results
			Pair<AirSearchQuery, AirSearchQuery> oneWayQueries = searchManager.splitQueries(searchQuery);
			AmadeusMasterPriceSearchManager searchManager2 = buildSearchManager(searchManager, oneWayQueries.getLeft());
			Future<AirSearchResult> searchResultFuture2 =
					ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager2.doMPTBSearch());
			futureTaskMap.put(oneWayQueries.getLeft(), searchResultFuture2);
			// Return-Oneway results
			AmadeusMasterPriceSearchManager searchManager3 =
					buildSearchManager(searchManager, oneWayQueries.getRight());
			Future<AirSearchResult> searchResultFuture3 =
					ExecutorUtils.getFlightSearchThreadPool().submit(() -> searchManager3.doMPTBSearch());
			futureTaskMap.put(oneWayQueries.getRight(), searchResultFuture3);
		} else {
			AmadeusMasterPriceSearchManager searchManager4 = buildSearchManager(searchManager, searchQuery);
			searchResult = searchManager4.doMPTBSearch();
		}
		for (AirSearchQuery sQuery : futureTaskMap.keySet()) {
			try {
				AirSearchResult newSResult = futureTaskMap.get(sQuery).get(30, TimeUnit.SECONDS);
				if (searchResult == null || MapUtils.isEmpty(searchResult.getTripInfos())) {
					searchResult = newSResult;
				} else {
					searchResult = AmadeusUtils.mergeSearchResult(searchResult, newSResult, sQuery);
				}
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				if (CollectionUtils.isNotEmpty(criticalMessageLogger)) {
					criticalMessageLogger.add(e.getMessage());
				} else {
					log.error(AirSourceConstants.AIR_SUPPLIER_SEARCH, supplierConf.getBasicInfo().getDescription(),
							searchQuery.getSearchId(), e);
				}
			}
		}
	}

	@Override
	public TripInfo review(TripInfo selectedTrip, String bookingId) {
		AmadeusSessionManager sessionManager = AmadeusSessionManager.builder().bookingId(bookingId).listener(listener)
				.supplierConfiguration(supplierConf).criticalMessageLogger(criticalMessageLogger).bookingUser(user)
				.build();
		initialize(false, bookingId);
		webServicesStub = bindingService.getWebServiceStub(searchQuery);
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.setListener(listener);
		AmadeusReviewManager reviewManager = null;
		boolean isSellSuccess = false;
		try {
			sessionManager.openSession();
			reviewServicesStub = bindingService.getReviewWebServiceStub(searchQuery);
			reviewManager = AmadeusReviewManager.builder().listener(listener).supplierConfiguration(supplierConf)
					.searchQuery(searchQuery).bookingId(bookingId).moneyExchnageComm(moneyExchnageComm)
					.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			reviewManager.setSession(sessionManager.getSessionSchema());
			reviewManager.setServicesStub(reviewServicesStub);
			reviewManager.init(sessionManager.getSession());
			selectedTrip = reviewManager.doFareInformativePricing(selectedTrip);
			if (selectedTrip != null) {
				isSellSuccess = reviewManager.doSellRecommendation(selectedTrip);
				reviewManager.setSSRInfos(selectedTrip);
			}
		} finally {
			if (isSellSuccess && selectedTrip != null) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().build();
				sessionInfo.setSessionToken(reviewManager.getSession());
				storeBookingSession(bookingId, sessionInfo, null, selectedTrip);
			}
			storeStubs();
		}
		return selectedTrip;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		initialize(false, bookingId);
		AmadeusSessionManager sessionManager = AmadeusSessionManager.builder().bookingId(bookingId).listener(listener)
				.supplierConfiguration(supplierConf).criticalMessageLogger(criticalMessageLogger).bookingUser(user)
				.build();
		searchQuery = AirUtils.getSearchQueryFromTripInfo(selectedTrip);
		webServicesStub = bindingService.getWebServiceStub(searchQuery);
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.openSession();
		TripFareRule tripFareRule = TripFareRule.builder().build();
		try {
			reviewServicesStub = bindingService.getReviewWebServiceStub(searchQuery);
			AmadeusReviewManager reviewManager = AmadeusReviewManager.builder().listener(listener)
					.supplierConfiguration(supplierConf).searchQuery(searchQuery).moneyExchnageComm(moneyExchnageComm)
					.criticalMessageLogger(criticalMessageLogger).bookingId(bookingId).bookingUser(user).build();
			reviewManager.setListener(listener);
			reviewManager.setSession(sessionManager.getSessionSchema());
			reviewManager.setServicesStub(reviewServicesStub);
			reviewManager.init(sessionManager.getSession());
			reviewManager.setSearchQuery(searchQuery);
			selectedTrip = reviewManager.doFareInformativePricing(selectedTrip);
			if (isDetailed) {
				AmadeusFareCheckRuleManager fareCheckRuleManager =
						AmadeusFareCheckRuleManager.builder().supplierConfiguration(supplierConf).listener(listener)
								.criticalMessageLogger(criticalMessageLogger).moneyExchnageComm(moneyExchnageComm)
								.selectedTrip(selectedTrip).bookingUser(user).build();
				fareCheckRuleManager.setSession(sessionManager.getSessionSchema());
				fareCheckRuleManager.setSearchQuery(searchQuery);
				fareCheckRuleManager.setWebServicesStub(webServicesStub);
				fareCheckRuleManager.setServicesStub(reviewServicesStub);
				tripFareRule = fareCheckRuleManager.getFareCheckRules(selectedTrip);
			} else {
				AmadeusMiniFareRuleManager miniFareRuleManager =
						AmadeusMiniFareRuleManager.builder().criticalMessageLogger(criticalMessageLogger)
								.listener(listener).moneyExchnageComm(moneyExchnageComm)
								.supplierConfiguration(supplierConf).bookingUser(user).build();
				miniFareRuleManager.setSession(sessionManager.getSessionSchema());
				miniFareRuleManager.setServicesStub(reviewServicesStub);
				miniFareRuleManager.setSearchQuery(searchQuery);
				miniFareRuleManager.setWebServicesStub(webServicesStub);
				tripFareRule = miniFareRuleManager.getTripFareRules(selectedTrip);
			}
		} finally {
			sessionManager.closeSession();
			storeStubs();
		}
		return tripFareRule;
	}


	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		AmadeusSSRManager ssrManager = null;
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		SupplierSession session = null;
		try {
			initialize(false, bookingId);
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(tripInfo.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			ssrManager = AmadeusSSRManager.builder().listener(listener).supplierConfiguration(supplierConf)
					.searchQuery(searchQuery).moneyExchnageComm(moneyExchnageComm).bookingId(bookingId)
					.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			if (session != null) {
				reviewServicesStub = bindingService.getReviewWebServiceStub(searchQuery);
				ssrManager.init(session.getSupplierSessionInfo().getSessionToken());
				ssrManager.setSsrStub(reviewServicesStub);
				for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
					List<SeatSSRInformation> seatInfos = ssrManager.getSeatMap(segmentInfo);
					if (CollectionUtils.isNotEmpty(seatInfos)) {
						SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatInfos).build();
						tripSeatMap.getTripSeat().put(segmentInfo.getId(), seatInformation);
					}
				}
			}
		} finally {
			storeStubs();
		}
		return tripSeatMap;
	}


	@Override
	public TripInfo getChangeClass(TripInfo tripInfo, String searchId) {
		initialize(true, searchId);
		webServicesStub = bindingService.getWebServiceStub(searchQuery);
		AmadeusSessionManager sessionManager =
				AmadeusSessionManager.builder().listener(listener).supplierConfiguration(supplierConf)
						.criticalMessageLogger(criticalMessageLogger).webServicesStub(webServicesStub).build();
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.setCriticalMessageLogger(criticalMessageLogger);
		try {
			sessionManager.openSession();
			AmadeusMultiAvailabilityManager searchManager = AmadeusMultiAvailabilityManager.builder().listener(listener)
					.supplierConfiguration(supplierConf).moneyExchnageComm(moneyExchnageComm)
					.criticalMessageLogger(criticalMessageLogger).searchQuery(searchQuery).build();
			searchManager.setSession(sessionManager.getSessionSchema());
			searchManager.setWebServicesStub(webServicesStub);
			searchManager.setCriticalMessageLogger(criticalMessageLogger);
			searchManager.searchAlternateClass(tripInfo);
		} catch (SupplierSessionException se) {
			if (sessionManager.hasSessionExpired(se) || sessionManager.isTooManyConnections(se)) {
				sessionManager.closeSession();
			}
		} finally {
			sessionManager.closeSession();
			storeStubs();
		}
		return tripInfo;
	}

	@Override
	public void closeSession(List<SupplierSession> sessionList, String bookingId) {
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		AmadeusSessionManager sessionManager = AmadeusSessionManager.builder().bookingId(bookingId).listener(listener)
				.supplierConfiguration(supplierConf).criticalMessageLogger(criticalMessageLogger).build();
		try {
			webServicesStub = bindingService.getWebServiceStub(null);
			for (SupplierSession session : sessionList) {
				sessionManager.init(session.getSupplierSessionInfo().getSessionToken());
				sessionManager.closeSession();
			}
		} finally {
			storeStubs();
		}
	}

	@Override
	public TripInfo getChangeClassFare(TripInfo tripInfo, String searchId) {
		AmadeusSessionManager sessionManager = AmadeusSessionManager.builder().bookingId(searchId).listener(listener)
				.supplierConfiguration(supplierConf).criticalMessageLogger(criticalMessageLogger).build();
		initialize(false, searchId);
		webServicesStub = bindingService.getWebServiceStub(searchQuery);
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.setListener(listener);
		sessionManager.setCriticalMessageLogger(criticalMessageLogger);
		AmadeusReviewManager reviewManager = null;
		try {
			sessionManager.openSession();
			reviewServicesStub = bindingService.getReviewWebServiceStub(searchQuery);
			reviewManager = AmadeusReviewManager.builder().listener(listener).supplierConfiguration(supplierConf)
					.searchQuery(searchQuery).listener(listener).criticalMessageLogger(criticalMessageLogger)
					.moneyExchnageComm(moneyExchnageComm).bookingId(searchId).build();
			reviewManager.setSession(sessionManager.getSessionSchema());
			reviewManager.setServicesStub(reviewServicesStub);
			reviewManager.init(sessionManager.getSession());
			tripInfo = reviewManager.doFareInformativePricing(tripInfo);
		} finally {
			sessionManager.closeSession();
			storeStubs();
		}
		return tripInfo;
	}

	public void initNoOfRecommendations() {
		Map<CabinClass, Integer> fareFamily = new HashMap<>();
		Integer noOfRecommndation = AmadeusUtils.getItinCount(sourceConfiguration, searchQuery).intValue();
		/*
		 * if (searchQuery.getCabinClass() != null) { fareFamily.put(searchQuery.getCabinClass(), noOfRecommndation); }
		 * else { fareFamily.put(CabinClass.ECONOMY, noOfRecommndation); }
		 */
		searchManager.setNO_OF_REC(new BigInteger(String.valueOf(noOfRecommndation)));
		searchManager.setFareFamily(fareFamily);
		if (searchQuery.isIntl() && !searchQuery.getCabinClass().equals(CabinClass.ECONOMY)) {
			searchManager.isCabinRequest = true;
		}
	}

	private AmadeusMasterPriceSearchManager buildSearchManager(AmadeusMasterPriceSearchManager searchManager,
			AirSearchQuery newSQuery) {
		AmadeusMasterPriceSearchManager masterPriceSearchManager = AmadeusMasterPriceSearchManager.builder()
				.listener(listener).supplierConfiguration(supplierConf).searchQuery(newSQuery).build();
		masterPriceSearchManager.setNO_OF_REC(searchManager.NO_OF_REC);
		masterPriceSearchManager.setFareFamily(searchManager.fareFamily);
		masterPriceSearchManager.init(null);
		AmadeusWebServicesStub webServicesStub = bindingService.getWebServiceStub(newSQuery);
		masterPriceSearchManager.setWebServicesStub(webServicesStub);
		masterPriceSearchManager.setCriticalMessageLogger(searchManager.getCriticalMessageLogger());
		AmadeusSessionManager sessionManager =
				AmadeusSessionManager.builder().listener(listener).supplierConfiguration(supplierConf).build();
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.setCriticalMessageLogger(searchManager.getCriticalMessageLogger());
		masterPriceSearchManager.setSessionManager(sessionManager);
		masterPriceSearchManager.setBindingService(bindingService);
		masterPriceSearchManager.setCabinRequest(searchManager.isCabinRequest);
		masterPriceSearchManager.setMoneyExchnageComm(moneyExchnageComm);
		masterPriceSearchManager.setBookingUser(user);
		return masterPriceSearchManager;
	}

	private void storeStubs() {
		bindingService = AmadeusBindingService.builder().cacheCommunicator(cachingComm).configuration(supplierConf)
				.user(user).build();
		bindingService.cleanUp(reviewServicesStub);
		bindingService.cleanUp(webServicesStub);
		bindingService.storeSessionMetaInfo(supplierConf, AmadeusReviewWebServicesStub.class, reviewServicesStub);
		bindingService.storeSessionMetaInfo(supplierConf, AmadeusWebServicesStub.class, webServicesStub);
	}

	@Override
	public Boolean initializeStubs() {
		// not storing due to stub security header issue
		// bindingService =
		// AmadeusBindingService.builder().cacheCommunicator(cachingComm).configuration(supplierConf).build();
		// webServicesStub = bindingService.getWebServiceStub(searchQuery);
		// storeStubs();
		return true;
	}
}
