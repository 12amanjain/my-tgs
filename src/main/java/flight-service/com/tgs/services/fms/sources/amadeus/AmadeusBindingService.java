package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.AmadeusBookingWebServicesStub;
import com.amadeus.xml.AmadeusCancelWebServicesStub;
import com.amadeus.xml.AmadeusReviewWebServicesStub;
import com.amadeus.xml.AmadeusWebServicesStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.CacheType;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.lang3.StringUtils;
import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Slf4j
@Builder
public class AmadeusBindingService {

	private GeneralCachingCommunicator cacheCommunicator;

	private SupplierConfiguration configuration;

	private SoapRequestResponseListner listner;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	protected User user;

	public AmadeusWebServicesStub getWebServiceStub(AirSearchQuery query) {
		AmadeusWebServicesStub sessionStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, AmadeusWebServicesStub.class, null));
			if (metaInfo != null) {
				sessionStub = (AmadeusWebServicesStub) metaInfo.getValue();
			}
			if (sessionStub == null) {
				log.info("Creating new WebServices Stub for searchQuery {}", query);
				sessionStub = new AmadeusWebServicesStub(configuration.getSupplierCredential().getUrl());
			}
		} catch (Exception e) {
			log.error("WebServices Binding Initilaized failed for cause", e);
		}
		setOptions(sessionStub);
		return sessionStub;
	}

	private void setOptions(Stub stub) {
		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			ServiceClient client = stub._getServiceClient();
			Options options = client.getOptions();
			if (options != null) {
				options.setProperty(org.apache.axis2.transport.http.HTTPConstants.HTTP_PROTOCOL_VERSION,
						org.apache.axis2.transport.http.HTTPConstants.HEADER_PROTOCOL_10);
				options.setProperty(org.apache.axis2.transport.http.HTTPConstants.SO_TIMEOUT,
						AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
				options.setProperty(org.apache.axis2.transport.http.HTTPConstants.CONNECTION_TIMEOUT,
						AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
				options.setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
				setConnectionManager(stub);
				client.setOptions(options);
			}
		}

	}

	public AmadeusReviewWebServicesStub getReviewWebServiceStub(AirSearchQuery query) {
		AmadeusReviewWebServicesStub bookingStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, AmadeusReviewWebServicesStub.class, null));
			if (metaInfo != null) {
				bookingStub = (AmadeusReviewWebServicesStub) metaInfo.getValue();
			}
			if (bookingStub == null) {
				log.info("Creating new WebServices Stub for searchQuery {}", query);
				bookingStub = new AmadeusReviewWebServicesStub(
						String.join("", configuration.getSupplierCredential().getUrl()));
			}
		} catch (Exception e) {
			log.error("Review WebServices Binding Initilaized failed for cause", e);
		}
		setOptions(bookingStub);
		return bookingStub;
	}


	public AmadeusBookingWebServicesStub getBookingWebServiceStub(AirSearchQuery query) {
		AmadeusBookingWebServicesStub bookingStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, AmadeusBookingWebServicesStub.class, null));
			if (metaInfo != null) {
				bookingStub = (AmadeusBookingWebServicesStub) metaInfo.getValue();
			}
			if (bookingStub == null) {
				log.info("Creating new WebServices Stub for searchQuery {}", query);
				bookingStub = new AmadeusBookingWebServicesStub(
						String.join("", configuration.getSupplierCredential().getUrl()));
			}
		} catch (Exception e) {
			log.error("Booking WebServices Binding Initilaized failed for cause", e);
		}
		setOptions(bookingStub);
		return bookingStub;
	}

	public AmadeusCancelWebServicesStub getCancelWebServiceStub(AirSearchQuery query) {
		AmadeusCancelWebServicesStub cancelStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, AmadeusCancelWebServicesStub.class, null));
			if (metaInfo != null) {
				cancelStub = (AmadeusCancelWebServicesStub) metaInfo.getValue();
			}
			if (cancelStub == null) {
				log.info("Creating new WebServices Stub for searchQuery {}", query);
				cancelStub = new AmadeusCancelWebServicesStub(
						String.join("", configuration.getSupplierCredential().getUrl()));
			}
		} catch (Exception e) {
			log.error("Booking WebServices Binding Initilaized failed for cause", e);
		}
		setOptions(cancelStub);
		return cancelStub;
	}

	@SuppressWarnings("rawtypes")
	public static SessionMetaInfo getSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder()
				.key(String.join("-", configuration.getBasicInfo().getRuleId().toString(), classx.toString()))
				.value(value).expiryTime(LocalDateTime.now().plusMinutes(600)).cacheType(CacheType.INMEMORY).build();
		return metaInfo;
	}

	@SuppressWarnings("rawtypes")
	public void storeSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		if (value != null) {
			SessionMetaInfo metaInfo = getSessionMetaInfo(configuration, classx, value);
			cacheCommunicator.storeInQueue(metaInfo);
		}
	}

	public SessionMetaInfo fetchFromQueue(SessionMetaInfo metaInfo) {
		metaInfo.setIndex(0);
		SessionMetaInfo cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
		if (cacheInfo == null) {
			metaInfo.setIndex(-1);
			cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
		}
		return cacheInfo;
	}

	public String fetchSessionToken(boolean isSearch) {
		if (isSearch) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder()
					.key(configuration.getBasicInfo().getRuleId().toString()).index(0).compress(false).build();
			metaInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
				metaInfo = SessionMetaInfo.builder().key(configuration.getBasicInfo().getRuleId().toString()).index(-1)
						.compress(false).build();
				metaInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			}

			if (metaInfo != null && metaInfo.getValue() != null) {
				return (String) metaInfo.getValue();
			}
		}
		return null;
	}

	public void storeSession(String sessionToken, AirSourceConfigurationOutput sourceConfiguration,
			AmadeusWebServicesStub webServicesStub) {
		if (StringUtils.isNotBlank(sessionToken)) {
			try {
				SessionMetaInfo metaInfo = SessionMetaInfo.builder()
						.key(configuration.getBasicInfo().getRuleId().toString()).compress(false).build();
				metaInfo.setValue(sessionToken);
				metaInfo.setExpiryTime(LocalDateTime.now()
						.plusMinutes(AirUtils.getSessionTime(configuration.getBasicInfo(), sourceConfiguration, user)));
				cacheCommunicator.storeInQueue(metaInfo);
			} catch (Exception e) {
				log.error("Unable to store session in queue ", e);
				AmadeusSessionManager sessionManager = AmadeusSessionManager.builder()
						.supplierConfiguration(configuration).listener(listner).bookingUser(user).build();
				sessionManager.init(sessionToken);
				sessionManager.setWebServicesStub(webServicesStub);
				sessionManager.closeSession();
			}
		}
	}

	/**
	 * @implNote : <br>
	 *           1. Clean Last Context Operation from transport clean <br>
	 *           2. stub clean up will clean axis service & axis config
	 */
	public void cleanUp(Stub stub) {
		if (stub != null) {
			try {
				// stub._getServiceClient().cleanupTransport();
				// stub._getServiceClient().cleanup();
				// stub.cleanup();
			} catch (Exception af) {
				log.error("Unable to clean up config ", af);
			}
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(30);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}
}
