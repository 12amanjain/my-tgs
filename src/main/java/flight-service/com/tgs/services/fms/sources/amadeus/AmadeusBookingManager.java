package com.tgs.services.fms.sources.amadeus;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.TimeZone;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.amadeus.xml.AmadeusBookingWebServicesStub;
import com.amadeus.xml.pnracc_15_1_1a.DataElementsIndiv_type1;
import com.amadeus.xml.pnracc_15_1_1a.DataElementsMaster_type0;
import com.amadeus.xml.pnracc_15_1_1a.FreeText_type3;
import com.amadeus.xml.pnracc_15_1_1a.GeneralErrorInfo_type0;
import com.amadeus.xml.pnracc_15_1_1a.ItineraryInfo_type1;
import com.amadeus.xml.pnracc_15_1_1a.OptionElement_type0;
import com.amadeus.xml.pnracc_15_1_1a.OriginDestinationDetails_type0;
import com.amadeus.xml.pnracc_15_1_1a.PNR_Reply;
import com.amadeus.xml.pnracc_15_1_1a.PnrHeader_type0;
import com.amadeus.xml.pnracc_15_1_1a.RelatedProduct_type0;
import com.amadeus.xml.pnracc_15_1_1a.ReservationInfo_type0;
import com.amadeus.xml.pnracc_15_1_1a.Reservation_type0;
import com.amadeus.xml.pnracc_15_1_1a.Ssr_type0;
import com.amadeus.xml.pnracc_15_1_1a.TravelProduct_type0;
import com.amadeus.xml.pnradd_15_1_1a.AirAuxItinerary_type0;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To1;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To10;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To14;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To19;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To199;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To2;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To25;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To3;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To35;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To4;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To5;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To56;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To57;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To70;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length1To8;
import com.amadeus.xml.pnradd_15_1_1a.AlphaNumericString_Length2To3;
import com.amadeus.xml.pnradd_15_1_1a.AlphaString_Length1To2;
import com.amadeus.xml.pnradd_15_1_1a.AlphaString_Length2To2;
import com.amadeus.xml.pnradd_15_1_1a.AlphaString_Length3To3;
import com.amadeus.xml.pnradd_15_1_1a.CommissionElementType;
import com.amadeus.xml.pnradd_15_1_1a.CommissionInformationType;
import com.amadeus.xml.pnradd_15_1_1a.DataElementMaster_type0;
import com.amadeus.xml.pnradd_15_1_1a.DataElementsIndiv_type0;
import com.amadeus.xml.pnradd_15_1_1a.DateAndTimeDetailsTypeI_56946C;
import com.amadeus.xml.pnradd_15_1_1a.DateAndTimeInformationType;
import com.amadeus.xml.pnradd_15_1_1a.Date_DDMMYY;
import com.amadeus.xml.pnradd_15_1_1a.Date_MMYY;
import com.amadeus.xml.pnradd_15_1_1a.DummySegmentTypeI;
import com.amadeus.xml.pnradd_15_1_1a.ElementManagementSegmentType;
import com.amadeus.xml.pnradd_15_1_1a.FormOfPaymentDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.FormOfPaymentTypeI;
import com.amadeus.xml.pnradd_15_1_1a.FreeFormatTourCodeType;
import com.amadeus.xml.pnradd_15_1_1a.FreeTextQualificationType;
import com.amadeus.xml.pnradd_15_1_1a.FrequentTravellerIdentificationTypeU;
import com.amadeus.xml.pnradd_15_1_1a.FrequentTravellerInformationTypeU;
import com.amadeus.xml.pnradd_15_1_1a.ItineraryInfo_type0;
import com.amadeus.xml.pnradd_15_1_1a.LocationTypeI;
import com.amadeus.xml.pnradd_15_1_1a.LongFreeTextType;
import com.amadeus.xml.pnradd_15_1_1a.MarketSpecificDataDetailsType;
import com.amadeus.xml.pnradd_15_1_1a.MarketSpecificDataType;
import com.amadeus.xml.pnradd_15_1_1a.MessageActionDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.MessageFunctionBusinessDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.NumericDecimal_Length1To5;
import com.amadeus.xml.pnradd_15_1_1a.NumericInteger_Length1To2;
import com.amadeus.xml.pnradd_15_1_1a.NumericInteger_Length1To3;
import com.amadeus.xml.pnradd_15_1_1a.OptionalPNRActionsType;
import com.amadeus.xml.pnradd_15_1_1a.OriginAndDestinationDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.OriginDestinationDetail_type0;
import com.amadeus.xml.pnradd_15_1_1a.PNR_AddMultiElements;
import com.amadeus.xml.pnradd_15_1_1a.PassengerData_type0;
import com.amadeus.xml.pnradd_15_1_1a.ProductDateTimeTypeI;
import com.amadeus.xml.pnradd_15_1_1a.ReferenceInfoType;
import com.amadeus.xml.pnradd_15_1_1a.ReferencingDetailsType;
import com.amadeus.xml.pnradd_15_1_1a.RelatedProductInformationTypeI;
import com.amadeus.xml.pnradd_15_1_1a.ReservationControlInformationDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.ReservationControlInformationTypeI;
import com.amadeus.xml.pnradd_15_1_1a.SpecialRequirementsDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.SpecialRequirementsTypeDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.TicketElementType;
import com.amadeus.xml.pnradd_15_1_1a.TicketInformationType;
import com.amadeus.xml.pnradd_15_1_1a.TourCodeType;
import com.amadeus.xml.pnradd_15_1_1a.TravelProductInformationType;
import com.amadeus.xml.pnradd_15_1_1a.TravellerDetailsTypeI;
import com.amadeus.xml.pnradd_15_1_1a.TravellerInformationTypeI;
import com.amadeus.xml.pnradd_15_1_1a.TravellerInfos_type0;
import com.amadeus.xml.pnradd_15_1_1a.TravellerSurnameInformationTypeI;
import com.amadeus.xml.tautcq_04_1_1a.ItemReferencesAndVersionsType;
import com.amadeus.xml.tautcq_04_1_1a.PsaList_type0;
import com.amadeus.xml.tautcq_04_1_1a.Ticket_CreateTSTFromPricing;
import com.amadeus.xml.tautcr_04_1_1a.ApplicationError_type3;
import com.amadeus.xml.tautcr_04_1_1a.ErrorFreeText_type2;
import com.amadeus.xml.tautcr_04_1_1a.Ticket_CreateTSTFromPricingReply;
import com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To60;
import com.amadeus.xml.tpcbrq_15_1_1a.AttributeInformationTypeU;
import com.amadeus.xml.tpcbrq_15_1_1a.AttributeType;
import com.amadeus.xml.tpcbrq_15_1_1a.CompanyIdentificationTypeI;
import com.amadeus.xml.tpcbrq_15_1_1a.CurrenciesType;
import com.amadeus.xml.tpcbrq_15_1_1a.CurrencyDetailsTypeU;
import com.amadeus.xml.tpcbrq_15_1_1a.Fare_PricePNRWithBookingClass;
import com.amadeus.xml.tpcbrq_15_1_1a.PricingOptionGroup_type1;
import com.amadeus.xml.tpcbrq_15_1_1a.PricingOptionKey;
import com.amadeus.xml.tpcbrq_15_1_1a.TransportIdentifierType;
import com.amadeus.xml.tpcbrr_15_1_1a.ErrorGroupType;
import com.amadeus.xml.tpcbrr_15_1_1a.FareList_type0;
import com.amadeus.xml.tpcbrr_15_1_1a.Fare_PricePNRWithBookingClassReply;
import com.amadeus.xml.tpcbrr_15_1_1a.MonetaryInformationDetailsType_223832C;
import com.amadeus.xml.tpcbrr_15_1_1a.MonetaryInformationType_187640S;
import com.amadeus.xml.tpcbrr_15_1_1a.ReferenceInformationTypeI;
import com.amadeus.xml.tpcbrr_15_1_1a.StructuredDateTimeInformationType;
import com.amadeus.xml.ttktiq_15_1_1a.DocIssuance_IssueTicket;
import com.amadeus.xml.ttktiq_15_1_1a.OptionGroup_type0;
import com.amadeus.xml.ttktir_15_1_1a.DocIssuance_IssueTicketReply;
import com.amadeus.xml.ttktir_15_1_1a.ErrorWarningDescription_type0;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineTimeLimitData;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SupplierFlowType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.tgs.utils.string.TgsStringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
final class AmadeusBookingManager extends AmadeusServiceManager {

	protected AmadeusBookingWebServicesStub bookingStub;

	protected AmadeusRetrieveBookingManager retrieveManager;

	protected GeneralServiceCommunicator gmsComm;

	protected List<SegmentInfo> segmentInfos;

	protected String airlineCode;

	protected boolean isHoldBooking;

	protected String supplierPNR;

	protected Double supplierAmount;
	protected String crsPNR;
	protected Calendar fareTimeLimit;
	protected LocalDateTime ticketingTimeLimit;

	protected DataElementMaster_type0 dataElement;
	protected List<DataElementsIndiv_type0> dataElementDiv;
	protected Integer divCount;

	protected List<Integer> tstList;

	protected static final int MAX_NAME_LENGTH = 59;
	protected static final String PNR_ADD_DETAILS_CODE = "0";
	protected static final String COMMIT_PNR_ACTION_CODE = "10";
	protected static final String PASSENGER_REFERENCE = "PR";
	protected static final String PASSENGER_NAME_ELEMENT = "NM";
	protected static final String DOB_QUALIFIER = "706";
	protected static final String OTHER_ELEMENT_TATOO_REF = "OT";
	protected static final String TICKETING_ELEMENT = "TK";
	protected static final String TICKET_INDICATOR = "OK";
	protected static final String COMMISSION_ELEMENT = "FM";
	protected static final String ADD_CONTACT_TO_PNR = "AP";
	protected static final String EMAIL_ADDRESS_TICKET_TYPE = "P02";
	protected static final String BOOKING_ADDRESS_TICKET_TYPE = "P22";
	protected static final String RETENION_TYPE = "RU";
	protected static final String MOBILE_PHONE_CONTACT = "7";
	protected static final String SPECIAL_SERVICE_REQUEST = "SSR";
	protected static final String TOUR_CODE_ELEMENT = "FT";
	protected static final String PASSENGER_TYPE = "PAX";
	protected static final String FREE_FORM_TEXT = "FF";

	protected static final String FOP_TRANSACTION_REF = "FP";
	protected static final String CREDIT_CARD = "CC";
	protected static final String CASH_IDENTIFICATION = "CA";

	protected static final String TRAVEL_DOCUMENTS_INFORMATION = "DOCS";
	protected static final String FOID_SSR = "FOID";
	protected static final String PASSPORT_ACTON_CODE = "HK";
	protected static final String FARE_BASIS_OVERIDE = "FBA";
	protected static final String SEGMENT_REF_QAULIFIER = "S";
	protected static final String PASSENGER_REF_QUALIFIER = "P";
	protected static final String NON_INFANT_PASSENGER_REF_QUALIFIER = "PA";
	protected static final String INFANT_PASSENGER_REF_QUALIFIER = "PI";
	protected static final String TST_QUALIFIER = "TST";
	protected static final String TOTAL_FARE_AMOUNT = "712";
	protected static final String END_TRANSACT_SEGMENT_NAME = "RF";
	protected static final String END_TRANSACT_SUBJECT_QUALIFIER = "3";
	protected static final String END_TRANSACT_TYPE = "P22";
	protected static final String END_TRANSACT_TEXT_STRING = "RF ADDED VIA PNRADD";
	protected static final String SUPPLIER_COMPANY_REFERENCE = "1A";

	protected static final String DOB_FORMAT = "ddMMyyyy";
	protected static final String FARE_TIME_LIMIT_FORMAT = "ddMMyyyy HHmm";
	protected static final Integer SLEEP_TIME_IN_SECS = 10;
	protected static final Integer TICKET_SLEEP_TIME_IN_SECS = 3;
	protected static final Integer SIMULTANEOUS_SLEEP_TIME_IN_SECS = 12;
	protected static final String PNR_ACTIVE = "HK";
	protected static final String SSR_STATUS = "HK";
	protected static final String ETICKET_ITINERARY_RECIPT = "ITR";
	protected static final String RETENSION_IDETIFICATION = "1A";
	protected static final String RETENSION_BUSINESS_CODE = "32";
	protected static final String RETENSION_NAME = "RU";
	protected static final String TICKET_TIME_LIMIT_FORMAT = "ddMMyyHHmm";
	protected static final String OK_ETICKET = "OK ETICKET";

	protected Boolean isNameTooLong;

	protected void init(AirSearchQuery searchQuery, boolean isHold, List<SegmentInfo> segmentInfos,
			List<SegmentInfo> segmentInfosCopy, Order order) {
		init(null);
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			travellerInfos = segmentInfosCopy.get(0).getBookingRelatedInfo().getTravellerInfo();
			adults = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT);
			childs = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD);
			infants = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT);
			// overide pax count from original travellers list
			adultCount = CollectionUtils.size(adults);
			childCount = CollectionUtils.size(childs);
			infantCount = CollectionUtils.size(infants);
			extractTravellers();
		}
		isHoldBooking = isHold;
		this.searchQuery = searchQuery;
		this.segmentInfos = segmentInfos;
		airlineCode = segmentInfos.get(0).getAirlineCode(false);
		dataElement = new DataElementMaster_type0();
		segmentIndexToRefMap = new HashMap<>();
		passengerRefToIndexMap = new HashMap<>();
		infantPassengerRefToIndexMap = new HashMap<>();
		passengerIndexToRefMap = new HashMap<>();
		supplierAmount = new Double(0);
		airlinePnrs = new ArrayList<>();
	}

	public void sendPNRAddMultiElements() {
		listener.setType("PNRAddMultiElements-".concat(SupplierFlowType.BOOK.name()));
		PNR_Reply pnrReply = null;
		PNR_AddMultiElements pnrRequest = null;
		try {
			pnrRequest = buildPNRAddMultiElementsRequest();
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			pnrReply = bookingStub.pNR_AddMultiElements(pnrRequest, getSessionSchema());
			checkForTravellerError(pnrReply);
			if (!checkPNRReplyErrorMessage(pnrReply) && !BooleanUtils.isTrue(isNameTooLong)) {
				parseAddPnrMultiElementsRes(pnrReply);
				sendPassportInformation();
			} else if (BooleanUtils.isTrue(isNameTooLong)) {
				throw new SupplierUnHandledFaultException("Traveller NameLength More Than 56 Characters!");
			} else {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (AxisFault af) {
			log.info("Error Occured on PNR Add Multielements bookingId {} ", bookingId, af);
			throw new SupplierUnHandledFaultException(af.getMessage());
		} catch (RemoteException re) {
			log.info("Error Occured on PNR Add Multielements bookingId {} ", bookingId, re);
			throw new SupplierRemoteException(re.getMessage());
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	public void commitPNR() {
		listener.setType("Commit-PNRAddMultiElements-".concat(SupplierFlowType.BOOK.name()));
		PNR_Reply pnrReply = null;
		PNR_AddMultiElements pnrRequest = null;
		try {
			pnrRequest = buildCommitPNRRequest();
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			pnrReply = bookingStub.pNR_AddMultiElements(pnrRequest, getSessionSchema());
			if (isSimultaneousChanges(pnrReply)) {
				Thread.sleep(1000 * SIMULTANEOUS_SLEEP_TIME_IN_SECS);
			} else if (!checkPNRReplyErrorMessage(pnrReply)) {
				crsPNR = parsePnrReply(pnrReply);
			} else {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (InterruptedException e) {
			log.error(AirSourceConstants.AIR_SUPPLIER_PNR_THREAD, bookingId);
			throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
		} catch (AxisFault af) {
			throw new SupplierUnHandledFaultException(af.getMessage());
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re.getMessage());
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private String parsePnrReply(PNR_Reply pnrReply) {
		String supplierPnr = null;
		if (pnrReply != null && pnrReply.getPnrHeader() != null && ArrayUtils.isNotEmpty(pnrReply.getPnrHeader())) {
			PnrHeader_type0[] pnrHeaders = pnrReply.getPnrHeader();
			for (PnrHeader_type0 pnrHeader : pnrHeaders) {
				ReservationInfo_type0 reservationInfo = pnrHeader.getReservationInfo();
				if (reservationInfo != null && ArrayUtils.isNotEmpty(reservationInfo.getReservation())) {
					for (Reservation_type0 reservation : reservationInfo.getReservation()) {
						if (reservation.getCompanyId().getCompanyId_type0()
								.equalsIgnoreCase(SUPPLIER_COMPANY_REFERENCE)) {
							supplierPnr = reservation.getControlNumber().getControlNumber_type0();
							break;
						}
					}
				}
			}

		}
		return supplierPnr;
	}

	private PNR_AddMultiElements buildCommitPNRRequest() {
		PNR_AddMultiElements pnrRequest = new PNR_AddMultiElements();
		DataElementsIndiv_type0[] elementsInDiv = new DataElementsIndiv_type0[1];
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType = buildElementManagement(null, null, END_TRANSACT_SEGMENT_NAME);
		buildMarker(pnrRequest);
		buildPNRAction(pnrRequest, COMMIT_PNR_ACTION_CODE);
		elementDiv.setElementManagementData(segmentType);
		LongFreeTextType longFreeTextType = new LongFreeTextType();
		FreeTextQualificationType freeText = new FreeTextQualificationType();
		freeText.setSubjectQualifier(getAlphaNumeric1To3(END_TRANSACT_SUBJECT_QUALIFIER));
		freeText.setType(getAlphaString1To4(END_TRANSACT_TYPE));
		longFreeTextType.setLongFreetext(getAlpha1To199(END_TRANSACT_TEXT_STRING));
		longFreeTextType.setFreetextDetail(freeText);
		elementDiv.setFreetextData(longFreeTextType);
		elementsInDiv[0] = elementDiv;
		dataElement.setDataElementsIndiv(elementsInDiv);
		pnrRequest.setDataElementMaster(dataElement);
		return pnrRequest;
	}

	private void parseAddPnrMultiElementsRes(PNR_Reply pnrReply) {
		parseTravellerReferences(pnrReply.getTravellerInfo());
		parseSegmentReferences(pnrReply.getOriginDestinationDetails());
	}

	private void parseSegmentReferences(OriginDestinationDetails_type0[] originDestinationDetails) {
		if (ArrayUtils.isNotEmpty(originDestinationDetails)) {
			for (OriginDestinationDetails_type0 originDestinationDetail : originDestinationDetails) {
				ItineraryInfo_type1[] itineraryInfos = originDestinationDetail.getItineraryInfo();
				if (ArrayUtils.isNotEmpty(itineraryInfos)) {
					for (ItineraryInfo_type1 itineraryInfo : itineraryInfos) {
						TravelProduct_type0 travelProduct = itineraryInfo.getTravelProduct();
						Integer segmentIndex = getSegmentIndex(travelProduct);
						if (segmentIndex != null && segmentIndex >= 0) {
							String refNumber = itineraryInfo.getElementManagementItinerary().getReference().getNumber()
									.getNumber_type8();
							segmentIndexToRefMap.put(segmentIndex, Integer.valueOf(refNumber));
							segmentInfos.get(segmentIndex).getPriceInfo(0).getMiscInfo().setSegmentKey(refNumber);
						}
					}
				}
			}
		}
	}

	private Integer getSegmentIndex(TravelProduct_type0 travelProduct) {
		Integer index = 0;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (isTravelSegmentMatches(segmentInfo, travelProduct)) {
				return index;
			}
			index++;
		}
		return null;
	}

	private boolean isTravelSegmentMatches(SegmentInfo segmentInfo, TravelProduct_type0 travelProduct) {
		if (travelProduct != null && travelProduct.getBoardpointDetail() != null
				&& travelProduct.getOffpointDetail() != null && travelProduct.getProductDetails() != null
				&& travelProduct.getCompanyDetail() != null
				&& segmentInfo.getDepartureAirportCode()
						.equalsIgnoreCase(travelProduct.getBoardpointDetail().getCityCode().getCityCode_type2())
				&& segmentInfo.getArrivalAirportCode()
						.equalsIgnoreCase(travelProduct.getOffpointDetail().getCityCode().getCityCode_type4())
				&& segmentInfo.getFlightNumber().equalsIgnoreCase(
						travelProduct.getProductDetails().getIdentification().getIdentification_type2())
				&& segmentInfo.getAirlineCode(false).equalsIgnoreCase(
						travelProduct.getCompanyDetail().getIdentification().getIdentification_type0())) {
			return true;
		}
		return false;
	}


	private void sendPassportInformation() {
		if (isPassportExists()) {
			PNR_Reply pnrReply = null;
			PNR_AddMultiElements pnrRequest = null;
			try {
				listener.setType("PNRAddMultiElements-PPT-".concat(SupplierFlowType.BOOK.name()));
				pnrRequest = buildPassPortInfo();
				bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				pnrReply = bookingStub.pNR_AddMultiElements(pnrRequest, getSessionSchema());
				if (checkPNRReplyErrorMessage(pnrReply)) {
					throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
				}
			} catch (AxisFault af) {
				throw new SupplierUnHandledFaultException(af.getMessage());
			} catch (RemoteException re) {
				throw new SupplierRemoteException(re.getMessage());
			} finally {
				bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			}
		}
	}

	private boolean isPassportExists() {
		boolean isExists = false;
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			if (StringUtils.isNotBlank(travellerInfo.getPassportNumber()) && travellerInfo.getExpiryDate() != null) {
				isExists = true;
				break;
			}
		}
		return isExists;
	}

	private PNR_AddMultiElements buildPassPortInfo() {
		PNR_AddMultiElements pnrAddMultiElements = new PNR_AddMultiElements();
		dataElementDiv = new ArrayList<>();
		buildMarker(pnrAddMultiElements);
		buildPNRAction(pnrAddMultiElements, PNR_ADD_DETAILS_CODE);
		buildPassPortData();
		buildFOIDSsr();
		addToDataElement(pnrAddMultiElements);
		return pnrAddMultiElements;
	}

	private void buildFOIDSsr() {
		Set<String> uniqueCarriers = getUniqueAirlines();
		for (String airlineCode : uniqueCarriers) {
			if (sourceConfig != null && CollectionUtils.isNotEmpty(sourceConfig.getFoidAirlines())
					&& sourceConfig.getFoidAirlines().contains(airlineCode)) {
				Integer travellerIndex = 0;
				for (FlightTravellerInfo travellerInfo : travellerInfos) {
					if (StringUtils.isNotBlank(travellerInfo.getPassportNumber())) {
						DataElementsIndiv_type0 elementsIndiv = new DataElementsIndiv_type0();
						ElementManagementSegmentType elementManagement = buildElementManagement(divCount.toString(),
								OTHER_ELEMENT_TATOO_REF, SPECIAL_SERVICE_REQUEST);
						SpecialRequirementsDetailsTypeI specialRequirements = new SpecialRequirementsDetailsTypeI();
						SpecialRequirementsTypeDetailsTypeI foidSSR = new SpecialRequirementsTypeDetailsTypeI();
						foidSSR.setCompanyId(getAlphaNumeric1To3(airlineCode));
						foidSSR.setType(getAlphaString1To4(FOID_SSR));
						String passport = StringUtils.join("PP", travellerInfo.getPassportNumber());
						AlphaNumericString_Length1To70[] length1To70s = new AlphaNumericString_Length1To70[1];
						length1To70s[0] = new AlphaNumericString_Length1To70();
						length1To70s[0] = getAlpha1To70(passport);
						foidSSR.setFreetext(length1To70s);
						specialRequirements.setSsr(foidSSR);
						Integer paxReference = passengerIndexToRefMap.get(travellerIndex);
						ReferenceInfoType referenceInfoType =
								buildReference(paxReference.toString(), PASSPORT_PASSENGER_REFRENCE);
						elementsIndiv.setReferenceForDataElement(referenceInfoType);
						elementsIndiv.setServiceRequest(specialRequirements);
						elementsIndiv.setElementManagementData(elementManagement);
						dataElementDiv.add(elementsIndiv);
						divCount++;
					}
					travellerIndex++;
				}
			}
		}
	}

	private void buildPassPortData() {
		Set<String> uniqueCarriers = getUniqueAirlines();
		for (String airlineCode : uniqueCarriers) {
			Integer travellerIndex = 0;
			for (FlightTravellerInfo travellerInfo : travellerInfos) {
				if (StringUtils.isNotBlank(travellerInfo.getPassportNumber())) {
					DataElementsIndiv_type0 elementsIndiv = new DataElementsIndiv_type0();
					ElementManagementSegmentType elementManagement = buildElementManagement(divCount.toString(),
							OTHER_ELEMENT_TATOO_REF, SPECIAL_SERVICE_REQUEST);
					SpecialRequirementsDetailsTypeI specialRequirements = new SpecialRequirementsDetailsTypeI();
					SpecialRequirementsTypeDetailsTypeI passportSSR = new SpecialRequirementsTypeDetailsTypeI();
					passportSSR.setCompanyId(getAlphaNumeric1To3(airlineCode));
					passportSSR.setType(getAlphaString1To4(TRAVEL_DOCUMENTS_INFORMATION));
					passportSSR.setStatus(getAlphaNumeric1To3(PASSPORT_ACTON_CODE));
					passportSSR.setQuantity(getNumeric1To3(BigInteger.ONE.toString()));
					String passport = AmadeusUtils.getPassportInfo(travellerInfo, gmsComm);
					AlphaNumericString_Length1To70[] length1To70s = new AlphaNumericString_Length1To70[1];
					length1To70s[0] = new AlphaNumericString_Length1To70();
					length1To70s[0] = getAlpha1To70(passport);
					passportSSR.setFreetext(length1To70s);
					specialRequirements.setSsr(passportSSR);
					Integer paxReference = passengerIndexToRefMap.get(travellerIndex);
					ReferenceInfoType referenceInfoType =
							buildReference(paxReference.toString(), PASSPORT_PASSENGER_REFRENCE);
					elementsIndiv.setReferenceForDataElement(referenceInfoType);
					elementsIndiv.setServiceRequest(specialRequirements);
					elementsIndiv.setElementManagementData(elementManagement);
					dataElementDiv.add(elementsIndiv);
					divCount++;
				}
				travellerIndex++;
			}
		}
	}

	private Set<String> getUniqueAirlines() {
		Set<String> airlines = new HashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			airlines.add(segmentInfo.getAirlineCode(false));
		});
		return airlines;
	}

	private PNR_AddMultiElements buildPNRAddMultiElementsRequest() {
		PNR_AddMultiElements pnrAddMultiElements = new PNR_AddMultiElements();
		dataElementDiv = new ArrayList<>();
		divCount = 1;
		buildMarker(pnrAddMultiElements);
		buildPNRAction(pnrAddMultiElements, PNR_ADD_DETAILS_CODE);
		buildTravellerInfo(pnrAddMultiElements);
		buildTicketingElement();
		buildCommission();
		buildContactEmail();
		buildContactPhone();
		buildTourCode();
		addToDataElement(pnrAddMultiElements);
		return pnrAddMultiElements;
	}

	private void addToDataElement(PNR_AddMultiElements pnrAddMultiElements) {
		if (dataElement == null) {
			dataElement = new DataElementMaster_type0();
		}
		Integer dataSize = dataElementDiv.size();
		if (dataSize > 0) {
			DataElementsIndiv_type0[] elementsIndivs = new DataElementsIndiv_type0[dataSize];
			int index = 0;
			for (DataElementsIndiv_type0 elementsIndiv : dataElementDiv) {
				elementsIndivs[index++] = elementsIndiv;
			}
			dataElement.setDataElementsIndiv(elementsIndivs);
		}
		pnrAddMultiElements.setDataElementMaster(dataElement);
	}

	private void buildTourCode() {
		String tourCode = getTourCode(segmentInfos);
		if (StringUtils.isNotBlank(tourCode)) {
			DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
			ElementManagementSegmentType elementData =
					buildElementManagement(divCount.toString(), OTHER_ELEMENT_TATOO_REF, TOUR_CODE_ELEMENT);
			elementDiv.setElementManagementData(elementData);
			divCount++;
			TourCodeType tourCodeType = new TourCodeType();
			tourCodeType.setPassengerType(getAlphaNumeric1To3(PASSENGER_TYPE));
			FreeFormatTourCodeType freeFormat = new FreeFormatTourCodeType();
			freeFormat.setIndicator(getAlpha1To2(FREE_FORM_TEXT));
			freeFormat.setFreetext(getAlpha1To14(tourCode));
			tourCodeType.setFreeFormatTour(freeFormat);
			elementDiv.setTourCode(tourCodeType);
			dataElementDiv.add(elementDiv);
		}
	}

	private void buildFrequentFlier() {
		Set<String> fflier = new HashSet<>();
		Integer travellerIndex = 0;
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			if (travellerInfo.getFrequentFlierMap() != null) {
				String airlineCode = segmentInfos.get(0).getPlatingCarrier(null);
				String freqFlier = travellerInfo.getFrequentFlierMap().get(airlineCode);
				if (StringUtils.isNotBlank(freqFlier) && !fflier.contains(freqFlier) && freqFlier.length() >= 2) {
					if (!freqFlier.startsWith(airlineCode)) {
						// Format to amadeus system airline code follwed with number
						freqFlier = StringUtils.join(airlineCode, freqFlier);
					}
					DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
					ElementManagementSegmentType segmentType = buildElementManagement(divCount.toString(),
							OTHER_ELEMENT_TATOO_REF, SPECIAL_SERVICE_REQUEST);
					elementDiv.setElementManagementData(segmentType);
					SpecialRequirementsDetailsTypeI detailsTypeI = new SpecialRequirementsDetailsTypeI();
					SpecialRequirementsTypeDetailsTypeI detailsTypeI1 = new SpecialRequirementsTypeDetailsTypeI();
					detailsTypeI1.setType(getAlphaString1To4(AmadeusConstants.FREQUENT_FLIER_INFORMATION_ELEMENT));
					detailsTypeI1.setCompanyId(getAlphaNumeric1To3(airlineCode));
					detailsTypeI.setSsr(detailsTypeI1);
					elementDiv.setServiceRequest(detailsTypeI);
					Integer paxReference = passengerIndexToRefMap.get(travellerIndex);
					ReferenceInfoType referenceInfoType =
							buildReference(paxReference.toString(), PASSPORT_PASSENGER_REFRENCE);
					elementDiv.setReferenceForDataElement(referenceInfoType);
					FrequentTravellerInformationTypeU travellerInformationType =
							new FrequentTravellerInformationTypeU();
					FrequentTravellerIdentificationTypeU frequentTraveller = new FrequentTravellerIdentificationTypeU();
					frequentTraveller.setCompanyId(getAlpha2To3(airlineCode));
					frequentTraveller.setMembershipNumber(getAlpha2To25(freqFlier));
					travellerInformationType.setFrequentTraveller(frequentTraveller);
					elementDiv.setFrequentTravellerData(travellerInformationType);
					dataElementDiv.add(elementDiv);
					fflier.add(freqFlier);
					divCount++;
				}
			}
			travellerIndex++;
		}
	}

	public void buildContactPhone() {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType =
				buildElementManagement(divCount.toString(), OTHER_ELEMENT_TATOO_REF, ADD_CONTACT_TO_PNR);
		elementDiv.setElementManagementData(segmentType);
		LongFreeTextType longFreeTextType = new LongFreeTextType();
		FreeTextQualificationType textQualification = new FreeTextQualificationType();
		textQualification.setSubjectQualifier(getAlphaNumeric1To3("3"));
		textQualification.setType(getAlphaString1To4(MOBILE_PHONE_CONTACT));
		String contact = AirSupplierUtils.getContactNumberWithCountryCode(deliveryInfo);
		longFreeTextType.setLongFreetext(getAlpha1To199(contact));
		longFreeTextType.setFreetextDetail(textQualification);
		elementDiv.setFreetextData(longFreeTextType);
		dataElementDiv.add(elementDiv);
		divCount++;
	}

	public void buildContactEmail() {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType =
				buildElementManagement(divCount.toString(), OTHER_ELEMENT_TATOO_REF, ADD_CONTACT_TO_PNR);
		elementDiv.setElementManagementData(segmentType);
		LongFreeTextType longFreeTextType = new LongFreeTextType();
		FreeTextQualificationType textQualification = new FreeTextQualificationType();
		textQualification.setSubjectQualifier(getAlphaNumeric1To3("3"));
		textQualification.setType(getAlphaString1To4(EMAIL_ADDRESS_TICKET_TYPE));
		String emailId = AirSupplierUtils.getEmailId(deliveryInfo);
		longFreeTextType.setLongFreetext(getAlpha1To199(emailId));
		longFreeTextType.setFreetextDetail(textQualification);
		elementDiv.setFreetextData(longFreeTextType);
		dataElementDiv.add(elementDiv);
		divCount++;
	}

	private void buildCommission() {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType elementData =
				buildElementManagement(divCount.toString(), OTHER_ELEMENT_TATOO_REF, COMMISSION_ELEMENT);
		elementDiv.setElementManagementData(elementData);
		CommissionElementType commissionElement = new CommissionElementType();
		CommissionInformationType commissionInfo = new CommissionInformationType();
		Double iataCommission = getIataCommission(segmentInfos);
		BigDecimal iata = new BigDecimal(iataCommission).setScale(2, BigDecimal.ROUND_HALF_UP);
		commissionInfo.setPercentage(getNumeric1To5(iata.toString()));
		commissionElement.setCommissionInfo(commissionInfo);
		elementDiv.setCommission(commissionElement);
		dataElementDiv.add(elementDiv);
		divCount++;
	}

	private Double getIataCommission(List<SegmentInfo> segmentInfos) {
		Double iataCommission = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getIata();
		if (iataCommission != null) {
			return iataCommission;
		}
		// send commission percent as zero if no commission
		return new Double(0);
	}

	private String getTourCode(List<SegmentInfo> segmentInfos) {
		String tourCode = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getTourCode();
		return StringUtils.isNotBlank(tourCode) ? tourCode : null;
	}

	private void buildTicketingElement() {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType elementData =
				buildElementManagement(divCount.toString(), OTHER_ELEMENT_TATOO_REF, TICKETING_ELEMENT);
		divCount++;
		elementDiv.setElementManagementData(elementData);
		elementDiv.setTicketElement(buildTicketElement());
		dataElementDiv.add(elementDiv);
	}

	private TicketElementType buildTicketElement() {
		TicketElementType ticketElement = new TicketElementType();
		TicketInformationType ticketInfo = new TicketInformationType();
		ticketInfo.setIndicator(getAlpha2To2(TICKET_INDICATOR));
		ticketElement.setTicket(ticketInfo);
		return ticketElement;
	}

	private ReferenceInfoType buildReference(String number, String qualifier) {
		ReferenceInfoType referenceInfo = new ReferenceInfoType();
		ReferencingDetailsType[] referenceDetail = new ReferencingDetailsType[1];
		referenceDetail[0] = new ReferencingDetailsType();
		referenceDetail[0].setQualifier(getAlphaNumeric1To3(qualifier));
		referenceDetail[0].setNumber(getAlphaNumeric1To5(number));
		referenceInfo.setReference(referenceDetail);
		return referenceInfo;
	}

	private void buildTravellerInfo(PNR_AddMultiElements pnrAddMultiElements) {
		TravellerInfos_type0[] travellers = new TravellerInfos_type0[paxCountWithOutInf];
		int paxIdIndex = 0;
		Integer paxRef = 1;
		Integer infantId = 0;
		for (int paxIndex = 0; paxIndex < adultCount; paxIndex++) {
			TravellerInfos_type0 passengerInfo = new TravellerInfos_type0();
			boolean isAddInfant = false;
			if (infantId < infantCount) {
				infantId++;
				isAddInfant = true;
			}
			FlightTravellerInfo adultTravellerInfo = adults.get(paxIndex);
			FlightTravellerInfo infantTraveller = null;
			Integer infantIndicator = 1;
			if (isAddInfant) {
				infantTraveller = infants.get(infantId - 1);
				if (StringUtils.isNotEmpty(infantTraveller.getLastName()) && infantTraveller.getDob() != null) {
					infantIndicator = 2;
				}
				if (StringUtils.isNotEmpty(infantTraveller.getFirstName())
						&& StringUtils.isNotEmpty(infantTraveller.getLastName()) && infantTraveller.getDob() != null) {
					infantIndicator = 3;
				}
			}
			List<PassengerData_type0> passengerDatas = new ArrayList<>();
			passengerInfo.setElementManagementPassenger(
					buildElementManagement(paxRef.toString(), PASSENGER_REFERENCE, PASSENGER_NAME_ELEMENT));
			passengerDatas = buildPassengerData(adultTravellerInfo, infantTraveller, isAddInfant, infantIndicator,
					new StringBuffer(), passengerDatas);
			passengerInfo.setPassengerData(passengerDatas.toArray(new PassengerData_type0[0]));
			travellers[paxIdIndex] = passengerInfo;
			paxIdIndex++;
			paxRef++;
		}
		for (int paxIndex = 0; paxIndex < childCount && paxRef - adultCount <= childCount; paxIndex++) {
			TravellerInfos_type0 passengerInfo = new TravellerInfos_type0();
			FlightTravellerInfo travellerInfo = childs.get(paxIndex);
			List<PassengerData_type0> passengerDatas = new ArrayList<>();
			passengerInfo.setElementManagementPassenger(
					buildElementManagement(paxRef.toString(), PASSENGER_REFERENCE, PASSENGER_NAME_ELEMENT));
			passengerDatas = buildPassengerData(travellerInfo, null, false, -1, new StringBuffer(), passengerDatas);
			passengerInfo.setPassengerData(passengerDatas.toArray(new PassengerData_type0[0]));
			travellers[paxIdIndex] = passengerInfo;
			paxRef++;
			paxIdIndex++;
		}
		pnrAddMultiElements.setTravellerInfos(travellers);
	}

	private List<PassengerData_type0> buildPassengerData(FlightTravellerInfo travellerInfo, FlightTravellerInfo infant,
			boolean isAddInfant, Integer infantIndicator, StringBuffer fullName,
			List<PassengerData_type0> passengerDatas) {
		int size = isAddInfant ? 2 : 1;
		PassengerData_type0 passengerData = new PassengerData_type0();
		TravellerInformationTypeI travellerInformation = buildTravellerInformation(travellerInfo, size);
		TravellerDetailsTypeI[] travellerD = new TravellerDetailsTypeI[size];
		if (fullName.length() != 0)
			fullName.append(" ").append(travellerInfo.getLastName());
		else
			fullName.append(travellerInfo.getLastName());
		TravellerDetailsTypeI travellerDetail = new TravellerDetailsTypeI();
		String paxFirstName = getFirstNameWithTitle(travellerInfo);
		travellerDetail.setFirstName(getAlphaString1To56(paxFirstName));
		travellerDetail.setType(getAlphaNumeric1To3(getPNRAddPaxType(travellerInfo.getPaxType())));
		fullName.append(" ").append(paxFirstName);
		if (isAddInfant) {
			travellerDetail.setInfantIndicator(getAplhaNumeric1To1(infantIndicator.toString()));
		}
		travellerD[0] = travellerDetail;
		if (infantIndicator == 2 && infant != null) {
			TravellerDetailsTypeI infantPassenger = new TravellerDetailsTypeI();
			String infantFirstName = getFirstNameWithTitle(infant);
			infantPassenger.setFirstName(getAlphaString1To56(infantFirstName));
			infantPassenger.setType(getAlphaNumeric1To3(getPNRAddPaxType(infant.getPaxType())));
			travellerD[1] = infantPassenger;
			fullName.append(" ").append(infantFirstName);
		}
		travellerInformation.setPassenger(travellerD);
		passengerData.setTravellerInformation(travellerInformation);
		if (!travellerInfo.getPaxType().equals(PaxType.ADULT) && travellerInfo.getDob() != null) {
			DateAndTimeInformationType dateOfBirth = new DateAndTimeInformationType();
			DateAndTimeDetailsTypeI_56946C dateTimeDetails = new DateAndTimeDetailsTypeI_56946C();
			dateTimeDetails.setQualifier(getAlphaNumeric1To3(DOB_QUALIFIER));
			String dob = DateFormatterHelper.format(travellerInfo.getDob(), DOB_FORMAT);
			dateTimeDetails.setDate(getAplha1To8(dob));
			dateOfBirth.setDateAndTimeDetails(dateTimeDetails);
			passengerData.setDateOfBirth(dateOfBirth);
			fullName.append(" ").append(dob);
		}
		passengerDatas.add(passengerData);
		if (infantIndicator == 3) {
			buildPassengerData(infant, null, false, -1, fullName, passengerDatas);
		}
		if (fullName.length() > MAX_NAME_LENGTH) {
			isNameTooLong = true;
		}
		return passengerDatas;
	}

	private String getFirstNameWithTitle(FlightTravellerInfo travellerInfo) {
		String firstName = getNameWithoutSpace(travellerInfo.getFirstName());
		String title = getNameWithoutSpace(travellerInfo.getTitle());
		return StringUtils.join(firstName, title);
	}

	private String getNameWithoutSpace(String name) {
		if (name != null) {
			return name.replaceAll("\\s+", "");
		}
		return StringUtils.EMPTY;
	}

	private TravellerInformationTypeI buildTravellerInformation(FlightTravellerInfo travellerInfo, Integer size) {
		TravellerInformationTypeI travellerInformation = new TravellerInformationTypeI();
		TravellerSurnameInformationTypeI surname = new TravellerSurnameInformationTypeI();
		surname.setSurname(getAlphaNumeric1To57(travellerInfo.getLastName()));
		surname.setQuantity(getNumericLen1To2(size.toString()));
		travellerInformation.setTraveller(surname);
		return travellerInformation;
	}

	private ElementManagementSegmentType buildElementManagement(String paxRef, String referenceCode,
			String segmentName) {
		ElementManagementSegmentType elementManagement = new ElementManagementSegmentType();
		if (StringUtils.isNotBlank(referenceCode) && StringUtils.isNotBlank(paxRef)) {
			ReferencingDetailsType referenceDetail = new ReferencingDetailsType();
			referenceDetail.setNumber(getAlphaNumeric1To5(paxRef));
			referenceDetail.setQualifier(getAlphaNumeric1To3(referenceCode));
			elementManagement.setReference(referenceDetail);
		}
		elementManagement.setSegmentName(getAlphaNumeric1To3(segmentName));
		return elementManagement;
	}

	public void buildPNRAction(PNR_AddMultiElements pnrAddMultiElements, String pnrDetailCode) {
		OptionalPNRActionsType pnrAction = new OptionalPNRActionsType();
		NumericInteger_Length1To3[] actionCode = new NumericInteger_Length1To3[1];
		actionCode[0] = new NumericInteger_Length1To3();
		actionCode[0].setNumericInteger_Length1To3(new BigInteger(pnrDetailCode));
		pnrAction.setOptionCode(actionCode);
		pnrAddMultiElements.setPnrActions(pnrAction);
	}

	public void buildMarker(PNR_AddMultiElements pnrAddMultiElements) {
		dataElement = new DataElementMaster_type0();
		DummySegmentTypeI dummySegment = new DummySegmentTypeI();
		dataElement.setMarker1(dummySegment);
	}

	public void pricePNRwithBookingClass() {
		listener.setType("FarePricePNRWithBookingClass-".concat(SupplierFlowType.BOOK.name()));
		Fare_PricePNRWithBookingClass farePricePNRWithBookingClassRq = null;
		Fare_PricePNRWithBookingClassReply bookingClassReply = null;
		try {
			farePricePNRWithBookingClassRq = buildFarePricePNRWithBookingClassRq();
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			bookingClassReply =
					bookingStub.fare_PricePNRWithBookingClass(farePricePNRWithBookingClassRq, getSessionSchema());
			if (bookingClassReply != null && !checkBookingClassErrorMessage(bookingClassReply)) {
				tstList = createTSTList(bookingClassReply);
				supplierAmount = getTotalAirlineAmount(bookingClassReply);
				fareTimeLimit = getFareTimeLimit(bookingClassReply);
			} else {
				throw new SupplierUnHandledFaultException("Pricing PNR with Booking Class Failed");
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re.getMessage());
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private Calendar getFareTimeLimit(Fare_PricePNRWithBookingClassReply bookingClassReply) {
		Calendar fareTimeLimit = null;
		StringBuilder fareLastTicketingDate = null;
		SimpleDateFormat dateFormat = new SimpleDateFormat(FARE_TIME_LIMIT_FORMAT);
		if (ArrayUtils.isNotEmpty(bookingClassReply.getFareList())) {
			StructuredDateTimeInformationType lastTktDate = bookingClassReply.getFareList()[0].getLastTktDate();
			if (lastTktDate != null && lastTktDate.getDateTime() != null) {
				fareLastTicketingDate = new StringBuilder();
				if (lastTktDate.getDateTime().getDay().getDay_nN().length() != 2) {
					fareLastTicketingDate.append("0").append(lastTktDate.getDateTime().getDay().getDay_nN());
				} else {
					fareLastTicketingDate.append(lastTktDate.getDateTime().getDay().getDay_nN());
				}
				if (lastTktDate.getDateTime().getMonth().getMonth_mM().length() != 2) {
					fareLastTicketingDate.append("0").append(lastTktDate.getDateTime().getMonth().getMonth_mM());
				} else {
					fareLastTicketingDate.append(lastTktDate.getDateTime().getMonth().getMonth_mM());
				}
				fareLastTicketingDate.append(
						String.valueOf(lastTktDate.getDateTime().getYear().getNumericInteger_Length1To6().intValue()));
				// temp end of day 23:59 in PNR Retreive exact date time will be parsed
				fareLastTicketingDate.append(" ").append("2359");
			}
		}
		try {
			fareTimeLimit = Calendar.getInstance();
			fareTimeLimit.setTime(dateFormat.parse(fareLastTicketingDate.toString()));
		} catch (ParseException pe) {
			log.error(AirSourceConstants.NOT_PARSABLE_TIME_LIMIT, bookingId, fareLastTicketingDate, pe);
		}
		return fareTimeLimit;
	}

	private Double getTotalAirlineAmount(Fare_PricePNRWithBookingClassReply bookingClassReply) {
		Double totalFare = new Double(0);
		if (ArrayUtils.isNotEmpty(bookingClassReply.getFareList())) {
			FareList_type0[] fareList = bookingClassReply.getFareList();
			for (FareList_type0 fare : fareList) {
				ReferenceInformationTypeI referenceInfo = fare.getPaxSegReference();
				com.amadeus.xml.tpcbrr_15_1_1a.ReferencingDetailsTypeI[] refDetails = referenceInfo.getRefDetails();
				int numOfPax = 0;
				if (refDetails != null) {
					for (com.amadeus.xml.tpcbrr_15_1_1a.ReferencingDetailsTypeI refDetail : refDetails) {
						if (refDetail.getRefQualifier().getAlphaNumericString_Length1To3()
								.equalsIgnoreCase(INFANT_PASSENGER_REF_QUALIFIER)
								|| refDetail.getRefQualifier().getAlphaNumericString_Length1To3()
										.equalsIgnoreCase(NON_INFANT_PASSENGER_REF_QUALIFIER)
								|| refDetail.getRefQualifier().getAlphaNumericString_Length1To3()
										.equalsIgnoreCase(PASSENGER_REF_QUALIFIER)) {
							numOfPax++;
						}
					}
				}

				MonetaryInformationType_187640S fareDataInformation = fare.getFareDataInformation();
				MonetaryInformationDetailsType_223832C[] fareSupplierInfos =
						fareDataInformation.getFareDataSupInformation();
				for (MonetaryInformationDetailsType_223832C fareSupplierInformation : fareSupplierInfos) {
					if (fareSupplierInformation.getFareDataQualifier().getAlphaNumericString_Length1To3()
							.equalsIgnoreCase(TOTAL_FARE_AMOUNT)) {
						totalFare += getAmountBasedOnCurrency(
								Double.valueOf(
										fareSupplierInformation.getFareAmount().getAlphaNumericString_Length1To11()),
								getCurrencyCode()) * numOfPax;

					}
				}
			}
		}
		return totalFare;
	}

	public void sendTicketCreateTST() {
		listener.setType("TicketCreateTST-".concat(SupplierFlowType.BOOK.name()));
		Ticket_CreateTSTFromPricingReply tstReply = null;
		Ticket_CreateTSTFromPricing tstPricingRq = null;
		try {
			tstPricingRq = buildTstPricingRq(tstList);
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			tstReply = bookingStub.ticket_CreateTSTFromPricing(tstPricingRq, getSessionSchema());
			checkTstErrorMessage(tstReply);
		} catch (AxisFault af) {
			throw new SupplierUnHandledFaultException(af.getMessage());
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re.getMessage());
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private Ticket_CreateTSTFromPricing buildTstPricingRq(List<Integer> tstList) {
		Ticket_CreateTSTFromPricing tstRq = new Ticket_CreateTSTFromPricing();
		PsaList_type0[] psaList = new PsaList_type0[tstList.size()];
		int index = 0;
		for (Integer tst : tstList) {
			psaList[index] = new PsaList_type0();
			ItemReferencesAndVersionsType reference = new ItemReferencesAndVersionsType();
			reference.setReferenceType(getTSTAlphaNumeric1To3(TST_QUALIFIER));
			reference.setUniqueReference(getTSTNumeric1To5(tst.toString()));
			psaList[index].setItemReference(reference);
			index++;
		}
		tstRq.setPsaList(psaList);
		return tstRq;
	}

	private boolean checkTstErrorMessage(Ticket_CreateTSTFromPricingReply tstReply) {
		boolean isError = false;
		if (tstReply == null) {
			throw new SupplierUnHandledFaultException("Create TST From Pricing Failed");
		} else if (tstReply != null && tstReply.getApplicationError() != null) {
			ApplicationError_type3 appError = tstReply.getApplicationError();
			StringJoiner errorMsg = new StringJoiner("");
			ErrorFreeText_type2 errorFree = appError.getErrorText().getErrorFreeText();
			if (errorFree != null) {
				isError = true;
				errorMsg.add(errorFree.getErrorFreeText_type1());
			}
			if (isError && StringUtils.isNotBlank(errorMsg.toString())) {
				log.error("Error TST Faild for Pricing {}", bookingId, errorMsg.toString());
				throw new SupplierUnHandledFaultException(errorMsg.toString());
			}
		}
		return isError;
	}

	private List<Integer> createTSTList(Fare_PricePNRWithBookingClassReply bookingClassReply) {
		List<Integer> tstReferences = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(bookingClassReply.getFareList())) {
			for (FareList_type0 fareList : bookingClassReply.getFareList()) {
				if (fareList.getFareReference().getReferenceType().getAlphaNumericString_Length1To3()
						.equalsIgnoreCase(TST_QUALIFIER)) {
					tstReferences.add(
							fareList.getFareReference().getUniqueReference().getNumericInteger_Length1To5().intValue());
				}
			}
		}
		return tstReferences;
	}

	private boolean checkBookingClassErrorMessage(Fare_PricePNRWithBookingClassReply bookingClassReply) {
		boolean isError = false;
		if (bookingClassReply == null) {
			return true;
		} else if (bookingClassReply != null && bookingClassReply.getApplicationError() != null) {
			ErrorGroupType errorGroup = bookingClassReply.getApplicationError();
			StringJoiner errorMsg = new StringJoiner("");
			if (errorGroup.getErrorWarningDescription() != null
					&& ArrayUtils.isNotEmpty(errorGroup.getErrorWarningDescription().getFreeText())) {
				isError = true;
				com.amadeus.xml.tpcbrr_15_1_1a.AlphaNumericString_Length1To199[] errors =
						errorGroup.getErrorWarningDescription().getFreeText();
				for (com.amadeus.xml.tpcbrr_15_1_1a.AlphaNumericString_Length1To199 error : errors) {
					errorMsg.add(error.getAlphaNumericString_Length1To199());
				}
			}
			if (isError && StringUtils.isNotBlank(errorMsg.toString())) {
				addMessageToLogger(errorMsg.toString());
				throw new SupplierUnHandledFaultException(errorMsg.toString());
			}
		}
		return isError;
	}

	private Fare_PricePNRWithBookingClass buildFarePricePNRWithBookingClassRq() {
		Fare_PricePNRWithBookingClass farePriceRq = new Fare_PricePNRWithBookingClass();
		List<PricingOptionGroup_type1> pricingGroups = buildPricingGroups();
		farePriceRq.setPricingOptionGroup(pricingGroups.toArray(new PricingOptionGroup_type1[0]));
		return farePriceRq;
	}

	private List<PricingOptionGroup_type1> buildPricingGroups() {
		List<String> uniqueFareBasis = AmadeusUtils.getUniqueFareBasisSet(segmentInfos);
		List<PricingOptionGroup_type1> pricingGroups = new ArrayList<>();
		// As Latest API No need to use FBA Options
		// pricingGroups.addAll(buildPricingGroupOnFBA(uniqueFareBasis));


		List<String> corpCodes = getCorporateCode(segmentInfos);
		if (isCorporateFare(segmentInfos) && CollectionUtils.isNotEmpty(corpCodes)) {
			// corp fare type
			PricingOptionGroup_type1 corpGroup = buildFareTypePricingGroup(AmadeusConstants.CORPORATE_FARE_TYPE);
			corpGroup.setOptionDetail(getOptionalDetail(corpCodes));
			pricingGroups.add(corpGroup);

			// Private Fare type
			PricingOptionGroup_type1 privateGroup = buildFareTypePricingGroup(AmadeusConstants.PRIVATE_FARE_TYPE);
			pricingGroups.add(privateGroup);
		} else {
			// public fare type
			PricingOptionGroup_type1 publicGroup = buildFareTypePricingGroup(AmadeusConstants.PUBLISHED_FARE_TYPE);
			pricingGroups.add(publicGroup);
		}

		// cheapest fare in fare type
		PricingOptionGroup_type1 publicGroup = buildFareTypePricingGroup(AmadeusConstants.CHEAPEST_FARE_TYPE);
		pricingGroups.add(publicGroup);
		// Validating carrier
		PricingOptionGroup_type1 carrierGroup = buildFareTypePricingGroup(AmadeusConstants.VALIDATING_CARRIER);
		TransportIdentifierType transportIdentifier = new TransportIdentifierType();
		CompanyIdentificationTypeI companyIdentification = new CompanyIdentificationTypeI();
		companyIdentification.setOtherCompany(getAlphaNumeric1To35(segmentInfos.get(0).getPlatingCarrier(null)));
		transportIdentifier.setCompanyIdentification(companyIdentification);
		carrierGroup.setCarrierInformation(transportIdentifier);
		pricingGroups.add(carrierGroup);
		if (StringUtils.isNotBlank(getSupplierCurrency())) {
			PricingOptionGroup_type1 currencyGroup = buildFareTypePricingGroup(AmadeusConstants.FARE_CURRENCY_OVERRIDE);
			currencyGroup.setCurrency(buildCurrencyInfo());
			pricingGroups.add(currencyGroup);
		}
		return pricingGroups;
	}

	private CurrenciesType buildCurrencyInfo() {
		CurrenciesType currencyType = new CurrenciesType();
		currencyType.setFirstCurrencyDetails(getFirstDetailCurrency());
		return currencyType;
	}

	private CurrencyDetailsTypeU getFirstDetailCurrency() {
		CurrencyDetailsTypeU currencyDetails = new CurrencyDetailsTypeU();
		currencyDetails.setCurrencyIsoCode(getAlpha1To3(getCurrencyCode()));
		currencyDetails.setCurrencyQualifier(getAlpha1To3(AmadeusConstants.FARE_CURRENCY_OVERRIDE));
		return currencyDetails;
	}

	private PricingOptionGroup_type1 buildFareTypePricingGroup(String fareType) {
		PricingOptionGroup_type1 pricingOptionGroup = new PricingOptionGroup_type1();
		PricingOptionKey optionKey = new PricingOptionKey();
		optionKey.setPricingOptionKey(getAlpha1To3(fareType));
		pricingOptionGroup.setPricingOptionKey(optionKey);
		return pricingOptionGroup;
	}

	public void setPaxSegInfAssociationLists(Map<String, Set<Integer>> segmentAssociations,
			Map<String, Set<Integer>> passengerAssociations, Map<String, Set<Integer>> infantPassengersAssociations) {
		for (int segIdx = 0; segIdx < segmentInfos.size(); segIdx++) {
			SegmentInfo segmentInfo = segmentInfos.get(segIdx);
			List<FlightTravellerInfo> travellerInfoList = segmentInfo.getTravellerInfo();
			for (int paxIdx = 0; paxIdx < travellerInfoList.size(); paxIdx++) {
				FareDetail fareDetail = travellerInfoList.get(paxIdx).getFareDetail();
				if (!segmentAssociations.containsKey(fareDetail.getFareBasis())) {
					segmentAssociations.put(fareDetail.getFareBasis(), new HashSet<>());
				}
				segmentAssociations.get(fareDetail.getFareBasis()).add(segmentIndexToRefMap.get(segIdx));
				if (travellerInfoList.get(paxIdx).getPaxType().equals(PaxType.INFANT)) {
					if (!infantPassengersAssociations.containsKey(fareDetail.getFareBasis())) {
						infantPassengersAssociations.put(fareDetail.getFareBasis(), new HashSet<>());
					}
					infantPassengersAssociations.get(fareDetail.getFareBasis()).add(passengerIndexToRefMap.get(paxIdx));
				} else {
					if (!passengerAssociations.containsKey(fareDetail.getFareBasis())) {
						passengerAssociations.put(fareDetail.getFareBasis(), new HashSet<>());
					}
					passengerAssociations.get(fareDetail.getFareBasis()).add(passengerIndexToRefMap.get(paxIdx));
				}
			}
		}
	}

	private List<PricingOptionGroup_type1> buildPricingGroupOnFBA(List<String> fareBasisList) {
		Map<String, Set<Integer>> segmentAssociations = new HashMap<>();
		Map<String, Set<Integer>> passengerAssociations = new HashMap<>();
		Map<String, Set<Integer>> infantPassengersAssociations = new HashMap<>();
		setPaxSegInfAssociationLists(segmentAssociations, passengerAssociations, infantPassengersAssociations);
		List<PricingOptionGroup_type1> pricingOption = new ArrayList<>();
		fareBasisList.forEach(fareBasis -> {
			PricingOptionGroup_type1 pricingOptionGroup = buildFareTypePricingGroup(FARE_BASIS_OVERIDE);
			pricingOptionGroup.setOptionDetail(getOptionalDetail(Arrays.asList(fareBasis)));
			List<String> overrideSegmentTatoos = getSegmenttTatoosOnFareBasis(fareBasis);
			setPaxTSTReference(pricingOptionGroup, fareBasis, passengerAssociations, infantPassengersAssociations,
					overrideSegmentTatoos);
			pricingOption.add(pricingOptionGroup);
		});
		return pricingOption;
	}

	private void setPaxTSTReference(PricingOptionGroup_type1 optionGroup, String tripFareBasis,
			Map<String, Set<Integer>> passengerAssociations, Map<String, Set<Integer>> infantPassengersAssociations,
			List<String> segments) {
		if (CollectionUtils.isNotEmpty(segments)) {
			com.amadeus.xml.tpcbrq_15_1_1a.ReferenceInfoType referenceInfoType =
					new com.amadeus.xml.tpcbrq_15_1_1a.ReferenceInfoType();
			List<com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType> referencingDetailsTypes = new ArrayList<>();
			Set<Integer> passengers = new HashSet<>();
			Set<Integer> infantPassengers = new HashSet<>();
			segments.forEach(segment -> {
				com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType referencingDetailsType =
						new com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType();
				referencingDetailsType.setType(getAlphaNumeric1To10(SEGMENT_REF_QAULIFIER));
				referencingDetailsType.setValue(getAlpha1To60(segment));
				referencingDetailsTypes.add(referencingDetailsType);
			});

			passengers = passengerAssociations.get(tripFareBasis);
			if (CollectionUtils.isNotEmpty(passengers)) {
				passengers.forEach(pax -> {
					if (pax != null) {
						com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType referencingDetailsType =
								new com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType();
						referencingDetailsType.setType(getAlphaNumeric1To10(NON_INFANT_PASSENGER_REF_QUALIFIER));
						referencingDetailsType.setValue(getAlpha1To60(String.valueOf(pax)));
						referencingDetailsTypes.add(referencingDetailsType);
					}
				});
			}
			infantPassengers = infantPassengersAssociations.get(tripFareBasis);
			if (CollectionUtils.isNotEmpty(infantPassengers)) {
				infantPassengers.forEach(infPasRef -> {
					if (infPasRef != null) {
						com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType referencingDetailsType =
								new com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType();
						referencingDetailsType.setType(getAlphaNumeric1To10(INFANT_PASSENGER_REF_QUALIFIER));
						referencingDetailsType.setValue(getAlpha1To60(String.valueOf(infPasRef)));
						referencingDetailsTypes.add(referencingDetailsType);
					}
				});
			}
			referenceInfoType.setReferenceDetails(
					referencingDetailsTypes.toArray(new com.amadeus.xml.tpcbrq_15_1_1a.ReferencingDetailsType[0]));
			optionGroup.setPaxSegTstReference(referenceInfoType);
		}
	}


	private AttributeType getOptionalDetail(List<String> codes) {
		AttributeType attributeType = new AttributeType();
		AttributeInformationTypeU[] informationTypeU = new AttributeInformationTypeU[codes.size()];
		int index = 0;
		for (String code : codes) {
			informationTypeU[index] = new AttributeInformationTypeU();
			informationTypeU[index].setAttributeType(getAlpha1To25(code));
			index++;
		}
		attributeType.setCriteriaDetails(informationTypeU);
		return attributeType;
	}


	private List<String> getSegmenttTatoosOnFareBasis(String fareBasis) {
		List<String> segmentTattos = new ArrayList<>();
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.getPriceInfo(0).getFareDetails().forEach((paxType, fareDetail) -> {
				String tatoo = segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey();
				if (fareBasis.equalsIgnoreCase(fareDetail.getFareBasis())) {
					if (!segmentTattos.contains(tatoo)) {
						segmentTattos.add(tatoo);
					}
				}
			});
		});
		return segmentTattos;
	}

	public void sendFOP(CreditCardInfo supplierBookingCreditCard) {
		listener.setType("FormOfPayment-".concat(SupplierFlowType.BOOK.name()));
		listener.getVisibilityGroups().addAll(AirSupplierUtils.getLogVisibility());
		PNR_Reply pnrReply = null;
		PNR_AddMultiElements pnrRequest = null;
		int retryCount = 1;
		boolean isFopSuccess = false;
		while (retryCount <= 3 && !isFopSuccess) {
			try {
				pnrRequest = buildFOPRequest(supplierBookingCreditCard);
				bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				pnrReply = bookingStub.pNR_AddMultiElements(pnrRequest, getSessionSchema());
				if (retryCount == 3) {
					// if not success in first 1 commit then some thing wrong so capture error note message
					checkPNRReplyErrorMessage(pnrReply);
				} else if (isSimultaneousChanges(pnrReply)) {
					Thread.sleep(1000 * SIMULTANEOUS_SLEEP_TIME_IN_SECS);
				} else if (!checkPNRReplyErrorMessage(pnrReply)) {
					isFopSuccess = true;
				}
			} catch (InterruptedException e) {
				log.error(AirSourceConstants.AIR_SUPPLIER_PNR_THREAD, bookingId);
				throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
			} catch (AxisFault af) {
				throw new SupplierUnHandledFaultException(af.getMessage());
			} catch (RemoteException re) {
				throw new SupplierRemoteException(re);
			} finally {
				retryCount++;
				bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			}
		}
	}

	public boolean isPNRStatusActive(Integer sleepInSecs, boolean isExtractPaxList) {
		boolean isHKStatus = false;
		try {
			if (sleepInSecs != null)
				Thread.sleep(1000 * sleepInSecs);
			PNR_Reply reply = retrieveManager.retrieveBooking(crsPNR);
			if (!checkPNRReplyErrorMessage(reply)) {
				isHKStatus = isSegmentsActive(reply);
				if (isExtractPaxList) {
					parseTravellerReferences(reply.getTravellerInfo());
				}
			}
		} catch (Exception e) {
			log.error("PNR Retreive Error Occured for bookingid {}", bookingId, e);
		}
		return isHKStatus;
	}

	private boolean isSegmentsActive(PNR_Reply reply) {
		boolean isSegmentHKStatus = true;
		segmentIndexToRefMap = new HashMap<>();
		if (reply != null && ArrayUtils.isNotEmpty(reply.getOriginDestinationDetails())) {
			OriginDestinationDetails_type0[] originDestinationDetails = reply.getOriginDestinationDetails();
			for (OriginDestinationDetails_type0 originDestinationDetail : originDestinationDetails) {
				ItineraryInfo_type1[] itineraryInfos = originDestinationDetail.getItineraryInfo();
				if (ArrayUtils.isNotEmpty(itineraryInfos)) {
					for (ItineraryInfo_type1 itineraryInfo : itineraryInfos) {
						TravelProduct_type0 travelProduct = itineraryInfo.getTravelProduct();
						Integer segmentIndex = getSegmentIndex(travelProduct);
						RelatedProduct_type0 relatedProduct = itineraryInfo.getRelatedProduct();
						if (relatedProduct.getStatus() != null && ArrayUtils.isNotEmpty(relatedProduct.getStatus())) {
							String status = relatedProduct.getStatus()[0].getStatus_type6();
							if (!status.equalsIgnoreCase(PNR_ACTIVE)) {
								isSegmentHKStatus = false;
							}
						}
						if (segmentIndex != null && segmentIndex >= 0) {
							String refNumber = itineraryInfo.getElementManagementItinerary().getReference().getNumber()
									.getNumber_type8();
							segmentIndexToRefMap.put(segmentIndex, Integer.valueOf(refNumber));
							segmentInfos.get(segmentIndex).getPriceInfo(0).getMiscInfo().setSegmentKey(refNumber);
						}
					}
				}
			}
		}
		return isSegmentHKStatus;
	}

	public void issueTicket() {
		listener.setType("DocIssuance-".concat(SupplierFlowType.BOOK.name()));
		DocIssuance_IssueTicket docIssueTicketRq = null;
		DocIssuance_IssueTicketReply docIssuanceReply = null;
		int retryCount = 1;
		boolean isTktSucccess = false;
		while (retryCount <= 3 && !isTktSucccess) {
			try {
				docIssueTicketRq = buildDocIssuanceRequest();
				bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				docIssuanceReply = bookingStub.docIssuance_IssueTicket(docIssueTicketRq, getSessionSchema());
				boolean isError = checkIsDocIssuanceSuccessfull(docIssuanceReply);
				if (retryCount == 3 && isError) {
					throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
				} else if (!isError) {
					isTktSucccess = true;
				}
			} catch (AxisFault af) {
				throw new SupplierUnHandledFaultException(af.getMessage());
			} catch (RemoteException re) {
				throw new SupplierRemoteException(re);
			} finally {
				bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
				retryCount++;
			}
		}
	}

	private DocIssuance_IssueTicket buildDocIssuanceRequest() {
		DocIssuance_IssueTicket docIssuanceRq = new DocIssuance_IssueTicket();
		OptionGroup_type0[] optionGroups = new OptionGroup_type0[1];
		optionGroups[0] = new OptionGroup_type0();
		com.amadeus.xml.ttktiq_15_1_1a.StatusTypeI status = new com.amadeus.xml.ttktiq_15_1_1a.StatusTypeI();
		com.amadeus.xml.ttktiq_15_1_1a.StatusDetailsTypeI detailsTypeI =
				new com.amadeus.xml.ttktiq_15_1_1a.StatusDetailsTypeI();
		detailsTypeI.setIndicator(getDIAlpha1To3(ETICKET_ITINERARY_RECIPT));
		// detailsTypeI.setIndicator(getDIAlpha1To3("ET"));
		status.setStatusDetails(detailsTypeI);
		optionGroups[0].setSwitches(status);
		docIssuanceRq.setOptionGroup(optionGroups);
		return docIssuanceRq;
	}

	private boolean checkIsDocIssuanceSuccessfull(DocIssuance_IssueTicketReply docIssuanceReply) {
		boolean isError = true;
		if (docIssuanceReply != null && docIssuanceReply.getErrorGroup() != null) {
			ErrorWarningDescription_type0 descriptionType =
					docIssuanceReply.getErrorGroup().getErrorWarningDescription();
			if (descriptionType.getFreeText() != null) {
				String errorMsg = descriptionType.getFreeText().getFreeText_type0();
				if (StringUtils.isNotBlank(errorMsg) && errorMsg.toUpperCase().contains(OK_ETICKET)) {
					isError = false;
				} else {
					addMessageToLogger(errorMsg);
					log.error("DocIssuance Failed for booking {}  error msg {}", bookingId, errorMsg);
				}
			}
		}
		return isError;
	}

	public void commitTicketNumber() {
		int tryCount = 1;
		boolean ticketNumberCreated = false;
		while (tryCount <= 3 && !ticketNumberCreated) {
			try {
				Thread.sleep(1000 * TICKET_SLEEP_TIME_IN_SECS);
				PNR_Reply reply = retrieveManager.retrieveBooking(crsPNR);
				ticketNumberCreated = true;
				ticketNumbers = new ArrayList<>();
				List<String> ticketNumbers = extractTicketNumbers(reply);
				setTicketNumbers(ticketNumbers, segmentInfos);
			} catch (Exception e) {
				log.error("PNR Retreive Ticket Number Error Occured for bookingid {}", bookingId, e);
			} finally {
				tryCount++;
			}
		}
	}

	private boolean isSimultaneousChanges(PNR_Reply reply) {
		boolean isSimultaneousChanges = false;
		StringJoiner errorMsg = new StringJoiner("");
		if (reply != null && reply.getGeneralErrorInfo() != null
				&& ArrayUtils.isNotEmpty(reply.getGeneralErrorInfo())) {
			GeneralErrorInfo_type0[] generalErrors = reply.getGeneralErrorInfo();
			for (GeneralErrorInfo_type0 generalError : generalErrors) {
				isSimultaneousChanges = true;
				FreeText_type3[] freeTypes = generalError.getErrorWarningDescription().getFreeText();
				for (FreeText_type3 freeType : freeTypes) {
					errorMsg.add(freeType.getFreeText_type2());
				}
			}
		}
		return isSimultaneousChanges && errorMsg.toString().toUpperCase().contains("SIMULTANEOUS CHANGES TO PNR");
	}

	private boolean isFinishOrIgnore(PNR_Reply reply) {
		boolean isFinishOrIgnore = false;
		StringJoiner errorMsg = new StringJoiner("");
		if (reply != null && reply.getGeneralErrorInfo() != null
				&& ArrayUtils.isNotEmpty(reply.getGeneralErrorInfo())) {
			GeneralErrorInfo_type0[] generalErrors = reply.getGeneralErrorInfo();
			for (GeneralErrorInfo_type0 generalError : generalErrors) {
				isFinishOrIgnore = true;
				FreeText_type3[] freeTypes = generalError.getErrorWarningDescription().getFreeText();
				for (FreeText_type3 freeType : freeTypes) {
					errorMsg.add(freeType.getFreeText_type2());
					addMessageToLogger(errorMsg.toString());
				}
			}
		}
		return isFinishOrIgnore && errorMsg.toString().toUpperCase().contains("FINISH OR IGNORE");
	}

	private PNR_AddMultiElements buildFOPRequest(CreditCardInfo supplierBookingCreditCard) {
		PNR_AddMultiElements pnrAddMultiElements = new PNR_AddMultiElements();
		dataElementDiv = new ArrayList<>();
		divCount = 1;
		buildMarker(pnrAddMultiElements);
		buildPNRAction(pnrAddMultiElements, PNR_ADD_DETAILS_CODE);
		buildReservationInfo(crsPNR, pnrAddMultiElements);
		buildFOP(supplierBookingCreditCard);
		addToDataElement(pnrAddMultiElements);
		return pnrAddMultiElements;
	}

	private void buildReservationInfo(String crsPNR, PNR_AddMultiElements pnrAddMultiElements) {
		ReservationControlInformationTypeI reservationInfo = new ReservationControlInformationTypeI();
		ReservationControlInformationDetailsTypeI type = new ReservationControlInformationDetailsTypeI();
		type.setControlNumber(getAlpha1To19(crsPNR));
		reservationInfo.setReservation(type);
		pnrAddMultiElements.setReservationInfo(reservationInfo);
	}

	private void buildFOP(CreditCardInfo creditCardInfo) {
		boolean isCreditCard = false;
		DataElementsIndiv_type0 elementsIndiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType =
				buildElementManagement(divCount.toString(), null, FOP_TRANSACTION_REF);
		elementsIndiv.setElementManagementData(segmentType);
		FormOfPaymentTypeI formOfPaymentType = new FormOfPaymentTypeI();
		FormOfPaymentDetailsTypeI paymentDetails = new FormOfPaymentDetailsTypeI();

		if (creditCardInfo != null && StringUtils.isNotBlank(creditCardInfo.getCardNumber())) {
			isCreditCard = true;
			String cardExpiryDate = AmadeusUtils.getCreditExpiryDate(creditCardInfo.getExpiry());
			paymentDetails.setAccountNumber(getAlpha1To35(creditCardInfo.getCardNumber()));
			paymentDetails.setCreditCardCode(getAlphaNumeric1To3(creditCardInfo.getCardType().getVendorCode()));
			paymentDetails.setIdentification(getAlphaNumeric1To3(CREDIT_CARD));
			Date_MMYY dateMMYY = new Date_MMYY();
			dateMMYY.setDate_MMYY(cardExpiryDate);
			paymentDetails.setExpiryDate(dateMMYY);
		} else {
			paymentDetails.setIdentification(getAlphaNumeric1To3(CASH_IDENTIFICATION));
		}
		MarketSpecificDataType[] specificInfo = new MarketSpecificDataType[1];
		specificInfo[0] = new MarketSpecificDataType();
		specificInfo[0].setFopSequenceNumber(getNumericLen1To2(BigInteger.ONE.toString()));
		if (isCreditCard) {
			MarketSpecificDataDetailsType specificDataDetailsType = new MarketSpecificDataDetailsType();
			if (StringUtils.isNotBlank(creditCardInfo.getCvv())) {
				specificDataDetailsType.setCvData(getAlpha1To10(creditCardInfo.getCvv()));
				specificInfo[0].setNewFopsDetails(specificDataDetailsType);
			} else {
				specificDataDetailsType.setCvData(getAlpha1To10(StringUtils.EMPTY));
				specificInfo[0].setNewFopsDetails(specificDataDetailsType);
			}
		}
		formOfPaymentType.addFop(paymentDetails);
		elementsIndiv.setFopExtension(specificInfo);
		elementsIndiv.setFormOfPayment(formOfPaymentType);
		dataElementDiv.add(elementsIndiv);
	}

	public void retrieveAirlinePNR(String crsPNR) {
		int tryCount = 1;
		boolean airlinePnrCreated = false;
		while (tryCount <= 3 && !airlinePnrCreated) {
			try {
				Thread.sleep(1000 * SLEEP_TIME_IN_SECS);
				PNR_Reply reply = retrieveManager.retrieveBooking(crsPNR);
				if (!checkPNRReplyErrorMessage(reply)) {
					airlinePnrs = parseAirlinePnr(reply);
					airlinePnrCreated = true;
					setSegmentsAirlinePnr(airlinePnrs, segmentInfos);
					if (isHoldBooking) {
						checkForTimeLimit(reply);
					}
				}
			} catch (Exception e) {
				log.error("PNR Retreive Error Occured for bookingid {}", bookingId, e);
			} finally {
				tryCount++;
			}
		}
	}

	private void checkForTimeLimit(PNR_Reply reply) {
		String plattingCarrier = segmentInfos.get(0).getPlatingCarrier(null);
		AirlineTimeLimitData airlineTimeLimitData =
				AirUtils.getAirlineTimeLimitData(plattingCarrier, sourceConfig, bookingUser);

		DateFormat dateFormat = new SimpleDateFormat(TICKET_TIME_LIMIT_FORMAT);
		dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		try {
			if (airlineTimeLimitData != null && reply != null && reply.getDataElementsMaster() != null) {
				DataElementsMaster_type0 dataElementMaster = reply.getDataElementsMaster();
				DataElementsIndiv_type1[] dataElementsInDivs = dataElementMaster.getDataElementsIndiv();
				if (ArrayUtils.isNotEmpty(dataElementsInDivs)) {
					DataElementsIndiv_type1 timeLimitElement = getTicketingTimeLimitDataElement(dataElementsInDivs);
					if (timeLimitElement != null) {

						OptionElement_type0 optionElement = timeLimitElement.getOptionElement();
						String segmentName =
								timeLimitElement.getElementManagementData().getSegmentName().getSegmentName_type4();
						if (optionElement != null && optionElement.getOptionElementInfo() != null
								&& optionElement.getOptionElementInfo().getFreetext() != null
								&& optionElement.getOptionElementInfo().getFreetext().getFreetext_type22() != null
								&& AmadeusConstants.TIME_LIMIT_TAGS.contains(segmentName)) {

							String freeText = optionElement.getOptionElementInfo().getFreetext().getFreetext_type22();
							if (StringUtils.isNotBlank(freeText)) {
								String rejexPattern = airlineTimeLimitData.getRegexPattern();
								String timeFormat = airlineTimeLimitData.getTimeFormat();
								List<String> matchedStrings = TgsStringUtils.parseStringByRejex(freeText, rejexPattern);
								if (CollectionUtils.isNotEmpty(matchedStrings)) {
									// Selecting first matched pattern of hold time limit.
									String timeLimitData = matchedStrings.get(0);
									ticketingTimeLimit = TgsDateUtils.convertStringToDate(timeLimitData, timeFormat);
									if (ticketingTimeLimit != null) {
										LocalDateTime now = LocalDateTime.now();
										// To handle without year case. TimeLimit without year will set the minimum
										// yearof LocalDateTime.
										if (ticketingTimeLimit.isBefore(now)) {
											ticketingTimeLimit = ticketingTimeLimit.withYear(now.getYear());
											// Booking at the last day of the year & getting time limit of next
											// year.
											if (ticketingTimeLimit.isBefore(now)) {
												ticketingTimeLimit = ticketingTimeLimit.plusYears(1);
											}
										}
										log.info("Ticketing time limit {} for bookingId {} ", bookingId,
												ticketingTimeLimit);
									}
								}
							}

						}
					}
				}
			}
		} catch (ParseException pe) {
			log.error(AirSourceConstants.NOT_PARSABLE_TIME_LIMIT, bookingId, null, pe);
		}
	}

	private DataElementsIndiv_type1 getTicketingTimeLimitDataElement(DataElementsIndiv_type1[] dataElementsInDivs) {
		DataElementsIndiv_type1 ticketElement = null;
		for (DataElementsIndiv_type1 dataElement : dataElementsInDivs) {
			Ssr_type0 ssrType =
					dataElement.getServiceRequest() != null && dataElement.getServiceRequest().getSsr() != null
							? dataElement.getServiceRequest().getSsr()
							: null;
			String segmentName = dataElement.getElementManagementData().getSegmentName().getSegmentName_type4();
			String elementName =
					(ssrType != null && ssrType.getType() != null) ? ssrType.getType().getType_type158() : null;
			if (AmadeusConstants.TIME_LIMIT_TAGS.contains(segmentName)
					|| segmentName.equalsIgnoreCase("SSR") && AmadeusConstants.TIME_LIMIT_TAGS.contains(elementName)) {
				ticketElement = dataElement;
				break;
			}
		}
		return ticketElement;
	}

	public void commitSSRs(PNR_AddMultiElements ssrElements, GstInfo gstInfo) {
		listener.setType("PNR_AddMultiElements-Ancillary-".concat(SupplierFlowType.BOOK.name()));
		PNR_Reply pnrReply = null;
		PNR_AddMultiElements pnrRequest = null;
		int retryCount = 1;
		boolean isSSRAdded = false;
		while (retryCount <= 3 && !isSSRAdded) {
			try {
				pnrRequest = buildSSRElements(ssrElements, gstInfo);
				if (pnrRequest != null) {
					bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
					pnrReply = bookingStub.pNR_AddMultiElements(pnrRequest, getSessionSchema());
					if (retryCount == 3) {
						// if not success in first 1 commit then some thing wrong so capture error note message
						checkPNRReplyErrorMessage(pnrReply);
					} else if (isSimultaneousChanges(pnrReply)) {
						Thread.sleep(1000 * SIMULTANEOUS_SLEEP_TIME_IN_SECS);
					} else if (!checkPNRReplyErrorMessage(pnrReply)) {
						isSSRAdded = true;
					}
				}
			} catch (InterruptedException e) {
				log.error(AirSourceConstants.AIR_SUPPLIER_PNR_THREAD, bookingId);
				throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
			} catch (AxisFault af) {
				throw new SupplierUnHandledFaultException(af.getMessage());
			} catch (RemoteException re) {
				throw new SupplierRemoteException(re.getMessage());
			} finally {
				retryCount++;
				bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			}
		}
	}

	private PNR_AddMultiElements buildSSRElements(PNR_AddMultiElements ssrElements, GstInfo gstInfo) {
		if (ssrElements == null) {
			ssrElements = new PNR_AddMultiElements();
			buildMarker(ssrElements);
			buildPNRAction(ssrElements, PNR_ADD_DETAILS_CODE);
		}
		// Add Any Special Request Here
		if (AirUtils.isSSRAddedInTrip(segmentInfos)) {
			buildSpecialRequest(segmentInfos);
		}

		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			buildGSTInfo(gstInfo);
		}
		buildFrequentFlier();
		addToDataElement(ssrElements);
		if (ssrElements != null && ssrElements.getDataElementMaster() != null) {
			return ssrElements;
		}
		return null;
	}

	private void buildSpecialRequest(List<SegmentInfo> segmentInfos) {

		for (int index = 0; index < segmentInfos.size(); index++) {
			int segmentKey = Integer.valueOf(segmentInfos.get(index).getPriceInfo(0).getMiscInfo().getSegmentKey());
			List<FlightTravellerInfo> bookingTravellerInfos =
					segmentInfos.get(index).getBookingRelatedInfo().getTravellerInfo();
			int travellerIndex = 0;
			for (FlightTravellerInfo traveller : bookingTravellerInfos) {
				Integer paxReference = passengerIndexToRefMap.get(travellerIndex);
				if (Objects.nonNull(traveller.getSsrMealInfo())) {
					buildSSRComponent(traveller, paxReference, segmentKey, traveller.getSsrMealInfo().getCode());
				}
				if (Objects.nonNull(traveller.getExtraServices())) {
					for (SSRInformation ssrInformation : traveller.getExtraServices()) {
						buildSSRComponent(traveller, paxReference, segmentKey, ssrInformation.getCode());
					}
				}
				if (Objects.nonNull(traveller.getSsrSeatInfo())) {
					buildSSRComponent(traveller, paxReference, segmentKey, traveller.getSsrSeatInfo().getCode());
				}
				travellerIndex++;
			}
		}
	}

	private void buildSSRComponent(FlightTravellerInfo traveller, int passIndex, int segIndex, String ssrType) {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType = buildElementManagement(null, null, SPECIAL_SERVICE_REQUEST);
		elementDiv.setElementManagementData(segmentType);


		ReferenceInfoType referenceForDataElement = new ReferenceInfoType();
		ReferencingDetailsType[] reference = new ReferencingDetailsType[2];
		reference[0] = getReference(traveller, passIndex, AmadeusConstants.PASSENGER_REFERENCE);
		reference[1] = getReference(traveller, segIndex, AmadeusConstants.SEGMENT_REFERENCE);
		referenceForDataElement.setReference(reference);
		elementDiv.setReferenceForDataElement(referenceForDataElement);


		SpecialRequirementsDetailsTypeI requirementDetails = new SpecialRequirementsDetailsTypeI();
		SpecialRequirementsTypeDetailsTypeI detailsTypeI = new SpecialRequirementsTypeDetailsTypeI();
		detailsTypeI.setType(getSSRType(ssrType));
		// detailsTypeI.setFreetext(getSSRFreeText());
		requirementDetails.setSsr(detailsTypeI);
		elementDiv.setServiceRequest(requirementDetails);

		dataElementDiv.add(elementDiv);
		divCount++;
	}


	private AlphaNumericString_Length1To70[] getSSRFreeText() {
		AlphaNumericString_Length1To70[] freeText = new AlphaNumericString_Length1To70[1];
		freeText[0] = new AlphaNumericString_Length1To70();
		freeText[0].setAlphaNumericString_Length1To70("OK");
		return freeText;
	}

	private AlphaNumericString_Length1To4 getSSRType(String ssrType) {
		AlphaNumericString_Length1To4 type = new AlphaNumericString_Length1To4();
		type.setAlphaNumericString_Length1To4(ssrType);
		return type;
	}

	private ReferencingDetailsType getReference(FlightTravellerInfo traveller, int index, String qualifier) {
		ReferencingDetailsType reference = new ReferencingDetailsType();
		reference.setQualifier(getQualifier(qualifier));
		// chnage this
		reference.setNumber(getQualifierNumber(String.valueOf(index)));
		return reference;
	}

	private AlphaNumericString_Length1To5 getQualifierNumber(String index) {
		AlphaNumericString_Length1To5 type = new AlphaNumericString_Length1To5();
		type.setAlphaNumericString_Length1To5(index);
		return type;
	}

	private AlphaNumericString_Length1To3 getQualifier(String qualifier) {
		AlphaNumericString_Length1To3 type = new AlphaNumericString_Length1To3();
		type.setAlphaNumericString_Length1To3(qualifier);
		return type;
	}

	private void buildGSTInfo(GstInfo gstInfo) {

		String plattingCarrier = segmentInfos.get(0).getPlatingCarrier(null);

		if (StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			String registeredName = BaseUtils.getCleanGstName(gstInfo.getRegisteredName()).replaceAll("[^\\w\\s]", "")
					.replaceAll(" ", "");
			String textLine = StringUtils.join("IND/", gstInfo.getGstNumber(),
					"/" + registeredName.subSequence(0, Math.min(35, registeredName.length())));
			buildGSTcomponent("GSTN", Arrays.asList(textLine), plattingCarrier);
		}
		if (StringUtils.isNotBlank(gstInfo.getMobile())) {
			String mobileNumber = AmadeusUtils.getContactNumberWithCountryCode(gstInfo.getMobile());
			String text = StringUtils.join("IND/", mobileNumber);
			buildGSTcomponent("GSTP", Arrays.asList(text), plattingCarrier);
		}

		if (StringUtils.isNotBlank(gstInfo.getEmail())) {
			String email = gstInfo.getEmail().replaceAll("[@]", "//");
			email = email.replaceAll("[_]", "..");
			buildGSTcomponent("GSTE", Arrays.asList(email), plattingCarrier);
		}

		/**
		 * if (StringUtils.isNotBlank(gstInfo.getAddress())) { String address =
		 * gstInfo.getAddress().replaceAll("[$&:.+;|]", ""); address = address.replaceAll("[,/]", " "); textLines =
		 * AmadeusUtils.getGSTAddressCharacterLimitations(address); buildGSTcomponent("GSTA", textLines, airlineCode); }
		 */
	}

	private void buildGSTcomponent(String gstType, List<String> textLines, String airlineCode) {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType = buildElementManagement(null, null, SPECIAL_SERVICE_REQUEST);
		elementDiv.setElementManagementData(segmentType);
		SpecialRequirementsDetailsTypeI requirementDetails = new SpecialRequirementsDetailsTypeI();
		SpecialRequirementsTypeDetailsTypeI detailsTypeI = new SpecialRequirementsTypeDetailsTypeI();
		detailsTypeI.setType(getAlphaString1To4(gstType));
		detailsTypeI.setStatus(getAlphaNumeric1To3(SSR_STATUS));
		detailsTypeI.setCompanyId(getAlphaNumeric1To3(airlineCode));
		AlphaNumericString_Length1To70[] freeTexts = new AlphaNumericString_Length1To70[textLines.size()];
		int index = 0;
		for (String text : textLines) {
			freeTexts[index] = getAlpha1To70(text);
			index++;
		}
		detailsTypeI.setFreetext(freeTexts);
		requirementDetails.setSsr(detailsTypeI);
		elementDiv.setServiceRequest(requirementDetails);
		dataElementDiv.add(elementDiv);
		divCount++;
	}

	public void buildSSRContacts(String contact, String ssrType) {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType =
				buildElementManagement(divCount.toString(), null, SPECIAL_SERVICE_REQUEST);
		elementDiv.setElementManagementData(segmentType);
		LongFreeTextType longFreeTextType = new LongFreeTextType();
		FreeTextQualificationType textQualification = new FreeTextQualificationType();
		textQualification.setSubjectQualifier(getAlphaNumeric1To3("3"));
		textQualification.setType(getAlphaString1To4(EMAIL_ADDRESS_TICKET_TYPE));
		String emailId = AirSupplierUtils.getEmailId(deliveryInfo);
		longFreeTextType.setLongFreetext(getAlpha1To199(emailId));
		longFreeTextType.setFreetextDetail(textQualification);
		elementDiv.setFreetextData(longFreeTextType);
		SpecialRequirementsDetailsTypeI ssr = new SpecialRequirementsDetailsTypeI();
		SpecialRequirementsTypeDetailsTypeI detailsTypeI = new SpecialRequirementsTypeDetailsTypeI();
		detailsTypeI.setType(getAlphaString1To4(ssrType));
		detailsTypeI.setStatus(getAlphaNumeric1To3(SSR_STATUS));
		detailsTypeI.setCompanyId(getAlphaNumeric1To3(airlineCode));
		AlphaNumericString_Length1To70[] freeTexts = new AlphaNumericString_Length1To70[1];
		freeTexts[0] = getAlpha1To70(contact);
		detailsTypeI.setFreetext(freeTexts);
		ssr.setSsr(detailsTypeI);
		elementDiv.setServiceRequest(ssr);
		dataElementDiv.add(elementDiv);
		divCount++;
	}

	public void buildReceivedFrom(String bookingCode) {
		DataElementsIndiv_type0 elementDiv = new DataElementsIndiv_type0();
		ElementManagementSegmentType segmentType =
				buildElementManagement(divCount.toString(), null, END_TRANSACT_SEGMENT_NAME);
		elementDiv.setElementManagementData(segmentType);
		LongFreeTextType longFreeTextType = new LongFreeTextType();
		FreeTextQualificationType textQualification = new FreeTextQualificationType();
		textQualification.setSubjectQualifier(getAlphaNumeric1To3("3"));
		textQualification.setType(getAlphaString1To4(BOOKING_ADDRESS_TICKET_TYPE));
		longFreeTextType.setLongFreetext(getAlpha1To199(bookingCode));
		longFreeTextType.setFreetextDetail(textQualification);
		elementDiv.setFreetextData(longFreeTextType);
		dataElementDiv.add(elementDiv);
		divCount++;
	}

	public void addRetenionPeriod(Integer retenionPeriod, PNR_AddMultiElements ssrElements) {
		List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentInfos);
		OriginDestinationDetail_type0[] originDestionation = new OriginDestinationDetail_type0[tripInfos.size()];
		int index = 0;
		for (TripInfo tripInfo : tripInfos) {
			originDestionation[index] = new OriginDestinationDetail_type0();
			originDestionation[index].setOriginDestination(buildOriginDestinationInfo(tripInfo));
			originDestionation[index].setItineraryInfo(buildItineraryInfo(retenionPeriod, tripInfo));
			index++;
		}
		ssrElements.setOriginDestinationDetail(originDestionation);
	}

	private ItineraryInfo_type0[] buildItineraryInfo(Integer plusDays, TripInfo tripInfo) {
		ItineraryInfo_type0[] itineraryInfos = new ItineraryInfo_type0[1];
		itineraryInfos[0] = new ItineraryInfo_type0();
		itineraryInfos[0].setElementManagementItinerary(
				buildElementManagement(divCount.toString(), OTHER_ELEMENT_TATOO_REF, RETENSION_NAME));
		AirAuxItinerary_type0 airAuxItinerary = new AirAuxItinerary_type0();
		airAuxItinerary.setMessageAction(buildMessageAction());
		airAuxItinerary.setTravelProduct(buildTravelProduct(plusDays, tripInfo));
		airAuxItinerary.setRelatedProduct(buildRelatedProduct(tripInfo));
		itineraryInfos[0].setAirAuxItinerary(airAuxItinerary);
		divCount++;
		return itineraryInfos;
	}

	private RelatedProductInformationTypeI buildRelatedProduct(TripInfo tripInfo) {
		RelatedProductInformationTypeI relatedProductInformationTypeI = new RelatedProductInformationTypeI();
		relatedProductInformationTypeI.setQuantity(getNumericLen1To2(Integer.valueOf(paxCountWithOutInf).toString()));
		relatedProductInformationTypeI.setStatus(getAplha1to2(SSR_STATUS));
		return relatedProductInformationTypeI;
	}

	private TravelProductInformationType buildTravelProduct(Integer plusDays, TripInfo tripInfo) {
		TravelProductInformationType travelProduct = new TravelProductInformationType();
		ProductDateTimeTypeI productDateTime = new ProductDateTimeTypeI();
		com.amadeus.xml.pnradd_15_1_1a.CompanyIdentificationTypeI companyInfo =
				new com.amadeus.xml.pnradd_15_1_1a.CompanyIdentificationTypeI();
		LocationTypeI boradLocationTypeI = new LocationTypeI();
		boradLocationTypeI.setCityCode(getAlphaNumeric1To5(tripInfo.getDepartureAirportCode()));
		travelProduct.setBoardpointDetail(boradLocationTypeI);
		LocationTypeI offLocationTypeI = new LocationTypeI();
		offLocationTypeI.setCityCode(getAlphaNumeric1To5(tripInfo.getArrivalAirportCode()));
		travelProduct.setOffpointDetail(offLocationTypeI);
		Date_DDMMYY date_ddmmyy = new Date_DDMMYY();
		LocalDateTime depatureTime = tripInfo.getDepartureTime();
		depatureTime = depatureTime.plusDays(plusDays);
		date_ddmmyy.setDate_DDMMYY(AmadeusUtils.formatToDDMMYY(depatureTime.toLocalDate()));
		productDateTime.setDepDate(date_ddmmyy);
		travelProduct.setProduct(productDateTime);
		companyInfo.setIdentification(getAlphaNumeric1To3(RETENSION_IDETIFICATION));
		travelProduct.setCompany(companyInfo);
		return travelProduct;
	}

	private MessageActionDetailsTypeI buildMessageAction() {
		MessageActionDetailsTypeI messageAction = new MessageActionDetailsTypeI();
		MessageFunctionBusinessDetailsTypeI businessDetails = new MessageFunctionBusinessDetailsTypeI();
		businessDetails.setFunction(getAlphaNumeric1To3(RETENSION_BUSINESS_CODE));
		messageAction.setBusiness(businessDetails);
		return messageAction;
	}

	private OriginAndDestinationDetailsTypeI buildOriginDestinationInfo(TripInfo tripInfo) {
		OriginAndDestinationDetailsTypeI originAndDestination = new OriginAndDestinationDetailsTypeI();
		originAndDestination.setOrigin(getAplha3To3(tripInfo.getDepartureAirportCode()));
		originAndDestination.setDestination(getAplha3To3(tripInfo.getArrivalAirportCode()));
		return originAndDestination;
	}

	public AlphaNumericString_Length1To5 getAlphaNumeric1To5(String code) {
		AlphaNumericString_Length1To5 alphaNumeric = new AlphaNumericString_Length1To5();
		alphaNumeric.setAlphaNumericString_Length1To5(code);
		return alphaNumeric;
	}

	public AlphaNumericString_Length1To3 getAlphaNumeric1To3(String code) {
		AlphaNumericString_Length1To3 alphaNumeric = new AlphaNumericString_Length1To3();
		alphaNumeric.setAlphaNumericString_Length1To3(code);
		return alphaNumeric;
	}

	public AlphaNumericString_Length1To57 getAlphaNumeric1To57(String code) {
		AlphaNumericString_Length1To57 alphaNumeric = new AlphaNumericString_Length1To57();
		alphaNumeric.setAlphaNumericString_Length1To57(code);
		return alphaNumeric;
	}

	public NumericInteger_Length1To2 getNumericLen1To2(String code) {
		NumericInteger_Length1To2 numericCode = new NumericInteger_Length1To2();
		numericCode.setNumericInteger_Length1To2(new BigInteger(code));
		return numericCode;
	}

	public AlphaNumericString_Length1To56 getAlphaString1To56(String code) {
		AlphaNumericString_Length1To56 alphaNumeric = new AlphaNumericString_Length1To56();
		alphaNumeric.setAlphaNumericString_Length1To56(code);
		return alphaNumeric;
	}

	public AlphaNumericString_Length1To1 getAplhaNumeric1To1(String code) {
		AlphaNumericString_Length1To1 alpha1To1 = new AlphaNumericString_Length1To1();
		alpha1To1.setAlphaNumericString_Length1To1(code);
		return alpha1To1;
	}

	public AlphaNumericString_Length1To8 getAplha1To8(String code) {
		AlphaNumericString_Length1To8 alpha1To8 = new AlphaNumericString_Length1To8();
		alpha1To8.setAlphaNumericString_Length1To8(code);
		return alpha1To8;
	}

	public AlphaString_Length2To2 getAlpha2To2(String code) {
		AlphaString_Length2To2 aplha2To2 = new AlphaString_Length2To2();
		aplha2To2.setAlphaString_Length2To2(code);
		return aplha2To2;
	}

	public NumericDecimal_Length1To5 getNumeric1To5(String code) {
		NumericDecimal_Length1To5 numeric = new NumericDecimal_Length1To5();
		numeric.setNumericDecimal_Length1To5(new BigDecimal(code));
		return numeric;
	}

	public AlphaNumericString_Length1To4 getAlphaString1To4(String code) {
		AlphaNumericString_Length1To4 length1To4 = new AlphaNumericString_Length1To4();
		length1To4.setAlphaNumericString_Length1To4(code);
		return length1To4;
	}

	public AlphaNumericString_Length1To199 getAlpha1To199(String text) {
		AlphaNumericString_Length1To199 length1To199 = new AlphaNumericString_Length1To199();
		int length = text.length() > 199 ? 199 : text.length();
		length1To199.setAlphaNumericString_Length1To199(text.substring(0, length));
		return length1To199;
	}

	public AlphaNumericString_Length2To3 getAlpha2To3(String code) {
		AlphaNumericString_Length2To3 length2To3 = new AlphaNumericString_Length2To3();
		length2To3.setAlphaNumericString_Length2To3(code);
		return length2To3;
	}

	public AlphaNumericString_Length1To25 getAlpha2To25(String code) {
		AlphaNumericString_Length1To25 length2To25 = new AlphaNumericString_Length1To25();
		int length = code.length() > 25 ? 25 : code.length();
		length2To25.setAlphaNumericString_Length1To25(code.substring(0, length));
		return length2To25;
	}

	public AlphaNumericString_Length1To2 getAlpha1To2(String code) {
		AlphaNumericString_Length1To2 length2To25 = new AlphaNumericString_Length1To2();
		int length = code.length() > 2 ? 2 : code.length();
		length2To25.setAlphaNumericString_Length1To2(code.substring(0, length));
		return length2To25;
	}

	public AlphaNumericString_Length1To14 getAlpha1To14(String code) {
		AlphaNumericString_Length1To14 length1To14 = new AlphaNumericString_Length1To14();
		int length = code.length() > 14 ? 14 : code.length();
		length1To14.setAlphaNumericString_Length1To14(code.substring(0, length));
		return length1To14;
	}

	public AlphaNumericString_Length1To35 getAlpha1To35(String code) {
		AlphaNumericString_Length1To35 length1To35 = new AlphaNumericString_Length1To35();
		int length = code.length() > 35 ? 35 : code.length();
		length1To35.setAlphaNumericString_Length1To35(code.substring(0, length));
		return length1To35;
	}

	public AlphaNumericString_Length1To10 getAlpha1To10(String code) {
		AlphaNumericString_Length1To10 length1To10 = new AlphaNumericString_Length1To10();
		int length = code.length() > 10 ? 10 : code.length();
		length1To10.setAlphaNumericString_Length1To10(code.substring(0, length));
		return length1To10;
	}

	public NumericInteger_Length1To3 getNumeric1To3(String code) {
		NumericInteger_Length1To3 length1To3 = new NumericInteger_Length1To3();
		int length = code.length() > 3 ? 3 : code.length();
		length1To3.setNumericInteger_Length1To3(new BigInteger(code.substring(0, length)));
		return length1To3;
	}

	public AlphaNumericString_Length1To70 getAlpha1To70(String code) {
		AlphaNumericString_Length1To70 length1To70 = new AlphaNumericString_Length1To70();
		int length = code.length() > 70 ? 70 : code.length();
		length1To70.setAlphaNumericString_Length1To70(code.substring(0, length));
		return length1To70;
	}

	public com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To3 getAlpha1To3(String code) {
		com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To3 length1To3 =
				new com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To3();
		int length = code.length() > 3 ? 3 : code.length();
		length1To3.setAlphaNumericString_Length1To3(code.substring(0, length));
		return length1To3;
	}

	public com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To25 getAlpha1To25(String code) {
		com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To25 length1To25 =
				new com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To25();
		int length = code.length() > 25 ? 25 : code.length();
		length1To25.setAlphaNumericString_Length1To25(code.substring(0, length));
		return length1To25;
	}

	public com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To10 getAlphaNumeric1To10(String code) {
		com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To10 length1To10 =
				new com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To10();
		int length = code.length() > 10 ? 10 : code.length();
		length1To10.setAlphaNumericString_Length1To10(code.substring(0, length));
		return length1To10;
	}

	public AlphaNumericString_Length1To60 getAlpha1To60(String code) {
		AlphaNumericString_Length1To60 length1To60 = new AlphaNumericString_Length1To60();
		int length = code.length() > 60 ? 60 : code.length();
		length1To60.setAlphaNumericString_Length1To60(code.substring(0, length));
		return length1To60;
	}

	public com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To35 getAlphaNumeric1To35(String code) {
		com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To35 length1To60 =
				new com.amadeus.xml.tpcbrq_15_1_1a.AlphaNumericString_Length1To35();
		int length = code.length() > 35 ? 35 : code.length();
		length1To60.setAlphaNumericString_Length1To35(code.substring(0, length));
		return length1To60;
	}

	public com.amadeus.xml.tautcq_04_1_1a.AlphaNumericString_Length1To3 getTSTAlphaNumeric1To3(String code) {
		com.amadeus.xml.tautcq_04_1_1a.AlphaNumericString_Length1To3 length1To3 =
				new com.amadeus.xml.tautcq_04_1_1a.AlphaNumericString_Length1To3();
		int length = code.length() > 3 ? 3 : code.length();
		length1To3.setAlphaNumericString_Length1To3(code.substring(0, length));
		return length1To3;
	}

	public com.amadeus.xml.tautcq_04_1_1a.NumericInteger_Length1To5 getTSTNumeric1To5(String code) {
		com.amadeus.xml.tautcq_04_1_1a.NumericInteger_Length1To5 length1To5 =
				new com.amadeus.xml.tautcq_04_1_1a.NumericInteger_Length1To5();
		int length = code.length() > 5 ? 5 : code.length();
		Integer tst = Integer.valueOf(code.substring(0, length));
		length1To5.setNumericInteger_Length1To5(BigInteger.valueOf(tst));
		return length1To5;
	}

	public AlphaNumericString_Length1To19 getAlpha1To19(String code) {
		AlphaNumericString_Length1To19 length1To19 = new AlphaNumericString_Length1To19();
		int length = code.length() > 19 ? 19 : code.length();
		length1To19.setAlphaNumericString_Length1To19(code.substring(0, length));
		return length1To19;
	}

	public com.amadeus.xml.ttktiq_15_1_1a.AlphaNumericString_Length1To3 getDIAlpha1To3(String code) {
		com.amadeus.xml.ttktiq_15_1_1a.AlphaNumericString_Length1To3 length1To3 =
				new com.amadeus.xml.ttktiq_15_1_1a.AlphaNumericString_Length1To3();
		int length = code.length() > 3 ? 3 : code.length();
		length1To3.setAlphaNumericString_Length1To3(code.substring(0, length));
		return length1To3;
	}

	public AlphaString_Length3To3 getAplha3To3(String code) {
		AlphaString_Length3To3 length3To3 = new AlphaString_Length3To3();
		length3To3.setAlphaString_Length3To3(code);
		return length3To3;
	}

	public AlphaString_Length1To2 getAplha1to2(String code) {
		AlphaString_Length1To2 length1To2 = new AlphaString_Length1To2();
		length1To2.setAlphaString_Length1To2(code);
		return length1To2;
	}
}

