package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import com.amadeus.xml.AmadeusCancelWebServicesStub;
import com.amadeus.xml.pnracc_15_1_1a.PNR_Reply;
import com.amadeus.xml.pnrxcl_15_1_1a.AMA_EDICodesetType_Length1;
import com.amadeus.xml.pnrxcl_15_1_1a.AMA_EDICodesetType_Length1To3;
import com.amadeus.xml.pnrxcl_15_1_1a.AlphaNumericString_Length1To20;
import com.amadeus.xml.pnrxcl_15_1_1a.CancelPNRElementType;
import com.amadeus.xml.pnrxcl_15_1_1a.ElementIdentificationType;
import com.amadeus.xml.pnrxcl_15_1_1a.NumericInteger_Length1To3;
import com.amadeus.xml.pnrxcl_15_1_1a.NumericInteger_Length1To5;
import com.amadeus.xml.pnrxcl_15_1_1a.OptionalPNRActionsType;
import com.amadeus.xml.pnrxcl_15_1_1a.PNR_Cancel;
import com.amadeus.xml.pnrxcl_15_1_1a.ReservationControlInformationDetailsTypeI;
import com.amadeus.xml.pnrxcl_15_1_1a.ReservationControlInformationType;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
final class AmadeusCancellationManager extends AmadeusServiceManager {


	protected AmadeusCancelWebServicesStub cancelStub;

	public boolean cancelPnr(String pnr) {
		boolean isSuccess = false;
		try {
			criticalMessageLogger = new ArrayList<>();
			listener.setType("PNR_Cancel");
			cancelStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			PNR_Reply cancelResponse = cancelStub.pNR_Cancel(getPnrCancelRequest(pnr), getSessionSchema());
			if (checkPNRReplyErrorMessage(cancelResponse)) {
				throw new NoPNRFoundException(String.join(",", criticalMessageLogger));
			} else {
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			cancelStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	private PNR_Cancel getPnrCancelRequest(String pnr) {
		PNR_Cancel pnrCancel = new PNR_Cancel();
		pnrCancel.setReservationInfo(getReservationInfo(pnr));
		pnrCancel.setPnrActions(getPnrActions());
		pnrCancel.setCancelElements(getCancelElements());
		return pnrCancel;
	}

	private ReservationControlInformationType getReservationInfo(String pnr) {
		ReservationControlInformationType reservationInfo = new ReservationControlInformationType();
		reservationInfo.setReservation(getReservationDetails(pnr));
		return reservationInfo;
	}

	private ReservationControlInformationDetailsTypeI getReservationDetails(String pnr) {
		ReservationControlInformationDetailsTypeI reservation = new ReservationControlInformationDetailsTypeI();
		reservation.setControlNumber(getControlNumber(pnr));
		return reservation;
	}

	private AlphaNumericString_Length1To20 getControlNumber(String pnr) {
		AlphaNumericString_Length1To20 number = new AlphaNumericString_Length1To20();
		number.setAlphaNumericString_Length1To20(pnr);
		return number;
	}

	private CancelPNRElementType[] getCancelElements() {
		CancelPNRElementType[] cancelElements = new CancelPNRElementType[1];
		cancelElements[0] = new CancelPNRElementType();
		// cancelElements[0].setElement(getElement());
		cancelElements[0].setEntryType(getEntryType());
		return cancelElements;
	}

	private AMA_EDICodesetType_Length1 getEntryType() {
		AMA_EDICodesetType_Length1 entryType = new AMA_EDICodesetType_Length1();
		entryType.setAMA_EDICodesetType_Length1("I");
		return entryType;
	}

	private ElementIdentificationType[] getElement() {
		ElementIdentificationType[] element = new ElementIdentificationType[1];
		element[0] = new ElementIdentificationType();
		element[0].setIdentifier(getElementIdentifier());
		element[0].setNumber(getElementnumber());
		return element;
	}

	private NumericInteger_Length1To5 getElementnumber() {
		NumericInteger_Length1To5 number = new NumericInteger_Length1To5();
		number.setNumericInteger_Length1To5(new BigInteger("8"));
		return number;
	}

	private AMA_EDICodesetType_Length1To3 getElementIdentifier() {
		AMA_EDICodesetType_Length1To3 identifer = new AMA_EDICodesetType_Length1To3();
		identifer.setAMA_EDICodesetType_Length1To3("OT");
		return identifer;
	}

	private OptionalPNRActionsType getPnrActions() {
		OptionalPNRActionsType pnrActions = new OptionalPNRActionsType();
		pnrActions.setOptionCode(getOptioncodfe());
		return pnrActions;
	}

	private NumericInteger_Length1To3[] getOptioncodfe() {
		NumericInteger_Length1To3[] optionCode = new NumericInteger_Length1To3[1];
		optionCode[0] = new NumericInteger_Length1To3();
		optionCode[0].setNumericInteger_Length1To3(new BigInteger("10"));
		return optionCode;
	}


}
