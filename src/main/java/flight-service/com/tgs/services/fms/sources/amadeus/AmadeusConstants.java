package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AmadeusConstants {

	protected static final BigInteger NUM_OF_REC = BigInteger.valueOf(100);

	public static final String PRIVATE_FARE_TYPE = "RU";
	public static final String CORPORATE_FARE_TYPE = "RW";
	public static final String PUBLISHED_FARE_TYPE = "RP";
	public static final String CHEAPEST_FARE_TYPE = "RLO";
	public static final String FARE_CURRENCY_OVERRIDE = "FCO";

	public static final String VALIDATING_CARRIER = "VC";
	public static final String DELIM = "-";

	public static final Integer TWO_THOUSAND = 2000;
	public static final Integer NINTEEN_HUNDRED = 1900;
	public static final Integer FIFTEEN_HUNDRED = 1500;

	public static final String PIECES = "N";
	public static final String KILOGRAMS = "700";
	public static final String KILOGRAM = "KG";
	public static final String PIECE = "PC";

	public static final String RECEIVED_FROM = "RF";
	public static final String RECEIVED_QULIFICATION = "P22";
	public static final String TOTAL_FARE_INFO = "712";

	public static final String TRAVEL_DOCUMENT_CODE = "DOCS";
	public static final String ACTION = "HK";

	protected static final String EMAIL_ADDRESS_TICKET_TYPE = "P02";
	protected static final String MOBILE_PHONE_CONTACT = "7";

	protected static final String GST_NUMBER = "GSTN";
	protected static final String GST_EMAIL = "GSTE";
	protected static final String GST_ADDRESS = "GSTA";
	protected static final String GST_CONTACT_NUMBER = "GSTP";
	protected static final String SEGMENT_REFERENCE = "ST";
	protected static final String PASSENGER_REFERENCE = "PT";
	protected static final String FREQUENT_FLIER_INFORMATION_ELEMENT = "FQTV";


	public static final String CASH_IDENTIFICATION = "CA";

	public static final String DOB_QUALIFIER = "706";

	public static final String ECONOMY_CLASS = "M";
	public static final String ECONOMY_STANDARD = "M";
	public static final String PREMIUM_ECONOMY = "W";
	public static final String BUSINESS = "C";
	public static final String FIRST_CLASS = "F";

	// Mini fare rules standalone
	public static final String REFERENCE_TYPE = "FRN";
	public static final String ALL_RECOMMENDATIONS = "ALL";
	public static final String BEFORE_DEPARTURE = "BDA";
	public static final String BEFORE_DEPARTURE_NO_SHOW = "BNA";
	public static final String AFTER_DEPARTUE = "ADA";
	public static final String AFTER_DEPARTURE_NO_SHOW = "ANA";

	public static final String FARE_TIME_LIMIT_FORMAT = "ddMMyyyy HHmm";

	protected static final String SPECIAL_SERVICE_REQUEST = "SSR";
	protected static final String PNR_ADD_DETAILS_CODE = "0";

	// TimeLimit
	public static final List<String> TIME_LIMIT_TAGS = Arrays.asList("ADTK", "OTHS", "OPW");

	public static List<String> ECONOMY_CLASS_LIST =
			Arrays.asList("Y", "V", "U", "M", "B", "H", "K", "L", "N", "Q", "O", "S", "X");
	public static List<String> PREMIUM_ECONOMY_CLASS_LIST = Arrays.asList("W", "E");
	public static List<String> BUSINESS_CLASS_LIST = Arrays.asList("C", "D", "R", "I", "Z");
	public static List<String> PREMIUM_BUSINESS_CLASS_LIST = Arrays.asList("J");
	public static List<String> FIRST_CLASS_LIST = Arrays.asList("F", "A");
	public static List<String> PREMIUM_FIRST_CLASS_LIST = Arrays.asList("P");
	public static List<String> SSR_REJECT_LIST = Arrays.asList("UN", "NO");

	public static HashMap<String, String> FARERULE_CATEGORY = new HashMap<String, String>() {
		{
			put("(1)", "Eligibility");
			put("(10)", "Combinations");
			put("(11)", "Blackout dates");
			put("(12)", "Surcharge");
			put("(13)", "Accompanied Travel");
			put("(14)", "Travel restrictions");
			put("(15)", "Sales restrictions");
			put("(16)", "Penalties");
			put("(17)", "Higher intermediate point");
			put("(18)", "Ticket endorsement");
			put("(19)", "Children discount");
			put("(2)", "Day/Time");
			put("(20)", "Tour conductor discount");
			put("(21)", "Agent discount");
			put("(22)", "All other discounts");
			put("(23)", "Miscellaneous Provisions");
			put("(25)", "Fare by Rule");
			put("(26)", "Group");
			put("(27)", "Tours");
			put("(28)", "Visit an other country");
			put("(29)", "Deposit");
			put("(3)", "Seasonability");
			put("(31)", "Reissue (voluntary changes)");
			put("(35)", "Nego");
			put("(4)", "Flight applications");
			put("(5)", "Advance Purchase");
			put("(50)", "Rule application");
			put("(6)", "Minimum Stay");
			put("(7)", "Maximum Stay");
			put("(8)", "Stopovers");
			put("(9)", "Transfer");
		}
	};

}
