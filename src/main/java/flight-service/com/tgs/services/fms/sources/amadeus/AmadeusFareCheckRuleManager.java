package com.tgs.services.fms.sources.amadeus;

import java.rmi.RemoteException;
import org.apache.axis2.AxisFault;
import org.apache.commons.lang3.StringUtils;
import com.amadeus.xml.AmadeusReviewWebServicesStub;
import com.amadeus.xml.farqnq_07_1_1a.FareRule_type0;
import com.amadeus.xml.farqnq_07_1_1a.Fare_CheckRules;
import com.amadeus.xml.farqnq_07_1_1a.ItemNumberDetails_type0;
import com.amadeus.xml.farqnq_07_1_1a.ItemNumber_type0;
import com.amadeus.xml.farqnq_07_1_1a.MessageFunctionDetails_type3;
import com.amadeus.xml.farqnq_07_1_1a.MessageFunction_type7;
import com.amadeus.xml.farqnq_07_1_1a.MsgType_type0;
import com.amadeus.xml.farqnq_07_1_1a.Number_type1;
import com.amadeus.xml.farqnq_07_1_1a.RuleSectionId_type1;
import com.amadeus.xml.farqnq_07_1_1a.TarifFareRule_type0;
import com.amadeus.xml.farqnr_07_1_1a.FareRuleText_type0;
import com.amadeus.xml.farqnr_07_1_1a.Fare_CheckRulesReply;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class AmadeusFareCheckRuleManager extends AmadeusServiceManager {

	protected AmadeusReviewWebServicesStub servicesStub;

	protected TripInfo selectedTrip;

	protected String bookingId;

	public TripFareRule getFareCheckRules(TripInfo selectedTrip) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		tripFareRule.getFareRule().put(FareRuleManager.getRuleKey(selectedTrip), getFareRuleInfo());
		return tripFareRule;
	}

	private FareRuleInformation getFareRuleInfo() {
		FareRuleInformation fareRuleInformation = new FareRuleInformation();
		try {
			listener.setType("FareCheckRules");
			Fare_CheckRules request = buildFareCheckRuleRq();
			Fare_CheckRulesReply reply = servicesStub.fare_CheckRules(request, getSessionSchema());
			fareRuleInformation.getMiscInfo().put(getFareRuleCategory(reply), formatText2(reply));
			return fareRuleInformation;
		} catch (AxisFault af) {
			throw new SupplierUnHandledFaultException(af.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			servicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private String getFareRuleCategory(Fare_CheckRulesReply reply) {
		String category = StringUtils.EMPTY;
		if (reply != null && reply.getTariffInfo() != null && reply.getTariffInfo()[0].getFareRuleInfo() != null) {
			category = AmadeusConstants.FARERULE_CATEGORY
					.get(reply.getTariffInfo()[0].getFareRuleInfo().getRuleCategoryCode().getRuleCategoryCode_type0());
		}
		if (StringUtils.isEmpty(category)) {
			category = "Conditions";
		}
		return category;
	}

	private String formatText2(Fare_CheckRulesReply reply) {
		String text = "";
		FareRuleText_type0[] fareRule = reply.getTariffInfo()[0].getFareRuleText();
		for (FareRuleText_type0 rule : fareRule) {
			text = text.concat(rule.getFreeText()[0].getFreeText_type12());
		}
		return text.trim().replaceAll(" +", " ");
	}

	private Fare_CheckRules buildFareCheckRuleRq() {
		Fare_CheckRules request = new Fare_CheckRules();
		request.setMsgType(getMessageType());
		request.setItemNumber(getItem());
		request.setFareRule(getfareRule());
		return request;
	}

	private FareRule_type0 getfareRule() {
		FareRule_type0 fareRule = new FareRule_type0();
		fareRule.setTarifFareRule(getTarifRule());
		return fareRule;
	}

	private TarifFareRule_type0 getTarifRule() {
		TarifFareRule_type0 tarifRule = new TarifFareRule_type0();
		tarifRule.setRuleSectionId(getRuleSectionId());
		return tarifRule;
	}

	private RuleSectionId_type1[] getRuleSectionId() {
		RuleSectionId_type1[] ruleSectionId = new RuleSectionId_type1[1];
		ruleSectionId[0] = new RuleSectionId_type1();
		ruleSectionId[0].setRuleSectionId_type0("PE");
		return ruleSectionId;
	}

	private ItemNumber_type0 getItem() {
		ItemNumber_type0 itemNumber = new ItemNumber_type0();
		itemNumber.setItemNumberDetails(getItemDetails());
		return itemNumber;
	}

	private ItemNumberDetails_type0[] getItemDetails() {
		ItemNumberDetails_type0[] itemDetails = new ItemNumberDetails_type0[1];
		itemDetails[0] = new ItemNumberDetails_type0();
		itemDetails[0].setNumber(getINumber());
		return itemDetails;
	}

	/*
	 * private Type_type1 getItype() { Type_type1 type = new Type_type1(); type.setType_type0("FC"); return type; }
	 */


	private Number_type1 getINumber() {
		Number_type1 number = new Number_type1();
		number.setNumber_type0("1");
		return number;
	}

	private MsgType_type0 getMessageType() {
		MsgType_type0 msgType = new MsgType_type0();
		msgType.setMessageFunctionDetails(getMessageFunctionDetails());
		return msgType;
	}

	private MessageFunctionDetails_type3 getMessageFunctionDetails() {
		MessageFunctionDetails_type3 messageFunction = new MessageFunctionDetails_type3();
		messageFunction.setMessageFunction(getMsgFunction());
		return messageFunction;
	}

	private MessageFunction_type7 getMsgFunction() {
		MessageFunction_type7 type = new MessageFunction_type7();
		type.setMessageFunction_type6("712");
		return type;
	}

}
