package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.AmadeusWebServicesStub;
import com.amadeus.xml.fmptbq_14_3_1a.*;
import com.amadeus.xml.fmptbr_14_3_1a.*;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.axiom.om.DeferredParsingException;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import org.apache.axiom.om.OMException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import com.amadeus.xml.fmptbr_14_3_1a.Fare_MasterPricerTravelBoardSearchReply;

@Slf4j
@Getter
@Setter
@SuperBuilder
final class AmadeusMasterPriceSearchManager extends AmadeusServiceManager {

	protected BigInteger NO_OF_REC;

	protected Map<CabinClass, Integer> fareFamily;

	public static final String TICKET_AVAILABILITY_CHECK = "TAC";
	public static final String ETICKET_ITINERARY = "ET"; // "ITR";
	public static final String CURRENCY_CONVERSION = "CUC";

	public static final String SEGMENT_QUALIFIER = "S";
	public static final String DURATION_SEGMENT_QUALIFIER = "EFT";
	public static final String PREFERED_AIRLINE_QUALIFIER = "M";
	public static final String MINI_RULE_QUALIFIER = "M";
	public static final String PLATING_CARRIER = "V";
	public static final String BAGGAGE_QUALIFIER = "B";
	public static final String MINIRULE_QUALIFIER_TAG = "MNR";
	public static final String FREE_BAGGAGE_ALLOWANCE_QUALIFIER = "FBA";

	private static final List<String> MINI_RULE = Arrays.asList("BDJ");
	protected final String ALLOWED_ACTION = "1";
	protected final String MINI_CONFIRM_CODE = "PEN";
	protected static final String INFANT_INDICATOR = "1";
	protected static final String PAX_UNIT = "PX";
	protected static final String RECOMMENDATION_COUNT = "RC";

	protected AmadeusSessionManager sessionManager;
	protected boolean isCabinRequest;


	public AirSearchResult doMPTBSearch() {
		AirSearchResult searchResult = new AirSearchResult();
		Fare_MasterPricerTravelBoardSearch searchRequest = null;
		Fare_MasterPricerTravelBoardSearchReply searchResponse = null;
		String sessionToken = bindingService.fetchSessionToken(true);
		int retryCount = 1;
		boolean isMasterPriceSuccess = false;
		while (retryCount <= 2 && !isMasterPriceSuccess) {
			try {
				log.debug("Session Token {} for search id {}", sessionToken, searchQuery.getSearchId());
				sessionManager.init(sessionToken);
				if (isSessionNotExists(sessionToken)) {
					sessionManager.openSession();
				}
				this.setSession(sessionManager.getSessionSchema());
				listener.setType(AirUtils.getLogType("MasterPricerTravelBoardSearch", supplierConfiguration));
				searchRequest = buildMPTBSearchRequest();
				webServicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				searchResponse = webServicesStub.fare_MasterPricerTravelBoardSearch(searchRequest, getSessionSchema());
				if (!checkSearchErrorMessage(searchResponse.getErrorMessage())) {
					searchResult = parseMPTBSearchResponse(searchResponse);
					isMasterPriceSuccess = true;
				}
			} catch (AxisFault af) {
				if (sessionManager.hasSessionExpired(af)) {
					isMasterPriceSuccess = false;
					sessionToken = null;
					sessionManager.setSession(null);
				} else {
					log.info("Search Failed for search {}", getSearchQuery().getSearchId(), af.getMessage());
					throw new NoSearchResultException(af.getMessage());
				}
			} catch (OMException om) {
				log.error("Search Failed for search {}", getSearchQuery().getSearchId(), om);
				throw new NoSearchResultException(om.getMessage());
			} catch (RemoteException re) {
				throw new SupplierRemoteException(re);
			} finally {
				retryCount++;
				webServicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
				if (sessionManager.getSessionSchema() != null
						&& StringUtils.isNotBlank(sessionManager.getSessionSchema().getSessionId())) {
					bindingService.setListner(listener);
					bindingService.storeSession(sessionManager.getSession(), sourceConfig, webServicesStub);
				}
				bindingService.cleanUp(webServicesStub);
				bindingService.storeSessionMetaInfo(supplierConfiguration, AmadeusWebServicesStub.class,
						webServicesStub);
			}
		}
		return searchResult;
	}

	private boolean isSessionNotExists(String sessionToken) {
		boolean isNotExists = false;
		if (StringUtils.isBlank(sessionToken) || sessionToken.equals(StringUtils.join(DELIM, DELIM, DELIM))
				|| sessionManager.getSessionSchema() != null && StringUtils.isBlank(sessionManager.getSessionId())) {
			isNotExists = true;
		}
		return isNotExists;
	}

	private AirSearchResult parseMPTBSearchResponse(Fare_MasterPricerTravelBoardSearchReply searchResponse) {
		AirSearchResult searchResult = new AirSearchResult();
		Recommendation_type0[] recommendations = searchResponse.getRecommendation();
		for (Recommendation_type0 recommendation : recommendations) {
			SegmentFlightRef_type0[] segmentFlightRefs = recommendation.getSegmentFlightRef();
			buildTripInfos(segmentFlightRefs, searchResponse, recommendation, searchResult);
		}
		return searchResult;
	}

	private AirSearchResult buildTripInfos(SegmentFlightRef_type0[] segmentFlightRefs,
			Fare_MasterPricerTravelBoardSearchReply searchResponse, Recommendation_type0 recommendation,
			AirSearchResult searchResult) {
		Map<Integer, ServiceCovInfoGrp_type0[]> baggageMap = null;
		Map<Integer, MnrDetails_type0> miniRules = null;
		baggageMap = createBaggageRefList(searchResponse.getServiceFeesGrp());
		miniRules = createMiniRules(searchResponse.getMnrGrp());

		for (SegmentFlightRef_type0 segmentFlightRef : segmentFlightRefs) {
			ReferencingDetail_type3[] referencingDetail = segmentFlightRef.getReferencingDetail();
			Integer tripIndex = 0;
			List<SegmentInfo> onwardSegmentInfos = new ArrayList<>();
			List<SegmentInfo> returnSegmentInfos = new ArrayList<>();
			try {
				for (ReferencingDetail_type3 reference : referencingDetail) {
					if (reference.getRefQualifier().getRefQualifier_type6().equalsIgnoreCase(SEGMENT_QUALIFIER)) {
						Integer refNumber = reference.getRefNumber().getRefNumber_type10().intValue();
						Integer baggageRefNumber = getReferenceNumber(referencingDetail, BAGGAGE_QUALIFIER);
						Integer miniRefNumber = getReferenceNumber(referencingDetail, MINI_RULE_QUALIFIER);
						Boolean refundableAction = getRefundableAction(miniRules, miniRefNumber);
						String platingCarrier = getPlatingCarrier(recommendation);
						List<SegmentInfo> refSegmentInfos = buildSegmentInfos(searchResponse, tripIndex, refNumber);
						setIsReturnSegment(refSegmentInfos, tripIndex);
						setPriceInfos(platingCarrier, recommendation, refSegmentInfos, tripIndex, refundableAction);
						if (baggageRefNumber != null && miniRefNumber != null) {
							setBaggageAllowance(baggageMap, baggageRefNumber, searchResponse.getServiceFeesGrp(),
									refSegmentInfos, tripIndex);
						}

						if (CollectionUtils.isNotEmpty(refSegmentInfos) && tripIndex == 1 && refNumber == 1
								&& searchQuery.isDomesticReturn()) {
							returnSegmentInfos.addAll(refSegmentInfos);
						} else {
							onwardSegmentInfos.addAll(refSegmentInfos);
						}
					}
					tripIndex++;
				}
			} catch (Exception e) {
				log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
			}
			if (searchQuery.isDomesticReturn() && CollectionUtils.isNotEmpty(onwardSegmentInfos)
					&& CollectionUtils.isNotEmpty(returnSegmentInfos)) {
				TripInfo onwardTripInfo = getTripInfo(onwardSegmentInfos);
				TripInfo returnTripInfo = getTripInfo(returnSegmentInfos);
				AmadeusUtils.splitFare(onwardTripInfo, returnTripInfo, searchQuery);
				addTripsToAirSearchReult(searchResult, TripInfoType.ONWARD, onwardTripInfo);
				addTripsToAirSearchReult(searchResult, TripInfoType.RETURN, returnTripInfo);
			} else if (!searchQuery.isDomesticReturn()) {
				TripInfo tripInfo = getTripInfo(onwardSegmentInfos);
				TripInfoType type = AmadeusUtils.getTripInfoType(searchQuery);
				addTripsToAirSearchReult(searchResult, type, tripInfo);
			}
		}
		return searchResult;
	}

	private Integer getReferenceNumber(ReferencingDetail_type3[] referencingDetails, String qualifier) {
		Integer refNumber = null;
		if (referencingDetails != null && ArrayUtils.isNotEmpty(referencingDetails)) {
			for (ReferencingDetail_type3 referencingDetail : referencingDetails) {
				if (referencingDetail.getRefQualifier() != null
						&& referencingDetail.getRefQualifier().getRefQualifier_type6().equalsIgnoreCase(qualifier)) {
					refNumber = referencingDetail.getRefNumber().getRefNumber_type10().intValue();
					break;
				}
			}
		}
		return refNumber;
	}

	private TripInfo getTripInfo(List<SegmentInfo> segmentInfos) {
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(segmentInfos);
		return tripInfo;
	}

	private void addTripsToAirSearchReult(AirSearchResult searchResult, TripInfoType tripInfoType, TripInfo tripInfo) {
		if (tripInfo != null && CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			AmadeusUtils.setTotalFareOnTripInfo(Arrays.asList(tripInfo));
			List<TripInfo> results =
					searchResult.getTripInfos().getOrDefault(tripInfoType.getName(), new ArrayList<>());
			results.add(tripInfo);
			searchResult.getTripInfos().put(tripInfoType.getName(), results);
		}
	}

	private Boolean getRefundableAction(Map<Integer, MnrDetails_type0> miniRules, Integer refNumber) {
		AtomicBoolean isAllowed = new AtomicBoolean();
		if (MapUtils.isNotEmpty(miniRules) && MapUtils.getObject(miniRules, refNumber) != null) {
			MnrDetails_type0 mnrDetail = miniRules.get(refNumber);
			for (CatGrp_type0 mnr : mnrDetail.getCatGrp()) {
				if (mnr.getCatInfo() != null && mnr.getCatInfo().getDescriptionInfo() != null) {
					String catDesc = mnr.getCatInfo().getDescriptionInfo().getNumber().getNumber_type28().toString();
					if (CANCELLATION_CAT.equalsIgnoreCase(catDesc)) {
						for (StatusInformation_type0 status : mnr.getStatusInfo().getStatusInformation()) {
							if (MINI_RULE.contains(status.getIndicator().getIndicator_type5())
									&& ALLOWED_ACTION.equals(status.getAction().getAction_type0())
									&& mnr.getMonInfo() != null && mnr.getMonInfo().getMonetaryDetails() != null) {
								Double amount = mnr.getMonInfo().getMonetaryDetails().getAmount().getAmount_type32()
										.doubleValue();
								if (amount != null && amount > 0) {
									isAllowed.set(Boolean.TRUE);
								}
							}
						}
					}
				}
			}
		}
		return isAllowed.get();
	}


	private Map<Integer, MnrDetails_type0> createMiniRules(MnrGrp_type0 mnrGrp) {
		Map<Integer, MnrDetails_type0> mnrGroups = new HashMap<>();
		if (mnrGrp != null && ArrayUtils.isNotEmpty(mnrGrp.getMnrDetails())) {
			MnrDetails_type0[] mnrDetails = mnrGrp.getMnrDetails();
			for (MnrDetails_type0 mnrDetail : mnrDetails) {
				Integer itemNumber = getItemNumber(mnrDetail);
				if (itemNumber != null) {
					mnrGroups.put(itemNumber, mnrDetail);
				}
			}
		}
		return mnrGroups;
	}

	private Integer getItemNumber(MnrDetails_type0 mnrDetail) {
		ItemNumberDetails_type2[] itemNumberIds = mnrDetail.getMnrRef().getItemNumberDetails();
		return ArrayUtils.isNotEmpty(itemNumberIds) ? Integer.valueOf(itemNumberIds[0].getNumber().getNumber_type26())
				: null;
	}

	private Map<Integer, ServiceCovInfoGrp_type0[]> createBaggageRefList(ServiceFeesGrp_type0[] serviceFeesGrp) {
		ServiceFeesGrp_type0 fbaGroup = getFreeBaggageServiceGroup(serviceFeesGrp);
		Map<Integer, ServiceCovInfoGrp_type0[]> baggageMap = new HashMap<>();
		AtomicInteger index = new AtomicInteger(0);
		for (ServiceCoverageInfoGrp_type0 serviceCoverageInfoGrp : fbaGroup.getServiceCoverageInfoGrp()) {
			baggageMap.put(index.get() + 1, serviceCoverageInfoGrp.getServiceCovInfoGrp());
			index.getAndIncrement();
		}
		return baggageMap;
	}

	private void setBaggageAllowance(Map<Integer, ServiceCovInfoGrp_type0[]> baggageMap, int baggageRefNumber,
			ServiceFeesGrp_type0[] serviceFeeGrps, List<SegmentInfo> parsedSegments, Integer segmentIndex) {
		BaggageInfo baggageInfo = null;
		ServiceFeesGrp_type0 serviceFeesGrp = getFreeBaggageServiceGroup(serviceFeeGrps);
		try {
			if (serviceFeesGrp != null) {
				ServiceCovInfoGrp_type0[] serviceCovInfoGrp = baggageMap.get(baggageRefNumber);
				if (ArrayUtils.isNotEmpty(serviceCovInfoGrp)) {
					ServiceCovInfoGrp_type0 segmentGroup = getSegmentCoverage(serviceCovInfoGrp, segmentIndex);
					if (segmentGroup != null) {
						Integer fbaReference = getFBAIndexCode(serviceCovInfoGrp, segmentIndex);
						List<Integer> allowedSegments = getAllowedLegs(segmentGroup);
						FreeBagAllownceInfo_type0 baggageType =
								serviceFeesGrp.getFreeBagAllowanceGrp()[fbaReference - 1].getFreeBagAllownceInfo();
						if (fbaReference != null && baggageType != null && baggageType.getBaggageDetails() != null) {
							BaggageDetails_type0 detailsType = null;
							try {
								detailsType = baggageType.getBaggageDetails();
								baggageInfo = new BaggageInfo();
								String quantityCode = detailsType.getQuantityCode().getQuantityCode_type0();
								Integer freeAllowance =
										detailsType.getFreeAllowance().getFreeAllowance_type0().intValue();
								String unit = detailsType.getUnitQualifier() != null
										? detailsType.getUnitQualifier().getUnitQualifier_type2()
										: StringUtils.EMPTY;
								baggageInfo
										.setAllowance(AmadeusUtils.baggageAllowance(quantityCode, freeAllowance, unit));
								setBaggageInfo(parsedSegments, allowedSegments, baggageInfo);
							} catch (Exception e) {
								log.error("Unable to parse baggage info from FBA map {} and details {} ", searchQuery,
										detailsType, e);
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse baggage info from FBA map {}", searchQuery, e);
		}

	}

	private List<Integer> getAllowedLegs(ServiceCovInfoGrp_type0 serviceCovInfoGrp) {
		List<Integer> allowedSegments = new ArrayList<>();
		if (serviceCovInfoGrp.getCoveragePerFlightsInfo() != null
				&& ArrayUtils.isNotEmpty(serviceCovInfoGrp.getCoveragePerFlightsInfo())) {
			for (CoveragePerFlightsInfo_type0 coverage : serviceCovInfoGrp.getCoveragePerFlightsInfo()) {
				if (ArrayUtils.isNotEmpty(coverage.getLastItemsDetails())) {
					for (LastItemsDetails_type0 lastItem : coverage.getLastItemsDetails()) {
						if (lastItem.getRefOfLeg() != null) {
							// segmentNum starts with 1 on supplier side
							Integer segmentNum = Integer.valueOf(lastItem.getRefOfLeg().getRefOfLeg_type0());
							allowedSegments.add(segmentNum - 1);
						}
					}
				}
			}
		}
		return allowedSegments;
	}

	private void setBaggageInfo(List<SegmentInfo> segmentInfos, List<Integer> segmentNums, BaggageInfo baggageInfo) {
		if (baggageInfo == null)
			return;
		segmentInfos.forEach(segmentInfo -> {
			if (segmentNums.contains(segmentInfo.getSegmentNum())) {
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
						if (!paxType.equals(PaxType.INFANT)) {
							fareDetail.setBaggageInfo(baggageInfo);
						}
					}));
				});
			}
		});
	}

	private boolean checkCoverage(ServiceCovInfoGrp_type0[] baggageList, Integer serviceRef) {
		AtomicBoolean isApplicable = new AtomicBoolean();
		for (ServiceCovInfoGrp_type0 serviceGrp : baggageList) {
			for (CoveragePerFlightsInfo_type0 coveragePerFlight : serviceGrp.getCoveragePerFlightsInfo()) {
				String coverageCode = coveragePerFlight.getNumberOfItemsDetails().getRefNum().getRefNum_type0();
				if (coverageCode.equalsIgnoreCase(String.valueOf(serviceRef))) {
					isApplicable.set(true);
				}
			}
		}
		return isApplicable.get();
	}

	private Integer getFBAIndexCode(ServiceCovInfoGrp_type0[] baggageList, int serviceRef) {
		if (serviceRef < baggageList.length) {
			return baggageList[serviceRef].getRefInfo().getReferencingDetail()[0].getRefNumber().getRefNumber_type20()
					.intValue();
		}
		return baggageList[0].getRefInfo().getReferencingDetail()[0].getRefNumber().getRefNumber_type20().intValue();
	}

	private ServiceCovInfoGrp_type0 getSegmentCoverage(ServiceCovInfoGrp_type0[] baggageList, int serviceRef) {
		if (ArrayUtils.isNotEmpty(baggageList)) {
			if (serviceRef < baggageList.length) {
				return baggageList[serviceRef];
			}
			return baggageList[0];
		}
		return null;
	}

	private ServiceFeesGrp_type0 getFreeBaggageServiceGroup(ServiceFeesGrp_type0[] serviceFeeGrps) {
		ServiceFeesGrp_type0 serviceFeesGrp = serviceFeeGrps[0];
		for (ServiceFeesGrp_type0 serviceFeesGrpTemp : serviceFeeGrps) {
			if (serviceFeesGrpTemp.getServiceTypeInfo().getCarrierFeeDetails().getType().getType_type12()
					.equals(FREE_BAGGAGE_ALLOWANCE_QUALIFIER)) {
				serviceFeesGrp = serviceFeesGrpTemp;
				break;
			}
		}
		return serviceFeesGrp;
	}

	private void setPriceInfos(String platingCarrier, Recommendation_type0 recommendation,
			List<SegmentInfo> refSegmentInfos, Integer tripIndex, Boolean refundableAction) {
		refSegmentInfos.forEach(segmentInfo -> {
			List<PriceInfo> priceInfos = new ArrayList<>();
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
			PriceMiscInfo miscInfo = priceInfo.getMiscInfo();
			Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
			if (StringUtils.isNotBlank(platingCarrier)) {
				miscInfo.setPlatingCarrier(AirlineHelper.getAirlineInfo(platingCarrier));
			}
			PaxFareProduct_type0[] paxProductFare = recommendation.getPaxFareProduct();
			for (PaxFareProduct_type0 paxFare : paxProductFare) {
				String paxCode = paxFare.getPaxReference()[0].getPtc()[0].getPtc_type0();
				PaxType paxType = getPaxType(paxCode);
				FareDetail fareDetail = priceInfo.getFareDetail(paxType, new FareDetail());
				FareDetails_type3 journeyFare = paxFare.getFareDetails()[tripIndex];
				GroupOfFares_type0 segmentFare = journeyFare.getGroupOfFares()[segmentInfo.getSegmentNum().intValue()];
				boolean isPrivateFare = isPrivateFare(segmentFare.getProductInformation().getFareProductDetail());
				miscInfo.setIsPrivateFare(isPrivateFare);
				setCorporateCode(segmentFare.getProductInformation(), priceInfo, miscInfo);
				setStaticFareDetail(fareDetail, segmentFare);
				fareDetail.setRefundableType(checkRefundPolicy(paxFare, refundableAction).getRefundableType());
				priceInfo.setFareIdentifier(
						AmadeusUtils.getFareType(segmentFare.getProductInformation(), searchQuery.isDomesticReturn()));
				if (segmentInfo.getSegmentNum() == 0 && tripIndex == 0) {
					Double baseFare = getBaseFare(paxFare);
					fareDetail.getFareComponents().put(FareComponent.BF, baseFare);
					fareDetail.getFareComponents().putAll(getTaxComponents(paxFare));
				} else {
					fareDetail.setFareComponents(new HashMap<>());
				}
				fareDetailMap.put(paxType, fareDetail);
			}
			priceInfo.setMiscInfo(miscInfo);
			priceInfo.setFareDetails(fareDetailMap);
			priceInfos.add(priceInfo);
			segmentInfo.setPriceInfoList(priceInfos);
		});
	}

	private RefundableType checkRefundPolicy(PaxFareProduct_type0 paxFare, Boolean refundableAction) {
		RefundableType refundableType = getDefaultRefundableType(refundableAction);
		if (ArrayUtils.isNotEmpty(paxFare.getFare())) {
			Fare_type0 fareDesc = getFareTypeDesc(paxFare.getFare());
			if (fareDesc != null) {
				// refundableType = updateRefundableType(fareDesc, refundableType);
			}
		}
		return refundableType;
	}

	private RefundableType updateRefundableType(Fare_type0 fareDesc, RefundableType refundableType) {
		if (ArrayUtils.isNotEmpty(fareDesc.getPricingMessage().getDescription())) {
			String informaticeCode = fareDesc.getPricingMessage().getFreeTextQualification().getInformationType()
					.getInformationType_type4();
			Description_type9[] fareDescFreeText = fareDesc.getPricingMessage().getDescription();
			String subjectQualifier = fareDesc.getPricingMessage().getFreeTextQualification().getTextSubjectQualifier()
					.getTextSubjectQualifier_type4();
			for (Description_type9 desc : fareDescFreeText) {
				if (subjectQualifier.equals(MINI_CONFIRM_CODE)) {
					String descMsg = desc.getDescription_type8().toUpperCase();
					if (informaticeCode.equals("70") && descMsg.contains("NON-REFUNDABLE")) {
						return RefundableType.NON_REFUNDABLE;
					} else if (informaticeCode.equals("73") && descMsg.contains("PENALTY APPLIES")) {
						return RefundableType.REFUNDABLE;
					}
				}
			}
		}
		return refundableType;
	}

	private Fare_type0 getFareTypeDesc(Fare_type0[] fare) {
		Fare_type0 fareType = null;
		for (Fare_type0 fare_type0 : fare) {
			if (fare_type0.getPricingMessage() != null
					&& fare_type0.getPricingMessage().getFreeTextQualification() != null
					&& fare_type0.getPricingMessage().getFreeTextQualification().getInformationType() != null) {
				fareType = fare_type0;
				break;
			}
		}
		return fareType;
	}

	private RefundableType getDefaultRefundableType(Boolean isRefundableAction) {
		RefundableType refundableType = RefundableType.NON_REFUNDABLE;
		if (isRefundableAction) {
			refundableType = RefundableType.REFUNDABLE;
		}
		return refundableType;
	}

	private Map<? extends FareComponent, ? extends Double> getTaxComponents(PaxFareProduct_type0 paxFare) {
		Map<FareComponent, Double> taxComponents = new HashMap<>();
		Map<String, GDSFareComponentMapper> gdsFares = GDSFareComponentMapper.getGdsFareComponents();
		if (paxFare.getPassengerTaxDetails() != null && paxFare.getPassengerTaxDetails().getTaxDetails() != null) {
			TaxDetails_type0[] taxDetails = paxFare.getPassengerTaxDetails().getTaxDetails();
			for (TaxDetails_type0 taxDetail : taxDetails) {
				String fareType = taxDetail.getType().getType_type8().trim();
				if (gdsFares.keySet().contains(fareType)) {
					taxComponents.put(gdsFares.get(fareType).getFareComponent(),
							(Double) ObjectUtils.defaultIfNull(
									taxComponents.get(gdsFares.get(fareType).getFareComponent()), new Double(0))
									+ getAmountBasedOnCurrency(Double.valueOf(taxDetail.getRate().getRate_type4()),
											getCurrencyCode()));
				} else {
					taxComponents.put(FareComponent.OC,
							(Double) ObjectUtils.defaultIfNull(taxComponents.get(FareComponent.OC), new Double(0))
									+ getAmountBasedOnCurrency(Double.valueOf(taxDetail.getRate().getRate_type4()),
											getCurrencyCode()));
				}
			}
		}
		return taxComponents;
	}

	private Double getBaseFare(PaxFareProduct_type0 paxFare) {
		return getAmountBasedOnCurrency(paxFare.getPaxFareDetail().getTotalFareAmount().getTotalFareAmount_type0()
				.subtract(paxFare.getPaxFareDetail().getTotalTaxAmount().getTotalTaxAmount_type0()).doubleValue(),
				getCurrencyCode());
	}

	private boolean isPrivateFare(FareProductDetail_type0 fareProductDetail) {
		AtomicBoolean isPrivate = new AtomicBoolean(true);
		if (ArrayUtils.isEmpty(fareProductDetail.getFareType())) {
			isPrivate.set(Boolean.FALSE);
		} else {
			FareType_type1[] fareTypes = fareProductDetail.getFareType();
			for (FareType_type1 fareType : fareTypes) {
				if (fareType.getFareType_type0().equalsIgnoreCase(AmadeusConstants.PUBLISHED_FARE_TYPE)) {
					isPrivate.set(false);
					break;
				}
			}
		}
		return isPrivate.get();
	}


	private String setCorporateCode(ProductInformation_type0 productInformation, PriceInfo priceInfo,
			PriceMiscInfo miscInfo) {
		String corporateCode = null;
		if (productInformation != null && ArrayUtils.isNotEmpty(productInformation.getCorporateId())) {
			corporateCode = productInformation.getCorporateId()[0].getCorporateId_type0();
			miscInfo.setAccountCode(corporateCode);
			priceInfo.setFareIdentifier(FareType.CORPORATE);
		}
		return corporateCode;
	}

	public void setStaticFareDetail(FareDetail fareDetail, GroupOfFares_type0 segmentFare) {
		fareDetail.setCabinClass(AmadeusUtils
				.getCabinClass(segmentFare.getProductInformation().getCabinProduct().getCabin().getCabin_type6()));
		fareDetail.setClassOfBooking(segmentFare.getProductInformation().getCabinProduct().getRbd().getRbd_type4());
		fareDetail.setFareBasis(
				segmentFare.getProductInformation().getFareProductDetail().getFareBasis().getFareBasis_type0());
		fareDetail.setSeatRemaining(Integer
				.valueOf(segmentFare.getProductInformation().getCabinProduct().getAvlStatus().getAvlStatus_type2()));
	}

	private void setIsReturnSegment(List<SegmentInfo> refSegmentInfos, Integer tripIndex) {
		if (tripIndex == 1 && (searchQuery.isIntlReturn() || searchQuery.isDomesticReturn())) {
			refSegmentInfos.forEach(segmentInfo -> {
				segmentInfo.setIsReturnSegment(true);
			});
		}
	}

	private String getPlatingCarrier(Recommendation_type0 recommendation) {
		String carrierCode = null;
		if (recommendation.getPaxFareProduct() != null && ArrayUtils.isNotEmpty(recommendation.getPaxFareProduct())) {
			PaxFareProduct_type0 paxFareProduct = recommendation.getPaxFareProduct()[0];
			CodeShareDetails_type1[] codeShareDetails = paxFareProduct.getPaxFareDetail().getCodeShareDetails();
			if (codeShareDetails != null && ArrayUtils.isNotEmpty(codeShareDetails)) {
				for (CodeShareDetails_type1 codeShare : codeShareDetails) {
					if (codeShare.getTransportStageQualifier() != null && codeShare.getTransportStageQualifier()
							.getTransportStageQualifier_type2().equalsIgnoreCase(PLATING_CARRIER)) {
						carrierCode = codeShare.getCompany().getCompany_type2();
					}
				}
			}
		}
		return carrierCode;
	}

	private List<SegmentInfo> buildSegmentInfos(Fare_MasterPricerTravelBoardSearchReply searchResponse,
			Integer segmentIndex, Integer refNumber) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		FlightIndex_type0[] flightsIndex = searchResponse.getFlightIndex();
		FlightIndex_type0 requestedFlight = null;
		if (ArrayUtils.isNotEmpty(flightsIndex)) {
			requestedFlight = getRequestedGroupFromFlightIndex(segmentIndex, flightsIndex);
			GroupOfFlights_type1 groupOfFlight =
					getRequestedFlightGroup(requestedFlight.getGroupOfFlights(), refNumber, segmentIndex);
			if (groupOfFlight != null) {
				PropFlightGrDetail_type0 propFlightGrDetail = groupOfFlight.getPropFlightGrDetail();
				FlightDetails_type1[] flightDetails = groupOfFlight.getFlightDetails();
				Integer segmentNum = 0;
				for (FlightDetails_type1 segmentDetail : flightDetails) {
					SegmentInfo segmentInfo = new SegmentInfo();
					segmentInfo.setSegmentNum(segmentNum);
					FlightInformation_type0 flightInformation = segmentDetail.getFlightInformation();
					ProductDateTime_type0 segmentDateTime = flightInformation.getProductDateTime();
					String flightNumber = flightInformation.getFlightOrtrainNumber().getFlightOrtrainNumber_type0();
					Location_type6[] locationDetails = flightInformation.getLocation();
					Location_type6 departureLocation = locationDetails[0];
					Location_type6 arrivalLocation = locationDetails[1];
					CompanyId_type0 airlineInfo = flightInformation.getCompanyId();
					ProductDetail_type0 productDetail = flightInformation.getProductDetail();
					segmentInfo.setDepartAirportInfo(
							AirportHelper.getAirport(departureLocation.getLocationId().getLocationId_type0()));
					segmentInfo.setArrivalAirportInfo(
							AirportHelper.getAirport(arrivalLocation.getLocationId().getLocationId_type0()));
					setDepartureTerminal(segmentInfo, departureLocation);
					setArrivalTerminal(segmentInfo, arrivalLocation);
					AirlineInfo operatingCarrier = null;
					if (airlineInfo.getOperatingCarrier() != null
							&& airlineInfo.getOperatingCarrier().getOperatingCarrier_type0() != null) {
						operatingCarrier = AirlineHelper
								.getAirlineInfo(airlineInfo.getOperatingCarrier().getOperatingCarrier_type0());
					}
					segmentInfo.setDepartTime(
							AmadeusUtils.parseDateTime(segmentDateTime.getDateOfDeparture().getDateOfDeparture_type0(),
									segmentDateTime.getTimeOfDeparture().getTimeOfDeparture_type0()));
					segmentInfo.setArrivalTime(
							AmadeusUtils.parseDateTime(segmentDateTime.getDateOfArrival().getDateOfArrival_type0(),
									segmentDateTime.getTimeOfArrival().getTimeOfArrival_type0()));
					segmentInfo.setDuration(getSegmentDuration(flightInformation));
					segmentInfo.setFlightDesignator(buildFlightDesignator(airlineInfo, flightNumber, productDetail));
					if (operatingCarrier != null && !operatingCarrier.getCode()
							.equalsIgnoreCase(segmentInfo.getFlightDesignator().getAirlineCode()))
						segmentInfo.setOperatedByAirlineInfo(AirlineHelper
								.getAirlineInfo(airlineInfo.getOperatingCarrier().getOperatingCarrier_type0()));
					if (productDetail != null && productDetail.getTechStopNumber() != null)
						segmentInfo.setStops(productDetail.getTechStopNumber().getTechStopNumber_type0().intValue());
					segmentInfo.setStopOverAirports(buildStopOverAirports(segmentDetail.getTechnicalStop()));
					segmentInfos.add(segmentInfo);
					segmentNum++;
				}
			}
		}
		return segmentInfos;
	}

	private void setArrivalTerminal(SegmentInfo segmentInfo, Location_type6 arrivalLocation) {
		if (arrivalLocation.getTerminal() != null
				&& StringUtils.isNotBlank(arrivalLocation.getTerminal().getTerminal_type0()))
			segmentInfo.getArrivalAirportInfo()
					.setTerminal(AirUtils.getTerminalInfo(arrivalLocation.getTerminal().getTerminal_type0()));
	}

	private void setDepartureTerminal(SegmentInfo segmentInfo, Location_type6 departureLocation) {
		if (departureLocation.getTerminal() != null
				&& StringUtils.isNotBlank(departureLocation.getTerminal().getTerminal_type0()))
			segmentInfo.getDepartAirportInfo()
					.setTerminal(AirUtils.getTerminalInfo(departureLocation.getTerminal().getTerminal_type0()));
	}

	private long getSegmentDuration(FlightInformation_type0 flightInformation) {
		AtomicLong duration = new AtomicLong();
		if (ArrayUtils.isNotEmpty(flightInformation.getAttributeDetails())) {
			Arrays.stream(flightInformation.getAttributeDetails()).forEach(attribute -> {
				if (attribute.getAttributeType().getAttributeType_type0()
						.equalsIgnoreCase(DURATION_SEGMENT_QUALIFIER)) {
					String durationTime = attribute.getAttributeDescription().getAttributeDescription_type0();
					long parsedDuration = (Integer.valueOf(durationTime.substring(0, 2)) * 60)
							+ Integer.valueOf(durationTime.substring(2, 4));
					duration.set(parsedDuration);
				}
			});
		}
		return duration.get();
	}

	private FlightIndex_type0 getRequestedGroupFromFlightIndex(Integer segmentIndex, FlightIndex_type0[] flightsIndex) {
		FlightIndex_type0 requestedFlight = null;
		if (segmentIndex < flightsIndex.length - 1
				&& flightsIndex[segmentIndex].getRequestedSegmentRef().getSegRef().getSegRef_type0().intValue()
						- 1 == segmentIndex) {
			requestedFlight = flightsIndex[segmentIndex];
		} else {
			for (FlightIndex_type0 eachFlight : flightsIndex) {
				if (eachFlight.getRequestedSegmentRef().getSegRef().getSegRef_type0().intValue() - 1 == segmentIndex) {
					requestedFlight = eachFlight;
					break;
				}
			}
		}
		return requestedFlight;
	}

	private List<AirportInfo> buildStopOverAirports(TechnicalStop_type0[] technicalStop) {
		List<AirportInfo> stopOverAirports = new ArrayList<>();
		if (technicalStop != null && ArrayUtils.isNotEmpty(technicalStop)) {
			Arrays.stream(technicalStop).forEach(stop -> {
				StopDetails_type0[] stopDetails = stop.getStopDetails();
				if (ArrayUtils.isNotEmpty(stopDetails)) {
					Arrays.stream(stopDetails).forEach(stopOver -> {
						if (stopOver.getLocationId() != null) {
							stopOverAirports
									.add(AirportHelper.getAirport(stopOver.getLocationId().getLocationId_type4()));
						}
					});
				}
			});
		}
		return stopOverAirports;
	}

	private FlightDesignator buildFlightDesignator(CompanyId_type0 airlineInfo, String flightNumber,
			ProductDetail_type0 productDetail) {
		FlightDesignator designator = new FlightDesignator();
		designator.setAirlineInfo(
				AirlineHelper.getAirlineInfo(airlineInfo.getMarketingCarrier().getMarketingCarrier_type0()));
		if (productDetail != null && productDetail.getEquipmentType() != null) {
			designator.setEquipType(productDetail.getEquipmentType().getEquipmentType_type0());
		}
		designator.setFlightNumber(flightNumber);
		return designator;
	}

	private GroupOfFlights_type1 getRequestedFlightGroup(GroupOfFlights_type1[] groupOfFlights, Integer refNumber,
			Integer segmentIndex) {
		GroupOfFlights_type1 groupOfFlight = null;
		for (GroupOfFlights_type1 flights : groupOfFlights) {
			if (Integer.valueOf(
					flights.getPropFlightGrDetail().getFlightProposal()[0].getRef().getRef_type0()) == refNumber) {
				groupOfFlight = flights;
				break;
			}
		}
		return groupOfFlight;
	}

	private boolean checkSearchErrorMessage(ErrorMessage_type0 errorSection) {
		AtomicBoolean isError = new AtomicBoolean();
		if (errorSection != null && errorSection.getApplicationError() != null
				&& errorSection.getErrorMessageText().getFreeTextQualification() != null) {
			Description_type3[] description = errorSection.getErrorMessageText().getDescription();
			if (description != null && ArrayUtils.isNotEmpty(description)) {
				isError.set(true);
				for (Description_type3 descriptionType : description) {
					criticalMessageLogger.add(descriptionType.getDescription_type2());
				}
			}
		}
		if (errorSection != null && errorSection.getApplicationError() != null) {
			throw new NoSearchResultException(String.join(",", criticalMessageLogger));
		}
		return isError.get();
	}

	private Fare_MasterPricerTravelBoardSearch buildMPTBSearchRequest() {
		Fare_MasterPricerTravelBoardSearch searchRequest = new Fare_MasterPricerTravelBoardSearch();
		FareFamilies_type0[] fareFamilies = buildFareFamilies();
		if (fareFamilies != null) {
			searchRequest.setFareFamilies(fareFamilies);
		}
		searchRequest.setNumberOfUnit(buildNoOfUnits());
		searchRequest.setFareOptions(buildFareOptions());
		searchRequest.setPaxReference(buildPaxReferences());
		searchRequest.setItinerary(buildItinerary());
		searchRequest.setTravelFlightInfo(buildPreferedOptions());
		return searchRequest;
	}

	private TravelFlightInformationType_185853S buildPreferedOptions() {
		TravelFlightInformationType_185853S travelOptions = new TravelFlightInformationType_185853S();;
		CompanyIdentificationType_233548C[] travelAirlines = getPreferredAirlines();
		if (travelAirlines != null) {
			travelOptions.setCompanyIdentity(travelAirlines);
		}
		if (isCabinRequest && searchQuery.getCabinClass() != null) {
			CabinIdentificationType_233500C cabinIdentification = new CabinIdentificationType_233500C();
			List<AlphaString_Length0To1> cabinCode = new ArrayList<>();
			cabinCode.add(getAlphaLength0To1(AmadeusUtils.getCabinDesignator(searchQuery.getCabinClass())));
			cabinIdentification.setCabin(cabinCode.toArray(new AlphaString_Length0To1[0]));
			travelOptions.setCabinId(cabinIdentification);
		}
		return travelOptions;
	}

	private CompanyIdentificationType_233548C[] getPreferredAirlines() {
		Set<AirlineInfo> airlines = searchQuery.getPreferredAirline();
		if (CollectionUtils.isNotEmpty(airlines)) {
			CompanyIdentificationType_233548C[] companyId = new CompanyIdentificationType_233548C[1];
			companyId[0] = new CompanyIdentificationType_233548C();
			AtomicInteger index = new AtomicInteger(0);
			AlphaNumericString_Length2To3[] prefAirlines = new AlphaNumericString_Length2To3[airlines.size()];
			airlines.forEach(airline -> {
				prefAirlines[index.get()] = getAlphaNumeric2to3(airline.getCode());
				index.getAndIncrement();
			});
			companyId[0].setCarrierId(prefAirlines);
			companyId[0].setCarrierQualifier(getAlphaNumeric0to1(PREFERED_AIRLINE_QUALIFIER));
			return companyId;
		}
		return null;
	}

	private Itinerary_type0[] buildItinerary() {
		Integer routeSize = searchQuery.getRouteInfos().size();
		Itinerary_type0[] itineraryTpes = new Itinerary_type0[routeSize];
		AtomicInteger routeIndex = new AtomicInteger(0);
		searchQuery.getRouteInfos().forEach(routeInfo -> {
			OriginAndDestinationRequestType destinationRequestType = new OriginAndDestinationRequestType();
			itineraryTpes[routeIndex.get()] = new Itinerary_type0();
			itineraryTpes[routeIndex.get()].setArrivalLocalization(getArrivalLocation(routeInfo));
			itineraryTpes[routeIndex.get()].setDepartureLocalization(getDepartureDetails(routeInfo));
			destinationRequestType.setSegRef(getNumeric1to2(routeIndex.get()));
			itineraryTpes[routeIndex.get()].setRequestedSegmentRef(destinationRequestType);
			itineraryTpes[routeIndex.get()].setTimeDetails(getDepartureTimeDetails(routeInfo));
			routeIndex.getAndIncrement();
		});
		return itineraryTpes;
	}

	private DateAndTimeInformationType_181295S getDepartureTimeDetails(RouteInfo routeInfo) {
		DateAndTimeInformationType_181295S dateAndTimeInfo = new DateAndTimeInformationType_181295S();
		DateAndTimeDetailsTypeI detailsTypeI = new DateAndTimeDetailsTypeI();
		Date_DDMMYY date_ddmmyy = new Date_DDMMYY();
		date_ddmmyy.setDate_DDMMYY(AmadeusUtils.formatToDDMMYY(routeInfo.getTravelDate()));
		detailsTypeI.setDate(date_ddmmyy);
		dateAndTimeInfo.setFirstDateTimeDetail(detailsTypeI);
		return dateAndTimeInfo;
	}

	private DepartureLocationType getDepartureDetails(RouteInfo routeInfo) {
		DepartureLocationType locationType = new DepartureLocationType();
		ArrivalLocationDetailsType_120834C detailsType = new ArrivalLocationDetailsType_120834C();
		detailsType.setLocationId(getAlphaNumeric3to5(routeInfo.getNearByFromCityOrAirport().getCode()));
		locationType.setDeparturePoint(detailsType);
		return locationType;
	}

	private ArrivalLocalizationType getArrivalLocation(RouteInfo routeInfo) {
		ArrivalLocalizationType localizationType = new ArrivalLocalizationType();
		ArrivalLocationDetailsType detailsType = new ArrivalLocationDetailsType();
		detailsType.setLocationId(getAlphaNumeric3to5(routeInfo.getNearByToCityOrAirport().getCode()));
		localizationType.setArrivalPointDetails(detailsType);
		return localizationType;
	}

	private NumberOfUnitsType buildNoOfUnits() {
		NumberOfUnitsType unitsType = new NumberOfUnitsType();
		NumberOfUnitDetailsType_260583C[] detailsType = new NumberOfUnitDetailsType_260583C[2];
		detailsType[0] = new NumberOfUnitDetailsType_260583C();
		detailsType[0].setNumberOfUnits(getNumeric1to6(NO_OF_REC.intValue()));
		detailsType[0].setTypeOfUnit(getAlphaNumericCode1to3(RECOMMENDATION_COUNT));
		detailsType[1] = new NumberOfUnitDetailsType_260583C();
		detailsType[1].setNumberOfUnits(getNumeric1to6(paxCountWithOutInf));
		detailsType[1].setTypeOfUnit(getAlphaNumericCode1to3(PAX_UNIT));
		unitsType.setUnitNumberDetail(detailsType);
		return unitsType;
	}

	private TravellerReferenceInformationType[] buildPaxReferences() {
		TravellerReferenceInformationType[] paxReference = new TravellerReferenceInformationType[paxCountWithInf];
		Integer index = 0;
		if (adultCount > 0) {
			paxReference[index] = new TravellerReferenceInformationType();
			paxReference[index].addPtc(getAlphaNumeric1to6(ADULT_PAX));
			paxReference[index].setTraveller(buildTraveller(ADULT_PAX, adultCount, 1));
			index++;
		}
		if (childCount > 0) {
			paxReference[index] = new TravellerReferenceInformationType();
			paxReference[index].addPtc(getAlphaNumeric1to6(CHILD_PAX));
			paxReference[index].setTraveller(buildTraveller(CHILD_PAX, childCount, adultCount + 1));
			index++;
		}
		if (infantCount > 0) {
			paxReference[index] = new TravellerReferenceInformationType();
			paxReference[index].addPtc(getAlphaNumeric1to6(INFANT_PAX));
			paxReference[index].setTraveller(buildTraveller(INFANT_PAX, infantCount, 1));
		}
		return paxReference;
	}

	private TravellerDetailsType[] buildTraveller(String paxCode, Integer paxCount, Integer paxId) {
		TravellerDetailsType[] travellerDetails = new TravellerDetailsType[paxCount];
		for (Integer paxIndex = 1; paxIndex <= paxCount; paxIndex++) {
			travellerDetails[paxIndex - 1] = new TravellerDetailsType();
			travellerDetails[paxIndex - 1].setRef(getNumeric1to3(paxId));
			if (paxCode.equalsIgnoreCase(INFANT_PAX)) {
				Integer indicator = Integer.valueOf(INFANT_INDICATOR);
				travellerDetails[paxIndex - 1].setInfantIndicator(getNumeric1to1(indicator));
			}
			paxId++;
		}
		return travellerDetails;
	}

	// Public or Corp Related & if any account code informations related
	private FareOptions_type0 buildFareOptions() {
		List<String> accountCodes = supplierConfiguration.getSupplierAdditionalInfo().getAccountCodes();
		if (CollectionUtils.isNotEmpty(accountCodes) && accountCodes.size() > 6) {
			accountCodes.removeAll(accountCodes.subList(6, accountCodes.size()));
		}
		FareOptions_type0 fareOptions = new FareOptions_type0();
		PricingTicketingDetailsType detailsType = new PricingTicketingDetailsType();
		detailsType.setPricingTicketing(buildPricingTicketingInfo(accountCodes));
		fareOptions.setPricingTickInfo(detailsType);
		if (StringUtils.isNotEmpty(getSupplierCurrency())) {
			fareOptions.setConversionRate(buildConversionRate());
		}
		if (CollectionUtils.isNotEmpty(accountCodes)) {
			fareOptions.setCorporate(buildCorporateDetails(accountCodes));
		}
		return fareOptions;
	}

	private CorporateIdentificationType buildCorporateDetails(List<String> accountCodes) {
		CorporateIdentificationType identificationType = new CorporateIdentificationType();
		CorporateIdentityType[] corporateIdentityTypes = new CorporateIdentityType[1];
		corporateIdentityTypes[0] = new CorporateIdentityType();
		corporateIdentityTypes[0].setCorporateQualifier(getAlphaNumeric0to3(AmadeusConstants.CORPORATE_FARE_TYPE));
		corporateIdentityTypes[0].setIdentity(getAlphaNumericList1to20(accountCodes));
		identificationType.setCorporateId(corporateIdentityTypes);
		return identificationType;
	}

	private ConversionRateType buildConversionRate() {
		ConversionRateType rateType = new ConversionRateType();
		ConversionRateDetailsType[] detailsTypes = new ConversionRateDetailsType[1];
		detailsTypes[0] = new ConversionRateDetailsType();
		detailsTypes[0].setCurrency(getAlphaString1to3(getCurrencyCode()));
		rateType.setConversionRateDetail(detailsTypes);
		return rateType;
	}

	private PricingTicketingInformationType buildPricingTicketingInfo(List<String> accountCodes) {

		PricingTicketingInformationType ticketingInformationType = new PricingTicketingInformationType();
		// common for public & corp
		List<String> pricingCode = new ArrayList<>();
		pricingCode.add(TICKET_AVAILABILITY_CHECK);
		pricingCode.add(ETICKET_ITINERARY);
		if (StringUtils.isNotEmpty(getSupplierCurrency())) {
			pricingCode.add(CURRENCY_CONVERSION);
		}
		pricingCode.add(MINIRULE_QUALIFIER_TAG);

		if (CollectionUtils.isNotEmpty(accountCodes)) {
			pricingCode.add(AmadeusConstants.CORPORATE_FARE_TYPE);
		} else {
			pricingCode.add(AmadeusConstants.PUBLISHED_FARE_TYPE);
		}
		ticketingInformationType.setPriceType(getAlphaNumericList0to3(pricingCode));
		return ticketingInformationType;
	}

	private FareFamilies_type0[] buildFareFamilies() {
		if (MapUtils.isNotEmpty(fareFamily)) {
			FareFamilies_type0[] fareFamiliy = new FareFamilies_type0[fareFamily.size()];
			AtomicInteger count = new AtomicInteger(0);
			fareFamily.forEach(((cabinClass, eachClassFlightsCount) -> {
				FareFamilies_type0 fareFamiliyType = new FareFamilies_type0();
				FareFamilyType fareFamilyType = new FareFamilyType();
				fareFamilyType.setFareFamilyname(getAlphaNumericCode1to10(cabinClass.getCode()));
				NumericInteger_Length1To4 hierarchy = new NumericInteger_Length1To4();
				hierarchy.setNumericInteger_Length1To4(new BigInteger(eachClassFlightsCount.toString()));
				fareFamilyType.setHierarchy(hierarchy);
				FareFamilyCriteriaType criteriaType = new FareFamilyCriteriaType();
				criteriaType.setCabinProduct(buildCabinProduct(AmadeusUtils.getCabinDesignator(cabinClass)));
				fareFamiliyType.setFamilyInformation(fareFamilyType);
				fareFamiliyType.setFamilyCriteria(criteriaType);
				fareFamiliy[count.get()] = fareFamiliyType;
				count.getAndIncrement();
			}));
			return fareFamiliy;
		}
		return null;
	}

	private CabinClassDesignationType[] buildCabinProduct(String cabinDesignator) {
		CabinClassDesignationType[] cabinClass = new CabinClassDesignationType[1];
		cabinClass[0] = new CabinClassDesignationType();
		cabinClass[0].setCabinDesignator(getAplha1to1(cabinDesignator));
		return cabinClass;
	}

	protected Pair<AirSearchQuery, AirSearchQuery> splitQueries(AirSearchQuery searchQuery) {
		AirSearchQuery onwardQ = new GsonMapper<>(searchQuery, AirSearchQuery.class).convert();
		AirSearchQuery returnQ = new GsonMapper<>(searchQuery, AirSearchQuery.class).convert();
		onwardQ.getRouteInfos().remove(1);
		returnQ.getRouteInfos().remove(0);
		onwardQ.isOneWay = true;
		returnQ.isOneWay = true;
		onwardQ.setSearchType(SearchType.ONEWAY);
		returnQ.setSearchType(SearchType.ONEWAY);
		returnQ.setOrigSearchType(SearchType.RETURN);
		return new ImmutablePair<>(onwardQ, returnQ);
	}
}
