package com.tgs.services.fms.sources.amadeus;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import com.amadeus.xml.tmrcrr_11_1_1a.*;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.amadeus.xml.AmadeusReviewWebServicesStub;
import com.amadeus.xml.tmrcrq_11_1_1a.AlphaNumericString_Length1To3;
import com.amadeus.xml.tmrcrq_11_1_1a.AlphaNumericString_Length1To35;
import com.amadeus.xml.tmrcrq_11_1_1a.ItemReferencesAndVersionsType;
import com.amadeus.xml.tmrcrq_11_1_1a.MiniRule_GetFromPricing;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.services.fms.datamodel.farerule.*;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Getter
@Setter
@SuperBuilder
final class AmadeusMiniFareRuleManager extends AmadeusServiceManager {

	protected AmadeusReviewWebServicesStub servicesStub;

	protected TripInfo selectedTrip;

	protected String bookingId;

	// Fare to pick codes
	private static final List<String> REISSUE_AFTER_TICKET = Arrays.asList("ADX");
	private static final List<String> REISSUE_BEFORE_TICKET = Arrays.asList("BDX");
	private static final List<String> CANCELLATION_AFTER_TICKET = Arrays.asList("ADX");
	private static final List<String> CANCELLATION_BEFORE_TICKET = Arrays.asList("BDX");
	private static final List<String> NOSHOW_BEFORE_TICKET = Arrays.asList("BNT");
	private static final List<String> NOSHOW_AFTER_TICKET = Arrays.asList("ANT");

	protected static final String AVAILABLE = "Available with Penalty";

	protected static final String NOT_APPLICABLE = "Not Applicable";

	protected static final String ALLOWED_INDICATOR = "1";

	protected final List<String> qualificationCode = Arrays.asList("BDA", "ADA");

	protected static HashMap<FareRuleTimeWindow, List<String>> fareRuleWindow;

	protected FareRuleInformation fareRuleInformation;

	// Reissue & date change Codes remain same only cat category will be different
	public static final List<String> BEFORE_DEPARTURE = Arrays.asList("BDA");
	public static final List<String> AFTER_DEPARTURE = Arrays.asList("ADA");
	public static final List<String> NOSHOW_BEFORE_DEPARTURE = Arrays.asList("BNA");
	public static final List<String> NOSHOW_AFTER_DEPARTURE = Arrays.asList("ANA");

	public TripFareRule getTripFareRules(TripInfo selectedTrip) {
		if (selectedTrip != null) {
			this.selectedTrip = selectedTrip;
			TripFareRule tripFareRule = TripFareRule.builder().build();
			FareRuleInformation information = getMiniFareRuleInfo();
			if (information != null) {
				tripFareRule.getFareRule().put(FareRuleManager.getRuleKey(selectedTrip), information);
			}
			return tripFareRule;
		}
		return null;
	}


	private FareRuleInformation getMiniFareRuleInfo() {
		FareRuleInformation fareRuleInformation = new FareRuleInformation();
		try {
			listener.setType("MiniRule_GetFromPricing");
			servicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MiniRule_GetFromPricingReply miniRuleReply =
					servicesStub.miniRule_GetFromPricing(getMiniRuleReq(), getSessionSchema());
			if (!checkErrorMessage(miniRuleReply)) {
				fareRuleInformation = parseMiniRule(selectedTrip, miniRuleReply);
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			servicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return fareRuleInformation;
	}

	private boolean checkErrorMessage(MiniRule_GetFromPricingReply miniRuleReply) {
		if (miniRuleReply != null && miniRuleReply.getErrorWarningGroup() != null) {
			ErrorGroupType errorGroupType = miniRuleReply.getErrorWarningGroup();
			if (errorGroupType != null && errorGroupType.getErrorOrWarningCodeDetails() != null) {
				FreeTextInformationType freeText = errorGroupType.getErrorWarningDescription();
				String message = freeText.getFreeText().getAlphaNumericString_Length1To199();
				log.error("Unable to fare rule for booking {} message {}", bookingId, message);
				if (criticalMessageLogger != null) {
					addMessageToLogger(message);
				}
				return true;
			}
		}
		return false;
	}

	private FareRuleInformation parseMiniRule(TripInfo selectedTrip, MiniRule_GetFromPricingReply miniRuleReply) {
		FareRuleInformation fareRuleInformation = new FareRuleInformation();
		try {
			List<String> selectedFareBasis = getSelectedFareBasis(selectedTrip.getSegmentInfos());
			MnrByFareRecommendation_type0 mnrByRecommendation = miniRuleReply.getMnrByFareRecommendation()[0];
			Set<String> fareRuleFareBasis = getMinifareBasis(mnrByRecommendation);
			if (isApplicableClass(fareRuleFareBasis, selectedFareBasis)
					&& mnrByRecommendation.getMnrRulesInfoGrp() != null) {
				MiniRulesRegulPropertiesType cancellationGroup =
						getMiniRulesOnCatType(mnrByRecommendation.getMnrRulesInfoGrp(), CANCELLATION_CAT);
				MiniRulesRegulPropertiesType reIssueGroup =
						getMiniRulesOnCatType(mnrByRecommendation.getMnrRulesInfoGrp(), REISSUE_CAT);
				MiniRulesRegulPropertiesType noShowGroup =
						getMiniRulesOnCatType(mnrByRecommendation.getMnrRulesInfoGrp(), NO_SHOW_CAT);

				if (cancellationGroup != null) {
					Map<FareRuleTimeWindow, FareRulePolicyContent> policies = new HashMap<>();
					if (checkRestrictionAction(cancellationGroup, AFTER_DEPARTURE)) {
						MnrMonInfoGrp_type0[] cancellationInfo = cancellationGroup.getMnrMonInfoGrp();
						Double cancellationAmount = getAmountBasedOnCurrency(getMonetaryInfoFare(cancellationInfo, CANCELLATION_AFTER_TICKET),getCurrencyCode());
						FareRulePolicyContent policyContent = new FareRulePolicyContent();
						policyContent.setAmount(cancellationAmount);
						policyContent.setPolicyInfo(AVAILABLE);
						policies.put(FareRuleTimeWindow.AFTER_DEPARTURE, policyContent);
					} else {
						policies.put(FareRuleTimeWindow.AFTER_DEPARTURE, getNotAllowedPolicyContent(NOT_APPLICABLE));
					}
					if (checkRestrictionAction(cancellationGroup, BEFORE_DEPARTURE)) {
						MnrMonInfoGrp_type0[] cancellationInfo = cancellationGroup.getMnrMonInfoGrp();
						Double cancellationAmount = getAmountBasedOnCurrency(getMonetaryInfoFare(cancellationInfo, CANCELLATION_BEFORE_TICKET),getCurrencyCode());
						FareRulePolicyContent policyContent = new FareRulePolicyContent();
						policyContent.setAmount(cancellationAmount);
						policyContent.setPolicyInfo(AVAILABLE);
						policies.put(FareRuleTimeWindow.BEFORE_DEPARTURE, policyContent);
					} else {
						policies.put(FareRuleTimeWindow.BEFORE_DEPARTURE, getNotAllowedPolicyContent(NOT_APPLICABLE));
					}
					fareRuleInformation.getFareRuleInfo().put(FareRulePolicyType.CANCELLATION, policies);
				}
				if (reIssueGroup != null) {
					Map<FareRuleTimeWindow, FareRulePolicyContent> policies = new HashMap<>();
					MnrMonInfoGrp_type0[] reIssueInfo = reIssueGroup.getMnrMonInfoGrp();
					if (checkRestrictionAction(reIssueGroup, BEFORE_DEPARTURE)) {
						Double reIssueAmount = getAmountBasedOnCurrency(getMonetaryInfoFare(reIssueInfo, REISSUE_BEFORE_TICKET),getCurrencyCode());
						FareRulePolicyContent policyContent = new FareRulePolicyContent();
						policyContent.setAmount(reIssueAmount);
						policyContent.setPolicyInfo(AVAILABLE);
						policies.put(FareRuleTimeWindow.BEFORE_DEPARTURE, policyContent);
					} else {
						policies.put(FareRuleTimeWindow.BEFORE_DEPARTURE, getNotAllowedPolicyContent(NOT_APPLICABLE));
					}
					if (checkRestrictionAction(reIssueGroup, AFTER_DEPARTURE)) {
						Double reIssueAmount = getAmountBasedOnCurrency(getMonetaryInfoFare(reIssueInfo, REISSUE_AFTER_TICKET),getCurrencyCode());
						FareRulePolicyContent policyContent = new FareRulePolicyContent();
						policyContent.setAmount(reIssueAmount);
						policyContent.setPolicyInfo(AVAILABLE);
						policies.put(FareRuleTimeWindow.AFTER_DEPARTURE, policyContent);
					} else {
						policies.put(FareRuleTimeWindow.AFTER_DEPARTURE, getNotAllowedPolicyContent(NOT_APPLICABLE));
					}
					fareRuleInformation.getFareRuleInfo().put(FareRulePolicyType.DATECHANGE, policies);
				}
				if (noShowGroup != null) {
					Map<FareRuleTimeWindow, FareRulePolicyContent> policies = new HashMap<>();
					MnrMonInfoGrp_type0[] noShowInfo = noShowGroup.getMnrMonInfoGrp();
					if (checkRestrictionAction(noShowGroup, NOSHOW_BEFORE_DEPARTURE)) {
						Double noShowAmount = getAmountBasedOnCurrency(getMonetaryInfoFare(noShowInfo, NOSHOW_BEFORE_TICKET),getCurrencyCode());
						FareRulePolicyContent policyContent = new FareRulePolicyContent();
						policyContent.setAmount(noShowAmount);
						policyContent.setPolicyInfo(AVAILABLE);
						policies.put(FareRuleTimeWindow.BEFORE_DEPARTURE, policyContent);
					} else {
						policies.put(FareRuleTimeWindow.BEFORE_DEPARTURE, getNotAllowedPolicyContent(NOT_APPLICABLE));
					}
					if (checkRestrictionAction(noShowGroup, NOSHOW_AFTER_DEPARTURE)) {
						Double noShowAmount = getAmountBasedOnCurrency(getMonetaryInfoFare(noShowInfo, NOSHOW_AFTER_TICKET),getCurrencyCode());
						FareRulePolicyContent policyContent = new FareRulePolicyContent();
						policyContent.setAmount(noShowAmount);
						policyContent.setPolicyInfo(AVAILABLE);
						policies.put(FareRuleTimeWindow.AFTER_DEPARTURE, policyContent);
					} else {
						policies.put(FareRuleTimeWindow.AFTER_DEPARTURE, getNotAllowedPolicyContent(NOT_APPLICABLE));
					}
					fareRuleInformation.getFareRuleInfo().put(FareRulePolicyType.NO_SHOW, policies);
				}
			}
		} catch (Exception e) {
			log.error("Unable to fetch Fare Rule {}", e);
		}
		return fareRuleInformation;
	}

	private FareRulePolicyContent getNotAllowedPolicyContent(String message) {
		FareRulePolicyContent policyContent = new FareRulePolicyContent();
		policyContent.setPolicyInfo(message);
		return policyContent;
	}

	private boolean checkRestrictionAction(MiniRulesRegulPropertiesType propertyRestriction,
			List<String> allowedCodes) {
		boolean isActionAllowed = false;
		if (propertyRestriction != null && propertyRestriction.getMnrRestriAppInfoGrp() != null) {
			MnrRestriAppInfoGrp_type0[] restriAppInfoGrps = propertyRestriction.getMnrRestriAppInfoGrp();
			for (MnrRestriAppInfoGrp_type0 restriAppInfo : restriAppInfoGrps) {
				StatusDetailsType[] statusDetails = restriAppInfo.getMnrRestriAppInfo().getStatusInformation();
				for (StatusDetailsType statusDetail : statusDetails) {
					if (allowedCodes.contains(statusDetail.getIndicator().getAlphaNumericString_Length1To3())
							&& statusDetail.getAction().getAlphaNumericString_Length1To3()
									.equalsIgnoreCase(ALLOWED_INDICATOR)) {
						isActionAllowed = true;
					}
				}
			}
		}
		return isActionAllowed;
	}

	private Double getMonetaryInfoFare(MnrMonInfoGrp_type0[] mnrInfos, List<String> allowedCodes) {
		Double amount = new Double(0);
		if (mnrInfos != null && ArrayUtils.isNotEmpty(mnrInfos)) {
			for (MnrMonInfoGrp_type0 mnrMonInfoGrp : mnrInfos) {
				MonetaryInformationDetailsType[] detailTypes = mnrMonInfoGrp.getMonetaryInfo().getMonetaryDetails();
				if (detailTypes != null && ArrayUtils.isNotEmpty(detailTypes)) {
					for (MonetaryInformationDetailsType info : detailTypes) {
						String qualifier = info.getTypeQualifier().getAlphaNumericString_Length1To3();
						if (allowedCodes.contains(qualifier)) {
							amount += Double.parseDouble(info.getAmount().getAlphaNumericString_Length1To12());
						}
					}
				}
			}
		}
		return amount;
	}

	private MiniRulesRegulPropertiesType getMiniRulesOnCatType(MiniRulesRegulPropertiesType[] mnrRulesInfoGrp,
			String catDesc) {
		if (mnrRulesInfoGrp != null && ArrayUtils.isNotEmpty(mnrRulesInfoGrp)) {
			for (MiniRulesRegulPropertiesType propertiesType : mnrRulesInfoGrp) {
				if (propertiesType.getMnrCatInfo() != null
						&& propertiesType.getMnrCatInfo().getDescriptionInfo() != null
						&& propertiesType.getMnrCatInfo().getDescriptionInfo().getNumber()
								.getNumericInteger_Length1To3().toString().equalsIgnoreCase(catDesc)) {
					return propertiesType;
				}
			}
		}
		return null;
	}

	private Set<String> getMinifareBasis(MnrByFareRecommendation_type0 mnrByRecommendation) {
		Set<String> fareBasis = new HashSet<>();
		for (FareComponentInfo_type0 fareComponentInfo : mnrByRecommendation.getFareComponentInfo()) {
			if (fareComponentInfo.getFareQualifierDetails() != null) {
				fareBasis.add(fareComponentInfo.getFareQualifierDetails().getAdditionalFareDetails().getRateClass()
						.getAlphaNumericString_Length1To35());
			}
		}
		return fareBasis;
	}

	private MiniRule_GetFromPricing getMiniRuleReq() {
		MiniRule_GetFromPricing miniRuleRequest = new MiniRule_GetFromPricing();
		miniRuleRequest.setFareRecommendationId(getFareRecommendation());
		return miniRuleRequest;
	}

	private ItemReferencesAndVersionsType[] getFareRecommendation() {
		ItemReferencesAndVersionsType[] fareRecomm = new ItemReferencesAndVersionsType[1];
		fareRecomm[0] = new ItemReferencesAndVersionsType();
		fareRecomm[0].setReferenceType(getReferneceNumber());
		fareRecomm[0].setUniqueReference(getUniqueReference());
		return fareRecomm;
	}

	private AlphaNumericString_Length1To35 getUniqueReference() {
		AlphaNumericString_Length1To35 value = new AlphaNumericString_Length1To35();
		value.setAlphaNumericString_Length1To35("ALL");
		return value;
	}

	private AlphaNumericString_Length1To3 getReferneceNumber() {
		AlphaNumericString_Length1To3 value = new AlphaNumericString_Length1To3();
		value.setAlphaNumericString_Length1To3("FRN");
		return value;
	}

	private boolean isApplicableClass(Set<String> fareRuleFareBasis, List<String> selectedFareBasis) {
		AtomicBoolean isAllowed = new AtomicBoolean();
		fareRuleFareBasis.forEach(fareBasis -> {
			if (!isAllowed.get() && selectedFareBasis.contains(fareBasis)) {
				isAllowed.set(true);
			}
		});
		return isAllowed.get();
	}
}
