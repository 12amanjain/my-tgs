package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.satrqt_13_2_1a.ActionCode_type1;
import com.amadeus.xml.satrqt_13_2_1a.Air_MultiAvailability;
import com.amadeus.xml.satrqt_13_2_1a.AirlineCode_type1;
import com.amadeus.xml.satrqt_13_2_1a.AirlineOrFlightOption_type0;
import com.amadeus.xml.satrqt_13_2_1a.ArrivalLocationInfo_type0;
import com.amadeus.xml.satrqt_13_2_1a.AvailabilityDetails_type0;
import com.amadeus.xml.satrqt_13_2_1a.AvailabilityOptions_type0;
import com.amadeus.xml.satrqt_13_2_1a.AvailabilityProductInfo_type0;
import com.amadeus.xml.satrqt_13_2_1a.BusinessFunction_type1;
import com.amadeus.xml.satrqt_13_2_1a.CityAirport_type1;
import com.amadeus.xml.satrqt_13_2_1a.CityAirport_type3;
import com.amadeus.xml.satrqt_13_2_1a.DepartureDate_type1;
import com.amadeus.xml.satrqt_13_2_1a.DepartureLocationInfo_type0;
import com.amadeus.xml.satrqt_13_2_1a.FlightIdentification_type0;
import com.amadeus.xml.satrqt_13_2_1a.FunctionDetails_type0;
import com.amadeus.xml.satrqt_13_2_1a.MessageActionDetails_type0;
import com.amadeus.xml.satrqt_13_2_1a.Number_type9;
import com.amadeus.xml.satrqt_13_2_1a.ProductTypeDetails_type0;
import com.amadeus.xml.satrqt_13_2_1a.RequestSection_type0;
import com.amadeus.xml.satrqt_13_2_1a.TypeOfRequest_type1;
import com.amadeus.xml.satrsp_13_2_1a.Air_MultiAvailabilityReply;
import com.amadeus.xml.satrsp_13_2_1a.ClassInformationType;
import com.amadeus.xml.satrsp_13_2_1a.FlightInfo_type0;
import com.amadeus.xml.satrsp_13_2_1a.SingleCityPairInfo_type0;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.datamodel.alternateclass.AlternateClass;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import java.rmi.RemoteException;
import java.util.*;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class AmadeusMultiAvailabilityManager extends AmadeusServiceManager {

	public void searchAlternateClass(TripInfo tripInfo) {
		Air_MultiAvailability airMultiAvailabilityRq = null;
		Air_MultiAvailabilityReply multiAvailabilityReply = null;
		try {
			listener.setType("Air_MultiAvailability");
			airMultiAvailabilityRq = buildMultiAvailability(tripInfo);
			webServicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			multiAvailabilityReply = webServicesStub.air_MultiAvailability(airMultiAvailabilityRq, getSessionSchema());
			buildAvailableClasses(tripInfo, multiAvailabilityReply);
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			webServicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private Air_MultiAvailability buildMultiAvailability(TripInfo tripInfo) {
		Air_MultiAvailability request = new Air_MultiAvailability();
		request.setMessageActionDetails(buildMessageAction());
		RequestSection_type0[] requestSection = new RequestSection_type0[tripInfo.getSegmentInfos().size()];
		int index = 0;
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			requestSection[index++] = buildRequestSection(segmentInfo);
		}
		request.setRequestSection(requestSection);
		return request;
	}

	private MessageActionDetails_type0 buildMessageAction() {
		MessageActionDetails_type0 messageActionDetails = new MessageActionDetails_type0();
		FunctionDetails_type0 functionDetails = new FunctionDetails_type0();
		functionDetails.setActionCode(getActionCode());
		functionDetails.setBusinessFunction(getBusinessFunction());
		messageActionDetails.setFunctionDetails(functionDetails);
		return messageActionDetails;
	}

	private BusinessFunction_type1 getBusinessFunction() {
		BusinessFunction_type1 type = new BusinessFunction_type1();
		type.setBusinessFunction_type0("1");
		return type;
	}

	private ActionCode_type1 getActionCode() {
		ActionCode_type1 type = new ActionCode_type1();
		type.setActionCode_type0("44");
		return type;
	}

	private RequestSection_type0 buildRequestSection(SegmentInfo segmentInfo) {
		RequestSection_type0 requestSection = new RequestSection_type0();
		buildAvailabilityOption(requestSection);
		buildAirlineOrFlightOption(segmentInfo, requestSection);
		buildAvailabilityProductInfo(segmentInfo, requestSection);
		return requestSection;

	}

	private void buildAvailabilityProductInfo(SegmentInfo segmentInfo, RequestSection_type0 requestSection) {
		AvailabilityProductInfo_type0 availabilityProductInfo = new AvailabilityProductInfo_type0();
		DepartureLocationInfo_type0 depLocation = new DepartureLocationInfo_type0();
		depLocation.setCityAirport(getCityOrAirport(segmentInfo.getDepartureAirportCode()));
		availabilityProductInfo.setDepartureLocationInfo(depLocation);
		ArrivalLocationInfo_type0 arrLocation = new ArrivalLocationInfo_type0();
		arrLocation.setCityAirport(getCityOrAirport2(segmentInfo.getArrivalAirportCode()));
		availabilityProductInfo.setArrivalLocationInfo(arrLocation);
		AvailabilityDetails_type0[] availabiltyDetails = new AvailabilityDetails_type0[1];
		availabiltyDetails[0] = new AvailabilityDetails_type0();
		availabiltyDetails[0].setDepartureDate(
				getDepartureDate(AmadeusUtils.formatToDDMMYY(segmentInfo.getDepartTime().toLocalDate())));
		availabilityProductInfo.setAvailabilityDetails(availabiltyDetails);
		requestSection.setAvailabilityProductInfo(availabilityProductInfo);
	}

	private DepartureDate_type1 getDepartureDate(String localDate) {
		DepartureDate_type1 departureDate = new DepartureDate_type1();
		departureDate.setDepartureDate_type0(localDate);
		return departureDate;
	}

	private CityAirport_type3 getCityOrAirport2(String code) {
		CityAirport_type3 city = new CityAirport_type3();
		city.setCityAirport_type2(code);
		return city;
	}

	private CityAirport_type1 getCityOrAirport(String code) {
		CityAirport_type1 city = new CityAirport_type1();
		city.setCityAirport_type0(code);
		return city;
	}

	private void buildAirlineOrFlightOption(SegmentInfo segmentInfo, RequestSection_type0 requestSection) {
		AirlineOrFlightOption_type0[] airlineOrFlightOption = new AirlineOrFlightOption_type0[1];
		airlineOrFlightOption[0] = new AirlineOrFlightOption_type0();
		FlightIdentification_type0[] flightIdentification = new FlightIdentification_type0[1];
		flightIdentification[0] = new FlightIdentification_type0();
		flightIdentification[0].setAirlineCode(getAirlineCode(segmentInfo.getAirlineCode(true)));
		flightIdentification[0].setNumber(getFlightNumber(segmentInfo.getFlightNumber()));
		airlineOrFlightOption[0].setFlightIdentification(flightIdentification);
		requestSection.setAirlineOrFlightOption(airlineOrFlightOption);
	}

	private Number_type9 getFlightNumber(String flightNumber) {
		Number_type9 type = new Number_type9();
		type.setNumber_type8(flightNumber);
		return type;
	}

	private AirlineCode_type1 getAirlineCode(String airlineCode) {
		AirlineCode_type1 type = new AirlineCode_type1();
		type.setAirlineCode_type0(airlineCode);
		return type;
	}

	private void buildAvailabilityOption(RequestSection_type0 requestSection) {
		AvailabilityOptions_type0 availabilityOptions = new AvailabilityOptions_type0();
		ProductTypeDetails_type0 productdetails = new ProductTypeDetails_type0();
		TypeOfRequest_type1 type = new TypeOfRequest_type1();
		type.setTypeOfRequest_type0("TN");
		productdetails.setTypeOfRequest(type);
		availabilityOptions.setProductTypeDetails(productdetails);
		requestSection.setAvailabilityOptions(availabilityOptions);
	}

	private void buildAvailableClasses(TripInfo tripInfo, Air_MultiAvailabilityReply reply) {

		if (reply != null && ArrayUtils.isNotEmpty(reply.getSingleCityPairInfo())) {
			Map<String, SingleCityPairInfo_type0> cityInfoMap = buildSingleCityPairMap(reply.getSingleCityPairInfo());
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				List<AlternateClass> classAvailable = new ArrayList<>();
				String key =
						getOriginDestKey(segmentInfo.getDepartureAirportCode(), segmentInfo.getArrivalAirportCode());
				SingleCityPairInfo_type0 cityInfo = cityInfoMap.get(key);
				if (Objects.nonNull(cityInfo) && cityInfo.getFlightInfo() != null
						&& ArrayUtils.isNotEmpty(cityInfo.getFlightInfo())) {
					FlightInfo_type0[] flightInfo = cityInfo.getFlightInfo();
					ClassInformationType[] classInformations = flightInfo[0].getInfoOnClasses();
					if (flightInfo[0].getInfoOnClasses() != null
							&& ArrayUtils.isNotEmpty(flightInfo[0].getInfoOnClasses())) {
						for (ClassInformationType classInfo : classInformations) {
							String availablity = StringUtils.EMPTY;
							// This regex will consider only those classes where avavilabilty status is numeric
							if (classInfo.getProductClassDetail().getAvailabilityStatus().toString().matches("\\d+")) {
								availablity = classInfo.getProductClassDetail().getAvailabilityStatus().toString();
							}
							if (StringUtils.isNotBlank(availablity)) {
								int availableSeats = Integer.valueOf(availablity);
								if (availableSeats >= 1) {
									// if seat greater than 1 the add this class info to class Available
									AlternateClass alternateClass = AlternateClass.builder()
											.classOfBook(classInfo.getProductClassDetail().getServiceClass().toString())
											.seatCount(availableSeats).build();
									classAvailable.add(alternateClass);
								}
							}
						}
					}
				}
				segmentInfo.getAlternateClass().addAll(classAvailable);
			}
		}
	}

	private Map<String, SingleCityPairInfo_type0> buildSingleCityPairMap(
			SingleCityPairInfo_type0[] singleCityPairInfo) {
		Map<String, SingleCityPairInfo_type0> seatMap = new HashMap<>();
		for (SingleCityPairInfo_type0 cityInfo : singleCityPairInfo) {
			String key = getOriginDestKey(cityInfo.getLocationDetails().getOrigin().toString(),
					cityInfo.getLocationDetails().getDestination().toString());
			seatMap.put(key, cityInfo);
		}
		return seatMap;
	}

	private String getOriginDestKey(String originAirportCode, String destAirportCode) {
		return StringUtils.join(originAirportCode, "_", destAirportCode);
	}

}
