package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.AmadeusBookingWebServicesStub;
import com.amadeus.xml.AmadeusWebServicesStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class AmadeusRetrieveBookingFactory extends AbstractAirBookingRetrieveFactory {

	protected SoapRequestResponseListner listener;

	protected AmadeusBindingService bindingService;

	protected AmadeusWebServicesStub webServicesStub;

	protected AmadeusBookingWebServicesStub bookingStub;

	AmadeusRetrieveBookingFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	public void initialize(String pnr) {
		bindingService = AmadeusBindingService.builder().cacheCommunicator(cachingCommunicator)
				.configuration(supplierConf).user(bookingUser).build();
		listener = new SoapRequestResponseListner(pnr, null, supplierConf.getBasicInfo().getSupplierName());
		webServicesStub = bindingService.getWebServiceStub(null);
		bookingStub = bindingService.getBookingWebServiceStub(null);
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		AmadeusSessionManager sessionManager = AmadeusSessionManager.builder().listener(listener)
				.supplierConfiguration(supplierConf).bookingUser(bookingUser).build();
		initialize(pnr);
		sessionManager.setWebServicesStub(webServicesStub);
		sessionManager.setListener(listener);
		try {
			sessionManager.openSession();
			AmadeusRetrieveBookingManager retrieveManager = AmadeusRetrieveBookingManager.builder()
					.supplierConfiguration(supplierConf).bookingUser(bookingUser).build();
			retrieveManager.setListener(listener);
			retrieveManager.setSession(sessionManager.getSessionSchema());
			retrieveManager.setBookingStub(bookingStub);
			retrieveManager.setWebServicesStub(webServicesStub);
			pnrBooking = retrieveManager.retrieve(pnr);
		} finally {
			sessionManager.closeSession();
			storeStubs();
		}
		return pnrBooking;
	}


	private void storeStubs() {
		bindingService = AmadeusBindingService.builder().cacheCommunicator(cachingCommunicator)
				.configuration(supplierConf).user(bookingUser).build();
		bindingService.cleanUp(bookingStub);
		bindingService.cleanUp(webServicesStub);
		bindingService.storeSessionMetaInfo(supplierConf, AmadeusBookingWebServicesStub.class, bookingStub);
		bindingService.storeSessionMetaInfo(supplierConf, AmadeusWebServicesStub.class, webServicesStub);
	}

}
