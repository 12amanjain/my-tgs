package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.amadeus.xml.AmadeusBookingWebServicesStub;
import com.amadeus.xml.pnracc_15_1_1a.CompanyDetail_type0;
import com.amadeus.xml.pnracc_15_1_1a.DataElementsIndiv_type1;
import com.amadeus.xml.pnracc_15_1_1a.DataElementsMaster_type0;
import com.amadeus.xml.pnracc_15_1_1a.EnhancedPassengerData_type1;
import com.amadeus.xml.pnracc_15_1_1a.EnhancedTravellerInformation_type0;
import com.amadeus.xml.pnracc_15_1_1a.FareData_type0;
import com.amadeus.xml.pnracc_15_1_1a.FareElement_type0;
import com.amadeus.xml.pnracc_15_1_1a.FlightDetail_type0;
import com.amadeus.xml.pnracc_15_1_1a.FreeText_type53;
import com.amadeus.xml.pnracc_15_1_1a.FrequentTraveler_type0;
import com.amadeus.xml.pnracc_15_1_1a.ItineraryInfo_type1;
import com.amadeus.xml.pnracc_15_1_1a.MonetaryInfo_type0;
import com.amadeus.xml.pnracc_15_1_1a.OriginDestinationDetails_type0;
import com.amadeus.xml.pnracc_15_1_1a.OtherDataFreetext_type0;
import com.amadeus.xml.pnracc_15_1_1a.PNR_Reply;
import com.amadeus.xml.pnracc_15_1_1a.ProductDetails_type1;
import com.amadeus.xml.pnracc_15_1_1a.Reference_type8;
import com.amadeus.xml.pnracc_15_1_1a.Reference_type9;
import com.amadeus.xml.pnracc_15_1_1a.Ssr_type0;
import com.amadeus.xml.pnracc_15_1_1a.TaxFields_type0;
import com.amadeus.xml.pnracc_15_1_1a.TravelProduct_type0;
import com.amadeus.xml.pnracc_15_1_1a.TravellerInfo_type0;
import com.amadeus.xml.pnracc_15_1_1a.TstData_type0;
import com.amadeus.xml.pnracc_15_1_1a.TstFreetext_type0;
import com.amadeus.xml.pnrret_15_1_1a.AlphaNumericString_Length1To20;
import com.amadeus.xml.pnrret_15_1_1a.NumericInteger_Length1To2;
import com.amadeus.xml.pnrret_15_1_1a.PNR_Retrieve;
import com.amadeus.xml.pnrret_15_1_1a.ReservationControlInformationDetailsType;
import com.amadeus.xml.pnrret_15_1_1a.ReservationControlInformationType;
import com.amadeus.xml.pnrret_15_1_1a.RetrievalFacts_type0;
import com.amadeus.xml.pnrret_15_1_1a.RetrievePNRType;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SegmentMiscInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Getter
@Setter
@SuperBuilder
@Slf4j
final class AmadeusRetrieveBookingManager extends AmadeusServiceManager {

	protected static final String BASEFARE_QUALIFIER_D = "F";

	protected static final String BASEFARE_QUALIFIER_I = "E";

	protected static final String TOTALFARE_QUALIFIER = "T";

	protected static final String PAX_QUALIFIER = "PT";

	protected static final String SPECIAL_SSR_REF = "SSR";

	protected AmadeusBookingWebServicesStub bookingStub;

	private static List<String> titleList = Arrays.asList("mr", "ms", "miss", "mstr", "mrs", "master");

	protected static final String RETRIEVE_PNR_TYPE = "2";

	protected String crsPnr;

	public void initialize(List<SegmentInfo> segmentInfos) {
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			travellerInfos = segmentInfos.get(0).getTravellerInfo();
			adults = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT);
			childs = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD);
			infants = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT);
			extractTravellers();
			airlinePnrs = new ArrayList<>();
			ticketNumbers = new ArrayList<>();
			segmentIndexToRefMap = new HashMap<>();
			passengerRefToIndexMap = new HashMap<>();
			infantPassengerRefToIndexMap = new HashMap<>();
			passengerIndexToRefMap = new HashMap<>();
		}
	}

	public PNR_Reply retrieveBooking(String supplierPnr) {
		PNR_Retrieve retrieve = null;
		PNR_Reply reply = null;
		try {
			crsPnr = supplierPnr;
			listener.setType("PNR_Retrieve");
			retrieve = buildRetreiveRq(supplierPnr);
			bookingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			reply = bookingStub.pNR_Retrieve(retrieve, getSessionSchema());
			if (checkPNRReplyErrorMessage(reply)) {
				throw new NoPNRFoundException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			bookingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return reply;
	}

	public AirImportPnrBooking retrieve(String crsPnr) {
		AirImportPnrBooking pnrBooking = AirImportPnrBooking.builder().build();
		criticalMessageLogger = new ArrayList<>();
		PNR_Reply reply = retrieveBooking(crsPnr);
		if (reply != null) {
			List<TripInfo> trips = parseTripInfos(reply);
			initialize(AirUtils.getSegmentInfos(trips));
			List<String> airlinePnrs = parseAirlinePnr(reply);
			parseTravellerReferences(reply.getTravellerInfo());
			parseTravellerSpecialServices(trips, reply.getDataElementsMaster());
			setSegmentsAirlinePnr(airlinePnrs, AirUtils.getSegmentInfos(trips));
			List<String> ticketNumbers = extractTicketNumbers(reply);
			setTicketNumbers(ticketNumbers, AirUtils.getSegmentInfos(trips));
			pnrBooking.setTripInfos(trips);
			pnrBooking.setDeliveryInfo(parseContactInfo(reply.getDataElementsMaster()));
			pnrBooking.setGstInfo(parseGSTInfo(reply.getDataElementsMaster(), crsPnr));
		}
		return pnrBooking;
	}

	private void parseTravellerSpecialServices(List<TripInfo> tripInfos, DataElementsMaster_type0 dataElementsMaster) {
		for (TripInfo trip : tripInfos) {
			Map<SSRType, List<? extends SSRInformation>> preSSR =
					AirUtils.getPreSSR(supplierConfiguration.getBasicInfo(), trip.getAirlineCode(true, 0), bookingUser);
			for (SegmentInfo segment : trip.getSegmentInfos()) {
				Integer segReference = Integer.parseInt(segment.getMiscInfo().getUpdatedUserfeeType());
				int travellerIndex = 0;
				for (FlightTravellerInfo travellerInfo : segment.getBookingRelatedInfo().getTravellerInfo()) {
					Integer paxReference = passengerIndexToRefMap.get(travellerIndex);
					setTravellerSSR(travellerInfo, paxReference, segReference, dataElementsMaster, preSSR);
					setPassportAndFrequentFlier(travellerInfo, paxReference, dataElementsMaster);
					travellerIndex++;
				}
				segment.getMiscInfo().setUpdatedUserfeeType(null);
			}
		}
	}

	private void setPassportAndFrequentFlier(FlightTravellerInfo travellerInfo, Integer paxReference,
			DataElementsMaster_type0 dataElementsMaster) {
		for (DataElementsIndiv_type1 dataElement : dataElementsMaster.getDataElementsIndiv()) {
			if (Objects.nonNull(dataElement.getElementManagementData())
					&& SPECIAL_SSR_REF
							.equalsIgnoreCase(dataElement.getElementManagementData().getSegmentName().toString())
					&& Objects.nonNull(dataElement.getReferenceForDataElement())
					&& ArrayUtils.isNotEmpty(dataElement.getReferenceForDataElement().getReference())) {
				Reference_type8 reference[] = dataElement.getReferenceForDataElement().getReference();
				if (AmadeusConstants.TRAVEL_DOCUMENT_CODE
						.equals(dataElement.getServiceRequest().getSsr().getType().getType_type158())
						&& AmadeusUtils.isValidSSRStatus(
								dataElement.getServiceRequest().getSsr().getStatus().getStatus_type30())) {
					setPassportdetails(travellerInfo, paxReference, dataElement.getServiceRequest().getSsr(),
							reference[0]);
				}
				if (AmadeusConstants.FREQUENT_FLIER_INFORMATION_ELEMENT
						.equals(dataElement.getServiceRequest().getSsr().getType().getType_type158())
						&& AmadeusUtils.isValidSSRStatus(
								dataElement.getServiceRequest().getSsr().getStatus().getStatus_type30())
						&& !travellerInfo.getPaxType().equals(PaxType.INFANT)) {
					setFrequentFlier(travellerInfo, paxReference, dataElement.getFrequentFlyerInformationGroup()
							.getFrequentTravellerInfo().getFrequentTraveler(), reference[0]);
				}
			}
		}

	}

	private void setFrequentFlier(FlightTravellerInfo travellerInfo, Integer paxReference,
			FrequentTraveler_type0 frequentTravelerType, Reference_type8 referenceType) {
		if (PASSENGER_REFERENCE.equals(referenceType.getQualifier().getQualifier_type142())) {
			if (referenceType.getNumber().getNumber_type58().equals(String.valueOf(paxReference))) {
				try {
					Map<String, String> freqFlierMap = new HashMap<String, String>();
					freqFlierMap.put(frequentTravelerType.getCompany().getCompany_type6(),
							frequentTravelerType.getMembershipNumber().getMembershipNumber_type6());
					travellerInfo.setFrequentFlierMap(freqFlierMap);
				} catch (Exception e) {
					log.error("Unable to parse Frequent Flier Details for booking iD {}", bookingId);
				}
			}
		}


	}

	private void setPassportdetails(FlightTravellerInfo travellerInfo, Integer paxReference, Ssr_type0 ssr,
			Reference_type8 referenceType) {
		if (PASSENGER_REFERENCE.equals(referenceType.getQualifier().getQualifier_type142())) {
			if (referenceType.getNumber().getNumber_type58().equals(String.valueOf(paxReference))) {
				try {
					FreeText_type53[] passportInfo = ssr.getFreeText();
					String[] passportData = passportInfo[0].getFreeText_type52().split("/");
					if (travellerInfo.getLastName().equalsIgnoreCase(passportData[7])
							&& (travellerInfo.getFirstName().equalsIgnoreCase(passportData[8])
									|| passportData[8].equals("NULL"))) {
						travellerInfo.setPassportNumber(passportData[2]);
						travellerInfo.setDob(AmadeusUtils.getLocaldateFormString(passportData[4]));
						travellerInfo.setExpiryDate(AmadeusUtils.getLocaldateFormString(passportData[6]));
					}
				} catch (Exception e) {
					log.error("Unable to parse passport details for booking iD {}", bookingId);
				}
			}
		}

	}

	private void setTravellerSSR(FlightTravellerInfo travellerInfo, int travellerRef, Integer segReference,
			DataElementsMaster_type0 dataElementsMaster, Map<SSRType, List<? extends SSRInformation>> preSSR) {
		if (MapUtils.isNotEmpty(preSSR)) {
			for (DataElementsIndiv_type1 dataElement : dataElementsMaster.getDataElementsIndiv()) {
				if (Objects.nonNull(dataElement.getElementManagementData())
						&& SPECIAL_SSR_REF
								.equalsIgnoreCase(dataElement.getElementManagementData().getSegmentName().toString())
						&& Objects.nonNull(dataElement.getReferenceForDataElement())
						&& ArrayUtils.isNotEmpty(dataElement.getReferenceForDataElement().getReference())) {
					Reference_type8 reference[] = dataElement.getReferenceForDataElement().getReference();
					boolean isValidPax = false;
					boolean isValidSegment = false;
					for (Reference_type8 ref : reference) {
						if (PASSENGER_REFERENCE.equals(ref.getQualifier().getQualifier_type142())) {
							if (ref.getNumber().getNumber_type58().equals(String.valueOf(travellerRef))) {
								// for this passenger , this SSR is applicable
								isValidPax = true;
							}
						}
						if (SEGMENT_REFERENCE.equals(ref.getQualifier().getQualifier_type142())) {
							if (ref.getNumber().getNumber_type58().equals(String.valueOf(segReference))) {
								// for this segment , this SSR is applicable
								isValidSegment = true;
							}
						}
					}
					if (isValidPax && isValidSegment && AmadeusUtils.isValidSSRStatus(
							dataElement.getServiceRequest().getSsr().getStatus().getStatus_type30())) {
						parseSSRtoTravller(dataElement.getServiceRequest().getSsr(), travellerInfo, preSSR);
					}
				}
			}
		}
	}

	private void parseSSRtoTravller(Ssr_type0 serviceSsr, FlightTravellerInfo travellerInfo,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {

		if (!travellerInfo.getPaxType().equals(PaxType.INFANT)) {
			for (Entry<SSRType, List<? extends SSRInformation>> entry : preSSR.entrySet()) {
				if (CollectionUtils.isNotEmpty(entry.getValue())) {
					for (SSRInformation ssr : entry.getValue()) {
						if (!"INFT".equalsIgnoreCase(ssr.getCode())
								&& ssr.getCode().equals(serviceSsr.getType().getType_type158())) {
							if (SSRType.MEAL.equals(entry.getKey())) {
								travellerInfo.setSsrMealInfo(ssr);
							} else if (SSRType.SEAT.equals(entry.getKey())) {
								travellerInfo.setSsrSeatInfo(ssr);
							} else if (SSRType.EXTRASERVICES.equals(entry.getKey())) {
								List<SSRInformation> extraServices =
										(CollectionUtils.isEmpty(travellerInfo.getExtraServices())) ? new ArrayList<>()
												: travellerInfo.getExtraServices();
								extraServices.add(ssr);
								travellerInfo.setExtraServices(extraServices);
							}
						}
					}
				}
			}
		}


	}

	private GstInfo parseGSTInfo(DataElementsMaster_type0 dataElementsMaster, String crsPnr) {
		GstInfo gstInfo = new GstInfo();;
		try {
			for (DataElementsIndiv_type1 dataElement : dataElementsMaster.getDataElementsIndiv()) {
				if (dataElement.getServiceRequest() != null && dataElement.getServiceRequest().getSsr() != null) {
					Ssr_type0 ssr = dataElement.getServiceRequest().getSsr();
					if (AmadeusConstants.GST_NUMBER.equals(ssr.getType().getType_type158())) {
						String[] gstData = ssr.getFreeText()[0].getFreeText_type52().split("/");
						gstInfo.setGstNumber(gstData[1]);
						gstInfo.setRegisteredName(gstData[2]);
					} else if (AmadeusConstants.GST_ADDRESS.equals(ssr.getType().getType_type158())) {
						String[] gstData = ssr.getFreeText()[0].getFreeText_type52().split("/");
						gstInfo.setAddress(gstData[1]);
					} else if (AmadeusConstants.GST_CONTACT_NUMBER.equals(ssr.getType().getType_type158())) {
						String[] gstData = ssr.getFreeText()[0].getFreeText_type52().split("/");
						gstInfo.setMobile(gstData[1].substring(gstData[1].indexOf('-'), gstData[1].length()));
					} else if (AmadeusConstants.GST_EMAIL.equals(ssr.getType().getType_type158())) {
						String emaildata = ssr.getFreeText()[0].getFreeText_type52().replaceAll("//", "@");
						String[] gstData = emaildata.split("/");
						gstInfo.setEmail(gstData[1].toLowerCase());
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse Gst Info for Supplier Ref {}", crsPnr);
		}
		return gstInfo;
	}

	private DeliveryInfo parseContactInfo(DataElementsMaster_type0 dataElementsMaster) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		List<String> emails = new ArrayList<>();
		List<String> contacts = new ArrayList<>();
		for (DataElementsIndiv_type1 dataElement : dataElementsMaster.getDataElementsIndiv()) {
			if (dataElement.getOtherDataFreetext() != null) {
				for (OtherDataFreetext_type0 otherDataText : dataElement.getOtherDataFreetext()) {
					if (AmadeusConstants.EMAIL_ADDRESS_TICKET_TYPE
							.equals(otherDataText.getFreetextDetail().getType().getType_type160())) {
						emails.add(otherDataText.getLongFreetext().getLongFreetext_type6().toLowerCase());
					} else if (AmadeusConstants.MOBILE_PHONE_CONTACT
							.equals(otherDataText.getFreetextDetail().getType().getType_type160())) {
						contacts.add(otherDataText.getLongFreetext().getLongFreetext_type6());
					}
				}
			}
		}
		deliveryInfo.setEmails(emails);
		deliveryInfo.setContacts(contacts);
		return deliveryInfo;
	}

	private List<TripInfo> parseTripInfos(PNR_Reply reply) {
		List<TripInfo> tripInfos = new ArrayList<>();
		if (Objects.nonNull(reply.getOriginDestinationDetails())) {
			for (OriginDestinationDetails_type0 originDest : reply.getOriginDestinationDetails()) {
				TripInfo trip = new TripInfo();
				LocalDateTime prevArrivalTime = null;
				int segmentNum = 0;
				int index = 0;
				boolean isReturnSegment = false;
				for (ItineraryInfo_type1 itinInfo : originDest.getItineraryInfo()) {
					if (Objects.nonNull(itinInfo.getFlightDetail())) {
						SegmentInfo segmentInfo = new SegmentInfo();
						SegmentMiscInfo miscInfo = SegmentMiscInfo.builder().build();
						miscInfo.setUpdatedUserfeeType(
								itinInfo.getElementManagementItinerary().getReference().getNumber().getNumber_type8());
						segmentInfo.setSegmentNum(segmentNum);
						setDepartureDetails(segmentInfo, itinInfo);
						setArrivalDetails(segmentInfo, itinInfo);
						segmentInfo.setFlightDesignator(getFlightDesignator(itinInfo));
						segmentInfo.setStops(itinInfo.getFlightDetail().getProductDetails().getNumOfStops()
								.getNumOfStops_type0().intValue());
						segmentInfo.setDuration(getDuration(itinInfo));
						segmentInfo.setMiscInfo(miscInfo);
						SegmentBookingRelatedInfo bookingRelatedInfo = SegmentBookingRelatedInfo.builder().build();
						List<PriceInfo> priceInfos = new ArrayList<>();
						PriceInfo priceInfo = PriceInfo.builder().build();
						priceInfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
						priceInfos.add(priceInfo);
						segmentInfo.setPriceInfoList(priceInfos);
						segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
						LocalDateTime departureTime = segmentInfo.getDepartTime();
						long connectingTime = 0;
						if (Objects.nonNull(prevArrivalTime)) {
							connectingTime = prevArrivalTime.until(departureTime, ChronoUnit.MINUTES);
							if (connectingTime > AmadeusConstants.FIFTEEN_HUNDRED) {
								tripInfos.add(trip);
								trip = new TripInfo();
								segmentNum = 0;
								segmentInfo.setSegmentNum(segmentNum);
							}
						}
						segmentInfo.setIsReturnSegment(isReturnSegment);
						prevArrivalTime = segmentInfo.getDepartTime();
						setTravellers(reply, reply.getTravellerInfo(), bookingRelatedInfo, index++, itinInfo);
						trip.getSegmentInfos().add(segmentInfo);
						segmentNum++;
					}
				}
				tripInfos.add(trip);
			}
		} else {
			throw new NoPNRFoundException("PNR may be cancelled or expired!");
		}
		if (tripInfos.size() == 2) {
			AmadeusUtils.setReturnSegments(tripInfos);
		}
		return tripInfos;
	}

	private long getDuration(ItineraryInfo_type1 itinInfo) {
		FlightDetail_type0 flightDetail = itinInfo.getFlightDetail();
		if (flightDetail != null && flightDetail.getProductDetails() != null
				&& itinInfo.getFlightDetail().getProductDetails().getDuration() != null) {
			String durationText = itinInfo.getFlightDetail().getProductDetails().getDuration().getDuration_type0();
			return AmadeusUtils.calculateDuration(durationText);
		}
		return 0;
	}


	private void setTravellers(PNR_Reply reply, TravellerInfo_type0[] travellerInfos,
			SegmentBookingRelatedInfo bookingRelatedInfo, int segmentIndex, ItineraryInfo_type1 itinInfo) {
		List<FlightTravellerInfo> bookingTravellerInfo = new ArrayList<>();
		TravelProduct_type0 travelProduct = itinInfo.getTravelProduct();
		for (TravellerInfo_type0 traveller : travellerInfos) {
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			String paxReference =
					traveller.getElementManagementPassenger().getReference().getNumber().getNumber_type6();
			TstData_type0 tst = getTst(reply.getTstData(), paxReference, false);
			travellerInfo.setSupplierBookingId(crsPnr);
			FareDetail fareDetail = buildFareDetail(itinInfo, tst, segmentIndex);
			EnhancedPassengerData_type1 enhancedTrav = traveller.getEnhancedPassengerData()[0];
			travellerInfo.setPaxType(getPaxType(enhancedTrav));
			AmadeusUtils.setTravellerName(travellerInfo, enhancedTrav);
			travellerInfo.setFareDetail(fareDetail);
			if (StringUtils.isEmpty(travellerInfo.getTitle())) {
				setTitleAndType(travellerInfo, enhancedTrav);
			}
			if (travellerInfo.getPaxType() == null) {
				travellerInfo.setPaxType(AmadeusUtils.getPaxTypeOnTitle(travellerInfo, travelProduct));
			}
			if (segmentIndex == 0) {
				parseFareComponents(fareDetail, tst);
			}
			bookingTravellerInfo.add(travellerInfo);
			if (isInfantAvailable(enhancedTrav)) {
				FlightTravellerInfo infant = new FlightTravellerInfo();
				infant.setPaxType(PaxType.INFANT);
				AmadeusUtils.setTravellerName(infant, traveller.getEnhancedPassengerData()[1]);
				infant.setSupplierBookingId(crsPnr);
				TstData_type0 infantTst = getTst(reply.getTstData(), paxReference, true);
				FareDetail infFare = buildFareDetail(itinInfo, infantTst, segmentIndex);
				infant.setFareDetail(infFare);
				if (segmentIndex == 0) {
					parseFareComponents(infFare, infantTst);
				}
				if (StringUtils.isEmpty(travellerInfo.getTitle())) {
					setTitleAndType(infant, traveller.getEnhancedPassengerData()[1]);
				}
				bookingTravellerInfo.add(infant);
			}
		}
		bookingRelatedInfo.setTravellerInfo(bookingTravellerInfo);
	}

	private TstData_type0 getTst(TstData_type0[] tstData, String paxReference, Boolean infantIndicator) {
		for (TstData_type0 tst : tstData) {
			if (BooleanUtils.isTrue(infantIndicator)) {
				for (TstFreetext_type0 tstFreeText : tst.getTstFreetext()) {
					if (tstFreeText.getLongFreetext().getLongFreetext_type8().equals("INF")) {
						return tst;
					}
				}
			} else {
				for (Reference_type9 reference : tst.getReferenceForTstData().getReference()) {
					if (reference.getQualifier().getQualifier_type150().equals(PAX_QUALIFIER)) {
						if (reference.getNumber().getNumber_type64().equals(paxReference)) {
							return tst;
						}
					}
				}
			}
		}
		return null;
	}

	private PaxType getPaxType(EnhancedPassengerData_type1 enhancedTrav) {
		PaxType paxType = null;
		if (enhancedTrav != null && enhancedTrav.getEnhancedTravellerInformation() != null) {
			EnhancedTravellerInformation_type0 enhancedTraveller = enhancedTrav.getEnhancedTravellerInformation();
			if (enhancedTraveller.getTravellerNameInfo() != null
					&& enhancedTraveller.getTravellerNameInfo().getType() != null) {
				String type = enhancedTraveller.getTravellerNameInfo().getType().getType_type6();
				paxType = getPaxType(type);
				if (!paxType.equals(PaxType.INFANT)) {
					return paxType;
				}
			}
		}
		return null;
	}


	private void setTitleAndType(FlightTravellerInfo travellerInfo, EnhancedPassengerData_type1 enhancedTrav) {
		String firstName = enhancedTrav.getEnhancedTravellerInformation().getOtherPaxNamesDetails()[0].getGivenName()
				.getGivenName_type0();
		if (travellerInfo != null && StringUtils.isNotBlank(firstName)) {
			String paxTitle = "";
			for (String title : titleList) {
				if (StringUtils.endsWithIgnoreCase(firstName, title)) {
					paxTitle = title;
					firstName = firstName.substring(0, firstName.length() - title.length());
					break;
				}
			}
			travellerInfo.setFirstName(StringUtils.capitalize(firstName));
			travellerInfo.setTitle(StringUtils.capitalize(paxTitle));
		}
	}

	private boolean isInfantAvailable(EnhancedPassengerData_type1 enhancedTrav) {
		return enhancedTrav.getEnhancedTravellerInformation() != null
				&& enhancedTrav.getEnhancedTravellerInformation().getTravellerNameInfo() != null
				&& enhancedTrav.getEnhancedTravellerInformation().getTravellerNameInfo().getInfantIndicator() != null
				&& enhancedTrav.getEnhancedTravellerInformation().getTravellerNameInfo().getInfantIndicator()
						.getInfantIndicator_type2() != null;
	}

	private FareDetail buildFareDetail(ItineraryInfo_type1 itinInfo, TstData_type0 tst, int segmentIndex) {
		FareDetail fareDetail = new FareDetail();
		String cabinClass =
				itinInfo.getCabinDetails().getCabinDetails().getClassDesignator().getClassDesignator_type0();
		String classOfBooking =
				itinInfo.getTravelProduct().getProductDetails().getClassOfService().getClassOfService_type0();
		BaggageInfo baggageInfo = new BaggageInfo();
		fareDetail.setClassOfBooking(classOfBooking);
		fareDetail.setCabinClass(AmadeusUtils.getCabinClass(cabinClass));
		fareDetail.setBaggageInfo(baggageInfo);
		fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
		FareElement_type0 fareElement = getFareBasisInfo(tst, segmentIndex);
		if (fareElement != null) {
			baggageInfo.setAllowance(fareElement.getBaggageAllowance().getBaggageAllowance_type0());
			fareDetail.setFareBasis(fareElement.getPrimaryCode().getPrimaryCode_type0()
					.concat(Objects.nonNull(fareElement.getFareBasis())
							? fareElement.getFareBasis().getFareBasis_type0()
							: StringUtils.EMPTY));
		}
		return fareDetail;
	}

	private FareElement_type0 getFareBasisInfo(TstData_type0 tst, int segmentIndex) {
		if (segmentIndex + 1 <= tst.getFareBasisInfo().getFareElement().length) {
			return tst.getFareBasisInfo().getFareElement()[segmentIndex];
		} else if (ArrayUtils.isNotEmpty(tst.getFareBasisInfo().getFareElement())) {
			return tst.getFareBasisInfo().getFareElement()[0];
		}
		return null;
	}

	private void parseFareComponents(FareDetail fareDetail, TstData_type0 tst) {
		FareData_type0 fareData = tst.getFareData();
		Double baseFare = getFareData(getBaseFareQualifier(fareData), fareData);
		Double totalFare = getFareData(TOTALFARE_QUALIFIER, fareData);
		fareDetail.getFareComponents().put(FareComponent.BF, baseFare);
		fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		TaxFields_type0[] taxes = tst.getFareData().getTaxFields();
		Map<String, GDSFareComponentMapper> gdsFares = GDSFareComponentMapper.getGdsFareComponents();
		if (Objects.nonNull(taxes)) {
			for (TaxFields_type0 tax : taxes) {
				String fareType = tax.getTaxCountryCode().getTaxCountryCode_type0().trim();
				if (gdsFares.keySet().contains(fareType)) {
					fareDetail.getFareComponents()
							.put(gdsFares.get(fareType).getFareComponent(),
									(double) ObjectUtils.defaultIfNull(fareDetail.getFareComponents()
											.get(gdsFares.get(fareType).getFareComponent()), (double) 0)
											+ Double.valueOf(tax.getTaxAmount().getTaxAmount_type0()));
				} else {
					fareDetail.getFareComponents()
							.put(FareComponent.OC,
									(double) ObjectUtils.defaultIfNull(
											fareDetail.getFareComponents().get(FareComponent.OC), (double) 0)
											+ Double.valueOf(tax.getTaxAmount().getTaxAmount_type0()));
				}
			}
		}
		AtomicDouble totalAmount = new AtomicDouble(0);
		fareDetail.getFareComponents().forEach((fareComponent, amount) -> {
			if (!fareComponent.equals(FareComponent.TF))
				totalAmount.addAndGet(amount);
		});
		fareDetail.getFareComponents().put(FareComponent.TF, totalAmount.doubleValue());
	}

	private String getBaseFareQualifier(FareData_type0 fareData) {
		String qualifier = BASEFARE_QUALIFIER_D;
		if (fareData.getMonetaryInfo() != null && ArrayUtils.isNotEmpty(fareData.getMonetaryInfo())) {
			for (MonetaryInfo_type0 monetaryInfo : fareData.getMonetaryInfo()) {
				if (monetaryInfo.getQualifier().getQualifier_type148().equalsIgnoreCase(BASEFARE_QUALIFIER_I)) {
					qualifier = BASEFARE_QUALIFIER_I;
					break;
				}
			}
		}
		return qualifier;
	}

	private Double getFareData(String qualifier, FareData_type0 fareData) {
		Double amount = new Double(0);
		if (fareData.getMonetaryInfo() != null && ArrayUtils.isNotEmpty(fareData.getMonetaryInfo())) {
			for (MonetaryInfo_type0 monetaryInfo : fareData.getMonetaryInfo()) {
				if (monetaryInfo.getQualifier().getQualifier_type148().equalsIgnoreCase(qualifier)) {
					amount = Double.parseDouble(monetaryInfo.getAmount().getAmount_type122());
				}
			}
		}
		return amount;
	}


	private FlightDesignator getFlightDesignator(ItineraryInfo_type1 itineraryInfo) {
		FlightDesignator flightDesignator = FlightDesignator.builder().build();
		TravelProduct_type0 travelProduct = itineraryInfo.getTravelProduct();
		CompanyDetail_type0 marketingAirline = travelProduct.getCompanyDetail();
		ProductDetails_type1 productDetails = itineraryInfo.getFlightDetail().getProductDetails();
		flightDesignator.setAirlineInfo(
				AirlineHelper.getAirlineInfo(marketingAirline.getIdentification().getIdentification_type0()));
		flightDesignator.setEquipType(productDetails.getEquipment().getEquipment_type0());
		flightDesignator
				.setFlightNumber(travelProduct.getProductDetails().getIdentification().getIdentification_type2());
		return flightDesignator;
	}

	private PNR_Retrieve buildRetreiveRq(String supplierPnr) {
		PNR_Retrieve pnrRetrieveRq = new PNR_Retrieve();
		RetrievalFacts_type0 retrievalFacts = new RetrievalFacts_type0();
		pnrRetrieveRq.setRetrievalFacts(retrievalFacts);
		ReservationControlInformationType reservationControlInfo = new ReservationControlInformationType();
		ReservationControlInformationDetailsType[] reservationInfo = new ReservationControlInformationDetailsType[1];
		reservationInfo[0] = new ReservationControlInformationDetailsType();
		reservationInfo[0].setControlNumber(getAlpha1to20(supplierPnr));
		reservationControlInfo.setReservation(reservationInfo);
		retrievalFacts.setReservationOrProfileIdentifier(reservationControlInfo);
		RetrievePNRType pnrType = new RetrievePNRType();
		pnrType.setType(getAlpha1To2(RETRIEVE_PNR_TYPE));
		retrievalFacts.setRetrieve(pnrType);
		return pnrRetrieveRq;
	}

	private void setArrivalDetails(SegmentInfo segmentInfo, ItineraryInfo_type1 itinInfo) {
		TravelProduct_type0 travelProduct = itinInfo.getTravelProduct();
		segmentInfo.setArrivalAirportInfo(
				AirportHelper.getAirportInfo(travelProduct.getOffpointDetail().getCityCode().getCityCode_type4()));
		segmentInfo
				.setArrivalTime(AmadeusUtils.parseDateTime(travelProduct.getProduct().getArrDate().getArrDate_type0(),
						travelProduct.getProduct().getArrTime().getArrTime_type0()));
		if (itinInfo.getFlightDetail() != null) {
			if (itinInfo.getFlightDetail().getArrivalStationInfo() != null
					&& itinInfo.getFlightDetail().getArrivalStationInfo().getTerminal() != null) {
				segmentInfo.getArrivalAirportInfo().setTerminal(AirUtils.getTerminalInfo(
						itinInfo.getFlightDetail().getArrivalStationInfo().getTerminal().getTerminal_type0()));
			}
		}
	}

	private void setDepartureDetails(SegmentInfo segmentInfo, ItineraryInfo_type1 itinInfo) {
		TravelProduct_type0 travelProduct = itinInfo.getTravelProduct();
		segmentInfo.setDepartAirportInfo(
				AirportHelper.getAirportInfo(travelProduct.getBoardpointDetail().getCityCode().getCityCode_type2()));
		segmentInfo.setDepartTime(AmadeusUtils.parseDateTime(travelProduct.getProduct().getDepDate().getDepDate_type0(),
				travelProduct.getProduct().getDepTime().getDepTime_type0()));
		if (itinInfo.getFlightDetail() != null) {
			if (itinInfo.getFlightDetail().getDepartureInformation() != null
					&& itinInfo.getFlightDetail().getDepartureInformation().getDepartTerminal() != null) {
				segmentInfo.getDepartAirportInfo().setTerminal(AirUtils.getTerminalInfo(itinInfo.getFlightDetail()
						.getDepartureInformation().getDepartTerminal().getDepartTerminal_type0()));
			}
		}
	}


	private AlphaNumericString_Length1To20 getAlpha1to20(String code) {
		AlphaNumericString_Length1To20 length1To20 = new AlphaNumericString_Length1To20();
		length1To20.setAlphaNumericString_Length1To20(code);
		return length1To20;
	}

	private NumericInteger_Length1To2 getAlpha1To2(String code) {
		NumericInteger_Length1To2 length1To2 = new NumericInteger_Length1To2();
		length1To2.setNumericInteger_Length1To2(new BigInteger(code));
		return length1To2;
	}
}
