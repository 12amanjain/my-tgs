package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.AmadeusReviewWebServicesStub;
import com.amadeus.xml.itareq_05_2_ia.*;
import com.amadeus.xml.itares_05_2_ia.*;
import com.amadeus.xml.satrqt_13_2_1a.ProductClassDetails_type0;
import com.amadeus.xml.tipnrq_15_1_1a.*;
import com.amadeus.xml.tipnrr_15_1_1a.*;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.axiom.om.OMException;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.StringJoiner;
import java.util.List;
import java.util.Map;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Set;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

@Setter
@Getter
@Slf4j
@SuperBuilder
final class AmadeusReviewManager extends AmadeusServiceManager {

	protected AmadeusReviewWebServicesStub servicesStub;

	protected static final String INFANT_PAX_QUALIFIER = "766";
	public static final String NON_REFUNDABLE_P1 = "1P1";
	public static final String NON_REFUNDABLE_P2 = "1P2";
	public static final String NON_REFUNDABLE_P5 = "1P5";
	// Equivalent Fare Which Converted By AMdeus When Airline Filed in Diff Currency
	public static final String EQUI_BASEFARE_QUALIFIER = "E";
	// B Qualifier Should use when Airline Filed Currency in Expected Currency
	public static final String BASE_FARE_QUALIFIER = "B";

	public static final String MESSGAGE_FUNCTION = "183";
	public static final String MESSAGE_ADDITIONAL = "M1";
	public static final String STATUS_CODE = "NN";
	public static final String OK_STATUS_CODE = "OK";
	public static final String SEGMENT_REFERENCE_TYPE = "ST";

	public TripInfo doFareInformativePricing(TripInfo selectedTrip) {
		Fare_InformativePricingWithoutPNR priceRequest = null;
		Fare_InformativePricingWithoutPNRReply fare_Reply = null;
		boolean isDomReturn = searchQuery.isDomesticReturn() && AirUtils.splitTripInfo(selectedTrip, false).size() == 2;
		try {
			listener.setType("FareInformativePricingWithoutPNR");
			servicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			priceRequest = buildFareInformativePricingRequest(selectedTrip);
			fare_Reply = servicesStub.fare_InformativePricingWithoutPNR(priceRequest, getSessionSchema());
			if (!checkFareInformativePricingError(fare_Reply)) {
				selectedTrip = fareFareInformativePricing(fare_Reply, selectedTrip, isDomReturn);
				AmadeusUtils.setTotalFareOnTripInfo(Arrays.asList(selectedTrip));
			}
		} catch (AxisFault af) {
			throw new NoSeatAvailableException(af.getMessage());
		} catch (OMException om) {
			throw new NoSeatAvailableException(om.getMessage());
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			servicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return selectedTrip;
	}

	private boolean checkFareInformativePricingError(Fare_InformativePricingWithoutPNRReply fareReply) {
		boolean isError = false;
		if (fareReply == null || (fareReply != null && fareReply.getErrorGroup() != null)) {
			FreeTextInformationType freeText = fareReply.getErrorGroup().getErrorWarningDescription();
			StringJoiner msg = new StringJoiner("");
			if (freeText != null && ArrayUtils.isNotEmpty(freeText.getFreeText())) {
				isError = true;
				for (AlphaNumericString_Length1To199 errorOrWarning : freeText.getFreeText()) {
					msg.add(errorOrWarning.getAlphaNumericString_Length1To199());
				}
			}
			log.error("FareInformativePricingError Failed for Booking {} error {}", bookingId, msg.toString());
			if (criticalMessageLogger != null && StringUtils.isNotBlank(msg.toString())) {
				criticalMessageLogger.add(msg.toString());
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
		}
		return isError;
	}

	private TripInfo fareFareInformativePricing(Fare_InformativePricingWithoutPNRReply reviewResponse,
			TripInfo tripInfo, boolean isDomReturn) {
		Map<String, List<String>> fareBasisStopPoint = new HashMap<>();
		AtomicReference<String> accountCode = new AtomicReference<>();
		if (reviewResponse != null && reviewResponse.getMainGroup() != null) {
			Map<String, Set<String>> parsedSegments = new HashMap<>();
			PricingGroupLevelGroup_type0[] pricingGroups = reviewResponse.getMainGroup().getPricingGroupLevelGroup();
			Map<PaxType, PricingGroupLevelGroup_type0> selectedSearchPricing =
					filterPricingGroups(tripInfo.getSegmentInfos(), pricingGroups);
			selectedSearchPricing.forEach((paxType, priceGroup) -> {
				FareInfoGroup_type0 fareInfoGroup = priceGroup.getFareInfoGroup();
				InteractiveFreeTextTypeI[] pricingText = fareInfoGroup.getTextData();
				MonetaryInformationType_187661S fareInfo = fareInfoGroup.getFareAmount();
				RefundableType refundableType = getRefundableType(pricingText);
				SegmentLevelGroup_type0[] segmentLevelGroups = fareInfoGroup.getSegmentLevelGroup();
				buildPrimitiveFare(segmentLevelGroups, tripInfo.getSegmentInfos(), paxType, priceGroup, refundableType);
				PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
				FareDetail fareDetail = new GsonMapper<>(priceInfo.getFareDetail(paxType), FareDetail.class).convert();
				Double baseFare = getBaseFare(fareInfo);
				TaxTypeI taxGroups = fareInfoGroup.getSurchargesGroup().getTaxesAmount();
				if (fareInfoGroup.getCorporateGroup() != null
						&& ArrayUtils.isNotEmpty(fareInfoGroup.getCorporateGroup())) {
					CorporateGroup_type0[] corporateGroup = fareInfoGroup.getCorporateGroup();
					for (CorporateGroup_type0 corp : corporateGroup) {
						if (corp.getCorporateData() != null) {
							accountCode
									.set(corp.getCorporateData().getLocationCode().getAlphaNumericString_Length1To25());
						}
					}
				}
				Map<FareComponent, Double> taxComponents = getTaxBreakUp(taxGroups, fareDetail);
				if (isDomReturn) {
					// split half fare
					tripInfo.getSegmentInfos().forEach(segmentInfo -> {
						if (segmentInfo.getSegmentNum().intValue() == 0) {
							PriceInfo priceInfo1 = segmentInfo.getPriceInfo(0);
							priceInfo1.setFareIdentifier(FareType.SPECIAL_RETURN);
							FareDetail newFareDetail = priceInfo1.getFareDetail(paxType);
							newFareDetail.getFareComponents().put(FareComponent.BF, baseFare);
							addTaxToFareDetail(taxComponents, newFareDetail);
							AmadeusUtils.adjustFareToHalf(newFareDetail);
							priceInfo1.getFareDetails().put(paxType, newFareDetail);
						}
					});
				} else {
					fareDetail.getFareComponents().put(FareComponent.BF, baseFare);
					fareDetail.getFareComponents().putAll(taxComponents);
					priceInfo.getFareDetails().put(paxType, fareDetail);
				}
				if (accountCode.get() != null && StringUtils.isNotBlank(accountCode.get())) {
					priceInfo.getMiscInfo().setAccountCode(accountCode.get());
					priceInfo.getMiscInfo().setIsPrivateFare(Boolean.TRUE);
				}
				if (StringUtils.isNotBlank(tripInfo.getPlatingCarrier())) {
					AirlineInfo plattingCarrier = AirlineHelper.getAirlineInfo(tripInfo.getPlatingCarrier());
					priceInfo.getMiscInfo().setPlatingCarrier(plattingCarrier);
				}
				// buildFBABreakPoints(fareBasisStopPoint, parsedSegments, fareInfoGroup);
			});
		}
		return tripInfo;
	}

	private void addTaxToFareDetail(Map<FareComponent, Double> taxComponents, FareDetail fareDetail) {
		taxComponents.forEach((taxComp, amount) -> {
			fareDetail.getFareComponents().put(taxComp, amount);
		});
	}

	private void buildFBABreakPoints(Map<String, List<String>> fareBasisStopPoint,
			Map<String, Set<String>> parsedSegments, FareInfoGroup_type0 fareInfoGroup) {
		if (ArrayUtils.isNotEmpty(fareInfoGroup.getFareComponentDetailsGroup())) {
			for (FareComponentDetailsType fareComponentDetailsGrp : fareInfoGroup.getFareComponentDetailsGroup()) {
				if (Objects.nonNull(fareComponentDetailsGrp.getComponentClassInfo())
						&& Objects.nonNull(fareComponentDetailsGrp.getComponentClassInfo().getFareBasisDetails())) {
					String fareBasis = fareComponentDetailsGrp.getComponentClassInfo().getFareBasisDetails()
							.getRateTariffClass().getAlphaNumericString_Length1To35();
					if (!parsedSegments.containsKey(fareBasis)) {
						parsedSegments.put(fareBasis, new HashSet<>());
					}

					if (ArrayUtils.isNotEmpty(fareComponentDetailsGrp.getCouponDetailsGroup())) {
						boolean isSegAdded = false;
						for (CouponDetailsType couponDetails : fareComponentDetailsGrp.getCouponDetailsGroup()) {
							if (couponDetails.getProductId().getReferenceDetails().getType()
									.getAlphaNumericString_Length1To10().equalsIgnoreCase(SEGMENT_REFERENCE_TYPE)) {
								if (!parsedSegments.get(fareBasis).contains(couponDetails.getProductId()
										.getReferenceDetails().getValue().getAlphaNumericString_Length1To60())) {
									parsedSegments.get(fareBasis).add(couponDetails.getProductId().getReferenceDetails()
											.getValue().getAlphaNumericString_Length1To60());
									isSegAdded = true;
									if (!fareBasisStopPoint.containsKey(fareBasis)) {
										fareBasisStopPoint.put(fareBasis, new ArrayList<>());
									}
									fareBasisStopPoint.get(fareBasis)
											.add(String
													.valueOf(Integer
															.valueOf(couponDetails.getProductId().getReferenceDetails()
																	.getValue().getAlphaNumericString_Length1To60())
															- 1));
								}
							}
						}
						if (isSegAdded) {
							fareBasisStopPoint.get(fareBasis).add("Y");
						}
					}
				}
			}
		}
	}

	private Map<FareComponent, Double> getTaxBreakUp(TaxTypeI taxGroups, FareDetail fareDetail) {
		Map<FareComponent, Double> taxComp = fareDetail.getFareComponents();
		Map<String, GDSFareComponentMapper> gdsFares = GDSFareComponentMapper.getGdsFareComponents();
		if (Objects.nonNull(taxGroups) && ArrayUtils.isNotEmpty(taxGroups.getTaxDetails())) {
			TaxDetailsTypeI[] taxDetails = taxGroups.getTaxDetails();
			for (TaxDetailsTypeI tax : taxDetails) {
				String taxCode = tax.getCountryCode().getAlphaNumericString_Length1To3().trim();
				Double currentRate = Double.parseDouble(tax.getRate().getAlphaNumericString_Length1To17());
				String currencyCode =
						tax.getCurrencyCode() != null ? tax.getCurrencyCode().getAlphaNumericString_Length1To3()
								: getCurrencyCode();
				if (gdsFares.containsKey(taxCode)) {
					Double previousAmount =
							taxComp.getOrDefault(gdsFares.get(taxCode).getFareComponent(), new Double(0));
					Double taxFare = getAmountBasedOnCurrency(currentRate, currencyCode);
					taxComp.put(gdsFares.get(taxCode).getFareComponent(), previousAmount + taxFare);
				} else {
					Double previousAmount = taxComp.getOrDefault(FareComponent.OC, new Double(0));
					Double taxFare = getAmountBasedOnCurrency(currentRate, currencyCode);
					taxComp.put(FareComponent.OC, previousAmount + taxFare);
				}
			}
		}
		return taxComp;
	}

	private Double getBaseFare(MonetaryInformationType_187661S monetary) {
		Double baseFare = new Double(0);
		if (monetary != null && ArrayUtils.isNotEmpty(monetary.getOtherMonetaryDetails())) {
			for (MonetaryInformationDetailsType_223867C monetaryInfo : monetary.getOtherMonetaryDetails()) {
				if (monetaryInfo != null && monetaryInfo.getTypeQualifier() != null && monetaryInfo.getTypeQualifier()
						.getAlphaNumericString_Length1To3().equalsIgnoreCase(EQUI_BASEFARE_QUALIFIER)) {
					Double amount = Double.parseDouble(monetaryInfo.getAmount().getAlphaNumericString_Length1To35());
					String currencyCode = monetaryInfo.getCurrency() != null
							? monetaryInfo.getCurrency().getAlphaNumericString_Length1To3()
							: getCurrencyCode();
					baseFare = getAmountBasedOnCurrency(amount, currencyCode);
					return baseFare;
				}
			}
		}
		if (monetary != null && monetary.getMonetaryDetails() != null
				&& monetary.getMonetaryDetails().getTypeQualifier() != null && monetary.getMonetaryDetails()
						.getTypeQualifier().getAlphaNumericString_Length1To3().equalsIgnoreCase(BASE_FARE_QUALIFIER)) {
			MonetaryInformationDetailsType_223867C monetaryInfo = monetary.getMonetaryDetails();
			Double amount = Double.parseDouble(monetaryInfo.getAmount().getAlphaNumericString_Length1To35());
			baseFare = amount;
		}
		return baseFare;
	}

	private RefundableType getRefundableType(InteractiveFreeTextTypeI[] pricingText) {
		RefundableType refundableType = RefundableType.REFUNDABLE;
		if (ArrayUtils.isNotEmpty(pricingText)) {
			for (InteractiveFreeTextTypeI text : pricingText) {
				if (text.getFreeTextQualification() != null
						&& text.getFreeTextQualification().getInformationType() != null) {
					String textData =
							text.getFreeTextQualification().getInformationType().getAlphaNumericString_Length1To4();
					if (textData.equalsIgnoreCase(NON_REFUNDABLE_P1) || textData.equalsIgnoreCase(NON_REFUNDABLE_P2)
							|| textData.equalsIgnoreCase(NON_REFUNDABLE_P5)) {
						refundableType = RefundableType.NON_REFUNDABLE;
						break;
					}
				}
			}
		}
		return refundableType;
	}

	private Map<PaxType, PricingGroupLevelGroup_type0> filterPricingGroups(List<SegmentInfo> segmentInfos,
			PricingGroupLevelGroup_type0[] pricingGroups) {
		Map<PaxType, PricingGroupLevelGroup_type0> filteredPriceMap = new LinkedHashMap<>();
		for (PricingGroupLevelGroup_type0 priceLevel : pricingGroups) {
			PaxType paxType = getPricingGroupPaxType(priceLevel);
			PriceInfo pricingInfo = segmentInfos.get(0).getPriceInfo(0);
			FareDetail fareDetail = pricingInfo.getFareDetail(paxType);
			if (fareDetail != null && AmadeusUtils.isPaxTypeExists(searchQuery, paxType)
					&& !filteredPriceMap.containsKey(paxType)) {
				filteredPriceMap.put(paxType, priceLevel);
			}
		}
		return filteredPriceMap;
	}

	private PaxType getPricingGroupPaxType(PricingGroupLevelGroup_type0 priceLevel) {
		String paxCode = priceLevel.getFareInfoGroup().getSegmentLevelGroup()[0].getPtcSegment().getQuantityDetails()
				.getUnitQualifier().getAlphaNumericString_Length1To3();
		return getPaxType(paxCode);
	}

	private String getFareBasis(PricingGroupLevelGroup_type0 priceLevel) {
		return priceLevel.getFareInfoGroup().getSegmentLevelGroup()[0].getFareBasis().getAdditionalFareDetails()
				.getRateClass().getAlphaNumericString_Length1To35();
	}

	private String getClassOfBook(SegmentLevelGroup_type0 segmentGroup) {
		return segmentGroup.getSegmentInformation().getFlightIdentification().getBookingClass()
				.getAlphaNumericString_Length1To17();
	}

	private void buildPrimitiveFare(SegmentLevelGroup_type0[] segmentLevelGroups, List<SegmentInfo> segmentInfos,
			PaxType paxType, PricingGroupLevelGroup_type0 priceGroup, RefundableType refundableType) {
		if (ArrayUtils.isNotEmpty(segmentLevelGroups) && CollectionUtils.isNotEmpty(segmentInfos)) {

			for (SegmentLevelGroup_type0 segmentGroup : segmentLevelGroups) {
				SegmentInfo segmentInfo = getSegmentInfo(segmentGroup, segmentInfos);
				if (segmentInfo != null) {
					PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
					FareDetail fareDetail = priceInfo.getFareDetail(paxType);
					fareDetail.setFareComponents(new HashMap<>());
					fareDetail.setFareBasis(getFareBasis(priceGroup));
					fareDetail.setClassOfBooking(getClassOfBook(segmentGroup));
					fareDetail.setRefundableType(refundableType.getRefundableType());
					fareDetail.setBaggageInfo(buildBaggaeInfo(segmentGroup));
					fareDetail.setCabinClass(getCabinClass(segmentGroup));
				}
			}
		}
	}

	private SegmentInfo getSegmentInfo(SegmentLevelGroup_type0 segmentGroup, List<SegmentInfo> segmentInfos) {
		SegmentInfo segmentInfo = null;
		TravelProductInformationTypeI_69238S productInfo = segmentGroup.getSegmentInformation();
		ProductDateTimeTypeI productDateTime = productInfo.getFlightDate();
		String boardPoint = productInfo.getBoardPointDetails().getTrueLocationId().getAlphaNumericString_Length1To25();
		String offPoint = productInfo.getOffpointDetails().getTrueLocationId().getAlphaNumericString_Length1To25();
		String departureDate = productDateTime.getDepartureDate().getAlphaNumericString_Length1To35();
		// String depatureTime = productDateTime.getDepartureTime().getNumericInteger_Length1To4().toString();
		LocalDateTime departureTime = AmadeusUtils.parseDateTime(departureDate, "0000");
		for (SegmentInfo segmentInfo1 : segmentInfos) {
			if (segmentInfo1.getDepartureAirportCode().equalsIgnoreCase(boardPoint)
					&& segmentInfo1.getArrivalAirportCode().equalsIgnoreCase(offPoint)
					&& segmentInfo1.getDepartTime().toLocalDate().equals(departureTime.toLocalDate())) {
				segmentInfo = segmentInfo1;
				break;
			}
		}
		return segmentInfo;
	}

	private Fare_InformativePricingWithoutPNR buildFareInformativePricingRequest(TripInfo selectedTrip) {
		Fare_InformativePricingWithoutPNR request = new Fare_InformativePricingWithoutPNR();
		request.setSegmentGroup(buildSegmentGroups(selectedTrip));
		request.setPassengersGroup(buildPassengerGroup());
		request.setPricingOptionGroup(buildPricingOptions(selectedTrip));
		return request;
	}

	private PricingOptionGroup_type0[] buildPricingOptions(TripInfo selectedTrip) {
		int pricingSize = StringUtils.isNotBlank(getSupplierCurrency()) ? 4 : 3;
		PricingOptionGroup_type0[] pricingOptions = new PricingOptionGroup_type0[pricingSize];
		// validating Carrier
		pricingOptions[0] = new PricingOptionGroup_type0();
		pricingOptions[0].setPricingOptionKey(buildPricingOption(AmadeusConstants.VALIDATING_CARRIER));
		pricingOptions[0].setCarrierInformation(buildCarrierInformation(selectedTrip));
		// fare Type - Public or Corp
		pricingOptions[1] = new PricingOptionGroup_type0();
		if (isCorporateFare(selectedTrip.getSegmentInfos())) {
			pricingOptions[1].setPricingOptionKey(buildPricingOption(AmadeusConstants.CORPORATE_FARE_TYPE));
			List<String> corporateCodes = getCorporateCode(selectedTrip.getSegmentInfos());
			OptionDetail_type0 optionDetail = new OptionDetail_type0();
			optionDetail.setCriteriaDetails(buildCriteriaDetails(corporateCodes));
			pricingOptions[1].setOptionDetail(optionDetail);
		} else {
			pricingOptions[1].setPricingOptionKey(buildPricingOption(AmadeusConstants.PUBLISHED_FARE_TYPE));
		}
		pricingOptions[2] = new PricingOptionGroup_type0();
		pricingOptions[2].setPricingOptionKey(buildPricingOption(AmadeusConstants.CHEAPEST_FARE_TYPE));
		if (StringUtils.isNotBlank(getSupplierCurrency())) {
			pricingOptions[3] = new PricingOptionGroup_type0();
			pricingOptions[3].setPricingOptionKey(buildCurrencyOptions());
			pricingOptions[3].setCurrency(getPricingCurrencyDetails());
		}
		return pricingOptions;
	}

	private Currency_type32 getPricingCurrencyDetails() {
		Currency_type32 currencyType = new Currency_type32();
		currencyType.setFirstCurrencyDetails(getFirstCurrencyDetails());
		return currencyType;
	}

	private FirstCurrencyDetails_type0 getFirstCurrencyDetails() {
		FirstCurrencyDetails_type0 firstCurrencyDetails = new FirstCurrencyDetails_type0();
		firstCurrencyDetails.setCurrencyIsoCode(getCurrencyIsoCode());
		firstCurrencyDetails.setCurrencyQualifier(getCurrencyQualifier());
		return firstCurrencyDetails;
	}

	private CurrencyIsoCode_type1 getCurrencyIsoCode() {
		CurrencyIsoCode_type1 currencyIsoCode = new CurrencyIsoCode_type1();
		currencyIsoCode.setCurrencyIsoCode_type0(getCurrencyCode());
		return currencyIsoCode;
	}

	private CurrencyQualifier_type1 getCurrencyQualifier() {
		CurrencyQualifier_type1 qualifier = new CurrencyQualifier_type1();
		qualifier.setCurrencyQualifier_type0(AmadeusConstants.FARE_CURRENCY_OVERRIDE);
		return qualifier;
	}

	private PricingOptionKey_type2 buildCurrencyOptions() {
		PricingOptionKey_type2 pricingOption = new PricingOptionKey_type2();
		PricingOptionKey_type1 pricingKey = new PricingOptionKey_type1();
		pricingKey.setPricingOptionKey_type0(AmadeusConstants.FARE_CURRENCY_OVERRIDE);
		pricingOption.setPricingOptionKey(pricingKey);
		return pricingOption;
	}

	private CriteriaDetails_type0[] buildCriteriaDetails(List<String> corporateCodes) {
		CriteriaDetails_type0[] criteriaDetails = new CriteriaDetails_type0[corporateCodes.size()];
		int corpIndex = 0;
		for (String corpCode : corporateCodes) {
			criteriaDetails[corpIndex] = new CriteriaDetails_type0();
			AttributeType_type1 attributeType = new AttributeType_type1();
			attributeType.setAttributeType_type0(corpCode);
			criteriaDetails[corpIndex].setAttributeType(attributeType);
			corpIndex++;
		}
		return criteriaDetails;
	}

	private CarrierInformation_type0 buildCarrierInformation(TripInfo selectedTrip) {
		CarrierInformation_type0 carrierInformation = new CarrierInformation_type0();
		CompanyIdentification_type10 companyId = new CompanyIdentification_type10();
		OtherCompany_type33 company = new OtherCompany_type33();
		company.setOtherCompany_type32(selectedTrip.getPlatingCarrier());
		companyId.setOtherCompany(company);
		carrierInformation.setCompanyIdentification(companyId);
		return carrierInformation;
	}

	private PricingOptionKey_type2 buildPricingOption(String code) {
		PricingOptionKey_type2 pricingOption = new PricingOptionKey_type2();
		PricingOptionKey_type1 key = new PricingOptionKey_type1();
		key.setPricingOptionKey_type0(code);
		pricingOption.setPricingOptionKey(key);
		return pricingOption;
	}

	private PassengersGroup_type0[] buildPassengerGroup() {
		int groupSize = AmadeusUtils.getPaxCount(searchQuery.getPaxInfo());
		PassengersGroup_type0[] passengersGroup = new PassengersGroup_type0[groupSize];
		AtomicInteger paxGroupIndex = new AtomicInteger(1);
		AtomicInteger paxMSRCode = new AtomicInteger(1);
		AtomicInteger paxIndex = new AtomicInteger(0);
		searchQuery.getPaxInfo().forEach(((paxType, paxCount) -> {
			if (paxCount > 0) {
				PassengersGroup_type0 passengerType = new PassengersGroup_type0();
				DiscountPtc_type0 ptcType = buildDiscountPTC(paxType);
				SegmentRepetitionControl_type0 segmentControl = buildSegmentRepetitionControl(paxCount, paxGroupIndex);
				TravellersID_type0 travellersID = buildTravellerId(paxCount, paxMSRCode, paxType);
				passengerType.setDiscountPtc(ptcType);
				passengerType.setSegmentRepetitionControl(segmentControl);
				passengerType.setTravellersID(travellersID);
				paxGroupIndex.getAndIncrement();
				passengersGroup[paxIndex.get()] = passengerType;
				paxIndex.getAndIncrement();
			}
		}));
		return passengersGroup;
	}

	private TravellersID_type0 buildTravellerId(Integer paxCount, AtomicInteger paxMSRCode, PaxType paxType) {
		if (paxType.equals(PaxType.INFANT)) {
			paxMSRCode.set(1);
		}
		TravellersID_type0 travellersID = new TravellersID_type0();
		TravellerDetails_type0[] travellerDetails = new TravellerDetails_type0[paxCount];
		int index = 0;
		while (index < paxCount) {
			travellerDetails[index] = new TravellerDetails_type0();
			MeasurementValue_type21 measurementValue = new MeasurementValue_type21();
			measurementValue.setMeasurementValue_type20(new BigDecimal(paxMSRCode.get()));
			travellerDetails[index].setMeasurementValue(measurementValue);
			paxMSRCode.getAndIncrement();
			index++;
		}
		travellersID.setTravellerDetails(travellerDetails);
		return travellersID;
	}

	private SegmentRepetitionControl_type0 buildSegmentRepetitionControl(Integer paxCount,
			AtomicInteger paxGroupIndex) {
		SegmentRepetitionControl_type0 sRControl = new SegmentRepetitionControl_type0();
		SegmentControlDetails_type1[] detailsType = new SegmentControlDetails_type1[1];
		detailsType[0] = new SegmentControlDetails_type1();
		Quantity_type7 quantity = new Quantity_type7();
		quantity.setQuantity_type6(new BigDecimal(paxGroupIndex.get()));
		NumberOfUnits_type4 numberOfUnits = new NumberOfUnits_type4();
		numberOfUnits.setNumberOfUnits_type3(new BigDecimal(paxCount));
		detailsType[0].setQuantity(quantity);
		detailsType[0].setNumberOfUnits(numberOfUnits);
		sRControl.setSegmentControlDetails(detailsType);
		return sRControl;
	}

	private DiscountPtc_type0 buildDiscountPTC(PaxType paxType) {
		DiscountPtc_type0 discountPtc = new DiscountPtc_type0();
		ValueQualifier_type1 valueQualifier = new ValueQualifier_type1();
		valueQualifier.setValueQualifier_type0(getPaxType(paxType));
		discountPtc.setValueQualifier(valueQualifier);
		if (paxType.equals(PaxType.INFANT)) {
			FareDetails_type9 fareDetails = new FareDetails_type9();
			Qualifier_type63 qualifier = new Qualifier_type63();
			qualifier.setQualifier_type62(INFANT_PAX_QUALIFIER);
			fareDetails.setQualifier(qualifier);
			discountPtc.setFareDetails(fareDetails);
		}
		return discountPtc;
	}

	private SegmentGroup_type0[] buildSegmentGroups(TripInfo selectedTrip) {
		List<SegmentInfo> segmentInfos = selectedTrip.getSegmentInfos();
		SegmentGroup_type0[] segmentGroups = new SegmentGroup_type0[segmentInfos.size()];
		int segmentIndex = 0;
		for (SegmentInfo segmentInfo : segmentInfos) {
			SegmentGroup_type0 segmentGroup = new SegmentGroup_type0();
			SegmentInformation_type2 segmentInformation = new SegmentInformation_type2();
			segmentInformation.setBoardPointDetails(getBoardingPoint(segmentInfo));
			segmentInformation.setOffpointDetails(getOffPoint(segmentInfo));
			segmentInformation.setFlightDate(getFlightDate(segmentInfo));
			segmentInformation.setCompanyDetails(getMarketingAirlineDetails(segmentInfo));
			segmentInformation.setFlightIdentification(getFlightIdentification(segmentInfo));
			segmentInformation.setFlightTypeDetails(getFlightTypeDetail(segmentInfo));
			segmentGroup.setSegmentInformation(segmentInformation);
			segmentGroups[segmentIndex] = segmentGroup;
			segmentIndex++;
		}
		return segmentGroups;
	}

	private FlightTypeDetails_type4 getFlightTypeDetail(SegmentInfo segmentInfo) {
		FlightTypeDetails_type4 flightType = new FlightTypeDetails_type4();
		FlightIndicator_type9[] indicator = new FlightIndicator_type9[1];
		indicator[0] = new FlightIndicator_type9();
		indicator[0].setFlightIndicator_type8(String.valueOf(segmentInfo.getSegmentNum() + 1));
		flightType.setFlightIndicator(indicator);
		return flightType;
	}

	private FlightDate_type4 getFlightDate(SegmentInfo segmentInfo) {
		FlightDate_type4 flightDate = new FlightDate_type4();
		DepartureDate_type25 departureDate = new DepartureDate_type25();
		DepartureTime_type9 departureTime = new DepartureTime_type9();
		departureDate.setDepartureDate_type24(AmadeusUtils.formatToDDMMYY(segmentInfo.getDepartTime().toLocalDate()));
		departureTime
				.setDepartureTime_type8(new BigDecimal(AmadeusUtils.formatToAmadeusTime(segmentInfo.getDepartTime())));
		flightDate.setDepartureDate(departureDate);
		flightDate.setDepartureTime(departureTime);
		return flightDate;
	}

	private FlightIdentification_type6 getFlightIdentification(SegmentInfo segmentInfo) {
		FlightIdentification_type6 flightIdentification = new FlightIdentification_type6();
		FlightNumber_type29 flightNumber = new FlightNumber_type29();
		flightNumber.setFlightNumber_type28(segmentInfo.getFlightNumber());
		BookingClass_type5 bookingClass = new BookingClass_type5();
		bookingClass.setBookingClass_type4(getBookingClass(segmentInfo));
		flightIdentification.setFlightNumber(flightNumber);
		flightIdentification.setBookingClass(bookingClass);
		return flightIdentification;
	}

	private CompanyDetails_type9 getMarketingAirlineDetails(SegmentInfo segmentInfo) {
		CompanyDetails_type9 companyDetails = new CompanyDetails_type9();
		MarketingCompany_type35 marketingCompany = new MarketingCompany_type35();
		marketingCompany.setMarketingCompany_type34(segmentInfo.getAirlineCode(false));
		AirlineInfo operatingAirline = segmentInfo.getOperatedByAirlineInfo();
		if (operatingAirline != null) {
			OperatingCompany_type3 operatingCompany = new OperatingCompany_type3();
			operatingCompany.setOperatingCompany_type2(operatingAirline.getCode());
			companyDetails.setOperatingCompany(operatingCompany);
		}
		companyDetails.setMarketingCompany(marketingCompany);
		return companyDetails;
	}

	private OffpointDetails_type4 getOffPoint(SegmentInfo segmentInfo) {
		OffpointDetails_type4 offPoint = new OffpointDetails_type4();
		TrueLocationId_type21 locationId = new TrueLocationId_type21();
		locationId.setTrueLocationId_type20(segmentInfo.getArrivalAirportCode());
		offPoint.setTrueLocationId(locationId);
		return offPoint;
	}

	private BoardPointDetails_type4 getBoardingPoint(SegmentInfo segmentInfo) {
		BoardPointDetails_type4 boardPoint = new BoardPointDetails_type4();
		TrueLocationId_type19 locationId = new TrueLocationId_type19();
		locationId.setTrueLocationId_type18(segmentInfo.getDepartureAirportCode());
		boardPoint.setTrueLocationId(locationId);
		return boardPoint;
	}

	public boolean doSellRecommendation(TripInfo selectedTrip) {
		Air_SellFromRecommendation sellRq = null;
		Air_SellFromRecommendationReply sellRs = null;
		try {
			listener.setType("AirSellFromRecommendation");
			servicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			sellRq = buildSellRequest(selectedTrip);
			sellRs = servicesStub.air_SellFromRecommendation(sellRq, getSessionSchema());
			if (checkSellErrorMessage(sellRs))
				throw new NoSeatAvailableException("AirSellFromRecommendation Sell Failed " + bookingId);
			return sellCheckStatusOK(sellRs);
		} catch (AxisFault af) {
			throw new NoSeatAvailableException(af.getMessage());
		} catch (OMException om) {
			throw new NoSeatAvailableException(om.getMessage());
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			servicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private boolean sellCheckStatusOK(Air_SellFromRecommendationReply sellRs) {
		boolean isSellSuccess = false;
		if (sellRs != null && sellRs.getItineraryDetail() != null
				&& ArrayUtils.isNotEmpty(sellRs.getItineraryDetail())) {
			ItineraryDetail_type0[] itinDetails = sellRs.getItineraryDetail();
			for (ItineraryDetail_type0 detail : itinDetails) {
				for (SegmentInformation_type1 segmentInfo : detail.getSegmentInformation()) {
					ActionDetails_type0 actionDetails = segmentInfo.getActionDetails();
					StatusCode_type5[] statusCode = actionDetails.getStatusCode();
					if (statusCode != null && statusCode[0].getStatusCode_type4() != null
							&& statusCode[0].getStatusCode_type4().equalsIgnoreCase(OK_STATUS_CODE)) {
						isSellSuccess = true;
					} else {
						isSellSuccess = false;
						break;
					}
				}
			}
		}
		return isSellSuccess;
	}

	private boolean checkSellErrorMessage(Air_SellFromRecommendationReply sellRs) {
		boolean isError = false;
		if (sellRs == null || (sellRs != null && sellRs.getErrorAtMessageLevel() != null
				&& ArrayUtils.isNotEmpty(sellRs.getErrorAtMessageLevel()))) {
			StringJoiner errorMsg = new StringJoiner("");
			ErrorAtMessageLevel_type0[] errorAtMessage = sellRs.getErrorAtMessageLevel();
			for (ErrorAtMessageLevel_type0 error : errorAtMessage) {
				isError = true;
				if (error.getInformationText() != null
						&& ArrayUtils.isNotEmpty(error.getInformationText().getFreeText())) {
					InformationText_type0 informationText = error.getInformationText();
					FreeText_type1[] freeText = informationText.getFreeText();
					for (FreeText_type1 text : freeText) {
						errorMsg.add(text.getFreeText_type0());
					}
				}
			}
			if (StringUtils.isNotBlank(errorMsg.toString()) && criticalMessageLogger != null) {
				criticalMessageLogger.add(errorMsg.toString());
				log.info("Error Occured on Sell Recommendation for booking {} error ", bookingId, errorMsg.toString());
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
		}
		return isError;
	}

	private Air_SellFromRecommendation buildSellRequest(TripInfo selectedTrip) {
		Air_SellFromRecommendation sellRq = new Air_SellFromRecommendation();
		List<TripInfo> tripInfos = selectedTrip.splitTripInfo(false);
		ItineraryDetails_type0[] itinDetails = buildItineraryDetails(tripInfos);
		buildMessageActionDetails(sellRq);
		sellRq.setItineraryDetails(itinDetails);
		return sellRq;
	}

	private ItineraryDetails_type0[] buildItineraryDetails(List<TripInfo> tripInfos) {
		Integer tripSize = tripInfos.size();
		ItineraryDetails_type0[] itinDetails = new ItineraryDetails_type0[tripSize];
		int tripNum = 0;
		for (TripInfo tripInfo : tripInfos) {
			ItineraryDetails_type0 itinType = new ItineraryDetails_type0();
			SegmentInformation_type0[] segmentInformation = buildSegmentInformations(tripInfo.getSegmentInfos());
			itinType.setSegmentInformation(segmentInformation);
			itinType.setOriginDestinationDetails(buildOriginDestinationDetails(tripInfo));
			itinType.setMessage(buildMessageState(tripInfo));
			itinDetails[tripNum] = itinType;
			tripNum++;
		}
		return itinDetails;
	}

	private Message_type0 buildMessageState(TripInfo tripInfo) {
		Message_type0 messageType = new Message_type0();
		MessageFunctionDetails_type1 functionDetails = new MessageFunctionDetails_type1();
		MessageFunction_type3 msgFunction = new MessageFunction_type3();
		msgFunction.setMessageFunction_type2(MESSGAGE_FUNCTION);
		functionDetails.setMessageFunction(msgFunction);
		messageType.setMessageFunctionDetails(functionDetails);
		return messageType;
	}

	private OriginDestinationDetails_type0 buildOriginDestinationDetails(TripInfo tripInfo) {
		OriginDestinationDetails_type0 originDestination = new OriginDestinationDetails_type0();
		Destination_type1 destionation = new Destination_type1();
		destionation.setDestination_type0(tripInfo.getArrivalAirportCode());
		originDestination.setDestination(destionation);
		Origin_type1 origin = new Origin_type1();
		origin.setOrigin_type0(tripInfo.getDepartureAirportCode());
		originDestination.setOrigin(origin);
		return originDestination;
	}

	private SegmentInformation_type0[] buildSegmentInformations(List<SegmentInfo> segmentInfos) {
		SegmentInformation_type0[] segmentInformations = new SegmentInformation_type0[segmentInfos.size()];
		int segmentNum = 0;
		for (SegmentInfo segmentInfo : segmentInfos) {
			TravelProductInformation_type0 productInfo = new TravelProductInformation_type0();
			productInfo.setBoardPointDetails(getSellBoardingPoint(segmentInfo));
			productInfo.setOffpointDetails(getSellOffPoint(segmentInfo));
			productInfo.setCompanyDetails(getCompanyDetails(segmentInfo));
			productInfo.setFlightIdentification(getSellFlightIdentification(segmentInfo));
			productInfo.setFlightDate(getSellFlightDate(segmentInfo));
			segmentInformations[segmentNum] = new SegmentInformation_type0();
			segmentInformations[segmentNum].setTravelProductInformation(productInfo);
			segmentInformations[segmentNum].setRelatedproductInformation(buildRelatedProductinfo());
			segmentNum++;
		}
		return segmentInformations;
	}

	private RelatedproductInformation_type0 buildRelatedProductinfo() {
		RelatedproductInformation_type0 relatedInfo = new RelatedproductInformation_type0();
		Quantity_type1 quantityType = new Quantity_type1();
		quantityType.setQuantity_type0(new BigDecimal(paxCountWithOutInf));
		relatedInfo.setQuantity(quantityType);
		StatusCode_type3[] statusCode = new StatusCode_type3[1];
		statusCode[0] = new StatusCode_type3();
		statusCode[0].setStatusCode_type2(STATUS_CODE);
		relatedInfo.setStatusCode(statusCode);
		return relatedInfo;
	}

	private FlightDate_type0 getSellFlightDate(SegmentInfo segmentInfo) {
		FlightDate_type0 flightDate = new FlightDate_type0();
		DepartureDate_type5 departureDate = new DepartureDate_type5();
		String depatureDate = AmadeusUtils.formatToDDMMYY(segmentInfo.getDepartTime().toLocalDate());
		departureDate.setDepartureDate_type4(depatureDate);
		flightDate.setDepartureDate(departureDate);
		return flightDate;
	}

	private FlightIdentification_type2 getSellFlightIdentification(SegmentInfo segmentInfo) {
		FlightIdentification_type2 flightId = new FlightIdentification_type2();
		FlightNumber_type5 flightNumber = new FlightNumber_type5();
		flightNumber.setFlightNumber_type4(segmentInfo.getFlightNumber());
		flightId.setFlightNumber(flightNumber);
		BookingClass_type1 bookingClass = new BookingClass_type1();
		bookingClass.setBookingClass_type0(getBookingClass(segmentInfo));
		flightId.setBookingClass(bookingClass);
		return flightId;
	}

	private CompanyDetails_type0 getCompanyDetails(SegmentInfo segmentInfo) {
		CompanyDetails_type0 companyDetails = new CompanyDetails_type0();
		MarketingCompany_type1 marketingAirline = new MarketingCompany_type1();
		marketingAirline.setMarketingCompany_type0(segmentInfo.getFlightDesignator().getAirlineCode());
		companyDetails.setMarketingCompany(marketingAirline);
		return companyDetails;
	}

	private BoardPointDetails_type0 getSellBoardingPoint(SegmentInfo segmentInfo) {
		BoardPointDetails_type0 boardPoint = new BoardPointDetails_type0();
		TrueLocationId_type1 trueLocation = new TrueLocationId_type1();
		trueLocation.setTrueLocationId_type0(segmentInfo.getDepartureAirportCode());
		boardPoint.setTrueLocationId(trueLocation);
		return boardPoint;
	}

	private OffpointDetails_type0 getSellOffPoint(SegmentInfo segmentInfo) {
		OffpointDetails_type0 offPoint = new OffpointDetails_type0();
		TrueLocationId_type3 trueLocationId = new TrueLocationId_type3();
		trueLocationId.setTrueLocationId_type2(segmentInfo.getArrivalAirportCode());
		offPoint.setTrueLocationId(trueLocationId);
		return offPoint;
	}

	private void buildMessageActionDetails(Air_SellFromRecommendation sellRq) {
		MessageActionDetails_type0 actionDetails = new MessageActionDetails_type0();
		MessageFunctionDetails_type0 functionDetails = new MessageFunctionDetails_type0();
		MessageFunction_type1 functionType = new MessageFunction_type1();
		AdditionalMessageFunction_type1[] additionalMsg = new AdditionalMessageFunction_type1[1];
		additionalMsg[0] = new AdditionalMessageFunction_type1();
		additionalMsg[0].setAdditionalMessageFunction_type0(MESSAGE_ADDITIONAL);
		functionType.setMessageFunction_type0(MESSGAGE_FUNCTION);
		functionDetails.setMessageFunction(functionType);
		functionDetails.setAdditionalMessageFunction(additionalMsg);
		actionDetails.setMessageFunctionDetails(functionDetails);
		sellRq.setMessageActionDetails(actionDetails);
	}

	public void setSSRInfos(TripInfo tripInfo) {
		if (tripInfo != null) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				FlightBasicFact fact = FlightBasicFact.builder().build().generateFact(segmentInfo);
				BaseUtils.createFactOnUser(fact, bookingUser);
				Map<SSRType, List<? extends SSRInformation>> ssrInfos = AirUtils.getPreSSR(fact);
				if (MapUtils.isNotEmpty(ssrInfos)) {
					segmentInfo.setSsrInfo(ssrInfos);
				}
			}
		}
	}

	private BaggageInfo buildBaggaeInfo(SegmentLevelGroup_type0 segmentGroup) {
		if (segmentGroup.getBaggageAllowance() != null
				&& segmentGroup.getBaggageAllowance().getBaggageDetails() != null) {
			BaggageInfo baggageInfo = new BaggageInfo();
			BaggageDetailsTypeI baggageDetails = segmentGroup.getBaggageAllowance().getBaggageDetails();
			String qunatityCode = baggageDetails.getQuantityCode().getAlphaNumericString_Length1To3();
			String freeAllowance = baggageDetails.getFreeAllowance().getNumericInteger_Length1To15().toString();
			String quantity = null;
			if (AmadeusConstants.PIECES.equalsIgnoreCase(qunatityCode)) {
				quantity = AmadeusConstants.PIECE;
			} else if (AmadeusConstants.KILOGRAMS.equalsIgnoreCase(qunatityCode)) {
				quantity = AmadeusConstants.KILOGRAM;
			}
			String checkInBaggage = StringUtils.join(freeAllowance, quantity);
			baggageInfo.setAllowance(checkInBaggage);
			return baggageInfo;
		}
		return null;
	}

	private com.tgs.services.base.enums.CabinClass getCabinClass(SegmentLevelGroup_type0 segmentGroup) {
		String cabinDesignator = null;
		if (segmentGroup.getCabinGroup() != null && ArrayUtils.isNotEmpty(segmentGroup.getCabinGroup())
				&& segmentGroup.getCabinGroup()[0].getCabinSegment().getBookingClassDetails() != null) {
			ProductDetailsTypeI[] classDetails =
					segmentGroup.getCabinGroup()[0].getCabinSegment().getBookingClassDetails();
			if (classDetails != null && ArrayUtils.isNotEmpty(classDetails) && classDetails[0].getOption() != null) {
				cabinDesignator = classDetails[0].getOption()[0].getAlphaNumericString_Length1To7();
			}
		}
		return AmadeusUtils.getCabinClass(cabinDesignator);
	}
}
