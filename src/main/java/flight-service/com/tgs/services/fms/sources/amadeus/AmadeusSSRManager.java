package com.tgs.services.fms.sources.amadeus;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;
import com.amadeus.xml.AmadeusReviewWebServicesStub;
import com.amadeus.xml.smpreq_97_1_ia.*;
import com.amadeus.xml.smpres_97_1_ia.*;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@Setter
@Getter
@Slf4j
@SuperBuilder
public class AmadeusSSRManager extends AmadeusServiceManager {

	protected AmadeusReviewWebServicesStub ssrStub;

	protected static final String SEAT_CHARACTERISTIC_AISLE = "A";

	protected static final String SEAT_CHARACTERISTIC_RECLINED = "1D";

	protected static final String SEAT_CHARACTERISTIC_RESTRICTED = "1";

	protected static final String SEAT_CHARACTERISTIC_FREE = "F";

	protected static final String SEAT_CHARACTERISTIC_OCCUPIED = "O";


	public List<SeatSSRInformation> getSeatMap(SegmentInfo segmentInfo) {
		listener.setType("Air_RetrieveSeatMap");
		Air_RetrieveSeatMap seatMapRequest = null;
		Air_RetrieveSeatMapReply seatMapRs = null;
		try {
			seatMapRequest = buildSeatRequest(segmentInfo);
			ssrStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			seatMapRs = ssrStub.air_RetrieveSeatMap(seatMapRequest, getSessionSchema());
			if (!checkErrorMessage(seatMapRs)) {
				return parseSeatResponse(seatMapRs);
			}
		} catch (Exception e) {
			log.error("Error Occured while fetching seat map for segment {} ", segmentInfo, e);
		} finally {
			ssrStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	private List<SeatSSRInformation> parseSeatResponse(Air_RetrieveSeatMapReply seatMapRs) {
		if (seatMapRs != null && seatMapRs.getSegment() != null && ArrayUtils.isNotEmpty(seatMapRs.getSegment())) {
			Segment_type0 segmentSeat = seatMapRs.getSegment()[0];
			List<SeatSSRInformation> seatSSRs = new ArrayList<>();
			if (segmentSeat.getSegmentErrorDetails() == null && segmentSeat.getRow() != null
					&& ArrayUtils.isNotEmpty(segmentSeat.getRow())) {
				Row_type0[] rowType = segmentSeat.getRow();
				Integer rowNumber = getRowNumber(rowType);
				for (Row_type0 row : rowType) {
					if (row.getRowDetails() != null && row.getRowDetails().getSeatOccupationDetails() != null
							&& ArrayUtils.isNotEmpty(row.getRowDetails().getSeatOccupationDetails())) {
						SeatOccupationDetails_type0[] seatOcuInfos = row.getRowDetails().getSeatOccupationDetails();
						for (SeatOccupationDetails_type0 sOd : seatOcuInfos) {
							SeatSSRInformation seatInfo = new SeatSSRInformation();
							String seatNumber = StringUtils.join(rowNumber, sOd.getSeatColumn().getSeatColumn_type6());
							seatInfo.setSeatNo(seatNumber);
							seatInfo.setCode(seatNumber);
							seatInfo.setIsAisle(getIsAisle(sOd));
							seatInfo.setIsBooked(getIsBookedOrRestricted(sOd));
							seatInfo.setAmount(getAmount(sOd));
							seatSSRs.add(seatInfo);
						}
					}
					rowNumber++;
				}
			}
			return seatSSRs;
		}
		return null;
	}

	private Double getAmount(SeatOccupationDetails_type0 sOd) {
		Double amount = null;
		if (sOd.getSeatOccupation() != null
				&& sOd.getSeatOccupation().getSeatOccupation_type0().equalsIgnoreCase(SEAT_CHARACTERISTIC_FREE)) {
			amount = new Double(0);
		}
		return amount;
	}

	private Boolean getIsBookedOrRestricted(SeatOccupationDetails_type0 sOd) {
		boolean isBookedOrRestricted = false;
		if (sOd != null && sOd.getSeatCharacteristic() != null) {
			for (SeatCharacteristic_type5 characteristic : sOd.getSeatCharacteristic()) {
				if (characteristic.getSeatCharacteristic_type4().equalsIgnoreCase(SEAT_CHARACTERISTIC_RECLINED)
						|| characteristic.getSeatCharacteristic_type4()
								.equalsIgnoreCase(SEAT_CHARACTERISTIC_RESTRICTED)) {
					isBookedOrRestricted = true;
					break;
				}
			}
		}
		if (sOd.getSeatOccupation() != null && sOd.getSeatOccupation().getSeatOccupation_type0() != null
				&& sOd.getSeatOccupation().getSeatOccupation_type0().equalsIgnoreCase(SEAT_CHARACTERISTIC_OCCUPIED)) {
			// seat occupied by other passenger
			isBookedOrRestricted = true;
		}
		return isBookedOrRestricted;
	}

	private Boolean getIsAisle(SeatOccupationDetails_type0 sOd) {
		boolean isAisle = false;
		if (sOd != null && sOd.getSeatCharacteristic() != null) {
			for (SeatCharacteristic_type5 characteristic : sOd.getSeatCharacteristic()) {
				if (characteristic.getSeatCharacteristic_type4().equalsIgnoreCase(SEAT_CHARACTERISTIC_AISLE)) {
					isAisle = true;
					break;
				}
			}
		}
		return isAisle;
	}

	private Integer getRowNumber(Row_type0[] rowType) {
		Integer rowNum = 0;
		for (Row_type0 row : rowType) {
			if (row.getRowDetails().getSeatRowNumber() != null
					&& row.getRowDetails().getSeatRowNumber().getSeatRowNumber_type10().intValue() == 10) {
				rowNum = 1;
				break;
			} else {
				rowNum++;
			}
		}
		return rowNum;
	}

	private boolean checkErrorMessage(Air_RetrieveSeatMapReply seatMapRs) {
		boolean isError = false;
		if (seatMapRs == null) {
			return true;
		}
		if (seatMapRs != null && seatMapRs.getErrorDetails() != null) {
			ErrorDetails_type0 errorDetails = seatMapRs.getErrorDetails();
			if (errorDetails != null && ArrayUtils.isNotEmpty(errorDetails.getErrorInformation())) {
				ErrorInformation_type0[] errorInfos = errorDetails.getErrorInformation();
				for (ErrorInformation_type0 errorInfo : errorInfos) {
					isError = true;
					log.error("Seat Map Error Occured for booking {} error {}", errorInfo.getErrorText());
				}
			}
		}
		return isError;
	}

	private Air_RetrieveSeatMap buildSeatRequest(SegmentInfo segmentInfo) {
		Air_RetrieveSeatMap seatMapRequest = new Air_RetrieveSeatMap();
		seatMapRequest.setTravelProductIdent(buildTravelProductIdentInfo(segmentInfo));
		return seatMapRequest;
	}

	private TravelProductIdent_type0 buildTravelProductIdentInfo(SegmentInfo segmentInfo) {
		TravelProductIdent_type0 travelProductIdent = new TravelProductIdent_type0();
		travelProductIdent.setBoardpointDetail(getBoardingPoint(segmentInfo));
		travelProductIdent.setOffPointDetail(getOffPoint(segmentInfo));
		travelProductIdent.setCompanyIdentification(getAirlineInfo(segmentInfo));
		travelProductIdent.setFlightIdentification(getFlightIdentification(segmentInfo));
		travelProductIdent.setProductDetails(getProductDetails(segmentInfo));
		return travelProductIdent;
	}

	private ProductDetails_type0 getProductDetails(SegmentInfo segmentInfo) {
		ProductDetails_type0 productDetails = new ProductDetails_type0();
		DepartureDate_type1 departureDate = new DepartureDate_type1();
		departureDate.setDepartureDate_type0(AmadeusUtils.formatToDDMMYY(segmentInfo.getDepartTime().toLocalDate()));
		productDetails.setDepartureDate(departureDate);
		return productDetails;
	}

	private FlightIdentification_type0 getFlightIdentification(SegmentInfo segmentInfo) {
		FlightIdentification_type0 flightIdentification = new FlightIdentification_type0();
		FlightNumber_type1 flightNumber = new FlightNumber_type1();
		flightNumber.setFlightNumber_type0(segmentInfo.getFlightNumber());
		flightIdentification.setFlightNumber(flightNumber);
		ClassOfService_type1 classOfService = new ClassOfService_type1();
		classOfService.setClassOfService_type0(getBookingClass(segmentInfo));
		flightIdentification.setClassOfService(classOfService);
		return flightIdentification;
	}

	private CompanyIdentification_type0 getAirlineInfo(SegmentInfo segmentInfo) {
		CompanyIdentification_type0 marketingAirline = new CompanyIdentification_type0();
		MarketingAirlineCode_type1 marketingAirlineCode = new MarketingAirlineCode_type1();
		marketingAirlineCode.setMarketingAirlineCode_type0(segmentInfo.getAirlineCode(false));
		marketingAirline.setMarketingAirlineCode(marketingAirlineCode);
		return marketingAirline;
	}

	private OffPointDetail_type0 getOffPoint(SegmentInfo segmentInfo) {
		OffPointDetail_type0 offPoint = new OffPointDetail_type0();
		ArrivalCityCode_type1 arrivalCityCode = new ArrivalCityCode_type1();
		arrivalCityCode.setArrivalCityCode_type0(segmentInfo.getArrivalAirportCode());
		offPoint.setArrivalCityCode(arrivalCityCode);
		return offPoint;
	}

	private BoardpointDetail_type0 getBoardingPoint(SegmentInfo segmentInfo) {
		BoardpointDetail_type0 boardpointDetail = new BoardpointDetail_type0();
		DepartureCityCode_type1 departureCityCode = new DepartureCityCode_type1();
		departureCityCode.setDepartureCityCode_type0(segmentInfo.getDepartureAirportCode());
		boardpointDetail.setDepartureCityCode(departureCityCode);
		return boardpointDetail;
	}

}
