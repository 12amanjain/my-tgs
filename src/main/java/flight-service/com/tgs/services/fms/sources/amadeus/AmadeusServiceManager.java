package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import com.amadeus.xml.AmadeusWebServicesStub;
import com.amadeus.xml.fmptbq_14_3_1a.*;
import com.amadeus.xml.pnracc_15_1_1a.*;
import com.amadeus.xml.ws._2009._01.wbs_session_2_0_xsd.Session;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
abstract class AmadeusServiceManager {

	protected String bookingId;

	protected SupplierConfiguration supplierConfiguration;

	protected AirSourceConfigurationOutput sourceConfig;

	protected AirSearchQuery searchQuery;

	protected List<String> criticalMessageLogger;

	protected ClientGeneralInfo clientInfo;

	protected AmadeusWebServicesStub webServicesStub;

	protected AmadeusBindingService bindingService;

	protected SoapRequestResponseListner listener;

	protected DeliveryInfo deliveryInfo;

	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected User bookingUser;

	protected static final String DELIM = "--";

	protected String accountCode;

	protected Integer paxCountWithInf;
	protected Integer paxCountWithOutInf;
	protected int adultCount;
	protected int childCount;
	protected int infantCount;

	protected static final String ADULT_PAX = "ADT";
	protected static final String CHILD_PAX = "CH";
	protected static final String CHILD_PNR_PAX = "CHD";
	protected static final String INFANT_PAX = "INF";


	private Session session;
	protected String sessionId;
	protected String sequenceNumber;
	protected String securityToken;

	protected List<FlightTravellerInfo> infants;
	protected List<FlightTravellerInfo> adults;
	protected List<FlightTravellerInfo> childs;

	protected List<FlightTravellerInfo> travellerInfos;

	protected List<String> travellerNames;

	protected static final String TICKET_NUMBER_ELEMENT = "FA";
	protected static final String AUTOMATED_TICKET = "P06";
	protected static final String TICKET_NUMBER_DELIM_1 = "/";
	protected static final String PASSPORT_PASSENGER_REFRENCE = "PT";
	protected static final String SEGMENT_REFERENCE = "ST";
	protected static final String PASSENGER_REFERENCE = "PT";

	protected Map<Integer, Integer> segmentIndexToRefMap;
	protected Map<Integer, Integer> passengerRefToIndexMap;
	protected Map<Integer, Integer> infantPassengerRefToIndexMap;
	protected Map<Integer, Integer> passengerIndexToRefMap;

	protected List<String> airlinePnrs;
	protected List<String> ticketNumbers;

	protected static final String CANCELLATION_CAT = "33";
	protected static final String REISSUE_CAT = "31";
	protected static final String NO_SHOW_CAT = "33";

	protected String getSupplierCurrency() {
		return supplierConfiguration.getSupplierCredential().getCurrencyCode();
	}


	protected void init(String session) {
		if (searchQuery != null) {
			paxCountWithInf = AirUtils.getPaxCount(searchQuery, true);
			paxCountWithOutInf = AirUtils.getPaxCount(searchQuery, false);
			adultCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
			childCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			infantCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT);
		}
		initSession(session);
	}

	protected void setSession(Session newSession) {
		if (newSession != null) {
			session = newSession;
			sequenceNumber = newSession.getSequenceNumber();
			securityToken = newSession.getSecurityToken();
			sessionId = newSession.getSessionId();
		} else {
			session = null;
			sequenceNumber = null;
			securityToken = null;
			sessionId = null;
		}
	}

	protected void initSession(String sessionToken) {
		if (StringUtils.isNotBlank(sessionToken) && ArrayUtils.isNotEmpty(sessionToken.split(DELIM))) {
			String data[] = sessionToken.split(DELIM);
			sessionId = data[0];
			sequenceNumber = data[1];
			securityToken = data[2];
		} else if (session != null) {
			sessionId = session.getSessionId();
			sequenceNumber = session.getSequenceNumber();
			securityToken = session.getSecurityToken();
		}
	}

	protected Session getSessionSchema() {
		if (session != null) {
			Integer sessionSeq = Integer.parseInt(sequenceNumber);
			sequenceNumber = Integer.toString(sessionSeq + 1);
			session.setSequenceNumber(sequenceNumber);
			return session;
		} else if (StringUtils.isNotBlank(sessionId)) {
			Session session1 = new Session();
			Integer sessionSeq = Integer.parseInt(sequenceNumber);
			sequenceNumber = Integer.toString(sessionSeq + 1);
			session1.setSecurityToken(securityToken);
			session1.setSequenceNumber(sequenceNumber);
			session1.setSessionId(sessionId);
			return session1;
		}
		return null;
	}

	protected String getSession() {
		return StringUtils.join(sessionId, DELIM, sequenceNumber, DELIM, securityToken);
	}

	public void extractTravellers() {
		travellerNames = new ArrayList<>();
		travellerInfos.forEach(travellerInfo -> {
			travellerNames.add(ObjectUtils.firstNonNull(travellerInfo.getFirstName(), StringUtils.EMPTY) + " "
					+ travellerInfo.getTitle() + " " + travellerInfo.getLastName());
		});
	}


	public AlphaNumericString_Length1To3 getAlphaNumericCode1to3(String code) {
		AlphaNumericString_Length1To3 aplha1to3 = new AlphaNumericString_Length1To3();
		aplha1to3.setAlphaNumericString_Length1To3(code);
		return aplha1to3;
	}

	public AlphaString_Length1To3 getAlphaString1to3(String code) {
		AlphaString_Length1To3 length1To3 = new AlphaString_Length1To3();
		length1To3.setAlphaString_Length1To3(code);
		return length1To3;
	}

	public AlphaNumericString_Length1To1 getAplha1to1(String code) {
		AlphaNumericString_Length1To1 length1To1 = new AlphaNumericString_Length1To1();
		length1To1.setAlphaNumericString_Length1To1(code);
		return length1To1;
	}

	public AlphaNumericString_Length1To10 getAlphaNumericCode1to10(String code) {
		AlphaNumericString_Length1To10 aplha1to10 = new AlphaNumericString_Length1To10();
		aplha1to10.setAlphaNumericString_Length1To10(code);
		return aplha1to10;
	}

	public AlphaNumericString_Length0To3 getAlphaNumeric0to3(String code) {
		AlphaNumericString_Length0To3 aplha1to3 = new AlphaNumericString_Length0To3();
		aplha1to3.setAlphaNumericString_Length0To3(code);
		return aplha1to3;
	}

	public AlphaNumericString_Length1To6 getAlphaNumeric1to6(String code) {
		AlphaNumericString_Length1To6 aplha1to6 = new AlphaNumericString_Length1To6();
		aplha1to6.setAlphaNumericString_Length1To6(code);
		return aplha1to6;
	}

	public AlphaString_Length3To5 getAlphaNumeric3to5(String code) {
		AlphaString_Length3To5 aplha1to6 = new AlphaString_Length3To5();
		aplha1to6.setAlphaString_Length3To5(code);
		return aplha1to6;
	}

	public AlphaNumericString_Length2To3 getAlphaNumeric2to3(String code) {
		AlphaNumericString_Length2To3 aplha2to3 = new AlphaNumericString_Length2To3();
		aplha2to3.setAlphaNumericString_Length2To3(code);
		return aplha2to3;
	}

	public AlphaNumericString_Length0To1 getAlphaNumeric0to1(String code) {
		AlphaNumericString_Length0To1 aplha0to1 = new AlphaNumericString_Length0To1();
		aplha0to1.setAlphaNumericString_Length0To1(code);
		return aplha0to1;
	}

	public NumericInteger_Length1To2 getNumeric1to2(Integer code) {
		NumericInteger_Length1To2 aplha1to2 = new NumericInteger_Length1To2();
		aplha1to2.setNumericInteger_Length1To2(new BigInteger(code.toString()));
		return aplha1to2;
	}

	public NumericInteger_Length1To3 getNumeric1to3(Integer code) {
		NumericInteger_Length1To3 aplha1to3 = new NumericInteger_Length1To3();
		aplha1to3.setNumericInteger_Length1To3(new BigInteger(code.toString()));
		return aplha1to3;
	}

	public NumericInteger_Length1To1 getNumeric1to1(Integer code) {
		NumericInteger_Length1To1 length1To1 = new NumericInteger_Length1To1();
		length1To1.setNumericInteger_Length1To1(new BigInteger(code.toString()));
		return length1To1;
	}

	public NumericInteger_Length1To6 getNumeric1to6(Integer code) {
		NumericInteger_Length1To6 length1To6 = new NumericInteger_Length1To6();
		length1To6.setNumericInteger_Length1To6(new BigInteger(code.toString()));
		return length1To6;
	}

	public AlphaNumericString_Length0To3[] getAlphaNumericList0to3(List<String> list) {
		// return list.stream().toArray(AlphaNumericString_Length0To3[]::new);
		AlphaNumericString_Length0To3[] length0To3s = new AlphaNumericString_Length0To3[list.size()];
		Integer index = 0;
		for (String code : list) {
			length0To3s[index] = new AlphaNumericString_Length0To3();
			length0To3s[index].setAlphaNumericString_Length0To3(code);
			index++;
		}
		return length0To3s;
	}

	public AlphaString_Length0To1 getAlphaLength0To1(String code) {
		AlphaString_Length0To1 length0To1 = new AlphaString_Length0To1();
		length0To1.setAlphaString_Length0To1(code);
		return length0To1;
	}

	public AlphaNumericString_Length1To20[] getAlphaNumericList1to20(List<String> list) {
		// return list.stream().toArray(AlphaNumericString_Length1To20[]::new);
		// return list.toArray(new AlphaNumericString_Length1To20[0]);
		AlphaNumericString_Length1To20[] length1To20 = new AlphaNumericString_Length1To20[list.size()];
		Integer index = 0;
		for (String code : list) {
			length1To20[index] = new AlphaNumericString_Length1To20();
			length1To20[index].setAlphaNumericString_Length1To20(code);
			index++;
		}
		return length1To20;
	}

	public String getCurrencyCode() {
		if (StringUtils.isNotEmpty(supplierConfiguration.getSupplierCredential().getCurrencyCode())) {
			return supplierConfiguration.getSupplierCredential().getCurrencyCode();
		}
		return ServiceCommunicatorHelper.getClientInfo().getCurrencyCode();
	}

	public PaxType getPaxType(String code) {
		switch (code.toUpperCase()) {
			case "CH":
				return PaxType.CHILD;
			case "ADT":
				return PaxType.ADULT;
			case "CHD":
				return PaxType.CHILD;
			default:
				return PaxType.INFANT;
		}
	}

	public String getPaxType(PaxType paxType) {
		if (paxType.equals(PaxType.INFANT)) {
			return INFANT_PAX;
		} else if (paxType.equals(PaxType.CHILD)) {
			return CHILD_PAX;
		}
		return ADULT_PAX;
	}

	public String getPNRAddPaxType(PaxType paxType) {
		if (paxType.equals(PaxType.INFANT)) {
			return INFANT_PAX;
		} else if (paxType.equals(PaxType.CHILD)) {
			return CHILD_PNR_PAX;
		}
		return ADULT_PAX;
	}

	protected boolean isCorporateFare(List<SegmentInfo> segmentInfos) {
		boolean isCorpFare = false;
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			SegmentInfo segmentInfo = segmentInfos.get(0);
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			if (StringUtils.isNotBlank(priceInfo.getMiscInfo().getAccountCode())) {
				isCorpFare = true;
			}
		}
		return isCorpFare;
	}

	protected List<String> getCorporateCode(List<SegmentInfo> segmentInfos) {
		List<String> corpCode = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			segmentInfos.forEach(segmentInfo -> {
				PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
				if (StringUtils.isNotBlank(priceInfo.getMiscInfo().getAccountCode())) {
					corpCode.add(priceInfo.getMiscInfo().getAccountCode());
				}
			});
		}
		return corpCode;
	}

	public double getAmountBasedOnCurrency(Double amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency,toCurrency)) {
			return amount.doubleValue();
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(getType()).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	public String getType() {
		AirSourceType sourceType = AirSourceType.getAirSourceType(supplierConfiguration.getSourceId());
		return StringUtils
				.join(sourceType.name(), "_", supplierConfiguration.getSupplierCredential().getAgentUserName())
				.toUpperCase();
	}

	public void addMessageToLogger(String message) {
		if (criticalMessageLogger != null) {
			criticalMessageLogger.add(message);
		}
	}

	public boolean checkPNRReplyErrorMessage(PNR_Reply pnrReply) {
		boolean isError = false;
		if (pnrReply == null) {
			isError = true;
		}
		if (pnrReply != null && pnrReply.getGeneralErrorInfo() != null
				&& ArrayUtils.isNotEmpty(pnrReply.getGeneralErrorInfo())) {
			GeneralErrorInfo_type0[] generalErrorInfos = pnrReply.getGeneralErrorInfo();
			StringJoiner errorMsg = new StringJoiner("");
			for (GeneralErrorInfo_type0 generalErrorInfo : generalErrorInfos) {
				isError = true;
				FreeText_type3[] freeTexts = generalErrorInfo.getErrorWarningDescription().getFreeText();
				for (FreeText_type3 errororWarning : freeTexts) {
					errorMsg.add(errororWarning.getFreeText_type2());
				}
			}
			if (isError && StringUtils.isNotBlank(errorMsg.toString())) {
				addMessageToLogger(errorMsg.toString());
			}
		}
		return isError;
	}

	public void checkForTravellerError(PNR_Reply pnrReply) {
		if (pnrReply != null && ArrayUtils.isNotEmpty(pnrReply.getTravellerInfo())) {
			StringJoiner errorMsg = new StringJoiner("");
			for (TravellerInfo_type0 traveller : pnrReply.getTravellerInfo()) {
				NameError_type0 nameError = traveller.getNameError();
				if (Objects.nonNull(nameError) && Objects.nonNull(nameError.getErrorWarningDescription())) {
					for (FreeText_type5 freeText : nameError.getErrorWarningDescription().getFreeText()) {
						errorMsg.add(freeText.getFreeText_type4());
					}
				}
			}
			if (StringUtils.isNotBlank(errorMsg.toString())) {
				addMessageToLogger(errorMsg.toString());
			}
		}
	}

	public List<String> getSelectedFareBasis(List<SegmentInfo> segmentInfos) {
		List<String> tripFareBasis = new ArrayList<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getBookingRelatedInfo() != null) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				if (CollectionUtils.isNotEmpty(travellerInfos)) {
					travellerInfos.forEach(travellerInfo -> {
						if (!tripFareBasis.contains(travellerInfo.getFareDetail().getFareBasis())) {
							tripFareBasis.add(travellerInfo.getFareDetail().getFareBasis());
						}
					});
				}
			} else if (segmentInfo.getPriceInfo(0) != null) {
				PriceInfo tempPrice = segmentInfo.getPriceInfo(0);
				if (MapUtils.isNotEmpty(tempPrice.getFareDetails())) {
					tempPrice.getFareDetails().forEach(((paxType, fareDetail) -> {
						if (!tripFareBasis.contains(fareDetail.getFareBasis())) {
							tripFareBasis.add(fareDetail.getFareBasis());
						}
					}));
				}
			}
		});
		return tripFareBasis;
	}

	public String getBookingClass(SegmentInfo segmentInfo) {
		return segmentInfo.getPriceInfo(0).getBookingClass(PaxType.ADULT);
	}

	public void setSegmentsAirlinePnr(List<String> airlinePnrs, List<SegmentInfo> segmentInfos) {
		if (CollectionUtils.isNotEmpty(airlinePnrs)) {
			log.info("Airline Pnrs {} for bookingid {} ", airlinePnrs, bookingId);
			if (airlinePnrs.size() == 1) {
				BookingUtils.updateAirlinePnr(segmentInfos, airlinePnrs.get(0));
			} else {
				int index = 0;
				for (String pnr : airlinePnrs) {
					BookingUtils.updateAirlinePnr(Arrays.asList(segmentInfos.get(index)), pnr);
					index++;
				}
			}
		}
	}

	public List<String> parseAirlinePnr(PNR_Reply reply) {
		String airlinePnr = StringUtils.EMPTY;
		if (reply != null) {
			OriginDestinationDetails_type0[] originDestinationDetails = reply.getOriginDestinationDetails();
			for (OriginDestinationDetails_type0 originDestinationDetail : originDestinationDetails) {
				ItineraryInfo_type1[] itineraryInfos = originDestinationDetail.getItineraryInfo();
				for (ItineraryInfo_type1 itineraryInfo : itineraryInfos) {
					ItineraryReservationInfo_type0 itineraryReservationInfo =
							itineraryInfo.getItineraryReservationInfo();
					if (Objects.nonNull(itineraryReservationInfo)) {
						if (Objects.nonNull(itineraryReservationInfo.getReservation())) {
							airlinePnr = itineraryReservationInfo.getReservation().getControlNumber()
									.getControlNumber_type2();
							if (StringUtils.isNotEmpty(airlinePnr)) {
								airlinePnrs.add(airlinePnr);
							}
						}
					}
				}
			}
		}
		return airlinePnrs;
	}

	protected void setTicketNumbers(List<String> ticketNumbers, List<SegmentInfo> segmentInfos) {
		if (CollectionUtils.isNotEmpty(ticketNumbers)) {
			AtomicInteger tktIndex = new AtomicInteger(0);
			ticketNumbers.forEach(tktNumber -> {
				BookingUtils.updateTicketNumber(segmentInfos, tktNumber, travellerInfos.get(tktIndex.get()));
				tktIndex.getAndIncrement();
			});
		}
	}

	protected List<String> extractTicketNumbers(PNR_Reply reply) {
		if (reply != null) {
			String[] ticketNumbersArray = null;
			DataElementsMaster_type0 elementMaster = reply.getDataElementsMaster();
			DataElementsIndiv_type1[] elementDivs = elementMaster.getDataElementsIndiv();
			for (DataElementsIndiv_type1 elementDiv : elementDivs) {
				String segmentName = elementDiv.getElementManagementData().getSegmentName().getSegmentName_type4();
				if (segmentName.equalsIgnoreCase(TICKET_NUMBER_ELEMENT)) {
					String ticketNo = "";
					boolean isInfant = true;
					OtherDataFreetext_type0[] otherDateFreeTextList = elementDiv.getOtherDataFreetext();
					for (OtherDataFreetext_type0 otherDataFreeText : otherDateFreeTextList) {
						String otherData = otherDataFreeText.getFreetextDetail().getType().getType_type160();
						if (otherData.equalsIgnoreCase(AUTOMATED_TICKET)) {
							String ticketNumText = otherDataFreeText.getLongFreetext().getLongFreetext_type6();
							String[] paxTicketRef = ticketNumText.split(TICKET_NUMBER_DELIM_1)[0].split("\\s+");
							ticketNo = paxTicketRef[1].replace("-", "");
							isInfant = paxTicketRef[0].trim().equalsIgnoreCase(INFANT_PAX);
							break;
						}
					}
					ReferenceForDataElement_type0 referenceElement = elementDiv.getReferenceForDataElement();
					if (referenceElement != null) {
						Reference_type8[] references = referenceElement.getReference();
						if (ArrayUtils.isNotEmpty(references)) {
							for (Reference_type8 reference : references) {
								if (reference.getQualifier().getQualifier_type142()
										.equalsIgnoreCase(PASSPORT_PASSENGER_REFRENCE)) {
									if (ArrayUtils.isEmpty(ticketNumbersArray)) {
										ticketNumbersArray = new String[travellerInfos.size()];
									}
									if (reference != null && reference.getNumber() != null) {
										Integer refNumber = Integer.valueOf(reference.getNumber().getNumber_type58());
										Integer paxIndex = null;
										if (isInfant) {
											paxIndex = infantPassengerRefToIndexMap.get(refNumber);
										} else {
											paxIndex = passengerRefToIndexMap.get(refNumber);
										}
										if (paxIndex != null) {
											ticketNumbersArray[paxIndex] = ticketNo;
										}
									}
									break;
								}
							}
						}
					}

				}
			}
			if (ArrayUtils.isNotEmpty(ticketNumbersArray)) {
				ticketNumbers = Arrays.asList(ticketNumbersArray);
			}
		}
		return ticketNumbers;
	}

	protected void parseTravellerReferences(TravellerInfo_type0[] travellers) {
		if (ArrayUtils.isNotEmpty(travellers)) {
			for (TravellerInfo_type0 travellerInfoType : travellers) {
				ElementManagementPassenger_type0 elementMaganement = travellerInfoType.getElementManagementPassenger();
				Reference_type1 reference = elementMaganement.getReference();
				Integer paxReference = Integer.valueOf(reference.getNumber().getNumber_type6());
				PassengerData_type1[] passengerDatas = travellerInfoType.getPassengerData();
				for (PassengerData_type1 paxData : passengerDatas) {
					TravellerInformation_type0 travellerInfo = paxData.getTravellerInformation();
					Traveller_type0 travellerType = travellerInfo.getTraveller();
					String surname = travellerType.getSurname().getSurname_type0();
					Passenger_type0[] nameList = travellerInfo.getPassenger();
					for (Passenger_type0 eachPax : nameList) {
						StringBuilder passengerName = new StringBuilder();
						passengerName.append(eachPax.getFirstName().getFirstName_type0()).append(" ").append(surname);
						int idxOfPassenger = getIndexOfPassenger(passengerName.toString());
						if (idxOfPassenger != -1) {
							if (!passengerRefToIndexMap.containsKey(paxReference)) {
								passengerRefToIndexMap.put(paxReference, idxOfPassenger);
							}
							if (!passengerIndexToRefMap.containsKey(idxOfPassenger)) {
								passengerIndexToRefMap.put(idxOfPassenger, paxReference);
							}
						}
					}
				}
			}
			Set<Integer> passengerIdxList = passengerIndexToRefMap.keySet();
			for (Integer paxId : passengerIdxList) {
				if (travellerInfos.get(paxId).getPaxType().equals(PaxType.INFANT)) {
					infantPassengerRefToIndexMap.put(passengerIndexToRefMap.get(paxId), paxId);
				}
			}
		}
	}


	private int getIndexOfPassenger(String toString) {
		for (int idx = 0; idx < travellerNames.size(); idx++) {
			if (travellerNames.get(idx).replaceAll("\\s+", "").equalsIgnoreCase(toString.replaceAll("\\s+", ""))) {
				return idx;
			}
		}
		return -1;
	}

	protected void setPaxCount() {
		// overide pax count from original travellers list in case of corporate fare
		adultCount = CollectionUtils.size(adults);
		childCount = CollectionUtils.size(childs);
		infantCount = CollectionUtils.size(infants);
	}
	
	protected String getOrganizationCode() {
		String orgCode = String.valueOf(supplierConfiguration.getSupplierCredential().getOrganisationCode());
		return StringUtils.isNotBlank(orgCode) ? orgCode : StringUtils.EMPTY;
	}
}
