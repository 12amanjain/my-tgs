package com.tgs.services.fms.sources.amadeus;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import com.amadeus.xml.vlsslq_06_1_1a.*;
import com.amadeus.xml.vlsslr_06_1_1a.ErrorSection_type0;
import com.amadeus.xml.vlsslr_06_1_1a.FreeText_type3;
import com.amadeus.xml.vlsslr_06_1_1a.Security_AuthenticateReply;
import com.amadeus.xml.vlssoq_04_1_1a.Security_SignOut;
import com.amadeus.xml.vlssor_04_1_1a.FreeText_type5;
import com.amadeus.xml.vlssor_04_1_1a.Security_SignOutReply;
import com.amadeus.xml.ws._2009._01.wbs_session_2_0_xsd.Session;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.wsdl.WSDLConstants;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@SuperBuilder
@Setter
@Getter
final class AmadeusSessionManager extends AmadeusServiceManager {

	private static final String DUTY_REFERENCE_QUALIFIER = "DUT";
	private static final String DUTY_CODE = "SU";
	private static final String CLEAR_PASSWORD = "E";
	private static final String AUTHENTICATED_OK = "P";

	private static final String NOT_AUTHENTICATED = "not authenticated";
	private static final String INACTIVE = "inactive";
	private static final String TOO_MANY_SESSION = "too many opened conversations";

	protected void openSession() {
		try {
			Security_Authenticate secAuthenticate = buildSecurityAuthenticate();
			listener.setType("SecurityAuth");
			listener.getVisibilityGroups().addAll(AirSupplierUtils.getLogVisibility());
			webServicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			Security_AuthenticateReply authenticationReply =
					webServicesStub.security_Authenticate(secAuthenticate, null);
			if (!checkIsAnyErrorFreeTextAuth(authenticationReply.getErrorSection())
					&& authenticationReply.getProcessStatus() != null
					&& authenticationReply.getProcessStatus().getStatusCode() != null) {
				String statusCode = authenticationReply.getProcessStatus().getStatusCode().getStatusCode_type0();
				if (statusCode.equalsIgnoreCase(AUTHENTICATED_OK)) {
					MessageContext messageContext = webServicesStub._getServiceClient().getLastOperationContext()
							.getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE);
					if (messageContext != null && messageContext.getEnvelope() != null
							&& messageContext.getEnvelope().getHeader() != null) {
						OMElement sessionElement =
								messageContext.getEnvelope().getHeader().getFirstChildWithName(Session.MY_QNAME);
						try {
							Session session = Session.Factory.parse(sessionElement.getXMLStreamReaderWithoutCaching());
							setSession(session);
						} catch (Exception e) {
							throw new SupplierSessionException(e.getMessage());
						}
					}
				}
			}
		} catch (AxisFault af) {
			log.error("Error Occured on Session create ", af);
			throw new SupplierSessionException(af.getMessage());
		} catch (RemoteException re) {
			log.error("Error Occured on Session create ", re);
			throw new SupplierRemoteException(re);
		} finally {
			webServicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	protected void closeSession() {
		try {
			listener.setType("SecuritySignOut");
			webServicesStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			Security_SignOutReply signOutReply =
					webServicesStub.security_SignOut(new Security_SignOut(), getSessionSchema());
			checkIsAnyErrorFreeTextSignOut(signOutReply.getErrorSection());
			setSession(null);
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			webServicesStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private boolean checkIsAnyErrorFreeTextAuth(ErrorSection_type0 errorSection) {
		AtomicBoolean isError = new AtomicBoolean();
		if (errorSection != null && errorSection.getInteractiveFreeText() != null
				&& errorSection.getInteractiveFreeText().getFreeTextQualif() != null) {
			FreeText_type3[] freeText = errorSection.getInteractiveFreeText().getFreeText();
			Arrays.stream(freeText).forEach(errorText -> {
				isError.set(true);
				criticalMessageLogger.add(errorText.getFreeText_type2());
			});
		}
		if (errorSection != null && errorSection.getApplicationError() != null) {
			throw new SupplierSessionException(String.join(",", criticalMessageLogger));
		}
		return isError.get();
	}

	private boolean checkIsAnyErrorFreeTextSignOut(com.amadeus.xml.vlssor_04_1_1a.ErrorSection_type1 errorSection) {
		AtomicBoolean isError = new AtomicBoolean();
		if (errorSection != null && errorSection.getInteractiveFreeText() != null
				&& errorSection.getInteractiveFreeText().getFreeTextQualification() != null) {
			FreeText_type5[] freeText = errorSection.getInteractiveFreeText().getFreeText();
			Arrays.stream(freeText).forEach(errorText -> {
				isError.set(true);
				criticalMessageLogger.add(errorText.getFreeText_type4());
			});
		}
		if (errorSection != null && errorSection.getApplicationError() != null) {
			throw new SupplierSessionException(String.join(",", criticalMessageLogger));
		}
		return isError.get();
	}

	private Security_Authenticate buildSecurityAuthenticate() {
		Security_Authenticate authenticate = new Security_Authenticate();
		authenticate.setUserIdentifier(getUserIdentifier());
		authenticate.setDutyCode(getDutyCode());
		authenticate.setSystemDetails(getSystemDetails());
		authenticate.setPasswordInfo(getPasswordInfo());
		return authenticate;
	}

	private PasswordInfo_type0[] getPasswordInfo() {
		PasswordInfo_type0[] passwordInfos = new PasswordInfo_type0[1];
		PasswordInfo_type0 typeInfo = new PasswordInfo_type0();
		DataLength_type1 dataLengthType = new DataLength_type1();
		BinaryData_type1 binaryData = new BinaryData_type1();
		DataType_type1 dataType = new DataType_type1();
		dataType.setDataType_type0(CLEAR_PASSWORD);
		if (BooleanUtils.isTrue(supplierConfiguration.getSupplierCredential().getIsTestCredential())) {
			Integer passwordLen = supplierConfiguration.getSupplierCredential().getPassword().length();
			dataLengthType.setDataLength_type0(new BigDecimal(String.valueOf(passwordLen)));
			binaryData.setBinaryData_type0(Base64.getEncoder()
					.encodeToString(supplierConfiguration.getSupplierCredential().getPassword().getBytes()));
		} else {
			// Integer passwordLen = supplierConfiguration.getSupplierCredential().getPassword().length();
			dataLengthType.setDataLength_type0(new BigDecimal(String.valueOf("8")));
			binaryData.setBinaryData_type0(supplierConfiguration.getSupplierCredential().getPassword());
		}
		typeInfo.setDataLength(dataLengthType);
		typeInfo.setBinaryData(binaryData);
		typeInfo.setDataType(dataType);
		passwordInfos[0] = typeInfo;
		return passwordInfos;
	}

	// Supplier - Organisation code
	private SystemDetails_type0 getSystemDetails() {
		SystemDetails_type0 systemDetails = new SystemDetails_type0();
		OrganizationDetails_type0 orgDetails = new OrganizationDetails_type0();
		OrganizationId_type1 organizationId = new OrganizationId_type1();
		organizationId.setOrganizationId_type0(getOrganizationCode());
		orgDetails.setOrganizationId(organizationId);
		systemDetails.setOrganizationDetails(orgDetails);
		return systemDetails;
	}

	private DutyCode_type0 getDutyCode() {
		DutyCode_type0 dutyCodeType = new DutyCode_type0();
		DutyCodeDetails_type0 codeDetails = new DutyCodeDetails_type0();
		ReferenceIdentifier_type1 referenceIdentifier = new ReferenceIdentifier_type1();
		referenceIdentifier.setReferenceIdentifier_type0(DUTY_CODE);
		codeDetails.setReferenceIdentifier(referenceIdentifier);
		ReferenceQualifier_type5 qualifier = new ReferenceQualifier_type5();
		qualifier.setReferenceQualifier_type4(DUTY_REFERENCE_QUALIFIER);
		codeDetails.setReferenceQualifier(qualifier);
		dutyCodeType.setDutyCodeDetails(codeDetails);
		return dutyCodeType;
	}


	protected UserIdentifier_type0[] getUserIdentifier() {
		UserIdentifier_type0[] types = new UserIdentifier_type0[1];
		UserIdentifier_type0 userIdentifier = new UserIdentifier_type0();
		userIdentifier.setOriginIdentification(getOriginUserIdentification());
		userIdentifier.setOriginator(getUserName());
		userIdentifier.setOriginatorTypeCode(getTypeCode());
		types[0] = userIdentifier;
		return types;
	}

	private OriginatorTypeCode_type1 getTypeCode() {
		OriginatorTypeCode_type1 typeCode = new OriginatorTypeCode_type1();
		typeCode.setOriginatorTypeCode_type0("U");
		return typeCode;
	}

	// Supplier - UserName
	private Originator_type1 getUserName() {
		Originator_type1 originator = new Originator_type1();
		originator.setOriginator_type0(supplierConfiguration.getSupplierCredential().getUserName());
		return originator;
	}

	// Supplier - Agent UserName
	protected OriginIdentification_type0 getOriginUserIdentification() {
		OriginIdentification_type0 identificationType = new OriginIdentification_type0();
		SourceOffice_type1 sourceOffice = new SourceOffice_type1();
		sourceOffice.setSourceOffice_type0(supplierConfiguration.getSupplierCredential().getAgentUserName());
		identificationType.setSourceOffice(sourceOffice);
		return identificationType;
	}

	public boolean hasSessionExpired(Exception ex) {
		return (ex != null && ex.getMessage() != null && (ex.getMessage().toLowerCase().contains(NOT_AUTHENTICATED)
				|| ex.getMessage().toLowerCase().contains(INACTIVE)));
	}

	public boolean isTooManyConnections(SupplierSessionException ex) {
		return (ex != null && ex.getMessage() != null && (ex.getMessage().toLowerCase().contains(TOO_MANY_SESSION)));
	}


}
