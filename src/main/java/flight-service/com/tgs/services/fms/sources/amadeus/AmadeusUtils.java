package com.tgs.services.fms.sources.amadeus;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.amadeus.xml.fmptbr_14_3_1a.ProductInformation_type0;
import com.amadeus.xml.pnracc_15_1_1a.EnhancedPassengerData_type1;
import com.amadeus.xml.pnracc_15_1_1a.TravelProduct_type0;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.gms.datamodel.CountryInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AmadeusUtils {

	public static BigInteger getItinCount(AirSourceConfigurationOutput sourceConfiguration,
			AirSearchQuery searchQuery) {
		int flightCount = AmadeusConstants.NUM_OF_REC.intValue();
		if (sourceConfiguration != null) {
			if (searchQuery.getIsDomestic()) {
				flightCount = org.apache.commons.lang3.ObjectUtils.firstNonNull(
						sourceConfiguration.getDomflightsCount(), sourceConfiguration.getFlightsCount(), flightCount);
			} else {
				flightCount = org.apache.commons.lang3.ObjectUtils.firstNonNull(sourceConfiguration.getFlightsCount(),
						sourceConfiguration.getDomflightsCount(), flightCount);
			}
		}
		return new BigInteger(String.valueOf(flightCount));
	}

	public static void splitFare(TripInfo onwardTrip, TripInfo returnTrip, AirSearchQuery searchQuery) {
		if (searchQuery.isDomesticReturn() && onwardTrip != null && returnTrip != null
				&& CollectionUtils.isNotEmpty(onwardTrip.getSegmentInfos())
				&& CollectionUtils.isNotEmpty(returnTrip.getSegmentInfos())) {
			/**
			 * @implSpec : TripWise Pricing for Domestic Return in Single search
			 * @implSpec 1. Split the fare into 2 and
			 * @implSpec adjust on both to half (original price will be in Onward trip)
			 */
			PriceInfo orgOnwardPrice = onwardTrip.getSegmentInfos().get(0).getPriceInfo(0);
			PriceInfo onwardPriceInfo = new GsonMapper<>(adjustFareToHalf(orgOnwardPrice), PriceInfo.class).convert();
			PriceInfo orgReturnPrice = returnTrip.getSegmentInfos().get(0).getPriceInfo(0);
			PriceInfo returnPriceInfo = new GsonMapper<>(orgReturnPrice, PriceInfo.class).convert();
			returnPriceInfo = copyFareFromOnwardToReturn(returnPriceInfo, onwardPriceInfo);
			onwardTrip.getSegmentInfos().get(0).setPriceInfoList(new ArrayList<>());
			onwardTrip.getSegmentInfos().get(0).getPriceInfoList().add(onwardPriceInfo);
			returnTrip.getSegmentInfos().get(0).setPriceInfoList(new ArrayList<>());
			returnTrip.getSegmentInfos().get(0).getPriceInfoList().add(returnPriceInfo);
			setFareType(onwardTrip);
			setFareType(returnTrip);
			TripPriceEngine.setSpecialReturnIdentifier(onwardTrip.getSegmentInfos().get(0),
					onwardTrip.getSegmentInfos().get(0).getPriceInfo(0), returnTrip.getSegmentInfos().get(0),
					returnTrip.getSegmentInfos().get(0).getPriceInfo(0));
		}
	}

	private static void setFareType(TripInfo trip) {
		trip.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				priceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
			});
		});
	}

	private static PriceInfo copyFareFromOnwardToReturn(PriceInfo orgReturnPrice, PriceInfo onwardPriceInfo) {
		orgReturnPrice.getFareDetails().forEach((paxType, fareDetail) -> {
			FareDetail onwardFareDetail = onwardPriceInfo.getFareDetail(paxType);
			if (MapUtils.isNotEmpty(onwardFareDetail.getFareComponents())) {
				onwardFareDetail.getFareComponents().forEach((fc, amount) -> {
					fareDetail.getFareComponents().put(fc, amount);
				});
			}
		});
		return orgReturnPrice;
	}

	public static PriceInfo adjustFareToHalf(PriceInfo orgPrice) {
		PriceInfo priceInfo = new GsonMapper<>(orgPrice, PriceInfo.class).convert();
		priceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
		for (PaxType paxType : priceInfo.getFareDetails().keySet()) {
			FareDetail fareDetail = priceInfo.getFareDetails().get(paxType);
			for (FareComponent fc : fareDetail.getFareComponents().keySet()) {
				fareDetail.getFareComponents().put(fc, fareDetail.getFareComponents().getOrDefault(fc, 0.0) / 2);
			}
		}
		return priceInfo;
	}

	public static void adjustFareToHalf(FareDetail fareDetail) {
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			for (FareComponent fc : fareDetail.getFareComponents().keySet()) {
				fareDetail.getFareComponents().put(fc, fareDetail.getFareComponents().getOrDefault(fc, 0.0) / 2);
			}
		}
	}

	public static Integer getInfantIndicator(FlightTravellerInfo infant) {
		int infantIndicator = 1;
		if (infant != null) {
			if (StringUtils.isNotEmpty(infant.getFirstName()) && infant.getDob() == null) {
				infantIndicator = 2;
			}
			if (StringUtils.isNotEmpty(infant.getFirstName()) && StringUtils.isNotEmpty(infant.getLastName())
					&& infant.getDob() != null) {
				infantIndicator = 3;
			}
		}
		return infantIndicator;
	}

	public static String getCabinDesignator(CabinClass cabinClass) {
		if (CabinClass.ECONOMY.equals(cabinClass)) {
			return AmadeusConstants.ECONOMY_CLASS;
		} else if (CabinClass.PREMIUM_ECONOMY.equals(cabinClass))
			return AmadeusConstants.PREMIUM_ECONOMY;
		else if (CabinClass.BUSINESS.equals(cabinClass)) {
			return AmadeusConstants.BUSINESS;
		} else if (CabinClass.FIRST.equals(cabinClass)) {
			return AmadeusConstants.FIRST_CLASS;
		}
		return AmadeusConstants.ECONOMY_CLASS;
	}

	public static String formatToDDMMYY(LocalDate date) {
		String[] parts = date.toString().split("-");
		return parts[2].concat(parts[1]).concat(parts[0].substring(parts[0].length() - 2));
	}

	public static String formatToAmadeusTime(LocalDateTime dateTime) {
		String[] time = dateTime.format(DateTimeFormatter.ISO_LOCAL_TIME).split(":");
		return time[0].concat(time[1]);
	}

	public static LocalDateTime parseDateTime(String depDate, String depTime) {
		int dom = Integer.valueOf(depDate.substring(0, 2));
		int month = Integer.valueOf(depDate.substring(2, 4));
		int year = Integer.valueOf(depDate.substring(4)) + AmadeusConstants.TWO_THOUSAND;
		int hour = Integer.valueOf(depTime.substring(0, 2));
		int min = Integer.valueOf(depTime.substring(2));
		return LocalDateTime.of(year, month, dom, hour, min);
	}

	public static LocalDate parseDate(String date) {
		int dom = Integer.valueOf(date.substring(0, 2));
		int month = Integer.valueOf(date.substring(2, 4));
		int year = Integer.valueOf(date.substring(4)) + AmadeusConstants.TWO_THOUSAND;
		return LocalDate.of(year, month, dom);
	}

	public static LocalDate parseImportDate(String date) {
		int dom = Integer.valueOf(date.substring(0, 2));
		int month = Integer.valueOf(date.substring(2, 4));
		int year = Integer.valueOf(date.substring(4));
		return LocalDate.of(year, month, dom);
	}

	public static CabinClass getCabinClass(String cabin) {
		if (AmadeusConstants.ECONOMY_CLASS_LIST.contains(cabin)) {
			return CabinClass.ECONOMY;
		} else if (AmadeusConstants.PREMIUM_ECONOMY_CLASS_LIST.contains(cabin)) {
			return CabinClass.PREMIUM_ECONOMY;
		} else if (AmadeusConstants.BUSINESS_CLASS_LIST.contains(cabin)
				|| AmadeusConstants.PREMIUM_BUSINESS_CLASS_LIST.contains(cabin)) {
			return CabinClass.BUSINESS;
		} else if (AmadeusConstants.FIRST_CLASS_LIST.contains(cabin)
				|| AmadeusConstants.PREMIUM_FIRST_CLASS_LIST.contains(cabin)) {
			return CabinClass.FIRST;
		} else {
			return CabinClass.ECONOMY;
		}
	}

	public static FareType getFareType(ProductInformation_type0 productInformation_type0, boolean specialReturn) {
		String fareType = productInformation_type0.getFareProductDetail().getFareType()[0].getFareType_type0();
		if (specialReturn)
			return FareType.SPECIAL_RETURN;
		if (fareType.equals("RB")) {
			return FareType.CORPORATE;

		} else
			return FareType.PUBLISHED;
	}

	public static void setTotalFareOnTripInfo(List<TripInfo> tripInfos) {

		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getBookingRelatedInfo() != null) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(travellerInfo -> {
						if (travellerInfo.getFareDetail() != null) {
							travellerInfo.getFareDetail().getFareComponents().put(FareComponent.TF,
									getTotalFare(travellerInfo.getFareDetail()));
						}
					});
				} else if (org.apache.commons.collections.CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						priceInfo.getFareDetails().forEach((paxType, fare) -> {
							Double totalFare = getTotalFare(fare);
							if (totalFare != 0) {
								priceInfo.getFareDetail(paxType).getFareComponents().put(FareComponent.TF,
										getTotalFare(fare));
							}
						});
					});
				}
			});
		});
	}

	public static Double getTotalFare(FareDetail fareDetail) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			fareDetail.getFareComponents().forEach((fareComponent, amount) -> {
				if (!fareComponent.equals(FareComponent.TF))
					totalAmount.addAndGet(amount);
			});
		}
		return totalAmount.doubleValue();
	}

	public static TripInfoType getTripInfoType(AirSearchQuery searchQuery) {
		if (searchQuery.isIntlReturn() || (searchQuery.isMultiCity() && searchQuery.isIntl())) {
			return TripInfoType.COMBO;
		} else if (searchQuery.isDomesticReturn() || SearchType.RETURN.equals(searchQuery.getOrigSearchType())) {
			return TripInfoType.RETURN;
		}
		return TripInfoType.ONWARD;
	}

	public static String baggageAllowance(String code, Integer allowance, String unit) {
		if (code.equals("N")) {
			return String.valueOf(allowance) + " Unit(s)";
		} else {
			return String.valueOf(allowance) + " Kg";
		}
	}

	public static AirSearchResult mergeSearchResult(AirSearchResult oldResult, AirSearchResult newSearchResult,
			AirSearchQuery searchQuery) {
		if (newSearchResult != null && oldResult != null && MapUtils.isNotEmpty(newSearchResult.getTripInfos())) {
			for (TripInfoType tripInfoType : TripInfoType.values()) {
				String tripType = tripInfoType.getName();
				List<TripInfo> copyTripInfos = new ArrayList<>();
				if (oldResult.getTripInfos().get(tripType) != null) {
					copyTripInfos = oldResult.getTripInfos().get(tripType);
				}
				if (newSearchResult.getTripInfos().get(tripType) != null) {
					copyTripInfos.addAll(newSearchResult.getTripInfos().get(tripType));
				}
				if (CollectionUtils.isNotEmpty(copyTripInfos)) {
					oldResult.getTripInfos().put(tripType, copyTripInfos);
				}
			}
		}
		return oldResult;
	}

	private static TripInfoType getTripType(AirSearchQuery searchQuery) {
		if (searchQuery.getOrigSearchType().equals(SearchType.RETURN) && searchQuery.isOneWay()) {
			return TripInfoType.RETURN;
		} else if ((searchQuery.isMultiCity() && searchQuery.isIntl()) || searchQuery.isIntlReturn()) {
			return TripInfoType.COMBO;
		}
		return TripInfoType.ONWARD;
	}

	public static boolean isPaxTypeExists(AirSearchQuery searchQuery, PaxType paxType) {
		Integer paxCount = searchQuery.getPaxInfo().getOrDefault(paxType, 0);
		return paxCount > 0;
	}

	public static String getNameWithTitle(FlightTravellerInfo travellerInfo) {
		String name = "";
		return name;
	}

	public static String getPassportInfo(FlightTravellerInfo travellerInfo, GeneralServiceCommunicator gmsComm) {
		if (StringUtils.isNotEmpty(travellerInfo.getPassportNumber())
				&& StringUtils.isNotEmpty(travellerInfo.getPassportNationality())) {
			CountryInfo countryInfo = gmsComm.getCountryInfo(travellerInfo.getPassportNationality());
			String passportNum = travellerInfo.getPassportNumber().replaceAll("[^\\w\\s]", "");
			return "P" + AmadeusConstants.DELIM + countryInfo.getIsoCode() + AmadeusConstants.DELIM + passportNum
					+ AmadeusConstants.DELIM + countryInfo.getIsoCode() + AmadeusConstants.DELIM
					+ DateFormatterHelper.format(travellerInfo.getDob(), "ddMMMyy") + AmadeusConstants.DELIM
					+ (isMale(travellerInfo.getTitle()) ? "M" : "F") + AmadeusConstants.DELIM
					+ DateFormatterHelper.format(travellerInfo.getExpiryDate(), "ddMMMyy") + AmadeusConstants.DELIM
					+ travellerInfo.getLastName() + AmadeusConstants.DELIM + travellerInfo.getFirstName();
		}
		return null;
	}

	public static boolean isMale(String title) {
		if (title.equalsIgnoreCase("Ms") || title.equalsIgnoreCase("Mrs"))
			return false;
		return true;
	}

	public static List<String> getUniqueFareBasisSet(List<SegmentInfo> segmentInfos) {
		List<String> tripFareBasis = new ArrayList<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getBookingRelatedInfo() != null) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				if (org.apache.commons.collections.CollectionUtils.isNotEmpty(travellerInfos)) {
					travellerInfos.forEach(travellerInfo -> {
						if (!tripFareBasis.contains(travellerInfo.getFareDetail().getFareBasis())) {
							tripFareBasis.add(travellerInfo.getFareDetail().getFareBasis());
						}
					});
				}
			} else if (segmentInfo.getPriceInfo(0) != null) {
				PriceInfo tempPrice = segmentInfo.getPriceInfo(0);
				if (org.apache.commons.collections4.MapUtils.isNotEmpty(tempPrice.getFareDetails())) {
					tempPrice.getFareDetails().forEach(((paxType, fareDetail) -> {
						if (!tripFareBasis.contains(fareDetail.getFareBasis())) {
							tripFareBasis.add(fareDetail.getFareBasis());
						}
					}));
				}
			}
		});
		return tripFareBasis;
	}

	public static long calculateDuration(String duration) {
		int hour = Integer.valueOf(duration.substring(0, 2));
		int minutes = Integer.valueOf(duration.substring(2, 4));
		return (hour * 60 + minutes);
	}

	public static void setTravellerName(FlightTravellerInfo trav, EnhancedPassengerData_type1 eti) {
		String givenName =
				eti.getEnhancedTravellerInformation().getOtherPaxNamesDetails()[0].getGivenName().getGivenName_type0();
		trav.setTitle(StringUtils.capitalize(getTitle(givenName)));
		trav.setLastName(StringUtils.capitalize(
				eti.getEnhancedTravellerInformation().getOtherPaxNamesDetails()[0].getSurname().getSurname_type2()));
		trav.setFirstName(StringUtils.capitalize(getFirstName(givenName, trav)));
		if (Objects.nonNull(eti.getDateOfBirthInEnhancedPaxData())
				&& Objects.nonNull(eti.getDateOfBirthInEnhancedPaxData().getDateAndTimeDetails())) {
			trav.setDob(AmadeusUtils.parseImportDate(
					eti.getDateOfBirthInEnhancedPaxData().getDateAndTimeDetails().getDate().getDate_type4()));
		}
	}

	private static String getTitle(String givenName) {
		if (givenName.contains(" ")) {
			return givenName.substring(givenName.lastIndexOf(' '), givenName.length());
		}
		return StringUtils.EMPTY;
	}

	private static String getFirstName(String givenName, FlightTravellerInfo travellerInfo) {
		if (StringUtils.isNotBlank(travellerInfo.getTitle()) && givenName.contains(travellerInfo.getTitle())) {
			return givenName.replaceAll(travellerInfo.getTitle(), "");
		}
		return givenName;
	}

	public static String getContactNumberWithCountryCode(String mobile) {
		String countryCode = ServiceCommunicatorHelper.getClientInfo().getCountryCode();
		if (mobile.startsWith(countryCode)) {
			return mobile;
		} else {
			return StringUtils.join(countryCode, mobile);
		}
	}

	public static int getPaxCount(Map<PaxType, Integer> paxInfo) {
		AtomicInteger groupSize = new AtomicInteger(0);
		paxInfo.forEach((paxType, count) -> {
			if (count > 0) {
				groupSize.getAndIncrement();
			}
		});
		return groupSize.get();
	}

	public static PaxType getPaxTypeOnTitle(FlightTravellerInfo travellerInfo, TravelProduct_type0 travelProduct) {
		PaxType paxType = PaxType.ADULT;
		LocalDate departureDate = AmadeusUtils.parseDate(travelProduct.getProduct().getDepDate().getDepDate_type0());
		if (travellerInfo != null && StringUtils.isNotBlank(travellerInfo.getTitle())) {
			if (isInfant(travellerInfo, departureDate)) {
				paxType = PaxType.INFANT;
			} else if (isChild(travellerInfo, departureDate)) {
				paxType = PaxType.CHILD;
			}
		}
		return paxType;
	}

	public static boolean isInfant(FlightTravellerInfo travellerInfo, LocalDate departureDate) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			String title = travellerInfo.getTitle().toUpperCase();
			LocalDate dob = travellerInfo.getDob();
			if ((title.equalsIgnoreCase("MASTER") || title.equalsIgnoreCase("MSTR") || title.equalsIgnoreCase("MS"))
					&& dob != null && Period.between(dob, departureDate).getYears() <= 2) {
				return true;
			}
		}
		return false;
	}

	public static boolean isChild(FlightTravellerInfo travellerInfo, LocalDate departureDate) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			String title = travellerInfo.getTitle().toUpperCase();
			LocalDate dob = travellerInfo.getDob();
			if ((title.equalsIgnoreCase("MR") || title.equalsIgnoreCase("MSTR") || title.equalsIgnoreCase("MS"))
					&& (dob != null && Period.between(dob, departureDate).getDays() >= 2
							&& Period.between(dob, departureDate).getYears() <= 12)) {
				return true;
			}
		}
		return false;
	}

	// date will be like 02SEP96 , 08JUN27 , and format this string to 02/Sep/1996
	public static LocalDate getLocaldateFormString(String date) {
		LocalDate localDate = null;
		try {
			StringJoiner joiner = new StringJoiner("/");
			String day = date.substring(0, 2);
			String month = date.substring(2, 5).toLowerCase();
			String year = date.substring(5, date.length());
			joiner.add(day);
			String firstChar = String.valueOf(month.charAt(0)).toUpperCase();
			String newMonth = StringUtils.join(firstChar, month.substring(1, month.length()));
			joiner.add(newMonth);
			joiner.add(year);
			localDate = TgsDateUtils.convertStringToDate(joiner.toString(), "dd/MMM/yy").toLocalDate();
		} catch (ParseException e) {
			log.error("Unable to parse date !!");
		}
		return localDate;
	}

	public static boolean isValidSSRStatus(String ssrCode) {
		if (AmadeusConstants.SSR_REJECT_LIST.contains(ssrCode)) {
			return false;
		}
		return true;
	}

	public static void setReturnSegments(List<TripInfo> trips) {
		if (isReturnTrip(trips)) {
			for (SegmentInfo segmentInfo : trips.get(1).getSegmentInfos()) {
				segmentInfo.setIsReturnSegment(true);
			}
		}
	}

	private static boolean isReturnTrip(List<TripInfo> tripInfos) {
		if (CollectionUtils.isNotEmpty(tripInfos) && CollectionUtils.isNotEmpty(tripInfos.get(0).getSegmentInfos())
				&& CollectionUtils.isNotEmpty(tripInfos.get(1).getSegmentInfos())) {
			if (tripInfos.get(0).getSegmentInfos().get(0).getDepartAirportInfo().getCode()
					.equals(tripInfos.get(1).getSegmentInfos().get(tripInfos.get(1).getSegmentInfos().size() - 1)
							.getArrivalAirportInfo().getCode())) {
				return true;
			}
		}
		return false;
	}

	public static String getCreditExpiryDate(String expiryDate) {
		String[] monthYear = expiryDate.split("/");
		String month = monthYear[0];
		String year = monthYear[1];
		if (year.length() == 4) {
			year = (String) year.subSequence(2, 4);
		}
		return StringUtils.join(month, year);
	}

}
