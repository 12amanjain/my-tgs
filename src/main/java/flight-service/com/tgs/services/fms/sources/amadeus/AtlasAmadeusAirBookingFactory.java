package com.tgs.services.fms.sources.amadeus;

import com.amadeus.xml.pnradd_15_1_1a.PNR_AddMultiElements;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.ArrayList;

@Slf4j
@Service
public class AtlasAmadeusAirBookingFactory extends AmadeusAirBookingFactory {

	protected static final String CONTACT_MOBILE = "CTCM";
	protected static final String CONTACT_EMAIL = "CTCE";


	public AtlasAmadeusAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}


	@Override
	protected void addAdditionalCommand() {
		if (ssrElements == null) {
			ssrElements = new PNR_AddMultiElements();
		}
		bookingManager.dataElementDiv = new ArrayList<>();
		String contactNumber = AirSupplierUtils.getContactNumberWithCountryCode(order.getDeliveryInfo());
		String deliveryEmail = AirSupplierUtils.getEmailId(order.getDeliveryInfo());
		deliveryEmail = deliveryEmail.replaceAll("@", "//");
		bookingManager.buildMarker(ssrElements);
		bookingManager.buildPNRAction(ssrElements, AmadeusConstants.PNR_ADD_DETAILS_CODE);
		bookingManager.buildSSRContacts(contactNumber, CONTACT_MOBILE);
		bookingManager.buildSSRContacts(deliveryEmail, CONTACT_EMAIL);
		bookingManager.buildReceivedFrom(bookingId);
		bookingManager.addRetenionPeriod(350, ssrElements);
	}


}
