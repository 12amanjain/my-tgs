package com.tgs.services.fms.sources.dealinventory;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ims.datamodel.InventoryMsgAttributes;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InventoryAirBookingFactory extends AbstractAirBookingFactory {

	private String supplierId;

	public InventoryAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
		this.supplierId = supplierConf.getSupplierCredential().getUserName();
	}

	@Autowired
	private DealInventoryCommunicator inventoryCommunicator;

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private UserServiceCommunicator userSrvCommunicator;

	@Override
	public boolean doBooking() {
		boolean isBooked = false;
		ClientGeneralInfo clientGeneralInfo = ServiceCommunicatorHelper.getClientInfo();
		isTkRequired = BooleanUtils.isTrue(clientGeneralInfo.getIsTicketingRequiredForInventory());
		List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
		List<FlightTravellerInfo> travellerInfos =
				bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();

		String inventoryId = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getInventoryId();

		LocalDate departureDate = segmentInfos.get(0).getDepartTime().toLocalDate();

		String source = segmentInfos.get(0).getDepartureAirportCode();

		String dest = segmentInfos.get(segmentInfos.size() - 1).getArrivalAirportCode();

		String airlinePnr = inventoryCommunicator.fetchPnrAndUpdateSeatsIfAvailable(inventoryId, departureDate,
				AirUtils.getPaxCountFromTravellerInfo(travellerInfos, false));
		double totalfare = 0;
		InventoryOrder invOrder = null;
		try {
			Map<PaxType, Double> paxWiseFare = inventoryCommunicator.fetchFareBeforeBooking(inventoryId, departureDate);
			Map<PaxType, Integer> travellerCount = new HashMap<>();
			travellerInfos.forEach(traveller -> travellerCount.put(traveller.getPaxType(),
					ObjectUtils.firstNonNull(travellerCount.get(traveller.getPaxType()), 0) + 1));
			for (PaxType type : travellerCount.keySet()) {
				totalfare += travellerCount.get(type) * paxWiseFare.get(type);
			}
			List<FlightTravellerInfo> info = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(travellerInfos),
					new TypeToken<List<FlightTravellerInfo>>() {}.getType());
			info.forEach(traveller -> {
				// This is the fare provided by supplier in the rate plan
				Double paxFare = paxWiseFare.get(traveller.getPaxType());
				traveller.setCostPrice(paxFare);
			});
			pnr = airlinePnr;
			if (StringUtils.isNotBlank(airlinePnr)) {
				isBooked = true;
				info.forEach(traveller -> traveller.setPnr(airlinePnr));
			}
			Set<String> flightNumbers = segmentInfos.stream().filter(s -> s.getFlightNumber() != null)
					.map(s -> s.getFlightNumber()).collect(Collectors.toSet());
			AirInventoryOrderInfo airOrderInfo = AirInventoryOrderInfo.builder().pnr(airlinePnr).source(source)
					.dest(dest).airline(segmentInfos.get(0).getAirlineCode(false))
					.flightNumbers(Joiner.on(",").join(flightNumbers)).build();
			invOrder = InventoryOrder.builder().validOn(departureDate).inventoryId(inventoryId)
					.additionalInfo(airOrderInfo).referenceId(order.getBookingId()).travellerInfo(info)
					.supplierId(supplierId).product(Product.AIR)
					.bookingUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId()).build();
			inventoryCommunicator.saveInventoryOrder(invOrder);
			Set<String> flights =
					segmentInfos.stream().map(s -> StringUtils.join(s.getAirlineCode(false), "-", s.getFlightNumber()))
							.collect(Collectors.toSet());
			sendSeatsSoldEmail(invOrder, Joiner.on(",").join(flights), totalfare);
		} catch (Exception e) {
			log.error("Unable to create inventory order because of {} ", e);
			inventoryCommunicator.revertSeatsSold(inventoryId, departureDate,
					AirUtils.getPaxCountFromTravellerInfo(travellerInfos, false));
		}
		// In case of fare diff also, we will create inventory order and decrease the seats sold.
		if (isFareDiff(totalfare))
			isBooked = false;
		return isBooked;
	}

	@Override
	public boolean doConfirmBooking() {
		return false;
	}

	@Override
	public String getTestPnr() {
		log.debug("Inventory pnr is {} ", pnr);
		return pnr;
	}

	private void sendSeatsSoldEmail(InventoryOrder invOrder, String flightNumbers, double totalfare) {
		AbstractMessageSupplier<InventoryMsgAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<InventoryMsgAttributes>() {
					@Override
					public InventoryMsgAttributes get() {
						User user = userSrvCommunicator.getUserFromCache(invOrder.getSupplierId());
						InventoryMsgAttributes emailAttributes =
								InventoryMsgAttributes.builder().toEmailId(user.getEmail()).supplier(user.getName())
										.refId(invOrder.getReferenceId()).role(user.getRole())
										.inventoryId(invOrder.getInventoryId())
										.airlinePnr(((AirInventoryOrderInfo) invOrder.getAdditionalInfo()).getPnr())
										.flightDetails(flightNumbers)
										.travelDate(DateFormatterHelper.formatDate(invOrder.getValidOn(),
												DateFormatType.BOOKING_EMAIL_FORMAT))
										.bookingDate(DateFormatterHelper.formatDate(LocalDate.now(),
												DateFormatType.BOOKING_EMAIL_FORMAT))
										.departure(((AirInventoryOrderInfo) invOrder.getAdditionalInfo()).getSource())
										.arrival(((AirInventoryOrderInfo) invOrder.getAdditionalInfo()).getDest())
										.departTime(DateFormatterHelper
												.format(bookingSegments.getSegmentInfos().get(0).getDepartTime(),
														"HH:mm"))
										.arrivalTime(DateFormatterHelper.format(bookingSegments.getSegmentInfos()
												.get(bookingSegments.getSegmentInfos().size() - 1).getArrivalTime(),
												"HH:mm"))
										.grossFare(String.valueOf(totalfare))
										.key(EmailTemplateKey.INVENTORY_ORDER_EMAIL.name()).build();
						emailAttributes
								.setInventoryName(inventoryCommunicator.getInventoryName(invOrder.getInventoryId()));
						List<String> paxNames = new ArrayList<>();
						bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo()
								.forEach(traveller -> {
									String tName = traveller.getFullName() + "("
											+ PaxType.getStringReprestation(traveller.getPaxType()) + ")";
									if (PaxType.INFANT.equals(traveller.getPaxType()) && traveller.getDob() != null)
										tName.concat("DOB: ").concat(traveller.getDob().toString());
									paxNames.add(tName);
								});
						emailAttributes.setPaxNames(Joiner.on("<br/>").join(paxNames));
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}

	@Override
	public boolean isBookingAllowed() {
		return true;
	}
}
