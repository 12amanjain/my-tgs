package com.tgs.services.fms.sources.dealinventory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.datamodel.air.AirInventoryTripTiming;
import com.tgs.services.ims.restmodel.air.AirSeatInventoryFilter;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InventoryAirInfoFactory extends AbstractAirInfoFactory {

	@Autowired
	private DealInventoryCommunicator inventoryCommunicator;

	@Autowired
	private SupplierConfigurationHelper supplierHelper;

	public InventoryAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableSchedules() {
		Set<AirlineInfo> preferredAirline = searchQuery.getPreferredAirline();
		Set<String> airlines = null;
		if (preferredAirline != null)
			airlines = preferredAirline.stream().map(p -> p.getCode()).collect(Collectors.toSet());
		AirSearchResult result = new AirSearchResult();
		List<String> suppliers =
				supplierHelper.getSupplierUserNames(String.valueOf(AirSourceType.DEALINVENTORY.getSourceId()), true);
		log.debug("[DealInventory] Supplier ids are : {} ", suppliers.toString());
		result.setTripInfos(new HashMap<>());
		AirSeatInventoryFilter onwardFilter = getSearchFilter(searchQuery.getRouteInfos().get(0), airlines, suppliers);
		List<TripInfo> onwardTripInfos = inventoryCommunicator.findTripInfos(onwardFilter);
		applySupplierConfig(onwardTripInfos);
		result.getTripInfos().put(TripInfoType.ONWARD.toString(), onwardTripInfos);
		if (searchQuery.isDomesticReturn()) {
			result.getTripInfos().put(TripInfoType.ONWARD.toString(), onwardTripInfos);
			AirSeatInventoryFilter returnFilter =
					getSearchFilter(searchQuery.getRouteInfos().get(1), airlines, suppliers);
			List<TripInfo> returnTripInfos = inventoryCommunicator.findTripInfos(returnFilter);
			applySupplierConfig(returnTripInfos);
			result.getTripInfos().put(TripInfoType.RETURN.toString(), returnTripInfos);
		}
		searchResult = result;
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo tripInfo = null;
		try {
			// searchAvailableSchedules();
			AirInventory airInventory = inventoryCommunicator.getAirInventory(
					Long.valueOf(selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getInventoryId()));
			updateTripInfo(airInventory, selectedTrip);
			// getProcessingManager().processAirSearchResult(searchResult, searchQuery);
			tripInfo = selectedTrip;
		} finally {
			storeBookingSession(bookingId, null, null, tripInfo);
		}
		return tripInfo;
	}

	private void updateTripInfo(AirInventory airInventory, TripInfo selectedTrip) {

		
		List<SegmentInfo> segmentInfoList = new ArrayList<SegmentInfo>();

		String searchKey =
				StringUtils.join(selectedTrip.getDepartureAirportCode(), selectedTrip.getArrivalAirportCode(), "_",
						selectedTrip.getAirlineCode(), "_", selectedTrip.getDepartureTime().toLocalDate());
		
		AirInventoryTripTiming tripTiming = inventoryCommunicator.getFlightsTiming(searchKey);


		for (SegmentInfo segmentInfo : airInventory.getSegmentInfos()) {
			segmentInfoList.add(new GsonMapper<>(segmentInfo, SegmentInfo.class).convert());
		}

		for (int segmentIndex = 0; segmentIndex < selectedTrip.getSegmentInfos().size(); segmentIndex++) {
			SegmentInfo airInventorySegmentInfo = segmentInfoList.get(segmentIndex);
			SegmentInfo segmentInfo = selectedTrip.getSegmentInfos().get(segmentIndex);
			if (airInventorySegmentInfo.getDepartTime() != null) {
				segmentInfo.setDepartTime(airInventorySegmentInfo.getDepartTime());
			}
			if (airInventorySegmentInfo.getArrivalTime() != null) {
				segmentInfo.setArrivalTime(airInventorySegmentInfo.getArrivalTime());
			}
			if (airInventorySegmentInfo.getFlightNumber() != null) {
				segmentInfo.getFlightDesignator().setFlightNumber(airInventorySegmentInfo.getFlightNumber());
			}

			if (tripTiming != null && MapUtils.isNotEmpty(tripTiming.getTripTimings())) {
				// update original supplier departure & arrival if present
				String segmentKey = StringUtils.join(segmentInfo.getAirlineCode(false), segmentInfo.getFlightNumber());
				InventoryAirUtils.updateTimingsFromCache(segmentInfo, tripTiming.getTripTimings().get(segmentKey));
				segmentInfo.setDuration(Duration.between(segmentInfo.getDepartTime(), segmentInfo.getArrivalTime()).toMinutes());
			}
		}
		SeatAllocation seatAllocation =
				inventoryCommunicator.getSeatAllocationFromDb(String.valueOf(airInventory.getId()),
						TgsDateUtils.calenderToLocalDate(TgsDateUtils.getCalendar(selectedTrip.getDepartureTime())));
		if (seatAllocation.getAllocationInfo().get(0).getTotalSeats() < AirUtils.getPaxCount(searchQuery, false)) {
			throw new NoSeatAvailableException("No Seats are available.");
		}

		Map<PaxType, FareDetail> ratePlanFareDetails =
				inventoryCommunicator.getRatePlanFareDetails(seatAllocation.getAllocationInfo().get(0).getInventoryId(),
						TgsDateUtils.calenderToLocalDate(TgsDateUtils.getCalendar(selectedTrip.getDepartureTime())),
						seatAllocation.getAllocationInfo().get(0).getTotalSeats());
		SegmentInfo segment = selectedTrip.getSegmentInfos().get(0);
		List<PriceInfo> priceInfoList = segment.getPriceInfoList();
		PriceInfo pInfo = priceInfoList.get(0);
		for (Map.Entry<PaxType, FareDetail> entry : pInfo.getFareDetails().entrySet()) {
			FareDetail ratePlanFareDetail = ratePlanFareDetails.get(entry.getKey());
			ratePlanFareDetail.getFareComponents().forEach((k, v) -> {
				entry.getValue().getFareComponents().put(k, v);
			});
		}
	}

	private AirSeatInventoryFilter getSearchFilter(RouteInfo routeInfo, Set<String> airlines, List<String> suppliers) {
		Set<String> sourceAirports = new HashSet<>();
		sourceAirports.add(routeInfo.getFromCityOrAirport().getCode());
		Set<String> destAirports = new HashSet<>();
		destAirports.add(routeInfo.getToCityOrAirport().getCode());
		AirSeatInventoryFilter filter = AirSeatInventoryFilter.builder().airlines(airlines).isEnabled(true)
				.source(sourceAirports).destination(destAirports).supplierIds(suppliers)
				.travelDate(routeInfo.getTravelDate()).cabinClass(searchQuery.getCabinClass()).build();
		filter.setTripType(searchQuery.getAirType());
		filter.setPaxInfo(searchQuery.getPaxInfo());
		filter.setTotalSeats(AirUtils.getPaxCount(searchQuery, false));
		return filter;
	}

	private void applySupplierConfig(List<TripInfo> tripInfos) {
		tripInfos.forEach(trip -> {
			trip.getSegmentInfos().forEach(segment -> {
				segment.getPriceInfoList().forEach(info -> {
					info.getSupplierBasicInfo().setRuleId(supplierConf.getBasicInfo().getRuleId());
				});
			});
		});
	}
}
