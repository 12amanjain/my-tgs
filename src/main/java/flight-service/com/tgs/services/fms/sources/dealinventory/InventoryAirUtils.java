package com.tgs.services.fms.sources.dealinventory;

import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.ims.datamodel.air.AirInventoryInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class InventoryAirUtils {

	public static void updateTimingsFromCache(SegmentInfo segment, AirInventoryInfo orgSupplierTiming) {
		if (orgSupplierTiming != null) {
			if (!segment.getDepartTime().equals(orgSupplierTiming.getDepartureTime())) {
				segment.setDepartTime(orgSupplierTiming.getDepartureTime());
				log.info("Departure Mismatch between deal inventory & supplier timings {}", segment);
			}
			if (!segment.getArrivalTime().equals(orgSupplierTiming.getArrivalTime())) {
				segment.setArrivalTime(orgSupplierTiming.getArrivalTime());
				log.info("Arrival Mismatch between deal inventory & supplier timings {}", segment);
			}
		}
	}
}
