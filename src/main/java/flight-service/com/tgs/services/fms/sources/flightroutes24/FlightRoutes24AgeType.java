package com.tgs.services.fms.sources.flightroutes24;


import lombok.Getter;

@Getter
enum FlightRoutes24AgeType {

	ADULT(0), CHILD(1);

	int code;

	FlightRoutes24AgeType(int code) {
		this.code = code;
	}
}
