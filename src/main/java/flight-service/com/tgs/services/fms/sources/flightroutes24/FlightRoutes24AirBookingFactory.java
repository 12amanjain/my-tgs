package com.tgs.services.fms.sources.flightroutes24;

import java.util.Arrays;
import PayVerify.PayVerifyResponseBody;
import applyTicket.ApplyTicketResponse;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
final public class FlightRoutes24AirBookingFactory extends AbstractAirBookingFactory {
	RestAPIListener listener = null;
	protected String tokenId;
	protected FlightAPIURLRuleCriteria apiURLS;

	public FlightRoutes24AirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		apiURLS = getEndPointURL();
	}

	@Override
	public boolean doBooking() {
		initialize();
		boolean isSuccess = false;
		String supplierReference = null;
		listener = new RestAPIListener(bookingId);

		FlightRoutes24BookingManager bookingServiceManager =
				FlightRoutes24BookingManager.builder().supplierConfiguration(supplierConf).bookingId(bookingId)
						.segmentInfos(bookingSegments.getSegmentInfos()).deliveryInfo(deliveryInfo).apiURLS(apiURLS)
						.listener(listener).bookingUser(bookingUser).build();
		pnr = bookingServiceManager.createPNR();
		if (StringUtils.isNotEmpty(pnr)) {
			isSuccess = true;
		}

		SegmentBookingRelatedInfo relatedInfo = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo();
		supplierReference = relatedInfo.getTravellerInfo().get(0).getSupplierBookingId();
		log.info("PNR {} for booking Id {} supplier reference {}", pnr, bookingId, supplierReference);
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		initialize();
		boolean status = false;
		if (StringUtils.isEmpty(pnr))
			pnr = bookingSegments.getAirlinePNR();
		if (listener == null) {
			listener = new RestAPIListener(bookingId);
		}
		FlightRoutes24PayVerifyManager payVerifyManager =
				FlightRoutes24PayVerifyManager.builder().supplierConfiguration(supplierConf).apiURLS(apiURLS)
						.listener(listener).bookingUser(bookingUser).build();
		PayVerifyResponseBody payVerifyResponse = payVerifyManager.payVerify(bookingSegments.getSegmentInfos().get(0)
				.getBookingRelatedInfo().getTravellerInfo().get(0).getSupplierBookingId());

		if (FlightRoutes24Constants.RESPONSE_SUCCESS_STATUS.equals(payVerifyResponse.getStatus())) {
			FlightRoutes24ApplyTicketManager applyTicketManager =
					FlightRoutes24ApplyTicketManager.builder().supplierConfiguration(supplierConf).apiURLS(apiURLS)
							.listener(listener).bookingUser(bookingUser).build();

			String applyTicketToken = StringUtils.join(Arrays.asList(payVerifyResponse.getTotalPrice().toString(),
					payVerifyResponse.getOrderNo(), getSecretKey()), "#");

			ApplyTicketResponse applyTicketResponse = applyTicketManager.applyTicket(applyTicketToken);

			if (FlightRoutes24Constants.RESPONSE_SUCCESS_STATUS.equals(applyTicketResponse.getStatus())) {
				FlightRoutes24BookingDetailManager queryOrderManager = FlightRoutes24BookingDetailManager.builder()
						.supplierConfiguration(supplierConf).apiURLS(apiURLS).listener(listener).bookingId(bookingId)
						.bookingUser(bookingUser).build();

				String queryOrderToken =
						StringUtils.join(Arrays.asList(applyTicketResponse.getOrderNo(), getSecretKey()), "#");
				status = queryOrderManager.queryOrder(queryOrderToken, bookingSegments.getSegmentInfos());
			}
		}
		return status;
	}

	public String getSecretKey() {
		return ObjectUtils.firstNonNull(supplierConf.getSupplierCredential().getPassword(),
				supplierConf.getSupplierCredential().getTxnPassword());
	}
}
