package com.tgs.services.fms.sources.flightroutes24;

import applyTicket.ApplyTicketRequest;
import applyTicket.ApplyTicketResponse;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;
import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.APPLYTICKET_URL;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;

@Slf4j
@SuperBuilder
@AllArgsConstructor
public class FlightRoutes24ApplyTicketManager extends FlightRoutes24ServiceManager {

	protected boolean status;

	protected int paymentMethod;

	public ApplyTicketResponse applyTicket(String token) {
		HttpUtils httpUtils = null;
		ApplyTicketResponse responseBody = new ApplyTicketResponse();
		ApplyTicketRequest requestBody = null;
		try {
			requestBody = generateRequestBody(token);
			httpUtils = HttpUtils.builder().urlString(getBookURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(ApplyTicketResponse.class).orElse(null);
			if (FlightRoutes24Constants.RESPONSE_SUCCESS_STATUS.equals(responseBody.getStatus()))
				status = true;
		} catch (IOException e) {
			log.error("Flight Routes 24 Apply ticket failed for booking id {} {}", bookingId, e);

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "ApplyTicket RQ"),
					formatRQRS(httpUtils.getResponseString(), "ApplyTicket RS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("ApplyTicket").build());
		}
		return responseBody;
	}

	private ApplyTicketRequest generateRequestBody(String token) {
		ApplyTicketRequest requestBody = null;
		try {
			requestBody = new ApplyTicketRequest();
			requestBody.setCid(getClientid());
			requestBody.setToken(EncryptionUtils.encryptUsingCBC(getSecretKey(), token));
			requestBody.setPaymentMethod(paymentMethod);
		} catch (Exception e) {
			throw new SupplierUnHandledFaultException("Encryption Failed for Supplier Payment");
		}
		return requestBody;
	}

	public String getBookURL() {
		String bookURL = null;
		if (apiURLS == null) {
			bookURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), APPLYTICKET_URL);
		} else {
			bookURL = apiURLS.getPricingURL();
		}
		log.debug("FlightRoute24 Apply Ticket URL {}", bookURL);
		return bookURL;
	}
}
