package com.tgs.services.fms.sources.flightroutes24;

import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.QUERYORDER_URL;
import java.io.IOException;
import java.util.List;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import QueryOrder.QueryOrderRequest;
import QueryOrder.QueryOrderResponse;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@NoArgsConstructor
@Slf4j
@SuperBuilder
public class FlightRoutes24BookingDetailManager extends FlightRoutes24ServiceManager {

	protected boolean status;

	public boolean queryOrder(String token, List<SegmentInfo> segmentInfos) {
		HttpUtils httpUtils = null;
		QueryOrderRequest requestBody = null;
		QueryOrderResponse responseBody = null;
		try {
			requestBody = generateRequestBody(token);
			httpUtils = HttpUtils.builder().urlString(getBookURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(QueryOrderResponse.class).orElse(null);
			if (FlightRoutes24Constants.RESPONSE_SUCCESS_STATUS.equals(responseBody.getStatus())) {
				for (SegmentInfo segmentInfo : segmentInfos) {
					for (int i = 0; i < segmentInfo.getBookingRelatedInfo().getTravellerInfo().size(); i++) {
						if (!responseBody.getPassengers()[i].getTicketNo().equals("")) {
							BookingUtils.updateTicketNumber(segmentInfos,
									(responseBody.getPassengers())[i].getTicketNo(),
									segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(i));
							status = true;
						} else {
							status = false;
							break;
						}
					}
				}
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "QueryOrderRQ"),
					formatRQRS(httpUtils.getResponseString(), "QueryOrderRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("QueryOrder").build());
		}
		return status;
	}

	private QueryOrderRequest generateRequestBody(String token) {
		QueryOrderRequest requestBody = new QueryOrderRequest();
		try {
			requestBody.setCid(getClientid());
			requestBody.setToken(EncryptionUtils.encryptUsingCBC(getSecretKey(), token));
		} catch (Exception e) {
			log.error("unable to encrypt token for booking detail ", e);
		}
		return requestBody;
	}

	public String getBookURL() {
		String bookURL = null;
		if (apiURLS == null)
			bookURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), QUERYORDER_URL);
		else
			bookURL = apiURLS.getAssesfeeURL();
		log.debug("FlightRoute24 Query Order URL {}", bookURL);
		return bookURL;
	}
}
