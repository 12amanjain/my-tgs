package com.tgs.services.fms.sources.flightroutes24;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import apiResponse.Segment;
import booking.BookingRequest;
import booking.BookingResponse;
import booking.Contact;
import booking.FlightSegments;
import booking.Passenger;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.BOOKING_URL;
import static com.tgs.services.fms.sources.flightroutes24.FlightRoutes24Constants.CARD_TYPE;
import static com.tgs.services.fms.sources.flightroutes24.FlightRoutes24Constants.NATIONALITY;
import static com.tgs.services.fms.sources.flightroutes24.FlightRoutes24Constants.CARD_EXPIRY_DATE;
import static com.tgs.services.fms.sources.flightroutes24.FlightRoutes24Constants.CARD_NUMBER;


@SuperBuilder
@Slf4j
final class FlightRoutes24BookingManager extends FlightRoutes24ServiceManager {
	private List<SegmentInfo> segmentInfos;
	@Getter
	private Calendar holdTimeLimit;

	public String createPNR() {
		HttpUtils httpUtils = null;
		BookingRequest requestBody = null;
		try {
			requestBody = generateRequestBody();
			httpUtils = HttpUtils.builder().urlString(getBookURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			BookingResponse responseBody = httpUtils.getResponse(BookingResponse.class).orElse(null);
			if (responseBody.getOrderNo() != null) {
				bookingOrderNo = responseBody.getOrderNo();
				BookingUtils.updateSupplierBookingId(segmentInfos, responseBody.getOrderNo());
				return responseBody.getPnrCode();
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "BookRQ"),
					formatRQRS(httpUtils.getResponseString(), "BookRSHold"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("6-B-Book").build());
		}
		return null;
	}

	private BookingRequest generateRequestBody() {
		BookingRequest bookingRequestBody = null;
		try {
			SegmentInfo segmentInfo = segmentInfos.get(0);
			int count = 0;
			tokenId = FlightRoutes24Utils.getEncryptedKey(getSecretKey(), segmentInfo);

			List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
			Passenger passengers[] = new Passenger[travellerInfos.size()];
			List<Segment> fromSegments = new ArrayList<>();
			List<Segment> retSegments = new ArrayList<>();
			for (SegmentInfo segment : segmentInfos) {
				if (!segment.isReturnSegment)
					fromSegments.add(createSegment(segment));
				else
					retSegments.add(createSegment(segment));
			}
			FlightSegments flightSegments =
					FlightSegments.builder().fromSegments(fromSegments).retSegments(retSegments).build();
			Contact contact = Contact.builder().name(travellerInfos.get(0).getFullName())
					.email(AirSupplierUtils.getEmailId(deliveryInfo))
					.mobile(AirSupplierUtils.getContactNumber(deliveryInfo)).build();
			for (int paxIndex = 0; paxIndex < travellerInfos.size(); paxIndex++) {

				FlightTravellerInfo travellerInfo = travellerInfos.get(paxIndex);
				passengers[paxIndex] = Passenger.builder()

						.firstName(EncryptionUtils.encryptUsingCBC(getSecretKey(), travellerInfo.getFirstName()))
						.lastName(EncryptionUtils.encryptUsingCBC(getSecretKey(), travellerInfo.getLastName()))
						.ageType(FlightRoutes24AgeType.valueOf(travellerInfo.getPaxType().name()).getCode())
						.gender(FlightRoutes24Utils.getGender(travellerInfo.getPaxType(), travellerInfo.getTitle()))
						.birthday(getDOB(travellerInfo))
						.cardNum(EncryptionUtils.encryptUsingCBC(getSecretKey(),
								CARD_NUMBER + getRandomCardNumber(count++)))
						.cardType(CARD_TYPE).cardIssuePlace(NATIONALITY).cardExpired(CARD_EXPIRY_DATE)
						.nationality(NATIONALITY).build();
			}

			bookingRequestBody = BookingRequest.builder().cid(getClientid()).token(tokenId).flightType(getFlightType())
					.contact(contact).passengers(Arrays.asList(passengers)).flightSegments(flightSegments).build();
		} catch (Exception e) {
			throw new SupplierUnHandledFaultException("Encryption for Booking Request ");
		}
		return bookingRequestBody;
	}

	private Segment createSegment(SegmentInfo segment) {
		return populateSegment(segment);
	}

	private String getFlightType() {
		if (FlightRoutes24Utils.isReturnFlight(segmentInfos))
			return FlightRoutes24SearchType.RETURN.getValue();
		else
			return FlightRoutes24SearchType.ONEWAY.getValue();
	}

	private String getDOB(FlightTravellerInfo travellerInfo) {
		LocalDate dobDate = null;
		if (travellerInfo.getDob() != null)
			dobDate = travellerInfo.getDob();
		return getDate(dobDate);
	}

	public String getBookURL() {
		String bookURL = null;
		if (apiURLS == null)
			bookURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), BOOKING_URL);
		else
			bookURL = apiURLS.getReservationURL();
		log.debug("FR24 Book URL {}", bookURL);
		return bookURL;
	}

	public String getRandomCardNumber(int count) {
		return count + "";
	}

}
