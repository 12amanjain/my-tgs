package com.tgs.services.fms.sources.flightroutes24;

public class FlightRoutes24Constants {


	public static String RESPONSE_SUCCESS_STATUS = "0";
	public static String CARD_TYPE = "PP";
	public static String LANGUAGE = "EN";
	public static String CARD_NUMBER = "1234567";
	public static String CARD_EXPIRY_DATE = "20230101";
	public static String NATIONALITY = "CN";

}
