package com.tgs.services.fms.sources.flightroutes24;

import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.FARERULE_URL;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import FareRule.FareRuleRequestBody;
import FareRule.FareRuleResponseBody;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class FlightRoutes24FareRuleManager extends FlightRoutes24ServiceManager {

	public TripFareRule getFareRule(String bookingId, TripInfo tripInfo) {
		HttpUtils httpUtils = null;
		try {
			String data = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareKey();
			String OrderNo = getOrderNo(tripInfo);
			FareRuleRequestBody requestBody = new FareRuleRequestBody(getClientid(), data, OrderNo);
			httpUtils =
					HttpUtils.builder().urlString(getFareRuleURL()).postData(GsonUtils.getGson().toJson(requestBody))
							.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			FareRuleResponseBody responseBody = httpUtils.getResponse(FareRuleResponseBody.class).orElse(null);
			if (requestBody != null) {
				return parseResponse(responseBody, tripInfo);
			}
		} catch (IOException e) {
			log.error("Unable to get fare rule for searchId {} ", bookingId, e);
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "FareRuleRQ"),
					formatRQRS(httpUtils.getResponseString(), "FareRuleRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("3-R-FareRuleRS").build());
		}
		return null;
	}


	private TripFareRule parseResponse(FareRuleResponseBody responseBody, TripInfo tripInfo) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		Map<String, String> fareRuleMap = new HashMap<>();
		FareRuleInformation fareRuleInfo = new FareRuleInformation();
		if (!CollectionUtils.isEmpty(responseBody.getFareRules())) {
			fareRuleMap = convertToMap(responseBody.getFareRules().get(0).getFareRule());
		}
		if (MapUtils.isNotEmpty(fareRuleMap)) {
			for (String key : fareRuleMap.keySet()) {
				fareRuleInfo.getMiscInfo().put(key, fareRuleMap.get(key));
			}
		} else if (StringUtils.isNotBlank(responseBody.getMsg())) {
			fareRuleInfo.getMiscInfo().put(FareRuleManager.getRuleKey(tripInfo), responseBody.getMsg());
		}
		if (fareRuleInfo != null) {
			tripFareRule.getFareRule().put(FareRuleManager.getRuleKey(tripInfo), fareRuleInfo);
		}
		return tripFareRule;
	}


	public String getFareRuleURL() {
		String fareRuleURL = null;
		if (apiURLS == null) {
			fareRuleURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), FARERULE_URL);
		} else {
			fareRuleURL = apiURLS.getFareRuleURL();
		}
		log.debug("FlightRoute24 FareRule URL {}", fareRuleURL);
		return fareRuleURL;
	}

	public String getOrderNo(TripInfo tripInfo) {
		if (tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
			if (tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0)
					.getSupplierBookingId() != null) {
				return tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0)
						.getSupplierBookingId();
			}
		}
		return null;
	}

}
