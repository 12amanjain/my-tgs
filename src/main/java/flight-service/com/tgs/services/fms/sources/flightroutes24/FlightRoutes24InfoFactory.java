package com.tgs.services.fms.sources.flightroutes24;

import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FlightRoutes24InfoFactory extends AbstractAirInfoFactory {
	protected RestAPIListener listener = null;
	protected String tokenId;
	protected FlightAPIURLRuleCriteria apiURLS;

	public FlightRoutes24InfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener("");
		}
		apiURLS = AirUtils.getAirEndPointURL(sourceConfiguration);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		FlightRoutes24SearchManager searchManager = null;
		searchManager = FlightRoutes24SearchManager.builder().supplierConfiguration(supplierConf)
				.searchQuery(searchQuery).apiURLS(apiURLS).moneyExchnageComm(moneyExchnageComm).listener(listener)
				.bookingUser(user).build();
		searchResult = searchManager.doSearch();
		return;
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo tripInfo = null;
		FlightRoutes24ReviewManager reviewManager = null;
		try {
			initialize();
			listener.setKey(bookingId);
			reviewManager = FlightRoutes24ReviewManager.builder().supplierConfiguration(supplierConf)
					.searchQuery(searchQuery).apiURLS(apiURLS).bookingId(bookingId).moneyExchnageComm(moneyExchnageComm)
					.listener(listener).bookingUser(user).build();
			tripInfo = reviewManager.reviewTripInfo(selectedTrip);
		} finally {
			if (reviewManager.isReviewSuccess() && tripInfo != null) {
				storeBookingSession(bookingId, null, null, tripInfo);
			}
		}
		return tripInfo;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo tripInfo, String bookingId, boolean isdetailed) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		initialize();
		listener.setKey(bookingId);
		FlightRoutes24FareRuleManager fareRuleServiceManager = FlightRoutes24FareRuleManager.builder()
				.supplierConfiguration(supplierConf).listener(listener).apiURLS(apiURLS).bookingUser(user).build();
		tripFareRule = fareRuleServiceManager.getFareRule(bookingId, tripInfo);
		return tripFareRule;
	}
}
