package com.tgs.services.fms.sources.flightroutes24;

import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.PAYVERIFY_URL;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import PayVerify.PayVerifyRequestBody;
import PayVerify.PayVerifyResponseBody;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;

@SuperBuilder
@Slf4j
public class FlightRoutes24PayVerifyManager extends FlightRoutes24ServiceManager {

	public PayVerifyResponseBody payVerify(String token) {
		HttpUtils httpUtils = null;
		PayVerifyRequestBody requestBody = null;
		PayVerifyResponseBody responseBody = null;
		try {
			requestBody = generateRequestBody(token);

			httpUtils = HttpUtils.builder().urlString(getBookURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();

			responseBody = httpUtils.getResponse(PayVerifyResponseBody.class).orElse(null);

		} catch (IOException e) {
			log.error("Flight Routes 24 Pay Verify failed for booking id {}", bookingId, e);

		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "PayVerify RQ"),
					formatRQRS(httpUtils.getResponseString(), "PayVerify RS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("7-PayVerify").build());
		}
		return responseBody;
	}

	private PayVerifyRequestBody generateRequestBody(String token) {
		PayVerifyRequestBody requestBody = new PayVerifyRequestBody();
		try {
			requestBody.setCid(getClientid());
			requestBody.setToken(EncryptionUtils.encryptUsingCBC(getSecretKey(), token));
		} catch (Exception e) {
			log.error("Encrypt failed for pay verify ", e);
		}
		return requestBody;
	}

	public String getBookURL() {
		String bookURL = null;
		if (apiURLS == null) {
			bookURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), PAYVERIFY_URL);
		} else
			bookURL = apiURLS.getPnrPaymentURL();
		log.debug("FlightRoute24 Pay verify URL {}", bookURL);
		return bookURL;
	}
}
