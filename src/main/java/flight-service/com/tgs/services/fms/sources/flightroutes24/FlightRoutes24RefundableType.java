package com.tgs.services.fms.sources.flightroutes24;

import lombok.Getter;

@Getter
enum FlightRoutes24RefundableType {

	NONREFUNDABLE("T"), REFUND_CONDITION("H"), FREE_TO_REFUND("F"), ACCORDING_TO_THE_AIRLINE("E");

	private final String code;

	private FlightRoutes24RefundableType(String code) {
		this.code = code;
	}
}
