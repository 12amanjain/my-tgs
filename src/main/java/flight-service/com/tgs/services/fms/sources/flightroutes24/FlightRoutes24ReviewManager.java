package com.tgs.services.fms.sources.flightroutes24;

import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.REVIEW_URL;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import apiResponse.FareQuoteRequest;
import apiResponse.FareQuoteResponse;
import apiResponse.Route;
import apiResponse.Segment;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
@Getter
final class FlightRoutes24ReviewManager extends FlightRoutes24ServiceManager {

	private boolean isReviewSuccess;

	public TripInfo reviewTripInfo(TripInfo selectedTrip) {
		HttpUtils httpUtils = null;
		FareQuoteResponse reviewResponse = null;
		TripInfo selectedTripInfo = null;
		try {
			String flightType = FlightRoutes24SearchType.valueOf(searchQuery.getSearchType().name()).getValue();
			FareQuoteRequest requestBody = createReviewRequestBody(selectedTrip, flightType, getClientid());
			httpUtils = HttpUtils.builder().urlString(getReviewURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			selectedTripInfo = selectedTrip;
			reviewResponse = httpUtils.getResponse(FareQuoteResponse.class).orElse(null);
			if (!parseResponse(reviewResponse, selectedTrip)) {
				selectedTripInfo = null;
				isReviewSuccess = false;
			}
		} catch (IOException ioe) {
			throw new SupplierRemoteException(ioe);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "FR24-Review"),
					formatRQRS(httpUtils.getResponseString(), "FR24-Review"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("FR24-Review").build());
		}
		return selectedTripInfo;
	}

	private FareQuoteRequest createReviewRequestBody(TripInfo selectedTrip, String flightType, String cid) {
		FareQuoteRequest requestBody = new FareQuoteRequest();
		requestBody.setAdultNum(selectedTrip.getPaxInfo().get(PaxType.ADULT));
		requestBody.setChildrenNum(selectedTrip.getPaxInfo().get(PaxType.CHILD));
		requestBody.setInfantNum(selectedTrip.getPaxInfo().get(PaxType.INFANT));
		requestBody.setCid(cid);
		requestBody.setFlightType(flightType);
		Route route = new Route();
		route.setData(selectedTrip.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo().getFareKey());
		List<Segment> fromSeg = new ArrayList<Segment>();
		for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {
			if (segment.getIsReturnSegment()) {
				Segment seg = populateSegment(segment);
				List<Segment> retSeg = new ArrayList<Segment>();
				retSeg.add(seg);
				route.setRetSegments(retSeg);
				requestBody.setRoute(route);
			} else {
				Segment seg = populateSegment(segment);
				fromSeg.add(seg);
				route.setFromSegments(fromSeg);
				requestBody.setRoute(route);
			}
		}
		return requestBody;
	}

	private boolean parseResponse(FareQuoteResponse reviewResponse, TripInfo selectedTrip) {
		boolean isSuccess = false;
		if (checkResponseError(reviewResponse.getStatus())) {

			throw new NoSeatAvailableException(reviewResponse.getMsg());
		}
		int retSegCount = 0;
		int fromSegCount = 0;

		for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {

			if (segment.getIsReturnSegment()) {
				segment.setDepartTime(
						getResponseDate(reviewResponse.getRoute().getRetSegments().get(retSegCount).getDepTime()));
				segment.setArrivalTime(
						getResponseDate(reviewResponse.getRoute().getRetSegments().get(retSegCount++).getArrTime()));
			} else {
				segment.setDepartTime(
						getResponseDate(reviewResponse.getRoute().getFromSegments().get(fromSegCount).getDepTime()));
				segment.setArrivalTime(
						getResponseDate(reviewResponse.getRoute().getFromSegments().get(fromSegCount++).getArrTime()));
			}
		}
		if (updateFareDetails(reviewResponse, selectedTrip)) {
			isReviewSuccess = true;
			isSuccess = true;
		}
		return isSuccess;
	}

	private boolean updateFareDetails(FareQuoteResponse reviewResponse, TripInfo selectedTrip) {
		boolean isSuccess = false;
		// double responsePrice = reviewResponse.getRoute().getPrice().getTotalPrice();
		// double tripTotal = AirUtils.getTotalFare(selectedTrip);
		// double totalFare = Math.round(tripTotal * 100.0) / 100.0;
		// double totalTripFare = getAmountBasedOnCurrency(totalFare, getCurrencyCode());
		int segCount = 0;
		if (reviewResponse.getStatus() == 0) {
			for (Segment segment : reviewResponse.getRoute().getFromSegments()) {

				populatePriceInfo(reviewResponse.getRoute(), segment, selectedTrip.getSegmentInfos().get(segCount),
						segCount);
				segCount++;
			}
			segCount = 0;
			if (!reviewResponse.getRoute().getRetSegments().isEmpty()) {
				for (Segment segment : reviewResponse.getRoute().getRetSegments()) {
					populatePriceInfo(reviewResponse.getRoute(), segment, selectedTrip.getSegmentInfos().get(segCount),
							segCount);
					segCount++;
				}
			}
			isSuccess = true;
		}
		return isSuccess;
	}

	public String getReviewURL() {
		String searchURL = null;
		if (apiURLS == null) {
			searchURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), REVIEW_URL);
		} else {
			// for verify.do url
			searchURL = apiURLS.getRetrieveURL();
		}
		log.debug("FlightRoute24 FareQuote URL {}", searchURL);
		return searchURL;
	}
}
