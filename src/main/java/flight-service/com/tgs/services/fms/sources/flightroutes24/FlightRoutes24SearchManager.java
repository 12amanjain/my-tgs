package com.tgs.services.fms.sources.flightroutes24;

import static com.tgs.services.fms.sources.flightroutes24.FR24Urls.FR24Urls.SEARCH_URL;
import static com.tgs.services.fms.sources.flightroutes24.FlightRoutes24Constants.LANGUAGE;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import apiResponse.Response;
import apiResponse.Route;
import apiResponse.SearchRequestBody;
import apiResponse.Segment;
import apiResponse.TravelPreference;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
final class FlightRoutes24SearchManager extends FlightRoutes24ServiceManager {

	AirSearchResult airSearchResult;

	private FlightRoutes24CabinClass cabinClass;

	public AirSearchResult doSearch() {
		HttpUtils httpUtils = null;
		Response responseBody = null;
		try {
			SearchRequestBody requestBody = generateRequestBody();
			airSearchResult = new AirSearchResult();
			httpUtils = HttpUtils.builder().urlString(getSearchURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(Response.class).orElse(null);
			if (!checkIfAnyError(responseBody)) {
				traceId = responseBody.getTraceId();
				parseSearchResponse(responseBody);
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SearchRQ"),
					formatRQRS(httpUtils.getResponseString(), "SearchRS"));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
					.type(AirUtils.getLogType("FR-Search", supplierConfiguration)).build());
		}
		return airSearchResult;
	}

	public SearchRequestBody generateRequestBody() {
		RouteInfo onwardRoute = searchQuery.getRouteInfos().get(0);
		String fromDate = getRequestDate(searchQuery.getRouteInfos().get(0).getTravelDate());
		String retDate = null;
		String cabinCode =
				(FlightRoutes24CabinClass.valueOf(searchQuery.getCabinClass().getName().toUpperCase())).getValue();
		Set<AirlineInfo> preferredAirlines = searchQuery.getPreferredAirline();
		String airlineCodes = (preferredAirlines != null && !preferredAirlines.isEmpty())
				? preferredAirlines.iterator().next().getCode()
				: "";
		TravelPreference travelpref = new TravelPreference(cabinCode, airlineCodes);
		String flightType = FlightRoutes24SearchType.valueOf(searchQuery.getSearchType().name()).getValue();
		SearchRequestBody searchRequestBody;
		RouteInfo retRoute = null;
		if (!flightType.equals(FlightRoutes24SearchType.ONEWAY.getValue())) {
			retRoute = searchQuery.getRouteInfos().get(1);
			retDate = getRequestDate(retRoute.getTravelDate());

		}
		searchRequestBody = new SearchRequestBody(getClientid(), flightType,
				onwardRoute.getFromCityOrAirport().getCityCode(), onwardRoute.getToCityOrAirport().getCityCode(),
				onwardRoute.getFromCityOrAirport().getCode(), onwardRoute.getToCityOrAirport().getCode(), fromDate,
				retDate, getCurrencyCode(), LANGUAGE, searchQuery.getPaxInfo().get(PaxType.ADULT),
				searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0), null, travelpref);
		return searchRequestBody;
	}

	private void parseSearchResponse(Response responseBody) {
		if (responseBody == null) {
			return;
		}
		populateTripInfo(responseBody.getRoute());
	}

	public boolean checkIfAnyError(Response responseBody) {
		boolean isError = false;
		List<Route> results = responseBody.getRoute();
		if (results == null || results.size() == 0
				|| responseBody.getFlightType() == null && responseBody.getCid() == "") {
			isError = true;
			if (responseBody != null && StringUtils.isNotBlank(responseBody.getMsg())) {
				throw new NoSearchResultException(responseBody.getMsg());
			} else {
				throw new NoSearchResultException();
			}
		}
		return isError;
	}

	public void populateTripInfo(List<Route> result) {
		List<TripInfo> onwardTripInfos = new ArrayList<>();
		List<TripInfo> returnTripInfos = new ArrayList<>();
		Map<String, List<TripInfo>> tripInfosMap = new HashMap<>();
		for (Route route : result) {
			TripInfo onwardTrip = new TripInfo();
			TripInfo returnTrip = new TripInfo();
			List<SegmentInfo> onwardsegments = new ArrayList<>();
			List<SegmentInfo> retSegments = new ArrayList<>();
			try {
				for (int segCount = 0; segCount < route.getFromSegments().size(); segCount++) {
					SegmentInfo segmentInfo =
							populateSegmentInfo(route, route.getFromSegments().get(segCount), segCount, false);
					onwardsegments.add(segmentInfo);
				}
				for (int segCount = 0; segCount < route.getRetSegments().size(); segCount++) {
					SegmentInfo segmentInfo =
							populateSegmentInfo(route, route.getRetSegments().get(segCount), segCount, true);
					if (searchQuery.isIntlReturn()) {
						onwardsegments.add(segmentInfo);
					} else
						retSegments.add(segmentInfo);
				}
			} catch (Exception e) {
				log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
			}
			if (CollectionUtils.isNotEmpty(onwardsegments)) {
				onwardTrip.getSegmentInfos().addAll(onwardsegments);
				onwardTripInfos.add(onwardTrip);
			}
			if (searchQuery.isDomesticReturn() && CollectionUtils.isNotEmpty(retSegments)) {
				returnTrip.getSegmentInfos().addAll(retSegments);
				returnTripInfos.add(returnTrip);
			}
			// currently we are not getting any special return prices from the supplier
			if ((onwardTrip != null) && (returnTrip != null) && searchQuery.isDomesticReturn()) {
				TripPriceEngine.setSpecialReturnIdentifier(onwardTrip.getSegmentInfos().get(0),
						onwardTrip.getSegmentInfos().get(0).getPriceInfo(0), returnTrip.getSegmentInfos().get(0),
						returnTrip.getSegmentInfos().get(0).getPriceInfo(0));
			}
		}
		if (searchQuery.isIntlReturn()) {
			tripInfosMap.put(TripInfoType.COMBO.getName(), onwardTripInfos);
		} else {
			tripInfosMap.put(TripInfoType.ONWARD.getName(), onwardTripInfos);
			if (searchQuery.isDomesticReturn()) {
				tripInfosMap.put(TripInfoType.RETURN.getName(), returnTripInfos);
			}
		}
		airSearchResult.setTripInfos(tripInfosMap);
	}

	private SegmentInfo populateSegmentInfo(Route route, Segment resultSegment, int segCount, boolean isReturnSegment) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(resultSegment.getDepAirport()));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(resultSegment.getArrAirport()));
		segmentInfo.setFlightDesignator(createFlightDesignator(resultSegment));
		segmentInfo.setDepartTime(getResponseDate(resultSegment.getDepTime()));
		segmentInfo.setArrivalTime(getResponseDate(resultSegment.getArrTime()));
		segmentInfo.getDepartAirportInfo().setTerminal(resultSegment.getDepTerminal());
		segmentInfo.getArrivalAirportInfo().setTerminal(resultSegment.getArrTerminal());
		segmentInfo.setStops(resultSegment.getStops());
		segmentInfo.setStopOverAirports(getStopOverAirports(resultSegment));
		segmentInfo.setIsReturnSegment(isReturnSegment);
		segmentInfo.setPriceInfoList(populatePriceInfo(route, resultSegment, segmentInfo, segCount));
		segmentInfo.setSegmentNum(segCount);

		if (resultSegment.getDuration() == null) {
			segmentInfo.setDuration(segmentInfo.calculateDuration());
		} else
			segmentInfo.setDuration(resultSegment.getDuration());
		return segmentInfo;
	}

	private List<AirportInfo> getStopOverAirports(Segment resultSegment) {
		List<AirportInfo> stopOvers = null;
		if (StringUtils.isNotBlank(resultSegment.getStopCities())) {
			stopOvers = new ArrayList<>();
			if (resultSegment.getStopCities().contains(",")) {
				String[] stops = resultSegment.getStopCities().split(",");
				for (String stop : stops) {
					stopOvers.add(AirportHelper.getAirportInfo(stop));
				}
			}
		}
		return stopOvers;
	}

	private FlightDesignator createFlightDesignator(Segment airline) {
		FlightDesignator flightDesign = new FlightDesignator();
		flightDesign.setAirlineInfo(AirlineHelper.getAirlineInfo(airline.getCarrier()));
		flightDesign.setFlightNumber(airline.getFlightNumber().replaceAll("[A-Z]", ""));
		flightDesign.setEquipType(airline.getAircraftCode());
		return flightDesign;
	}

	public String getSearchURL() {
		String searchURL = null;
		if (apiURLS == null) {
			searchURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl(), SEARCH_URL);
		} else {
			searchURL = apiURLS.getFlightURL();
		}
		log.debug("FlightRoute24 Search URL {}", searchURL);
		return searchURL;
	}
}
