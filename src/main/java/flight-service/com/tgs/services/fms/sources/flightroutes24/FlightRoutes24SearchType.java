package com.tgs.services.fms.sources.flightroutes24;

import lombok.Getter;

@Getter
public enum FlightRoutes24SearchType {

	ONEWAY("1"), RETURN("2");

	private String value;

	private FlightRoutes24SearchType(String value) {
		this.value = value;
	}
}
