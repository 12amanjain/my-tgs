package com.tgs.services.fms.sources.flightroutes24;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.fms.helper.AirlineHelper;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.service.tbo.datamodel.Error;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.ums.datamodel.User;
import apiResponse.Baggage;
import apiResponse.Route;
import apiResponse.Segment;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
abstract class FlightRoutes24ServiceManager {

	protected SupplierConfiguration supplierConfiguration;
	protected AirSearchQuery searchQuery;
	protected String traceId, tokenId, bookingId;
	protected String bookingOrderNo;
	protected RestAPIListener listener;
	protected FlightAPIURLRuleCriteria apiURLS;
	private String reqDate;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected DeliveryInfo deliveryInfo;
	protected User bookingUser;

	public String getRequestDate(LocalDate date) {
		return date.format(DateTimeFormatter.ofPattern("yyyy/MM/dd"));
	}

	public String getDate(LocalDate date) {
		return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	public String getCurrencyCode() {
		String currencyCode = supplierConfiguration.getSupplierCredential().getCurrencyCode();
		if (StringUtils.isNotBlank(currencyCode)) {
			return currencyCode;
		}
		return AirSourceConstants.getClientCurrencyCode();
	}

	public LocalDateTime getResponseDate(String date) {
		return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("yyyyMMddHHmm"));
	}

	public String getDate(LocalDateTime date) {
		return date.format(DateTimeFormatter.ofPattern("yyyyMMddHHmm"));
	}

	public Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		return headerParams;
	}

	protected String getErrorMessage(Error error) {
		String errorMsg = null;
		if (error != null) {
			errorMsg = error.getErrorMessage();
		}
		return errorMsg;
	}

	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	protected BaggageInfo createBaggageInfo(Route result, Segment resultSegment, int count, PaxType paxType) {
		BaggageInfo bgInfo = new BaggageInfo();
		List<Baggage> baggages = result.getRule().getBaggages();
		if (CollectionUtils.isNotEmpty(baggages)) {
			for (Baggage baggage : baggages) {
				if (baggage.getArrAirport().equals(resultSegment.getArrAirport())
						&& baggage.getDepAirport().equals(resultSegment.getDepAirport())) {
					Baggage bag = result.getRule().getBaggages().get(count);
					if (paxType.equals(PaxType.ADULT)) {
						if (bag.getBaggageWeightFree() != null)
							bgInfo.setAllowance(bag.getBaggageWeightFree());
						else
							bgInfo.setAllowance(bag.getBaggageQuantityFree() + "PC");
					} else if (paxType.equals(PaxType.CHILD)) {
						if (bag.getChdBaggageWeightFree() != null)
							bgInfo.setAllowance(bag.getChdBaggageWeightFree());
					} else
						bgInfo.setAllowance(bag.getChdBaggageQuantityFree() + "PC");
				}
			}
		}
		return bgInfo;
	}

	/**
	 * in case of domestic ONEWAY and international flight we are populating the combined price info in first onward
	 * segment and in case of domestic return we are adding price info in first segment of onward as well as return trip
	 */
	protected Map<FareComponent, Double> getFareComponents(Route result, PaxType paxType) {
		Map<FareComponent, Double> fc = new HashMap();
		double airlineTax = 0, baseCharge = 0, serviceFee = 0;
		int dividingFactor = 1;
		if (searchQuery.isDomesticReturn()) {
			dividingFactor = 2;
		}
		if (paxType.equals(PaxType.ADULT)) {
			airlineTax = getAmount(result.getPrice().getAdultTax(), dividingFactor);
			baseCharge = getAmount(result.getPrice().getAdultPrice(), dividingFactor);
			serviceFee = getAmount(result.getPrice().getServiceFee(), dividingFactor);
		}
		if (paxType.equals(PaxType.CHILD)) {
			airlineTax = getAmount(result.getPrice().getChildTax(), dividingFactor);
			baseCharge = getAmount(result.getPrice().getChildPrice(), dividingFactor);
			serviceFee = getAmount(result.getPrice().getServiceFee(), dividingFactor);
		}
		fc.put(FareComponent.BF, baseCharge);
		fc.put(FareComponent.OC, serviceFee);
		fc.put(FareComponent.AT, airlineTax);
		fc.put(FareComponent.TF, baseCharge + airlineTax + serviceFee);
		return fc;
	}

	protected FareDetail createFareDetail(Segment resultSegment, Route result, int count, PaxType paxtype,
			boolean isReturnSegment) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.setCabinClass(searchQuery.getCabinClass());
		fareDetail.setSeatRemaining(Integer.parseInt(resultSegment.getAvailSeatNum()));
		fareDetail.setClassOfBooking(resultSegment.getCabin());
		fareDetail.setRefundableType(getRefundableType(result).getRefundableType());
		if (paxtype.equals(PaxType.ADULT)) {
			fareDetail.setBaggageInfo(createBaggageInfo(result, resultSegment, count, PaxType.ADULT));
			fareDetail.setFareBasis(resultSegment.getFareBasis());
			fareDetail.setFareComponents(null);
			if (!searchQuery.getIsDomestic() && !isReturnSegment && count == 0
					|| count == 0 && searchQuery.getIsDomestic()) {
				fareDetail.setFareComponents(getFareComponents(result, PaxType.ADULT));
			}
		} else {
			if (paxtype.equals(PaxType.CHILD)) {
				fareDetail.setFareType(FareType.PUBLISHED.getName());
				fareDetail.setFareComponents(null);
				fareDetail.setBaggageInfo(createBaggageInfo(result, resultSegment, count, PaxType.CHILD));
				if (count == 0) {
					fareDetail.setFareComponents(getFareComponents(result, PaxType.CHILD));
				}
			}
		}
		return fareDetail;
	}

	// Journey wise pricing
	protected List<PriceInfo> populatePriceInfo(Route result, Segment resultSegment, SegmentInfo segmentInfo,
			int segCount) {
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		String plattingCarrier = resultSegment.getActualCarrier();
		PriceInfo priceInfo = priceInfoList.isEmpty() ? PriceInfo.builder().build() : priceInfoList.get(0);
		if (priceInfo.getSupplierBasicInfo() == null) {
			priceInfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
		}
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		FareDetail fareDetail =
				createFareDetail(resultSegment, result, segCount, PaxType.ADULT, segmentInfo.getIsReturnSegment());
		fareDetails.put(PaxType.values()[0], fareDetail);
		if (searchQuery.getPaxInfo().get(PaxType.CHILD) > 0) {
			FareDetail fareDetail2 =
					createFareDetail(resultSegment, result, segCount, PaxType.CHILD, segmentInfo.getIsReturnSegment());
			fareDetails.put(PaxType.values()[1], fareDetail2);
		}
		// Infant Not Supported currently

		priceInfo.getMiscInfo().setFareKey(result.getData());
		if (StringUtils.isNotBlank(plattingCarrier)) {
			priceInfo.getMiscInfo().setPlatingCarrier(AirlineHelper.getAirlineInfo(plattingCarrier));
		}
		if (priceInfoList.isEmpty())
			priceInfoList.add(priceInfo);
		return priceInfoList;
	}

	protected RefundableType getRefundableType(Route result) {
		if (result.getRule().getRefunds().get(0).getRefundStatus().isEmpty())
			return RefundableType.NON_REFUNDABLE;
		else if (result.getRule().getRefunds().get(0).getRefundStatus()
				.equals(FlightRoutes24RefundableType.NONREFUNDABLE.getCode())) {
			return RefundableType.NON_REFUNDABLE;
		} else if (result.getRule().getRefunds().get(0).getRefundStatus()
				.equals(FlightRoutes24RefundableType.FREE_TO_REFUND.getCode())) {
			return RefundableType.REFUNDABLE;
		} else
			return RefundableType.PARTIAL_REFUNDABLE;
	}

	protected Map<String, String> convertToMap(Object obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

	public String getClientid() {
		return supplierConfiguration.getSupplierCredential().getClientId();
	}

	protected Boolean checkResponseError(int status) {
		Boolean isResponseError = false;
		if (status != 0)
			isResponseError = true;
		return isResponseError;
	}

	protected Segment populateSegment(SegmentInfo segment) {
		Segment Segment = new Segment();
		Segment.setArrAirport(segment.getArrivalAirportInfo().getCode());
		Segment.setArrTime(getDate(segment.getArrivalTime()));
		Segment.setCabin(segment.getPriceInfoList().get(0).getFareDetail(PaxType.ADULT).getClassOfBooking());
		Segment.setCabinGrade(segment.getPriceInfoList().get(0).getFareDetail(PaxType.ADULT).getCabinClass().getCode());
		Segment.setCarrier(segment.getFlightDesignator().getAirlineInfo().getCode());
		Segment.setDepAirport(segment.getDepartAirportInfo().getCode());
		Segment.setDepTime(getDate(segment.getDepartTime()));
		Segment.setFlightNumber(StringUtils.join(segment.getAirlineCode(false), segment.getFlightNumber()));
		return Segment;
	}

	/*
	 * we are getting amount in USD from our supplier but we need it to show the amount in INR in our frontend
	 */
	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.type(AirSourceType.FLIGHTROUTES24.name().toUpperCase()).toCurrency(toCurrency).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	public String getSecretKey() {
		return ObjectUtils.firstNonNull(supplierConfiguration.getSupplierCredential().getPassword(),
				supplierConfiguration.getSupplierCredential().getTxnPassword());
	}

	public double getAmount(Double price, int dividingFactor) {
		if (price == null)
			return 0.0;
		Double amount = price / dividingFactor;
		return getAmountBasedOnCurrency(amount, getCurrencyCode());
	}
}
