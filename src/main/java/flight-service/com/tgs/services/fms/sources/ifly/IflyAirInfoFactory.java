package com.tgs.services.fms.sources.ifly;

import java.util.List;
import com.tgs.services.base.SoapRequestResponseListner;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirSupplierUtils;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class IflyAirInfoFactory extends AbstractAirInfoFactory {

	protected IflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public IflyAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	private void initialize() {
		bindingService = IflyBindingService.builder().configuration(supplierConf).user(user).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		AirSupplierUtils.isStoreLogRequired(searchQuery);
		IflySearchManager searchManager = IflySearchManager.builder().bindingService(bindingService)
				.searchQuery(searchQuery).criticalMessageLogger(criticalMessageLogger).configuration(supplierConf)
				.sourceConfiguration(sourceConfiguration).listener(listener).bookingUser(user).build();
		searchManager.init();
		searchResult = searchManager.doSearch();
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize();
		IflyReviewManager reviewManager = null;
		try {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			reviewManager = IflyReviewManager.builder().bindingService(bindingService).configuration(supplierConf)
					.segmentInfos(selectedTrip.getSegmentInfos()).bookingId(bookingId).searchQuery(searchQuery)
					.criticalMessageLogger(criticalMessageLogger).sourceConfiguration(sourceConfiguration)
					.listener(listener).bookingUser(user).build();
			reviewManager.init();
			reviewManager.setClientSessionId(
					selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getSessionId());
			reviewManager.doPriceQuote();
			reviewManager.doAncillaries(selectedTrip);
		} finally {
			if (selectedTrip != null) {
				storeBookingSession(bookingId, null, null, selectedTrip);
			}
		}
		return selectedTrip;
	}

	@Override
	protected List<SourceRouteInfo> addRoutesToSystem(List<SourceRouteInfo> routes, String searchKey) {
		IFlyRouteManager routeManager = IFlyRouteManager.builder().configuration(supplierConf)
				.criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
				.sourceConfiguration(sourceConfiguration).listener(listener).build();
		routeManager.init();
		// routeManager.addRouteInfoToSystem();
		return routes;
	}

	@Override
	public Boolean initializeStubs() {
		log.info("Initialize Ifly Stubs");
		bindingService = IflyBindingService.builder().configuration(supplierConf).user(user).build();
		return true;
	}

}
