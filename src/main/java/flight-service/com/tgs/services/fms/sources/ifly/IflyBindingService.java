package com.tgs.services.fms.sources.ifly;

import com.ibsplc.wsdl.Ancillary;
import com.ibsplc.wsdl.Availability;
import com.ibsplc.wsdl.Price;
import com.ibsplc.wsdl.Reservations;
import com.ibsplc.www.wsdl.AncillaryStub;
import com.ibsplc.www.wsdl.AvailabilityStub;
import com.ibsplc.www.wsdl.PriceStub;
import com.ibsplc.www.wsdl.ReservationsStub;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.lang3.StringUtils;
import javax.xml.namespace.QName;
import java.util.List;
import java.util.UUID;


@Setter
@Slf4j
@Builder
public class IflyBindingService {

	protected SupplierConfiguration configuration;

	protected String clientToken;

	private static Availability availabilityService;

	private static Price priceService;

	private static Reservations reservationService;

	private static Ancillary ancillary;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	protected User user;

	public static final String HEADER_NAMESPACE =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd";

	private static final String HEADER_OASIS_UTIL =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd";

	private static final String USER_NAME_TOKEN =
			"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText";

	public static final String SECURITY = "Security";
	public static final String USERNAMETOKEN = "UsernameToken";
	public static final String USERNAME = "Username";
	public static final String PASSWORD = "Password";

	static OMFactory factory = OMAbstractFactory.getOMFactory();


	public AvailabilityStub getAvailablityStub() {
		AvailabilityStub availabilityStub = null;
		String availaibilityUrl = null;
		try {
			availaibilityUrl = String.join("", configuration.getSupplierCredential().getUrl(), "AvailabilityPort");
			availabilityStub = new AvailabilityStub(availaibilityUrl);
		} catch (Exception e) {
			log.error("AvailabilityStub Binding Initilaized failed for {} cause", availaibilityUrl, e);
		}
		addCustomSecurityHeader(availabilityStub);
		setProxy(availabilityStub);
		return availabilityStub;
	}


	public PriceStub getPriceStub() {
		PriceStub priceStub = null;
		String priceStubUrl = null;
		try {
			priceStubUrl = String.join("", configuration.getSupplierCredential().getUrl(), "PricePort");
			priceStub = new PriceStub(priceStubUrl);
		} catch (Exception e) {
			log.error("PriceStub Binding Initilaized failed for {} cause", priceStubUrl, e);
		}
		addCustomSecurityHeader(priceStub);
		setProxy(priceStub);
		return priceStub;
	}


	public AncillaryStub getAncillaryStub() {
		AncillaryStub ancillaryStub = null;
		String ancillaryUrl = null;
		try {
			ancillaryUrl = String.join("", configuration.getSupplierCredential().getUrl(), "AncillaryPort");
			ancillaryStub = new AncillaryStub(ancillaryUrl);
		} catch (Exception e) {
			log.error("AncillaryStub Binding Initilaized failed for {} cause", ancillaryUrl, e);
		}
		addCustomSecurityHeader(ancillaryStub);
		setProxy(ancillaryStub);
		return ancillaryStub;
	}

	public ReservationsStub getReservationsStub() {
		ReservationsStub reservationsStub = null;
		String reservationUrl = null;
		try {
			reservationUrl = String.join("", configuration.getSupplierCredential().getUrl(), "ReservationsPort");
			reservationsStub = new ReservationsStub(reservationUrl);
		} catch (Exception e) {
			log.error("AncillaryStub Binding Initilaized failed for {} cause", reservationUrl, e);
		}
		addCustomSecurityHeader(reservationsStub);
		setProxy(reservationsStub);
		return reservationsStub;
	}

	public Reservations getReservationService() {
		if (reservationService == null) {
			try {
				reservationService = new Reservations();
			} catch (Exception e) {
				log.error("Unable to intialize Reservations service", e);
			}
		}
		return reservationService;
	}


	public SupplierCredential getSupplierCredential() {
		return configuration.getSupplierCredential();
	}

	private void addCustomSecurityHeader(Stub stub) {
		if (stub != null && stub._getServiceClient() != null) {
			OMElement omSecurityElement =
					factory.createOMElement(new QName(HEADER_NAMESPACE, "Security", "wsse"), null);
			omSecurityElement.addAttribute("wsu", HEADER_OASIS_UTIL, null);
			OMElement omusertoken =
					factory.createOMElement(new QName(HEADER_NAMESPACE, "UsernameToken", "wsse"), omSecurityElement);
			String token = String.join("-", "UsernameToken", UUID.randomUUID().toString());
			omusertoken.addAttribute("Id", token, null);
			OMElement omuserName =
					factory.createOMElement(new QName(HEADER_NAMESPACE, "Username", "wsse"), omusertoken);
			omuserName.setText(configuration.getSupplierCredential().getUserName());

			OMElement omPassword =
					factory.createOMElement(new QName(HEADER_NAMESPACE, "Password", "wsse"), omusertoken);
			omPassword.addAttribute("Type", USER_NAME_TOKEN, null);
			omPassword.setText(configuration.getSupplierCredential().getPassword());

			omusertoken.addChild(omuserName);
			omusertoken.addChild(omPassword);
			omSecurityElement.addChild(omusertoken);
			stub._getServiceClient().addHeader(omSecurityElement);
		}
	}

	public void setProxy(Stub stub) {
		String proxyAddress = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		String proxy = null;
		if (configuratorInfo != null) {
			proxy = configuratorInfo.getProxyAddress();
		}
		if (StringUtils.isNotBlank(proxy)) {
			int proxyPort = 3128;
			if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
				ServiceClient client = stub._getServiceClient();
				Options options = client.getOptions();
				options.setProperties(null);
				proxyAddress = proxy.split(":")[0];
				proxyPort = Integer.valueOf(proxy.split(":")[1]);
				if (StringUtils.isNotBlank(proxyAddress)) {
					HttpTransportProperties.ProxyProperties proxyProperties =
							new HttpTransportProperties.ProxyProperties();
					proxyProperties.setProxyName(proxyAddress); // 10.10.16.165
					proxyProperties.setProxyPort(proxyPort); // 3128
					options.setProperty(org.apache.axis2.transport.http.HTTPConstants.PROXY, proxyProperties);
					options.setProperty(HTTPConstants.CHUNKED, false);
				}
				client.setOptions(options);
			}
		}

		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			ServiceClient serviceClient = stub._getServiceClient();
			serviceClient.getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			if (StringUtils.isNotBlank(clientToken)) {
				serviceClient.getOptions().setProperty("CLIENT_SESSION_ID", clientToken);
			} else {
				clientToken = UUID.randomUUID().toString();
				serviceClient.getOptions().setProperty("CLIENT_SESSION_ID", UUID.randomUUID().toString());
			}
			setConnectionManager(stub);
		}
	}

	public void updateOptions(Stub stub) {
		if (stub != null) {
			ServiceClient client = stub._getServiceClient();
			Options options = client.getOptions();
			options.setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
			client.setOptions(options);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(20);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}
}
