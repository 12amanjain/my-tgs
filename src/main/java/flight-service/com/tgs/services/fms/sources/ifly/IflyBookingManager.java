package com.tgs.services.fms.sources.ifly;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import com.google.common.util.concurrent.AtomicDouble;
import com.ibsplc.www.ires.simpletypes.CreateBookingRSE;
import com.ibsplc.www.ires.simpletypes.ContactDetails_Type;
import com.ibsplc.www.ires.simpletypes.FareInfoType;
import com.ibsplc.www.ires.simpletypes.ItineraryDetailsType;
import com.ibsplc.www.ires.simpletypes.GuestPaymentInfoType;
import com.ibsplc.www.ires.simpletypes.PaxCountDetailsType;
import com.ibsplc.www.ires.simpletypes.CreateBookingRQ;
import com.ibsplc.www.ires.simpletypes.CreateBookingRQE;
import com.ibsplc.www.ires.simpletypes.AddressType;
import com.ibsplc.www.ires.simpletypes.PnrContactType;
import com.ibsplc.www.wsdl.ReservationsStub;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@SuperBuilder
final class IflyBookingManager extends IflyServiceManager {

	public String createPNR() {
		ReservationsStub reservationsStub = bindingService.getReservationsStub();
		CreateBookingRSE createBookingRS = null;
		CreateBookingRQE createBookingRQ = null;
		try {
			listener.setType(AirUtils.getLogType("CreateBooking", getConfiguration()));
			reservationsStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			createBookingRQ = buildCreateBookingRq();
			createBookingRS = reservationsStub.saveCreateBooking(createBookingRQ);
			if (createBookingRS.getCreateBookingRS() != null
					&& !isErrorResponse(createBookingRS.getCreateBookingRS().getErrorType())) {
				double amountPaid = getAmountPaid(createBookingRS);
				double amountPaidToBeSupplier = getAmountPaidToSupplier(createBookingRS);
				log.info("Booking Confirmed for BookingId {},PNR {},amount paid {} and amountPaidToBeSupplier {} ",
						getBookingId(), createBookingRS.getCreateBookingRS().getPnrNumber(), amountPaid,
						amountPaidToBeSupplier);
				return createBookingRS.getCreateBookingRS().getPnrNumber();
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			reservationsStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	private double getAmountPaid(CreateBookingRSE bookingresponse) {
		BigDecimal amount = new BigDecimal(bookingresponse.getCreateBookingRS().getTotalAmountPaid().getAmount());
		double amountPaid = getAmountBasedOnCurrency(amount,
				bookingresponse.getCreateBookingRS().getTotalAmountPaid().getCurrency());
		return amountPaid;
	}

	private double getAmountPaidToSupplier(CreateBookingRSE bookingresponse) {
		BigDecimal amount = new BigDecimal(bookingresponse.getCreateBookingRS().getTotalAmountToBePaid().getAmount());
		double amountPaid = getAmountBasedOnCurrency(amount,
				bookingresponse.getCreateBookingRS().getTotalAmountToBePaid().getCurrency());
		return amountPaid;
	}

	private CreateBookingRQE buildCreateBookingRq() {
		CreateBookingRQE createBookingRQE = new CreateBookingRQE();
		CreateBookingRQ bookingRequest = new CreateBookingRQ();
		bookingRequest.setAgencyCode(getAgencyCode());
		bookingRequest.setCurrentAgentID(getAgencyCode());
		bookingRequest.setOriginalAgentID(getAgencyCode());
		bookingRequest.setCanContinueWithSsr(true);
		bookingRequest.setBookerDetails(buildBookerDetails());
		bookingRequest.setBookingChannel(getBookingChannelType());
		bookingRequest.setCurrency(getCurrencyCode());
		bookingRequest.setNumberOfSeats(AirUtils.getPaxCountFromTravellerInfo(
				getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), false));
		bookingRequest.setAirlineCode(iFlyAirline.getAirlineCode());
		bookingRequest.setFareInfo(buildFareInfoTypes());
		bookingRequest.setItineraryDetails(buildItnearyDetailsTypes());
		bookingRequest.setPaxCountDetails(buildPaxCountDetails());
		bookingRequest.setGuestDetails(buildGuestdetails());
		bookingRequest.setGuestPaymentInfo(buildGuestPaymentInfos());
		bookingRequest.setSSRDetails(buildbaggageSSR());
		bookingRequest.setPnrContact(buildPnrContact());
		bookingRequest.setPointOfSale(getPointOfSale());
		createBookingRQE.setCreateBookingRQ(bookingRequest);
		return createBookingRQE;
	}

	private PnrContactType[] buildPnrContact() {
		PnrContactType[] contactTypes = new PnrContactType[1];
		PnrContactType pnrContactType = new PnrContactType();
		pnrContactType.setIsPrefferedContact(true);
		pnrContactType.setAddress(getPnrAddress());
		// H means Home contact
		pnrContactType.setContactType(ContactDetails_Type.H);
		contactTypes[0] = pnrContactType;
		return contactTypes;
	}

	private AddressType getPnrAddress() {
		String areaCode;
		AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(getOrder().getBookingUserId(), getClientInfo());
		areaCode = String.valueOf(getClientInfo().getAreaCode());
		AddressType addressType = new AddressType();
		addressType.setCountryName(contactAddress.getCityInfo().getCountry().toUpperCase());
		addressType.setAddress1(contactAddress.getAddress());
		addressType.setCity(contactAddress.getCityInfo().getName());
		addressType.setEmailAddress(AirSupplierUtils.getEmailId(getDeliveryInfo()));
		addressType.setZipCode(areaCode);
		addressType.setPhoneNumber(AirSupplierUtils.getContactNumber(getDeliveryInfo()));
		addressType.setCellNumber(AirSupplierUtils.getContactNumber(getDeliveryInfo()));
		addressType.setSendItineraryToEmailId(true);
		addressType.setSendItineraryToSMS(true);
		addressType.setPhoneNumberCountryCode(StringUtils.join("+", getClientInfo().getCountryCode()));
		addressType.setCellNumberCountryCode(StringUtils.join("+", getClientInfo().getCountryCode()));
		return addressType;
	}


	private FareInfoType[] buildFareInfoTypes() {
		FareInfoType[] fareInfoTypes = new FareInfoType[1];
		fareInfoTypes[0] = getFareInfoType();
		return fareInfoTypes;
	}

	private ItineraryDetailsType[] buildItnearyDetailsTypes() {
		ItineraryDetailsType[] itineraryDetails = new ItineraryDetailsType[1];
		itineraryDetails[0] = buildItnearyDetailsType();
		return itineraryDetails;
	}

	public PaxCountDetailsType[] buildPaxCountDetails() {
		List<PaxCountDetailsType> paxCountDetailsTypes = new ArrayList<>();
		Map<PaxType, FareDetail> fareDetails = getSegmentInfos().get(0).getPriceInfoList().get(0).getFareDetails();
		for (PaxType paxType : fareDetails.keySet()) {
			int quantity = AirUtils.getParticularPaxTravellerInfo(
					getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), paxType).size();
			PaxCountDetailsType paxInfo = new PaxCountDetailsType();
			paxInfo.setPaxCount(quantity);
			paxInfo.setPaxType(paxType.toString());
			paxCountDetailsTypes.add(paxInfo);
		}
		return paxCountDetailsTypes.toArray(new PaxCountDetailsType[0]);
	}

	private GuestPaymentInfoType[] buildGuestPaymentInfos() {
		long index = 0;

		List<FlightTravellerInfo> travellerList = getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
		GuestPaymentInfoType[] guestPaymentList = new GuestPaymentInfoType[travellerList.size()];
		int paxIndex = 0;
		for (FlightTravellerInfo travellerInfo : travellerList) {
			GuestPaymentInfoType paymentInfo = new GuestPaymentInfoType();
			paymentInfo.setGuestId((index++));
			// Agency Credit
			paymentInfo.setFormOfPaymentCode(iFlyAirline.getAgencyPaymentType());
			paymentInfo.setPaymentAmount(getAmountOfTraveller(travellerInfo));
			paymentInfo.setExchangeRate(iFlyAirline.getExchangeRate());
			// set anything(Here it's : atlas)
			paymentInfo.setAgentID(getAgencyCode());
			paymentInfo.setPaymentCurrency(getCurrencyCode());
			guestPaymentList[paxIndex++] = paymentInfo;
		}
		return guestPaymentList;
	}

	public Double getAmountOfTraveller(FlightTravellerInfo travellerInfo) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		getSegmentInfos().forEach(segmentInfo -> {
			List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
			List<FlightTravellerInfo> travellers =
					AirUtils.getParticularPaxTravellerInfo(travellerInfos, travellerInfo.getPaxType());
			Optional<FlightTravellerInfo> filteredTraveller = travellers.stream()
					.filter(t -> t.getFirstName().equals(travellerInfo.getFirstName())
							&& t.getLastName().equals(travellerInfo.getLastName())
							&& travellerInfo.getPaxType().equals(t.getPaxType()))
					.findFirst();
			if (filteredTraveller.isPresent()) {
				totalAmount.getAndAdd(BookingUtils.getAmountChargedByAirline(filteredTraveller.get()));
			}
		});
		return totalAmount.get();
	}


}
