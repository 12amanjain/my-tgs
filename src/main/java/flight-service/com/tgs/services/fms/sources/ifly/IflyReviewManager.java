package com.tgs.services.fms.sources.ifly;

import com.google.api.client.repackaged.com.google.common.base.Enums;
import com.google.common.reflect.TypeToken;
import com.google.common.util.concurrent.AtomicDouble;
import com.ibsplc.www.ires.simpletypes.AncillaryService;
import com.ibsplc.www.ires.simpletypes.SaleableAncillaries;
import com.ibsplc.www.ires.simpletypes.PriceBreakDownType;
import com.ibsplc.www.ires.simpletypes.SurchargeType;
import com.ibsplc.www.ires.simpletypes.TaxType;
import com.ibsplc.www.ires.simpletypes.AmountDetailsType;
import com.ibsplc.www.ires.simpletypes.GuestPriceBreakDownType;
import com.ibsplc.www.ires.simpletypes.ItinPriceType;
import com.ibsplc.www.ires.simpletypes.TotalAmountDetailsType;
import com.ibsplc.www.ires.simpletypes.ConfirmPriceRS;
import com.ibsplc.www.ires.simpletypes.ConfirmPriceRQ;
import com.ibsplc.www.ires.simpletypes.FareInfoType;
import com.ibsplc.www.ires.simpletypes.ItineraryDetailsType;
import com.ibsplc.www.ires.simpletypes.FlightSegmentDetailsType;
import com.ibsplc.www.ires.simpletypes.PaxCountDetailsType;
import com.ibsplc.www.ires.simpletypes.ListBaggageServicesRQ;
import com.ibsplc.www.ires.simpletypes.ListBaggageServicesRS;
import com.ibsplc.www.ires.simpletypes.PassengerList;
import com.ibsplc.www.ires.simpletypes.Passenger;
import com.ibsplc.www.wsdl.AncillaryStub;
import com.ibsplc.www.wsdl.PriceStub;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

@Getter
@Slf4j
@SuperBuilder
final class IflyReviewManager extends IflyServiceManager {

	protected Double totalAmountToPaid;

	protected void doPriceQuote() {
		PriceStub priceStub = bindingService.getPriceStub();
		ConfirmPriceRQ confirmPriceRQ = null;
		ConfirmPriceRS confirmPriceRS = null;
		try {
			listener.setType(AirUtils.getLogType("ConfirmPrice", getConfiguration()));
			priceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			confirmPriceRQ = buildConfirmPriceRq();
			confirmPriceRS = priceStub.confirmPrice(confirmPriceRQ);
			if (!isErrorResponse(confirmPriceRS.getErrorType())) {
				if (isBookingFlow()) {
					parseTotalTobePaid(confirmPriceRS);
				} else {
					parseReviewResponse(confirmPriceRS);
					parseFlightSegmentId(confirmPriceRS);
				}
			} else {
				throw new NoSeatAvailableException(getErrorMessage());
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			priceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private void parseTotalTobePaid(ConfirmPriceRS confirmPriceRS) {
		AtomicDouble totalAmount = new AtomicDouble();
		TotalAmountDetailsType[] amountDetailsTypes = confirmPriceRS.getTotalAmountDetails();
		for (TotalAmountDetailsType detailsType : amountDetailsTypes) {
			for (AmountDetailsType amountDetailsType : detailsType.getAmount()) {
				BigDecimal amount = new BigDecimal(amountDetailsType.getAmount());
				Double convertedAmount = getAmountBasedOnCurrency(amount, amountDetailsType.getCurrency());
				totalAmount.getAndAdd(convertedAmount);
			}
		}
		parseFlightSegmentId(confirmPriceRS);
		totalAmountToPaid = new Double(totalAmount.get());
	}


	private void parseFlightSegmentId(ConfirmPriceRS confirmPriceRS) {
		ItineraryDetailsType[] itineraryDetailsTypes = confirmPriceRS.getItinerary();
		for (ItineraryDetailsType detailsType : itineraryDetailsTypes) {
			int segmentIndex = 0;
			for (FlightSegmentDetailsType segmentDetail : detailsType.getFlightSegmentDetails()) {
				getSegmentInfos().get(segmentIndex++).getPriceInfo(0).getMiscInfo()
						.setTokenId(segmentDetail.getFlightSegmentGroupID());
			}
		}
	}

	private ConfirmPriceRQ buildConfirmPriceRq() {
		ConfirmPriceRQ request = new ConfirmPriceRQ();
		request.setAgencyCode(getConfiguration().getSupplierCredential().getOrganisationCode());
		request.setCanContinueWithSsr(true);
		request.setBookingChannel(getBookingChannelType());
		request.setCurrency(getCurrencyCode());
		request.setAirlineCode(iFlyAirline.getAirlineCode());
		request.setNumberOfSeats(getPaxCount());
		request.setPointOfSale(getPointOfSale());
		request.setFareInfo(buildFareInfoTypes());
		request.setItineraryDetails(buildItnearyDetailsTypes());
		request.setPaxCountDetails(getPaxCountDetails());
		if (isBookingFlow()) {
			request.setGuestDetails(buildGuestdetails());
			request.setSSRDetails(buildbaggageSSR());
		}
		return request;
	}

	private FareInfoType[] buildFareInfoTypes() {
		FareInfoType[] fareInfoTypes = new FareInfoType[1];
		fareInfoTypes[0] = getFareInfoType();
		return fareInfoTypes;
	}

	private ItineraryDetailsType[] buildItnearyDetailsTypes() {
		ItineraryDetailsType[] itineraryDetails = new ItineraryDetailsType[1];
		itineraryDetails[0] = buildItnearyDetailsType();
		return itineraryDetails;
	}

	private Integer getPaxCount() {
		if (getSearchQuery() != null) {
			return AirUtils.getPaxCount(getSearchQuery(), false);
		} else if (getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
			return AirUtils.getPaxCountFromTravellerInfo(
					getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), false);
		}
		return 0;
	}

	private PaxCountDetailsType[] getPaxCountDetails() {
		List<PaxCountDetailsType> paxDetails = new ArrayList<>();
		if (getSearchQuery() != null) {
			for (PaxType paxType : getSearchQuery().getPaxInfo().keySet()) {
				if (getSearchQuery().getPaxInfo().get(paxType) > 0) {
					PaxCountDetailsType paxInfo = new PaxCountDetailsType();
					paxInfo.setPaxCount(getSearchQuery().getPaxInfo().get(paxType));
					paxInfo.setPaxType(paxType.toString());
					paxDetails.add(paxInfo);
				}
			}
		} else {
			if (getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
				List<FlightTravellerInfo> travellerInfos =
						getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
				Integer count = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).size();
				if (count > 0) {
					paxDetails.add(getPaxCountDetailsType(count, PaxType.ADULT));
				}
				count = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD).size();
				if (count > 0) {
					paxDetails.add(getPaxCountDetailsType(count, PaxType.CHILD));
				}
				count = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT).size();
				if (count > 0) {
					paxDetails.add(getPaxCountDetailsType(count, PaxType.INFANT));
				}
			}
		}
		return paxDetails.toArray(new PaxCountDetailsType[0]);
	}


	private PaxCountDetailsType getPaxCountDetailsType(Integer count, PaxType paxType) {
		PaxCountDetailsType detailsType = new PaxCountDetailsType();
		detailsType.setPaxCount(count);
		detailsType.setPaxType(paxType.name());
		return detailsType;
	}

	private boolean parseReviewResponse(ConfirmPriceRS confirmPriceRS) {
		boolean isSuccess = false;

		ItinPriceType[] itinPriceTypes = confirmPriceRS.getItinPrice();
		// PriceInfo oldPriceInfo = getSegmentInfos().get(0).getPriceInfoList().get(0);
		// SegmentInfo segmentInfo = segmentInfos.get(0);
		// segmentInfo.getMiscInfo().setTraceId(oldPriceInfo.getMiscInfo().getFareKey());

		for (GuestPriceBreakDownType guestPriceBreakDown : itinPriceTypes[0].getGuestPriceBreakDown()) {
			int priceIndex = 0;
			for (PriceBreakDownType breakDownType : guestPriceBreakDown.getPriceBreakDown()) {
				PriceInfo priceInfo = getPriceInfo(priceIndex, getSegmentInfos());
				SegmentInfo segmentInfo1 = getSegmentInfo(priceIndex, getSegmentInfos());
				segmentInfo1.getPriceInfo(0).getMiscInfo()
						.setTraceId(breakDownType.getFareDetailsForGuestType().getFareComponentId());
				FareDetail fareDetail =
						priceInfo.getFareDetail(PaxType.valueOf(guestPriceBreakDown.getGuestType().getValue()));
				Map<FareComponent, Double> fareComponents = new HashMap<>();
				fareComponents.put(FareComponent.BF, breakDownType.getAppliedFareDetailsType().getBaseFare());
				fareComponents.put(FareComponent.TF, breakDownType.getAppliedFareDetailsType().getDisplayFareAmount());
				if (breakDownType.getSurcharge() != null) {
					for (SurchargeType surchargeType : breakDownType.getSurcharge()) {
						fareComponents.put(FareComponent.AT,
								getPreviousAmount(FareComponent.AT, fareComponents) + surchargeType.getAmount());
					}
				}
				if (guestPriceBreakDown.getTax() != null) {
					for (TaxType taxType : guestPriceBreakDown.getTax()) {
						if (segmentInfo1.getPriceInfo(0).getMiscInfo().getTraceId()
								.equalsIgnoreCase(taxType.getFareComponentId())) {
							FareComponent fareComponent =
									Enums.getIfPresent(GDSFareComponentMapper.class, taxType.getCode())
											.or(GDSFareComponentMapper.TX).getFareComponent();
							if (taxType.getCode().equals(FareComponent.CGST.name())
									|| taxType.getCode().equals(FareComponent.SGST.name())) {
								fareComponents.put(FareComponent.AGST,
										getPreviousAmount(FareComponent.AGST, fareComponents) + taxType.getAmount());
							} else {
								fareComponents.put(fareComponent,
										getPreviousAmount(fareComponent, fareComponents) + taxType.getAmount());
							}
						}
					}
				}
				fareDetail.setFareComponents(fareComponents);
				priceIndex++;
			}
			isSuccess = true;
		}
		return isSuccess;
	}

	private Double getPreviousAmount(FareComponent targetComponent, Map<FareComponent, Double> fareComponents) {
		return fareComponents.getOrDefault(targetComponent, 0.0);
	}


	private SegmentInfo getSegmentInfo(int segmentIndex, List<SegmentInfo> segmentInfos) {
		List<SegmentInfo> filteredPriceSegments = segmentInfos.stream().filter(segmentInfo -> {
			PriceInfo tempPriceInfo = segmentInfo.getPriceInfo(0);
			return StringUtils.isNotBlank(tempPriceInfo.getMiscInfo().getFareKey())
					&& isFareDetailPresent(tempPriceInfo);
		}).collect(Collectors.toList());
		return filteredPriceSegments.get(segmentIndex);
	}

	private PriceInfo getPriceInfo(int priceIndex, List<SegmentInfo> segmentInfos) {
		List<SegmentInfo> filteredPriceSegments = segmentInfos.stream().filter(segmentInfo -> {
			PriceInfo tempPriceInfo = segmentInfo.getPriceInfo(0);
			return StringUtils.isNotBlank(tempPriceInfo.getMiscInfo().getFareKey())
					&& isFareDetailPresent(tempPriceInfo);
		}).collect(Collectors.toList());
		return filteredPriceSegments.get(priceIndex).getPriceInfo(0);
	}

	private boolean isFareDetailPresent(PriceInfo tempPriceInfo) {
		AtomicBoolean isPresent = new AtomicBoolean();
		tempPriceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
			if (!paxType.equals(PaxType.INFANT) && !isPresent.get()) {
				if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
					Double airlinefare = fareDetail.getAirlineFare();
					if (Objects.nonNull(airlinefare) && !airlinefare.equals(Double.valueOf(0))) {
						isPresent.set(true);
					}
				}
			}
		}));
		return isPresent.get();
	}

	protected void doAncillaries(TripInfo selectedTrip) {
		AncillaryStub ancillaryStub = bindingService.getAncillaryStub();
		ListBaggageServicesRQ baggageServicesRQ = null;
		ListBaggageServicesRS baggageServicesRS = null;
		try {
			listener.setType(AirUtils.getLogType("Ancillary", getConfiguration()));
			ancillaryStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			baggageServicesRQ = buildBagaggeServiceRq(selectedTrip);
			baggageServicesRS = ancillaryStub.listBaggageServices(baggageServicesRQ);
			if (!isErrorResponse(baggageServicesRS.getErrorType())) {
				parseBaggageResponse(selectedTrip, baggageServicesRS);
				copyAncillaryToConnecting(selectedTrip);
			}
		} catch (RemoteException e) {
			log.error("Unable to connect to baggage service for {} cause ", getBookingId(), e);
		} finally {
			ancillaryStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private void parseBaggageResponse(TripInfo selectedTrip, ListBaggageServicesRS baggageServicesRS) {
		try {
			SaleableAncillaries[] saleableAncillaries = baggageServicesRS.getSaleableAncillaries();
			for (SaleableAncillaries saleableAncillaries1 : saleableAncillaries) {
				for (ItineraryDetailsType itinerary : saleableAncillaries1.getItinerary()) {
					for (FlightSegmentDetailsType flightSegment : itinerary.getFlightSegmentDetails()) {
						String segmentId = flightSegment.getSegmentId();
						String segmentGroupId = flightSegment.getFlightSegmentGroupID();
						List<BaggageSSRInformation> ssrInformations = new ArrayList<>();
						SegmentInfo segmentInfo = getSegmentInfo(selectedTrip.getSegmentInfos(), segmentGroupId);
						if (segmentInfo != null) {
							AncillaryService[] ancillaryServices =
									saleableAncillaries1.getSaleableSsrs().getAncillaryService();
							for (AncillaryService ancillary : ancillaryServices) {
								if (!ancillary.getIsFreeBaggageAllowance()
										&& getSegmentId(ancillary).contains(segmentId)
										&& ancillary.getPassengerList().getPassenger()[0].getPaxType()
												.equals(PaxType.ADULT.name())) {
									BaggageSSRInformation ssrInformation = new BaggageSSRInformation();
									ssrInformation.setDesc(ancillary.getServiceName().split(" ")[0]);
									ssrInformation.setCode(ancillary.getServiceName());
									BigDecimal amount = new BigDecimal(ancillary.getFeeInformation().getFeeAmount());
									ssrInformation.setAmount(getAmountBasedOnCurrency(amount, null));
									ssrInformations.add(ssrInformation);
								}
							}
							if (segmentInfo.getSsrInfo() == null) {
								segmentInfo.setSsrInfo(new HashMap<>());
							}
							segmentInfo.getSsrInfo().put(SSRType.BAGGAGE, ssrInformations);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse SSR for booking {}", getBookingId(), e);
		}
	}

	private List<String> getSegmentId(AncillaryService ancillary) {
		List<String> segmentIds = new ArrayList<>();
		if (ancillary.getSegmentIds() != null && ArrayUtils.isNotEmpty(ancillary.getSegmentIds().getSegmentId())) {
			for (long segmentId : ancillary.getSegmentIds().getSegmentId()) {
				segmentIds.add(String.valueOf(segmentId));
			}
		}
		return segmentIds;
	}

	private SegmentInfo getSegmentInfo(List<SegmentInfo> segmentInfos, String segmentGroup) {
		Optional<SegmentInfo> segmentInfoOptional = segmentInfos.stream()
				.filter(segmentInfo -> segmentInfo.getPriceInfo(0).getMiscInfo().getTokenId().equals(segmentGroup))
				.findFirst();
		if (segmentInfoOptional.isPresent()) {
			return segmentInfoOptional.get();
		}
		return null;
	}

	private SegmentInfo getSegmentInfoOnID(TripInfo selectedTrip, long segmentId) {
		SegmentInfo segmentInfo = null;
		int segmentIndex = (int) (segmentId);
		if (segmentIndex < selectedTrip.getSegmentInfos().size()) {
			segmentInfo = selectedTrip.getSegmentInfos().get(segmentIndex);
		}
		return segmentInfo;
	}

	private void copyAncillaryToConnecting(TripInfo selectedTrip) {
		List<TripInfo> tripInfos = AirUtils.splitTripInfo(selectedTrip, false);
		tripInfos.forEach(tripInfo -> {
			SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
			tripInfo.getSegmentInfos().forEach(segmentInfo1 -> {
				List<BaggageSSRInformation> baggageSSRInfo = GsonUtils.getGson().fromJson(
						GsonUtils.getGson().toJson(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE)),
						new TypeToken<List<BaggageSSRInformation>>() {}.getType());
				if (CollectionUtils.isNotEmpty(baggageSSRInfo)) {
					setAmountToNull(baggageSSRInfo);
					if (!segmentInfo1.getSegmentNum().equals(0)) {
						if (segmentInfo1.getSsrInfo() == null) {
							segmentInfo1.setSsrInfo(new HashMap<>());
						}
						segmentInfo1.getSsrInfo().put(SSRType.BAGGAGE, baggageSSRInfo);
					}
				}
			});
		});
	}

	private void setAmountToNull(List<? extends SSRInformation> baggageSSRInfo) {
		baggageSSRInfo.forEach(ssrInfo -> {
			ssrInfo.setAmount(null);
		});
	}


	private ListBaggageServicesRQ buildBagaggeServiceRq(TripInfo selectedTrip) {
		ListBaggageServicesRQ baggageServicesRQ = new ListBaggageServicesRQ();
		baggageServicesRQ.setAgencyCode(getAgencyCode());
		baggageServicesRQ.setAirlineCode(iFlyAirline.getAirlineCode());
		baggageServicesRQ.setBookingChannel(getBookingChannel());
		baggageServicesRQ.setPointOfSale(getPointOfSale());
		baggageServicesRQ.setPassengerList(buildPassengerList());
		baggageServicesRQ.setItinerary(buildItnearyDetailsTypes(selectedTrip));
		baggageServicesRQ.setFareInfo(buildFareInfoTypes());
		baggageServicesRQ.setSaleDate(TgsDateUtils.getCalendarWithoutTimeZone(selectedTrip.getDepartureTime()).getTime());
		return baggageServicesRQ;
	}

	private ItineraryDetailsType[] buildItnearyDetailsTypes(TripInfo selectedTrip) {
		ItineraryDetailsType[] itineraryTypes = new ItineraryDetailsType[1];
		ItineraryDetailsType detailsType = new ItineraryDetailsType();
		setSegmentInfos(selectedTrip.getSegmentInfos());
		detailsType.setFlightSegmentDetails(buildFlightDetails());
		itineraryTypes[0] = detailsType;
		return itineraryTypes;
	}


	private PassengerList buildPassengerList() {
		PassengerList passengerList = new PassengerList();
		getSearchQuery().getPaxInfo().forEach((paxType, count) -> {
			if (count > 0) {
				Passenger passenger = new Passenger();
				passenger.setPaxType(getPaxDetailsType(paxType).getValue());
				passenger.setQuantity(count);
				passengerList.addPassenger(passenger);
			}
		});
		return passengerList;
	}


}

