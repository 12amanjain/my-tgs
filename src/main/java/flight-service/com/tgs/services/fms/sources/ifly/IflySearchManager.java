package com.tgs.services.fms.sources.ifly;

import com.ibsplc.www.ires.simpletypes.AirAvailabilityRS;
import com.ibsplc.www.ires.simpletypes.AirAvailabilityRQ;
import com.ibsplc.www.ires.simpletypes.OriginDestinationInfo;
import com.ibsplc.www.ires.simpletypes.SegmentAvailabilityType;
import com.ibsplc.www.ires.simpletypes.PaxCountType;
import com.ibsplc.www.ires.simpletypes.TripType;
import com.ibsplc.www.ires.simpletypes.TripInfoType;
import com.ibsplc.www.ires.simpletypes.SegmentInfoType;
import com.ibsplc.www.ires.simpletypes.NearBySearchType;
import com.ibsplc.www.ires.simpletypes.PricingInfoType;
import com.ibsplc.www.ires.simpletypes.PaxPricingInfoType;
import com.ibsplc.www.ires.simpletypes.PaxBaseFareType;
import com.ibsplc.www.ires.simpletypes.PricingComponentInfoType;
import com.ibsplc.www.wsdl.AvailabilityStub;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.axiom.om.OMElement;
import org.apache.axis2.AxisFault;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.wsdl.WSDLConstants;
import org.apache.commons.lang3.ArrayUtils;
import java.rmi.RemoteException;
import java.util.*;

@SuperBuilder
@Slf4j
final class IflySearchManager extends IflyServiceManager {

	private static final String AUTH_FAILED = "Authentication failed";

	protected AirSearchResult airSearchResult;

	protected String clientId;

	public AirSearchResult doSearch() throws NoSearchResultException {
		AvailabilityStub availabilityStub = bindingService.getAvailablityStub();
		listener.setType(AirUtils.getLogType("Availability", getConfiguration()));
		AirAvailabilityRS availabilityRS = null;
		try {
			AirAvailabilityRQ availabilityRQ = buildAvailabilityRq();
			availabilityStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			availabilityRS = availabilityStub.getAirAvailability(availabilityRQ);
			if (!isErrorResponse(availabilityRS.getErrorType())) {
				airSearchResult = new AirSearchResult();
				setClientSession(availabilityStub);
				parseSearchResponse(availabilityRS);
			} else {
				throw new NoSearchResultException(getErrorMessage());
			}
		} catch (AxisFault af) {
			if (AUTH_FAILED.equalsIgnoreCase(af.getMessage())) {
				throw new SupplierSessionException(af.getMessage());
			} else {
				throw new NoSearchResultException(af);
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			availabilityStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return airSearchResult;
	}

	private void parseSearchResponse(AirAvailabilityRS airAvailabilityRS) throws NoSearchResultException {
		if (ArrayUtils.isNotEmpty(airAvailabilityRS.getOriginDestinationInfo())) {
			for (OriginDestinationInfo originDestinationInfo : airAvailabilityRS.getOriginDestinationInfo()) {
				TripInfoType[] tripInfos = originDestinationInfo.getTripInfo();
				PricingInfoType[] pricingInfos = originDestinationInfo.getPricingInfo();
				String pricingUnitId = String.valueOf(originDestinationInfo.getPricingUnitID());
				for (int tripIndex = 0; tripIndex < tripInfos.length; tripIndex++) {
					populateTripInfo(originDestinationInfo.getTripDirection(), tripInfos[tripIndex], pricingUnitId,
							pricingInfos);
				}
			}
		} else {
			throw new NoSearchResultException("No Journey Specified");
		}
	}

	private void populateTripInfo(String tripDirection, TripInfoType tripInfoType, String pricingUnitId,
			PricingInfoType[] pricingInfos) {
		TripInfo tripInfo = new TripInfo();
		try {
			List<SegmentInfo> segments = new ArrayList<>();
			int segNum = 0;
			Map<String, Integer> availableBookingClass =
					IflyUtils.getAvailableBookingClass(tripInfoType, getSearchQuery());
			if (isValidTrip(availableBookingClass, tripInfoType)) {
				for (SegmentInfoType segmentInfoType : tripInfoType.getSegmentInfo()) {
					boolean isReturnSegment = TripType.RT.getValue().equals(tripDirection);
					SegmentInfo segmentInfo = populateSegmentInfo(isReturnSegment, segmentInfoType, segNum++);
					populatePriceInfo(availableBookingClass, segmentInfo, tripInfoType, pricingInfos);
					populateAdditionalPriceInfoAttributes(segmentInfo, segmentInfoType, pricingUnitId, tripInfoType);
					segments.add(segmentInfo);
				}
				tripInfo.getSegmentInfos().addAll(segments);
				tripInfo.setConnectionTime();
				airSearchResult.addTripInfo(getTripType(tripDirection), tripInfo);
			}
		} catch (Exception e) {
			log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, getSearchQuery().getSearchId(), e);
		}
	}

	private void populateAdditionalPriceInfoAttributes(SegmentInfo segmentInfo, SegmentInfoType segmentInfoType,
			String pricingUnitId, TripInfoType tripInfoType) {
		for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
			priceInfo.getMiscInfo().setLogicalFlightId(segmentInfoType.getSegmentIndex());
			priceInfo.getMiscInfo().setJourneyKey(pricingUnitId);
			priceInfo.getMiscInfo().setTokenId(Long.valueOf(tripInfoType.getTripIndex()).toString());
		}

	}

	private void populatePriceInfo(Map<String, Integer> availableBookingClass, SegmentInfo segmentInfo,
			TripInfoType tripInfoType, PricingInfoType[] pricingInfos) {

		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		for (PricingInfoType pricingInfoType : pricingInfos) {
			if (pricingInfoType.getTripRefIndex() == tripInfoType.getTripIndex()) {
				String bookingClass = pricingInfoType.getSegmentReferenceInfo()[0].getBookingClass();
				PriceInfo priceInfo = PriceInfo.builder().build();
				priceInfo.setSupplierBasicInfo(getConfiguration().getBasicInfo());
				priceInfo.setFareIdentifier(IflyUtils.getFareType(pricingInfoType.getPricingComponentInfo()));
				priceInfo.getMiscInfo().setFareKey(
						String.valueOf(pricingInfoType.getPricingComponentInfo()[0].getPricingComponentIndex()));
				priceInfo.getMiscInfo().setSessionId(clientId);
				Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
				int seatRem = availableBookingClass.get(bookingClass);
				for (PaxPricingInfoType paxPricingInfoType : pricingInfoType.getPaxPricingInfo()) {
					PaxType paxType = PaxType.valueOf(paxPricingInfoType.getPaxType());
					FareDetail fareDetail = createFareDetail(paxPricingInfoType, pricingInfoType, seatRem);
					PricingComponentInfoType componentInfoType = pricingInfoType.getPricingComponentInfo()[0];
					PaxBaseFareType paxBaseFareType = getPaxWiseFare(componentInfoType, paxType);
					fareDetails.put(paxType, fareDetail);
					if (segmentInfo.getSegmentNum() == 0) {
						addFareComponents(paxPricingInfoType, fareDetail, paxBaseFareType.getAmount());
					}
				}
				Integer fareTypeId = Integer.parseInt(pricingInfoType.getPricingComponentInfo()[0].getFareId());
				priceInfo.getMiscInfo().setFareTypeId(fareTypeId);
				priceInfo.getMiscInfo().setFareLevel(pricingInfoType.getPricingComponentInfo()[0].getFareLevel());
				priceInfoList.add(priceInfo);
			}
		}
	}

	private void addFareComponents(PaxPricingInfoType paxPricingInfoType, FareDetail fareDetail, Double baseFare) {
		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();

		Double totalAirlineTax = getAmountBasedOnCurrency(paxPricingInfoType.getTax().getAmount(),
				paxPricingInfoType.getTax().getCurrencyCode());
		Double surchargeCharge = getAmountBasedOnCurrency(paxPricingInfoType.getSurcharge().getAmount(),
				paxPricingInfoType.getSurcharge().getCurrencyCode());
		if (baseFare.doubleValue() == 0) {
			baseFare = paxPricingInfoType.getDisplayFare().getAmount();
		}
		fareComponents.put(FareComponent.BF, baseFare);
		fareComponents.put(FareComponent.AT, totalAirlineTax);
		fareComponents.put(FareComponent.OC, surchargeCharge);
		fareComponents.put(FareComponent.TF, baseFare + totalAirlineTax + surchargeCharge);
	}

	private PaxBaseFareType getPaxWiseFare(PricingComponentInfoType componentInfoType, PaxType paxType) {
		Optional<PaxBaseFareType> paxFare = Arrays.stream(componentInfoType.getPaxBaseFare())
				.filter(paxBaseFareType -> paxBaseFareType.getPaxType().equals(paxType.name())).findFirst();
		if (paxFare.isPresent()) {
			return paxFare.get();
		}
		return null;
	}

	private FareDetail createFareDetail(PaxPricingInfoType paxPricingInfoType, PricingInfoType pricingInfoType,
			int seatRem) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.setCabinClass(CabinClass.ECONOMY);
		fareDetail.setFareBasis(pricingInfoType.getPricingComponentInfo()[0].getFareBasis());
		fareDetail.setIsMealIncluded(false);
		fareDetail.setFareType(paxPricingInfoType.getFareType());
		fareDetail.setSeatRemaining(seatRem);
		fareDetail.setClassOfBooking(pricingInfoType.getSegmentReferenceInfo()[0].getBookingClass());
		fareDetail.setFareComponents(new HashMap<>());
		fareDetail.setRefundableType(RefundableType.REFUNDABLE.getRefundableType());
		return fareDetail;
	}

	private SegmentInfo populateSegmentInfo(boolean isRs, SegmentInfoType segmentInfoType, int segNum) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setDuration(IflyUtils.getJourneyMinutesFromHoursFormat(segmentInfoType.getJourneyTime()));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(segmentInfoType.getArrivalInfo().getAirportCode()));
		segmentInfo.setArrivalTime(TgsDateUtils.toLocalDateTime(segmentInfoType.getArrivalInfo().getDateTime()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(segmentInfoType.getDepartureInfo().getAirportCode()));
		segmentInfo.setDepartTime(TgsDateUtils.toLocalDateTime(segmentInfoType.getDepartureInfo().getDateTime()));
		segmentInfo.setSegmentNum(segNum);
		segmentInfo.setFlightDesignator(createFlightDesignator(segmentInfoType));
		segmentInfo.setIsReturnSegment(isRs);
		segmentInfo.setStops(segmentInfoType.getStops());
		segmentInfo.calculateDuration();
		segmentInfo.setStopOverAirports(null);
		return segmentInfo;
	}


	private FlightDesignator createFlightDesignator(SegmentInfoType segmentInfoType) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setAirlineInfo(
				AirlineHelper.getAirlineInfo(segmentInfoType.getFlightIdentifierInfo().getCarrierCode()));
		flightDesignator.setEquipType(segmentInfoType.getAircraftInfo().getType());
		flightDesignator.setFlightNumber(String.valueOf(segmentInfoType.getFlightIdentifierInfo().getFlightNumber()));
		return flightDesignator;
	}


	private void setClientSession(AvailabilityStub availabilityStub) throws AxisFault {
		/*
		 * MessageContext messageContext = availabilityStub._getServiceClient().getLastOperationContext()
		 * .getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE); if (messageContext != null &&
		 * messageContext.getEnvelope() != null && messageContext.getEnvelope().getHeader() != null) { OMElement
		 * sessionElement = messageContext.getEnvelope().getHeader(); try { clientId = sessionElement.getText(); } catch
		 * (Exception e) { throw new SupplierSessionException(e.getMessage()); } }
		 */
		clientId = bindingService.clientToken;
	}

	private AirAvailabilityRQ buildAvailabilityRq() {
		AirAvailabilityRQ availabilityRQ = new AirAvailabilityRQ();
		availabilityRQ.setAirlineCode(iFlyAirline.getAirlineCode());
		availabilityRQ.setIsDisplayAllFare(iFlyAirline.isAllClassPriceRequired());
		availabilityRQ.setTripType(getTripType());
		availabilityRQ.setBookingChannel(getBookingChannel());
		availabilityRQ.setTravelAgencyCode(getConfiguration().getSupplierCredential().getOrganisationCode());
		NearBySearchType[] searchTypes = new NearBySearchType[getSearchQuery().getRouteInfos().size()];
		int routeIndex = 0;
		for (RouteInfo routeInfo : getSearchQuery().getRouteInfos()) {
			NearBySearchType nearBySearchType = new NearBySearchType();
			nearBySearchType.setCabinClass(getSearchQuery().getCabinClass().getName());
			nearBySearchType.setOrigin(routeInfo.getFromCityAirportCode());
			nearBySearchType.setDestination(routeInfo.getToCityAirportCode());
			nearBySearchType.setCanSearchNearbyDestination(true);
			nearBySearchType.setTravelDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()).getTime());
			searchTypes[routeIndex++] = nearBySearchType;
		}
		availabilityRQ.setAvailabilitySearches(searchTypes);
		availabilityRQ.setPointOfPurchase(getPointOfSale());
		PaxCountType[] paxCountTypes = new PaxCountType[getPaxTypeCount()];
		int paxIndex = 0;
		for (PaxType paxType : getSearchQuery().getPaxInfo().keySet()) {
			if (getSearchQuery().getPaxInfo().get(paxType) > 0) {
				PaxCountType paxCountType = new PaxCountType();
				paxCountType.setPaxCount(getSearchQuery().getPaxInfo().get(paxType));
				paxCountType.setPaxType(paxType.name());
				paxCountTypes[paxIndex++] = paxCountType;
			}
		}
		availabilityRQ.setPaxCountDetails(paxCountTypes);
		return availabilityRQ;
	}

	private boolean isValidTrip(Map<String, Integer> availableBookingClass, TripInfoType tripInfoType) {
		boolean isValid = true;
		for (SegmentInfoType segmentInfo : tripInfoType.getSegmentInfo()) {
			for (SegmentAvailabilityType segmentAvailablity : segmentInfo.getSegmentAvailability()) {
				if (!availableBookingClass.containsKey(segmentAvailablity.getBookingClass())) {
					return false;
				}
			}
		}
		return isValid;
	}
}

