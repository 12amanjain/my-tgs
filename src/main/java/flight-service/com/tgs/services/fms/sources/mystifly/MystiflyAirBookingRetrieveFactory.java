package com.tgs.services.fms.sources.mystifly;

import com.tgs.services.base.SoapRequestResponseListner;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.OnePointStub;

@Slf4j
@Service
public class MystiflyAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	protected OnePointStub onePointStub;

	protected String fareSourceCode;

	protected String sessionId;

	protected MystiflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public MystiflyAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	void initialize() {
		onePointStub = bindingService.getOnePointStub();
		listener = new SoapRequestResponseListner(pnr, null, supplierConf.getBasicInfo().getSupplierName());
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
		initialize();
		try {
			pnrBooking = AirImportPnrBooking.builder().build();
			// booking Id Missing
			MystiflySessionManager sessionManager = MystiflySessionManager.builder().configuration(supplierConf)
					.listener(listener).onePointStub(onePointStub).bookingUser(bookingUser).build();
			sessionId = sessionManager.createSessionId();
			MystiflyBookingRetreiveManager retreiveManager =
					MystiflyBookingRetreiveManager.builder().configuration(supplierConf).onePointStub(onePointStub)
							.listener(listener).sessionId(sessionId).bookingUser(bookingUser).build();
			retreiveManager.setSupplierPNR(pnr);
			// Here Pnr will be SupplierPnr
			pnrBooking = retreiveManager.retrieveBooking();
		} catch (Exception e) {
			log.error("Unable to error booking for pnr {}", pnr, e);
		}
		return pnrBooking;
	}

}
