package com.tgs.services.fms.sources.mystifly;

import com.tgs.services.base.SoapRequestResponseListner;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.OnePoint;
import onepoint.mystifly.OnePointStub;

@Slf4j
@Service
public class MystiflyAirCancellationFactory extends AbstractAirCancellationFactory {

	protected OnePointStub onePointStub;

	protected String fareSourceCode;

	protected String sessionId;

	protected MystiflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;


	public MystiflyAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	void initialize() {
		onePointStub = bindingService.getOnePointStub();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isSuccess = false;
		bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
		initialize();
		MystiflySessionManager sessionManager =
				MystiflySessionManager.builder().configuration(supplierConf).onePointStub(onePointStub).build();
		sessionId = sessionManager.createSessionId();
		MystiflyCancellationManager cancellationManager =
				MystiflyCancellationManager.builder().onePointStub(onePointStub).configuration(supplierConf)
						.listener(listener).criticalMessageLogger(criticalMessageLogger).sessionId(sessionId)
						.bookingId(bookingId).bookingUser(bookingUser).build();
		cancellationManager.setSupplierPNR(
				bookingSegments.getSegmentInfos().get(0).getTravellerInfo().get(0).getSupplierBookingId());
		isSuccess = cancellationManager.cancelPnr();
		return isSuccess;
	}


}
