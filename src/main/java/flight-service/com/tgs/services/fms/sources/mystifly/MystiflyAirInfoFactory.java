package com.tgs.services.fms.sources.mystifly;

import java.time.LocalDateTime;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.utils.AirSupplierUtils;
import onepoint.mystifly.OnePointStub;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.OnePoint;


@Slf4j
@Service
public class MystiflyAirInfoFactory extends AbstractAirInfoFactory {

	protected String sessionId;

	protected OnePointStub onePointStub;

	protected MystiflyBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public MystiflyAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize() {
		onePointStub = bindingService.getOnePointStub();
	}

	@Override
	protected void searchAvailableSchedules() {
		bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
		MystiflySessionManager sessionManager = null;
		int numTry = 0;
		boolean doRetry = false;
		do {
			try {
				doRetry = false;
				initialize();
				listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
						supplierConf.getBasicInfo().getSupplierName());
				SessionMetaInfo sessionMetaInfo = fetchSession();
				AirSupplierUtils.isStoreLogRequired(searchQuery);
				sessionManager = MystiflySessionManager.builder().configuration(supplierConf).onePointStub(onePointStub)
						.listener(listener).criticalMessageLogger(criticalMessageLogger).searchQuery(searchQuery)
						.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
				if (sessionMetaInfo == null || sessionMetaInfo.getValue() == null) {
					sessionId = sessionManager.createSessionId();
				} else {
					sessionId = sessionMetaInfo.getValue().toString();
				}
				if (StringUtils.isNotBlank(sessionId)) {
					MystiflySearchManager searchManager = MystiflySearchManager.builder().sessionId(sessionId)
							.criticalMessageLogger(criticalMessageLogger).searchQuery(searchQuery)
							.onePointStub(onePointStub).listener(listener).configuration(supplierConf).bookingUser(user)
							.build();
					searchResult = searchManager.doSearch();
				}
			} catch (NoSearchResultException | SupplierSessionException | SupplierRemoteException e) {
				if (sessionManager.isRetry(e)) {
					sessionId = null;
					doRetry = true;
				} else {
					doRetry = false;
					throw e;
				}
			} finally {
				if (StringUtils.isNotBlank(sessionId) && !doRetry) {
					storeSessionInQueue(sessionId, sessionManager);
				}
				sessionManager = null;
			}
		} while (++numTry < 2 && doRetry);
	}

	private void storeSessionInQueue(String sessionId, MystiflySessionManager sessionManager) {
		if (StringUtils.isNotBlank(sessionId)) {
			try {
				SessionMetaInfo metaInfo = SessionMetaInfo.builder()
						.key(supplierConf.getBasicInfo().getRuleId().toString()).compress(false).build();
				metaInfo.setValue(sessionId).setExpiryTime(LocalDateTime.now().plusMinutes(20));
				cachingComm.storeInQueue(metaInfo);
			} catch (Exception e) {
				log.error("Unable to store session in queue ", e);
				// sessionManager.closeSession(sessionId);
			}
		}
	}

	public SessionMetaInfo fetchSession() {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
				.index(0).compress(false).build();
		metaInfo = cachingComm.fetchFromQueue(metaInfo);
		if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
			metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString()).index(-1)
					.compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
		}
		return metaInfo;
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		try {
			bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			MystiflySessionManager sessionManager = MystiflySessionManager.builder().configuration(supplierConf)
					.criticalMessageLogger(criticalMessageLogger).onePointStub(onePointStub).searchQuery(searchQuery)
					.listener(listener).bookingUser(user).build();
			sessionId = sessionManager.createSessionId();
			MystiflyReviewManager reviewManager = MystiflyReviewManager.builder().configuration(supplierConf)
					.onePointStub(onePointStub).sessionId(sessionId).bookingId(bookingId).listener(listener)
					.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			selectedTrip = reviewManager.reviewTripInfo(selectedTrip);
		} catch (NoSeatAvailableException e) {
			selectedTrip = null;
			throw e;
		} finally {
			if (selectedTrip != null) {
				storeSession(bookingId, sessionId, selectedTrip);
			}
		}
		return selectedTrip;
	}

	private void storeSession(String bookingId, String sessionId, TripInfo selectedTrip) {
		SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(sessionId).build();
		storeBookingSession(bookingId, sessionInfo, null, selectedTrip);
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		bindingService = MystiflyBindingService.builder().configuration(supplierConf).build();
		TripFareRule tripFareRule = TripFareRule.builder().build();
		initialize();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		try {
			MystiflySessionManager sessionManager = MystiflySessionManager.builder().configuration(supplierConf)
					.criticalMessageLogger(criticalMessageLogger).onePointStub(onePointStub).searchQuery(searchQuery)
					.bookingId(bookingId).listener(listener).bookingUser(user).build();
			sessionId = sessionManager.createSessionId();
			if (StringUtils.isNotBlank(sessionId)) {
				MystiflyFareRuleManager fareRuleManager = MystiflyFareRuleManager.builder().onePointStub(onePointStub)
						.criticalMessageLogger(criticalMessageLogger).configuration(supplierConf).sessionId(sessionId)
						.bookingId(bookingId).listener(listener).bookingUser(user).build();
				tripFareRule = fareRuleManager.getFareRule(selectedTrip, bookingId, isDetailed);
			}
		} catch (Exception e) {
			log.error("Unable to fetch fare rule {}", bookingId, e);
		}
		return tripFareRule;
	}

	@Override
	public Boolean initializeStubs() {
		log.info("Initialize Mystifly Stubs");
		initialize();
		return true;
	}

}
