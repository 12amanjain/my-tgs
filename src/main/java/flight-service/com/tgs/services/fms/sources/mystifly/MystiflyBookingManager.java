package com.tgs.services.fms.sources.mystifly;

import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirBookRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirBookRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirOrderTicketRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirOrderTicketRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirTraveler;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirTripDetailsRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfAirTraveler;
import org.datacontract.schemas._2004._07.mystifly_onepoint.CustomerInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ETicket;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerName;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Passport;
import org.datacontract.schemas._2004._07.mystifly_onepoint.TravelerInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.BookFlight;
import onepoint.mystifly.BookFlightResponse;
import onepoint.mystifly.TicketOrder;
import onepoint.mystifly.TicketOrderResponse;

@Slf4j
@SuperBuilder
@Getter
@Setter
final class MystiflyBookingManager extends MystiflyServiceManager {

	protected DeliveryInfo deliveryInfo;
	private Order order;
	private List<SegmentInfo> segmentInfos;

	private LocalDateTime ticketTimeLimit;

	public String createPNR(MystiflyBookingRetreiveManager retreiveManager) {
		try {
			listener.setType(AirUtils.getLogType("BookFlight", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			BookFlight bookFlight = new BookFlight();
			AirBookRQ airBookRQ = createAirBookRequest();
			bookFlight.setRq(airBookRQ);
			BookFlightResponse bookFlightresponse = onePointStub.bookFlight(bookFlight);
			AirBookRS airBookRS = bookFlightresponse.getBookFlightResult();
			// removeInterceptorChains(client);
			if (airBookRS != null
					&& (airBookRS.getErrors() != null && !isAnyCriticalException(airBookRS.getErrors()))) {
				/**
				 * Book RS status can be confirmed or Pending (Not Ticketed ) with MF number, we Need to hit TripDetails
				 * for fetching airline PNR and Ticket Number
				 */
				if (!airBookRS.getUniqueID().isEmpty()) {
					/**
					 * There can be 2 cases status : {CONFIRMED, PENDING} In case of Pending We need to call TripDetails
					 * regularly.
					 */
					String supplierRefernce = airBookRS.getUniqueID();
					if (isHoldBooking) {
						if (Objects.nonNull(airBookRS.getTktTimeLimit())
								&& Objects.nonNull(airBookRS.getTktTimeLimit())) {
							ticketTimeLimit = TgsDateUtils.toLocalDateTime(airBookRS.getTktTimeLimit());
						}
					}
					BookingUtils.updateSupplierBookingId(segmentInfos, supplierRefernce);
					log.info("CRS PNR {}, for booking Id {}", supplierRefernce, bookingId);
					return getPnrFromTripDetails(retreiveManager, supplierRefernce);
				}
			}
		} catch (RemoteException e) {
			logCriticalMessage(e.getMessage());
			throw new SupplierRemoteException(e.getMessage());
		}

		finally {
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			// removeInterceptorChains(client);
		}
		return null;
	}

	private String getPnrFromTripDetails(MystiflyBookingRetreiveManager retreiveManager, String supplierRefernce) {
		retreiveManager.setSupplierPNR(supplierRefernce);
		AirTripDetailsRS tripDetailsRS = retreiveManager.getTripDetail();
		String airlinePnr =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getReservationItems().getReservationItem()[0]
						.getAirlinePNR();
		log.info("CRS PNR {} for bookingID {} airlinePnr {}", supplierRefernce, bookingId, airlinePnr);
		return airlinePnr;
	}

	private AirBookRQ createAirBookRequest() {
		AirBookRQ bookRequest = new AirBookRQ();
		bookRequest.setFareSourceCode(fareSourceCode);
		bookRequest.setLccHoldBooking(isHoldBooking);
		bookRequest.setTarget(getTarget());
		bookRequest.setSessionId(sessionId);
		bookRequest.setTravelerInfo(getTravellerInfos());
		return bookRequest;
	}

	private TravelerInfo getTravellerInfos() {
		TravelerInfo travelerInfo = new TravelerInfo();

		String countryCode, postalCode, areaCode;
		ClientGeneralInfo clientInfoCriteria = ServiceCommunicatorHelper.getClientInfo();
		postalCode = clientInfoCriteria.getPostalCode();
		countryCode = clientInfoCriteria.getNationality();
		areaCode = String.valueOf(clientInfoCriteria.getAreaCode());

		travelerInfo.setAreaCode(areaCode);
		travelerInfo.setCountryCode(countryCode);
		travelerInfo.setPostCode(postalCode);

		travelerInfo.setEmail(AirSupplierUtils.getEmailId(deliveryInfo));
		travelerInfo.setPhoneNumber(AirSupplierUtils.getContactNumber(deliveryInfo));

		travelerInfo.setAirTravelers(getAirTravelers(countryCode));
		return travelerInfo;
	}

	private ArrayOfAirTraveler getAirTravelers(String countryCode) {
		ArrayOfAirTraveler arrayOfAirTraveler = new ArrayOfAirTraveler();
		List<FlightTravellerInfo> travellerInfos = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		AirTraveler[] airTravelerList = new AirTraveler[travellerInfos.size()];
		int travIndex = 0;
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			AirTraveler airTraveler = new AirTraveler();
			airTraveler.setGender(getGenderFromTitle(travellerInfo.getTitle()));
			airTraveler.setDateOfBirth(TgsDateUtils.getCalendar(getDOB(travellerInfo)));
			airTraveler.setPassengerNationality(countryCode);
			airTraveler.setPassengerType(getPassengerCode(travellerInfo.getPaxType()));
			airTraveler.setPassengerName(getPassengerName(travellerInfo));
			if (travellerInfo.getPassportNumber() != null) {
				airTraveler.setPassport(getPassportInformation(travellerInfo));
			}
			if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
				String airlineCode = segmentInfos.get(0).getPlatingCarrier(null);
				String freqFlier = travellerInfo.getFrequentFlierMap().get(airlineCode);
				if (StringUtils.isNotBlank(freqFlier)) {
					airTraveler.setFrequentFlyerNumber(freqFlier);
				}
			}
			airTravelerList[travIndex++] = airTraveler;
		}
		arrayOfAirTraveler.setAirTraveler(airTravelerList);
		return arrayOfAirTraveler;
	}

	private LocalDate getDOB(FlightTravellerInfo travellerInfo) {
		LocalDate dobDate = null;
		if (travellerInfo.getDob() != null) {
			dobDate = travellerInfo.getDob();
			return dobDate;
		}
		return TgsDateUtils.calenderToLocalDate(TgsDateUtils.getDefaultDOB());
	}

	private Passport getPassportInformation(FlightTravellerInfo flightTravellerInfo) {

		Passport passport = new Passport();
		passport.setPassportNumber(flightTravellerInfo.getPassportNumber());
		passport.setExpiryDate(TgsDateUtils.getCalendar(flightTravellerInfo.getExpiryDate()));
		passport.setCountry(flightTravellerInfo.getPassportNationality());
		return passport;
	}

	private PassengerName getPassengerName(FlightTravellerInfo flightTravellerInfo) {
		PassengerName paxName = new PassengerName();
		paxName.setPassengerFirstName(flightTravellerInfo.getFirstName());
		paxName.setPassengerLastName(flightTravellerInfo.getLastName());
		paxName.setPassengerTitle(getPassengerTitle(flightTravellerInfo.getTitle()));
		return paxName;
	}

	/**
	 * In case of LCC this method update the airline Pnr, and in case of GDS it will update airline Pnr as well as
	 * Ticket Number
	 *
	 * @param supplierReference
	 */

	public void fetchTicketNumbersOrPnr(MystiflyBookingRetreiveManager retreiveManager, String supplierReference) {
		orderTicket(supplierReference);
		retreiveManager.setSupplierPNR(supplierReference);
		AirTripDetailsRS tripDetailsRS = retreiveManager.getTripDetail();
		if (tripDetailsRS != null) {
			isAnyCriticalException(tripDetailsRS.getErrors());
			if (tripDetailsRS.getTravelItinerary().getTicketStatus().equalsIgnoreCase("TktInProcess")) {
				log.info("Ticket Pending from Supplier for MF Number {} , bookingId {}", supplierReference, bookingId);
				criticalMessageLogger.add("Ticket in Progress .");
			}
		}

		/**
		 * We need to update Airline PNR , because in case of Hold We set Dummy PNR in our system
		 */
		String airlinePnr =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getReservationItems().getReservationItem()[0]
						.getAirlinePNR();
		BookingUtils.updateAirlinePnr(segmentInfos, airlinePnr);


		CustomerInfo[] customerInfos =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getCustomerInfos().getCustomerInfo();


		ETicket[] eTicketList =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getCustomerInfos().getCustomerInfo()[0]
						.getETickets().getETicket();

		/**
		 * Only set ticket in case of GDS , if ticket number and pnr are same then its LCC otherwise it's GDS
		 */
		if (ArrayUtils.isNotEmpty(eTicketList) && !eTicketList[0].getETicketNumber().equals(airlinePnr)) {
			segmentInfos.forEach(segmentInfo -> {
				for (int i = 0; i < segmentInfo.getBookingRelatedInfo().getTravellerInfo().size(); i++) {

					BookingUtils.updateTicketNumber(segmentInfos,
							customerInfos[i].getETickets().getETicket()[0].getETicketNumber(),
							segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(i));
				}
			});
		}
	}

	public void orderTicket(String supplierReference) {
		try {
			// bindInterceptors("OrderTicket", bookingId);
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			log.info("Ordering Ticekt for MF number {} , booking Id {}", supplierReference, bookingId);
			AirOrderTicketRQ airOrderTicketRQ = new AirOrderTicketRQ();
			airOrderTicketRQ.setSessionId(sessionId);
			airOrderTicketRQ.setTarget(getTarget());
			airOrderTicketRQ.setUniqueID(supplierReference);
			TicketOrder ticketOrder = new TicketOrder();
			ticketOrder.setRq(airOrderTicketRQ);
			TicketOrderResponse ticketOrderResponse;

			ticketOrderResponse = onePointStub.ticketOrder(ticketOrder);
			AirOrderTicketRS airOrderTicketRS = ticketOrderResponse.getTicketOrderResult();
			if (airOrderTicketRS != null) {
				isAnyCriticalException(airOrderTicketRS.getErrors());
			}
		} catch (RemoteException e) {
			logCriticalMessage(e.getMessage());
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			// removeInterceptorChains(client);
		}
	}

}
