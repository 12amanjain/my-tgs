package com.tgs.services.fms.sources.mystifly;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.tgs.services.base.utils.TgsDateUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirTripDetailsRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirTripDetailsRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Customer;
import org.datacontract.schemas._2004._07.mystifly_onepoint.CustomerInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ReservationItem;
import org.datacontract.schemas._2004._07.mystifly_onepoint.TravelItinerary;
import org.datacontract.schemas._2004._07.mystifly_onepoint.TripDetailsPTC_FareBreakdown;
import org.datacontract.schemas._2004._07.mystifly_onepoint.TripDetailsPassengerFare;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.TripDetails;
import onepoint.mystifly.TripDetailsResponse;

@Setter
@Slf4j
@SuperBuilder
final class MystiflyBookingRetreiveManager extends MystiflyServiceManager {

	protected String supplierPNR;

	public AirTripDetailsRS getTripDetail() {
		try {
			listener.setType(AirUtils.getLogType("TripDetails", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			TripDetails tripDetails = new TripDetails();
			AirTripDetailsRQ tripDetailsRQ = createTripDetailsRQ(supplierPNR);
			tripDetails.setRq(tripDetailsRQ);
			TripDetailsResponse tripResponse;

			tripResponse = onePointStub.tripDetails(tripDetails);

			AirTripDetailsRS tripDetailsRS = tripResponse.getTripDetailsResult();
			if (!isAnyCriticalException(tripDetailsRS.getErrors())) {
				return tripDetailsRS;
			}
		} catch (RemoteException e) {
			logCriticalMessage(e.getMessage());
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	public AirImportPnrBooking retrieveBooking() {
		AirImportPnrBooking pnrBooking = null;
		AirTripDetailsRS tripDetailsRS = getTripDetail();
		List<TripInfo> tripInfos = fetchTripDetails(tripDetailsRS);
		DeliveryInfo deliveryInfo = fetchDeliveryInfo(tripDetailsRS);
		pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).build();
		return pnrBooking;
	}

	private DeliveryInfo fetchDeliveryInfo(AirTripDetailsRS tripDetailsRS) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		List<String> emails = new ArrayList<>();
		CustomerInfo[] customerInfos =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getCustomerInfos().getCustomerInfo();
		if (ArrayUtils.isNotEmpty(customerInfos)) {
			emails.add(customerInfos[0].getCustomer().getEmailAddress());
		}
		deliveryInfo.setEmails(emails);
		return deliveryInfo;
	}

	private AirItemStatus fetchItemStatus(TravelItinerary travelItinerary) {
		// Booked, TicketInProgress
		/**
		 * If No Pnr , then it is {ON_HOLD} if Pnr, and no ticket {Confirm_Pending}, If Pnr and wbefare then its LCC
		 * Booking & status:{Success}
		 *
		 */
		AirItemStatus itemStatus = AirItemStatus.IN_PROGRESS;
		// for LCC
		String pnr = travelItinerary.getItineraryInfo().getReservationItems().getReservationItem()[0].getAirlinePNR();
		if (StringUtils.isEmpty(pnr)) {
			itemStatus = AirItemStatus.ON_HOLD;
		} else {
			if (travelItinerary.getFareType().equalsIgnoreCase("WebFare")) {
				itemStatus = AirItemStatus.SUCCESS;
			} else {
				itemStatus = AirItemStatus.CONFIRM_PENDING;
			}
		}
		return itemStatus;
	}

	private List<TripInfo> fetchTripDetails(AirTripDetailsRS tripDetailsRS) {

		ReservationItem[] resItems =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getReservationItems().getReservationItem();
		List<TripInfo> tripInfos = getTripDetils(tripDetailsRS);

		String airlinePnr = resItems[0].getAirlinePNR();
		List<FlightTravellerInfo> flightTravellerInfos = getTripTravaller(airlinePnr,
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getCustomerInfos().getCustomerInfo());
		shiftFareDetailsToFlightTraveller(tripInfos, flightTravellerInfos);
		return tripInfos;
	}

	private void shiftFareDetailsToFlightTraveller(List<TripInfo> tripInfos,
			List<FlightTravellerInfo> flightTravellerInfos) {

		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				SegmentBookingRelatedInfo bookingRelatedInfo = SegmentBookingRelatedInfo.builder().build();
				segmentInfo.setBookingRelatedInfo(bookingRelatedInfo);
				segmentInfo.getBookingRelatedInfo().setTravellerInfo(new ArrayList<>());
				segmentInfo.getPriceInfo(0).getFareDetails().forEach((paxType, fareDetail) -> {
					List<FlightTravellerInfo> travellerInfos1 =
							GsonUtils.getGson()
									.fromJson(
											GsonUtils.getGson()
													.toJson(AirUtils.getParticularPaxTravellerInfo(flightTravellerInfos,
															paxType)),
											new TypeToken<List<FlightTravellerInfo>>() {}.getType());
					travellerInfos1.forEach(travellerInfo -> {
						travellerInfo.setFareDetail(fareDetail);
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().add(travellerInfo);
					});
				});
				// Finally Make Price Info Fare Detail to empty
				segmentInfo.getPriceInfo(0).setFareDetails(null);
			});
		});

	}

	private List<FlightTravellerInfo> getTripTravaller(String airlinePnr, CustomerInfo[] customerInfos) {
		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(customerInfos)) {
			for (CustomerInfo customerInfo : customerInfos) {
				Customer customer = customerInfo.getCustomer();
				FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
				if (StringUtils.isNotBlank(airlinePnr)) {
					travellerInfo.setPnr(airlinePnr);
				}
				if (Objects.nonNull(customer.getDateOfBirth())) {
					travellerInfo.setDob(TgsDateUtils.calenderToLocalDate(customer.getDateOfBirth()));
				}
				travellerInfo.setFirstName(customer.getPaxName().getPassengerFirstName());
				travellerInfo.setLastName(customer.getPaxName().getPassengerLastName());
				travellerInfo.setTitle(customer.getPaxName().getPassengerTitle());
				travellerInfo.setPaxType(MystiflyUtils.getPaxType(customer.getPassengerType()));
				travellerInfo.setPassportNationality(customer.getPassportNationality());
				travellerInfo.setSupplierBookingId(supplierPNR);
				travellerInfo.setPassportNumber(customer.getPassportNumber());
				if (Objects.nonNull(customer.getPassportExpiresOn())) {
					travellerInfo.setExpiryDate(TgsDateUtils.calenderToLocalDate(customer.getPassportExpiresOn()));

				}
				travellerInfos.add(travellerInfo);
			}
		}
		return travellerInfos;
	}

	private List<TripInfo> getTripDetils(AirTripDetailsRS tripDetailsRS) {

		List<TripInfo> tripInfos = new ArrayList<>();
		ReservationItem[] resItems =
				tripDetailsRS.getTravelItinerary().getItineraryInfo().getReservationItems().getReservationItem();

		TripDetailsPTC_FareBreakdown[] ptcFareBreakDown = tripDetailsRS.getTravelItinerary().getItineraryInfo()
				.getTripDetailsPTC_FareBreakdowns().getTripDetailsPTC_FareBreakdown();
		TripInfo tripInfo = new TripInfo();
		int index = 0;
		if (ArrayUtils.isNotEmpty(resItems)) {
			for (ReservationItem resItem : resItems) {
				if (!resItem.isIsReturnSpecified()) {
					index++;
				}
				SegmentInfo segmentInfo = getSegmentInfo(resItem, ptcFareBreakDown, index);
				populatePriceInfo(segmentInfo, ptcFareBreakDown, resItem);
				tripInfo.getSegmentInfos().add(segmentInfo);
			}

		}
		tripInfos.add(tripInfo);

		return tripInfos;
	}

	private void populatePriceInfo(SegmentInfo segmentInfo, TripDetailsPTC_FareBreakdown[] ptcFareBreakDownList,
			ReservationItem resItem) {
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();

		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		for (TripDetailsPTC_FareBreakdown ptcFareBreakdown : ptcFareBreakDownList) {

			FareDetail fareDetail = createFareDetail(resItem);
			fareDetails.put(MystiflyUtils.getPaxType(ptcFareBreakdown.getPassengerTypeQuantity().getCode()),
					fareDetail);
			if (segmentInfo.getSegmentNum() == 0 && !segmentInfo.getIsReturnSegment()) {
				addFareComponents(ptcFareBreakdown, fareDetail);
			}

		}
		priceInfoList.add(priceInfo);

	}

	private void addFareComponents(TripDetailsPTC_FareBreakdown ptcFareBreakdown, FareDetail fareDetail) {
		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
		TripDetailsPassengerFare passengerFare = ptcFareBreakdown.getTripDetailsPassengerFare();
		fareComponents.put(FareComponent.BF, Double.parseDouble(passengerFare.getEquiFare().getAmount()));
		fareComponents.put(FareComponent.TF, Double.parseDouble(passengerFare.getTotalFare().getAmount()));
		fareComponents.put(FareComponent.TAF, Double.parseDouble(passengerFare.getTax().getAmount()));
	}

	private FareDetail createFareDetail(ReservationItem resItem) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.getBaggageInfo().setAllowance(resItem.getBaggage());
		return fareDetail;
	}

	private SegmentInfo getSegmentInfo(ReservationItem resItem, TripDetailsPTC_FareBreakdown[] ptcFareBreakDown,
			int index) {
		SegmentInfo segmentInfo = new SegmentInfo();

		if (resItem.isIsReturnSpecified()) {
			segmentInfo.setSegmentNum(resItem.getItemRPH() - 1 - index);
		} else {
			segmentInfo.setSegmentNum(resItem.getItemRPH() - 1);
		}
		segmentInfo.setIsReturnSegment(resItem.isIsReturnSpecified());
		segmentInfo.setDuration(Long.valueOf(resItem.getJourneyDuration()));
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(resItem.getDepartureAirportLocationCode()));
		segmentInfo.setDepartTime(TgsDateUtils.toLocalDateTime(resItem.getDepartureDateTime()));
		segmentInfo.setStops(resItem.getStopQuantity());
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(resItem.getArrivalAirportLocationCode()));
		segmentInfo.setArrivalTime(TgsDateUtils.toLocalDateTime(resItem.getArrivalDateTime()));
		segmentInfo.setFlightDesignator(createFlightDesignator(resItem));

		AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(resItem.getOperatingAirlineCode());
		if (operatingAirline != null
				&& !operatingAirline.getCode().equalsIgnoreCase(segmentInfo.getFlightDesignator().getAirlineCode())) {
			segmentInfo.setOperatedByAirlineInfo(operatingAirline);
		}
		return segmentInfo;
	}

	private FlightDesignator createFlightDesignator(ReservationItem resItem) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(resItem.getMarketingAirlineCode()));
		flightDesignator.setEquipType(resItem.getAirEquipmentType());
		flightDesignator.setFlightNumber(resItem.getFlightNumber());
		return flightDesignator;
	}

	private AirTripDetailsRQ createTripDetailsRQ(String uniqueId) {
		AirTripDetailsRQ request = new AirTripDetailsRQ();
		request.setSessionId(sessionId);
		request.setTarget(getTarget());
		request.setUniqueID(uniqueId);
		return request;
	}

}
