package com.tgs.services.fms.sources.mystifly;

import java.rmi.RemoteException;
import java.util.List;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint_airrules1_1.AirRulesRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint_airrules1_1.AirRulesRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint_airrules1_1.FareRule;
import org.datacontract.schemas._2004._07.mystifly_onepoint_airrules1_1.RuleDetail;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.FareRules1_1;
import onepoint.mystifly.FareRules1_1Response;

@Slf4j
@SuperBuilder
final class MystiflyFareRuleManager extends MystiflyServiceManager {

	public TripFareRule getFareRule(TripInfo tripInfo, String bookingId, boolean isDetailedApiCall) {
		try {
			listener.setType(AirUtils.getLogType("FareRule", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			TripFareRule fareRule = TripFareRule.builder().build();
			AirRulesRQ airRulesRQ = getAirRuleRequest(tripInfo);
			FareRules1_1 fareRule11 = new FareRules1_1();
			fareRule11.setRq(airRulesRQ);
			FareRules1_1Response fareRule11Response;
			fareRule11Response = onePointStub.fareRules1_1(fareRule11);
			AirRulesRS airRulesRS = fareRule11Response.getFareRules1_1Result();
			if (airRulesRS.isSuccessSpecified()) {
				return parseFarerules(airRulesRS, tripInfo, fareRule);
			}
		} catch (RemoteException e) {
			logCriticalMessage(e.getMessage());
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	private TripFareRule parseFarerules(AirRulesRS rulesRS, TripInfo tripInfo, TripFareRule fareRule) {
		if (rulesRS != null && rulesRS.getFareRules() != null && rulesRS.getFareRules() != null
				&& ArrayUtils.isNotEmpty(rulesRS.getFareRules().getFareRule())) {
			FareRule[] fareRuleList = rulesRS.getFareRules().getFareRule();
			FareRuleInformation ruleInformation = new FareRuleInformation();
			String fareRuleInfo = StringUtils.EMPTY;
			for (FareRule rule : fareRuleList) {
				if (rule.getRuleDetails() != null && rule.getRuleDetails() != null
						&& ArrayUtils.isNotEmpty(rule.getRuleDetails().getRuleDetail())) {
					RuleDetail[] ruleDetails = rule.getRuleDetails().getRuleDetail();
					for (RuleDetail ruleDetail : ruleDetails) {
						if (ruleDetail.getCategory() != null && ruleDetail.getRules() != null
								&& StringUtils.isNotEmpty(ruleDetail.getCategory())
								&& StringUtils.isNotEmpty(ruleDetail.getRules())) {
							if ("PENALTIES".equalsIgnoreCase(ruleDetail.getCategory())
									|| "GENERAL".equalsIgnoreCase(ruleDetail.getCategory())) {
								fareRuleInfo = org.apache.commons.lang3.StringUtils.join(fareRuleInfo, " ",
										ruleDetail.getRules());
							}
						}
					}
				}
			}

			ruleInformation.getMiscInfo().put("Conditions", fareRuleInfo);
			String tripKey = FareRuleManager.getRuleKey(tripInfo);
			fareRule.getFareRule().put(tripKey, ruleInformation);
		}
		return fareRule;
	}

	private AirRulesRQ getAirRuleRequest(TripInfo tripInfo) {
		AirRulesRQ rulesRQ = new AirRulesRQ();
		rulesRQ.setSessionId(sessionId);
		rulesRQ.setFareSourceCode(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey());
		rulesRQ.setTarget(getTarget());
		return rulesRQ;
	}

}
