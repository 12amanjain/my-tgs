package com.tgs.services.fms.sources.mystifly;

import java.rmi.RemoteException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.lang.BooleanUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirItineraryPricingInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirRevalidateRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirRevalidateRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfFlightSegment;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfOriginDestinationOption;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfPricedItinerary;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfTax;
import org.datacontract.schemas._2004._07.mystifly_onepoint.FlightSegment;
import org.datacontract.schemas._2004._07.mystifly_onepoint.OriginDestinationOption;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PTC_FareBreakdown;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerFare;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PricedItinerary;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Tax;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.AirRevalidate;
import onepoint.mystifly.AirRevalidateResponse;

@Slf4j
@SuperBuilder
final class MystiflyReviewManager extends MystiflyServiceManager {

	public TripInfo reviewTripInfo(TripInfo selectedTrip) throws NoSeatAvailableException {
		TripInfo selectedTripInfo = selectedTrip;
		try {
			listener.setType(AirUtils.getLogType("AirRevalidate", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			fareSourceCode = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey();
			AirRevalidate airRevalidate = new AirRevalidate();
			AirRevalidateRQ airRevalidateRQ = createAirRevalidateRQ();
			airRevalidate.setRq(airRevalidateRQ);
			AirRevalidateResponse airRevalidateRS;
			airRevalidateRS = onePointStub.airRevalidate(airRevalidate);
			if ((airRevalidateRS.getAirRevalidateResult().getErrors() != null
					&& isAnyCriticalException(airRevalidateRS.getAirRevalidateResult().getErrors()))
					|| !parseReviewResponse(airRevalidateRS.getAirRevalidateResult(), selectedTripInfo)) {
				throw new NoSeatAvailableException();
			}
		} catch (RemoteException e) {
			logCriticalMessage(e.getMessage());
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return selectedTripInfo;

	}

	private boolean parseReviewResponse(AirRevalidateRS airRevalidateRS, TripInfo selectedTrip) {
		boolean isSuccess = false;
		ArrayOfPricedItinerary arrayOfPricedItinerary = airRevalidateRS.getPricedItineraries();
		if (arrayOfPricedItinerary != null) {
			for (PricedItinerary pricedItinerary : arrayOfPricedItinerary.getPricedItinerary()) {
				// update new Fare Source Code
				String journeyKey = pricedItinerary.getAirItineraryPricingInfo().getFareSourceCode();
				ArrayOfOriginDestinationOption arrayOfOriginDestinationOption =
						pricedItinerary.getOriginDestinationOptions();
				boolean isFirstSeg = true;
				int segIndex = 0;
				for (OriginDestinationOption originDest : arrayOfOriginDestinationOption.getOriginDestinationOption()) {

					ArrayOfFlightSegment arrayOfFlightSegment = originDest.getFlightSegments();
					// only need to populate the first segment PriceInfo
					for (FlightSegment flightSegment : arrayOfFlightSegment.getFlightSegment()) {
						SegmentInfo segmentInfo = selectedTrip.getSegmentInfos().get(segIndex);
						AirlineInfo airlineInfo = segmentInfo.getFlightDesignator().getAirlineInfo();
						airlineInfo.setIsTkRequired(BooleanUtils.isTrue(flightSegment.isEticketSpecified()));
						updatePriceInfo(flightSegment, pricedItinerary, selectedTrip, isFirstSeg, segIndex++,
								journeyKey);
						PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
						priceInfo.getMiscInfo().setIsEticket(BooleanUtils.isTrue(flightSegment.isEticketSpecified()));
						segmentInfo.setPriceInfoList(Arrays.asList(priceInfo));
						isFirstSeg = false;
					}
				}
				isSuccess = true;
			}
		} else {
			throw new NoSeatAvailableException("PricedItinerary is Empty");
		}
		return isSuccess;
	}

	private void updatePriceInfo(FlightSegment flightSegment, PricedItinerary pricedItinerary, TripInfo selectedTrip,
			boolean isFirstSeg, int index, String journeyKey) {
		AirItineraryPricingInfo airItineraryPricingInfo = pricedItinerary.getAirItineraryPricingInfo();

		PriceInfo oldPriceInfo = selectedTrip.getSegmentInfos().get(index).getPriceInfo(0);
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(oldPriceInfo.getSupplierBasicInfo());
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		priceInfo.setFareIdentifier(oldPriceInfo.getFareIdentifier());

		/**
		 * We are setting the old FSC that we received in Search Response , because we have to re-validate again while
		 * going for booking, so will have to pass the old FSC.....
		 *
		 */
		priceInfo.getMiscInfo().setJourneyKey(fareSourceCode);
		for (PTC_FareBreakdown fareBreakDown : airItineraryPricingInfo.getPTC_FareBreakdowns().getPTC_FareBreakdown()) {
			FareDetail fareDetail = new FareDetail();
			// condition for first segment
			if (isFirstSeg) {
				updateFareDetail(fareBreakDown, fareDetail);
			}
			// Change seat remaining
			fareDetail.setSeatRemaining(flightSegment.getSeatsRemaining().getNumber());
			fareDetail.setBaggageInfo(createBaggaeInfo(fareBreakDown, index));
			// check this also
			fareDetail.setCabinClass(CabinClass.getEnumFromCode(flightSegment.getCabinClassCode()));
			fareDetail.setRefundableType(getRefundableType(airItineraryPricingInfo.getIsRefundable()));
			fareDetail.setFareType(airItineraryPricingInfo.getFareType().getValue());
			fareDetail.setFareBasis(getFareBasis(fareBreakDown, index));
			fareDetail.setClassOfBooking(flightSegment.getResBookDesigCode());
			fareDetail.setIsMealIncluded(getIsMealIncluded(flightSegment.getMealCode()));
			fareDetails.put(MystiflyUtils.getPaxType(fareBreakDown.getPassengerTypeQuantity().getCode()), fareDetail);
		}
		selectedTrip.getSegmentInfos().get(index).setPriceInfoList(Arrays.asList(priceInfo));
	}

	private void updateFareDetail(PTC_FareBreakdown fareBreakDown, FareDetail fareDetail) {
		Map<FareComponent, Double> fareComponents = new HashMap<>();
		PassengerFare passengerFare = fareBreakDown.getPassengerFare();
		fareComponents.put(FareComponent.TF, Double.parseDouble(passengerFare.getTotalFare().getAmount()));
		fareComponents.put(FareComponent.BF, Double.parseDouble(passengerFare.getEquivFare().getAmount()));
		ArrayOfTax arrayOfTax = passengerFare.getTaxes();
		for (Tax tax : arrayOfTax.getTax()) {
			FareComponent fareComponent = MystiflyUtils.getFareComponent(tax);
			fareComponents.put(fareComponent,
					fareComponents.getOrDefault(fareComponent, 0.0) + Double.parseDouble(tax.getAmount()));
		}
		fareDetail.setFareComponents(fareComponents);
	}

	private AirRevalidateRQ createAirRevalidateRQ() {
		AirRevalidateRQ revalidateRQ = new AirRevalidateRQ();
		revalidateRQ.setTarget(getTarget());
		revalidateRQ.setSessionId(sessionId);
		revalidateRQ.setFareSourceCode(fareSourceCode);
		return revalidateRQ;
	}

	public double priceQuote(List<SegmentInfo> segmentInfos) {
		try {
			// bindInterceptors("Revalidate", bookingId);
			listener.setType("Revalidate");
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			AirRevalidate airRevalidate = new AirRevalidate();
			airRevalidate.setRq(createAirRevalidateRQ());
			AirRevalidateResponse airRevalidateResponse = onePointStub.airRevalidate(airRevalidate);
			AirRevalidateRS airRevalidateRS = airRevalidateResponse.getAirRevalidateResult();
			PricedItinerary pricedItinerary = airRevalidateRS.getPricedItineraries().getPricedItinerary()[0];
			segmentInfos.get(0).getPriceInfo(0).getMiscInfo()
					.setJourneyKey(pricedItinerary.getAirItineraryPricingInfo().getFareSourceCode());
			String totalAmount =
					pricedItinerary.getAirItineraryPricingInfo().getItinTotalFare().getTotalFare().getAmount();
			return Double.valueOf(totalAmount);
		} catch (Exception e) {
			log.error("Unable to review booking for bookingId {}", bookingId, e);
		} finally {
			// removeInterceptorChains(client);
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return 0;
	}

}
