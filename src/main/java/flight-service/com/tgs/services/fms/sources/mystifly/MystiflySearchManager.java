package com.tgs.services.fms.sources.mystifly;

import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import onepoint.mystifly.AirLowFareSearch;
import onepoint.mystifly.AirLowFareSearchResponse;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirItineraryPricingInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirLowFareSearchRQ;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirLowFareSearchRS;
import org.datacontract.schemas._2004._07.mystifly_onepoint.AirTripType;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfFlightSegment;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfOriginDestinationInformation;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfOriginDestinationOption;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfPTC_FareBreakdown;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfPassengerTypeQuantity;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfPricedItinerary;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfStopQuantityInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.ArrayOfTax;
import org.datacontract.schemas._2004._07.mystifly_onepoint.CabinClassPreference;
import org.datacontract.schemas._2004._07.mystifly_onepoint.CabinPreferLevel;
import org.datacontract.schemas._2004._07.mystifly_onepoint.FlightSegment;
import org.datacontract.schemas._2004._07.mystifly_onepoint.MaxStopsQuantity;
import org.datacontract.schemas._2004._07.mystifly_onepoint.OperatingAirline;
import org.datacontract.schemas._2004._07.mystifly_onepoint.OriginDestinationInformation;
import org.datacontract.schemas._2004._07.mystifly_onepoint.OriginDestinationOption;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PTC_FareBreakdown;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerFare;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PassengerTypeQuantity;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Preferences;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PricedItinerary;
import org.datacontract.schemas._2004._07.mystifly_onepoint.PricingSourceType;
import org.datacontract.schemas._2004._07.mystifly_onepoint.RequestOptions;
import org.datacontract.schemas._2004._07.mystifly_onepoint.StopQuantityInfo;
import org.datacontract.schemas._2004._07.mystifly_onepoint.Tax;
import org.datacontract.schemas._2004._07.mystifly_onepoint.TravelPreferences;

@SuperBuilder
@Slf4j
final class MystiflySearchManager extends MystiflyServiceManager {

	AirSearchResult airSearchResult;


	public AirSearchResult doSearch() {
		try {
			listener.setType(AirUtils.getLogType("LowFareSearch", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			AirLowFareSearch airLowFareSaerch = new AirLowFareSearch();
			airLowFareSaerch.setRq(buildAirLowFareSearchRQ());
			AirLowFareSearchResponse airLowFareRS = onePointStub.airLowFareSearch(airLowFareSaerch);
			airSearchResult = new AirSearchResult();
			parseSearchResponse(airLowFareRS.getAirLowFareSearchResult());
			return airSearchResult;
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			onePointStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

	}

	private AirLowFareSearchRQ buildAirLowFareSearchRQ() {
		AirLowFareSearchRQ lowAirfareRQ = new AirLowFareSearchRQ();
		lowAirfareRQ.setTarget(getTarget());
		// Pricing source can be Public Private or Default
		lowAirfareRQ.setPricingSourceType(PricingSourceType.All);
		lowAirfareRQ.setRequestOptions(getRequestOptions());
		lowAirfareRQ.setSessionId(sessionId);
		lowAirfareRQ.setPassengerTypeQuantities(getPassengerInformation(searchQuery));
		lowAirfareRQ.setTravelPreferences(getTravelPreferences());
		lowAirfareRQ.setOriginDestinationInformations(getOriginDestinationInfo(searchQuery));
		return lowAirfareRQ;
	}

	/*
	 * private RequestOptions getRequestOptions() {
	 * 
	 * return null; }
	 */


	private void parseSearchResponse(AirLowFareSearchRS airLowFareRS) throws NoSearchResultException {
		if (isAnyCriticalException(airLowFareRS.getErrors())
				|| ArrayUtils.isEmpty(airLowFareRS.getPricedItineraries().getPricedItinerary())) {
			throw new NoSearchResultException(String.join(",", getCriticalMessageLogger()));
		} else {
			ArrayOfPricedItinerary arrayofPricedItenary = airLowFareRS.getPricedItineraries();
			for (PricedItinerary pricedItenary : arrayofPricedItenary.getPricedItinerary()) {
				populateTripInfo(pricedItenary);
			}
		}
	}

	private void populateTripInfo(PricedItinerary pricedItenary) {
		TripInfo tripInfo = new TripInfo();
		try {
			ArrayOfOriginDestinationOption arrayOfOriginDestinationOption = pricedItenary.getOriginDestinationOptions();
			int routeSeq = 0;
			int baggageIndex = 0;
			for (OriginDestinationOption originDestinationOption : arrayOfOriginDestinationOption
					.getOriginDestinationOption()) {
				List<SegmentInfo> segments = new ArrayList<>();
				ArrayOfFlightSegment arrayOfFlightSegment = originDestinationOption.getFlightSegments();
				int segNum = 0;
				for (FlightSegment flightSegment : arrayOfFlightSegment.getFlightSegment()) {
					SegmentInfo segmentInfo = populateSegmentInfo(flightSegment, segNum++, routeSeq);
					populatePriceInfo(segmentInfo, pricedItenary, flightSegment, routeSeq++, baggageIndex++);
					segmentInfo.getPriceInfo(0).getMiscInfo()
							.setJourneyKey(pricedItenary.getAirItineraryPricingInfo().getFareSourceCode());
					segmentInfo.getPriceInfo(0).getMiscInfo().setMarriageGrp(flightSegment.getMarriageGroup());
					segments.add(segmentInfo);
				}
				tripInfo.getSegmentInfos().addAll(segments);
			}
		} catch (Exception e) {
			log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
		}
		if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			airSearchResult.addTripInfo(getTripType(pricedItenary), tripInfo);
		}
	}

	private void populatePriceInfo(SegmentInfo segmentInfo, PricedItinerary pricedItenary, FlightSegment flightSegment,
			int routeSeq, int baggageIndex) {
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		if (pricedItenary.getValidatingAirlineCode() != null
				&& StringUtils.isNotBlank(pricedItenary.getValidatingAirlineCode())) {
			priceInfo.getMiscInfo().setPlatingCarrier(getPlatingCarrier(pricedItenary.getValidatingAirlineCode()));
		}
		priceInfo.setFareIdentifier(FareType.PUBLISHED);
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();

		ArrayOfPTC_FareBreakdown arrayOfPTCFareBreakdown =
				pricedItenary.getAirItineraryPricingInfo().getPTC_FareBreakdowns();
		for (PTC_FareBreakdown ptcFareBreakdown : arrayOfPTCFareBreakdown.getPTC_FareBreakdown()) {
			FareDetail fareDetail = createFareDetail(pricedItenary.getAirItineraryPricingInfo(), flightSegment,
					ptcFareBreakdown, routeSeq, baggageIndex);
			fareDetails.put(MystiflyUtils.getPaxType(ptcFareBreakdown.getPassengerTypeQuantity().getCode()),
					fareDetail);
			if (routeSeq == 0)
				addFareComponents(pricedItenary.getAirItineraryPricingInfo(), fareDetail, ptcFareBreakdown);
		}
		priceInfoList.add(priceInfo);
	}

	private void addFareComponents(AirItineraryPricingInfo value, FareDetail fareDetail,
			PTC_FareBreakdown ptcFareBreakdown) {
		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
		PassengerFare passengerFare = ptcFareBreakdown.getPassengerFare();
		fareComponents.put(FareComponent.BF, Double.parseDouble(passengerFare.getEquivFare().getAmount()));
		fareComponents.put(FareComponent.TF, Double.parseDouble(passengerFare.getTotalFare().getAmount()));

		// this can contain as many components such as GST,K3,CGST,YQ etc..
		ArrayOfTax arrayOfTax = passengerFare.getTaxes();
		if (arrayOfTax.getTax() != null) {
			for (Tax tax : arrayOfTax.getTax()) {
				FareComponent fareComponent = MystiflyUtils.getFareComponent(tax);
				fareComponents.put(fareComponent, fareDetail.getFareComponents().getOrDefault(fareComponent, 0.0)
						+ Double.parseDouble(tax.getAmount()));
			}
		}

	}

	private FareDetail createFareDetail(AirItineraryPricingInfo airItineraryPricingInfo, FlightSegment flightSegment,
			PTC_FareBreakdown fareBreakDown, int segNum, int index) {
		FareDetail fareDetail = new FareDetail();

		fareDetail.setSeatRemaining(flightSegment.getSeatsRemaining().getNumber());

		String equipmentType = flightSegment.getOperatingAirline().getEquipment();
		/**
		 * Check In baggage and fare basis ,may not be present for , if the equipment type is bus or train
		 */
		if (equipmentType != null && !equipmentType.equalsIgnoreCase("bus")
				&& !equipmentType.equalsIgnoreCase("train")) {
			fareDetail.setBaggageInfo(createBaggaeInfo(fareBreakDown, index));
			fareDetail.setFareBasis(getFareBasis(fareBreakDown, index));
		} else {
			index--;
		}

		// check this also
		fareDetail.setCabinClass(CabinClass.getEnumFromCode(flightSegment.getCabinClassCode()));
		fareDetail.setRefundableType(getRefundableType(airItineraryPricingInfo.getIsRefundable()));
		fareDetail.setFareType(airItineraryPricingInfo.getFareType().getValue());
		fareDetail.setClassOfBooking(flightSegment.getResBookDesigCode());
		// meal Code
		fareDetail.setIsMealIncluded(getIsMealIncluded(flightSegment.getMealCode()));

		return fareDetail;
	}

	private SegmentInfo populateSegmentInfo(FlightSegment flightSegment, int segNum, int routeSeq) {

		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setDuration(flightSegment.getJourneyDuration());
		segmentInfo.setIsReturnSegment((segNum == routeSeq) ? false : true);
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(flightSegment.getArrivalAirportLocationCode()));
		segmentInfo.setArrivalTime(TgsDateUtils.toLocalDateTime(flightSegment.getArrivalDateTime()));

		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(flightSegment.getDepartureAirportLocationCode()));
		segmentInfo.setDepartTime(TgsDateUtils.toLocalDateTime(flightSegment.getDepartureDateTime()));

		// Only have Marketing AirLine Code ,
		segmentInfo.setFlightDesignator(createFlightDesignator(flightSegment.getMarketingAirlineCode(),
				flightSegment.getOperatingAirline(), flightSegment));

		AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(flightSegment.getOperatingAirline().getCode());
		if (operatingAirline != null
				&& !operatingAirline.getCode().equals(segmentInfo.getFlightDesignator().getAirlineCode())) {
			segmentInfo.setOperatedByAirlineInfo(operatingAirline);
		}
		segmentInfo.setSegmentNum(segNum);
		segmentInfo.setStops(flightSegment.getStopQuantity());
		segmentInfo.setStopOverAirports(getStopOverAirports(flightSegment));
		return segmentInfo;
	}

	private List<AirportInfo> getStopOverAirports(FlightSegment flightSegment) {

		List<AirportInfo> stopOverAirports = new ArrayList<>();
		if (flightSegment.getStopQuantity() != 0) {
			ArrayOfStopQuantityInfo arrayOfStopQuantityInfo = flightSegment.getStopQuantityInformations();
			for (StopQuantityInfo stopQuantityInfo : arrayOfStopQuantityInfo.getStopQuantityInfo()) {
				stopOverAirports.add(AirportHelper.getAirport(stopQuantityInfo.getLocationCode()));
			}
			return stopOverAirports;
		}
		return null;
	}

	private FlightDesignator createFlightDesignator(String marketingAirlineCode, OperatingAirline opAirline,
			FlightSegment flightSegment) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setFlightNumber(flightSegment.getFlightNumber());
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(marketingAirlineCode));
		flightDesignator.setEquipType(opAirline.getEquipment());
		return flightDesignator;
	}

	private AirLowFareSearchRQ createAirLowFareSearchRQ() {
		AirLowFareSearchRQ lowAirfareRQ = new AirLowFareSearchRQ();
		lowAirfareRQ.setTarget(getTarget());
		// Pricing source can be Public Private or Default
		lowAirfareRQ.setPricingSourceType(PricingSourceType.All);
		lowAirfareRQ.setRequestOptions(getRequestOptions());
		lowAirfareRQ.setSessionId(sessionId);
		lowAirfareRQ.setPassengerTypeQuantities(getPassengerInformation(searchQuery));
		lowAirfareRQ.setTravelPreferences(getTravelPreferences());
		lowAirfareRQ.setOriginDestinationInformations(getOriginDestinationInfo(searchQuery));
		return lowAirfareRQ;
	}

	private RequestOptions getRequestOptions() {
		return RequestOptions.Default;
	}

	private TravelPreferences getTravelPreferences() {
		TravelPreferences travelPreferences = new TravelPreferences();
		travelPreferences.setCabinPreference(getCabinType(searchQuery.getCabinClass()));
		travelPreferences.setPreferences(buildPreferences());
		if (searchQuery.isDomesticReturn() || searchQuery.isIntlReturn()) {
			travelPreferences.setAirTripType(AirTripType.Return);
		} else {
			travelPreferences.setAirTripType(AirTripType.OneWay);
		}
		travelPreferences.setMaxStopsQuantity(MaxStopsQuantity.All);
		travelPreferences.setVendorPreferenceCodes(getPreffAirlines(searchQuery));
		return travelPreferences;
	}

	private Preferences buildPreferences() {
		Preferences preferences = new Preferences();
		preferences.setCabinClassPreference(getCabinClassPreference());
		return preferences;
	}

	// Restricted will give Business then only business class, no option lower to higher
	private CabinClassPreference getCabinClassPreference() {
		CabinClassPreference cb = new CabinClassPreference();
		cb.setCabinType(getCabinType(searchQuery.getCabinClass()));
		cb.setPreferenceLevel(CabinPreferLevel.Restricted);
		return cb;
	}

	private ArrayOfstring getPreffAirlines(AirSearchQuery searchQuery) {
		ArrayOfstring airlines = new ArrayOfstring();
		if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline()))
			searchQuery.getPreferredAirline().forEach(airlineInfo -> {
				airlines.addString(airlineInfo.getCode());
			});
		return airlines;
	}

	private ArrayOfPassengerTypeQuantity getPassengerInformation(AirSearchQuery searchQuery) {
		AtomicInteger paxCount = new AtomicInteger();
		searchQuery.getPaxInfo().forEach((paxType, quantity) -> {
			if (quantity > 0) {
				paxCount.getAndIncrement();
			}
		});
		ArrayOfPassengerTypeQuantity arrayOfPassengerType = new ArrayOfPassengerTypeQuantity();
		PassengerTypeQuantity[] passenegrTypeQuantityArray = new PassengerTypeQuantity[paxCount.get()];
		AtomicInteger pasIndex = new AtomicInteger();
		searchQuery.getPaxInfo().forEach((paxType, quantity) -> {
			if (quantity > 0) {
				PassengerTypeQuantity passenegrTypeQuantity = new PassengerTypeQuantity();
				passenegrTypeQuantity.setQuantity(quantity);
				passenegrTypeQuantity.setCode(getPassengerCode(paxType));
				passenegrTypeQuantityArray[pasIndex.get()] = passenegrTypeQuantity;
				pasIndex.getAndIncrement();
			}
		});
		arrayOfPassengerType.setPassengerTypeQuantity(passenegrTypeQuantityArray);
		return arrayOfPassengerType;
	}

	private ArrayOfOriginDestinationInformation getOriginDestinationInfo(AirSearchQuery searchQuery) {
		ArrayOfOriginDestinationInformation array = new ArrayOfOriginDestinationInformation();
		OriginDestinationInformation[] originDestinationInformations =
				new OriginDestinationInformation[searchQuery.getRouteInfos().size()];
		int routeIndex = 0;
		for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
			OriginDestinationInformation originDest = new OriginDestinationInformation();
			originDest.setOriginLocationCode(routeInfo.getFromCityAirportCode());
			originDest.setDestinationLocationCode(routeInfo.getToCityAirportCode());
			originDest.setDepartureDateTime(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
			originDestinationInformations[routeIndex] = originDest;
			routeIndex++;
		}
		array.setOriginDestinationInformation(originDestinationInformations);
		return array;
	}

}
