package com.tgs.services.fms.sources.mystifly;

import java.rmi.RemoteException;
import java.util.Objects;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import onepoint.mystifly.CreateSession;
import onepoint.mystifly.CreateSessionResponse;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.mystifly_onepoint.SessionCreateRQ;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class MystiflySessionManager extends MystiflyServiceManager {

	// Invalid Session
	protected static final String INVALID_SESSION = "invalid session";


	public String createSessionId() throws SupplierSessionException {
		CreateSession sessionRequest = buildSessionRequest();
		try {
			listener.setType(AirUtils.getLogType("CreateSession", configuration));
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			CreateSessionResponse sessionResponse = onePointStub.createSession(sessionRequest);
			if (!isAnyCriticalException(sessionResponse.getCreateSessionResult().getErrors())
					&& BooleanUtils.isTrue(sessionResponse.getCreateSessionResult().getSessionStatus())) {
				return sessionResponse.getCreateSessionResult().getSessionId();
			} else {
				throw new SupplierSessionException(String.join(",", getCriticalMessageLogger()));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			onePointStub._getServiceClient().getAxisService().addMessageContextListener(listener);
		}
	}

	private CreateSession buildSessionRequest() {
		CreateSession sessionRequest = new CreateSession();
		SessionCreateRQ sessionRq = new SessionCreateRQ();
		sessionRq.setUserName(getConfiguration().getSupplierCredential().getUserName());
		sessionRq.setPassword(getConfiguration().getSupplierCredential().getPassword());
		sessionRq.setAccountNumber(getConfiguration().getSupplierCredential().getOrganisationCode());
		sessionRq.setTarget(getTarget());
		sessionRequest.setRq(sessionRq);
		return sessionRequest;
	}


	/*
	 * public void closeSession(String sessionId) {
	 * 
	 * if (Objects.nonNull(searchQuery)) { bindInterceptors("EndSession", searchQuery.getSearchId()); } else {
	 * bindInterceptors("EndSession", bookingId); } onePoint.endSession(sessionId); removeInterceptorChains(client); }
	 */

	public boolean isRetry(Exception e) {
		return e != null && StringUtils.isNotBlank(e.getMessage())
				&& e.getMessage().toLowerCase().contains(INVALID_SESSION);
	}

}
