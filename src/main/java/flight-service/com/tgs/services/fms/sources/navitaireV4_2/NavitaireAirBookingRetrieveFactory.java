package com.tgs.services.fms.sources.navitaireV4_2;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.navitaire.schemas.webservices.BookingManagerStub;
import com.navitaire.schemas.webservices.SessionManagerStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NavitaireAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	protected SoapRequestResponseListner listener = null;

	protected SessionManagerStub sessionStub;

	protected BookingManagerStub bookingStub;

	protected NavitaireBindingService bindingService;

	public NavitaireAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	public void initialize() {
		bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingCommunicator)
				.bookingUser(bookingUser).build();
		sessionStub = bindingService.getSessionManagerStub(supplierConf, null);
		bookingStub = bindingService.getBookingManagerStub(supplierConf, null);
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		NavitaireSessionManager sessionManager = null;
		try {
			initialize();
			pnrBooking = AirImportPnrBooking.builder().build();
			listener = new SoapRequestResponseListner(pnr, "", supplierConf.getBasicInfo().getSupplierId());
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).sessionBinding(sessionStub)
					.listener(listener).bookingId(pnr).bookingUser(bookingUser).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotEmpty(sessionManager.getSessionSignature())) {
				log.debug("Inside Retreieve Manager to fetch Trips {}", pnr);
				NavitaireBookingRetrieveManager retrieveManager = NavitaireBookingRetrieveManager.builder()
						.configuration(supplierConf).sessionSignature(sessionManager.sessionSignature)
						.bookingBinding(bookingStub).listener(listener).moneyExchnageComm(moneyExchangeComm)
						.bookingUser(bookingUser).bookingUser(bookingUser).build();
				retrieveManager.init();
				pnrBooking = retrieveManager.retrieveBooking(pnr);
				updateTimeLimit(retrieveManager.holdTimeLimit);
			}
		} finally {
			sessionManager.closeSession();
			bindingService.storeSessionMetaInfo(supplierConf, SessionManagerStub.class, sessionStub);
			bindingService.storeSessionMetaInfo(supplierConf, BookingManagerStub.class, bookingStub);
		}
		return pnrBooking;
	}

	@Override
	public AirCreditShellResponse fetchBalance(AirCreditShellRequest request) {
		listener =
				new SoapRequestResponseListner(request.getBookingId(), "", supplierConf.getBasicInfo().getSupplierId());
		NavitaireBookingRetrieveManager retrieveManager =
				NavitaireBookingRetrieveManager.builder().configuration(supplierConf).listener(listener)
						.moneyExchnageComm(moneyExchangeComm).bookingUser(bookingUser).build();
		retrieveManager.init();
		retrieveManager.setSourceConfiguration(sourceConfiguration);
		return retrieveManager.fetchCreditShellBalance(request);
	}

}
