package com.tgs.services.fms.sources.navitaireV4_2;

import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.RequestPaymentMethodType;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBookingFromStateResponse;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.oms.datamodel.Order;
import java.time.LocalDateTime;
import java.util.List;
import com.tgs.utils.exception.air.CancellationException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.navitaire.schemas.webservices.BookingManagerStub;
import com.navitaire.schemas.webservices.SessionManagerStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;

@Slf4j
@Service
public class NavitaireAirCancellationFactory extends AbstractAirCancellationFactory {

	protected SoapRequestResponseListner listener = null;

	protected SessionManagerStub sessionStub;

	protected BookingManagerStub bookingStub;

	protected NavitaireBindingService bindingService;

	protected NavitaireSessionManager sessionManager = null;

	protected NavitaireBookingRetrieveManager retrieveManager = null;

	protected NavitaireAirline navitaireAirline;

	private String sessionSignature;

	public NavitaireAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	public void initialize() {
		bindingService =
				NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(bookingUser).build();
		sessionStub = bindingService.getSessionManagerStub(supplierConf, null);
		bookingStub = bindingService.getBookingManagerStub(supplierConf, null);
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
				.sessionBinding(sessionStub).bookingId(bookingId).bookingUser(bookingUser).build();
		retrieveManager =
				NavitaireBookingRetrieveManager.builder().configuration(supplierConf).bookingBinding(bookingStub)
						.listener(listener).bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
						.bookingSegments(bookingSegments).bookingUser(bookingUser).build();
		this.navitaireAirline = NavitaireAirline.getNavitaireAirline(supplierConf.getSourceId());
		sessionManager.init();
		retrieveManager.init();
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isCancelSuccess = false;
		try {
			initialize();
			sessionManager.openSession();
			sessionSignature = sessionManager.getSessionSignature();
			retrieveManager.setSessionSignature(sessionSignature);
			Booking bookingResponse = retrieveManager.getBookingResponse(pnr);
			isCancelSuccess = retrieveManager.isCancelled(bookingResponse, pnr);
			if (!isCancelSuccess && retrieveManager.isValidPNRBooking(bookingResponse, pnr)) {
				NavitaireCancellationManager cancellationManager = NavitaireCancellationManager.builder()
						.configuration(supplierConf).sessionSignature(sessionSignature).bookingBinding(bookingStub)
						.listener(listener).bookingUser(bookingUser).build();
				cancellationManager.init();
				cancellationManager.cancelAll();
				NavitaireBookingManager bookingManager = NavitaireBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionSignature).bookingId(bookingId)
						.sourceConfiguration(sourceConfiguration).listener(listener).bookingBinding(bookingStub)
						.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
				bookingManager.initialize(bookingSegments.getSegmentInfos());
				bookingManager.initializeTravellerInfo(
						bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo());
				double amountToBeCalculated = bookingResponse.getBookingSum().getTotalCost().doubleValue();
				if (!isHoldBooking) {
					// Booking which done payment and do cancel for reverting payment
					// only if payment done
					bookingManager.setTotalFare(-1 * amountToBeCalculated);
					bookingManager.doPayment();
				}
				bookingManager.commitBooking(false, bookingSegments.getSegmentInfos().get(0).getTravellerInfo(), pnr,
						true);
				bookingResponse = retrieveManager.getBookingResponse(pnr);
				isCancelSuccess = cancellationManager.checkCancelStatus(bookingResponse);
			}
		} finally {
			sessionManager.closeSession();
			storeSessionInfo();
		}
		return isCancelSuccess;
	}

	@Override
	public AirCancellationDetail reviewBookingForCancellation(AirCancellationDetail cancellationDetail) {
		boolean isPreCancelled = false;
		try {
			initialize();
			sessionManager.openSession();
			sessionSignature = sessionManager.getSessionSignature();
			retrieveManager.setSessionSignature(sessionSignature);
			Booking bookingResponse = retrieveManager.getBookingResponse(pnr);
			isPreCancelled = retrieveManager.isCancelled(bookingResponse, pnr);
			if (isPreCancelled) {
				throw new CancellationException(String.join(",", retrieveManager.JOURNEY_MISMATCH));
			}
			if (!isPreCancelled && !navitaireAirline.codeShareFlights()
					.contains(bookingSegments.getCancelSegments().get(0).getAirlineCode(true))) {
				NavitaireCancellationManager cancellationManager = NavitaireCancellationManager.builder()
						.configuration(supplierConf).sessionSignature(sessionSignature).bookingBinding(bookingStub)
						.listener(listener).segmentInfos(bookingSegments.getCancelSegments()).bookingId(bookingId)
						.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
				cancellationManager.init();
				cancellationManager.prefillSegmentDetails(bookingResponse, bookingSegments.getCancelSegments());
				cancellationManager.cancelSegments(bookingSegments.getCancelSegments(),
						bookingSegments.getCancelSegments().size() == bookingSegments.getSegmentInfos().size(),
						"ReviewCancel");
				GetBookingFromStateResponse bookingState = cancellationManager.getCancellingBookingState();
				cancellationManager.setAirlineCancellationFee(bookingState, bookingSegments, false);
				cancellationDetail.setAutoCancellationAllowed(Boolean.TRUE);
			}
		} finally {
			storeSessionInfo();
			sessionManager.closeSession();
		}
		return cancellationDetail;
	}

	@Override
	public boolean confirmBookingForCancellation() {
		boolean isCancelled = false;
		String dividedPNR = null;
		boolean isCommitSuccess = false;
		try {
			initialize();
			sessionManager.openSession();
			sessionSignature = sessionManager.getSessionSignature();
			retrieveManager.setSessionSignature(sessionSignature);
			Booking bookingResponse = retrieveManager.getBookingResponse(pnr);
			if (!retrieveManager.isCancelled(bookingResponse, pnr)) {
				NavitaireCancellationManager cancellationManager = NavitaireCancellationManager.builder()
						.configuration(supplierConf).sessionSignature(sessionSignature).bookingBinding(bookingStub)
						.listener(listener).bookingSegments(bookingSegments)
						.segmentInfos(bookingSegments.getCancelSegments()).criticalMessageLogger(criticalMessageLogger)
						.bookingId(bookingId).bookingUser(bookingUser).build();
				NavitaireBookingManager bookingManager = NavitaireBookingManager.builder().configuration(supplierConf)
						.sessionSignature(sessionSignature).bookingId(bookingId)
						.sourceConfiguration(sourceConfiguration).listener(listener).bookingBinding(bookingStub)
						.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
				cancellationManager.init();
				bookingManager.init();
				List<FlightTravellerInfo> cancelledTravellers =
						cancellationManager.getCancelledTravellerInfoList(bookingSegments.getCancelSegments());
				cancellationManager.initializeTravellerInfo(cancelledTravellers);
				cancellationManager.prefillSegmentDetails(bookingResponse, bookingSegments.getCancelSegments());
				if (NavitaireUtils.isDivideByPnr(bookingSegments, cancelledTravellers)) {
					dividedPNR = dividePNR(cancellationManager, bookingResponse, cancelledTravellers);
					if (StringUtils.isBlank(dividedPNR)) {
						throw new SupplierUnHandledFaultException(
								"DividePnr Failed for " + getSupplierConf().getSupplierId() + " " + pnr);
					}
					sessionManager.openSession();
					sessionSignature = sessionManager.getSessionSignature();
					cancellationManager.setSessionSignature(sessionSignature);
					retrieveManager.setSessionSignature(sessionSignature);
					retrieveManager.getBookingResponse(dividedPNR);
					cancellationManager.getCancellingBookingState();
				}
				cancellationManager.cancelSegments(bookingSegments.getCancelSegments(),
						bookingSegments.getCancelSegments().size() == bookingSegments.getSegmentInfos().size(),
						"CancelBooking " + getSupplierConf().getSourceId());
				GetBookingFromStateResponse bookingState = cancellationManager.getCancellingBookingState();
				cancellationManager.setAirlineCancellationFee(bookingState, bookingSegments, true);
				bookingManager.setSessionSignature(sessionSignature);
				String cancelledPNR = StringUtils.isNotBlank(dividedPNR) ? dividedPNR : pnr;
				double airlineCancellationFee = NavitaireUtils.getAirCancellationFees(bookingSegments);
				if (!isFareDiff(airlineCancellationFee)) {
					if (!getSupplierConf().getSourceId().equals(AirSourceType.GOAIR.getSourceId()))
						bookingManager.addPaymentToBooking(RequestPaymentMethodType.AgencyAccount,
								cancellationManager.getBalanceDue().doubleValue());
					cancellationManager.commitBooking(false, cancelledTravellers, cancelledPNR, false);
					isCommitSuccess = true;
					bookingResponse = retrieveManager.getBookingResponse(cancelledPNR);
					if (navitaireAirline.isCancelStatusCheckRq()) {
						isCancelled = retrieveManager.isCancelled(bookingResponse.getBookingInfo().getBookingStatus());
					} else {
						// in case of indigo after commit success we can consider cancellation is also success,
						// due to immediate status change in Indigo not working on GetBooking.
						isCancelled = true;
					}
				}
			}
		} finally {
			String message = null;
			if (!isCommitSuccess && !isCancelled && StringUtils.isNotBlank(dividedPNR)) {
				message = StringUtils.join("Divide PNR ", dividedPNR, " For Cancellation Travellers");
				criticalMessageLogger.add(message);
			} else if (isCommitSuccess && !isCancelled) {
				message = StringUtils
						.join("Commit Success,Final Cancellation Status Mismatch for " + supplierConf.getSupplierId());
				criticalMessageLogger.add(message);
			} else if (!isCancelled) {
				message = StringUtils.join("Cancellation Failed for " + getSupplierConf().getSupplierId());
				criticalMessageLogger.add(message);
			}
			log.error("{} for bookingid {}", message, bookingId);
			storeSessionInfo();
			sessionManager.closeSession();
		}
		return isCancelled;
	}

	@Override
	public boolean isSupplierCancellationAllowed(Integer sourceId) {
		boolean isCancellationAllowed = false;

		if (AirSourceType.INDIGO.getSourceId() == sourceId || AirSourceType.SPICEJET.getSourceId() == sourceId) {
			if (AirUtils.calculateAvailablePNRCredit(bookingSegments.getSegmentInfos()) > 0) {
				criticalMessageLogger.add("Credit Shell Booking Cannot Be Auto Cancelled");
				return isCancellationAllowed;
			}
		}
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(bookingSegments.getSegmentInfos());
		LocalDateTime departureTime = this.bookingSegments.getCancelSegments().get(0).getDepartTime();
		String fareType = this.bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getFareType();
		AirType airType = AirUtils.getAirType(tripInfo);
		if (cancelConfiguration != null) {
			isCancellationAllowed =
					AirCancelUtils.isBasedOnFareTypeAllowed(cancelConfiguration, fareType, criticalMessageLogger)
							&& AirCancelUtils.isDependsOnDepartureTime(cancelConfiguration, airType, departureTime,
									fareType, criticalMessageLogger);
		}
		// addCancellationMessageToNotes(true);
		boolean isPassengerWiseCancellation = false;
		for (int index = 0; index < bookingSegments.getCancelSegments().size(); index++) {
			SegmentInfo cancelledSegment = bookingSegments.getCancelSegments().get(index);
			SegmentInfo originalSegment = bookingSegments.getSegmentInfos().get(index);

			if (cancelledSegment.getTravellerInfo().size() != originalSegment.getTravellerInfo().size()) {
				isPassengerWiseCancellation = true;
				break;
			}
		}

		if (isPassengerWiseCancellation) {
			isCancellationAllowed = NavitaireAirline.getNavitaireAirline(sourceId).isPassengerWiseCancellationAllowed();
			if (!isCancellationAllowed) {
				criticalMessageLogger.add("PassengerWise Cancellation not allowed");
			}
		}
		return isCancellationAllowed;
	}

	private void storeSessionInfo() {
		bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingComm).build();
		bindingService.storeSessionMetaInfo(supplierConf, SessionManagerStub.class, sessionStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingManagerStub.class, bookingStub);
	}

	private String dividePNR(NavitaireCancellationManager cancellationManager, Booking bookingResponse,
			List<FlightTravellerInfo> travellerInfos) {
		String dividedPNR = null;
		try {
			dividedPNR = cancellationManager.divideByPnr(bookingResponse);
			NavitaireUtils.updateDivideByPnr(bookingSegments.getSegmentInfos(), travellerInfos, dividedPNR);
			BookingUtils.updateAirlinePnr(bookingSegments.getCancelSegments(), dividedPNR);
		} finally {
			if (StringUtils.isNotBlank(dividedPNR)) {
				log.info("Divided PNR {} for BookingId {}", dividedPNR, bookingId);
				itemComm.updateOrderAndItem(bookingSegments.getSegmentInfos(), order, null);
			}
			sessionManager.closeSession();
		}
		return dividedPNR;
	}
}
