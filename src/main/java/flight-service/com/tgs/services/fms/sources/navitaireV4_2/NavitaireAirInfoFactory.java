package com.tgs.services.fms.sources.navitaireV4_2;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import com.navitaire.schemas.webservices.BookingManagerStub;
import com.navitaire.schemas.webservices.ContentManagerStub;
import com.navitaire.schemas.webservices.SessionManagerStub;
import com.navitaire.schemas.webservices.UtilitiesManagerStub;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.ListOutput;
import com.tgs.services.fms.datamodel.alternateclass.AlternateClass;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.extern.slf4j.Slf4j;
import tgs.navitaire.schemas.webservices.BookingManagerStubV3;

@Slf4j
@Service
public class NavitaireAirInfoFactory extends AbstractAirInfoFactory {

	protected SoapRequestResponseListner listener = null;

	protected SessionManagerStub sessionStub;

	protected BookingManagerStub bookingStub;

	protected ContentManagerStub contentStub;

	protected BookingManagerStubV3 bookingStubV3;

	protected String sessionToken;

	protected NavitaireBindingService bindingService;


	public NavitaireAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public NavitaireAirInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	public void initialize(boolean isSearch) {
		bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(user).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
		sessionToken = fetchSessionToken(isSearch);
		sessionStub = bindingService.getSessionManagerStub(supplierConf, searchQuery);
		bookingStub = bindingService.getBookingManagerStub(supplierConf, searchQuery);
		if (isSearch) {
			bookingStubV3 = bindingService.getBookingManagerStubV3(supplierConf, searchQuery);
		}
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize(true);
		log.debug("Intialization completed  for search Query {} ", searchQuery);
		searchResult = new AirSearchResult();
		NavitaireSessionManager sessionManager = null;
		int numTry = 0;
		boolean doRetry = false;
		do {
			try {
				listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
						supplierConf.getBasicInfo().getSupplierName());
				sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
						.sessionBinding(sessionStub).searchQuery(searchQuery)
						.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
				sessionManager.init();
				listener.setStoreLogs(sessionManager.isStoreLog(searchQuery));
				if (StringUtils.isBlank(sessionToken)) {
					sessionManager.openSession();
					sessionToken = sessionManager.getSessionSignature();
				}
				NavitaireSearchManager searchManager = NavitaireSearchManager.builder().configuration(supplierConf)
						.searchQuery(searchQuery).sessionSignature(sessionToken).listener(listener)
						.moneyExchnageComm(moneyExchnageComm).bookingBinding(bookingStub).bookingStubV3(bookingStubV3)
						.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
						.supplierAnalyticInfos(supplierAnalyticsInfos).bookingUser(user).build();
				searchManager.init();
				searchResult = searchManager.doSearch(true);
				// NavitaireUtils.filterSearchResults(searchResult, searchQuery, supplierConf);
				searchResult = searchManager.processSearchResult(searchResult);
				searchResult = searchManager.doSearchItineraryPrice(searchResult,
						StringUtils.isEmpty(contextData.getBookingId()));
				searchResult = searchManager.setInfantPrice(searchResult, sessionManager);
				NavitaireUtils.filterTripAndPriceOptions(searchResult, searchQuery);
				// TripPriceEngine.filterTripOnLayOverTime(searchResult, searchQuery,
				// sourceConfiguration);
			} catch (NotOperatingSectorException | NoSearchResultException | SupplierSessionException
					| NoSeatAvailableException | SupplierRemoteException e) {
				setEmptySearchResult();
				if (sessionManager.isRetry(e)) {
					if (StringUtils.isNotBlank(sessionToken)) {
						sessionManager.setSessionSignature(sessionToken);
						sessionManager.closeSession();
					}
					sessionToken = null;
					doRetry = true;
				} else {
					doRetry = false;
					throw e;
				}
			} finally {
				if (StringUtils.isNotBlank(sessionToken) && !doRetry) {
					storeSession(sessionToken);
				}
				storeSessionStubs();
			}
		} while (++numTry < 2 && doRetry);
	}

	private void storeSessionStubs() {
		bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(user).build();
		bindingService.storeSessionMetaInfo(supplierConf, SessionManagerStub.class, sessionStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingManagerStub.class, bookingStub);
		bindingService.storeSessionMetaInfo(supplierConf, BookingManagerStubV3.class, bookingStubV3);
		bindingService.storeSessionMetaInfo(supplierConf, ContentManagerStub.class, contentStub);
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize(false);
		NavitaireSessionManager sessionManager = null;
		SupplierSession session = null;
		boolean isReviewSuccess = true;
		try {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
					.bookingUser(user).build();
			sessionManager.init();
			sessionManager.setSessionToken(session);
			NavitaireSearchManager searchManager = NavitaireSearchManager.builder().configuration(supplierConf)
					.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature).bookingId(bookingId)
					.moneyExchnageComm(moneyExchnageComm).listener(listener).bookingBinding(bookingStub)
					.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
					.bookingUser(user).build();
			searchManager.init();
			selectedTrip = searchManager.getGetItineraryPrice(Arrays.asList(Pair.of(selectedTrip, 0)), false).get(0);
			if (Objects.isNull(selectedTrip)) {
				throw new NoSeatAvailableException();
			}
			NavitaireBookingManager reviewManager = NavitaireBookingManager.builder().configuration(supplierConf)
					.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature).bookingId(bookingId)
					.moneyExchnageComm(moneyExchnageComm).listener(listener).bookingBinding(bookingStub)
					.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
					.bookingUser(user).build();
			NavitaireSSRManager ssrManager = NavitaireSSRManager.builder().configuration(supplierConf)
					.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature).bookingId(bookingId)
					.moneyExchnageComm(moneyExchnageComm).listener(listener).bookingBinding(bookingStub)
					.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
					.bookingUser(user).build();
			reviewManager.init();
			reviewManager.sendSellJourneyRequest(Arrays.asList(selectedTrip));
			ssrManager.init();
			ssrManager.doSSRSearch(selectedTrip);
			searchManager.setInfantPriceOnReview(Arrays.asList(selectedTrip));
			searchManager.baggageAllowance(selectedTrip, bookingId);
		} catch (NoSeatAvailableException | SupplierRemoteException ex) {
			sessionManager.closeSession();
			isReviewSuccess = false;
			throw new NoSeatAvailableException();
		} finally {
			if (isReviewSuccess) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().build();
				sessionInfo.setSessionToken(sessionManager.getSessionSignature());
				storeBookingSession(bookingId, sessionInfo, session, selectedTrip);
			}
			storeSessionStubs();
		}
		return selectedTrip;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		initialize(false);
		NavitaireSessionManager sessionManager = null;
		try {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).criticalMessageLogger(criticalMessageLogger).bookingId(bookingId)
					.bookingUser(user).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				NavitaireFareRuleManager fareRuleManager = NavitaireFareRuleManager.builder()
						.configuration(supplierConf).sessionSignature(sessionManager.sessionSignature)
						.listener(listener).bookingBinding(bookingStub).bookingId(bookingId)
						.criticalMessageLogger(criticalMessageLogger).sourceConfiguration(sourceConfiguration)
						.bookingUser(user).build();
				fareRuleManager.init();
				contentStub = bindingService.getContentManagerStub(supplierConf);
				fareRuleManager.setContentBinding(contentStub);
				FareRuleInformation fareRule = new FareRuleInformation();
				fareRule.getMiscInfo().put("Conditions", fareRuleManager.getFareRule(selectedTrip));
				tripFareRule.getFareRule().put(FareRuleManager.getRuleKey(selectedTrip), fareRule);
			}
		} finally {
			sessionManager.closeSession();
			storeSessionStubs();
		}
		return tripFareRule;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		NavitaireSessionManager sessionManager = null;
		SupplierSession session = null;
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		initialize(false);
		try {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(tripInfo.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).bookingId(bookingId).bookingUser(user).build();
			sessionManager.init();
			sessionManager.setSessionSignature(session.getSupplierSessionInfo().getSessionToken());
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				NavitaireSSRManager seatMapManager =
						NavitaireSSRManager.builder().configuration(supplierConf).moneyExchnageComm(moneyExchnageComm)
								.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature)
								.bookingId(bookingId).sourceConfiguration(sourceConfiguration).listener(listener)
								.criticalMessageLogger(criticalMessageLogger).bookingBinding(bookingStub)
								.bookingUser(user).build();
				seatMapManager.setPaxCount();
				seatMapManager.init();
				tripInfo.getSegmentInfos().forEach(segmentInfo -> {
					List<SeatSSRInformation> seatInfos = seatMapManager.getSeatMap(segmentInfo);
					if (CollectionUtils.isNotEmpty(seatInfos)) {
						SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatInfos).build();
						tripSeatMap.getTripSeat().put(segmentInfo.getId(), seatInformation);
					}
				});
			}
		} catch (Exception e) {
			log.error("Unable to fetch seat map for trip {}", tripInfo.toString(), e);
		} finally {
			storeSessionStubs();
		}
		return tripSeatMap;
	}

	public String fetchSessionToken(boolean isSearch) {
		if (isSearch) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
					.index(0).compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
			if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
				metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString()).index(-1)
						.compress(false).build();
				metaInfo = cachingComm.fetchFromQueue(metaInfo);
			}

			if (metaInfo != null && metaInfo.getValue() != null) {
				return (String) metaInfo.getValue();
			}
		}
		return null;
	}

	public void storeSession(String sessionToken) {
		if (StringUtils.isNotBlank(sessionToken)) {
			try {
				SessionMetaInfo metaInfo = SessionMetaInfo.builder()
						.key(supplierConf.getBasicInfo().getRuleId().toString()).compress(false).build();
				metaInfo.setValue(sessionToken);
				metaInfo.setExpiryTime(LocalDateTime.now()
						.plusMinutes(AirUtils.getSessionTime(supplierConf.getBasicInfo(), sourceConfiguration, user)));
				cachingComm.storeInQueue(metaInfo);
			} catch (Exception e) {
				log.error(AirSourceConstants.SEARCH_SESSION_FAILURE, searchQuery.getSearchId(), e);
				NavitaireSessionManager sessionManager = NavitaireSessionManager.builder().searchQuery(searchQuery)
						.configuration(supplierConf).criticalMessageLogger(criticalMessageLogger).listener(listener)
						.sessionSignature(sessionToken).sessionBinding(sessionStub).bookingUser(user).build();
				sessionManager.init();
				sessionManager.closeSession();
			}
		}
	}

	private void setEmptySearchResult() {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			searchResult = null;
		}
	}

	@Override
	public TripInfo getChangeClass(TripInfo tripInfo, String searchId) {
		FlightBasicFact flightfact = FlightBasicFact.createFact();
		flightfact.generateFact(tripInfo);
		flightfact.setAirType(AirUtils.getAirType(tripInfo));
		BaseUtils.createFactOnUser(flightfact, user);
		ListOutput<String> alternateClass =
				AirConfiguratorHelper.getAirConfigRule(flightfact, AirConfiguratorRuleType.CHANGE_CLASS);
		if (alternateClass != null && CollectionUtils.isNotEmpty(alternateClass.getValues())) {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<AlternateClass> alternateClassList = new ArrayList<>();
				alternateClass.getValues().forEach(alternateClass1 -> {
					AlternateClass class1 = AlternateClass.builder().classOfBook(alternateClass1).build();
					alternateClassList.add(class1);
				});
				segmentInfo.getAlternateClass().addAll(alternateClassList);
			});
		}
		return tripInfo;
	}

	@Override
	public TripInfo getChangeClassFare(TripInfo tripInfo, String searchId) {
		NavitaireSessionManager sessionManager = null;
		try {
			initialize(true);
			listener = new SoapRequestResponseListner(tripInfo.getId(), null,
					supplierConf.getBasicInfo().getSupplierName());
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				NavitaireSearchManager searchManager = NavitaireSearchManager.builder().configuration(supplierConf)
						.searchQuery(searchQuery).sessionSignature(sessionManager.getSessionSignature())
						.listener(listener).bookingBinding(bookingStub).bookingStubV3(bookingStubV3)
						.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
						.bookingUser(user).build();
				searchManager.init();
				searchResult = searchManager.doSearch(true);
			}
			sessionManager.closeSession();
		} catch (Exception e) {
			log.error("Unable to fetch alternate class fare sq {} excep {}", searchQuery, e);
			sessionManager.closeSession();
		} finally {
			storeSessionStubs();
		}
		return tripInfo;
	}

	@Override
	public void closeSession(List<SupplierSession> sessionList, String bookingId) {
		NavitaireSessionManager sessionManager = null;
		bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(user).build();
		if (sessionStub == null) {
			sessionStub = bindingService.getSessionManagerStub(supplierConf, searchQuery);
		}
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
				.sessionBinding(sessionStub).bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
				.bookingUser(user).build();
		sessionManager.init();
		try {
			for (SupplierSession session : sessionList) {
				sessionManager.setSessionSignature(session.getSupplierSessionInfo().getSessionToken());
				sessionManager.closeSession();
			}
		} finally {
			storeSessionStubs();
		}
	}

	@Override
	public List<SourceRouteInfo> addRoutesToSystem(List<SourceRouteInfo> routes, String searchKey) {
		NavitaireSessionManager sessionManager = null;
		try {
			bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(user).build();
			listener = new SoapRequestResponseListner(searchKey, null, supplierConf.getBasicInfo().getSupplierName());
			sessionStub = bindingService.getSessionManagerStub(supplierConf, null);
			sessionManager = NavitaireSessionManager.builder().configuration(supplierConf).listener(listener)
					.sessionBinding(sessionStub).criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			sessionManager.init();
			sessionManager.openSession();
			if (StringUtils.isNotBlank(sessionManager.getSessionSignature())) {
				bindingService =
						NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(user).build();
				UtilitiesManagerStub utilityStub = bindingService.getUtilitiesManagerStub(supplierConf);
				listener =
						new SoapRequestResponseListner(searchKey, null, supplierConf.getBasicInfo().getSupplierName());
				NavitaireRouteManager routeManager =
						NavitaireRouteManager.builder().sessionSignature(sessionManager.getSessionSignature())
								.configuration(supplierConf).criticalMessageLogger(criticalMessageLogger)
								.utilityStub(utilityStub).listener(listener).bookingUser(user).build();
				routeManager.init();
				routeManager.addRouteInfoToSystem(routes);
			}

		} finally {
			sessionManager.closeSession();
			storeSessionStubs();
		}
		return routes;
	}

	@Override
	public Boolean initializeStubs() {
		bindingService = NavitaireBindingService.builder().cacheCommunicator(cachingComm).bookingUser(user).build();
		sessionStub = bindingService.getSessionManagerStub(supplierConf, null);
		bookingStub = bindingService.getBookingManagerStub(supplierConf, null);
		bookingStubV3 = bindingService.getBookingManagerStubV3(supplierConf, null);
		storeSessionStubs();
		return true;
	}

}
