package com.tgs.services.fms.sources.navitaireV4_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.util.Pair;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfBookingName;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerTravelDocument;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingContact;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingName;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerTravelDocument;
import com.navitaire.schemas.webservices.datacontracts.booking.SeatInfo;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.ApplyServiceBundle;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.MessageState;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.PriceItineraryBy;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.SSRAvailabilityMode;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.sources.navitaireV4_2.indigo.IndigoCreditShellManager;
import com.tgs.services.fms.sources.navitaireV4_2.spicejet.SpiceJetCreditShellManager;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;

@Getter
public enum NavitaireAirline {

	GOAIR(AirSourceType.GOAIR.getSourceId()) {
		@Override
		public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration,
				AirSearchQuery searchQuery) {

			if (productClass.equalsIgnoreCase("GB") || productClass.equalsIgnoreCase("GC")) {
				if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
					priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
						fareDetail.setCabinClass(CabinClass.BUSINESS);
					}));
				}
			}
			FareType fareIdentifier = FareType.PUBLISHED;
			if (productClass.equalsIgnoreCase("GF")) {
				fareIdentifier = FareType.FLEXI;
			}
			if (configuration != null && MapUtils.isNotEmpty(configuration.getSupplierCredential().getFareTypes())) {
				fareIdentifier =
						FareType.getEnumFromName(configuration.getSupplierCredential().getFareTypes().get("DEFAULT"));
			}
			if (productClass.equalsIgnoreCase("MG")) {
				fareIdentifier = FareType.GOMORE;
			} else if (productClass.equalsIgnoreCase("SP")) {
				fareIdentifier = FareType.COUPON;
			}

			/**
			 * @implNote : <br>
			 *           1. GB, GC - Corporate - Business Class<br>
			 *           2. BC - Corporate - Economy Class <br>
			 *           3. GS - SALE <br>
			 *           4. SP - SPECIAL ECONOMY <br>
			 *           5. Rest - PUBLISHED
			 */
			/*
			 * if (configuration.isTestCredential()) { if (productClass.equalsIgnoreCase("GB") ||
			 * productClass.equalsIgnoreCase("GC")) { if (priceInfo != null &&
			 * MapUtils.isNotEmpty(priceInfo.getFareDetails())) { priceInfo.getFareDetails().forEach(((paxType,
			 * fareDetail) -> { fareDetail.setCabinClass(CabinClass.BUSINESS); })); } if
			 * (productClass.equalsIgnoreCase("GC")) { fareIdentifier = FareType.CORPORATE; }
			 * 
			 * } else if (productClass.equalsIgnoreCase("BC")) { // GB -> Corporate & Business Fare ; BC -> SmartCorp
			 * Corporate Economy fareIdentifier = FareType.CORPORATE; } else if (productClass.equalsIgnoreCase("SP")) {
			 * fareIdentifier = FareType.COUPON; } else { // Published Fare fareIdentifier = FareType.PUBLISHED; } }
			 */
			priceInfo.setFareIdentifier(fareIdentifier);
		}

		@Override
		public boolean isItineraryPriceOnSearch() {
			return true;
		}

		@Override
		public Map<FareComponent, List<FareComponent>> getMappedFareComponents() {
			Map<FareComponent, List<FareComponent>> mappedComponents = new HashMap<>();
			List<FareComponent> componentList = new ArrayList<>();
			componentList.add(FareComponent.AT);
			componentList.add(FareComponent.OC);
			mappedComponents.put(FareComponent.YQ, componentList);
			return mappedComponents;
		}


		@Override
		public boolean getEnforceSeatGroupRestrictions() {
			return true;
		}

		@Override
		public SSRAvailabilityMode getSSRBunbleMode(TripInfo tripInfo) {
			String fareType = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getFareType();
			if (fareType.equals(FareType.SME.name()) || fareType.equals(FareType.CORPORATE.name())) {
				return SSRAvailabilityMode.BundledSSRs;
			}
			return SSRAvailabilityMode.NonBundledSSRs;
		}

		@Override
		public int getContractVersion() {
			return 430;
		}

		@Override
		public ApplyServiceBundle isServiceBundleApplicable(AirSourceConfigurationOutput sourceConfiguration,
				List<Pair<TripInfo, Integer>> flightList) {
			if (CollectionUtils.isNotEmpty(flightList)) {
				TripInfo trip = flightList.get(0).getFirst();
				if (sourceConfiguration != null && SSRAvailabilityMode.BundledSSRs.equals(getSSRBunbleMode(trip))
						&& BooleanUtils.isFalse(sourceConfiguration.getIsServiceBundle())) {
					return ApplyServiceBundle.No;
				}
			}
			return ApplyServiceBundle.Yes;
		}

	},
	SPICEJET(AirSourceType.SPICEJET.getSourceId()) {
		@Override
		public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration,
				AirSearchQuery searchQuery) {

			FareType fareIdentifier = FareType.PUBLISHED;

			/**
			 * @implNote : 1. CP - Corporate Fare <br>
			 *           2. HO - Hand Baggage Fare <br>
			 *           3. RS, SS - Regular - SALE Fare <br>
			 *           4. XA - SPECIAL RETURN Fare <br>
			 *           5. NN , NF - Promo ADVANCED <br>
			 *           6. XB - FRIENDLY Fare <br>
			 *           7. EF - Flex Fare <br>
			 *           8. BC -Business <br>
			 *           9. CL -COVID Special 10. Rest - PUBLISHED
			 */
			if ((productClass.equalsIgnoreCase("CP") || productClass.equalsIgnoreCase("SA"))
					&& priceInfo.getSupplierId().toUpperCase().contains("SME")) {
				fareIdentifier = FareType.SME;
			} else if (productClass.equalsIgnoreCase("CP") && isCorporateAccount(priceInfo)) {
				fareIdentifier = FareType.CORPORATE;
			} else if (productClass.equalsIgnoreCase("CP")) {
				fareIdentifier = FareType.CORPORATE;
			} else if (productClass.equalsIgnoreCase("XB")) {
				fareIdentifier = FareType.FAMILY;
			} else if (productClass.equalsIgnoreCase("SS")) {
				fareIdentifier = FareType.SALE;
			} else if (productClass.equalsIgnoreCase("XA")) {
				fareIdentifier = FareType.SPECIAL_RETURN;
			} else if (productClass.equalsIgnoreCase("DD")) {
				fareIdentifier = FareType.COUPON;
			} else if (productClass.equalsIgnoreCase("CL")) {
				fareIdentifier = FareType.COVID_SPECIAL;
			} else if (productClass.equalsIgnoreCase("HO")) {
				// fare
				fareIdentifier = FareType.HANDBAGGAGE;
				if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
					priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
						fareDetail.setIsHandBaggage(true);
					}));
				}
			} else if (productClass.equalsIgnoreCase("BC")) {
				if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
					priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
						fareDetail.setCabinClass(CabinClass.BUSINESS);
						fareDetail.setIsMealIncluded(true);
					}));
				}
			}
			priceInfo.setFareIdentifier(fareIdentifier);
		}

		@Override
		public boolean isAdditionalSSRRequired(List<SegmentInfo> segments) {
			boolean isRequired = NavitaireUtils.isBusinessClass(segments.get(0));
			return isRequired;
		}

		@Override
		public List<String> getAdditionalSSRCodes(List<SegmentInfo> segments) {
			List<String> additionalSSRList = new ArrayList<>();
			if (NavitaireUtils.isBusinessClass(segments.get(0)))
				additionalSSRList.add("BIZZ");
			return additionalSSRList;
		}

		@Override
		public boolean isItineraryPriceOnSearch() {
			return true;
		}

		@Override
		public Map<FareComponent, List<FareComponent>> getMappedFareComponents() {
			Map<FareComponent, List<FareComponent>> mappedComponents = new HashMap<>();
			List<FareComponent> componentList = new ArrayList<>();
			componentList.add(FareComponent.AT);
			componentList.add(FareComponent.OC);
			mappedComponents.put(FareComponent.AGST, componentList);
			return mappedComponents;
		}

		@Override
		public boolean isPassengerWiseCancellationAllowed() {
			return false;
		}

		@Override
		public boolean isSeperateAPIForCSPayment() {
			return true;
		}

		@Override
		public NavitaireServiceManager getCreditShellManager(User user) {
			return SpiceJetCreditShellManager.builder().bookingUser(user).build();
		}

	},
	INDIGO(AirSourceType.INDIGO.getSourceId()) {
		@Override
		public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration,
				AirSearchQuery searchQuery) {

			FareType fareIdentifier = FareType.PUBLISHED;
			if (productClass.equalsIgnoreCase("J")) {
				fareIdentifier = FareType.FLEXI_PLUS;
			} else if (productClass.equalsIgnoreCase("A")) {
				fareIdentifier = FareType.FAMILY;
			} else if (productClass.equalsIgnoreCase("B")) {
				fareIdentifier = FareType.LITE;
			} else if (productClass.equalsIgnoreCase("F")) {
				fareIdentifier = FareType.CORPORATE;
			} else if (productClass.equalsIgnoreCase("N")) {
				fareIdentifier = FareType.SPECIAL_RETURN;
			} else if (productClass.equalsIgnoreCase("S")) {
				/**
				 * Temp fix to combine PUBLISHED and SALE fare in Intl Return
				 */
				if (searchQuery != null && searchQuery.isIntlReturn()) {
					fareIdentifier = FareType.PUBLISHED;
				} else {
					fareIdentifier = FareType.SALE;
				}
			} else if (productClass.equalsIgnoreCase("T")) {
				fareIdentifier = FareType.TACTICAL;
			} else if (productClass.equalsIgnoreCase("M")) {
				fareIdentifier = FareType.SME;
			} else if (productClass.equalsIgnoreCase("C")) {
				fareIdentifier = FareType.COUPON;
			}
			priceInfo.setFareIdentifier(fareIdentifier);
		}

		@Override
		public boolean isRecievedInfoAllowed() {
			return false;
		}

		@Override
		public String getGSTTypeCode() {
			return "I";
		}

		@Override
		public short getInstallmentCode() {
			return 1;
		}

		/*
		 * In case of connecting flight Indigo Will support only on First segment , later from their end pushed to
		 * entire journey
		 */
		/*
		 * @param segmentinfo
		 */
		@Override
		protected boolean isSSRApplicableOnAllSegment(SegmentInfo segmentInfo) {
			if (segmentInfo.getSegmentNum().equals(0)) {
				return true;
			}
			return false;
		}

		@Override
		public boolean isGetAvailabilityV2() {
			return false;
		}

		@Override
		public boolean isBaggageAllowanceFromAPI() {
			return true;
		}

		@Override
		public boolean isAncillaryGSTRequired() {
			return false;
		}

		@Override
		public boolean isItineraryPriceOnSearch() {
			return true;
		}

		@Override
		public boolean isPaxCountOnSell() {
			return false;
		}

		@Override
		public String getTitle(FlightTravellerInfo travellerInfo) {
			if (travellerInfo.getPaxType().equals(PaxType.CHILD)) {
				return "CHD";
			} else if (travellerInfo.getPaxType().equals(PaxType.INFANT)) {
				return StringUtils.EMPTY;
			} else if (NavitaireUtils.isMrsMs(travellerInfo) && travellerInfo.getPaxType().equals(PaxType.ADULT)) {
				return travellerInfo.getTitle().toUpperCase();
			}
			return travellerInfo.getTitle().toUpperCase();
		}

		@Override
		public Map<FareComponent, List<FareComponent>> getMappedFareComponents() {
			Map<FareComponent, List<FareComponent>> mappedComponents = new HashMap<>();
			List<FareComponent> componentList = new ArrayList<>();
			componentList.add(FareComponent.AT);
			componentList.add(FareComponent.OC);
			mappedComponents.put(FareComponent.YQ, componentList);
			return mappedComponents;
		}

		@Override
		public List<String> codeShareFlights() {
			return Arrays.asList("TK");
		}

		@Override
		public boolean isSeatApplicable(SegmentInfo segmentInfo, SeatInfo seat) {
			if (seat != null && seat.getSeatDesignator().equalsIgnoreCase("1A")
					&& NavitaireUtils.isDomesticSegment(segmentInfo)) {
				return false;
			}
			return true;
		}

		@Override
		public NavitaireServiceManager getCreditShellManager(User user) {
			return IndigoCreditShellManager.builder().bookingUser(user).build();
		}

		@Override
		public boolean isCancelStatusCheckRq() {
			return false;
		}


	},
	FLYSCOOT(AirSourceType.SCOOT.getSourceId()) {
		@Override
		public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration,
				AirSearchQuery searchQuery) {
			FareType fareIdentifier = FareType.PUBLISHED;
			if (productClass.equalsIgnoreCase("E1")) {
				fareIdentifier = FareType.FLY;
			} else if (productClass.equalsIgnoreCase("E2") || productClass.equalsIgnoreCase("E4")
					|| productClass.equalsIgnoreCase("E5")) {
				fareIdentifier = FareType.FLYBAG;
			} else if (productClass.equalsIgnoreCase("E3")) {
				fareIdentifier = FareType.FLYBAGEAT;
			} else if (productClass.equalsIgnoreCase("E3")) {
				fareIdentifier = FareType.FLYBAGEAT;
			} else if (productClass.equalsIgnoreCase("J")) {
				fareIdentifier = FareType.SCOOTPLUS;
				if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
					priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
						fareDetail.setCabinClass(CabinClass.PREMIUM_ECONOMY);
					}));
				}
			}
			priceInfo.setFareIdentifier(fareIdentifier);
		}

		@Override
		public boolean isInfantFareOnJourneyWise() {
			return false;
		}

		@Override
		public void setOtherDetails(BookingContact contact, ClientGeneralInfo companyInformation) {
			contact.setOtherPhone(companyInformation.getWorkPhone());
			contact.setProvinceState(companyInformation.getStateCode());
		}

		@Override
		public String getLocationCode(SupplierConfiguration configuration) {
			return StringUtils.EMPTY;
		}

		@Override
		public String getCultureCode() {
			return "en-SG";
		}

		@Override
		public boolean isRecievedInfoAllowed() {
			return false;
		}

		@Override
		public String getAgentCode() {
			return StringUtils.EMPTY;
		}

		@Override
		public String getTitle(FlightTravellerInfo travellerInfo) {
			if ((travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())
					|| travellerInfo.getPaxType().getType().equals(PaxType.INFANT.getType()))
					&& NavitaireUtils.isMaster(travellerInfo)) {
				return "MSTR";
			} else if ((travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())
					|| travellerInfo.getPaxType().getType().equals(PaxType.INFANT.getType()))
					&& NavitaireUtils.isMiss(travellerInfo)) {
				return "MISS";
			}
			return travellerInfo.getTitle().toUpperCase();
		}

		@Override
		public boolean isInfantFareOnSegmentWise(SegmentInfo segmentInfo) {
			return true;
		}

		@Override
		public boolean isBaggageAmountSegmentWise() {
			return true;
		}

	},
	JAZEERA(AirSourceType.JAZEERA.getSourceId()) {
		@Override
		public void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration,
				AirSearchQuery searchQuery) {
			FareType fareIdentifier = FareType.PUBLISHED;
			AtomicReference<CabinClass> cabinClass = new AtomicReference<>(CabinClass.ECONOMY);
			if (productClass.equalsIgnoreCase("BU")) {
				fareIdentifier = FareType.BUSINESS;
				cabinClass.set(CabinClass.BUSINESS);
			} else if (productClass.equalsIgnoreCase("EL")) {
				fareIdentifier = FareType.LIGHT;
			} else if (productClass.equalsIgnoreCase("EV")) {
				fareIdentifier = FareType.VALUE;
			} else if (productClass.equalsIgnoreCase("EE")) {
				fareIdentifier = FareType.EXTRA;
			}
			if (priceInfo != null && MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
				priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
					fareDetail.setCabinClass(cabinClass.get());
				}));
			}
			priceInfo.setFareIdentifier(fareIdentifier);
		}

		@Override
		public int getContractVersion() {
			return 0;
		}

		@Override
		public String getMessageContractVersion(SupplierConfiguration configuration) {
			return configuration.getSupplierCredential().getUserName();
		}

		@Override
		public boolean isGetAvailabilityV2() {
			return false;
		}

		@Override
		public void setOtherDetails(BookingContact contact, ClientGeneralInfo companyInformation) {
			contact.setOtherPhone(companyInformation.getWorkPhone());
			contact.setProvinceState(companyInformation.getStateCode());
		}

		@Override
		public boolean isItineraryPriceOnSearch() {
			return true;
		}

		@Override
		public boolean getIsSSRInSellJourney() {
			return false;
		}

		@Override
		public boolean isAllowedInBucket() {
			return false;
		}

		@Override
		public boolean isInfantFareFixed() {
			return false;
		}

		@Override
		public String getPassportTypeCode() {
			return "PASS";
		}

	};

	private int sourceId;

	private NavitaireAirline(int source) {
		this.sourceId = source;
	}

	public static final String PASS_DOC_TYPE_CODE = "P";

	public static final String FF_DOC_TYPE_CODE = "OAFF";

	public static NavitaireAirline getNavitaireAirline(int id) {
		for (NavitaireAirline navitaireType : NavitaireAirline.values()) {
			if (navitaireType.getSourceId() == id) {
				return navitaireType;
			}
		}
		return null;
	}

	public boolean isRecievedInfoAllowed() {
		return true;
	}

	public boolean isPassengerWiseCancellationAllowed() {
		return true;
	}

	protected PriceItineraryBy getPriceItineraryByValue() {
		return PriceItineraryBy.JourneyBySellKey;
	}

	protected boolean sendSellRequestByJourneyKey() {
		return true;
	}

	public abstract void processPriceInfo(String productClass, PriceInfo priceInfo, SupplierConfiguration configuration,
			AirSearchQuery searchQuery);

	public String getGSTTypeCode() {
		return "G";
	}

	public String getContactTypeCode() {
		return "P";
	}

	public short getInstallmentCode() {
		return 0;
	}

	/* SSR Applicable on All segment */
	protected boolean isSSRApplicableOnAllSegment(SegmentInfo segmentInfo) {
		return true;
	}

	public String getAgentCode() {
		return "AG";
	}

	public String getCreditFilePaymentCode() {
		return "CF";
	}

	public String getCreditShellCode() {
		return "CS";
	}

	public short getConnectingFlightsCount(Boolean isDomesticSearch, AirSourceConfigurationOutput sourceConfiguration) {
		if (isDomesticSearch && Objects.nonNull(sourceConfiguration)
				&& Objects.nonNull(sourceConfiguration.getDomflightsCount())) {
			return sourceConfiguration.getDomflightsCount().shortValue();
		} else if (!isDomesticSearch && Objects.nonNull(sourceConfiguration)
				&& Objects.nonNull(sourceConfiguration.getFlightsCount())) {
			return sourceConfiguration.getFlightsCount().shortValue();
		}
		return 20;
	}

	public boolean isGetAvailabilityV2() {
		return true;
	}

	public boolean isBaggageAllowanceFromAPI() {
		return false;
	}

	public boolean isAncillaryGSTRequired() {
		return true;
	}

	public boolean isItineraryPriceOnSearch() {
		return false;
	}

	public boolean isPaxCountOnSell() {
		return true;
	}

	public String getTitle(FlightTravellerInfo travellerInfo) {
		if ((travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())
				|| travellerInfo.getPaxType().getType().equals(PaxType.INFANT.getType()))
				&& NavitaireUtils.isMaster(travellerInfo)) {
			return "MR";
		} else if ((travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())
				|| travellerInfo.getPaxType().getType().equals(PaxType.INFANT.getType()))
				&& NavitaireUtils.isMiss(travellerInfo)) {
			return "MS";
		}
		return travellerInfo.getTitle().toUpperCase();
	}

	public boolean isCorporateAccount(PriceInfo priceInfo) {
		String supplierId = priceInfo.getSupplierId().toUpperCase();
		return supplierId.contains("CORP") || supplierId.contains("CORPORATE");
	}

	public ArrayOfPassengerTravelDocument getPassengerTravelDocument(FlightTravellerInfo travellerInfo,
			FlightTravellerInfo infantTraveller, String carrierCode) {
		ArrayOfPassengerTravelDocument passengerTravelDocument = new ArrayOfPassengerTravelDocument();

		// adult passport
		PassengerTravelDocument travelDocument = getTravelDocument(travellerInfo, false, carrierCode);
		if (travelDocument != null) {
			passengerTravelDocument.addPassengerTravelDocument(travelDocument);
		}
		// Freq flier
		if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
			travelDocument = getTravelDocument(travellerInfo, true, carrierCode);
			if (travelDocument != null) {
				passengerTravelDocument.addPassengerTravelDocument(travelDocument);
			}
		}
		// Infant
		if (infantTraveller != null) {
			travelDocument = getTravelDocument(infantTraveller, false, carrierCode);
			if (travelDocument != null) {
				passengerTravelDocument.addPassengerTravelDocument(travelDocument);
			}
			if (MapUtils.isNotEmpty(infantTraveller.getFrequentFlierMap())) {
				travelDocument = getTravelDocument(infantTraveller, true, carrierCode);
				if (travelDocument != null) {
					passengerTravelDocument.addPassengerTravelDocument(travelDocument);
				}
			}
		}

		return passengerTravelDocument;
	}

	public PassengerTravelDocument getTravelDocument(FlightTravellerInfo travellerInfo, boolean isFF,
			String carrierCode) {
		PassengerTravelDocument travelDocument = null;
		if (StringUtils.isNotBlank(travellerInfo.getPassportNumber()) && !isFF) {
			travelDocument = new PassengerTravelDocument();
			travelDocument.setState(MessageState.New);
			travelDocument.setGender(NavitaireUtils.getGender(travellerInfo.getTitle(), travellerInfo));
			travelDocument.setDocTypeCode(this.getPassportTypeCode());// passport type
			travelDocument.setDOB(TgsDateUtils.getCalendar(travellerInfo.getDob()));
			travelDocument.setNationality(travellerInfo.getPassportNationality());
			travelDocument.setDocNumber(travellerInfo.getPassportNumber());
			travelDocument.setNames(getBookingPaxName(travellerInfo));
			travelDocument.setExpirationDate(TgsDateUtils.getCalendar(travellerInfo.getExpiryDate()));
			travelDocument.setIssuedDate(TgsDateUtils.getCalendar(travellerInfo.getPassportIssueDate()));
			travelDocument.setIssuedByCode(travellerInfo.getPassportNationality());
			if (travellerInfo.getPaxType().equals(PaxType.INFANT)) {
				travelDocument.setDocSuffix("I");
			}
		} else if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
			String ffNumber = travellerInfo.getFrequentFlierMap().getOrDefault(carrierCode, null);
			if (StringUtils.isNotBlank(ffNumber)) {
				travelDocument = new PassengerTravelDocument();
				travelDocument.setState(MessageState.New);
				travelDocument.setGender(NavitaireUtils.getGender(travellerInfo.getTitle(), travellerInfo));
				travelDocument.setDocTypeCode(FF_DOC_TYPE_CODE);// ffliercode type
				travelDocument.setDOB(TgsDateUtils.getCalendar(travellerInfo.getDob()));
				travelDocument.setNationality(travellerInfo.getPassportNationality());
				travelDocument.setDocNumber(ffNumber);
				travelDocument.setNames(getBookingPaxName(travellerInfo));
				travelDocument.setIssuedByCode(travellerInfo.getPassportNationality());
			}
		}
		return travelDocument;
	}

	public ArrayOfBookingName getBookingPaxName(FlightTravellerInfo flightTravellerInfo) {
		ArrayOfBookingName bookingPaxName = new ArrayOfBookingName();
		BookingName[] bookingNames = new BookingName[1];
		bookingNames[0] = new BookingName();
		bookingNames[0].setFirstName(flightTravellerInfo.getFirstName());
		bookingNames[0].setLastName(flightTravellerInfo.getLastName());
		bookingNames[0].setState(MessageState.New);
		bookingNames[0].setTitle(getTitle(flightTravellerInfo));
		bookingPaxName.setBookingName(bookingNames);
		return bookingPaxName;
	}

	public void setOtherDetails(BookingContact contact, ClientGeneralInfo companyInformation) {
		contact.setProvinceState(StringUtils.EMPTY);
	}

	public int getContractVersion() {
		return 420;
	}

	public String getMessageContractVersion(SupplierConfiguration configuration) {
		return StringUtils.EMPTY;
	}

	public boolean getTaxAndFeesAllowed() {
		return true;
	}

	// This will returned the Mapped components.
	public Map<FareComponent, List<FareComponent>> getMappedFareComponents() {
		Map<FareComponent, List<FareComponent>> mappedComponents = new HashMap<>();
		List<FareComponent> componentList = new ArrayList<>();
		componentList.add(FareComponent.AT);
		componentList.add(FareComponent.OC);
		mappedComponents.put(FareComponent.YQ, componentList);
		return mappedComponents;
	}

	public List<String> codeShareFlights() {
		return new ArrayList<>();
	}

	public boolean getEnforceSeatGroupRestrictions() {
		return false;
	}


	public String getLocationCode(SupplierConfiguration configuration) {
		return configuration.getSupplierCredential().getDomain();
	}

	public String getCultureCode() {
		return "en-GB";
	}

	public String getPaymentMethodCode() {
		return "AG";
	}

	public boolean isAdditionalSSRRequired(List<SegmentInfo> segments) {
		return false;
	}

	public List<String> getAdditionalSSRCodes(List<SegmentInfo> segments) {
		return new ArrayList<>();
	}

	public boolean isInfantFareOnSegmentWise(SegmentInfo segmentInfo) {
		return segmentInfo.getSegmentNum().intValue() == 0;
	}

	public boolean isNonBundleFare(SegmentInfo segmentInfo) {
		if (segmentInfo.getPriceInfo(0).getFareType().equals(FareType.SME.name())
				|| segmentInfo.getPriceInfo(0).getFareType().equals(FareType.CORPORATE.name())) {
			return false;
		}
		return true;
	}

	public SSRAvailabilityMode getSSRBunbleMode(TripInfo tripInfo) {
		return SSRAvailabilityMode.NonBundledSSRs;
	}

	public AirportInfo getSourceAirportCode(AirSearchQuery searchQuery) {
		if (searchQuery != null) {
			return searchQuery.getRouteInfos().get(0).getFromCityOrAirport();
		}
		return null;
	}

	public boolean getIsSSRInSellJourney() {
		return true;
	}

	public boolean isAllowedInBucket() {
		return true;
	}

	public boolean isInfantFareFixed() {
		return true;
	}

	public boolean isSeatApplicable(SegmentInfo segmentInfo, SeatInfo seat) {
		return true;
	}

	public boolean isInfantFareOnJourneyWise() {
		return true;
	}

	public boolean isBaggageAmountSegmentWise() {
		return false;
	}

	public String getPassportTypeCode() {
		return PASS_DOC_TYPE_CODE;
	}

	public String getAccountNumber(SupplierConfiguration configuration) {
		return configuration.getSupplierCredential().getOrganisationCode().toUpperCase();
	}

	public ApplyServiceBundle isServiceBundleApplicable(AirSourceConfigurationOutput sourceConfiguration,
			List<Pair<TripInfo, Integer>> flightList) {
		return ApplyServiceBundle.Yes;
	}

	public boolean isSeperateAPIForCSPayment() {
		return false;
	}

	public NavitaireServiceManager getCreditShellManager(User user) {
		return null;
	}

	public boolean isCancelStatusCheckRq() {
		return true;
	}

}
