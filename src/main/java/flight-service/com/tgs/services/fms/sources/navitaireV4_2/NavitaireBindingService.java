package com.tgs.services.fms.sources.navitaireV4_2;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;
import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.lang3.StringUtils;
import com.navitaire.schemas.webservices.BookingManagerStub;
import com.navitaire.schemas.webservices.ContentManagerStub;
import com.navitaire.schemas.webservices.SessionManagerStub;
import com.navitaire.schemas.webservices.UtilitiesManagerStub;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.cacheservice.datamodel.CacheType;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;
import tgs.navitaire.schemas.webservices.BookingManagerStubV3;

@Setter
@Slf4j
@Builder
public class NavitaireBindingService {

	private GeneralCachingCommunicator cacheCommunicator;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	protected User bookingUser;

	public SessionManagerStub getSessionManagerStub(SupplierConfiguration configuration, AirSearchQuery query) {
		SessionManagerStub sessionStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, SessionManagerStub.class, null));
			if (metaInfo != null) {
				sessionStub = (SessionManagerStub) metaInfo.getValue();
			}
			if (sessionStub == null) {
				log.info("Creating new Session Stub for searchQuery {}", query);
				sessionStub = new SessionManagerStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "SessionManager.svc"));
			}
		} catch (Exception e) {
			log.error("Session Binding Initilaized failed for  cause", e);
		}
		setProxy(sessionStub);
		return sessionStub;
	}

	@SuppressWarnings("rawtypes")
	public static SessionMetaInfo getSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder()
				.key(String.join("-", configuration.getBasicInfo().getRuleId().toString(), classx.toString()))
				.value(value).expiryTime(LocalDateTime.now().plusMinutes(600)).cacheType(CacheType.INMEMORY).build();
		return metaInfo;
	}

	@SuppressWarnings("rawtypes")
	public void storeSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		if (value != null) {
			SessionMetaInfo metaInfo = getSessionMetaInfo(configuration, classx, value);
			cacheCommunicator.storeInQueue(metaInfo);
		}
	}

	public SessionMetaInfo fetchFromQueue(SessionMetaInfo metaInfo) {
		metaInfo.setIndex(0);
		SessionMetaInfo cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
		if (cacheInfo == null) {
			metaInfo.setIndex(-1);
			cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
		}
		return cacheInfo;
	}

	public BookingManagerStub getBookingManagerStub(SupplierConfiguration configuration, AirSearchQuery query) {
		BookingManagerStub bookingStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, BookingManagerStub.class, null));
			if (metaInfo != null) {
				bookingStub = (BookingManagerStub) metaInfo.getValue();
			}

			if (bookingStub == null) {
				log.info("Creating new booking Stub for searchQuery {}", query);
				bookingStub = new BookingManagerStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "BookingManager.svc"));
			}
		} catch (Exception e) {
			log.error("Booking Binding Initilaized failed for cause", e);
		}
		setProxy(bookingStub);
		return bookingStub;
	}

	public BookingManagerStubV3 getBookingManagerStubV3(SupplierConfiguration configuration, AirSearchQuery query) {
		BookingManagerStubV3 bookingStub = null;
		// At moment Only for Indigo in old get avialbility
		if (configuration.getBasicInfo().getSourceId().equals(NavitaireAirline.INDIGO.getSourceId())
				|| configuration.getBasicInfo().getSourceId().equals(NavitaireAirline.JAZEERA.getSourceId())) {
			try {
				SessionMetaInfo metaInfo =
						fetchFromQueue(getSessionMetaInfo(configuration, BookingManagerStubV3.class, null));
				if (metaInfo != null) {
					bookingStub = (BookingManagerStubV3) metaInfo.getValue();
				}
				if (bookingStub == null) {
					log.info("Creating new bookingV3 Stub for searchQuery {}", query);
					bookingStub = new BookingManagerStubV3(
							String.join("", configuration.getSupplierCredential().getUrl(), "BookingManager.svc"));
				}
			} catch (Exception e) {
				log.error("Booking BindingV3 Initilaized failed ", e);
			}
			setProxy(bookingStub);
		}
		return bookingStub;
	}

	public ContentManagerStub getContentManagerStub(SupplierConfiguration configuration) {
		ContentManagerStub contentManager = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, ContentManagerStub.class, null));
			if (metaInfo != null) {
				contentManager = (ContentManagerStub) metaInfo.getValue();
			}
			if (contentManager == null) {
				contentManager = new ContentManagerStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "ContentManager.svc"));
			}
		} catch (Exception e) {
			log.error("Content Binding Initilaized failed for {} cause ", configuration.getBasicInfo().getSupplierId(),
					e);
		}
		setProxy(contentManager);
		return contentManager;
	}

	public UtilitiesManagerStub getUtilitiesManagerStub(SupplierConfiguration configuration) {
		UtilitiesManagerStub utilityManagerStub = null;
		try {
			SessionMetaInfo metaInfo =
					fetchFromQueue(getSessionMetaInfo(configuration, UtilitiesManagerStub.class, null));
			// utilityManagerStub = cacheCommunicator.fetchFromQueue(metaInfo);
			if (utilityManagerStub == null) {
				utilityManagerStub = new UtilitiesManagerStub(
						String.join("", configuration.getSupplierCredential().getUrl(), "UtilitiesManager.svc"));

			}
		} catch (Exception e) {
			log.error("Utility Binding Initilaized failed for {} cause ", configuration.getBasicInfo().getSupplierId(),
					e);
		}
		setProxy(utilityManagerStub);
		return utilityManagerStub;
	}

	public void setProxy(Stub stub) {
		String proxyAddress = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		String proxy = null;
		if (configuratorInfo != null) {
			proxy = configuratorInfo.getProxyAddress();
		}
		if (StringUtils.isNotBlank(proxy)) {
			int proxyPort = 3128;
			if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
				ServiceClient client = stub._getServiceClient();
				Options options = client.getOptions();
				options.setProperties(null);
				proxyAddress = proxy.split(":")[0];
				proxyPort = Integer.valueOf(proxy.split(":")[1]);
				if (StringUtils.isNotBlank(proxyAddress)) {
					HttpTransportProperties.ProxyProperties proxyProperties =
							new HttpTransportProperties.ProxyProperties();
					proxyProperties.setProxyName(proxyAddress); // 10.10.16.165
					proxyProperties.setProxyPort(proxyPort); // 3128
					options.setProperty(org.apache.axis2.transport.http.HTTPConstants.PROXY, proxyProperties);
					options.setProperty(HTTPConstants.CHUNKED, false);
					// options.setProperty(Constants.Configuration.DISABLE_SOAP_ACTION,
					// Boolean.TRUE);
				}
				client.setOptions(options);
			}
		}

		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions()
					.setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			setConnectionManager(stub);
		}
	}

	public void updateOptions(Stub stub) {
		if (stub != null) {
			ServiceClient client = stub._getServiceClient();
			Options options = client.getOptions();
			options.setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
			client.setOptions(options);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(30);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}

}
