package com.tgs.services.fms.sources.navitaireV4_2;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOflong;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfshort;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.navitaire.schemas.webservices.GetBookingFromStateRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.AddPaymentToBookingRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.AgencyAccount;
import com.navitaire.schemas.webservices.datacontracts.booking.ApplyPromotionRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfBookingContact;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassenger;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerAddress;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerBag;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerEMDCoupon;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerInfant;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerProgram;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerTypeInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaxSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaymentAddress;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaymentField;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSegmentSSRRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSegmentSeatRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingContact;
import com.navitaire.schemas.webservices.datacontracts.booking.CreditShell;
import com.navitaire.schemas.webservices.datacontracts.booking.Journey;
import com.navitaire.schemas.webservices.datacontracts.booking.Leg;
import com.navitaire.schemas.webservices.datacontracts.booking.MCCRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.Passenger;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerInfant;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerProgram;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerTypeInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxTicket;
import com.navitaire.schemas.webservices.datacontracts.booking.Payment;
import com.navitaire.schemas.webservices.datacontracts.booking.PaymentVoucher;
import com.navitaire.schemas.webservices.datacontracts.booking.SSRRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SeatSellRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.Segment;
import com.navitaire.schemas.webservices.datacontracts.booking.SegmentSSRRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SegmentSeatRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SellFee;
import com.navitaire.schemas.webservices.datacontracts.booking.SellJourneyByKeyRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SellJourneyRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SellRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.SellSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.Success;
import com.navitaire.schemas.webservices.datacontracts.booking.ThreeDSecureRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.UpdateContactsRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.UpdatePassengersRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.Warning;
import com.navitaire.schemas.webservices.datacontracts.common.FlightDesignator;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.BookingStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.PaidStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.BookingPaymentStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.DistributionOption;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.Gender;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.MessageState;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.NotificationPreference;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.PaymentReferenceType;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.RequestPaymentMethodType;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.SeatAssignmentMode;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.SellBy;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.UnitHoldType;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.WeightCategory;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.AddPaymentToBookingRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.AddPaymentToBookingResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.ApplyPromotionRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.ApplyPromotionResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.AssignSeatsRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.AssignSeatsResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBookingFromStateResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.SellRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.SellResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.UpdateContactsRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.UpdateContactsResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.UpdatePassengerResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.UpdatePassengersRequest;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierPaymentException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
final class NavitaireBookingManager extends NavitaireServiceManager {

	protected AirSearchResult searchResult;

	protected GeneralServiceCommunicator gmsCommunicator;

	protected String carrierCode;

	protected boolean isTicketNumberRequired;

	protected User bookingUser;
	
	protected double totalFare;
	
	protected double paymentToBeCollected;

	public void initialize(List<SegmentInfo> segmentInfos) {
		init();
		carrierCode = segmentInfos.get(0).getAirlineCode(false);
	}

	public boolean sendSellJourneyRequest(List<TripInfo> tripInfos) {
		boolean isSellSuccess = false;
		try {
			SellRequest sellRequest = new SellRequest();
			SellRequestData sellRequestData = new SellRequestData();
			sellRequestData.setSellBy(SellBy.JourneyBySellKey);
			sellRequestData.setSellJourneyRequest(new SellJourneyRequest());
			sellRequestData.setSellSSR(null);
			sellRequestData.setSellFee(new SellFee());
			SellJourneyByKeyRequest sellKeyRequest = new SellJourneyByKeyRequest();
			sellKeyRequest.setSellJourneyByKeyRequestData(
					getSellJourneyByKeyRequestData(NavitaireUtils.getTripPairs(tripInfos), false, false));
			sellRequestData.setSellJourneyByKeyRequest(sellKeyRequest);
			sellRequest.setSellRequestData(sellRequestData);
			listener.setType("4-SELLTRIP");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			SellResponse sellResponse = bookingBinding.sell(sellRequest, getContractVersion(), getExceptionStackTrace(),
					getMessageContractversion(), getSignature(), getSourceId());
			boolean isError = isAnySellError(sellResponse.getBookingUpdateResponseData(), true);
			isSellSuccess = getIsSellSuccess(sellResponse);
			if (!isError && isSellSuccess) {
				double newPrice = getTotalSellfare(sellResponse.getBookingUpdateResponseData().getSuccess());
				log.info("Total Supplier Price {} for Review Trip Booking id on Sell Request {} ", newPrice, bookingId);
			} else {
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException re) {
			logCriticalMessage(re.getMessage());
			throw new SupplierRemoteException(re.getMessage());
		} catch (com.navitaire.schemas.webservices.IBookingManager_Sell_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIWarningFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIFaultFault_FaultMessage fault) {
			logCriticalMessage(fault.getMessage());
			throw new NoSeatAvailableException();
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSellSuccess;
	}

	private boolean getIsSellSuccess(SellResponse sellResponse) {
		boolean isSellSuccess = false;
		if (sellResponse != null && sellResponse.getBookingUpdateResponseData() != null) {
			Success success = sellResponse.getBookingUpdateResponseData().getSuccess();
			if (success != null && success.getPNRAmount() != null) {
				isSellSuccess = true;
			}
		}
		if (sellResponse.getBookingUpdateResponseData().getWarning() != null) {
			Warning warning = sellResponse.getBookingUpdateResponseData().getWarning();
			log.info("Warning Occured on booking id {} message {} ", getRequestId(), warning.getWarningText());
		}
		return isSellSuccess;
	}

	public List<SegmentInfo> sendSellSSRRequest(List<SegmentInfo> segmentInfos, boolean isInfantSell) {
		searchResult = new AirSearchResult();
		double newPrice = 0;
		try {
			if (navitaireAirline.sendSellRequestByJourneyKey()) {
				SellRequest sellRequest = new SellRequest();
				SellRequestData sellRequestData = new SellRequestData();
				sellRequestData.setSellBy(SellBy.SSR);
				sellRequestData.setSellJourneyRequest(new SellJourneyRequest());
				sellRequestData.setSellFee(new SellFee());
				sellRequestData.setSellJourneyByKeyRequest(null);
				sellRequestData.setSellSSR(getSSRTrip(segmentInfos, isInfantSell));
				sellRequest.setSellRequestData(sellRequestData);
				listener.setType("B-SELLSSR");
				bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
				SellResponse sellResponse = bookingBinding.sell(sellRequest, getContractVersion(),
						getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
				if (!isAnySellError(sellResponse.getBookingUpdateResponseData(), true)) {
					// Sell Success - Trip Itinerary
					newPrice = getTotalSellfare(sellResponse.getBookingUpdateResponseData().getSuccess());
					log.info("Total Supplier Price {} for Booking id {} on Sell SSR Request", newPrice, bookingId);
				} else {
					throw new SupplierUnHandledFaultException("Sell SSR Failed");
				}
			}
		} catch (java.rmi.RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (com.navitaire.schemas.webservices.IBookingManager_Sell_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIWarningFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_Sell_APIFaultFault_FaultMessage apiError) {
			throw new SupplierUnHandledFaultException(apiError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return segmentInfos;
	}

	private SellSSR getSSRTrip(List<SegmentInfo> segmentInfos, boolean isInfantSell) {
		SellSSR sellSSR = new SellSSR();
		if (AirUtils.isSSRAddedInTrip(segmentInfos) || infantCount > 0
				|| navitaireAirline.isAdditionalSSRRequired(segmentInfos)) {
			SSRRequest ssrRequest = new SSRRequest();
			ArrayOfSegmentSSRRequest segmentSSRRequest = new ArrayOfSegmentSSRRequest();
			segmentInfos.forEach(segmentInfo -> {
				SegmentSSRRequest ssrSegmentRequest = new SegmentSSRRequest();
				ssrSegmentRequest.setDepartureStation(segmentInfo.getDepartAirportInfo().getCode());
				ssrSegmentRequest.setArrivalStation(segmentInfo.getArrivalAirportInfo().getCode());
				ssrSegmentRequest.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
				FlightDesignator designator = new FlightDesignator();
				designator.setCarrierCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
				designator.setFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber());
				ssrSegmentRequest.setFlightDesignator(designator);
				ArrayOfPaxSSR paxSSR = getPaxSSR(segmentInfo, isInfantSell);
				if (paxSSR != null && paxSSR.getPaxSSR() != null) {
					ssrSegmentRequest.setPaxSSRs(paxSSR);
					segmentSSRRequest.addSegmentSSRRequest(ssrSegmentRequest);
				}
			});
			ssrRequest.setSegmentSSRRequests(segmentSSRRequest);
			ssrRequest.setCurrencyCode(getCurrencyCode());
			sellSSR.setSSRRequest(ssrRequest);
		}
		return sellSSR;
	}

	public double sendSellPassengerinfo() {
		double sellFare = 0;
		try {
			UpdatePassengersRequest passengersRequest = new UpdatePassengersRequest();
			passengersRequest.setUpdatePassengersRequestData(getPassengerRequestData());
			listener.setType("7-B-uPax");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			UpdatePassengerResponse response = bookingBinding.updatePassengers(passengersRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			isAnySellError(response.getBookingUpdateResponseData(), true);
			sellFare = getTotalSellfare(response.getBookingUpdateResponseData().getSuccess());
			totalFare = getTotalSellfare(response.getBookingUpdateResponseData().getSuccess());
			sellFare = getAmountBasedOnCurrency(new BigDecimal(sellFare), getCurrencyCode());
			log.info("Passenger Details Captured for bookingId {} and total airline Fare {} sell fare {}", bookingId,
					totalFare, sellFare);
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdatePassengers_APIWarningFaultFault_FaultMessage apirError) {
			throw new SupplierUnHandledFaultException(apirError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return sellFare;
	}

	private UpdatePassengersRequestData getPassengerRequestData() {
		UpdatePassengersRequestData passengersRequestData = new UpdatePassengersRequestData();
		passengersRequestData.setWaiveNameChangeFee(false);
		passengersRequestData.setPassengers(getPassengers());
		return passengersRequestData;
	}

	private ArrayOfPassenger getPassengers() {
		ArrayOfPassenger passengers = new ArrayOfPassenger();
		int totalPaxIndex = 0;
		for (int paxIndex = 0; paxIndex < adults.size(); paxIndex++) {
			passengers.addPassenger(getPassengerDetails(adults.get(paxIndex), paxIndex, totalPaxIndex));
			totalPaxIndex++;
		}
		for (int paxIndex = 0; paxIndex < child.size(); paxIndex++) {
			passengers.addPassenger(getPassengerDetails(child.get(paxIndex), paxIndex, totalPaxIndex));
			totalPaxIndex++;
		}
		return passengers;
	}

	private Passenger getPassengerDetails(FlightTravellerInfo travellerInfo, int paxIndex, int totalPaxIndex) {
		Passenger passenger = new Passenger();
		passenger.setPassengerPrograms(getPassengerProgram());
		passenger.setCustomerNumber(StringUtils.EMPTY);
		passenger.setPassengerNumber((short) totalPaxIndex);
		passenger.setFamilyNumber((short) 0);
		passenger.setPaxDiscountCode(getPaxDiscountCode());
		passenger.setNames(navitaireAirline.getBookingPaxName(travellerInfo));
		PassengerInfant infant = null;
		if (travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())
				&& CollectionUtils.isNotEmpty(infants)) {
			infant = getInfantPassenger(travellerInfo, paxIndex);
			if (infant != null) {
				passenger.setInfant(infant);
			}
		}
		passenger.setPassengerInfo(getPassenegerInfo(travellerInfo));
		passenger.setPassengerProgram(new PassengerProgram());
		passenger.setPassengerFees(new ArrayOfPassengerFee());
		passenger.setPassengerAddresses(new ArrayOfPassengerAddress());
		FlightTravellerInfo infantTravellerInfo = getInfantTravellerInfo(travellerInfo, paxIndex);
		passenger.setPassengerTravelDocuments(
				navitaireAirline.getPassengerTravelDocument(travellerInfo, infantTravellerInfo, carrierCode));
		passenger.setPassengerBags(new ArrayOfPassengerBag());
		passenger.setPassengerID(totalPaxIndex);
		passenger.setPassengerTypeInfos(getPassengerTypeInfo(travellerInfo));
		passenger.setPseudoPassenger(false);
		passenger.setPassengerInfos(new ArrayOfPassengerInfo());
		passenger.setPassengerInfants(new ArrayOfPassengerInfant());
		passenger.setPassengerTypeInfo(null);
		passenger.setPassengerEMDCouponList(new ArrayOfPassengerEMDCoupon());

		return passenger;
	}

	private ArrayOfPassengerTypeInfo getPassengerTypeInfo(FlightTravellerInfo flightTravellerInfo) {
		ArrayOfPassengerTypeInfo passengerTypeInfo = new ArrayOfPassengerTypeInfo();
		PassengerTypeInfo[] typeInfo = new PassengerTypeInfo[1];
		typeInfo[0] = new PassengerTypeInfo();
		typeInfo[0] = getPaxTypeInfo(flightTravellerInfo);
		passengerTypeInfo.setPassengerTypeInfo(typeInfo);
		return passengerTypeInfo;
	}

	private PassengerTypeInfo getPaxTypeInfo(FlightTravellerInfo flightTravellerInfo) {
		PassengerTypeInfo typeInfo = new PassengerTypeInfo();
		if (flightTravellerInfo.getDob() != null) {
			typeInfo.setDOB(TgsDateUtils.getCalendar(flightTravellerInfo.getDob()));
		} else {
			typeInfo.setDOB(TgsDateUtils.getDefaultDOB());
		}
		typeInfo.setState(MessageState.New);
		if (flightTravellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			typeInfo.setPaxType(PAX_TYPE_ADULT);
		} else if (flightTravellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())) {
			typeInfo.setPaxType(PAX_TYPE_CHILD);
		}
		return typeInfo;
	}

	private PassengerInfo getPassenegerInfo(FlightTravellerInfo flightTravellerInfo) {
		PassengerInfo passengerInfo = new PassengerInfo();
		passengerInfo.setGender(NavitaireUtils.getGender(flightTravellerInfo.getTitle(), flightTravellerInfo));
		passengerInfo.setNationality(flightTravellerInfo.getPassportNationality());
		passengerInfo.setWeightCategory(getWeightCategory(flightTravellerInfo));
		passengerInfo.setResidentCountry(flightTravellerInfo.getPassportNationality());
		passengerInfo.setState(MessageState.New);
		return passengerInfo;
	}

	private WeightCategory getWeightCategory(FlightTravellerInfo travellerInfo) {
		WeightCategory weightCategory = WeightCategory.Unmapped;
		Gender gender = NavitaireUtils.getGender(travellerInfo.getTitle(), travellerInfo);
		if (travellerInfo.getPaxType().getType().equals(PaxType.CHILD.getType())) {
			weightCategory = WeightCategory.Child;
		} else if (Gender._Male.equalsIgnoreCase(gender.getValue())
				&& travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			weightCategory = WeightCategory.Male;
		} else if (Gender._Female.equalsIgnoreCase(gender.getValue())
				&& travellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())) {
			weightCategory = WeightCategory.Female;
		}
		return weightCategory;
	}

	private PassengerInfant getInfantPassenger(FlightTravellerInfo flightTravellerInfo, int paxIndex) {
		PassengerInfant infant = null;
		FlightTravellerInfo infantTraveller = getInfantTravellerInfo(flightTravellerInfo, paxIndex);
		if (infantTraveller != null) {
			infant = new PassengerInfant();
			if (infantTraveller.getDob() != null) {
				infant.setDOB(TgsDateUtils.getCalendar(infantTraveller.getDob()));
			} else {
				infant.setDOB(TgsDateUtils.getDefaultDOB());
			}
			infant.setNames(navitaireAirline.getBookingPaxName(infantTraveller));
			infant.setGender(NavitaireUtils.getGender(infantTraveller.getTitle(), infantTraveller));
			infant.setNationality(NavitaireUtils.getNationality(infantTraveller));
			infant.setPaxType(StringUtils.EMPTY);
			infant.setResidentCountry(NavitaireUtils.getNationality(infantTraveller));
			infant.setState(MessageState.New);
		}
		return infant;
	}

	private FlightTravellerInfo getInfantTravellerInfo(FlightTravellerInfo flightTravellerInfo, int paxIndex) {
		FlightTravellerInfo infantTraveller = null;
		if (flightTravellerInfo.getPaxType().getType().equals(PaxType.ADULT.getType())
				&& infants.size() >= (paxIndex + 1)) {
			infantTraveller = infants.get(paxIndex);
		}
		return infantTraveller;
	}

	private ArrayOfPassengerProgram getPassengerProgram() {
		ArrayOfPassengerProgram paxProgram = new ArrayOfPassengerProgram();
		PassengerProgram[] passengerPrograms = new PassengerProgram[1];
		paxProgram.setPassengerProgram(passengerPrograms);
		return paxProgram;
	}

	public String getPaxDiscountCode() {
		return StringUtils.EMPTY;
	}

	public double doPayment() {
		double pnrCreditBalance = AirUtils.calculateAvailablePNRCredit(segmentInfos);
		if (pnrCreditBalance > 0) {
			if (navitaireAirline.isSeperateAPIForCSPayment()) {
				paymentToBeCollected += pnrCreditBalance;
			} else {
				paymentToBeCollected += addPaymentToBooking(RequestPaymentMethodType.CreditFile, pnrCreditBalance);
			}
		}
		if (totalFare - pnrCreditBalance > 0) {
			paymentToBeCollected +=
					addPaymentToBooking(RequestPaymentMethodType.AgencyAccount, totalFare - paymentToBeCollected);
		}
		return paymentToBeCollected;
	}

	public double addPaymentToBooking(RequestPaymentMethodType paymentMethod, double totalFare) {
		try {
			AddPaymentToBookingRequest paymentToBookingRequest = new AddPaymentToBookingRequest();
			paymentToBookingRequest.setAddPaymentToBookingReqData(getPaymentRequestData(paymentMethod, totalFare));
			listener.setType("8-B-PAYMENT " + getSourceId());
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			AddPaymentToBookingResponse paymentToBookingResponse =
					bookingBinding.addPaymentToBooking(paymentToBookingRequest, getContractVersion(),
							getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			if (!isAnyPaymentError(paymentToBookingResponse)) {
				Payment payment =
						paymentToBookingResponse.getBookingPaymentResponse().getValidationPayment().getPayment();
				paymentToBeCollected = getAmountBasedOnCurrency(payment.getPaymentAmount(), payment.getCurrencyCode());
			} else {
				throw new SupplierPaymentException(StringUtils.join(",", getCriticalMessageLogger()));
			}
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (
				com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AddPaymentToBooking_APIWarningFaultFault_FaultMessage apirError) {
			throw new SupplierUnHandledFaultException(apirError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return paymentToBeCollected;
	}

	private AddPaymentToBookingRequestData getPaymentRequestData(RequestPaymentMethodType paymentMethod,
			double totalFare) {
		AddPaymentToBookingRequestData paymentRequestData = new AddPaymentToBookingRequestData();
		paymentRequestData.setMessageState(MessageState.New);
		paymentRequestData.setWaiveFee(false);
		paymentRequestData.setReferenceType(PaymentReferenceType.Default);
		if (RequestPaymentMethodType.CreditFile.equals(paymentMethod)) {
			String creditBalancePNR = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCreditShellPNR();
			paymentRequestData.setPaymentMethodType(paymentMethod);
			paymentRequestData.setPaymentMethodCode(navitaireAirline.getCreditFilePaymentCode());
			paymentRequestData.setAccountNumber(creditBalancePNR);
			paymentRequestData.setCreditFile(NavitaireUtils.getCreditFile(creditBalancePNR));
		} else if (RequestPaymentMethodType.AgencyAccount.equals(paymentMethod)) {
			paymentRequestData.setPaymentMethodType(paymentMethod);
			paymentRequestData.setPaymentMethodCode(navitaireAirline.getPaymentMethodCode());
			paymentRequestData.setAccountNumber(navitaireAirline.getAccountNumber(configuration));
		}
		paymentRequestData.setQuotedCurrencyCode(getCurrencyCode());
		paymentRequestData.setQuotedAmount(BigDecimal.valueOf(totalFare));
		paymentRequestData.setStatus(BookingPaymentStatus.New);
		paymentRequestData.setAccountNumberID(0);
		paymentRequestData.setParentPaymentID(0);
		paymentRequestData.setInstallments(navitaireAirline.getInstallmentCode());
		paymentRequestData.setPaymentText(StringUtils.EMPTY);
		paymentRequestData.setDeposit(false);
		paymentRequestData.setExpiration(getExpirationCalendar());
		paymentRequestData.setPaymentFields(new ArrayOfPaymentField());
		paymentRequestData.setPaymentAddresses(new ArrayOfPaymentAddress());
		paymentRequestData.setAgencyAccount(new AgencyAccount());
		paymentRequestData.setCreditShell(new CreditShell());
		paymentRequestData.setPaymentVoucher(new PaymentVoucher());
		paymentRequestData.setThreeDSecureRequest(new ThreeDSecureRequest());
		paymentRequestData.setMCCRequest(new MCCRequest());
		paymentRequestData.setAuthorizationCode(StringUtils.EMPTY);
		return paymentRequestData;
	}

	private ArrayOfBookingContact getBookingContacts(List<FlightTravellerInfo> travellerInfos, String bookingUserId) {
		ArrayOfBookingContact bookingContact = new ArrayOfBookingContact();
		ClientGeneralInfo companyInformation = ServiceCommunicatorHelper.getClientInfo();
		AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(bookingUserId, companyInformation);
		FlightTravellerInfo travellerInfo =
				AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).get(0);
		BookingContact contact = new BookingContact();
		contact.setState(MessageState.New);
		contact.setTypeCode(navitaireAirline.getContactTypeCode());
		contact.setEmailAddress(AirSupplierUtils.getEmailId(deliveryInfo));
		contact.setHomePhone(AirSupplierUtils.getContactNumberWithCountryCode(deliveryInfo));
		contact.setWorkPhone(AirSupplierUtils.getContactNumberWithCountryCode(deliveryInfo));
		contact.setSourceOrganization(configuration.getSupplierCredential().getUserName());
		contact.setNotificationPreference(NotificationPreference.None);
		contact.setCountryCode(companyInformation.getNationality());
		contact.setFax(companyInformation.getFax());
		contact.setCompanyName(NavitaireUtils.getCompanyName(bookingUser, companyInformation));
		contact.setPostalCode(contactAddress.getPincode());
		contact.setCustomerNumber(StringUtils.EMPTY);
		contact.setNames(navitaireAirline.getBookingPaxName(travellerInfo));
		contact.setCultureCode(navitaireAirline.getCultureCode());
		contact.setAddressLine1(NavitaireUtils.getAddress(contactAddress.getAddress()));
		contact.setCity(NavitaireUtils.getCity(contactAddress.getCityInfo().getName()));
		contact.setDistributionOption(DistributionOption.Email);
		navitaireAirline.setOtherDetails(contact, companyInformation);
		bookingContact.addBookingContact(contact);
		return bookingContact;
	}


	public void sendGSTAndContactInfo(GstInfo gstInfo, String bookingUserId) {
		try {
			UpdateContactsRequest contactRequest = new UpdateContactsRequest();
			contactRequest.setUpdateContactsRequestData(getUpdateContactAndGSTdata(gstInfo, bookingUserId));
			listener.setType("5-U-GST&CONT");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			UpdateContactsResponse contactResponse = bookingBinding.updateContacts(contactRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			isAnySellError(contactResponse.getBookingUpdateResponseData(), true);
			if (contactResponse.isBookingUpdateResponseDataSpecified()) {
				double newFare = getTotalSellfare(contactResponse.getBookingUpdateResponseData().getSuccess());
				log.info("Sell GST new fare {} for booking id {}", newFare, bookingId);
			}
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_UpdateContacts_APIWarningFaultFault_FaultMessage apiError) {
			throw new SupplierUnHandledFaultException(apiError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	/**
	 * GST Number - Should Be in valid Format GST Company Name -Should not be empty Mandatory Field = GST Number &
	 * Company name
	 */
	private UpdateContactsRequestData getUpdateContactAndGSTdata(GstInfo gstInfo, String bookingUserId) {
		UpdateContactsRequestData contactData = new UpdateContactsRequestData();
		ArrayOfBookingContact bookingContact = new ArrayOfBookingContact();
		int index = 0;
		BookingContact[] contact = new BookingContact[1];
		if (gstInfo != null && StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			contact = new BookingContact[2];
			contact[index] = new BookingContact();
			contact[index] = getGSTContactInfo(gstInfo);
			index++;
		}

		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		travellerInfos.addAll(adults);
		contact[index] = new BookingContact();
		ArrayOfBookingContact bookingContact1 = getBookingContacts(travellerInfos, bookingUserId);
		if (bookingContact1 != null && ArrayUtils.isNotEmpty(bookingContact1.getBookingContact())) {
			contact[index] = bookingContact1.getBookingContact()[0];
		}
		bookingContact.setBookingContact(contact);
		contactData.setBookingContactList(bookingContact);
		return contactData;
	}

	public BookingContact getGSTContactInfo(GstInfo gstInfo) {
		BookingContact contact = new BookingContact();
		String gstCompanyName =
				NavitaireUtils.getValidCompanyName(BaseUtils.getCleanGstName(gstInfo.getRegisteredName()));
		contact.setState(MessageState.New);
		contact.setTypeCode(navitaireAirline.getGSTTypeCode());
		contact.setNames(null);
		contact.setEmailAddress(AirSupplierUtils.removeIllegalCharacters(gstInfo.getEmail()));
		contact.setSourceOrganization(StringUtils.EMPTY);
		contact.setNotificationPreference(NotificationPreference.None);
		contact.setCountryCode(StringUtils.EMPTY);
		contact.setFax(StringUtils.EMPTY);
		contact.setCompanyName(gstCompanyName);
		contact.setProvinceState(gstInfo.getState());
		contact.setPostalCode(gstInfo.getPincode());
		contact.setCustomerNumber(gstInfo.getGstNumber());
		contact.setDistributionOption(DistributionOption.None);
		if (navitaireAirline.isAncillaryGSTRequired()) {
			contact.setCultureCode(navitaireAirline.getCultureCode());
			contact.setWorkPhone(AirSupplierUtils.getContactNumberWithCountryCode(gstInfo.getMobile()));
			contact.setHomePhone(AirSupplierUtils.getContactNumberWithCountryCode(gstInfo.getMobile()));
		}
		return contact;
	}

	public double getBookingState() {
		GetBookingFromStateRequest bookingRequest = new GetBookingFromStateRequest();
		try {
			listener.setType("6-B-State");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetBookingFromStateResponse bookingResponse =
					bookingBinding.getBookingFromState(bookingRequest, getContractVersion(), getExceptionStackTrace(),
							getMessageContractversion(), getSignature(), getSourceId());
			log.info("Get Booking State {} for booking Id {}",
					bookingResponse.getBookingData().getNumericRecordLocator(), bookingId);
			isTicketNumberRequired = checkIsETicketRequired(bookingResponse);
			return bookingResponse.getBookingData().getBookingSum().getTotalCost().doubleValue();
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (
				com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIWarningFaultFault_FaultMessage apiError) {
			throw new SupplierUnHandledFaultException(apiError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private boolean checkIsETicketRequired(GetBookingFromStateResponse bookingResponse) {
		AtomicBoolean isEticket = new AtomicBoolean();
		if (bookingResponse != null && bookingResponse.getBookingData().getJourneys() != null) {
			Journey[] journeys = bookingResponse.getBookingData().getJourneys().getJourney();
			Arrays.stream(journeys).forEach(journey -> {
				Segment[] segments = journey.getSegments().getSegment();
				Arrays.stream(segments).forEach(segment -> {
					Leg[] legs = segment.getLegs().getLeg();
					Arrays.stream(legs).forEach(leg -> {
						if (!isEticket.get() && leg != null && leg.getLegInfo() != null) {
							/*
							 * if (!isEticket.get()) { // As Still not design completed on Navitaire
							 * isEticket.set(leg.getLegInfo().getETicket()); }
							 */
							/**
							 * @implSpec : there are case some time code flight pnr should be there eticket is also
							 *           false , to handle that scenario, have to hard code in system
							 */
							if (!isEticket.get() && navitaireAirline.codeShareFlights()
									.contains(leg.getLegInfo().getOperatingCarrier())) {
								isEticket.set(true);
							}
						}
					});
				});
			});
		}
		return isEticket.get();
	}

	public double applyPromoCode(String promoCode) {
		ApplyPromotionRequest promotionRequest = new ApplyPromotionRequest();
		double sellfare = 0;
		try {
			listener.setType("B-Promo");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			promotionRequest.setApplyPromotionReqData(getPromotionRequest(promoCode));
			ApplyPromotionResponse promoCodeResponse =
					bookingBinding.applyPromotion(promotionRequest, getContractVersion(), getExceptionStackTrace(),
							getMessageContractversion(), getSignature(), getSourceId());
			boolean isError = isAnySellError(promoCodeResponse.getBookingUpdateResponseData(), false);
			if (!isError) {
				sellfare = getTotalSellfare(promoCodeResponse.getBookingUpdateResponseData().getSuccess());
				totalFare = sellfare;
				sellfare = getAmountBasedOnCurrency(new BigDecimal(sellfare), getCurrencyCode());
				String message = StringUtils.join("Promo Code Applied ", promoCode);
				log.info("{} and amount to be paid {} sell fare {} for BookingId {}", message, totalFare, sellfare,
						bookingId);
				return sellfare;
			}
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_ApplyPromotion_APIWarningFaultFault_FaultMessage apiError) {
			throw new SupplierUnHandledFaultException(apiError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return 0.0;
	}

	private ApplyPromotionRequestData getPromotionRequest(String promoCode) {
		ApplyPromotionRequestData requestData = new ApplyPromotionRequestData();
		requestData.setPromotionCode(promoCode);
		requestData.setSourcePointOfSale(getPOS());
		return requestData;
	}

	public double sendSeatSellRequest(List<SegmentInfo> segmentInfos) {
		AssignSeatsResponse seatResp = null;
		double totalSellFare = 0;
		boolean isFailed = false;
		try {
			AssignSeatsRequest seatsRequest = new AssignSeatsRequest();
			SeatSellRequest seatSellRequest = new SeatSellRequest();
			seatSellRequest.setSeatAssignmentMode(SeatAssignmentMode.PreSeatAssignment);
			seatSellRequest.setSegmentSeatRequests(getSegmentsSeatRequest(segmentInfos));
			seatSellRequest.setBlockType(UnitHoldType.Session);
			seatSellRequest.setSameSeatRequiredOnThruLegs(false);
			seatSellRequest.setAssignNoSeatIfAlreadyTaken(false);
			seatSellRequest.setWaiveFee(false);
			seatSellRequest.setIgnoreSeatSSRs(false);
			seatSellRequest.setIncludeSeatData(true);
			seatsRequest.setSellSeatRequest(seatSellRequest);
			listener.setType("R-ASSIGNSEAT");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			seatResp = bookingBinding.assignSeats(seatsRequest, getContractVersion(), getExceptionStackTrace(),
					getMessageContractversion(), getSignature(), getSourceId());
			isFailed = isAnySellError(seatResp.getBookingUpdateResponseData(), true);
		} catch (java.rmi.RemoteException e) {
			throw new SupplierRemoteException(e);
		} catch (com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_AssignSeats_APIWarningFaultFault_FaultMessage apiError) {
			throw new SupplierUnHandledFaultException(apiError.getMessage());
		} finally {
			if (isFailed)
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		if (seatResp != null && !isFailed) {
			totalSellFare = getTotalSellfare(seatResp.getBookingUpdateResponseData().getSuccess());
			totalFare = totalSellFare;
			totalSellFare = getAmountBasedOnCurrency(new BigDecimal(totalSellFare), getCurrencyCode());
			log.info("Sell Seat new fare {} Total Fare {} for booking id {}", totalSellFare, totalFare, bookingId);
		}
		return totalSellFare;
	}

	public ArrayOfSegmentSeatRequest getSegmentsSeatRequest(List<SegmentInfo> segmentInfos) {
		ArrayOfSegmentSeatRequest segmentSeatRequest = new ArrayOfSegmentSeatRequest();
		segmentInfos.forEach(segmentInfo -> {
			addPaxSeatSSR(segmentInfo, segmentSeatRequest);
		});
		return segmentSeatRequest;
	}

	public void addPaxSeatSSR(SegmentInfo segmentInfo, ArrayOfSegmentSeatRequest segmentSeatRequest) {
		// Add Seat if Any to travaller
		SegmentBookingRelatedInfo relatedInfo = segmentInfo.getBookingRelatedInfo();
		List<FlightTravellerInfo> flightTravellerInfos = relatedInfo.getTravellerInfo();
		for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
			SSRInformation seatInformation = flightTravellerInfos.get(paxIndex).getSsrSeatInfo();
			if (Objects.nonNull(seatInformation)) {
				long[] paxIDs = new long[1];
				short[] paxNums = new short[1];
				paxIDs[0] = 0;
				paxNums[0] = (short) (paxIndex);
				ArrayOflong longPax = new ArrayOflong();
				ArrayOfshort paxShort = new ArrayOfshort();
				longPax.set_long(paxIDs);
				paxShort.set_short(paxNums);
				SegmentSeatRequest seatRequest = new SegmentSeatRequest();
				seatRequest.setCompartmentDesignator(StringUtils.EMPTY);
				seatRequest.setPassengerSeatPreferences(null);
				seatRequest.setRequestedSSRs(new ArrayOfstring());
				seatRequest.setArrivalStation(segmentInfo.getArrivalAirportInfo().getCode());
				seatRequest.setDepartureStation(segmentInfo.getDepartAirportInfo().getCode());
				seatRequest.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
				FlightDesignator designator = new FlightDesignator();
				designator.setCarrierCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
				designator.setFlightNumber(StringUtils.leftPad(segmentInfo.getFlightNumber(), 4, " "));
				seatRequest.setFlightDesignator(designator);
				seatRequest.setPassengerIDs(longPax);
				seatRequest.setPassengerNumbers(paxShort);
				seatRequest.setUnitDesignator(seatInformation.getCode());
				segmentSeatRequest.addSegmentSeatRequest(seatRequest);
			}
		}
	}

	public boolean updateTicketNumber(Booking retrieveBookingRS, List<SegmentInfo> segmentInfos) {
		boolean isSuccess = false;
		try {
			Journey[] journeys = retrieveBookingRS.getJourneys().getJourney();
			Arrays.stream(journeys).forEach(journey -> {
				Segment[] segments = journey.getSegments().getSegment();
				Arrays.stream(segments).forEach(segment -> {
					String actionCode = segment.getActionStatusCode();
					if (StringUtils.isNotBlank(actionCode)) {
						SegmentInfo segmentInfo = getSegmentInfo(segmentInfos, segment.getDepartureStation(),
								segment.getArrivalStation(), segment.getSTD());
						if (segmentInfo != null && segment.getPaxTickets() != null
								&& ArrayUtils.isNotEmpty(segment.getPaxTickets().getPaxTicket())) {
							List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
							travellerInfos.addAll(adults);
							travellerInfos.addAll(child);
							List<FlightTravellerInfo> infantTravellerInfos = AirUtils.getParticularPaxTravellerInfo(
									segmentInfo.getBookingRelatedInfo().getTravellerInfo(), PaxType.INFANT);
							PaxTicket[] paxTickets = segment.getPaxTickets().getPaxTicket();
							AtomicInteger paxIndex = new AtomicInteger(0);
							Arrays.stream(paxTickets).forEach(paxTicket -> {
								if (StringUtils.isNotBlank(paxTicket.getTicketNumber())) {
									FlightTravellerInfo travellerInfo =
											travellerInfos.get(paxTicket.getPassengerNumber());
									travellerInfo.setTicketNumber(paxTicket.getTicketNumber());
									if (paxTicket.isInfantTicketNumberSpecified()) {
										infantTravellerInfos.get(paxIndex.get())
												.setTicketNumber(paxTicket.getInfantTicketNumber());
									}
									paxIndex.getAndIncrement();
								}
							});
						}
					}
				});
			});
			isSuccess = BookingUtils.hasTicketNumberInAllSegments(segmentInfos);
		} catch (Exception e) {
			log.error("Unable to Update Ticket Number for booking {}", bookingId, e);
		} finally {
			if (!isSuccess) {
				criticalMessageLogger.add("Code Share Flight,Ticket is Not Updated");
			}
		}
		return isSuccess;
	}

	public void updateSupplierReference(Booking retrieveBookingRS, List<SegmentInfo> segmentInfos) {
		if (retrieveBookingRS != null && retrieveBookingRS.getRecordLocators() != null
				&& ArrayUtils.isNotEmpty(retrieveBookingRS.getRecordLocators().getRecordLocator())) {
			Arrays.stream(retrieveBookingRS.getRecordLocators().getRecordLocator()).forEach(recordLocator -> {
				String operatingAirline = recordLocator.getSystemCode();
				String supplierReference = recordLocator.getRecordCode();
				segmentInfos.forEach(segmentInfo -> {
					if (segmentInfo.getOperatedByAirlineInfo() != null
							&& StringUtils.isNotBlank(segmentInfo.getOperatedByAirlineInfo().getCode())
							&& operatingAirline.equalsIgnoreCase(segmentInfo.getOperatedByAirlineInfo().getCode())) {
						BookingUtils.updateSupplierBookingId(Arrays.asList(segmentInfo), supplierReference);
					}
				});
			});
		}
	}

	private SegmentInfo getSegmentInfo(List<SegmentInfo> segmentInfos, String departureStation, String arrivalStation,
			Calendar std) {
		LocalDateTime departureDate = TgsDateUtils.calenderToLocalDateTime(std);
		Optional<SegmentInfo> segmentInfoOptional = Optional.ofNullable(segmentInfos.stream()
				.filter(segmentInfo1 -> segmentInfo1.getDepartureAirportCode().equals(departureStation)
						&& segmentInfo1.getArrivalAirportCode().equals(arrivalStation)
						&& segmentInfo1.getDepartTime().equals(departureDate))
				.findFirst()).get();
		return segmentInfoOptional.get();
	}

	// For Confirmation Booking Only
	public boolean isBookingSuccess(BookingInfo bookingInfo) {
		return bookingInfo != null
				&& BookingStatus._Confirmed.equalsIgnoreCase(bookingInfo.getBookingStatus().getValue())
				&& PaidStatus._PaidInFull.equalsIgnoreCase(bookingInfo.getPaidStatus().getValue());
	}

}
