package com.tgs.services.fms.sources.navitaireV4_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfBookingServiceCharge;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfLeg;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerTravelDocument;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaxSeat;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPayment;
import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingContact;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingServiceCharge;
import com.navitaire.schemas.webservices.datacontracts.booking.Fare;
import com.navitaire.schemas.webservices.datacontracts.booking.Journey;
import com.navitaire.schemas.webservices.datacontracts.booking.LegInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.Passenger;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerInfant;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerTravelDocument;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxFare;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.Payment;
import com.navitaire.schemas.webservices.datacontracts.booking.Segment;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.BookingStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.LiftStatus;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.PNRCreditInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.utils.exception.air.NoPNRFoundException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class NavitaireBookingRetrieveManager extends NavitaireServiceManager {

	protected String carrierCode;

	protected Calendar holdTimeLimit;

	protected static final String PNR_NA = "No PNR/Journey Available ";

	public static final String JOURNEY_MISMATCH = "Journey Mismatch Either due to Departure Date or Sector Change";

	protected static final String INVALID_PNR = "Invalid PNR ";

	public AirImportPnrBooking retrieveBooking(String pnr) {
		AirImportPnrBooking pnrBooking = null;
		List<TripInfo> tripInfos = null;
		AirItemStatus itemStatus = null;
		GstInfo gstInfo = null;
		DeliveryInfo deliveryInfo = null;
		try {
			Booking booking = getBookingResponse(pnr);
			if (isValidPNRBooking(booking, pnr)) {
				tripInfos = fetchTripDetails(booking);
				itemStatus = fetchItemStatus(booking.getBookingInfo());
				updateHoldTimeLimit(booking, tripInfos, itemStatus);
				gstInfo = fetchGSTInfo(booking);
				deliveryInfo = fetchDeliveryInfo(booking);
			}
		} finally {
			if (CollectionUtils.isNotEmpty(tripInfos)) {
				resetTravellerId(tripInfos);
				pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).gstInfo(gstInfo)
						.deliveryInfo(deliveryInfo).itemStatus(itemStatus).build();
			}
		}
		return pnrBooking;
	}

	private AirItemStatus fetchItemStatus(BookingInfo bookingInfo) {
		AirItemStatus itemStatus = AirItemStatus.IN_PROGRESS;
		if (BookingStatus.Confirmed.equals(bookingInfo.getBookingStatus())) {
			itemStatus = AirItemStatus.SUCCESS;
		} else if (BookingStatus.Hold.equals(bookingInfo.getBookingStatus())) {
			itemStatus = AirItemStatus.ON_HOLD;
		} else if (BookingStatus.HoldCanceled.equals(bookingInfo.getBookingStatus())) {
			itemStatus = AirItemStatus.ABORTED;
		}
		return itemStatus;
	}

	private GstInfo fetchGSTInfo(Booking booking) {
		GstInfo gstInfo = null;
		BookingContact gstContactInfo = getBookingContact(booking, navitaireAirline.getGSTTypeCode());
		if (Objects.nonNull(gstContactInfo)) {
			gstInfo = new GstInfo();
			gstInfo.setGstNumber(gstContactInfo.getCustomerNumber());
			gstInfo.setEmail(gstContactInfo.getEmailAddress());
			gstInfo.setRegisteredName(gstContactInfo.getCompanyName());
			gstInfo.setAddress(StringUtils.join(gstContactInfo.getAddressLine1(), gstContactInfo.getAddressLine2(),
					gstContactInfo.getAddressLine3()));
			gstInfo.setMobile(gstContactInfo.getHomePhone());
			gstInfo.setCityName(gstContactInfo.getCity());
			gstInfo.setPincode(gstContactInfo.getPostalCode());
			gstInfo.setState(gstContactInfo.getProvinceState());
		}
		return gstInfo;
	}

	public BookingContact getBookingContact(Booking booking, String contactTypeCode) {
		BookingContact[] bookingContacts = booking.getBookingContacts().getBookingContact();
		List<BookingContact> contacts = Arrays.stream(bookingContacts).filter(bookingContact -> {
			return (StringUtils.isNotEmpty(bookingContact.getTypeCode())
					&& bookingContact.getTypeCode().equals(contactTypeCode));
		}).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(contacts)) {
			return contacts.get(0);
		}
		return null;
	}

	private DeliveryInfo fetchDeliveryInfo(Booking booking) {
		DeliveryInfo deliveryInfo = null;
		BookingContact bookingContact = getBookingContact(booking, navitaireAirline.getContactTypeCode());
		if (Objects.nonNull(bookingContact)) {
			deliveryInfo = new DeliveryInfo();
			deliveryInfo.setContacts(Arrays.asList(bookingContact.getHomePhone()));
			deliveryInfo.setEmails(Arrays.asList(bookingContact.getEmailAddress()));
		}
		return deliveryInfo;
	}

	private List<TripInfo> fetchTripDetails(Booking bookingDetails) {
		String confirmationCode = bookingDetails.getRecordLocator();
		List<TripInfo> tripInfos = getTripDetails(bookingDetails);
		List<FlightTravellerInfo> flightTravellerInfos =
				getTripTravaller(bookingDetails.getPassengers().getPassenger(), confirmationCode, tripInfos);
		TripPriceEngine.shiftFareDetailsToFlightTraveller(tripInfos, flightTravellerInfos);
		NavitaireUtils.setTotalFareOnTripInfo(tripInfos);
		removeExtraSSRFromPax(tripInfos);
		setSeatInfosToTravellers(bookingDetails, tripInfos, confirmationCode);
		unsetSSRinfos(tripInfos);
		setTravellerBoardingStatus(bookingDetails.getJourneys().getJourney(), tripInfos);
		return tripInfos;
	}

	private void setTravellerBoardingStatus(Journey[] journeys, List<TripInfo> tripInfos) {
		for (Journey journey : journeys) {
			Segment[] segments = journey.getSegments().getSegment();
			for (Segment segment : segments) {
				SegmentInfo segmentInfo = NavitaireUtils.findSegmentFromTripInfos(tripInfos, segment);
				if (segmentInfo != null) {
					PaxSegment[] paxSegments = segment.getPaxSegments().getPaxSegment();
					for (PaxSegment paxSegment : paxSegments) {
						FlightTravellerInfo travellerInfo = segmentInfo.getBookingRelatedInfo().getTravellerInfo()
								.get(paxSegment.getPassengerNumber());
						LiftStatus liftStatus = paxSegment.getLiftStatus();
						if (liftStatus.equals(LiftStatus.NoShow)) {
							travellerInfo.setStatus(TravellerStatus.NO_SHOW);
						} else if (liftStatus.equals(LiftStatus.Boarded)) {
							travellerInfo.setStatus(TravellerStatus.BOARDED);
						}
					}
				}
			}
		}
	}

	private void setSeatInfosToTravellers(Booking bookingDetails, List<TripInfo> tripInfos, String confirmationCode) {
		try {
			Journey[] journeys = bookingDetails.getJourneys().getJourney();
			AtomicInteger tripIndex = new AtomicInteger(0);
			Arrays.stream(journeys).forEach(journey -> {
				AtomicInteger segmentIndex = new AtomicInteger(0);
				Segment[] segments = journey.getSegments().getSegment();
				Arrays.stream(segments).forEach(segment -> {
					SegmentInfo segmentInfo = tripInfos.get(tripIndex.get()).getSegmentInfos().get(segmentIndex.get());
					ArrayOfPaxSeat paxSeat = segment.getPaxSeats();
					if (paxSeat != null && ArrayUtils.isNotEmpty(paxSeat.getPaxSeat())) {
						List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
						Arrays.stream(paxSeat.getPaxSeat()).forEach(paxSeat1 -> {
							if (paxSeat1.getDepartureStation().equalsIgnoreCase(segmentInfo.getDepartureAirportCode())
									&& paxSeat1.getArrivalStation()
											.equalsIgnoreCase(segmentInfo.getArrivalAirportCode())) {
								Integer paxID = Integer.valueOf(paxSeat1.getPassengerNumber());
								for (FlightTravellerInfo travellerInfo : travellerInfos) {
									if (travellerInfo.getId() != null && travellerInfo.getPaxType() != PaxType.INFANT
											&& travellerInfo.getSsrSeatInfo() != null
											&& travellerInfo.getId().intValue() == paxID) {
										travellerInfo.getSsrSeatInfo().setCode(paxSeat1.getUnitDesignator());
										travellerInfo.getSsrSeatInfo().setDesc(paxSeat1.getUnitDesignator());
									}
								}
							}
						});
					}
					segmentIndex.getAndIncrement();
				});
				tripIndex.getAndIncrement();
			});
		} catch (Exception e) {
			log.error("Unable to add seat to pax {}", confirmationCode, e);
		}
	}

	private void unsetSSRinfos(List<TripInfo> tripInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getBookingRelatedInfo() != null
						&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(travellerInfo -> {
						travellerInfo.setSsrSeatInfos(null);
						travellerInfo.setSsrMealInfos(null);
						travellerInfo.setSsrBaggageInfos(null);
					});
				}
			});
		});
	}

	private void removeExtraSSRFromPax(List<TripInfo> tripInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				String departureDate = tripInfo.getDepartureTime().toLocalDate().toString().replaceAll("-", "");
				List<FlightTravellerInfo> flightTravellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				flightTravellerInfos.forEach(travellerInfo -> {
					if (CollectionUtils.isNotEmpty(travellerInfo.getSsrBaggageInfos())) {
						List<SSRInformation> filteredBaggage =
								travellerInfo.getSsrBaggageInfos().stream().filter(baggageInfo -> {
									return baggageInfo.getKey().contains(departureDate);
								}).collect(Collectors.toList());
						if (CollectionUtils.isNotEmpty(filteredBaggage)) {
							filteredBaggage.get(0).setKey(null);
							travellerInfo.setSsrBaggageInfo(filteredBaggage.get(0));
						}
					}
					if (CollectionUtils.isNotEmpty(travellerInfo.getSsrMealInfos())) {
						List<SSRInformation> filteredMeal =
								travellerInfo.getSsrMealInfos().stream().filter(mealInfo -> {
									return mealInfo.getKey().contains(departureDate);
								}).collect(Collectors.toList());
						if (CollectionUtils.isNotEmpty(filteredMeal)) {
							filteredMeal.get(0).setKey(null);
							travellerInfo.setSsrMealInfo(filteredMeal.get(0));
						}
					}
				});
			});
		});
	}

	public List<FlightTravellerInfo> getTripTravaller(Passenger[] passengers, String confirmationCode,
			List<TripInfo> tripInfos) {
		List<FlightTravellerInfo> travellerInfos = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(passengers)) {
			int paxIndex = 0;
			for (Passenger passenger : passengers) {
				FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
				travellerInfo.setPnr(confirmationCode);
				travellerInfo.setId(Long.valueOf(paxIndex));
				travellerInfo.setFirstName(passenger.getNames().getBookingName()[0].getFirstName());
				travellerInfo.setLastName(passenger.getNames().getBookingName()[0].getLastName());
				travellerInfo.setTitle(passenger.getNames().getBookingName()[0].getTitle());
				travellerInfo.setPaxType(getPaxType(passenger.getPassengerTypeInfo().getPaxType()));
				travellerInfo.setDob(NavitaireUtils.getRetreieveDOB(passenger));
				setTravelDocumentDetails(passenger, travellerInfo);
				if (passenger.getPassengerFees() != null
						&& ArrayUtils.isNotEmpty(passenger.getPassengerFees().getPassengerFee())) {
					// SSR Functionality
					PassengerFee[] passengerFees = passenger.getPassengerFees().getPassengerFee();
					Map<SSRType, List<? extends SSRInformation>> preSSR =
							AirUtils.getPreSSR(configuration.getBasicInfo(), getAirline(tripInfos.get(0)), bookingUser);
					Arrays.stream(passengerFees).forEach(passengerFee -> {
						SSRInformation baggageSSRInfo =
								AirUtils.getSSRInfo(preSSR.get(SSRType.BAGGAGE), passengerFee.getSSRCode());
						SSRInformation mealSSRInfo =
								AirUtils.getSSRInfo(preSSR.get(SSRType.MEAL), passengerFee.getSSRCode());
						SSRInformation seatSSrInfo =
								AirUtils.getSSRInfo(preSSR.get(SSRType.SEAT), passengerFee.getFeeCode());
						BaggageSSRInformation baggInformation = new BaggageSSRInformation();
						MealSSRInformation mealInformation = new MealSSRInformation();
						SeatSSRInformation seatInformation = new SeatSSRInformation();
						if (baggageSSRInfo != null) {
							baggInformation.setAmount(getSSRAmount(passengerFee.getServiceCharges()));
							baggInformation.setDesc(baggageSSRInfo.getDesc());
							baggInformation.setCode(passengerFee.getSSRCode());
							baggInformation.setKey(passengerFee.getFlightReference());
							travellerInfo.getSsrBaggageInfos().add(baggInformation);
						} else if (mealSSRInfo != null) {
							mealInformation.setCode(passengerFee.getSSRCode());
							mealInformation.setDesc(mealSSRInfo.getDesc());
							mealInformation.setAmount(getSSRAmount(passengerFee.getServiceCharges()));
							mealInformation.setKey(passengerFee.getFlightReference());
							travellerInfo.getSsrMealInfos().add(mealInformation);
						} else if (seatSSrInfo != null) {
							seatInformation.setAmount(getSSRAmount(passengerFee.getServiceCharges()));
							seatInformation.setKey(passengerFee.getFlightReference());
							travellerInfo.getSsrSeatInfos().add(seatInformation);
						}
					});
				}
				if (Objects.nonNull(passenger.getInfant())) {
					FlightTravellerInfo infantTraveller = new FlightTravellerInfo();
					infantTraveller = parsePaxInfo(passenger.getInfant(), confirmationCode);
					setTravelDocumentDetails(passenger, infantTraveller);
					travellerInfos.add(infantTraveller);
					tripInfos.forEach(tripInfo -> {
						tripInfo.getSegmentInfos().forEach(segmentInfo -> {
							if (navitaireAirline.isInfantFareOnJourneyWise()
									&& navitaireAirline.isInfantFareOnSegmentWise(segmentInfo)) {
								String tripKey = StringUtils.join(tripInfo.getDepartureAirportCode(),
										tripInfo.getArrivalAirportCode());
								FareDetail infantFare = getInfantFareFromAdult(passenger, tripKey);
								Map<PaxType, FareDetail> infantPax = new ConcurrentHashMap<PaxType, FareDetail>();
								infantPax.put(PaxType.INFANT, infantFare);
								segmentInfo.getPriceInfo(0).getFareDetails().put(PaxType.INFANT, infantFare);
							} else {
								String segmentKey = StringUtils.join(segmentInfo.getDepartureAirportCode(),
										segmentInfo.getArrivalAirportCode());
								FareDetail infantFare = getInfantFareFromAdult(passenger, segmentKey);
								Map<PaxType, FareDetail> infantPax = new ConcurrentHashMap<PaxType, FareDetail>();
								infantPax.put(PaxType.INFANT, infantFare);
								segmentInfo.getPriceInfo(0).getFareDetails().put(PaxType.INFANT, infantFare);
							}
						});
					});
				}
				travellerInfos.add(travellerInfo);
				paxIndex++;
			}
		}
		return travellerInfos;
	}


	private void setTravelDocumentDetails(Passenger passenger, FlightTravellerInfo travellerInfo) {
		if (passenger.getPassengerTravelDocuments() != null
				&& ArrayUtils.isNotEmpty(passenger.getPassengerTravelDocuments().getPassengerTravelDocument())) {
			PassengerTravelDocument travelDoc = getFilteredDocument(passenger.getPassengerTravelDocuments(),
					travellerInfo, NavitaireAirline.PASS_DOC_TYPE_CODE);
			if (travelDoc != null) {
				travellerInfo.setPassportNumber(travelDoc.getDocNumber());
				travellerInfo.setPassportNationality(travelDoc.getNationality());
				travellerInfo.setPassportIssueDate(TgsDateUtils.calenderToLocalDate(travelDoc.getIssuedDate()));
				travellerInfo.setExpiryDate(TgsDateUtils.calenderToLocalDate(travelDoc.getExpirationDate()));
				travellerInfo.setPassportNationality(passenger.getPassengerInfo().getNationality());
			}
			PassengerTravelDocument ffTravelDoc = getFilteredDocument(passenger.getPassengerTravelDocuments(),
					travellerInfo, NavitaireAirline.FF_DOC_TYPE_CODE);
			if (ffTravelDoc != null) {
				if (travellerInfo.getFrequentFlierMap() == null
						|| MapUtils.isEmpty(travellerInfo.getFrequentFlierMap())) {
					travellerInfo.setFrequentFlierMap(new HashMap<>());
				}
				travellerInfo.getFrequentFlierMap().put(carrierCode, ffTravelDoc.getDocNumber());
			}
		}
	}

	private PassengerTravelDocument getFilteredDocument(ArrayOfPassengerTravelDocument passengerTravelDocuments,
			FlightTravellerInfo travellerInfo, String doctTypeCode) {
		for (PassengerTravelDocument travelDocument : passengerTravelDocuments.getPassengerTravelDocument()) {
			String passengerName = NavitaireUtils.getPassengerName(travelDocument.getNames().getBookingName()[0]);
			String travellerName = StringUtils.join(travellerInfo.getFirstName(), travellerInfo.getLastName());
			if (StringUtils.isNotEmpty(travelDocument.getDocTypeCode())
					&& travelDocument.getDocTypeCode().equalsIgnoreCase(doctTypeCode)
					&& passengerName.equalsIgnoreCase(travellerName)) {
				return travelDocument;
			}
		}
		return null;
	}

	private FareDetail getInfantFareFromAdult(Passenger pax, String keyToMatch) {
		FareDetail infantFare = new FareDetail();
		if (pax.getPassengerFees() != null && ArrayUtils.isNotEmpty(pax.getPassengerFees().getPassengerFee())) {
			for (PassengerFee paxFee : pax.getPassengerFees().getPassengerFee()) {
				if (paxFee.getFeeCode().equalsIgnoreCase(PAX_TYPE_INFANT)
						&& paxFee.getFlightReference().contains(keyToMatch)) {
					PaxFare paxFare = new PaxFare();
					paxFare.setPaxType(PAX_TYPE_INFANT);
					paxFare.setTicketFareBasisCode("");
					infantFare = parsePaxWiseFareBreakUp(infantFare, paxFare, PAX_TYPE_INFANT, 1,
							paxFee.getServiceCharges().getBookingServiceCharge());
				}
			}
		}
		return infantFare;
	}

	private FlightTravellerInfo parsePaxInfo(PassengerInfant infant, String confirmationCode) {
		FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
		travellerInfo.setFirstName(infant.getNames().getBookingName()[0].getFirstName());
		travellerInfo.setLastName(infant.getNames().getBookingName()[0].getLastName());
		travellerInfo.setTitle(infant.getNames().getBookingName()[0].getTitle());
		travellerInfo.setDob(TgsDateUtils.calenderToLocalDate(infant.getDOB()));
		travellerInfo.setPaxType(PaxType.INFANT);
		travellerInfo.setPnr(confirmationCode);
		return travellerInfo;
	}

	private List<TripInfo> getTripDetails(Booking booking) {
		int journeyIndex = 0;
		List<TripInfo> tripInfos = new ArrayList<>();
		Map<PaxType, Integer> paxCountInBooking = getPaxCountInBooking(booking);
		if (ArrayUtils.isNotEmpty(booking.getJourneys().getJourney())) {
			for (Journey journey : booking.getJourneys().getJourney()) {
				String journeyKey = journey.getJourneySellKey();
				int segmentNum = 0;
				TripInfo tripInfo = new TripInfo();
				for (Segment segment : journey.getSegments().getSegment()) {
					SegmentInfo segmentInfo = getSegmentDetails(segment, journeyKey, segmentNum, paxCountInBooking);
					if (journeyIndex == 1 && segmentNum == 0) {
						segmentInfo.setIsReturnSegment(true);
					}
					tripInfo.getSegmentInfos().add(segmentInfo);
					segmentNum++;
				}
				tripInfo.setConnectionTime();
				tripInfos.add(tripInfo);
				journeyIndex++;
			}
		}
		return tripInfos;
	}

	private Map<PaxType, Integer> getPaxCountInBooking(Booking booking) {
		Map<PaxType, Integer> paxTypes = new HashMap<>();
		if (booking.getPassengers() != null && ArrayUtils.isNotEmpty(booking.getPassengers().getPassenger())) {
			Passenger[] passengers = booking.getPassengers().getPassenger();
			for (Passenger passenger : passengers) {
				PaxType paxType = getPaxType(passenger.getPassengerTypeInfo().getPaxType());
				Integer paxCount = paxTypes.getOrDefault(paxType, 0);
				paxTypes.put(paxType, paxCount + 1);
			}
		}
		return paxTypes;
	}

	private SegmentInfo getSegmentDetails(Segment segment, String journeyKey, int segmentNum,
			Map<PaxType, Integer> paxCountInBooking) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setSegmentNum(segmentNum);
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirportInfo(segment.getDepartureStation()));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirportInfo(segment.getArrivalStation()));
		segmentInfo.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(segment.getSTA()));
		segmentInfo.setDepartTime(TgsDateUtils.calenderToLocalDateTime(segment.getSTD()));
		segmentInfo.setFlightDesignator(getFlightDesignator(segment.getFlightDesignator()));
		segmentInfo.setDuration(segmentInfo.calculateDuration());
		segmentInfo.setIsReturnSegment(false);
		segmentInfo.setStopOverAirports(getStopOverAirports(segment.getLegs(), segmentInfo));
		segmentInfo.setStops(CollectionUtils.size(segmentInfo.getStopOverAirports()));
		carrierCode = segmentInfo.getAirlineCode(false);
		AirlineInfo operatingAirline =
				getOperatingCarrier(segment.getLegs().getLeg(), segmentInfo.getFlightDesignator());
		if (operatingAirline != null
				&& !operatingAirline.getCode().equalsIgnoreCase(segmentInfo.getFlightDesignator().getAirlineCode())) {
			segmentInfo.setOperatedByAirlineInfo(operatingAirline);
		}
		if (org.apache.commons.lang3.ArrayUtils.isNotEmpty(segment.getLegs().getLeg())) {
			LegInfo legInfo = segment.getLegs().getLeg()[0].getLegInfo();
			segmentInfo.getDepartAirportInfo().setTerminal(AirUtils.getTerminalInfo(legInfo.getDepartureTerminal()));
			segmentInfo.getArrivalAirportInfo().setTerminal(AirUtils.getTerminalInfo(legInfo.getArrivalTerminal()));
			if (StringUtils.isNotBlank(legInfo.getEquipmentType())) {
				segmentInfo.getFlightDesignator().setEquipType(legInfo.getEquipmentType());
			}
		}
		List<PriceInfo> priceInfos = new ArrayList<PriceInfo>();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		if (ArrayUtils.isNotEmpty(segment.getFares().getFare())) {
			Fare[] fares = segment.getFares().getFare();
			setNewFareComponent(priceInfo, fares[0].getPaxFares().getPaxFare(), fares[0], paxCountInBooking);
			priceInfo.getMiscInfo().setJourneyKey(journeyKey);
		}
		priceInfos.add(priceInfo);
		segmentInfo.setPriceInfoList(priceInfos);
		return segmentInfo;
	}

	private void updateHoldTimeLimit(Booking booking, List<TripInfo> tripInfos, AirItemStatus itemStatus) {
		if (AirItemStatus.ON_HOLD.equals(itemStatus) && booking.getBookingHold() != null
				&& booking.getBookingHold().getHoldDateTime() != null) {
			holdTimeLimit = booking.getBookingHold().getHoldDateTime();
		}
	}

	private List<AirportInfo> getStopOverAirports(ArrayOfLeg legs, SegmentInfo segmentInfo) {
		List<AirportInfo> stops = new ArrayList<>();
		if (legs != null && ArrayUtils.isNotEmpty(legs.getLeg())) {
			Arrays.stream(legs.getLeg()).forEach(leg -> {
				if (!segmentInfo.getArrivalAirportCode().equalsIgnoreCase(leg.getArrivalStation())) {
					stops.add(AirportHelper.getAirportInfo(leg.getArrivalStation().trim()));
				}
			});
		}
		return stops;
	}

	private PriceInfo setNewFareComponent(PriceInfo priceInfo, PaxFare[] paxFares, Fare fare,
			Map<PaxType, Integer> paxCountInBooking) {
		Map<PaxType, FareDetail> fareDetails = new ConcurrentHashMap<>();
		FareDetail fareDetail = new FareDetail();
		PaxFare paxFare = getPaxFare(paxFares, PAX_TYPE_ADULT);
		if (paxCountInBooking.getOrDefault(PaxType.ADULT, 0) > 0) {
			// Adult Fare
			if (Objects.nonNull(paxFare)) {
				fareDetail = parsePaxWiseFareBreakUp(fareDetail, paxFare, PAX_TYPE_ADULT, 1,
						paxFare.getServiceCharges().getBookingServiceCharge());
			}
			fareDetail = setCommonPaxFareType(fareDetail, fare);
			fareDetails.put(PaxType.ADULT, fareDetail);
		}
		if (paxCountInBooking.getOrDefault(PaxType.CHILD, 0) > 0) {
			// Child Fare
			paxFare = getPaxFare(paxFares, PAX_TYPE_CHILD);
			fareDetail = new FareDetail();
			if (Objects.nonNull(getPaxFare(paxFares, PAX_TYPE_CHILD))) {
				fareDetail = parsePaxWiseFareBreakUp(fareDetail, paxFare, PAX_TYPE_CHILD, 1,
						paxFare.getServiceCharges().getBookingServiceCharge());
			}
			fareDetail = setCommonPaxFareType(fareDetail, fare);
			fareDetails.put(PaxType.CHILD, fareDetail);
		}
		priceInfo.setFareDetails(fareDetails);
		PriceMiscInfo miscInfo = PriceMiscInfo.builder().fareKey(fare.getFareSellKey()).build();
		priceInfo.setMiscInfo(miscInfo);
		navitaireAirline.processPriceInfo(fare.getProductClass(), priceInfo, null, searchQuery);
		return priceInfo;
	}

	public FareDetail setCommonPaxFareType(FareDetail paxWiseFare, Fare fare) {
		paxWiseFare.setFareBasis(fare.getFareBasisCode());
		paxWiseFare.setClassOfBooking(fare.getProductClass());
		paxWiseFare.setCabinClass(CabinClass.ECONOMY);
		paxWiseFare.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
		return paxWiseFare;
	}


	// @param serviceCharges *includedtax where amount already added on FarePrice with tax
	public double getSSRAmount(ArrayOfBookingServiceCharge serviceCharges) {
		double amount = 0;
		if (serviceCharges != null && ArrayUtils.isNotEmpty(serviceCharges.getBookingServiceCharge())) {
			for (BookingServiceCharge serviceCharge : serviceCharges.getBookingServiceCharge()) {
				if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
					if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
							&& serviceCharge.getAmount().doubleValue() < 0) {
						serviceCharge.setAmount(serviceCharge.getAmount().abs());
					}
					amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
				}
			}
		}
		return amount;
	}


	public boolean isCancelled(Booking bookingResponse, String pnr) {
		BookingStatus bookingStatus = bookingResponse.getBookingInfo().getBookingStatus();
		if (!isCancelled(bookingStatus) && isValidPNRBooking(bookingResponse, pnr)) {
			log.debug("Check Booking status for {} Booking Segments {} ", bookingId, bookingSegments);
			Journey[] journeys = bookingResponse.getJourneys().getJourney();
			List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(bookingSegments.getCancelSegments());
			for (TripInfo tripInfo : tripInfos) {
				String tripKey = tripInfo.getDepartureAirportCode() + "_" + tripInfo.getArrivalAirportCode() + "_"
						+ tripInfo.getDepartureTime().toLocalDate().toString();
				if (journeys != null && journeys.length != 0) {
					for (Journey journey : journeys) {
						String departureCode = journey.getSegments().getSegment()[0].getDepartureStation();
						String arrivalAirportCode =
								journey.getSegments().getSegment()[journey.getSegments().getSegment().length - 1]
										.getArrivalStation();
						String departureDate = TgsDateUtils
								.calenderToLocalDate(journey.getSegments().getSegment()[0].getSTD()).toString();
						String journeyKey = departureCode + "_" + arrivalAirportCode + "_" + departureDate;
						if (tripKey.equalsIgnoreCase(journeyKey)) {
							return false;
						}
					}
				}
			}
		}
		return true;
	}

	public boolean isValidPNRBooking(Booking booking, String pnr) {
		if (booking != null && StringUtils.isNotEmpty(booking.getRecordLocator())
				&& (booking.getJourneys() == null || ArrayUtils.isEmpty(booking.getJourneys().getJourney()))) {
			logCriticalMessage(PNR_NA);
			throw new NoPNRFoundException(PNR_NA + booking.getRecordLocator());
		} else if (booking == null || StringUtils.isEmpty(booking.getRecordLocator())) {
			logCriticalMessage(INVALID_PNR);
			throw new NoPNRFoundException(INVALID_PNR + pnr);
		}
		return true;
	}

	private static void resetTravellerId(List<TripInfo> tripInfos) {
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (FlightTravellerInfo traveller : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					traveller.setId(null);
				}
			}
		}
	}

	public AirCreditShellResponse fetchCreditShellBalance(AirCreditShellRequest request) {
		AirCreditShellResponse csResponse = null;
		NavitaireServiceManager manager = navitaireAirline.getCreditShellManager(bookingUser);
		if (manager != null) {
			manager.setSourceConfiguration(sourceConfiguration);
			manager.setConfiguration(configuration);
			manager.setListener(listener);
			listener.setKey(request.getBookingId());
			csResponse = manager.fetchCreditShellBalance(request);
		}
		return csResponse;
	}
}
