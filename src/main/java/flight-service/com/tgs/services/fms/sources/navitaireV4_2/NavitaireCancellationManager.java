package com.tgs.services.fms.sources.navitaireV4_2;

import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfJourney;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.ChargeType;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.DivideAction;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingServiceCharge;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingName;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassenger;
import com.navitaire.schemas.webservices.datacontracts.booking.Passenger;
import com.navitaire.schemas.webservices.datacontracts.common.FlightDesignator;
import com.navitaire.schemas.webservices.datacontracts.booking.Journey;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APICriticalFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APIFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APIGeneralFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APISecurityFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APIUnhandledServerFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APIValidationFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Cancel_APIWarningFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APICriticalFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APIFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APIGeneralFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APISecurityFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APIUnhandledServerFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APIValidationFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_Divide_APIWarningFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.datacontracts.booking.Segment;
import com.navitaire.schemas.webservices.datacontracts.booking.CancelJourney;
import com.navitaire.schemas.webservices.datacontracts.booking.CancelJourneyRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.navitaire.schemas.webservices.datacontracts.booking.CancelRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.DivideRequestData;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.CancelBy;
import com.navitaire.schemas.webservices.faultcontracts.APIGeneralFault;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.CancelRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.DivideRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.DivideResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.CancelResponse;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.BookingStatus;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfshort;
import com.navitaire.schemas.webservices.GetBookingFromStateRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBookingFromStateResponse;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.utils.exception.air.CancellationException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

@Slf4j
@SuperBuilder
@Getter
@Setter
final class NavitaireCancellationManager extends NavitaireServiceManager {

	protected double paymentToBeCollected;

	protected BigDecimal balanceDue;

	private final static String CANCELLATION_ACTION_CODE = "NU";

	private final static String JOURNEY_NOT_FOUND = "JourneyNotFound";

	public void init() {
		this.sourceId = configuration.getBasicInfo().getSourceId();
		this.navitaireAirline = NavitaireAirline.getNavitaireAirline(sourceId);
	}

	public boolean cancelAll() {
		boolean isCancelSuccess = false;
		try {
			listener.setType("CANCEL");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			CancelRequest cancelRequest = buildCancelRequest();
			CancelResponse cancelResponse = bookingBinding.cancel(cancelRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature());
			if (cancelResponse != null && cancelResponse.getBookingUpdateResponseData() != null
					&& cancelResponse.getBookingUpdateResponseData().getSuccess() != null) {
				isCancelSuccess = true;
			}

		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (IBookingManager_Cancel_APIValidationFaultFault_FaultMessage
				| IBookingManager_Cancel_APIUnhandledServerFaultFault_FaultMessage
				| IBookingManager_Cancel_APICriticalFaultFault_FaultMessage
				| IBookingManager_Cancel_APIGeneralFaultFault_FaultMessage
				| IBookingManager_Cancel_APISecurityFaultFault_FaultMessage
				| IBookingManager_Cancel_APIFaultFault_FaultMessage
				| IBookingManager_Cancel_APIWarningFaultFault_FaultMessage e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isCancelSuccess;
	}

	private CancelRequest buildCancelRequest() {
		CancelRequest cancelRequest = new CancelRequest();
		CancelRequestData requestData = new CancelRequestData();
		requestData.setCancelBy(CancelBy.All);
		requestData.setCancelJourney(null);
		requestData.setCancelFee(null);
		requestData.setCancelSSR(null);
		cancelRequest.setCancelRequestData(requestData);
		return cancelRequest;
	}


	public boolean checkCancelStatus(Booking bookingResponse) {
		boolean isCancelled = false;
		ArrayOfJourney journey = bookingResponse.getJourneys();
		if (bookingResponse != null && bookingResponse.getBookingInfo() != null) {
			BookingStatus bookingStatus = bookingResponse.getBookingInfo().getBookingStatus();
			if (bookingStatus.equals(BookingStatus.Closed) || bookingStatus.equals(BookingStatus.HoldCanceled)) {
				isCancelled = true;
			}
		}
		if (!isCancelled && (journey == null || (journey != null && ArrayUtils.isEmpty(journey.getJourney())))) {
			isCancelled = true;
		}
		return isCancelled;
	}

	public GetBookingFromStateResponse getCancellingBookingState() {
		GetBookingFromStateRequest bookingRequest = new GetBookingFromStateRequest();
		try {
			listener.setType("Cancel-State " + getSourceId());
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetBookingFromStateResponse bookingResponse =
					bookingBinding.getBookingFromState(bookingRequest, getContractVersion(), getExceptionStackTrace(),
							getMessageContractversion(), getSignature(), getSourceId());
			return bookingResponse;
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (
				com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBookingFromState_APIWarningFaultFault_FaultMessage apiError) {
			throw new SupplierUnHandledFaultException(apiError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	public String divideByPnr(Booking bookingResponse) {
		String recordLocatorCode = null;
		try {
			listener.setType("DivideByPnr");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			DivideRequest divideRequest = getDivideByPnrReq(bookingResponse);
			DivideResponse divideResponse = bookingBinding.divide(divideRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature());
			if (divideResponse != null && divideResponse.getBookingUpdateResponseData() != null
					&& divideResponse.getBookingUpdateResponseData().getSuccess() != null) {
				recordLocatorCode = divideResponse.getBookingUpdateResponseData().getSuccess().getRecordLocator();
			}
		} catch (RemoteException | IBookingManager_Divide_APIValidationFaultFault_FaultMessage
				| IBookingManager_Divide_APIUnhandledServerFaultFault_FaultMessage
				| IBookingManager_Divide_APICriticalFaultFault_FaultMessage
				| IBookingManager_Divide_APIGeneralFaultFault_FaultMessage
				| IBookingManager_Divide_APISecurityFaultFault_FaultMessage
				| IBookingManager_Divide_APIFaultFault_FaultMessage
				| IBookingManager_Divide_APIWarningFaultFault_FaultMessage e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return recordLocatorCode;
	}


	public DivideRequest getDivideByPnrReq(Booking bookingResponse) {
		List<Short> cancelledPaxList = getCancelledPaxList(bookingResponse);
		DivideRequest divideRequest = new DivideRequest();
		DivideRequestData divideReqData = new DivideRequestData();
		divideReqData.setDivideAction(DivideAction.Divide);
		divideReqData.setSourceRecordLocator(bookingSegments.getAirlinePNR());
		ArrayOfshort paxlist = new ArrayOfshort();
		short[] paxnumbers = new short[cancelledPaxList.size()];
		for (int i = 0; i < cancelledPaxList.size(); i++) {
			paxnumbers[i] = cancelledPaxList.get(i);
		}
		paxlist.set_short(paxnumbers);
		divideReqData.setPassengerNumbers(paxlist);
		divideReqData.setAutoDividePayments(Boolean.TRUE);
		divideRequest.setDivideReqData(divideReqData);
		return divideRequest;
	}


	public List<Short> getCancelledPaxList(Booking bookingResponse) {
		List<Short> cancelledPaxList = new ArrayList<>();
		for (Passenger passenger : bookingResponse.getPassengers().getPassenger()) {
			SegmentInfo segmentInfo = segmentInfos.get(0);
			for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getTravellerInfo()) {
				if (flightTravellerInfo.getFirstName()
						.equalsIgnoreCase(passenger.getNames().getBookingName()[0].getFirstName())
						&& flightTravellerInfo.getLastName()
								.equalsIgnoreCase(passenger.getNames().getBookingName()[0].getLastName())) {
					cancelledPaxList.add(passenger.getPassengerNumber());
				}
			}
		}
		return cancelledPaxList;
	}

	public boolean cancelSegments(List<SegmentInfo> segmentInfos, boolean isCancelAll, String listenerType) {
		boolean isCancelSuccess = false;
		try {
			listener.setType(listenerType);
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			CancelRequest cancelRequest = buildSegmentCancelRequest(segmentInfos, isCancelAll);
			CancelResponse cancelResponse = bookingBinding.cancel(cancelRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature());
			if (cancelResponse != null && cancelResponse.getBookingUpdateResponseData() != null
					&& cancelResponse.getBookingUpdateResponseData().getSuccess() != null) {
				isCancelSuccess = true;
				if (cancelResponse.getBookingUpdateResponseData().getSuccess().getPNRAmount() != null && cancelResponse
						.getBookingUpdateResponseData().getSuccess().getPNRAmount().getBalanceDue() != null) {
					balanceDue =
							cancelResponse.getBookingUpdateResponseData().getSuccess().getPNRAmount().getBalanceDue();
				}
			}
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (IBookingManager_Cancel_APIGeneralFaultFault_FaultMessage generalFualt) {
			APIGeneralFault apiGeneralFault = generalFualt.getFaultMessage().getAPIGeneralFault();
			if (apiGeneralFault.getErrorType().equalsIgnoreCase(JOURNEY_NOT_FOUND)) {
				throw new CancellationException(
						StringUtils.join(CancellationException.PARTIAL_CANCELATTION, apiGeneralFault.getMessage()));
			} else {
				throw new SupplierUnHandledFaultException(generalFualt.getMessage());
			}
		} catch (IBookingManager_Cancel_APIValidationFaultFault_FaultMessage
				| IBookingManager_Cancel_APIUnhandledServerFaultFault_FaultMessage
				| IBookingManager_Cancel_APICriticalFaultFault_FaultMessage
				| IBookingManager_Cancel_APISecurityFaultFault_FaultMessage
				| IBookingManager_Cancel_APIFaultFault_FaultMessage
				| IBookingManager_Cancel_APIWarningFaultFault_FaultMessage generalFualt) {
			throw new SupplierUnHandledFaultException(generalFualt.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isCancelSuccess;
	}

	private CancelRequest buildSegmentCancelRequest(List<SegmentInfo> segmentInfos, boolean isCancelAll) {
		CancelRequest cancelRequest = new CancelRequest();
		CancelRequestData requestData = new CancelRequestData();
		if (isCancelAll) {
			requestData.setCancelBy(CancelBy.All);
		} else {
			requestData.setCancelBy(CancelBy.Journey);
			requestData.setCancelJourney(buildCancelJourneyRequest(segmentInfos));
		}
		requestData.setCancelFee(null);
		requestData.setCancelSSR(null);
		cancelRequest.setCancelRequestData(requestData);
		return cancelRequest;
	}

	private CancelJourney buildCancelJourneyRequest(List<SegmentInfo> segmentInfos) {
		CancelJourney cancelJourney = new CancelJourney();
		CancelJourneyRequest journeyRequest = new CancelJourneyRequest();
		List<TripInfo> cancelTrips = AirCancelUtils.createTripListFromSegmentListCancellation(segmentInfos);
		ArrayOfJourney arrayOfJourney = new ArrayOfJourney();
		Journey[] journeys = new Journey[cancelTrips.size()];
		int index = 0;
		for (TripInfo tripInfo : cancelTrips) {
			Journey journey = new Journey();
			journey.setSegments(new ArrayOfSegment());
			StringJoiner joiner = new StringJoiner("^");
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				Segment segment = new Segment();
				segment.setDepartureStation(segmentInfo.getDepartureAirportCode());
				segment.setArrivalStation(segmentInfo.getArrivalAirportCode());
				segment.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
				segment.setSTA(TgsDateUtils.getCalendar(segmentInfo.getArrivalTime()));
				segment.setActionStatusCode(segmentInfo.getPriceInfo(0).getMiscInfo().getSessionId());
				segment.setSegmentSellKey(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
				segment.setFlightDesignator(buildFlightDesignator(segmentInfo));
				joiner.add(segment.getSegmentSellKey());
				journey.getSegments().addSegment(segment);
			});
			journey.setJourneySellKey(joiner.toString());
			journeys[index] = journey;
			index++;
		}
		arrayOfJourney.setJourney(journeys);
		journeyRequest.setJourneys(arrayOfJourney);
		journeyRequest.setWaivePenaltyFee(false);
		journeyRequest.setWaiveSpoilageFee(false);
		journeyRequest.setPreventReprice(false);
		cancelJourney.setCancelJourneyRequest(journeyRequest);
		return cancelJourney;
	}

	private FlightDesignator buildFlightDesignator(SegmentInfo segmentInfo) {
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator.setCarrierCode(segmentInfo.getAirlineCode(false));
		flightDesignator.setFlightNumber(StringUtils.leftPad(segmentInfo.getFlightNumber(), 4, " "));
		return flightDesignator;
	}

	public void prefillSegmentDetails(Booking bookingResponse, List<SegmentInfo> segmentInfos) {
		ArrayOfJourney arrayOfJourney = bookingResponse.getJourneys();
		if (arrayOfJourney != null && ArrayUtils.isNotEmpty(arrayOfJourney.getJourney())) {
			segmentInfos.forEach(segmentInfo -> {
				for (Journey journey : arrayOfJourney.getJourney()) {
					if (journey.getSegments() != null && ArrayUtils.isNotEmpty(journey.getSegments().getSegment())) {
						Segment[] arrayOfSegment = journey.getSegments().getSegment();
						for (Segment segment : arrayOfSegment) {
							// LocalDateTime departureDate = TgsDateUtils.calenderToLocalDateTime(segment.getSTD());
							// LocalDateTime arrivalDate = TgsDateUtils.calenderToLocalDateTime(segment.getSTA());
							LocalDate departureDate = TgsDateUtils.calenderToLocalDate(segment.getSTD());
							LocalDate arrivalDate = TgsDateUtils.calenderToLocalDate(segment.getSTA());
							if (segment.getDepartureStation().equalsIgnoreCase(segmentInfo.getDepartureAirportCode())
									&& segment.getArrivalStation().equalsIgnoreCase(segmentInfo.getArrivalAirportCode())
									&& departureDate.equals(segmentInfo.getDepartTime().toLocalDate())
									&& arrivalDate.equals(segmentInfo.getArrivalTime().toLocalDate())) {
								PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
								priceInfo.getMiscInfo().setJourneyKey(segment.getSegmentSellKey());
								priceInfo.getMiscInfo().setSessionId(segment.getActionStatusCode());
							}
						}
					}
				}
			});
		}
	}

	public void setAirlineCancellationFee(GetBookingFromStateResponse bookingState, BookingSegments bookingSegments,
			boolean isConfirm) {
		if (bookingState.getBookingData() != null && bookingState.getBookingData().getPassengers() != null) {
			ArrayOfPassenger arrayOfPassenger = bookingState.getBookingData().getPassengers();
			for (Passenger passenger : arrayOfPassenger.getPassenger()) {
				PassengerFee[] passengerFees = passenger.getPassengerFees().getPassengerFee();
				if (passengerFees != null) {
					for (PassengerFee passengerFee : passengerFees) {
						// Each PassengerFee is Journey wise
						SegmentInfo segmentInfo =
								NavitaireUtils.getSegmentInfo(bookingSegments.getCancelSegments(), passengerFee);
						FlightTravellerInfo travellerInfo = NavitaireUtils
								.filteredTraveller(bookingSegments.getCancelSegments(), passenger, passengerFee);
						if (passenger.getInfant() != null && passenger.getInfant().getNames() != null
								|| travellerInfo != null) {
							buildFareDetail(travellerInfo, passengerFee, isConfirm);
							if (passenger.getInfant() != null && passenger.getInfant().getNames() != null) {
								BookingName infantName = passenger.getInfant().getNames().getBookingName()[0];
								FlightTravellerInfo infant = NavitaireUtils.getTravellerInfo(segmentInfo, infantName);
								if (infant != null) {
									/**
									 * @implSpec : <br>
									 *           Infant Airline Cancellation if Any Infant No Airline <br>
									 *           Cancellation Fee {no seat assigned to infant <br>
									 *           & infant always assigned to adult}
									 */
									infant.getFareDetail().getFareComponents().put(FareComponent.ACF, new Double(0));
									// infant.getFareDetail().getFareComponents().put(FareComponent.ACFT, new
									// Double(0));
								}
							}
						}
					}
				}
			}
		}
	}

	// set airline cancellation fee
	private void buildFareDetail(FlightTravellerInfo travellerInfo, PassengerFee passengerFee, boolean isConfirm) {
		if (passengerFee.getServiceCharges() != null
				&& ArrayUtils.isNotEmpty(passengerFee.getServiceCharges().getBookingServiceCharge())
				&& travellerInfo != null
				&& (travellerInfo.getStatus() == null || (travellerInfo.getStatus() != null
						&& !travellerInfo.getStatus().equals(TravellerStatus.CANCELLED)
						&& !travellerInfo.getStatus().equals(TravellerStatus.VOIDED)))) {
			FareDetail fareDetail = travellerInfo.getFareDetail();
			for (BookingServiceCharge charge : passengerFee.getServiceCharges().getBookingServiceCharge()) {
				// String chargeType = charge.getChargeType().getValue();
				if (!ChargeType._IncludedTax.equalsIgnoreCase(charge.getChargeType().getValue())) {
					// all airline cancellation fee will got to ACF (even if contains gst, included tax other charge
					// type)
					NavitaireCharges navitaireCharges = NavitaireCharges.CANCELLATION_FEE;
					// NavitaireCharges navitaireCharges = NavitaireCharges.getNavitaireChargeOnChargeType(chargeType);
					// String chargeCode = charge.getChargeCode();
					// navitaireCharges = NavitaireCharges.getNavitaireChargeOnFeeType(chargeCode, navitaireCharges);
					double amount = getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
					navitaireCharges.setFareComponent(fareDetail, travellerInfo.getPaxType().getType(), amount,
							charge.getCurrencyCode());
					if (isConfirm)
						travellerInfo.setStatus(TravellerStatus.CANCELLED);
				}
			}
		}
	}

}
