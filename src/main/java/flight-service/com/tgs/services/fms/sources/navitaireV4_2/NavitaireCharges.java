package com.tgs.services.fms.sources.navitaireV4_2;

import java.util.Objects;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FareDetail;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Getter
enum NavitaireCharges {

	OTHER_CHARGES(0, "Other Charges", "Other Charges") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.OC);
			fareDetail.getFareComponents().put(FareComponent.OC, initialAmount + amount);
		}
	},
	BASE_FARE(1, "Base Fare", "FarePrice") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.BF);
			fareDetail.getFareComponents().put(FareComponent.BF, amount + initialAmount);
		}
	},
	AIRLINE_TAX(2, "Airline Tax", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	TRAVEL_FEE(3, "Travel Fee", "Tax") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	GST(4, "GST", "Tax") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	INFT(5, "INFT", "ServiceCharge") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.BF);
			fareDetail.getFareComponents().put(FareComponent.BF, initialAmount + amount);
		}
	},
	PROMOTION_DISCOUNT(6, "PRM", "PromotionDiscount") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = 0.0;
			if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.AT))
					&& fareDetail.getFareComponents().get(FareComponent.AT).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.AT).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.AT, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Airline Tax", amount, paxType);
			} else if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.BF))
					&& fareDetail.getFareComponents().get(FareComponent.BF).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.BF).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.BF, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Base Fare", amount, paxType);
			}
		}
	},
	CHANGE_FEE(7, "Penalty- Change Fee", "PenaltyFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	CANCEL_FEE(8, "Penalty- Cancel Fee", "PenaltyFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	SEAT_FEE(9, "Seat Fee", "SeatFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	SPOILAGE_FEE(10, "SpoilageFee", "SpoilageFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	PRIORITY_FEE(11, "Priority Checkin And Baggage", "SSRFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}

	},
	UNACCOMPANIED_MINOR_FEE(12, "UMNR", "SSRFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	WEAPON_CARRIAGE_FEE(13, "WPNN", "SSRFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	AIRPORT_DEVELOPMENT_FEE(14, "ADF", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.WO);
			fareDetail.getFareComponents().put(FareComponent.WO, initialAmount + amount);
		}
	},
	COMMISSION(15, "COMM", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	COMMISSIONBF(16, "COMMBF", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	CUTE_CHARGE(17, "PHF", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AT);
			fareDetail.getFareComponents().put(FareComponent.AT, initialAmount + amount);
		}
	},
	PASSENGER_SERVICE_FEE(18, "PSF", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.PSF);
			fareDetail.getFareComponents().put(FareComponent.PSF, initialAmount + amount);
		}
	},
	RCS_FEE(19, "RCS", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.RCF);
			fareDetail.getFareComponents().put(FareComponent.RCF, initialAmount + amount);
		}
	},
	USER_DEVELOPMENT_FEE(20, "UDF", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.UDF);
			fareDetail.getFareComponents().put(FareComponent.UDF, initialAmount + amount);
		}
	},
	AIRLINE_FUEL_CHARGE(21, "YQ", "TravelFee") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.YQ);
			fareDetail.getFareComponents().put(FareComponent.YQ, initialAmount + amount);
		}
	},
	AGENCY_COMMISSION(22, "TTF", "Tax") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.OC);
			fareDetail.getFareComponents().put(FareComponent.OC, initialAmount + amount);
		}
	},
	CT_GST(23, "CT", "Tax") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	ST_GST(24, "ST", "Tax") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	IT_GST(25, "IT", "Tax") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.AGST);
			fareDetail.getFareComponents().put(FareComponent.AGST, initialAmount + amount);
		}
	},
	DISCOUNT(26, "", "Discount") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = 0.0;
			if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.AT))
					&& fareDetail.getFareComponents().get(FareComponent.AT).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.AT).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.AT, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Airline Tax", amount, paxType);
			} else if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
					&& Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.BF))
					&& fareDetail.getFareComponents().get(FareComponent.BF).doubleValue() - amount > 0) {
				initialAmount = fareDetail.getFareComponents().get(FareComponent.BF).doubleValue();
				fareDetail.getFareComponents().put(FareComponent.BF, initialAmount - amount);
				log.debug("API Promotion Discount Applied fare {} for PaxType {} on Base Fare", amount, paxType);
			}
		}
	},
	CANCELLATION_FEE(27, "CXL", "ServiceCharge") {
		@Override
		public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {
			double initialAmount = getInitialAmount(fareDetail, FareComponent.ACF);
			fareDetail.getFareComponents().put(FareComponent.ACF, initialAmount + amount);
		}
	};

	private int id = -1;

	private String feeType;

	private String chargeType;

	private NavitaireCharges(int value, String feeType, String chargeType) {
		id = value;
		this.feeType = feeType;
		this.chargeType = chargeType;
	}

	public static NavitaireCharges getNavitaireChargeOnChargeType(String chargeType) {
		for (NavitaireCharges charges : NavitaireCharges.values()) {
			if (charges.chargeType.equalsIgnoreCase(chargeType)) {
				return charges;
			}
		}
		return NavitaireCharges.OTHER_CHARGES;
	}

	public static NavitaireCharges getNavitaireChargeOnFeeType(String feeType, NavitaireCharges defaultCharge) {
		for (NavitaireCharges charges : NavitaireCharges.values()) {
			if (StringUtils.isNotBlank(charges.getFeeType())) {
				if (charges.feeType.equalsIgnoreCase(feeType) || feeType.contains(charges.feeType)) {
					return charges;
				}
			}
		}
		return defaultCharge;
	}

	public void setFareComponent(FareDetail fareDetail, String paxType, double amount, String currencyCode) {}

	public double getInitialAmount(FareDetail fareDetail, FareComponent fareComponent) {
		double initialAmount = 0.0;
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
				&& Objects.nonNull(fareDetail.getFareComponents().get(fareComponent))
				&& fareDetail.getFareComponents().get(fareComponent).doubleValue() > 0) {
			initialAmount = fareDetail.getFareComponents().get(fareComponent).doubleValue();
		}
		return initialAmount;
	}

}
