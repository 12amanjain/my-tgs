package com.tgs.services.fms.sources.navitaireV4_2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfAvailablePaxSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfEquipmentInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfEquipmentProperty;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfLegKey;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSSRSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSeatGroupPassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.AvailablePaxSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingServiceCharge;
import com.navitaire.schemas.webservices.datacontracts.booking.EquipmentProperty;
import com.navitaire.schemas.webservices.datacontracts.booking.LegKey;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxFare;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxSSRPrice;
import com.navitaire.schemas.webservices.datacontracts.booking.SSRAvailabilityForBookingRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SSRSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.SeatAvailabilityRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SeatGroupPassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.SeatInfo;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.SeatAssignmentMode;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.SeatAvailability;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetSSRAvailabilityForBookingRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetSSRAvailabilityForBookingResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetSeatAvailabilityRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetSeatAvailabilityResponse;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
final class NavitaireSSRManager extends NavitaireServiceManager {

	protected boolean isSellAllSSR;


	public void doSSRSearch(TripInfo selectedTrip) {
		// Always We will be doing Review for Only one trip as Journey wise
		// Case : Onward will be one trip & return will be one trip with different key
		// so SSR should also on journey wise
		AirUtils.splitTripInfo(selectedTrip, false).forEach(tripInfo -> {
			getAllSSRDetailsforTrip(tripInfo);
		});
	}

	private void getAllSSRDetailsforTrip(TripInfo tripInfo) {
		try {

			GetSSRAvailabilityForBookingRequest ssrRequest = new GetSSRAvailabilityForBookingRequest();
			ssrRequest.setSSRAvailabilityForBookingRequest(getSSRAvailabilityRequest(tripInfo));
			listener.setType("R-GETSSR");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetSSRAvailabilityForBookingResponse ssrResponse =
					bookingBinding.getSSRAvailabilityForBooking(ssrRequest, getContractVersion(),
							getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			parseSSRtoTrip(ssrResponse, tripInfo);
			mergeBaggagePriceToFirstSegment(tripInfo);


		} catch (Exception e) {
			log.error("Error Occured while fetching SSR for Trip {} ", tripInfo, e);
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}


	// combine baggage fare for all segments in first segment
	private void mergeBaggagePriceToFirstSegment(TripInfo tripInfo) {
		try {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())) {
					if (segmentInfo.getSegmentNum() != 0 && navitaireAirline.isBaggageAmountSegmentWise()) {
						NavitaireUtils.mergeBaggageinFirstSegment(tripInfo.getSegmentInfos().get(0), segmentInfo);
					}
				}
			});
		} catch (Exception e) {
			log.error("SSR Merging failed for booking {} for trip {}  cause {}", bookingId, tripInfo.toString(), e);
		}
	}

	private void parseSSRtoTrip(GetSSRAvailabilityForBookingResponse ssrResponse, TripInfo tripInfo) {
		try {
			if (Objects.nonNull(ssrResponse)
					&& Objects.nonNull(ssrResponse.getSSRAvailabilityForBookingResponse().getSSRSegmentList())) {
				/**
				 * 
				 * Getting common baggage SSR code for all segments to avoid Baggage not available issues. As there is
				 * difference in baggage SSr for domestic segments and International segments.
				 * 
				 * ex DEL-DXB (DEL-BOM and BOM-DXB segment)
				 * 
				 **/

				List<AvailablePaxSSR> commonSSR = getCommonSSR(tripInfo.getSegmentInfos().get(0),
						ssrResponse.getSSRAvailabilityForBookingResponse().getSSRSegmentList());
				tripInfo.getSegmentInfos().forEach(segmentInfo -> {
					Map<SSRType, List<? extends SSRInformation>> ssrInfo =
							new HashMap<SSRType, List<? extends SSRInformation>>();
					ArrayOfSSRSegment ssrSegment =
							ssrResponse.getSSRAvailabilityForBookingResponse().getSSRSegmentList();
					FlightBasicFact fact = FlightBasicFact.builder().build().generateFact(segmentInfo);
					BaseUtils.createFactOnUser(fact, bookingUser);
					Map<SSRType, List<? extends SSRInformation>> preSSR = AirUtils.getPreSSR(fact);
					for (SSRSegment ssr : ssrSegment.getSSRSegment()) {
						LegKey legKey = ssr.getLegKey();
						if (isExactSegmentForSSR(segmentInfo, legKey)) {
							List<BaggageSSRInformation> baggageSSRInformations = new ArrayList<BaggageSSRInformation>();
							List<MealSSRInformation> mealSSRInformations = new ArrayList<MealSSRInformation>();
							for (AvailablePaxSSR paxSSR : ssr.getAvailablePaxSSRList().getAvailablePaxSSR()) {
								List<? extends SSRInformation> baggagePreSSR = new ArrayList<>();
								List<? extends SSRInformation> mealPreSSR = new ArrayList<>();
								if (preSSR != null) {
									baggagePreSSR = preSSR.get(SSRType.BAGGAGE);
									mealPreSSR = preSSR.get(SSRType.MEAL);
								}
								SSRInformation baggageSSRInfo = null;
								if (isSSRPresent(commonSSR, paxSSR.getSSRCode())) {
									baggageSSRInfo = AirUtils.getSSRInfo(baggagePreSSR, paxSSR.getSSRCode());
								}
								SSRInformation mealSSRInfo = AirUtils.getSSRInfo(mealPreSSR, paxSSR.getSSRCode());
								if (Objects.nonNull(baggageSSRInfo)) {
									BaggageSSRInformation ssrInformation = new BaggageSSRInformation();
									ssrInformation.setCode(paxSSR.getSSRCode());
									ssrInformation.setDesc(baggageSSRInfo.getDesc());
									if (navitaireAirline.isBaggageAmountSegmentWise()) {
										ssrInformation.setAmount(getTotalSSRAmount(paxSSR));
									} else {
										if (segmentInfo.getSegmentNum().intValue() == 0
												|| BooleanUtils.isTrue(segmentInfo.isCombinationFirstSegment)) {
											ssrInformation.setAmount(getTotalSSRAmount(paxSSR));
										} else {
											ssrInformation.setAmount(null);
										}
									}

									baggageSSRInformations.add(ssrInformation);
								} else if (Objects.nonNull(mealSSRInfo)) {
									MealSSRInformation ssrInformation = new MealSSRInformation();
									if (navitaireAirline.isNonBundleFare(segmentInfo)) {
										ssrInformation.setAmount(getTotalSSRAmount(paxSSR));
									}
									ssrInformation.setCode(paxSSR.getSSRCode());
									ssrInformation.setDesc(mealSSRInfo.getDesc());
									mealSSRInformations.add(ssrInformation);
								} else if (infantCount > 0 && paxSSR.getSSRCode().equalsIgnoreCase(PAX_TYPE_INFANT)
										&& navitaireAirline.isInfantFareOnSegmentWise(segmentInfo)
										&& navitaireAirline.isInfantFareFixed()) {
									PaxFare paxFare = new PaxFare();
									FareDetail infantFareDetail =
											segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT, new FareDetail());
									if (MapUtils.isEmpty(infantFareDetail.getFareComponents())) {
										infantFare = new GsonMapper<>(infantFareDetail, FareDetail.class).convert();
										paxFare.setPaxType(PAX_TYPE_INFANT);
										paxFare.setTicketFareBasisCode("");
										for (PaxSSRPrice ssrPrice : paxSSR.getPaxSSRPriceList().getPaxSSRPrice()) {
											infantFare = setPaxWiseFareBreakUp(infantFare, paxFare, 1,
													ssrPrice.getPaxFee().getServiceCharges().getBookingServiceCharge());
										}
										infantFareDetail.setFareComponents(infantFare.getFareComponents());
									}
								}
							}
							ssrInfo.put(SSRType.BAGGAGE, baggageSSRInformations);
							ssrInfo.put(SSRType.MEAL, mealSSRInformations);
						}
					}

					segmentInfo.setSsrInfo(ssrInfo);
				});
			}
		} catch (Exception e) {
			log.error("SSR Parsing failed for booking {} for trip {}  cause {}", bookingId, tripInfo.toString(), e);
		}
	}

	private boolean isSSRPresent(List<AvailablePaxSSR> commonSSR, String ssrCode) {
		List<AvailablePaxSSR> retrievedCommonSSR = commonSSR.stream()
				.filter(ssr -> StringUtils.equalsIgnoreCase(ssr.getSSRCode(), ssrCode)).collect(Collectors.toList());
		return !retrievedCommonSSR.isEmpty();
	}

	private List<AvailablePaxSSR> getCommonSSR(SegmentInfo segmentInfo, ArrayOfSSRSegment ssrSegmentList) {
		List<AvailablePaxSSR> commonSSRList = null;
		for (SSRSegment ssrSegment : ssrSegmentList.getSSRSegment()) {
			LegKey legKey = ssrSegment.getLegKey();
			if (isExactSegmentForSSR(segmentInfo, legKey)) {
				commonSSRList = new ArrayList<AvailablePaxSSR>();
				commonSSRList = Arrays.asList(ssrSegment.getAvailablePaxSSRList().getAvailablePaxSSR());
			} else if (commonSSRList != null) {
				commonSSRList = commonSSRList
						.stream().filter(
								persistPaxSSR -> Arrays.asList(ssrSegment.getAvailablePaxSSRList().getAvailablePaxSSR())
										.stream().anyMatch(ssr -> StringUtils
												.equalsIgnoreCase(persistPaxSSR.getSSRCode(), ssr.getSSRCode())))
						.collect(Collectors.toList());
			}
		}
		return commonSSRList;
	}

	private SSRAvailabilityForBookingRequest getSSRAvailabilityRequest(TripInfo tripInfo) {
		SSRAvailabilityForBookingRequest ssrRequest = new SSRAvailabilityForBookingRequest();
		setCommonSSRRequest(ssrRequest, tripInfo);
		if (!isSellAllSSR) {
			ssrRequest.setSSRAvailabilityMode(navitaireAirline.getSSRBunbleMode(tripInfo));
		}
		ssrRequest.setSegmentKeyList(getSegmentSSRKey(tripInfo));
		return ssrRequest;
	}

	private ArrayOfLegKey getSegmentSSRKey(TripInfo tripInfo) {
		ArrayOfLegKey legKey = new ArrayOfLegKey();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			LegKey segmentKey = new LegKey();
			segmentKey.setDepartureStation(segmentInfo.getDepartAirportInfo().getCode());
			segmentKey.setArrivalStation(segmentInfo.getArrivalAirportInfo().getCode());
			segmentKey.setDepartureDate(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
			segmentKey.setCarrierCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
			segmentKey.setFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber());
			segmentKey.setOpSuffix(StringUtils.EMPTY);
			legKey.addLegKey(segmentKey);
		});
		return legKey;
	}

	private boolean isExactSegmentForSSR(SegmentInfo segmentInfo, LegKey legKey) {
		return legKey.getDepartureStation().equals(segmentInfo.getDepartAirportInfo().getCode())
				&& legKey.getArrivalStation().equals(segmentInfo.getArrivalAirportInfo().getCode());
	}

	@Override
	public double getTotalSSRAmount(AvailablePaxSSR paxSSR) {
		double amount = 0;
		if (Objects.nonNull(paxSSR.getPaxSSRPriceList().getPaxSSRPrice())) {
			for (PaxSSRPrice price : paxSSR.getPaxSSRPriceList().getPaxSSRPrice()) {
				for (BookingServiceCharge serviceCharge : price.getPaxFee().getServiceCharges()
						.getBookingServiceCharge()) {
					if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
						if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
								&& serviceCharge.getAmount().doubleValue() < 0) {
							serviceCharge.setAmount(serviceCharge.getAmount().abs());
						}
						amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
					}
				}
			}
		}
		return amount;
	}


	public double getSeatSSRAmount(BookingServiceCharge[] bookingServiceCharges) {
		double amount = 0;
		for (BookingServiceCharge serviceCharge : bookingServiceCharges) {
			if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
				if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
						&& serviceCharge.getAmount().doubleValue() < 0) {
					serviceCharge.setAmount(serviceCharge.getAmount().abs());
				}
				amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
			}
		}
		return amount;
	}

	private List<SeatSSRInformation> parseSeatMapInfo(GetSeatAvailabilityResponse response, SegmentInfo segmentInfo) {
		Map<String, SeatSSRInformation> ssrInformations = new HashMap<>();
		// get Total Seats
		ArrayOfEquipmentInfo equipmentInfo = response.getSeatAvailabilityResponse().getEquipmentInfos();
		List<SeatInfo> seats = new ArrayList<>();
		if (equipmentInfo != null && ArrayUtils.isNotEmpty(equipmentInfo.getEquipmentInfo())) {
			Arrays.stream(equipmentInfo.getEquipmentInfo()).forEach(equipmentInfo1 -> {
				Arrays.stream(equipmentInfo1.getCompartments().getCompartmentInfo()).forEach(compartmentInfo -> {
					if (compartmentInfo.getSeats() != null && compartmentInfo.getSeats().getSeatInfo() != null) {
						seats.addAll(Arrays.asList(compartmentInfo.getSeats().getSeatInfo()));
					} else {
						return;
					}
				});
			});
		}
		// get Seat Group fare
		Map<String, SeatGroupPassengerFee> seatGroupPassengerFee = new HashMap<>();
		ArrayOfSeatGroupPassengerFee seatFeeList = response.getSeatAvailabilityResponse().getSeatGroupPassengerFees();
		if (seatFeeList != null && ArrayUtils.isNotEmpty(seatFeeList.getSeatGroupPassengerFee())) {
			Arrays.stream(seatFeeList.getSeatGroupPassengerFee()).forEach(seatGroupFee -> {
				seatGroupPassengerFee.put(String.valueOf(seatGroupFee.getSeatGroup()), seatGroupFee);
			});
		}
		// map Seat with Seat group to get fare
		seats.forEach(seat -> {
			try {
				if (!seat.getSeatDesignator().contains("$")) {
					SeatSSRInformation seatSSRInformation = new SeatSSRInformation();
					seatSSRInformation.setSeatNo(seat.getSeatDesignator().trim());
					ArrayOfEquipmentProperty seatProperty = seat.getPropertyList();
					if (seatGroupPassengerFee.getOrDefault(Short.toString(seat.getSeatGroup()), null) != null) {
						BookingServiceCharge[] bookingCharge =
								seatGroupPassengerFee.get(Short.toString(seat.getSeatGroup())).getPassengerFee()
										.getServiceCharges().getBookingServiceCharge();
						seatSSRInformation.setAmount(getSeatSSRAmount(bookingCharge));
						seatSSRInformation.setCode(seat.getSeatDesignator().trim());
						seatSSRInformation.setIsBooked(!getSeatAvailable(seat, segmentInfo));

						// In case of stop over flights legs will have same seat number available multiple times under
						// single segment
						if (ssrInformations.get(seatSSRInformation.getSeatNo()) != null) {
							seatSSRInformation.setIsBooked(seatSSRInformation.getIsBooked()
									&& ssrInformations.get(seatSSRInformation.getSeatNo()).getIsBooked());
						}
						ssrInformations.put(seatSSRInformation.getSeatNo(), seatSSRInformation);
					}
					if (seatProperty != null) {
						EquipmentProperty[] properties = seatProperty.getEquipmentProperty();
						Arrays.stream(properties).forEach(property -> {
							String seatType = property.getTypeCode();
							String seatTypeValue = property.getValue();
							if ("AISLE".equalsIgnoreCase(seatType)) {
								seatSSRInformation.setIsAisle(Boolean.valueOf(seatTypeValue));
							} else if ("LEGROOM".equalsIgnoreCase(seatType)) {
								seatSSRInformation.setIsLegroom(Boolean.valueOf(seatTypeValue));
							}
						});
					}
				}
			} catch (Exception e) {
				log.error("Error in Seat map response for bookingid {} and seat {} ", bookingId, seat);
			}
		});
		return new ArrayList<>(ssrInformations.values());
	}

	private Boolean getSeatAvailable(SeatInfo seat, SegmentInfo segmentInfo) {
		if (seat.getSeatAvailability().equals(SeatAvailability.Open)
				&& navitaireAirline.isSeatApplicable(segmentInfo, seat)) {
			return true;
		} else if (!seat.getSeatAvailability().equals(SeatAvailability.Reserved)) {
			return false;
		}
		return false;
	}

	public List<SeatSSRInformation> getSeatMap(SegmentInfo segmentInfo) {
		try {
			GetSeatAvailabilityRequest seatAvailabilityRequest = new GetSeatAvailabilityRequest();
			seatAvailabilityRequest.setSeatAvailabilityRequest(getSeatAvailabilityParam(segmentInfo));
			listener.setType("R-SEATMAP");
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetSeatAvailabilityResponse response =
					bookingBinding.getSeatAvailability(seatAvailabilityRequest, getContractVersion(),
							getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			return parseSeatMapInfo(response, segmentInfo);
		} catch (Exception e) {
			log.error("Seat Segment Failed for booking id {} and segment {}", bookingId, segmentInfo.toString(), e);
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	private SeatAvailabilityRequest getSeatAvailabilityParam(SegmentInfo segmentInfo) {
		SeatAvailabilityRequest request = new SeatAvailabilityRequest();
		request.setDepartureStation(segmentInfo.getDepartureAirportCode());
		request.setArrivalStation(segmentInfo.getArrivalAirportCode());
		request.setCarrierCode(segmentInfo.getAirlineCode(false));
		request.setCollectedCurrencyCode(getCurrencyCode());
		request.setIncludeSeatFees(true);
		request.setSeatAssignmentMode(SeatAssignmentMode.PreSeatAssignment);
		request.setSeatGroup((short) 0);
		request.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime()));
		request.setFlightNumber(StringUtils.leftPad(segmentInfo.getFlightNumber(), 4, " "));
		request.setCompressProperties(false);
		request.setIncludePropertyLookup(true);
		request.setExcludeEquipmentConfiguration(false);
		request.setEnforceSeatGroupRestrictions(navitaireAirline.getEnforceSeatGroupRestrictions());

		return request;
	}

}
