package com.tgs.services.fms.sources.navitaireV4_2;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.navitaire_newskies_webservices_datacontracts_common_enumerations.LoyaltyFilter;
import org.springframework.data.util.Pair;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APICriticalFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APIFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APIGeneralFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APISecurityFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APIUnhandledServerFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APIValidationFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.IBookingManager_GetAvailabilityVer2_APIWarningFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfAvailabilityRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfAvailableFare2;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfAvailableSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfBaggageAllowance;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfJourney;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfJourneyDateMarketVer2;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfLeg;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassenger;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaxSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.AvailabilityRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.AvailableFare2;
import com.navitaire.schemas.webservices.datacontracts.booking.AvailableJourney;
import com.navitaire.schemas.webservices.datacontracts.booking.AvailableSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.BaggageAllowance;
import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.navitaire.schemas.webservices.datacontracts.booking.Fare;
import com.navitaire.schemas.webservices.datacontracts.booking.GetBaggageAllowancesRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.GetBaggageAllowancesResponseData;
import com.navitaire.schemas.webservices.datacontracts.booking.ItineraryPriceRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.Journey;
import com.navitaire.schemas.webservices.datacontracts.booking.JourneyDateMarketVer2;
import com.navitaire.schemas.webservices.datacontracts.booking.LegInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.OperationsInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.Passenger;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxFare;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxSegment;
import com.navitaire.schemas.webservices.datacontracts.booking.Segment;
import com.navitaire.schemas.webservices.datacontracts.booking.TripAvailabilityRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.TripAvailabilityResponseVer2;
import com.navitaire.schemas.webservices.datacontracts.booking.TypeOfSale;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.ArrayOfJourneySortKey;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.AvailabilityFilter;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.AvailabilityType;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.BookingStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.DOW;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.DepartureStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.FareRuleFilter;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.FlightType;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.InboundOutbound;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.JourneySortKey;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.PriceItineraryBy;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.SSRCollectionsMode;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetAvailabilityRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetAvailabilityVer2Response;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBaggageAllowancesRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBaggageAllowancesResponse;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.PriceItineraryRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.PriceItineraryResponse;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SegmentMiscInfo;
import com.tgs.services.fms.datamodel.SupplierFlowType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class NavitaireSearchManager extends NavitaireServiceManager {

	protected AirSearchResult searchResult;

	private static final String INVALID_STATION_CODE = "InvalidStationCode";

	// product class
	protected String classToSearch;

	public AirSearchResult doSearch(boolean isSearch) {
		if (navitaireAirline.isGetAvailabilityV2()) {
			ArrayOfAvailabilityRequest availability = new ArrayOfAvailabilityRequest();
			GetAvailabilityRequest availabilityRequest = new GetAvailabilityRequest();
			searchResult = new AirSearchResult();
			AvailabilityRequest[] availabilityRequests = new AvailabilityRequest[searchQuery.getRouteInfos().size()];
			int avilabilityIndex = 0;
			for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
				AvailabilityRequest request = new AvailabilityRequest();
				setCommonAvailabilityRequest(request, isSearch);
				setAirSearchReqProperty(request, routeInfo);
				availabilityRequests[avilabilityIndex] = request;
				avilabilityIndex++;
			}
			availability.setAvailabilityRequest(availabilityRequests);
			TripAvailabilityRequest tripAvailabilityRequest = new TripAvailabilityRequest();
			tripAvailabilityRequest.setAvailabilityRequests(availability);
			tripAvailabilityRequest.setLoyaltyFilter(LoyaltyFilter.MonetaryOnly);
			tripAvailabilityRequest.setLowFareMode(false);
			availabilityRequest.setTripAvailabilityRequest(tripAvailabilityRequest);
			return getAvailabilityV2(availabilityRequest);
		} else {
			NavitaireSearchManagerV4_1 searchManagerV41 = NavitaireSearchManagerV4_1.builder()
					.configuration(configuration).searchQuery(searchQuery).sessionSignature(sessionSignature)
					.listener(listener).bookingStubV3(bookingStubV3).moneyExchnageComm(moneyExchnageComm)
					.sourceConfiguration(sourceConfiguration).bookingUser(bookingUser).build();
			searchManagerV41.init();
			return searchManagerV41.doSearch(isSearch);
		}
	}

	public AirSearchResult getAvailabilityV2(GetAvailabilityRequest availabilityRequest) {
		GetAvailabilityVer2Response response = null;
		try {
			listener.setType(AirUtils.getLogType("2-GETAVAIL", configuration));
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			response = bookingBinding.getAvailabilityVer2(availabilityRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			if (response != null) {
				parseAvailabilityTripResponse(response.getGetTripAvailabilityVer2Response());
			}
		} catch (IBookingManager_GetAvailabilityVer2_APIGeneralFaultFault_FaultMessage apiFault) {
			if (apiFault.getFaultMessage() != null && apiFault.getFaultMessage().getAPIGeneralFault() != null
					&& StringUtils.equalsIgnoreCase(apiFault.getFaultMessage().getAPIGeneralFault().getErrorType(),
							INVALID_STATION_CODE)) {
				throw new NotOperatingSectorException(apiFault.getMessage());
			} else {
				throw new NoSearchResultException(apiFault.getMessage());
			}
		} catch (IBookingManager_GetAvailabilityVer2_APIValidationFaultFault_FaultMessage
				| IBookingManager_GetAvailabilityVer2_APIUnhandledServerFaultFault_FaultMessage
				| IBookingManager_GetAvailabilityVer2_APICriticalFaultFault_FaultMessage
				| IBookingManager_GetAvailabilityVer2_APISecurityFaultFault_FaultMessage
				| IBookingManager_GetAvailabilityVer2_APIFaultFault_FaultMessage
				| IBookingManager_GetAvailabilityVer2_APIWarningFaultFault_FaultMessage notOperatingSector) {
			throw new NoSearchResultException(notOperatingSector.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return searchResult;
	}

	private void setCommonAvailabilityRequest(AvailabilityRequest request, boolean isSearch) {
		request.setCarrierCode(getAirlineCode());
		request.setDow(DOW.Daily);
		request.setAvailabilityType(AvailabilityType.Default);
		request.setAvailabilityFilter(AvailabilityFilter.ExcludeUnavailable);
		request.setMaximumConnectingFlights(
				navitaireAirline.getConnectingFlightsCount(searchQuery.getIsDomestic(), sourceConfiguration));
		request.setSSRCollectionsMode(SSRCollectionsMode.None);
		request.setNightsStay(0);
		request.setIncludeAllotments(false);
		request.setFlightType(FlightType.All);
		request.setMinimumFarePrice(BigDecimal.ZERO);
		request.setMaximumFarePrice(BigDecimal.ZERO);
		request.setInboundOutbound(InboundOutbound.None);
		request.setIncludeTaxesAndFees(isSearch);
		request.setFareClassControl(getFareClassControl());
		request.setFareRuleFilter(FareRuleFilter.Default);
		request.setLoyaltyFilter(LoyaltyFilter.MonetaryOnly);
		request.setDiscountCode(StringUtils.EMPTY);
		request.setPromotionCode(getPromoCode(searchQuery));
		request.setProductClassCode(StringUtils.EMPTY);
	}

	private String getAirlineCode() {
		String airlineCode = null;
		AirSourceConfigurationOutput airSourceOutput =
				AirUtils.getAirSourceConfiguration(searchQuery, configuration.getBasicInfo(), bookingUser);
		if (airSourceOutput != null && CollectionUtils.isNotEmpty(airSourceOutput.getIncludedAirlines())) {
			airlineCode = airSourceOutput.getIncludedAirlines().stream().findFirst().get();
		} else if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			airlineCode = searchQuery.getPrefferedAirline();
		}
		return airlineCode;
	}

	private void setAirSearchReqProperty(AvailabilityRequest request, RouteInfo routeInfo) {
		request.setPaxPriceTypes(getPaxPriceType(true));
		request.setPaxCount((short) AirUtils.getPaxCount(searchQuery, false));
		request.setCurrencyCode(getCurrencyCode());
		request.setFareTypes(getFareTypes());
		request.setProductClasses(getProductClasses());
		request.setFareClasses(null);
		ArrayOfJourneySortKey journeySortKey = new ArrayOfJourneySortKey();
		journeySortKey.addJourneySortKey(JourneySortKey.ServiceType);
		request.setJourneySortKeys(journeySortKey);
		request.setBeginDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
		request.setEndDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
		request.setDepartureStation(routeInfo.getFromCityOrAirport().getCode());
		request.setArrivalStation(routeInfo.getToCityOrAirport().getCode());
	}

	private ArrayOfstring getFareTypes() {
		ArrayOfstring arrString = new ArrayOfstring();
		if (Objects.nonNull(configuration.getSupplierAdditionalInfo().getFareTypes())
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareTypes(searchQuery)))
			arrString.setString(configuration.getSupplierAdditionalInfo().getFareTypes(searchQuery).split(","));
		return arrString;
	}

	private ArrayOfstring getProductClasses() {
		ArrayOfstring arrString = new ArrayOfstring();
		if (StringUtils.isNotBlank(classToSearch)) {
			arrString.setString(new String[] {classToSearch});
		} else {
			if (Objects.nonNull(configuration.getSupplierAdditionalInfo().getProductClasses())
					&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getProductClasses(searchQuery)))
				arrString
						.setString(configuration.getSupplierAdditionalInfo().getProductClasses(searchQuery).split(","));
		}
		return arrString;
	}

	private void parseAvailabilityTripResponse(TripAvailabilityResponseVer2 availibiltyResponse)
			throws NoSearchResultException {
		if (Objects.nonNull(availibiltyResponse) && Objects.nonNull(availibiltyResponse.getFares())
				&& Objects.nonNull(availibiltyResponse.getSchedules())) {
			int journeyCount = 0; // 0 (Onward) or 1 (Return)
			Fare[] fare = availibiltyResponse.getFares().getFare();
			ArrayOfJourneyDateMarketVer2[] journeyMarket =
					availibiltyResponse.getSchedules().getArrayOfJourneyDateMarketVer2();
			if (ArrayUtils.isNotEmpty(fare) && ArrayUtils.isNotEmpty(journeyMarket)) {
				for (ArrayOfJourneyDateMarketVer2 journeyDateMarketVer2 : journeyMarket) {
					JourneyDateMarketVer2[] journey = journeyDateMarketVer2.getJourneyDateMarketVer2();
					if (journey != null && ArrayUtils.isNotEmpty(journey)) {
						for (JourneyDateMarketVer2 journeyDateMarketVer21 : journey) {
							boolean isTaxIncluded = journeyDateMarketVer21.getIncludesTaxesAndFees();
							AvailableJourney[] availableJourney =
									journeyDateMarketVer21.getAvailableJourneys().getAvailableJourney();
							searchResult = NavitaireUtils.segregateResult(searchQuery, journeyDateMarketVer21,
									parseTripInfos(availableJourney, isTaxIncluded, fare, journeyCount), searchResult,
									journeyCount, navitaireAirline, bookingUser);
						}
					}
					journeyCount++;
				}
			} else {
				throw new NoSearchResultException(AirSourceConstants.NO_SCHEDULE_FOUND);
			}
		}
	}

	protected TypeOfSale getTypeOfSale() {
		TypeOfSale typeOfSale = new TypeOfSale();
		typeOfSale.setPromotionCode(getPromoCode(searchQuery));
		return typeOfSale;
	}

	public List<TripInfo> parseTripInfos(AvailableJourney[] availability, boolean isTaxIncluded, Fare[] fare,
			int journeyCount) {
		List<TripInfo> tripInfos = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(availability)) {
			for (AvailableJourney listSegment : availability) {
				try {
					TripInfo tripInfo = new TripInfo();
					List<SegmentInfo> segmentInfoList = parseSegmentInfo(listSegment.getAvailableSegment(),
							listSegment.getJourneySellKey(), isTaxIncluded, fare, journeyCount);
					if (hasPriceInfos(segmentInfoList)) {
						tripInfo.setSegmentInfos(segmentInfoList);
						tripInfo.setConnectionTime();
						tripInfos.add(tripInfo);
					}
				} catch (Exception e) {
					log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
				}
			}
		}
		return tripInfos;
	}

	private boolean hasPriceInfos(List<SegmentInfo> segmentInfos) {
		return CollectionUtils.isNotEmpty(segmentInfos)
				&& CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList());
	}

	public List<SegmentInfo> parseSegmentInfo(ArrayOfAvailableSegment segments, String journeyKey,
			boolean isTaxIncluded, Fare[] fare, int journeyCount) {
		List<SegmentInfo> segmentInfoList = new ArrayList<>();
		Integer segmentIndex = 0;
		for (AvailableSegment segment : segments.getAvailableSegment()) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentIndex);
			SegmentMiscInfo segMiscInfo = SegmentMiscInfo.builder().build();
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(segment.getDepartureStation()));
			segmentInfo.setIsReturnSegment(journeyCount == 1 ? Boolean.TRUE : Boolean.FALSE);
			segmentInfo.setDepartTime(TgsDateUtils.calenderToLocalDateTime(segment.getSTD()));
			segmentInfo.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(segment.getSTA()));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(segment.getArrivalStation()));
			segmentInfo.setFlightDesignator(getFlightDesignator(segment));
			segmentInfo.setStopOverAirports(getStopOverAirports(segment.getLegs(), segmentInfo));
			segmentInfo.setDuration(segmentInfo.calculateDuration());
			if (segment.isLegsSpecified() && Objects.nonNull(segment.getLegs())
					&& ArrayUtils.isNotEmpty(segment.getLegs().getLeg())) {
				LegInfo legInfo = segment.getLegs().getLeg()[0].getLegInfo();
				OperationsInfo operationsInfo = segment.getLegs().getLeg()[0].getOperationsInfo();
				if (operationsInfo != null && DepartureStatus.Delayed.equals(operationsInfo.getDepartureStatus())) {
					segmentInfo.setDepartTime(TgsDateUtils.calenderToLocalDateTime(operationsInfo.getETD()));
					segmentInfo.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(operationsInfo.getETA()));
				}
				segmentInfo.getFlightDesignator().setEquipType(legInfo.getEquipmentType());
				if (StringUtils.isNotBlank(legInfo.getOperatingCarrier()))
					segmentInfo.setOperatedByAirlineInfo(AirlineHelper.getAirlineInfo(legInfo.getOperatingCarrier()));
				segmentInfo.getDepartAirportInfo()
						.setTerminal(AirUtils.getTerminalInfo(legInfo.getDepartureTerminal()));
				segmentInfo.getArrivalAirportInfo().setTerminal(AirUtils.getTerminalInfo(legInfo.getArrivalTerminal()));
			}
			segmentInfo.setMiscInfo(segMiscInfo);
			segmentInfo.setStops(CollectionUtils.size(segmentInfo.getStopOverAirports()));
			List<PriceInfo> priceInfo = new ArrayList<PriceInfo>();
			ArrayOfAvailableFare2 availableFare2 = segment.getAvailableFares();
			if (availableFare2.isAvailableFare2Specified()
					&& ArrayUtils.isNotEmpty(availableFare2.getAvailableFare2())) {
				for (AvailableFare2 fareComponent : availableFare2.getAvailableFare2()) {
					PriceInfo pInfo = PriceInfo.builder().build();
					pInfo.setSupplierBasicInfo(configuration.getBasicInfo());
					Map<PaxType, FareDetail> fareDetails =
							parseFareComponents(pInfo, fareComponent, fare, segmentIndex, configuration);
					CabinClass classSearched = NavitaireUtils.getAnyFareDetails(fareDetails);
					if (searchQuery.getCabinClass() == null
							|| searchQuery.getCabinClass().isAllowedCabinClass(classSearched)) {
						pInfo.setFareDetails(fareDetails);
						PriceMiscInfo miscInfo = pInfo.getMiscInfo();
						miscInfo.setJourneyKey(journeyKey);
						if (CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
							miscInfo.setAccountCode(configuration.getSupplierAdditionalInfo().getAccountCodes().get(0));
						}
						NavitaireUtils.setIsSpecialPrivateFare(pInfo, configuration, sourceConfiguration);
						pInfo.setMiscInfo(miscInfo);
						priceInfo.add(pInfo);
						segmentInfo.setPriceInfoList(priceInfo);
					}
				}
				segmentInfoList.add(segmentInfo);
			}
			segmentIndex++;
		}
		return segmentInfoList;
	}

	private List<AirportInfo> getStopOverAirports(ArrayOfLeg legs, SegmentInfo segmentInfo) {
		List<AirportInfo> stops = new ArrayList<>();
		if (legs != null && ArrayUtils.isNotEmpty(legs.getLeg())) {
			Arrays.stream(legs.getLeg()).forEach(leg -> {
				if (!segmentInfo.getArrivalAirportCode().equalsIgnoreCase(leg.getArrivalStation())) {
					stops.add(AirportHelper.getAirport(leg.getArrivalStation().trim()));
				}
			});
		}
		return stops;
	}

	private Map<PaxType, FareDetail> parseFareComponents(PriceInfo priceInfo, AvailableFare2 fareComponent, Fare[] fare,
			int segmentIndex, SupplierConfiguration configuration) {
		Map<PaxType, FareDetail> fareDetails = new ConcurrentHashMap<>();
		int fareIndex = fareComponent.getFareIndex();
		FareDetail fareDetail = priceInfo.getFareDetail(PaxType.ADULT, new FareDetail());
		PaxFare[] paxFare = fare[fareIndex].getPaxFares().getPaxFare();
		fareDetail =
				parsePaxWiseFareBreakUp(fareDetail, getPaxFare(paxFare, PAX_TYPE_ADULT), PAX_TYPE_ADULT, adultCount);
		fareDetail = setCommonPaxFareType(fareDetail, fareComponent, fare, fareIndex);
		fareDetails.put(PaxType.ADULT, fareDetail);
		if (childCount > 0) {
			fareDetail = priceInfo.getFareDetail(PaxType.CHILD, new FareDetail());
			fareDetail = parsePaxWiseFareBreakUp(fareDetail, getPaxFare(paxFare, PAX_TYPE_CHILD), PAX_TYPE_CHILD,
					childCount);
			fareDetail = setCommonPaxFareType(fareDetail, fareComponent, fare, fareIndex);
			fareDetails.put(PaxType.CHILD, fareDetail);
		}
		if (infantCount > 0) {
			fareDetail = priceInfo.getFareDetail(PaxType.INFANT, new FareDetail());
			fareDetails.put(PaxType.INFANT, fareDetail);
		}
		PriceMiscInfo priceMisc = PriceMiscInfo.builder().build();
		priceMisc.setFareKey(fare[fareIndex].getFareSellKey());
		priceInfo.setMiscInfo(priceMisc);
		priceMisc.setClassOfService(fare[fareIndex].getClassOfService());
		navitaireAirline.processPriceInfo(fare[fareIndex].getProductClass(), priceInfo, configuration, searchQuery);
		return fareDetails;
	}

	private PriceInfo setNewFareComponent(PriceInfo priceInfo, PaxFare[] paxFares, AirSearchQuery searchQuery,
			ArrayOfPassenger passenger) {
		Map<PaxType, FareDetail> fareDetails = new ConcurrentHashMap<>();
		FareDetail fareDetail = new FareDetail();
		int paxCount = 0;
		// Adult Fare
		paxCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
		fareDetail =
				parsePaxWiseFareBreakUp(fareDetail, getPaxFare(paxFares, PAX_TYPE_ADULT), PAX_TYPE_ADULT, paxCount);
		fareDetail = copyStaticInfo(fareDetail, priceInfo.getFareDetail(PaxType.ADULT));
		fareDetails.put(PaxType.ADULT, fareDetail);
		// Child Fare
		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD) > 0) {
			fareDetail = new FareDetail();
			paxCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			fareDetail =
					parsePaxWiseFareBreakUp(fareDetail, getPaxFare(paxFares, PAX_TYPE_CHILD), PAX_TYPE_CHILD, paxCount);
			fareDetail = copyStaticInfo(fareDetail, priceInfo.getFareDetail(PaxType.CHILD));
			fareDetails.put(PaxType.CHILD, fareDetail);
		}

		if (AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT) > 0) {
			// Infant Fare
			if (passenger != null && ArrayUtils.isNotEmpty(passenger.getPassenger())) {
				fareDetail = priceInfo.getFareDetail(PaxType.INFANT, new FareDetail());
				Passenger infantPassenger = getPassengerFiltered(passenger.getPassenger(), PAX_TYPE_INFANT);
				if (infantPassenger != null) {
					PassengerFee[] passengerFees = infantPassenger.getPassengerFees().getPassengerFee();
					PaxFare paxFare = new PaxFare();
					paxFare.setPaxType(PAX_TYPE_INFANT);
					for (PassengerFee fee : passengerFees) {
						if (fee.getServiceCharges() != null
								&& ArrayUtils.isNotEmpty(fee.getServiceCharges().getBookingServiceCharge())) {
							parsePaxWiseFareBreakUp(fareDetail, paxFare, PAX_TYPE_INFANT, 1,
									fee.getServiceCharges().getBookingServiceCharge());
						}
					}
				}
				fareDetails.put(PaxType.INFANT, fareDetail);
			} else {
				fareDetail = priceInfo.getFareDetail(PaxType.INFANT, new FareDetail());
				// fareDetail.setFareComponents(new HashMap<>());
				fareDetails.put(PaxType.INFANT, fareDetail);
			}
		}
		priceInfo.setFareDetails(fareDetails);
		return priceInfo;
	}

	private Passenger getPassengerFiltered(Passenger[] passegers, String paxType) {
		Passenger passenger = null;
		for (Passenger pax : passegers) {
			ArrayOfPassengerFee passengerFee = pax.getPassengerFees();
			if (passengerFee != null && ArrayUtils.isNotEmpty(passengerFee.getPassengerFee())) {
				for (PassengerFee passengerFee1 : passengerFee.getPassengerFee()) {
					if (passengerFee1.getSSRCode().equalsIgnoreCase(paxType)) {
						passenger = pax;
						break;
					}
				}
			}
		}
		return passenger;
	}

	public static PaxFare getPaxFare(PaxFare[] paxFares, String paxType) {
		PaxFare fare = null;
		if (ArrayUtils.isNotEmpty(paxFares)) {
			for (PaxFare paxFare : paxFares) {
				if (paxType.equalsIgnoreCase(paxFare.getPaxType())) {
					fare = paxFare;
					break;
				}
			}
		}
		return fare;
	}

	private static FareDetail copyStaticInfo(FareDetail newFareDetail, FareDetail oldFareDetail) {
		if (newFareDetail != null && oldFareDetail != null) {
			newFareDetail.setCabinClass(oldFareDetail.getCabinClass());
			newFareDetail.setClassOfBooking(oldFareDetail.getClassOfBooking());
			newFareDetail.setFareBasis(oldFareDetail.getFareBasis());
			newFareDetail.setFareType(oldFareDetail.getFareType());
			newFareDetail.setIsHandBaggage(oldFareDetail.getIsHandBaggage());
			newFareDetail.setIsMealIncluded(oldFareDetail.getIsMealIncluded());
			newFareDetail.setRefundableType(oldFareDetail.getRefundableType());
			newFareDetail.setSeatRemaining(oldFareDetail.getSeatRemaining());
		}
		return newFareDetail;
	}

	private FareDetail setCommonPaxFareType(FareDetail paxWiseFare, AvailableFare2 fareComponent, Fare[] fare,
			int fareIndex) {
		paxWiseFare.setSeatRemaining((int) fareComponent.getAvailableCount());
		paxWiseFare.setFareBasis(fare[fareIndex].getFareBasisCode());
		paxWiseFare.setClassOfBooking(fare[fareIndex].getProductClass());
		paxWiseFare.setCabinClass(CabinClass.ECONOMY);
		paxWiseFare.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
		return paxWiseFare;
	}

	private FareDetail parsePaxWiseFareBreakUp(FareDetail fareDetail, PaxFare paxFare, String paxType, int paxCount) {
		if (Objects.nonNull(paxFare) && paxFare.getPaxType().equalsIgnoreCase(paxType)) {
			return setPaxWiseFareBreakUp(fareDetail, paxFare, paxCount,
					paxFare.getServiceCharges().getBookingServiceCharge());
		}
		return fareDetail;
	}

	public com.tgs.services.fms.datamodel.FlightDesignator getFlightDesignator(AvailableSegment segment) {
		com.tgs.services.fms.datamodel.FlightDesignator designator = com.tgs.services.fms.datamodel.FlightDesignator
				.builder().flightNumber(segment.getFlightDesignator().getFlightNumber().trim())
				.airlineInfo(AirlineHelper.getAirlineInfo(segment.getFlightDesignator().getCarrierCode())).build();
		return designator;
	}

	public List<TripInfo> getGetItineraryPrice(List<Pair<TripInfo, Integer>> flightList, boolean isSearch) {
		PriceItineraryResponse priceItineraryResponse = null;
		PriceItineraryRequest priceItineraryRequest = null;
		List<TripInfo> tripList = new ArrayList<>();
		try {
			priceItineraryRequest = new PriceItineraryRequest();
			ItineraryPriceRequest itineraryPriceRequest = new ItineraryPriceRequest();
			PriceItineraryBy priceItinBy = navitaireAirline.getPriceItineraryByValue();
			TypeOfSale typeOfSale = getTypeOfSale();
			itineraryPriceRequest.setTypeOfSale(typeOfSale);
			if (navitaireAirline.getIsSSRInSellJourney()) {
				itineraryPriceRequest.setSSRRequest(null);
			} else {
				itineraryPriceRequest.setSSRRequest(getInfantSSR(flightList.get(0).getFirst().getSegmentInfos(), true));
			}
			itineraryPriceRequest.setSellRequest(null);
			itineraryPriceRequest.setPriceItineraryBy(priceItinBy);
			itineraryPriceRequest.setSellByKeyRequest(getSellJourneyByKeyRequestData(flightList, true, isSearch));
			itineraryPriceRequest.setBookingStatus(BookingStatus.Default);
			priceItineraryRequest.setItineraryPriceRequest(itineraryPriceRequest);
			listener.setType(AirUtils.getLogType("3-ITINPRICE", configuration));
			if (!bookingBinding._getServiceClient().getAxisService()
					.hasMessageContextListener(SoapRequestResponseListner.class)) {
				bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			}
			priceItineraryResponse = bookingBinding.getItineraryPrice(priceItineraryRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			tripList = parseTripPriceResponse(priceItineraryResponse, flightList, searchQuery, bookingId, typeOfSale,
					isSearch);
		} catch (AxisFault e) {
			addExceptionToLogger(e);
			log.info("Itinerary Price AxisError searchid {} due to {} ", searchQuery.getSearchId(), e.getMessage());
		} catch (Exception e) {
			addExceptionToLogger(e);
			log.error("Error occured on Itinerary Price for search {} supplier {} booking {} ",
					searchQuery.getSearchId(), getSupplierDesc(), bookingId, e);
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return tripList;
	}

	private List<TripInfo> parseTripPriceResponse(PriceItineraryResponse priceItineraryResponse,
			List<Pair<TripInfo, Integer>> flightList, AirSearchQuery searchQuery, String bookingId,
			TypeOfSale typeOfSale, boolean isSearch) {
		Booking booking = priceItineraryResponse.getBooking();

		ArrayOfJourney journey = booking.getJourneys();
		ArrayOfPassenger passenger = isSearch ? booking.getPassengers() : null;
		List<TripInfo> tripList = NavitaireUtils.collectTripInfos(flightList);

		if (journey != null && ArrayUtils.isNotEmpty(journey.getJourney())) {
			for (Journey journeys : booking.getJourneys().getJourney()) {
				TripInfo tripInfo = null;
				int segmentIndex = 0;
				for (Segment segment : journeys.getSegments().getSegment()) {
					try {
						tripInfo = NavitaireUtils.findTripInfo(tripList, journeys.getJourneySellKey());
						if (tripInfo != null) {
							SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(segmentIndex);
							for (Fare fare : segment.getFares().getFare()) {
								PriceInfo priceInfo = NavitaireUtils.getTripPriceInfo(
										tripInfo.getSegmentInfos().get(segmentIndex), fare.getFareSellKey());
								if (Objects.nonNull(priceInfo) && Objects.nonNull(fare)
										&& Objects.nonNull(fare.getPaxFares())) {
									setNewFareComponent(priceInfo, fare.getPaxFares().getPaxFare(), searchQuery,
											passenger);
									passenger = null;
								}
								if (typeOfSale != null && StringUtils.isNotEmpty(typeOfSale.getPromotionCode())) {
									priceInfo.getMiscInfo().setAccountCode(typeOfSale.getPromotionCode());
								}
							}
							ArrayOfPaxSegment paxSegment = segment.getPaxSegments();
							parsePaxSegments(paxSegment, segmentInfo);
						}
					} catch (Exception e) {
						log.error("Fare Parsing failed for booking Id {} trip {} excep ", bookingId,
								tripInfo.toString(), e);
					}
					segmentIndex++;
				}
			}
		}
		NavitaireUtils.setTotalFareOnTripInfo(tripList);
		return tripList;
	}

	private void parsePaxSegments(ArrayOfPaxSegment paxSegments, SegmentInfo segmentInfo) {
		if (paxSegments != null && ArrayUtils.isNotEmpty(paxSegments.getPaxSegment())) {
			PaxSegment paxSegment = paxSegments.getPaxSegment()[0];
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
					if (!paxType.equals(PaxType.INFANT)) {
						BaggageInfo baggageInfo = fareDetail.getBaggageInfo();
						String allowance = StringUtils.join(paxSegment.getBaggageAllowanceWeight(),
								paxSegment.getBaggageAllowanceWeightType().getValue());
						baggageInfo.setAllowance(allowance);
						fareDetail.setBaggageInfo(baggageInfo);
					}
				});
			});
		}
	}

	public void baggageAllowance(TripInfo tripInfo, String bookingId) {
		if (navitaireAirline.isBaggageAllowanceFromAPI()) {
			GetBaggageAllowancesRequest baggageAllowancesRQ = new GetBaggageAllowancesRequest();
			try {
				baggageAllowancesRQ.setGetBaggageAllowancesRequestData(getBaggageAllowanceRequest(tripInfo));
				listener.setType("C-BAGGAGE");
				bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
				GetBaggageAllowancesResponse allowancesResponse =
						bookingBinding.getBaggageAllowances(baggageAllowancesRQ, getContractVersion(),
								getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
				parseBaggageAllowanceResponse(allowancesResponse, tripInfo);
			} catch (Exception e) {
				addExceptionToLogger(e);
				log.error("Baggage allowance error occured for booking {}", bookingId, e);
			} finally {
				bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
			}
		}
	}

	private void parseBaggageAllowanceResponse(GetBaggageAllowancesResponse allowancesResponse, TripInfo tripInfo) {
		GetBaggageAllowancesResponseData baggageAllowance = allowancesResponse.getGetBaggageAllowancesResponseData();
		if (baggageAllowance != null) {
			ArrayOfBaggageAllowance arrayOfBaggageAllowance = baggageAllowance.getBaggageAllowanceList();
			if (arrayOfBaggageAllowance != null
					&& ArrayUtils.isNotEmpty(arrayOfBaggageAllowance.getBaggageAllowance())) {
				BaggageAllowance[] baggageAllowances = arrayOfBaggageAllowance.getBaggageAllowance();
				tripInfo.getSegmentInfos().forEach(segmentInfo -> {
					BaggageAllowance filteredBaggage = getFilteredBaggageAllownace(baggageAllowances, segmentInfo);
					if (filteredBaggage != null) {
						boolean isInfantAppl = filteredBaggage.getForInfant();
						String baggageWeight =
								StringUtils.join(filteredBaggage.getTotalWeight(), filteredBaggage.getWeightUnitType());
						if (StringUtils.isNotBlank(baggageWeight)) {
							segmentInfo.getPriceInfoList().forEach((priceInfo -> {
								priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
									if (!paxType.equals(PaxType.INFANT) || isInfantAppl) {
										fareDetail.getBaggageInfo().setAllowance(baggageWeight);
									}
								}));
							}));
						}
					}
				});
			}

		}
	}

	private BaggageAllowance getFilteredBaggageAllownace(BaggageAllowance[] baggageAllowances,
			SegmentInfo segmentInfo) {
		AtomicReference<BaggageAllowance> baggageAllowance1 = new AtomicReference<>();
		Arrays.stream(baggageAllowances).forEach(allowance -> {
			AtomicReference<String> segmentKey = new AtomicReference<>();
			Arrays.stream(allowance.getFlightKeyList().getString()).forEach(flight -> {
				segmentKey.set(StringUtils.join(segmentKey.get(), flight));
			});
			String segmentRef = segmentInfo.getDepartureAndArrivalAirport().replaceAll("-", "");
			if (segmentKey.get().contains(segmentRef)) {
				baggageAllowance1.set(allowance);
			} else {
				baggageAllowance1.set(null);
			}
		});
		return baggageAllowance1.get();
	}

	public GetBaggageAllowancesRequestData getBaggageAllowanceRequest(TripInfo tripInfo) {
		GetBaggageAllowancesRequestData requestData = new GetBaggageAllowancesRequestData();
		requestData.setIncludeUsageDetails(true);
		return requestData;
	}

	public AirSearchResult doSearchItineraryPrice(AirSearchResult searchResult, boolean isItineraryPrice) {
		if (isItineraryPrice && navitaireAirline.isItineraryPriceOnSearch() && searchResult != null
				&& MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			AtomicLong startTime = new AtomicLong(0);
			AtomicLong endTime = new AtomicLong(0);
			AtomicInteger totalTripSize = new AtomicInteger(0);
			AtomicInteger batchTripsCount = new AtomicInteger(0);
			AtomicInteger itinPriceRqCount = new AtomicInteger(0);
			try {
				listener.setStoreLogs(true);
				List<Future<List<TripInfo>>> futureTaskList = new ArrayList<>();
				startTime.set(System.currentTimeMillis());
				searchResult.getTripInfos().forEach((key, trips) -> {
					totalTripSize.set(totalTripSize.get() + trips.size());
					if (sourceConfiguration != null) {
						List<FareComponent> components = sourceConfiguration.getCacheableFareComponents();
						FareComponentHelper.fetchTripFareComponents(trips, components,
								navitaireAirline.getMappedFareComponents(), configuration);
					}

					List<List<Pair<TripInfo, Integer>>> batchTrips = new ArrayList<>();
					if (navitaireAirline.isAllowedInBucket()) {
						int batchSize = NavitaireUtils.getBatchSize(trips, searchQuery);
						batchTrips = NavitaireUtils.getBatchTrips(trips, batchSize, sourceConfiguration);
					} else {
						batchTrips = NavitaireUtils.getTripsInBucket(trips, 5, sourceConfiguration);
					}
					batchTrips.forEach(batch -> {
						batchTripsCount.set(batchTripsCount.get() + batch.size());
						futureTaskList.add(ExecutorUtils.getFlightSearchThreadPool()
								.submit(() -> this.getGetItineraryPrice(batch, true)));
					});
					log.debug(
							"Total number of trips {} number of pricing calls {} pricing fetched from cache {} for the search {}",
							trips.size(), batchTripsCount.get(), trips.size() - batchTripsCount.get(),
							searchQuery.getSearchId());
				});
				log.debug("Size of Itinerary Price batch size {} for search {}", futureTaskList.size(),
						searchQuery.getSearchId());
				futureTaskList.forEach(task -> {
					try {
						task.get(30, TimeUnit.SECONDS);
					} catch (InterruptedException | ExecutionException | TimeoutException e) {
						log.info(AirSourceConstants.AIR_SUPPLIER_SEARCH_PRICE_THREAD, searchQuery, e);
						throw new NoSearchResultException("ItineraryPrice Thread Interrupted ");
					}
				});
				endTime.set(System.currentTimeMillis());
				itinPriceRqCount.set(futureTaskList.size());
			} finally {
				cacheFareComponent(searchResult);
				searchResult = this.splitAndProcessPricedItinerary(searchResult);
				listener.setStoreLogs(true);
				long searchtime = Long.valueOf(endTime.longValue() - startTime.longValue());
				if (totalTripSize.get() > 0) {
					SupplierAnalyticsQuery supplierAnalyticsQuery = SupplierAnalyticsQuery.builder()
							.tripsize(totalTripSize.get()).batchsize(itinPriceRqCount.get())
							.cacheapplied(totalTripSize.get() - batchTripsCount.get())
							.supplierhit(batchTripsCount.get()).flowtype(SupplierFlowType.ITINERARY_PRICE.name())
							.searchtimeinsec(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(searchtime)).intValue())
							.build();
					supplierAnalyticInfos.add(supplierAnalyticsQuery);
				}
			}
		}
		return searchResult;
	}


	private void cacheFareComponent(AirSearchResult searchResult) {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			for (Entry<String, List<TripInfo>> entry : searchResult.getTripInfos().entrySet()) {
				NavitaireUtils.cacheFareComponents(entry.getValue(), sourceConfiguration, configuration);
			}
		}
	}

	public AirSearchResult setInfantPrice(AirSearchResult searchResult, NavitaireSessionManager sessionManager) {
		try {
			AtomicBoolean isInfantFarePresentInTrip = new AtomicBoolean();
			if (navitaireAirline.isInfantFareFixed() && searchResult != null
					&& MapUtils.isNotEmpty(searchResult.getTripInfos()) && infantCount > 0) {
				if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromCache())
						&& StringUtils.isBlank(getPromoCode(searchQuery))) {
					fetchAndSetInfantFareFromCache(searchResult, isInfantFarePresentInTrip);
				}
				if (!isInfantFarePresentInTrip.get()) {
					NavitaireSSRManager ssrManager = NavitaireSSRManager.builder().configuration(configuration)
							.searchQuery(searchQuery).sessionSignature(sessionManager.sessionSignature)
							.bookingId(bookingId).moneyExchnageComm(moneyExchnageComm).listener(listener)
							.bookingBinding(bookingBinding).sourceConfiguration(sourceConfiguration)
							.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
					NavitaireBookingManager reviewManager =
							NavitaireBookingManager.builder().configuration(configuration).searchQuery(searchQuery)
									.sessionSignature(sessionManager.getSessionSignature()).listener(listener)
									.moneyExchnageComm(moneyExchnageComm).bookingBinding(bookingBinding)
									.sourceConfiguration(sourceConfiguration).bookingUser(bookingUser).build();
					reviewManager.init();
					ssrManager.init();
					try {
						searchResult.getTripInfos().forEach((tripType, trips) -> {
							if (CollectionUtils.isNotEmpty(trips)) {
								infantFare = new FareDetail();
								sellInfantFare(reviewManager, sessionManager, ssrManager, trips);
							}
						});
					} catch (NoSearchResultException nos) {
						addExceptionToLogger(nos);
						searchResult = null;
					}
				}
			}

		} catch (Exception e) {
			addExceptionToLogger(e);
			throw new NoSearchResultException(StringUtils.join(",", criticalMessageLogger));
		}
		return searchResult;
	}

	private void fetchAndSetInfantFareFromCache(AirSearchResult searchResult, AtomicBoolean isInfantFarePresent) {
		searchResult.getTripInfos().forEach((tripType, trips) -> {
			infantFare = new FareDetail();
			infantFare = FareComponentHelper.getInfantfareFromCache(searchQuery, sourceId);
			if (CollectionUtils.isNotEmpty(trips) && infantFare != null
					&& MapUtils.isNotEmpty(infantFare.getFareComponents())) {
				isInfantFarePresent.set(Boolean.TRUE.booleanValue());
				for (TripInfo tripInfo : trips) {
					tripInfo.getSegmentInfos().forEach(segmentInfo -> {
						if (segmentInfo.getSegmentNum() == 0 && navitaireAirline.isInfantFareOnJourneyWise()) {
							copyInfantFareToSegment(segmentInfo, infantFare);
						}
					});
				}
				NavitaireUtils.setTotalFareOnTripInfo(trips);
			}
		});

	}

	private void sellInfantFare(NavitaireBookingManager reviewManager, NavitaireSessionManager sessionManager,
			NavitaireSSRManager ssrManager, List<TripInfo> copyTripInfos) {
		int index = 0;
		TripInfo tripInfo = null;
		boolean isSuccess = false;
		do {
			try {
				sessionManager.openSession();
				reviewManager.setSessionSignature(sessionManager.getSessionSignature());
				ssrManager.setSessionSignature(sessionManager.getSessionSignature());
				ssrManager.setSellAllSSR(true);
				tripInfo = copyTripInfos.get(index);
				reviewManager.sendSellJourneyRequest(Arrays.asList(tripInfo));
				ssrManager.doSSRSearch(tripInfo);
				removeSSROnSearch(tripInfo);
				reviewManager.setInfantFareToTrip(copyTripInfos, tripInfo);
				isSuccess = true;
			} catch (Exception e) {
				index++;
				addExceptionToLogger(e);
				log.info("Infant Fare is Not available checking for another trip {}", tripInfo, e);
			} finally {
				sessionManager.closeSession();
				ssrManager.setSellAllSSR(false);
			}
		} while (!isSuccess && index - 1 < copyTripInfos.size() - 1);
		if (!isSuccess && index == copyTripInfos.size()) {
			throw new NoSearchResultException("Infant Fare Failed for all trips");
		}
	}

	private void removeSSROnSearch(TripInfo tripInfo) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.setSsrInfo(null);
		});
	}

	public void setInfantPriceOnReview(List<TripInfo> newTrips) {
		if (CollectionUtils.isNotEmpty(newTrips)) {
			TripInfo selectedTrip = newTrips.get(0);
			AirUtils.splitTripInfo(selectedTrip, false).forEach(trip -> {
				if (infantCount > 0) {
					// As Selling Infant while booking copy infant from Old Trip
					trip.getSegmentInfos().forEach(segmentInfo -> {
						FareDetail infantFare = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.INFANT);
						setInfantFareToTrip(segmentInfo, infantFare);
					});
				}
				NavitaireUtils.setTotalFareOnTripInfo(Arrays.asList(trip));
				NavitaireUtils.cacheFareComponents(Arrays.asList(trip), sourceConfiguration, configuration);
			});
		}
	}

	public AirSearchResult processSearchResult(AirSearchResult searchResult) {
		if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos()) && searchQuery.isIntlReturn()) {
			searchResult = NavitaireUtils.processSearchResultForItineraryPrice(searchResult,
					configuration.getSourceId(), searchQuery.isIntlReturn(), bookingUser);
			if (searchResult.getTripInfos().get(TripInfoType.COMBO.name()) != null) {
				log.info("Number of combination made for itinerary pricing is {} for searchId {}",
						searchResult.getTripInfos().get(TripInfoType.COMBO.name()).size(), searchQuery.getSearchId());
			}
		}
		return searchResult;
	}

	public AirSearchResult splitAndProcessPricedItinerary(AirSearchResult searchResult) {
		if (searchQuery.isIntlReturn() && searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())
				&& CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(TripInfoType.COMBO.name()))) {
			searchResult = NavitaireUtils.splitItineraryPricedTrips(searchResult);
			searchResult = AirSearchResultProcessingManager.processSearchTypeResult(searchResult,
					configuration.getSourceId(), searchQuery.isIntlReturn(), bookingUser);
		}
		return searchResult;
	}

}
