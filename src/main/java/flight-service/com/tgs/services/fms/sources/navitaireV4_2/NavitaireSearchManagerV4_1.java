package com.tgs.services.fms.sources.navitaireV4_2;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.StopWatch;
import org.datacontract.schemas._2004._07.navitaire_newskies_webservices_datacontracts_common_enumerations.LoyaltyFilter;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.DepartureStatus;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfAvailabilityRequest;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfFare;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfJourney;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfJourneyDateMarket;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfLeg;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaxPriceType;
import tgs.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSegment;
import tgs.navitaire.schemas.webservices.datacontracts.booking.AvailabilityRequest;
import tgs.navitaire.schemas.webservices.datacontracts.booking.AvailableFare;
import tgs.navitaire.schemas.webservices.datacontracts.booking.BookingServiceCharge;
import tgs.navitaire.schemas.webservices.datacontracts.booking.Fare;
import tgs.navitaire.schemas.webservices.datacontracts.booking.LegInfo;
import tgs.navitaire.schemas.webservices.datacontracts.booking.OperationsInfo;
import tgs.navitaire.schemas.webservices.datacontracts.booking.PaxFare;
import tgs.navitaire.schemas.webservices.datacontracts.booking.PaxPriceType;
import tgs.navitaire.schemas.webservices.datacontracts.booking.Segment;
import tgs.navitaire.schemas.webservices.datacontracts.booking.TripAvailabilityRequest;
import tgs.navitaire.schemas.webservices.datacontracts.booking.TripAvailabilityResponse;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.ArrayOfJourneySortKey;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.AvailabilityFilter;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.AvailabilityType;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.DOW;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.FareClassControl;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.FareRuleFilter;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.FlightType;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.InboundOutbound;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.JourneySortKey;
import tgs.navitaire.schemas.webservices.datacontracts.common.enumerations.SSRCollectionsMode;
import tgs.navitaire.schemas.webservices.servicecontracts.bookingservice.GetAvailabilityByTripResponse;
import tgs.navitaire.schemas.webservices.servicecontracts.bookingservice.GetAvailabilityRequest;

@Slf4j
@SuperBuilder
final class NavitaireSearchManagerV4_1 extends NavitaireServiceManager {

	protected AirSearchResult searchResult;

	public AirSearchResult doSearch(boolean isSearch) {
		try {
			searchResult = new AirSearchResult();
			GetAvailabilityByTripResponse response = null;
			ArrayOfAvailabilityRequest availability = new ArrayOfAvailabilityRequest();
			AvailabilityRequest[] availabilityRequests = new AvailabilityRequest[searchQuery.getRouteInfos().size()];
			int avilabilityIndex = 0;
			for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
				AvailabilityRequest request = new AvailabilityRequest();
				setCommonAvailabilityRequest(request, isSearch);
				setAirSearchReqProperty(request, routeInfo);
				availabilityRequests[avilabilityIndex] = request;
				avilabilityIndex++;
			}
			availability.setAvailabilityRequest(availabilityRequests);
			GetAvailabilityRequest availabilityRequest = new GetAvailabilityRequest();
			TripAvailabilityRequest tripAvailabilityRequest = new TripAvailabilityRequest();
			tripAvailabilityRequest.setAvailabilityRequests(availability);
			tripAvailabilityRequest.setLoyaltyFilter(LoyaltyFilter.MonetaryOnly);
			tripAvailabilityRequest.setLowFareMode(false);
			availabilityRequest.setTripAvailabilityRequest(tripAvailabilityRequest);
			listener.setType(AirUtils.getLogType("S-GETAVAIL", configuration));
			bookingStubV3._getServiceClient().getAxisService().addMessageContextListener(listener);
			SystemContextHolder.getContextData().setSearchWatch(new StopWatch());
			SystemContextHolder.getContextData().getSearchWatch().start();
			log.debug("Sending search request for searchQuery {}", searchQuery);
			response = getAvailabilityResponse(availabilityRequest);
			log.debug("Received search response for searchQuery {}", searchQuery);
			log.debug("Total time took to process key {} is {}", searchQuery.getSearchId(),
					SystemContextHolder.getContextData().getSearchWatch().getTime());
			searchResult = parseTripResponse(response);
			return searchResult;
		} finally {
			bookingStubV3._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private GetAvailabilityByTripResponse getAvailabilityResponse(GetAvailabilityRequest availabilityRequest)
			throws SupplierRemoteException, NotOperatingSectorException, NoSearchResultException {
		GetAvailabilityByTripResponse response = null;
		try {
			response = bookingStubV3.getAvailability(availabilityRequest, getContractVersionV41(),
					getEnableExceptionVersionV41(), getMessageContractversion1(), getSignatureV41(), getSourceId());
		} catch (AxisFault e) {
			if (NavitaireUtils.isStationNotExist(e)) {
				throw new NotOperatingSectorException(e.getMessage());
			}
			throw new NoSearchResultException(e.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		}
		return response;
	}

	private AirSearchResult parseTripResponse(GetAvailabilityByTripResponse response) {
		TripAvailabilityResponse tripAvailabilityResponse = response.getGetTripAvailabilityResponse();
		if (Objects.nonNull(tripAvailabilityResponse.getSchedules())
				&& ArrayUtils.isNotEmpty(tripAvailabilityResponse.getSchedules().getArrayOfJourneyDateMarket())) {
			ArrayOfJourneyDateMarket[] journeyMarket =
					tripAvailabilityResponse.getSchedules().getArrayOfJourneyDateMarket();

			AtomicInteger journeyCount = new AtomicInteger(0);
			for (ArrayOfJourneyDateMarket journeyDateMarket : journeyMarket) {
				AtomicInteger marketCount = new AtomicInteger(0);
				List<TripInfo> tripInfos = new ArrayList<>();
				if (journeyDateMarket.getJourneyDateMarket() != null) {
					ArrayOfJourney ofJourneyS =
							journeyDateMarket.getJourneyDateMarket()[marketCount.intValue()].getJourneys();
					if (ofJourneyS != null && ofJourneyS.getJourney() != null) {
						Arrays.stream(ofJourneyS.getJourney()).forEach(journey -> {
							String journeySellKey = journey.getJourneySellKey();
							TripInfo tripInfo = new TripInfo();
							List<SegmentInfo> segmentInfos =
									getSegmentInfos(journey.getSegments(), journeySellKey, journeyCount.intValue());
							if (CollectionUtils.isNotEmpty(segmentInfos) && hasPriceInfos(segmentInfos)) {
								tripInfo.setSegmentInfos(segmentInfos);
								tripInfo.setConnectionTime();
								tripInfos.add(tripInfo);
							}
						});
					}
				}
				if (ArrayUtils.isNotEmpty(journeyMarket) && journeyMarket[journeyCount.intValue()] != null
						&& ArrayUtils.isNotEmpty(journeyMarket[journeyCount.intValue()].getJourneyDateMarket())) {
					searchResult = NavitaireUtils.segregateResult(searchQuery,
							journeyMarket[journeyCount.intValue()].getJourneyDateMarket()[marketCount.intValue()],
							tripInfos, searchResult, journeyCount.intValue(), navitaireAirline, bookingUser);
				}
				journeyCount.getAndIncrement();
			}
		} else {
			throw new NoSearchResultException();
		}
		return searchResult;
	}

	private List<SegmentInfo> getSegmentInfos(ArrayOfSegment journey, String journeyKey, Integer journeyCount) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		try {
			if (journey != null && ArrayUtils.isNotEmpty(journey.getSegment())) {
				AtomicInteger segmentNum = new AtomicInteger(0);
				for (Segment segment : journey.getSegment()) {
					SegmentInfo segmentInfo = new SegmentInfo();
					segmentInfo.setSegmentNum(segmentNum.intValue());
					segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(segment.getDepartureStation()));
					segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(segment.getArrivalStation()));
					segmentInfo.setDepartTime(TgsDateUtils.calenderToLocalDateTime(segment.getSTD()));
					segmentInfo.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(segment.getSTA()));
					segmentInfo.setFlightDesignator(getFlightDesignator(segment));
					segmentInfo.setDuration(segmentInfo.calculateDuration());
					segmentInfo.setIsReturnSegment(Boolean.FALSE);
					segmentInfo.setStopOverAirports(getStopOverAirports(segment.getLegs(), segmentInfo));

					if (journeyCount.intValue() == 1) {
						segmentInfo.setIsReturnSegment(Boolean.TRUE);
					}
					if (segment.isLegsSpecified() && Objects.nonNull(segment.getLegs())
							&& ArrayUtils.isNotEmpty(segment.getLegs().getLeg())) {
						LegInfo legInfo = segment.getLegs().getLeg()[0].getLegInfo();
						OperationsInfo operationsInfo = segment.getLegs().getLeg()[0].getOperationsInfo();
						if (operationsInfo != null
								&& DepartureStatus.Delayed.equals(operationsInfo.getDepartureStatus())) {
							segmentInfo.setDepartTime(TgsDateUtils.calenderToLocalDateTime(operationsInfo.getETD()));
							segmentInfo.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(operationsInfo.getETA()));
						}
						if (legInfo != null) {
							if (StringUtils.isNotBlank(legInfo.getOperatingCarrier())) {
								segmentInfo.setOperatedByAirlineInfo(
										AirlineHelper.getAirlineInfo(legInfo.getOperatingCarrier()));
							}
							segmentInfo.getDepartAirportInfo()
									.setTerminal(AirUtils.getTerminalInfo(legInfo.getDepartureTerminal()));
							segmentInfo.getArrivalAirportInfo()
									.setTerminal(AirUtils.getTerminalInfo(legInfo.getArrivalTerminal()));
						}
					}
					segmentInfo.setStops(CollectionUtils.size(segmentInfo.getStopOverAirports()));
					ArrayOfFare arrayOfFare = segment.getFares();
					List<PriceInfo> priceInfos = new ArrayList<>();
					if (arrayOfFare != null && ArrayUtils.isNotEmpty(arrayOfFare.getFare())) {
						for (Fare fare : arrayOfFare.getFare()) {
							int seatCount = ((AvailableFare) fare).getAvailableCount();
							// To Handle Seat Count
							if (seatCount > 0 && AirUtils.getPaxCount(searchQuery, false) <= seatCount) {
								// To Handle Seat Count
								Map<PaxType, FareDetail> fareDetails =
										getPaxWiseFareDetail(fare, ((AvailableFare) fare).getAvailableCount());
								CabinClass classSearched = NavitaireUtils.getAnyFareDetails(fareDetails);
								if (searchQuery.getCabinClass() == null
										|| searchQuery.getCabinClass().isAllowedCabinClass(classSearched)) {
									PriceInfo priceInfo = PriceInfo.builder().build();
									priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
									priceInfo.setMiscInfo(
											PriceMiscInfo.builder().fareKey(fare.getFareSellKey()).build());
									priceInfo.setFareDetails(fareDetails);
									navitaireAirline.processPriceInfo(fare.getProductClass(), priceInfo, configuration,
											searchQuery);
									priceInfo.getMiscInfo().setClassOfService(fare.getClassOfService());
									priceInfo.getMiscInfo().setJourneyKey(journeyKey);
									priceInfos.add(priceInfo);
								}
							}
						}
						if (CollectionUtils.isNotEmpty(priceInfos)) {
							segmentInfo.setPriceInfoList(priceInfos);
						} else {
							segmentInfos = null;
							break;
						}
					}
					segmentInfos.add(segmentInfo);
					segmentNum.getAndIncrement();
				}
			}
		} catch (Exception e) {
			segmentInfos = new ArrayList<>();
			log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
		}
		return segmentInfos;
	}

	private List<AirportInfo> getStopOverAirports(ArrayOfLeg legs, SegmentInfo segmentInfo) {
		List<AirportInfo> stops = new ArrayList<>();
		if (legs != null && ArrayUtils.isNotEmpty(legs.getLeg())) {
			Arrays.stream(legs.getLeg()).forEach(leg -> {
				if (!segmentInfo.getArrivalAirportCode().equalsIgnoreCase(leg.getArrivalStation())) {
					stops.add(AirportHelper.getAirport(leg.getArrivalStation().trim()));
				}
			});
		}
		return stops;
	}

	private boolean hasPriceInfos(List<SegmentInfo> segmentInfos) {
		return CollectionUtils.isNotEmpty(segmentInfos)
				&& CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList());
	}

	private Map<PaxType, FareDetail> getPaxWiseFareDetail(Fare fare, int seatCount) {
		Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
		if (fare != null && fare.getPaxFares() != null && ArrayUtils.isNotEmpty(fare.getPaxFares().getPaxFare())) {
			PaxFare[] paxFares = fare.getPaxFares().getPaxFare();
			if (ArrayUtils.isNotEmpty(paxFares)) {
				Arrays.stream(paxFares).forEach(paxFare -> {
					FareDetail fareDetail = new FareDetail();
					String paxType = paxFare.getPaxType();
					if (paxType.equalsIgnoreCase(PAX_TYPE_CHILD)) {
						paxType = PaxType.CHILD.getType();
					}
					fareDetail.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
					fareDetail.setClassOfBooking(fare.getProductClass());
					fareDetail.setSeatRemaining(seatCount);
					fareDetail.setFareBasis(fare.getFareBasisCode());
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					BookingServiceCharge[] serviceCharges = paxFare.getServiceCharges().getBookingServiceCharge();
					setPaxWiseFareBreakUpV4(fareDetail, paxFare,
							searchQuery.getPaxInfo().getOrDefault(PaxType.getPaxType(paxType), 1), serviceCharges);
					fareDetailMap.put(PaxType.getPaxType(paxType), fareDetail);
				});
			} else {
				// As Fare is empty
				fareDetailMap.putAll(getDefaultFareDetail(fare, seatCount));
			}
		} else {
			// As Fare is empty
			fareDetailMap.putAll(getDefaultFareDetail(fare, seatCount));
		}
		return fareDetailMap;
	}

	public Map<PaxType, FareDetail> getDefaultFareDetail(Fare fare, int seatCount) {
		Map<PaxType, FareDetail> fareDetailMap = new HashMap<>();
		searchQuery.getPaxInfo().forEach((paxType, count) -> {
			if (count > 0) {
				FareDetail fareDetail = new FareDetail();
				fareDetail.setFareComponents(new HashMap<>());
				fareDetail.setRefundableType(RefundableType.PARTIAL_REFUNDABLE.getRefundableType());
				fareDetail.setClassOfBooking(fare.getProductClass());
				fareDetail.setSeatRemaining(seatCount);
				fareDetail.setFareBasis(fare.getFareBasisCode());
				fareDetail.setCabinClass(searchQuery.getCabinClass());
				fareDetailMap.put(paxType, fareDetail);
			}
		});
		return fareDetailMap;
	}

	private void setCommonAvailabilityRequest(AvailabilityRequest request, boolean isSearch) {
		request.setCarrierCode(getAirlineCode());
		request.setDow(DOW.Daily);
		request.setAvailabilityType(AvailabilityType.Default);
		request.setAvailabilityFilter(AvailabilityFilter.Default);
		request.setMaximumConnectingFlights(
				navitaireAirline.getConnectingFlightsCount(searchQuery.getIsDomestic(), sourceConfiguration));
		request.setSSRCollectionsMode(SSRCollectionsMode.None);
		request.setNightsStay(0);
		request.setIncludeAllotments(false);
		request.setFlightType(FlightType.All);
		request.setMinimumFarePrice(BigDecimal.ZERO);
		request.setMaximumFarePrice(BigDecimal.ZERO);
		request.setInboundOutbound(InboundOutbound.None);
		request.setIncludeTaxesAndFees(navitaireAirline.getTaxAndFeesAllowed());
		request.setFareClassControl(getFareClass());
		request.setFareRuleFilter(FareRuleFilter.Default);
		request.setLoyaltyFilter(LoyaltyFilter.MonetaryOnly);
		request.setPromotionCode(getPromoCode(searchQuery));
		request.setDiscountCode(StringUtils.EMPTY);
	}

	public FareClassControl getFareClass() {
		FareClassControl classControl = FareClassControl.LowestFareClass;
		if (configuration != null && configuration.getSupplierAdditionalInfo() != null
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareClass())) {
			String fareClass = configuration.getSupplierAdditionalInfo().getFareClass().toUpperCase();
			if (FareClassControl._CompressByProductClass.toUpperCase().equals(fareClass)) {
				classControl = FareClassControl.CompressByProductClass;
			} else if (FareClassControl._Default.equals(fareClass)) {
				classControl = FareClassControl.Default;
			} else {
				classControl = FareClassControl.LowestFareClass;
			}
		}
		return classControl;
	}

	private FlightDesignator getFlightDesignator(Segment segment) {
		FlightDesignator flightDesignator = FlightDesignator.builder().build();
		flightDesignator.setFlightNumber(segment.getFlightDesignator().getFlightNumber().trim());
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(segment.getFlightDesignator().getCarrierCode()));
		if (segment.getLegs() != null && ArrayUtils.isNotEmpty(segment.getLegs().getLeg())
				&& segment.getLegs().getLeg()[0] != null) {
			flightDesignator.setEquipType(segment.getLegs().getLeg()[0].getLegInfo().getEquipmentType());
		}
		return flightDesignator;
	}

	private String getAirlineCode() {
		String airlineCode = null;
		AirSourceConfigurationOutput airSourceOutput =
				AirUtils.getAirSourceConfiguration(searchQuery, configuration.getBasicInfo(), bookingUser);
		if (airSourceOutput != null && CollectionUtils.isNotEmpty(airSourceOutput.getIncludedAirlines())) {
			airlineCode = airSourceOutput.getIncludedAirlines().stream().findFirst().get();
		} else if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			airlineCode = searchQuery.getPrefferedAirline();
		}
		return airlineCode;
	}

	private void setAirSearchReqProperty(AvailabilityRequest request, RouteInfo routeInfo) {
		request.setPaxPriceTypes(getPaxPriceTypeV4());
		request.setPaxCount((short) AirUtils.getPaxCount(searchQuery, false));
		request.setCurrencyCode(getCurrencyCode());
		request.setFareTypes(getFareTypes());
		request.setProductClasses(getProductClasses());
		request.setFareClasses(null);
		ArrayOfJourneySortKey journeySortKey = new ArrayOfJourneySortKey();
		journeySortKey.addJourneySortKey(JourneySortKey.LowestFare);
		request.setJourneySortKeys(journeySortKey);

		request.setBeginDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
		request.setEndDate(TgsDateUtils.getCalendar(routeInfo.getTravelDate()));
		request.setDepartureStation(routeInfo.getFromCityOrAirport().getCode());
		request.setArrivalStation(routeInfo.getToCityOrAirport().getCode());
	}

	private ArrayOfstring getFareTypes() {
		ArrayOfstring arrString = new ArrayOfstring();
		if (Objects.nonNull(configuration.getSupplierAdditionalInfo().getFareTypes())
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareTypes(searchQuery)))
			arrString.setString(configuration.getSupplierAdditionalInfo().getFareTypes(searchQuery).split(","));
		return arrString;
	}

	private ArrayOfstring getProductClasses() {
		ArrayOfstring arrString = new ArrayOfstring();
		if (Objects.nonNull(configuration.getSupplierAdditionalInfo().getProductClasses())
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getProductClasses(searchQuery)))
			arrString.setString(configuration.getSupplierAdditionalInfo().getProductClasses(searchQuery).split(","));
		return arrString;
	}

	public ArrayOfPaxPriceType getPaxPriceTypeV4() {
		ArrayOfPaxPriceType priceType = new ArrayOfPaxPriceType();
		priceType.addPaxPriceType(getPaxPriceTypeV4(adultCount, PAX_TYPE_ADULT));
		if (childCount > 0) {
			priceType.addPaxPriceType(getPaxPriceTypeV4(childCount, PAX_TYPE_CHILD));
		}
		return priceType;
	}

	public PaxPriceType getPaxPriceTypeV4(int paxCount, String paxCode) {
		PaxPriceType priceType = new PaxPriceType();
		priceType.setPaxType(paxCode);
		return priceType;
	}

	public FareDetail setPaxWiseFareBreakUpV4(FareDetail fareDetail, PaxFare paxFare, int paxCount,
			BookingServiceCharge[] serviceCharge) {
		for (BookingServiceCharge charge : serviceCharge) {
			String chargeType = charge.getChargeType().getValue(); // Charge Type
			NavitaireCharges navitaireCharges = NavitaireCharges.getNavitaireChargeOnChargeType(chargeType);
			String chargeCode = charge.getChargeCode(); // FeeType
			String ticketCode = charge.getTicketCode();
			double amount = getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
			if (amount > 0 && !chargeType.equalsIgnoreCase("IncludedTax")) {
				if (navitaireCharges != null && StringUtils.isNotBlank(chargeCode)) {
					// Set Amount Based on Sub Type
					navitaireCharges = NavitaireCharges.getNavitaireChargeOnFeeType(chargeCode, navitaireCharges);
					if (StringUtils.isNotBlank(ticketCode) && GST_TICKET_CODE.contains(ticketCode)) {
						navitaireCharges = NavitaireCharges.GST;
					}
					navitaireCharges.setFareComponent(fareDetail, paxFare.getPaxType(), amount,
							charge.getCurrencyCode());
				} else {
					// Set Directly to Other or Chargetype Fare component
					navitaireCharges.setFareComponent(fareDetail, paxFare.getPaxType(), amount,
							charge.getCurrencyCode());
				}
			}
		}
		return fareDetail;
	}
}
