package com.tgs.services.fms.sources.navitaireV4_2;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.xml.namespace.QName;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBookingRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.GetBookingResponse;
import com.navitaire.schemas.webservices.datacontracts.booking.GetByRecordLocator;
import com.navitaire.schemas.webservices.datacontracts.booking.GetBookingRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingHold;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingCommitRequestData;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.BookingCommitRequest;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.BookingCommitResponse;
import com.navitaire.schemas.webservices.datacontracts.booking.ReceivedByInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPassenger;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.FareClassControl;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMElement;
import org.apache.axiom.om.OMFactory;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.navitaire_newskies_webservices_datacontracts_common_enumerations.LoyaltyFilter;
import org.springframework.data.util.Pair;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfshort;
import com.microsoft.schemas._2003._10.serialization.arrays.ArrayOfstring;
import com.navitaire.schemas.webservices.BookingManagerStub;
import com.navitaire.schemas.webservices.ContractVersion;
import com.navitaire.schemas.webservices.EnableExceptionStackTrace;
import com.navitaire.schemas.webservices.MessageContractVersion;
import com.navitaire.schemas.webservices.SessionManagerStub;
import com.navitaire.schemas.webservices.Signature;
import com.navitaire.schemas.webservices.UtilitiesManagerStub;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaxPriceType;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfPaxSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSegmentSSRRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.ArrayOfSellKeyList;
import com.navitaire.schemas.webservices.datacontracts.booking.AvailablePaxSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.Booking;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingServiceCharge;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingUpdateResponseData;
import com.navitaire.schemas.webservices.datacontracts.booking.Leg;
import com.navitaire.schemas.webservices.datacontracts.booking.LegInfo;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxFare;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxPriceType;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxSSR;
import com.navitaire.schemas.webservices.datacontracts.booking.PaxSSRPrice;
import com.navitaire.schemas.webservices.datacontracts.booking.PaymentValidationError;
import com.navitaire.schemas.webservices.datacontracts.booking.SSRAvailabilityForBookingRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SSRRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SegmentSSRRequest;
import com.navitaire.schemas.webservices.datacontracts.booking.SellJourneyByKeyRequestData;
import com.navitaire.schemas.webservices.datacontracts.booking.SellKeyList;
import com.navitaire.schemas.webservices.datacontracts.booking.Success;
import com.navitaire.schemas.webservices.servicecontracts.bookingservice.AddPaymentToBookingResponse;
import com.navitaire.schemas.webservices.datacontracts.common.ArrayOfRecordLocator;
import com.navitaire.schemas.webservices.datacontracts.common.BookingPointOfSale;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.GetBookingBy;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.utils.exception.air.ImportPNRNotAllowedException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.navitaire.schemas.webservices.datacontracts.common.ArrayOfOtherServiceInformation;
import com.navitaire.schemas.webservices.datacontracts.common.PointOfSale;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.BookingStatus;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.MessageState;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import tgs.navitaire.schemas.webservices.BookingManagerStubV3;

@Slf4j
@Setter
@Getter
@SuperBuilder
public abstract class NavitaireServiceManager {

	protected String sessionSignature;
	protected int sourceId;
	protected NavitaireAirline navitaireAirline;
	protected SupplierConfiguration configuration;
	protected AirSearchQuery searchQuery;
	protected DeliveryInfo deliveryInfo;
	protected String bookingId;
	protected List<SegmentInfo> segmentInfos;
	protected final static String PAX_TYPE_ADULT = "ADT";
	protected final static String PAX_TYPE_CHILD = "CHD";
	protected final static String PAX_TYPE_INFANT = "INFT";
	protected SessionManagerStub sessionBinding;
	protected BookingManagerStub bookingBinding;
	protected BookingManagerStubV3 bookingStubV3;
	protected UtilitiesManagerStub utilityStub;
	protected List<FlightTravellerInfo> infants;
	protected List<FlightTravellerInfo> adults;
	protected List<FlightTravellerInfo> child;
	protected int adultCount;
	protected int childCount;
	protected int infantCount;
	protected FareDetail infantFare;
	protected SoapRequestResponseListner listener;
	protected AirSourceConfigurationOutput sourceConfiguration;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected ClientGeneralInfo clientInfo;
	protected List<SupplierAnalyticsQuery> supplierAnalyticInfos;
	protected BookingSegments bookingSegments;
	protected User bookingUser;

	protected static String SYSTEM_GEN = ": SYSTEM GENERATED";

	protected static final String PNR_NA = "No PNR/Journey Available ";

	protected List<String> criticalMessageLogger;

	protected static List<String> GST_TICKET_CODE = Arrays.asList("SST", "CST", "IST");

	public void setPaxCount() {
		if (Objects.nonNull(searchQuery)) {
			this.adultCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.ADULT);
			this.childCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.CHILD);
			this.infantCount = AirUtils.getParticularPaxCount(searchQuery, PaxType.INFANT);
		}
	}

	public void init() {
		this.sourceId = configuration.getBasicInfo().getSourceId();
		this.navitaireAirline = NavitaireAirline.getNavitaireAirline(sourceId);
		setPaxCount();
	}

	public void initializeTravellerInfo(List<FlightTravellerInfo> travellerInfos) {
		adults = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT);
		child = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD);
		infants = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT);
		this.adultCount = adults.size();
		this.childCount = child.size();
		this.infantCount = infants.size();
	}

	public OMElement setSignatureHeader() {
		OMFactory omFactory = OMAbstractFactory.getOMFactory();
		OMElement omSecurityElement = omFactory.createOMElement(
				new QName("http://schemas.navitaire.com/WebServices", "Signature", sessionSignature), null);
		return omSecurityElement;
	}

	public ContractVersion getContractVersion() {
		ContractVersion contractVersion = new ContractVersion();
		contractVersion.setContractVersion(navitaireAirline.getContractVersion());
		return contractVersion;
	}

	public EnableExceptionStackTrace getExceptionStackTrace() {
		EnableExceptionStackTrace exceptionStackTrace = new EnableExceptionStackTrace();
		exceptionStackTrace.setEnableExceptionStackTrace(true);
		return exceptionStackTrace;
	}

	public Signature getSignature() {
		Signature sign = new Signature();
		sign.setSignature(sessionSignature);
		return sign;
	}

	public tgs.navitaire.schemas.webservices.ContractVersion getContractVersionV41() {
		tgs.navitaire.schemas.webservices.ContractVersion contractVersion =
				new tgs.navitaire.schemas.webservices.ContractVersion();
		contractVersion.setContractVersion(navitaireAirline.getContractVersion());
		return contractVersion;
	}

	public tgs.navitaire.schemas.webservices.EnableExceptionStackTrace getEnableExceptionVersionV41() {
		tgs.navitaire.schemas.webservices.EnableExceptionStackTrace contractVersion =
				new tgs.navitaire.schemas.webservices.EnableExceptionStackTrace();
		contractVersion.setEnableExceptionStackTrace(true);
		return contractVersion;
	}

	public tgs.navitaire.schemas.webservices.Signature getSignatureV41() {
		tgs.navitaire.schemas.webservices.Signature sign = new tgs.navitaire.schemas.webservices.Signature();
		sign.setSignature(sessionSignature);
		return sign;
	}

	public MessageContractVersion getMessageContractversion() {
		MessageContractVersion contractVersion = new MessageContractVersion();
		contractVersion.setMessageContractVersion(navitaireAirline.getMessageContractVersion(configuration));
		return contractVersion;
	}

	public tgs.navitaire.schemas.webservices.MessageContractVersion getMessageContractversion1() {
		tgs.navitaire.schemas.webservices.MessageContractVersion contractVersion =
				new tgs.navitaire.schemas.webservices.MessageContractVersion();
		contractVersion.setMessageContractVersion(navitaireAirline.getMessageContractVersion(configuration));
		return contractVersion;
	}

	public double getAmountBasedOnCurrency(BigDecimal amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue();
		}
		AirSourceType sourceType = AirSourceType.getAirSourceType(getSourceId());
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(sourceType.name().toUpperCase()).build();
		return moneyExchnageComm.getExchangeValue(amount.doubleValue(), filter, true);
	}

	public ArrayOfPaxPriceType getPaxPriceType(boolean isPaxPrice) {
		ArrayOfPaxPriceType priceType = new ArrayOfPaxPriceType();
		if (navitaireAirline.isPaxCountOnSell()) {
			priceType.addPaxPriceType(getPaxPriceType(adultCount, PAX_TYPE_ADULT, isPaxPrice));
			if (childCount > 0) {
				priceType.addPaxPriceType(getPaxPriceType(childCount, PAX_TYPE_CHILD, isPaxPrice));
			}
		} else {
			priceType = getPaxPriceType(adultCount, PAX_TYPE_ADULT, priceType);
			if (childCount > 0) {
				priceType = getPaxPriceType(childCount, PAX_TYPE_CHILD, priceType);
			}
		}
		return priceType;
	}

	public PaxPriceType getPaxPriceType(int paxCount, String paxCode, boolean isPaxPrice) {
		PaxPriceType priceType = new PaxPriceType();
		priceType.setPaxType(paxCode);
		priceType.setPaxCount((short) paxCount);
		return priceType;
	}

	public ArrayOfPaxPriceType getPaxPriceType(int paxCount, String paxCode, ArrayOfPaxPriceType priceType) {
		for (int index = 0; index < paxCount; index++) {
			PaxPriceType paxpriceType = new PaxPriceType();
			paxpriceType.setPaxType(paxCode);
			priceType.addPaxPriceType(paxpriceType);
		}
		return priceType;
	}

	public PointOfSale getPOS() {
		PointOfSale pos = new PointOfSale();
		pos.setState(MessageState.New);
		pos.setDomainCode(configuration.getSupplierCredential().getDomain());
		pos.setAgentCode(navitaireAirline.getAgentCode());
		pos.setLocationCode(navitaireAirline.getLocationCode(configuration));
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getOrganisationCode())) {
			pos.setOrganizationCode(configuration.getSupplierCredential().getOrganisationCode());
		}
		return pos;
	}

	public String getCurrencyCode() {
		String currencyCode = getCurrencyCodeFromAirPortInfo();
		if (StringUtils.isBlank(currencyCode)) {
			if (configuration != null
					&& StringUtils.isNotBlank(configuration.getSupplierCredential().getCurrencyCode())) {
				return configuration.getSupplierCredential().getCurrencyCode();
			}
			clientInfo = ServiceCommunicatorHelper.getClientInfo();
			if (clientInfo != null && StringUtils.isNotBlank(clientInfo.getCurrencyCode())) {
				return clientInfo.getCurrencyCode();
			}
		}
		return currencyCode;
	}

	private String getCurrencyCodeFromAirPortInfo() {
		AirportInfo departureAirport = navitaireAirline.getSourceAirportCode(searchQuery);
		if (Objects.isNull(departureAirport) && Objects.nonNull(segmentInfos)) {
			departureAirport = segmentInfos.get(0).getDepartAirportInfo();
		}
		if (Objects.nonNull(departureAirport)) {
			if (sourceConfiguration != null && CollectionUtils.isNotEmpty(sourceConfiguration.getCurrencyCodes())) {
				AirportInfo depAirport = departureAirport;
				Optional<KeyValue> currencyCode = sourceConfiguration.getCurrencyCodes().stream()
						.filter(currencyKey -> currencyKey.getKey().equalsIgnoreCase(depAirport.getCountryCode())
								|| currencyKey.getKey().contains(depAirport.getCode()))
						.findFirst();
				if (currencyCode.isPresent()) {
					return currencyCode.get().getValue();
				}
			}
		}
		return null;
	}

	public Calendar getExpirationCalendar() {
		LocalDate localDate = LocalDate.now();
		localDate = localDate.withYear(0001).withMonth(01).withDayOfYear(01);
		Calendar calendar = TgsDateUtils.getCalendar(localDate);
		return calendar;
	}


	public SellJourneyByKeyRequestData getSellJourneyByKeyRequestData(List<Pair<TripInfo, Integer>> flightList,
			boolean isPaxPrice, boolean isSearch) {
		SellJourneyByKeyRequestData sellJourneyByKeyReqData = new SellJourneyByKeyRequestData();
		sellJourneyByKeyReqData.setCurrencyCode(getCurrencyCode());
		sellJourneyByKeyReqData.setServiceBundleList(new ArrayOfstring());
		sellJourneyByKeyReqData
				.setApplyServiceBundle(navitaireAirline.isServiceBundleApplicable(sourceConfiguration, flightList));
		sellJourneyByKeyReqData.setPaxPriceType(getPaxPriceType(isPaxPrice));
		ArrayOfSellKeyList arrayOfSellKeyList = new ArrayOfSellKeyList();
		for (Pair<TripInfo, Integer> tInfoPair : flightList) {
			// Split Trip info will handle Intl Return to sell as trip journey wise

			AirUtils.splitTripInfo(tInfoPair.getFirst(), true).forEach(tripInfo -> {
				StringJoiner fareKey = getTripPriceFareKey(tripInfo, isSearch, tInfoPair.getSecond());
				SellKeyList sellKeyList = new SellKeyList();
				sellKeyList.setJourneySellKey(getJourneySellKey(tripInfo));
				sellKeyList.setFareSellKey(fareKey.toString());
				arrayOfSellKeyList.addSellKeyList(sellKeyList);

			});
		}
		sellJourneyByKeyReqData.setJourneySellKeys(arrayOfSellKeyList);
		sellJourneyByKeyReqData.setPaxCount((short) (adultCount + childCount));
		sellJourneyByKeyReqData.setLoyaltyFilter(LoyaltyFilter.MonetaryOnly);
		sellJourneyByKeyReqData.setIsAllotmentMarketFare(false);
		sellJourneyByKeyReqData.setSourcePOS(getPOS());
		sellJourneyByKeyReqData.setActionStatusCode("NN");
		return sellJourneyByKeyReqData;
	}


	private String getJourneySellKey(TripInfo tInfo) {
		StringJoiner joiner = new StringJoiner("^");
		SegmentInfo segmentInfo = tInfo.getSegmentInfos().get(0);
		if (segmentInfo.getMiscInfo() != null
				&& StringUtils.isNotBlank(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey())
				&& (segmentInfo.getSegmentNum() == 0 || segmentInfo.getIsReturnSegment()
						|| BooleanUtils.isTrue(segmentInfo.getIsCombinationFirstSegment()))) {
			joiner.add(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
		}
		return joiner.toString();
	}

	private StringJoiner getTripPriceFareKey(TripInfo tInfo, boolean isSearch, Integer priceIndex) {
		StringJoiner joiner = new StringJoiner("^");
		for (SegmentInfo segmentInfo : tInfo.getSegmentInfos()) {
			PriceInfo priceInfo = segmentInfo.getPriceInfo(priceIndex);
			joiner.add(priceInfo.getMiscInfo().getFareKey());
		}
		return joiner;
	}

	public PaxType getPaxType(String paxCode) {
		PaxType paxType = PaxType.ADULT;
		if (paxCode.equals(PAX_TYPE_CHILD)) {
			paxType = PaxType.CHILD;
		} else if (paxCode.equals(PAX_TYPE_INFANT)) {
			paxType = PaxType.INFANT;
		}
		return paxType;
	}

	public FlightDesignator getFlightDesignator(
			com.navitaire.schemas.webservices.datacontracts.common.FlightDesignator flightDesignator) {
		FlightDesignator designator = FlightDesignator.builder().flightNumber(flightDesignator.getFlightNumber().trim())
				.airlineInfo(AirlineHelper.getAirlineInfo(flightDesignator.getCarrierCode())).build();
		return designator;
	}

	public AirlineInfo getOperatingCarrier(Leg[] legs, FlightDesignator designator) {
		LegInfo legInfo = legs[0].getLegInfo();
		if (Objects.nonNull(legInfo) && StringUtils.isNotBlank(legInfo.getOperatingCarrier())) {
			AirlineHelper.getAirlineInfo(legInfo.getOperatingCarrier());
		}
		return designator.getAirlineInfo();
	}

	public static PaxFare getPaxFare(PaxFare[] paxFares, String paxType) {
		PaxFare fare = null;
		if (ArrayUtils.isNotEmpty(paxFares)) {
			for (PaxFare paxFare : paxFares) {
				if (paxType.equalsIgnoreCase(paxFare.getPaxType())) {
					fare = paxFare;
					break;
				}
			}
		}
		return fare;
	}

	public FareDetail parsePaxWiseFareBreakUp(FareDetail fareDetail, PaxFare paxFare, String paxType, int paxCount,
			BookingServiceCharge[] serviceCharge) {
		if (Objects.nonNull(paxFare) && paxFare.getPaxType().equalsIgnoreCase(paxType)) {
			return setPaxWiseFareBreakUp(fareDetail, paxFare, paxCount, serviceCharge);
		}
		return fareDetail;
	}

	public FareDetail setPaxWiseFareBreakUp(FareDetail fareDetail, PaxFare paxFare, int paxCount,
			BookingServiceCharge[] serviceCharge) {
		for (BookingServiceCharge charge : serviceCharge) {
			String chargeType = charge.getChargeType().getValue(); // Charge Type
			NavitaireCharges navitaireCharges = NavitaireCharges.getNavitaireChargeOnChargeType(chargeType);
			String chargeCode = charge.getChargeCode(); // FeeType
			double amount = getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
			String ticketCode = charge.getTicketCode();
			if (amount > 0 && !chargeType.equalsIgnoreCase("IncludedTax")) {
				if (!NavitaireCharges.DISCOUNT.equals(navitaireCharges)) {
					if (navitaireCharges != null && StringUtils.isNotBlank(chargeCode)) {
						// Set Amount Based on Sub Type
						navitaireCharges = NavitaireCharges.getNavitaireChargeOnFeeType(chargeCode, navitaireCharges);
					}
					if (StringUtils.isNotBlank(ticketCode) && GST_TICKET_CODE.contains(ticketCode)) {
						navitaireCharges = NavitaireCharges.GST;
					}
				}
				navitaireCharges.setFareComponent(fareDetail, paxFare.getPaxType(), amount, charge.getCurrencyCode());
			}
			fareDetail.setFareBasis(paxFare.getTicketFareBasisCode());
		}
		return fareDetail;
	}

	public double getTotalSellfare(Success success) {
		if (success != null && success.getPNRAmount() != null) {
			return success.getPNRAmount().getTotalCost().doubleValue();
		}
		return 0;
	}

	public SSRAvailabilityForBookingRequest setCommonSSRRequest(SSRAvailabilityForBookingRequest ssrRequest,
			TripInfo tripInfo) {
		ssrRequest.setCurrencyCode(getCurrencyCode());
		ssrRequest.setInventoryControlled(true);
		ssrRequest.setNonInventoryControlled(true);
		ssrRequest.setNonSeatDependent(true);
		ssrRequest.setSeatDependent(true);
		short[] passengerNumberList = new short[1];
		passengerNumberList[0] = 0;
		ArrayOfshort ofShort = new ArrayOfshort();
		ofShort.set_short(passengerNumberList);
		ssrRequest.setPassengerNumberList(ofShort);
		return ssrRequest;
	}

	public void setInfantFareToTrip(List<TripInfo> tripInfos, TripInfo reviewedTrip) {
		List<TripInfo> reviewedTrips = AirUtils.splitTripInfo(reviewedTrip, false);
		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsInfantFareFromCache())) {
			FareComponentHelper.storeInfantFare(searchQuery, sourceId,
					reviewedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.INFANT));
		}
		for (TripInfo newTripInfo : tripInfos) {
			AtomicInteger segmentNum = new AtomicInteger(0);
			AtomicBoolean isIntlSecondTrip = new AtomicBoolean();
			newTripInfo.getSegmentInfos().forEach(segmentInfo -> {
				FareDetail infantFareDetail;
				if (isIntlSecondTrip.get() || (segmentInfo.getSegmentNum() == 0 && segmentNum.get() != 0)) {
					isIntlSecondTrip.set(true);
					infantFareDetail = getInfantFareBasedOnSourceType(reviewedTrips.get(1), segmentInfo);
				} else {
					infantFareDetail = getInfantFareBasedOnSourceType(reviewedTrips.get(0), segmentInfo);
				}
				copyInfantFareToSegment(segmentInfo, infantFareDetail);
				segmentNum.getAndIncrement();
			});
		}
		NavitaireUtils.setTotalFareOnTripInfo(tripInfos);
	}


	public void copyInfantFareToSegment(SegmentInfo segmentInfo, FareDetail infantFareDetail) {
		segmentInfo.getPriceInfoList().forEach(priceInfo -> {
			FareDetail fareDetail = new FareDetail();
			fareDetail.setFareComponents(new HashMap<>());
			if (infantFareDetail != null && MapUtils.isNotEmpty(infantFareDetail.getFareComponents())) {
				if (navitaireAirline.isInfantFareOnSegmentWise(segmentInfo)) {
					infantFareDetail.getFareComponents().forEach((fc, amount) -> {
						fareDetail.getFareComponents().put(fc, amount);
					});
				}
			}
			fareDetail.setCabinClass(searchQuery.getCabinClass());
			priceInfo.getFareDetail(PaxType.INFANT, new FareDetail()).setFareComponents(fareDetail.getFareComponents());
		});
	}

	private FareDetail getInfantFareBasedOnSourceType(TripInfo reviewedTrip, SegmentInfo segmentInfo) {
		if (navitaireAirline.isInfantFareOnSegmentWise(segmentInfo)) {
			if (reviewedTrip.getSegmentInfos().size() > segmentInfo.getSegmentNum()) {
				return reviewedTrip.getSegmentInfos().get(segmentInfo.getSegmentNum()).getPriceInfo(0)
						.getFareDetail(PaxType.INFANT);
			}
		}
		return infantFare;
	}

	/**
	 * Infant fare applicable on Journey wise
	 */
	public SegmentInfo setInfantFareToTrip(SegmentInfo segmentInfo, FareDetail oldfareDetail) {
		if (oldfareDetail != null && MapUtils.isNotEmpty(oldfareDetail.getFareComponents())) {
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				FareDetail fareDetail = priceInfo.getFareDetail(PaxType.INFANT, new FareDetail());
				if (MapUtils.isEmpty(fareDetail.getFareComponents())) {
					fareDetail.setFareComponents(new HashMap<>());
					oldfareDetail.getFareComponents().forEach((fc, amount) -> {
						fareDetail.getFareComponents().put(fc, amount);
					});
					fareDetail.setCabinClass(searchQuery.getCabinClass());
					priceInfo.getFareDetail(PaxType.INFANT, fareDetail)
							.setFareComponents(fareDetail.getFareComponents());
				}
			});
		}
		return segmentInfo;
	}

	public boolean isAnyPaymentError(AddPaymentToBookingResponse response) {
		boolean isError = false;
		String message = null;
		if (response != null && response.getBookingPaymentResponse() != null
				&& response.getBookingPaymentResponse().getValidationPayment() != null) {
			PaymentValidationError[] paymentValidationError = response.getBookingPaymentResponse()
					.getValidationPayment().getPaymentValidationErrors().getPaymentValidationError();
			if (paymentValidationError != null && paymentValidationError.length > 0) {
				message = StringUtils.join(paymentValidationError[0].getErrorType(),
						paymentValidationError[0].getErrorDescription());
				log.info("Payment Error Type  {} Description {} for bookingId {}", message, bookingId);
				isError = true;
			}
		}
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			criticalMessageLogger.add(message);
		}
		return isError;
	}

	public boolean isAnySellError(BookingUpdateResponseData bookingUpdateResponseData, boolean isWarningRequired) {
		boolean isError = false;
		StringJoiner message = new StringJoiner("");
		if (bookingUpdateResponseData != null) {
			if (bookingUpdateResponseData.getError() != null
					&& StringUtils.isNotEmpty(bookingUpdateResponseData.getError().getErrorText())) {
				message.add(bookingUpdateResponseData.getError().getErrorText());
				addOtherServiceInWarningOrError(bookingUpdateResponseData);
				log.info("Sell Failed Error Message for Booking {} due to {}  ", bookingId, message.toString());
				isError = true;
			}
			if (isWarningRequired && (bookingUpdateResponseData.getWarning() != null
					&& StringUtils.isNotBlank(bookingUpdateResponseData.getWarning().getWarningText()))) {
				log.info("Sell Failed Warning Message for Booking {} due to {}  ", bookingId,
						bookingUpdateResponseData.getWarning().getWarningText());
				message.add(bookingUpdateResponseData.getWarning().getWarningText());
				addOtherServiceInWarningOrError(bookingUpdateResponseData);
				isError = true;
			}
		}
		logCriticalMessage(message.toString());
		return isError;
	}


	protected void logCriticalMessage(String message) {
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			criticalMessageLogger.add(message);
		}
	}

	public void addOtherServiceInWarningOrError(BookingUpdateResponseData bookingUpdateResponseData) {
		ArrayOfOtherServiceInformation serviceInformation = bookingUpdateResponseData.getOtherServiceInformations();
		if (serviceInformation != null && ArrayUtils.isNotEmpty(serviceInformation.getOtherServiceInformation())) {
			Arrays.stream(serviceInformation.getOtherServiceInformation()).forEach(information -> {
				if (criticalMessageLogger != null) {
					String message = StringUtils.join(information.getText(), information.getOSITypeCode());
					criticalMessageLogger.add(message);
				}
			});
		}
	}

	protected String getPromoCode(AirSearchQuery searchQuery) {
		FlightBasicFact fact =
				FlightBasicFact.createFact().generateFact(searchQuery).generateFact(configuration.getBasicInfo());
		BaseUtils.createFactOnUser(fact, bookingUser);
		String promoCode = AirUtils.getSupplierPromotionCode(fact);
		if (StringUtils.isEmpty(promoCode) && configuration != null && configuration.getSupplierAdditionalInfo() != null
				&& CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			promoCode = configuration.getSupplierAdditionalInfo().getAccountCodes().get(0);
		}
		return promoCode;
	}

	/**
	 * @param paxSSR
	 *        <p>
	 *        includedtax where amount already added on FarePrice with tax
	 *        </p>
	 */
	public double getTotalSSRAmount(AvailablePaxSSR paxSSR) {
		double amount = 0;
		for (PaxSSRPrice price : paxSSR.getPaxSSRPriceList().getPaxSSRPrice()) {
			for (BookingServiceCharge serviceCharge : price.getPaxFee().getServiceCharges().getBookingServiceCharge()) {
				if (!serviceCharge.getChargeType().toString().toLowerCase().contains("includedtax")) {
					if (serviceCharge.getChargeType().toString().toLowerCase().contains("discount")
							&& serviceCharge.getAmount().doubleValue() < 0) {
						// TODO Hack Getting Discount Fare on Negative
						serviceCharge.setAmount(serviceCharge.getAmount().abs());
					}
					amount += getAmountBasedOnCurrency(serviceCharge.getAmount(), serviceCharge.getCurrencyCode());
				}
			}
		}
		return amount;
	}

	public String getSupplierDesc() {
		return configuration.getBasicInfo().getDescription();
	}

	public boolean isStoreLog(AirSearchQuery searchQuery) {
		if (searchQuery.getSearchModifiers() != null
				&& BooleanUtils.isTrue(searchQuery.getSearchModifiers().getStoreSearchLog())) {
			return true;
		}
		return false;
	}

	public List<FlightTravellerInfo> getCancelledTravellerInfoList(List<SegmentInfo> cancelSegments) {
		List<FlightTravellerInfo> cancelledTravellerInfoList = new ArrayList<FlightTravellerInfo>();
		for (SegmentInfo segmentInfo : cancelSegments) {
			for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getTravellerInfo()) {
				cancelledTravellerInfoList.add(flightTravellerInfo);
			}
		}
		return cancelledTravellerInfoList;

	}

	public SSRRequest getInfantSSR(List<SegmentInfo> segmentInfos, boolean isInfantSell) {
		SSRRequest ssrRequest = new SSRRequest();
		if (infantCount > 0 || navitaireAirline.isAdditionalSSRRequired(segmentInfos)) {
			ArrayOfSegmentSSRRequest segmentSSRRequest = new ArrayOfSegmentSSRRequest();
			segmentInfos.forEach(segmentInfo -> {
				SegmentSSRRequest ssrSegmentRequest = new SegmentSSRRequest();
				ssrSegmentRequest.setDepartureStation(segmentInfo.getDepartAirportInfo().getCode());
				ssrSegmentRequest.setArrivalStation(segmentInfo.getArrivalAirportInfo().getCode());
				ssrSegmentRequest.setSTD(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
				com.navitaire.schemas.webservices.datacontracts.common.FlightDesignator designator =
						new com.navitaire.schemas.webservices.datacontracts.common.FlightDesignator();
				designator.setCarrierCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode());
				designator.setFlightNumber(segmentInfo.getFlightDesignator().getFlightNumber());
				ssrSegmentRequest.setFlightDesignator(designator);
				ArrayOfPaxSSR paxSSR = getPaxSSR(segmentInfo, isInfantSell);
				if (paxSSR != null && paxSSR.getPaxSSR() != null) {
					ssrSegmentRequest.setPaxSSRs(paxSSR);
					segmentSSRRequest.addSegmentSSRRequest(ssrSegmentRequest);
				}
			});
			ssrRequest.setSegmentSSRRequests(segmentSSRRequest);
			ssrRequest.setCurrencyCode(getCurrencyCode());
		}
		return ssrRequest;
	}

	public ArrayOfPaxSSR getPaxSSR(SegmentInfo segment, boolean isInfantAlreadySelled) {
		ArrayOfPaxSSR paxSSR = new ArrayOfPaxSSR();
		// Add Baggage if Any to travaller
		if (Objects.nonNull(segment.getBookingRelatedInfo())
				&& Objects.nonNull(segment.getBookingRelatedInfo().getTravellerInfo())) {
			for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
				SSRInformation baggageInformation =
						segment.getBookingRelatedInfo().getTravellerInfo().get(paxIndex).getSsrBaggageInfo();
				if (Objects.nonNull(baggageInformation) && navitaireAirline.isSSRApplicableOnAllSegment(segment)) {
					PaxSSR ssr = new PaxSSR();
					ssr.setActionStatusCode("NN");
					ssr.setArrivalStation(segment.getArrivalAirportInfo().getCode());
					ssr.setDepartureStation(segment.getDepartAirportInfo().getCode());
					ssr.setPassengerNumber((short) paxIndex);
					ssr.setSSRCode(baggageInformation.getCode());
					paxSSR.addPaxSSR(ssr);
				}
			}
			// Add Meal if Any to travaller
			for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
				SSRInformation mealInformation =
						segment.getBookingRelatedInfo().getTravellerInfo().get(paxIndex).getSsrMealInfo();
				if (Objects.nonNull(mealInformation)) {
					PaxSSR ssr = new PaxSSR();
					ssr.setActionStatusCode("NN");
					ssr.setArrivalStation(segment.getArrivalAirportInfo().getCode());
					ssr.setDepartureStation(segment.getDepartAirportInfo().getCode());
					ssr.setPassengerNumber((short) paxIndex);
					ssr.setSSRCode(mealInformation.getCode());
					paxSSR.addPaxSSR(ssr);
				}
			}

			if (navitaireAirline.isAdditionalSSRRequired(Arrays.asList(segment))) {
				List<String> additionalSSRCodes = navitaireAirline.getAdditionalSSRCodes(Arrays.asList(segment));
				for (int paxIndex = 0; paxIndex < adults.size() + child.size(); paxIndex++) {
					for (String ssrCode : additionalSSRCodes) {
						if (StringUtils.isNotEmpty(ssrCode)) {
							PaxSSR ssr = new PaxSSR();
							ssr.setActionStatusCode("NN");
							ssr.setArrivalStation(segment.getArrivalAirportInfo().getCode());
							ssr.setDepartureStation(segment.getDepartAirportInfo().getCode());
							ssr.setPassengerNumber((short) paxIndex);
							ssr.setSSRCode(ssrCode);
							paxSSR.addPaxSSR(ssr);
						}
					}
				}
			}
		}
		if (isInfantAlreadySelled) {
			for (int paxIndex = 0; paxIndex < infantCount; paxIndex++) {
				PaxSSR ssr = new PaxSSR();
				ssr.setActionStatusCode("NN");
				ssr.setArrivalStation(segment.getArrivalAirportInfo().getCode());
				ssr.setDepartureStation(segment.getDepartAirportInfo().getCode());
				ssr.setPassengerNumber((short) paxIndex);
				ssr.setSSRCode(PAX_TYPE_INFANT);
				paxSSR.addPaxSSR(ssr);
			}
		}
		return paxSSR;
	}

	public void addExceptionToLogger(Exception e) {
		if (criticalMessageLogger != null && e != null && StringUtils.isNotBlank(e.getMessage())) {
			criticalMessageLogger.add(e.getMessage());
		}
	}

	public String getAirline(TripInfo tripInfo) {
		String airline = tripInfo.getAirlineCode();
		if (sourceConfiguration != null && CollectionUtils.isNotEmpty(sourceConfiguration.getIncludedAirlines())) {
			return sourceConfiguration.getIncludedAirlines().stream().findFirst().get();
		}
		return airline;
	}

	public String getRequestId() {
		if (StringUtils.isNotBlank(bookingId)) {
			return bookingId;
		} else if (searchQuery != null) {
			return searchQuery.getSearchId();
		}
		return null;
	}

	protected boolean isCancelled(BookingStatus bookingStatus) {
		return BookingStatus._HoldCanceled.equals(bookingStatus.getValue())
				|| BookingStatus._Closed.equals(bookingStatus.getValue());
	}


	public String commitBooking(boolean isHoldBook, List<FlightTravellerInfo> travellerInfo, String pnr,
			boolean isPosRequired) {
		String recordLocator = null;
		try {
			BookingCommitRequest commitRequest = new BookingCommitRequest();
			commitRequest.setBookingCommitRequestData(
					getBookingCommitRequest(travellerInfo, isHoldBook, pnr, isPosRequired));
			listener.setType("9-B-COMMIT " + getSourceId());
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			BookingCommitResponse response = bookingBinding.bookingCommit(commitRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			isAnySellError(response.getBookingUpdateResponseData(), true);
			if (response != null && response.getBookingUpdateResponseData() != null
					&& response.getBookingUpdateResponseData().getSuccess() != null) {
				recordLocator = response.getBookingUpdateResponseData().getSuccess().getRecordLocator();
			}
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APIFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_BookingCommit_APIWarningFaultFault_FaultMessage apirError) {
			throw new SupplierUnHandledFaultException(apirError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return recordLocator;
	}

	private BookingCommitRequestData getBookingCommitRequest(List<FlightTravellerInfo> travellerInfos,
			boolean isHoldBook, String pnr, boolean isPosRequired) {
		BookingCommitRequestData requestData = new BookingCommitRequestData();
		requestData.setState(MessageState.New);
		requestData.setRecordLocator(pnr);
		requestData.setCurrencyCode(getCurrencyCode());
		requestData.setPaxCount((short) (adults.size() + child.size()));
		requestData.setSystemCode(StringUtils.EMPTY);
		requestData.setBookingID(0);
		requestData.setBookingParentID(0);
		requestData.setParentRecordLocator(StringUtils.EMPTY);
		requestData.setBookingChangeCode(StringUtils.EMPTY);
		requestData.setGroupName(StringUtils.EMPTY);
		if (isPosRequired)
			requestData.setSourcePOS(getPOS());
		requestData.setBookingHold(getHoldBooking(isHoldBook));
		if (navitaireAirline.isRecievedInfoAllowed()) {
			requestData.setReceivedBy(getReceivedByInfo());
		} else {
			requestData.setReceivedBy(null);
		}
		requestData.setRecordLocators(new ArrayOfRecordLocator());
		requestData.setPassengers(new ArrayOfPassenger());
		requestData.setBookingComments(null);
		requestData.setNumericRecordLocator(StringUtils.EMPTY);
		requestData.setRestrictionOverride(false);
		requestData.setChangeHoldDateTime(false);
		requestData.setWaiveNameChangeFee(false);
		requestData.setWaivePenaltyFee(false);
		requestData.setWaiveSpoilageFee(false);
		requestData.setDistributeToContacts(true);
		requestData.setSourceBookingPOS(new BookingPointOfSale());
		return requestData;
	}

	private BookingHold getHoldBooking(boolean isHoldBook) {
		BookingHold bookingHold = null;
		if (isHoldBook) {
			bookingHold = new BookingHold();
			Calendar holdDate = TgsDateUtils.getCalendar(TgsDateUtils.getCurrentDate());
			holdDate.add(Calendar.DAY_OF_YEAR, 1);
			bookingHold.setHoldDateTime(holdDate);
			bookingHold.setState(MessageState.New);
		}
		return bookingHold;
	}

	private ReceivedByInfo getReceivedByInfo() {
		ReceivedByInfo receivedByInfo = new ReceivedByInfo();
		receivedByInfo.setReceivedBy(configuration.getSupplierCredential().getUserName());
		receivedByInfo.setReceivedReference(StringUtils.EMPTY);
		receivedByInfo.setReferralCode(StringUtils.EMPTY);
		receivedByInfo.setLatestReceivedBy(StringUtils.EMPTY);
		receivedByInfo.setLatestReceivedReference(StringUtils.EMPTY);
		return receivedByInfo;
	}

	public Booking getBookingResponse(String pnr) {
		try {
			GetBookingRequest bookingRequest = new GetBookingRequest();
			bookingRequest.setGetBookingReqData(getBookingRequestData(pnr));
			listener.setType("P-RETRIEVE " + getSourceId());
			bookingBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			GetBookingResponse bookedResponse = bookingBinding.getBooking(bookingRequest, getContractVersion(),
					getExceptionStackTrace(), getMessageContractversion(), getSignature(), getSourceId());
			return bookedResponse.getBooking();
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} catch (com.navitaire.schemas.webservices.IBookingManager_GetBooking_APIValidationFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBooking_APICriticalFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBooking_APIUnhandledServerFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBooking_APIGeneralFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBooking_APISecurityFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBooking_APIWarningFaultFault_FaultMessage
				| com.navitaire.schemas.webservices.IBookingManager_GetBooking_APIFaultFault_FaultMessage apiError) {
			throw new ImportPNRNotAllowedException(apiError.getMessage());
		} finally {
			bookingBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private GetBookingRequestData getBookingRequestData(String pnr) {
		GetBookingRequestData requestData = new GetBookingRequestData();
		requestData.setGetByRecordLocator(getRecordLocator(pnr));
		requestData.setGetBookingBy(GetBookingBy.RecordLocator);
		return requestData;
	}

	private GetByRecordLocator getRecordLocator(String pnr) {
		GetByRecordLocator recordLocator = new GetByRecordLocator();
		recordLocator.setRecordLocator(pnr);
		return recordLocator;
	}

	public FareClassControl getFareClassControl() {
		FareClassControl classControl = FareClassControl.LowestFareClass;
		if (configuration != null && configuration.getSupplierAdditionalInfo() != null
				&& StringUtils.isNotBlank(configuration.getSupplierAdditionalInfo().getFareClass())) {
			String fareClass = configuration.getSupplierAdditionalInfo().getFareClass().toUpperCase();
			if (FareClassControl._CompressByProductClass.toUpperCase().equals(fareClass)) {
				classControl = FareClassControl.CompressByProductClass;
			} else if (FareClassControl._Default.equals(fareClass)) {
				classControl = FareClassControl.Default;
			} else {
				classControl = FareClassControl.LowestFareClass;
			}
		}
		return classControl;
	}

	protected void captureRequestResponse(String bookingId, String request, String response, String methodName) {
		String requestHead = " Request";
		String responseHead = " Response";
		if (StringUtils.isNotEmpty(bookingId)) {
			requestHead = StringUtils.join(requestHead, " -B");
			responseHead = StringUtils.join(responseHead, " -B");
		}
		listener.extractMessage(request, AirUtils.getLogType(StringUtils.join(methodName, requestHead), configuration));
		listener.extractMessage(response,
				AirUtils.getLogType(StringUtils.join(methodName, responseHead), configuration));
	}

	protected AirCreditShellResponse fetchCreditShellBalance(AirCreditShellRequest request) {
		return null;
	}

	public boolean validateCreditShellPNR(String creditShellPNR, String sessionToken) {
		return true;
	}

	public double addCreditShellPaymentToBooking(double pnrCreditBalance, String newPNR, String csPNR) {
		return 0d;
	}

}
