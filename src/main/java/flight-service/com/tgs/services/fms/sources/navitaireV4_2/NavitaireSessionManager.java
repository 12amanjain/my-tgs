package com.tgs.services.fms.sources.navitaireV4_2;

import com.navitaire.schemas.webservices.ISessionManager_Logon_APICriticalFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logon_APIWarningFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.datacontracts.session.LogonRequestData;
import com.navitaire.schemas.webservices.servicecontracts.sessionservice.KeepAliveRequest;
import com.navitaire.schemas.webservices.servicecontracts.sessionservice.LogonRequest;
import com.navitaire.schemas.webservices.servicecontracts.sessionservice.LogoutRequest;
import com.navitaire.schemas.webservices.ISessionManager_Logon_APIGeneralFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logon_APIFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logon_APIValidationFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logon_APIUnhandledServerFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logon_APISecurityFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APIValidationFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APICriticalFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APIUnhandledServerFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APIGeneralFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APISecurityFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APIWarningFaultFault_FaultMessage;
import com.navitaire.schemas.webservices.ISessionManager_Logout_APIFaultFault_FaultMessage;
import com.tgs.services.base.enums.LogDataVisibilityGroup;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.axiom.om.OMException;
import org.apache.commons.lang3.StringUtils;
import java.rmi.RemoteException;
import java.util.Objects;

@Slf4j
@SuperBuilder
final class NavitaireSessionManager extends NavitaireServiceManager {


	// Session token authentication failure. : No such session
	protected static final String SESSION_FAILURE = "session token authentication failure";

	// Session Timeout
	protected static final String NO_SUCH_SESSION = "no such session";

	// Invalid Session
	protected static final String INVALID_SESSION = "invalid session";

	public void openSession() throws SupplierSessionException, SupplierRemoteException {
		try {
			LogonRequest logonRequest = new LogonRequest();
			listener.setType("1-LOGIN " + getSourceId());
			// listener.getVisibilityGroups().addAll(AirSupplierUtils.getLogVisibility());
			sessionBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			logonRequest.setLogonRequestData(getLogOnRequest());
			sessionSignature = sessionBinding
					.logon(logonRequest, getContractVersion(), getExceptionStackTrace(), getSourceId()).getSignature();
			sessionBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
			log.debug("Session Created for {} token {}", configuration.getSupplierId(), sessionSignature);
			// sessionBinding._getServiceClient().addHeader(setSignatureHeader());
		} catch (ISessionManager_Logon_APIGeneralFaultFault_FaultMessage
				| ISessionManager_Logon_APIWarningFaultFault_FaultMessage
				| ISessionManager_Logon_APIFaultFault_FaultMessage
				| ISessionManager_Logon_APICriticalFaultFault_FaultMessage
				| ISessionManager_Logon_APIValidationFaultFault_FaultMessage
				| ISessionManager_Logon_APISecurityFaultFault_FaultMessage
				| ISessionManager_Logon_APIUnhandledServerFaultFault_FaultMessage se) {
			throw new SupplierSessionException(se.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			sessionBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}


	public void keepAlive() {
		try {
			listener.setType("ALIVE");
			sessionBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
			sessionBinding.keepAlive(new KeepAliveRequest(), getContractVersion(), getExceptionStackTrace(),
					getMessageContractversion(), getSignature(), getSourceId());
			log.debug("Session Alive for {} token {}", configuration.getBasicInfo().getSupplierId(), sessionSignature);
			sessionBinding._getServiceClient().addHeader(setSignatureHeader());
		} catch (RemoteException e) {
			// log.error("keep session failed {}", e);
		} finally {
			sessionBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	public void closeSession() throws SupplierRemoteException {
		try {
			if (StringUtils.isNotEmpty(sessionSignature)) {
				listener.setType("LOGOUT");
				sessionBinding._getServiceClient().getAxisService().addMessageContextListener(listener);
				sessionBinding.logout(new LogoutRequest(), getContractVersion(), getExceptionStackTrace(),
						getMessageContractversion(), getSignature(), getSourceId());
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} catch (ISessionManager_Logout_APIValidationFaultFault_FaultMessage
				| ISessionManager_Logout_APICriticalFaultFault_FaultMessage
				| ISessionManager_Logout_APIUnhandledServerFaultFault_FaultMessage
				| ISessionManager_Logout_APIGeneralFaultFault_FaultMessage
				| ISessionManager_Logout_APISecurityFaultFault_FaultMessage
				| ISessionManager_Logout_APIWarningFaultFault_FaultMessage
				| ISessionManager_Logout_APIFaultFault_FaultMessage ie) {
			if (ie.getMessage().toLowerCase().contains(NO_SUCH_SESSION)) {
				log.info("No Such Session {} cause {}", getRequestId(), ie.getMessage());
			} else {
				log.error(AirSourceConstants.SESSION_CLOSE, getRequestId(), ie);
			}
		} catch (OMException ome) {
			// session is closed & logs get attched properly
			// AxisEngine.MessageContextError
		} catch (Exception e) {
			log.error(AirSourceConstants.SESSION_CLOSE, getRequestId(), e);
		} finally {
			sessionBinding._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	public LogonRequestData getLogOnRequest() {
		LogonRequestData requestData = new LogonRequestData();
		requestData.setAgentName(configuration.getSupplierCredential().getUserName());
		requestData.setPassword(configuration.getSupplierCredential().getPassword());
		requestData.setDomainCode(configuration.getSupplierCredential().getDomain());
		return requestData;
	}

	public boolean isRetry(Exception e) {
		return e != null && StringUtils.isNotBlank(e.getMessage())
				&& (e.getMessage().toLowerCase().contains(SESSION_FAILURE)
						|| e.getMessage().toLowerCase().contains(NO_SUCH_SESSION)
						|| e.getMessage().toLowerCase().contains(INVALID_SESSION));
	}

	public void setSessionToken(SupplierSession session) {
		if (Objects.isNull(session)) {
			openSession();
		} else {
			setSessionSignature(session.getSupplierSessionInfo().getSessionToken());
		}
	}
}
