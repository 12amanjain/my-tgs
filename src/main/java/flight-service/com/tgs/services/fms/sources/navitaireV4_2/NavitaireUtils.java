package com.tgs.services.fms.sources.navitaireV4_2;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import com.tgs.services.base.enums.FareType;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.collect.Lists;
import org.springframework.data.util.Pair;
import com.google.common.util.concurrent.AtomicDouble;
import com.google.gson.reflect.TypeToken;
import com.navitaire.schemas.webservices.datacontracts.booking.PassengerFee;
import com.navitaire.schemas.webservices.datacontracts.booking.BookingName;
import com.navitaire.schemas.webservices.datacontracts.booking.JourneyDateMarketVer2;
import com.navitaire.schemas.webservices.datacontracts.booking.Passenger;
import com.navitaire.schemas.webservices.datacontracts.booking.CreditFile;
import com.navitaire.schemas.webservices.datacontracts.booking.Segment;
import com.navitaire.schemas.webservices.datacontracts.common.enumerations.Gender;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.FareComponentHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import tgs.navitaire.schemas.webservices.datacontracts.booking.JourneyDateMarket;

@Slf4j
final class NavitaireUtils {

	// Session Timeout
	protected static final String NO_SUCH_SESSION = "no such session";

	// Invalid Session
	protected static final String INVALID_SESSION = "invalid session";

	public static AirSearchResult segregateResult(AirSearchQuery searchQuery, JourneyDateMarketVer2 journeyDateMarket,
			List<TripInfo> tripInfos, AirSearchResult searchResult, int journeyType, NavitaireAirline airline,
			User bookingUser) {
		log.debug("Got {} trips for searchQuery {}", tripInfos.size(), searchQuery);
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			setTotalFareOnTripInfo(tripInfos);
			searchQuery.getRouteInfos().forEach(routeInfo -> {
				if (journeyType == 0 && (journeyDateMarket.getDepartureStation()
						.equals(routeInfo.getFromCityOrAirport().getCode())
						|| journeyDateMarket.getArrivalStation().equals(routeInfo.getToCityOrAirport().getCode())
						|| journeyDateMarket.getDepartureStation()
								.equals(getNearByAirport(routeInfo.getFromCityOrAirport().getCode(), searchQuery,
										airline.getSourceId(), bookingUser)))) {
					searchResult.getTripInfos().put(TripInfoType.ONWARD.name(), tripInfos);
				} else if (journeyType == 1 && (journeyDateMarket.getDepartureStation()
						.equals(routeInfo.getFromCityOrAirport().getCode())
						|| journeyDateMarket.getArrivalStation().equals(routeInfo.getToCityOrAirport().getCode())
						|| journeyDateMarket.getDepartureStation()
								.equals(getNearByAirport(routeInfo.getFromCityOrAirport().getCode(), searchQuery,
										airline.getSourceId(), bookingUser)))) {
					searchResult.getTripInfos().put(TripInfoType.RETURN.name(), tripInfos);
				}
			});
		}
		return searchResult;
	}

	public static AirSearchResult segregateResult(AirSearchQuery searchQuery, JourneyDateMarket journeyDateMarket,
			List<TripInfo> tripInfos, AirSearchResult searchResult, int journeyType, NavitaireAirline airline,
			User bookingUser) {
		log.debug("Got {} trips for searchQuery {}", tripInfos.size(), searchQuery);
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			setTotalFareOnTripInfo(tripInfos);
			searchQuery.getRouteInfos().forEach(routeInfo -> {
				if (journeyType == 0
						&& (journeyDateMarket.getDepartureStation().equals(routeInfo.getFromCityOrAirport().getCode())
								|| journeyDateMarket.getDepartureStation()
										.equals(getNearByAirport(routeInfo.getFromCityOrAirport().getCode(),
												searchQuery, airline.getSourceId(), bookingUser))
								|| journeyDateMarket.getArrivalStation()
										.equals(routeInfo.getToCityOrAirport().getCode()))) {
					searchResult.getTripInfos().put(TripInfoType.ONWARD.name(), tripInfos);
				} else if (journeyType == 1
						&& (journeyDateMarket.getArrivalStation().equals(routeInfo.getToCityOrAirport().getCode())
								|| journeyDateMarket.getArrivalStation()
										.equals(getNearByAirport(routeInfo.getToCityOrAirport().getCode(), searchQuery,
												airline.getSourceId(), bookingUser))
								|| journeyDateMarket.getDepartureStation()
										.equals(routeInfo.getFromCityOrAirport().getCode()))) {
					searchResult.getTripInfos().put(TripInfoType.RETURN.name(), tripInfos);
				}
			});
		}
		return searchResult;
	}

	public static TripInfo findTripInfo(List<TripInfo> flightList, String journeyKey) {
		List<TripInfo> flight = new ArrayList<>();
		flightList.forEach(tripInfo -> {
			AirUtils.splitTripInfo(tripInfo, true).forEach(trip -> {
				if (trip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey()
						.equalsIgnoreCase(journeyKey)) {
					flight.add(trip);
				}
			});
		});
		if (CollectionUtils.isNotEmpty(flight)) {
			return flight.get(0);
		}
		return null;
	}

	public static void setTotalFareOnTripInfo(List<TripInfo> tripInfos) {

		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (segmentInfo.getBookingRelatedInfo() != null) {
					List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos.forEach(travellerInfo -> {
						if (travellerInfo.getFareDetail() != null) {
							travellerInfo.getFareDetail().getFareComponents().put(FareComponent.TF,
									getTotalFare(travellerInfo.getFareDetail()));
						}
					});
				} else if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					segmentInfo.getPriceInfoList().forEach(priceInfo -> {
						priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
							Map<FareComponent, Double> fareComp = fareDetail.getFareComponents();
							Double totalFare = getTotalFare(fareDetail);
							if (totalFare != 0) {
								fareComp.put(FareComponent.TF, getTotalFare(fareDetail));
							}
							fareDetail.setFareComponents(fareComp);
						});
					});
				}
			});
		});
	}

	public static Double getTotalFare(FareDetail fareDetail) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			fareDetail.getFareComponents().forEach((fareComponent, amount) -> {
				if (!fareComponent.equals(FareComponent.TF))
					totalAmount.addAndGet(amount);
			});
		}
		return totalAmount.doubleValue();
	}

	public static PriceInfo getTripPriceInfo(SegmentInfo segmentInfo, String fareSellKey) {
		List<PriceInfo> filteredPriceList = new ArrayList<>();
		filteredPriceList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
			return StringUtils.isNotEmpty(priceInfo.getMiscInfo().getFareKey())
					&& priceInfo.getMiscInfo().getFareKey().equals(fareSellKey);
		}).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(filteredPriceList)) {
			return filteredPriceList.get(0);
		}
		return null;
	}

	public static PriceInfo getTripPriceInfoOnFareType(SegmentInfo segmentInfo, String fareType) {
		List<PriceInfo> filteredPriceList = new ArrayList<>();
		filteredPriceList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
			return StringUtils.isNotEmpty(priceInfo.getMiscInfo().getFareKey())
					&& priceInfo.getFareIdentifier().equals(fareType);
		}).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(filteredPriceList)) {
			return filteredPriceList.get(0);
		}
		return null;
	}

	public static Gender getGender(String title, FlightTravellerInfo travellerInfo) {
		if (title.toUpperCase().equalsIgnoreCase("MR") || isMaster(travellerInfo)) {
			return Gender.Male;
		} else if (title.toUpperCase().equalsIgnoreCase("MRS") || isMiss(travellerInfo)) {
			return Gender.Female;
		}
		return Gender.Unmapped;
	}

	public static boolean isMaster(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MASTER")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MSTR");
		}
		return false;
	}

	public static boolean isMiss(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MISS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	public static boolean isMrsMs(FlightTravellerInfo travellerInfo) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			return travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MRS")
					|| travellerInfo.getTitle().toUpperCase().equalsIgnoreCase("MS");
		}
		return false;
	}

	/**
	 * @return true or false - if any SSR Added to atleast one Pax
	 */
	public static boolean isSeatAddedInTrip(List<SegmentInfo> segmentInfos) {
		boolean isSSRAdded = false;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					if (Objects.nonNull(travellerInfo.getSsrSeatInfo())) {
						isSSRAdded = true;
						break;
					}
				}
			}
		}
		return isSSRAdded;
	}

	public static LocalDate getRetreieveDOB(Passenger pax) {
		LocalDate localDate = TgsDateUtils.calenderToLocalDate(pax.getPassengerTypeInfo().getDOB());
		LocalDate defaultDOB = LocalDate.of(9999, 12, 31);
		if (localDate.equals(defaultDOB)) {
			return null;
		}
		return localDate;
	}

	public static void cacheFareComponents(List<TripInfo> tripInfos, AirSourceConfigurationOutput sourceConfiguration,
			SupplierConfiguration configuration) {

		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getCacheFareComponents())) {
			FareComponentHelper.cacheFareComponents(tripInfos, sourceConfiguration.getCacheableFareComponents(),
					configuration);
		}
	}

	public static String getNearByAirport(String airportCode, AirSearchQuery searchQuery, Integer sourceId,
			User bookingUser) {
		String nearByCode = AirUtils.getNearByAirport(airportCode, sourceId, searchQuery, bookingUser);
		if (StringUtils.isNotBlank(nearByCode)) {
			return nearByCode;
		}
		return airportCode;
	}

	public static boolean isStationNotExist(AxisFault e) {
		return (e != null && StringUtils.isNotBlank(e.getMessage())
				&& e.getMessage().contains("Station does not exist"));
	}

	public static boolean isBusinessClass(SegmentInfo segment) {
		FareDetail fareDetail = segment.getFareDetailsForPriceInfo(0).getOrDefault(PaxType.ADULT, null);
		return fareDetail != null && CabinClass.BUSINESS.equals(fareDetail.getCabinClass());
	}


	public static void filterSearchResults(AirSearchResult searchResult, AirSearchQuery searchQuery,
			SupplierConfiguration configuration) {
		if (searchQuery.isIntlReturn() && searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			searchResult.getTripInfos().forEach((tripType, trips) -> {
				if (trips.size() > 14) {
					log.info("Removing Trips for {} trips size {} sq {}", configuration.getSourceId(), trips.size(),
							searchQuery);
					trips.removeAll(trips.subList(14, trips.size()));
				}
			});
		}
	}

	public static String getNationality(FlightTravellerInfo flightTravellerInfo) {
		if (StringUtils.isNotBlank(flightTravellerInfo.getPassportNationality())) {
			return flightTravellerInfo.getPassportNationality();
		}
		ClientGeneralInfo generalInfo = ServiceCommunicatorHelper.getClientInfo();
		return generalInfo.getNationality();
	}

	public static List<List<Pair<TripInfo, Integer>>> getBatchTrips(List<TripInfo> tripInfos, Integer batchSize,
			AirSourceConfigurationOutput sourceConfiguration) {
		List<List<Pair<TripInfo, Integer>>> batchTrips1 = new ArrayList<>();
		List<TripInfo> directFlights = tripInfos.stream()
				.filter(tripInfo -> (tripInfo.getSegmentInfos().size() == 1 && !AirUtils
						.isAllFareComponentPresent(tripInfo, sourceConfiguration.getCacheableFareComponents(), false)))
				.collect(Collectors.toList());
		List<TripInfo> connectingFlights = tripInfos.stream()
				.filter((tripInfo -> tripInfo.getSegmentInfos().size() > 1 && !AirUtils
						.isAllFareComponentPresent(tripInfo, sourceConfiguration.getCacheableFareComponents(), false)))
				.collect(Collectors.toList());
		Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets = new HashMap<>();
		AtomicInteger index = new AtomicInteger(0);
		initializeBucket(tripBuckets, index.get());
		connectingFlights.forEach(connectingFlight -> {
			int validPriceCount = NavitaireUtils.getValidPriceCount(connectingFlight);
			for (int priceIndex = 0; priceIndex < validPriceCount; priceIndex++) {
				addToBucket(tripBuckets, connectingFlight, index, batchSize, priceIndex);
			}
		});
		index.set(0);
		directFlights.forEach(directFlight -> {
			int validPriceCount = NavitaireUtils.getValidPriceCount(directFlight);
			for (int priceIndex = 0; priceIndex < validPriceCount; priceIndex++) {
				addToBucket(tripBuckets, directFlight, index, batchSize, priceIndex);
			}
		});
		tripBuckets.forEach((ind, trips) -> {
			batchTrips1.addAll(Lists.partition(trips, batchSize));
		});

		return batchTrips1;
	}

	private static boolean addToBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets,
			TripInfo currentTripInfo, AtomicInteger index, Integer batchSize, Integer priceIndex) {
		boolean isAdded = false;

		if (!checkIfTripExistsInBucket(tripBuckets, index.get(), currentTripInfo)
				&& !isShiftBucket(tripBuckets, index.get(), batchSize)) {
			tripBuckets.get(index.get()).add(Pair.of(currentTripInfo, priceIndex));
			isAdded = true;
		} else {
			index.getAndIncrement();
			initializeBucket(tripBuckets, index.get());
			isAdded = addToBucket(tripBuckets, currentTripInfo, index, batchSize, priceIndex);
		}

		return isAdded;
	}

	private static boolean isShiftBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets, Integer index,
			Integer batchSize) {
		return tripBuckets.get(index).size() >= batchSize;
	}

	private static boolean checkIfTripExistsInBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets,
			Integer index, TripInfo currentTripInfo) {
		List<String> flightKey = currentTripInfo.getFlightNumberSet().stream().collect(Collectors.toList());
		AtomicBoolean isExist = new AtomicBoolean();
		if (MapUtils.isNotEmpty(tripBuckets)) {
			AtomicInteger bucketIndex = new AtomicInteger(0);
			tripBuckets.forEach((bucket, tripPairs) -> {
				if (index == bucketIndex.intValue()) {
					tripPairs.forEach(pair -> {
						List<String> bucketKey =
								pair.getFirst().getFlightNumberSet().stream().collect(Collectors.toList());
						if (!isExist.get()) {
							isExist.set(isTwoFlightInSameBucket(bucketKey, flightKey));
						}
					});
				}
				bucketIndex.getAndIncrement();
			});
		}
		return isExist.get();
	}

	public static boolean isTwoFlightInSameBucket(List<String> bucketFlightKey, List<String> flightKey) {
		AtomicBoolean isExist = new AtomicBoolean();
		flightKey.forEach(key -> {
			if (!isExist.get() && bucketFlightKey.contains(key)) {
				isExist.set(true);
			}
		});
		return isExist.get();
	}

	private static void initializeBucket(Map<Integer, List<Pair<TripInfo, Integer>>> tripBuckets, Integer index) {
		if (MapUtils.isEmpty(tripBuckets) || MapUtils.getObject(tripBuckets, index) == null) {
			tripBuckets.put(index, new ArrayList<>());
			return;
		}
	}

	public static List<List<Pair<TripInfo, Integer>>> getTripsInBucket(List<TripInfo> trips, Integer batchSize,
			AirSourceConfigurationOutput sourceConfiguration) {
		List<List<Pair<TripInfo, Integer>>> batchTrips1 = new ArrayList<>();
		trips.forEach(trip -> {
			int validPriceCount = NavitaireUtils.getValidPriceCount(trip);
			for (int priceIndex = 0; priceIndex < validPriceCount; priceIndex++) {
				batchTrips1.add(Arrays.asList(Pair.of(trip, priceIndex)));
			}
		});
		return batchTrips1;
	}


	public static boolean isDomesticSegment(SegmentInfo segmentInfo) {
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setFromCityOrAirport(AirportHelper.getAirportInfo(segmentInfo.getDepartureAirportCode()));
		routeInfo.setToCityOrAirport(AirportHelper.getAirportInfo(segmentInfo.getArrivalAirportCode()));
		if (routeInfo.getFromCityOrAirport().getCountry().equals(clientInfo.getCountry())
				&& routeInfo.getToCityOrAirport().getCountry().equals(clientInfo.getCountry())) {
			// Domestic Sector is Not applicable
			return true;
		}
		return false;
	}

	public static String getPassengerName(BookingName bookingName) {
		return StringUtils.join(bookingName.getFirstName(), bookingName.getLastName());
	}

	public static String getCompanyName(User bookingUser, ClientGeneralInfo clientInfo) {
		String companyName = AirSupplierUtils.getCompanyName(bookingUser, clientInfo);
		if (companyName.length() > 64) {
			return companyName.substring(0, 63);
		}
		return companyName;
	}

	/**
	 * copy fare from other segments to first segment for same ssrCode only for baggage SSR
	 * 
	 * @param firstSegmentInfo
	 * @param otherSegmentInfo
	 */
	@SuppressWarnings("unchecked")
	public static void mergeBaggageinFirstSegment(SegmentInfo firstSegmentInfo, SegmentInfo otherSegmentInfo) {
		List<BaggageSSRInformation> baggageSSRFirstSegment =
				(List<BaggageSSRInformation>) firstSegmentInfo.getSsrInfo().get(SSRType.BAGGAGE);
		List<BaggageSSRInformation> baggageSSROtherSegment =
				(List<BaggageSSRInformation>) otherSegmentInfo.getSsrInfo().get(SSRType.BAGGAGE);
		for (Iterator<BaggageSSRInformation> iter = baggageSSRFirstSegment.iterator(); iter.hasNext();) {
			BaggageSSRInformation baggage = iter.next();
			if (!parseAmountToFirstSegment(baggage, baggageSSROtherSegment)) {
				iter.remove();
			}
		}
		setSSRAmountToZero(baggageSSROtherSegment);
	}

	public static AirSearchResult processSearchResultForItineraryPrice(AirSearchResult searchResult, Integer sourceId,
			boolean isIntlReturn, User bookingUser) {
		if (isIntlReturn) {
			List<TripInfo> combinedTrips =
					buildCombinationWithoutRedundantTrip(searchResult.getTripInfos().get(TripInfoType.ONWARD.name()),
							searchResult.getTripInfos().get(TripInfoType.RETURN.name()), sourceId, bookingUser);
			searchResult = new AirSearchResult();
			if (CollectionUtils.isNotEmpty(combinedTrips)) {
				searchResult.getTripInfos().put(TripInfoType.COMBO.name(), combinedTrips);
			}
			// searchResult.getTripInfos().remove(TripInfoType.ONWARD.name());
			// searchResult.getTripInfos().remove(TripInfoType.RETURN.name());
		}
		return searchResult;
	}

	public static List<TripInfo> buildCombinationWithoutRedundantTrip(List<TripInfo> onwardTrips,
			List<TripInfo> returnTrips, int sourceId, User bookingUser) {
		if (CollectionUtils.isEmpty(onwardTrips) || CollectionUtils.isEmpty(returnTrips)) {
			return null;
		}
		List<TripInfo> result = new ArrayList<TripInfo>();
		int returnIndex = 0;
		Map<String, Boolean> combinedTripMap = new HashMap<>();
		List<TripInfo> tempReturnTripList = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(returnTrips),
				new TypeToken<List<TripInfo>>() {}.getType());
		for (int onwardIndex = 0; onwardIndex < onwardTrips.size(); onwardIndex++) {
			TripInfo onwardTrip = onwardTrips.get(onwardIndex);
			boolean breakValue = false;
			int returnIndexCopy = returnIndex;
			do {
				TripInfo returnTrip = returnTrips.get(returnIndexCopy);
				TripInfo combinedTrip = getCombinedTrip(onwardTrip, returnTrip, sourceId, bookingUser);
				if (combinedTrip != null && combinedTripMap.get(getTripKey(combinedTrip)) == null) {
					result.add(combinedTrip);
					combinedTripMap.put(getTripKey(combinedTrip), true);
					breakValue = true;
					tempReturnTripList.remove(returnTrip);
				}
				returnIndexCopy = (returnIndexCopy + 1 == returnTrips.size()) ? 0 : returnIndexCopy + 1;
				if (returnIndexCopy == returnIndex) {
					breakValue = true;
				}
			} while (!breakValue);
			returnIndex = returnIndexCopy;
		}

		int onwardIndex = 0;
		for (returnIndex = 0; returnIndex < tempReturnTripList.size(); returnIndex++) {
			TripInfo returnTrip = tempReturnTripList.get(returnIndex);
			boolean breakValue = false;
			int onwardIndexCopy = onwardIndex;
			do {
				TripInfo onwardTrip = onwardTrips.get(onwardIndexCopy);
				TripInfo combinedTrip = getCombinedTrip(onwardTrip, returnTrip, sourceId, bookingUser);
				if (combinedTrip != null && combinedTripMap.get(getTripKey(combinedTrip)) == null) {
					result.add(combinedTrip);
					combinedTripMap.put(getTripKey(combinedTrip), true);
					breakValue = true;
				}
				onwardIndexCopy = (onwardIndexCopy + 1 == onwardTrips.size()) ? 0 : onwardIndexCopy + 1;
				if (onwardIndexCopy == onwardIndex) {
					breakValue = true;
				}
			} while (!breakValue);
			onwardIndex = onwardIndexCopy;
		}
		return result;
	}

	public static TripInfo getCombinedTrip(TripInfo onwardTrip, TripInfo returnTrip, int sourceId, User BookingUser) {
		LocalDateTime onwardArrivalTime = onwardTrip.getArrivalTime();
		LocalDateTime returnDepartureTime = returnTrip.getDepartureTime();
		if (Duration.between(onwardArrivalTime, returnDepartureTime).toMinutes() > AirUtils
				.getCombinationAllowedMinutes(sourceId, BookingUser)) {
			TripInfo onwardTripCopy = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
			TripInfo returnTripCopy = new GsonMapper<>(returnTrip, TripInfo.class).convert();
			if (onwardTripCopy.isPriceInfosNotEmpty() && returnTripCopy.isPriceInfosNotEmpty()) {
				returnTripCopy.getSegmentInfos().get(0).setIsReturnSegment(true);
				onwardTripCopy.getSegmentInfos().addAll(returnTripCopy.getSegmentInfos());
				return onwardTripCopy;
			}
		}
		return null;
	}

	private static String getTripKey(TripInfo combinedTrip) {
		StringJoiner joiner = new StringJoiner("_");
		combinedTrip.getSegmentInfos().forEach(segment -> {
			joiner.add(segment.getDepartureAirportCode());
			joiner.add(segment.getArrivalAirportCode());
			joiner.add(segment.getAirlineCode(false));
			joiner.add(segment.getFlightNumber());
		});
		return joiner.toString();
	}

	public static AirSearchResult splitItineraryPricedTrips(AirSearchResult searchResult) {
		List<TripInfo> onwardTrips = new ArrayList<TripInfo>();
		List<TripInfo> returnTrips = new ArrayList<TripInfo>();
		Map<String, Boolean> onwardTripMap = new HashMap<String, Boolean>();
		Map<String, Boolean> returnTripMap = new HashMap<String, Boolean>();
		if (CollectionUtils.isNotEmpty(searchResult.getTripInfos().get(TripInfoType.COMBO.name()))) {
			searchResult.getTripInfos().get(TripInfoType.COMBO.name()).forEach(tripInfo -> {
				List<TripInfo> combinedTrips = AirUtils.splitTripInfo(tripInfo, false);
				if (combinedTrips.size() == 2) {
					TripInfo onwardTrip = combinedTrips.get(0);
					TripInfo returnTrip = combinedTrips.get(1);
					if (onwardTripMap.get(getTripKey(onwardTrip)) == null) {
						onwardTrips.add(onwardTrip);
						onwardTripMap.put(getTripKey(onwardTrip), true);
					}
					if (returnTripMap.get(getTripKey(returnTrip)) == null) {
						returnTrips.add(returnTrip);
						returnTripMap.put(getTripKey(returnTrip), true);
					}
				}
			});
			searchResult = new AirSearchResult();
			searchResult.getTripInfos().put(TripInfoType.ONWARD.name(), onwardTrips);
			searchResult.getTripInfos().put(TripInfoType.RETURN.name(), returnTrips);
		}
		return searchResult;
	}

	private static boolean parseAmountToFirstSegment(BaggageSSRInformation firstbaggage,
			List<BaggageSSRInformation> baggageSSROtherSegment) {
		for (BaggageSSRInformation baggage : baggageSSROtherSegment) {
			if (firstbaggage.getCode().equals(baggage.getCode()) && Objects.nonNull(baggage.getAmount())) {
				firstbaggage.setAmount(firstbaggage.getAmount() + baggage.getAmount());
				return true;
			}
		}
		return false;
	}

	private static void setSSRAmountToZero(List<BaggageSSRInformation> baggageSSROtherSegment) {
		baggageSSROtherSegment.forEach(ssrInfo -> {
			ssrInfo.setAmount(null);
		});
	}

	public static String getCity(String name) {
		if (name.length() > 32) {
			return name.substring(0, 31);
		}
		return name;
	}

	public static String getAddress(String address) {
		if (StringUtils.isNotBlank(address) && address.length() > 51) {
			// address cannot be More than 51 Characters
			address = address.substring(0, 51);
		}
		return address;
	}

	public static String getValidCompanyName(String gstName) {
		if (StringUtils.isNotBlank(gstName) && gstName.length() > 64) {
			gstName = gstName.substring(0, 63);
		}
		return gstName;
	}

	public static int getBatchSize(List<TripInfo> trips, AirSearchQuery searchQuery) {
		if (searchQuery.isIntlReturn()) {
			return 3;
		} else {
			return 6;
		}
	}


	public static boolean isInfantFarePresentInTrip(TripInfo tripInfo) {
		AtomicBoolean isPresent = new AtomicBoolean();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.getPriceInfoList().forEach(priceInfo -> {
				FareDetail infantFareDetail = priceInfo.getFareDetail(PaxType.INFANT);
				if (infantFareDetail != null && MapUtils.isNotEmpty(infantFareDetail.getFareComponents())) {
					isPresent.set(true);
				}
			});
		});
		return isPresent.get();
	}


	public static String getFlightReference(SegmentInfo segmentInfo) {
		return StringUtils.join(segmentInfo.getDepartureAirportCode(), segmentInfo.getArrivalAirportCode());
	}

	public static FlightTravellerInfo getTravellerInfo(SegmentInfo segmentInfo, BookingName bookingName) {
		FlightTravellerInfo travellerInfo = null;
		if (segmentInfo != null) {
			for (FlightTravellerInfo travellerInfo1 : segmentInfo.getTravellerInfo()) {
				if (StringUtils.equalsIgnoreCase(travellerInfo1.getFirstName(), bookingName.getFirstName())
						&& StringUtils.equalsIgnoreCase(travellerInfo1.getLastName(), bookingName.getLastName())) {
					travellerInfo = travellerInfo1;
				}
			}
		}
		return travellerInfo;
	}

	public static FlightTravellerInfo filteredTraveller(List<SegmentInfo> segmentInfos, Passenger passenger,
			PassengerFee passengerFee) {
		FlightTravellerInfo travellerInfo = null;
		BookingName bookingName = passenger.getNames().getBookingName()[0];
		for (SegmentInfo segmentInfo : segmentInfos) {
			String flightReference = NavitaireUtils.getFlightReference(segmentInfo);
			if (passengerFee.getFlightReference().contains(flightReference)) {
				travellerInfo = NavitaireUtils.getTravellerInfo(segmentInfo, bookingName);
			}
		}
		return travellerInfo;
	}

	public static SegmentInfo getSegmentInfo(List<SegmentInfo> segmentInfos, PassengerFee passengerFee) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			String flightReference = NavitaireUtils.getFlightReference(segmentInfo);
			if (passengerFee.getFlightReference().contains(flightReference)
					&& passengerFee.getFlightReference().contains(segmentInfo.getAirlineCode(false))
					&& passengerFee.getFlightReference().contains(segmentInfo.getFlightNumber())) {
				return segmentInfo;
			}
		}
		return null;
	}

	public static CabinClass getAnyFareDetails(Map<PaxType, FareDetail> fareDetails) {
		AtomicReference<CabinClass> cabinClass = new AtomicReference<>(CabinClass.ECONOMY);
		fareDetails.forEach(((paxType, fareDetail) -> {
			cabinClass.set(fareDetail.getCabinClass());
		}));
		return cabinClass.get();
	}

	public static List<TripInfo> collectTripInfos(List<Pair<TripInfo, Integer>> tripPair) {
		List<TripInfo> flightList = new ArrayList<>();
		for (Pair<TripInfo, Integer> pair : tripPair) {
			flightList.add(pair.getFirst());
		}
		return flightList;
	}

	public static List<Pair<TripInfo, Integer>> getTripPairs(List<TripInfo> trips) {
		List<Pair<TripInfo, Integer>> tripPairs = new ArrayList<Pair<TripInfo, Integer>>();
		for (TripInfo trip : trips) {
			tripPairs.add(Pair.of(trip, 0));
		}
		return tripPairs;
	}

	private static int getValidPriceCount(TripInfo tripInfo) {
		AtomicInteger validPriceCount = new AtomicInteger(tripInfo.getSegmentInfos().get(0).getPriceInfoList().size());
		tripInfo.getSegmentInfos().forEach(segment -> {
			if (segment.getPriceInfoList().size() < validPriceCount.get()) {
				validPriceCount.set(segment.getPriceInfoList().size());
			}
		});
		return validPriceCount.get();
	}

	public static void filterTripAndPriceOptions(AirSearchResult searchResult, AirSearchQuery searchQuery) {
		try {
			if (searchResult != null && MapUtils.isNotEmpty(searchResult.getTripInfos())) {
				for (String key : searchResult.getTripInfos().keySet()) {
					List<TripInfo> tripInfos = searchResult.getTripInfos().get(key);
					if (CollectionUtils.isNotEmpty(tripInfos)) {
						for (Iterator<TripInfo> tripIter = tripInfos.iterator(); tripIter.hasNext();) {
							filterRedundantPriceInTrip(tripIter.next());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to filter tripAndPriceoptions for searchQuery {]", searchQuery);
		}
	}

	public static void filterRedundantPriceInTrip(TripInfo tripInfo) {
		if (tripInfo != null && CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			AtomicInteger minimumPriceOptionCount =
					new AtomicInteger(tripInfo.getSegmentInfos().get(0).getPriceInfoList().size());
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					List<String> fareTypes = new ArrayList<>();
					for (Iterator<PriceInfo> priceIter = segmentInfo.getPriceInfoList().iterator(); priceIter
							.hasNext();) {
						PriceInfo priceInfo = priceIter.next();
						// Only 1 fareType per tripInfo//segmentinfo will be returned , rest will be
						// filtered
						String key = StringUtils.join(priceInfo.getFareType(), priceInfo.getCabinClass(PaxType.ADULT));
						if (fareTypes.contains(key)) {
							log.debug("Removing priceInfo {} for tripInfo {}", priceInfo, tripInfo);
							priceIter.remove();
						} else {
							fareTypes.add(key);
						}
					}
					if (minimumPriceOptionCount.get() > segmentInfo.getPriceInfoList().size()) {
						minimumPriceOptionCount.set(segmentInfo.getPriceInfoList().size());
					}
				}
			});
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				if (minimumPriceOptionCount.get() < segmentInfo.getPriceInfoList().size()) {
					List<PriceInfo> priceList = new ArrayList<>();
					for (int index = 0; index < minimumPriceOptionCount.get(); index++) {
						priceList.add(segmentInfo.getPriceInfo(index));
					}
					segmentInfo.setPriceInfoList(priceList);
				}
			});
		}
	}


	public static boolean isDivideByPnr(BookingSegments bookingSegments,
			List<FlightTravellerInfo> cancelledTravellerInfoList) {
		int unCancelledPassenger = 0;
		List<SegmentInfo> selectedCancelledOriginalSegments = new ArrayList<SegmentInfo>();
		for (SegmentInfo cancelledsegmentInfo : bookingSegments.getCancelSegments()) {
			for (SegmentInfo originalSegment : bookingSegments.getSegmentInfos()) {
				if (cancelledsegmentInfo.getId().equals(originalSegment.getId())) {
					selectedCancelledOriginalSegments.add(originalSegment);
				}
			}
		}
		for (SegmentInfo segmentInfo : selectedCancelledOriginalSegments) {
			for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
				if (travellerInfo.getStatus() == null || !travellerInfo.getStatus().equals(TravellerStatus.CANCELLED)) {
					unCancelledPassenger++;
				}
			}
		}

		boolean isDivideDeByPnrAllowed = unCancelledPassenger != cancelledTravellerInfoList.size();
		if (isDivideDeByPnrAllowed) {
			String parentPnr = cancelledTravellerInfoList.get(0).getPnr();
			for (FlightTravellerInfo cancellFlightTravellerInfo : selectedCancelledOriginalSegments.get(0)
					.getTravellerInfo()) {
				if (!cancellFlightTravellerInfo.getPnr().equals(parentPnr)) {
					isDivideDeByPnrAllowed = false;
					break;
				}
			}
		}
		return isDivideDeByPnrAllowed;
	}

	public static SegmentInfo findSegmentFromTripInfos(List<TripInfo> tripInfos, Segment segment) {
		String segmentKey = StringUtils.join(segment.getDepartureStation(), "_", segment.getArrivalStation());
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (segmentInfo.getSegmentKey().equalsIgnoreCase(segmentKey)) {
					return segmentInfo;
				}
			}
		}
		return null;
	}

	public static void updateDivideByPnr(List<SegmentInfo> segmentInfos,
			List<FlightTravellerInfo> cancelledTravellerInfoList, String dividedPNR) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
				for (FlightTravellerInfo cancelledTraveller : cancelledTravellerInfoList) {
					if (flightTravellerInfo.getId().equals(cancelledTraveller.getId())) {
						flightTravellerInfo.setPnr(dividedPNR);
					}
				}
			}
		}

	}

	public static double getAirCancellationFees(BookingSegments bookingSegments) {
		double airlineCancellationFee = 0.0;
		for (SegmentInfo segmentInfo : bookingSegments.getCancelSegments()) {
			for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
				if (travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.ACF, 0.0) != null) {
					airlineCancellationFee +=
							travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.ACF, 0.0);
				}

			}
		}
		return airlineCancellationFee;
	}

	public static CreditFile getCreditFile(String pnr) {
		CreditFile creditFile = new CreditFile();
		creditFile.setRecordLocator(pnr);
		return creditFile;
	}

	public static void setIsSpecialPrivateFare(PriceInfo pInfo, SupplierConfiguration configuration,
			AirSourceConfigurationOutput sourceConfiguration) {
		if (CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())
				&& sourceConfiguration != null
				&& CollectionUtils.isNotEmpty(sourceConfiguration.getSpecialAccountCodes())) {
			if (sourceConfiguration.getSpecialAccountCodes()
					.containsAll(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
				pInfo.getMiscInfo().setIsPrivateFare(Boolean.TRUE);
				pInfo.setFareIdentifier(FareType.PUBLISHED);
			}
		}
	}
}
