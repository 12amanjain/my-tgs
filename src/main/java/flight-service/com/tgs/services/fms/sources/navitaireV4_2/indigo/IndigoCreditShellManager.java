package com.tgs.services.fms.sources.navitaireV4_2.indigo;

import com.tgs.services.base.LogData;
import lombok.extern.slf4j.Slf4j;
import lombok.experimental.SuperBuilder;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.sources.navitaireV4_2.NavitaireServiceManager;
import com.tgs.services.fms.supplier.indigo.AccountCreditData;
import com.tgs.services.fms.supplier.indigo.AccountCredits;
import com.tgs.services.fms.supplier.indigo.IndiGoCSBalance;
import com.tgs.services.fms.supplier.indigo.IndiGoError;
import com.tgs.services.fms.supplier.indigo.IndiGoGetBookingResponse;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.utils.exception.air.SupplierRemoteException;

@Slf4j
@SuperBuilder
public class IndigoCreditShellManager extends NavitaireServiceManager {

	static DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public final static String RETREIEVE_URL = "https://book.goindigo.in/Booking/RetrieveAEM";

	public final static String VIEW_URL = "https://book.goindigo.in/Booking/ViewAEM";

	// header
	public static final String KEY_ACCEPT = "Accept";
	public static final String KEY_ACCEPT_ENCODING = "Accept-Encoding";
	public static final String KEY_ACCEPT_LANGUAGE = "Accept-Language";
	public static final String KEY_CACHE_CONTROL = "Cache-Control";
	public static final String KEY_CONNECTION = "Connection";
	public static final String KEY_USER_AGENT = "User-Agent";


	// value
	public static final String VALUE_KEEP_ALIVE = "keep-alive";
	public static final String VALUE_NO_CACHE = "no-cache";
	public static final String VALUE_ACCEPT_LANGUAGE = "en-GB,en-US;q=0.9,en;q=0.8";
	public static final String VALUE_ACCEPT_ENCODING = "gzip, deflate, br";
	public static final String VALUE_USER_AGENT =
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";

	public static final String VALUE_ACCEPT =
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";

	@Override
	public AirCreditShellResponse fetchCreditShellBalance(AirCreditShellRequest request) {
		AirCreditShellResponse shellResponse = AirCreditShellResponse.builder().build();
		Gson gson = GsonUtils.getGson();

		String requestUrl = StringUtils.join(RETREIEVE_URL, getRequestData(false, new LinkedHashMap<>()));
		IndiGoGetBookingResponse bookingResponse = null;
		try {
			listener.setType("CSBalance");
			Connection connection =
					Jsoup.connect(requestUrl).timeout(60000).ignoreContentType(true).ignoreHttpErrors(true);
			setConnectionProperties(connection);

			Document pnrResponse = getResponse(connection, Connection.Method.POST, getGetBookingParameters(request));
			bookingResponse = gson.fromJson(pnrResponse.text(), IndiGoGetBookingResponse.class);
			if (bookingResponse != null && CollectionUtils.isEmpty(bookingResponse.getIndiGoError().getErrors())) {
				Map<String, String> cookies = connection.response().cookies();
				connection = Jsoup.connect(VIEW_URL).timeout(60000).ignoreContentType(true).ignoreHttpErrors(true);
				setConnectionProperties(connection);
				connection.cookies(cookies);
				pnrResponse = getResponse(connection, Connection.Method.GET, null);
				bookingResponse = gson.fromJson(pnrResponse.text(), IndiGoGetBookingResponse.class);
				if (bookingResponse.getIndiGoError() != null && bookingResponse.getIndigoCSBalance() != null
						&& bookingResponse.getIndigoCSBalance().getAccount() != null) {
					IndiGoCSBalance csBalance = bookingResponse.getIndigoCSBalance();
					AccountCredits credits = csBalance.getAccount().getAccountCredits();
					if (CollectionUtils.isNotEmpty(credits.getItems())) {
						try {
							Double creditBalance = new Double(0);
							Calendar csCalendar = Calendar.getInstance();
							csCalendar.setTime(df.parse(csBalance.getCSExpirationDateTime()));
							LocalDateTime minExpriyDate = TgsDateUtils.toLocalDateTime(csCalendar);
							for (AccountCreditData creditData : credits.items) {
								if (creditData.getAvailable() > 0) {
									// credit shell available balance which means remaining or available balance
									creditBalance += creditData.getAvailable();
									Calendar calendar = Calendar.getInstance();
									calendar.setTime(df.parse(creditData.getExpiration()));
									LocalDateTime currentExpiry = TgsDateUtils.toLocalDateTime(calendar);
									if (currentExpiry.isBefore(minExpriyDate)) {
										minExpriyDate = currentExpiry;
									}
								}
							}
							shellResponse.setCreditBalance(creditBalance);
							shellResponse.setExpiryDate(minExpriyDate);
						} catch (ParseException pe) {
							log.error("Unable to parse credit balance expiry date {}", request.getPnr());
						}
					}
				} else {
					IndiGoError error = bookingResponse.getIndiGoError();
					if (CollectionUtils.isNotEmpty(error.getErrors())) {
						throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA,
								error.getErrors().get(0).getMessage());
					}
				}
			}
		} finally {
			if (listener != null) {
				listener.extractMessage(requestUrl, "CSBalanceRq");
				listener.extractMessage(gson.toJson(bookingResponse), "CSBalanceRs");
			}
		}
		return shellResponse;
	}

	private LinkedHashMap<String, String> getGetBookingParameters(AirCreditShellRequest request) {
		List<FlightTravellerInfo> travellerInfos = request.getTravellerInfos();
		String lastName = null;
		if (StringUtils.isNotBlank(request.getLastName())) {
			lastName = request.getLastName();
		} else {
			lastName = travellerInfos.get(0).getLastName();
		}
		LinkedHashMap<String, String> parameters = new LinkedHashMap<>();
		parameters.put("indiGoRetrieveBooking.RecordLocator", request.getPnr());
		parameters.put("polymorphicField", lastName);
		parameters.put("typeSelected", "SearchByNAMEFLIGHT");
		parameters.put("indiGoRetrieveBooking.IndiGoRegisteredStrategy",
				"Nps.IndiGo.Strategies.IndigoValidatePnrContactNameStrategy, Nps.IndiGo");
		parameters.put("indiGoRetrieveBooking.LastName", lastName);
		parameters.put("indiGoRetrieveBooking.IsToEmailItinerary", "false");
		parameters.put("submit", "Retrieve Booking");
		return parameters;
	}

	private static String getRequestData(boolean isEncodedUTF, LinkedHashMap<String, String> requestData) {
		Set<String> keys = requestData.keySet();
		StringBuffer sbuf = new StringBuffer();
		if (isEncodedUTF) {
			for (String key : keys) {
				try {
					sbuf.append(key).append("=").append(URLEncoder.encode(requestData.get(key), "UTF-8")).append("&");
				} catch (UnsupportedEncodingException e) {
					sbuf.append(key).append("=").append(requestData.get(key)).append("&");
				}
			}

		} else {
			for (String key : keys) {
				sbuf.append(key).append("=").append(requestData.get(key)).append("&");
			}

		}
		return sbuf.toString();
	}

	// jsoup
	private static void setConnectionProperties(Connection connection) {
		connection.header(KEY_ACCEPT, VALUE_ACCEPT);
		connection.header(KEY_ACCEPT_ENCODING, VALUE_ACCEPT_ENCODING);
		connection.header(KEY_ACCEPT_LANGUAGE, VALUE_ACCEPT_LANGUAGE);
		connection.header(KEY_CACHE_CONTROL, VALUE_NO_CACHE);
		connection.header(KEY_CONNECTION, VALUE_KEEP_ALIVE);
		connection.header(KEY_USER_AGENT, VALUE_USER_AGENT);
	}

	private static Document getResponse(Connection connection, Connection.Method method,
			LinkedHashMap<String, String> postData) {
		try {
			connection.method(method);
			if (Connection.Method.POST.equals(method)) {
				connection.data(postData);
				return connection.post();
			} else {
				return connection.get();
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		}
	}

}
