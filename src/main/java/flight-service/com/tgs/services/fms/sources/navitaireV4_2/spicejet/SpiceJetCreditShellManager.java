package com.tgs.services.fms.sources.navitaireV4_2.spicejet;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.navitaireV4_2.NavitaireServiceManager;
import com.tgs.services.fms.supplier.spicejet.SGCreditShellResponse;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class SpiceJetCreditShellManager extends NavitaireServiceManager {

	@Override
	public AirCreditShellResponse fetchCreditShellBalance(AirCreditShellRequest request) {
		AirCreditShellResponse shellResponse = AirCreditShellResponse.builder().build();
		Map<String, Object> rq = buildFetchBalanceRequest(request);
		FlightAPIURLRuleCriteria apiUrlRule = AirUtils.getAirEndPointURL(sourceConfiguration);
		HttpUtils httpUtils = HttpUtils.builder().urlString(apiUrlRule.getFetchCreditShellURL())
				.postData(GsonUtils.getGson().toJson(rq)).headerParams(getHeaderParams())
				.proxy(AirUtils.getProxy(bookingUser)).build();
		SGCreditShellResponse response = null;
		try {
			response = httpUtils.getResponse(SGCreditShellResponse.class).orElse(null);
			if (response == null) {
				throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA,
						StringUtils.join("Unable to retrieve balance for credit shell pnr - ", request.getPnr()));
			} else if (response != null && response.getStatus() == 0) {
				if (StringUtils.isNotBlank(response.getRemarks())) {
					throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA, response.getRemarks());
				} else if (response.getExpiryDate() == null || LocalDateTime.now().isAfter(response.getExpiryDate())) {
					throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA);
				} else if (response.getAmount() == null || Double.compare(response.getAmount(), 0d) == 0) {
					throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA);
				}
			} else if (response != null && response.getExpiryDate() != null
					&& LocalDateTime.now().isBefore(response.getExpiryDate())) {
				shellResponse.setCreditBalance(response.getAmount());
				shellResponse.setExpiryDate(response.getExpiryDate());
			}
		} catch (IOException ie) {
			log.error("Unable to connect to service for credit shell pnr - {}", request.getPnr());
			throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA, ie.getMessage());
		} finally {
			listener.extractMessage(GsonUtils.getGson().toJson(rq), "CSBalanceRq");
			listener.extractMessage(GsonUtils.getGson().toJson(response), "CSBalanceRs");
		}
		return shellResponse;
	}

	private Map<String, Object> buildFetchBalanceRequest(AirCreditShellRequest request) {
		Map<String, Object> rq = new HashMap<>();
		rq.put("Lastname", BaseUtils.getParticularPaxTravellerInfo(request.getTravellerInfos(), PaxType.ADULT).get(0)
				.getLastName());
		rq.put("OrganizationCode", request.getSupplierConfig().getSupplierCredential().getOrganisationCode());
		rq.put("CSPNR", request.getPnr());
		return rq;
	}

	private Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("Content-Type", "application/json");
		return headerParams;
	}

	@Override
	public double addCreditShellPaymentToBooking(double pnrCreditBalance, String newPNR, String csPNR) {
		double amountPaidInCreditShell = 0d;
		Map<String, Object> request = new HashMap<>();
		SGCreditShellResponse response = null;
		try {
			request.put("NewPNR", newPNR);
			request.put("CSPNR", csPNR);
			Map<String, String> headerParams = new HashMap<String, String>();
			headerParams.put("Content-Type", "application/json");
			FlightAPIURLRuleCriteria apiUrlRule = AirUtils.getAirEndPointURL(sourceConfiguration);
			HttpUtils httpUtils = HttpUtils.builder().urlString(apiUrlRule.getCreditShellPaymentURL())
					.postData(GsonUtils.getGson().toJson(request)).headerParams(headerParams)
					.proxy(AirUtils.getProxy(bookingUser)).build();
			response = httpUtils.getResponse(SGCreditShellResponse.class).orElse(null);
			if (response == null) {
				throw new SupplierUnHandledFaultException(
						"Unable to add credit shell payment for booking - " + bookingId);
			} else if (response != null && response.getStatus() == 0) {
				if (StringUtils.isNotBlank(response.getError()) || StringUtils.isNotBlank(response.getRemarks())) {
					throw new SupplierUnHandledFaultException(
							ObjectUtils.firstNonNull(response.getError(), response.getRemarks()));
				} else if (response.getExpiryDate() == null || LocalDateTime.now().isBefore(response.getExpiryDate())) {
					throw new SupplierUnHandledFaultException(SystemError.CREDIT_SHELL_BALANCE_NA.getMessage());
				}
			} else if (response != null && response.getStatus() != 0) {
				if (Double.compare(response.getAmount(), pnrCreditBalance) != 0) {
					throw new SupplierUnHandledFaultException(SystemError.CREDIT_SHELL_BALANCE_MISMATCH.getMessage());
				}
				amountPaidInCreditShell = response.getAmount().doubleValue();
			}
		} catch (IOException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			captureRequestResponse(bookingId, GsonUtils.getGson().toJson(request), GsonUtils.getGson().toJson(response),
					"AddCreditShellPayment");
		}
		return amountPaidInCreditShell;
	}

	@Override
	public boolean validateCreditShellPNR(String creditShellPNR, String sessionToken) {
		Map<String, Object> rq = new HashMap<>();
		SGCreditShellResponse response = null;
		try {
			rq.put("Signature", sessionToken);
			rq.put("CSPNR", creditShellPNR);
			Map<String, String> headerParams = new HashMap<String, String>();
			headerParams.put("Content-Type", "application/json");
			FlightAPIURLRuleCriteria apiUrlRule = AirUtils.getAirEndPointURL(sourceConfiguration);
			HttpUtils httpUtils = HttpUtils.builder().urlString(apiUrlRule.getValidateCreditShellURL())
					.postData(GsonUtils.getGson().toJson(rq)).headerParams(headerParams)
					.proxy(AirUtils.getProxy(bookingUser)).build();

			response = httpUtils.getResponse(SGCreditShellResponse.class).orElse(null);
			if (response == null) {
				throw new SupplierUnHandledFaultException("Unable to validate credit shell for booking - " + bookingId);
			} else if (response != null && response.getStatus() == 0) {
				if (StringUtils.isNotBlank(response.getError()) || StringUtils.isNotBlank(response.getRemarks())) {
					throw new SupplierUnHandledFaultException(
							ObjectUtils.firstNonNull(response.getError(), response.getRemarks()));
				} else if (response.getExpiryDate() == null || LocalDateTime.now().isBefore(response.getExpiryDate())) {
					throw new SupplierUnHandledFaultException(SystemError.CREDIT_SHELL_BALANCE_NA.getMessage());
				}
			} else if (response != null && response.getStatus() != 0) {
				return true;
			}
		} catch (IOException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			captureRequestResponse(bookingId, GsonUtils.getGson().toJson(rq), GsonUtils.getGson().toJson(response),
					"ValidateCreditShell");
		}
		return false;
	}

}
