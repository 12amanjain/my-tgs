package com.tgs.services.fms.sources.otaradixx;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CreatePNRActionTypes;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;
import org.tempuri.RetrievePNRResponse;

@Slf4j
@Service
public class RadixxAirBookingFactory extends AbstractAirBookingFactory {

	private RadixxAirline airline;

	private String blockedPnr;

	protected SoapRequestResponseListner listener = null;

	private RadixxBindingService bindingService;

	public RadixxAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		this.airline = RadixxAirline.getOTARadixxAirline(supplierConf.getSourceId());
		FlightAPIURLRuleCriteria apiUrlRule = RadixxUtils.getRadixxPointURL(sourceConfiguration);
		bindingService = RadixxBindingService.builder().cacheCommunicator(cachingComm).apiUrlRule(apiUrlRule).build();
		if (listener == null) {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		}
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		initialize();
		/**
		 * In case session is expired or traveller took more than 20 min to complete payment/review section.
		 */
		RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf)
				.bindingService(bindingService).airline(airline).listener(listener).bookingUser(bookingUser).build();
		loginManager.init();
		if (Objects.isNull(supplierSession)) {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().sessionToken(loginManager.generateToken()).build();
			supplierSession = SupplierSession.builder().supplierSessionInfo(sessionInfo).bookingId(order.getBookingId())
					.sourceId(supplierConf.getSourceId()).supplierId(supplierConf.getSupplierId()).build();
		}
		loginManager.setBinaryToken(supplierSession.getSupplierSessionInfo().getSessionToken());
		double agencyCommission = loginManager.getAgencyCommission();
		int totalCommissionalablePaxCount = loginManager.getFromSegmentsPaxCount(bookingSegments.getSegmentInfos());
		agencyCommission = agencyCommission * totalCommissionalablePaxCount;
		log.info("Total Supplier Commission for Booking {} and Commission is {}", bookingId, agencyCommission);
		List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
		RadixxBookingManager bookingManager = RadixxBookingManager.builder().bindingService(bindingService)
				.configuration(supplierConf).airline(airline).bookingId(bookingId).deliveryInfo(deliveryInfo)
				.criticalMessageLogger(criticalMessageLogger)
				.binaryToken(supplierSession.getSupplierSessionInfo().getSessionToken()).listener(listener)
				.gstInfo(gstInfo).bookingUser(bookingUser).build();
		bookingManager.init();
		bookingManager.setGmsCommunicator(gmsCommunicator);
		double amountToBePaidtoAirline = bookingManager.summaryPNR(segmentInfos);
		double amountWithoutCommission = amountToBePaidtoAirline - agencyCommission;
		if (airline.isAssessFeeAllowed()) {
			amountToBePaidtoAirline = bookingManager.assessAgencyTransactionFees();
			amountWithoutCommission = amountToBePaidtoAirline - agencyCommission;
		}
		if (!isFareDiff(amountWithoutCommission)) {
			blockedPnr = bookingManager.createPNR(CreatePNRActionTypes.CommitSummary, isHoldBooking);
			if (StringUtils.isNotBlank(blockedPnr)) {
				if (!isHoldBooking) {
					bookingManager.setConfirmationNumber(blockedPnr);
					BigDecimal amountToPay =
							new BigDecimal(amountToBePaidtoAirline).setScale(2, BigDecimal.ROUND_HALF_UP);
					log.info("Processing PNR Payment {} for Booking ID {}", blockedPnr, bookingId);
					bookingManager.processPnrPayment(amountToPay);
					pnr = bookingManager.createPNR(CreatePNRActionTypes.SaveReservation, isHoldBooking);
					if (StringUtils.isBlank(pnr)) {
						log.info("Failed to Confirm PNR {} for Booking ID {}", blockedPnr, bookingId);
						isSuccess = false;
						pnr = blockedPnr;
					} else {
						log.info("Confirmed PNR {} for Booking ID {}", pnr, bookingId);
						isSuccess = true;
					}
				} else {
					log.info("Blocked PNR {} for Booking ID {}", blockedPnr, bookingId);
					updateTimeLimit(bookingManager.getHoldTimeLimit());
					pnr = blockedPnr;
					isSuccess = true;
				}
			}
		}
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		initialize();
		RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf)
				.bindingService(bindingService).airline(airline).listener(listener).bookingUser(bookingUser).build();
		loginManager.init();
		if (supplierSession == null) {
			String binaryToken = loginManager.generateToken();
			supplierSession = SupplierSession.builder().bookingId(order.getBookingId())
					.sourceId(supplierConf.getSourceId()).supplierId(supplierConf.getSupplierId()).build();
			if (supplierSession.getSupplierSessionInfo() == null) {
				supplierSession.setSupplierSessionInfo(SupplierSessionInfo.builder().build());
			}
			supplierSession.getSupplierSessionInfo().setSessionToken(binaryToken);
		}
		airline = RadixxAirline.getOTARadixxAirline(supplierConf.getBasicInfo().getSourceId());
		RadixxRetrieveBookingManager retrieveManager = RadixxRetrieveBookingManager.builder()
				.bindingService(bindingService).configuration(supplierConf).airline(airline).bookingId(bookingId)
				.binaryToken(supplierSession.getSupplierSessionInfo().getSessionToken()).listener(listener)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
		retrieveManager.init();
		if (StringUtils.isEmpty(blockedPnr)) {
			blockedPnr = pnr;
		}
		retrieveManager.pnr = blockedPnr;
		loginManager.setBinaryToken(supplierSession.getSupplierSessionInfo().getSessionToken());
		RetrievePNRResponse retrievePNRResponse = retrieveManager.retrieveBooking();
		double agencyCommission = loginManager.getAgencyCommission();
		int totalCommissionalablePaxCount = loginManager.getFromSegmentsPaxCount(bookingSegments.getSegmentInfos());
		BigDecimal reservationBalance = retrievePNRResponse.getRetrievePNRResult().getReservationBalance();
		agencyCommission = agencyCommission * totalCommissionalablePaxCount;
		log.info("Total Supplier Commission for Booking {} and Commission is {}", bookingId, agencyCommission);
		Double amountToDeductFromCustomer = reservationBalance.doubleValue() - agencyCommission;
		if (!isFareDiff(amountToDeductFromCustomer)) {
			RadixxBookingManager bookingManager = RadixxBookingManager.builder().bindingService(bindingService)
					.configuration(supplierConf).airline(airline)
					.binaryToken(supplierSession.getSupplierSessionInfo().getSessionToken()).bookingId(bookingId)
					.deliveryInfo(deliveryInfo).criticalMessageLogger(criticalMessageLogger).listener(listener)
					.bookingUser(bookingUser).build();
			bookingManager.init();
			bookingManager.setGmsCommunicator(gmsCommunicator);
			bookingManager.confirmationNumber = blockedPnr;
			bookingManager.processPnrPayment(reservationBalance);
			String confirmationNumber = bookingManager.createPNR(CreatePNRActionTypes.SaveReservation, false);
			if (StringUtils.isNotBlank(confirmationNumber)) {
				pnr = confirmationNumber;
				isSuccess = true;
				log.info("Ticket Confirmed for Supplier {} BookingID {} & PNR {} ", getSupplierConf().getSupplierId(),
						bookingId, confirmationNumber);
			} else {
				log.info("Failed to Confirm PNR {} for Booking ID {}", blockedPnr, bookingId);
				isSuccess = false;
			}
		}
		return isSuccess;
	}

}
