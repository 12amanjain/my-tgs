package com.tgs.services.fms.sources.otaradixx;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RadixxAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	private RadixxAirline airline;

	private RadixxBindingService bindingService;

	protected SoapRequestResponseListner listener = null;


	public RadixxAirBookingRetrieveFactory(SupplierConfiguration supplierConf, String pnr) {
		super(supplierConf, pnr);
	}

	public void initialize() {
		this.airline = RadixxAirline.getOTARadixxAirline(supplierConf.getBasicInfo().getSourceId());
		FlightAPIURLRuleCriteria apiUrlRule = RadixxUtils.getRadixxPointURL(sourceConfiguration);
		bindingService =
				RadixxBindingService.builder().cacheCommunicator(cachingCommunicator).apiUrlRule(apiUrlRule).build();
	}


	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		listener = new SoapRequestResponseListner(pnr, null, supplierConf.getBasicInfo().getSupplierName());
		RadixxTokenManager loginManager = RadixxTokenManager.builder().bindingService(bindingService).listener(listener)
				.configuration(supplierConf).airline(airline).bookingUser(bookingUser).build();
		loginManager.init();
		loginManager.generateToken();
		if (StringUtils.isNotEmpty(loginManager.getBinaryToken())) {
			log.debug("Inside Retreieve Manager to fetch Trips {}", pnr);
			RadixxRetrieveBookingManager retreievManager =
					RadixxRetrieveBookingManager.builder().bindingService(bindingService).configuration(supplierConf)
							.airline(airline).binaryToken(loginManager.getBinaryToken()).listener(listener)
							.bookingUser(bookingUser).bookingUser(bookingUser).build();
			retreievManager.init();
			double agencyCommission = loginManager.getAgencyCommission();
			retreievManager.setPnr(pnr);
			retreievManager.setAgencyCommission(agencyCommission);
			pnrBooking = retreievManager.retrievePNRBooking();
			updateTimeLimit(retreievManager.getHoldTimeLimit());
		}
		return pnrBooking;
	}
}
