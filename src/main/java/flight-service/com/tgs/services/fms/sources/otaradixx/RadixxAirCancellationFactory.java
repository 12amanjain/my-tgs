package com.tgs.services.fms.sources.otaradixx;

import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CreatePNRActionTypes;
import org.springframework.stereotype.Service;
import org.tempuri.RetrievePNRResponse;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.fms.utils.AirCancelUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RadixxAirCancellationFactory extends AbstractAirCancellationFactory {

	private RadixxAirline airline;

	private RadixxBindingService bindingService;

	protected SoapRequestResponseListner listener = null;

	public RadixxAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {

		super(bookingSegments, order, supplierConf);

	}

	public void initialize() {
		this.airline = RadixxAirline.getOTARadixxAirline(supplierConf.getSourceId());
		FlightAPIURLRuleCriteria apiUrlRule = RadixxUtils.getRadixxPointURL(sourceConfiguration);
		bindingService = RadixxBindingService.builder().cacheCommunicator(cachingComm).apiUrlRule(apiUrlRule).build();
		if (listener == null) {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		}
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isCancelSuccess = false;
		initialize();
		RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf).airline(airline)
				.listener(listener).criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
				.bookingUser(bookingUser).build();
		loginManager.init();
		loginManager.generateToken();
		String binaryToken = loginManager.getBinaryToken();
		RadixxRetrieveBookingManager retrieveManager =
				RadixxRetrieveBookingManager.builder().bindingService(bindingService).configuration(supplierConf)
						.airline(airline).criticalMessageLogger(criticalMessageLogger)
						.binaryToken(loginManager.getBinaryToken()).listener(listener).bookingUser(bookingUser).build();
		RadixxBookingManager bookingManager =
				RadixxBookingManager.builder().bindingService(bindingService).configuration(supplierConf)
						.airline(airline).bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
						.binaryToken(binaryToken).listener(listener).bookingUser(bookingUser).build();
		retrieveManager.init();
		retrieveManager.setPnr(pnr);
		RetrievePNRResponse retrieveResponse = retrieveManager.retrieveBooking();
		isCancelSuccess = retrieveManager.isCancelled(retrieveResponse);
		if (!isCancelSuccess) {
			RadixxAirCancellationManager cancellationManager =
					RadixxAirCancellationManager.builder().bindingService(bindingService).configuration(supplierConf)
							.airline(airline).criticalMessageLogger(criticalMessageLogger).binaryToken(binaryToken)
							.listener(listener).bookingUser(bookingUser).build();
			cancellationManager.init();
			bookingManager.init();
			cancellationManager.cancelPNR(pnr);
			bookingManager.setConfirmationNumber(pnr);
			bookingManager.createPNR(CreatePNRActionTypes.SaveReservation, isHoldBooking);
			retrieveResponse = retrieveManager.retrieveBooking();
			isCancelSuccess = retrieveManager.isCancelled(retrieveResponse);
		}
		return isCancelSuccess;
	}

	@Override
	public AirCancellationDetail reviewBookingForCancellation(AirCancellationDetail cancellationDetail) {
		boolean isCancelSuccess = false;
		initialize();
		RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf).airline(airline)
				.listener(listener).criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
				.bookingUser(bookingUser).build();
		loginManager.init();
		loginManager.generateToken();
		String binaryToken = loginManager.getBinaryToken();
		RadixxRetrieveBookingManager retrieveManager =
				RadixxRetrieveBookingManager.builder().bindingService(bindingService).configuration(supplierConf)
						.airline(airline).criticalMessageLogger(criticalMessageLogger)
						.binaryToken(loginManager.getBinaryToken()).listener(listener).bookingUser(bookingUser).build();
		RadixxBookingManager bookingManager =
				RadixxBookingManager.builder().bindingService(bindingService).configuration(supplierConf)
						.airline(airline).bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
						.binaryToken(binaryToken).listener(listener).bookingUser(bookingUser).build();
		retrieveManager.init();
		retrieveManager.setPnr(pnr);
		RetrievePNRResponse retrieveResponse = retrieveManager.retrieveBooking();
		isCancelSuccess = retrieveManager.isCancelled(retrieveResponse);
		if (!isCancelSuccess) {
			RadixxAirCancellationManager cancellationManager =
					RadixxAirCancellationManager.builder().bindingService(bindingService).configuration(supplierConf)
							.airline(airline).criticalMessageLogger(criticalMessageLogger).binaryToken(binaryToken)
							.listener(listener).bookingUser(bookingUser).build();
			cancellationManager.init();
			bookingManager.init();
			cancellationManager.cancelPNR(pnr);
			bookingManager.setConfirmationNumber(pnr);
			// retrieve booking after cancel pnr call for cancellation fees
			retrieveResponse = retrieveManager.retrieveBooking();
			cancellationManager.setAirlineCancellationFee(retrieveResponse, bookingSegments, false);
			cancellationDetail.setAutoCancellationAllowed(Boolean.TRUE);
		}
		return cancellationDetail;
	}

	@Override
	public boolean confirmBookingForCancellation() {
		boolean isCancelled = false;
		boolean isCancelSuccess = false;
		try {
			initialize();
			RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf).airline(airline)
					.listener(listener).criticalMessageLogger(criticalMessageLogger).bindingService(bindingService)
					.bookingUser(bookingUser).build();
			loginManager.init();
			loginManager.generateToken();
			String binaryToken = loginManager.getBinaryToken();
			RadixxRetrieveBookingManager retrieveManager = RadixxRetrieveBookingManager.builder()
					.bindingService(bindingService).configuration(supplierConf).airline(airline)
					.criticalMessageLogger(criticalMessageLogger).binaryToken(loginManager.getBinaryToken())
					.listener(listener).bookingUser(bookingUser).build();
			RadixxBookingManager bookingManager =
					RadixxBookingManager.builder().bindingService(bindingService).configuration(supplierConf)
							.airline(airline).bookingId(bookingId).criticalMessageLogger(criticalMessageLogger)
							.binaryToken(binaryToken).listener(listener).bookingUser(bookingUser).build();
			retrieveManager.init();
			retrieveManager.setPnr(pnr);
			RetrievePNRResponse retrieveResponse = retrieveManager.retrieveBooking();
			isCancelSuccess = retrieveManager.isCancelled(retrieveResponse);
			if (!isCancelSuccess) {
				RadixxAirCancellationManager cancellationManager = RadixxAirCancellationManager.builder()
						.bindingService(bindingService).configuration(supplierConf).airline(airline)
						.criticalMessageLogger(criticalMessageLogger).binaryToken(binaryToken).listener(listener)
						.bookingUser(bookingUser).build();
				cancellationManager.init();
				bookingManager.init();
				cancellationManager.cancelPNR(pnr);
				bookingManager.setConfirmationNumber(pnr);
				// retrieve booking after cancel pnr call for cancellation fees
				retrieveResponse = retrieveManager.retrieveBooking();
				cancellationManager.setAirlineCancellationFee(retrieveResponse, bookingSegments, true);
				double airlineCancellationFee = AirCancelUtils.getAirCancellationFees(bookingSegments);
				if (!isFareDiff(airlineCancellationFee)) {
					// creating pnr to confirm cancellation
					bookingManager.createPNR(CreatePNRActionTypes.SaveReservation, false);
					isCancelled = true;
				}
			}
		} finally {
			if (!isCancelled) {
				String message = StringUtils
						.join("Cancellation Failed for " + getSupplierConf().getBasicInfo().getSupplierName());
				criticalMessageLogger.add(message);
				log.error("{} for bookingid {}", message, bookingId);
			}
		}
		return isCancelled;
	}

	@Override
	protected boolean isPassengerWiseCancellationAllowed(Integer sourceId) {
		return RadixxAirline.getOTARadixxAirline(sourceId).isPassengerWiseCancellationAllowed();
	}
}
