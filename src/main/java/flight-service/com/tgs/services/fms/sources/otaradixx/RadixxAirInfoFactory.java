package com.tgs.services.fms.sources.otaradixx;

import java.util.List;
import java.util.Objects;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.manager.AirSearchResultProcessingManager;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierSessionException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.utils.AirUtils;
import lombok.extern.slf4j.Slf4j;
import org.tempuri.ConnectPoint_PricingStub;
import org.tempuri.ConnectPoint_SecurityStub;
import org.tempuri.ConnectPoint_TravelAgentsStub;

@Slf4j
@Service
public class RadixxAirInfoFactory extends AbstractAirInfoFactory {

	private RadixxAirline airline;

	private String binaryToken;

	protected SoapRequestResponseListner listener = null;

	private RadixxBindingService bindingService;

	public RadixxAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize() {
		this.airline = RadixxAirline.getOTARadixxAirline(supplierConf.getSourceId());
		FlightAPIURLRuleCriteria apiUrlRule = RadixxUtils.getRadixxPointURL(sourceConfiguration);
		bindingService = RadixxBindingService.builder().cacheCommunicator(cachingComm).apiUrlRule(apiUrlRule)
				.supplierConf(supplierConf).sourceConfiguration(sourceConfiguration).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
		if (contextData != null && StringUtils.isBlank(contextData.getBookingId())) {
			binaryToken = bindingService.fetchSessionToken(true);
		}
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		int numTry = 0;
		boolean doRetry = false;
		do {
			try {
				if (listener == null) {
					listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
							supplierConf.getBasicInfo().getSupplierName());
				}
				RadixxTokenManager loginManager = RadixxTokenManager.builder().configuration(supplierConf)
						.airline(airline).searchQuery(searchQuery).listener(listener)
						.criticalMessageLogger(criticalMessageLogger).bindingService(bindingService).bookingUser(user)
						.build();
				loginManager.init();
				RadixxSearchManager searchManager = RadixxSearchManager.builder().configuration(supplierConf)
						.airline(airline).searchQuery(searchQuery).bindingService(bindingService).listener(listener)
						.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
						.bookingUser(user).build();
				searchManager.init();
				if (StringUtils.isBlank(binaryToken)) {
					binaryToken = loginManager.generateToken();
					searchManager.setBinaryToken(binaryToken);
				}
				loginManager.binaryToken = binaryToken;
				searchManager.agencyCommission = loginManager.getAgencyCommission();
				if (StringUtils.isNotBlank(binaryToken)) {
					searchManager.setBinaryToken(binaryToken);
					searchResult = searchManager.doSearch();
				}
			} catch (SupplierSessionException e) {
				binaryToken = null;
				doRetry = true;
				numTry++;
			} finally {
				if (StringUtils.isNotBlank(binaryToken) && StringUtils.isEmpty(contextData.getBookingId())) {
					bindingService.storeSession(binaryToken, user);
				}
			}
		} while (numTry < 2 && doRetry);
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo newTripInfo = null;
		SupplierSession session = null;
		boolean isReviewSuccess = true;
		try {
			initialize();
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			if (Objects.nonNull(session)) {
				binaryToken = session.getSupplierSessionInfo().getSessionToken();
			}
			this.searchAvailableSchedules();
			boolean isDomReturn =
					searchQuery.isDomesticReturn() && AirUtils.splitTripInfo(selectedTrip, false).size() == 2;

			searchResult = combineResults(searchResult, isDomReturn);

			newTripInfo = RadixxUtils.filterResults(searchResult, selectedTrip);
			if (newTripInfo == null) {
				throw new NoSeatAvailableException();
			}
			RadixxSSRManager ssrManager =
					RadixxSSRManager.builder().bindingService(bindingService).configuration(supplierConf)
							.airline(airline).searchQuery(searchQuery).binaryToken(binaryToken).bookingId(bookingId)
							.listener(listener).criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			ssrManager.init();
			newTripInfo = ssrManager.getSpecialServices(newTripInfo, searchQuery);
		} catch (NoSeatAvailableException e) {
			isReviewSuccess = false;
			throw e;
		} catch (Exception e) {
			isReviewSuccess = false;
			log.error("Unable to perform review for booking  {} selected trip {}  ", bookingId, selectedTrip, e);
		} finally {
			if (Objects.isNull(session) && isReviewSuccess) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(binaryToken).build();
				storeBookingSession(bookingId, sessionInfo, session, newTripInfo);
			}
		}
		return newTripInfo;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		return null;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		initialize();
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		SupplierSessionInfo sessionInfo =
				SupplierSessionInfo.builder().tripKey(tripInfo.getSegmentsBookingKey()).build();
		SupplierSession session = getSessionIfExists(bookingId, sessionInfo);
		if (Objects.nonNull(session)) {
			binaryToken = session.getSupplierSessionInfo().getSessionToken();
			RadixxSSRManager ssrManager = RadixxSSRManager.builder().bindingService(bindingService)
					.configuration(supplierConf).airline(airline).binaryToken(binaryToken).bookingId(bookingId)
					.listener(listener).criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			ssrManager.init();
			ssrManager.getSeatMap(tripSeatMap, tripInfo);
		}
		return tripSeatMap;
	}

	@Override
	protected List<SourceRouteInfo> addRoutesToSystem(List<SourceRouteInfo> routes, String searchKey) {
		RadixxRouteManager routeManager = RadixxRouteManager.builder().configuration(supplierConf).airline(airline)
				.binaryToken(binaryToken).bindingService(bindingService).listener(listener)
				.sourceConfiguration(sourceConfiguration).bookingUser(user).build();
		routeManager.init();
		// routeManager.addRouteInfoToSystem();
		return routes;
	}

	@Override
	public AirSearchResult combineResults(AirSearchResult searchResult) {
		searchResult = combineResults(searchResult, false);
		return searchResult;
	}

	public AirSearchResult combineResults(AirSearchResult searchResult, boolean isDomesticSingleSell) {
		if (searchResult != null && supplierConf.getSourceId() == AirSourceType.AIRINDIAEXPRESS.getSourceId()) {
			/*
			 * International and domestic return ONWARD and RETURN trips comes separately. So at the time of review if
			 * it is a single sell then the search result needs to combined.
			 */
			if (searchQuery.isIntlReturn() || isDomesticSingleSell) {
				List<TripInfo> onwardTrips = searchResult.getTripInfos().get(TripInfoType.ONWARD.name());
				List<TripInfo> returnTrips = searchResult.getTripInfos().get(TripInfoType.RETURN.name());
				if (CollectionUtils.isNotEmpty(onwardTrips) && CollectionUtils.isNotEmpty(returnTrips)) {
					List<TripInfo> combinedTrips =
							RadixxUtils.buildCombination(onwardTrips, returnTrips, supplierConf.getSourceId(), user);
					searchResult = new AirSearchResult();
					if (CollectionUtils.isNotEmpty(combinedTrips)) {
						searchResult.getTripInfos().put(TripInfoType.COMBO.name(), combinedTrips);
					}
				}
			}
		} else {
			searchResult = AirSearchResultProcessingManager.processSearchTypeResult(searchResult,
					supplierConf.getSourceId(), searchQuery.isIntlReturn(), user);
		}
		return searchResult;
	}

	@Override
	public Boolean initializeStubs() {
		this.airline = RadixxAirline.getOTARadixxAirline(supplierConf.getSourceId());
		FlightAPIURLRuleCriteria apiUrlRule = RadixxUtils.getRadixxPointURL(sourceConfiguration);
		bindingService = RadixxBindingService.builder().cacheCommunicator(cachingComm).apiUrlRule(apiUrlRule).build();
		ConnectPoint_SecurityStub securityStub = bindingService.getSecurityStub(supplierConf);
		ConnectPoint_PricingStub pricingStub = bindingService.getPricingStub(supplierConf);
		ConnectPoint_TravelAgentsStub travelAgentStub = bindingService.getTravelAgentStub(supplierConf);
		bindingService.storeSessionMetaInfo(supplierConf, ConnectPoint_PricingStub.class, pricingStub);
		bindingService.storeSessionMetaInfo(supplierConf, ConnectPoint_SecurityStub.class, securityStub);
		bindingService.storeSessionMetaInfo(supplierConf, ConnectPoint_TravelAgentsStub.class, travelAgentStub);
		return true;
	}

}
