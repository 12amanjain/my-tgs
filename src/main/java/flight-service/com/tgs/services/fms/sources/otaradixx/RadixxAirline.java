package com.tgs.services.fms.sources.otaradixx;

import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsCurrencyCodeTypes;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.helper.AirSourceType;
import lombok.Getter;

@Getter
public enum RadixxAirline {

	FLYDUBAI(AirSourceType.FLYDUBAI.getSourceId()) {
		@Override
		public int getBaggageCategoryId() {
			return RadixxConstantsInfo.FZBAGGAGECATEGORYID;
		}

		@Override
		public boolean isAssessFeeAllowed() {
			return true;
		}

		@Override
		public boolean isBusinessSearchSupported(AirSearchQuery searchQuery) {
			if (searchQuery.getCabinClass().getName().equals(CabinClass.BUSINESS.getName())) {
				return true;
			}
			return false;
		}

		@Override
		public int getMealCategoryId() {
			return RadixxConstantsInfo.FZMEALCATEGORYID;
		}

		@Override
		public Map<String, String> getBaggageType() {
			Map<String, String> baggageMap = new HashMap<>();
			baggageMap.put("BAGB", "20");
			baggageMap.put("BAGL", "30");
			baggageMap.put("BAGX", "40");
			// checked in baggage
			baggageMap.put("BUPL", "");
			baggageMap.put("BUPX", "");
			baggageMap.put("BUPZ", "");
			return baggageMap;
		}

		@Override
		public Map<String, Map<String, List<String>>> allowedBaggage() {
			Map<String, Map<String, List<String>>> baggageMap = new HashMap<>();
			Map<String, List<String>> allowance = new HashMap<>();
			allowance.put("10", Arrays.asList("BAGB"));
			baggageMap.put("BUPL", allowance);
			allowance = new HashMap<>();
			allowance.put("20", Arrays.asList("BAGB"));
			baggageMap.put("BUPX", allowance);
			allowance = new HashMap<>();
			allowance.put("30", Arrays.asList("BAGL"));
			baggageMap.put("BUPZ", allowance);
			return baggageMap;
		}

		@Override
		public List<String> getMealType() {
			return Arrays.asList("MLIN");
		}


		@Override
		public boolean isZoneTimeRequired() {
			return true;
		}

		@Override
		public boolean isMealApplicableonAPI() {
			return false;
		}

		@Override
		public int getSeatCategoryId() {
			return 8;
		}
	},
	AIRINDIAEXPRESS(AirSourceType.AIRINDIAEXPRESS.getSourceId()) {
		@Override
		public int getBaggageCategoryId() {
			return RadixxConstantsInfo.IXBAGGAGECATEGORYID;
		}

		@Override
		public int getMealCategoryId() {
			return RadixxConstantsInfo.IXMEALCATEGORYID;
		}

		@Override
		public Map<String, String> getBaggageType() {
			Map<String, String> baggageMap = new HashMap<>();
			baggageMap.put("FB10", "10");
			baggageMap.put("FB20", "20");
			baggageMap.put("FB25", "25");
			baggageMap.put("FB30", "30");
			baggageMap.put("FB40", "40");

			// Into India to Gulf
			baggageMap.put("XB05", "5");
			baggageMap.put("XB10", "10");

			// Into Gulf to India
			baggageMap.put("BS05", "5");
			baggageMap.put("BS10", "10");
			baggageMap.put("BH05", "5");
			baggageMap.put("BH10", "10");

			// Into Singapore to India
			baggageMap.put("XBAG", "10");
			// checked in baggage
			baggageMap.put("FBA", "");
			return baggageMap;
		}

		@Override
		public Map<String, Map<String, List<String>>> allowedBaggage() {
			Map<String, Map<String, List<String>>> baggageMap = new HashMap<>();
			Map<String, List<String>> allowance = new HashMap<>();
			allowance.put("ALL", Arrays.asList("FB10", "FB20", "FB25", "FB30", "FB40"));
			baggageMap.put("FBA", allowance);
			return baggageMap;
		}

		@Override
		public boolean isGSTApplicable() {
			return true;
		}

		@Override
		public List<String> getMealType() {
			return Arrays.asList("ML01", "ML02", "ML03", "ML04", "ML05", "ML06", "ML07", "ML08", "ML09", "ML10", "ML11",
					"ML12", "ML13", "ML14", "ML15");
		}

		@Override
		public int getSeatCategoryId() {
			return 8;
		}
	},
	SALAMAIR(AirSourceType.SALAMAIR.getSourceId()) {

		@Override
		public Map<String, Map<String, List<String>>> allowedBaggage() {
			Map<String, Map<String, List<String>>> baggageMap = new HashMap<>();
			Map<String, List<String>> allowance = new HashMap<>();
			allowance.put("ALL", Arrays.asList("FB10", "FB20", "FB25", "FB30", "FB40"));
			baggageMap.put("FBA", allowance);
			return baggageMap;
		}

		@Override
		public boolean isGSTApplicable() {
			return false;
		}

		@Override
		public List<String> getMealType() {
			return Arrays.asList("ML01", "ML02", "ML03", "ML04", "ML05", "ML06", "ML07", "ML08", "ML09", "ML10", "ML11",
					"ML12", "ML13", "ML14", "ML15");
		}

		@Override
		public int getSeatCategoryId() {
			return 8;
		}

	};

	private int sourceId;

	private RadixxAirline(int sourceId) {
		this.sourceId = sourceId;
	}

	public int getMealCategoryId() {
		return 0;
	}

	public abstract int getSeatCategoryId();

	public static RadixxAirline getOTARadixxAirline(int sourceId) {
		for (RadixxAirline helper : RadixxAirline.values()) {
			if (helper.getSourceId() == sourceId) {
				return helper;
			}
		}
		return null;
	}

	/* scope for future */
	public EnumerationsCurrencyCodeTypes getCurrencyCode(SupplierCredential supplierCredential) {
		if (supplierCredential.getCurrencyCode() != null
				&& supplierCredential.getCurrencyCode().equalsIgnoreCase("INR")) {
			return EnumerationsCurrencyCodeTypes.INR;
		}
		return EnumerationsCurrencyCodeTypes.INR;
	}

	/* scope for future */
	public String getLocalCurrencyCode(SupplierCredential supplierCredential) {
		if (supplierCredential.getCurrencyCode() != null
				&& supplierCredential.getCurrencyCode().equalsIgnoreCase("INR")) {
			return "INR";
		}
		return "INR";
	}

	public int getBaggageCategoryId() {
		return 0;
	}

	public boolean isZoneTimeRequired() {
		return false;
	}

	public boolean isAssessFeeAllowed() {
		return false;
	}

	public boolean isGSTApplicable() {
		return false;
	}

	public Map<String, String> getBaggageType() {
		return new HashMap<>();
	}

	public Map<String, Map<String, List<String>>> allowedBaggage() {
		return new HashMap<>();
	}


	public List<String> getMealType() {
		return new ArrayList<>();
	}

	public boolean isBusinessSearchSupported(AirSearchQuery searchQuery) {
		return false;
	}

	public boolean isMealApplicableonAPI() {
		return true;
	}


	/**
	 * @implSpec : Purely for Retrieve booking
	 */
	public List<String> getBaseFareCode() {
		return Arrays.asList("AIR");
	}

	public List<String> getTaxCode() {
		return Arrays.asList("TAX", "TFEE");
	}

	public List<String> getFuelTaxCode() {
		return Arrays.asList("FUEL");
	}

	public boolean isPassengerWiseCancellationAllowed() {
		return true;
	}

}
