package com.tgs.services.fms.sources.otaradixx;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import org.apache.axis2.client.Stub;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.sources.AirSourceConstants;
import lombok.Setter;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.ConnectPoint_FeesStub;
import org.tempuri.ConnectPoint_FlightStub;
import org.tempuri.ConnectPoint_FulfillmentStub;
import org.tempuri.ConnectPoint_PricingStub;
import org.tempuri.ConnectPoint_ReservationStub;
import org.tempuri.ConnectPoint_SecurityStub;
import org.tempuri.ConnectPoint_TravelAgentsStub;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.CacheType;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Setter
@Getter
@Slf4j
@Builder
public class RadixxBindingService {

	private GeneralCachingCommunicator cacheCommunicator;

	private FlightAPIURLRuleCriteria apiUrlRule;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected SupplierConfiguration supplierConf;

	public ConnectPoint_SecurityStub getSecurityStub(SupplierConfiguration configuration) {
		ConnectPoint_SecurityStub securityStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_SecurityStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				securityStub = (ConnectPoint_SecurityStub) metaInfo.getValue();
			}

			if (securityStub == null) {
				log.debug("ConnectPoint_SecurityStub URL {} ", apiUrlRule.getSecurityURL());
				securityStub = new ConnectPoint_SecurityStub(apiUrlRule.getSecurityURL());
			}
		} catch (Exception e) {
			log.error("Security Stub Binding Initilaized failed ", e);
		}
		setOptions(securityStub);
		return securityStub;
	}

	public ConnectPoint_TravelAgentsStub getTravelAgentStub(SupplierConfiguration configuration) {
		ConnectPoint_TravelAgentsStub travelAgentsStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_TravelAgentsStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				travelAgentsStub = (ConnectPoint_TravelAgentsStub) metaInfo.getValue();
			}

			if (travelAgentsStub == null) {
				log.debug("ConnectPoint_TravelAgentsStub URL {} ", apiUrlRule.getTravelAgent());
				travelAgentsStub = new ConnectPoint_TravelAgentsStub(apiUrlRule.getTravelAgent());
			}
		} catch (Exception e) {
			log.error("TravelAgent Stub Binding Initilaized failed ", e);
		}
		setOptions(travelAgentsStub);
		return travelAgentsStub;
	}

	public ConnectPoint_PricingStub getPricingStub(SupplierConfiguration configuration) {
		ConnectPoint_PricingStub pricingStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_PricingStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				pricingStub = (ConnectPoint_PricingStub) metaInfo.getValue();
			}

			if (pricingStub == null) {
				log.debug("ConnectPoint_PricingStub URL {} ", apiUrlRule.getPricingURL());
				pricingStub = new ConnectPoint_PricingStub(apiUrlRule.getPricingURL());
			}
		} catch (Exception e) {
			log.error("Pricing Stub Binding Initilaized failed ", e);
		}
		setOptions(pricingStub);
		return pricingStub;
	}

	public ConnectPoint_PricingStub getSSRStub(SupplierConfiguration configuration) {
		ConnectPoint_PricingStub ssrStub = null;
		try {

			/*
			 * SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_PricingStub.class, null); //metaInfo
			 * = cacheCommunicator.fetchFromQueue(metaInfo); if (metaInfo != null) { ssrStub =
			 * (ConnectPoint_PricingStub) metaInfo.getValue(); }
			 */
			// Due to same Endpoint but Different URL Have to Disable caching for this
			if (ssrStub == null) {
				if (configuration.getBasicInfo().getSourceId() == AirSourceType.AIRINDIAEXPRESS.getSourceId()) {
					log.debug("ConnectPoint_PricingStub URL {} ", apiUrlRule.getPricingURL());
					ssrStub = new ConnectPoint_PricingStub(apiUrlRule.getPricingURL());
				} else {
					log.debug("ConnectPoint_PricingStub URL {} ", apiUrlRule.getSsrURL());
					ssrStub = new ConnectPoint_PricingStub(apiUrlRule.getSsrURL());
				}
			}
		} catch (Exception e) {
			log.error("SSR Stub Binding Initilaized failed ", e);
		}
		setOptions(ssrStub);
		return ssrStub;
	}

	public ConnectPoint_ReservationStub getSummaryReservationStub(SupplierConfiguration configuration) {
		ConnectPoint_ReservationStub reservationStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_ReservationStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				reservationStub = (ConnectPoint_ReservationStub) metaInfo.getValue();
			}

			if (reservationStub == null) {
				log.debug("ConnectPoint_ReservationStub URL {} ", apiUrlRule.getReservationURL());
				reservationStub = new ConnectPoint_ReservationStub(apiUrlRule.getReservationURL());
			}
		} catch (Exception e) {
			log.error("Reservation summary Stub Binding Initilaized failed ", e);
		}
		setOptions(reservationStub);
		return reservationStub;
	}

	public ConnectPoint_ReservationStub getBookReservationStub(SupplierConfiguration configuration) {
		ConnectPoint_ReservationStub reservationStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_ReservationStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				reservationStub = (ConnectPoint_ReservationStub) metaInfo.getValue();
			}

			if (reservationStub == null) {
				log.debug("ConnectPoint_ReservationStub URL {} ", apiUrlRule.getReservationURL());
				reservationStub = new ConnectPoint_ReservationStub(apiUrlRule.getReservationURL());
			}
		} catch (Exception e) {
			log.error("Reservation book Stub Binding Initilaized failed for ", e);
		}
		setOptions(reservationStub);
		return reservationStub;
	}

	public ConnectPoint_FeesStub getAssesFeeStub(SupplierConfiguration configuration) {
		ConnectPoint_FeesStub feesStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_FeesStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				feesStub = (ConnectPoint_FeesStub) metaInfo.getValue();
			}

			if (feesStub == null) {
				log.debug("ConnectPoint_FeesStub URL {} ", apiUrlRule.getAssesfeeURL());
				feesStub = new ConnectPoint_FeesStub(apiUrlRule.getAssesfeeURL());

			}
		} catch (Exception e) {
			log.error("AssesFee Stub Binding Initilaized failed ", e);
		}
		setOptions(feesStub);
		return feesStub;
	}

	public ConnectPoint_FulfillmentStub getFullFillMentStub(SupplierConfiguration configuration) {
		ConnectPoint_FulfillmentStub fulfillmentStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_FulfillmentStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				fulfillmentStub = (ConnectPoint_FulfillmentStub) metaInfo.getValue();
			}
			if (fulfillmentStub == null) {
				log.debug("ConnectPoint_FulfillmentStub URL {} ", apiUrlRule.getPnrPaymentURL());
				fulfillmentStub = new ConnectPoint_FulfillmentStub(apiUrlRule.getPnrPaymentURL());
			}
		} catch (Exception e) {
			log.error("FullFillment Stub Binding Initilaized failed", e);
		}
		setOptions(fulfillmentStub);
		return fulfillmentStub;
	}

	public ConnectPoint_FlightStub getFlightStub(SupplierConfiguration configuration) {
		ConnectPoint_FlightStub flightStub = null;
		try {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, ConnectPoint_FlightStub.class, null);
			metaInfo = fetchFromQueue(metaInfo);
			if (metaInfo != null) {
				flightStub = (ConnectPoint_FlightStub) metaInfo.getValue();
			}
			if (flightStub == null) {
				log.debug("ConnectPoint_FlightStub URL {} ", apiUrlRule.getFlightURL());
				flightStub = new ConnectPoint_FlightStub(apiUrlRule.getFlightURL());
			}
		} catch (Exception e) {
			log.error("flight Stub Binding Initilaized failed", e);
		}
		setOptions(flightStub);
		return flightStub;
	}

	private static String getKey(SupplierConfiguration configuration, String type) {
		SupplierBasicInfo basicInfo = configuration.getBasicInfo();
		return String.join("-", basicInfo.getRuleId().toString(), type);
	}

	private static SessionMetaInfo buildMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(getKey(configuration, classx.toString())).value(value)
				.expiryTime(LocalDateTime.now().plusMinutes(600)).cacheType(CacheType.INMEMORY).build();
		metaInfo.setIndex(0);
		return metaInfo;
	}

	public SessionMetaInfo fetchFromQueue(SessionMetaInfo metaInfo) {
		metaInfo.setIndex(0);
		try {
			SessionMetaInfo cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			if (cacheInfo == null) {
				metaInfo.setIndex(-1);
				cacheInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			}
			return cacheInfo;
		} catch (Exception e) {
			log.error("Unable to Pop Session from inmemopry queue {} ", metaInfo, e);
		}
		return null;
	}

	@SuppressWarnings("rawtypes")
	public void storeSessionMetaInfo(SupplierConfiguration configuration, Class classx, Object value) {
		if (value != null) {
			SessionMetaInfo metaInfo = buildMetaInfo(configuration, classx, value);
			cacheCommunicator.storeInQueue(metaInfo);
		}
	}

	private void setOptions(Stub stub) {
		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions()
					.setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			setConnectionManager(stub);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(20);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}

	public String fetchSessionToken(boolean isSearch) {
		if (isSearch) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
					.index(0).compress(false).build();
			metaInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
				metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString()).index(-1)
						.compress(false).build();
				metaInfo = cacheCommunicator.fetchFromQueue(metaInfo);
			}
			if (metaInfo != null && metaInfo.getValue() != null) {
				return (String) metaInfo.getValue();
			}
		}
		return null;
	}

	public void storeSession(String sessionToken, User bookingUser) {
		if (StringUtils.isNotBlank(sessionToken)) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
					.compress(false).build();
			metaInfo.setValue(sessionToken);
			metaInfo.setExpiryTime(LocalDateTime.now().plusMinutes(
					AirUtils.getSessionTime(supplierConf.getBasicInfo(), sourceConfiguration, bookingUser)));
			cacheCommunicator.storeInQueue(metaInfo);
		}
	}

}
