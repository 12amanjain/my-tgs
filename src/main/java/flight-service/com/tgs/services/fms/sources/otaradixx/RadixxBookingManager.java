package com.tgs.services.fms.sources.otaradixx;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.utils.exception.air.SupplierPaymentException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_fees_request.AssessAgencyTransactionFees;
import org.datacontract.schemas._2004._07.radixx_connectpoint_fulfillment_request.ArrayOfProcessPNRPayment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_fulfillment_request.PNRPayments;
import org.datacontract.schemas._2004._07.radixx_connectpoint_fulfillment_request.PNRPaymentsActionTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_fulfillment_request.ProcessPNRPayment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsContactTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsGenderTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsPaymentMethodTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsPaymentTransactionStatusTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsRelationshipTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.Address;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ArrayOfContactInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ArrayOfPayment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ArrayOfPerson;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ArrayOfSegment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ArrayOfSpecialService;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ContactInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CreatePNR;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.CreatePNRActionTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.Payment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.Person;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.Segment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.SpecialService;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.SummaryPNR;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.SummaryPNRActionTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.ArrayOfSeat;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.Seat;
import org.tempuri.AssessAgencyTransactionFeesResponse;
import org.tempuri.ConnectPoint_FeesStub;
import org.tempuri.ConnectPoint_FulfillmentStub;
import org.tempuri.ConnectPoint_ReservationStub;
import org.tempuri.CreatePNRResponse;
import org.tempuri.ProcessPNRPaymentResponse;
import org.tempuri.SummaryPNRResponse;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.XMLUtils;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@SuperBuilder
final class RadixxBookingManager extends RadixxServiceManager {

	private BigDecimal reservationBalance;

	protected String confirmationNumber;

	protected GeneralServiceCommunicator gmsCommunicator;

	private static int PERSONORGID = -214;
	private GstInfo gstInfo;

	public static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public static final String CREATE_PNR_ACTION = "http://tempuri.org/IConnectPoint_Reservation/CreatePNR";

	public double summaryPNR(List<SegmentInfo> segmentInfos) {
		ConnectPoint_ReservationStub summaryPnrStub = null;
		try {
			reservationBalance = new BigDecimal(0);
			companyInfo = ServiceCommunicatorHelper.getClientInfo();
			org.tempuri.SummaryPNR summaryPnr = new org.tempuri.SummaryPNR();
			summaryPnrStub = bindingService.getSummaryReservationStub(configuration);
			SummaryPNR summaryPnrRequest = new SummaryPNR();
			summaryPnrRequest.setSecurityGUID(binaryToken);
			summaryPnr.setSummaryPnrRequest(getSummaryPNRRequest(summaryPnrRequest, segmentInfos));
			listener.setType("6-B-Summary");
			summaryPnrStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			SummaryPNRResponse summaryPnrResponse = summaryPnrStub.summaryPNR(summaryPnr);
			if (!isCriticalException(summaryPnrResponse.getSummaryPNRResult().getExceptions())) {
				reservationBalance = summaryPnrResponse.getSummaryPNRResult().getReservationBalance();
			}
			return reservationBalance.doubleValue();
		} catch (java.rmi.RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			summaryPnrStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_ReservationStub.class, summaryPnrStub);
		}
	}

	public SummaryPNR getSummaryPNRRequest(SummaryPNR summaryPnrRequest, List<SegmentInfo> segmentInfos) {
		int contactID = RadixxConstantsInfo.PERSON_CONTACT_ID;
		long profileId = RadixxConstantsInfo.PERSON_PROFILE_ID;
		int infantTrvlWith = RadixxConstantsInfo.PERSON_ORG_ID;
		int personOrgId = RadixxConstantsInfo.PERSONAORGID;
		List<FlightTravellerInfo> travellerInfos = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		summaryPnrRequest.setCarrierCodes(getCarrierCode());
		summaryPnrRequest.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
		summaryPnrRequest.setActionType(SummaryPNRActionTypes.GetSummary);
		summaryPnrRequest.setReservationInfo(RadixxUtils.getReservationInfo(confirmationNumber));
		summaryPnrRequest.setSecurityToken(binaryToken);
		summaryPnrRequest.setCarrierCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
		summaryPnrRequest.setDisplayCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
		summaryPnrRequest.setIATANum(configuration.getSupplierCredential().getIataNumber());
		summaryPnrRequest.setUser(configuration.getSupplierCredential().getAgentUserName());
		summaryPnrRequest.setReceiptLanguageID(RadixxConstantsInfo.RECEIPTLANGUAGEID);
		summaryPnrRequest.setPromoCode(RadixxConstantsInfo.NOTAPPLICABLE);
		summaryPnrRequest.setExternalBookingID(RadixxConstantsInfo.EXTERNALBOOKINGID);
		summaryPnrRequest.setAddress(RadixxUtils.getAddress(companyInfo));
		summaryPnrRequest
				.setContactInfos(getPassengerContactInfo(RadixxConstantsInfo.PERSONAORGID, getContactID(personOrgId)));
		summaryPnrRequest
				.setPassengers(getPassengers(travellerInfos, contactID, profileId, infantTrvlWith, personOrgId));
		summaryPnrRequest.setSegments(getArrayOfSegment(segmentInfos));
		summaryPnrRequest.setPayments(getPaymentDetailsForPnr());
		return summaryPnrRequest;
	}

	private ArrayOfSegment getArrayOfSegment(List<SegmentInfo> segmentInfos) {
		ArrayOfSegment arrayOfSegment = new ArrayOfSegment();
		AtomicInteger size = new AtomicInteger(1);
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfos.size() > 1 && segmentInfo.getSegmentNum() == 0
					&& BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
				size.set(2);
			}
		});
		Segment[] segment = new Segment[size.get()];
		TripInfo onwardTripInfo = getRequestedTripInfo(segmentInfos, false);
		segment[0] = getSummarySegmentInfo(onwardTripInfo);
		if (size.get() == 2) {
			TripInfo returnTripInfo = getRequestedTripInfo(segmentInfos, true);
			if (returnTripInfo != null) {
				segment[1] = getSummarySegmentInfo(returnTripInfo);
			}
		}
		arrayOfSegment.setSegment(segment);
		return arrayOfSegment;
	}

	private Segment getSummarySegmentInfo(TripInfo tripInfo) {
		Calendar departureDate =
				TgsDateUtils.getCalendar(tripInfo.getSegmentInfos().get(0).getDepartTime().toLocalDate());
		Integer lfId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getLogicalFlightId().intValue();
		Integer fareTypeId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareTypeId();
		int specialServicesBagCount = 0;
		int serviceIndex = 0;
		int personOrgId = -214;
		Segment segment = new Segment();
		segment.setPersonOrgID(PERSONORGID);
		segment.setFareInformationID(fareTypeId);
		segment.setMarketingCode(RadixxConstantsInfo.NOTAPPLICABLE);
		segment.setStoreFrontID(RadixxConstantsInfo.NOTAPPLICABLE);
		SegmentBookingRelatedInfo bookingRelatedInfo = tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo();

		Integer physicalFlightId =
				Integer.valueOf(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getJourneyKey());

		// Baggage On Trip wise
		for (FlightTravellerInfo travellerInfo : bookingRelatedInfo.getTravellerInfo()) {
			if (Objects.nonNull(travellerInfo.getSsrBaggageInfo())) {
				specialServicesBagCount++;
			}
		}
		// Additional Baggage
		ArrayOfSpecialService arrayOfSpecialService = new ArrayOfSpecialService();

		if (specialServicesBagCount != 0) {
			SpecialService[] service = new SpecialService[specialServicesBagCount];
			for (PaxType paxType : PaxType.values()) {
				List<FlightTravellerInfo> paxwiseTravellerInfoList = AirUtils.getParticularPaxTravellerInfo(
						tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), paxType);
				if (paxwiseTravellerInfoList.size() > 0) {
					for (int i = 0; i < paxwiseTravellerInfoList.size(); i++) {
						if (Objects.nonNull(paxwiseTravellerInfoList.get(i).getSsrBaggageInfo())
								&& Objects.nonNull(paxwiseTravellerInfoList.get(i).getSsrBaggageInfo().getCode())) {
							service[serviceIndex] =
									getBaggageSpecialService(paxwiseTravellerInfoList.get(i), physicalFlightId);
							service[serviceIndex].setSourceId(configuration.getSourceId());
							service[serviceIndex].setLogicalFlightID(lfId);
							service[serviceIndex].setDepartureDate(departureDate);
							service[serviceIndex].setPersonOrgID(personOrgId);
							serviceIndex++;
						}
						personOrgId--;
					}
				}
			}
			arrayOfSpecialService.setSpecialService(service);
		}

		// Meal on SegmentWise
		AtomicInteger mealpersonOrgId = new AtomicInteger(RadixxConstantsInfo.PERSONAORGID);

		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			for (PaxType paxType : PaxType.values()) {
				List<FlightTravellerInfo> paxwiseTravellerInfoList = AirUtils
						.getParticularPaxTravellerInfo(segmentInfo.getBookingRelatedInfo().getTravellerInfo(), paxType);
				if (paxwiseTravellerInfoList.size() > 0) {
					for (int i = 0; i < paxwiseTravellerInfoList.size(); i++) {
						if (Objects.nonNull(paxwiseTravellerInfoList.get(i).getSsrMealInfo())
								&& StringUtils.isNotEmpty(paxwiseTravellerInfoList.get(i).getSsrMealInfo().getCode())) {
							Integer phsId = Integer.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
							SpecialService specialService =
									getMealSpecialService(paxwiseTravellerInfoList.get(i), phsId);
							specialService.setLogicalFlightID(
									segmentInfo.getPriceInfo(0).getMiscInfo().getLogicalFlightId().intValue());
							specialService.setDepartureDate(
									TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
							specialService.setSourceId(configuration.getSourceId());
							specialService.setPersonOrgID(mealpersonOrgId.get());
							arrayOfSpecialService.addSpecialService(specialService);
							mealpersonOrgId.getAndDecrement();
						}
					}
				}
			}
		});


		// seat on SegmentWise
		AtomicInteger seatpersonOrgId = new AtomicInteger(RadixxConstantsInfo.PERSONAORGID);

		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			for (PaxType paxType : PaxType.values()) {
				List<FlightTravellerInfo> paxwiseTravellerInfoList = AirUtils
						.getParticularPaxTravellerInfo(segmentInfo.getBookingRelatedInfo().getTravellerInfo(), paxType);
				if (paxwiseTravellerInfoList.size() > 0) {
					for (int i = 0; i < paxwiseTravellerInfoList.size(); i++) {
						if (paxwiseTravellerInfoList.get(i).getPaxType() != PaxType.INFANT
								&& Objects.nonNull(paxwiseTravellerInfoList.get(i).getSsrSeatInfo())
								&& StringUtils.isNotEmpty(paxwiseTravellerInfoList.get(i).getSsrSeatInfo().getCode())) {
							Integer phsId = Integer.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
							SeatSSRInformation seatSSRInformation =
									(SeatSSRInformation) paxwiseTravellerInfoList.get(i).getSsrSeatInfo();
							SpecialService specialService =
									getSeatSpecialService(paxwiseTravellerInfoList.get(i), phsId);
							specialService.setCodeType(seatSSRInformation.getMiscInfo().getSeatCodeType());
							specialService.setChargeComment(seatSSRInformation.getMiscInfo().getSeatCodeType());
							specialService.setSourceId(configuration.getSourceId());
							specialService.setLogicalFlightID(
									segmentInfo.getPriceInfo(0).getMiscInfo().getLogicalFlightId().intValue());
							specialService.setDepartureDate(
									TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
							specialService.setPersonOrgID(seatpersonOrgId.get());
							arrayOfSpecialService.addSpecialService(specialService);
							seatpersonOrgId.getAndDecrement();
						}
					}
				}
			}
		});

		segment.setSeats(getSeatInfo(tripInfo));
		segment.setSpecialServices(arrayOfSpecialService);

		return segment;
	}

	private ArrayOfSeat getSeatInfo(TripInfo tripInfo) {
		ArrayOfSeat arrayOfSeat = new ArrayOfSeat();
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			// seat on SegmentWise
			AtomicInteger seatpersonOrgId = new AtomicInteger(RadixxConstantsInfo.PERSONAORGID);
			for (PaxType paxType : PaxType.values()) {
				List<FlightTravellerInfo> paxwiseTravellerInfoList = AirUtils
						.getParticularPaxTravellerInfo(segmentInfo.getBookingRelatedInfo().getTravellerInfo(), paxType);
				if (paxwiseTravellerInfoList.size() > 0) {
					for (int i = 0; i < paxwiseTravellerInfoList.size(); i++) {
						if (paxwiseTravellerInfoList.get(i).getPaxType() != PaxType.INFANT
								&& Objects.nonNull(paxwiseTravellerInfoList.get(i).getSsrSeatInfo())
								&& StringUtils.isNotEmpty(paxwiseTravellerInfoList.get(i).getSsrSeatInfo().getCode())) {
							FlightTravellerInfo travellerInfo = paxwiseTravellerInfoList.get(i);
							Integer phsId = Integer.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey());
							SSRInformation ssrInformation = travellerInfo.getSsrSeatInfo();
							Seat seat = new Seat();
							seat.setDepartureDate(TgsDateUtils.getCalendar(segmentInfo.getDepartTime().toLocalDate()));
							seat.setLogicalFlightID(
									segmentInfo.getPriceInfo(0).getMiscInfo().getLogicalFlightId().intValue());
							seat.setPersonOrgID(seatpersonOrgId.get());
							seat.setPhysicalFlightID(phsId);
							String seatCode = ssrInformation.getCode();
							String rowNumber = seatCode.replaceAll("[^0-9]", "");
							String code = seatCode.replaceAll("[^A-Z]", "");
							seat.setRowNumber(rowNumber);
							seat.setSeatSelected(code);
							arrayOfSeat.addSeat(seat);
							seatpersonOrgId.getAndDecrement();
						}
					}
				}
			}
		});

		return arrayOfSeat;
	}

	private SpecialService getBaggageSpecialService(FlightTravellerInfo travellerInfo, Integer physicalFlightId) {
		SpecialService specialService = new SpecialService();
		specialService.setServiceID(RadixxConstantsInfo.SERVICEID);
		specialService.setCodeType(travellerInfo.getSsrBaggageInfo().getCode());
		specialService.setAmount(BigDecimal.valueOf(travellerInfo.getSsrBaggageInfo().getAmount()));
		specialService.setSSRCategory(airline.getBaggageCategoryId());
		specialService.setPhysicalFlightID(physicalFlightId);
		specialService.setOverrideAmount(true);
		specialService.setCurrencyCode(airline.getCurrencyCode(configuration.getSupplierCredential()));
		specialService.setChargeComment(travellerInfo.getSsrBaggageInfo().getCode());
		return specialService;
	}

	private SpecialService getMealSpecialService(FlightTravellerInfo travellerInfo, Integer physicalFlightId) {
		SpecialService specialService = new SpecialService();
		specialService.setServiceID(RadixxConstantsInfo.SERVICEID);
		specialService.setCodeType(travellerInfo.getSsrMealInfo().getCode());
		specialService.setAmount(BigDecimal.valueOf(travellerInfo.getSsrMealInfo().getAmount()));
		specialService.setSSRCategory(airline.getMealCategoryId());
		specialService.setPhysicalFlightID(physicalFlightId);
		specialService.setOverrideAmount(true);
		specialService.setCurrencyCode(airline.getCurrencyCode(configuration.getSupplierCredential()));
		specialService.setChargeComment(travellerInfo.getSsrMealInfo().getCode());
		return specialService;
	}

	private SpecialService getSeatSpecialService(FlightTravellerInfo travellerInfo, Integer physicalFlightId) {
		SpecialService specialService = new SpecialService();
		specialService.setServiceID(RadixxConstantsInfo.SERVICEID);
		specialService.setCodeType(travellerInfo.getSsrSeatInfo().getCode());
		specialService.setAmount(BigDecimal.valueOf(travellerInfo.getSsrSeatInfo().getAmount()));
		specialService.setSSRCategory(airline.getSeatCategoryId());
		specialService.setPhysicalFlightID(physicalFlightId);
		specialService.setOverrideAmount(true);
		specialService.setCurrencyCode(airline.getCurrencyCode(configuration.getSupplierCredential()));
		specialService.setChargeComment(travellerInfo.getSsrSeatInfo().getCode());
		return specialService;
	}

	private TripInfo getRequestedTripInfo(List<SegmentInfo> segmentInfos, boolean isReturn) {
		TripInfo newTripInfo = new TripInfo();
		List<SegmentInfo> newSegmentList = new ArrayList<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getIsReturnSegment().booleanValue() == isReturn) {
				newSegmentList.add(segmentInfo);
			}
		});
		if (CollectionUtils.isNotEmpty(newSegmentList)) {
			newTripInfo.setSegmentInfos(newSegmentList);
			return newTripInfo;
		}
		return null;
	}

	public double assessAgencyTransactionFees() {
		ConnectPoint_FeesStub stub = null;
		try {
			double totalFare = 0;
			stub = bindingService.getAssesFeeStub(configuration);
			org.tempuri.AssessAgencyTransactionFees assessAgencyTransactionFees =
					new org.tempuri.AssessAgencyTransactionFees();
			AssessAgencyTransactionFees agencyTransactionFeesRequest = new AssessAgencyTransactionFees();
			agencyTransactionFeesRequest.setSecurityGUID(binaryToken);
			agencyTransactionFeesRequest.setCarrierCodes(getCarrierCode());
			agencyTransactionFeesRequest.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
			agencyTransactionFeesRequest.setPaymentMethod(EnumerationsPaymentMethodTypes.INVC);
			agencyTransactionFeesRequest
					.setAgencyCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
			agencyTransactionFeesRequest.setClientIPAddress(StringUtils.EMPTY);
			assessAgencyTransactionFees.setAssessAgencyTransactionFeesRequest(agencyTransactionFeesRequest);
			listener.setType("7-B-AgencyFee");
			stub._getServiceClient().getAxisService().addMessageContextListener(listener);
			AssessAgencyTransactionFeesResponse agencyTransactionResponse =
					stub.assessAgencyTransactionFees(assessAgencyTransactionFees);
			if (!isCriticalException(
					agencyTransactionResponse.getAssessAgencyTransactionFeesResult().getExceptions())) {
				totalFare = agencyTransactionResponse.getAssessAgencyTransactionFeesResult().getReservationBalance()
						.doubleValue();
			}
			return totalFare;
		} catch (java.rmi.RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_FeesStub.class, stub);
		}
	}

	public void processPnrPayment(BigDecimal reservationBalance) {
		ConnectPoint_FulfillmentStub stub = null;
		try {
			this.reservationBalance = reservationBalance;
			org.tempuri.ProcessPNRPayment processPnrPayment = new org.tempuri.ProcessPNRPayment();
			stub = bindingService.getFullFillMentStub(configuration);
			PNRPayments pnrPaymentRequest = new PNRPayments();
			pnrPaymentRequest.setPNRPayments(getPaymentRequest(bookingId));
			pnrPaymentRequest.setActionType(PNRPaymentsActionTypes.ProcessPNRPayment);
			pnrPaymentRequest.setReservationInfo(RadixxUtils.getReservationInfo(confirmationNumber));
			pnrPaymentRequest
					.setTransactionInfo(RadixxUtils.getTransactionInfo(binaryToken, getCarrierCode(), configuration));
			processPnrPayment.setPNRPaymentRequest(pnrPaymentRequest);
			listener.setType("B-PAYMENT");
			stub._getServiceClient().getAxisService().addMessageContextListener(listener);
			ProcessPNRPaymentResponse pnrPaymentResponse = stub.processPNRPayment(processPnrPayment);
			if (isCriticalException(pnrPaymentResponse.getProcessPNRPaymentResult().getExceptions())) {
				throw new SupplierPaymentException("Available Credit Error Payment Failed " + bookingId);
			}
		} catch (java.rmi.RemoteException rE) {
			throw new SupplierRemoteException(rE);
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_FulfillmentStub.class, stub);
		}
	}

	private ArrayOfProcessPNRPayment getPaymentRequest(String bookingId) {
		ArrayOfProcessPNRPayment processPNRPayment = new ArrayOfProcessPNRPayment();
		ProcessPNRPayment[] processPNRPayments = new ProcessPNRPayment[1];
		org.datacontract.schemas._2004._07.radixx_connectpoint_fulfillment_request.ProcessPNRPayment pnrPayment =
				new org.datacontract.schemas._2004._07.radixx_connectpoint_fulfillment_request.ProcessPNRPayment();
		pnrPayment.setBaseAmount(reservationBalance);
		pnrPayment.setCardHolder(StringUtils.EMPTY);
		pnrPayment.setCardNumber(StringUtils.EMPTY);
		pnrPayment.setCheckNumber(RadixxConstantsInfo.CHECKNUMBER);
		pnrPayment.setCurrencyPaid(airline.getCurrencyCode(configuration.getSupplierCredential()));
		pnrPayment.setCVCode(StringUtils.EMPTY);
		pnrPayment.setDatePaid(Calendar.getInstance());
		pnrPayment.setDocumentReceivedBy(configuration.getSupplierCredential().getAgentUserName());
		pnrPayment.setExpirationDate(Calendar.getInstance());
		pnrPayment.setExchangeRate(BigDecimal.ONE);
		pnrPayment.setExchangeRateDate(Calendar.getInstance());
		pnrPayment.setFFNumber(RadixxConstantsInfo.FFNUMBER);
		pnrPayment.setPaymentComment(bookingId);
		pnrPayment.setPaymentAmount(reservationBalance);
		pnrPayment.setPaymentMethod(EnumerationsPaymentMethodTypes.INVC);
		pnrPayment.setReference(bookingId);
		pnrPayment.setTerminalID(1);
		pnrPayment.setUserData(StringUtils.EMPTY);
		pnrPayment.setUserID(configuration.getSupplierCredential().getAgentUserName());
		pnrPayment.setIataNumber(configuration.getSupplierCredential().getIataNumber());
		pnrPayment.setValueCode(StringUtils.EMPTY);
		pnrPayment.setVoucherNumber(RadixxConstantsInfo.VOUCHERNUMBER);
		pnrPayment.setIsTACreditCard(false);
		pnrPayment.setGcxID(RadixxConstantsInfo.TICKETPACKAGEID);
		pnrPayment.setGcxOptOption(RadixxConstantsInfo.TICKETPACKAGEID);
		pnrPayment.setOriginalCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
		pnrPayment.setOriginalAmount(reservationBalance);
		pnrPayment.setTransactionStatus(EnumerationsPaymentTransactionStatusTypes.APPROVED);
		pnrPayment.setAuthorizationCode(StringUtils.EMPTY);
		pnrPayment.setPaymentReference(RadixxConstantsInfo.NOTAPPLICABLE);
		pnrPayment.setResponseMessage(RadixxConstantsInfo.NOTAPPLICABLE);
		pnrPayment.setCardCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
		pnrPayment.setBaseCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
		pnrPayment.setBillingCountry(StringUtils.EMPTY);
		pnrPayment.setFingerPrintingSessionID(StringUtils.EMPTY);
		pnrPayment.setPayor(getPayor(bookingId));
		processPNRPayments[0] = pnrPayment;
		processPNRPayment.setProcessPNRPayment(processPNRPayments);
		return processPNRPayment;
	}

	private Person getPayor(String bookingId) {
		Person person = new Person();
		person.setPersonOrgID(PERSONORGID);
		person.setFirstName(configuration.getSupplierCredential().getAgentUserName());
		person.setLastName(configuration.getSupplierCredential().getAgentUserName());
		person.setMiddleName(StringUtils.EMPTY);
		person.setDOB(TgsDateUtils.getCalendar(companyInfo.getDob()));
		person.setAge(TgsDateUtils.getAgeFromBirthDate(companyInfo.getDob()));
		person.setGender(EnumerationsGenderTypes.Male);
		person.setTitle(companyInfo.getTitle());
		person.setNationalityLaguageID(RadixxConstantsInfo.NATIONALITYLAGUAGEID);
		person.setRelationType(EnumerationsRelationshipTypes.Self);
		person.setWBCID(RadixxConstantsInfo.WBCID);
		person.setPTCID(RadixxConstantsInfo.PTC);
		person.setPTC(RadixxConstantsInfo.TICKETPACKAGEID);
		person.setTravelsWithPersonOrgID(PERSONORGID);
		person.setRedressNumber(RadixxConstantsInfo.NOTAPPLICABLE);
		person.setKnownTravelerNumber(RadixxConstantsInfo.NOTAPPLICABLE);
		person.setMarketingOptIn(false);
		person.setUseInventory(false);
		person.setAddress(getPayorAddress());
		person.setCompany(companyInfo.getCompanyName());
		person.setComments(bookingId);
		person.setPassport(StringUtils.EMPTY);
		person.setNationality(StringUtils.EMPTY);
		person.setProfileId(RadixxConstantsInfo.PROFILEID);
		person.setIsPrimaryPassenger(true);
		ArrayOfContactInfo contactInfo = getPassengerContactInfo(RadixxConstantsInfo.PERSONAORGID,
				getContactID(RadixxConstantsInfo.PERSONAORGID));
		person.setContactInfos(contactInfo);
		return person;
	}

	private ArrayOfContactInfo getPassengerContactInfo(int personOrgId, int contactID) {
		ArrayOfContactInfo arrayOfContactInfo = new ArrayOfContactInfo();
		ContactInfo[] contactInfos = new ContactInfo[3];
		int count = 0;
		String bookingEmailId = getBookingEmail(deliveryInfo);
		for (int paxContactInfoIndex = 0; paxContactInfoIndex < 3; paxContactInfoIndex++) {
			contactInfos[paxContactInfoIndex] = new ContactInfo();
			if (count == 0) {
				// adult
				contactInfos[paxContactInfoIndex].setContactField(bookingEmailId);
				contactInfos[paxContactInfoIndex].setContactType(EnumerationsContactTypes.Email);
				contactInfos[paxContactInfoIndex].setPreferredContactMethod(true);
			} else if (count == 1) {
				// child
				contactInfos[paxContactInfoIndex].setContactField(getMobileNumber(deliveryInfo));
				contactInfos[paxContactInfoIndex].setContactType(EnumerationsContactTypes.MobilePhone);
				contactInfos[paxContactInfoIndex].setPreferredContactMethod(false);
			} else if (count == 2) {
				// infant
				contactInfos[paxContactInfoIndex].setContactField(companyInfo.getHomePhone());
				contactInfos[paxContactInfoIndex].setContactType(EnumerationsContactTypes.HomePhone);
				contactInfos[paxContactInfoIndex].setPreferredContactMethod(false);
			}
			contactInfos[paxContactInfoIndex].setAreaCode(companyInfo.getPostalCode());
			contactInfos[paxContactInfoIndex].setContactID(contactID--);
			contactInfos[paxContactInfoIndex].setCountryCode(companyInfo.getCountryCode());
			contactInfos[paxContactInfoIndex].setPersonOrgID(personOrgId);
			contactInfos[paxContactInfoIndex].setPhoneNumber(companyInfo.getWorkPhone());
			contactInfos[paxContactInfoIndex].setExtension(StringUtils.EMPTY);
			contactInfos[paxContactInfoIndex].setDisplay(companyInfo.getCountryCode());
			count++;
		}
		arrayOfContactInfo.setContactInfo(contactInfos);
		return arrayOfContactInfo;
	}

	private String getMobileNumber(DeliveryInfo deliveryInfo) {
		if (deliveryInfo != null && CollectionUtils.isNotEmpty(deliveryInfo.getContacts())) {
			return deliveryInfo.getContacts().get(0);
		}
		return companyInfo.getHomePhone();
	}

	private String getBookingEmail(DeliveryInfo deliveryInfo) {
		if (deliveryInfo != null && CollectionUtils.isNotEmpty(deliveryInfo.getEmails())) {
			return deliveryInfo.getEmails().get(0);
		}
		return companyInfo.getEmail();
	}

	private Address getPayorAddress() {
		Address address = new Address();
		address.setAddress1(companyInfo.getAddress1());
		address.setAddress2(companyInfo.getAddress2());
		address.setCity(companyInfo.getCity());
		address.setState(companyInfo.getState());
		address.setPostal(companyInfo.getPostalCode());
		address.setCountry(companyInfo.getCountry());
		address.setCountryCode(companyInfo.getCountryCode());
		address.setAreaCode(RadixxConstantsInfo.TICKETPACKAGEID);
		address.setPhoneNumber(companyInfo.getHomePhone());
		address.setDisplay(RadixxConstantsInfo.NOTAPPLICABLE);
		return address;
	}

	public String createPNR(CreatePNRActionTypes commitType, boolean isHold) {
		ConnectPoint_ReservationStub stub = bindingService.getBookReservationStub(configuration);
		try {
			org.tempuri.CreatePNR createPnr = new org.tempuri.CreatePNR();
			CreatePNR createPnrRequest = new CreatePNR();
			createPnrRequest.setSecurityGUID(binaryToken);
			createPnrRequest.setClientIPAddress(StringUtils.EMPTY);
			createPnrRequest.setCarrierCodes(getCarrierCode());
			createPnrRequest.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
			createPnrRequest.setActionType(commitType);
			createPnrRequest.setReservationInfo(RadixxUtils.getReservationInfo(confirmationNumber));
			createPnr.setCreatePnrRequest(createPnrRequest);
			CreatePNRResponse pnrResponse = new CreatePNRResponse();
			listener.setType(String.join("-", "B-", commitType.getValue()));
			stub._getServiceClient().getAxisService().addMessageContextListener(listener);
			pnrResponse = stub.createPNR(createPnr, null);

			if (pnrResponse.getCreatePNRResult().getValuePackageData() != null
					&& configuration.getBasicInfo().getSourceId().intValue() == AirSourceType.FLYDUBAI.getSourceId()) {
				String request = pnrResponse.getCreatePNRResult().getValuePackageData();
				request = addCreatePNREnvelopeHeader(request);
				String response = getResponseByRequest(request, "CreatePNR", CREATE_PNR_ACTION,
						bindingService.getApiUrlRule().getReservationURL());
				if (StringUtils.isBlank(response) || !parsePNRAndHoldLimit(response, isHold)) {
					criticalMessageLogger.add("Unable to confirm pnr " + confirmationNumber);
					confirmationNumber = null;
				}
			} else {
				boolean isErrorInConfirmation = isCriticalException(pnrResponse.getCreatePNRResult().getExceptions());
				if (!isErrorInConfirmation) {
					confirmationNumber = pnrResponse.getCreatePNRResult().getConfirmationNumber();
					holdTimeLimit = pnrResponse.getCreatePNRResult().getReservationFulfillmentRequiredByODT();
				} else {
					// unable to confirm pnr
					confirmationNumber = null;
				}
			}
		} catch (java.rmi.RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (Exception e) {
			throw new SupplierUnHandledFaultException(commitType.getValue() + " PNR Failed ");
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_ReservationStub.class, stub);
		}
		return confirmationNumber;
	}

	private boolean parsePNRAndHoldLimit(String response, boolean isHold) {
		boolean isSuccess = true;
		try {
			Document document = XMLUtils.createDocumentFromString(response);
			NodeList createPNRResult = XMLUtils.getNodeList(document,
					"//Envelope//Body//CreatePNRResponse//CreatePNRResult//ConfirmationNumber");
			if (createPNRResult != null) {
				confirmationNumber = createPNRResult.item(0).getFirstChild().getNodeValue();
			}
			if (isHold) {
				NodeList holdTimeLimitNode = XMLUtils.getNodeList(document,
						"//Envelope//Body//CreatePNRResponse//CreatePNRResult//ReservationFulfillmentRequiredByODT");
				if (holdTimeLimitNode != null) {
					String timeLimit = holdTimeLimitNode.item(0).getFirstChild().getNodeValue();
					if (StringUtils.isNotBlank(timeLimit)) {
						holdTimeLimit = TgsDateUtils.getCalendar(LocalDateTime.parse(timeLimit));
					}
				}
			}
		} catch (Exception e) {
			isSuccess = false;
			log.error("Unable to parse Create PNR for booking {}", bookingId, e);
		}
		return isSuccess;
	}

	private String addCreatePNREnvelopeHeader(String request) {
		request = request.replaceAll(RadixxConstantsInfo.TEMP_URI, "");
		request = request.replaceAll(RadixxConstantsInfo.REMOVE_ENVELOPE_CREATE, "");
		return StringUtils.join(RadixxConstantsInfo.CREATE_PNR_START_ENVELOPE, request,
				RadixxConstantsInfo.CREATE_PNR_CLOSE_ENVELOPE);
	}

	private ArrayOfPerson getPassengers(List<FlightTravellerInfo> travellerInfos, int contactID, long profileId,
			int infantTrvlWith, int personeOrdID) {
		ArrayOfPerson passengers = new ArrayOfPerson();
		int count = 1;
		int adultInfantmappingId = personeOrdID;
		int travelPersonOrgId = personeOrdID;
		for (int paxCountIndex = 0; paxCountIndex < travellerInfos.size(); paxCountIndex++) {

			Person person = new Person();
			person.setIsPrimaryPassenger(false);
			FlightTravellerInfo travellerInfo = travellerInfos.get(paxCountIndex);

			if (travellerInfo.getPaxType() == PaxType.ADULT) {
				if (count++ == 1) {
					person.setIsPrimaryPassenger(true);
				}
				setPaxIDandContactInfo(person, RadixxConstantsInfo.ADULT_PAX_ID, personeOrdID,
						RadixxConstantsInfo.ADULT_TRVL_PERSON_ID);
				if (airline.getSourceId() == AirSourceType.FLYDUBAI.getSourceId()) {
					person.setTravelsWithPersonOrgID(travelPersonOrgId);
				}
			} else if (travellerInfo.getPaxType() == PaxType.CHILD) {
				setPaxIDandContactInfo(person, RadixxConstantsInfo.CHILD_PAX_ID, personeOrdID,
						RadixxConstantsInfo.CHILD_TRVL_PERSON_ID);
				if (airline.getSourceId() == AirSourceType.FLYDUBAI.getSourceId()) {
					person.setTravelsWithPersonOrgID(travelPersonOrgId);
				}
			} else if (travellerInfo.getPaxType() == PaxType.INFANT) {
				setPaxIDandContactInfo(person, RadixxConstantsInfo.INFANT_PAX_ID, personeOrdID, adultInfantmappingId);
				person.setTravelsWithPersonOrgID(infantTrvlWith--);
			}
			person.setAddress(RadixxUtils.getAddress(companyInfo));
			person.setContactInfos(getPassengerContactInfo(personeOrdID, getContactID(personeOrdID)));
			person.setTitle(RadixxUtils.getTitle(travellerInfo, airline));
			person.setFirstName(travellerInfo.getFirstName());
			person.setLastName(travellerInfo.getLastName());
			person.setMiddleName(travellerInfo.getMiddleName());
			person.setGender(getGender(travellerInfo));
			person.setDOB(getDateOfBirth(travellerInfo));
			person.setAge(TgsDateUtils.getAgeFromBirthDate(travellerInfo.getDob()));

			person.setRelationType(EnumerationsRelationshipTypes.Self);
			person.setNationalityLaguageID(RadixxConstantsInfo.NATIONALITYLAGUAGEID);
			person.setPassport(travellerInfo.getPassportNumber());
			person.setNationality(travellerInfo.getPassportNationality());
			person.setMarketingOptIn(true);
			person.setUseInventory(false);
			person.setCompany(StringUtils.EMPTY);
			person.setComments(StringUtils.EMPTY);
			person.setKnownTravelerNumber(StringUtils.EMPTY);
			person.setRedressNumber(getAdditionalInfo());
			person.setProfileId(profileId);
			passengers.addPerson(person);
			personeOrdID--;
		}
		return passengers;
	}

	private Person setPaxIDandContactInfo(Person passenger, int paxTypeId, int personOrgID, int travelPersonOrgId) {
		passenger.setPersonOrgID(personOrgID);
		passenger.setPTCID(paxTypeId);
		passenger.setPTC(paxTypeId + "");
		passenger.setWBCID(paxTypeId);
		int contactID = getContactID(personOrgID);
		passenger.setContactInfos(getPassengerContactInfo(personOrgID, contactID));
		if (airline.getSourceId() == AirSourceType.FLYDUBAI.getSourceId()) {
			passenger.setTravelsWithPersonOrgID(personOrgID);
		} else {
			passenger.setTravelsWithPersonOrgID(travelPersonOrgId);
		}
		return passenger;
	}

	/**
	 * @param personeOrdID
	 * @return contactId
	 */
	private int getContactID(int personeOrdID) {
		int contactID = Integer.parseInt(personeOrdID + "1");
		return contactID;
	}

	/**
	 * @implSpec : This method will retun NA when gst not applicable 1. if GST Applicable and GST Instance exists with
	 *           Valid GST number then return GST number
	 */
	private String getAdditionalInfo() {
		String additionalReference = RadixxConstantsInfo.NOTAPPLICABLE;
		if (airline.isGSTApplicable() && gstInfo != null && StringUtils.isNotEmpty(gstInfo.getGstNumber())) {
			additionalReference = gstInfo.getGstNumber();
		}
		return additionalReference;
	}

	private Calendar getDateOfBirth(FlightTravellerInfo travellerInfo) {
		Calendar dobCal = null;
		if (travellerInfo.getDob() == null) {
			dobCal = TgsDateUtils.getDefaultDOB();
		} else {
			dobCal = TgsDateUtils.getCalendar(travellerInfo.getDob());
		}
		dobCal.clear(Calendar.ZONE_OFFSET);
		return dobCal;
	}

	private ArrayOfPayment getPaymentDetailsForPnr() {
		ArrayOfPayment payment = new ArrayOfPayment();
		int size = 1;
		Payment[] payments = new Payment[size];
		for (int pnrPaymentIndex = 0; pnrPaymentIndex < size; pnrPaymentIndex++) {
			payments[pnrPaymentIndex] = new Payment();
			payments[pnrPaymentIndex].setReservationPaymentID(24);
			payments[pnrPaymentIndex].setFirstName(configuration.getSupplierCredential().getAgentUserName());
			payments[pnrPaymentIndex]
					.setPaymentCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
			payments[pnrPaymentIndex].setLastName(configuration.getSupplierCredential().getAgentUserName());
			payments[pnrPaymentIndex].setISOCurrency(1);
			payments[pnrPaymentIndex].setPaymentAmount(BigDecimal.ZERO);
			payments[pnrPaymentIndex].setPaymentMethod(EnumerationsPaymentMethodTypes.INVC);
			payments[pnrPaymentIndex].setExpirationDate(Calendar.getInstance());
			payments[pnrPaymentIndex].setIsTACreditCard(false);
			payments[pnrPaymentIndex].setVoucherNum(RadixxConstantsInfo.VOUCHERNUMBER);
			payments[pnrPaymentIndex].setGcxID(RadixxConstantsInfo.TICKETPACKAGEID);
			payments[pnrPaymentIndex].setGcxOpt(RadixxConstantsInfo.TICKETPACKAGEID);
			payments[pnrPaymentIndex].setOriginalAmount(BigDecimal.ZERO);
			payments[pnrPaymentIndex]
					.setOriginalCurrency(airline.getLocalCurrencyCode(configuration.getSupplierCredential()));
			payments[pnrPaymentIndex].setExchangeRate(BigDecimal.valueOf(1));
			payments[pnrPaymentIndex].setExchangeRateDate(Calendar.getInstance());
			payments[pnrPaymentIndex].setPaymentComment(RadixxConstantsInfo.NOTAPPLICABLE);
			payments[pnrPaymentIndex].setCardType(RadixxConstantsInfo.CREDIT);
			payments[pnrPaymentIndex].setCardNum(StringUtils.EMPTY);
			payments[pnrPaymentIndex].setCVCode(StringUtils.EMPTY);
			payments[pnrPaymentIndex].setCardHolder(configuration.getSupplierCredential().getAgentUserName());
			payments[pnrPaymentIndex].setCompanyName(companyInfo.getCompanyName());
			payments[pnrPaymentIndex].setBillingCountry(companyInfo.getNationality());
		}
		payment.setPayment(payments);
		return payment;
	}

	/**
	 * @param travellerInfo
	 * @return GenderTypes
	 */
	private EnumerationsGenderTypes getGender(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getTitle().toUpperCase().equals("MR") || travellerInfo.getTitle().toUpperCase().equals("MSTR")
				|| travellerInfo.getTitle().toUpperCase().equals("MASTER")) {
			return EnumerationsGenderTypes.Male;
		} else if (travellerInfo.getTitle().toUpperCase().equals("MRS")
				|| travellerInfo.getTitle().toUpperCase().equals("MISS")
				|| travellerInfo.getTitle().toUpperCase().equals("MS")) {
			return EnumerationsGenderTypes.Female;
		}
		return EnumerationsGenderTypes.Male;
	}
}
