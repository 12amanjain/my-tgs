package com.tgs.services.fms.sources.otaradixx;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.commons.lang3.StringUtils;

public class RadixxConstantsInfo {

	public static final int CORPORATIONID = -214;
	public static final int SEARCHLFID = -214;
	public static final int FZBAGGAGECATEGORYID = 99;
	public static final int FZMEALCATEGORYID = 121;
	public static final int IXBAGGAGECATEGORYID = 22;
	public static final int IXMEALCATEGORYID = 6;
	public static final String EXTERNALBOOKINGID = "na";
	public static final String NOTAPPLICABLE = "na";
	public static final String SERIESNUMBER = "299";
	public static final String RECEIPTLANGUAGEID = "1";
	public static final String POSTAL = "353";
	public static final String AREACODE = "4";
	public static final int NATIONALITYLAGUAGEID = 1;
	public static final int WBCID = 1;
	public static final int PTC = 1;
	public static final int PERSONAORGID = -214;
	public static final long VOUCHERNUMBER = -2147483648;
	public static final long PROFILEID = -2147483648;
	public static final int SERVICEID = -2147483648;
	public static final BigDecimal CHECKNUMBER = BigDecimal.valueOf(1234);
	public static final String EMPTY = StringUtils.EMPTY;
	public static final String LANUGUAGECODE = "en";
	public static final String TICKETPACKAGEID = "1";
	public static final String FUELCODE = "FUEL";
	public static final String GST = "K3";
	public static final int FARECATEGORYID = 1;
	public static final String FFNUMBER = "11";
	public static final String CREDIT = "Credit";
	public static final int PERSON_ORG_ID = -214;
	public static final int PERSON_CONTACT_ID = -2141;
	public static final int PERSON_PROFILE_ID = -2147483648;


	// Baggage
	public static final String FBA10KG = "10KG";
	public static final String FBA20KG = "20KG";
	public static final String FBA25KG = "25KG";
	public static final String FBA30KG = "30KG";
	public static final String FBA40KG = "40KG";

	public static final String BAGB_10 = "10 KG Included";
	public static final String BAGB_20 = "20 KG Included";
	public static final String BAGB_25 = "25 KG Included";
	public static final String BAGB_30 = "30 KG Included";
	public static final String BAGB_40 = "40 KG Included";


	// fare identifier
	// public static final String EXPRESS_VALUE = "EXPRESS VALUE";
	// public static final String EXPRESS_FLEXI = "EXPRESS FLEXI";
	// public static final String LITE = "LITE";
	// public static final String LITEHB = "LITE + HANDBAGGAGE";
	// public static final String VALUE = "VALUE";
	// public static final String FLEX = "FLEX";
	// public static final String BUSINESS = "BUSINESS";
	// public static final String PAYTOCHANGE = "PAY TO CHANGE";
	// public static final String FREETOCHANGE = "FREE TO CHANGE";
	// public static final String NOCHANGE = "NO CHANGE";
	// public static final String LIGHT = "LIGHT";
	// public static final String WLIGHT = "WLIGHT";
	// public static final String FRIENDLY = "FRIENDLY";
	// public static final String WFRIENDLY = "WFRIENDLY";
	// public static final String WFLEX = "WFLEX";
	// public static final String FLEXI = "FLEXI";


	public static final int ADULT_PAX_ID = 1;
	public static final int CHILD_PAX_ID = 6;
	public static final int INFANT_PAX_ID = 5;

	public static final int ADULT_TRVL_PERSON_ID = -2147483648;
	public static final int CHILD_TRVL_PERSON_ID = -2147483648;

	public static final String TEMP_URI = "xmlns:ns7=\"http://tempuri.org/\"";
	// CREATE PNR

	public static final String REMOVE_ENVELOPE_CREATE =
			"xmlns:ns3=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns3:CreatePNR\"";
	public static final String CREATE_PNR_START_ENVELOPE =
			"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns7=\"http://tempuri.org/\" xmlns:ns3=\"http://schemas.datacontract.org/2004/07/Radixx.ConnectPoint.Reservation.Request\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:type=\"ns3:CreatePNR\"><soapenv:Header/>";
	public static final String CREATE_PNR_CLOSE_ENVELOPE = "</soapenv:Envelope>";

	//http://connectpoint.radixx.com/sdk/20.2/#Automated%20Error%20Handling.html
	public static final int SECURITY_EXCEP_START = 800;
	public static final int SECURITY_EXCEP_END = 899;

}
