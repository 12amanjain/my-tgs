package com.tgs.services.fms.sources.otaradixx;

import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import lombok.Getter;

@Getter
public enum RadixxPriceInfo {

	EXPRESS_PROMO_20KG("Express Promo") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_PROMO);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_VALUE_20KG("Express VALUE (FBA 20Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_VALUE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_VALUE_25KG("Express VALUE (FBA 25Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_VALUE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_VALUE_30KG("Express VALUE (FBA 30Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_VALUE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_VALUE_40KG("Express VALUE (FBA 40Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_VALUE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_FLEXI_20KG("Express FLEXI (FBA 20Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_FLEXI);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_FLEXI_25KG("Express FLEXI (FBA 25Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_FLEXI);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_FLEXI_30KG("Express FLEXI (FBA 30Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_FLEXI);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_FLEXI_40KG("Express FLEXI (FBA 40Kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.EXPRESS_FLEXI);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	LITE("LITE") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			// As it Hand Bagagge Fare
			priceInfo.setFareIdentifier(FareType.LITE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	VALUE("VALUE") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.VALUE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	FLEX("FLEX") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.FLEX);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	BUSINESS("Business") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.BUSINESS);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
			updateCabinClass(priceInfo, CabinClass.BUSINESS);
		}
	},
	SPECIAL_OFFER("Special Offer") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.PROMO);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	EXPRESS_PROMO_40KG("Express PROMO (FBA 40kg)") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.PROMO);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	PAY_TO_CHANGE("Pay To Change") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.PAYTOCHANGE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	FREE_TO_CHANGE("Free To Change") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.FREETOCHANGE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	NO_CHANGE("No Change") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.NOCHANGE);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	BASIC("Basic") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.BUSINESS);
			updateCabinClass(priceInfo, CabinClass.BUSINESS);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	LIGHT("Light") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.LIGHT);
			updateRefundableType(priceInfo, RefundableType.NON_REFUNDABLE);
		}
	},
	WLIGHT("WLight") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.WLIGHT);
			updateRefundableType(priceInfo, RefundableType.NON_REFUNDABLE);
		}
	},
	FRIENDLY("FRIENDLY") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.FRIENDLY);
			updateRefundableType(priceInfo, RefundableType.NON_REFUNDABLE);
		}
	},
	WFRIENDLY("WFRIENDLY") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.WFRIENDLY);
			updateRefundableType(priceInfo, RefundableType.NON_REFUNDABLE);
		}
	},
	WFLEX("WFLEX") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.WFLEX);
			updateRefundableType(priceInfo, RefundableType.NON_REFUNDABLE);
		}
	},
	FLEXI("Flexi") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.FLEXI);
			updateRefundableType(priceInfo, RefundableType.REFUNDABLE);
		}
	},
	DEFAULT("NONE") {
		@Override
		public void processPriceInfo(PriceInfo priceInfo) {
			priceInfo.setFareIdentifier(FareType.PUBLISHED);
			updateRefundableType(priceInfo, RefundableType.NON_REFUNDABLE);
		}
	};

	private String fareType;

	public abstract void processPriceInfo(PriceInfo priceInfo);

	public static RadixxPriceInfo getOTARadixxPriceInfo(String fareType) {
		for (RadixxPriceInfo otaRadixxPriceInfo : RadixxPriceInfo.values()) {
			if (otaRadixxPriceInfo.fareType.equalsIgnoreCase(fareType)) {
				return otaRadixxPriceInfo;
			}
		}
		return RadixxPriceInfo.DEFAULT;
	}

	public void updateCabinClass(PriceInfo priceInfo, CabinClass cabinClass) {
		if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
			priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
				fareDetail.setCabinClass(cabinClass);
			}));
		}
	}

	public void updateRefundableType(PriceInfo priceInfo, RefundableType refundableType) {
		if (MapUtils.isNotEmpty(priceInfo.getFareDetails())) {
			priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
				fareDetail.setRefundableType(refundableType.getRefundableType());
			}));
		}
	}

	private RadixxPriceInfo(String fareType) {
		this.fareType = fareType;
	}

}
