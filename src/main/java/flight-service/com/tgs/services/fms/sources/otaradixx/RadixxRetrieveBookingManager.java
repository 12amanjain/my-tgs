package com.tgs.services.fms.sources.otaradixx;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import com.google.common.base.Enums;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.RetrievePNRActionTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.Airline;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.AirlinePerson;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.ArrayOfCustomer;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.Charge;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.ContactInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.Customer;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.LogicalFlight;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.ViewPNR;
import org.tempuri.ConnectPoint_ReservationStub;
import org.tempuri.RetrievePNR;
import org.tempuri.RetrievePNRResponse;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import lombok.extern.slf4j.Slf4j;

@Setter
@Slf4j
@SuperBuilder
final class RadixxRetrieveBookingManager extends RadixxServiceManager {

	protected String pnr;

	public AirImportPnrBooking retrievePNRBooking() {
		AirImportPnrBooking pnrBooking = null;
		RetrievePNRResponse pnrResponse = retrieveBooking();
		if (isValidPNRBooking(pnrResponse, pnr)) {
			List<TripInfo> tripInfos = parseTripResponse(pnrResponse, pnr);
			GstInfo gstInfo = fetchGSTInfo(pnrResponse);
			DeliveryInfo deliveryInfo = fetchDeliveryInfo(pnrResponse);
			pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).gstInfo(gstInfo)
					.build();
			updateHoldTimeLimit(pnrResponse);
		}
		return pnrBooking;
	}

	private GstInfo fetchGSTInfo(RetrievePNRResponse pnrResponse) {
		GstInfo gstInfo = null;
		// GST not Suppoerted On Radixx
		return gstInfo;
	}

	private DeliveryInfo fetchDeliveryInfo(RetrievePNRResponse pnrResponse) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		ContactInfo[] contactInfos = pnrResponse.getRetrievePNRResult().getContactInfos().getContactInfo();
		// Set Email
		List<ContactInfo> emailInfo = Arrays.stream(contactInfos).filter(bookingContact -> {
			return (Objects.nonNull(bookingContact.getContactType()) && bookingContact.getContactType() == 4);
		}).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(emailInfo)) {
			deliveryInfo.setEmails(Arrays.asList(emailInfo.get(0).getContactField()));
		}
		// Set Contact Info of Customer Number
		List<ContactInfo> contact = Arrays.stream(contactInfos).filter(bookingContact -> {
			return (Objects.nonNull(bookingContact.getContactType()) && bookingContact.getContactType() == 2);
		}).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(contact)) {
			deliveryInfo.setContacts(Arrays.asList(contact.get(0).getContactField()));
		}
		return deliveryInfo;
	}

	private List<TripInfo> parseTripResponse(RetrievePNRResponse pnrResponse, String pnr) {
		List<TripInfo> tripInfos = new ArrayList<>();
		ViewPNR viewPNR = pnrResponse.getRetrievePNRResult();
		if (ArrayUtils.isNotEmpty(viewPNR.getAirlines().getAirline())) {
			Airline[] airlines = pnrResponse.getRetrievePNRResult().getAirlines().getAirline();
			Arrays.stream(airlines).forEach(airline -> {
				LogicalFlight[] logicalFlights = airline.getLogicalFlight().getLogicalFlight();
				Arrays.stream(logicalFlights).forEach(logicalFlight -> {
					TripInfo tripInfo = new TripInfo();
					AtomicInteger segmentNum = new AtomicInteger(0);
					Arrays.stream(logicalFlight.getPhysicalFlights().getPhysicalFlight()).forEach(physicalFlight -> {
						SegmentInfo segmentInfo = new SegmentInfo();
						segmentInfo.setSegmentNum(segmentNum.get());
						segmentInfo.setIsReturnSegment(false);
						segmentInfo.setArrivalAirportInfo(
								AirportHelper.getAirportInfo(physicalFlight.getDestination().trim()));
						segmentInfo
								.setDepartAirportInfo(AirportHelper.getAirportInfo(physicalFlight.getOrigin().trim()));
						segmentInfo
								.setDepartTime(TgsDateUtils.calenderToLocalDateTime(physicalFlight.getDepartureTime()));
						segmentInfo
								.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(physicalFlight.getArrivaltime()));
						segmentInfo.setDuration(segmentInfo.calculateDuration());
						Map<SSRType, List<? extends SSRInformation>> preSSR =
								AirUtils.getPreSSR(configuration.getBasicInfo(), segmentInfo.getAirlineCode(false), bookingUser);
						FlightDesignator designator = FlightDesignator.builder().build();
						designator.setAirlineInfo(AirlineHelper.getAirlineInfo(physicalFlight.getCarrierCode().trim()));
						designator.setEquipType(physicalFlight.getAirCraftDescription());
						designator.setFlightNumber(physicalFlight.getFlightNumber().trim());
						segmentInfo.getDepartAirportInfo()
								.setTerminal(AirUtils.getTerminalInfo(physicalFlight.getFromTerminal()));
						segmentInfo.getArrivalAirportInfo()
								.setTerminal(AirUtils.getTerminalInfo(physicalFlight.getToTerminal()));
						segmentInfo.setFlightDesignator(designator);
						AirlineInfo operatingAirline =
								AirlineHelper.getAirlineInfo(physicalFlight.getOperatingCarrier().trim());
						if (operatingAirline != null && !operatingAirline.getCode()
								.equalsIgnoreCase(segmentInfo.getFlightDesignator().getAirlineCode())) {
							segmentInfo.setOperatedByAirlineInfo(operatingAirline);
						}
						segmentInfo.setBookingRelatedInfo(getBookingRelatedInfo(physicalFlight.getCustomers(), preSSR));
						PriceInfo priceInfo = PriceInfo.builder().build();
						priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
						segmentInfo.setPriceInfoList(Arrays.asList(priceInfo));
						tripInfo.getSegmentInfos().add(segmentInfo);
						segmentNum.getAndIncrement();
					});
					tripInfos.add(tripInfo);
				});
			});
		}
		return processTrips(tripInfos);
	}

	private List<TripInfo> processTrips(List<TripInfo> tripInfos) {
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			if (tripInfos.size() == 2) {
				if (AirUtils.isDomesticTrip(tripInfos.get(0)) && AirUtils.isDomesticTrip(tripInfos.get(1))) {
					tripInfos.get(1).getSegmentInfos().forEach(segmentInfo -> {
						segmentInfo.setIsReturnSegment(true);
					});
				}
			}
			tripInfos.forEach(tripInfo -> {
				sortSegmentInfosOnDepatureTime(tripInfo.getSegmentInfos());
				setSegmentNumOnSegment(tripInfo);
				tripInfo.setConnectionTime();
			});

			sortTripInfosOnDepatureTime(tripInfos);
			return tripInfos;
		}
		return null;
	}

	private void updateHoldTimeLimit(RetrievePNRResponse pnrResponse) {
		if ((pnrResponse.getRetrievePNRResult().getPayments() == null
				|| ArrayUtils.isEmpty(pnrResponse.getRetrievePNRResult().getPayments().getPayment()))
				&& pnrResponse.getRetrievePNRResult().getReservationFulfillmentRequiredByODT() != null) {
			holdTimeLimit = pnrResponse.getRetrievePNRResult().getReservationFulfillmentRequiredByODT();
		}
	}

	private void setSegmentNumOnSegment(TripInfo tripInfo) {
		AtomicInteger segmentIndex = new AtomicInteger(0);
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.setSegmentNum(segmentIndex.get());
			segmentIndex.getAndIncrement();
		});
	}

	private void sortTripInfosOnDepatureTime(List<TripInfo> tripInfos) {
		Collections.sort(tripInfos, new Comparator<TripInfo>() {
			public int compare(TripInfo t1, TripInfo t2) {
				return t1.getDepartureTime().compareTo(t2.getDepartureTime());
			}
		});
	}

	private void sortSegmentInfosOnDepatureTime(List<SegmentInfo> segmentInfos) {
		Collections.sort(segmentInfos, new Comparator<SegmentInfo>() {
			public int compare(SegmentInfo s1, SegmentInfo s2) {
				return s1.getDepartTime().compareTo(s2.getDepartTime());
			}
		});
	}

	private SegmentBookingRelatedInfo getBookingRelatedInfo(ArrayOfCustomer arrayOfCustomer,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {
		List<FlightTravellerInfo> travellerInfoList = new ArrayList<>();
		Customer[] customers = arrayOfCustomer.getCustomer();
		for (AirlinePerson person : customers[0].getAirlinePersons().getAirlinePerson()) {
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			travellerInfo.setTitle(person.getTitle());
			travellerInfo.setPaxType(RadixxPaxType.getPaxType(person.getPTCID()));
			travellerInfo.setFirstName(person.getFirstName());
			travellerInfo.setLastName(person.getLastName());
			travellerInfo.setAge(person.getAge());
			travellerInfo.setPassportNumber(person.getPassport());
			travellerInfo.setPassportNationality(person.getNationality());
			travellerInfo.setPnr(pnr);
			FareDetail fareDetail = getFareDetail(person, travellerInfo, preSSR);
			CabinClass cabinClass = Enums.getIfPresent(CabinClass.class, person.getCabin()).or(CabinClass.ECONOMY);
			fareDetail.setCabinClass(cabinClass);
			travellerInfo.setFareDetail(fareDetail);
			travellerInfoList.add(travellerInfo);
		}
		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfoList).build();
		return bookingRelatedInfo;
	}

	private FareDetail getFareDetail(AirlinePerson person, FlightTravellerInfo travellerInfo,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {
		FareDetail fareDetail = new FareDetail();
		Map<FareComponent, Double> fareComponents = new HashMap<>();
		fareComponents.put(FareComponent.BF, getBaseFare(person.getCharges().getCharge()));
		fareComponents.put(FareComponent.AT, getAirlineTax(person.getCharges().getCharge()) - agencyCommission);
		fareComponents.put(FareComponent.YQ, getFuelYQFare(person.getCharges().getCharge()));
		fareComponents.put(FareComponent.TF, getTotalFare(person.getCharges().getCharge()));
		travellerInfo.setSsrBaggageInfo(getBaggageSSRInfo(person.getCharges().getCharge(), fareDetail));
		travellerInfo.setSsrMealInfo(getMealInfo(person.getCharges().getCharge(), fareDetail, preSSR));
		fareDetail.setFareComponents(fareComponents);
		fareDetail.setRefundableType(RefundableType.REFUNDABLE.getRefundableType());
		RadixxPriceInfo radixxPriceInfo = RadixxPriceInfo.getOTARadixxPriceInfo(person.getWebFareType());
		fareDetail.setFareType(radixxPriceInfo.getFareType());
		return fareDetail;
	}

	private Double getTotalFare(Charge[] charges) {
		Double totalFare = 0.0;
		if (ArrayUtils.isNotEmpty(charges)) {
			for (Charge charge : charges) {
				if (!charge.getIsSSR()) {
					totalFare += charge.getAmount().doubleValue();
				}
			}
		}
		return totalFare;
	}

	private Double getBaseFare(Charge[] charges) {
		double baseFare = 0;
		if (ArrayUtils.isNotEmpty(charges)) {
			for (Charge charge : charges) {
				if (!charge.getIsSSR()) {
					if (airline.getBaseFareCode().contains(charge.getCodeType().trim().toUpperCase())) {
						baseFare += getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
					}
				}
			}
		}
		return baseFare;
	}

	private Double getAirlineTax(Charge[] charges) {
		double airlineTax = 0;
		if (ArrayUtils.isNotEmpty(charges)) {
			for (Charge charge : charges) {
				if (!charge.getIsSSR()) {
					if (airline.getTaxCode().contains(charge.getCodeType().trim().toUpperCase())) {
						airlineTax += getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
					}
				}
			}
		}
		return airlineTax;
	}

	private Double getFuelYQFare(Charge[] charges) {
		double fuelCharge = 0;
		if (ArrayUtils.isNotEmpty(charges)) {
			for (Charge charge : charges) {
				if (!charge.getIsSSR()) {
					if (airline.getFuelTaxCode().contains(charge.getCodeType().trim().toUpperCase())) {
						fuelCharge += getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
					}
				}
			}
		}
		return fuelCharge;
	}

	private SSRInformation getBaggageSSRInfo(Charge[] charges, FareDetail fareDetail) {
		SSRInformation ssrInformation = null;
		double amount = 0;
		if (ArrayUtils.isNotEmpty(charges)) {
			for (Charge charge : charges) {
				String codeType = charge.getCodeType().toUpperCase().replaceAll("\\s+", "");
				if (charge.getIsSSR() && airline.getBaggageType().containsKey(codeType)) {
					ssrInformation = new SSRInformation();
					amount += getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
					ssrInformation.setKey(codeType);
					ssrInformation.setCode(codeType);
					ssrInformation.setDesc(airline.getBaggageType().get(codeType) + " KG");
				}
				if (ssrInformation != null && StringUtils.isNotEmpty(ssrInformation.getDesc())) {
					ssrInformation.setAmount(amount);
				}

				// this is to set check in baggage
				if (StringUtils.isNotBlank(codeType)) {
					if (airline.allowedBaggage().containsKey(codeType)) {
						BaggageInfo baggageInfo = new BaggageInfo();
						Map<String, List<String>> baggageMap = airline.allowedBaggage().get(codeType);
						baggageMap.forEach((key, value) -> {
							baggageInfo.setAllowance(key + " KG");
						});
						fareDetail.setBaggageInfo(baggageInfo);
					} else if (codeType.equalsIgnoreCase("FBA")) {
						BaggageInfo baggageInfo = new BaggageInfo();
						baggageInfo.setAllowance(" KG");
						fareDetail.setBaggageInfo(baggageInfo);
					}
				}
			}
		}
		return ssrInformation;
	}


	private SSRInformation getMealInfo(Charge[] charges, FareDetail fareDetail,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {
		SSRInformation ssrInformation = null;
		double amount = 0;
		if (ArrayUtils.isNotEmpty(charges)) {
			for (Charge charge : charges) {
				if (charge.getIsSSR()
						&& RadixxUtils.containsSSRCode(charge.getCodeType().trim(), preSSR, true, false)) {
					ssrInformation = new SSRInformation();
					amount += getAmountBasedOnCurrency(charge.getAmount(), charge.getCurrencyCode());
					ssrInformation.setKey(charge.getCodeType().trim());
					ssrInformation.setDesc("Meal Included");
					fareDetail.setIsMealIncluded(true);
				}
				if (ssrInformation != null && StringUtils.isNotEmpty(ssrInformation.getDesc())) {
					ssrInformation.setAmount(amount);
				}
			}
		}
		return ssrInformation;
	}

	public RetrievePNRResponse retrieveBooking() {
		ConnectPoint_ReservationStub reservationStub = null;
		try {
			RetrievePNR retrievePNR = new RetrievePNR();
			reservationStub = bindingService.getBookReservationStub(configuration);
			org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.RetrievePNR request =
					new org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.RetrievePNR();
			request.setCarrierCodes(getCarrierCode());
			request.setSecurityGUID(binaryToken);
			request.setActionType(RetrievePNRActionTypes.GetReservation);
			request.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
			request.setReservationInfo(RadixxUtils.getReservationInfo(pnr));
			retrievePNR.setRetrievePnrRequest(request);
			listener.setType("9-R-RetreivePNR");
			reservationStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			RetrievePNRResponse pnrResponse = reservationStub.retrievePNR(retrievePNR);
			if (isCriticalException(pnrResponse.getRetrievePNRResult().getExceptions())) {
				pnrResponse = null;
			}
			return pnrResponse;
		} catch (java.rmi.RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			reservationStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_ReservationStub.class, reservationStub);
		}
	}

	public boolean isValidPNRBooking(RetrievePNRResponse pnrResponse, String pnr) {
		if (pnrResponse != null && pnrResponse.getRetrievePNRResult() != null
				&& StringUtils.isNotEmpty(pnrResponse.getRetrievePNRResult().getConfirmationNumber())
				&& (pnrResponse.getRetrievePNRResult().getAirlines() == null
						|| ArrayUtils.isEmpty(pnrResponse.getRetrievePNRResult().getAirlines().getAirline()))) {
			throw new NoPNRFoundException("No PNR/Journey Available " + pnr);
		} else if (pnrResponse == null || pnrResponse.getRetrievePNRResult() == null
				|| StringUtils.isEmpty(pnrResponse.getRetrievePNRResult().getConfirmationNumber())) {
			throw new NoPNRFoundException("Invalid PNR " + pnr);
		} else if (pnrResponse != null && pnrResponse.getRetrievePNRResult() != null && isCancelled(pnrResponse)) {
			throw new NoPNRFoundException("PNR/Journey Expired " + pnr);
		}
		return true;
	}

}
