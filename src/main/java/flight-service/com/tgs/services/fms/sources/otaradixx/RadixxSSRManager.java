package com.tgs.services.fms.sources.otaradixx;


import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatPosition;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_request.RetrieveSeatAvailabilityList;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.ArrayOfCabin;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.ArrayOfSeatAssignment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.ArrayOfSeatCharge;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.Cabin;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.PhysicalFlight;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.SeatAssignment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.SeatCharge;
import org.datacontract.schemas._2004._07.radixx_connectpoint_flight_response.SeatMap;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_service.ArrayOfServiceQuote;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_service.RetrieveServiceQuotes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_service.ServiceQuote;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.ArrayOfViewServiceQuote;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.ViewServiceQuote;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsReservationChannelTypes;
import org.tempuri.ConnectPoint_FlightStub;
import org.tempuri.ConnectPoint_PricingStub;
import org.tempuri.RetrieveSeatAvailabilityListResponse;
import org.tempuri.RetrieveServiceQuotesResponse;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Arrays;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

@Slf4j
@Setter
@SuperBuilder
final class RadixxSSRManager extends RadixxServiceManager {


	public TripInfo getSpecialServices(TripInfo tripInfo, AirSearchQuery searchQuery) {
		ConnectPoint_PricingStub pricingStub = null;
		try {
			pricingStub = bindingService.getSSRStub(configuration);
			org.tempuri.RetrieveServiceQuotes serviceQuoteReq = new org.tempuri.RetrieveServiceQuotes();
			RetrieveServiceQuotes retrieveServiceQuote = new RetrieveServiceQuotes();
			retrieveServiceQuote.setSecurityGUID(binaryToken);
			retrieveServiceQuote.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
			retrieveServiceQuote.setCarrierCodes(getCarrierCode());
			retrieveServiceQuote.setRetrieveServiceQuotes(getServiceQuote(searchQuery, tripInfo));
			serviceQuoteReq.setRetrieveServiceQuotesRequest(retrieveServiceQuote);
			listener.setType("5-R-SSR");
			pricingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			RetrieveServiceQuotesResponse serviceQuoteResponse = pricingStub.retrieveServiceQuotes(serviceQuoteReq);
			if (!isCriticalException(serviceQuoteResponse.getRetrieveServiceQuotesResult().getExceptions())
					&& serviceQuoteResponse.getRetrieveServiceQuotesResult().getServiceQuotes() != null
					&& ArrayUtils.isNotEmpty(serviceQuoteResponse.getRetrieveServiceQuotesResult().getServiceQuotes()
							.getViewServiceQuote())) {
				tripInfo = parseSpecialService(tripInfo,
						serviceQuoteResponse.getRetrieveServiceQuotesResult().getServiceQuotes());
			}
		} catch (Exception e) {
			addExceptionToLogger(e);
			log.error("Unable to fetch ssr for booking {} trip {} cause ", bookingId, tripInfo.toString(), e);
		} finally {
			pricingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return tripInfo;
	}

	private ArrayOfServiceQuote getServiceQuote(AirSearchQuery searchQuery, TripInfo tripInfo) {
		ArrayOfServiceQuote ssrQuote = new ArrayOfServiceQuote();
		ServiceQuote[] serviceQuote = new ServiceQuote[1];
		if (searchQuery.isReturn()) {
			serviceQuote = new ServiceQuote[2];
		}
		for (int index = 0; index < serviceQuote.length; index++) {
			ServiceQuote quote = new ServiceQuote();
			SegmentInfo segment = new SegmentInfo();
			if (index == 0) {
				segment = getRequestedSegmentInfo(tripInfo, index);
			} else if (index == 1) {
				segment = getRequestedSegmentInfo(tripInfo, index);
			}
			quote.setSourceId(configuration.getSourceId());
			quote.setLogicalFlightID(segment.getPriceInfo(0).getMiscInfo().getLogicalFlightId().intValue());
			quote.setDepartureDate(TgsDateUtils.getCalendar(segment.getDepartTime().toLocalDate()));
			quote.setAirportCode(segment.getDepartAirportInfo().getCode());
			quote.setCabin(segment.getPriceInfo(0).getFareDetails().get(PaxType.ADULT).getCabinClass().name());
			quote.setCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()));
			quote.setUTCOffset(0);
			quote.setOperatingCarrierCode(carrierCode);
			quote.setMarketingCarrierCode(carrierCode);
			quote.setReservationChannel(EnumerationsReservationChannelTypes.TPAPI);
			quote.setDestinationAirportCode(searchQuery.getRouteInfos().get(index).getToCityOrAirport().getCode());
			serviceQuote[index] = quote;
		}
		ssrQuote.setServiceQuote(serviceQuote);
		return ssrQuote;
	}

	private SegmentInfo getRequestedSegmentInfo(TripInfo tripInfo, int returnIndex) {
		SegmentInfo segmentInfo = new SegmentInfo();
		if (returnIndex == 1) {
			segmentInfo = tripInfo.getSegmentInfos().stream()
					.filter(segmentInfo1 -> BooleanUtils.isTrue(segmentInfo1.getIsReturnSegment())).findFirst().get();
		} else if (returnIndex == 0) {
			segmentInfo = tripInfo.getSegmentInfos().get(0);
		}
		return segmentInfo;
	}

	public TripInfo parseSpecialService(TripInfo tripInfo, ArrayOfViewServiceQuote arrayOfServiceQuote) {
		SegmentInfo previousSegmentInfo = null;
		for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
			if (segment.getSegmentNum() == 0) {
				previousSegmentInfo = segment;
			}
			Map<SSRType, List<? extends SSRInformation>> ssrInfo = new HashMap<>();
			List<BaggageSSRInformation> baggageInfoList = new ArrayList<>();
			List<MealSSRInformation> mealInfoList = new ArrayList<>();
			for (ViewServiceQuote serviceQuote : arrayOfServiceQuote.getViewServiceQuote()) {
				if (serviceQuote.getLogicalFlightID() == segment.getPriceInfo(0).getMiscInfo().getLogicalFlightId()
						&& serviceQuote.getQuantityAvailable() > 0 && serviceQuote.getAmount().doubleValue() != 0.0) {
					if (serviceQuote.getCategoryID() == airline.getBaggageCategoryId()
							&& StringUtils.isNotBlank(serviceQuote.getSSRCode())
							&& airline.getBaggageType().containsKey(serviceQuote.getSSRCode())) {
						setBaggageSSR(serviceQuote, baggageInfoList);
					} else if (serviceQuote.getCategoryID() == airline.getMealCategoryId()) {
						setMealSSR(serviceQuote, mealInfoList);
					}
				}
			}

			if (CollectionUtils.isNotEmpty(baggageInfoList)) {
				if (segment.getSegmentNum() > 0) {
					baggageInfoList.forEach(baggage -> {
						baggage.setAmount(null);
					});
				}
			}
			if (CollectionUtils.isNotEmpty(mealInfoList)) {
				if (segment.getSegmentNum() > 0) {
					mealInfoList.forEach(mealInfo -> {
						mealInfo.setAmount(0d);
					});
				}
			}

			// As trip Wise fare , SSR also applicable on Trip Wise from Supplier side
			ssrInfo.put(SSRType.BAGGAGE, preCheckWithCheckedInBaggage(previousSegmentInfo, baggageInfoList));

			if (airline.isMealApplicableonAPI()) {
				ssrInfo.put(SSRType.MEAL, preCheckWithMeal(previousSegmentInfo, mealInfoList));
			}
			segment.setSsrInfo(ssrInfo);
		}
		return tripInfo;
	}

	/**
	 * @implSpec : Reason to keep as First Segment Info is , As Trip wise Fare Everything includes for Connecting
	 *           segment as well
	 */
	private List<BaggageSSRInformation> preCheckWithCheckedInBaggage(SegmentInfo segment,
			List<BaggageSSRInformation> baggageInfoList) {
		List<BaggageSSRInformation> finalBaggageInfoList = baggageInfoList;
		if (CollectionUtils.isNotEmpty(baggageInfoList)
				&& segment.getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			BaggageInfo baggageInfo = segment.getPriceInfo(0).getFareDetail(PaxType.ADULT).getBaggageInfo();
			if (baggageInfo != null && StringUtils.isNotEmpty(baggageInfo.getAllowance())) {
				String allowance = baggageInfo.getAllowance().replaceAll("[^0-9]", "");
				Integer baggageSize = Integer.valueOf(allowance);
				List<String> allowedAdditionalBaggage = getAllowedBagagge(allowance, baggageSize);
				if (airline.getBaggageType().containsValue(allowance)) {
					airline.getBaggageType().forEach((type, value) -> {
						BaggageSSRInformation ssrInformation = getFilteredBaggage(finalBaggageInfoList, type);
						if (airline.getSourceId() == AirSourceType.FLYDUBAI.getSourceId()) {
							if (ssrInformation != null && allowedAdditionalBaggage != null
									&& allowedAdditionalBaggage.contains(ssrInformation.getCode())) {
								if (StringUtils.isNotBlank(value)) {
									if (Integer.valueOf(value) < baggageSize) {
										finalBaggageInfoList.remove(ssrInformation);
									}
								}
							} else {
								finalBaggageInfoList.remove(ssrInformation);
							}
						}
					});
				}
			}
			return finalBaggageInfoList;
		} else {
			return null;
		}
	}

	private List<String> getAllowedBagagge(String allowance, Integer baggageSize) {
		AtomicReference<List<String>> allowedBag = new AtomicReference<>(new ArrayList<>());
		airline.allowedBaggage().forEach((key, value) -> {
			List<String> bagagge = value.getOrDefault(String.valueOf(baggageSize), null);
			if (CollectionUtils.isNotEmpty(bagagge)) {
				allowedBag.set(Arrays.asList(key));
			}
		});
		return allowedBag.get();
	}

	/**
	 * @implSpec : Reason to keep as First Segment Info is , As Trip wise Fare Everything includes for Connecting
	 *           segment as well
	 */
	private List<MealSSRInformation> preCheckWithMeal(SegmentInfo segment, List<MealSSRInformation> ssrInformations) {
		if (segment.getPriceInfo(0).getFareDetail(PaxType.ADULT) != null) {
			FareDetail fareDetail = segment.getPriceInfo(0).getFareDetail(PaxType.ADULT);
			if (BooleanUtils.isTrue(fareDetail.getIsMealIncluded())) {
				return null;
			} else {
				return ssrInformations;
			}
		}
		return null;
	}


	public BaggageSSRInformation getFilteredBaggage(List<BaggageSSRInformation> baggageInfoList, String code) {
		Optional<BaggageSSRInformation> ssrInfo = Optional.ofNullable(
				baggageInfoList.stream().filter(baggage -> baggage.getCode().equals(code)).findFirst().orElse(null));
		if (ssrInfo.isPresent()) {
			return ssrInfo.get();
		}
		return null;
	}

	private void setBaggageSSR(ViewServiceQuote serviceQuote, List<BaggageSSRInformation> baggageInfoList) {
		BaggageSSRInformation baggageSSRInfo = new BaggageSSRInformation();
		baggageSSRInfo.setCode(serviceQuote.getSSRCode());
		baggageSSRInfo.setDesc(serviceQuote.getDescription());
		baggageSSRInfo.setAmount(serviceQuote.getAmount().doubleValue());
		baggageSSRInfo.setQuantity(serviceQuote.getQuantityAvailable());
		baggageInfoList.add(baggageSSRInfo);
	}

	private void setMealSSR(ViewServiceQuote serviceQuote, List<MealSSRInformation> mealInfoList) {
		MealSSRInformation mealSSRInfo = new MealSSRInformation();
		mealSSRInfo.setCode(serviceQuote.getSSRCode());
		mealSSRInfo.setDesc(serviceQuote.getDescription());
		mealSSRInfo.setAmount(serviceQuote.getAmount().doubleValue());
		mealInfoList.add(mealSSRInfo);
	}

	public void getSeatMap(TripSeatMap tripSeatMap, TripInfo tripInfo) {
		org.tempuri.RetrieveSeatAvailabilityList seatRequest = new org.tempuri.RetrieveSeatAvailabilityList();
		ConnectPoint_FlightStub flightStub = bindingService.getFlightStub(configuration);
		try {
			RetrieveSeatAvailabilityList request = getSeatMapRequest(tripInfo);
			seatRequest.setRetrieveSeatAvailabilityListRequest(request);
			listener.setType("R-Seat");
			flightStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			RetrieveSeatAvailabilityListResponse seatMapResponse = flightStub.retrieveSeatAvailabilityList(seatRequest);
			if (!isCriticalException(seatMapResponse.getRetrieveSeatAvailabilityListResult().getExceptions())
					&& seatMapResponse.getRetrieveSeatAvailabilityListResult().getPhysicalFlights() != null
					&& seatMapResponse.getRetrieveSeatAvailabilityListResult().getPhysicalFlights()
							.getPhysicalFlight() != null) {
				parseSeatMapResponse(seatMapResponse, tripSeatMap, tripInfo);
			}
		} catch (RemoteException se) {
			throw new SupplierRemoteException(se);
		} finally {
			flightStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private void parseSeatMapResponse(RetrieveSeatAvailabilityListResponse seatMapResponse, TripSeatMap tripSeatMap,
			TripInfo tripInfo) {
		PhysicalFlight[] physicalFlights =
				seatMapResponse.getRetrieveSeatAvailabilityListResult().getPhysicalFlights().getPhysicalFlight();
		for (int segmentIndex = 0; segmentIndex < physicalFlights.length; segmentIndex++) {
			PhysicalFlight physicalFlight = physicalFlights[segmentIndex];
			if (physicalFlight.getCabins() != null) {
				Cabin cabin = getCabin(physicalFlight.getCabins(), tripInfo.getCabinClass());
				SeatInformation seatInfo = null;
				if (cabin != null && cabin.getSeatMaps() != null) {
					seatInfo = SeatInformation.builder().build();
					List<SeatSSRInformation> seatSSRInfoList = new ArrayList<>();
					List<String> assignedSeats = getAssignedSeats(cabin.getSeatAssignments());
					for (SeatMap seatMap : cabin.getSeatMaps().getSeatMap()) {
						String seats = seatMap.getSeats();
						for (int index = 0; index < seats.length(); index++) {
							char seat = seats.charAt(index);
							if (seat != ' ') {
								String seatCode = StringUtils.join(seatMap.getRowNumber(), seat);

								SeatSSRInformation seatSSRInfo = new SeatSSRInformation();
								seatSSRInfo.setSeatNo(seatCode.trim());
								seatSSRInfo.setCode(seatCode.trim());
								SeatPosition seatPosition =
										SeatPosition.builder().row((int) seatMap.getRowNumber()).column(seat).build();
								seatSSRInfo.setSeatPosition(seatPosition);
								seatSSRInfo.setIsAisle(getIsAsile(seat, index, seats));
								seatSSRInfo.setIsBooked((seatMap.getBlockedSeats() != null
										&& seatMap.getBlockedSeats().contains(String.valueOf(seat)))
										|| (CollectionUtils.isNotEmpty(assignedSeats)
												&& assignedSeats.contains(seatCode)));
								seatSSRInfoList.add(seatSSRInfo);

							}
						}
					}
					if (CollectionUtils.isNotEmpty(seatSSRInfoList)) {
						seatInfo.setSeatsInfo(seatSSRInfoList);
						setSSRSeatCharges(seatSSRInfoList, cabin.getSeatCharges());
						tripSeatMap.getTripSeat().put(tripInfo.getSegmentInfos().get(segmentIndex).getId(), seatInfo);
					}
				}
			}
		}

	}

	private List<String> getAssignedSeats(ArrayOfSeatAssignment arrayOfSeatAssignment) {
		List<String> assignedSeats = new ArrayList<>();
		if (arrayOfSeatAssignment != null && ArrayUtils.isNotEmpty(arrayOfSeatAssignment.getSeatAssignment())) {
			for (SeatAssignment seatAssignment : arrayOfSeatAssignment.getSeatAssignment()) {
				if (StringUtils.isNotEmpty(seatAssignment.getSeat().trim())) {
					assignedSeats.add(StringUtils.join(seatAssignment.getRowNumber(), seatAssignment.getSeat().trim()));
				}
			}
		}
		return assignedSeats;
	}

	private Boolean getIsAsile(char seat, int index, String seats) {
		boolean isAsile = false;
		if (index < seats.length() - 1) {
			isAsile = seats.charAt(index + 1) == ' ';
		}
		if (!isAsile && index > 0 && seats.length() > 0 && index < seats.length() - 1) {
			isAsile = seats.charAt(index - 1) == ' ';
		}
		return isAsile;
	}

	private Cabin getCabin(ArrayOfCabin arrayOfCabin, CabinClass cabinClass) {
		if (arrayOfCabin != null && arrayOfCabin.getCabin() != null) {
			for (Cabin cabin : arrayOfCabin.getCabin()) {
				if (cabin.getCabinName().equalsIgnoreCase(cabinClass.getName())) {
					return cabin;
				}
			}
		}
		return null;
	}

	private void setSSRSeatCharges(List<SeatSSRInformation> seatSSRInfoList, ArrayOfSeatCharge arrayOfSeatCharges) {
		Map<String, SeatCharge> seatChargeMap = getSeatChargeMap(arrayOfSeatCharges);
		for (SeatSSRInformation seatSSRInfo : seatSSRInfoList) {
			SeatCharge seatCharge = seatChargeMap.get(seatSSRInfo.getSeatNo());
			if (seatCharge != null) {
				seatSSRInfo.setAmount(seatCharge.getAmount().doubleValue());
				if (StringUtils.isNotBlank(seatCharge.getServiceCode())) {
					seatSSRInfo.getMiscInfo().setSeatCodeType(seatCharge.getServiceCode().replaceAll(" ", ""));
				}
			} else {
				// If no seat charge & desc then seat cannot be booked.
				seatSSRInfo.setIsBooked(true);
			}
		}
	}

	private Map<String, SeatCharge> getSeatChargeMap(ArrayOfSeatCharge arrayOfSeatCharges) {
		Map<String, SeatCharge> seatChargeMap = new HashMap<>();
		if (arrayOfSeatCharges != null && arrayOfSeatCharges.getSeatCharge() != null
				&& arrayOfSeatCharges.getSeatCharge().length > 0) {
			for (SeatCharge seatCharge : arrayOfSeatCharges.getSeatCharge()) {
				seatChargeMap.put(StringUtils.join(seatCharge.getRowNumber(), seatCharge.getSeat().trim()), seatCharge);
			}
		}
		return seatChargeMap;
	}

	private RetrieveSeatAvailabilityList getSeatMapRequest(TripInfo tripInfo) {
		RetrieveSeatAvailabilityList seatAvailabilityList = new RetrieveSeatAvailabilityList();
		seatAvailabilityList.setSecurityGUID(binaryToken);
		seatAvailabilityList.setCarrierCodes(getCarrierCode());
		seatAvailabilityList.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
		seatAvailabilityList.setCabinName(getCabinClass(tripInfo));
		seatAvailabilityList.setLogicalFlightID(
				tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getLogicalFlightId().intValue());
		seatAvailabilityList
				.setDepartureDate(TgsDateUtils.localDateToCalendar(tripInfo.getDepartureTime().toLocalDate()));
		seatAvailabilityList.setCurrency(airline.getCurrencyCode(configuration.getSupplierCredential()).getValue());
		seatAvailabilityList.setFareBasisCode(getFareBasisCode(tripInfo));
		return seatAvailabilityList;
	}
}
