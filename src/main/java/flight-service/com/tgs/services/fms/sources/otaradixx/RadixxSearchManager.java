package com.tgs.services.fms.sources.otaradixx;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request.EnumsFareGroupMethodType;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request.EnumsInventoryFilterMethodType;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_farequote.ArrayOfFareQuoteDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_farequote.ArrayOfFareQuoteRequestInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_farequote.FareQuoteDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_farequote.FareQuoteRequestInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request_farequote.RetrieveFareQuote;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.ApplicableTaxDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.FareInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.FareType;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.FlightLegDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.FlightSegment;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.LegDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.SegmentDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.TaxDetail;
import org.tempuri.ConnectPoint_PricingStub;
import org.tempuri.RetrieveFareQuoteResponse;
import com.google.common.base.Enums;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NotOperatingSectorException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class RadixxSearchManager extends RadixxServiceManager {

	private static final String NO_OPERATING_CARRIER = "is not established for this carrier";
	private static final String NO_OPERATING_SOURCE = "Error Generation Source Location";


	/**
	 * @return AirSearchResult
	 * @implSpec : This Method is will search availabilities based on searchquery
	 * @implNote :1 : Incase of flights or fare is empty it will throw NoSearchResultException
	 */
	public AirSearchResult doSearch()
			throws NotOperatingSectorException, NoSearchResultException, SupplierRemoteException {
		ConnectPoint_PricingStub pricingStub = null;
		AirSearchResult searchResult = null;
		try {
			pricingStub = bindingService.getPricingStub(configuration);
			RetrieveFareQuote fareQuote = new RetrieveFareQuote();
			fareQuote.setSecurityGUID(binaryToken);
			fareQuote.setCarrierCodes(getCarrierCode());
			fareQuote.setCurrencyOfFareQuote(airline.getCurrencyCode(configuration.getSupplierCredential()));
			fareQuote.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
			fareQuote.setIataNumberOfRequestor(configuration.getSupplierCredential().getIataNumber());
			fareQuote.setCorporationID(RadixxConstantsInfo.CORPORATIONID);
			fareQuote.setFareFilterMethod(RadixxUtils.getFareFilterMethod(searchQuery));
			fareQuote.setFareGroupMethod(EnumsFareGroupMethodType.WebFareTypes);
			fareQuote.setInventoryFilterMethod(EnumsInventoryFilterMethodType.Available);
			fareQuote.setFareQuoteDetails(getFareQuoteDetail());
			org.tempuri.RetrieveFareQuote fareQuoteReq = new org.tempuri.RetrieveFareQuote();
			fareQuoteReq.setRetrieveFareQuoteRequest(fareQuote);
			listener.setType(AirUtils.getLogType("4-S-FlightSearch", configuration));
			pricingStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			RetrieveFareQuoteResponse pricingResponse = pricingStub.retrieveFareQuote(fareQuoteReq);
			if (!isCriticalException(pricingResponse.getRetrieveFareQuoteResult().getExceptions())) {
				searchResult = parsePricingResponse(pricingResponse);
			} else {
				throw new NoSearchResultException(AirSourceConstants.NO_SCHEDULE_FOUND);
			}
		} catch (AxisFault e) {
			if (e.getMessage() != null && isNotOperatingSector(e.getMessage())) {
				throw new NotOperatingSectorException(e.getMessage());
			} else if (e.getMessage() != null && e.getMessage().toLowerCase().contains("session")) {
				throw new SupplierSessionException(e.getMessage());
			} else {
				throw new NoSearchResultException(e.getMessage());
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			pricingStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_PricingStub.class, pricingStub);
		}
		return searchResult;
	}

	private boolean isNotOperatingSector(String message) {
		return StringUtils.isNotBlank(message) && message.toLowerCase().contains(NO_OPERATING_CARRIER.toLowerCase())
				&& message.toLowerCase().contains(NO_OPERATING_SOURCE);
	}

	/*
	 * @param pricingResponse - flight availabilities
	 * 
	 * @return AirSearchResult
	 */
	private AirSearchResult parsePricingResponse(RetrieveFareQuoteResponse pricingResponse) {
		AirSearchResult searchResult = new AirSearchResult();
		List<TripInfo> tripInfos = getTripInfos(pricingResponse);
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			searchResult.setTripInfos(splitSROnSearchQuery(tripInfos));
			// Set Empty result incase on intl return
			if (searchQuery.isIntlReturn() && (searchResult.getTripInfos().get(TripInfoType.ONWARD.name()) == null
					|| searchResult.getTripInfos().get(TripInfoType.RETURN.name()) == null)) {
				searchResult = new AirSearchResult();
			}
		}
		return searchResult;
	}

	/**
	 * @param tripInfoList - Journey trips This will split based on searchquery to TripType
	 */
	private Map<String, List<TripInfo>> splitSROnSearchQuery(List<TripInfo> tripInfoList) {
		Map<String, List<TripInfo>> tripInfos = new HashMap<>();
		List<TripInfo> onwardList = new ArrayList<>();
		List<TripInfo> returnList = new ArrayList<>();
		tripInfoList.forEach(trip -> {
			String departureCode = trip.getDepartureAirportCode();
			if (departureCode.equals(searchQuery.getRouteInfos().get(0).getFromCityOrAirport().getCode())
					|| AirUtils.isNearByAirport(departureCode, sourceId, searchQuery, sourceConfiguration,
							SystemContextHolder.getContextData().getUser())) {
				onwardList.add(trip);
			} else {
				List<SegmentInfo> segmentInfoList = new ArrayList<>();
				segmentInfoList = trip.getSegmentInfos();
				segmentInfoList.forEach(segmentInfo -> {
					segmentInfo.setIsReturnSegment(true);
				});
				returnList.add(trip);
			}
		});

		setTripInfo(tripInfos, onwardList, TripInfoType.ONWARD.name());
		setTripInfo(tripInfos, returnList, TripInfoType.RETURN.name());
		return tripInfos;
	}

	private void retainPriceOptions(List<TripInfo> tripInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<PriceInfo> priceInfos = new ArrayList<>();
				List<String> allowedFaretypes = new ArrayList<>();
				segmentInfo.getPriceInfoList().forEach(priceInfo -> {
					if (!allowedFaretypes.contains(priceInfo.getFareType())) {
						priceInfos.add(priceInfo);
						allowedFaretypes.add(priceInfo.getFareType());
					}
				});
				segmentInfo.setPriceInfoList(priceInfos);
			});
		});
	}


	private void setTripInfo(Map<String, List<TripInfo>> tripInfos, List<TripInfo> tripInfoList, String tripType) {
		if (CollectionUtils.isNotEmpty(tripInfoList)) {
			tripInfos.put(tripType, tripInfoList);
		}
	}

	private List<TripInfo> getTripInfos(RetrieveFareQuoteResponse pricingResponse) throws NoSearchResultException {
		List<TripInfo> tripInfos = new ArrayList<>();
		FlightSegment[] flightSegmentArray = null;
		SegmentDetail[] segmentDetailArray = null;
		LegDetail[] legDetailArray = null;
		TaxDetail[] taxdetails = null;

		if (Objects.nonNull(pricingResponse.getRetrieveFareQuoteResult().getFlightSegments())
				&& Objects.nonNull(pricingResponse.getRetrieveFareQuoteResult().getSegmentDetails())
				&& Objects.nonNull(pricingResponse.getRetrieveFareQuoteResult().getLegDetails())
				&& ArrayUtils.isNotEmpty(
						pricingResponse.getRetrieveFareQuoteResult().getFlightSegments().getFlightSegment())) {
			flightSegmentArray = pricingResponse.getRetrieveFareQuoteResult().getFlightSegments().getFlightSegment();
			segmentDetailArray = pricingResponse.getRetrieveFareQuoteResult().getSegmentDetails().getSegmentDetail();
			legDetailArray = pricingResponse.getRetrieveFareQuoteResult().getLegDetails().getLegDetail();
			taxdetails = pricingResponse.getRetrieveFareQuoteResult().getTaxDetails().getTaxDetail();
		} else {
			throw new NoSearchResultException(AirSourceConstants.NO_SCHEDULE_FOUND);
		}

		if (ArrayUtils.isNotEmpty(flightSegmentArray) && ArrayUtils.isNotEmpty(segmentDetailArray)) {
			for (SegmentDetail segmentDetail : segmentDetailArray) {
				FlightSegment flightSegment = getFlightSegment(segmentDetail.getLFID(), flightSegmentArray);
				if (flightSegment != null) {
					try {
						List<SegmentInfo> segmentInfos = getSegmentInfos(flightSegment, segmentDetail, legDetailArray);
						TripInfo tripInfo = new TripInfo();
						Map<SSRType, List<? extends SSRInformation>> preSSR = AirUtils.getPreSSR(
								configuration.getBasicInfo(), segmentInfos.get(0).getAirlineCode(false), bookingUser);
						List<PriceInfo> priceInfos =
								getPriceInfos(flightSegment, flightSegment.getFareTypes().getFareType(), taxdetails,
										segmentInfos, flightSegment.getLFID(), preSSR);
						segmentInfos.get(0).setPriceInfoList(priceInfos);
						if (segmentInfos.size() > 1) {
							copyStaticBaggageInfo(segmentInfos, priceInfos);
						}
						tripInfo.setSegmentInfos(segmentInfos);
						tripInfo.setConnectionTime();
						tripInfos.add(tripInfo);
					} catch (Exception e) {
						log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
					}
				}
			}
		}
		return tripInfos;
	}

	private void copyStaticBaggageInfo(List<SegmentInfo> segmentInfos, List<PriceInfo> priceInfos) {

		for (int index = 1; index < segmentInfos.size(); index++) {
			SegmentInfo segmentInfo = segmentInfos.get(index);
			priceInfos.forEach(priceInfo -> {
				segmentInfo.getPriceInfoList().forEach(priceInfo1 -> {
					if (priceInfo1 != null && priceInfo1.getFareIdentifier().equals(priceInfo.getFareIdentifier())) {
						priceInfo1.getFareDetails().forEach(((paxType, fareDetail) -> {
							fareDetail.setBaggageInfo(priceInfo.getFareDetail(paxType).getBaggageInfo());
						}));
					}
				});
			});
		}

	}

	private List<SegmentInfo> getSegmentInfos(FlightSegment flightSegment, SegmentDetail segmentDetail,
			LegDetail[] legDetailsArray) {
		List<SegmentInfo> segmentInfoList = new ArrayList<>();
		Integer segmentIndex = 0;
		FlightLegDetail[] flightLegDetailArray = flightSegment.getFlightLegDetails().getFlightLegDetail();
		for (FlightLegDetail flightLegDetail : flightLegDetailArray) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentIndex);
			LegDetail legDetail = getLegDetail(legDetailsArray, flightLegDetail.getPFID());

			segmentInfo.setFlightDesignator(RadixxUtils.getFlightDesignator(legDetail));
			AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(segmentDetail.getOperatingCarrier().trim());
			if (operatingAirline != null
					&& !operatingAirline.getCode().equals(segmentInfo.getFlightDesignator().getAirlineCode())) {
				segmentInfo.setOperatedByAirlineInfo(operatingAirline);
			}
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(legDetail.getOrigin()));
			segmentInfo.setDepartTime(TgsDateUtils.calenderToLocalDateTime(legDetail.getDepartureDate()));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(legDetail.getDestination()));
			segmentInfo.setArrivalTime(TgsDateUtils.calenderToLocalDateTime(legDetail.getArrivalDate()));

			RadixxUtils.setTerminalInfo(segmentInfo, legDetail);

			segmentInfo.setDuration(segmentInfo.calculateDuration());
			segmentInfo.setIsReturnSegment(Boolean.FALSE);
			segmentInfo.setPriceInfoList(initializePriceInfo(flightSegment.getFareTypes().getFareType(),
					flightSegment.getLFID(), String.valueOf(flightLegDetail.getPFID())));

			segmentInfoList.add(segmentInfo);
			segmentIndex++;
		}

		return segmentInfoList;
	}

	private List<PriceInfo> initializePriceInfo(FareType[] fareTypeArray, int lfId, String pfId) {
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (FareType fareType : fareTypeArray) {
			Map<PaxType, FareDetail> fareDetails = new HashMap<>();
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
			FareInfo[] fareInfos = fareType.getFareInfos().getFareInfo();
			for (FareInfo fareInfo : fareInfos) {
				FareDetail fareDetail = getZeroFareDetail(fareInfo);
				fareDetails.put(RadixxPaxType.getPaxType(fareInfo.getPTCID()), fareDetail);
				priceInfo.setFareDetails(fareDetails);
				RadixxPriceInfo radixxPriceInfo = RadixxPriceInfo.getOTARadixxPriceInfo(fareType.getFareTypeName());
				radixxPriceInfo.processPriceInfo(priceInfo);
			}
			priceInfo.getMiscInfo().setLogicalFlightId(Long.valueOf(lfId));
			priceInfo.getMiscInfo().setJourneyKey(pfId);
			priceInfoList.add(priceInfo);
		}
		return priceInfoList;
	}

	private List<PriceInfo> getPriceInfos(FlightSegment flightSegment, FareType[] fareTypeArray, TaxDetail[] taxDetails,
			List<SegmentInfo> segmentInfos, int lFId, Map<SSRType, List<? extends SSRInformation>> preSSR) {
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (int fareIndex = 0; fareIndex < fareTypeArray.length; fareIndex++) {
			FareType fareType = fareTypeArray[fareIndex];
			Map<PaxType, FareDetail> fareDetails = new HashMap<>();
			PriceInfo priceInfo = segmentInfos.get(0).getPriceInfo(fareIndex);
			FareInfo[] fareInfos = fareType.getFareInfos().getFareInfo();
			for (FareInfo fareInfo : fareInfos) {
				PaxType paxType = RadixxPaxType.getPaxType(fareInfo.getPTCID());
				String classOfBook = fareType.getFareTypeName();
				FareDetail fareDetail = getFareDetail(fareInfo, taxDetails, paxType, classOfBook, preSSR);
				fareDetails.put(paxType, fareDetail);
				priceInfo.setFareDetails(fareDetails);
				RadixxPriceInfo radixxPriceInfo = RadixxPriceInfo.getOTARadixxPriceInfo(fareType.getFareTypeName());
				radixxPriceInfo.processPriceInfo(priceInfo);
				if (paxType.equals(PaxType.ADULT)) {
					priceInfo.getMiscInfo().setFareTypeId(fareInfo.getFareID());
				}
			}
			priceInfoList.add(priceInfo);
		}
		return priceInfoList;
	}

	/**
	 * @implSpec : This is to handle Agency Commission
	 */
	private FareDetail getZeroFareDetail(FareInfo fareInfo) {
		FareDetail fares = new FareDetail();
		Map<FareComponent, Double> fareComponents = new HashMap<>();
		fares.setCabinClass(Enums.getIfPresent(CabinClass.class, fareInfo.getCabin()).or(CabinClass.ECONOMY));
		fareComponents.put(FareComponent.BF, 0.0);
		fareComponents.put(FareComponent.AT, 0.0);
		fareComponents.put(FareComponent.YQ, 0.0);
		fareComponents.put(FareComponent.TF, 0.0);
		fares.setFareComponents(fareComponents);
		return fares;
	}

	private FareDetail getFareDetail(FareInfo fareInfo, TaxDetail[] taxDetails, PaxType paxType, String classOfBook,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {
		FareDetail fare = new FareDetail();
		fare.setCabinClass(Enums.getIfPresent(CabinClass.class, fareInfo.getCabin()).or(CabinClass.ECONOMY));
		fare.setSeatRemaining(fareInfo.getSeatsAvailable());
		fare.setClassOfBooking(classOfBook);
		fare.setFareBasis(fareInfo.getFBCode());
		fare.getFareComponents().put(FareComponent.BF, fareInfo.getBaseFareAmtNoTaxes().doubleValue());
		fare.getFareComponents().put(FareComponent.TF, fareInfo.getBaseFareAmtInclTax().doubleValue());
		/**
		 * @implSpec : Reasong to comment is Supplier COmmission Wont be dedeucted from Customer ,And While paying also
		 *           Radixx Structure will deduct Whole Amount and reverse commission Make More Discussion While making
		 *           Changes (Depends on Booking Also)
		 */
		// fare.getFareComponents().put(FareComponent.SC, agencyCommission);
		setTaxDetails(fare, fareInfo, taxDetails, paxType, preSSR);
		return fare;
	}

	public FareDetail setTaxDetails(FareDetail fareDetail, FareInfo fareInfo, TaxDetail[] taxDetail, PaxType paxType,
			Map<SSRType, List<? extends SSRInformation>> preSSR) {
		if (Objects.nonNull(fareInfo.getApplicableTaxDetails())
				&& ArrayUtils.isNotEmpty(fareInfo.getApplicableTaxDetails().getApplicableTaxDetail())) {
			ApplicableTaxDetail[] taxDetails = fareInfo.getApplicableTaxDetails().getApplicableTaxDetail();
			Arrays.stream(taxDetails).forEach(tax -> {
				setTaxDetail(fareDetail, tax, taxDetail, paxType, preSSR);
			});
		}
		return fareDetail;
	}

	private FareDetail setTaxDetail(FareDetail fareDetail, ApplicableTaxDetail applicableTaxDetail,
			TaxDetail[] taxDetails, PaxType paxType, Map<SSRType, List<? extends SSRInformation>> preSSR) {
		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
		Arrays.stream(taxDetails).forEach(taxDetail -> {
			if (Objects.nonNull(taxDetail) && Objects.nonNull(applicableTaxDetail)
					&& taxDetail.getTaxID() == applicableTaxDetail.getTaxID()) {
				double amount = applicableTaxDetail.getAmt().doubleValue();

				if (taxDetail.getTaxCode().contains(RadixxConstantsInfo.GST)) {
					double previousGST = getPreviousFare(fareComponents, FareComponent.AGST);
					fareComponents.put(FareComponent.AGST, amount + previousGST);
				} else if (taxDetail.getCodeType().contains(RadixxConstantsInfo.FUELCODE)) {
					double previousTax = getPreviousFare(fareComponents, FareComponent.YQ);
					fareComponents.put(FareComponent.YQ, amount + previousTax);
				} else {
					double previousTax = getPreviousFare(fareComponents, FareComponent.AT);
					fareComponents.put(FareComponent.AT, amount + previousTax);
				}

				if (StringUtils.isNotEmpty(taxDetail.getTaxDesc())) {
					if (airline.getBaggageType().containsKey(taxDetail.getTaxCode())
							|| taxDetail.getTaxDesc().toUpperCase().contains("KG")
							|| taxDetail.getTaxDesc().toUpperCase().contains("BAGGAGE")) {
						fareDetail.setBaggageInfo(getBagaggeInfo(taxDetail, paxType));
					} else if (RadixxUtils.containsSSRCode(taxDetail.getTaxCode(), preSSR, true, false)
							|| taxDetail.getTaxDesc().toUpperCase().contains("MEAL")) {
						fareDetail.setIsMealIncluded(true);
					}
				}

			}
		});
		fareDetail.setFareComponents(fareComponents);
		return fareDetail;
	}

	private BaggageInfo getBagaggeInfo(TaxDetail taxDetail, PaxType paxType) {
		BaggageInfo baggageInfo = new BaggageInfo();
		String baggageDesc = taxDetail.getTaxDesc().toUpperCase().replaceAll("\\s+", "");
		if (baggageDesc.contains(RadixxConstantsInfo.FBA30KG) && baggageDesc.contains(paxType.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_30);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA20KG) && baggageDesc.contains(paxType.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_20);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA25KG) && baggageDesc.contains(paxType.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_25);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA40KG) && baggageDesc.contains(paxType.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_40);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA10KG) && baggageDesc.contains(paxType.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_10);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA30KG) && !baggageDesc.contains(PaxType.INFANT.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_30);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA40KG) && !baggageDesc.contains(PaxType.INFANT.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_40);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA20KG) && !baggageDesc.contains(PaxType.INFANT.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_20);
		} else if (baggageDesc.contains(RadixxConstantsInfo.FBA25KG) && !baggageDesc.contains(PaxType.INFANT.name())) {
			baggageInfo.setAllowance(RadixxConstantsInfo.BAGB_25);
		}
		return baggageInfo;
	}

	public static double getPreviousFare(Map<FareComponent, Double> fareComponents, FareComponent fc) {
		return fareComponents.getOrDefault(fc, 0.0).doubleValue();
	}

	private ArrayOfFareQuoteDetail getFareQuoteDetail() {
		ArrayOfFareQuoteDetail arrayOfFareQuoteDetail = new ArrayOfFareQuoteDetail();
		searchQuery.getRouteInfos().forEach(routeInfo -> {
			FareQuoteDetail fareQuoteDetail = new FareQuoteDetail();
			fareQuoteDetail.setOrigin(getNearByAirport(routeInfo.getFromCityOrAirport().getCode(), bookingUser));
			fareQuoteDetail.setDestination(getNearByAirport(routeInfo.getToCityOrAirport().getCode(), bookingUser));
			fareQuoteDetail.setUseAirportsNotMetroGroups(false);
			if (airline.isZoneTimeRequired()) {
				fareQuoteDetail
						.setDateOfDeparture(TgsDateUtils.localDateToCalendarWithCurrentZone(routeInfo.getTravelDate()));
			} else {
				fareQuoteDetail.setDateOfDeparture(TgsDateUtils.localDateToCalendar(routeInfo.getTravelDate()));
			}
			fareQuoteDetail.setFareTypeCategory(RadixxConstantsInfo.FARECATEGORYID);
			fareQuoteDetail.setMarketingCarrierCode(carrierCode);
			fareQuoteDetail.setOperatingCarrierCode(carrierCode);
			fareQuoteDetail.setLFID(RadixxConstantsInfo.SEARCHLFID);
			fareQuoteDetail.setLanguageCode(RadixxConstantsInfo.LANUGUAGECODE);
			fareQuoteDetail.setTicketPackageID(RadixxConstantsInfo.TICKETPACKAGEID);
			fareQuoteDetail.setFareQuoteRequestInfos(getFareQuoteRequestInfo(searchQuery.getPaxInfo()));
			arrayOfFareQuoteDetail.addFareQuoteDetail(fareQuoteDetail);
		});
		return arrayOfFareQuoteDetail;
	}

	private ArrayOfFareQuoteRequestInfo getFareQuoteRequestInfo(Map<PaxType, Integer> paxInfo) {
		ArrayOfFareQuoteRequestInfo arrayOfFareQuoteRequest = new ArrayOfFareQuoteRequestInfo();
		paxInfo.forEach((paxType, seatsCount) -> {
			if (seatsCount > 0) {
				arrayOfFareQuoteRequest.addFareQuoteRequestInfo(getPassengerTypeId(paxType, seatsCount));
			}
		});
		return arrayOfFareQuoteRequest;
	}

	private FareQuoteRequestInfo getPassengerTypeId(PaxType paxType, Integer seatsCount) {
		FareQuoteRequestInfo requestInfo = new FareQuoteRequestInfo();
		requestInfo.setPassengerTypeID(RadixxPaxType.getPaxId(paxType));
		requestInfo.setTotalSeatsRequired(seatsCount);
		return requestInfo;
	}

	private LegDetail getLegDetail(LegDetail[] legDetailArray, int pfId) {
		for (LegDetail legDetail : legDetailArray) {
			if (legDetail.getPFID() == pfId) {
				return legDetail;
			}
		}
		return null;
	}

	private FlightSegment getFlightSegment(int lfid, FlightSegment[] flightSegmentArray) {
		for (FlightSegment flightSegment : flightSegmentArray) {
			if (lfid == flightSegment.getLFID()) {
				return flightSegment;
			}
		}
		return null;
	}
}
