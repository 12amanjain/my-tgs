package com.tgs.services.fms.sources.otaradixx;


import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_exceptions.ExceptionInformationExceptions;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.ArrayOfCarrierCode;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.CarrierCode;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.dbmodel.DbSupplierSession;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.tempuri.RetrievePNRResponse;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.zip.GZIPInputStream;

@Slf4j
@Getter
@Setter
@SuperBuilder
abstract class RadixxServiceManager {

	protected AirSearchQuery searchQuery;

	protected SupplierConfiguration configuration;

	protected double agencyCommission;

	protected RadixxAirline airline;

	protected DbSupplierSession session;

	protected static String CRITICALERROR = "CRITICAL";

	protected String binaryToken;

	protected RadixxBindingService bindingService;

	protected String bookingId;

	protected String searchId;

	protected Integer sourceId;

	protected String carrierCode;

	protected DeliveryInfo deliveryInfo;

	protected ClientGeneralInfo companyInfo;

	protected SoapRequestResponseListner listener;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected static String SYSTEM_GEN = ": SYSTEM GENERATED";

	protected List<String> criticalMessageLogger;

	protected Calendar holdTimeLimit;

	protected User bookingUser;

	public void init() {
		AirSourceConfigurationOutput airSourceOutput =
				AirUtils.getAirSourceConfiguration(searchQuery, configuration.getBasicInfo(), bookingUser);
		this.carrierCode = airSourceOutput.getIncludedAirlines().stream().findFirst().get();
		this.sourceId = configuration.getBasicInfo().getSourceId();
		this.companyInfo = ServiceCommunicatorHelper.getClientInfo();
		if (searchQuery != null) {
			this.searchId = searchQuery.getSearchId();
		}
	}


	public ArrayOfCarrierCode getCarrierCode() {
		ArrayOfCarrierCode arrayOfCarrierCode = new ArrayOfCarrierCode();
		CarrierCode carrier = new CarrierCode();
		carrier.setAccessibleCarrierCode(carrierCode);
		arrayOfCarrierCode.addCarrierCode(carrier);
		return arrayOfCarrierCode;
	}

	public boolean isCriticalException(ExceptionInformationExceptions exceptions) {
		AtomicBoolean isError = new AtomicBoolean();
		StringJoiner message = new StringJoiner("");
		if (exceptions != null && ArrayUtils.isNotEmpty(exceptions.getExceptionInformationException())) {
			Arrays.stream(exceptions.getExceptionInformationException()).forEach(excp -> {
				if (excp.getExceptionLevel().getValue().toUpperCase().equals(CRITICALERROR)) {
					log.info("Exception Occured on Airline {} for {} Source - {} Code {} Description -{} ", carrierCode,
							getRequestId(), excp.getExceptionSource(), excp.getExceptionCode(),
							excp.getExceptionDescription());
					message.add(excp.getExceptionSource() + " " + excp.getExceptionDescription());
					isError.set(true);
				}
			});
		}
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message.toString())) {
			criticalMessageLogger.add(message.toString());
		}
		return isError.get();
	}

	public String getResponseByRequest(String requestXML, String methodName, String soapAction, String requestURL)
			throws Exception {
		String sbStr = null;
		int numTry = 0;
		URLConnection urlCon = null;
		boolean doRetry = false;
		if (StringUtils.isNotEmpty(requestXML)) {
			do {
				try {
					URL url = new URL(requestURL);
					urlCon = url.openConnection();
					urlCon.setDoInput(true);
					urlCon.setDoOutput(true);
					urlCon.setUseCaches(false);
					urlCon.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
					urlCon.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1");
					urlCon.setRequestProperty("Accept-Encoding", "gzip,deflate");
					urlCon.setRequestProperty("SOAPAction", soapAction);
					urlCon.setRequestProperty("Host", bindingService.getApiUrlRule().getHost());
					HttpURLConnection httpUrlCon = (HttpURLConnection) url.openConnection();
					httpUrlCon.setRequestMethod(HttpUtils.REQ_METHOD_POST);

					DataOutputStream printout = new DataOutputStream(urlCon.getOutputStream());
					printout.writeBytes(requestXML);
					printout.flush();
					printout.close();

					InputStream rawInStream = urlCon.getInputStream();
					InputStream dataInput = null;
					log.info("Method Name {} for booking {}", methodName, bookingId);

					urlCon.getHeaderFields().forEach((key, value) -> {
						List<String> header = value;
						header.forEach(v -> {
							// This is just to confirm all headers are fine if testing required
							log.info(" {}", key + " : " + v);
						});
					});


					String encoding = urlCon.getContentEncoding();
					if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
						dataInput = new GZIPInputStream(new BufferedInputStream(rawInStream));
					} else {
						dataInput = new BufferedInputStream(rawInStream);
					}
					StringBuffer sb = new StringBuffer();
					int rc;
					while ((rc = dataInput.read()) != -1) {
						sb.append((char) rc);
					}
					dataInput.close();
					sbStr = sb.toString();

				} catch (IOException e) {
					log.error("Not able to connect for methodName {}", methodName);
					doRetry = false;
				} catch (Exception e) {
					log.error("Exception Occured while trying  methodName {}", methodName, e);
					doRetry = false;
				} finally {
					listener.extractMessage(requestXML, StringUtils.join(methodName, " Request"));
					listener.extractMessage(sbStr, StringUtils.join(methodName, " Response"));
				}
			} while (numTry++ < 3 && doRetry);
		}
		return sbStr;
	}

	public Note getNote(NoteType noteType, String message) {
		Note note = Note.builder().bookingId(bookingId).build();
		if (noteType != null) {
			note.setNoteType(noteType);
		} else {
			note.setNoteType(NoteType.SUPPLIER_MESSAGE);
		}
		if (StringUtils.isNotBlank(message)) {
			note.setNoteMessage(StringUtils.join(message, SYSTEM_GEN));
		}
		return note;
	}

	public int getFromSegmentsPaxCount(List<SegmentInfo> segmentInfos) {
		SegmentInfo segmentInfo = segmentInfos.get(0);
		List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
		return AirUtils.getPaxCountFromTravellerInfo(travellerInfos, true) * segmentInfos.size();
	}

	public static double getAmountBasedOnCurrency(BigDecimal amount, String currencyCode) {
		// Currently we are not using currency but in future we might need to set amount
		// based on current , so keeping scope for future
		return amount.doubleValue();
	}

	public String getNearByAirport(String airportCode, User bookingUser) {
		String nearByCode = AirUtils.getNearByAirport(airportCode, airline.getSourceId(), searchQuery, bookingUser);
		if (StringUtils.isNotBlank(nearByCode)) {
			return nearByCode;
		}
		return airportCode;
	}

	public String getFareBasisCode(TripInfo tripInfo) {
		PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
		AtomicReference<String> fbCode = new AtomicReference<>();
		priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
			if (!paxType.equals(PaxType.INFANT) && StringUtils.isNotBlank(fareDetail.getFareBasis())) {
				fbCode.set(fareDetail.getFareBasis());
			}
		}));
		return fbCode.get();
	}

	public String getCabinClass(TripInfo tripInfo) {
		PriceInfo priceInfo = tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
		AtomicReference<CabinClass> fbCode = new AtomicReference<>();
		fbCode.set(CabinClass.ECONOMY); // defualt economy
		priceInfo.getFareDetails().forEach(((paxType, fareDetail) -> {
			if (!paxType.equals(PaxType.INFANT) && fareDetail.getCabinClass() != null) {
				fbCode.set(fareDetail.getCabinClass());
			}
		}));
		return fbCode.get().name();
	}

	public void addExceptionToLogger(Exception e) {
		if (criticalMessageLogger != null && e != null && StringUtils.isNotBlank(e.getMessage())) {
			criticalMessageLogger.add(e.getMessage());
		}
	}

	public String getRequestId() {
		if (StringUtils.isNotBlank(bookingId)) {
			return bookingId;
		} else if (searchQuery != null) {
			return searchQuery.getSearchId();
		}
		return null;
	}

	public boolean isCancelled(RetrievePNRResponse retrieveResponse) {
		// Pax count less than zero which is already cancelled
		return retrieveResponse.getRetrievePNRResult().getActivePassengerCount() <= 0;
	}
}
