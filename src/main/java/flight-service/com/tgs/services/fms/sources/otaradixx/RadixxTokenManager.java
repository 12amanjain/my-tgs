package com.tgs.services.fms.sources.otaradixx;

import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.experimental.SuperBuilder;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_exceptions.ExceptionInformationException;
import org.datacontract.schemas._2004._07.radixx_connectpoint_exceptions.ExceptionInformationExceptions;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.EnumerationsCurrencyCodeTypes;
import org.datacontract.schemas._2004._07.radixx_connectpoint_security_request.RetrieveSecurityToken;
import org.datacontract.schemas._2004._07.radixx_connectpoint_travelagents_request.LoginTravelAgent;
import org.datacontract.schemas._2004._07.radixx_connectpoint_travelagents_request.RetrieveAgencyCommission;
import org.datacontract.schemas._2004._07.radixx_connectpoint_travelagents_response.ArrayOfTravelAgencyCommission;
import org.datacontract.schemas._2004._07.radixx_connectpoint_travelagents_response.TravelAgencyCommission;
import org.tempuri.ConnectPoint_SecurityStub;
import org.tempuri.ConnectPoint_TravelAgentsStub;
import org.tempuri.LoginTravelAgentResponse;
import org.tempuri.RetrieveAgencyCommissionResponse;
import org.tempuri.RetrieveSecurityTokenResponse;
import lombok.extern.slf4j.Slf4j;
import java.rmi.RemoteException;

@Slf4j
@SuperBuilder
final class RadixxTokenManager extends RadixxServiceManager {

	/**
	 * 1. TA Authentication - retrieve Security Token 2. If Security Token Not Blank - Then Login Credentials
	 * Authenticated
	 *
	 * @return true or false if login success
	 */
	public String generateToken() throws SupplierSessionException, SupplierRemoteException {
		binaryToken = generateBinaryToken();
		if (StringUtils.isNotBlank(binaryToken) && loginTravelAgent()) {
			return binaryToken;
		}
		return null;
	}

	// Step 1. TA Authentication - retrieve Security Token Based on TA Login login Credentials
	public String generateBinaryToken() throws SupplierSessionException, SupplierRemoteException {
		ConnectPoint_SecurityStub securityStub = null;
		try {
			securityStub = bindingService.getSecurityStub(configuration);
			RetrieveSecurityToken retrieveSecurityTokenrequest = new RetrieveSecurityToken();
			retrieveSecurityTokenrequest.setCarrierCodes(getCarrierCode());
			retrieveSecurityTokenrequest.setLogonID(configuration.getSupplierCredential().getUserName());
			retrieveSecurityTokenrequest.setPassword(configuration.getSupplierCredential().getPassword());
			org.tempuri.RetrieveSecurityToken retrieveSecurityToken = new org.tempuri.RetrieveSecurityToken();
			retrieveSecurityToken.setRetrieveSecurityTokenRequest(retrieveSecurityTokenrequest);
			listener.setType("1-L-Token");
			listener.getVisibilityGroups().addAll(AirSupplierUtils.getLogVisibility());
			securityStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			RetrieveSecurityTokenResponse response = securityStub.retrieveSecurityToken(retrieveSecurityToken);
			securityStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			if (!isCriticalException(response.getRetrieveSecurityTokenResult().getExceptions())) {
				binaryToken = response.getRetrieveSecurityTokenResult().getSecurityToken();
				return binaryToken;
			} else {
				throw new SupplierSessionException(String.join(",", getCriticalMessageLogger()));
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			securityStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_SecurityStub.class, securityStub);
		}
	}

	// Step 2. If Security Token Not Blank - Then Login Credentials Authenticated
	public boolean loginTravelAgent() throws SupplierSessionException, SupplierRemoteException {
		ConnectPoint_TravelAgentsStub travelAgentStub = null;
		try {
			travelAgentStub = bindingService.getTravelAgentStub(configuration);
			LoginTravelAgentResponse response = null;
			LoginTravelAgent loginTravelAgentRequest = new LoginTravelAgent();
			loginTravelAgentRequest.setSecurityGUID(binaryToken);
			loginTravelAgentRequest.setCarrierCodes(getCarrierCode());
			loginTravelAgentRequest.setIATANumber(configuration.getSupplierCredential().getIataNumber());
			loginTravelAgentRequest.setUserName(configuration.getSupplierCredential().getAgentUserName());
			loginTravelAgentRequest.setPassword(configuration.getSupplierCredential().getAgentPassword());
			org.tempuri.LoginTravelAgent loginTravelAgent = new org.tempuri.LoginTravelAgent();
			loginTravelAgent.setLoginTravelAgentRequest(loginTravelAgentRequest);
			listener.setType("2-L-Login");
			travelAgentStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			response = travelAgentStub.loginTravelAgent(loginTravelAgent);
			if (!isCriticalException(response.getLoginTravelAgentResult().getExceptions())) {
				return response.getLoginTravelAgentResult().getLoggedIn();
			} else {
				throw new SupplierSessionException(String.join(",", getCriticalMessageLogger()));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			travelAgentStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_TravelAgentsStub.class, travelAgentStub);
		}
	}

	@Override
	public double getAgencyCommission() {
		RetrieveAgencyCommissionResponse response = null;
		ConnectPoint_TravelAgentsStub travelagentStub = null;
		try {
			travelagentStub = bindingService.getTravelAgentStub(configuration);
			RetrieveAgencyCommission agencyCommissionRequest = new RetrieveAgencyCommission();
			agencyCommissionRequest.setSecurityGUID(binaryToken);
			agencyCommissionRequest.setCarrierCodes(getCarrierCode());
			agencyCommissionRequest.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
			agencyCommissionRequest.setCurrencyCode(EnumerationsCurrencyCodeTypes.INR);
			org.tempuri.RetrieveAgencyCommission agencyCommission = new org.tempuri.RetrieveAgencyCommission();
			agencyCommission.setRetrieveAgencyCommissionRequest(agencyCommissionRequest);
			listener.setType("3-L-Commission");
			travelagentStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			response = travelagentStub.retrieveAgencyCommission(agencyCommission);
			if (!isCriticalException(response.getRetrieveAgencyCommissionResult().getExceptions())
					&& ArrayUtils.isNotEmpty(response.getRetrieveAgencyCommissionResult().getTravelAgencyCommissions()
							.getTravelAgencyCommission())) {
				ArrayOfTravelAgencyCommission commission =
						response.getRetrieveAgencyCommissionResult().getTravelAgencyCommissions();
				if (ArrayUtils.isNotEmpty(commission.getTravelAgencyCommission())) {
					TravelAgencyCommission travelAgencyCommission = commission.getTravelAgencyCommission()[0];
					return travelAgencyCommission.getAmount().doubleValue();
				}
			} else {
				ExceptionInformationExceptions exceptions =
						response.getRetrieveAgencyCommissionResult().getExceptions();
				if (exceptions != null && ArrayUtils.isNotEmpty(exceptions.getExceptionInformationException())) {
					ExceptionInformationException info = exceptions.getExceptionInformationException()[0];
					if (RadixxConstantsInfo.SECURITY_EXCEP_START >= info.getExceptionCode()
							&& info.getExceptionCode() <= RadixxConstantsInfo.SECURITY_EXCEP_END) {
						throw new SupplierSessionException("TravelAgencyCommission Security Check Failed");
					}
				} else {
					throw new SupplierUnHandledFaultException("TravelAgencyCommission Failed");
				}
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			travelagentStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			bindingService.storeSessionMetaInfo(configuration, ConnectPoint_TravelAgentsStub.class, travelagentStub);
		}
		return 0;
	}
}
