package com.tgs.services.fms.sources.otaradixx;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import com.tgs.services.base.enums.FareType;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_request.EnumsFareFilterMethodType;
import org.datacontract.schemas._2004._07.radixx_connectpoint_pricing_response.LegDetail;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.ArrayOfCarrierCode;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.ReservationInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_request.TransactionInfo;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_request.Address;
import org.datacontract.schemas._2004._07.radixx_connectpoint_reservation_response.PhysicalFlight;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class RadixxUtils {

	public static FlightAPIURLRuleCriteria getRadixxPointURL(AirSourceConfigurationOutput sourceConfigurationOutput) {
		FlightAPIURLRuleCriteria flightUrls = AirUtils.getAirEndPointURL(sourceConfigurationOutput);
		return flightUrls;
	}

	public static FlightDesignator getFlightDesignator(LegDetail legDetail) {
		FlightDesignator flightDesignator = FlightDesignator.builder().build();
		flightDesignator.setAirlineInfo(AirlineHelper.getAirlineInfo(legDetail.getOperatingCarrier().trim()));
		flightDesignator.setFlightNumber(legDetail.getFlightNum().trim());
		flightDesignator.setEquipType(legDetail.getAircraftType());
		return flightDesignator;
	}

	public static void setTerminalInfo(SegmentInfo segmentInfo, LegDetail legDetail) {
		if (StringUtils.isNotEmpty(legDetail.getFromTerminal())) {
			segmentInfo.getDepartAirportInfo().setTerminal(legDetail.getFromTerminal());
		}
		if (StringUtils.isNotEmpty(legDetail.getToTerminal())) {
			segmentInfo.getArrivalAirportInfo().setTerminal(legDetail.getToTerminal());
		}
	}

	public static TransactionInfo getTransactionInfo(String binaryToken, ArrayOfCarrierCode carrierCode,
			SupplierConfiguration configuration) {
		TransactionInfo transactionInfo = new TransactionInfo();
		transactionInfo.setSecurityGUID(binaryToken);
		transactionInfo.setCarrierCodes(carrierCode);
		transactionInfo.setHistoricUserName(configuration.getSupplierCredential().getAgentUserName());
		return transactionInfo;
	}

	public static ReservationInfo getReservationInfo(String confirmationNumber) {
		ReservationInfo reservationInfo = new ReservationInfo();
		reservationInfo.setSeriesNumber(RadixxConstantsInfo.SERIESNUMBER);
		if (StringUtils.isNotBlank(confirmationNumber)) {
			reservationInfo.setConfirmationNumber(confirmationNumber);
		} else {
			reservationInfo.setConfirmationNumber(StringUtils.EMPTY);
		}
		return reservationInfo;
	}

	public static Address getAddress(ClientGeneralInfo companyInfo) {
		Address address = new Address();
		address.setAddress1(companyInfo.getAddress1());
		address.setAddress2(companyInfo.getAddress2());
		address.setCity(companyInfo.getCity());
		address.setState(companyInfo.getCity());
		address.setPostal(RadixxConstantsInfo.POSTAL);
		address.setCountry(companyInfo.getCountry());
		address.setCountryCode(companyInfo.getCountryCode());
		address.setAreaCode(RadixxConstantsInfo.AREACODE);
		address.setPhoneNumber(companyInfo.getWorkPhone());
		address.setDisplay(RadixxConstantsInfo.NOTAPPLICABLE);
		return address;
	}

	public static String getTitle(FlightTravellerInfo travellerInfo, RadixxAirline airline) {
		if (airline.getSourceId() == AirSourceType.FLYDUBAI.getSourceId()) {
			if (travellerInfo.getPaxType().equals(PaxType.CHILD) || travellerInfo.getPaxType().equals(PaxType.INFANT)) {
				if (isMaster(travellerInfo)) {
					return "Mstr";
				} else {
					return "Miss";
				}
			} else {
				return travellerInfo.getTitle();
			}
		}
		return travellerInfo.getTitle().toUpperCase();
	}

	public static boolean isMaster(FlightTravellerInfo travellerInfo) {
		return travellerInfo.getTitle().toUpperCase().equals("MR")
				|| travellerInfo.getTitle().toUpperCase().equals("MASTER")
				|| travellerInfo.getTitle().toUpperCase().equals("MSTR");
	}

	public static TripInfo filterResults(AirSearchResult searchResult, TripInfo oldTrip) {
		TripInfo newTrip = AirUtils.filterTripFromAirSearchResult(searchResult, oldTrip);
		AtomicInteger segmentIndex = new AtomicInteger(0);
		oldTrip.getSegmentInfos().forEach(oldSegmentInfo -> {
			if (oldSegmentInfo.getSegmentNum().intValue() == 0) {
				String fareIdentifier = oldSegmentInfo.getPriceInfo(0).getFareType();
				String oldFBA = oldSegmentInfo.getPriceInfo(0).getFareBasis(PaxType.ADULT);
				CabinClass cabinClass = oldSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass();
				SegmentInfo newSegmentInfo = newTrip.getSegmentInfos().get(segmentIndex.get());
				List<PriceInfo> filteredPriceList = newSegmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
					return (StringUtils.isNotEmpty(priceInfo.getFareType())
							&& priceInfo.getFareIdentifier().getName().equals(fareIdentifier))
							&& (priceInfo.getFareDetail(PaxType.ADULT).getCabinClass().equals(cabinClass)
									&& priceInfo.getFareBasis(PaxType.ADULT).equalsIgnoreCase(oldFBA));
				}).collect(Collectors.toList());
				if (CollectionUtils.isNotEmpty(filteredPriceList)
						&& AirUtils.matchSegmentInfo(newSegmentInfo, oldSegmentInfo)) {
					List<PriceInfo> newPriceInfos = new ArrayList<>();
					newPriceInfos.add(filteredPriceList.get(0));
					newSegmentInfo.setPriceInfoList(newPriceInfos);
				}
			}
			segmentIndex.getAndIncrement();
		});
		return newTrip;
	}

	public static List<TripInfo> buildCombination(List<TripInfo> onwardTrips, List<TripInfo> returnTrips,
			Integer sourceId, User bookingUser) {
		List<TripInfo> combinedTrips = new ArrayList<>();
		for (TripInfo onwardTrip : onwardTrips) {
			for (TripInfo returnTrip : returnTrips) {
				LocalDateTime onwardArrivalTime = onwardTrip.getArrivalTime();
				LocalDateTime returnDepartureTime = returnTrip.getDepartureTime();
				if (Duration.between(onwardArrivalTime, returnDepartureTime).toMinutes() > AirUtils
						.getCombinationAllowedMinutes(sourceId, bookingUser)) {
					TripInfo copyOnwardTrip = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
					TripInfo copyReturnTrip = new GsonMapper<>(returnTrip, TripInfo.class).convert();
					if (onwardTrip.isPriceInfosNotEmpty() && returnTrip.isPriceInfosNotEmpty()) {
						int onwardPriceSize = onwardTrip.getSegmentInfos().get(0).getPriceInfoList().size();
						int returnPriceSize = returnTrip.getSegmentInfos().get(0).getPriceInfoList().size();
						for (int segmentIndex = 0; segmentIndex < copyOnwardTrip.getSegmentInfos()
								.size(); segmentIndex++) {
							copyOnwardTrip.getSegmentInfos().get(segmentIndex).setPriceInfoList(new ArrayList<>());
							for (int priceIndex = 0; priceIndex < onwardTrip.getSegmentInfos().get(segmentIndex)
									.getPriceInfoList().size(); priceIndex++) {
								for (int index = 0; index < returnPriceSize; index++) {
									PriceInfo priceInfo =
											new GsonMapper<PriceInfo>(onwardTrip.getSegmentInfos().get(segmentIndex)
													.getPriceInfoList().get(priceIndex), PriceInfo.class).convert();
									copyOnwardTrip.getSegmentInfos().get(segmentIndex).getPriceInfoList()
											.add(priceInfo);
								}
							}
						}
						for (int index = 0; index < onwardPriceSize - 1; index++) {
							for (int segmentIndex = 0; segmentIndex < copyReturnTrip.getSegmentInfos()
									.size(); segmentIndex++) {
								copyReturnTrip.getSegmentInfos().get(segmentIndex).getPriceInfoList()
										.addAll(GsonUtils.getGson().fromJson(
												GsonUtils.getGson()
														.toJson(returnTrip.getSegmentInfos().get(segmentIndex)
																.getPriceInfoList()),
												new TypeToken<List<PriceInfo>>() {}.getType()));
							}
						}
						copyReturnTrip.getSegmentInfos().get(0).setIsReturnSegment(true);
						copyOnwardTrip.getSegmentInfos().addAll(copyReturnTrip.getSegmentInfos());
						setAndCheckMixedFareType(sourceId, copyOnwardTrip);
						combinedTrips.add(copyOnwardTrip);
					}
				}
			}
		}
		return combinedTrips;
	}

	private static void setAndCheckMixedFareType(Integer sourceId, TripInfo tripInfo) {
		try {
			if (sourceId == AirSourceType.AIRINDIAEXPRESS.getSourceId()
					&& CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
				List<PriceInfo> initialPriceInfos = tripInfo.getSegmentInfos().get(0).getPriceInfoList();
				Integer totalPriceSize = CollectionUtils.size(initialPriceInfos);
				for (int priceIndex = 0; priceIndex < totalPriceSize - 1; priceIndex++) {
					boolean isSameFareType = true;
					String fareIdentifier = tripInfo.getSegmentInfos().get(0).getPriceInfo(priceIndex).getFareType();
					for (int segmentIndex = 1; segmentIndex <= tripInfo.getSegmentInfos().size() - 1; segmentIndex++) {
						if (!tripInfo.getSegmentInfos().get(segmentIndex).getPriceInfo(priceIndex).getFareType()
								.equalsIgnoreCase(fareIdentifier)) {
							isSameFareType = false;
						}
					}
					if (!isSameFareType) {
						setMixedFareType(tripInfo, priceIndex, FareType.EXPRESS_MIXED);
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to change fare type for search {} due to ", tripInfo, e);
		}
	}

	private static void setMixedFareType(TripInfo tripInfo, int priceIndex, FareType expressMixed) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.getPriceInfoList().get(priceIndex).setFareIdentifier(expressMixed);
		});
	}

	public static boolean containsSSRCode(String ssrCode, Map<SSRType, List<? extends SSRInformation>> preSSR,
			boolean isMeal, boolean isBaggage) {
		if (MapUtils.isNotEmpty(preSSR)) {
			List<? extends SSRInformation> ssrList = new ArrayList<SSRInformation>();
			if (isMeal) {
				ssrList = preSSR.get(SSRType.MEAL);
			}

			if (isBaggage) {
				ssrList = preSSR.get(SSRType.BAGGAGE);
			}
			SSRInformation matchedSSR = getMatchedSSR(ssrList, ssrCode);
			if (matchedSSR != null) {
				return true;
			}
		}
		return false;
	}

	public static SSRInformation getMatchedSSR(List<? extends SSRInformation> ssrList, String ssrCode) {
		if (CollectionUtils.isNotEmpty(ssrList)) {
			for (SSRInformation ssr : ssrList) {
				if (ssrCode.equals(ssr.getCode())) {
					return ssr;
				}
			}
		}
		return null;
	}

	public static boolean isIntlMulticity(AirSearchQuery searchQuery) {
		return searchQuery.isMultiCity() && searchQuery.isIntl();
	}

	public static EnumsFareFilterMethodType getFareFilterMethod(AirSearchQuery searchQuery) {
		if (isIntlMulticity(searchQuery)) {
			return EnumsFareFilterMethodType.NoCombinabilityRoundtripLowestFarePerFlight;
		}
		return EnumsFareFilterMethodType.NoCombinabilityRoundtripLowestFarePerFareType;
	}

	public static SegmentInfo getSegmentInfo(PhysicalFlight physicalFlight, List<SegmentInfo> cancelSegments) {
		for (SegmentInfo segment : cancelSegments) {
			if (segment.getDepartureAirportCode().equals(physicalFlight.getOrigin().trim())
					&& segment.getArrivalAirportCode().equals(physicalFlight.getDestination().trim())
					&& segment.getFlightNumber().equals(physicalFlight.getFlightNumber().trim())
					&& segment.getAirlineCode(true).equals(physicalFlight.getCarrierCode().trim())) {
				return segment;
			}
		}
		return null;
	}

	public static double getInitialAmount(FareDetail fareDetail, FareComponent fareComponent) {
		double initialAmount = 0.0;
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())
				&& Objects.nonNull(fareDetail.getFareComponents().get(fareComponent))
				&& fareDetail.getFareComponents().get(fareComponent).doubleValue() > 0) {
			initialAmount = fareDetail.getFareComponents().get(fareComponent).doubleValue();
		}
		return initialAmount;
	}
}
