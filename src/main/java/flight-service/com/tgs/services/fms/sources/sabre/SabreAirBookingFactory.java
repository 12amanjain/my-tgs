package com.tgs.services.fms.sources.sabre;

import java.util.List;
import java.util.Objects;
import com.tgs.services.base.SoapRequestResponseListner;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.manager.AirBookingEngine;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SabreAirBookingFactory extends AbstractAirBookingFactory {

	protected SabreBindingService bindingService;

	private SoapRequestResponseListner listener;

	public SabreAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	protected SabreBookingManager bookingManager;

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		String binaryToken = StringUtils.EMPTY;
		isTkRequired = true;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getSupplierId());
		try {
			if (Objects.nonNull(supplierSession)) {
				List<SegmentInfo> bookingSegmentInfos = bookingSegments.getSegmentInfos();
				// bookingSegmentsCopy should be used within the booking Manager as paxInfo may be changed by private
				// fare logic.
				List<SegmentInfo> bookingSegmentsCopy =
						AirUtils.checkAndApplyPrivateFareLogic(bookingSegments.getSegmentInfos());

				binaryToken = supplierSession.getSupplierSessionInfo().getSessionToken();
				bookingManager = SabreBookingManager.builder().binaryToken(binaryToken).configuration(supplierConf)
						.order(order).listener(listener).segmentInfos(bookingSegmentInfos).bookingId(bookingId)
						.criticalMessageLogger(criticalMessageLogger).receivedFrom(bookingId).deliveryInfo(deliveryInfo)
						.gmsCommunicator(gmsCommunicator).moneyExchnageComm(moneyExchangeComm)
						.sourceConfiguration(sourceConfiguration).bindingService(bindingService)
						.bookingUser(bookingUser).build();
				if (bookingManager.updatePassengerDetails(gstInfo, bookingSegmentsCopy)) {
					String crsPnr = bookingManager.getCrsPnr();
					if (StringUtils.isNotBlank(crsPnr)) {
						BookingUtils.updateSupplierBookingId(bookingSegmentInfos, crsPnr);
						boolean isPriceStored = bookingManager.storePrice(crsPnr, bookingSegmentsCopy);
						bookingSegments.setSupplierBookingId(crsPnr);
						if (isPriceStored && !isFareDiff(bookingManager.getAmountPaidToAirline(crsPnr))) {
							pnr = bookingManager.getAirlinePnr(crsPnr);
							if (AirUtils.isSSRAddedInTrip(bookingSegmentInfos)) {
								bookingManager.specialServiceRq(crsPnr);
							}
							this.sendCommandsPostProcessing();
							isSuccess = true;
						}
					}
				}
				if (isHoldBooking && isSuccess) {
					updateTimeLimit(TgsDateUtils.getCalendar(bookingManager.getTicketingTimelimit()));
				}
			}
		} finally {
			if (StringUtils.isNotEmpty(binaryToken))
				bookingManager.sessionClose();
		}
		return isSuccess;
	}

	protected void sendCommandsPostProcessing() {
		return;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		String binaryToken = StringUtils.EMPTY;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getSupplierId());
		try {

			Thread.sleep(5 * 1000);
			List<SegmentInfo> bookingSegmentInfos = bookingSegments.getSegmentInfos();

			// bookingSegmentsCopy should be used within the booking Manager as paxInfo may be changed by private fare
			// logic.
			List<SegmentInfo> bookingSegmentsCopy = AirUtils.checkAndApplyPrivateFareLogic(bookingSegmentInfos);

			SabreTokenServiceManager tokenManager =
					SabreTokenServiceManager.builder().bookingId(bookingId).configuration(supplierConf)
							.listener(listener).bindingService(bindingService).bookingUser(bookingUser).build();
			binaryToken = tokenManager.generateSessionToken();
			updateSupplierAndDoChangeAAA(bookingSegmentInfos, tokenManager);
			pnr = bookingSegments.getSupplierBookingId();
			bookingManager = SabreBookingManager.builder().binaryToken(binaryToken).configuration(supplierConf)
					.creditCard(getSupplierBookingCreditCard()).order(order).deliveryInfo(deliveryInfo)
					.bookingId(bookingId).listener(listener).criticalMessageLogger(criticalMessageLogger)
					.moneyExchnageComm(moneyExchangeComm).segmentInfos(bookingSegmentInfos)
					.bindingService(bindingService).bookingUser(bookingUser).build();
			bookingManager.designatePrinter();
			bookingManager.isTravelItinerarySuccess(pnr);
			bookingManager.airTicket(bookingSegmentsCopy);
			if (!isFareDiff(bookingManager.getAmountPaidToAirline(pnr))) {
				bookingManager.ticketing(pnr);
				isSuccess = true;
			}
		} catch (InterruptedException e) {
			log.error("Ticketing Sleep Failed for BookingId {}", bookingId, e);
			throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
		} finally {
			bookingManager.sessionClose();
		}
		return isSuccess;
	}

	public void updateSupplierAndDoChangeAAA(List<SegmentInfo> segmentInfos, SabreTokenServiceManager tokenManager) {
		String ticketingSupplierId = AirBookingEngine.getTicketingSupplierId(supplierConf.getSupplierAdditionalInfo(),
				segmentInfos, bookingUser);
		if (StringUtils.isNotEmpty(ticketingSupplierId)
				&& !supplierConf.getBasicInfo().getSupplierId().equals(ticketingSupplierId)) {
			SupplierInfo updatedSupplierInfo = SupplierConfigurationHelper.getSupplierInfo(ticketingSupplierId);
			SupplierBasicInfo updatedBasicInfo = SupplierBasicInfo.builder().sourceId(updatedSupplierInfo.getSourceId())
					.supplierId(updatedSupplierInfo.getSupplierId()).supplierName(updatedSupplierInfo.getName())
					.build();
			supplierConf.setBasicInfo(updatedBasicInfo);
			supplierConf.setSupplierCredential(updatedSupplierInfo.getCredentialInfo());
			updateSupplierId(bookingSegments.getSegmentInfos());
			log.info("Ticketing Pcc changed for booking id {} new supplier id {}", order.getBookingId(),
					ticketingSupplierId);
			tokenManager.changeAAA(updatedSupplierInfo.getCredentialInfo().getPcc());
		}
	}

	@Override
	public boolean confirmBeforeTicket() {
		boolean isFareDiff = false;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		List<SegmentInfo> segmentInfos = bookingSegments.getSegmentInfos();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getSupplierId());

		// bookingSegmentsCopy should be used within the booking Manager as paxInfo may be changed by private fare
		// logic.
		List<SegmentInfo> bookingSegmentsCopy =
				AirUtils.checkAndApplyPrivateFareLogic(bookingSegments.getSegmentInfos());

		SabreTokenServiceManager tokenManager =
				SabreTokenServiceManager.builder().configuration(supplierConf).bookingId(bookingId)
						.bindingService(bindingService).listener(listener).bookingUser(bookingUser).build();
		String binaryToken = tokenManager.generateSessionToken();
		SegmentBookingRelatedInfo bookingRelatedInfo = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo();
		String crsPNR = bookingRelatedInfo.getTravellerInfo().get(0).getSupplierBookingId();
		SabreBookingManager bookingManager = SabreBookingManager.builder().binaryToken(binaryToken)
				.configuration(supplierConf).order(order).deliveryInfo(deliveryInfo).bookingId(bookingId)
				.segmentInfos(segmentInfos).moneyExchnageComm(moneyExchangeComm).listener(listener)
				.bindingService(bindingService).bookingUser(bookingUser).build();
		boolean isSegmentsActive = bookingManager.isSegmentsActiveForTicketing(crsPNR);

		if (isSegmentsActive && !isSameDayTicketing()) {
			bookingManager.deletePriceQuote();
			bookingManager.storePrice(crsPNR, bookingSegmentsCopy);
		}

		if (!isSegmentsActive || isFareDiff(bookingManager.getAmountPaidToAirline(crsPNR))) {
			isFareDiff = true;
		}
		return isFareDiff;
	}

}
