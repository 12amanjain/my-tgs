package com.tgs.services.fms.sources.sabre;

import com.tgs.services.base.SoapRequestResponseListner;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SabreAirCancellationFactory extends AbstractAirCancellationFactory {

	protected SabreBindingService bindingService;

	private SoapRequestResponseListner listener;

	public SabreAirCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	@Override
	public boolean releaseHoldPNR() {
		boolean isAbortSuccess = false;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		SabreCancellationManager sabreCancellationManager = null;
		try {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			SabreTokenServiceManager tokenManager =
					SabreTokenServiceManager.builder().configuration(supplierConf).bookingId(bookingId)
							.listener(listener).bindingService(bindingService).bookingUser(bookingUser).build();
			String binaryToken = tokenManager.generateSessionToken();
			sabreCancellationManager = SabreCancellationManager.builder().binaryToken(binaryToken).bookingId(bookingId)
					.configuration(supplierConf).order(order).pnr(pnr).segmentInfos(bookingSegments.getSegmentInfos())
					.sourceConfiguration(sourceConfiguration).listener(listener).bindingService(bindingService)
					.bookingUser(bookingUser).build();
			isAbortSuccess = sabreCancellationManager.releaseHoldPNR(bookingSegments.getSupplierBookingId());
		} finally {
			if (sabreCancellationManager != null)
				sabreCancellationManager.sessionClose();
		}
		return isAbortSuccess;
	}

}
