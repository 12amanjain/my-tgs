package com.tgs.services.fms.sources.sabre;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SabreAirInfoFactory extends AbstractAirInfoFactory {

	protected SabreBindingService bindingService;

	private SoapRequestResponseListner listener;

	public SabreAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public SabreAirInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	@Override
	public void searchAvailableSchedules() {
		int numTry = 0;
		boolean doRetry = false;
		String binaryToken = null;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null, supplierConf.getSupplierId());
		try {
			do {
				SessionMetaInfo metaInfo = fetchSession();
				AirSupplierUtils.isStoreLogRequired(searchQuery);
				if (metaInfo == null || metaInfo.getValue() == null) {
					SabreTokenServiceManager tokenManager =
							SabreTokenServiceManager.builder().searchQuery(searchQuery).configuration(supplierConf)
									.listener(listener).bindingService(bindingService).bookingUser(user).build();
					binaryToken = tokenManager.generateBinaryToken();
					// binaryToken
					// ="T1RLAQIo9MScTAAngAqwZM1yErcH2ULyPRAO0VknO5KcAcmfqddmY8qIAACwV8I9G73qFWfs6XSBGhl4ri+Zai4MG7DI0wCfV0PGUqejKApeuLYlXGaFumyWhF3FsQyzz7qwdXITS9aIiquz0o1241zXzvY0Jk0ouzLeJYTtQxt2ZcrlGSp6H6eVNJUO+2d54tg/sVWw9HjXV2JvmAkkrAGCra0YtKRRjne2V0zr3AL8l7VAdGvTRuosRzIX0QnVXTgBIyHr3TOhjUjDT2RIQUsmJmK+z3ejaI9r5zw*";
					log.debug("Received binary token searchid {} btoken {}", searchQuery.getSearchId(), binaryToken);
				} else {
					binaryToken = (String) metaInfo.getValue();
				}

				SabreBFMServiceManager bfmServiceManager = SabreBFMServiceManager.builder().binaryToken(binaryToken)
						.configuration(supplierConf).listener(listener).sourceConfiguration(sourceConfiguration)
						.searchQuery(searchQuery).bindingService(bindingService).moneyExchnageComm(moneyExchnageComm)
						.bookingUser(user).build();
				searchResult = bfmServiceManager.search();
			} while (++numTry < 2 && doRetry);
		} finally {
			storeSession(binaryToken);
		}
	}

	public SessionMetaInfo fetchSession() {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
				.index(0).compress(false).build();
		metaInfo = cachingComm.fetchFromQueue(metaInfo);
		if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
			metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString()).index(-1)
					.compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
		}
		return metaInfo;
	}

	public void storeSession(String binaryToken) {
		if (StringUtils.isNotBlank(binaryToken)) {
			try {
				SessionMetaInfo metaInfo = SessionMetaInfo.builder()
						.key(supplierConf.getBasicInfo().getRuleId().toString()).compress(false).build();
				metaInfo.setValue(binaryToken).setExpiryTime(LocalDateTime.now().plusMinutes(60));
				cachingComm.storeInQueue(metaInfo);
			} catch (Exception e) {
				log.error(AirSourceConstants.SEARCH_SESSION_FAILURE, searchQuery.getSearchId(), e);
				if (listener == null) {
					listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
							supplierConf.getSupplierId());
				}
				SabreTokenServiceManager tokenManager = SabreTokenServiceManager.builder().searchQuery(searchQuery)
						.configuration(supplierConf).listener(listener).bindingService(bindingService)
						.binaryToken(binaryToken).bookingUser(user).build();
				tokenManager.sessionClose();
			}
		}
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		boolean isNewToken = false;
		AtomicBoolean isReviewSuccess = new AtomicBoolean();
		String sessionId = null;
		TripInfo reviewedTrip = null;
		SupplierSession session = null;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getSupplierId());
		try {
			SupplierSessionInfo sessionInfo =
					SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
			session = getSessionIfExists(bookingId, sessionInfo);
			if (Objects.isNull(session)) {
				SabreTokenServiceManager tokenManager =
						SabreTokenServiceManager.builder().configuration(supplierConf).bindingService(bindingService)
								.listener(listener).bookingId(bookingId).bookingUser(user).build();
				isNewToken = true;
				sessionId = tokenManager.generateSessionToken();
			} else {
				sessionId = session.getSupplierSessionInfo().getSessionToken();
			}
			SabreReviewManager reviewManager = SabreReviewManager.builder().configuration(supplierConf)
					.binaryToken(sessionId).searchQuery(searchQuery).bookingId(bookingId)
					.criticalMessageLogger(criticalMessageLogger).moneyExchnageComm(moneyExchnageComm)
					.listener(listener).bindingService(bindingService).bookingUser(user).build();
			reviewedTrip = reviewManager.reviewFlight(selectedTrip, isReviewSuccess);
			reviewManager.setSSRInfos(reviewedTrip);
		} finally {
			if (isNewToken && isReviewSuccess.get()) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(sessionId).build();
				storeBookingSession(bookingId, sessionInfo, session, selectedTrip);
			}
		}
		return reviewedTrip;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		try {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getSupplierId());
			bindingService = SabreBindingService.builder().configuration(supplierConf).build();
			SabreTokenServiceManager tokenManager = SabreTokenServiceManager.builder().configuration(supplierConf)
					.bookingId(bookingId).bindingService(bindingService).listener(listener).bookingUser(user).build();
			String binaryToken = tokenManager.generateSessionToken();
			SabreFareRuleManager fareRuleManager = SabreFareRuleManager.builder().binaryToken(binaryToken)
					.configuration(supplierConf).selectedTrip(selectedTrip).bookingId(bookingId)
					.criticalMessageLogger(criticalMessageLogger).listener(listener)
					.moneyExchnageComm(moneyExchnageComm).bindingService(bindingService).bookingUser(user).build();
			tripFareRule = fareRuleManager.getTripFareRule(isDetailed);
		} catch (Exception e) {
			log.error("Unable to fetch fare rule excep booking {} ", bookingId, e);
			throw e;
		}
		return tripFareRule;
	}

	@Override
	public TripInfo getChangeClass(TripInfo tripInfo, String searchId) {
		String binaryToken = StringUtils.EMPTY;
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(searchId, null, supplierConf.getSupplierId());
		SabreTokenServiceManager tokenManager = null;
		try {
			tokenManager = SabreTokenServiceManager.builder().configuration(supplierConf).listener(listener)
					.bindingService(bindingService).bookingId(searchId).bookingUser(user).build();
			binaryToken = tokenManager.generateSessionToken();
			SabreAlternateClassManager searchManager = SabreAlternateClassManager.builder().binaryToken(binaryToken)
					.configuration(supplierConf).sourceConfiguration(sourceConfiguration)
					.criticalMessageLogger(criticalMessageLogger).moneyExchnageComm(moneyExchnageComm)
					.listener(listener).bindingService(bindingService).bookingUser(user).build();
			searchManager.setBookingId(searchId);
			searchManager.searchAlternateClass(tripInfo);
		} catch (Exception e) {
			log.error("Exception occurred while searching for alternate class {}", searchId, e);
		} finally {
			if (StringUtils.isNotEmpty(binaryToken)) {
				tokenManager.setBinaryToken(binaryToken);
				tokenManager.sessionClose();
			}
		}
		return tripInfo;
	}

	@Override
	public void closeSession(List<SupplierSession> sessionList, String bookingId) {
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getSupplierId());
		SabreTokenServiceManager tokenManager = SabreTokenServiceManager.builder().configuration(supplierConf)
				.bookingId(bookingId).listener(listener).bindingService(bindingService).bookingUser(user).build();
		for (SupplierSession session : sessionList) {
			tokenManager.setBinaryToken(session.getSupplierSessionInfo().getSessionToken());
			tokenManager.sessionClose();
		}
	}

	@Override
	public TripInfo getChangeClassFare(TripInfo tripInfo, String searchId) {
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(searchId, null, supplierConf.getSupplierId());
		SabreTokenServiceManager sessionManager = null;
		String binaryToken = StringUtils.EMPTY;
		AtomicBoolean isReviewSuccess = new AtomicBoolean();
		try {
			sessionManager = SabreTokenServiceManager.builder().configuration(supplierConf).bookingId(searchId)
					.bindingService(bindingService).listener(listener).bookingUser(user).build();
			binaryToken = sessionManager.generateSessionToken();
			SabreReviewManager reviewManager = SabreReviewManager.builder().configuration(supplierConf)
					.binaryToken(binaryToken).searchQuery(searchQuery).bookingId(searchId)
					.criticalMessageLogger(criticalMessageLogger).moneyExchnageComm(moneyExchnageComm)
					.listener(listener).bindingService(bindingService).bookingUser(user).build();
			tripInfo = reviewManager.reviewFlight(tripInfo, isReviewSuccess);
		} catch (NoSeatAvailableException nos) {
			throw nos;
		} finally {
			sessionManager.sessionClose();
		}
		return tripInfo;
	}

	@Override
	public Boolean initializeStubs() {
		log.info("Initialize Sabre Stubs");
		bindingService = SabreBindingService.builder().configuration(supplierConf).build();
		bindingService.getTokenService();
		bindingService.getBfmS();
		return true;
	}
}
