package com.tgs.services.fms.sources.sabre;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.axis2.databinding.types.NonNegativeInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import org.opentravel.www.ota._2003._05.*;
import com.google.common.base.Enums;
import com.sabre.webservices.websvc.BargainFinderMaxServiceStub;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class SabreBFMServiceManager extends SabreServiceManager {

	private static final String ITINCOUNT = "ITINS";
	private static final String MULTITICKETPOLICY = "SOW";

	public AirSearchResult search() throws NoSearchResultException {
		AirSearchResult airSearchResult = null;
		BargainFinderMaxServiceStub serviceBfmS = bindingService.getBfmS();
		OTA_AirLowFareSearchRQ lowAirFareSearchRQ = null;
		OTA_AirLowFareSearchRS lowAirFareSearchRes = null;
		try {
			listener.setType(AirUtils.getLogType("BFM_Search", configuration));
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.BFM_SEARCH, SabreConstants.CONVERSATION_ID);
			lowAirFareSearchRQ = buildLowFareSearchRQ();
			serviceBfmS._getServiceClient().getAxisService().addMessageContextListener(listener);
			lowAirFareSearchRes = serviceBfmS.bargainFinderMaxRQ(lowAirFareSearchRQ, messageHeader, getSecurity());
			checkAnyErrors(lowAirFareSearchRes);
			airSearchResult = parseAirFareSearchRes(lowAirFareSearchRes);
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			serviceBfmS._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return airSearchResult;
	}

	private void checkAnyErrors(OTA_AirLowFareSearchRS lowAirFareSearchRes) {
		StringJoiner message = new StringJoiner("");
		if (lowAirFareSearchRes != null
				&& lowAirFareSearchRes.getOTA_AirLowFareSearchRSChoice_type0().getErrors() != null
				&& ArrayUtils.isNotEmpty(
						lowAirFareSearchRes.getOTA_AirLowFareSearchRSChoice_type0().getErrors().getError())) {
			for (ErrorType error : lowAirFareSearchRes.getOTA_AirLowFareSearchRSChoice_type0().getErrors().getError()) {
				message.add(error.getString());
			}
			if (StringUtils.isNotBlank(message.toString())) {
				throw new NoSearchResultException(message.toString());
			}
		}
	}

	public OTA_AirLowFareSearchRQ buildLowFareSearchRQ() {
		OTA_AirLowFareSearchRQ lowAirFareSearchRQ = new OTA_AirLowFareSearchRQ();
		lowAirFareSearchRQ.setVersion(SabreConstants.BFM_VERSION);
		lowAirFareSearchRQ.setPOS(getPOSConfiguration());
		OTA_AirLowFareSearchRQSequence_type0 otaAirLowFareRqType = new OTA_AirLowFareSearchRQSequence_type0();
		otaAirLowFareRqType.setOriginDestinationInformation(getOriginDestinationInformationConfiguration(searchQuery));
		otaAirLowFareRqType.setTravelPreferences(getTravelPreferenceConfiguration(searchQuery));
		otaAirLowFareRqType.setTravelerInfoSummary(getTravellerInfoSummaryConfiguration(searchQuery));
		lowAirFareSearchRQ.setOTA_AirLowFareSearchRQSequence_type0(otaAirLowFareRqType);
		lowAirFareSearchRQ.setTPA_Extensions(getTPAExtensionsConfiguration());
		return lowAirFareSearchRQ;
	}


	/**
	 * This function will parse response received from sabre and build AirSearchResult out of it.
	 *
	 * @param lowAirFareSearchRes
	 * @return
	 */
	private AirSearchResult parseAirFareSearchRes(OTA_AirLowFareSearchRS lowAirFareSearchRes) {
		AirSearchResult airSearchResult = new AirSearchResult();
		if (lowAirFareSearchRes.getPricedItineraries() == null) {
			throw new NoSearchResultException();
		}
		Map<String, TripInfo> onwardFlightMap = new HashMap<>();
		Map<String, TripInfo> returnFlightMap = new HashMap<>();
		if (lowAirFareSearchRes.getOneWayItineraries() != null
				&& ArrayUtils.isNotEmpty(lowAirFareSearchRes.getOneWayItineraries().getSimpleOneWayItineraries())) {
			for (SimpleOneWayItineraries_type0 oneWayItinerary : lowAirFareSearchRes.getOneWayItineraries()
					.getSimpleOneWayItineraries()) {

				for (PricedItineraryType pricedItinerary : oneWayItinerary.getPricedItinerary()) {
					OriginDestinationOptionType[] originDestinationOptions = pricedItinerary.getAirItinerary()
							.getOriginDestinationOptions().getOriginDestinationOption();
					AirItineraryPricingInfo_type0[] airItineraryPricingInfoList =
							pricedItinerary.getAirItineraryPricingInfo();
					TripInfo tripInfo = getTripInfo(originDestinationOptions, airItineraryPricingInfoList,
							pricedItinerary.getTPA_Extensions(), true);
					if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
						if (tripInfo.getSegmentInfos().get(0).getDepartAirportInfo().getCode()
								.equals(searchQuery.getRouteInfos().get(0).getFromCityOrAirport().getCode())) {
							onwardFlightMap.put(SabreUtils.getKey(tripInfo), tripInfo);
							airSearchResult.addTripInfo(TripInfoType.ONWARD.getName(), tripInfo);
						} else {
							returnFlightMap.put(SabreUtils.getKey(tripInfo), tripInfo);
							airSearchResult.addTripInfo(TripInfoType.RETURN.getName(), tripInfo);
						}
					}
				}
			}
		}

		for (PricedItineraryType pricedItinerary : lowAirFareSearchRes.getPricedItineraries().getPricedItinerary()) {
			OriginDestinationOptionType[] originDestinationOptions =
					pricedItinerary.getAirItinerary().getOriginDestinationOptions().getOriginDestinationOption();

			AirItineraryPricingInfo_type0[] airItineraryPricingInfoList = pricedItinerary.getAirItineraryPricingInfo();
			TripInfo tripInfo = getTripInfo(originDestinationOptions, airItineraryPricingInfoList,
					pricedItinerary.getTPA_Extensions(), (searchQuery.isDomesticReturn() ? false : true));
			if (searchQuery.isDomesticReturn()) {
				SabreUtils.splitTripAndFare(tripInfo, airSearchResult, onwardFlightMap, returnFlightMap,
						getAmountBasedOnCurrency(
								airItineraryPricingInfoList[0].getItinTotalFare().getTotalFare().getAmount().getMoney(),
								airItineraryPricingInfoList[0].getItinTotalFare().getTotalFare().getCurrencyCode()
										.getAlphaLength3()));
			} else if (searchQuery.isOneWay() || (searchQuery.isMultiCity() && searchQuery.getIsDomestic())) {
				airSearchResult.addTripInfo(TripInfoType.ONWARD.getName(), tripInfo);
			} else if ((searchQuery.isReturn() || searchQuery.isMultiCity()) && !searchQuery.getIsDomestic()) {
				airSearchResult.addTripInfo(TripInfoType.COMBO.getName(), tripInfo);
			}
		}
		return airSearchResult;
	}

	/**
	 * This will take care of Segment Information
	 *
	 * @param originDestinationOption
	 * @param isReturnSegment
	 * @return
	 */
	public List<SegmentInfo> parseSegmentInfo(OriginDestinationOptionType originDestinationOption,
			boolean isReturnSegment) {
		int segmentNo = 0;
		List<SegmentInfo> segmentInfoList = new ArrayList<>();
		for (BookFlightSegmentType flightSegment : originDestinationOption.getFlightSegment()) {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setSegmentNum(segmentNo);
			segmentInfo.setFlightDesignator(getFlightDesignator(flightSegment));
			AirlineInfo operatingAirline = getAirlineInfo(flightSegment);
			if (operatingAirline != null
					&& !operatingAirline.getCode().equals(segmentInfo.getFlightDesignator().getAirlineCode())) {
				segmentInfo.setOperatedByAirlineInfo(getAirlineInfo(flightSegment));
			}
			Integer stops = flightSegment.getStopQuantity().intValue();
			segmentInfo.setStops(stops);
			segmentInfo.setStopOverAirports(getStopOverAirportInformation(flightSegment));
			Long flightduration = (long) flightSegment.getElapsedTime();
			segmentInfo.setDuration(flightduration);
			String departCityCode = flightSegment.getDepartureAirport().getLocationCode().getStringLength1To8();
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(departCityCode));
			if (flightSegment.getDepartureAirport().getTerminalID() != null) {
				String departureTerminal = AirUtils
						.getTerminalInfo(flightSegment.getDepartureAirport().getTerminalID().getAlphaNumericString());
				segmentInfo.getDepartAirportInfo().setTerminal(departureTerminal);
			}
			String arrivalCityCode = flightSegment.getArrivalAirport().getLocationCode().getStringLength1To8();
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(arrivalCityCode));
			if (flightSegment.getArrivalAirport().getTerminalID() != null) {
				String arrivalTerminal = AirUtils
						.getTerminalInfo(flightSegment.getArrivalAirport().getTerminalID().getAlphaNumericString());
				segmentInfo.getArrivalAirportInfo().setTerminal(arrivalTerminal);
			}

			segmentInfo.setDepartTime(SabreUtils.getIsoDateTime(flightSegment.getDepartureDateTime()));
			segmentInfo.setArrivalTime(SabreUtils.getIsoDateTime(flightSegment.getArrivalDateTime()));
			segmentInfo.setIsReturnSegment(isReturnSegment);
			List<PriceInfo> priceInfoList = new ArrayList<>();
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
			priceInfo.getMiscInfo().setMarriageGrp(flightSegment.getMarriageGrp().getStringLength1To16());
			priceInfoList.add(priceInfo);
			segmentInfo.setPriceInfoList(priceInfoList);
			segmentInfoList.add(segmentInfo);
			segmentNo++;
		}
		return segmentInfoList;
	}

	public List<AirportInfo> getStopOverAirportInformation(BookFlightSegmentType flightSegment) {
		if (flightSegment.getStopAirports() != null
				&& !ArrayUtils.isEmpty(flightSegment.getStopAirports().getStopAirport())) {
			List<AirportInfo> stopOverAirports = new ArrayList<>();
			StopAirport_type0[] stopAirportList = flightSegment.getStopAirports().getStopAirport();
			for (StopAirport_type0 stopAirport : stopAirportList) {
				AirportInfo stopAirportInfo;
				String stopAirportLocationCode = stopAirport.getLocationCode().getStringLength1To8();
				stopAirportInfo = AirportHelper.getAirport(stopAirportLocationCode);
				stopOverAirports.add(stopAirportInfo);
			}
			return stopOverAirports;
		}
		return null;
	}

	private TripInfo getTripInfo(OriginDestinationOptionType[] originDestinationOptions,
			AirItineraryPricingInfo_type0[] airItineraryPricingInfoList, TPA_Extensions_type14 tpaExtensions,
			boolean parseAdditionalFare) {
		TripInfo tripInfo = new TripInfo();

		try {
			boolean isReturnSegment = false;
			for (OriginDestinationOptionType originDestinationOption : originDestinationOptions) {
				List<SegmentInfo> segmentInfos = parseSegmentInfo(originDestinationOption, isReturnSegment);
				if (CollectionUtils.isNotEmpty(segmentInfos)) {
					tripInfo.getSegmentInfos().addAll(segmentInfos);
					isReturnSegment = true;
				}
			}

			if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
				int priceOptionIndex = 0;
				this.parsePriceInfo(tripInfo, airItineraryPricingInfoList[0].getPTC_FareBreakdowns(),
						priceOptionIndex++, tpaExtensions);
				if (parseAdditionalFare) {
					if (tpaExtensions != null && tpaExtensions.getAdditionalFares() != null) {
						for (int i = 1; i < tpaExtensions.getAdditionalFares().length; i++) {
							this.parsePriceInfo(tripInfo, tpaExtensions.getAdditionalFares()[i]
									.getAirItineraryPricingInfo().getPTC_FareBreakdowns(), priceOptionIndex++,
									tpaExtensions);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
		}

		return tripInfo;
	}

	private FlightDesignator getFlightDesignator(BookFlightSegmentType flightSegment) {
		String marketingAirlineCode = flightSegment.getMarketingAirline().getCode();
		AirlineInfo airlineInfo = AirlineHelper.getAirlineInfo(marketingAirlineCode);
		String flightNumber = flightSegment.getFlightNumber().getFlightNumberType();
		String equipType = flightSegment.getEquipment()[0].getAirEquipType().getStringLength3();
		FlightDesignator flightDesignator = FlightDesignator.builder().flightNumber(flightNumber)
				.airlineInfo(airlineInfo).equipType(equipType).build();
		return flightDesignator;
	}

	private AirlineInfo getAirlineInfo(BookFlightSegmentType flightSegment) {
		String operatedByAirlineCode = flightSegment.getOperatingAirline().getCode();
		AirlineInfo operatedByAirline = AirlineHelper.getAirlineInfo(operatedByAirlineCode);
		return operatedByAirline;
	}

	private void parsePriceInfo(TripInfo tripInfo, PTC_FareBreakdowns_type0 ptcFareBreakdowns, int priceOptionIndex,
			TPA_Extensions_type14 tpaExtension) throws Exception {
		AtomicInteger priceIndex = new AtomicInteger(0);
		for (PTCFareBreakdownType ptcBreakDown : ptcFareBreakdowns.getPTC_FareBreakdown()) {
			String paxType = getPaxTypeCode(ptcBreakDown.getPassengerTypeQuantity().getCode().getPassengerCodeType(),
					priceIndex);

			if (PaxType.getPaxType(paxType) == null)
				throw new RuntimeException("Received invalid Paxtype " + paxType + " from supplier");

			FareDetail fareDetail =
					tripInfo.getSegmentInfos().get(0).getPriceInfo(priceOptionIndex, getConfiguration().getBasicInfo())
							.getFareDetail(paxType, new FareDetail());
			if (ptcBreakDown.getPassengerFare().getOBFees() != null) {
				Double initialFare = 0.0;
				for (OBFeeType oBFee : ptcBreakDown.getPassengerFare().getOBFees().getOBFee()) {
					if (fareDetail.getFareComponents().get(FareComponent.OB) != null) {
						initialFare = fareDetail.getFareComponents().get(FareComponent.OB);
					}
					fareDetail.getFareComponents().put(FareComponent.OB,
							initialFare + getAmountBasedOnCurrency(oBFee.getAmount().getMoney(),
									oBFee.getCurrencyCode().getAlphaLength3()));
				}
			}
			fareDetail.getFareComponents().put(FareComponent.BF,
					getAmountBasedOnCurrency(
							ptcBreakDown.getPassengerFare().getEquivFare().getAmount() != null
									? ptcBreakDown.getPassengerFare().getEquivFare().getAmount().getMoney()
									: ptcBreakDown.getPassengerFare().getBaseFare().getAmount().getMoney(),
							ptcBreakDown.getPassengerFare().getEquivFare().getAmount() != null
									? ptcBreakDown.getPassengerFare().getEquivFare().getCurrencyCode().getAlphaLength3()
									: ptcBreakDown.getPassengerFare().getBaseFare().getCurrencyCode()
											.getAlphaLength3()));
			fareDetail.getFareComponents().put(FareComponent.TF,
					getAmountBasedOnCurrency(ptcBreakDown.getPassengerFare().getTotalFare().getAmount().getMoney(),
							ptcBreakDown.getPassengerFare().getTotalFare().getCurrencyCode().getAlphaLength3()));
			if (Objects.nonNull(ptcBreakDown.getPassengerFare().getTaxes())
					&& ArrayUtils.isNotEmpty(ptcBreakDown.getPassengerFare().getTaxes().getTaxSummary())) {

				for (AirTaxSummaryType taxSummary : ptcBreakDown.getPassengerFare().getTaxes().getTaxSummary()) {

					FareComponent fareComponent = Enums
							.getIfPresent(GDSFareComponentMapper.class, taxSummary.getTaxCode().getStringLength1To16())
							.or(GDSFareComponentMapper.TX).getFareComponent();
					if (!Double.valueOf(taxSummary.getAmount()).isNaN()) {
						fareDetail.getFareComponents().put(fareComponent,
								fareDetail.getFareComponents().getOrDefault(fareComponent, 0.0)
										+ getAmountBasedOnCurrency(BigDecimal.valueOf(taxSummary.getAmount()),
												taxSummary.getLocalCurrency()));
					}
				}
			}

			if (Objects.nonNull(ptcBreakDown.getEndorsements().getNonRefundableIndicator()))
				fareDetail.setRefundableType(
						BooleanUtils.isTrue(ptcBreakDown.getEndorsements().getNonRefundableIndicator())
								? RefundableType.NON_REFUNDABLE.getRefundableType()
								: RefundableType.REFUNDABLE.getRefundableType());

			if (ptcBreakDown.getPassengerFare().getPenaltiesInfo() != null
					&& ArrayUtils.isNotEmpty(ptcBreakDown.getPassengerFare().getPenaltiesInfo().getPenalty())) {
				for (Penalty_type2 penalty : ptcBreakDown.getPassengerFare().getPenaltiesInfo().getPenalty()) {
					if ("Refund".equals(penalty.getType()) && "Before".equals(penalty.getApplicability())) {
						fareDetail.setRefundableType(BooleanUtils.isTrue(penalty.getRefundable())
								? RefundableType.REFUNDABLE.getRefundableType()
								: RefundableType.NON_REFUNDABLE.getRefundableType());
					}
				}
			}

			int fareBasisIndex = 0;
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				PriceInfo priceInfo = segmentInfo.getPriceInfo(priceOptionIndex, getConfiguration().getBasicInfo());
				// * , @ type indicates private fare applied
				priceInfo.setFareIdentifier(FareType.PUBLISHED);
				if (Objects.isNull(priceInfo.getMiscInfo().getAccountCode())
						&& Objects.nonNull(ptcBreakDown.getPrivateFareType())
						&& PRIVATE_FARE_INDICATOR.contains(ptcBreakDown.getPrivateFareType().toString())
						&& SabreUtils.isAccountCodeAvailable(ptcBreakDown, fareBasisIndex)) {
					String accountCode = ptcBreakDown.getFareBasisCodes().getFareBasisCode()[fareBasisIndex]
							.getAccountCode().getStringLength1To20();
					PriceMiscInfo miscInfo =
							PriceMiscInfo.builder().accountCode(accountCode).isPrivateFare(true).build();

					priceInfo.setMiscInfo(miscInfo);
					priceInfo.setFareIdentifier(FareType.CORPORATE);

					if (configuration != null
							&& MapUtils.isNotEmpty(configuration.getSupplierCredential().getFareTypes())) {
						if (configuration.getSupplierCredential().getFareTypes().get(accountCode) != null)
							priceInfo.setFareIdentifier(FareType.getEnumFromName(
									configuration.getSupplierCredential().getFareTypes().get(accountCode)));
					}
				}

				if (tpaExtension.getValidatingCarrier() != null)
					priceInfo.getMiscInfo().setPlatingCarrier(AirlineHelper
							.getAirlineInfo(tpaExtension.getValidatingCarrier().getCode().getCarrierCodeOrEmpty()));
				FareDetail segmentWiseFareDetail = priceInfo.getFareDetail(paxType, new FareDetail());
				segmentWiseFareDetail.setFareBasis(
						ptcBreakDown.getFareBasisCodes().getFareBasisCode()[fareBasisIndex].getStringLength1To16());
				segmentWiseFareDetail
						.setClassOfBooking(ptcBreakDown.getFareBasisCodes().getFareBasisCode()[fareBasisIndex]
								.getBookingCode().getBookingCode_type0());
				segmentWiseFareDetail.setCabinClass(CabinClass
						.getCabinClass(
								ptcBreakDown.getFareInfos().getFareInfo()[0].getTPA_Extensions().getCabin().getCabin())
						.orElse(null));
				fareBasisIndex++;
			}
			priceIndex.getAndIncrement();
		}

		parseBaggageInformation(tripInfo, ptcFareBreakdowns, priceOptionIndex);
		parseAdditionalPriceAttributes(tripInfo, ptcFareBreakdowns, priceOptionIndex);
	}

	private void parseAdditionalPriceAttributes(TripInfo tripInfo, PTC_FareBreakdowns_type0 ptcFareBreakdowns,
			int priceOptionIndex) {
		AtomicInteger priceIndex = new AtomicInteger(0);
		for (PTCFareBreakdownType ptcFareBreakdown : ptcFareBreakdowns.getPTC_FareBreakdown()) {
			int segmentIndex = 0;
			for (FareInfo_type0 fareInfo : ptcFareBreakdown.getFareInfos().getFareInfo()) {
				String type = getPaxTypeCode(
						ptcFareBreakdown.getPassengerTypeQuantity().getCode().getPassengerCodeType(), priceIndex);
				FareDetail fareDetail = tripInfo.getSegmentInfos().get(segmentIndex)
						.getPriceInfo(priceOptionIndex, getConfiguration().getBasicInfo())
						.getFareDetail(type, new FareDetail());
				fareDetail.setCabinClass(
						CabinClass.getCabinClass(fareInfo.getTPA_Extensions().getCabin().getCabin()).orElse(null));
				fareDetail.setSeatRemaining(fareInfo.getTPA_Extensions().getSeatsRemaining().getNumber());

				/*
				 * Meal will not be applicable if Meal component is not present in the BFM response or meal code
				 * included in PAID_MEAL_CODES.
				 */
				if (fareInfo.getTPA_Extensions().getMeal() != null
						&& fareInfo.getTPA_Extensions().getMeal().getCode() != null
						&& isFreeMeal(fareInfo.getTPA_Extensions().getMeal().getCode())) {
					fareDetail.setIsMealIncluded(true);
				}

				segmentIndex++;
			}
			priceIndex.getAndIncrement();
		}
	}

	private void parseBaggageInformation(TripInfo tripInfo, PTC_FareBreakdowns_type0 ptcFareBreakdowns,
			int priceOptionIndex) {
		try {
			int segmentIndex = 0;
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				AtomicInteger priceIndex = new AtomicInteger(0);
				for (PTCFareBreakdownType ptcFareBreakdown : ptcFareBreakdowns.getPTC_FareBreakdown()) {
					String type = getPaxTypeCode(
							ptcFareBreakdown.getPassengerTypeQuantity().getCode().getPassengerCodeType(), priceIndex);
					FareDetail fareDetail =
							segmentInfo.getPriceInfo(priceOptionIndex, getConfiguration().getBasicInfo())
									.getFareDetail(type, new FareDetail());
					boolean isBaggageInfoAvailable = false;
					if (ptcFareBreakdown.getPassengerFare().getTPA_Extensions() != null) {
						BaggageInformationListType list =
								ptcFareBreakdown.getPassengerFare().getTPA_Extensions().getBaggageInformationList();
						if (Objects.nonNull(list)) {
							BaggageInfo baggageInfo = new BaggageInfo();
							for (BaggageInformationType baggageInformationType : list.getBaggageInformation()) {
								isBaggageInfoAvailable = true;
								for (Segment_type1 segment : baggageInformationType.getSegment()) {
									if (segment.getId() == segmentIndex) {
										baggageInfo.setAllowance(
												getBaggageAllowance(baggageInformationType.getAllowance()[0]));
									}
								}
							}
							fareDetail.setBaggageInfo(baggageInfo);
						}
					}
					if (!isBaggageInfoAvailable) {
						log.debug("Baggage information is not available for tripinfo {}",
								getBaggageTripInfo(tripInfo, fareDetail));
					}
					priceIndex.getAndIncrement();
				}
				segmentIndex++;
			}
		} catch (Exception e) {
			log.info("Unable to fetch baggage information for searchQuery {}", searchQuery);
		}
	}

	public String getBaggageTripInfo(TripInfo tripInfo, FareDetail fareDetail) {
		StringBuffer buff = new StringBuffer();
		for (SegmentInfo sInfo : tripInfo.getSegmentInfos()) {
			buff.append(sInfo.getDepartureAirportCode() + "-" + sInfo.getArrivalAirportCode() + "-"
					+ sInfo.getAirlineCode(false) + "-" + sInfo.getFlightNumber()).append("#");
		}
		buff.append(fareDetail.getCabinClass() + "-" + fareDetail.getClassOfBooking() + "-" + fareDetail.getFareBasis()
				+ "-" + tripInfo.getDepartureTime() + "-" + tripInfo.getArrivalTime());
		return buff.toString();
	}

	public String getBaggageAllowance(Allowance_type0 allowance) {
		if (allowance.getUnit() != null) {
			return allowance.getWeight() + " " + allowance.getUnit();
		} else if (Objects.nonNull(allowance.getPieces())) {
			return allowance.getPieces() + " Piece";
		}

		return allowance.getDescription1();
	}

	private POS_Type getPOSConfiguration() {
		POS_Type posType = new POS_Type();
		SourceType sourceArray[] = new SourceType[1];
		SourceType source = new SourceType();
		source.setPseudoCityCode(getStringLength1To16(configuration.getSupplierCredential().getPcc()));
		UniqueID_Type requestorId = new UniqueID_Type();
		requestorId.setID(getStringLenth1To31("1"));
		requestorId.setType((getOTACodeType("1")));
		CompanyNameType nameType = new CompanyNameType();
		nameType.setCode("TN");
		nameType.setString("TN");
		requestorId.setCompanyName(nameType);
		SourceGroup sourceGroup = new SourceGroup();
		sourceGroup.setRequestorID(requestorId);
		source.setSourceGroup(sourceGroup);
		sourceArray[0] = source;
		posType.setSource(sourceArray);
		return posType;
	}


	private AirSearchPrefsType getTravelPreferenceConfiguration(AirSearchQuery searchQuery) {
		AirSearchPrefsType travelPreference = new AirSearchPrefsType();
		TPA_Extensions_type2 tpaExtension = new TPA_Extensions_type2();
		LongConnectTime_type0 longConnectTime = new LongConnectTime_type0();
		longConnectTime.setEnable(true);
		tpaExtension.setLongConnectTime(longConnectTime);
		travelPreference.setValidInterlineTicket(true);
		Baggage_type0 baggageType = new Baggage_type0();
		baggageType.setRequestType(BaggageRequestType.A);
		travelPreference.setBaggage(baggageType);
		if (!CabinClass.ECONOMY.equals(searchQuery.getCabinClass())) {
			travelPreference.setETicketDesired(true);
			CabinPrefType[] cabinPrefTypeArr = new CabinPrefType[1];
			CabinPrefType cabinPrefType = new CabinPrefType();
			cabinPrefType.setCabin(SabreUtils.getCabinType(searchQuery));
			cabinPrefTypeArr[0] = cabinPrefType;
			travelPreference.setCabinPref(cabinPrefTypeArr);
			JumpCabinLogicType jumpLogic = new JumpCabinLogicType();
			jumpLogic.setDisabled(true);
			tpaExtension.setJumpCabinLogic(jumpLogic);
			KeepSameCabinType keepSameCabin = new KeepSameCabinType();
			keepSameCabin.setEnabled(true);
			tpaExtension.setKeepSameCabin(keepSameCabin);
		}
		TripType_type0 tripType = new TripType_type0();
		if (searchQuery.isReturn() && searchQuery.isIntl()) {
			tripType.setValue(AirTripType.Return);
		} else if (searchQuery.isIntl() && searchQuery.isMultiCity()) {
			tripType.setValue(AirTripType.Other);
			LongConnectTime_type0 lct = new LongConnectTime_type0();
			lct.setEnable(true);
			if (sourceConfiguration.getMinimumConnectingTime() != null) {
				lct.setMin(sourceConfiguration.getMinimumConnectingTime().shortValue());
			}
			tpaExtension.setLongConnectTime(lct);
		} else {
			tripType.setValue(AirTripType.OneWay);
		}
		setFlexiFareParams(tpaExtension);
		tpaExtension.setTripType(tripType);

		if (searchQuery.getPreferredAirline() != null) {
			int size = searchQuery.getPreferredAirline().size();
			int index = 0;
			CompanyNamePrefType[] companyPrefArray = new CompanyNamePrefType[size];
			for (AirlineInfo preferredAirline : searchQuery.getPreferredAirline()) {
				if (preferredAirline.getCode() != null) {
					CompanyNamePrefType companyPref = new CompanyNamePrefType();
					companyPref.setString(preferredAirline.getCode());
					companyPrefArray[index] = companyPref;
					index++;
				}
			}
			travelPreference.setVendorPref(companyPrefArray);
		}
		travelPreference.setTPA_Extensions(tpaExtension);
		return travelPreference;
	}


	public void setFlexiFareParams(TPA_Extensions_type2 tpaExtension) {
		if (CabinClass.ECONOMY.equals(searchQuery.getCabinClass()) && searchQuery.getIsDomestic()) {
			FlexibleFaresType flexiFare = new FlexibleFaresType();
			FareParameters_type0[] fareParameter = new FareParameters_type0[3];
			fareParameter[0] = getFareParameter(CabinType.Y);
			fareParameter[1] = getFareParameter(CabinType.S);
			fareParameter[2] = getFareParameter(CabinType.C);
			flexiFare.setFareParameters(fareParameter);
			tpaExtension.setFlexibleFares(flexiFare);
		}
	}


	public FareParameters_type0 getFareParameter(CabinType cabinType) {
		FareParameters_type0 fareParameter = new FareParameters_type0();
		if (CollectionUtils.isEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			PublicFare_type0 publicFare = new PublicFare_type0();
			publicFare.setInd(true);
			fareParameter.setPublicFare(publicFare);
		} else {
			int size = configuration.getSupplierAdditionalInfo().getAccountCodes().size();
			FareParametersChoice_type0[] choice_type0s = new FareParametersChoice_type0[size];
			PrivateFare_type0 privateFare = new PrivateFare_type0();
			privateFare.setInd(true);
			fareParameter.setPrivateFare(privateFare);
			List<AccountCode_type0> accountCodes = new ArrayList<>();
			int index = 0;
			for (String code : configuration.getSupplierAdditionalInfo().getAccountCodes()) {
				FareParametersChoice_type0 choice_type0 = new FareParametersChoice_type0();
				AccountCode_type0 accountCode = new AccountCode_type0();
				accountCode.setCode(getStringLenth1To20(code));
				accountCodes.add(accountCode);
				choice_type0s[index++] = choice_type0;
			}
			fareParameter.setFareParametersChoice_type0(choice_type0s);
		}
		Cabin_type0 cabin = new Cabin_type0();
		cabin.setType(cabinType);
		fareParameter.setCabin(cabin);

		VoluntaryChangesSMPType voluntaryChanges = new VoluntaryChangesSMPType();
		voluntaryChanges.setMatch(Match_type0.Info);

		fareParameter.setVoluntaryChanges(voluntaryChanges);
		return fareParameter;
	}

	private TPA_Extensions_type5 getTPAExtensionsConfiguration() {
		TPA_Extensions_type5 tpaExtensions = new TPA_Extensions_type5();
		TransactionType transactionType = new TransactionType();
		RequestType_type0 requestType = new RequestType_type0();
		requestType.setName(getItinCount());
		transactionType.setRequestType(requestType);
		tpaExtensions.setIntelliSellTransaction(transactionType);
		if (searchQuery.getIsDomestic() && searchQuery.isReturn()) {
			MultiTicket_type0 multiTicket = new MultiTicket_type0();
			multiTicket.setDisplayPolicy(DisplayPolicy_type0.SOW);
			tpaExtensions.setMultiTicket(multiTicket);
		}
		return tpaExtensions;
	}


	public String getItinCount() {
		int flightCount = 100;
		if (sourceConfiguration != null) {
			if (searchQuery.getIsDomestic()) {
				flightCount = ObjectUtils.firstNonNull(sourceConfiguration.getDomflightsCount(),
						sourceConfiguration.getFlightsCount(), flightCount);
			} else {
				flightCount = ObjectUtils.firstNonNull(sourceConfiguration.getFlightsCount(),
						sourceConfiguration.getDomflightsCount(), flightCount);
			}
		}
		return flightCount + ITINCOUNT;
	}

	private OriginDestinationInformation_type0[] getOriginDestinationInformationConfiguration(
			AirSearchQuery searchQuery) {
		OriginDestinationInformation_type0[] originDestinationInformationList =
				new OriginDestinationInformation_type0[searchQuery.getRouteInfos().size()];
		int index = 0;
		for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
			TravelDateTimeTypeChoice_type0 travelDateTime = new TravelDateTimeTypeChoice_type0();
			IntDateTime dateTime = new IntDateTime();
			dateTime.setIntDateTime(routeInfo.getTravelDate().toString() + "T00:00:00");
			travelDateTime.setDepartureDateTime(dateTime);
			OriginDestinationInformation_type0 originDest = new OriginDestinationInformation_type0();
			originDest.setTravelDateTimeTypeChoice_type0(travelDateTime);
			OriginLocation_type0 originLocation = new OriginLocation_type0();
			LocationCode_type0 locationCode = new LocationCode_type0();
			locationCode.setLocationCode_type0(routeInfo.getFromCityOrAirport().getCode());
			originLocation.setLocationCode(locationCode);
			originLocation.setString(routeInfo.getFromCityOrAirport().getCode());
			originDest.setOriginLocation(originLocation);
			DestinationLocation_type0 destLocation = new DestinationLocation_type0();
			LocationCode_type0 destLocationCode = new LocationCode_type0();
			destLocationCode.setLocationCode_type0(routeInfo.getToCityOrAirport().getCode());
			destLocation.setString(routeInfo.getToCityOrAirport().getCode());
			destLocation.setLocationCode(destLocationCode);
			destLocation.setString(routeInfo.getToCityOrAirport().getCode());
			originDest.setDestinationLocation(destLocation);
			originDestinationInformationList[index] = originDest;
			index++;
		}
		return originDestinationInformationList;
	}


	private TravelerInfoSummaryType getTravellerInfoSummaryConfiguration(AirSearchQuery searchQuery) {
		TravelerInfoSummaryType travelerInfoSummaryType = new TravelerInfoSummaryType();
		List<TravelerInformationType> travelerInformationTypes = new ArrayList<>();
		TravelerInformationType travelerInformationType = new TravelerInformationType();
		searchQuery.getPaxInfo().forEach((paxType, quantity) -> {
			if (quantity > 0) {
				PassengerTypeQuantityType passengerTypeQuantity = new PassengerTypeQuantityType();
				passengerTypeQuantity.setCode(getPaxTypeCode(paxType.getType()));
				passengerTypeQuantity.setQuantity(getNumeric1To999(quantity));
				TPA_Extensions_type1 tpaExtension = new TPA_Extensions_type1();
				VoluntaryChangesSMPType voluntaryChanges = new VoluntaryChangesSMPType();
				voluntaryChanges.setMatch(Match_type0.Info);
				tpaExtension.setVoluntaryChanges(voluntaryChanges);
				passengerTypeQuantity.setTPA_Extensions(tpaExtension);
				travelerInformationType.addPassengerTypeQuantity(passengerTypeQuantity);
			}
		});
		travelerInformationTypes.add(travelerInformationType);
		travelerInfoSummaryType.setAirTravelerAvail(travelerInformationTypes.toArray(new TravelerInformationType[0]));
		if (CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			travelerInfoSummaryType.setPriceRequestInformation(getPriceRequestInformation());
		}
		travelerInfoSummaryType.setSeatsRequested(getSeatsRequested());
		return travelerInfoSummaryType;
	}

	private NonNegativeInteger[] getSeatsRequested() {
		NonNegativeInteger[] integerArray = new NonNegativeInteger[1];
		NonNegativeInteger nonNegativeInteger =
				new NonNegativeInteger(String.valueOf(AirUtils.getPaxCount(searchQuery, false)));
		integerArray[0] = nonNegativeInteger;
		return integerArray;
	}

	private Numeric1To999 getNumeric1To999(Integer num) {
		Numeric1To999 object = new Numeric1To999();
		object.setNumeric1To999(BigInteger.valueOf(num));
		return object;
	}


	private PriceRequestInformationType getPriceRequestInformation() {
		PriceRequestInformationType priceReqInfo = new PriceRequestInformationType();
		priceReqInfo.setPriceRequestInformationTypeChoice_type0(getNegotiatedFares());

		TPA_Extensions_type3 tpaExtension = new TPA_Extensions_type3();
		if (CollectionUtils.isNotEmpty(configuration.getSupplierAdditionalInfo().getAccountCodes())) {
			int size = configuration.getSupplierAdditionalInfo().getAccountCodes().size();
			PriceRequestInformationTypeChoice_type0[] choices = new PriceRequestInformationTypeChoice_type0[size];
			int index = 0;
			for (String accCode : configuration.getSupplierAdditionalInfo().getAccountCodes()) {
				PriceRequestInformationTypeChoice_type0 choice = new PriceRequestInformationTypeChoice_type0();
				AccountCode_type2 accountCode = new AccountCode_type2();
				StringLength1To20 str = new StringLength1To20();
				str.setStringLength1To20(accCode);
				accountCode.setCode(str);
				choice.setAccountCode(accountCode);
				choices[index++] = choice;
			}

			PrivateFare_type1 privateFare = new PrivateFare_type1();
			privateFare.setInd(true);
			tpaExtension.setPrivateFare(privateFare);
			priceReqInfo.setTPA_Extensions(tpaExtension);
			priceReqInfo.setPriceRequestInformationTypeChoice_type0(choices);
		} else {
			PublicFare_type1 publicFare = new PublicFare_type1();
			publicFare.setInd(true);
			tpaExtension.setPublicFare(publicFare);
			priceReqInfo.setTPA_Extensions(tpaExtension);
		}
		String currencyCode = getCurrencyCode();
		if (StringUtils.isNotBlank(currencyCode)) {
			priceReqInfo.setCurrencyCode(getStringLen3(currencyCode));
		}
		return priceReqInfo;
	}

	private PriceRequestInformationTypeChoice_type0[] getNegotiatedFares() {
		PriceRequestInformationTypeChoice_type0[] negotiatedArray =
				new PriceRequestInformationTypeChoice_type0[configuration.getSupplierAdditionalInfo().getAccountCodes()
						.size()];
		int index = 0;
		for (String accountCode : configuration.getSupplierAdditionalInfo().getAccountCodes()) {
			PriceRequestInformationTypeChoice_type0 negotiatedfare = new PriceRequestInformationTypeChoice_type0();
			negotiatedfare.setNegotiatedFareCode(getNegotiatedFareCode(accountCode));
			negotiatedArray[index] = negotiatedfare;
			index++;
		}
		return negotiatedArray;
	}

	private NegotiatedFareCode_type0 getNegotiatedFareCode(String accountCode) {
		// TODO Auto-generated method stub
		return null;
	}


}
