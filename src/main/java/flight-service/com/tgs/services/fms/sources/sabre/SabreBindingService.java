package com.tgs.services.fms.sources.sabre;

import com.sabre.webservices.websvc.ARUNK_ServiceStub;
import com.sabre.webservices.websvc.AirTicketServiceStub;
import com.sabre.webservices.websvc.BargainFinderMaxServiceStub;
import com.sabre.webservices.websvc.ContextChangeServiceStub;
import com.sabre.webservices.websvc.OTA_CancelServiceStub;
import com.sabre.webservices.websvc.DeletePriceQuoteServiceStub;
import com.sabre.webservices.websvc.DesignatePrinterServiceStub;
import com.sabre.webservices.websvc.EndTransactionServiceStub;
import com.sabre.webservices.websvc.EnhancedAirBookServiceStub;
import com.sabre.webservices.websvc.OTA_AirAvailServiceStub;
import com.sabre.webservices.websvc.OTA_AirPriceServiceStub;
import com.sabre.webservices.websvc.OTA_AirRulesServiceStub;
import com.sabre.webservices.websvc.PassengerDetailsServiceStub;
import com.sabre.webservices.websvc.SpecialServiceServiceStub;
import com.sabre.webservices.websvc.SabreCommandLLSServiceStub;
import com.sabre.webservices.websvc.SessionCloseRQServiceStub;
import com.sabre.webservices.websvc.SessionCreateRQServiceStub;
import com.sabre.webservices.websvc.StructureFareRulesRQStub;
import com.sabre.webservices.websvc.TokenCreateRQServiceStub;
import com.sabre.webservices.websvc.TravelItineraryReadServiceStub;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AirSourceConstants;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import java.util.List;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;


@Setter
@Getter
@Slf4j
@Builder
final class SabreBindingService {

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	private SupplierConfiguration configuration;


	public TokenCreateRQServiceStub getTokenService() {
		TokenCreateRQServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize TokenCreateRQService Stub Started");
				serviceStub = new TokenCreateRQServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize TokenCreateRQService Stubs Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public SessionCreateRQServiceStub getSessionService() {
		SessionCreateRQServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize SessionCreateRQService Stub Started");
				serviceStub = new SessionCreateRQServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize SessionCreateRQService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public TravelItineraryReadServiceStub getTravelItin() {
		TravelItineraryReadServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize TravelItineraryReadServiceStub Stub Started");
				serviceStub = new TravelItineraryReadServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize TravelItineraryReadServiceStub Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public EndTransactionServiceStub getEndTS() {
		EndTransactionServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize EndTransactionService Stub Started");
				serviceStub = new EndTransactionServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize EndTransactionService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public SabreCommandLLSServiceStub getSabreCommand() {
		SabreCommandLLSServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize SabreCommandLLSService Stub Started");
				serviceStub = new SabreCommandLLSServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize SabreCommandLLSService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public ContextChangeServiceStub getContextChange() {
		ContextChangeServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize ContextChangeService Stub Started");
				serviceStub = new ContextChangeServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize ContextChangeService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public SessionCloseRQServiceStub getSessionClose() {
		SessionCloseRQServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize SessionCloseRQService Stub Started");
				serviceStub = new SessionCloseRQServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize SessionCloseRQService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public DesignatePrinterServiceStub getDesignateService() {
		DesignatePrinterServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize DesignatePrinterService Stub Started");
				serviceStub = new DesignatePrinterServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize DesignatePrinterService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public BargainFinderMaxServiceStub getBfmS() {
		BargainFinderMaxServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize BargainFinderMaxService Stub Started");
				serviceStub = new BargainFinderMaxServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize BargainFinderMaxService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public OTA_AirAvailServiceStub getClassService() {
		OTA_AirAvailServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize OTA_AirAvailService Stub Started");
				serviceStub = new OTA_AirAvailServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize OTA_AirAvailService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public PassengerDetailsServiceStub getPsd() {
		PassengerDetailsServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize PassengerDetailsService Stub Started");
				serviceStub = new PassengerDetailsServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize PassengerDetailsService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public DeletePriceQuoteServiceStub getDeletePrice() {
		DeletePriceQuoteServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize DeletePriceQuoteService Stub Started");
				serviceStub = new DeletePriceQuoteServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize DeletePriceQuoteService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public OTA_AirPriceServiceStub getOtaPrcie() {
		OTA_AirPriceServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize OTA_AirPriceService Stub Started");
				serviceStub = new OTA_AirPriceServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize OTA_AirPriceService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public AirTicketServiceStub getAirTicket() {
		AirTicketServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize AirTicketService Stub Started");
				serviceStub = new AirTicketServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize AirTicketService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public ARUNK_ServiceStub getArunkService() {
		ARUNK_ServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize ARUNK_Service Stub Started");
				serviceStub = new ARUNK_ServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize ARUNK_Service Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public StructureFareRulesRQStub getFareRuleSS() {
		StructureFareRulesRQStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize StructureFareRulesRQ Stub Started");
				serviceStub = new StructureFareRulesRQStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize StructureFareRulesRQ Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public OTA_AirRulesServiceStub getRulesService() {
		OTA_AirRulesServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize OTA_AirRulesService Stub Started");
				serviceStub = new OTA_AirRulesServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize OTA_AirRulesService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public EnhancedAirBookServiceStub getEab() {
		EnhancedAirBookServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize EnhancedAirBookService Stub Started");
				serviceStub = new EnhancedAirBookServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize EnhancedAirBookService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public OTA_CancelServiceStub getCancelService() {
		OTA_CancelServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize OTA_CancelServiceStub Stub Started");
				serviceStub = new OTA_CancelServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize OTA_CancelServiceStub Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public SpecialServiceServiceStub getSpecialService() {
		SpecialServiceServiceStub serviceStub = null;
		if (serviceStub == null) {
			try {
				log.debug("Initialize SpecialServiceService Stub  Started");
				serviceStub = new SpecialServiceServiceStub(configuration.getSupplierCredential().getUrl());
				log.debug("Initialize SpecialServiceService Stub Done");
			} catch (AxisFault e) {
				throw new CustomGeneralException(e.getMessage());
			}
		}
		setConnectionProperties(serviceStub);
		return serviceStub;
	}

	public void setConnectionProperties(Stub stub) {
		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			stub._getServiceClient().getOptions()
					.setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			setConnectionManager(stub);
		}
	}

	public void updateOptions(Stub stub) {
		if (stub != null) {
			ServiceClient client = stub._getServiceClient();
			Options options = client.getOptions();
			options.setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
			client.setOptions(options);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(20);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}

}
