package com.tgs.services.fms.sources.sabre;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import javax.xml.datatype.XMLGregorianCalendar;
import com.sabre.services.sp.pd.v3_4.*;
import com.sabre.services.sp.pd.v3_4.AdvancePassenger_type0;
import com.sabre.services.sp.pd.v3_4.Airline_type1;
import com.sabre.services.sp.pd.v3_4.CustLoyalty_type0;
import com.sabre.services.sp.pd.v3_4.Document_type0;
import com.sabre.services.sp.pd.v3_4.EndTransaction_type0;
import com.sabre.services.sp.pd.v3_4.Gender_type0;
import com.sabre.services.sp.pd.v3_4.PersonName_type1;
import com.sabre.services.sp.pd.v3_4.Service_type0;
import com.sabre.services.sp.pd.v3_4.Source_type0;
import com.sabre.services.sp.pd.v3_4.SpecialServiceInfo_type0;
import com.sabre.services.sp.pd.v3_4.Ticketing_type0;
import com.sabre.services.sp.pd.v3_4.VendorPrefs_type1;
import com.sabre.webservices.sabrexml._2011._10.*;
import com.sabre.webservices.sabrexml._2011._10.AirItineraryPricingInfo_type0;
import com.sabre.webservices.sabrexml._2011._10.Airline_type0;
import com.sabre.webservices.sabrexml._2011._10.CC_Info_type0;
import com.sabre.webservices.sabrexml._2011._10.Commission_type0;
import com.sabre.webservices.sabrexml._2011._10.PaymentCard_type0;
import com.sabre.webservices.sabrexml._2011._10.PersonName_type2;
import com.sabre.webservices.sabrexml._2011._10.PriceQuote_type0;
import com.sabre.webservices.sabrexml._2011._10.TourCode_type0;
import com.sabre.webservices.sabrexml._2011._10.VendorPrefs_type0;
import com.sabre.webservices.websvc.*;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.axis2.databinding.types.YearMonth;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import com.sabre.services.res.tir.v3_9.FlightSegment_type2;
import com.sabre.services.res.tir.v3_9.Item_type0;
import com.sabre.services.res.tir.v3_9.ItineraryInfo_type0;
import com.sabre.services.res.tir.v3_9.SupplierRef_type0;
import com.sabre.services.res.tir.v3_9.Ticketing_type1;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRS;
import com.sabre.services.sp.common.simple.v3.FullDate;
import com.sabre.services.stl_messagecommon.v02_01.CompletionCodes;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.CountryInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
@Setter
final class SabreBookingManager extends SabreServiceManager {

	private CreditCardInfo creditCard;

	protected static final String AIRTICKET_RETRY = "PNR HAS BEEN UPDATED-IGN AND RETRY";

	protected LocalDateTime ticketingTimelimit;

	protected static final BigInteger WAIT_INTERVAL = new BigInteger("100");

	@Builder.Default
	private String crsPnr = null;

	protected GeneralServiceCommunicator gmsCommunicator;

	protected boolean updatePassengerDetails(GstInfo gstInfo, List<SegmentInfo> bookingSegmentsCopy) {
		PassengerDetailsServiceStub serviceStub = bindingService.getPsd();
		boolean isSuccess = false;
		listener.setType(AirUtils.getLogType("PassengerDetails", configuration));
		PassengerDetailsRQ passengerDetailRQ = null;
		try {
			passengerDetailRQ = new PassengerDetailsRQ();
			passengerDetailRQ.setIgnoreOnError(true);
			passengerDetailRQ.setVersion(getVesrionType0(SabreConstants.PASSENGER_DETAILS_VERSION));
			passengerDetailRQ.setTravelItineraryAddInfoRQ(getTravelItineraryAddInfo(bookingSegmentsCopy));
			passengerDetailRQ.setSpecialReqDetails(getSpecialServiceReq(gstInfo));
			passengerDetailRQ.setPostProcessing(getPassengerDetailPostProcess());
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.PASSENGER_DETAILS, SabreConstants.CONVERSATION_ID);
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			PassengerDetailsRS passengerDetailsRs =
					serviceStub.passengerDetailsRQ(passengerDetailRQ, messageHeader, getSecurity());
			isAnyWarning(passengerDetailsRs.getApplicationResults());
			isAnyCritical(passengerDetailsRs.getApplicationResults());
			if (passengerDetailsRs.getApplicationResults().getStatus() == CompletionCodes.Complete) {
				crsPnr = passengerDetailsRs.getItineraryRef().getID();
				log.info("Crs pnr {} for bookingid {}", crsPnr, bookingId);
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	protected boolean airPrice(List<SegmentInfo> bookingSegmentsCopy) {
		boolean isSuccess = false;
		OTA_AirPriceServiceStub serviceStub = bindingService.getOtaPrcie();
		OTA_AirPriceRQ airPriceRQ = new OTA_AirPriceRQ();
		OTA_AirPriceRS airPriceRS = null;
		listener.setType(AirUtils.getLogType("AirPrice", configuration));
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			airPriceRQ.setVersion(SabreConstants.AIR_PRICE_VERSION);
			PriceRequestInformation_type0 priceRequestInfo = new PriceRequestInformation_type0();
			priceRequestInfo.setRetain(true);
			priceRequestInfo.setOptionalQualifiers(getOptionalQualifiers(bookingSegmentsCopy));
			airPriceRQ.setPriceRequestInformation(priceRequestInfo);
			MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.AIR_PRICE, SabreConstants.CONVERSATION_ID);
			airPriceRS = serviceStub.oTA_AirPriceRQ(airPriceRQ, messageHeaderHolder, getSecurity());
			isAnyWarning(airPriceRS.getApplicationResults());
			isAnyCritical(airPriceRS.getApplicationResults());
			com.sabre.services.stl_header.v120.CompletionCodes applicationStatus =
					airPriceRS.getApplicationResults().getStatus();
			if (applicationStatus == com.sabre.services.stl_header.v120.CompletionCodes.Complete) {
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

		return isSuccess;
	}

	protected boolean specialService(List<SegmentInfo> segmentInfos) {
		boolean isSSRAdditionSuccess = false;
		SpecialServiceServiceStub serviceStub = bindingService.getSpecialService();
		listener.setType(AirUtils.getLogType("SpecialServiceSSR", configuration));
		try {
			SpecialServiceRQ specialServiceRequest = new SpecialServiceRQ();
			com.sabre.webservices.sabrexml._2011._10.SpecialServiceInfo_type0 specialServiceInfo =
					new com.sabre.webservices.sabrexml._2011._10.SpecialServiceInfo_type0();
			int nameNumber = 1;
			for (int index = 0; index < segmentInfos.size(); index++) {
				List<FlightTravellerInfo> bookingTravellerInfos =
						segmentInfos.get(index).getBookingRelatedInfo().getTravellerInfo();
				for (FlightTravellerInfo traveller : bookingTravellerInfos) {
					parseService(specialServiceInfo, traveller, index + 1, StringUtils.join(nameNumber, ".1"));
					nameNumber++;
				}
				nameNumber = 1;
			}

			// SpecialServiceLLSRQ should be called if any special services are selected.
			if (ArrayUtils.isNotEmpty(specialServiceInfo.getService())) {
				serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				specialServiceRequest.setSpecialServiceInfo(specialServiceInfo);
				specialServiceRequest.setVersion(SabreConstants.SPECIAL_SERVICE_VERSION);
				MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
						SabreConstants.SPECIAL_SERVICE, SabreConstants.CONVERSATION_ID);
				SpecialServiceRS response =
						serviceStub.specialServiceRQ(specialServiceRequest, messageHeaderHolder, getSecurity());
				isAnyWarning(response.getApplicationResults());
				isAnyCritical(response.getApplicationResults());
				if (response.getApplicationResults()
						.getStatus() == com.sabre.services.stl_header.v120.CompletionCodes.Complete) {
					isSSRAdditionSuccess = true;
				}
			}
		} catch (RemoteException r) {
			throw new SupplierRemoteException(r);
		} finally {

		}
		return isSSRAdditionSuccess;
	}

	private OptionalQualifiers_type0 getOptionalQualifiers(List<SegmentInfo> bookingSegmentsCopy) {
		OptionalQualifiers_type0 optionalQualifiers = new OptionalQualifiers_type0();
		PricingQualifiers_type0 pricingQualifiers = new PricingQualifiers_type0();
		List<PassengerType_type0> passengerTypeList = new ArrayList<PassengerType_type0>();
		for (PaxType paxType : PaxType.values()) {
			List<FlightTravellerInfo> paxwiseTravellerInfoList = AirUtils.getParticularPaxTravellerInfo(
					bookingSegmentsCopy.get(0).getBookingRelatedInfo().getTravellerInfo(), paxType);
			int paxQuantity = paxwiseTravellerInfoList.size();
			if (paxQuantity > 0) {
				PassengerType_type0 passengerType = new PassengerType_type0();
				passengerType.setCode(paxType.getType());
				passengerType.setQuantity(Integer.toString(paxQuantity));
				passengerTypeList.add(passengerType);
			}
		}
		pricingQualifiers.setPassengerType(passengerTypeList.toArray(new PassengerType_type0[0]));
		String accountCode = SabreUtils.getAccountCodeFromTrip(segmentInfos);
		String[] accountCodeArray = new String[1];
		if (StringUtils.isNotEmpty(accountCode)) {
			Account_type0 account = new Account_type0();
			accountCodeArray[0] = accountCode;
			account.setCode(accountCodeArray);
			pricingQualifiers.setAccount(account);
			FareOptions_type0 fareOptions = new FareOptions_type0();
			fareOptions.setPrivate(true);
			pricingQualifiers.setFareOptions(fareOptions);
		}

		pricingQualifiers.setCurrencyCode(getCurrencyCode());
		optionalQualifiers.setPricingQualifiers(pricingQualifiers);
		MiscQualifiers_type0 miscQualifier = new MiscQualifiers_type0();
		if (segmentInfos.get(0).getPriceInfo(0).getMiscInfo() != null
				&& segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getIata() != null) {
			Commission_type0 commission = new Commission_type0();
			String iataCommission = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getIata().toString();
			commission.setPercent(iataCommission);
			miscQualifier.setCommission(commission);
			log.info("Applying IATA Commission on block booking {} commission percent {}", bookingId, iataCommission);
		}
		optionalQualifiers.setMiscQualifiers(miscQualifier);
		return optionalQualifiers;
	}

	// To do get from orders
	protected boolean airTicket(List<SegmentInfo> bookingSegmentsCopy) {
		boolean isSuccess = false;
		AirTicketServiceStub serviceStub = bindingService.getAirTicket();
		listener.setType(AirUtils.getLogType("AirTicket", configuration));
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			String marketingAirline = segmentInfos.get(0).getPlatingCarrier(null);
			AirTicketRQ airTicketRQ = new AirTicketRQ();
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.AIR_TICKET, SabreConstants.CONVERSATION_ID);
			airTicketRQ.setVersion(airTicketRQ.getVersion());
			airTicketRQ.setNumResponses(BigInteger.valueOf(1));
			airTicketRQ.setOptionalQualifiers(getAirTicketOptionalQualifiers(marketingAirline, bookingSegmentsCopy));
			AirTicketRS airTicketRS = serviceStub.airTicketRQ(airTicketRQ, messageHeader, getSecurity());
			isAnyWarning(airTicketRS.getApplicationResults());
			String criticalMessage = isAnyCritical(airTicketRS.getApplicationResults());
			if (airTicketRS.getApplicationResults()
					.getStatus() == com.sabre.services.stl_header.v120.CompletionCodes.Complete) {
				isSuccess = true;
			} else {
				if (criticalMessage.contains(AIRTICKET_RETRY)) {
					// Ignore Command - To retry the booking
					sabreCommand(SabreConstants.IGNORE_RETRY_COMMAND);
				}
			}
		} catch (RemoteException r) {
			throw new SupplierRemoteException(r);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	private OptionalQualifiers_type0 getAirTicketOptionalQualifiers(String marketingAirline,
			List<SegmentInfo> bookingSegmentsCopy) {
		OptionalQualifiers_type0 optionalQualifiers = new OptionalQualifiers_type0();
		optionalQualifiers.setFlightQualifiers(getFlightQualifiers(marketingAirline));
		optionalQualifiers.setFOP_Qualifiers(getFOPQualifiers());
		MiscQualifiers_type0 miscQualifiers = new MiscQualifiers_type0();
		Ticket_type0 ticket = new Ticket_type0();
		ticket.setLocalType("ETR");
		setTourCode(miscQualifiers);
		miscQualifiers.setLocalTicket(ticket);
		PricingQualifiers_type0 pricingQualifiers = new PricingQualifiers_type0();
		PriceQuote_type0 priceQuote = new PriceQuote_type0();
		Record_type0 record = new Record_type0();
		int paxTypeCount = getPaxTypeCount(bookingSegmentsCopy.get(0).getBookingRelatedInfo().getTravellerInfo());
		record.setNumber(BigInteger.valueOf(1));
		if (paxTypeCount > 1) {
			record.setEndNumber(BigInteger.valueOf(paxTypeCount));
		}
		setCommissionPercent(miscQualifiers);
		optionalQualifiers.setMiscQualifiers(miscQualifiers);
		priceQuote.setLocalRecord(record);
		pricingQualifiers.setLocalPriceQuote(priceQuote);
		optionalQualifiers.setPricingQualifiers(pricingQualifiers);
		return optionalQualifiers;
	}

	private void setCommissionPercent(MiscQualifiers_type0 miscQualifiers) {
		if (segmentInfos.get(0).getPriceInfo(0).getMiscInfo() != null
				&& segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getIata() != null) {
			Commission_type0 commission = new Commission_type0();
			Double iata = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getIata();
			BigDecimal iataCommission = new BigDecimal(iata).setScale(2, BigDecimal.ROUND_HALF_UP);
			commission.setPercent(String.valueOf(iataCommission));
			miscQualifiers.setCommission(commission);
			log.info("Applying IATA Commission on eticket booking {} commission percent {}", bookingId, iataCommission);
		}
	}

	private void setTourCode(MiscQualifiers_type0 miscQualifiers) {
		String tourCodeMisc = segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getTourCode();
		if (StringUtils.isNotEmpty(tourCodeMisc)) {
			TourCode_type0 tourCode = new TourCode_type0();
			SuppressIT_type0 suppressIT = new SuppressIT_type0();
			suppressIT.setInd(true);
			tourCode.setSuppressIT(suppressIT);
			tourCode.setText(tourCodeMisc);
			miscQualifiers.setTourCode(tourCode);
		}
	}

	private int getPaxTypeCount(List<FlightTravellerInfo> travellerInfos) {
		int paxTypeCount = 0;
		for (PaxType paxType : PaxType.values()) {
			if (AirUtils.getParticularPaxTravellerInfo(travellerInfos, paxType).size() > 0) {
				paxTypeCount++;
			}
		}
		return paxTypeCount;
	}

	private FOP_Qualifiers_type0 getFOPQualifiers() {
		FOP_Qualifiers_type0 fOPQualifiers = new FOP_Qualifiers_type0();
		BasicFOP_type0 basicFOP = new BasicFOP_type0();
		if (creditCard != null) {
			CC_Info_type0 cCInfo = new CC_Info_type0();
			cCInfo.setSuppress(true);
			PaymentCard_type0 paymentCard = new PaymentCard_type0();
			paymentCard.setCode(creditCard.getCardType().getVendorCode());
			paymentCard.setExpireDate(getExpireDate(creditCard.getExpiry()));
			paymentCard.setNumber(new BigInteger(creditCard.getCardNumber()));
			cCInfo.setPaymentCard(paymentCard);
			basicFOP.setCC_Info(cCInfo);
		} else {
			basicFOP.setType("CA");
		}
		fOPQualifiers.setBasicFOP(basicFOP);
		return fOPQualifiers;
	}

	private ExpireDate getExpireDate(String dateText) {
		ExpireDate expireDate = new ExpireDate();
		expireDate.setExpireDate(getyearMonth(dateText));
		return expireDate;
	}

	private YearMonth getyearMonth(String dateText) {
		YearMonth yearMonth = null;
		try {
			String[] date = dateText.split("/");
			Integer month = Integer.valueOf(date[0]);
			Year year = (date[1].length() == 4) ? Year.of(Integer.valueOf(date[1]))
					: Year.parse(date[1], DateTimeFormatter.ofPattern("yy"));
			yearMonth = new YearMonth(year.getValue(), month);
		} catch (Exception e) {
			log.error("Unable to parse credit card year {}", bookingId, e);
		}
		return yearMonth;
	}

	private FlightQualifiers_type0 getFlightQualifiers(String marketingAirline) {
		FlightQualifiers_type0 flightQualifiers = new FlightQualifiers_type0();
		VendorPrefs_type0 vendorPref = new VendorPrefs_type0();
		Airline_type0 airline = new Airline_type0();
		airline.setCode(marketingAirline);
		vendorPref.setAirline(airline);
		flightQualifiers.setVendorPrefs(vendorPref);
		return flightQualifiers;
	}

	protected TravelItineraryAddInfoRQ_type0 getTravelItineraryAddInfo(List<SegmentInfo> bookingSegmentsCopy) {
		TravelItineraryAddInfoRQ_type0 travelItineraryAddInfoRq = new TravelItineraryAddInfoRQ_type0();
		AgencyInfo_type0 agencyInfo = new AgencyInfo_type0();
		agencyInfo.setAddress(SabreUtils.getAddress(segmentInfos, order.getBookingUserId()));
		agencyInfo.setTicketing(getTicketing());
		travelItineraryAddInfoRq.setAgencyInfo(agencyInfo);
		travelItineraryAddInfoRq.setCustomerInfo(getCustomerInfo(bookingSegmentsCopy));
		return travelItineraryAddInfoRq;
	}

	private CustomerInfo_type0 getCustomerInfo(List<SegmentInfo> bookingSegmentInfos) {
		CustomerInfo_type0 customerInfo = new CustomerInfo_type0();
		ContactNumbers_type0 contactNumbers = new ContactNumbers_type0();
		ContactNumber_type0[] contactNumberArray = new ContactNumber_type0[1];
		ContactNumber_type0 contactNumber = new ContactNumber_type0();
		contactNumber.setNameNumber("1.1");
		contactNumber.setPhone(AirSupplierUtils.getContactNumber(deliveryInfo));
		contactNumber.setPhoneUseType("H");
		contactNumberArray[0] = contactNumber;
		contactNumbers.setContactNumber(contactNumberArray);
		customerInfo.setContactNumbers(contactNumbers);
		List<FlightTravellerInfo> travellerInfos =
				bookingSegmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		int nameNumber = 1;
		int travIndex = 0;
		PersonName_type4[] personArray = new PersonName_type4[travellerInfos.size()];
		for (int i = 0; i < travellerInfos.size(); i++) {
			FlightTravellerInfo travellerInfo = travellerInfos.get(i);
			PersonName_type4 personName = new PersonName_type4();
			personName.setPassengerType(travellerInfo.getPaxType().getType());
			personName.setGivenName(getGivenName(travellerInfo));
			personName.setSurname(getLastName(travellerInfo));
			if (!(travellerInfo.getPaxType() == PaxType.ADULT)) {
				personName.setNameReference(SabreUtils.getNameRefrenece(travellerInfo.getPaxType(),
						fetchDob(travellerInfo, bookingSegmentInfos.get(0).getDepartTime())));
			}

			if (travellerInfo.getPaxType() != PaxType.INFANT) {
				if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
					CustLoyalty_type0[] custLoyalityArray =
							new CustLoyalty_type0[travellerInfo.getFrequentFlierMap().size()];
					int loyalityIndex = 0;
					for (Entry<String, String> entrySet : travellerInfo.getFrequentFlierMap().entrySet()) {
						CustLoyalty_type0 custLoyalty = new CustLoyalty_type0();
						custLoyalty.setMembershipID(entrySet.getValue());
						custLoyalty.setNameNumber(String.valueOf(nameNumber) + ".1");
						custLoyalty.setProgramID(entrySet.getKey());
						custLoyalty.setTravelingCarrierCode(bookingSegmentInfos.get(0).getPlatingCarrier(null));
						custLoyalityArray[loyalityIndex] = custLoyalty;
						loyalityIndex++;
					}
				}

			} else {
				personName.setInfant(true);
			}
			personName.setNameNumber(String.valueOf(nameNumber) + ".1");
			personArray[travIndex] = personName;
			travIndex++;
			nameNumber++;
		}
		customerInfo.setPersonName(personArray);
		return customerInfo;
	}

	private String getGivenName(FlightTravellerInfo travellerInfo) {
		return getFirstName(travellerInfo) + travellerInfo.getTitle();
	}

	private String getFirstName(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isBlank(travellerInfo.getFirstName())) {
			return StringUtils.EMPTY;
		}
		// firstName and title should be separated by whitespace.
		return travellerInfo.getFirstName().trim() + " ";
	}

	private String getLastName(FlightTravellerInfo travellerInfo) {
		if (StringUtils.isBlank(travellerInfo.getLastName())) {
			return StringUtils.EMPTY;
		}
		return travellerInfo.getLastName().trim();
	}

	private Ticketing_type0 getTicketing() {
		Ticketing_type0 ticketing = new Ticketing_type0();
		ticketing.setTicketType(SabreConstants.TICKET_TYPE);
		return ticketing;
	}

	private LocalDate fetchDob(FlightTravellerInfo travellerInfo, LocalDateTime departureDate) {
		return travellerInfo.getDob() != null ? travellerInfo.getDob() : SabreUtils.getDob(departureDate);
	}

	protected PostProcessing_type0 getPassengerDetailPostProcess() {
		PostProcessing_type0 postProcess = new PostProcessing_type0();
		RedisplayReservation_type0 reservation_type0 = new RedisplayReservation_type0();
		WaitInterval_type0 waitInterval = new WaitInterval_type0();
		waitInterval.setWaitInterval_type0(WAIT_INTERVAL);
		reservation_type0.setWaitInterval(waitInterval);
		postProcess.setRedisplayReservation(reservation_type0);
		postProcess.setUnmaskCreditCard(false);
		EndTransactionRQ_type0 endTransactionRq = new EndTransactionRQ_type0();
		EndTransaction_type0 endTransaction = new EndTransaction_type0();
		endTransaction.setInd(true);
		endTransactionRq.setEndTransaction(endTransaction);
		Source_type0 source = new Source_type0();
		source.setReceivedFrom(order.getBookingId());
		endTransactionRq.setSource(source);
		postProcess.setEndTransactionRQ(endTransactionRq);
		return postProcess;
	}


	public SpecialReqDetails_type0 getSpecialServiceReq(GstInfo gstInfo) {
		SpecialReqDetails_type0 specialReqDetails = new SpecialReqDetails_type0();
		SpecialServiceRQ_type0 specialServiceRequest = new SpecialServiceRQ_type0();
		SpecialServiceInfo_type0 specialServiceInfo = new SpecialServiceInfo_type0();
		List<FlightTravellerInfo> travellerInfos = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		int nameNumber = 1;
		/*
		 * Infant detail should be send in SSR request
		 */
		List<FlightTravellerInfo> infantTravellers =
				AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT);
		List<Service_type0> serviceArray = new ArrayList<Service_type0>();
		for (FlightTravellerInfo infant : infantTravellers) {
			Service_type0 service = new Service_type0();
			// "A" can be specified to associate the SSR to all segments
			service.setSegmentNumber("A");
			service.setSSR_Code("INFT");
			PersonName_type3 personName = new PersonName_type3();
			personName.setNameNumber(String.valueOf(nameNumber) + ".1");
			service.setPersonName(personName);
			String dob = SabreUtils.getFormattedDob(infant.getDob());
			service.setText(StringUtils.join(infant.getLastName(), "/", infant.getFirstName(), " ", infant.getTitle(),
					"/", dob));
			serviceArray.add(service);
			nameNumber++;
		}

		nameNumber = AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).size() + 1;
		List<FlightTravellerInfo> childTravellers =
				AirUtils.getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD);
		for (FlightTravellerInfo child : childTravellers) {
			if (child.getDob() != null) {
				Service_type0 service = new Service_type0();
				service.setSegmentNumber("A");
				service.setSSR_Code("CHLD");
				PersonName_type3 personName = new PersonName_type3();
				personName.setNameNumber(String.valueOf(nameNumber) + ".1");
				service.setPersonName(personName);
				String dob = SabreUtils.getFormattedDob(child.getDob());
				service.setText(dob.toUpperCase());
				serviceArray.add(service);
			}
			nameNumber++;
		}
		getTravellerPassportDetails(specialServiceInfo);
		if (Objects.nonNull(gstInfo) && StringUtils.isNotEmpty(gstInfo.getGstNumber())) {
			serviceArray.addAll(getGSTServices(gstInfo));
		}
		specialServiceInfo.setService(serviceArray.toArray(new Service_type0[0]));
		specialServiceRequest.setSpecialServiceInfo(specialServiceInfo);
		specialReqDetails.setSpecialServiceRQ(specialServiceRequest);

		return specialReqDetails;
	}

	private void parseService(com.sabre.webservices.sabrexml._2011._10.SpecialServiceInfo_type0 specialServiceInfo,
			FlightTravellerInfo traveller, int index, String nameNumber) {
		PersonName_type2 personName = new PersonName_type2();
		personName.setNameNumber(nameNumber);
		List<com.sabre.webservices.sabrexml._2011._10.Service_type0> serviceList =
				new ArrayList<com.sabre.webservices.sabrexml._2011._10.Service_type0>();
		if (Objects.nonNull(traveller.getSsrSeatInfo())) {
			com.sabre.webservices.sabrexml._2011._10.Service_type0 seatService =
					new com.sabre.webservices.sabrexml._2011._10.Service_type0();
			seatService.setSSR_Code(traveller.getSsrSeatInfo().getCode());
			seatService.setSegmentNumber(String.valueOf(index));
			seatService.setPersonName(personName);
			serviceList.add(seatService);
		}
		if (Objects.nonNull(traveller.getSsrMealInfo())) {

			com.sabre.webservices.sabrexml._2011._10.Service_type0 mealService =
					new com.sabre.webservices.sabrexml._2011._10.Service_type0();
			mealService.setSSR_Code(traveller.getSsrMealInfo().getCode());
			mealService.setSegmentNumber(String.valueOf(index));
			mealService.setPersonName(personName);
			serviceList.add(mealService);
		}
		if (CollectionUtils.isNotEmpty(traveller.getExtraServices())) {
			for (SSRInformation travellerSSRInfo : traveller.getExtraServices()) {
				com.sabre.webservices.sabrexml._2011._10.Service_type0 extraService =
						new com.sabre.webservices.sabrexml._2011._10.Service_type0();
				extraService.setSSR_Code(travellerSSRInfo.getCode());
				extraService.setSegmentNumber(String.valueOf(index));
				extraService.setPersonName(personName);
				serviceList.add(extraService);
			}

		}
		if (CollectionUtils.isNotEmpty(serviceList)) {
			specialServiceInfo
					.setService(serviceList.toArray(new com.sabre.webservices.sabrexml._2011._10.Service_type0[0]));
		}
	}

	private void getTravellerPassportDetails(SpecialServiceInfo_type0 specialServiceInfo) {
		int nameNumber = 1;
		int infNameNumber = 1;
		List<FlightTravellerInfo> travellerInfos = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		List<AdvancePassenger_type0> advancePassengerList = new ArrayList<AdvancePassenger_type0>();
		for (int i = 0; i < travellerInfos.size(); i++) {
			FlightTravellerInfo traveller = travellerInfos.get(i);
			if (StringUtils.isNotEmpty(traveller.getPassportNumber()) && traveller.getExpiryDate() != null) {
				/**
				 * @implSpec : If passing Passport number then have to pass passport Expiry date also , other wise
				 *           Passenger RS will be on warning (request to retyrn with Expiry)
				 */
				AdvancePassenger_type0 advancePassenger = new AdvancePassenger_type0();
				Document_type0 document = new Document_type0();
				document.setExpirationDate(getFullDate(traveller.getExpiryDate()));
				document.setNumber(traveller.getPassportNumber());
				document.setType("P");
				CountryInfo countryInfo = gmsCommunicator.getCountryInfo(traveller.getPassportNationality());
				document.setNationalityCountry(countryInfo.getCode());
				document.setIssueCountry(countryInfo.getCode());
				advancePassenger.setDocument(document);
				PersonName_type1 personName = new PersonName_type1();
				if (traveller.getDob() != null) {
					personName.setDateOfBirth(getFullDate(traveller.getDob()));
				}
				String gender = SabreUtils.getGenderInfo(traveller.getTitle());
				if (traveller.getPaxType().equals(PaxType.INFANT)) {
					gender += "I";
					personName.setNameNumber(String.valueOf(infNameNumber) + ".1");
					infNameNumber++;
				} else {
					personName.setNameNumber(String.valueOf(nameNumber) + ".1");
				}

				personName.setGivenName(getGivenName(traveller));
				personName.setSurname(traveller.getLastName());
				advancePassenger.setPersonName(personName);
				personName.setGender(getGenderType(gender));
				VendorPrefs_type1 vendorPref = new VendorPrefs_type1();
				Airline_type1 airline = new Airline_type1();
				airline.setHosted(SabreUtils.isHostRequired(segmentInfos));
				vendorPref.setAirline(airline);
				advancePassenger.setVendorPrefs(vendorPref);
				nameNumber += 1;
				advancePassengerList.add(advancePassenger);
			}
		}
		if (CollectionUtils.isNotEmpty(advancePassengerList)) {
			specialServiceInfo.setAdvancePassenger(advancePassengerList.toArray(new AdvancePassenger_type0[0]));
		}
	}


	private Gender_type0 getGenderType(String gender) {
		Gender_type0 genderType = null;
		if ("M".equals(gender)) {
			genderType = Gender_type0.M;
		} else if ("F".equals(gender)) {
			genderType = Gender_type0.F;
		} else if ("MI".equals(gender)) {
			genderType = Gender_type0.MI;
		} else if ("FI".equals(gender)) {
			genderType = Gender_type0.FI;
		}
		return genderType;
	}

	public FullDate getFullDate(LocalDate localDate) {
		FullDate fullDate = new FullDate();
		try {
			fullDate.setFullDate(TgsDateUtils.getDOBasDate(localDate));
		} catch (Exception e) {
			log.error("Unable to parse Date {}", e.getMessage());
		}
		return fullDate;
	}

	public boolean storePrice(String blockedPnr, List<SegmentInfo> bookingSegmentsCopy) {
		boolean isEndTxnSuccess = false;
		int numTry = 0;
		do {
			try {
				TravelItineraryReadRS travelItineraryResponse = travelItineraryReadLLS(blockedPnr);
				if (travelItineraryResponse == null || !airPrice(bookingSegmentsCopy)) {
					break;
				}
				isEndTxnSuccess = this.endTransaction();

				this.sabreCommand(SabreConstants.IGNORE_COMMAND);

				if (!isEndTxnSuccess && hasArrivalUnknownSegments() && numTry == 0) {
					travelItineraryReadLLS(blockedPnr);
					arunk();
				}

				if (!isEndTxnSuccess) {
					Thread.sleep(3 * 1000);
				}
			} catch (InterruptedException iE) {
				log.error("Thread Intrepputed for TravelItinerary {}", bookingId, iE);
				throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
			}
		} while (!isEndTxnSuccess && ++numTry < 3);
		return isEndTxnSuccess;
	}

	private Collection<? extends Service_type0> getGSTServices(GstInfo gstInfo) {
		String nameNumber = "1.1";
		gstInfo.cleanData();
		List<Service_type0> serviceList = new ArrayList<>();
		List<String> sSRCodeList = Arrays.asList("GSTN", "GSTA", "GSTP");
		List<String> gstInfoList = Arrays.asList(
				StringUtils.join(gstInfo.getGstNumber(), "/",
						SabreUtils.applyLengthLimitation(SabreUtils.replaceSpecialCharacter(
								BaseUtils.getCleanGstName(gstInfo.getRegisteredName())), 0, 35)),
				StringUtils.join(SabreUtils.splitGSTA(SabreUtils.replaceSpecialCharacter(gstInfo.getAddress())),
						gstInfo.getCityName(), "/", gstInfo.getState(), "/", gstInfo.getPincode()),
				gstInfo.getMobile());
		for (int i = 0; i < sSRCodeList.size(); i++) {
			Service_type0 service = new Service_type0();
			service.setSSR_Code(sSRCodeList.get(i));
			PersonName_type3 personName = new PersonName_type3();
			personName.setNameNumber(nameNumber);
			service.setPersonName(personName);
			service.setText("IND/" + gstInfoList.get(i));
			serviceList.add(service);
		}
		return serviceList;
	}

	public void specialServiceRq(String blockedPNR) {
		boolean isEndTxnSuccess = false;
		boolean isSSRAdditionSuccess = false;
		int numTry = 0;
		do {
			try {
				isSSRAdditionSuccess = specialService(segmentInfos);
				isEndTxnSuccess = endTransaction();
				if (!isEndTxnSuccess) {
					this.sabreCommand(SabreConstants.IGNORE_COMMAND);
					Thread.sleep(3 * 1000);
				}
				this.travelItineraryReadLLS(blockedPNR);
			} catch (InterruptedException iE) {
				log.error("Thread Interrupted for Booking Id {} ", bookingId, iE);
				throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
			}
		} while (!isEndTxnSuccess && ++numTry < 3);
		if (!isSSRAdditionSuccess || !isEndTxnSuccess) {
			log.info("Special Service addition failed for bookingid {}", bookingId);
			logCriticalMessage("Special Service addition failed");
		}
	}

	public String getAirlinePnr(String crsPnr) {
		TravelItineraryReadRS travelItineraryRS = this.travelItineraryReadLLS(crsPnr);
		ItineraryInfo_type0 itineraryInfo = travelItineraryRS.getTravelItinerary().getItineraryInfo();
		if (itineraryInfo != null && itineraryInfo.getReservationItems() != null) {
			for (Item_type0 item : itineraryInfo.getReservationItems().getItem()) {
				if (isValidReservationItem(item)) {
					SupplierRef_type0 supplierRef = item.getFlightSegment()[0].getSupplierRef();
					SegmentInfo matchingSegment = getMatchingSegment(item, segmentInfos);
					matchingSegment.updateAirlinePnr(SabreUtils.parsePnr(supplierRef.getID()));
				}
			}
			if (itineraryInfo.getReservationItems().getItem().length < segmentInfos.size()) {
				throw new RuntimeException("Booking Segments doesn't match with number of reserved segments.");
			}
			pnr = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
		}
		ticketingTimelimit = getHoldTimeLimit(travelItineraryRS.getTravelItinerary());
		return pnr;
	}

	private SegmentInfo getMatchingSegment(Item_type0 item, List<SegmentInfo> bookingSegments) {
		for (int index = 0; index < bookingSegments.size(); index++) {
			SegmentInfo segment = bookingSegments.get(index);
			DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
			if (segment.getDepartureAirportCode()
					.equals(item.getFlightSegment()[0].getOriginLocation().getLocationCode())
					&& segment.getArrivalAirportCode()
							.equals(item.getFlightSegment()[item.getFlightSegment().length - 1].getDestinationLocation()
									.getLocationCode())
					&& segment.getDepartTime().isEqual(LocalDateTime
							.parse(item.getFlightSegment()[0].getDepartureDateTime().getDateTime(), formatter))) {
				return segment;
			}
		}
		return null;
	}

	protected boolean ticketing(String blockedPnr) {
		boolean isSuccess = false;
		TravelItineraryReadRS travelItineraryResponse = this.travelItineraryReadLLS(blockedPnr);
		Ticketing_type1[] ticketingList =
				travelItineraryResponse.getTravelItinerary().getItineraryInfo().getTicketing();
		if (ArrayUtils.isNotEmpty(ticketingList)) {
			List<String> ticketNumbers = SabreUtils.fetchTicketNumber(ticketingList);
			log.info("Ticket number generated for bookingId {} are, {}", order.getBookingId(), ticketNumbers);
			isSuccess = SabreUtils.updateTicketNumbers(segmentInfos, travelItineraryResponse);
		}
		return isSuccess;
	}

	protected boolean deletePriceQuote() {
		DeletePriceQuoteServiceStub serviceStub = bindingService.getDeletePrice();
		DeletePriceQuoteRQ deleteQuoteRQ = getDeletePriceQuoteRQ();
		listener.setType(AirUtils.getLogType("DeletePriceQuote", configuration));
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.DELETE_PRICE_QUOTE, SabreConstants.CONVERSATION_ID);
			DeletePriceQuoteRS deleteQuoteRS =
					serviceStub.deletePriceQuoteRQ(deleteQuoteRQ, messageHeader, getSecurity());
			if (com.sabre.services.stl_header.v120.CompletionCodes.Complete
					.equals(deleteQuoteRS.getApplicationResults().getStatus())) {
				return true;
			}
		} catch (RemoteException r) {
			throw new SupplierRemoteException(r);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return false;
	}

	private DeletePriceQuoteRQ getDeletePriceQuoteRQ() {
		DeletePriceQuoteRQ rq = new DeletePriceQuoteRQ();
		rq.setVersion(SabreConstants.DELETE_PRICEQUOTE_VERSION);
		AirItineraryPricingInfo_type0 itinPricingInfo = new AirItineraryPricingInfo_type0();
		Record_type0 record = new Record_type0();
		record.setAll(true);
		// itinPricingInfo.
		// itinPricingInfo.getRecord().add(record);
		rq.setAirItineraryPricingInfo(itinPricingInfo);
		return rq;
	}

	public boolean isSegmentsActiveForTicketing(String crsPNR) {
		TravelItineraryReadRS tirResponse = this.travelItineraryReadLLS(crsPNR);
		if (isValidTripBooking(tirResponse) && BooleanUtils.isFalse(hasCancelledFlightSegments(
				tirResponse.getTravelItinerary().getItineraryInfo().getReservationItems().getItem()))) {
			return true;
		} else {
			return false;
		}
	}

	private Boolean hasCancelledFlightSegments(Item_type0[] items) {
		Boolean hasCancelledSegments = null;
		for (Item_type0 item : items) {
			if (ArrayUtils.isNotEmpty(item.getFlightSegment())) {
				hasCancelledSegments = false;
				for (FlightSegment_type2 flightSegment : item.getFlightSegment()) {
					if (SabreConstants.SEGMENT_CANCELLED_STATUS.equalsIgnoreCase(flightSegment.getStatus())) {
						return true;
					}
				}
			}
		}
		return hasCancelledSegments;
	}

	// To add arrival unknown segments.
	public void arunk() {
		ARUNK_ServiceStub serviceStub = bindingService.getArunkService();
		ARUNK_RQ arunkRQ = new ARUNK_RQ();
		listener.setType(AirUtils.getLogType("ARUNK", configuration));
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			arunkRQ.setVersion(SabreConstants.ARUNK_VERSION);
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.ARUNK_SERVICE, SabreConstants.CONVERSATION_ID);
			ARUNK_RS arunkRS = serviceStub.aRUNK_RQ(arunkRQ, messageHeader, getSecurity());
			if (!com.sabre.services.stl_header.v120.CompletionCodes.Complete
					.equals(arunkRS.getApplicationResults().getStatus())) {
				log.error("ARUNK failed for the bookingId {}", bookingId);
			} else {
				endTransaction();
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

}
