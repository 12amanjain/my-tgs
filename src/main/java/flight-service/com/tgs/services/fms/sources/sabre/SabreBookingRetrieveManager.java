package com.tgs.services.fms.sources.sabre;

import java.math.BigDecimal;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.Map.Entry;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.sabre.services.res.tir.v3_9.PassengerTypeQuantity_type0;
import com.sabre.services.res.tir.v3_9.Meal_type0;
import com.sabre.services.res.tir.v3_9.PassengerData_type0;
import com.sabre.services.res.tir.v3_9.PassengerInfo_type0;
import com.sabre.services.res.tir.v3_9.FlightSegment_type2;
import com.sabre.services.res.tir.v3_9.TaxBreakdownCode_type0;
import com.sabre.services.res.tir.v3_9.ItinTotalFare_type0;
import com.sabre.services.res.tir.v3_9.CustLoyalty_type1;
import com.sabre.services.res.tir.v3_9.PersonName_type2;
import com.sabre.services.res.tir.v3_9.CustomerInfo_type0;
import com.sabre.services.res.tir.v3_9.SpecialServiceInfo_type0;
import com.sabre.services.res.tir.v3_9.FlightSegment_type1;
import com.sabre.services.res.tir.v3_9.Item_type0;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRS;
import com.sabre.services.res.tir.v3_9.PriceQuote_type0;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.Amenities;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoPNRFoundException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
class SabreBookingRetrieveManager extends SabreServiceManager {

	protected String supplierRefId;

	private static List<String> titleList = Arrays.asList("mr", "ms", "miss", "mstr", "mrs", "master");

	public AirImportPnrBooking retrieveBooking(TravelItineraryReadRS travelItineraryResponse) {
		AirImportPnrBooking pnrBooking = AirImportPnrBooking.builder().build();
		if (!isValidTripBooking(travelItineraryResponse)) {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + supplierRefId);
		} else if (!isValidPriceQuote(travelItineraryResponse)) {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_PRICING_UNAVAILABLE + supplierRefId);
		}

		List<SegmentInfo> segmentInfos = fetchSegmentInfos(travelItineraryResponse);
		GstInfo gstInfo = getGstInfo(travelItineraryResponse);
		parseTravellerSpecialServices(segmentInfos,
				travelItineraryResponse.getTravelItinerary().getSpecialServiceInfo());
		DeliveryInfo deliveryInfo = fetchDeliveryInfo(travelItineraryResponse);
		List<TripInfo> tripInfos = getTripInfos(segmentInfos);
		pnrBooking =
				AirImportPnrBooking.builder().tripInfos(tripInfos).gstInfo(gstInfo).deliveryInfo(deliveryInfo).build();
		return pnrBooking;
	}

	private List<SegmentInfo> fetchSegmentInfos(TravelItineraryReadRS travelItineraryResponse) {

		List<SegmentInfo> segmentInfos = new ArrayList<>();

		Item_type0[] reservationItems =
				travelItineraryResponse.getTravelItinerary().getItineraryInfo().getReservationItems().getItem();
		PriceQuote_type0[] priceQuotes =
				travelItineraryResponse.getTravelItinerary().getItineraryInfo().getItineraryPricing().getPriceQuote();
		List<PriceQuote_type0> activePriceQuotes = getActivePriceQuote(priceQuotes);
		Map<String, String> passportInfoMap =
				getPassportInfoMap(travelItineraryResponse.getTravelItinerary().getSpecialServiceInfo());

		LocalDateTime departureDate = null;
		for (int itemIndex = 0; itemIndex < reservationItems.length; itemIndex++) {
			Item_type0 item = reservationItems[itemIndex];
			if (isValidReservationItem(item)) {
				if (itemIndex == 0) {
					DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
					departureDate = LocalDateTime.parse(item.getFlightSegment()[0].getDepartureDateTime().getDateTime(),
							formatter);
				}
				SegmentInfo segment = fetchSegmentDetails(activePriceQuotes.toArray(new PriceQuote_type0[0]), item,
						itemIndex, passportInfoMap, departureDate);
				segmentInfos.add(segment);
			}
		}

		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			fetchPricingDetails(segmentInfos, travelItineraryResponse, activePriceQuotes);
			SabreUtils.updateTicketNumbers(segmentInfos, travelItineraryResponse);
			updateFrequentFlierInfo(segmentInfos, travelItineraryResponse.getTravelItinerary().getCustomerInfo());
		}

		return segmentInfos;
	}

	private void updateFrequentFlierInfo(List<SegmentInfo> segmentInfos, CustomerInfo_type0 customerInfo) {
		try {
			if (customerInfo != null && ArrayUtils.isNotEmpty(customerInfo.getCustLoyalty())
					&& ArrayUtils.isNotEmpty(customerInfo.getPersonName())) {
				Map<String, CustLoyalty_type1> customerLoyaltyMap = new HashMap<>();

				for (PersonName_type2 person : customerInfo.getPersonName()) {
					for (CustLoyalty_type1 custLoyalty : customerInfo.getCustLoyalty()) {
						if (custLoyalty.getNameNumber().equals(person.getNameNumber())) {
							customerLoyaltyMap.put(StringUtils
									.join(person.getSurname(), person.getGivenName(), custLoyalty.getProgramID())
									.replace(" ", "").toLowerCase(), custLoyalty);
							break;
						}
					}
				}
				if (MapUtils.isNotEmpty(customerLoyaltyMap)) {
					for (SegmentInfo segment : segmentInfos) {
						for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
							String key =
									StringUtils
											.join(traveller.getLastName(), traveller.getFirstName(),
													traveller.getTitle(), segment.getAirlineCode(false))
											.replace(" ", "").toLowerCase();
							CustLoyalty_type1 custLoyalty = customerLoyaltyMap.get(key);
							if (custLoyalty != null) {
								if (traveller.getFrequentFlierMap() == null) {
									traveller.setFrequentFlierMap(new HashMap<>());
								}
								traveller.getFrequentFlierMap().put(segment.getPlatingCarrier(segment.getPriceInfo(0)),
										custLoyalty.getMembershipID());
							}
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse frequent flyer for supplierRefId {}", supplierRefId);
		}
	}

	private void fetchPricingDetails(List<SegmentInfo> segmentInfos, TravelItineraryReadRS travelItineraryResponse,
			List<PriceQuote_type0> activePriceQuotes) {
		PriceInfo priceInfo = segmentInfos.get(0).getPriceInfo(0);
		List<FlightTravellerInfo> segmentTravellerInfo = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		String platingCarrier = null;
		Map<PaxType, FareDetail> fareDetails = new HashMap<>();
		for (PriceQuote_type0 activePriceQuote : activePriceQuotes) {
			FareDetail fareDetail = new FareDetail();
			ItinTotalFare_type0 itinFare =
					activePriceQuote.getPricedItinerary()[0].getAirItineraryPricingInfo().getItinTotalFare()[0];
			if (platingCarrier == null) {
				platingCarrier = activePriceQuote.getPricedItinerary()[0].getValidatingCarrier();
			}
			Map<FareComponent, Double> fareComponents = new HashMap<>();
			fareComponents.put(FareComponent.BF, getBaseFareFromItinFare(itinFare));
			fareComponents.put(FareComponent.TF, getAmountBasedOnCurrency(
					new BigDecimal(itinFare.getTotalFare().getAmount()), itinFare.getTotalFare().getCurrencyCode()));
			setTaxBreakUp(fareComponents, itinFare);
			fareDetail.setFareComponents(fareComponents);
			PassengerTypeQuantity_type0 paxTypeQuantity =
					activePriceQuote.getPricedItinerary()[0].getAirItineraryPricingInfo().getPassengerTypeQuantity()[0];
			PaxType paxType = PaxType.getPaxType(paxTypeQuantity.getCode());
			splitFareDetailToEachTraveller(segmentTravellerInfo, fareDetail, paxType);
			fareDetails.put(paxType, fareDetail);
		}

		if (StringUtils.isNotEmpty(platingCarrier)) {
			priceInfo.getMiscInfo().setPlatingCarrier(AirlineHelper.getAirlineInfo(platingCarrier));
		}

		super.setSegmentInfos(segmentInfos);
		priceInfo.getMiscInfo().setTimeLimit(getHoldTimeLimit(travelItineraryResponse.getTravelItinerary()));
		setAccountCode(priceInfo, activePriceQuotes.get(0));
		setTourCode(priceInfo, activePriceQuotes.get(0));;
	}

	/**
	 * fall back model. if getting tax break up fails this will set the total tax in OC.
	 * 
	 * @param fareComponents
	 * @param itinFare
	 */
	private void setTaxBreakUp(Map<FareComponent, Double> fareComponents, ItinTotalFare_type0 itinFare) {
		try {
			TaxBreakdownCode_type0[] taxBreakdownCode = itinFare.getTaxes().getTaxBreakdownCode();
			if (ArrayUtils.isNotEmpty(taxBreakdownCode)) {
				for (TaxBreakdownCode_type0 tax : taxBreakdownCode) {
					String taxValueString = tax.getString().toUpperCase();
					if (StringUtils.isNotEmpty(taxValueString)) {
						boolean isATComponent = true;
						for (GDSFareComponentMapper gdsComponent : GDSFareComponentMapper.values()) {
							if (taxValueString.contains(gdsComponent.name())) {
								isATComponent = false;
								// '(?i)' pattern matches with case insensitive mode.
								Double amount =
										Double.valueOf(taxValueString.replaceAll("(?i)" + gdsComponent.name(), ""));
								Double previousAmount =
										fareComponents.getOrDefault(gdsComponent.getFareComponent(), 0.0);
								fareComponents.put(gdsComponent.getFareComponent(), previousAmount + amount);
								break;
							}
						}
						if (isATComponent) {
							Double previousAmount = fareComponents.getOrDefault(FareComponent.AT, 0.0);
							Double amount = Double.valueOf(taxValueString.split("[a-zA-Z]")[0]);
							fareComponents.put(FareComponent.AT, previousAmount + amount);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse Tax breakup for supplierRefId {}", supplierRefId);
			fareComponents.put(FareComponent.OC,
					getAmountBasedOnCurrency(new BigDecimal(itinFare.getTaxes().getTax().getAmount()),
							itinFare.getTotalFare().getCurrencyCode()));
		}

	}

	private void splitFareDetailToEachTraveller(List<FlightTravellerInfo> travellerInfos, FareDetail fareDetail,
			PaxType paxType) {
		List<FlightTravellerInfo> paxWiseTravellers = AirUtils.getParticularPaxTravellerInfo(travellerInfos, paxType);
		int paxwiseCount = paxWiseTravellers.size();
		for (Entry<FareComponent, Double> entry : fareDetail.getFareComponents().entrySet()) {
			for (FlightTravellerInfo traveller : paxWiseTravellers) {
				FareDetail travellerFareDetail = traveller.getFareDetail();
				travellerFareDetail.getFareComponents().put(entry.getKey(),
						travellerFareDetail.getFareComponents().getOrDefault(entry.getKey(), 0.0)
								+ (entry.getValue() / paxwiseCount));
			}
		}
	}

	private Double getBaseFareFromItinFare(ItinTotalFare_type0 itinFare) {
		String amount = "";
		String code = "";
		if (itinFare.getEquivFare() != null) {
			amount = itinFare.getEquivFare().getAmount();
			code = itinFare.getEquivFare().getCurrencyCode();
		} else {
			amount = itinFare.getBaseFare().getAmount();
			code = itinFare.getBaseFare().getCurrencyCode();
		}
		return getAmountBasedOnCurrency(new BigDecimal(amount), code);
	}

	private void setAccountCode(PriceInfo priceInfo, PriceQuote_type0 activePriceQuote) {
		if (ArrayUtils.isNotEmpty(activePriceQuote.getPricedItinerary())
				&& activePriceQuote.getPricedItinerary()[0].getNetTicketingInfo() != null && StringUtils
						.isNotEmpty(activePriceQuote.getPricedItinerary()[0].getNetTicketingInfo().getAccountCode())) {
			priceInfo.getMiscInfo()
					.setAccountCode(activePriceQuote.getPricedItinerary()[0].getNetTicketingInfo().getAccountCode());
		}
	}

	private void setTourCode(PriceInfo priceInfo, PriceQuote_type0 activePriceQuote) {
		if (activePriceQuote.getPriceQuotePlus() != null
				&& StringUtils.isNotEmpty(activePriceQuote.getPriceQuotePlus().getTourCode())) {
			priceInfo.getMiscInfo().setTourCode(activePriceQuote.getPriceQuotePlus().getTourCode());
		}
	}

	private boolean isValidPriceQuote(TravelItineraryReadRS travelItineraryResponse) {
		PriceQuote_type0[] priceQuotes =
				travelItineraryResponse.getTravelItinerary().getItineraryInfo().getItineraryPricing().getPriceQuote();
		List<PriceQuote_type0> activePriceQuotes = getActivePriceQuote(priceQuotes);
		if (CollectionUtils.isNotEmpty(activePriceQuotes)) {
			return true;
		}
		return false;
	}

	private List<FlightTravellerInfo> fetchTravellerInfos(SegmentInfo segmentInfo, PriceQuote_type0[] activePriceQuotes,
			Item_type0 reservationItem, int itemIndex, FlightSegment_type2 flightSegment, CabinClass cabin,
			boolean isMealIncluded, Map<String, String> passportInfoMap, LocalDateTime departureDate) {
		List<FlightTravellerInfo> flightTravellerInfos = new ArrayList<>();
		Map<PaxType, FareDetail> fareDetails = fetchZeroFareDetails(activePriceQuotes,
				flightSegment.getResBookDesigCode(), cabin, itemIndex, isMealIncluded);
		for (PriceQuote_type0 activePriceQuote : activePriceQuotes) {
			FlightSegment_type2 itemFlight = getFlightSegmentFromItem(segmentInfo, reservationItem);
			PassengerInfo_type0 passengerInfo = activePriceQuote.getPriceQuotePlus().getPassengerInfo();
			for (PassengerData_type0 passengerData : passengerInfo.getPassengerData()) {
				FlightTravellerInfo traveller = new FlightTravellerInfo();
				traveller.setSupplierBookingId(supplierRefId);
				String travellerName = passengerData.getString();
				String paxTitle = "";
				String firstName = travellerName.substring(travellerName.indexOf("/") + 1);
				for (String title : titleList) {
					if (firstName.substring(firstName.lastIndexOf(" ") + 1).toLowerCase().contains(title)) {
						paxTitle = title;
					}
				}
				traveller.setLastName(travellerName.substring(0, travellerName.indexOf("/")));
				traveller.setFirstName(firstName.substring(0, firstName.length() - paxTitle.length()));
				traveller.setTitle(StringUtils.capitalize(paxTitle));
				traveller.setPaxType(PaxType.getPaxType(passengerInfo.getPassengerType()));
				if (traveller.getPaxType() == null) {
					traveller.setPaxType(SabreUtils.getPaxTypeOnTitle(traveller, departureDate.toLocalDate()));
				}
				if (itemFlight != null) {
					traveller.setPnr(SabreUtils.parsePnr(itemFlight.getSupplierRef().getID()));
				} else {
					traveller.setPnr(SabreUtils.parsePnr(flightSegment.getSupplierRef().getID()));
				}
				traveller.setFareDetail(fareDetails.get(traveller.getPaxType()));
				if (!isTravellerExists(traveller, flightTravellerInfos)) {
					setTravellerPassport(traveller, passportInfoMap);
					flightTravellerInfos.add(traveller);
				}
			}
		}
		return flightTravellerInfos;
	}

	private FlightSegment_type2 getFlightSegmentFromItem(SegmentInfo segmentInfo, Item_type0 reservationItem) {
		for (FlightSegment_type2 segment_type21 : reservationItem.getFlightSegment()) {
			if (segment_type21.getOriginLocation().getLocationCode()
					.equalsIgnoreCase(segmentInfo.getDepartureAirportCode())
					&& segment_type21.getDestinationLocation().getLocationCode()
							.equalsIgnoreCase(segmentInfo.getArrivalAirportCode())
					&& segment_type21.getFlightNumber().contains(segmentInfo.getFlightNumber())) {
				return segment_type21;
			}
		}
		return null;
	}

	private void setTravellerPassport(FlightTravellerInfo traveller, Map<String, String> passportInfoMap) {
		String key = StringUtils.join(traveller.getLastName(), "/", traveller.getFirstName()).replace(" ", "");
		try {
			if (MapUtils.isNotEmpty(passportInfoMap) && passportInfoMap.get(key) != null) {
				String[] passportInfo = passportInfoMap.get(key).split("/");
				traveller.setPassportNationality(passportInfo[0]);
				traveller.setPassportNumber(passportInfo[1]);
				traveller.setDob(TgsDateUtils.convertStringToDate(passportInfo[2], "ddMMMyy").toLocalDate());
				traveller.setExpiryDate(TgsDateUtils.convertStringToDate(passportInfo[3], "ddMMMyy").toLocalDate());
			}
		} catch (Exception e) {
			log.error("Unable to parse passport information for traveller {} for supplierRefId {}", traveller,
					supplierRefId);
		}
	}

	private boolean isTravellerExists(FlightTravellerInfo traveller, List<FlightTravellerInfo> travellerInfos) {
		boolean isExists = false;
		if (CollectionUtils.isNotEmpty(travellerInfos)) {
			Optional<FlightTravellerInfo> existTraveller = AirUtils.findTraveller(travellerInfos, traveller);
			if (existTraveller.isPresent()) {
				isExists = true;
			}
		}
		return isExists;
	}

	private Map<PaxType, FareDetail> fetchZeroFareDetails(PriceQuote_type0[] activePriceQuotes, String resBookDesigCode,
			CabinClass cabin, int itemIndex, boolean isMealIncluded) {

		Map<PaxType, FareDetail> fareDetails = new HashMap<>();
		for (PriceQuote_type0 activePriceQuote : activePriceQuotes) {
			if (activePriceQuote.getPriceQuotePlus() != null
					&& activePriceQuote.getPriceQuotePlus().getPassengerInfo() != null && StringUtils
							.isNotEmpty(activePriceQuote.getPriceQuotePlus().getPassengerInfo().getPassengerType())) {
				PaxType paxType =
						PaxType.getPaxType(activePriceQuote.getPriceQuotePlus().getPassengerInfo().getPassengerType());
				FareDetail fareDetail = new FareDetail();

				fareDetail.setCabinClass(cabin);
				fareDetail.setClassOfBooking(resBookDesigCode);
				BaggageInfo baggageInfo = new BaggageInfo();
				if (ArrayUtils.isNotEmpty(activePriceQuote.getPricedItinerary())
						&& activePriceQuote.getPricedItinerary()[0].getAirItineraryPricingInfo() != null
						&& ArrayUtils.isNotEmpty(activePriceQuote.getPricedItinerary()[0].getAirItineraryPricingInfo()
								.getPTC_FareBreakdown())
						&& ArrayUtils.isNotEmpty(activePriceQuote.getPricedItinerary()[0].getAirItineraryPricingInfo()
								.getPTC_FareBreakdown()[0].getFlightSegment())) {
					FlightSegment_type1 pricingFlightSegment = getPricingFlightSegment(activePriceQuote, itemIndex + 1);
					if (pricingFlightSegment != null) {
						baggageInfo.setAllowance(pricingFlightSegment.getBaggageAllowance().getNumber());
						if (pricingFlightSegment.getFareBasis() != null
								&& pricingFlightSegment.getFareBasis().getCode() != null) {
							fareDetail.setFareBasis(pricingFlightSegment.getFareBasis().getCode());
						}
					}
				}
				fareDetail.setBaggageInfo(baggageInfo);
				fareDetail.setIsMealIncluded(isMealIncluded);
				fareDetails.put(paxType, fareDetail);
			}
		}
		return fareDetails;
	}

	private FlightSegment_type1 getPricingFlightSegment(PriceQuote_type0 priceQuote, int itemIndex) {
		FlightSegment_type1[] flightSegments =
				priceQuote.getPricedItinerary()[0].getAirItineraryPricingInfo().getPTC_FareBreakdown()[0]
						.getFlightSegment();
		for (int index = 0; index < flightSegments.length; index++) {
			FlightSegment_type1 flightSegment = flightSegments[index];
			if (flightSegment.getSegmentNumber() != null
					&& Integer.valueOf(flightSegment.getSegmentNumber()) == itemIndex
					&& flightSegment.getDepartureDateTime() != null && flightSegment.getFlightNumber() != null
					&& flightSegment.getResBookDesigCode() != null && flightSegment.getStatus() != null
					&& (flightSegment.getStatus().equalsIgnoreCase("OK")
							|| flightSegment.getStatus().equalsIgnoreCase("NS"))
					&& flightSegment.getBaggageAllowance() != null
					&& flightSegment.getBaggageAllowance().getNumber() != null) {
				return flightSegment;
			}
		}
		return null;
	}

	private SegmentInfo fetchSegmentDetails(PriceQuote_type0[] activePriceQuotes, Item_type0 item, int itemIndex,
			Map<String, String> passportInfoMap, LocalDateTime departureDate) {
		FlightSegment_type2 firstFlightSegment = item.getFlightSegment()[0];
		FlightSegment_type2 lastFlightSegment = item.getFlightSegment()[item.getFlightSegment().length - 1];
		CabinClass cabin = CabinClass.ECONOMY;
		if (firstFlightSegment.getCabin() != null && firstFlightSegment.getCabin().getSabreCode() != null) {
			cabin = ObjectUtils.firstNonNull(CabinClass.getEnumFromCode(firstFlightSegment.getCabin().getSabreCode()),
					CabinClass.ECONOMY);
		}
		SegmentInfo segment = new SegmentInfo();
		FlightDesignator flightDesignator = new FlightDesignator();
		flightDesignator
				.setAirlineInfo(AirlineHelper.getAirlineInfo(firstFlightSegment.getMarketingAirline().getCode()));
		flightDesignator.setFlightNumber(Integer.valueOf(firstFlightSegment.getFlightNumber()).toString());
		flightDesignator.setEquipType(firstFlightSegment.getEquipment().getAirEquipType());
		segment.setFlightDesignator(flightDesignator);
		if (!firstFlightSegment.getMarketingAirline().getCode()
				.equals(firstFlightSegment.getOperatingAirline()[0].getCode())) {
			segment.setOperatedByAirlineInfo(
					AirlineHelper.getAirlineInfo(firstFlightSegment.getOperatingAirline()[0].getCode()));
		}
		segment.setDepartAirportInfo(
				AirportHelper.getAirportInfo(firstFlightSegment.getOriginLocation().getLocationCode()));
		setTerminalInfo(segment.getDepartAirportInfo(), firstFlightSegment.getOriginLocation().getTerminal(),
				firstFlightSegment.getOriginLocation().getTerminalCode());
		segment.setArrivalAirportInfo(
				AirportHelper.getAirportInfo(lastFlightSegment.getDestinationLocation().getLocationCode()));
		setTerminalInfo(segment.getArrivalAirportInfo(), lastFlightSegment.getDestinationLocation().getTerminal(),
				lastFlightSegment.getDestinationLocation().getTerminalCode());
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		segment.setDepartTime(LocalDateTime.parse(firstFlightSegment.getDepartureDateTime().getDateTime(), formatter));
		segment.setArrivalTime(SabreUtils.convertToLocalTime(segment.getDepartTime(),
				lastFlightSegment.getArrivalDateTime().getDateTime()));
		segment.setDuration(SabreUtils.calculateDuration(segment, firstFlightSegment.getElapsedTime()));
		List<AirportInfo> stopOvers = getStopOverAirports(item);
		if (CollectionUtils.isNotEmpty(stopOvers)) {
			segment.setStopOverAirports(stopOvers);
			segment.setStops(stopOvers.size());
		}

		Amenities amenities = new Amenities();
		amenities.setIsSmokingAllowed(firstFlightSegment.getSmokingAllowed());
		segment.setAmenities(amenities);
		segment.setIsReturnSegment(false);

		String mealCodes = "";
		if (ArrayUtils.isNotEmpty(firstFlightSegment.getMeal())) {
			for (Meal_type0 meal : firstFlightSegment.getMeal()) {
				mealCodes = StringUtils.join(mealCodes, meal.getCode());
			}
		}
		boolean isMealIncluded = isFreeMeal(mealCodes);

		List<FlightTravellerInfo> travellerInfos = fetchTravellerInfos(segment, activePriceQuotes, item, itemIndex,
				firstFlightSegment, cabin, isMealIncluded, passportInfoMap, departureDate);
		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfos).build();
		segment.setBookingRelatedInfo(bookingRelatedInfo);

		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		segment.getPriceInfoList().add(priceInfo);

		return segment;
	}

	private void setTerminalInfo(AirportInfo airportInfo, String terminal, String terminalCode) {
		if (StringUtils.isNotEmpty(terminal)) {
			airportInfo.setTerminal(terminal);
		} else if (StringUtils.isNotEmpty(terminalCode)) {
			airportInfo.setTerminal(AirUtils.getTerminalInfo(terminalCode));
		}
	}

	private List<AirportInfo> getStopOverAirports(Item_type0 item) {
		List<AirportInfo> stopOverAirports = new ArrayList<>();
		// ToDo
		/*
		 * if (ArrayUtils.isNotEmpty(item.getProduct().getProductDetails().getProductDetailsTypeChoice_type0().getAir().
		 * getHiddenStop())) { item.getProduct().getProductDetails().getAir().getHiddenStop().forEach(stop -> {
		 * stopOverAirports.add(AirportHelper.getAirportInfo(stop.getAirport())); }); }
		 */
		return stopOverAirports;
	}

	private GstInfo getGstInfo(TravelItineraryReadRS travelItineraryResponse) {
		GstInfo gstInfo = new GstInfo();
		SpecialServiceInfo_type0[] specialServicesInfos =
				travelItineraryResponse.getTravelItinerary().getSpecialServiceInfo();

		try {
			for (SpecialServiceInfo_type0 specialService : specialServicesInfos) {
				if (StringUtils.isNotEmpty(specialService.getService().getSSR_Type())) {
					if (specialService.getService().getSSR_Type().equals("GSTN")) {
						String[] gstData = specialService.getService().getText()[0].split("/");
						if (gstData.length >= 4) {
							gstInfo.setGstNumber(gstData[2]);
							gstInfo.setRegisteredName(gstData[3]);
						}
					} else if (specialService.getService().getSSR_Type().equals("GSTA")) {
						String[] gstData = specialService.getService().getText()[0].split("/");
						if (gstData.length >= 7) {
							gstInfo.setAddress(gstData[2]);
							gstInfo.setCityName(gstData[4]);
							gstInfo.setState(gstData[5]);
							gstInfo.setPincode(gstData[6]);
						}
					} else if (specialService.getService().getSSR_Type().equals("GSTP")) {
						String[] gstData = specialService.getService().getText()[0].split("/");
						if (gstData.length >= 3) {
							gstInfo.setMobile(gstData[2]);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("unable to fetch GST Info for supplierRefId {}", supplierRefId);
		}
		return gstInfo;
	}

	private void parseTravellerSpecialServices(List<SegmentInfo> segmentInfos,
			SpecialServiceInfo_type0[] specialServicesInfos) {

		if (CollectionUtils.isNotEmpty(segmentInfos) && ArrayUtils.isNotEmpty(specialServicesInfos)) {

			Map<SSRType, List<? extends SSRInformation>> preSSR = getPreSSRMap(segmentInfos.get(0));
			for (SegmentInfo segment : segmentInfos) {
				for (FlightTravellerInfo travellerInfo : segment.getBookingRelatedInfo().getTravellerInfo()) {
					setTravellerSSR(travellerInfo,
							StringUtils.join(segment.getDepartureAirportCode(), segment.getArrivalAirportCode()),
							specialServicesInfos, preSSR);
				}
			}

		}

	}

	/*
	 * To reduce iterations, forming map with travellerName(LastName/FirstName) as key and
	 * (Nationality/PassportNumber/DOB/ExpiryDate) as value.
	 */
	private Map<String, String> getPassportInfoMap(SpecialServiceInfo_type0[] specialServicesInfos) {

		Map<String, String> passportInfoMap = new HashMap<>();
		try {
			List<SpecialServiceInfo_type0> travellerDocs = new ArrayList<>();

			// Filtering passport DOCS from list SpecialServiceInfo.
			for (SpecialServiceInfo_type0 specialService : specialServicesInfos) {

				// Traveller document information can be found in the SpecialServiceInfo which has SSRType as "DOCS"
				if (specialService.getService() != null
						&& "DOCS".equalsIgnoreCase(specialService.getService().getSSR_Type())
						&& ArrayUtils.isNotEmpty(specialService.getService().getText())) {

					/*
					 * eg : HK1/P/IN/SFH37HDED/IN/14AUG1998/M/13AUG2026/KUMAR/YOGESH
					 * PNRStatus/DOCTYPE/Nationality/Passport Number/Issued country/DOB/Gender/DOC ExpiryDate/Last
					 * Name/First Name.
					 */
					String[] serviceTextArray = specialService.getService().getText()[0].split("/");
					if (ArrayUtils.isNotEmpty(serviceTextArray) && serviceTextArray.length > 2
							&& "P".equalsIgnoreCase(serviceTextArray[1])) {
						travellerDocs.add(specialService);
					}
				}
			}
			if (CollectionUtils.isNotEmpty(travellerDocs)) {
				for (SpecialServiceInfo_type0 travellerDoc : travellerDocs) {
					String[] serviceTextArray = travellerDoc.getService().getText()[0].split("/");
					if (serviceTextArray.length == 10) {
						StringJoiner value = new StringJoiner("/");
						value.add(serviceTextArray[2]);
						value.add(serviceTextArray[3]);
						value.add(serviceTextArray[5]);
						value.add(serviceTextArray[7]);
						String key = StringUtils
								.join(serviceTextArray[8], "/",
										(serviceTextArray[9] != null) ? removeTitleFromKey(serviceTextArray[9]) : "")
								.trim();
						passportInfoMap.put(key.toString().toUpperCase(), value.toString());
					}
				}
			}
		} catch (Exception e) {
			log.error("unable to fetch Passport Info for supplierRefId {}", supplierRefId);
		}


		return passportInfoMap;
	}

	private String removeTitleFromKey(String givenName) {
		givenName = givenName.trim();
		for (String title : titleList) {
			if (givenName.substring(givenName.lastIndexOf(" ") + 1).toLowerCase().contains(title)) {
				givenName = givenName.substring(0, givenName.lastIndexOf(" ") + 1);
			}
		}
		return givenName;
	}

	private void setTravellerSSR(FlightTravellerInfo travellerInfo, String segmentKey,
			SpecialServiceInfo_type0[] specialServicesInfos, Map<SSRType, List<? extends SSRInformation>> preSSR) {
		String travellerName = getTravellerNameKey(travellerInfo);
		if (MapUtils.isNotEmpty(preSSR)) {
			for (SpecialServiceInfo_type0 service : specialServicesInfos) {
				if (ArrayUtils.isNotEmpty(service.getService().getText())
						&& service.getService().getText()[0].contains(segmentKey)
						&& ArrayUtils.isNotEmpty(service.getService().getPersonName())) {
					if (travellerName.equalsIgnoreCase(
							service.getService().getPersonName()[0].getString().replaceAll(" ", ""))) {
						for (Entry<SSRType, List<? extends SSRInformation>> entry : preSSR.entrySet()) {
							if (CollectionUtils.isNotEmpty(entry.getValue())) {
								for (SSRInformation ssr : entry.getValue()) {
									if (!"INFT".equalsIgnoreCase(ssr.getCode())
											&& ssr.getCode().equals(service.getService().getSSR_Type())) {
										if (SSRType.MEAL.equals(entry.getKey())) {
											travellerInfo.setSsrMealInfo(ssr);
										} else if (SSRType.SEAT.equals(entry.getKey())) {
											travellerInfo.setSsrSeatInfo(ssr);
										} else if (SSRType.EXTRASERVICES.equals(entry.getKey())) {
											List<SSRInformation> extraServices =
													(CollectionUtils.isEmpty(travellerInfo.getExtraServices()))
															? new ArrayList<>()
															: travellerInfo.getExtraServices();
											extraServices.add(ssr);
											travellerInfo.setExtraServices(extraServices);
										}
									}
								}
							}
						}
					} else if (service.getService().getSSR_Code().equalsIgnoreCase("SSR")
							&& service.getService().getSSR_Type().equalsIgnoreCase("INFT")
							&& service.getService().getText()[0].replaceAll(" ", "").toLowerCase()
									.contains(travellerName.toLowerCase())) {
						String[] infantFreeText = service.getService().getText()[0].split("/");
						if (infantFreeText != null && infantFreeText.length == 4) {
							try {
								travellerInfo.setDob(
										TgsDateUtils.convertStringToDate(infantFreeText[3], "ddMMMyy").toLocalDate());
							} catch (ParseException e) {
								log.error("Exception while parsing DOB for infant traveller {} for supplierRefId {}",
										travellerInfo.getFullName(), supplierRefId);
							}
						}
					}
				}
			}
		}
	}

	/**
	 * 
	 * @param travellerInfo
	 * @return traveller name in sabre format.
	 */
	private String getTravellerNameKey(FlightTravellerInfo travellerInfo) {
		String key = StringUtils.join(travellerInfo.getLastName(), "/", travellerInfo.getFirstName(),
				travellerInfo.getTitle());
		return key.replace(" ", "");
	}

	private Map<SSRType, List<? extends SSRInformation>> getPreSSRMap(SegmentInfo segmentInfo) {
		Map<SSRType, List<? extends SSRInformation>> ssrInfos =
				AirUtils.getPreSSR(configuration.getBasicInfo(), segmentInfo.getAirlineCode(true), bookingUser);
		return ssrInfos;
	}

	private DeliveryInfo fetchDeliveryInfo(TravelItineraryReadRS travelItineraryResponse) {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		List<String> contacts = new ArrayList<>();

		try {
			if (travelItineraryResponse.getTravelItinerary() != null
					&& travelItineraryResponse.getTravelItinerary().getCustomerInfo() != null) {
				CustomerInfo_type0 customerInfo = travelItineraryResponse.getTravelItinerary().getCustomerInfo();
				if (customerInfo.getContactNumbers() != null) {
					String number = customerInfo.getContactNumbers().getContactNumber()[0].getPhone();
					if (number.contains("-")) {
						// to split version number
						number = number.split("-")[0];
						contacts.add(number);
					} else {
						contacts.add(customerInfo.getContactNumbers().getContactNumber()[0].getPhone());
					}
				}
			}
		} catch (Exception e) {
			log.error("unable to fetch Delivery Info for supplierRefId {}", supplierRefId);
		}

		// To do set email in delivery info.

		deliveryInfo.setContacts(contacts);
		return deliveryInfo;
	}

	/**
	 * If connecting time between segments is more than 1440 minutes (24 hrs) then it is considered as new trip.
	 */
	public List<TripInfo> getTripInfos(List<SegmentInfo> segmentInfos) {
		if (CollectionUtils.isNotEmpty(segmentInfos)) {
			List<TripInfo> tripInfos = new ArrayList<>();
			TripInfo tripInfo = new TripInfo();
			int segmentNum = 0;
			for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
				SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
				segmentInfo.setSegmentNum(segmentNum);
				if (segmentIndex + 1 == segmentInfos.size()
						|| AirUtils.getConnectingTime(segmentInfo, segmentInfos.get(segmentIndex + 1)) > 1440) {
					tripInfo.getSegmentInfos().add(segmentInfo);
					tripInfos.add(tripInfo);
					tripInfo = new TripInfo();
					segmentNum = 0;
				} else {
					tripInfo.getSegmentInfos().add(segmentInfo);
					segmentNum++;
				}
			}

			if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
				tripInfos.add(tripInfo);
			}

			// To set return segment
			if (SabreUtils.setIsReturnSegment(tripInfos, bookingUser)) {
				tripInfos.get(1).getSegmentInfos().forEach(segment -> {
					segment.setIsReturnSegment(true);
				});
			}
			return tripInfos;
		}
		return null;
	}
}
