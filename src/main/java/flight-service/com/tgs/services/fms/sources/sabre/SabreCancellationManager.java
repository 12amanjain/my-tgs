package com.tgs.services.fms.sources.sabre;

import java.rmi.RemoteException;
import java.util.List;
import java.util.ArrayList;
import java.math.BigInteger;
import com.sabre.services.res.tir.v3_9.FlightSegment_type2;
import com.sabre.services.res.tir.v3_9.Item_type0;
import com.sabre.webservices.sabrexml._2011._10.OTA_CancelRS;
import com.sabre.webservices.sabrexml._2011._10.Segment_type0;
import com.sabre.webservices.sabrexml._2011._10.OTA_CancelRQ;
import com.sabre.webservices.websvc.OTA_CancelServiceStub;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.Setter;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRS;
import com.sabre.services.stl_header.v120.CompletionCodes;
import lombok.experimental.SuperBuilder;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.ebxml.www.namespaces.messageheader.MessageHeader;

@SuperBuilder
@Getter
@Setter
final class SabreCancellationManager extends SabreServiceManager {


	public boolean releaseHoldPNR(String supplierRefId) {
		boolean isAbortSuccessfull = false;
		TravelItineraryReadRS tirResponse = travelItineraryReadLLS(supplierRefId);
		if (tirResponse != null) {
			List<Integer> activeSegmentNumbers = getActiveFlightSegmentNumbers(tirResponse);
			/*
			 * if activeSegmentNumbers is empty then there are no active segments in PNR, which means airline cancelled
			 * the flight segments for the PNR because of hold time limit exceeds.
			 */
			isAbortSuccessfull = true;

			if (CollectionUtils.isNotEmpty(activeSegmentNumbers)) {
				isAbortSuccessfull = otaCancel(activeSegmentNumbers);
				endTransaction();
			}
		}
		return isAbortSuccessfull;
	}

	private boolean otaCancel(List<Integer> activeSegmentNumbers) {
		boolean isAbortSuccessfull = false;
		OTA_CancelRQ cancellationRQ = null;
		OTA_CancelServiceStub service = null;
		try {
			cancellationRQ = getCancellationRequest(activeSegmentNumbers);
			service = bindingService.getCancelService();
			listener.setType(AirUtils.getLogType("OTA_Cancel", configuration));
			service._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.OTA_CANCEL_SERVICE, SabreConstants.CONVERSATION_ID);
			OTA_CancelRS cancellationRS = service.oTA_CancelRQ(cancellationRQ, messageHeader, getSecurity());
			if (cancellationRS != null && cancellationRS.getApplicationResults() != null
					&& CompletionCodes.Complete.equals(cancellationRS.getApplicationResults().getStatus())) {
				isAbortSuccessfull = true;
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} finally {
			service._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isAbortSuccessfull;

	}

	private List<Integer> getActiveFlightSegmentNumbers(TravelItineraryReadRS tirResponse) {
		List<Integer> activeSegmentNumbers = new ArrayList<>();
		if (tirResponse != null && tirResponse.getTravelItinerary() != null
				&& tirResponse.getTravelItinerary().getItineraryInfo() != null
				&& tirResponse.getTravelItinerary().getItineraryInfo().getReservationItems() != null) {
			Item_type0[] reservationItems =
					tirResponse.getTravelItinerary().getItineraryInfo().getReservationItems().getItem();
			for (int itemCount = 0; itemCount < reservationItems.length; itemCount++) {
				boolean isAllsegmentActive = true;
				if (ArrayUtils.isNotEmpty(reservationItems[itemCount].getFlightSegment())) {
					for (FlightSegment_type2 flightSegment : reservationItems[itemCount].getFlightSegment()) {
						if (SabreConstants.SEGMENT_CANCELLED_STATUS.equals(flightSegment.getStatus()))
							isAllsegmentActive = false;
					}
					if (isAllsegmentActive)
						activeSegmentNumbers.add(itemCount + 1);
				}
			}
		}
		return activeSegmentNumbers;
	}

	private OTA_CancelRQ getCancellationRequest(List<Integer> activeSegmentNumbers) {
		OTA_CancelRQ cancellationRQ = new OTA_CancelRQ();
		cancellationRQ.setNumResponses("1");
		cancellationRQ.setReturnHostCommand(true);
		cancellationRQ.setVersion(SabreConstants.OTA_CANCEL_VERSION);
		for (Integer segmentNumber : activeSegmentNumbers) {
			Segment_type0 segment = new Segment_type0();
			segment.setNumber(BigInteger.valueOf(segmentNumber.longValue()));
			cancellationRQ.addSegment(segment);
		}
		return cancellationRQ;
	}

}
