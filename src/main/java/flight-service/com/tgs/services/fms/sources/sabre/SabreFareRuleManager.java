package com.tgs.services.fms.sources.sabre;

import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import com.sabre.webservices.websvc.OTA_AirRulesServiceStub;
import com.sabre.webservices.websvc.StructureFareRulesRQStub;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import com.sabre.services.stl_header.v120.CompletionCodes;
import com.sabre.webservices.sabrexml._2003._07.AccountCodeType;
import com.sabre.webservices.sabrexml._2003._07.AirItinerary_type0;
import com.sabre.webservices.sabrexml._2003._07.AlphaNumCharType;
import com.sabre.webservices.sabrexml._2003._07.ArrivalAirport_type0;
import com.sabre.webservices.sabrexml._2003._07.BookingCodeType;
import com.sabre.webservices.sabrexml._2003._07.CarrierCodeType;
import com.sabre.webservices.sabrexml._2003._07.CurrencyCodeType;
import com.sabre.webservices.sabrexml._2003._07.DepartureAirport_type0;
import com.sabre.webservices.sabrexml._2003._07.FareBasisCodeType;
import com.sabre.webservices.sabrexml._2003._07.LocationType;
import com.sabre.webservices.sabrexml._2003._07.MarketingAirline_type0;
import com.sabre.webservices.sabrexml._2003._07.OTADateTimeType;
import com.sabre.webservices.sabrexml._2003._07.OperatingAirline_type0;
import com.sabre.webservices.sabrexml._2003._07.OriginDestinationOption_type0;
import com.sabre.webservices.sabrexml._2003._07.OriginDestinationOptions_type0;
import com.sabre.webservices.sabrexml._2003._07.PassengerDetail_type0;
import com.sabre.webservices.sabrexml._2003._07.PassengerTypeCodeType;
import com.sabre.webservices.sabrexml._2003._07.PassengerType_type0;
import com.sabre.webservices.sabrexml._2003._07.PassengerTypes_type0;
import com.sabre.webservices.sabrexml._2003._07.PaxTypeInformation_type0;
import com.sabre.webservices.sabrexml._2003._07.PenaltyInfoType;
import com.sabre.webservices.sabrexml._2003._07.PriceRequestInformation_type0;
import com.sabre.webservices.sabrexml._2003._07.RealReservationStatus_type0;
import com.sabre.webservices.sabrexml._2003._07.SegmentInformation_type0;
import com.sabre.webservices.sabrexml._2003._07.StructureFareRulesRS;
import com.sabre.webservices.sabrexml._2003._07.ValidatingCarrier_type0;
import com.sabre.webservices.sabrexml._2011._10.Account_type0;
import com.sabre.webservices.sabrexml._2011._10.DateTime;
import com.sabre.webservices.sabrexml._2011._10.DestinationLocation_type0;
import com.sabre.webservices.sabrexml._2011._10.FareBasis_type0;
import com.sabre.webservices.sabrexml._2011._10.FlightSegment_type0;
import com.sabre.webservices.sabrexml._2011._10.MarketingCarrier_type0;
import com.sabre.webservices.sabrexml._2011._10.OTA_AirRulesRQ;
import com.sabre.webservices.sabrexml._2011._10.OTA_AirRulesRS;
import com.sabre.webservices.sabrexml._2011._10.OptionalQualifiers_type0;
import com.sabre.webservices.sabrexml._2011._10.OriginDestinationInformation_type0;
import com.sabre.webservices.sabrexml._2011._10.OriginLocation_type0;
import com.sabre.webservices.sabrexml._2011._10.Paragraph_type0;
import com.sabre.webservices.sabrexml._2011._10.PricingQualifiers_type0;
import com.sabre.webservices.sabrexml._2011._10.RuleReqInfo_type0;
import com.sabre.webservices.websvc.OTA_AirRulesService;
import com.sabre.webservices.websvc.StructureFareRulesRQ;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.manager.FareRuleManager;
import com.tgs.services.fms.utils.AirUtils;
import lombok.Builder;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class SabreFareRuleManager extends SabreServiceManager {

	protected TripInfo selectedTrip;

	protected String bookingId;

	@Builder.Default
	protected Map<PaxType, Set<String>> paxTypeFareBasis = null;

	public TripFareRule getTripFareRule(boolean isDetailed) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		if (isDetailed) {
			getDetailedFareRule(tripFareRule);
		} else {
			FareRuleInformation fareRuleInfo = getStructuredFareRule();
			if (fareRuleInfo != null) {
				tripFareRule.getFareRule().put(FareRuleManager.getRuleKey(selectedTrip), fareRuleInfo);
			}
		}
		return tripFareRule;
	}

	protected FareRuleInformation getStructuredFareRule() {
		StructureFareRulesRS structureFareRulesRS = null;
		StructureFareRulesRQStub serviceStub = bindingService.getFareRuleSS();
		try {
			listener.setType(AirUtils.getLogType("StructureFareRule", configuration));
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			paxTypeFareBasis = new HashMap<>();
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.STRUCTURE_FARE_RULE, SabreConstants.CONVERSATION_ID);
			com.sabre.webservices.sabrexml._2003._07.StructureFareRulesRQ structureFareRulesRQ =
					getStructureFareRulesRQ();
			structureFareRulesRS = serviceStub.structureFareRulesRQ(structureFareRulesRQ, messageHeader, getSecurity());
			if (Objects.isNull(structureFareRulesRS.getStructureFareRulesRSSequence_type0().getSummary())) {
				log.error("Unable to find fare rule for searchId {} ", bookingId);
				return null;
			}
			FareRuleInformation fareRuleInfo = parseStructureFareRules(structureFareRulesRS);
			return fareRuleInfo;
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	// AirFareRule tries with trip. if fare rule fails for trip then it tries with segments.
	protected void getDetailedFareRule(TripFareRule tripFareRule) {
		List<TripInfo> tripInfos = AirUtils.splitTripInfo(selectedTrip, false);
		tripInfos.forEach(trip -> {
			FareRuleInformation fareRuleInfo = airFareRule(trip, null);
			if (fareRuleInfo != null) {
				tripFareRule.getFareRule().put(
						StringUtils.join(trip.getDepartureAirportCode(), "-", trip.getArrivalAirportCode()),
						fareRuleInfo);
			} else {
				FareRuleInformation copyFareRuleInfo = null;
				String departAirportCode = trip.getSegmentInfos().get(0).getDepartureAirportCode();
				String arrivalAirportCode = trip.getSegmentInfos().get(0).getArrivalAirportCode();
				for (SegmentInfo segment : trip.getSegmentInfos()) {
					fareRuleInfo = airFareRule(null, segment);
					if (Objects.nonNull(fareRuleInfo)) {
						if (tripFareRule.getFareRule()
								.get(StringUtils.join(departAirportCode, "-", arrivalAirportCode)) != null) {
							departAirportCode = segment.getDepartureAirportCode();
						}
						tripFareRule.getFareRule().put(
								StringUtils.join(departAirportCode, "-", segment.getArrivalAirportCode()),
								fareRuleInfo);
						departAirportCode = segment.getDepartureAirportCode();
						arrivalAirportCode = segment.getArrivalAirportCode();
						copyFareRuleInfo = fareRuleInfo;
					} else {
						tripFareRule.getFareRule().remove(StringUtils.join(departAirportCode, "-", arrivalAirportCode));
						arrivalAirportCode = segment.getArrivalAirportCode();
						if (copyFareRuleInfo != null) {
							tripFareRule.getFareRule().put(StringUtils.join(departAirportCode, "-", arrivalAirportCode),
									copyFareRuleInfo);
						}
					}
				}
			}
		});

	}

	private FareRuleInformation airFareRule(TripInfo tripInfo, SegmentInfo segment) {
		FareRuleInformation fareRuleInfo = null;
		OTA_AirRulesServiceStub serviceStub = bindingService.getRulesService();
		OTA_AirRulesRQ airRuleRequest = getAirRulesRq(tripInfo, segment);
		try {
			listener.setType(AirUtils.getLogType("OTA_AirRules", configuration));
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.AIR_FARE_RULES, SabreConstants.CONVERSATION_ID);
			OTA_AirRulesRS airRuleResponse = serviceStub.oTA_AirRulesRQ(airRuleRequest, messageHeader, getSecurity());
			if (airRuleResponse.getApplicationResults().getStatus() == CompletionCodes.Complete) {
				Map<String, String> fareRuleMap = parseAirFareRules(airRuleResponse);
				fareRuleInfo = new FareRuleInformation();
				for (String key : fareRuleMap.keySet()) {
					fareRuleInfo.getMiscInfo().put(key, fareRuleMap.get(key).toString().replaceAll("\n", ""));
				}
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return fareRuleInfo;
	}

	private Map<String, String> parseAirFareRules(OTA_AirRulesRS airRuleResponse) {
		Map<String, String> fareRuleMap = new HashMap<>();
		if (!(Objects.isNull(airRuleResponse.getFareRuleInfo())
				|| Objects.isNull(airRuleResponse.getFareRuleInfo().getRules())
				|| Objects.isNull(airRuleResponse.getFareRuleInfo().getRules().getParagraph()))) {
			for (Paragraph_type0 paragraph : airRuleResponse.getFareRuleInfo().getRules().getParagraph()) {
				fareRuleMap.put(paragraph.getTitle(), paragraph.getText());
			}
		}
		return fareRuleMap;
	}

	private OTA_AirRulesRQ getAirRulesRq(TripInfo tripInfo, SegmentInfo segment) {
		OTA_AirRulesRQ airRulesRQ = new OTA_AirRulesRQ();
		airRulesRQ.setVersion(SabreConstants.AIR_RULES_VERSION);
		airRulesRQ.setReturnHostCommand(true);
		String accountCode = "";
		FareBasis_type0 fareBasis = new FareBasis_type0();
		if (tripInfo != null) {
			airRulesRQ.setOriginDestinationInformation(getOriginDestinationInfo(tripInfo.getDepartureAirportCode(),
					tripInfo.getArrivalAirportCode(), tripInfo.getDepartureTime(), tripInfo.getAirlineCode()));
			accountCode = SabreUtils.getAccountCodeFromSegment(tripInfo.getSegmentInfos().get(0));
			fareBasis.setCode(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getFareBasis(PaxType.ADULT));
		} else {
			airRulesRQ.setOriginDestinationInformation(getOriginDestinationInfo(segment.getDepartureAirportCode(),
					segment.getArrivalAirportCode(), segment.getDepartTime(), segment.getAirlineCode(false)));
			accountCode = SabreUtils.getAccountCodeFromSegment(segment);
			fareBasis.setCode(segment.getPriceInfo(0).getFareBasis(PaxType.ADULT));
		}
		if (StringUtils.isNotEmpty(accountCode)) {
			OptionalQualifiers_type0 optionalQualifiers = new OptionalQualifiers_type0();
			PricingQualifiers_type0 pricingQualifiers = new PricingQualifiers_type0();
			Account_type0 account = new Account_type0();
			String[] accArray = new String[1];
			accArray[0] = accountCode;
			account.setCode(accArray);
			pricingQualifiers.setAccount(account);
			optionalQualifiers.setPricingQualifiers(pricingQualifiers);
			airRulesRQ.setOptionalQualifiers(optionalQualifiers);
		}
		RuleReqInfo_type0 ruleReqInfo = new RuleReqInfo_type0();
		ruleReqInfo.setFareBasis(fareBasis);
		airRulesRQ.setRuleReqInfo(ruleReqInfo);
		return airRulesRQ;
	}

	private OriginDestinationInformation_type0 getOriginDestinationInfo(String departureAirportCode,
			String arrivalAirportCode, LocalDateTime departureTime, String airlineCode) {
		OriginDestinationInformation_type0 originDestinationInfo = new OriginDestinationInformation_type0();
		FlightSegment_type0[] flightArray = new FlightSegment_type0[1];
		FlightSegment_type0 flightSegment = new FlightSegment_type0();
		flightSegment
				.setDepartureDateTime(getDateTime(StringUtils.join(String.format("%02d", departureTime.getMonthValue()),
						"-", String.format("%02d", departureTime.getDayOfMonth()))));
		MarketingCarrier_type0 marketingCarrier = new MarketingCarrier_type0();
		marketingCarrier.setCode(airlineCode);
		flightSegment.setMarketingCarrier(marketingCarrier);
		DestinationLocation_type0 destinLoc = new DestinationLocation_type0();
		OriginLocation_type0 originLoc = new OriginLocation_type0();
		destinLoc.setLocationCode(arrivalAirportCode);
		originLoc.setLocationCode(departureAirportCode);
		flightSegment.setDestinationLocation(destinLoc);
		flightSegment.setOriginLocation(originLoc);
		flightArray[0] = flightSegment;
		originDestinationInfo.setFlightSegment(flightArray);
		return originDestinationInfo;
	}

	private DateTime getDateTime(String dateText) {
		DateTime dateTime = new DateTime();
		dateTime.setDateTime(dateText);
		return dateTime;
	}

	private com.sabre.webservices.sabrexml._2003._07.StructureFareRulesRQ getStructureFareRulesRQ() {
		com.sabre.webservices.sabrexml._2003._07.StructureFareRulesRQ structureFareRuleRQ =
				new com.sabre.webservices.sabrexml._2003._07.StructureFareRulesRQ();
		structureFareRuleRQ.setPriceRequestInformation(getPriceRequestInfo());
		structureFareRuleRQ.setAirItinerary(getAirItinerary());
		return structureFareRuleRQ;
	}

	private AirItinerary_type0 getAirItinerary() {
		AirItinerary_type0 airItinerary = new AirItinerary_type0();
		airItinerary.setOriginDestinationOptions(getOriginDestinationOption());
		return airItinerary;
	}

	private OriginDestinationOptions_type0 getOriginDestinationOption() {
		OriginDestinationOptions_type0 originDestinationOption = new OriginDestinationOptions_type0();
		originDestinationOption.setOriginDestinationOption(getOriginDestination());
		return originDestinationOption;
	}

	private OriginDestinationOption_type0[] getOriginDestination() {
		List<OriginDestinationOption_type0> originDestinationList = new ArrayList<>();
		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			OriginDestinationOption_type0 originDestination = new OriginDestinationOption_type0();
			com.sabre.webservices.sabrexml._2003._07.FlightSegment_type0 flightSegment =
					new com.sabre.webservices.sabrexml._2003._07.FlightSegment_type0();
			flightSegment.setArrivalDate(getDate(segmentInfo.getArrivalTime()));
			flightSegment.setDepartureDate(getDate(segmentInfo.getDepartTime()));
			flightSegment.setBookingDate(getDate(null));
			flightSegment.setFlightNumber(Short.valueOf(segmentInfo.getFlightDesignator().getFlightNumber()));
			flightSegment.setRealReservationStatus(getRealReservationStatus(SabreConstants.OK_STATUS));
			flightSegment.setResBookDesigCode(
					getBookingCodeType(segmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getClassOfBooking()));
			flightSegment.setSegmentNumber(getSegmentNumber(segmentInfo.getSegmentNum()));
			flightSegment.setSegmentType(getAlphaNumericType("A"));
			flightSegment.setDepartureAirport(getDepartureAirport(segmentInfo.getDepartAirportInfo().getCode()));
			flightSegment.setArrivalAirport(getArrivalAirport(segmentInfo.getArrivalAirportInfo().getCode()));
			flightSegment.setMarketingAirline(
					getMarketingAirline(segmentInfo.getFlightDesignator().getAirlineInfo().getCode()));
			String operatingAirlineCode = (segmentInfo.getOperatedByAirlineInfo() != null
					&& StringUtils.isNotEmpty(segmentInfo.getOperatedByAirlineInfo().getCode()))
							? segmentInfo.getOperatedByAirlineInfo().getCode()
							: segmentInfo.getFlightDesignator().getAirlineInfo().getCode();
			flightSegment.setOperatingAirline(getOperatingAirline(operatingAirlineCode));
			originDestination.setFlightSegment(flightSegment);
			originDestination.setSegmentInformation(getSegmentInformation(segmentInfo.getSegmentNum()));
			originDestination.setPaxTypeInformation(getPaxTypeInfo(segmentInfo));
			originDestinationList.add(originDestination);
		}
		return originDestinationList.toArray(new OriginDestinationOption_type0[0]);
	}

	private AlphaNumCharType getAlphaNumericType(String type) {
		AlphaNumCharType alphaNumericType = new AlphaNumCharType();
		alphaNumericType.setAlphaNumCharType(type);
		return alphaNumericType;
	}

	private BookingCodeType getBookingCodeType(String classOfBooking) {
		BookingCodeType codeType = new BookingCodeType();
		codeType.setBookingCodeType(classOfBooking);
		return codeType;
	}

	private RealReservationStatus_type0 getRealReservationStatus(String okStatus) {
		RealReservationStatus_type0 resStatus = new RealReservationStatus_type0();
		resStatus.setRealReservationStatus_type0(okStatus);
		return resStatus;
	}

	private PaxTypeInformation_type0[] getPaxTypeInfo(SegmentInfo segmentInfo) {
		List<PaxTypeInformation_type0> paxTypeInfoList = new ArrayList<>();
		Map<PaxType, Integer> paxInfo = selectedTrip.getPaxInfo();
		for (PaxType paxType : PaxType.values()) {
			int paxCount = paxInfo.get(paxType);
			if (paxCount > 0) {
				PaxTypeInformation_type0 paxTypeInformation = new PaxTypeInformation_type0();
				paxTypeInformation.setFareBasisCode(
						getFareBasisCodeType(segmentInfo.getPriceInfo(0).getFareDetail(paxType).getFareBasis()));
				paxTypeInformation.setPassengerType(getPassengerTypeCode(paxType.getType()));
				// To do set FareComponentAmount and FareComponentNumber
				short fareComponentNumber = 1;
				if (paxTypeFareBasis.get(paxType) != null) {
					for (String fareBasis : paxTypeFareBasis.get(paxType)) {
						if (!fareBasis.equals(paxTypeInformation.getFareBasisCode())) {
							fareComponentNumber++;
						}
					}
				} else {
					paxTypeFareBasis.put(paxType, new LinkedHashSet<>());
				}
				paxTypeFareBasis.get(paxType).add(paxTypeInformation.getFareBasisCode().getFareBasisCodeType());
				paxTypeInformation.setFareComponentNumber(fareComponentNumber);
				paxTypeInfoList.add(paxTypeInformation);
			}
		}
		return paxTypeInfoList.toArray(new PaxTypeInformation_type0[0]);
	}

	private PassengerTypeCodeType getPassengerTypeCode(String type) {
		PassengerTypeCodeType passType = new PassengerTypeCodeType();
		passType.setPassengerTypeCodeType(type);
		return passType;
	}

	private FareBasisCodeType getFareBasisCodeType(String fareBasis) {
		FareBasisCodeType fareBasisType = new FareBasisCodeType();
		fareBasisType.setFareBasisCodeType(fareBasis);
		return fareBasisType;
	}

	private short getSegmentNumber(Integer segmentNumber) {
		return (short) (segmentNumber + 1);
	}

	private OperatingAirline_type0 getOperatingAirline(String airlineCode) {
		OperatingAirline_type0 operatingAirLine = new OperatingAirline_type0();
		operatingAirLine.setCode(getCarrierCode(airlineCode));
		return operatingAirLine;
	}

	private CarrierCodeType getCarrierCode(String airlineCode) {
		CarrierCodeType carrierType = new CarrierCodeType();
		carrierType.setCarrierCodeType(airlineCode);
		return carrierType;
	}

	private MarketingAirline_type0 getMarketingAirline(String airlineCode) {
		MarketingAirline_type0 marketingAirLine = new MarketingAirline_type0();
		marketingAirLine.setCode(getCarrierCode(airlineCode));
		return marketingAirLine;
	}

	private SegmentInformation_type0 getSegmentInformation(Integer segmentNum) {
		SegmentInformation_type0 segmentInfo = new SegmentInformation_type0();
		segmentInfo.setSegmentNumber(getSegmentNumber(segmentNum));
		return segmentInfo;
	}

	private DepartureAirport_type0 getDepartureAirport(String airPortCode) {
		DepartureAirport_type0 departureAirport = new DepartureAirport_type0();
		departureAirport.setLocationCode(getLocaltionType(airPortCode));
		return departureAirport;
	}

	private LocationType getLocaltionType(String airPortCode) {
		LocationType locationType = new LocationType();
		locationType.setLocationType(airPortCode);
		return locationType;
	}

	private ArrivalAirport_type0 getArrivalAirport(String airPortCode) {
		ArrivalAirport_type0 arrivalAirPort = new ArrivalAirport_type0();
		arrivalAirPort.setLocationCode(getLocaltionType(airPortCode));
		return arrivalAirPort;
	}

	private PriceRequestInformation_type0 getPriceRequestInfo() {
		PriceRequestInformation_type0 priceRequestInfo = new PriceRequestInformation_type0();
		priceRequestInfo.setBuyingDate(getDate(null));
		String accountCode = SabreUtils.getAccountCodeFromTrip(AirUtils.getSegmentInfos(Arrays.asList(selectedTrip)));
		com.sabre.webservices.sabrexml._2003._07.Account_type0[] accountArray =
				new com.sabre.webservices.sabrexml._2003._07.Account_type0[1];
		if (StringUtils.isNotEmpty(accountCode)) {
			com.sabre.webservices.sabrexml._2003._07.Account_type0 account =
					new com.sabre.webservices.sabrexml._2003._07.Account_type0();
			account.setCode(getAccountCodeType(accountCode));
			accountArray[0] = account;
		}
		priceRequestInfo.setAccount(accountArray);
		// To do get from currency code.
		priceRequestInfo.setCurrencyCode(getCurrencyCodeType(getCurrencyCode()));
		priceRequestInfo.setPassengerTypes(getPassengerTypes());
		priceRequestInfo.setValidatingCarrier(getValidatingCarrierCode());
		return priceRequestInfo;
	}

	private CurrencyCodeType getCurrencyCodeType(String currencyCode) {
		if (StringUtils.isNotBlank(currencyCode)) {
			CurrencyCodeType currencyCodeType = new CurrencyCodeType();
			currencyCodeType.setCurrencyCodeType(currencyCode);
			return currencyCodeType;
		}
		return null;
	}

	private AccountCodeType getAccountCodeType(String accountCode) {
		AccountCodeType accountCodeType = new AccountCodeType();
		accountCodeType.setAccountCodeType(accountCode);
		return accountCodeType;
	}

	private ValidatingCarrier_type0 getValidatingCarrierCode() {
		ValidatingCarrier_type0 validatingCarrier = new ValidatingCarrier_type0();
		validatingCarrier.setCode(getCarrierCode(selectedTrip.getAirlineCode(false, 0)));
		return validatingCarrier;
	}

	private PassengerTypes_type0 getPassengerTypes() {
		PassengerTypes_type0 passengerTypes = new PassengerTypes_type0();
		Map<PaxType, Integer> paxInfo = selectedTrip.getPaxInfo();
		List<PassengerType_type0> passengerTypeList = new ArrayList<PassengerType_type0>();
		for (PaxType paxType : PaxType.values()) {
			int paxCount = paxInfo.get(paxType);
			if (paxCount > 0) {
				passengerTypeList.add(getPassengerType(paxType, paxCount));
			}
		}
		passengerTypes.setPassengerType(passengerTypeList.toArray(new PassengerType_type0[0]));
		return passengerTypes;
	}

	private PassengerType_type0 getPassengerType(PaxType paxType, int paxCount) {
		PassengerType_type0 passengerType = new PassengerType_type0();
		passengerType.setCode(getPassengerTypeCode(paxType.getType()));
		passengerType.setCount((short) paxCount);
		return passengerType;
	}

	private OTADateTimeType getDate(LocalDateTime localDateTime) {
		OTADateTimeType otaDate = new OTADateTimeType();
		if (Objects.isNull(localDateTime)) {
			localDateTime = LocalDateTime.now();
		}
		otaDate.setOTADateTimeType(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(localDateTime.withNano(0)));
		return otaDate;
	}

	private FareRuleInformation parseStructureFareRules(StructureFareRulesRS structureFareRulesRS) {
		PassengerDetail_type0 passengerDetail = findAdultPassengerDetails(structureFareRulesRS
				.getStructureFareRulesRSSequence_type0().getSummary().getPassengerDetails().getPassengerDetail());
		FareRuleInformation fareRuleInfo = new FareRuleInformation();
		if (passengerDetail != null && passengerDetail.getPenaltiesInfo() != null
				&& ArrayUtils.isNotEmpty(passengerDetail.getPenaltiesInfo().getPenalty())) {
			for (PenaltyInfoType penalty : passengerDetail.getPenaltiesInfo().getPenalty()) {
				if (penalty.getType().getValue().equals("Exchange")) {
					setFareRuleInfo(FareRulePolicyType.DATECHANGE, fareRuleInfo, penalty);
				} else if (penalty.getType().getValue().equals("Refund")) {
					setFareRuleInfo(FareRulePolicyType.CANCELLATION, fareRuleInfo, penalty);
				}
			}
		}
		return fareRuleInfo;
	}

	private void setFareRuleInfo(FareRulePolicyType policyType, FareRuleInformation fareRuleInfo,
			PenaltyInfoType penalty) {
		String key = getFareRuleKey(penalty.getApplicability().getValue());
		FareRulePolicyContent policyContent = new FareRulePolicyContent();
		/**
		 * Amount can be empty in case of Non-Refundable
		 */
		if (penalty.getAmount() != null)
			policyContent
					.setAmount(getAmountBasedOnCurrency(BigDecimal.valueOf(penalty.getAmount().getMonetaryAmountType()),
							penalty.getCurrencyCode().getCurrencyCodeType()));
		if (penalty.getChangeable()) {
			policyContent.setPolicyInfo(getExchangePolicy(penalty.getChangeable()));
		} else if (penalty.getRefundable()) {
			policyContent.setPolicyInfo(getCancellationPolicy(penalty.getRefundable()));
		}
		Map<FareRuleTimeWindow, FareRulePolicyContent> content = fareRuleInfo.getFareRuleInfo().get(policyType);
		if (content == null) {
			content = new HashMap<>();
		}
		content.put(FareRuleTimeWindow.getTimeWindow(key), policyContent);
		fareRuleInfo.getFareRuleInfo().put(policyType, content);
	}

	private PassengerDetail_type0 findAdultPassengerDetails(PassengerDetail_type0[] passengerDetailList) {
		for (PassengerDetail_type0 passengerDetail : passengerDetailList) {
			if (passengerDetail.getPassengerTypeCode().getPassengerTypeCodeType().equals(PaxType.ADULT.getType())) {
				return passengerDetail;
			}
		}
		return null;
	}

	private String getFareRuleKey(String serviceScenerio) {
		if (serviceScenerio.toLowerCase().equals("after")) {
			return "After Departure";
		}
		return "Before Departure";
	}

	private String getExchangePolicy(boolean isChangeable) {
		if (isChangeable) {
			return "Date Changeable with Penalty";
		}
		return "No Change Available for this Class of Booking";
	}

	private String getCancellationPolicy(boolean isRefundable) {
		if (isRefundable) {
			return "Refund available with Penalty";
		}
		return "No Cancellation/Refund Available for this Class of Booking";
	}

}
