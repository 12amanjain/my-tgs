package com.tgs.services.fms.sources.sabre;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.YearMonth;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicInteger;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeConstants;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Holder;
import com.sabre.webservices.websvc.*;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.opentravel.www.ota._2003._05.*;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transport.http.asyncclient.AsyncHTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.ebxml.www.namespaces.messageheader.From_type0;
import org.ebxml.www.namespaces.messageheader.MessageData_type0;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import org.ebxml.www.namespaces.messageheader.NonEmptyString;
import org.ebxml.www.namespaces.messageheader.PartyId_type0;
import org.ebxml.www.namespaces.messageheader.Service_type1;
import org.ebxml.www.namespaces.messageheader.To_type0;
import org.opentravel.www.ota._2002._11.POS_type0;
import org.opentravel.www.ota._2002._11.SessionCloseRQ;
import org.opentravel.www.ota._2002._11.SessionCloseRS;
import org.xmlsoap.schemas.ws._2002._12.secext.UsernameToken_type0;
import com.sabre.services.res.tir.v3_9.Service_type0;
import com.sabre.services.res.tir.v3_9.SpecialServiceInfo_type0;
import com.sabre.services.res.tir.v3_9.TravelItinerary_type0;
import com.sabre.services.res.tir.v3_9.Item_type0;
import com.sabre.services.res.tir.v3_9.ItinTotalFare_type0;
import com.sabre.services.res.tir.v3_9.PriceQuote_type0;
import com.sabre.services.res.tir.v3_9.PricedItinerary_type0;
import com.sabre.services.res.tir.v3_9.MessagingDetails_type0;
import com.sabre.services.res.tir.v3_9.SubjectAreas_type0;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRQ;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRS;
import com.sabre.services.res.tir.v3_9.UniqueID_type0;
import com.sabre.services.sp.eab.v3_10.Account_type0;
import com.sabre.services.sp.eab.v3_10.ItineraryOptions_type0;
import com.sabre.services.sp.eab.v3_10.OTA_AirPriceRQ_type0;
import com.sabre.services.sp.eab.v3_10.OptionalQualifiers_type0;
import com.sabre.services.sp.eab.v3_10.PassengerType_type1;
import com.sabre.services.sp.eab.v3_10.PriceRequestInformation_type0;
import com.sabre.services.sp.eab.v3_10.PricingQualifiers_type0;
import com.sabre.services.sp.eab.v3_10.SegmentSelect_type0;
import com.sabre.services.stl_header.v120.CompletionCodes;
import com.sabre.services.stl_messagecommon.v02_01.MessageCondition;
import com.sabre.services.stl_payload.v02_01.ApplicationResults;
import com.sabre.services.stl_payload.v02_01.ProblemInformation;
import com.sabre.services.stl_payload.v02_01.SystemSpecificResults;
import com.sabre.services.stl_payload.v02_01.Version_type0;
import com.sabre.webservices.sabrexml._2003._07.EchoToken_type0;
import com.sabre.webservices.sabrexml._2003._07.Output_type0;
import com.sabre.webservices.sabrexml._2003._07.Request_type0;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRQ;
import com.sabre.webservices.sabrexml._2003._07.SabreCommandLLSRS;
import com.sabre.webservices.sabrexml._2011._10.ChangeAAA_type0;
import com.sabre.webservices.sabrexml._2011._10.ContextChangeRQ;
import com.sabre.webservices.sabrexml._2011._10.ContextChangeRS;
import com.sabre.webservices.sabrexml._2011._10.DesignatePrinterRQ;
import com.sabre.webservices.sabrexml._2011._10.DesignatePrinterRS;
import com.sabre.webservices.sabrexml._2011._10.EndTransactionRQ;
import com.sabre.webservices.sabrexml._2011._10.EndTransactionRS;
import com.sabre.webservices.sabrexml._2011._10.EndTransaction_type0;
import com.sabre.webservices.sabrexml._2011._10.Profile_type0;
import com.sabre.webservices.sabrexml._2011._10.PseudoCityCode_type0;
import com.sabre.webservices.sabrexml._2011._10.Source_type0;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CxfRequestLogInterceptor;
import com.tgs.services.base.CxfResponseLogInterceptor;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineTimeLimitData;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.string.TgsStringUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
abstract class SabreServiceManager {

	protected Order order;

	protected SupplierConfiguration configuration;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected Client client;

	protected String binaryToken;

	public static final String PARTYIDTYPE = "urn:x12.org:IO5:01";

	protected List<SegmentInfo> segmentInfos;

	protected String pnr;

	protected String bookingId;

	protected AirSearchQuery searchQuery;

	private String receivedFrom;

	public SabreBindingService bindingService;

	protected DeliveryInfo deliveryInfo;

	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected SoapRequestResponseListner listener;

	// 1. "*" By default loaded on PCC level 2. "@" By Passing code and some times may very
	protected static List<String> PRIVATE_FARE_INDICATOR = Arrays.asList("*", "@");

	protected static List<String> PAID_MEAL_CODES = Arrays.asList("P", "F", "G", "N", "V");

	protected List<String> criticalMessageLogger;

	protected User bookingUser;

	public PartyId_type0[] getPartyId(String type, String value) {
		PartyId_type0[] partyIdArray = new PartyId_type0[1];
		PartyId_type0 partyId = new PartyId_type0();
		partyId.setNonEmptyString(value);
		partyId.setType(getNonEmptyString(type));
		partyIdArray[0] = partyId;
		return partyIdArray;
	}

	public org.ebxml.www.namespaces.messageheader.Service_type0 getService(String value) {
		org.ebxml.www.namespaces.messageheader.Service_type0 service =
				new org.ebxml.www.namespaces.messageheader.Service_type0();
		// service.setType(type);
		service.setNonEmptyString(value);
		return service;
	}

	public MessageHeader getMessageHeader(String cpAId, String action, String conversationId) {
		MessageHeader messageHeader = new MessageHeader();
		From_type0 from = new From_type0();
		from.setPartyId(getPartyId(PARTYIDTYPE, "999999"));
		messageHeader.setFrom(from);
		To_type0 toValue = new To_type0();
		toValue.setPartyId(getPartyId(PARTYIDTYPE, "123123"));
		messageHeader.setTo(toValue);
		messageHeader.setCPAId(getNonEmptyString(cpAId));
		messageHeader.setConversationId(getNonEmptyString(conversationId));
		messageHeader.setService(getService(action));
		messageHeader.setAction(getNonEmptyString(action));
		messageHeader.setVersion(getNonEmptyString("1.0.0"));
		messageHeader.setMessageData(getMessagedata(conversationId));
		return messageHeader;
	}

	private MessageData_type0 getMessagedata(String conversationId) {
		MessageData_type0 messagedata = new MessageData_type0();
		messagedata.setMessageId(getNonEmptyString(conversationId));
		messagedata.setTimestamp(LocalDateTime.now().toString());
		return messagedata;
	}

	public NonEmptyString getNonEmptyString(String value) {
		NonEmptyString type = new NonEmptyString();
		type.setNonEmptyString(value);
		return type;
	}

	public UsernameToken_type0 getUserNameToken(String pcc, String userName, String password) {
		UsernameToken_type0 userNameToken = new UsernameToken_type0();
		userNameToken.setDomain("DEFAULT");
		userNameToken.setOrganization(pcc);
		userNameToken.setPassword(password);
		userNameToken.setUsername(userName);
		return userNameToken;

	}

	public void bindInterceptors(Object port, String type, String key, boolean logVisibility) {
		bindInterceptors(port, type, key, true, logVisibility);
	}

	public void bindInterceptors(Object port, String type, String key) {
		bindInterceptors(port, type, key, true, false);
	}

	public void bindInterceptors(Object port, String type, String key, boolean storeLogs, boolean logVisibility) {
		String endpointURL = configuration.getSupplierCredential().getUrl();
		BindingProvider bp = (BindingProvider) port;
		bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);

		client = ClientProxy.getClient(port);

		if (StringUtils.isBlank(bookingId)) {
			HTTPConduit http = (HTTPConduit) client.getConduit();
			HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
			httpClientPolicy.setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			httpClientPolicy.setReceiveTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			http.setClient(httpClientPolicy);
			// client.getBus().setProperty(AsyncHTTPConduit.USE_ASYNC, Boolean.TRUE);
		}

		List<String> visibilityGroups = logVisibility ? AirSupplierUtils.getLogVisibility() : new ArrayList<>();
		client.getInInterceptors().add(new CxfResponseLogInterceptor(type, key, storeLogs, visibilityGroups));
		client.getOutInterceptors().add(new CxfRequestLogInterceptor(type, key, storeLogs, visibilityGroups));
	}

	public double getAmountBasedOnCurrency(BigDecimal amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue();
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(getType()).build();
		return moneyExchnageComm.getExchangeValue(amount.doubleValue(), filter, true);
	}

	public String getType() {
		AirSourceType sourceType = AirSourceType.getAirSourceType(configuration.getSourceId());
		return StringUtils.join(sourceType.name(), "_", configuration.getSupplierCredential().getPcc()).toUpperCase();
	}

	public String getCurrencyCode() {
		return configuration.getSupplierCredential().getCurrencyCode();
	}

	public Security getSecurity() {
		Security security = new Security();
		security.setBinarySecurityToken(binaryToken);
		return security;
	}

	protected TravelItineraryReadRS travelItineraryReadLLS(String blockedPnr) {
		TravelItineraryReadRQ travelItineraryRQ = new TravelItineraryReadRQ();
		TravelItineraryReadServiceStub serviceStub = bindingService.getTravelItin();
		listener.setType(AirUtils.getLogType("TravelItinerary", configuration));
		try {
			MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.TRAVEL_ITINERARY, SabreConstants.CONVERSATION_ID);
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			travelItineraryRQ.setVersion(SabreConstants.TRAVEL_ITINARY_READLLS_VERSION);
			MessagingDetails_type0 messagingDetails = new MessagingDetails_type0();
			SubjectAreas_type0 subjectAreas = new SubjectAreas_type0();
			List<String> subjectAreaList = new ArrayList<>();
			subjectAreaList.add("FULL");
			subjectAreaList.add("AIR_CABIN");
			subjectAreas.setSubjectArea(subjectAreaList.toArray(new String[0]));
			messagingDetails.setSubjectAreas(subjectAreas);
			travelItineraryRQ.setMessagingDetails(messagingDetails);
			UniqueID_type0 uniqueId = new UniqueID_type0();
			uniqueId.setID(blockedPnr);
			travelItineraryRQ.setUniqueID(uniqueId);
			TravelItineraryReadRS travelItineraryRS =
					serviceStub.travelItineraryReadRQ(travelItineraryRQ, messageHeaderHolder, getSecurity());
			isAnyWarning(travelItineraryRS.getApplicationResults());
			isAnyCritical(travelItineraryRS.getApplicationResults());
			if (travelItineraryRS.getApplicationResults().getStatus() == CompletionCodes.Complete) {
				return travelItineraryRS;
			}
		} catch (RemoteException e) {
			throw new SecurityException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	public boolean endTransaction() {
		boolean isSuccess = false;
		EndTransactionRQ endTransactionRQ = new EndTransactionRQ();
		EndTransactionServiceStub serviceStub = bindingService.getEndTS();
		try {
			MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.END_TRANSACTION, SabreConstants.CONVERSATION_ID);
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			listener.setType(AirUtils.getLogType("CancelEndTransaction", configuration));
			endTransactionRQ.setVersion(SabreConstants.END_TRANSACTION_VERSION);
			EndTransaction_type0 endTransaction = new EndTransaction_type0();
			endTransaction.setInd(true);
			endTransactionRQ.setEndTransaction(endTransaction);
			Source_type0 source = new Source_type0();
			source.setReceivedFrom(ObjectUtils.firstNonNull(receivedFrom, SabreConstants.RECEIVED_FROM));
			endTransactionRQ.setSource(source);
			EndTransactionRS endTransactionRS;

			endTransactionRS = serviceStub.endTransactionRQ(endTransactionRQ, messageHeaderHolder, getSecurity());

			isAnyWarning(endTransactionRS.getApplicationResults());
			isAnyCritical(endTransactionRS.getApplicationResults());
			if (endTransactionRS.getApplicationResults().getStatus() == CompletionCodes.Complete) {
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	public boolean sabreCommand(String command) {
		boolean isSuccess = false;
		SabreCommandLLSServiceStub serviceStub = bindingService.getSabreCommand();
		SabreCommandLLSRQ sabreCommandLLSRQ = new SabreCommandLLSRQ();
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.SABRE_COMMAND, SabreConstants.CONVERSATION_ID);
			sabreCommandLLSRQ.setEchoToken(getEchoToken());
			sabreCommandLLSRQ.setVersion(SabreConstants.SABRE_CMD_VERSION);
			Request_type0 request = new Request_type0();
			request.setOutput(Output_type0.SCREEN);
			request.setHostCommand(command);
			sabreCommandLLSRQ.setRequest(request);
			SabreCommandLLSRS sabreCommandLLSRS =
					serviceStub.sabreCommandLLSRQ(sabreCommandLLSRQ, messageHeaderHolder, getSecurity());
			if (Objects.nonNull(sabreCommandLLSRS.getResponse())
					&& !(sabreCommandLLSRS.getResponse().contains("RE-ENTER")
							|| sabreCommandLLSRS.getResponse().contains("NOT ENT BGNG WITH"))) {
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}


	public EchoToken_type0 getEchoToken() {
		EchoToken_type0 echoToken = new EchoToken_type0();
		echoToken.setEchoToken_type0(SabreConstants.ECHO_TOKEN);
		return echoToken;
	}

	public String changeAAA(String newPcc) {
		ContextChangeRQ contextChangeRQ = new ContextChangeRQ();
		ContextChangeServiceStub serviceStub = bindingService.getContextChange();
		try {
			listener.setType(AirUtils.getLogType("ChangeAAA", configuration));
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.CONTEXT_CHANGE, SabreConstants.CONVERSATION_ID);
			ChangeAAA_type0 value = new ChangeAAA_type0();
			value.setPseudoCityCode(getnewPcc(newPcc));
			contextChangeRQ.setVersion(SabreConstants.CONTEXT_CHANGE_VERSION);
			contextChangeRQ.setChangeAAA(value);
			ContextChangeRS contextChangeRS =
					serviceStub.contextChangeRQ(contextChangeRQ, messageHeaderHolder, getSecurity());

			isAnyWarning(contextChangeRS.getApplicationResults());
			isAnyCritical(contextChangeRS.getApplicationResults());
			if (contextChangeRS.getApplicationResults().getStatus() == CompletionCodes.Complete
					&& contextChangeRS.getSecurityToken() != null
					&& StringUtils.isNotEmpty(contextChangeRS.getSecurityToken().getString())) {
				return contextChangeRS.getSecurityToken().getString();
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}


	private PseudoCityCode_type0 getnewPcc(String value) {
		PseudoCityCode_type0 pseudoCity = new PseudoCityCode_type0();
		pseudoCity.setPseudoCityCode_type0(value);
		return pseudoCity;
	}

	public boolean sessionClose() {
		if (StringUtils.isBlank(binaryToken)) {
			return false;
		}
		boolean isSuccess = false;
		SessionCloseRQServiceStub serviceStub = bindingService.getSessionClose();
		SessionCloseRQ sessionCloseRQ = new SessionCloseRQ();
		try {
			listener.setType(AirUtils.getLogType("SessionClose", configuration));
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			MessageHeader messageHeaderHolder = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.SESSION_CLOSE, SabreConstants.CONVERSATION_ID);
			POS_type0 pos = new POS_type0();
			org.opentravel.www.ota._2002._11.Source_type0 source = new org.opentravel.www.ota._2002._11.Source_type0();
			source.setPseudoCityCode(configuration.getSupplierCredential().getPcc());
			pos.setSource(source);
			sessionCloseRQ.setPOS(pos);
			SessionCloseRS sessionCloseRS =
					serviceStub.sessionCloseRQ(sessionCloseRQ, messageHeaderHolder, getSecurity());
			String status = sessionCloseRS.getStatus();
			if (status.equals("Complete")) {
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}


	public boolean designatePrinter() {
		boolean isSuccess = false;
		DesignatePrinterRQ designatePrinterRQ = new DesignatePrinterRQ();
		DesignatePrinterServiceStub serviceStub = bindingService.getDesignateService();
		listener.setType(AirUtils.getLogType("DesignatePrinter", configuration));
		try {
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.DESIGNATE_PRINTER, SabreConstants.CONVERSATION_ID);
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			designatePrinterRQ.setReturnHostCommand(true);
			designatePrinterRQ.setVersion(SabreConstants.DESIGNATE_PRINTER_VERSION);
			Profile_type0 profile = new Profile_type0();
			profile.setNumber(BigInteger.valueOf(1));
			designatePrinterRQ.setProfile(profile);

			DesignatePrinterRS designatePrinterRS =
					serviceStub.designatePrinterRQ(designatePrinterRQ, messageHeader, getSecurity());
			isAnyWarning(designatePrinterRS.getApplicationResults());
			isAnyCritical(designatePrinterRS.getApplicationResults());
			CompletionCodes apiResponseStatus = designatePrinterRS.getApplicationResults().getStatus();
			if (apiResponseStatus == CompletionCodes.Complete) {
				isSuccess = true;
			}
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	public double getAmountPaidToAirline(String blockedPnr) {
		TravelItineraryReadRS travelItineraryRS = this.travelItineraryReadLLS(blockedPnr);
		Double amountToBePaidToAirline = 0.0;
		if (travelItineraryRS != null) {
			if (travelItineraryRS.getTravelItinerary().getItineraryInfo() != null
					&& travelItineraryRS.getTravelItinerary().getItineraryInfo().getItineraryPricing() != null) {
				List<PriceQuote_type0> priceQuotes = getActivePriceQuote(travelItineraryRS.getTravelItinerary()
						.getItineraryInfo().getItineraryPricing().getPriceQuote());
				if (CollectionUtils.isNotEmpty(priceQuotes)) {
					for (PriceQuote_type0 priceQuote : priceQuotes) {
						if (ArrayUtils.isNotEmpty(priceQuote.getPricedItinerary())) {
							for (PricedItinerary_type0 pricedItin : priceQuote.getPricedItinerary()) {
								if (pricedItin.getAirItineraryPricingInfo() != null && ArrayUtils
										.isNotEmpty(pricedItin.getAirItineraryPricingInfo().getItinTotalFare())) {
									for (ItinTotalFare_type0 itinTotalFare : pricedItin.getAirItineraryPricingInfo()
											.getItinTotalFare()) {
										if (itinTotalFare.getTotalFare() != null
												&& StringUtils.isNotEmpty(itinTotalFare.getTotalFare().getAmount())
												&& StringUtils
														.isNotEmpty(itinTotalFare.getTotalFare().getCurrencyCode())) {
											int paxCount = 1;
											if (ArrayUtils.isNotEmpty(
													pricedItin.getAirItineraryPricingInfo().getPassengerTypeQuantity())
													&& StringUtils.isNotEmpty(pricedItin.getAirItineraryPricingInfo()
															.getPassengerTypeQuantity()[0].getQuantity())) {
												paxCount = Integer.valueOf(pricedItin.getAirItineraryPricingInfo()
														.getPassengerTypeQuantity()[0].getQuantity());
											}
											amountToBePaidToAirline += getAmountBasedOnCurrency(
													BigDecimal.valueOf(
															Double.valueOf(itinTotalFare.getTotalFare().getAmount())),
													itinTotalFare.getTotalFare().getCurrencyCode()) * paxCount;
										}
									}
								}
							}
						}
					}
				}
			}
		}
		return amountToBePaidToAirline;
	}

	protected boolean isTravelItinerarySuccess(String blockedPnr) {
		boolean isSuccess = false;
		if (this.travelItineraryReadLLS(blockedPnr).getApplicationResults().getStatus() == CompletionCodes.Complete) {
			isSuccess = true;
		}
		return isSuccess;
	}

	public void isAnyWarning(ApplicationResults applicationResults) {
		StringJoiner warningMessage = new StringJoiner("");
		if (applicationResults != null && ArrayUtils.isNotEmpty(applicationResults.getWarning())) {
			for (ProblemInformation warning : applicationResults.getWarning()) {
				for (SystemSpecificResults result : warning.getSystemSpecificResults()) {
					for (MessageCondition messageCondition : result.getMessage()) {
						warningMessage.add(
								StringUtils.join(messageCondition.getCode(), "  ", messageCondition.getTextLong()));
					}
				}
				log.info("Warning occured on booking {} type {} - warning message {}", bookingId, warning.getType(),
						warningMessage.toString());
			}
		}
		if (!warningMessage.toString().toUpperCase().contains("TTY REQ PEND")) {
			logCriticalMessage(warningMessage.toString());
		}
	}


	public void isAnyCritical(ApplicationResults applicationResults) {
		StringJoiner message = new StringJoiner("");
		if (applicationResults != null && ArrayUtils.isNotEmpty(applicationResults.getError())) {
			for (ProblemInformation error : applicationResults.getError()) {
				for (SystemSpecificResults result : error.getSystemSpecificResults()) {
					for (MessageCondition messageCondition : result.getMessage()) {
						message.add(StringUtils.join(messageCondition.getCode(), "  ", messageCondition.getTextLong()));
					}
				}
				log.info("Error occured on booking {} type {} - error message {}", bookingId, error.getType(),
						message.toString());
			}
		}
		logCriticalMessage(message.toString());
	}

	public void isAnyWarning(com.sabre.services.stl.v01.ApplicationResults applicationResults) {
		StringJoiner warningMessage = new StringJoiner("");
		if (applicationResults != null && ArrayUtils.isNotEmpty(applicationResults.getWarning())) {
			for (com.sabre.services.stl.v01.ProblemInformation warning : applicationResults.getWarning()) {
				for (com.sabre.services.stl.v01.SystemSpecificResults result : warning.getSystemSpecificResults()) {
					for (com.sabre.services.stl_header.v120.MessageCondition messageCondition : result.getMessage()) {
						warningMessage.add(
								StringUtils.join(messageCondition.getCode(), "  ", messageCondition.getTextLong()));
					}
				}
				log.info("Warning occured on booking {} type {} - warning message {}", bookingId, warning.getType(),
						warningMessage.toString());
			}
		}
		logCriticalMessage(warningMessage.toString());
	}

	public String isAnyCritical(com.sabre.services.stl.v01.ApplicationResults applicationResults) {
		StringJoiner errorMessage = new StringJoiner("");
		if (applicationResults != null && ArrayUtils.isNotEmpty(applicationResults.getError())) {
			for (com.sabre.services.stl.v01.ProblemInformation error : applicationResults.getError()) {
				for (com.sabre.services.stl.v01.SystemSpecificResults result : error.getSystemSpecificResults()) {
					for (com.sabre.services.stl_header.v120.MessageCondition messageCondition : result.getMessage()) {
						errorMessage.add(
								StringUtils.join(messageCondition.getCode(), "  ", messageCondition.getTextLong()));
					}
				}
				log.info("Error occured on booking {} type {} - error message {}", bookingId, error.getType(),
						errorMessage.toString());
			}
		}
		logCriticalMessage(errorMessage.toString());
		return errorMessage.toString();
	}


	public String getPaxTypeCode(String code, AtomicInteger priceIndex) {
		if (searchQuery != null) {
			if (priceIndex.get() == 1) {
				if (searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0) > 0) {
					return PaxType.CHILD.getType();
				} else if (searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0) > 0) {
					return PaxType.INFANT.getType();
				}
			}
			if (priceIndex.get() == 2 && searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0) > 0) {
				return PaxType.INFANT.getType();
			}
		}
		return code;
	}

	protected void logCriticalMessage(String message) {
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			criticalMessageLogger.add(message);
		}
	}

	public boolean isFreeMeal(String mealCodes) {
		boolean isFreeMeal = false;
		for (char code : mealCodes.toCharArray()) {
			isFreeMeal = isFreeMeal || !(PAID_MEAL_CODES.contains(String.valueOf(code)));
		}
		return isFreeMeal;
	}

	// logic to retry sabreCommand with Ignore command.
	public boolean sabreCommandWithRetryLogic(String command) {
		boolean isSuccess = sabreCommand(command);
		if (!isSuccess) {
			sabreCommand(SabreConstants.IGNORE_COMMAND);
			isSuccess = sabreCommand(command);
		}
		return isSuccess;
	}

	public String getBookingClass(TripInfo tripInfo) {
		String bookingClass = null;
		SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
		PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
		bookingClass = priceInfo.getBookingClass(PaxType.ADULT);
		return bookingClass;
	}

	protected boolean isValidTripBooking(TravelItineraryReadRS travelItineraryResponse) {
		if (travelItineraryResponse != null && travelItineraryResponse.getTravelItinerary() != null
				&& travelItineraryResponse.getTravelItinerary().getItineraryInfo() != null
				&& travelItineraryResponse.getTravelItinerary().getItineraryInfo().getReservationItems() != null
				&& ArrayUtils.isNotEmpty(
						travelItineraryResponse.getTravelItinerary().getItineraryInfo().getReservationItems().getItem())
				&& travelItineraryResponse.getTravelItinerary().getItineraryInfo().getItineraryPricing() != null
				&& ArrayUtils.isNotEmpty(travelItineraryResponse.getTravelItinerary().getItineraryInfo()
						.getItineraryPricing().getPriceQuote())) {
			return true;
		}
		return false;
	}

	// If arrival airport and next departure airport are not in same city then ARUNK should be called.
	// Note : Not Same airport , its should be city
	public boolean hasArrivalUnknownSegments() {
		boolean hasUnknownSegment = false;
		for (int segmentIndex = 0; segmentIndex < segmentInfos.size() - 1; segmentIndex++) {
			AirportInfo arrivalAirport =
					AirportHelper.getAirport(segmentInfos.get(segmentIndex).getArrivalAirportCode());
			AirportInfo departureAirport =
					AirportHelper.getAirport(segmentInfos.get(segmentIndex + 1).getDepartureAirportCode());
			if (!arrivalAirport.getCityCode().equals(departureAirport.getCityCode())) {
				hasUnknownSegment = true;
			}
		}
		return hasUnknownSegment;
	}

	protected boolean isValidReservationItem(Item_type0 item) {
		return ArrayUtils.isNotEmpty(item.getFlightSegment()) && hasSupplierRefInItem(item);
	}

	private boolean hasSupplierRefInItem(Item_type0 item) {
		if (ArrayUtils.isNotEmpty(item.getFlightSegment()) && item.getFlightSegment()[0] != null
				&& item.getFlightSegment()[0].getSupplierRef() != null
				&& StringUtils.isNotBlank(item.getFlightSegment()[0].getSupplierRef().getID())) {
			return true;
		}
		return false;
	}

	protected LocalDateTime getHoldTimeLimit(TravelItinerary_type0 travelItinerary) {
		LocalDateTime timeLimit = null;
		try {
			SpecialServiceInfo_type0[] specialServiceInfos = travelItinerary.getSpecialServiceInfo();
			String timeLimitText = "";
			if (ArrayUtils.isNotEmpty(specialServiceInfos)) {
				for (SpecialServiceInfo_type0 serviceInfo : specialServiceInfos) {
					if (StringUtils.isNotEmpty(serviceInfo.getType())
							&& (serviceInfo.getType().equalsIgnoreCase("GFX")
									|| serviceInfo.getType().equalsIgnoreCase("AFX"))
							&& serviceInfo.getService() != null) {
						Service_type0 service = serviceInfo.getService();
						if (StringUtils.isNotEmpty(service.getSSR_Type())
								&& (service.getSSR_Type().equalsIgnoreCase("ADTK")
										|| service.getSSR_Type().equalsIgnoreCase("OTHS"))) {
							timeLimitText += service.getText();
						}
					}
				}
				timeLimit = parseHoldLimit(timeLimitText);
			}
		} catch (Exception e) {
			log.error("Exception occurred while parsing hold time limit for booking id {} excep {}", bookingId, e);
		}
		return timeLimit;
	}

	private LocalDateTime parseHoldLimit(String timeLimitText) throws ParseException {
		PriceInfo priceInfo = segmentInfos.get(0).getPriceInfo(0);
		AirlineTimeLimitData airlineTimeLimitData = AirUtils.getAirlineTimeLimitData(
				segmentInfos.get(0).getPlatingCarrier(priceInfo), sourceConfiguration, bookingUser);
		if (StringUtils.isNotEmpty(timeLimitText) && airlineTimeLimitData != null) {
			String rejexPattern = airlineTimeLimitData.getRegexPattern();
			String timeFormat = airlineTimeLimitData.getTimeFormat();
			List<String> matchedStrings = TgsStringUtils.parseStringByRejex(timeLimitText, rejexPattern);
			if (CollectionUtils.isNotEmpty(matchedStrings)) {
				// Selecting first matched pattern of hold time limit.
				String timeLimitData = matchedStrings.get(0);
				LocalDateTime ticketingTimeLimit = TgsDateUtils.convertStringToDate(timeLimitData, timeFormat);
				if (ticketingTimeLimit != null) {
					LocalDateTime now = LocalDateTime.now();
					// To handle without year case. TimeLimit without year will set the minimum yearof LocalDateTime.
					if (ticketingTimeLimit.isBefore(now)) {
						ticketingTimeLimit = ticketingTimeLimit.withYear(now.getYear());
						// Booking at the last day of the year & getting time limit of next year.
						if (ticketingTimeLimit.isBefore(now)) {
							ticketingTimeLimit = ticketingTimeLimit.plusYears(1);
						}
					}
					log.info("Ticketing time limit {} for bookingId {} ", bookingId, ticketingTimeLimit);
					return ticketingTimeLimit;
				}
			}
		}
		return null;
	}

	public OTA_AirPriceRQ_type0[] getOTAAirPriceReq(TripInfo tripInfo, AirSearchQuery searchQuery,
			SupplierConfiguration configuration) {
		OTA_AirPriceRQ_type0[] airPriceRQArray = new OTA_AirPriceRQ_type0[1];
		OTA_AirPriceRQ_type0 airPriceRQ = new OTA_AirPriceRQ_type0();
		PriceRequestInformation_type0 priceReqInfo = new PriceRequestInformation_type0();
		// priceReqInfo.setRetain(true); // before enabling discuss with flight team
		OptionalQualifiers_type0 optionalQualifiers = new OptionalQualifiers_type0();
		PricingQualifiers_type0 pricingQualifiers = new PricingQualifiers_type0();
		ItineraryOptions_type0 itineraryOptions = new ItineraryOptions_type0();
		SegmentSelect_type0[] segmentSelectArray = new SegmentSelect_type0[tripInfo.getSegmentInfos().size()];
		int index = 0;
		for (int i = 1; i <= tripInfo.getSegmentInfos().size(); i++) {
			SegmentSelect_type0 segmentSelect = new SegmentSelect_type0();
			segmentSelect.setNumber(String.valueOf(i));
			segmentSelectArray[index] = segmentSelect;
			index++;

		}
		itineraryOptions.setSegmentSelect(segmentSelectArray);
		pricingQualifiers.setItineraryOptions(itineraryOptions);
		List<PassengerType_type1> passengerTypeList = new ArrayList<PassengerType_type1>();
		for (PaxType pax : PaxType.values()) {
			int paxQuantity = AirUtils.getParticularPaxCount(searchQuery, pax);
			if (paxQuantity > 0) {
				PassengerType_type1 passengerType = new PassengerType_type1();
				passengerType.setCode(pax.getType());
				passengerType.setQuantity(Integer.toString(paxQuantity));
				passengerTypeList.add(passengerType);
			}
		}
		pricingQualifiers.setPassengerType(passengerTypeList.toArray(new PassengerType_type1[0]));
		pricingQualifiers.setCurrencyCode(getCurrencyCode());
		pricingQualifiers.setAccount(
				SabreUtils.getAccountCodes(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo()));
		optionalQualifiers.setPricingQualifiers(pricingQualifiers);
		priceReqInfo.setOptionalQualifiers(optionalQualifiers);
		airPriceRQ.setPriceRequestInformation(priceReqInfo);
		airPriceRQArray[0] = airPriceRQ;
		return airPriceRQArray;
	}


	protected List<PriceQuote_type0> getActivePriceQuote(PriceQuote_type0[] priceQuotes) {
		List<PriceQuote_type0> activePriceQuotes = new ArrayList<>();
		if (ArrayUtils.isNotEmpty(priceQuotes)) {
			for (PriceQuote_type0 priceQuote : priceQuotes) {
				if (priceQuote.getMiscInformation() != null
						&& ArrayUtils.isNotEmpty(priceQuote.getMiscInformation().getSignatureLine()) && "ACTIVE"
								.equalsIgnoreCase(priceQuote.getMiscInformation().getSignatureLine()[0].getStatus())) {
					activePriceQuotes.add(priceQuote);
				}
			}
		}
		return activePriceQuotes;
	}

	public StringLength1To16 getStringLength1To16(String value) {
		StringLength1To16 object = new StringLength1To16();
		object.setStringLength1To16(value);
		return object;
	}

	public StringLength1To32 getStringLenth1To31(String value) {
		StringLength1To32 object = new StringLength1To32();
		object.setStringLength1To32(value);
		return object;
	}

	public StringLength1To20 getStringLenth1To20(String value) {
		StringLength1To20 object = new StringLength1To20();
		object.setStringLength1To20(value);
		return object;
	}

	public OTA_CodeType getOTACodeType(String value) {
		OTA_CodeType codeType = new OTA_CodeType();
		codeType.setOTA_CodeType(value);
		return codeType;
	}

	public PassengerCodeType getPaxTypeCode(String code) {
		PassengerCodeType paxType = new PassengerCodeType();
		paxType.setPassengerCodeType(code);
		return paxType;
	}

	public Version_type0 getVesrionType0(String value) {
		Version_type0 version = new Version_type0();
		version.setVersion_type0(value);
		return version;
	}

	public AlphaLength3 getStringLen3(String code) {
		AlphaLength3 length3 = new AlphaLength3();
		length3.setAlphaLength3(code);
		return length3;
	}


}
