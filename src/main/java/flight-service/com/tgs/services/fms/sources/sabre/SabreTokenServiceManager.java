package com.tgs.services.fms.sources.sabre;

import java.rmi.RemoteException;
import javax.xml.namespace.QName;

import com.tgs.services.fms.utils.AirUtils;
import org.apache.axiom.om.OMElement;
import org.apache.axis2.context.MessageContext;
import org.apache.axis2.wsdl.WSDLConstants;
import org.ebxml.www.namespaces.messageheader.MessageHeader;
import org.opentravel.www.ota._2002._11.POS_type0;
import org.opentravel.www.ota._2002._11.SessionCreateRQ;
import org.opentravel.www.ota._2002._11.SessionCreateRS;
import org.opentravel.www.ota._2002._11.Source_type0;
import org.xmlsoap.schemas.ws._2002._12.secext.Security;
import com.sabre.webservices.TokenCreateRQ;
import com.sabre.webservices.TokenCreateRS;
import com.sabre.webservices.websvc.SessionCreateRQServiceStub;
import com.sabre.webservices.websvc.TokenCreateRQServiceStub;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class SabreTokenServiceManager extends SabreServiceManager {

	private static final String HEADER_NAMESPACE = "http://www.ebxml.org/namespaces/messageHeader";

	private static final String SECURITY_NAMESPACE = "http://schemas.xmlsoap.org/ws/2002/12/secext";

	private static final String VERSION = "1.0.0";

	public String generateBinaryToken() throws SupplierSessionException {
		TokenCreateRQServiceStub tokenStub = null;
		try {
			listener.setType(AirUtils.getLogType("1_TokenCreate", configuration));
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.SESSIONLESS_SERVICE, SabreConstants.CONVERSATION_ID);
			Security security = new Security();
			security.setUsernameToken(getUserNameToken(configuration.getSupplierCredential().getPcc(),
					configuration.getSupplierCredential().getUserName(),
					configuration.getSupplierCredential().getPassword()));
			tokenStub = bindingService.getTokenService();
			TokenCreateRQ tokenCreateRQ = new TokenCreateRQ();
			tokenCreateRQ.setVersion(VERSION);
			tokenStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			TokenCreateRS tokenRs = tokenStub.tokenCreateRQ(tokenCreateRQ, messageHeader, security);

			if (tokenRs.getSuccess() != null) {
				MessageContext messageContext = tokenStub._getServiceClient().getLastOperationContext()
						.getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE);
				if (messageContext != null && messageContext.getEnvelope() != null
						&& messageContext.getEnvelope().getHeader() != null) {
					OMElement securityElement = messageContext.getEnvelope().getHeader()
							.getFirstChildWithName(new QName(SECURITY_NAMESPACE, "Security", "wsse"));
					try {
						Security security1 = Security.Factory.parse(securityElement.getXMLStreamReaderWithoutCaching());
						return security1.getBinarySecurityToken();
					} catch (Exception e) {
						throw new SupplierSessionException(e.getMessage());
					}
				} else {
					throw new SupplierSessionException("Authentication Failed");
				}
			}

		} catch (RemoteException e) {
			throw new SupplierSessionException(e.getMessage());
		} finally {
			tokenStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;

	}

	public String generateSessionToken() {
		SessionCreateRQServiceStub serviceStub = null;
		SessionCreateRQ sessionCreateRQ = null;
		try {
			listener.setType(AirUtils.getLogType("2_SessionCreate",configuration));
			MessageHeader messageHeader = getMessageHeader(configuration.getSupplierCredential().getPcc(),
					SabreConstants.SESSION_SERVICE, SabreConstants.CONVERSATION_ID);
			Security security = new Security();
			security.setUsernameToken(getUserNameToken(configuration.getSupplierCredential().getPcc(),
					configuration.getSupplierCredential().getUserName(),
					configuration.getSupplierCredential().getPassword()));
			serviceStub = bindingService.getSessionService();
			sessionCreateRQ = new SessionCreateRQ();
			POS_type0 pos = new POS_type0();
			Source_type0 source = new Source_type0();
			source.setPseudoCityCode(configuration.getSupplierCredential().getPcc());
			pos.setSource(source);
			sessionCreateRQ.setPOS(pos);
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			SessionCreateRS sessionRS = serviceStub.sessionCreateRQ(sessionCreateRQ, messageHeader, security);
			if (sessionRS.getStatus().equals("Approved")) {
				MessageContext messageContext = serviceStub._getServiceClient().getLastOperationContext()
						.getMessageContext(WSDLConstants.MESSAGE_LABEL_IN_VALUE);
				if (messageContext != null && messageContext.getEnvelope() != null
						&& messageContext.getEnvelope().getHeader() != null) {
					OMElement securityElement = messageContext.getEnvelope().getHeader()
							.getFirstChildWithName(new QName(SECURITY_NAMESPACE, "Security", "wsse"));
					try {
						Security security1 = Security.Factory.parse(securityElement.getXMLStreamReaderWithoutCaching());
						binaryToken = security1.getBinarySecurityToken();
						return binaryToken;
					} catch (Exception e) {
						throw new SupplierSessionException(e.getMessage());
					}
				} else {
					throw new SupplierSessionException("Authentication Failed");
				}
			}
		} catch (Exception e) {
			throw new SupplierSessionException(e.getMessage());
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

}
