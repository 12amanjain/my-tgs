package com.tgs.services.fms.sources.sabre;

import java.math.BigInteger;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import com.sabre.services.res.tir.v3_9.SupplierRef_type0;
import com.sabre.services.res.tir.v3_9.Ticketing_type1;
import com.sabre.services.res.tir.v3_9.TravelItineraryReadRS;
import com.tgs.services.base.enums.*;
import com.tgs.services.fms.manager.TripPriceEngine;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.sabre.services.sp.common.simple.v3.DateTime;
import com.sabre.services.sp.eab.v3_10.Account_type0;
import com.sabre.services.sp.eab.v3_10.Code_type2;
import com.sabre.services.sp.eab.v3_10.DestinationLocation_type0;
import com.sabre.services.sp.eab.v3_10.FlightSegment_type1;
import com.sabre.services.sp.eab.v3_10.HaltOnStatus_type0;
import com.sabre.services.sp.eab.v3_10.MarketingAirline_type1;
import com.sabre.services.sp.eab.v3_10.MarriageGrp_type1;
import com.sabre.services.sp.eab.v3_10.NumAttempts_type0;
import com.sabre.services.sp.eab.v3_10.OTA_AirBookRQ_type0;
import com.sabre.services.sp.eab.v3_10.OriginDestinationInformation_type0;
import com.sabre.services.sp.eab.v3_10.OriginLocation_type0;
import com.sabre.services.sp.eab.v3_10.PostProcessing_type0;
import com.sabre.services.sp.eab.v3_10.RedisplayReservation_type0;
import com.sabre.services.sp.eab.v3_10.RedisplayReservation_type1;
import com.sabre.services.sp.eab.v3_10.RetryRebook_type0;
import com.sabre.services.sp.eab.v3_10.WaitInterval_type0;
import com.sabre.services.sp.pd.v3_4.Address_type0;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import org.opentravel.www.ota._2003._05.CabinType;
import org.opentravel.www.ota._2003._05.PTCFareBreakdownType;


@Slf4j
class SabreUtils {

	public static CabinType getCabinType(AirSearchQuery searchQuery) {
		CabinType cabinType = null;
		CabinClass cabinClass = searchQuery.getCabinClass();
		if (cabinClass != null) {
			if (CabinClass.BUSINESS.equals(cabinClass)) {
				cabinType = CabinType.Business;
			} else if (CabinClass.PREMIUM_BUSINESS.equals(cabinClass)) {
				cabinType = CabinType.PremiumBusiness;
			} else if (CabinClass.PREMIUMFIRST.equals(cabinClass)) {
				cabinType = CabinType.PremiumFirst;
			} else if (CabinClass.PREMIUM_ECONOMY.equals(cabinClass)) {
				cabinType = CabinType.PremiumEconomy;
			} else if (CabinClass.ECONOMY.equals(cabinClass)) {
				cabinType = CabinType.Economy;
			} else if (CabinClass.FIRST.equals(cabinClass)) {
				cabinType = CabinType.First;
			}
		}
		return cabinType;
	}

	public static void splitTripAndFare(TripInfo tripInfo, AirSearchResult airSearchResult,
			Map<String, TripInfo> onwardFlightMap, Map<String, TripInfo> returnFlightMap, double itineraryFare) {
		TripInfo onwardTrip = new TripInfo();
		TripInfo returnTrip = new TripInfo();
		PriceInfo priceInfo = null;
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			if (segmentInfo.getIsReturnSegment()) {
				returnTrip.getSegmentInfos().add(segmentInfo);
				if (segmentInfo.getSegmentNum() == 0) {
					returnTrip.getSegmentInfos().get(0).setPriceInfoList(new ArrayList<>());
					returnTrip.getSegmentInfos().get(0).getPriceInfoList()
							.add(new GsonMapper<>(priceInfo, PriceInfo.class).convert());
				}
			} else {
				if (segmentInfo.getSegmentNum() == 0) {
					priceInfo = adjustFareToHalf(segmentInfo);
				}
				onwardTrip.getSegmentInfos().add(segmentInfo);
			}
		}
		String onwardFlightNumberKey = getKey(onwardTrip);
		String returnFlightumberKey = getKey(returnTrip);
		if (onwardFlightMap.get(onwardFlightNumberKey) == null || returnFlightMap.get(returnFlightumberKey) == null
				|| itineraryFare < (onwardFlightMap.get(onwardFlightNumberKey).getSegmentInfos().get(0).getPriceInfo(0)
						.getFareDetail(PaxType.ADULT).getFareComponents().get(FareComponent.TF)
						+ returnFlightMap.get(returnFlightumberKey).getSegmentInfos().get(0).getPriceInfo(0)
								.getFareDetail(PaxType.ADULT).getFareComponents().get(FareComponent.TF))) {
			String specialOnwardFlightNumberKey = getSpeciaReturnKey(onwardTrip);
			String specialReturnFlightumberKey = getSpeciaReturnKey(returnTrip);
			TripInfo tempOnwardTrip =
					ObjectUtils.firstNonNull(onwardFlightMap.get(specialOnwardFlightNumberKey), onwardTrip);
			TripInfo tempReturnTrip =
					ObjectUtils.firstNonNull(returnFlightMap.get(specialReturnFlightumberKey), returnTrip);
			TripPriceEngine.setSpecialReturnIdentifier(tempOnwardTrip.getSegmentInfos().get(0),
					tempOnwardTrip.getSegmentInfos().get(0).getPriceInfo(0), tempReturnTrip.getSegmentInfos().get(0),
					tempReturnTrip.getSegmentInfos().get(0).getPriceInfo(0));
			if (onwardFlightMap.get(specialOnwardFlightNumberKey) == null) {
				onwardFlightMap.put(specialOnwardFlightNumberKey, tempOnwardTrip);
				airSearchResult.addTripInfo(TripInfoType.ONWARD.getName(), onwardTrip);
			}
			if (returnFlightMap.get(specialReturnFlightumberKey) == null) {
				returnFlightMap.put(specialReturnFlightumberKey, tempReturnTrip);
				airSearchResult.addTripInfo(TripInfoType.RETURN.getName(), returnTrip);
			}
		}
	}

	private static String getSpeciaReturnKey(TripInfo tripInfo) {
		String key = StringUtils.join(getKey(tripInfo), tripInfo.getSegmentInfos().get(0).getPriceInfo(0)
				.getFareDetail(PaxType.ADULT).getFareComponents().get(FareComponent.TF));
		return key;
	}

	public static PriceInfo adjustFareToHalf(SegmentInfo segmentInfo) {
		PriceInfo priceInfo = null;
		if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
			priceInfo = segmentInfo.getPriceInfo(0);
			priceInfo.setFareIdentifier(FareType.SPECIAL_RETURN);
			for (PaxType paxType : priceInfo.getFareDetails().keySet()) {
				FareDetail fareDetail = segmentInfo.getPriceInfo(0).getFareDetails().get(paxType);
				for (FareComponent fc : fareDetail.getFareComponents().keySet()) {
					fareDetail.getFareComponents().put(fc, fareDetail.getFareComponents().getOrDefault(fc, 0.0) / 2);
				}
			}
		}
		return priceInfo;
	}

	public static String getGenderInfo(String title) {
		String gender = "";
		if (title.compareToIgnoreCase("Mr") == 0 || title.compareToIgnoreCase("Mstr") == 0
				|| title.compareToIgnoreCase("Master") == 0) {
			gender = "M";
		} else if (title.compareToIgnoreCase("Ms") == 0 || title.compareToIgnoreCase("Mrs") == 0
				|| title.compareToIgnoreCase("Miss") == 0) {
			gender = "F";
		}
		return gender;
	}

	protected static OTA_AirBookRQ_type0 getOTAAirBookRQ(TripInfo tripInfo, AirSearchQuery searchQuery) {
		OTA_AirBookRQ_type0 airbookRq = new OTA_AirBookRQ_type0();
		RetryRebook_type0 retryRebook = new RetryRebook_type0();
		retryRebook.setOption(getRetryOption(searchQuery));
		airbookRq.setRetryRebook(retryRebook);
		airbookRq.setHaltOnStatus(getHaltOnStatusList());
		OriginDestinationInformation_type0 originDestinationInfo = new OriginDestinationInformation_type0();
		int totalPaxCount = AirUtils.getPaxCount(searchQuery, false);
		FlightSegment_type1[] flightSegmentArray = new FlightSegment_type1[tripInfo.getSegmentInfos().size()];
		AtomicInteger index = new AtomicInteger();
		tripInfo.getSegmentInfos().forEach(segment -> {
			FlightSegment_type1 flightSegment = new FlightSegment_type1();
			flightSegment.setDepartureDateTime(getDateTime(segment.getDepartTime()));
			flightSegment.setArrivalDateTime(getDateTime(segment.getArrivalTime()));
			flightSegment.setFlightNumber(segment.getFlightDesignator().getFlightNumber());
			flightSegment.setNumberInParty(String.valueOf(totalPaxCount));
			flightSegment.setResBookDesigCode(segment.getPriceInfo(0).getFareDetail(PaxType.ADULT).getClassOfBooking());
			flightSegment.setStatus("NN");
			DestinationLocation_type0 destinationLocation = new DestinationLocation_type0();
			destinationLocation.setLocationCode(segment.getArrivalAirportInfo().getCode());
			flightSegment.setDestinationLocation(destinationLocation);
			MarketingAirline_type1 marketingAirline = new MarketingAirline_type1();
			marketingAirline.setCode(segment.getFlightDesignator().getAirlineInfo().getCode());
			marketingAirline.setFlightNumber(segment.getFlightDesignator().getFlightNumber());
			flightSegment.setMarketingAirline(marketingAirline);
			OriginLocation_type0 originLocation = new OriginLocation_type0();
			originLocation.setLocationCode(segment.getDepartAirportInfo().getCode());
			flightSegment.setOriginLocation(originLocation);
			setMarriageGroup(flightSegment, segment);
			flightSegmentArray[index.get()] = flightSegment;
			index.getAndIncrement();
		});
		originDestinationInfo.setFlightSegment(flightSegmentArray);
		airbookRq.setOriginDestinationInformation(originDestinationInfo);
		RedisplayReservation_type0 redeiplayReservation = new RedisplayReservation_type0();
		redeiplayReservation.setNumAttempts(getDisplayNumAttaempts(SabreConstants.MAX_NUM_ATTEMPTS));
		redeiplayReservation.setWaitInterval(getDisplayWaitInterval(SabreConstants.WAIT_INTERVAL));
		airbookRq.setRedisplayReservation(redeiplayReservation);
		return airbookRq;
	}

	private static DateTime getDateTime(LocalDateTime localDateTime) {
		DateTime dateTime = new DateTime();
		dateTime.setDateTime(localDateTime.toString());
		return dateTime;
	}

	private static WaitInterval_type0 getDisplayWaitInterval(int waitInterval) {
		WaitInterval_type0 waitIntervalType = new WaitInterval_type0();
		waitIntervalType.setWaitInterval_type0(BigInteger.valueOf(waitInterval));
		return waitIntervalType;
	}

	private static NumAttempts_type0 getDisplayNumAttaempts(int maxNumAttempts) {
		NumAttempts_type0 numAttempts = new NumAttempts_type0();
		numAttempts.setNumAttempts_type0(BigInteger.valueOf(maxNumAttempts));
		return numAttempts;
	}

	private static Boolean getRetryOption(AirSearchQuery searchQuery) {
		if (searchQuery.getFlowType() != null && searchQuery.getFlowType().equals(AirFlowType.ALTERNATE_CLASS)) {
			return false;
		}
		return true;
	}


	private static void setMarriageGroup(FlightSegment_type1 flightSegment, SegmentInfo segment) {
		String marriageGrp = segment.getPriceInfo(0).getMiscInfo().getMarriageGrp();
		if (StringUtils.isNotBlank(marriageGrp) && !"O".equals(marriageGrp)) {
			flightSegment.setMarriageGrp(getMarriageType(marriageGrp));
		}
	}

	/**
	 * marriageGrpType is Enum Here
	 * 
	 * @param marriageGrp
	 * @return
	 */
	private static MarriageGrp_type1 getMarriageType(String marriageGrp) {
		MarriageGrp_type1 marriageType = null;
		if ("I".equals(marriageGrp)) {
			marriageType = MarriageGrp_type1.I;
		}
		return marriageType;
	}

	public static HaltOnStatus_type0[] getHaltOnStatusList() {
		List<String> statusList = Arrays.asList("UC", "NN", "LL", "PN", "NO", "US", "UU");
		HaltOnStatus_type0[] haltOnStatusList = new HaltOnStatus_type0[statusList.size()];
		int index = 0;
		for (String status : statusList) {
			haltOnStatusList[index] = getHaltOnStatus(status);
			index++;
		}
		return haltOnStatusList;
	}

	public static HaltOnStatus_type0 getHaltOnStatus(String code) {
		HaltOnStatus_type0 haltOnStatus = new HaltOnStatus_type0();
		haltOnStatus.setCode(getHaltOnStatusCodeType(code));
		return haltOnStatus;
	}

	private static Code_type2 getHaltOnStatusCodeType(String code) {
		Code_type2 codeType = new Code_type2();
		codeType.setCode_type2(code);
		return codeType;
	}

	public static Account_type0 getAccountCodes(PriceMiscInfo miscInfo) {
		Account_type0 account = null;
		if (StringUtils.isNotEmpty(miscInfo.getAccountCode())) {
			account = new Account_type0();
			account.setForce("true");
			account.setCode(getAccountCodeStringArray(miscInfo.getAccountCode()));
		}
		return account;
	}

	private static String[] getAccountCodeStringArray(String accountCode) {
		String[] accountCodeArray = new String[1];
		accountCodeArray[0] = accountCode;
		return accountCodeArray;
	}

	public static List<String> fetchTicketNumber(Ticketing_type1[] ticketing) {
		List<String> ticketNumbers = new ArrayList<>();
		for (Ticketing_type1 ticket : ticketing) {
			if (Objects.nonNull(ticket.getETicketNumber())) {
				String ticketNumber = ticket.getETicketNumber();
				ticketNumber = ticketNumber.substring(0, ticketNumber.indexOf("-")).replaceAll("TE", "").trim();
				ticketNumbers.add(ticketNumber);
			}
		}
		return ticketNumbers;
	}

	public static String parsePnr(String crsPnr) {
		return crsPnr.substring(crsPnr.indexOf("*") + 1, crsPnr.length());
	}

	public static String getFormattedDob(LocalDate travellerDob) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMMyy");
		return travellerDob.format(formatter);
	}

	/*
	 * @param travellerInfo
	 *
	 * @return String
	 */
	public static String getNameRefrenece(PaxType paxType, LocalDate travellerDob) {
		return StringUtils.join(paxType.name().substring(0, 1),
				(TgsDateUtils.getAgeFromBirthDate(travellerDob) == 0) ? 1
						: TgsDateUtils.getAgeFromBirthDate(travellerDob));
	}

	public static Address_type0 getAddress(List<SegmentInfo> segmentInfos, String bookingUserId) {
		ClientGeneralInfo companyInfo = ServiceCommunicatorHelper.getClientInfo();
		AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(bookingUserId, companyInfo);
		Address_type0 address = new Address_type0();

		// Sabre doesn't support number as first character in AddressLine. So appending whitespace.
		address.setAddressLine(SabreUtils.applyLengthLimitation(
				StringUtils.join(" ", SabreUtils.replaceSpecialCharacter(contactAddress.getAddress())), 0, 50));

		address.setCityName(contactAddress.getCityInfo().getName());
		address.setCountryCode(companyInfo.getCountryCode());
		address.setPostalCode(contactAddress.getPincode());
		address.setStreetNmbr(SabreUtils
				.applyLengthLimitation(SabreUtils.replaceSpecialCharacter(contactAddress.getAddress()), 0, 50));
		return address;
	}

	public static PriceInfo getPriceInfo(TripInfo tripInfo) {
		return tripInfo.getSegmentInfos().get(0).getPriceInfo(0);
	}

	public static double getAmountPaidToAirline(TravelItineraryReadRS travelItineraryRS) {
		Double amountToBePaidToAirline = 0.0;
		if (travelItineraryRS != null) {
			amountToBePaidToAirline = Double.valueOf(travelItineraryRS.getTravelItinerary().getItineraryInfo()
					.getItineraryPricing().getPriceQuoteTotals().getTotalFare().getAmount());
		}
		return amountToBePaidToAirline;
	}

	public static List<String> getTicketNumbers(TravelItineraryReadRS travelItineraryRS) {
		Ticketing_type1[] ticketingList = travelItineraryRS.getTravelItinerary().getItineraryInfo().getTicketing();
		if (ArrayUtils.isNotEmpty(ticketingList)) {
			return SabreUtils.fetchTicketNumber(ticketingList);
		}
		return null;
	}

	public static String getSupplierBookingId(TravelItineraryReadRS travelItineraryRS) {
		SupplierRef_type0 supplierRef =
				travelItineraryRS.getTravelItinerary().getItineraryInfo().getReservationItems().getItem()[0]
						.getFlightSegment()[0].getSupplierRef();
		if (Objects.nonNull(supplierRef)) {
			return SabreUtils.parsePnr(supplierRef.getID());
		}
		return null;
	}

	public static boolean getIsReturn(List<TripInfo> tripInfos) {
		if (CollectionUtils.isNotEmpty(tripInfos)) {
			if (tripInfos.get(0).getSegmentInfos().get(0).getDepartAirportInfo().getCode()
					.equals(tripInfos.get(1).getSegmentInfos().get(tripInfos.get(1).getSegmentInfos().size() - 1)
							.getArrivalAirportInfo().getCode())) {
				return true;
			}
		}
		return false;
	}

	public static String getKey(TripInfo tripInfo) {
		String flightNumberAsKey = StringUtils.EMPTY;
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			flightNumberAsKey =
					StringUtils.join(flightNumberAsKey, segmentInfo.getFlightDesignator().getAirlineInfo().getCode(),
							segmentInfo.getFlightDesignator().getFlightNumber());
		}
		return flightNumberAsKey;
	}

	public static LocalDateTime getIsoDateTime(String dateTime) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		LocalDateTime formattedDateTime = LocalDateTime.parse(dateTime, formatter);
		return formattedDateTime;
	}

	public static LocalDateTime convertToLocalTime(LocalDateTime referenceTime, String arrivalData) {
		String month = arrivalData.split("T")[0].split("-")[0];
		String day = arrivalData.split("T")[0].split("-")[1];
		LocalDateTime arrivalTime = null;
		LocalTime localTime = LocalTime.parse(arrivalData.split("T")[1], DateTimeFormatter.ofPattern("HH:mm"));
		try {
			arrivalTime = LocalDateTime.of(referenceTime.getYear(), Integer.parseInt(month), Integer.parseInt(day),
					localTime.getHour(), localTime.getMinute());
		} catch (Exception e) {
			arrivalTime = LocalDateTime.of(referenceTime.getYear() + 1, Integer.parseInt(month), Integer.parseInt(day),
					localTime.getHour(), localTime.getMinute());
		}
		if (arrivalTime.compareTo(referenceTime) < 0) {
			arrivalTime = arrivalTime.plusYears(1);
		}
		return arrivalTime;
	}

	// As child dob is mandatory for sabre we are passing dummy dob with the age of
	// 6 yrs.
	// Considering age as 6 yrs from departure date
	public static LocalDate getDob(LocalDateTime departueDateTime) {
		LocalDate dummyDob = null;
		if (departueDateTime != null) {
			dummyDob = LocalDate.of(departueDateTime.getYear(), departueDateTime.getMonthValue(),
					departueDateTime.getDayOfMonth());
			dummyDob = dummyDob.minusYears(6);
		}
		return dummyDob;
	}

	public static boolean updateTicketNumbers(List<SegmentInfo> segmentInfos,
			TravelItineraryReadRS travelItineraryResponse) {
		boolean isSuccess = false;
		Ticketing_type1[] ticketingList =
				travelItineraryResponse.getTravelItinerary().getItineraryInfo().getTicketing();
		if (ArrayUtils.isNotEmpty(ticketingList)) {
			int travellerIndex = 0;
			List<FlightTravellerInfo> travellerInfoList =
					segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
			List<String> ticketNumbers = SabreUtils.fetchTicketNumber(ticketingList);
			// To handle the case of segment wise ticket number.
			if (isTicketNumberSegmentWise(ticketNumbers, segmentInfos)) {
				int ticketNumberIndex = 0;
				for (SegmentInfo segment : segmentInfos) {
					for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
						traveller.setTicketNumber(ticketNumbers.get(ticketNumberIndex));
						ticketNumberIndex++;
					}
				}
			} else {
				for (String ticketNumber : ticketNumbers) {
					BookingUtils.updateTicketNumber(segmentInfos, ticketNumber,
							travellerInfoList.get(travellerIndex++));
				}
			}
			isSuccess = true;
		}
		return isSuccess;
	}

	private static boolean isTicketNumberSegmentWise(List<String> ticketNumbers, List<SegmentInfo> segmentInfos) {
		if (ticketNumbers.size() == segmentInfos.size()
				* segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().size()) {
			return true;
		}
		return false;
	}

	public static String getAccountCodeFromTrip(List<SegmentInfo> segmentInfos) {
		String accountCode = null;
		if (CollectionUtils.isNotEmpty(segmentInfos)
				&& CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList())) {
			PriceInfo priceInfo = segmentInfos.get(0).getPriceInfo(0);
			accountCode = priceInfo.getAccountCode();
		}
		return accountCode;
	}

	public static String getAccountCodeFromSegment(SegmentInfo segment) {
		String accountCode = null;
		if (CollectionUtils.isNotEmpty(segment.getPriceInfoList())) {
			accountCode = segment.getPriceInfo(0).getAccountCode();
		}
		return accountCode;
	}

	public static String replaceSpecialCharacter(String input) {
		return input.replaceAll("[-.:\\\";@&()',/]", "");
	}

	public static boolean isHostRequired(List<SegmentInfo> segmentInfos) {
		boolean isAmericanAirline = false;
		List<String> hostRequiredAirline = Arrays.asList("AA");
		String platingCarrier = segmentInfos.get(0).getPlatingCarrier(null);
		if (StringUtils.isNotEmpty(platingCarrier) && hostRequiredAirline.contains(platingCarrier)) {
			isAmericanAirline = true;
		}
		return isAmericanAirline;
	}

	public static String commandCTCE(String email) {
		String[] emailString = email.split("@");
		String command = StringUtils.join("3CTCE/", emailString[0], "//", emailString[1], "-1.1");
		return command;
	}

	public static String commandCTCM(String mobile) {
		String command = StringUtils.join("3CTCM/", mobile, "-1.1");
		return command;
	}

	// As per the sabre current system PNR can be retained till 6 months from the
	// last segment flight time.
	public static String pnrRetention(int retainForDays, List<SegmentInfo> bookingSegmentInfos) {
		String carrierCode = bookingSegmentInfos.get(0).getPlatingCarrier(bookingSegmentInfos.get(0).getPriceInfo(0));
		LocalDateTime lastSegmentFlightTime = bookingSegmentInfos.get(bookingSegmentInfos.size() - 1).getArrivalTime();
		String retainTill = DateFormatterHelper.format(lastSegmentFlightTime.plusDays(retainForDays), "ddMMM");
		int paxCount = AirUtils.getPaxCountFromTravellerInfo(
				bookingSegmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo(), true);
		String command = StringUtils.join("0OTH", carrierCode, "GK", paxCount,
				bookingSegmentInfos.get(0).getDepartureAirportCode(), retainTill, "-KEEP PNR ACTIVE");
		return command;
	}

	public static PostProcessing_type0 getPostProcessing() {
		PostProcessing_type0 pp = new PostProcessing_type0();
		RedisplayReservation_type1 redisplayReservation = new RedisplayReservation_type1();
		redisplayReservation.setWaitInterval(BigInteger.valueOf(1000));
		pp.setRedisplayReservation(redisplayReservation);
		return pp;
	}

	/**
	 * 
	 * @param gstAddress
	 * 
	 *        As Sabre System doesn't support more than 35 characters in line. We are splitting GSTA into 35 characters
	 *        String.
	 * @return
	 */
	public static String splitGSTA(String input) {
		String gstA = "";
		int startIndex = 0;

		if (input.length() > 35) {
			gstA = StringUtils.join(SabreUtils.applyLengthLimitation(input, startIndex, 35) + "/");
			gstA = StringUtils.join(gstA, SabreUtils.applyLengthLimitation(input, 35, 60) + "/");
		} else {
			gstA = StringUtils.join(SabreUtils.applyLengthLimitation(input, startIndex, 35), "//");
		}
		return gstA;
	}

	public static String applyLengthLimitation(String input, int startIndex, int endIndex) {
		return input.substring(startIndex, Math.min(input.length(), endIndex));
	}

	public static boolean setIsReturnSegment(List<TripInfo> tripInfos, User bookingUser) {
		if (CollectionUtils.isNotEmpty(tripInfos) && tripInfos.size() == 2) {
			TripInfo onwardTrip = tripInfos.get(0);
			TripInfo returnTrip = tripInfos.get(1);
			String onwardDepartureAirport = onwardTrip.getDepartureAirportCode();
			String onwardArrivalAirport = onwardTrip.getArrivalAirportCode();
			String returnDepartureAirport = returnTrip.getDepartureAirportCode();
			String retunrArrivalAirport = returnTrip.getArrivalAirportCode();
			if ((onwardDepartureAirport.equals(retunrArrivalAirport)
					|| AirUtils.isGNNearByAirport(onwardDepartureAirport, onwardTrip.getSupplierInfo().getSourceId(),
							null, bookingUser)
					|| AirUtils.isGNNearByAirport(retunrArrivalAirport, onwardTrip.getSupplierInfo().getSourceId(),
							null, bookingUser))
					&& (onwardArrivalAirport.equals(returnDepartureAirport)
							|| AirUtils.isGNNearByAirport(onwardArrivalAirport,
									onwardTrip.getSupplierInfo().getSourceId(), null, bookingUser)
							|| AirUtils.isGNNearByAirport(returnDepartureAirport,
									onwardTrip.getSupplierInfo().getSourceId(), null, bookingUser))) {
				return true;
			}
		}
		return false;
	}

	public static Integer calculateDuration(SegmentInfo segmentInfo, Integer elapsedTime) {
		if (elapsedTime != null) {
			return elapsedTime;
		}
		return (int) segmentInfo.calculateDuration();
	}

	public static Integer calculateDuration(SegmentInfo segmentInfo, String elapsedTime) {
		if (StringUtils.isNotBlank(elapsedTime)) {
			String[] splitsTimings = elapsedTime.split("[.]");
			if (splitsTimings.length == 1) {
				return Integer.valueOf(splitsTimings[0]);
			}
			return Integer.valueOf(splitsTimings[0]) * 60 + Integer.valueOf(splitsTimings[1]);
		}
		return (int) segmentInfo.calculateDuration();
	}

	public static boolean isAccountCodeAvailable(PTCFareBreakdownType ptcBreakDown, int fareBasisIndex) {
		return ptcBreakDown.getFareBasisCodes() != null
				&& ArrayUtils.isNotEmpty(ptcBreakDown.getFareBasisCodes().getFareBasisCode())
				&& ptcBreakDown.getFareBasisCodes().getFareBasisCode()[fareBasisIndex].getAccountCode() != null
				&& StringUtils.isNotBlank(ptcBreakDown.getFareBasisCodes().getFareBasisCode()[fareBasisIndex]
						.getAccountCode().getStringLength1To20());
	}

	public static PaxType getPaxTypeOnTitle(FlightTravellerInfo traveller, LocalDate departureDate) {
		PaxType paxType = PaxType.ADULT;
		if (traveller != null && StringUtils.isNotBlank(traveller.getTitle())) {
			if (isInfant(traveller, departureDate)) {
				paxType = PaxType.INFANT;
			} else if (isChild(traveller, departureDate)) {
				paxType = PaxType.CHILD;
			}
		}
		return paxType;
	}

	public static boolean isInfant(FlightTravellerInfo travellerInfo, LocalDate departureDate) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			String title = travellerInfo.getTitle().toUpperCase();
			LocalDate dob = travellerInfo.getDob();
			if ((title.equalsIgnoreCase("MASTER") || title.equalsIgnoreCase("MSTR") || title.equalsIgnoreCase("MS"))
					&& dob != null && Period.between(dob, departureDate).getDays() <= 2) {
				return true;
			}
		}
		return false;
	}

	public static boolean isChild(FlightTravellerInfo travellerInfo, LocalDate departureDate) {
		if (travellerInfo != null && StringUtils.isNotEmpty(travellerInfo.getTitle())) {
			String title = travellerInfo.getTitle().toUpperCase();
			LocalDate dob = travellerInfo.getDob();
			if ((title.equalsIgnoreCase("MR") || title.equalsIgnoreCase("MSTR") || title.equalsIgnoreCase("MS"))
					&& (dob != null && Period.between(dob, departureDate).getDays() >= 2
							&& Period.between(dob, departureDate).getDays() <= 12)) {
				return true;
			}
		}
		return false;
	}
}
