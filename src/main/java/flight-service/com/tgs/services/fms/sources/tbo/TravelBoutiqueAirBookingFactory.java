package com.tgs.services.fms.sources.tbo;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.service.tbo.datamodel.getBookingDetails.BookingDetailsResBody;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TravelBoutiqueAirBookingFactory extends AbstractAirBookingFactory {

	RestAPIListener listener = null;

	protected String tokenId;

	protected FlightAPIURLRuleCriteria apiURLS;

	public TravelBoutiqueAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		apiURLS = getEndPointURL();
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		String supplierReference = null;
		try {
			initialize();
			listener = new RestAPIListener(bookingId);
			TravelBoutiqueBookingManager bookingServiceManager =
					TravelBoutiqueBookingManager.builder().supplierConfiguration(supplierConf).bookingId(bookingId)
							.gstInfo(gstInfo).segmentInfos(bookingSegments.getSegmentInfos()).order(order)
							.deliveryInfo(deliveryInfo).listener(listener).apiURLS(apiURLS)
							.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
			boolean isLCC = bookingSegments.getSegmentInfos().get(0).getFlightDesignator().getAirlineInfo().getIsLcc();
			if (!isLCC) {
				pnr = bookingServiceManager.createPNR(TravelBoutiqueBookingManager.URL_SUFFIX1);
			} else {
				pnr = bookingServiceManager.createPNR(TravelBoutiqueBookingManager.URL_SUFFIX2);
				BookingUtils.updateAirlinePnr(bookingSegments.getSegmentInfos(), pnr);
			}
			SegmentBookingRelatedInfo relatedInfo = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo();
			supplierReference = relatedInfo.getTravellerInfo().get(0).getSupplierBookingId();
			log.info("PNR {} for booking Id {} supplier reference {}", pnr, bookingId, supplierReference);
			/**
			 * @implNote : This is able to handle only for GDS As its Travel Vendor,Blindly consider that their system
			 *           will have Fare Diff Condition
			 */
			tokenId = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
			String traceId = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
			boolean isFareDiff = bookingDetailsWithFareDiff(traceId, supplierReference);
			if (!isFareDiff && !isLCC) {
				isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos());
			} else if (isLCC && StringUtils.isNotBlank(pnr)) {
				isSuccess = BookingUtils.hasPNRInAllSegments(bookingSegments.getSegmentInfos());
			}
		} finally {

		}
		return isSuccess;
	}

	public boolean bookingDetailsWithFareDiff(String traceId, String supplierReference) {
		double amountPaidToSupplier = 0;
		boolean isFareDiff = false;
		try {
			TravelBoutiqueBookingDetailManager bookingDetailRes =
					TravelBoutiqueBookingDetailManager.builder().supplierConfiguration(supplierConf).pnr(pnr)
							.bookingId(bookingId).supplierBookingId(supplierReference).traceId(traceId).tokenId(tokenId)
							.apiURLS(apiURLS).segmentInfos(bookingSegments.getSegmentInfos()).listener(listener)
							.bookingUser(bookingUser).build();
			BookingDetailsResBody bookingDetails = bookingDetailRes.getBookingDetails();
			if (bookingDetails != null && !bookingDetailRes.isCriticalException(bookingDetails.getResponse().getError(),
					bookingDetails.getResponse().getResponseStatus())) {
				amountPaidToSupplier = bookingDetailRes.getPublishedFare(bookingDetails);
				isFareDiff = isFareDiff(amountPaidToSupplier);
				return isFareDiff;
			}
		} catch (Exception e) {
			log.error("Unable to fetch bookingDetails for bookingId {} post pnr generation", bookingId, e);
		}
		return isFareDiff;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		initialize();
		if (StringUtils.isEmpty(pnr)) {
			pnr = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
		}
		// login
		try {
			if (listener == null) {
				listener = new RestAPIListener(bookingId);
			}
			if (bookingSegments.getSegmentInfos().get(0).getMiscInfo() == null || StringUtils.isEmpty(tokenId)) {
				TravelBoutiqueAuthenticationManager loginServiceManager = TravelBoutiqueAuthenticationManager.builder()
						.supplierConfiguration(supplierConf).apiURLS(apiURLS).uniqueId(bookingId).listener(listener)
						.bookingUser(bookingUser).build();
				tokenId = loginServiceManager.login().getTokenId();
			} else {
				tokenId = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
			}

			if (StringUtils.isBlank(pnr)) {
				log.error("Pnr empty for bookingId {}", bookingId);
				return false;
			}
			// confirm booking using new Token ID
			SegmentBookingRelatedInfo bookingRelatedInfo =
					bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo();
			TravelBoutiqueAirConfirmBookingManager confirmBookingManager = TravelBoutiqueAirConfirmBookingManager
					.builder().supplierConfiguration(supplierConf).tokenId(tokenId).bookingId(bookingId)
					.supplierBookingId(bookingRelatedInfo.getTravellerInfo().get(0).getSupplierBookingId())
					.segmentInfos(bookingSegments.getSegmentInfos()).criticalMessageLogger(criticalMessageLogger)
					.pnr(pnr).apiURLS(apiURLS).listener(listener).bookingUser(bookingUser).build();

			confirmBookingManager.fetchTicketNumber();
			log.info("Booking Confirmed  bookingId -{} & PNR {} ", bookingId, pnr);
			isSuccess = true;

		} finally {

		}
		return isSuccess;
	}
}
