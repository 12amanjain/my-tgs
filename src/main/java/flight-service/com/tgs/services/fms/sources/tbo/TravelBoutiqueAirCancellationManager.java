package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.cancellation.TboCancellationRequest;
import com.tgs.service.tbo.datamodel.cancellation.TboCancellationResponse;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class TravelBoutiqueAirCancellationManager extends TravelBoutiqueServiceManager {

	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/ReleasePNRRequest/";

	private String source;

	public boolean cancelPnr() {
		boolean isSuccess = false;
		HttpUtils httpUtils = null;
		try {
			TboCancellationRequest cancellationRequest = new TboCancellationRequest(tokenId, pnr, source, IP);
			httpUtils = HttpUtils.builder().urlString(getCancelURL(URL_SUFFIX))
					.postData(GsonUtils.getGson().toJson(cancellationRequest)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			TboCancellationResponse cancellationresponse =
					httpUtils.getResponse(TboCancellationResponse.class).orElse(null);
			if (!isCriticalException(cancellationresponse.getResponse().getError(),
					cancellationresponse.getResponse().getResponseStatus())) {
				isSuccess = true;
				log.info("Cancellation  Confirmed for  bookingId -{} & PNR {} ", bookingId, pnr);
			}
		} catch (IOException e) {
			log.info("Ubale to Cancel , for PNR {} and bookingId {}", pnr, bookingId);
			throw new SupplierRemoteException(e.getMessage());
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "ReleasePnrRQ"),
					formatRQRS(httpUtils.getResponseString(), "ReleasePnrRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("8-ReleasePnr").build());
		}
		return isSuccess;
	}


	public String getCancelURL(String urlSuffix) {
		String cancelUrl = null;
		if (apiURLS == null) {
			cancelUrl =
					StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[2], urlSuffix);
		} else {
			cancelUrl = apiURLS.getFlightURL();
		}
		log.debug("TBO Cancellation URL {}", cancelUrl);
		return cancelUrl;
	}


}
