package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import java.util.List;
import com.tgs.service.tbo.datamodel.confirmbook.ConfirmBookingRequestBody;
import com.tgs.service.tbo.datamodel.confirmbook.ConfirmBookingResponseBody;
import com.tgs.service.tbo.datamodel.confirmbook.Passenger;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@SuperBuilder
final class TravelBoutiqueAirConfirmBookingManager extends TravelBoutiqueServiceManager {

	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/Ticket/";
	private List<SegmentInfo> segmentInfos;
	private String pnr;
	private @Getter ConfirmBookingResponseBody confirmBookingResponse;
	private String supplierBookingId;

	public void fetchTicketNumber() {
		HttpUtils httpUtils = null;
		try {
			ConfirmBookingRequestBody requestBody =
					new ConfirmBookingRequestBody(tokenId, supplierBookingId, traceId, pnr, IP);
			httpUtils = HttpUtils.builder().urlString(getBookURL(URL_SUFFIX))
					.postData(GsonUtils.getGson().toJson(requestBody)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();

			confirmBookingResponse = httpUtils.getResponse(ConfirmBookingResponseBody.class).orElse(null);
			if (!isCriticalException(confirmBookingResponse.getResponse().getError(),
					confirmBookingResponse.getResponse().getResponseStatus().toString())) {
				Passenger[] passenger =
						confirmBookingResponse.getResponse().getResponse().getFlightItinerary().getPassenger();
				segmentInfos.forEach(segmentInfo -> {
					for (int i = 0; i < passenger.length; i++) {
						if (passenger[i].getTicket() != null
								&& StringUtils.isNotBlank(passenger[i].getTicket().getTicketNumber())) {
							// Airline prefix we will get from validating airline
							String ticketNumber = StringUtils.join(passenger[i].getTicket().getValidatingAirline(),
									passenger[i].getTicket().getTicketNumber());
							BookingUtils.updateTicketNumber(segmentInfos, ticketNumber,
									segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(i));
						}
					}
				});

				updateBookingPnr(confirmBookingResponse.getResponse().getResponse().getFlightItinerary(), segmentInfos);
			}
		} catch (IOException io) {
			throw new SupplierRemoteException(io);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "ConfirmRQ"),
					formatRQRS(httpUtils.getResponseString(), "ConfirmRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("7-B-Confirm")
					.supplier(supplierConfiguration.getBasicInfo().getSupplierId()).build());
		}
	}


	public String getBookURL(String urlSuffix) {
		String bookURL = null;
		if (apiURLS == null) {
			bookURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[2], urlSuffix);
		} else {
			bookURL = apiURLS.getAssesfeeURL();
		}
		log.debug("TBO Confirm Book URL {}", bookURL);
		return bookURL;
	}
}
