package com.tgs.services.fms.sources.tbo;

import java.time.LocalDateTime;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.service.tbo.datamodel.login.LoginResponseBody;
import com.tgs.service.tbo.datamodel.ssr.SSRResponseBody;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

@Slf4j
@Service
public class TravelBoutiqueAirInfoFactory extends AbstractAirInfoFactory {

	protected RestAPIListener listener = null;

	protected String tokenId;

	protected FlightAPIURLRuleCriteria apiURLS;

	public TravelBoutiqueAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize() {
		if (listener == null) {
			listener = new RestAPIListener("");
		}
		apiURLS = AirUtils.getAirEndPointURL(sourceConfiguration);
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize();
		LoginResponseBody loginBody = null;
		int numTry = 0;
		boolean doRetry = false;
		TravelBoutiqueSearchManager searchManager = null;
		tokenId = getTokenId();
		do {
			try {
				AirSupplierUtils.isStoreLogRequired(searchQuery);
				if (StringUtils.isEmpty(tokenId)) {
					TravelBoutiqueAuthenticationManager loginManager = TravelBoutiqueAuthenticationManager.builder()
							.supplierConfiguration(supplierConf).searchQuery(searchQuery).listener(listener)
							.apiURLS(apiURLS).bookingUser(user).build();
					loginBody = loginManager.login();
					tokenId = loginBody.getTokenId();
				}
				searchManager = TravelBoutiqueSearchManager.builder().supplierConfiguration(supplierConf)
						.searchQuery(searchQuery).tokenId(tokenId).apiURLS(apiURLS).listener(listener).bookingUser(user)
						.build();
				searchResult = searchManager.doSearch();
				if (searchManager.isNewSearchRequired()) {
					doRetry = true;
					tokenId = null;
				}
			} catch (SupplierRemoteException e) {
				doRetry = true;
				if (numTry == 1) {
					throw e;
				}
			} finally {
				if (!searchManager.isNewSearchRequired() && StringUtils.isNotEmpty(tokenId)) {
					storeTokenId();
				}
			}
		} while (++numTry < 2 && doRetry);
		return;
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		String resultIndex = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareKey();
		TripInfo tripInfo = null;
		TravelBoutiqueReviewManager reviewManager = null;
		try {
			initialize();
			listener.setKey(bookingId);
			reviewManager = TravelBoutiqueReviewManager.builder().supplierConfiguration(supplierConf)
					.searchQuery(searchQuery).bookingId(bookingId).listener(listener).apiURLS(apiURLS)
					.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			tripInfo = reviewManager.reviewTripInfo(resultIndex, selectedTrip);

			if (tripInfo != null) {
				TravelBoutiqueSSRManager ssrManager =
						TravelBoutiqueSSRManager.builder().supplierConfiguration(supplierConf).searchQuery(searchQuery)
								.bookingId(bookingId).listener(listener).apiURLS(apiURLS)
								.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
				selectedTrip = ssrManager.getTripInfoWithSSR(tripInfo, resultIndex);
				getFareRule(selectedTrip, bookingId, true);
			}
		} finally {
			if (reviewManager.isReviewSuccess() && tripInfo != null) {
				storeBookingSession(bookingId, null, null, tripInfo);
			}
		}
		return tripInfo;

	}


	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		String resultIndex = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareKey();
		initialize();
		listener.setKey(bookingId);
		TravelBoutiqueSSRManager ssrManager = TravelBoutiqueSSRManager.builder().supplierConfiguration(supplierConf)
				.searchQuery(searchQuery).bookingId(bookingId).listener(listener).apiURLS(apiURLS)
				.criticalMessageLogger(criticalMessageLogger).build();
		SSRResponseBody response = ssrManager.getSeatMap(tripInfo, resultIndex);
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			List<SeatSSRInformation> seatInfos =
					ssrManager.createSeatMap(ssrManager.getRowSeat(response, segmentInfo.getSegmentKey()));
			if (CollectionUtils.isNotEmpty(seatInfos)) {
				SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatInfos).build();
				tripSeatMap.getTripSeat().put(segmentInfo.getId(), seatInformation);
			}
		});
		return tripSeatMap;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		try {
			initialize();
			listener.setKey(bookingId);
			TravelBoutiqueFareRuleManager fareRuleServiceManager = TravelBoutiqueFareRuleManager.builder()
					.supplierConfiguration(supplierConf).tripInfo(selectedTrip).listener(listener).apiURLS(apiURLS)
					.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
			FareRuleInformation fareRuleInfo = new FareRuleInformation();
			String fareRule = StringEscapeUtils.escapeHtml(fareRuleServiceManager.getFareRule(bookingId));
			if (StringUtils.isNotEmpty(fareRule)) {
				// To remove </> HTML tags
				fareRule = fareRule.replaceAll("\\<.*?\\>", "");
				fareRuleInfo.getMiscInfo().put("Conditions", fareRule);
				tripFareRule.getFareRule().put(selectedTrip.getDepartureArrivalKey(), fareRuleInfo);
			}
		} catch (Exception e) {
			log.error("FareRule Failed for trip {} bookingid {} cause", selectedTrip.toString(), bookingId, e);
		}
		return tripFareRule;
	}

	public String getTokenId() {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
				.compress(false).index(0).build();
		metaInfo = cachingComm.fetchFromQueue(metaInfo);
		if (metaInfo == null) {
			metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString()).index(-1)
					.compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
		}
		if (metaInfo != null && metaInfo.getValue() != null) {
			return (String) metaInfo.getValue();
		}
		return null;
	}

	public void storeTokenId() {
		SessionMetaInfo metaInfo = SessionMetaInfo.builder().key(supplierConf.getBasicInfo().getRuleId().toString())
				.compress(false).build();
		metaInfo.setValue(tokenId);
		metaInfo.setExpiryTime(LocalDateTime.MAX);
		cachingComm.storeInQueue(metaInfo);
	}

}

