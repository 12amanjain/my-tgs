package com.tgs.services.fms.sources.tbo;

import com.tgs.services.base.enums.FareType;

public class TravelBoutiqueAirUtils {


	public static FareType getFareType(String airlineRemark) {
		FareType fareType = FareType.PUBLISHED;

		if (airlineRemark.contains("SME")) {
			fareType = FareType.SME;
		} else if (airlineRemark.contains("FLEXI")) {
			fareType = FareType.FLEXI;
		} else if (airlineRemark.contains("CORP")) {
			fareType = FareType.CORPORATE;
		} else if (airlineRemark.contains("SALE")) {
			fareType = FareType.SALE;
		} else if (airlineRemark.contains("TACTICAL")) {
			fareType = FareType.TACTICAL;
		} else if (airlineRemark.contains("PROMO")) {
			fareType = FareType.PROMO;
		} else if (airlineRemark.contains("FAMILY")) {
			fareType = FareType.FAMILY;
		} else if (airlineRemark.contains("LITE")) {
			fareType = FareType.LITE;
		} else if (airlineRemark.contains("COUPON")) {
			fareType = FareType.COUPON;
		}

		return fareType;
	}

}
