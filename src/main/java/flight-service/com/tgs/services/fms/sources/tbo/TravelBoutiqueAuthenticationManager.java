package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.login.LoginRequestBody;
import com.tgs.service.tbo.datamodel.login.LoginResponseBody;
import com.tgs.service.tbo.datamodel.logout.LogoutRequestBody;
import com.tgs.service.tbo.datamodel.logout.LogoutResponseBody;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierSessionException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class TravelBoutiqueAuthenticationManager extends TravelBoutiqueServiceManager {

	// This is are production environment
	private static final String URL_LOGIN_SUFFIX = "/SharedAPI/SharedData.svc/rest/Authenticate";

	// This is are production environment
	private static final String URL_LOGOUT_SUFFIX = "/SharedAPI/SharedData.svc/rest/Logout";

	private String uniqueId;

	public LoginResponseBody login() throws SupplierSessionException, SupplierRemoteException {
		HttpUtils httpUtils = null;
		LoginResponseBody responseBody = null;
		try {
			LoginRequestBody loginRequest =
					new LoginRequestBody(getSupplierConfiguration().getSupplierCredential().getClientId(),
							getSupplierConfiguration().getSupplierCredential().getUserName(),
							getSupplierConfiguration().getSupplierCredential().getPassword(), IP);
			httpUtils = HttpUtils.builder().urlString(getLoginURL()).postData(GsonUtils.getGson().toJson(loginRequest))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(LoginResponseBody.class).orElse(null);
			if (!isCriticalException(responseBody.getError(), String.valueOf(responseBody.getStatus()))) {
				return responseBody;
			} else {
				throw new SupplierSessionException(responseBody.getError().getErrorMessage());
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			if (StringUtils.isEmpty(uniqueId)) {
				if (searchQuery != null)
					uniqueId = searchQuery.getSearchId();
			}
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "AuthenticateRQ"),
					formatRQRS(httpUtils.getResponseString(), "AuthenticateRS"));
			listener.addLog(LogData.builder().key(uniqueId).logData(endPointRQRS)
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).type("1-L-Authenticate").build());
		}
	}

	public LogoutResponseBody logout(String TokenId, String TokenMemberId, String TokenAgencyId) throws IOException {
		HttpUtils httpUtils = null;
		try {
			LogoutRequestBody requestBody = new LogoutRequestBody(TokenId, TokenMemberId, TokenAgencyId,
					supplierConfiguration.getSupplierCredential().getClientId(), IP);
			httpUtils = HttpUtils.builder().urlString(getLogOutURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			return httpUtils.getResponse(LogoutResponseBody.class).orElse(null);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "LogOutRQ"),
					formatRQRS(httpUtils.getResponseString(), "LogOutRS"));
			listener.addLog(
					LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS).type("Logout").build());
		}
	}

	public String getLoginURL() {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[0],
					URL_LOGIN_SUFFIX);
		} else {
			apirUrl = apiURLS.getSecurityURL();
		}
		log.info("TBO Authentication URL {}", apirUrl);
		return apirUrl;
	}

	public String getLogOutURL() {
		String apirUrl = null;
		if (apiURLS == null) {
			apirUrl = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[0],
					URL_LOGOUT_SUFFIX);
		} else {
			apirUrl = apiURLS.getLogOutURL();
		}
		log.debug("TBO Log Out  URL {}", apirUrl);
		return apirUrl;
	}
}
