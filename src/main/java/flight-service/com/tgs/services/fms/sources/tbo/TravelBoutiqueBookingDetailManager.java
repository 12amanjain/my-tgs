package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import java.util.List;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.getBookingDetails.BookingDetailsReqBody;
import com.tgs.service.tbo.datamodel.getBookingDetails.BookingDetailsResBody;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class TravelBoutiqueBookingDetailManager extends TravelBoutiqueServiceManager {

	@Getter
	private BookingDetailsResBody responseBody;

	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/GetBookingDetails/";
	private String supplierBookingId;
	private List<SegmentInfo> segmentInfos;

	public BookingDetailsResBody getBookingDetails() throws IOException {
		HttpUtils httpUtils = null;
		try {
			BookingDetailsReqBody requestBody = null;
			if (!StringUtils.isBlank(traceId)) {
				requestBody = new BookingDetailsReqBody(tokenId, traceId, null, supplierBookingId, IP);
			} else if (!StringUtils.isBlank(supplierBookingId)) {
				requestBody = new BookingDetailsReqBody(tokenId, null, null, supplierBookingId, IP);
			} else {
				return null;
			}

			httpUtils = HttpUtils.builder().urlString(getBookingDetailURL())
					.postData(GsonUtils.getGson().toJson(requestBody)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(BookingDetailsResBody.class).orElse(null);
			if (responseBody != null && !isCriticalException(responseBody.getResponse().getError(),
					String.valueOf(responseBody.getResponse().getResponseStatus()))) {
				updateBookingPnr(responseBody.getResponse().getFlightItinerary(), segmentInfos);
			}
			return responseBody;
		} catch (IOException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to fetch bookingDetails for booking ID {}", bookingId, e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "Booking Detail RQ"),
					formatRQRS(httpUtils.getResponseString(), "Booking Detail RS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("BookingDetail").build());
		}

		return null;
	}

	public double getPublishedFare(BookingDetailsResBody responseBody) {
		double totalFare = 0;
		if (responseBody != null && responseBody.getResponse() != null
				&& !isCriticalException(responseBody.getResponse().getError(),
						responseBody.getResponse().getResponseStatus())) {
			totalFare = responseBody.getResponse().getFlightItinerary().getFare().getPublishedFare();
			log.info("Suppplier Published Fare {} for booking id {} ", totalFare, bookingId);
		} else {
			totalFare = 0;
			if (responseBody != null) {
				log.error("Booking failed for bookingid {} error msg {}", bookingId,
						getErrorMessage(responseBody.getResponse().getError()));
			}
		}
		return totalFare;
	}

	public String getBookingDetailURL() {
		String detailURL = null;
		if (apiURLS == null) {
			detailURL =
					StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[2], URL_SUFFIX);
		} else {
			detailURL = apiURLS.getHost();
		}
		log.debug("TBO Detail URL {}", detailURL);
		return detailURL;
	}

}
