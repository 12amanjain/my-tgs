package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.book.Baggage;
import com.tgs.service.tbo.datamodel.book.BookResponse;
import com.tgs.service.tbo.datamodel.book.Fare;
import com.tgs.service.tbo.datamodel.book.PassengerRequest;
import com.tgs.service.tbo.datamodel.book.PassengerResponse;
import com.tgs.service.tbo.datamodel.book.SeatDynamic;
import com.tgs.service.tbo.datamodel.book.TBOBookRequestBody;
import com.tgs.service.tbo.datamodel.book.TBOBookResponseBody;
import com.tgs.service.tbo.datamodel.getBookingDetails.BookingDetailsResBody;
import com.tgs.service.tbo.datamodel.ssr.MealDynamic;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class TravelBoutiqueBookingManager extends TravelBoutiqueServiceManager {

	public static final String URL_SUFFIX1 = "/AirAPI_V10/AirService.svc/rest/Book/";
	public static final String URL_SUFFIX2 = "/AirAPI_V10/AirService.svc/rest/Ticket/";

	private GstInfo gstInfo;
	private Order order;
	private List<SegmentInfo> segmentInfos;
	@Getter
	private Calendar holdTimeLimit;

	public String createPNR(String urlSuffix) {
		HttpUtils httpUtils = null;
		TBOBookRequestBody requestBody = null;
		try {
			requestBody = generateRequestBody();
			httpUtils = HttpUtils.builder().urlString(getBookURL(urlSuffix))
					.postData(GsonUtils.getGson().toJson(requestBody)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();

			TBOBookResponseBody responseBody = httpUtils.getResponse(TBOBookResponseBody.class).orElse(null);
			if (responseBody != null && !isCriticalException(responseBody.getResponse().getError(),
					String.valueOf(responseBody.getResponse().getResponseStatus()))) {
				BookResponse bookResponse = responseBody.getResponse().getResponse();
				Integer supplierRefernce = bookResponse.getBookingId();
				BookingUtils.updateSupplierBookingId(segmentInfos, String.valueOf(supplierRefernce));
				/**
				 * @implSpec : Where some LCC airlines has Ticket Number on Book Call, EG: G9
				 */
				PassengerResponse[] passenger = bookResponse.getFlightItinerary().getPassenger();
				segmentInfos.forEach(segmentInfo -> {
					for (int i = 0; i < passenger.length; i++) {
						if (passenger[i].getTicket() != null
								&& StringUtils.isNotBlank(passenger[i].getTicket().getTicketNumber())) {
							BookingUtils.updateTicketNumber(segmentInfos, passenger[i].getTicket().getTicketNumber(),
									segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(i));
						}
					}
				});

				if (airLinePnrPresent(bookResponse.getFlightItinerary().getSegments())) {
					setReservetionPNRForSegment(segmentInfos, bookResponse.getPNR());
					segmentInfos.forEach(segmentInfo -> {
						segmentInfo.updateAirlinePnr(
								getAirlinePnr(segmentInfo, bookResponse.getFlightItinerary().getSegments()));
					});
				}
				return bookResponse.getPNR();
			}
		} catch (IOException e) {
			log.error("TBO Booking failed because of connection failure for bookingId {}", bookingId, e);
			BookingDetailsResBody bookingDetail = getBookingDetails();
			if (bookingDetail != null) {
				return bookingDetail.getResponse().getFlightItinerary().getPNR();
			}
		} catch (Exception e) {
			log.error("Booking failed for booking Id {}", bookingId, e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "BookRQ"),
					formatRQRS(httpUtils.getResponseString(), "BookRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("6-B-Book")
					.visibilityGroups(AirSupplierUtils.getLogVisibility()).build());
		}
		return null;
	}

	public BookingDetailsResBody getBookingDetails() {
		int numTry = 0;
		do {
			try {
				Thread.sleep(10 * 1000);
				SegmentBookingRelatedInfo relatedInfo = segmentInfos.get(0).getBookingRelatedInfo();
				return TravelBoutiqueBookingDetailManager.builder().supplierConfiguration(supplierConfiguration)
						.supplierBookingId(relatedInfo.getTravellerInfo().get(0).getSupplierBookingId())
						.bookingId(bookingId).tokenId(tokenId).traceId(traceId).bookingUser(bookingUser).build()
						.getBookingDetails();
			} catch (InterruptedException e) {
				log.error("Interrupted expection occured for bookingid {}", bookingId, e);
			} catch (IOException e) {
				log.error("Booking-Details Fetch failed because of connection failure for booking Id {}", bookingId, e);
			}
		} while (++numTry < 5);
		return null;
	}

	private TBOBookRequestBody generateRequestBody() {
		SegmentInfo segmentInfo = null;
		try {
			segmentInfo = segmentInfos.get(0);
			tokenId = segmentInfo.getPriceInfo(0).getMiscInfo().getTokenId();
			traceId = segmentInfo.getPriceInfo(0).getMiscInfo().getTraceId();

			String resultIndex = getResultIndex(segmentInfo);

			String addressLine1;
			String city;
			String countryCode;
			String countryName;

			List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
			PassengerRequest passengers[] = new PassengerRequest[travellerInfos.size()];

			if (passengers.length == 0) {
				return new TBOBookRequestBody(IP, tokenId, traceId, resultIndex, passengers);
			}

			ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
			AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(order.getBookingUserId(), clientInfo);
			addressLine1 = contactAddress.getAddress();
			city = contactAddress.getCityInfo().getName();
			countryCode = clientInfo.getNationality();
			countryName = contactAddress.getCityInfo().getCountry();

			for (int paxIndex = 0; paxIndex < passengers.length; paxIndex++) {
				FlightTravellerInfo travellerInfo = travellerInfos.get(paxIndex);
				ArrayList<MealDynamic> mealDynamicList = new ArrayList<>();
				ArrayList<Baggage> baggageList = new ArrayList<>();
				ArrayList<SeatDynamic> seatList = new ArrayList<>();
				segmentInfos.forEach(segmentInfo1 -> {
					List<FlightTravellerInfo> travellerInfos1 = segmentInfo1.getBookingRelatedInfo().getTravellerInfo();
					travellerInfos1.forEach(travellerInfo1 -> {
						if (getPaxName(travellerInfo).equals(getPaxName(travellerInfo1))) {
							SSRInformation ssrMealInfo = travellerInfo1.getSsrMealInfo();
							SSRInformation ssrBaggageInfo = travellerInfo1.getSsrBaggageInfo();
							SSRInformation ssrSeatInfo = travellerInfo1.getSsrSeatInfo();
							if (ssrMealInfo != null) {
								mealDynamicList.add(getMealDynamic(segmentInfo1, ssrMealInfo, segmentInfos));
							}
							if (ssrBaggageInfo != null) {
								baggageList.add(getBaggage(segmentInfo1, ssrBaggageInfo, segmentInfos));
							}
							if (ssrSeatInfo != null) {
								seatList.add(getSeat(segmentInfo1, ssrSeatInfo, segmentInfos));
							}
						}
					});
				});

				passengers[paxIndex] = PassengerRequest.builder().Title(travellerInfo.getTitle())
						.FirstName(travellerInfo.getFirstName()).LastName(travellerInfo.getLastName())
						.PaxType(TravelBoutiquePaxType.valueOf(travellerInfo.getPaxType().name()).getValue())
						.DateOfBirth(getDOB(travellerInfo)).Gender(getGenderFromTitle(travellerInfo.getTitle()))
						.PassportNo(travellerInfo.getPassportNumber()).AddressLine1(addressLine1).City(city)
						.CountryCode(countryCode).CountryName(countryName)
						.ContactNo(AirSupplierUtils.getContactNumber(deliveryInfo))
						.Email(AirSupplierUtils.getEmailId(deliveryInfo)).IsLeadPax(paxIndex == 0)
						.FFAirlineCode(segmentInfo.getFlightDesignator().getAirlineInfo().getCode())
						.FFNumber(segmentInfo.getFlightDesignator().getFlightNumber())
						.Fare(getBookFare(
								segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(paxIndex).getFareDetail()))
						.build();

				if (travellerInfo.getPassportNumber() != null && travellerInfo.getExpiryDate() != null
						&& StringUtils.isNotBlank(travellerInfo.getPassportNationality())) {
					passengers[paxIndex].setPassportIssueDate(travellerInfo.getPassportIssueDate());
					passengers[paxIndex].setPassportIssueCountryCode(travellerInfo.getPassportNationality());
					passengers[paxIndex].setPassportExpiry(travellerInfo.getExpiryDate());
				}

				if (!travellerInfo.getPaxType().equals(PaxType.INFANT)) {
					if (CollectionUtils.isNotEmpty(mealDynamicList)) {
						passengers[paxIndex].setMealDynamic(mealDynamicList.toArray(new MealDynamic[] {}));
					}
					if (CollectionUtils.isNotEmpty(baggageList)) {
						passengers[paxIndex].setBaggage(baggageList.toArray(new Baggage[] {}));
					}
					if (CollectionUtils.isNotEmpty(seatList)) {
						passengers[paxIndex].setSeatDynamic(seatList.toArray(new SeatDynamic[] {}));
					}

				}
				if (gstInfo != null) {
					setGSTInfoforPax(passengers[paxIndex], gstInfo);
				}
			}

			return new TBOBookRequestBody(IP, tokenId, traceId, resultIndex, passengers);
		} catch (Exception e) {
			log.error("Unable to generate Booking/Ticketing request for bookingId  {}", bookingId, e);
		}
		return null;
	}

	private SeatDynamic getSeat(SegmentInfo segmentInfo, SSRInformation ssrSeatInfo, List<SegmentInfo> segmentInfos) {
		SeatDynamic seat = new SeatDynamic();
		int wayType = Integer.parseInt(ssrSeatInfo.getMiscInfo().getSeatCodeType());
		seat.setWayType(wayType);
		seat.setCode(ssrSeatInfo.getCode());
		seat.setPrice(ssrSeatInfo.getAmount());
		seat.setDescription(ssrSeatInfo.getMiscInfo().getJourneyType());
		seat.setOrigin(segmentInfo.getDepartAirportInfo().getCode());
		seat.setDestination(segmentInfo.getArrivalAirportInfo().getCode());
		return seat;
	}

	private String getResultIndex(SegmentInfo segmentInfo) {
		if (StringUtils.isNotBlank(segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey())) {
			return segmentInfo.getPriceInfo(0).getMiscInfo().getJourneyKey();
		}
		return segmentInfo.getPriceInfo(0).getMiscInfo().getFareKey();
	}

	private static void setGSTInfoforPax(PassengerRequest passenger, GstInfo gstInfo) {
		if (gstInfo != null && StringUtils.isNotEmpty(gstInfo.getGstNumber())) {
			passenger.setGSTNumber(gstInfo.getGstNumber());
			passenger.setGSTCompanyContactNumber(gstInfo.getMobile());
			passenger.setGSTCompanyName(BaseUtils.getCleanGstName(gstInfo.getRegisteredName()));
			passenger.setGSTCompanyEmail(gstInfo.getEmail());
			passenger.setGSTCompanyAddress(gstInfo.getAddress());
		}
	}

	private String getPaxName(FlightTravellerInfo travellerInfo) {
		String paxname = StringUtils.EMPTY;
		if (StringUtils.isNotBlank(travellerInfo.getFirstName())) {
			paxname = travellerInfo.getFirstName().toUpperCase();
		}
		return StringUtils.join(paxname, travellerInfo.getLastName().toUpperCase());
	}

	private MealDynamic getMealDynamic(SegmentInfo segmentInfo, SSRInformation ssrMealInfo,
			List<SegmentInfo> segmentInfos) {
		MealDynamic mealDynamic = new MealDynamic();
		int wayType = Integer.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getMealWayType());

		mealDynamic.setWayType(wayType);
		mealDynamic.setCode(ssrMealInfo.getCode());
		mealDynamic.setDescription(ssrMealInfo.getMiscInfo().getJourneyType());
		mealDynamic.setPrice(ssrMealInfo.getAmount());
		mealDynamic.setOrigin(segmentInfo.getDepartAirportInfo().getCode());
		mealDynamic.setDestination(segmentInfo.getArrivalAirportInfo().getCode());
		return mealDynamic;
	}

	private Baggage getBaggage(SegmentInfo segmentInfo, SSRInformation ssrBaggageInfo, List<SegmentInfo> segmentInfos) {
		int wayType = Integer.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getBaggageWayType());
		Baggage baggage = new Baggage();
		baggage.setWayType(wayType);
		baggage.setWeight(Integer.valueOf(segmentInfo.getPriceInfo(0).getMiscInfo().getWeight()));
		baggage.setCode(ssrBaggageInfo.getCode());
		baggage.setPrice(ssrBaggageInfo.getAmount());
		baggage.setDescription(ssrBaggageInfo.getMiscInfo().getJourneyType());
		baggage.setOrigin(segmentInfo.getDepartAirportInfo().getCode());
		baggage.setDestination(segmentInfo.getArrivalAirportInfo().getCode());
		return baggage;
	}

	private LocalDate getDOB(FlightTravellerInfo travellerInfo) {

		LocalDate dobDate = null;
		if (travellerInfo.getDob() != null) {
			dobDate = travellerInfo.getDob();
		} else if (travellerInfo.getPaxType().equals(PaxType.CHILD)) {
			dobDate = TgsDateUtils.getCurrentDate().minusYears(4);
		} else if (travellerInfo.getPaxType().equals(PaxType.INFANT)) {
			dobDate = TgsDateUtils.getCurrentDate().minusYears(1);
		}
		return dobDate;
	}

	private Fare getBookFare(FareDetail fareDetail) {

		double totalTax = fareDetail.getFareComponents().getOrDefault(FareComponent.AT, 0d)
				+ fareDetail.getFareComponents().getOrDefault(FareComponent.YQ, 0d);
		return Fare.builder().BaseFare(fareDetail.getFareComponents().getOrDefault(FareComponent.BF, 0d)).Tax(totalTax)
				.YQTax(0d).AdditionalTxnFeeOfrd(0d).AdditionalTxnFeePub(0d).build();
	}

	public String getBookURL(String urlSuffix) {
		String bookURL = null;
		if (apiURLS == null) {
			bookURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[2], urlSuffix);
		} else {
			if (urlSuffix.equals(URL_SUFFIX1)) {
				bookURL = apiURLS.getReservationURL();
			} else {
				bookURL = apiURLS.getAssesfeeURL();
			}
		}
		log.debug("TBO Book URL {}", bookURL);
		return bookURL;
	}

}
