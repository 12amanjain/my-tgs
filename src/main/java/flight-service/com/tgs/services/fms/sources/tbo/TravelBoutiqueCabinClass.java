package com.tgs.services.fms.sources.tbo;

import lombok.Getter;

@Getter
public enum TravelBoutiqueCabinClass {

	ECONOMY(1), PREMIUM_ECONOMY(3), BUSINESS(4), PREMIUM_BUSINESS(5), FIRST(6);

	private int value;

	private TravelBoutiqueCabinClass(int value) {
		this.value = value;
	}

}
