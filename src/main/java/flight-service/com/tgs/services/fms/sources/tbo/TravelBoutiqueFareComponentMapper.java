package com.tgs.services.fms.sources.tbo;

import com.tgs.services.base.enums.FareComponent;
import lombok.Getter;

@Getter
public enum TravelBoutiqueFareComponentMapper {

	K3(FareComponent.AGST);

	private FareComponent fareComponent;

	private TravelBoutiqueFareComponentMapper(FareComponent fareComponent) {
		this.fareComponent = fareComponent;
	}
}
