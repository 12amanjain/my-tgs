package com.tgs.services.fms.sources.tbo;

import com.tgs.service.tbo.datamodel.farerrule.FareRuleRequestBody;
import com.tgs.service.tbo.datamodel.farerrule.FareRuleResponseBody;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@SuperBuilder
final class TravelBoutiqueFareRuleManager extends TravelBoutiqueServiceManager {

	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/FareRule";

	private TripInfo tripInfo;

	public String getFareRule(String bookingId) {
		HttpUtils httpUtils = null;
		try {
			String resultIndex = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getFareKey();
			FareRuleRequestBody requestBody = new FareRuleRequestBody(IP,
					tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId(),
					tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId(), resultIndex);

			httpUtils =
					HttpUtils.builder().urlString(getFareRuleURL()).postData(GsonUtils.getGson().toJson(requestBody))
							.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			FareRuleResponseBody responseBody = httpUtils.getResponse(FareRuleResponseBody.class).orElse(null);
			if (requestBody != null && responseBody.getResponse() != null
					&& ArrayUtils.isNotEmpty(responseBody.getResponse().getFareRules())) {
				return responseBody.getResponse().getFareRules()[0].getFareRuleDetail();
			}
		} catch (Exception e) {
			log.error("Unable to get fare rule for priceID {}, tripInfo {}", bookingId, tripInfo.toString(), e);
			return null;
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "FareRuleRQ"),
					formatRQRS(httpUtils.getResponseString(), "FareRuleRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("3-R-FareRuleRS").build());
		}
		return null;
	}

	public String getFareRuleURL() {
		String fareRuleURL = null;
		if (apiURLS == null) {
			fareRuleURL =
					StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[1], URL_SUFFIX);
		} else {
			fareRuleURL = apiURLS.getFareRuleURL();
		}
		log.debug("TBO fareRule URL {}", fareRuleURL);
		return fareRuleURL;
	}
}
