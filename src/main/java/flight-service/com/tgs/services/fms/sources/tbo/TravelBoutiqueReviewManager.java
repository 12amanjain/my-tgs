package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import java.util.Objects;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.review.FareBreakdown;
import com.tgs.service.tbo.datamodel.review.FareQuoteRequestBody;
import com.tgs.service.tbo.datamodel.review.FareQuoteResponseBody;
import com.tgs.service.tbo.datamodel.review.FareQuoteSegment;
import com.tgs.service.tbo.datamodel.review.Response;
import com.tgs.service.tbo.datamodel.review.Results;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.GstConditions;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import lombok.Builder;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class TravelBoutiqueReviewManager extends TravelBoutiqueServiceManager {
	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/FareQuote";
	@Getter
	private FareQuoteResponseBody reviewResponse;
	private TripInfo selectedTripInfo;
	private TravelBoutiqueCabinClass cabinClass;

	@Getter
	@Builder.Default
	private boolean isReviewSuccess = true;

	public TripInfo reviewTripInfo(String resultIndex, TripInfo selectedTrip) {

		/**
		 * To re-do review after traceId becomes more than 2 minutes old. This is done so that we have 13 minutes till
		 * booking. TBO TraceId expires in 15 minutes. -[21-09-18, 13:20]. TraceId returned in search response should be
		 * same at the time of booking.
		 */
		if (selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getReviewTimeLimit().plusMinutes(10)
				.isBefore(LocalDateTime.now())) {
			isReviewSuccess = false;
			log.info("Review failed because Trace Id is more than 2 minutes old for bookingId {}", bookingId);
			throw new NoSeatAvailableException();
		}

		tokenId = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
		traceId = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();

		HttpUtils httpUtils = null;
		selectedTripInfo = null;
		try {
			FareQuoteRequestBody requestBody = new FareQuoteRequestBody(tokenId, resultIndex, traceId, IP);
			httpUtils = HttpUtils.builder().urlString(getReviewURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			selectedTripInfo = selectedTrip;
			reviewResponse = httpUtils.getResponse(FareQuoteResponseBody.class).orElse(null);

			if (!parseResponse()) {
				selectedTripInfo = null;
				isReviewSuccess = false;
			}
		} catch (IOException e) {
			isReviewSuccess = false;
			log.error("Unable to review booking for bookingId {}", bookingId, e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "FareQuoteRQ"),
					formatRQRS(httpUtils.getResponseString(), "FareQuoteRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("4-R-FareQuoteRS").build());
		}
		return selectedTripInfo;
	}

	private boolean parseResponse() {
		int i = 0;
		boolean isSuccess = false;
		try {
			Response response = reviewResponse.getResponse();
			if (!"1".equals(response.getResponseStatus()) || response.getError().getErrorCode() != 0) {
				String message = StringUtils.join("TBO Response Status: Fare quote failed for booking ID: ", bookingId);
				log.info("Review failed for booking id {} error {}", bookingId, message);
				criticalMessageLogger.add(message);
				isSuccess = false;
			}
			for (FareQuoteSegment[] resultSegmentList : response.getResults().getSegments()) {
				for (FareQuoteSegment resultSegment : resultSegmentList) {
					populatePriceInfo(response, resultSegment, i == 0, response.getResults().getIsLCC());
					i++;
				}
			}
			isSuccess = true;
		} catch (Exception e) {
			log.error("Unable to get response from the API ");
		}
		return isSuccess;
	}


	private void populatePriceInfo(Response response, FareQuoteSegment resultSegment, boolean isFirstSegmentInfo,
			Boolean isLcc) throws Exception {
		Results result = response.getResults();
		try {
			PriceInfo oldPriceInfo = selectedTripInfo.getSegmentInfos().get(0).getPriceInfo(0);
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(oldPriceInfo.getSupplierBasicInfo());
			Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
			if (StringUtils.isNotBlank(result.getAirlineRemark())) {
				priceInfo
						.setFareIdentifier(TravelBoutiqueAirUtils.getFareType(result.getAirlineRemark().toUpperCase()));
			} else {
				priceInfo.setFareIdentifier(FareType.PUBLISHED);
			}

			priceInfo.setMiscInfo(oldPriceInfo.getMiscInfo());
			priceInfo.getMiscInfo().setJourneyKey(result.getResultIndex());
			for (FareBreakdown fareBreakDown : result.getFareBreakdown()) {
				FareDetail fareDetail = new FareDetail();
				cabinClass = TravelBoutiqueCabinClass.valueOf(searchQuery.getCabinClass().name());
				fareDetail.setCabinClass(CabinClass.valueOf(cabinClass.name()));
				if (isFirstSegmentInfo) {
					updateFareDetail(fareDetail, fareBreakDown, resultSegment, result, result.getFare());
				} else {
					fareDetail.setFareComponents(getDefaultComponents());
				}
				fareDetails.put(PaxType.values()[fareBreakDown.getPassengerType() - 1], fareDetail);
			}
			setTripConditions(priceInfo, isLcc, result);
			if (Objects.nonNull(isLcc)) {
				priceInfo.getMiscInfo().setIsEticket(!isLcc);
			}
			if (isFirstSegmentInfo)
				selectedTripInfo.getSegmentInfos().get(0).setPriceInfoList(Arrays.asList(priceInfo));
		} catch (Exception e) {
			log.error("Unable to populate price-info for booking {}", bookingId, e);
		}
	}

	private void setTripConditions(PriceInfo priceInfo, Boolean isLcc, Results result) {
		try {
			selectedTripInfo.getBookingConditions()
					.setGstInfo(GstConditions.builder().isGSTMandatory(result.getIsGSTMandatory())
							.gstApplicable(Boolean.parseBoolean(result.getGSTAllowed())).build());
			if (Objects.nonNull(isLcc)) {
				selectedTripInfo.getBookingConditions().setIsBlockingAllowed(!isLcc);
			}
		} catch (Exception e) {
			log.error("Error Occured while setting trip Conditions for bookingId {}", bookingId);
		}
	}


	public String getReviewURL() {
		String searchURL = null;
		if (apiURLS == null) {
			searchURL =
					StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[1], URL_SUFFIX);
		} else {
			searchURL = apiURLS.getPricingURL();
		}
		log.debug("TBO FareQuote URL {}", searchURL);
		return searchURL;
	}

}
