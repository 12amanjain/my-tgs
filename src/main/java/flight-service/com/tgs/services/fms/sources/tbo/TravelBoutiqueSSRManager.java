package com.tgs.services.fms.sources.tbo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.ssr.MealDynamic;
import com.tgs.service.tbo.datamodel.ssr.RowSeat;
import com.tgs.service.tbo.datamodel.ssr.SSRRequestBody;
import com.tgs.service.tbo.datamodel.ssr.SSRResponseBody;
import com.tgs.service.tbo.datamodel.ssr.Seat;
import com.tgs.service.tbo.datamodel.ssr.SeatDynamic;
import com.tgs.service.tbo.datamodel.ssr.SegmentSeat;
import com.tgs.service.tbo.datamodel.ssr.TBOSSRBaggage;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatPosition;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class TravelBoutiqueSSRManager extends TravelBoutiqueServiceManager {

	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/SSR";
	private SSRResponseBody responseBody;

	public TripInfo getTripInfoWithSSR(TripInfo tripInfo, String resultIndex) {
		tokenId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
		traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();

		HttpUtils httpUtils = null;
		try {
			SSRRequestBody requestBody = new SSRRequestBody(IP, tokenId, traceId, resultIndex);
			httpUtils = HttpUtils.builder().urlString(getSSRURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(SSRResponseBody.class).orElse(null);

			if (!isCriticalException(responseBody.getResponse().getError(),
					String.valueOf(responseBody.getResponse().getResponseStatus()))) {
				populateSSRInfo(tripInfo);
			}
			return tripInfo;
		} catch (Exception e) {
			log.error("Unable to parse SSRInfo for booking Id {}, trip {} ", bookingId, tripInfo.toString(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SSRRQ"),
					formatRQRS(httpUtils.getResponseString(), "SSRRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("5-R-SSRRS").build());
		}
		return tripInfo;
	}

	private void populateSSRInfo(TripInfo tripInfo) {
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			if (segmentInfo.getFlightDesignator().getAirlineInfo().getIsLcc()) {
				Map<SSRType, List<? extends SSRInformation>> ssrInfo = new HashMap<>();

				List<BaggageSSRInformation> baggageSSRInformations =
						getBaggageInfos(segmentInfo.getFlightDesignator().getFlightNumber(), segmentInfo);
				if (baggageSSRInformations != null)
					ssrInfo.put(SSRType.BAGGAGE, baggageSSRInformations);

				List<MealSSRInformation> mealSSRInformations =
						getMealInfos(segmentInfo.getFlightDesignator().getFlightNumber(), segmentInfo);
				if (mealSSRInformations != null)
					ssrInfo.put(SSRType.MEAL, mealSSRInformations);

				segmentInfo.setSsrInfo(ssrInfo);
			}
		}
	}

	private List<BaggageSSRInformation> getBaggageInfos(String flightNumber, SegmentInfo segmentInfo) {
		if (flightNumber == null)
			return null;

		List<BaggageSSRInformation> baggageSSRInformations = new ArrayList<>();
		try {
			if (responseBody.getResponse().getBaggage() != null) {
				TBOSSRBaggage[][] baggages = responseBody.getResponse().getBaggage();
				for (TBOSSRBaggage[] baggageList : baggages) {
					for (TBOSSRBaggage baggage : baggageList) {
						try {
							if (baggage != null
									&& baggage.getOrigin().equalsIgnoreCase(segmentInfo.getDepartureAirportCode())
									&& baggage.getDestination().equalsIgnoreCase(segmentInfo.getArrivalAirportCode())) {
								if (!(baggage.getCode().equals("No Baggage"))) {
									if (flightNumber.equals(baggage.getFlightNumber())) {
										baggageSSRInformations.add(getBaggageSSRInformation(baggage, segmentInfo));
									}
								}
							} else if (baggage != null && StringUtils.isBlank(baggage.getOrigin())
									&& StringUtils.isBlank(baggage.getDestination())) {
								if (!(baggage.getCode().equals("No Baggage"))) {
									if (flightNumber.equals(baggage.getFlightNumber())) {
										baggageSSRInformations.add(getBaggageSSRInformation(baggage, segmentInfo));
									}
								}
							}
						} catch (Exception e) {
							log.error("Unable to parse BaggageSSRInfo for booking Id {} tbo-baggage {}", bookingId,
									baggage, e);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse BaggageSSRInfos for booking Id {} ", bookingId, e);
		}
		return baggageSSRInformations;
	}

	private BaggageSSRInformation getBaggageSSRInformation(TBOSSRBaggage baggage, SegmentInfo segmentInfo) {
		BaggageSSRInformation baggageSSRInformation = new BaggageSSRInformation();
		PriceMiscInfo miscInfo = segmentInfo.getPriceInfo(0).getMiscInfo();
		miscInfo.setWeight(String.valueOf(baggage.getWeight()));
		miscInfo.setBaggageWayType(String.valueOf(baggage.getWayType()));
		baggageSSRInformation.setUnit("kg");
		baggageSSRInformation.setDesc(String.valueOf(baggage.getWeight()) + " Kg");
		baggageSSRInformation.getMiscInfo().setJourneyType(baggage.getDescription());
		baggageSSRInformation.setCode(baggage.getCode());
		baggageSSRInformation.setAmount((double) baggage.getPrice());
		return baggageSSRInformation;
	}

	private List<MealSSRInformation> getMealInfos(String flightNumber, SegmentInfo segmentInfo) {
		if (flightNumber == null)
			return null;

		List<MealSSRInformation> mealSSRInformations = new ArrayList<>();
		try {
			if (responseBody.getResponse().getMealDynamic() != null) {
				MealDynamic[][] meals = responseBody.getResponse().getMealDynamic();
				for (MealDynamic[] mealList : meals) {
					for (MealDynamic meal : mealList) {
						try {
							if (!(meal.getCode().equals("No Meal"))) {
								if (flightNumber.equals(meal.getFlightNumber())) {
									mealSSRInformations.add(getMealSSRInformation(meal, segmentInfo));
								}
							}
						} catch (Exception e) {
							log.error("Unable to parse MealSSRInfo for booking Id {} tbo-baggage {}", bookingId, meal,
									e);
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Unable to parse MealSSRInfos for booking Id {} ", bookingId, e);
		}
		return mealSSRInformations;
	}

	private MealSSRInformation getMealSSRInformation(MealDynamic meal, SegmentInfo segmentInfo) {
		MealSSRInformation mealSSRInformation = new MealSSRInformation();
		PriceMiscInfo miscInfo = segmentInfo.getPriceInfo(0).getMiscInfo();
		miscInfo.setMealWayType(String.valueOf(meal.getWayType()));
		mealSSRInformation.setCode(meal.getCode());
		mealSSRInformation.setAmount((double) meal.getPrice());
		mealSSRInformation.setDesc(meal.getAirlineDescription());
		mealSSRInformation.getMiscInfo().setJourneyType(meal.getDescription());
		return mealSSRInformation;
	}

	public String getSSRURL() {
		String ssrURL = null;
		if (apiURLS == null) {
			ssrURL = StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[1], URL_SUFFIX);
		} else {
			ssrURL = apiURLS.getSsrURL();
		}
		log.debug("TBO SSR  URL {}", ssrURL);
		return ssrURL;
	}

	public SSRResponseBody getSeatMap(TripInfo tripInfo, String resultIndex) {
		tokenId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTokenId();
		traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();

		HttpUtils httpUtils = null;
		try {
			SSRRequestBody requestBody = new SSRRequestBody(IP, tokenId, traceId, resultIndex);
			httpUtils = HttpUtils.builder().urlString(getSSRURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(SSRResponseBody.class).orElse(null);
			if (Objects.nonNull(responseBody))
				return responseBody;
		} catch (Exception e) {
			log.error("Unable to fetch Seat Map for booking Id {}, trip {} ", bookingId, tripInfo.toString(), e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SeatRQ"),
					formatRQRS(httpUtils.getResponseString(), "SeatRS"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("Seat Map").build());
		}
		return null;
	}

	public List<SeatSSRInformation> createSeatMap(RowSeat[] rowSeat) {
		List<SeatSSRInformation> seatSSRInfo = null;
		if (Objects.nonNull(rowSeat)) {
			seatSSRInfo = new ArrayList<>();
			for (RowSeat row : rowSeat) {
				for (Seat seat : row.getSeats()) {
					if (!(seat.getCode().equalsIgnoreCase("NoSeat"))) {
						SeatSSRInformation seatInfo = new SeatSSRInformation();
						seatInfo.getMiscInfo().setSeatCodeType(seat.getSeatWayType().toString());
						seatInfo.setAmount(seat.getPrice().doubleValue());
						seatInfo.setCode(seat.getCode());
						seatInfo.setSeatNo(seat.getCode());
						seatInfo.getMiscInfo().setJourneyType(seat.getDescription());
						seatInfo.setIsBooked(isBooked(seat.getAvailablityType()));
						seatInfo.setIsAisle(isAisle(seat.getSeatType()));
						seatInfo.setSeatPosition(SeatPosition.builder().row(Integer.parseInt(seat.getRowNo()))
								.column(seat.getSeatNo().charAt(0) - 'A' + 1).build());
						seatSSRInfo.add(seatInfo);
					}
				}
			}
		}
		return seatSSRInfo;
	}

	private Boolean isAisle(Integer seatType) {
		/**
		 * seatType [Window = 1,Aisle = 2,Middle = 3]
		 **/
		return seatType == 2 ? true : false;
	}

	private Boolean isBooked(Integer availablityType) {
		/**
		 * availabilityType [Open = 1,Reserved = 3]
		 * 
		 **/
		return availablityType == 1 ? false : true;
	}

	public RowSeat[] getRowSeat(SSRResponseBody responseBody, String segKey) {
		if (Objects.nonNull(responseBody.getResponse())
				&& Objects.nonNull(responseBody.getResponse().getSeatDynamic())) {
			for (SeatDynamic seatDynamic : responseBody.getResponse().getSeatDynamic()) {
				for (SegmentSeat segmentSeat : seatDynamic.getSegmentSeat()) {
					for (RowSeat rowSeat : segmentSeat.getRowSeats()) {
						if (segKey.equalsIgnoreCase(
								createKey(rowSeat.getSeats()[0].getOrigin(), rowSeat.getSeats()[0].getDestination()))) {
							return segmentSeat.getRowSeats();
						} else {
							break;
						}
					}
				}
			}
		}
		return null;

	}

	private String createKey(String origin, String destination) {

		return StringUtils.join(origin, "_", destination);
	}

}
