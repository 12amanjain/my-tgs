package com.tgs.services.fms.sources.tbo;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.service.tbo.datamodel.Airline;
import com.tgs.service.tbo.datamodel.Fare;
import com.tgs.service.tbo.datamodel.FareBreakdown;
import com.tgs.service.tbo.datamodel.FareRule;
import com.tgs.service.tbo.datamodel.Result;
import com.tgs.service.tbo.datamodel.ResultSegment;
import com.tgs.service.tbo.datamodel.TaxBreakup;
import com.tgs.service.tbo.datamodel.search.Response;
import com.tgs.service.tbo.datamodel.search.SearchRequestBody;
import com.tgs.service.tbo.datamodel.search.SearchRequestSegment;
import com.tgs.service.tbo.datamodel.search.SearchResponseBody;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripInfoType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class TravelBoutiqueSearchManager extends TravelBoutiqueServiceManager {

	private static final String URL_SUFFIX = "/AirAPI_V10/AirService.svc/rest/Search";

	private @Getter SearchResponseBody responseBody;
	AirSearchResult airSearchResult;
	private TravelBoutiqueCabinClass cabinClass;

	@Getter
	protected boolean isNewSearchRequired;

	private static String GDS_CODE = "GDS";

	public AirSearchResult doSearch() throws SupplierRemoteException, NoSearchResultException {
		HttpUtils httpUtils = null;
		try {
			isNewSearchRequired = false;
			SearchRequestBody requestBody = generateRequestBody();
			airSearchResult = new AirSearchResult();
			if (requestBody == null) {
				throw new NoSearchResultException(AirSourceConstants.NO_SEARCH_RESULT);
			}

			httpUtils = HttpUtils.builder().urlString(getSearchURL()).postData(GsonUtils.getGson().toJson(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			responseBody = httpUtils.getResponse(SearchResponseBody.class).orElse(null);
			traceId = responseBody.getResponse().getTraceId();
			if (!checkIsSessionExpired(responseBody)) {
				parseSearchResponse();
			}
			return airSearchResult;
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(formatRQRS(httpUtils.getPostData(), "SearchRQ"),
					formatRQRS(httpUtils.getResponseString(), "SearchRS"));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
					.type(AirUtils.getLogType("2-S-Search", supplierConfiguration)).build());
		}
	}

	private boolean checkIsSessionExpired(SearchResponseBody responseBody) {
		if (responseBody != null) {
			Response response = responseBody.getResponse();
			if (response != null && response.getError() != null && response.getResponseStatus() == 3
					&& (StringUtils.isNotBlank(response.getError().getErrorMessage())
							&& (response.getError().getErrorMessage().toUpperCase().contains("INVALID")))) {
				isNewSearchRequired = true;
			}
		}
		return isNewSearchRequired;
	}

	public SearchRequestBody generateRequestBody() {
		SearchRequestSegment[] segmentArray = new SearchRequestSegment[searchQuery.getRouteInfos().size()];
		int segmentIndex = 0;
		for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
			cabinClass = TravelBoutiqueCabinClass.valueOf(searchQuery.getCabinClass().name());
			int cabinClassVal = cabinClass.getValue();
			segmentArray[segmentIndex++] = new SearchRequestSegment(routeInfo.getTravelDate().atStartOfDay(),
					routeInfo.getTravelDate().atStartOfDay(), cabinClassVal, routeInfo.getFromCityOrAirport().getCode(),
					routeInfo.getToCityOrAirport().getCode());
		}
		SearchRequestBody searchRequestBody = new SearchRequestBody(
				TravelBoutiqueSearchType.valueOf(searchQuery.getSearchType().name()).getValue(), tokenId,
				searchQuery.getPaxInfo().get(PaxType.ADULT), searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0),
				searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0), false, false, segmentArray, null, null, IP);
		setSourceAndPrefferedAirline(searchRequestBody);
		return searchRequestBody;
	}

	private void setSourceAndPrefferedAirline(SearchRequestBody searchRequestBody) {
		// case To set GDS Airlines
		if (StringUtils.isNotBlank(searchQuery.getPrefferedAirline())) {
			List<String> airlines = new ArrayList<>();
			searchQuery.getPreferredAirline().forEach(airlineInfo -> {
				if (!airlineInfo.getIsLcc()) {
					airlines.add(airlineInfo.getCode());
				}
			});
			if (CollectionUtils.isNotEmpty(airlines)) {
				searchRequestBody.setPreferredAirlines(new String[] {});
				searchRequestBody.setPreferredAirlines(airlines.toArray(new String[airlines.size()]));
			}
		}
		// case To set LCC Airlines
		if (StringUtils.isNotBlank(searchQuery.getPrefferedAirline())) {
			List<String> sources = new ArrayList<>();
			searchQuery.getPreferredAirline().forEach(airlineInfo -> {
				if (airlineInfo.getIsLcc()) {
					sources.add(airlineInfo.getCode());
				} else if (!sources.contains(GDS_CODE)) {
					sources.add(GDS_CODE);
				}
			});
			if (CollectionUtils.isNotEmpty(sources)) {
				// searchRequestBody.setSources(new String[]{});
				// searchRequestBody.setSources(sources.toArray(new String[sources.size()]));
			}
		}
	}

	private void parseSearchResponse() {
		Response response = responseBody.getResponse();
		if (response == null) {
			return;
		}

		Result[][] results = response.getResults();

		if (results == null || results.length == 0
				|| isCriticalException(response.getError(), String.valueOf(response.getResponseStatus()))) {
			throw new NoSearchResultException();
		}

		if (results.length == 2) {
			populateTripInfo(results[0], TripInfoType.ONWARD);
			populateTripInfo(results[1], TripInfoType.RETURN);
		} else {
			populateTripInfo(results[0], searchQuery.getTripType());
		}
	}

	public void populateTripInfo(Result[] resultList, TripInfoType tripInfoType) {
		for (Result result : resultList) {
			TripInfo tripInfo = new TripInfo();
			try {
				List<SegmentInfo> segments = tripInfo.getSegmentInfos();
				for (ResultSegment[] resultSegmentList : result.getSegments()) {
					int segmentNum = 0;
					for (ResultSegment resultSegment : resultSegmentList) {
						// length of the segment is the length of the farerules
						SegmentInfo segmentInfo = populateSegmentInfo(resultSegment);
						segmentInfo.setIsReturnSegment(resultSegment.getTripIndicator() == 2 && segmentNum == 0);
						populatePriceInfo(result, resultSegment, segmentInfo, segmentNum == 0);
						segmentInfo.setSegmentNum(segmentNum++);
						String resultIndex = result.getResultIndex();
						segmentInfo.getPriceInfoList().forEach(priceInfo -> {
							PriceMiscInfo miscInfo = priceInfo.getMiscInfo();
							miscInfo.setFareKey(resultIndex);
							miscInfo.setReviewTimeLimit(LocalDateTime.now());
							miscInfo.setTraceId(traceId);
							miscInfo.setTokenId(tokenId);
							priceInfo.setMiscInfo(miscInfo);
						});
						segments.add(segmentInfo);
					}
				}
				airSearchResult.addTripInfo(tripInfoType.getTripType(), tripInfo);
			} catch (Exception e) {
				log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
			}
		}

	}

	/***
	 * @implSpec : isFirstSegmentInfo & !BooleanUtils.isTrue(segmentInfo.getIsReturnSegment()) to handle for Intl Fares
	 *           As it Only one price fare from TBO side.
	 */
	private void populatePriceInfo(Result result, ResultSegment resultSegment, SegmentInfo segmentInfo,
			boolean isFirstSegmentInfo) {
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
		for (FareBreakdown fareBreakdown : result.getFareBreakdown()) {
			FareDetail fareDetail = createFareDetail(fareBreakdown, resultSegment, result);
			if (isFirstSegmentInfo && fareDetail != null && !BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
				addFareComponents(result, fareDetail, fareBreakdown, result.getFare());
			} else {
				fareDetail.setFareComponents(getDefaultComponents());
			}
			fareDetails.put(PaxType.values()[fareBreakdown.getPassengerType() - 1], fareDetail);
		}
		if (StringUtils.isNotBlank(result.getAirlineRemark())) {
			priceInfo.setFareIdentifier(TravelBoutiqueAirUtils.getFareType(result.getAirlineRemark().toUpperCase()));
		} else {
			priceInfo.setFareIdentifier(FareType.PUBLISHED);
		}

		priceInfoList.add(priceInfo);
	}

	private void addFareComponents(Result result, FareDetail fareDetail, FareBreakdown fareBreakDown, Fare fare) {
		Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
		// This BF is for all the pax of a particular type
		Double basefare = (fareBreakDown.getBaseFare()) / (fareBreakDown.getPassengerCount());
		double tax = (fareBreakDown.getTax()) / (fareBreakDown.getPassengerCount());
		fareComponents.put(FareComponent.BF, basefare);

		fareComponents.put(FareComponent.SC, getSupplierCommission(fare));


		/**
		 * @implNote : YQFare is part of Tax on TBO Side , as Bifurcating on Our Side and so on We have reduce from tax
		 *           as well
		 */

		double yqFare = (fareBreakDown.getYQTax()) / (fareBreakDown.getPassengerCount());
		fareComponents.put(FareComponent.YQ, yqFare);

		/**
		 * @implNote : OtherCharges which includes : MarkUp, Other Charges,Convenience charge on TBO Side and applicable
		 *           for all pax
		 */

		int paxCount = AirUtils.getPaxCount(searchQuery, true);
		double extraCharges = result.getFare().getOtherCharges() / paxCount;
		fareComponents.put(FareComponent.AT, tax - yqFare);
		fareComponents.put(FareComponent.OC, extraCharges);
		fareComponents.put(FareComponent.SMF, 0d);

		/**
		 * @implNote : Tax which inlcudes total tax from TBO side has(YQ as well)
		 *
		 */
		fareComponents.put(FareComponent.TF, basefare + (tax - yqFare) + yqFare + extraCharges);


		/**
		 * We are not getting GST information Passenger wise , hence we need to divide total among all passengers
		 */
		for (TaxBreakup tb : result.getFare().getTaxBreakup()) {
			if (tb.getKey().toUpperCase().equals(TravelBoutiqueFareComponentMapper.K3.name())) {
				double value = tb.getValue() / paxCount;
				fareComponents.put(FareComponent.AGST, value);
				// for componenets which are already added in tax
				if (value > 0) {
					fareComponents.put(FareComponent.AT, fareComponents.getOrDefault(FareComponent.AT, 0.0) - value);
				}
			}
		}
	}

	private Double getSupplierCommission(Fare fare) {
		Double supplierCommission = 0.0;
		supplierCommission += (fare.getCommissionEarned() + fare.getPLBEarned() + fare.getIncentiveEarned());
		return (supplierCommission / AirUtils.getPaxCount(searchQuery, true));
	}

	private FareDetail createFareDetail(FareBreakdown fareBreakdown, ResultSegment resultSegment, Result result) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.setCabinClass(CabinClass.valueOf(cabinClass.name()));

		fareDetail.setBaggageInfo(createBaggageInfo(resultSegment));
		fareDetail.setSeatRemaining(resultSegment.getNoOfSeatAvailable());
		fareDetail.setRefundableType(result.isRefundable() ? RefundableType.REFUNDABLE.getRefundableType()
				: RefundableType.NON_REFUNDABLE.getRefundableType());
		fareDetail.setClassOfBooking(resultSegment.getAirline().getFareClass());
		fareDetail.setIsMealIncluded(result.getFare().getTotalMealCharges() != 0 ? true : null);

		FareRule[] fr = result.getFareRules();
		if (fr.length > 0) {
			fareDetail.setFareBasis(fr[0].getFareBasisCode());
		}

		String airlineRemark = result.getAirlineRemark();
		if (airlineRemark != null)
			fareDetail.setIsHandBaggage(airlineRemark.contains("hand bag"));

		return fareDetail;
	}

	private BaggageInfo createBaggageInfo(ResultSegment resultSegment) {
		BaggageInfo bgInfo = new BaggageInfo();
		bgInfo.setAllowance(resultSegment.getBaggage());
		bgInfo.setCabinBaggage(resultSegment.getCabinBaggage());
		return bgInfo;

	}

	private SegmentInfo populateSegmentInfo(ResultSegment resultSegment) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setFlightDesignator(createFlightDesignator(resultSegment.getAirline()));
		AirlineInfo operatingAirline = AirlineHelper.getAirlineInfo(resultSegment.getAirline().getOperatingCarrier());
		if (operatingAirline != null
				&& !operatingAirline.getCode().equals(segmentInfo.getFlightDesignator().getAirlineCode())) {
			segmentInfo.setOperatedByAirlineInfo(operatingAirline);
		}
		boolean stopOver = resultSegment.getStopOver();
		segmentInfo.setStops(stopOver ? 1 : 0);
		if (StringUtils.isNotBlank(resultSegment.getStopPoint())) {
			AirportInfo stopOverAirport = AirportHelper.getAirport(resultSegment.getStopPoint());
			if (stopOverAirport != null) {
				segmentInfo.setStopOverAirports(Arrays.asList(stopOverAirport));
			}
		}
		long duration = resultSegment.getDuration() != null ? resultSegment.getDuration() : 0;
		segmentInfo.setDepartTime(resultSegment.getOrigin().getDepTime());
		segmentInfo.setArrivalTime(resultSegment.getDestination().getArrTime());
		segmentInfo.setDepartAirportInfo(
				AirportHelper.getAirport(resultSegment.getOrigin().getAirport().getAirportCode()));
		segmentInfo.setArrivalAirportInfo(
				AirportHelper.getAirport(resultSegment.getDestination().getAirport().getAirportCode()));
		segmentInfo.getDepartAirportInfo().setTerminal(resultSegment.getOrigin().getAirport().getTerminal());
		segmentInfo.getArrivalAirportInfo().setTerminal(resultSegment.getDestination().getAirport().getTerminal());
		if (duration == 0) {
			duration = segmentInfo.calculateDuration();
		}
		segmentInfo.setDuration(duration);
		return segmentInfo;
	}

	private FlightDesignator createFlightDesignator(Airline airline) {
		FlightDesignator flightDesign = new FlightDesignator();
		flightDesign.setAirlineInfo(AirlineHelper.getAirlineInfo(airline.getAirlineCode()));
		flightDesign.setFlightNumber(airline.getFlightNumber());
		return flightDesign;
	}


	public String getSearchURL() {
		String searchURL = null;
		if (apiURLS == null) {
			searchURL =
					StringUtils.join(supplierConfiguration.getSupplierCredential().getUrl().split(",")[1], URL_SUFFIX);
		} else {
			searchURL = apiURLS.getTravelAgent();
		}
		log.debug("TBO Search URL {}", searchURL);
		return searchURL;
	}
}
