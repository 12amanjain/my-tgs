package com.tgs.services.fms.sources.tbo;

import lombok.Getter;

@Getter
public enum TravelBoutiqueSearchType {

	ONEWAY(1), RETURN(2), MULTICITY(3);

	private int value;

	private TravelBoutiqueSearchType(int value) {
		this.value = value;
	}
}
