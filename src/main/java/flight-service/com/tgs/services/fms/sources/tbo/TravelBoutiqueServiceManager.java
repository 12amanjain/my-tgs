package com.tgs.services.fms.sources.tbo;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.service.tbo.datamodel.Error;
import com.tgs.service.tbo.datamodel.Segments;
import com.tgs.service.tbo.datamodel.TaxBreakup;
import com.tgs.service.tbo.datamodel.confirmbook.FlightItinerary;
import com.tgs.service.tbo.datamodel.review.Fare;
import com.tgs.service.tbo.datamodel.review.FareBreakdown;
import com.tgs.service.tbo.datamodel.review.FareQuoteSegment;
import com.tgs.service.tbo.datamodel.review.Results;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Getter
@Setter
@NoArgsConstructor
abstract class TravelBoutiqueServiceManager {

	public static final String IP = "192.168.3.4";
	protected static final int MALE = 1, FEMALE = 2;

	protected SupplierConfiguration supplierConfiguration;
	protected AirSearchQuery searchQuery;
	protected String traceId, tokenId, bookingId;
	protected String pnr;
	protected User bookingUser;

	protected RestAPIListener listener;

	protected FlightAPIURLRuleCriteria apiURLS;

	protected List<String> criticalMessageLogger;

	protected DeliveryInfo deliveryInfo;

	public Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		return headerParams;
	}


	protected int getGenderFromTitle(String title) {
		title = title.toLowerCase();
		if (title.startsWith("mrs") || title.startsWith("miss"))
			return FEMALE;
		return MALE;
	}


	protected boolean isCriticalException(Error error, String responseStatus) {
		boolean isError = false;
		if (error != null) {
			/**
			 * @implSpec : All response will be having Error Msg if 0 | 1 then it success response else something gone
			 *           wrong
			 */
			if (!"1".equals(responseStatus) && error.getErrorCode() != 0) {
				isError = true;
				log.info("Error Occured for booking id {} and error message {}", bookingId, error.getErrorMessage());
				logCriticalMessage(error.getErrorMessage());
			}
		}
		return isError;
	}

	protected String getErrorMessage(Error error) {
		String errorMsg = null;
		if (error != null) {
			errorMsg = error.getErrorMessage();
		}
		return errorMsg;
	}

	public void updateFareDetail(FareDetail fareDetail, FareBreakdown fareBreakDown, FareQuoteSegment resultSegment,
			Results result, Fare fare) throws Exception {
		try {
			Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
			Double basefare = (fareBreakDown.getBaseFare()) / (fareBreakDown.getPassengerCount());
			double tax = (fareBreakDown.getTax()) / (fareBreakDown.getPassengerCount());
			fareComponents.put(FareComponent.BF, basefare);

			fareComponents.put(FareComponent.SC, getSupplierCommission(fare));

			/**
			 * @implNote : YQFare is part of Tax on TBO Side , as Bifurcating on Our Side and so on We have reduce from
			 *           tax as well
			 */

			double yQFare = (fareBreakDown.getYQTax()) / (fareBreakDown.getPassengerCount());
			fareComponents.put(FareComponent.YQ, yQFare);

			/**
			 * @implNote : OtherCharges which includes : MarkUp, Other Charges,Convenience charge on TBO Side and
			 *           applicable for all pax
			 */

			int paxCount = AirUtils.getPaxCount(searchQuery, true);
			double extraCharges = +result.getFare().getOtherCharges() / paxCount;

			fareComponents.put(FareComponent.AT, tax - yQFare);
			fareComponents.put(FareComponent.OC, extraCharges);
			fareComponents.put(FareComponent.SMF, extraCharges);

			/**
			 * @implNote : Tax which inlcudes total tax from TBO side has(YQ as well)
			 *
			 */
			fareComponents.put(FareComponent.TF, basefare + (tax - yQFare) + yQFare + extraCharges);

			/**
			 * We are not getting GST information Passenger wise , hence we need to divide total among all passengers
			 */
			for (TaxBreakup tb : result.getFare().getTaxBreakup()) {
				if (tb.getKey().toUpperCase().equals(TravelBoutiqueFareComponentMapper.K3.name())) {
					double value = tb.getValue() / paxCount;
					fareComponents.put(FareComponent.AGST, value);
					// for componenets which are already added in tax
					if (value > 0) {
						fareComponents.put(FareComponent.AT,
								fareComponents.getOrDefault(FareComponent.AT, 0.0) - value);
					}
				}
			}
			fareDetail.setBaggageInfo(createBaggageInfo(resultSegment));
			if (resultSegment.getNoOfSeatAvailable() != null)
				fareDetail.setSeatRemaining(resultSegment.getNoOfSeatAvailable());
			fareDetail.setRefundableType(result.getIsRefundable() ? RefundableType.REFUNDABLE.getRefundableType()
					: RefundableType.NON_REFUNDABLE.getRefundableType());
			fareDetail.setClassOfBooking(resultSegment.getAirline().getFareClass());
			fareDetail.setIsMealIncluded(result.getFare().getTotalMealCharges() != 0 ? true : null);
		} catch (Exception e) {
			log.error("Unable to parse fare-details for result-segment {} ", bookingId, e);
		}
	}

	private Double getSupplierCommission(Fare fare) {
		Double supplierCommission = 0.0;
		supplierCommission += fare.getCommissionEarned() + fare.getPLBEarned() + fare.getIncentiveEarned();
		return (supplierCommission / AirUtils.getPaxCount(searchQuery, true));
	}


	private BaggageInfo createBaggageInfo(FareQuoteSegment segment) throws Exception {
		BaggageInfo bgInfo = new BaggageInfo();
		try {
			bgInfo.setAllowance(segment.getBaggage());
			bgInfo.setCabinBaggage(segment.getCabinBaggage());
		} catch (Exception e) {
			log.error("Unable to parse baggage-info for farequote-segment booking id {} ", bookingId, e);
		}
		return bgInfo;
	}


	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}


	protected void logCriticalMessage(String message) {
		if (criticalMessageLogger != null) {
			criticalMessageLogger.add(message);
		}
	}

	public Map<FareComponent, Double> getDefaultComponents() {
		Map<FareComponent, Double> defaultComponents = new HashMap<>();
		defaultComponents.put(FareComponent.BF, new Double(0));
		defaultComponents.put(FareComponent.AT, new Double(0));
		defaultComponents.put(FareComponent.TF, new Double(0));
		return defaultComponents;
	}

	protected void updateBookingPnr(FlightItinerary flightItinerary, List<SegmentInfo> segmentInfos) {
		if (airLinePnrPresent(flightItinerary.getSegments())) {
			setReservetionPNRForSegment(segmentInfos, flightItinerary.getPNR());
			segmentInfos.forEach(segmentInfo -> {
				segmentInfo.updateAirlinePnr(getAirlinePnr(segmentInfo, flightItinerary.getSegments()));
			});
		}
	}

	protected boolean airLinePnrPresent(Segments[] segments) {
		if (!ObjectUtils.isEmpty(segments)) {
			for (Segments segment : segments) {
				if (StringUtils.isNotBlank(segment.getAirlinePNR())) {
					return true;
				}
			}
		}
		return false;
	}

	protected String getAirlinePnr(SegmentInfo segmentInfo, Segments[] segments) {
		String segmentInfoTripKey =
				segmentInfo.getDepartureAndArrivalAirport().concat("-").concat(segmentInfo.getFlightNumber());
		for (Segments segment : segments) {
			String departCode = segment.getOrigin().getAirport().getAirportCode();
			String arrivalCode = segment.getDestination().getAirport().getAirportCode();
			String flightNumber = segment.getAirline().getFlightNumber();
			String segmentTripKey = departCode.concat("-").concat(arrivalCode).concat("-").concat(flightNumber);
			if (segmentInfoTripKey.equalsIgnoreCase(segmentTripKey)
					&& StringUtils.isNotBlank(segment.getAirlinePNR())) {
				return segment.getAirlinePNR();
			}
		}
		return null;
	}

	protected void setReservetionPNRForSegment(List<SegmentInfo> segmentInfos, String reservationPNR) {
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.updateReservationPnr(reservationPNR);
		});
	}

}
