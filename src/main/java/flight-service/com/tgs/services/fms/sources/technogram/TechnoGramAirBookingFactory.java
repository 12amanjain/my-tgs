package com.tgs.services.fms.sources.technogram;

import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;

@Service
public class TechnoGramAirBookingFactory extends AbstractAirBookingFactory {

	private FlightAPIURLRuleCriteria apiURLS;

	private RestAPIListener apiListener;

	private String supplierRefId;

	public TechnoGramAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
		apiListener = new RestAPIListener("");
		apiURLS = AirUtils.getAirEndPointURL(sourceConfiguration);
		this.supplierRefId = ObjectUtils.firstNonNull(
				bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getProviderCode(),
				bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0)
						.getSupplierBookingId());
	}

	@Override
	public boolean doBooking() {
		boolean isSuccess = false;

		// This will remove the supplierRefId from provider code.
		unsetProviderCode(bookingSegments);
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(bookingId).build();
		TechnoGramAirBookingManager bookingManager = TechnoGramAirBookingManager.builder()
				.bookingSegments(bookingSegments).supplierConfiguration(supplierConf).referenceId(bookingId)
				.apiURLS(apiURLS).criticalMessageLogger(criticalMessageLogger).supplierRefId(supplierRefId)
				.bindingService(bindingService).apiListener(apiListener)
				.airlineCharges(BookingUtils.getAmountChargedByAirline(bookingSegments.getSegmentInfos()))
				.bookingUser(bookingUser).build();
		BookingUtils.updateSupplierBookingId(bookingSegments.getSegmentInfos(), supplierRefId);
		isSuccess = bookingManager.book(gstInfo, isHoldBooking, order.getDeliveryInfo());
		if (isSuccess) {
			pnr = bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
			if (isHoldBooking) {
				updateTimeLimit(TgsDateUtils.getCalendar(bookingManager.holdTimeLimit));
			}
		}
		isTkRequired = false;
		return isSuccess;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isSuccess = false;
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(bookingId).build();
		TechnoGramAirBookingManager bookingManager = TechnoGramAirBookingManager.builder()
				.bookingSegments(bookingSegments).supplierConfiguration(supplierConf).referenceId(bookingId)
				.apiURLS(apiURLS).supplierRefId(supplierRefId).criticalMessageLogger(criticalMessageLogger)
				.apiListener(apiListener).bindingService(bindingService)
				.airlineCharges(BookingUtils.getAmountChargedByAirline(bookingSegments.getSegmentInfos()))
				.isConfirmBooking(true).bookingUser(bookingUser).build();
		double amountToBePaidToAirline = bookingManager.getAmountToBePaidToAirline();
		if (!isFareDiff(amountToBePaidToAirline)) {
			isSuccess = bookingManager.book(gstInfo, false, order.getDeliveryInfo());
		}
		return isSuccess;
	}

	@Override
	public boolean confirmBeforeTicket() {
		boolean isFareAvailable = false;
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(bookingId).build();
		TechnoGramAirBookingManager bookingManager = TechnoGramAirBookingManager.builder()
				.bindingService(bindingService).bookingSegments(bookingSegments).supplierConfiguration(supplierConf)
				.referenceId(bookingId).apiURLS(apiURLS).supplierRefId(supplierRefId)
				.criticalMessageLogger(criticalMessageLogger).apiListener(apiListener).bookingUser(bookingUser).build();
		BookingResponse fareValidateResponse = bookingManager.getFareValidateResponse();
		if (bookingManager.isSuccessfulResponse(fareValidateResponse, true)) {
			isFareAvailable = true;
		}
		return isFareAvailable;
	}

	private void unsetProviderCode(BookingSegments bookingSegments) {
		for (SegmentInfo segmentInfo : bookingSegments.getSegmentInfos()) {
			for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
				priceInfo.getMiscInfo().setProviderCode(null);
			}
		}
	}

}
