package com.tgs.services.fms.sources.technogram;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.StopWatch;
import com.google.gson.TypeAdapterFactory;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.RuntimeTypeAdapterFactory;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderDetails;
import com.tgs.services.pms.datamodel.PaymentMode;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class TechnoGramAirBookingManager extends TechnoGramServiceManager {

	protected boolean isConfirmBooking;

	protected BookingSegments bookingSegments;

	protected String supplierRefId;

	protected double airlineCharges;

	protected LocalDateTime holdTimeLimit;

	public boolean book(GstInfo gstInfo, boolean isHoldBooking, DeliveryInfo deliveryInfo) {
		boolean isSuccess = false;
		try {
			AirBookingRequest bookingRequest = getBookingRequest(isHoldBooking, gstInfo, deliveryInfo);
			BookingResponse bookingResponse = getResponseByRequest(GsonUtils.getGson().toJson(bookingRequest), "Book",
					bindingService.getBookingURL(isConfirmBooking), BookingResponse.class);
			if (isSuccessfulResponse(bookingResponse, true)) {
				BookingDetailResponse retrieveResponse = retrieveBooking(supplierRefId);
				if (retrieveResponse == null) {
					return isSuccess;
				}
				OrderStatus bookingStatus = TechnoGramAirUtils.getBookingStatus(retrieveResponse);
				if ((isHoldBooking && OrderStatus.ON_HOLD.equals(bookingStatus))
						|| (!isHoldBooking && OrderStatus.SUCCESS.equals(bookingStatus))) {
					isSuccess = true;
					TechnoGramAirUtils.updatePNRandTicketNumber(retrieveResponse, bookingSegments);
					if (isHoldBooking) {
						AirOrderDetails OrderDetails =
								(AirOrderDetails) retrieveResponse.getItemInfos().get(OrderType.AIR.name());
						holdTimeLimit = OrderDetails.getTimeLimit();
					}
				}
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e.getMessage());
		}
		return isSuccess;
	}

	public PaymentMode retrievePaymentMode() {
		PaymentMode paymentMode = null;
		PaymentModeRequest paymentModeRequest = getPaymentModeRequest();
		try {
			PaymentModeResponse paymentModeResponse =
					getResponseByRequest(GsonUtils.getGson().toJson(paymentModeRequest), "PaymentMode",
							bindingService.getPaymentModeURL(), PaymentModeResponse.class);
			if (isSuccessfulResponse(paymentModeResponse, true)) {
				paymentMode = parsePaymentModeResponse(paymentModeResponse, airlineCharges);
				if (paymentMode == null) {
					addToCritcalMessage("Payment Mode not available for the booking");
				}
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e.getMessage());
		}
		return paymentMode;
	}

	private PaymentMode parsePaymentModeResponse(PaymentModeResponse paymentModeResponse, Double airlineCharges) {
		if (CollectionUtils.isNotEmpty(paymentModeResponse.getPaymentModes())) {
			List<PaymentMode> credits = new ArrayList<>();
			List<PaymentMode> wallets = new ArrayList<>();
			for (PaymentMode mode : paymentModeResponse.getPaymentModes()) {
				if (PaymentMedium.CREDIT.equals(mode.getMode())) {
					credits.add(mode);
				} else if (PaymentMedium.WALLET.equals(mode.getMode())) {
					wallets.add(mode);
				}
			}
			if (CollectionUtils.isNotEmpty(wallets)) {
				return wallets.get(0);
			}
			if (CollectionUtils.isNotEmpty(credits)) {
				return credits.get(0);
			}
		}
		return null;
	}

	private PaymentModeRequest getPaymentModeRequest() {
		double totalAmount = airlineCharges;
		PaymentModeRequest paymentModeRequest = PaymentModeRequest.builder().considerWalletBalance(true)
				.amount(BigDecimal.valueOf(totalAmount)).bookingId(supplierRefId).build();
		return paymentModeRequest;
	}

	public BookingDetailResponse retrieveBooking(String supplierRefId) {
		BookingDetailRequest retrieveRequest = BookingDetailRequest.builder().bookingId(supplierRefId).build();
		BookingDetailResponse retrieveResponse = null;
		boolean isRetry = false;
		do {
			try {
				retrieveResponse =
						processRetrieveResponse(getResponseByRequest(GsonUtils.getGson().toJson(retrieveRequest),
								"Retrieve Booking", bindingService.getRetrieveBookingURL(), null));
				StopWatch timer = new StopWatch();
				timer.start();
				if (isSuccessfulResponse(retrieveResponse, true)) {
					long retryInSeconds = getRetrySeconds(retrieveResponse);
					if (isRetryRequired(retryInSeconds, timer, TechnoGramAirUtils.bookingDetailTTO())) {
						Thread.sleep(retryInSeconds * 1000);
						isRetry = true;
					} else {
						isRetry = false;
					}
				}
			} catch (IOException | InterruptedException e) {
				throw new SupplierRemoteException(e.getMessage());
			}
		} while (isRetry);
		return retrieveResponse;
	}

	@SuppressWarnings("unchecked")
	private BookingDetailResponse processRetrieveResponse(String responseString) {
		List<TypeAdapterFactory> typeAdapters = new ArrayList<TypeAdapterFactory>();
		RuntimeTypeAdapterFactory<ItemDetails> itemAdapter =
				RuntimeTypeAdapterFactory.of(ItemDetails.class, "itemInfos");
		itemAdapter.registerSubtype(AirOrderDetails.class);
		typeAdapters.add(itemAdapter);
		GsonUtils gsonUtil = GsonUtils.builder().typeFactories(typeAdapters).build();
		BookingDetailResponse bookingDetailResponse =
				gsonUtil.buildGson().fromJson(responseString, BookingDetailResponse.class);
		return bookingDetailResponse;
	}

	private AirBookingRequest getBookingRequest(boolean isHoldBook, GstInfo gstInfo, DeliveryInfo deliveryInfo) {
		AirBookingRequest bookingRequest = new AirBookingRequest();
		bookingRequest.setTravellerInfo(getTravellerInfos());
		if (gstInfo != null && StringUtils.isNotEmpty(gstInfo.getGstNumber())) {
			bookingRequest.setGstInfo(getGSTInfo(gstInfo));
		}
		bookingRequest.setDeliveryInfo(deliveryInfo);
		bookingRequest.setBookingId(supplierRefId);

		if (!isHoldBook) {
			List<PaymentRequest> paymentInfos = new ArrayList<>();
			paymentInfos.add(PaymentRequest.builder()
					.amount(BigDecimal.valueOf(getAmountBasedOnCurrency(airlineCharges, true))).build());
			bookingRequest.setPaymentInfos(paymentInfos);
		}
		return bookingRequest;
	}

	private GstInfo getGSTInfo(GstInfo gstInfo) {
		GstInfo supplierGSTInfo = new GsonMapper<>(gstInfo, null, GstInfo.class).convert();
		supplierGSTInfo.setBookingId(supplierRefId);
		supplierGSTInfo.setBookingUserId(
				ObjectUtils.firstNonNull(supplierConfiguration.getSupplierCredential().getUserName(), ""));
		return supplierGSTInfo;
	}

	/*
	 * private DeliveryInfo getDeliveryInfo() { ClientGeneralInfo clientInfo =
	 * ServiceCommunicatorHelper.getClientInfo(); DeliveryInfo deliveryInfo = new DeliveryInfo(); List<String> email =
	 * new ArrayList<>(); email.add(clientInfo.getEmail()); deliveryInfo.setEmails(email); List<String> contact = new
	 * ArrayList<>(); contact.add(ObjectUtils.firstNonNull(clientInfo.getHomePhone(), clientInfo.getWorkPhone()));
	 * deliveryInfo.setContacts(contact); return deliveryInfo; }
	 */

	private List<FlightTravellerInfo> getTravellerInfos() {
		List<FlightTravellerInfo> travellerInfos = new ArrayList<FlightTravellerInfo>();
		for (FlightTravellerInfo traveller : bookingSegments.getSegmentInfos().get(0).getBookingRelatedInfo()
				.getTravellerInfo()) {
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			travellerInfo.setFirstName(traveller.getFirstName());
			travellerInfo.setLastName(traveller.getLastName());
			travellerInfo.setDob(traveller.getDob());
			travellerInfo.setFrequentFlierMap(traveller.getFrequentFlierMap());
			travellerInfo.setExpiryDate(traveller.getExpiryDate());
			travellerInfo.setPassportIssueDate(traveller.getPassportIssueDate());
			travellerInfo.setPassportNationality(traveller.getPassportNationality());
			travellerInfo.setPassportNumber(traveller.getPassportNumber());
			travellerInfo.setPaxType(traveller.getPaxType());
			travellerInfo.setTitle(traveller.getTitle());
			travellerInfos.add(travellerInfo);
		}
		setSegmentSSR(travellerInfos);
		return travellerInfos;
	}

	private void setSegmentSSR(List<FlightTravellerInfo> travellerInfos) {
		for (FlightTravellerInfo traveller : travellerInfos) {
			for (SegmentInfo segment : bookingSegments.getSegmentInfos()) {

				FlightTravellerInfo segmentTraveller = getSegmentTraveller(traveller, segment);
				SSRInformation meal = segmentTraveller.getSsrMealInfo();
				SSRInformation baggage = segmentTraveller.getSsrBaggageInfo();
				SSRInformation seat = segmentTraveller.getSsrSeatInfo();
				List<SSRInformation> extraServices = segmentTraveller.getExtraServices();
				String segmentKey = segment.getPriceInfo(0).getMiscInfo().getSegmentKey();
				// meal
				if (meal != null && StringUtils.isNotEmpty(meal.getCode())) {
					traveller.getSsrMealInfos().add(getSSRInfo(meal.getCode(), segmentKey));
				}

				// baggage
				if (baggage != null && StringUtils.isNotEmpty(baggage.getCode())) {
					traveller.getSsrBaggageInfos().add(getSSRInfo(baggage.getCode(), segmentKey));
				}

				// seat
				if (seat != null && StringUtils.isNotEmpty(seat.getCode())) {
					traveller.getSsrSeatInfos().add(getSSRInfo(seat.getCode(), segmentKey));
				}

				// extra services
				if (CollectionUtils.isNotEmpty(extraServices)) {
					for (SSRInformation extraService : extraServices) {
						if (StringUtils.isNotEmpty(extraService.getCode())) {
							traveller.getSsrExtraServiceInfos().add(getSSRInfo(extraService.getCode(), segmentKey));
						}
					}
				}
			}
		}
	}

	private SSRInformation getSSRInfo(String code, String segmentKey) {
		SSRInformation ssrInfo = new SSRInformation();
		ssrInfo.setCode(code);
		ssrInfo.setKey(segmentKey);
		return ssrInfo;
	}

	private FlightTravellerInfo getSegmentTraveller(FlightTravellerInfo traveller, SegmentInfo segment) {
		for (FlightTravellerInfo segmentTraveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
			if (StringUtils.equalsIgnoreCase(segmentTraveller.getFirstName(), traveller.getFirstName())
					&& StringUtils.equalsIgnoreCase(segmentTraveller.getLastName(), traveller.getLastName())) {
				return segmentTraveller;
			}
		}
		return null;
	}

	public double getAmountToBePaidToAirline() {
		double amountToBePaidToAirline = 0.0;
		BookingDetailResponse retrieveResponse = retrieveBooking(supplierRefId);
		if (retrieveResponse != null && retrieveResponse.getOrder() != null) {
			if (!OrderStatus.ON_HOLD.equals(TechnoGramAirUtils.getBookingStatus(retrieveResponse))) {
				addToCritcalMessage("Invalid Booking status for confirmation.");
				return amountToBePaidToAirline;
			}
			amountToBePaidToAirline = getAmountBasedOnCurrency(retrieveResponse.getOrder().getAmount()
					+ ObjectUtils.firstNonNull(retrieveResponse.getOrder().getMarkup(), 0.0), false);
		}
		return amountToBePaidToAirline;
	}


	public BookingResponse getFareValidateResponse() {
		BookingRequest fareValidateRequest = new BookingRequest();
		fareValidateRequest.setBookingId(supplierRefId);
		BookingResponse fareValidateResponse = null;
		try {
			fareValidateResponse =
					processFareValidateResponse(getResponseByRequest(GsonUtils.getGson().toJson(fareValidateRequest),
							"Fare-Validate", bindingService.getFareValidateURL(), null));
		} catch (IOException e) {
			throw new SupplierRemoteException(e.getMessage());
		}
		return fareValidateResponse;

	}

	private BookingResponse processFareValidateResponse(String responseString) {
		List<TypeAdapterFactory> typeAdapters = new ArrayList<TypeAdapterFactory>();
		GsonUtils gsonUtil = GsonUtils.builder().typeFactories(typeAdapters).build();
		BookingResponse bookingResponse = gsonUtil.buildGson().fromJson(responseString, BookingResponse.class);
		return bookingResponse;
	}
}
