package com.tgs.services.fms.sources.technogram;

import java.io.IOException;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.restmodel.AirReviewRequest;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.AirReviewSeatResponse;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;

@Service
public class TechnoGramAirInfoFactory extends AbstractAirInfoFactory {

	private FlightAPIURLRuleCriteria apiURLS;

	private RestAPIListener apiListener;

	public TechnoGramAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
		apiListener = new RestAPIListener("");
		apiURLS = AirUtils.getAirEndPointURL(sourceConfiguration);
	}

	@Override
	protected void searchAvailableSchedules() throws SupplierRemoteException, NoSearchResultException {
		int attemptNum = 1;
		boolean retry = false;
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(searchQuery.getSearchId()).build();
		TechnoGramSearchManager searchManager = TechnoGramSearchManager.builder().apiURLS(apiURLS)
				.searchQuery(searchQuery).supplierConfiguration(supplierConf).apiListener(apiListener)
				.bindingService(bindingService).referenceId(searchQuery.getSearchId()).bookingUser(user).build();
		do {
			try {
				if (BooleanUtils.isFalse(sourceConfiguration.getIsSingleSell())) {
					List<String> searchIds = searchManager.getSearchQueryList();
					searchResult = searchManager.doSearch(searchIds);
				} else {
					searchResult = searchManager.doSingleSearch();
				}
			} catch (IOException sre) {
				retry = true;
				if (attemptNum == 2) {
					throw new SupplierRemoteException(sre.getMessage());
				}
			}
			attemptNum++;
		} while (retry && attemptNum <= 2);
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo reviewedTrip = null;
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(searchQuery.getSearchId()).build();
		TechnoGramReviewManager reviewManager = TechnoGramReviewManager.builder().apiURLS(apiURLS)
				.bindingService(bindingService).searchQuery(searchQuery).supplierConfiguration(supplierConf)
				.referenceId(bookingId).apiListener(apiListener).bookingUser(user).build();
		try {
			reviewedTrip = reviewManager.reviewByPriceId(selectedTrip);
			if (reviewedTrip == null) {
				throw new NoSeatAvailableException();
			}
		} finally {
			if (reviewedTrip != null) {
				storeBookingSession(bookingId, null, null, reviewedTrip);
			}
		}
		return reviewedTrip;
	}

	@Override
	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(bookingId).build();
		TechnoGramFareRuleManager fareRuleManager = TechnoGramFareRuleManager.builder().apiURLS(apiURLS)
				.bindingService(bindingService).supplierConfiguration(supplierConf).apiListener(apiListener)
				.referenceId(bookingId).bookingUser(user).build();
		TripFareRule tripFareRule = fareRuleManager.getFareRule(selectedTrip, flowType, isDetailed);
		return tripFareRule;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		TechnogramBindingService bindingService = TechnogramBindingService.builder().apiURLS(apiURLS)
				.supplierConfiguration(supplierConf).referenceId(bookingId).build();
		TechnoGramSeatSSRManager seatMapManager = TechnoGramSeatSSRManager.builder().apiURLS(apiURLS)
				.bindingService(bindingService).supplierConfiguration(supplierConf).referenceId(bookingId)
				.apiListener(apiListener).bookingUser(user).build();
		AirReviewSeatResponse seatResponse =
				seatMapManager.getSeatMap(tripInfo.getTripPriceInfos().get(0).getMiscInfo().getProviderCode());
		tripSeatMap = seatMapManager.getTripSeatMap(tripInfo, seatResponse);
		return tripSeatMap;
	}

}
