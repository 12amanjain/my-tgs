package com.tgs.services.fms.sources.technogram;

import lombok.extern.slf4j.Slf4j;
import lombok.experimental.SuperBuilder;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.CollectionUtils;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.restmodel.FareRuleRequest;
import com.tgs.services.fms.restmodel.FareRuleTripResponse;
import com.tgs.utils.exception.air.SupplierSessionException;
import org.apache.commons.lang3.BooleanUtils;

@Slf4j
@SuperBuilder
final class TechnoGramFareRuleManager extends TechnoGramServiceManager {

	public TripFareRule getFareRule(TripInfo selectedTrip, AirFlowType flowType, Boolean isDetailed) {
		TripFareRule tripFareRule = TripFareRule.builder().build();
		FareRuleRequest fareRuleRequest = getFareRuleRequest(selectedTrip, flowType, isDetailed);
		try {
			FareRuleTripResponse fareRuleResponse = getResponseByRequest(GsonUtils.getGson().toJson(fareRuleRequest),
					"FareRule", bindingService.getFareRuleURL(), FareRuleTripResponse.class);
			if (isSuccessfulResponse(fareRuleResponse, false)) {
				tripFareRule = processFareRuleResponse(tripFareRule, fareRuleResponse, selectedTrip);
			}
		} catch (IOException e) {
			throw new SupplierSessionException(e.getMessage());
		}
		return tripFareRule;
	}

	private FareRuleRequest getFareRuleRequest(TripInfo selectedTrip, AirFlowType flowType, Boolean isDetailed) {
		List<String> priceIds = collectPriceIds(selectedTrip);
		FareRuleRequest fareRuleRequest = new FareRuleRequest();
		if (CollectionUtils.isNotEmpty(priceIds)) {
			fareRuleRequest.setId(priceIds.get(0));
		} else {
			fareRuleRequest.setId(selectedTrip.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo()
					.get(0).getSupplierBookingId());
		}
		fareRuleRequest.setFlowType(flowType);
		fareRuleRequest.setIsDetailed(BooleanUtils.isTrue(isDetailed));
		return fareRuleRequest;
	}

	private TripFareRule processFareRuleResponse(TripFareRule tripFareRule, FareRuleTripResponse fareRuleResponse,
			TripInfo selectedTrip) {
		if (MapUtils.isNotEmpty(fareRuleResponse.getFareRule())) {
			for (Entry<String, FareRuleInformation> fareRules : fareRuleResponse.getFareRule().entrySet()) {
				if (fareRules.getValue() != null && MapUtils.isNotEmpty(fareRules.getValue().getFareRuleInfo())) {
					for (Entry<FareRulePolicyType, Map<FareRuleTimeWindow, FareRulePolicyContent>> fareRuleInfo : fareRules
							.getValue().getFareRuleInfo().entrySet()) {
						if (MapUtils.isNotEmpty(fareRuleInfo.getValue())) {
							for (Entry<FareRuleTimeWindow, FareRulePolicyContent> policyContents : fareRuleInfo
									.getValue().entrySet()) {
								FareRulePolicyContent policyContent = policyContents.getValue();
								if (policyContent != null && MapUtils.isNotEmpty(policyContent.getFareComponents())) {
									double totalFare = 0.0;
									for (Entry<FareComponent, Double> fareComponent : policyContent.getFareComponents()
											.entrySet()) {
										if (fareComponent.getValue() != null) {
											totalFare += getAmountBasedOnCurrency(fareComponent.getValue(), false);
										}
									}
									policyContent.setFareComponents(new HashMap<>());
									policyContent.setAmount(totalFare);
									policyContent.setAdditionalFee(0.0);
								}
							}
						}
					}
				}
			}
			tripFareRule = TripFareRule.builder().fareRule(fareRuleResponse.getFareRule()).build();
		}
		return tripFareRule;
	}

}
