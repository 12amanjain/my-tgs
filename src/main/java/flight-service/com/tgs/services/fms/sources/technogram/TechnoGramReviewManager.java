package com.tgs.services.fms.sources.technogram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.Conditions;
import com.tgs.services.base.datamodel.FareChangeAlert;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.RuntimeTypeAdapterFactory;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.ExtraServiceSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.services.fms.restmodel.AirReviewRequest;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.utils.exception.air.SupplierRemoteException;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
final class TechnoGramReviewManager extends TechnoGramServiceManager {

	public TripInfo reviewByPriceId(TripInfo selectedTrip) {
		TripInfo reviewedTrip = null;
		AirReviewRequest reviewRequest = getReviewRequest(selectedTrip, null);
		AirReviewResponse reviewResponse = review(reviewRequest);
		if (isSuccessfulResponse(reviewResponse, false) && CollectionUtils.isNotEmpty(reviewResponse.getTripInfos())) {
			reviewedTrip = parseReviewResponse(reviewResponse, selectedTrip);
		}
		return reviewedTrip;
	}

	public AirReviewResponse review(AirReviewRequest reviewRequest) {
		AirReviewResponse reviewResponse = null;
		try {
			reviewResponse = processReviewResponse(getResponseByRequest(GsonUtils.getGson().toJson(reviewRequest),
					"Review", bindingService.getReviewURL(), null));

		} catch (IOException e) {
			throw new SupplierRemoteException(e.getMessage());
		}
		return reviewResponse;
	}

	@SuppressWarnings("unchecked")
	private AirReviewResponse processReviewResponse(String responseString) {
		List<TypeAdapterFactory> typeAdapters = new ArrayList<TypeAdapterFactory>();
		RuntimeTypeAdapterFactory<Conditions> itemAdapter =
				RuntimeTypeAdapterFactory.of(Conditions.class, "conditions");
		itemAdapter.registerSubtype(AirBookingConditions.class);
		typeAdapters.add(itemAdapter);
		RuntimeTypeAdapterFactory<Alert> alertAdapter =
				RuntimeTypeAdapterFactory.of(Alert.class, "alerts").registerSubtype(FareChangeAlert.class);
		typeAdapters.add(alertAdapter);
		GsonUtils gsonUtil = GsonUtils.builder().typeFactories(typeAdapters).build();
		AirReviewResponse response = gsonUtil.buildGson().fromJson(responseString, AirReviewResponse.class);
		return response;
	}

	private TripInfo parseReviewResponse(AirReviewResponse reviewResponse, TripInfo selectedTrip) {
		int segmentStartIndex = 0;
		int segmentEndIndex = 0;
		for (TripInfo tripInfo : selectedTrip.splitTripInfo(true)) {
			segmentStartIndex = segmentEndIndex;
			segmentEndIndex = segmentStartIndex + tripInfo.getSegmentInfos().size();
			TripInfo supplierTrip = getTripFromReviewResponse(reviewResponse, tripInfo);
			supplierTrip = buildTripInfo(supplierTrip, searchQuery, supplierConfiguration);
			updateFare(selectedTrip.getSegmentInfos().subList(segmentStartIndex, segmentEndIndex),
					supplierTrip.getSegmentInfos());
			updateSupplierRefId(selectedTrip, reviewResponse.getBookingId());
		}
		selectedTrip.setBookingConditions(
				new GsonMapper<AirBookingConditions>(reviewResponse.getConditions(), null, AirBookingConditions.class)
						.convert());
		if (Objects.nonNull(selectedTrip.getBookingConditions().getPassportConditions())) {
			selectedTrip.getBookingConditions().getPassportConditions().setPassportEligible(true);
		}
		return selectedTrip;
	}

	private TripInfo getTripFromReviewResponse(AirReviewResponse reviewResponse, TripInfo selectedTrip) {
		for (TripInfo responseTrip : reviewResponse.getTripInfos()) {
			if (isSameTrip(responseTrip, selectedTrip)) {
				return responseTrip;
			}
		}
		return null;
	}

	private boolean isSameTrip(TripInfo responseTrip, TripInfo selectedTrip) {
		if (selectedTrip.getDepartureAirportCode().equals(responseTrip.getDepartureAirportCode())
				&& selectedTrip.getArrivalAirportCode().equals(responseTrip.getArrivalAirportCode())
				&& selectedTrip.getDepartureTime().equals(responseTrip.getDepartureTime())
				&& selectedTrip.getArrivalTime().equals(responseTrip.getArrivalTime())) {
			return true;
		}
		return false;
	}

	private void updateFare(List<SegmentInfo> selectedSegments, List<SegmentInfo> supplierSegments) {

		for (int segmentIndex = 0; segmentIndex < supplierSegments.size(); segmentIndex++) {
			SegmentInfo selectedSegment = selectedSegments.get(segmentIndex);
			SegmentInfo supplierSegment = supplierSegments.get(segmentIndex);
			for (int priceIndex = 0; priceIndex < supplierSegment.getPriceInfoList().size(); priceIndex++) {
				PriceInfo supplierPrice = supplierSegment.getPriceInfo(priceIndex);
				PriceInfo selectedPrice = selectedSegment.getPriceInfo(priceIndex);
				for (Entry<PaxType, FareDetail> entry : supplierPrice.getFareDetails().entrySet()) {
					selectedPrice.getFareDetail(entry.getKey()).setFareComponents(entry.getValue().getFareComponents());
				}
				selectedPrice.getMiscInfo().setSegmentKey(supplierSegment.getId());
			}
			if (MapUtils.isNotEmpty(supplierSegment.getSsrInfo())) {

				Map<SSRType, List<? extends SSRInformation>> ssrMap = new HashMap<>();
				List<BaggageSSRInformation> baggageSSRs = GsonUtils.getGson().fromJson(
						GsonUtils.getGson().toJson(parseSSRResponse(supplierSegment.getSsrInfo().get(SSRType.BAGGAGE))),
						new TypeToken<List<BaggageSSRInformation>>() {}.getType());
				List<MealSSRInformation> mealSSRs = GsonUtils.getGson().fromJson(
						GsonUtils.getGson().toJson(parseSSRResponse(supplierSegment.getSsrInfo().get(SSRType.MEAL))),
						new TypeToken<List<MealSSRInformation>>() {}.getType());
				List<SeatSSRInformation> seatSSRs = GsonUtils.getGson().fromJson(
						GsonUtils.getGson().toJson(parseSSRResponse(supplierSegment.getSsrInfo().get(SSRType.SEAT))),
						new TypeToken<List<SeatSSRInformation>>() {}.getType());
				List<ExtraServiceSSRInformation> extraSSRS =
						GsonUtils.getGson()
								.fromJson(
										GsonUtils.getGson()
												.toJson(parseSSRResponse(
														supplierSegment.getSsrInfo().get(SSRType.EXTRASERVICES))),
										new TypeToken<List<ExtraServiceSSRInformation>>() {}.getType());
				selectedSegment.addSSRInfo(SSRType.BAGGAGE, baggageSSRs);
				selectedSegment.addSSRInfo(SSRType.MEAL, mealSSRs);
				selectedSegment.addSSRInfo(SSRType.EXTRASERVICES, extraSSRS);
				selectedSegment.addSSRInfo(SSRType.SEAT, seatSSRs);
			}
		}
	}

	private List<? extends SSRInformation> parseSSRResponse(List<? extends SSRInformation> ssrList) {
		if (CollectionUtils.isNotEmpty(ssrList)) {
			for (SSRInformation ssrInfo : ssrList) {
				if (ssrInfo.getAmount() != null)
					ssrInfo.setAmount(getAmountBasedOnCurrency(ssrInfo.getAmount(), false));
			}
		}
		return ssrList;
	}


	/*
	 * At the time of booking supplier booking id needs to be send in payment and booking request.
	 */
	private void updateSupplierRefId(TripInfo selectedTrip, String supplierRefId) {
		for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {
			for (PriceInfo priceInfo : segment.getPriceInfoList()) {
				priceInfo.getMiscInfo().setProviderCode(supplierRefId);
			}
		}
	}

	private AirReviewRequest getReviewRequest(TripInfo selectedTrip, String supplierRefId) {
		AirReviewRequest reviewRequest = new AirReviewRequest();
		if (StringUtils.isNotEmpty(supplierRefId)) {
			reviewRequest.setBookingId(supplierRefId);
		} else {
			List<String> priceIds = collectPriceIds(selectedTrip);
			if (CollectionUtils.isNotEmpty(priceIds)) {
				reviewRequest.setPriceValidation(true);
				reviewRequest.setPriceIds(priceIds);
			}
		}
		return reviewRequest;
	}

}
