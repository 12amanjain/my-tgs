package com.tgs.services.fms.sources.technogram;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import com.tgs.services.fms.restmodel.AirSearchResponse;
import com.tgs.utils.exception.air.NoSearchResultException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.StopWatch;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirSearchQueryListResponse;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class TechnoGramSearchManager extends TechnoGramServiceManager {

	public AirSearchResult doSearch(List<String> searchIds) throws IOException {
		AirSearchResult searchResult = new AirSearchResult();
		List<Future<AirSearchResponse>> futureTaskList = new ArrayList<>();
		for (String searchId : searchIds) {
			Future<AirSearchResponse> supplierwiseSearchResponse =
					ExecutorUtils.getFlightSearchThreadPool().submit(() -> doSearchOnId(searchId));
			futureTaskList.add(supplierwiseSearchResponse);
		}

		for (int index = 0; index < futureTaskList.size(); index++) {
			try {
				AirSearchResponse searchResponse = futureTaskList.get(index).get();
				if (isSuccessfulResponse(searchResponse, false)) {
					parseSearchResponse(searchResult, searchResponse);
				}
			} catch (InterruptedException | ExecutionException e) {
				throw new NoSearchResultException("Search Thread Interrupted " + searchQuery.getSearchId());
			}
		}

		return searchResult;
	}

	private AirSearchResponse doSearchOnId(String supplirSearchId) {
		AirSearchResponse searchResponse = null;
		boolean isRetryRequired = false;
		AirSearchRequest searchRequest = new AirSearchRequest();
		searchRequest.setSearchId(supplirSearchId);
		StopWatch timer = new StopWatch();
		timer.start();
		do {
			try {
				searchResponse = getResponseByRequest(GsonUtils.getGson().toJson(searchRequest), "Search",
						bindingService.getSearchURL(), AirSearchResponse.class);
				long retryInSeconds = getRetrySeconds(searchResponse);
				if (isRetryRequired(retryInSeconds, timer, TechnoGramAirUtils.searchTTO())) {
					Thread.sleep(retryInSeconds * 1000);
					isRetryRequired = true;
				} else {
					isRetryRequired = false;
				}
			} catch (IOException | InterruptedException e) {
				log.error("Error occurred on search for searchId {} exception {}", searchQuery.getSearchId(), e);
				isRetryRequired = false;
			}
		} while (isRetryRequired);
		return searchResponse;
	}

	protected List<String> getSearchQueryList() throws IOException {
		AirSearchQueryListResponse response =
				getResponseByRequest(GsonUtils.getGson().toJson(getSearchQueryListRequest()), "Searchquery List",
						bindingService.getSearchQueryListURL(), AirSearchQueryListResponse.class);
		if (!isSuccessfulResponse(response, false)) {
			if (CollectionUtils.isEmpty(response.getSearchIds())) {
				throw new NoSearchResultException(
						StringUtils.join("Empty search query list for searchId ", searchQuery.getSearchId()));
			}
		}
		return response.getSearchIds();
	}

	private AirSearchRequest getSearchQueryListRequest() {
		AirSearchRequest request = new AirSearchRequest();
		AirSearchQuery searchQuery = new AirSearchQuery();
		searchQuery.setPaxInfo(this.searchQuery.getPaxInfo());
		searchQuery.setSearchType(this.searchQuery.getSearchType());
		searchQuery.setCabinClass(this.searchQuery.getCabinClass());
		searchQuery.setPreferredAirline(this.searchQuery.getPreferredAirline());
		List<RouteInfo> routeInfos = new ArrayList<>();
		for (RouteInfo routeInfo : this.searchQuery.getRouteInfos()) {
			routeInfos.add(RouteInfo.builder()
					.fromCityOrAirport(AirportInfo.builder().code(routeInfo.getFromCityAirportCode()).build())
					.toCityOrAirport(AirportInfo.builder().code(routeInfo.getToCityAirportCode()).build())
					.travelDate(routeInfo.getTravelDate()).build());
		}
		searchQuery.setRouteInfos(routeInfos);
		request.setSearchQuery(searchQuery);
		return request;
	}

	private AirSearchResult parseSearchResponse(AirSearchResult searchResult, AirSearchResponse searchResponse) {
		if (searchResponse.getSearchResult() != null
				&& MapUtils.isNotEmpty(searchResponse.getSearchResult().getTripInfos())) {
			Map<String, List<TripInfo>> tripTypeMap = searchResponse.getSearchResult().getTripInfos();
			for (Entry<String, List<TripInfo>> tripType : tripTypeMap.entrySet()) {
				if (CollectionUtils.isNotEmpty(tripType.getValue())) {
					for (TripInfo tripInfo : tripType.getValue()) {
						TripInfo trip = buildTripInfo(tripInfo, searchQuery, supplierConfiguration);
						searchResult.addTripInfo(tripType.getKey(), trip);
					}
				}
			}
		}
		return searchResult;
	}

	public AirSearchResult doSingleSearch() throws IOException {
		AirSearchResult searchResult = new AirSearchResult();
		AirSearchResponse response = getResponseByRequest(GsonUtils.getGson().toJson(getSearchQueryListRequest()),
				"Search All", bindingService.getSearchAllURL(), AirSearchResponse.class);
		if (isSuccessfulResponse(response, false)) {
			parseSearchResponse(searchResult, response);
		}
		return searchResult;
	}


}
