package com.tgs.services.fms.sources.technogram;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.util.StopWatch;
import com.google.api.client.repackaged.com.google.common.base.Enums;
import com.google.common.reflect.TypeToken;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
abstract class TechnoGramServiceManager {

	protected FlightAPIURLRuleCriteria apiURLS;

	protected SupplierConfiguration supplierConfiguration;

	protected RestAPIListener apiListener;

	protected List<String> criticalMessageLogger;

	protected String referenceId;

	protected TechnogramBindingService bindingService;

	protected User bookingUser;

	protected AirSearchQuery searchQuery;

	protected <T> T getResponseByRequest(String request, String serviceName, String url, Class<T> type)
			throws IOException {
		HttpUtils httpUtils = HttpUtils.builder().urlString(url).postData(request).headerParams(getHeaderParams())
				.proxy(getProxy()).build();
		T responseBody = null;
		try {
			responseBody = httpUtils.getResponse(type).orElse(null);
		} catch (IOException ie) {
			if (StringUtils.isNotBlank(httpUtils.getResponseString())) {
				BaseResponse response = GsonUtils.getGson().fromJson(httpUtils.getResponseString(), BaseResponse.class);
				logCriticalMessage(response);
			}
			throw ie;
		} finally {
			serviceName = AirUtils.getLogType(serviceName, supplierConfiguration);
			String logData = StringUtils.join(formatRQRS(httpUtils.getPostData(), StringUtils.join(serviceName, "RQ")),
					formatRQRS(httpUtils.getResponseString(), StringUtils.join(serviceName, "RS")));
			apiListener.addLog(LogData.builder().key(ObjectUtils.firstNonNull(referenceId, "")).logData(logData)
					.type(serviceName).build());
		}
		return responseBody;
	}

	private Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "application/json");
		headerParams.put("apikey", supplierConfiguration.getSupplierCredential().getPassword());
		return headerParams;
	}

	private Proxy getProxy() {
		Proxy proxy = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (configuratorInfo != null && StringUtils.isNotBlank(configuratorInfo.getProxyAddress())) {
			String proxyAddress = configuratorInfo.getProxyAddress();
			proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
					new InetSocketAddress(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1])));
		}
		return proxy;
	}

	protected boolean isSuccessfulResponse(BaseResponse baseResponse, boolean logCriticalMsg) {
		boolean isSuccess = false;
		if (baseResponse != null && CollectionUtils.isEmpty(baseResponse.getErrors())
				&& baseResponse.getStatus() != null && baseResponse.getStatus().getSuccess()
				&& baseResponse.getStatus().getHttpStatus() == HttpStatus.OK.value()) {
			isSuccess = true;
		} else if (logCriticalMsg) {
			logCriticalMessage(baseResponse);
		}
		return isSuccess;
	}

	private void logCriticalMessage(BaseResponse baseResponse) {
		if (criticalMessageLogger != null && baseResponse != null
				&& CollectionUtils.isNotEmpty(baseResponse.getErrors())) {
			StringJoiner errorMsg = new StringJoiner("-");
			for (ErrorDetail error : baseResponse.getErrors()) {
				errorMsg.add(error.getMessage());
			}
			criticalMessageLogger.add(errorMsg.toString());
		}
	}

	protected long getRetrySeconds(BaseResponse baseResponse) {
		long retrySeconds = 0;
		if (baseResponse != null && baseResponse.getRetryInSecond() != null) {
			retrySeconds = baseResponse.getRetryInSecond();
		}
		return retrySeconds;
	}

	protected boolean isRetryRequired(long retryInSeconds, StopWatch timer, double tto) {
		return retryInSeconds > 0 && timer.getTotalTimeSeconds() < tto;
	}

	public void addToCritcalMessage(String criticalMsg) {
		if (CollectionUtils.isNotEmpty(criticalMessageLogger)) {
			this.criticalMessageLogger.add(criticalMsg);
		}
	}

	public String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	protected List<String> collectPriceIds(TripInfo selectedTrip) {
		Set<String> priceIds = new LinkedHashSet<>();
		for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {
			if (StringUtils.isNotEmpty(segment.getPriceInfo(0).getMiscInfo().getSessionId())) {
				priceIds.add(segment.getPriceInfo(0).getMiscInfo().getSessionId());
			}
		}
		return new ArrayList<>(priceIds);
	}

	protected double getAmountBasedOnCurrency(double amount, boolean isReverseConversion) {
		String fromCurrency = getDefaultCurrency();
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount;
		}
		if (isReverseConversion) {
			String temp = fromCurrency;
			fromCurrency = toCurrency;
			toCurrency = temp;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(getType()).build();
		return TechnoGramAirUtils.getExchangeValue(amount, filter);
	}

	private String getDefaultCurrency() {
		if (StringUtils.isNotBlank(supplierConfiguration.getSupplierCredential().getCurrencyCode())) {
			return supplierConfiguration.getSupplierCredential().getCurrencyCode();
		}
		return AirSourceConstants.getClientCurrencyCode();
	}


	protected String getType() {
		return AirSourceType.getAirSourceType(supplierConfiguration.getSourceId()).name();
	}

	protected TripInfo buildTripInfo(TripInfo tripInfo, AirSearchQuery searchQuery,
			SupplierConfiguration supplierConfig) {
		TripInfo newTrip = new GsonMapper<TripInfo>(tripInfo, null, TripInfo.class).convert();
		boolean isLegPricing = newTrip.isPriceInfosNotEmpty();
		newTrip.setId(StringUtils.EMPTY);
		buildSegmentInfos(newTrip, supplierConfig);
		if (!isLegPricing && CollectionUtils.size(newTrip.getSegmentInfos()) > 1) {
			resetPriceComponents(newTrip);
		}
		return newTrip;
	}

	protected void resetPriceComponents(TripInfo newTrip) {
		for (int segmentIndex = 1; segmentIndex < newTrip.getSegmentInfos().size(); segmentIndex++) {
			SegmentInfo segmentInfo = newTrip.getSegmentInfos().get(segmentIndex);
			for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
				for (Entry<PaxType, FareDetail> fareDetail : priceInfo.getFareDetails().entrySet()) {
					fareDetail.getValue().setFareComponents(getZeroFareComponents());
				}
			}
		}
	}

	protected void buildSegmentInfos(TripInfo tripInfo, SupplierConfiguration supplierConfig) {
		boolean isTripPricing = false;
		for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
			// Handling both api_v0 and api_v1
			if (segment.getPriceInfoList().isEmpty()) {
				List<PriceInfo> tempTripPrice =
						GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(tripInfo.getTripPriceInfos()),
								new TypeToken<List<PriceInfo>>() {}.getType());
				segment.setPriceInfoList(tempTripPrice);
				isTripPricing = true;
			}
			buildPriceInfos(segment, supplierConfig, isTripPricing);
		}
	}

	protected void buildPriceInfos(SegmentInfo segmentInfo, SupplierConfiguration supplierConfig,
			boolean isTripPricing) {
		for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
			priceInfo.setSupplierBasicInfo(supplierConfig.getBasicInfo());
			priceInfo.getMiscInfo().setSessionId(priceInfo.getId());
			priceInfo.setId(StringUtils.EMPTY);
			setFareDetails(segmentInfo, priceInfo, isTripPricing);
		}
	}

	protected void setFareDetails(SegmentInfo segmentInfo, PriceInfo priceInfo, boolean isTripPricing) {

		for (Entry<PaxType, FareDetail> fareDetail : priceInfo.getFareDetails().entrySet()) {
			if (isTripPricing && segmentInfo.getSegmentNum() > 0) {
				fareDetail.getValue().setFareComponents(getZeroFareComponents());
				fareDetail.getValue().setAddlFareComponents(null);
			} else if (segmentInfo.getSegmentNum() == 0) {
				Map<FareComponent, Double> updatedFareComponents = buildFareComponents(fareDetail.getValue());
				fareDetail.getValue().setFareComponents(updatedFareComponents);
				fareDetail.getValue().setAddlFareComponents(null);
			}
		}
	}

	// it calucaltes gross fare
	protected Map<FareComponent, Double> buildFareComponents(FareDetail fareDetail) {
		boolean isGSTtoOtherCharges = BooleanUtils.isTrue(ServiceCommunicatorHelper.getClientInfo().getIsGstAllowed());
		Map<FareComponent, Double> fareComponents = new HashMap<>();
		if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			// This keeps TF and airline components as it is. Commission and user fee will set in OC.
			for (Entry<FareComponent, Double> fareComponent : fareDetail.getFareComponents().entrySet()) {

				if (FareComponent.getCommissionComponents().contains(fareComponent.getKey())
						|| FareComponent.TDS.equals(fareComponent.getKey())) {
					continue;
				}

				if (!FareComponent.NF.equals(fareComponent.getKey())
						&& !FareComponent.NCM.equals(fareComponent.getKey())) {
					if (fareComponent.getKey() != null) {
						FareComponent fareComp = Enums.getIfPresent(FareComponent.class, fareComponent.getKey().name())
								.or(FareComponent.OC);
						if (fareComp.airlineComponent()) {
							// keep airline components as it is as break up component
							Double initialFare = fareComponents.getOrDefault(fareComp, 0d);
							fareComponents.put(fareComponent.getKey(),
									initialFare + getAmountBasedOnCurrency(fareComponent.getValue(), false));
						} else if (FareComponent.TAF.equals(fareComp)
								&& MapUtils.isNotEmpty(fareDetail.getAddlFareComponents())
								&& MapUtils.isNotEmpty(fareDetail.getAddlFareComponents().get(fareComp))) {
							Map<FareComponent, Double> breakUpComps =
									fareDetail.getAddlFareComponents().get(FareComponent.TAF);
							for (FareComponent breakUp : breakUpComps.keySet()) {
								if (!isGSTtoOtherCharges && FareComponent.AGST.equals(breakUp)) {
									Double initialFare = fareComponents.getOrDefault(FareComponent.OC, 0d);
									fareComponents.put(FareComponent.OC,
											initialFare + getAmountBasedOnCurrency(breakUpComps.get(breakUp), false));
								} else if (breakUp.airlineComponent()) {
									// keep airline components as it is as break up component
									Double initialFare = fareComponents.getOrDefault(breakUp, 0d);
									fareComponents.put(breakUp,
											initialFare + getAmountBasedOnCurrency(breakUpComps.get(breakUp), false));
								} else {
									// keep MF,MFT and other client components as OC which is not belongs to
									// AirlineComponents
									Double initialFare = fareComponents.getOrDefault(FareComponent.OC, 0d);
									fareComponents.put(FareComponent.OC,
											initialFare + getAmountBasedOnCurrency(breakUpComps.get(breakUp), false));
								}
							}
						} else if (!FareComponent.TF.equals(fareComp)) {
							Double initialFare = fareComponents.getOrDefault(FareComponent.OC, 0d);
							fareComponents.put(FareComponent.OC,
									initialFare + getAmountBasedOnCurrency(fareComponent.getValue(), false));
						}
					}
				}
			}
			
			fareComponents.put(FareComponent.SC, getAmountBasedOnCurrency(getSupplierCommission(fareDetail), false));
			fareComponents.put(FareComponent.SMF,
					getAmountBasedOnCurrency(getSupplierManagementFee(fareDetail), false));
			fareComponents.put(FareComponent.TF,
					getAmountBasedOnCurrency(fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0d), false));
		}
		return fareComponents;
	}

	protected double getSupplierCommission(FareDetail fareDetail) {
		double supplierCommission = 0.0;
		if (Objects.nonNull(fareDetail.getFareComponents().get(FareComponent.NCM))) {
			// supplierCommission = fareDetail.getFareComponents().get(FareComponent.NCM);
			supplierCommission = fareDetail.getAddlFareComponents().getOrDefault(FareComponent.NCM, new HashMap<>())
					.getOrDefault(FareComponent.OT, 0.0);
		}
		return supplierCommission;
	}

	protected double getSupplierManagementFee(FareDetail fareDetail) {
		double supplierManagementFee = 0.0;
		if (MapUtils.isNotEmpty(fareDetail.getAddlFareComponents())) {
			for (Entry<FareComponent, Double> fareComponent : fareDetail.getAddlFareComponents().get(FareComponent.TAF)
					.entrySet()) {
				if (FareComponent.MF.equals(fareComponent.getKey())
						|| FareComponent.MFT.equals(fareComponent.getKey())) {
					supplierManagementFee += fareComponent.getValue();
				}
			}
		} else {
			supplierManagementFee = fareDetail.getFareComponents().getOrDefault(FareComponent.MF, 0.0)
					+ fareDetail.getFareComponents().getOrDefault(FareComponent.MFT, 0.0);
		}
		return supplierManagementFee;
	}

	protected Map<FareComponent, Double> getZeroFareComponents() {
		Map<FareComponent, Double> newFareComponents = new HashMap<FareComponent, Double>();
		newFareComponents.put(FareComponent.TF, 0.0);
		newFareComponents.put(FareComponent.BF, 0.0);
		newFareComponents.put(FareComponent.AT, 0.0);
		return newFareComponents;
	}

}
