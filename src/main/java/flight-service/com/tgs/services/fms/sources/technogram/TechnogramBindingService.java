package com.tgs.services.fms.sources.technogram;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
final class TechnogramBindingService {

	private FlightAPIURLRuleCriteria apiURLS;

	private SupplierConfiguration supplierConfiguration;

	private String referenceId;

	public String getSearchURL() {
		String searchURL = null;
		if (apiURLS == null) {
			searchURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			searchURL = apiURLS.getFlightURL();
		}
		log.debug("Search URL {} for searchId {}", searchURL, referenceId);
		return searchURL;
	}

	public String getSearchQueryListURL() {
		String searchQueryListURL = null;
		if (apiURLS == null) {
			searchQueryListURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			searchQueryListURL = apiURLS.getTravelAgent();
		}
		log.debug("Search Query List URL {} for searchId {}", searchQueryListURL, referenceId);
		return searchQueryListURL;
	}
	
	public String getSearchAllURL() {
		String searchAllURL = null;
		if (apiURLS == null) {
			searchAllURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			searchAllURL = apiURLS.getSecurityURL();
		}
		log.debug("Search All URL {} for searchId {}", searchAllURL, referenceId);
		return searchAllURL;
	}

	public String getReviewURL() {
		String reviewURL = null;
		if (apiURLS == null) {
			reviewURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			reviewURL = apiURLS.getPricingURL();
		}
		log.debug("Review URL {} for searchId {}", reviewURL, referenceId);
		return reviewURL;
	}

	public String getBookingURL(boolean isConfirmBooking) {
		String bookingURL = null;
		if (apiURLS == null) {
			bookingURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			bookingURL = apiURLS.getReservationURL();
		}
		if (isConfirmBooking) {
			bookingURL = StringUtils.join(bookingURL, "confirm-book/");
		} else {
			bookingURL = StringUtils.join(bookingURL, "book/");
		}
		log.debug("Booking URL {} for bookingId {}", bookingURL, referenceId);
		return bookingURL;
	}

	public String getRetrieveBookingURL() {
		String retrieveBookingURL = null;
		if (apiURLS == null) {
			retrieveBookingURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			retrieveBookingURL = apiURLS.getRetrieveURL();
		}
		log.debug("Retrieve booking URL {} for bookingId {}", retrieveBookingURL, referenceId);
		return retrieveBookingURL;
	}

	public String getPaymentModeURL() {
		String paymentModeURL = null;
		if (apiURLS == null) {
			paymentModeURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			paymentModeURL = apiURLS.getPnrPaymentURL();
		}
		log.debug("Payment Modes URL {} for bookingId {}", paymentModeURL, referenceId);
		return paymentModeURL;
	}

	public String getFareRuleURL() {
		String faerRuleURL = null;
		if (apiURLS == null) {
			faerRuleURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			faerRuleURL = apiURLS.getFareRuleURL();
		}
		log.debug("Fare rule URL {} for bookingId {}", faerRuleURL, referenceId);
		return faerRuleURL;
	}
	
	public String getFareValidateURL() {
		String fareValidateURL = null;
		if (apiURLS == null) {
			fareValidateURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			fareValidateURL = apiURLS.getFareValidateURL();
		}
		log.debug("Fare-Validate URL {} for bookingId {}", fareValidateURL, referenceId);
		return fareValidateURL;
	}
	
	public String getSeatMapURL() {
		String seatMapURL = null;
		if (apiURLS == null) {
			seatMapURL = supplierConfiguration.getSupplierCredential().getUrl().split(",")[1];
		} else {
			seatMapURL = apiURLS.getSsrURL();
		}
		log.debug("Seat Map URL {} for bookingId {}", seatMapURL, referenceId);
		return seatMapURL;
	}


}
