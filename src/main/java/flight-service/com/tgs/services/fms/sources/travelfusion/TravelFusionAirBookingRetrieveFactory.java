package com.tgs.services.fms.sources.travelfusion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;

@Service
public class TravelFusionAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	@Autowired
	public static Jaxb2Marshaller marshaller;

	private TravelFusionBindingService bindingService;
	private TravelFusionBookingRetrieveManager retrieveManager;
	protected RestAPIListener listener = null;

	public void initialize() {
		bindingService = TravelFusionBindingService.builder().build();
		if (listener == null) {
			listener = new RestAPIListener("");
		}
		retrieveManager = TravelFusionBookingRetrieveManager.builder().cachingComm(cachingCommunicator)
				.supplierReference(pnr).bindingService(bindingService).supplierConfiguration(supplierConf)
				.listener(listener).bookingUser(bookingUser).build();

	}

	public TravelFusionAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}

	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		retrieveManager.initLoginId(true, "TF-ImportPnr");
		pnrBooking = retrieveManager.updateBookingDetails(retrieveManager.getBookingDetails());
		return pnrBooking;
	}

}
