package com.tgs.services.fms.sources.travelfusion;

import java.util.List;
import org.apache.tomcat.util.buf.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TravelFusionAirInfoFactory extends AbstractAirInfoFactory {

	protected RestAPIListener listener = null;
	protected String tokenId;
	protected FlightAPIURLRuleCriteria apiURLS;

	protected TravelFusionBindingService bindingService;

	public TravelFusionAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public void initialize(boolean isSearch) {
		bindingService = TravelFusionBindingService.builder().configuration(supplierConf).build();
		if (listener == null) {
			listener = new RestAPIListener("");
		}
	}

	@Override
	protected void searchAvailableSchedules() {
		initialize(true);
		TravelFusionSearchManager searchManager = TravelFusionSearchManager.builder().bindingService(bindingService)
				.supplierConfiguration(supplierConf).moneyExchnageComm(moneyExchnageComm).searchQuery(searchQuery)
				.listener(listener).cachingComm(cachingComm).bookingUser(user).build();
		searchManager.initLoginId(true, searchQuery.getSearchId());
		searchResult = searchManager.doSearch();
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		TripInfo tripInfo = null;
		initialize(true);
		TravelFusionReviewManager reviewManager =
				TravelFusionReviewManager.builder().bindingService(bindingService).supplierConfiguration(supplierConf)
						.moneyExchnageComm(moneyExchnageComm).cachingComm(cachingComm).searchQuery(searchQuery)
						.apiURLS(apiURLS).bookingId(bookingId).listener(listener).bookingUser(user).build();
		try {
			listener.setKey(bookingId);
			reviewManager.initLoginId(true, bookingId);
			tripInfo = reviewManager.processDetails(selectedTrip);
		} finally {
			if (reviewManager.isReviewSuccess() && tripInfo != null) {
				storeBookingSession(bookingId, null, null, tripInfo);
			}
		}
		return tripInfo;
	}

	@Override
	public List<SourceRouteInfo> addRoutesToSystem(List<SourceRouteInfo> routes, String searchKey) {
		initialize(true);
		TravelFusionSupplierRoutesManager supplierRoutes = TravelFusionSupplierRoutesManager.builder()
				.bindingService(bindingService).supplierConfiguration(supplierConf).listener(listener)
				.listenerKey(searchKey.concat(supplierConf.getSourceId().toString())).cachingComm(cachingComm)
				.bookingUser(user).build();
		supplierRoutes.initLoginId(false, searchKey);
		supplierRoutes.addRouteInfoToSystem(routes);
		return routes;

	}
}
