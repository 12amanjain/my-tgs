package com.tgs.services.fms.sources.travelfusion;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierSessionException;
import TravelFusionRequests.CheckBookingType;
import TravelFusionRequests.CheckRoutingType;
import TravelFusionRequests.GeneralRequestCommandListType;
import TravelFusionRequests.GetBookingDetailsType;
import TravelFusionRequests.GetBranchSupplierListType;
import TravelFusionRequests.ListSupplierRoutesType;
import TravelFusionRequests.LoginType;
import TravelFusionRequests.ObjectFactory;
import TravelFusionRequests.ProcessDetailsType;
import TravelFusionRequests.ProcessTermsType;
import TravelFusionRequests.StartBookingType;
import TravelFusionRequests.StartRoutingType;
import TravelFusionResponses.SpecificCommandExecutionFailureType;
import TravelFusionResponses.CheckBookingResponseType;
import TravelFusionResponses.CheckRoutingResponseType;
import TravelFusionResponses.GeneralResponseCommandListType;
import TravelFusionResponses.GetBookingDetailsResponseType;
import TravelFusionResponses.GetBranchSupplierListResponseType;
import TravelFusionResponses.ListSupplierRoutesResponseType;
import TravelFusionResponses.ProcessDetailsResponseType;
import TravelFusionResponses.ProcessTermsResponseType;
import TravelFusionResponses.StartBookingResponseType;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.Builder;
import org.apache.commons.lang3.StringUtils;

@Builder
final class TravelFusionBindingService {

	protected SupplierConfiguration configuration;

	public static JAXBContext objToString;

	public static JAXBContext StringToObj;

	public static ObjectFactory objReq = new ObjectFactory();

	private static final String DELIMETER = "-";

	public static TravelFusionResponses.ObjectFactory objRes = new TravelFusionResponses.ObjectFactory();

	// for creating only one instance as JAXB is a costly task
	private static JAXBContext getJAXBinstance() throws JAXBException {
		if (objToString == null)
			objToString = JAXBContext.newInstance(ObjectFactory.class);
		return objToString;
	}

	protected <T> String loginMarshaller(LoginType request, Class<T> clazz) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setLogin(request);
		return getRequestString(genReq);
	}

	protected static String loginUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res == null) {
			throw new SupplierSessionException(TravelFusionConstants.AUTH_FAILED);
		}
		if (res != null && res.getValue() != null && res.getValue().getCommandExecutionFailure() != null
				&& res.getValue().getCommandExecutionFailure().getLogin() != null) {
			throw new SupplierSessionException(res.getValue().getCommandExecutionFailure().getLogin().getEtext());
		}
		return res.getValue().getLogin().getLoginId();
	}

	protected <T> String searchRouteMarshaller(StartRoutingType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setStartRouting(request);
		request.setTimeout(BigInteger.valueOf(TravelFusionConstants.START_ROUTING_TIMEOUT));
		return getRequestString(genReq);
	}

	protected static String searchRouteUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res == null || (res != null && (res.getValue().getDataValidationFailure() != null))) {
			throw new NoSearchResultException("DataValidationFailure");
		} else if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getStartRouting());
			throw new NoSearchResultException(errorMessage);
		}
		return res.getValue().getStartRouting().getRoutingId().getValue();
	}

	protected <T> String checkRoutingMarshaller(CheckRoutingType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setCheckRouting(request);
		return getRequestString(genReq);
	}

	protected static CheckRoutingResponseType checkRoutingUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getCheckRouting());
			throw new NoSearchResultException(errorMessage);
		}
		return res.getValue().getCheckRouting();
	}

	protected String getRequestString(GeneralRequestCommandListType genReq) throws JAXBException {
		StringWriter sw = new StringWriter();
		JAXBContext contextObj = getJAXBinstance();
		Marshaller marshallerObj = contextObj.createMarshaller();
		marshallerObj.marshal(objReq.createCommandList(genReq), sw);
		String req = sw.toString();
		return req;
	}

	protected static JAXBElement<GeneralResponseCommandListType> getApiResponse(String response) throws JAXBException {
		JAXBContext jaxbContext = getJAXBResponseInstance();
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		StringReader reader = new StringReader(response);
		JAXBElement<GeneralResponseCommandListType> res =
				(JAXBElement<GeneralResponseCommandListType>) jaxbUnmarshaller.unmarshal(reader);
		return res;
	}

	// for creating only one instance as JAXB is a costly task
	private static JAXBContext getJAXBResponseInstance() throws JAXBException {
		if (StringToObj == null)
			StringToObj = JAXBContext.newInstance(GeneralResponseCommandListType.class);
		return StringToObj;
	}

	protected CheckBookingType buildCheckBookingRequest(String loginId, String TFref) {
		CheckBookingType request = objReq.createCheckBookingType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		request.setTFBookingReference(TFref);
		return request;
	}

	protected <T> String checkBookingMarshaller(CheckBookingType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setCheckBooking(request);
		return getRequestString(genReq);
	}

	protected static CheckBookingResponseType checkBookingUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getCheckBooking());
			throw new SupplierUnHandledFaultException(errorMessage);
		}
		return res.getValue().getCheckBooking();
	}

	protected <T> String getBookingDetailsMarshaller(GetBookingDetailsType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setGetBookingDetails(request);
		return getRequestString(genReq);
	}

	protected static GetBookingDetailsResponseType getBookingDetailsUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getGetBookingDetails());
			throw new SupplierUnHandledFaultException(errorMessage);
		}
		return res.getValue().getGetBookingDetails();
	}

	protected <T> String reviewMarshaller(ProcessDetailsType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setProcessDetails(request);
		return getRequestString(genReq);
	}

	protected ProcessDetailsResponseType processDetailsUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getProcessDetails());
			throw new NoSeatAvailableException(errorMessage);
		}
		return res.getValue().getProcessDetails();
	}

	protected <T> String processTermsMarshaller(ProcessTermsType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setProcessTerms(request);
		return getRequestString(genReq);
	}

	protected ProcessTermsResponseType processTermsUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getProcessTerms());
			throw new SupplierUnHandledFaultException(errorMessage);
		}
		return res.getValue().getProcessTerms();
	}

	protected <T> String startBookingMarshaller(StartBookingType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setStartBooking(request);
		return getRequestString(genReq);
	}

	protected StartBookingResponseType startBookingUnmarshaller(String response) throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getStartBooking());
			throw new SupplierUnHandledFaultException(errorMessage);
		}
		return res.getValue().getStartBooking();
	}

	protected static ListSupplierRoutesResponseType getSupplierRoutesListUnmarshaller(String response)
			throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		if (res != null && (res.getValue().getCommandExecutionFailure() != null)) {
			String errorMessage =
					getCommandFailureMessgage(res.getValue().getCommandExecutionFailure().getListSupplierRoutes());
			throw new SupplierUnHandledFaultException(errorMessage);
		}
		return res.getValue().getListSupplierRoutes();
	}

	protected <T> String supplierRouteMarshaller(ListSupplierRoutesType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setListSupplierRoutes(request);
		return getRequestString(genReq);
	}

	protected static GetBranchSupplierListResponseType getBranchSupplierListUnmarshaller(String response)
			throws JAXBException {
		JAXBElement<GeneralResponseCommandListType> res = getApiResponse(response);
		return res.getValue().getGetBranchSupplierList();
	}

	protected <T> String getBranchSupplierListMarshaller(GetBranchSupplierListType request) throws JAXBException {
		GeneralRequestCommandListType genReq = objReq.createGeneralRequestCommandListType();
		genReq.setGetBranchSupplierList(request);
		return getRequestString(genReq);
	}

	private static String getCommandFailureMessgage(SpecificCommandExecutionFailureType failureInfo) {
		String message = StringUtils.EMPTY;
		if (failureInfo != null && StringUtils.isNotBlank(failureInfo.getEcode())) {
			message = StringUtils.join(failureInfo.getEcode(), DELIMETER, failureInfo.getEtext(), DELIMETER,
					failureInfo.getEdetail(), DELIMETER, failureInfo.getEdate());
		}
		return message;
	}

}
