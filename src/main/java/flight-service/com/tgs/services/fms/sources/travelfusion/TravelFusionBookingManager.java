package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.*;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionUtils.getAge;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionUtils.getTitle;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.Period;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.xml.bind.JAXBException;
import TravelFusionResponses.*;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.BookingProfileBillingDetailsType;
import TravelFusionRequests.BookingProfileContactDetailsType;
import TravelFusionRequests.BookingProfileCreditCardType;
import TravelFusionRequests.BookingProfileTravellerList;
import TravelFusionRequests.BookingProfileTravellerType;
import TravelFusionRequests.BookingProfileType;
import TravelFusionRequests.CheckBookingType;
import TravelFusionRequests.CommonCustomSupplierParameterList;
import TravelFusionRequests.CommonFinalBookingStatusType;
import TravelFusionRequests.CommonName;
import TravelFusionRequests.CommonNamePartListType;
import TravelFusionRequests.CommonPhone;
import TravelFusionRequests.CommonString;
import TravelFusionRequests.CustomSupplierParameterType;
import TravelFusionRequests.FakeBookingType;
import TravelFusionRequests.ProcessTermsType;
import TravelFusionRequests.StartBookingType;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
final class TravelFusionBookingManager extends TravelFusionServiceManager {

	private Order order;
	private List<SegmentInfo> segmentInfos;
	protected Boolean isTicketRequired;

	protected ProcessTermsResponseType processTerms;

	protected Double supplierAmount;
	protected BigDecimal supplierChargeableAmount;

	public void createPNR(boolean isHoldBooking, CreditCardInfo card) {
		HttpUtils httpUtils = null;
		try {
			ProcessTermsType requestBody = buildProcessTermsRequest(loginId, isHoldBooking, card);
			httpUtils = getHttpUtil();
			httpUtils.setPostData(bindingService.processTermsMarshaller(requestBody));
			httpUtils.setTimeout(120 * 1000);
			String response = httpUtils.getResponse(null).orElse("").toString();
			processTerms = bindingService.processTermsUnmarshaller(response);
			if (processTerms != null && processTerms.getTFBookingReference() != null) {
				bookingOrderNo = processTerms.getTFBookingReference();
				BookingUtils.updateSupplierBookingId(segmentInfos, bookingOrderNo);
			}
			log.info("Create PNR Response Booking {} and tf booking order no {} ", bookingId, bookingOrderNo);
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("ProcessTerms").build());
			if (StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getClientId()))
				storeSession(loginId);
		}
	}

	protected String startBook(TravelFusionRequests.CommonPrice commonPrice) {
		String tfReference = null;
		HttpUtils httpUtils = null;
		try {
			StartBookingType requestBody = buildStartBookingRequest(loginId, bookingOrderNo, commonPrice);
			httpUtils = getHttpUtil();
			httpUtils.setPostData(bindingService.startBookingMarshaller(requestBody));
			httpUtils.setTimeout(20 * 1000);
			String response = (String) httpUtils.getResponse(null).orElse("");
			StartBookingResponseType startBooking = bindingService.startBookingUnmarshaller(response);
			if (startBooking != null)
				tfReference = startBooking.getTFBookingReference();
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("StartBooking").build());
			if (StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getClientId()))
				storeSession(loginId);
		}
		return tfReference;
	}

	protected String checkBookingStatus(String tfReference) {
		String pnr = null;
		CheckBookingResponseType checkBooking = new CheckBookingResponseType();
		if (StringUtils.isNotBlank(tfReference)) {
			// CHeckBooking
			int tryCount = 0;
			boolean isSuccess = false;
			while (pnr == null && (bookingStatus != BOOKING_FAILED || bookingStatus != DUPLICATE)
					&& tryCount < BOOK_COUNT_RETRY) {
				try {
					Thread.sleep(SLEEP_TIME);
					checkBooking = checkBooking(loginId, tfReference);
					if (checkBooking.getSupplierReference() != null) {
						pnr = checkBooking.getSupplierReference().getValue();
						BookingUtils.updateAirlinePnr(segmentInfos, pnr);
					}
					bookingStatus = checkBooking.getStatus().value();
					tryCount++;
				} catch (InterruptedException e) {
					log.error(AirSourceConstants.AIR_SUPPLIER_PNR_THREAD, bookingId);
					throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
				} catch (JAXBException fe) {
					isSuccess = false;
					throw new SupplierUnHandledFaultException(StringUtils.join(fe.getErrorCode(), fe.getMessage()));
				} catch (IOException ioe) {
					isSuccess = false;
				}
			}
			setTicketNumber(checkBooking);
		}
		return pnr;
	}

	private void setTicketNumber(CheckBookingResponseType checkBooking) {
		isTicketRequired = false;
		if (checkBooking != null) {
			GetBookingDetailsResponseType getBookingDetailsResponse = getBookingDetails(loginId, checkBooking);
			// If we get the tickets from the supplier set it and make the TKrequired variable true
			if (getBookingDetailsResponse.getRouterHistory() != null
					&& getBookingDetailsResponse.getRouterHistory().getBookingRouter() != null
					&& getBookingDetailsResponse.getRouterHistory().getBookingRouter().getSupplierInfoList() != null
					&& CollectionUtils.isNotEmpty(getBookingDetailsResponse.getRouterHistory().getBookingRouter()
							.getSupplierInfoList().getSupplierInfo())) {
				for (RouterSupplierInfoType info : getBookingDetailsResponse.getRouterHistory().getBookingRouter()
						.getSupplierInfoList().getSupplierInfo()) {
					if (StringUtils.isNotEmpty(info.getDisplayName())
							&& info.getDisplayName().toUpperCase().contains("Ticket number for".toUpperCase())) {
						for (SegmentInfo segmentInfo : segmentInfos) {
							for (FlightTravellerInfo traveller : segmentInfo.getBookingRelatedInfo()
									.getTravellerInfo()) {
								if (info.getDisplayName().toUpperCase()
										.contains(StringUtils
												.join(getTitle(traveller.getTitle()), " ", traveller.getPaxKey())
												.toUpperCase())) {
									BookingUtils.updateTicketNumber(segmentInfos, info.getInfo(), traveller);
									isTicketRequired = true;
								}
							}
						}
					}
				}
			}
		}
	}

	private StartBookingType buildStartBookingRequest(String loginId, String bookingOrderNo,
			TravelFusionRequests.CommonPrice commonPrice) {
		StartBookingType request = bindingService.objReq.createStartBookingType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		request.setTFBookingReference(bookingOrderNo);
		request.setExpectedPrice(commonPrice);
		if (BooleanUtils.isTrue(supplierConfiguration.isTestCredential())) {
			FakeBookingType fakeBooking = new FakeBookingType();
			fakeBooking.setEnableFakeBooking(true);
			fakeBooking.setEnableFakeCardVerification(false);
			fakeBooking.setFakeBookingSimulatedDelaySeconds(BigInteger.valueOf(0));
			fakeBooking.setFakeBookingStatus(CommonFinalBookingStatusType.SUCCEEDED);
			request.setFakeBooking(fakeBooking);
		}
		return request;
	}

	private ProcessTermsType buildProcessTermsRequest(String loginId, boolean isHoldBooking, CreditCardInfo card) {
		ProcessTermsType request = bindingService.objReq.createProcessTermsType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		request.setRoutingId(getRouteId(segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getJourneyKey()));
		request.setOutwardId(getRouteId(TravelFusionUtils.getTripId(segmentInfos, false)));
		String returnId = TravelFusionUtils.getTripId(segmentInfos, true);
		if (StringUtils.isNotBlank(returnId))
			request.setReturnId(getRouteId(returnId));
		BookingProfileType bookingProfile = new BookingProfileType();
		if (card == null) {
			// BSP payment mode airline wise payment
			if (isBSPModeAllowed()) {
				bookingProfile.setCustomSupplierParameterList(createPaymentType(USE_PRE_REGISTERED_CARD, TRUE));
			} else {
				bookingProfile.setCustomSupplierParameterList(createPaymentType(USE_TF_PREPAY, TF_PREPAY_VALUE));
			}
		}
		bookingProfile.setTravellerList(getTravellersList());
		bookingProfile.setBillingDetails(getBillingDetails(card));
		bookingProfile.setContactDetails(getContactDetails());
		bookingProfile
				.setCustomSupplierParameterList(getCustomData(bookingProfile.getCustomSupplierParameterList(), card));
		request.setBookingProfile(bookingProfile);
		return request;
	}

	private boolean isBSPModeAllowed() {
		for (SegmentInfo segment : segmentInfos) {
			if (CollectionUtils.isEmpty(supplierConfiguration.getSupplierAdditionalInfo().getBspAllowedAirlines())
					|| !supplierConfiguration.getSupplierAdditionalInfo().getBspAllowedAirlines()
							.contains(segment.getPlatingCarrier(segment.getPriceInfoList().get(0)))) {
				return false;
			}
		}
		return true;
	}

	private BookingProfileContactDetailsType getContactDetails() {
		BookingProfileContactDetailsType contactDetails = new BookingProfileContactDetailsType();
		contactDetails.setName(getName(segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().get(0)));
		contactDetails.setAddress(getAddress(order.getBookingUserId(), true));
		contactDetails.setMobilePhone(getPhone());
		contactDetails.setEmail(getEmail());
		return contactDetails;
	}

	private CommonPhone getPhone() {
		CommonPhone phone = new CommonPhone();
		phone.setInternationalCode(
				getCommonString(ServiceCommunicatorHelper.getClientInfo().getCountryCode().replaceAll("\\W", "")));
		phone.setAreaCode(getCommonString(StringUtils.EMPTY));
		phone.setNumber(getCommonString(AirSupplierUtils.getContactNumber(deliveryInfo)));
		phone.setExtension(getCommonString(StringUtils.EMPTY));
		return phone;
	}

	private CommonString getEmail() {
		CommonString email = new CommonString();
		email.setValue(AirSupplierUtils.getEmailId(deliveryInfo));
		return email;
	}

	private BookingProfileTravellerList getTravellersList() {
		Period age = null;
		BookingProfileTravellerList travellerList = new BookingProfileTravellerList();
		List<FlightTravellerInfo> travellerInfos = segmentInfos.get(0).getTravellerInfo();
		LocalDate dateOfDeparture = segmentInfos.get(0).getDepartTime().toLocalDate();
		for (SegmentInfo segment : segmentInfos) {
			if (BooleanUtils.isTrue(segment.isReturnSegment)) {
				dateOfDeparture = segment.getDepartTime().toLocalDate();
				break;
			}
		}
		for (FlightTravellerInfo traveller : travellerInfos) {
			BookingProfileTravellerType traveler = new BookingProfileTravellerType();
			age = getAge(TravelFusionUtils.getTravellerDOB(traveller), dateOfDeparture);
			traveler.setAge(BigInteger.valueOf(age.getYears()));
			traveler.setName(getName(traveller));
			traveler.setCustomSupplierParameterList(getTravellerCustomParameters(traveller));
			travellerList.getTraveller().add(traveler);
		}
		return travellerList;
	}

	private CommonCustomSupplierParameterList getTravellerCustomParameters(FlightTravellerInfo traveller) {
		CommonCustomSupplierParameterList travellerCustomParameters = new CommonCustomSupplierParameterList();
		travellerCustomParameters.getCustomSupplierParameter().add(getCustomParameter(DATE_OF_BIRTH,
				TravelFusionUtils.getDate(TravelFusionUtils.getTravellerDOB(traveller))));
		SSRInformation ssrInfo = null;
		if (traveller.getSsrBaggageInfo() != null) {
			ssrInfo = traveller.getSsrBaggageInfo();
			travellerCustomParameters.getCustomSupplierParameter().add(getCustomParameter(
					traveller.getSsrBaggageInfo().getMiscInfo().getSeatCodeType(), ssrInfo.getCode()));
		}

		SSRInformation mealSSRInfo = null;
		if (traveller.getSsrMealInfo() != null) {
			mealSSRInfo = traveller.getSsrMealInfo();
			travellerCustomParameters.getCustomSupplierParameter()
					.add(getCustomParameter(MEALTYPE, mealSSRInfo.getCode()));
		}
		int count = 0;
		for (SegmentInfo segment : segmentInfos) {
			if (count != 0) {
				List<FlightTravellerInfo> travellerInfos = segment.getBookingRelatedInfo().getTravellerInfo();
				for (FlightTravellerInfo travellerSsr : travellerInfos) {
					if (travellerSsr.getSsrBaggageInfo() != null
							&& traveller.getFullName().equals(travellerSsr.getFullName())) {
						if (segment.isReturnSegment && travellerSsr.getSsrBaggageInfo().getMiscInfo().getSeatCodeType()
								.equals(OUTWARDLUGGAGEOPTIONS)) {
							ssrInfo = travellerSsr.getSsrBaggageInfo();
							String tagName = RETURNLUGGAGEOPTIONS;
							travellerCustomParameters.getCustomSupplierParameter()
									.add(getCustomParameter(tagName, ssrInfo.getCode()));
						}
						if (!segment.isReturnSegment) {
							travellerCustomParameters.getCustomSupplierParameter()
									.add(getCustomParameter(
											travellerSsr.getSsrBaggageInfo().getMiscInfo().getSeatCodeType(),
											ssrInfo.getCode()));
						}
					}
					// Travel Fusion donot support meal ssr for round trip outward will be added automatically to
					// return.
					if (traveller.getSsrMealInfo() != null
							&& traveller.getFullName().equals(travellerSsr.getFullName())) {
						mealSSRInfo = traveller.getSsrMealInfo();
						travellerCustomParameters.getCustomSupplierParameter()
								.add(getCustomParameter(MEALTYPE, mealSSRInfo.getCode()));
					}
				}
			}
			count++;
		}
		if (MapUtils.isNotEmpty(traveller.getFrequentFlierMap())) {
			traveller.getFrequentFlierMap().values().forEach(flierId -> {
				travellerCustomParameters.getCustomSupplierParameter().add(getCustomParameter(FREQUENT_FLYER, flierId));
			});
		}
		if (StringUtils.isNotBlank(traveller.getPassportNumber()))
			travellerCustomParameters.getCustomSupplierParameter()
					.add(getCustomParameter(PASSPORT_NUMBER, traveller.getPassportNumber()));
		if (!ObjectUtils.isEmpty(traveller.getExpiryDate()))
			travellerCustomParameters.getCustomSupplierParameter()
					.add(getCustomParameter(PASSPORT_EXPIRY, TravelFusionUtils.getDate(traveller.getExpiryDate())));
		if (StringUtils.isNotBlank(traveller.getPassportNationality())) {
			travellerCustomParameters.getCustomSupplierParameter()
					.add(getCustomParameter(PASSPORT_ISSUE_COUNTRY, traveller.getPassportNationality()));
			travellerCustomParameters.getCustomSupplierParameter()
					.add(getCustomParameter(NATIONALITY, traveller.getPassportNationality()));
		}

		return travellerCustomParameters;
	}

	private CommonName getName(FlightTravellerInfo traveller) {
		CommonName name = new CommonName();
		name.setTitle(getTitle(traveller.getTitle()));
		CommonNamePartListType nameList = new CommonNamePartListType();
		nameList.getNamePart().add(traveller.getFirstName());
		nameList.getNamePart().add(traveller.getLastName());
		name.setNamePartList(nameList);
		return name;
	}

	private BookingProfileBillingDetailsType getBillingDetails(CreditCardInfo card) {
		BookingProfileBillingDetailsType billingDetails = new BookingProfileBillingDetailsType();
		String name = supplierConfiguration.getSupplierCredential().getUserName();
		billingDetails.setName(getName(name));
		billingDetails.setAddress(getAddress(order.getBookingUserId(), true));
		if (card != null && StringUtils.isNotBlank(card.getCardNumber())) {
			billingDetails.setCreditCard(getcreditCard(card));
		}
		return billingDetails;
	}

	private BookingProfileCreditCardType getcreditCard(CreditCardInfo card) {
		BookingProfileCreditCardType creditCard = new BookingProfileCreditCardType();
		if (card != null) {
			creditCard.setCompany(getCommonString(""));
			creditCard.setCardType(getCommonString(TravelFusionUtils.getCardType(card.getCardType())));
			creditCard.setExpiryDate(getCommonString(getExpiryDate(card.getExpiry()).toString()));
			creditCard.setStartDate(getCommonString(CREDIT_CARD_START_DATE));
			creditCard.setIssueNumber(getCommonString(CREDIT_CARD_ISSUE_NUMBER));
			creditCard.setNumber(getCommonString(card.getCardNumber()));
			creditCard.setSecurityCode(getCommonString(card.getCvv()));
			CommonName holderName = new CommonName();
			CommonNamePartListType namePart = new CommonNamePartListType();
			Boolean cardInfoVal = segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getLinkavailablity();
			if (BooleanUtils.isTrue(cardInfoVal)) {
				holderName.setTitle(MR);
				namePart.getNamePart().addAll(Arrays.asList(card.getHolderName().split(" ")));
			} else
				namePart.getNamePart().add(card.getHolderName());
			holderName.setNamePartList(namePart);
			creditCard.setNameOnCard(holderName);
		}
		return creditCard;
	}


	private String getExpiryDate(String expiry) {
		String[] date = expiry.toString().split("/");
		String month = date[0];
		String year = date[1];
		String expDate = null;
		if (year.length() == 4)
			expDate = String.join("/", month, year.substring(year.length() - 2, year.length()));
		return expDate;
	}

	private CommonNamePartListType getNamePart(String name) {
		CommonNamePartListType namePart = new CommonNamePartListType();
		namePart.getNamePart().add(name);
		return namePart;
	}

	private CommonCustomSupplierParameterList getCustomData(CommonCustomSupplierParameterList commonCustomParameter,
			CreditCardInfo card) {
		if (commonCustomParameter == null)
			commonCustomParameter = bindingService.objReq.createCommonCustomSupplierParameterList();
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(END_USER_IP_ADDRESS, IP));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(END_USER_BROWSER_AGENT, BROWSER));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(REQUEST_ORIGIN, ORIGIN));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(POINT_OF_SALE, POS));
		Stream<String> userData =
				Arrays.asList(order.getDeliveryInfo().getEmails().get(0), order.getDeliveryInfo().getContacts().get(0),
						segmentInfos.get(0).getTravellerInfo().get(0).getFullName()).stream();
		commonCustomParameter.getCustomSupplierParameter()
				.add(getCustomParameter(USER_DATA, userData.collect(Collectors.joining(","))));
		return commonCustomParameter;
	}

	private CommonCustomSupplierParameterList createPaymentType(String tagName, String tagValue) {
		CommonCustomSupplierParameterList customParameters = new CommonCustomSupplierParameterList();
		CustomSupplierParameterType parameter = new CustomSupplierParameterType();
		parameter.setName(tagName);
		CustomSupplierParameterType.Value value = new CustomSupplierParameterType.Value();
		value.setValue(tagValue);
		parameter.setValue(value);
		customParameters.getCustomSupplierParameter().add(parameter);
		return customParameters;
	}

	protected CheckBookingResponseType checkBooking(String loginId, String TFref) throws JAXBException, IOException {
		CheckBookingResponseType checkBooking = null;
		HttpUtils httpUtils = null;
		try {
			CheckBookingType requestBody = bindingService.buildCheckBookingRequest(loginId, TFref);
			httpUtils = getHttpUtil();
			httpUtils.setPostData(bindingService.checkBookingMarshaller(requestBody));
			httpUtils.setTimeout(10 * 1000);
			String response = (String) httpUtils.getResponse(null).orElse("");
			checkBooking = bindingService.checkBookingUnmarshaller(response);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("CheckBook").build());
		}
		return checkBooking;
	}

	public double getSupplierChargeableAmount(CreditCardInfo card, TravelFusionResponses.CommonPrice price) {
		Double totalFare = getAmountBasedOnCurrency(price.getAmount().doubleValue(), price.getCurrency());
		Double supplierFare = 0d;
		if (price != null) {
			Double totalOrgFare = price.getAmount().doubleValue();
			supplierAmount = totalFare;
			supplierFare = totalOrgFare;
		}
		supplierChargeableAmount = new BigDecimal(supplierFare).setScale(2, BigDecimal.ROUND_HALF_UP);
		return supplierAmount;
	}
}
