package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.ADULT_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CANCELLATION;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CHECKED_IN;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CHILD_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.DIVIDING_FACTOR_2;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.HOLD_BAG;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INCLUDES;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INFANT_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.LARGE_CABIN_BAG;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.LUGGAGEOPTIONS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MEALTYPE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.OUTWARDLUGGAGEOPTIONS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.RETURNLUGGAGEOPTIONS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.SUPPLIER_CLASS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.TF_CHILD_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.TF_INFANT_AGE;
import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoPNRFoundException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.GetBookingDetailsType;
import TravelFusionResponses.BookingProfileContactDetailsType;
import TravelFusionResponses.BookingProfileType;
import TravelFusionResponses.CommonCustomSupplierParameterList;
import TravelFusionResponses.CommonPhone;
import TravelFusionResponses.CommonPrice;
import TravelFusionResponses.CommonRouter;
import TravelFusionResponses.CustomSupplierParameterType.Value;
import TravelFusionResponses.FlightLegData;
import TravelFusionResponses.GetBookingDetailsResponseType;
import TravelFusionResponses.PricePassengerPriceListType;
import TravelFusionResponses.RouterRequiredParameterListType;
import TravelFusionResponses.RouterRequiredParameterType;
import TravelFusionResponses.RouterSegmentType;
import TravelFusionResponses.RouterStopListType;
import TravelFusionResponses.RouterStopType;
import TravelFusionResponses.RouterSupplierFeatureConditionType;
import TravelFusionResponses.RouterSupplierFeatureOptionType;
import TravelFusionResponses.RouterSupplierFeatureType;
import TravelFusionResponses.RouterTravelClassType;
import lombok.experimental.SuperBuilder;

@SuperBuilder
final class TravelFusionBookingRetrieveManager extends TravelFusionServiceManager {

	private String supplierReference;
	private CommonRouter detailsRouter;
	private BookingProfileType bookingProfile;
	private CommonRouter termsRouter;
	private HttpUtils httpUtils;

	public GetBookingDetailsResponseType getBookingDetails() {

		try {
			GetBookingDetailsType request = buildGetBookingDetailsRequest();
			httpUtils = getHttpUtil();
			httpUtils.setPostData(bindingService.getBookingDetailsMarshaller(request));
			String response = httpUtils.getResponse(null).orElse("").toString();
			GetBookingDetailsResponseType bookingDetails = bindingService.getBookingDetailsUnmarshaller(response);
			return bookingDetails;
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS =
					StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "GetBookingDetails"),
							TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "GetBookingDetails"));
			listener.addLog(LogData.builder().key("TravelFusion-ImportPnr").logData(endPointRQRS)
					.type("GetBookingDetails").build());
			if (StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getClientId()))
				storeSession(loginId);
		}
	}

	private GetBookingDetailsType buildGetBookingDetailsRequest() {
		GetBookingDetailsType request = bindingService.objReq.createGetBookingDetailsType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		request.setSupplierReference(supplierReference);
		return request;
	}

	public AirImportPnrBooking updateBookingDetails(GetBookingDetailsResponseType bookingDetails) {
		// TripInfo
		AirImportPnrBooking pnrBooking = null;
		if (bookingDetails != null) {
			detailsRouter = bookingDetails.getRouterHistory().getDetailsRouter();
			termsRouter = bookingDetails.getRouterHistory().getTermsRouter();
			bookingProfile = bookingDetails.getBookingProfile();
			features = detailsRouter.getFeatures();
			if (Objects.nonNull(detailsRouter) && Objects.nonNull(bookingProfile) && Objects.nonNull(termsRouter)) {
				List<TripInfo> tripInfos = fetchTripInfo();
				DeliveryInfo deliveryInfo = fetchDeliveryInfo();
				pnrBooking = AirImportPnrBooking.builder().tripInfos(tripInfos).deliveryInfo(deliveryInfo).build();
				pnrBooking.setPnr(supplierReference);
			}
			return pnrBooking;
		} else {
			throw new NoPNRFoundException(AirSourceConstants.AIR_PNR_JOURNEY_UNAVAILABLE + supplierReference);
		}
	}

	private DeliveryInfo fetchDeliveryInfo() {
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		List<String> emailList = new ArrayList<>();
		List<String> contactList = new ArrayList<>();
		BookingProfileContactDetailsType contact = bookingProfile.getContactDetails();
		if (contact != null) {
			emailList.add(contact.getEmail().getValue());
			CommonPhone phone = contact.getMobilePhone() != null ? contact.getMobilePhone()
					: contact.getHomePhone() != null ? contact.getHomePhone() : contact.getWorkPhone();
			if (phone != null)
				contactList.add(phone.getInternationalCode().getValue() + phone.getNumber().getValue());
			deliveryInfo.setContacts(contactList);
			deliveryInfo.setEmails(emailList);
		}
		return deliveryInfo;
	}

	private List<TripInfo> fetchTripInfo() {
		List<TripInfo> tripInfos = new ArrayList<>();
		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(fetchSegments(detailsRouter));
		tripInfos.add(tripInfo);
		return tripInfos;
	}

	private List<SegmentInfo> fetchSegments(CommonRouter router) {
		List<SegmentInfo> segmentsList = new ArrayList<>();
		AtomicInteger segmentNumber = new AtomicInteger(0);
		Double additionalAmountOnBooking = 0.0;
		int paxCount = 0;
		List<FlightLegData> outwardLeg = new ArrayList<FlightLegData>();
		List<FlightLegData> returnLeg = new ArrayList<FlightLegData>();
		if (router.getGroupList() != null && router.getGroupList().getGroup() != null) {
			CommonPrice price = router.getGroupList().getGroup().get(0).getPrice();
			if (Objects.nonNull(router) && Objects.nonNull(router.getGroupList())) {
				if (Objects.nonNull(router.getGroupList().getGroup().get(0).getOutwardList().getOutward())) {
					outwardLeg = router.getGroupList().getGroup().get(0).getOutwardList().getOutward();
				}

				if (Objects.nonNull(router.getGroupList().getGroup().get(0).getReturnList())
						&& Objects.nonNull(router.getGroupList().getGroup().get(0).getReturnList().getReturn())) {
					returnLeg = router.getGroupList().getGroup().get(0).getReturnList().getReturn();
				}
			}
			paxCount = getPessengerNumberFromResponse(price, outwardLeg.get(0).getPrice());
			if (Objects.nonNull(price)) {
				additionalAmountOnBooking = getAdditionalTaxOnBookingPerPassenger(price, paxCount);
			}
			/*
			 * 
			 * We are keeping the price applicable on whole trip each passenger wise In case of onward simply divide by
			 * number of pax but In case of round trip we have to divide by (paxCount*2) as this price is for whole trip
			 * So to keep it as each pax wise used this
			 * 
			 */
			if (Objects.nonNull(returnLeg) && !returnLeg.isEmpty()) {
				roundTrip = true;
				additionalAmountOnBooking = additionalAmountOnBooking / DIVIDING_FACTOR_2;
			}
			double taxOnTrip = getTotalTaxOnEachLeg(outwardLeg, returnLeg, paxCount);
			for (FlightLegData leg : outwardLeg) {
				createSegment(segmentsList, leg, false, segmentNumber, additionalAmountOnBooking + taxOnTrip,
						getPriceList(price, leg));
			}
			segmentNumber.set(0);
			if (Objects.nonNull(returnLeg)) {
				for (FlightLegData leg : returnLeg) {
					createSegment(segmentsList, leg, true, segmentNumber, additionalAmountOnBooking + taxOnTrip,
							getPriceList(price, leg));
				}
			}
		}
		return segmentsList;
	}


	private int getPessengerNumberFromResponse(CommonPrice price, CommonPrice legPrice) {
		int passengerNumber = 0;
		if (Objects.nonNull(price) && Objects.nonNull(price.getPassengerPriceList())
				&& Objects.nonNull(price.getPassengerPriceList().getPassengerPrice())) {
			passengerNumber = price.getPassengerPriceList().getPassengerPrice().size();
		} else if (Objects.nonNull(legPrice) && Objects.nonNull(legPrice.getPassengerPriceList())
				&& Objects.nonNull(legPrice.getPassengerPriceList().getPassengerPrice())) {
			passengerNumber = legPrice.getPassengerPriceList().getPassengerPrice().size();
		}
		return passengerNumber;
	}

	private void createSegment(List<SegmentInfo> segmentsList, FlightLegData leg, boolean isReturnSegment,
			AtomicInteger segmentNumber, double amount, PricePassengerPriceListType passengerPriceList) {

		leg.getSegmentList().getSegment().forEach(segment -> {
			SegmentInfo segmentInfo = new SegmentInfo();
			segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(segment.getOrigin().getCode()));
			segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(segment.getDestination().getCode()));
			segmentInfo.setDepartTime(TravelFusionUtils.getResponseDate(segment.getDepartDate().getValue()));
			segmentInfo.setArrivalTime(TravelFusionUtils.getResponseDate(segment.getArriveDate().getValue()));
			segmentInfo.setFlightDesignator(createFlightDesignator(segment));
			segmentInfo.getDepartAirportInfo().setTerminal(segment.getOrigin().getTerminal());
			segmentInfo.getArrivalAirportInfo().setTerminal(segment.getDestination().getTerminal());
			segmentInfo.setIsReturnSegment(isReturnSegment);
			if (segment.getStopList() != null) {
				segmentInfo.setStops(segment.getStopList().getStop().size());
				segmentInfo.setStopOverAirports(TravelFusionUtils.getStopOverAirports(segment.getStopList()));
			}
			if (segment.getDuration() != null) {
				segmentInfo.setDuration(segment.getDuration().longValue());
			} else {
				segmentInfo.calculateDuration();
			}
			List<PriceInfo> priceInfos = new ArrayList<>();
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
			AirlineInfo platingCarrier = AirlineHelper.getAirlineInfo(detailsRouter.getGroupList().getGroup().get(0)
					.getOutwardList().getOutward().get(0).getSegmentList().getSegment().get(0).getOperator().getCode());
			priceInfo.getMiscInfo().setPlatingCarrier(platingCarrier);
			priceInfos.add(priceInfo);
			segmentInfo.setPriceInfoList(priceInfos);
			segmentInfo.setBookingRelatedInfo(getBookingRelatedInfo(leg.getSeatsRemaining(), segment, bookingProfile,
					segmentNumber, amount, passengerPriceList, isReturnSegment));
			segmentInfo.setSegmentNum(segmentNumber.getAndIncrement());
			segmentInfo.updateAirlinePnr(supplierReference);
			segmentsList.add(segmentInfo);
		});
	}

	private SegmentBookingRelatedInfo getBookingRelatedInfo(BigInteger seatsRemaining, RouterSegmentType segment,
			BookingProfileType bookingProfile, AtomicInteger segmentNumber, double amount,
			PricePassengerPriceListType passengerPriceList, boolean isReturnSegment) {
		List<FlightTravellerInfo> travellerInfoList = new ArrayList<>();
		bookingProfile.getTravellerList().getTraveller().forEach(traveller -> {
			FlightTravellerInfo travellerInfo = new FlightTravellerInfo();
			travellerInfo.setAge(traveller.getAge().intValue());
			travellerInfo.setFirstName(traveller.getName().getNamePartList().getNamePart().get(0));
			travellerInfo.setLastName(traveller.getName().getNamePartList().getNamePart().get(1));
			PaxType pax = traveller.getAge().intValue() <= TF_INFANT_AGE ? PaxType.INFANT
					: traveller.getAge().intValue() <= TF_CHILD_AGE ? PaxType.CHILD : PaxType.ADULT;
			int age = pax.equals(PaxType.ADULT) ? ADULT_AGE : pax.equals(PaxType.CHILD) ? CHILD_AGE : INFANT_AGE;
			travellerInfo.setPaxType(pax);
			// travellerInfo.setSupplierBookingId(pnr);
			travellerInfo.setFareDetail(createFareDetail(seatsRemaining, amount, passengerPriceList,
					segment.getTravelClass(), segmentNumber.get(), age, detailsRouter.getFeatures(), pax));
			updateSSR(travellerInfo, detailsRouter.getRequiredParameterList(),
					traveller.getCustomSupplierParameterList(), isReturnSegment);
			travellerInfoList.add(travellerInfo);
		});
		SegmentBookingRelatedInfo bookingRelatedInfo =
				SegmentBookingRelatedInfo.builder().travellerInfo(travellerInfoList).build();

		return bookingRelatedInfo;
	}

	private void updateSSR(FlightTravellerInfo travellerInfo, RouterRequiredParameterListType requiredParameterList,
			CommonCustomSupplierParameterList customSupplierParameterList, boolean isReturnSegment) {
		customSupplierParameterList.getCustomSupplierParameter().forEach(parameter -> {
			if (parameter.getName().equals("DateOfBirth"))
				travellerInfo.setDob(
						LocalDate.parse(parameter.getValue().getValue(), DateTimeFormatter.ofPattern("dd/MM/yyyy")));
			if (!isReturnSegment && parameter.getName().equals(LUGGAGEOPTIONS)
					|| parameter.getName().equals(OUTWARDLUGGAGEOPTIONS)) {
				requiredParameterList.getRequiredParameter().forEach(reqPrameter -> {
					if (reqPrameter.getName().equals(LUGGAGEOPTIONS)
							|| reqPrameter.getName().equals(OUTWARDLUGGAGEOPTIONS)) {
						travellerInfo.setSsrBaggageInfo(setSSR(reqPrameter, parameter.getValue()));
					}
				});
			}
			if (isReturnSegment && parameter.getName().equals(RETURNLUGGAGEOPTIONS)) {
				requiredParameterList.getRequiredParameter().forEach(reqPrameter -> {
					if (reqPrameter.getName().equals(RETURNLUGGAGEOPTIONS)) {
						travellerInfo.setSsrBaggageInfo(setSSR(reqPrameter, parameter.getValue()));
					}
				});
			}
			if (!isReturnSegment && parameter.getName().equals(MEALTYPE)) {
				requiredParameterList.getRequiredParameter().forEach(reqPrameter -> {
					if (reqPrameter.getName().equals(MEALTYPE)) {
						travellerInfo.setSsrMealInfo(setMealSSR(reqPrameter.getDisplayText(), parameter.getValue()));
					}
				});
			}
		});
	}

	private SSRInformation setMealSSR(String displayText, Value value) {
		SSRInformation ssrMealInfo = new SSRInformation();
		// retrieving meal options from string.
		Pattern pattern1 = Pattern.compile("[A-Za-z]+[\\s]");
		Pattern pattern2 = Pattern.compile("[\\d{1,9}]+[\\.]+[\\d{1,5}]+[\\s]+[A-Za-z]+");
		String[] breaked = displayText.split(":")[1].split(",");
		for (String option : breaked) {
			if (option.trim().split(" ")[0].equals(value.getValue())) {
				Matcher matcher1 = pattern1.matcher(option);
				Matcher matcher2 = pattern2.matcher(option);
				while (matcher1.find()) {
					ssrMealInfo.setDesc(matcher1.group());
				}
				while (matcher2.find()) {
					ssrMealInfo.setAmount(getAmountBasedOnCurrency(Double.parseDouble(matcher2.group().split(" ")[0]),
							matcher2.group().split(" ")[1]));
				}
			}
		}
		return ssrMealInfo;
	}

	private SSRInformation setSSR(RouterRequiredParameterType reqPrameter, Value value) {
		SSRInformation ssrInfo = new SSRInformation();
		// retrieving luggage options from string.
		Pattern pattern1 = Pattern.compile("[\\d{1,3}]+[A-Za-z]+");
		Pattern pattern2 = Pattern.compile("[\\d{1,9}]+[\\.]+[\\d{1,5}]+[\\s]+[A-Za-z]+");
		String[] breaked = reqPrameter.getDisplayText().split(":")[1].split(",");

		for (String option : breaked) {
			if (option.trim().split(" ")[0].equals(value.getValue())) {
				Matcher matcher1 = pattern1.matcher(option);
				Matcher matcher2 = pattern2.matcher(option);
				while (matcher1.find()) {
					ssrInfo.setDesc(matcher1.group());
				}
				while (matcher2.find()) {
					ssrInfo.setAmount(getAmountBasedOnCurrency(Double.parseDouble(matcher2.group().split(" ")[0]),
							matcher2.group().split(" ")[1]));
				}
			}
		}
		return ssrInfo;
	}
}
