package com.tgs.services.fms.sources.travelfusion;

import TravelFusionRequests.TfClassEnumerationType;
import lombok.Getter;

@Getter
public enum TravelFusionCabinClass {
	// Economy Without Restrictions and Economy With Restrictions are kept same check it for differences.
	ECONOMY("Economy With Restrictions", TfClassEnumerationType.ECONOMY_WITH_RESTRICTIONS),
	ECONOMY_WITHOUT_RESTRICTIONS("Economy Without Restrictions", TfClassEnumerationType.ECONOMY_WITHOUT_RESTRICTIONS),
	PREMIUM_ECONOMY("Economy Premium", TfClassEnumerationType.ECONOMY_PREMIUM),
	BUSINESS("Business", TfClassEnumerationType.BUSINESS),
	FIRST("First", TfClassEnumerationType.FIRST);

	private String value;
	private TfClassEnumerationType cabin;

	private TravelFusionCabinClass(String value, TfClassEnumerationType cabin) {
		this.value = value;
		this.cabin = cabin;
	}


	public static String getEnumByString(String code) {
		for (TravelFusionCabinClass enumCode : TravelFusionCabinClass.values()) {
			if (enumCode.value.equals(code))
				return enumCode.name();
		}
		return null;
	}
}
