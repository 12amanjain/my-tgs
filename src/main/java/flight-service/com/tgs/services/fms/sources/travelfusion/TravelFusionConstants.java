package com.tgs.services.fms.sources.travelfusion;

import com.tgs.services.fms.sources.AirSourceConstants;
import eu.bitwalker.useragentutils.Browser;
import java.math.BigInteger;

public class TravelFusionConstants {

	public static final BigInteger MAX_HOPS = new BigInteger("4");
	public static final BigInteger MAX_CHANGES = new BigInteger("2");
	public static final String MODE = "plane";
	public static final String BOOKING_SUCCESS = "Succeeded";
	public static final String BOOKING_ON_HOLD = "BookingOnHold";
	public static final String AUTH_FAILED = "No Response Error";
	public static final String BOOKING_FAILED = "Failed";
	public static final String DUPLICATE = "Duplicate";
	public static final String USE_PRE_REGISTERED_CARD = "UseCardPreRegisteredWithSupplier";
	public static final String MOBILE_CODE = "91";
	public static final String DATE_OF_BIRTH = "DateOfBirth";
	public static final String MASTER = "Master";
	public static final String MISS = "Ms";
	public static final String INCLUDE_FEATURES = "IncludeStructuredFeatures";
	public static final String YQ4 = "YQ4";
	public static final String CGST = "CGST";
	public static final String SGST = "SGST";
	public static final String PROVINCE = "OT";
	public static final String HOLD_BAG = "HoldBag";
	public static final String LARGE_CABIN_BAG = "LargeCabinBag";
	public static final String SUPPLIER_CLASS = "SupplierClass";
	public static final String TRAVELLER_TYPE = "TravellerType";
	public static final String CHD = "CHD";
	public static final String MAX_WEIGHT = "MaxWeight";
	public static final String WEIGHT = "Weight";
	public static final String POS = "IN";
	public static final String IP = "1.1.1.1";
	public static final String END_USER_IP_ADDRESS = "EndUserIPAddress";
	public static final String END_USER_BROWSER_AGENT = "EndUserBrowserAgent";
	public static final String REQUEST_ORIGIN = "RequestOrigin";
	public static final String POINT_OF_SALE = "PointOfSale";
	public static final String ORIGIN = "India-TripJack.com";
	public static final String INC_FEATURES = "y";
	public static final String USER_DATA = "UserData";
	public static final int ADULT_AGE = 30;
	public static final int CHILD_AGE = 7;
	public static final int INFANT_AGE = 0;
	public static final int TIME_OUT = AirSourceConstants.TIME_OUT_IN_SECONDS;
	public static final int SLEEP_TIME = 5000;
	public static final String CREDIT_CARD_START_DATE = "01/20";
	public static final String CREDIT_CARD_ISSUE_NUMBER = "0";
	public static final String MR = "Mr";
	public static final String CHECKED_IN = "checked-in";
	public static final String INCLUDES = "includes";
	public static final String CANCELLATION = "Cancellation";
	public static final String UNKNOWN = "Unknown";
	public static final String LUGGAGEOPTIONS = "LuggageOptions";
	public static final String RETURNLUGGAGEOPTIONS = "ReturnLuggageOptions";
	public static final String OUTWARDLUGGAGEOPTIONS = "OutwardLuggageOptions";
	public static final String MEALTYPE = "MealType";
	public static final String BROWSER = "Mozilla/5.0 (X11; Linux x86_64; rv:12.0)";
	public static final String USE_TF_PREPAY = "UseTFPrepay";
	public static final String TF_PREPAY_VALUE = "Always";
	public static final int TF_CHILD_AGE = 12;
	public static final int TF_INFANT_AGE = 2;
	public static final int SEARCH_SLEEP_TIME = 2000;
	public static final String FULLCARDNAMEBREAKDOWN = "FullCardNameBreakdown";
	public static final int BOOK_COUNT_RETRY = 6;
	public static final int SEARCH_COUNT = 3;

	// Data Read timeouts
	public static final int START_ROUTING_TIMEOUT = 30 * 1000;


	// card type
	public static final String VISA = "Visa Credit";
	public static final String MASTERCARD = "MasterCard";
	public static final String AMERICAN_EXPRESS = "American Express";
	public static final String DINERS_CLUB = "Diners Club";
	public static final String FREQUENT_FLYER = "FrequentFlyerNumber";
	public static final String PASSPORT_NUMBER = "PassportNumber";
	public static final String PASSPORT_ISSUE_COUNTRY = "PassportCountryOfIssue";
	public static final String NATIONALITY = "Nationality";
	public static final String PASSPORT_EXPIRY = "PassportExpiryDate";
	public static final String ECONOMY_WITHOUT_RESTRICTION = "Economy Without Restrictions";
	public static final int DIVIDING_FACTOR_2 = 2;
	public static final int DIVIDING_FACTOR_1 = 1;
	public static final String BANK_HANDLING_CHARGE = "Bank Handling Charge";
	public static final String BANK_CONVERSION_CHARGE = "Bank Conversion Charge";
	public static final String CREDIT_CARD_SURCHARGE = "Credit card surcharge";
	public static final String TRUE = "true";

}

