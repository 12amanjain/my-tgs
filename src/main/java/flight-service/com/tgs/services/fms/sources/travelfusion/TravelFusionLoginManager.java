package com.tgs.services.fms.sources.travelfusion;

import java.io.IOException;
import javax.xml.bind.JAXBException;

import com.tgs.services.fms.utils.AirSupplierUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.LoginType;
import lombok.experimental.SuperBuilder;

@SuperBuilder
final class TravelFusionLoginManager extends TravelFusionServiceManager {

	public String login() {
		HttpUtils httpUtils = null;
		try {
			LoginType req = buildLoginRequest();
			httpUtils = getHttpUtil();
			httpUtils.setPostData(bindingService.loginMarshaller(req, LoginType.class));
			String response = httpUtils.getResponse(null).orElse("").toString();
			return bindingService.loginUnmarshaller(response);
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(listenerKey).visibilityGroups(AirSupplierUtils.getLogVisibility())
					.logData(endPointRQRS).type("Login").build());
		}
	}

	private LoginType buildLoginRequest() {
		LoginType request = bindingService.objReq.createLoginType();
		request.setUsername(supplierConfiguration.getSupplierCredential().getUserName());
		request.setPassword(supplierConfiguration.getSupplierCredential().getPassword());
		return request;
	}
}
