package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.FULLCARDNAMEBREAKDOWN;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.LUGGAGEOPTIONS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MEALTYPE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.OUTWARDLUGGAGEOPTIONS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.RETURNLUGGAGEOPTIONS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.DIVIDING_FACTOR_2;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.DIVIDING_FACTOR_1;
import java.io.IOException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.JAXBException;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.*;
import com.tgs.utils.exception.air.SupplierRemoteException;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.mchange.v1.util.ListUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.MealSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.ProcessDetailsType;
import TravelFusionResponses.CommonPrice;
import TravelFusionResponses.FlightLegData;
import TravelFusionResponses.LuggageFeesInfoType;
import TravelFusionResponses.ProcessDetailsResponseType;
import TravelFusionResponses.RouterRequiredParameterListType;
import TravelFusionResponses.RouterRequiredParameterType;
import TravelFusionResponses.TaxItemType;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
@Getter
final class TravelFusionReviewManager extends TravelFusionServiceManager {
	private boolean isReviewSuccess;

	public TripInfo processDetails(TripInfo selectedTrip) {
		HttpUtils httpUtils = null;
		TripInfo selectedTripInfo = null;
		try {
			ProcessDetailsType requestBody = buildRequestBody(selectedTrip, loginId);
			httpUtils = HttpUtils.builder().urlString(getURL()).postData(bindingService.reviewMarshaller(requestBody))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			selectedTripInfo = selectedTrip;
			httpUtils.setTimeout(120 * 1000);
			String response = httpUtils.getResponse(null).orElse("").toString();
			ProcessDetailsResponseType reviewResponse = bindingService.processDetailsUnmarshaller(response);
			isErrorInResponse(reviewResponse);
			if (!parseResponse(reviewResponse, selectedTrip)) {
				selectedTripInfo = null;
				isReviewSuccess = false;
			}
			setCardNameInfo(reviewResponse.getRouter().getRequiredParameterList(),
					selectedTripInfo.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo());
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("ProcessDetails").build());
			if (StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getClientId()))
				storeSession(loginId);
		}
		return selectedTripInfo;
	}

	private boolean isErrorInResponse(ProcessDetailsResponseType reviewResponse) {
		if (reviewResponse == null || reviewResponse.getRouter() == null
				|| reviewResponse.getRouter().getGroupList() == null) {
			throw new NoSeatAvailableException("ProcessDetails Router is empty");
		} else if (StringUtils.isNotBlank(reviewResponse.getRouter().getGroupList().getEcode())
				&& StringUtils.isNotBlank(reviewResponse.getRouter().getGroupList().getEdetail())) {
			throw new NoSeatAvailableException(StringUtils.join(reviewResponse.getRouter().getGroupList().getEtext(),
					" ", reviewResponse.getRouter().getGroupList().getEdetail()));
		}
		return false;
	}

	private ProcessDetailsType buildRequestBody(TripInfo selectedTrip, String loginId) {
		ProcessDetailsType requestBody = bindingService.objReq.createProcessDetailsType();
		requestBody.setLoginId(loginId);
		requestBody.setXmlLoginId(loginId);
		requestBody.setRoutingId(getRouteId(
				selectedTrip.getSegmentInfos().get(0).getPriceInfoList().get(0).getMiscInfo().getJourneyKey()));
		requestBody.setOutwardId(getRouteId(TravelFusionUtils.getTripId(selectedTrip.getSegmentInfos(), false)));
		String returnId = TravelFusionUtils.getTripId(selectedTrip.getSegmentInfos(), true);
		if (StringUtils.isNotBlank(returnId))
			requestBody.setReturnId(getRouteId(returnId));
		return requestBody;
	}

	private boolean parseResponse(ProcessDetailsResponseType reviewResponse, TripInfo selectedTrip) {
		boolean isSuccess = false;
		if (reviewResponse == null) {
			throw new NoSeatAvailableException();
		}
		LuggageFeesInfoType luggageSsr = null;
		if (reviewResponse.getFeesList() != null && reviewResponse.getFeesList().getLuggage() != null)
			luggageSsr = reviewResponse.getFeesList().getLuggage();
		RouterRequiredParameterType mealParameter =
				isMealPresent(reviewResponse.getRouter().getRequiredParameterList());
		int retSegCount = 0;
		int fromSegCount = 0;
		RouterRequiredParameterListType parameterList = reviewResponse.getRouter().getRequiredParameterList();
		for (SegmentInfo segment : selectedTrip.getSegmentInfos()) {
			if (luggageSsr != null) {
				if (segment.getSsrInfo() == null) {
					segment.setSsrInfo(new HashMap<>());
				}
				segment.getSsrInfo().put(SSRType.BAGGAGE,
						getSsrInfo(luggageSsr, parameterList, segment, segment.isReturnSegment));
			}
			if (!searchQuery.isIntlReturn() && mealParameter != null) {
				segment.getSsrInfo().put(SSRType.MEAL, getMealSsr(mealParameter.getDisplayText()));
			}
			if (segment.getIsReturnSegment()) {

				segment.setDepartTime(TravelFusionUtils.getResponseDate(
						reviewResponse.getRouter().getGroupList().getGroup().get(0).getReturnList().getReturn().get(0)
								.getSegmentList().getSegment().get(retSegCount).getDepartDate().getValue()));
				segment.setArrivalTime(TravelFusionUtils.getResponseDate(
						reviewResponse.getRouter().getGroupList().getGroup().get(0).getReturnList().getReturn().get(0)
								.getSegmentList().getSegment().get(retSegCount++).getArriveDate().getValue()));
			} else {
				segment.setDepartTime(TravelFusionUtils.getResponseDate(
						reviewResponse.getRouter().getGroupList().getGroup().get(0).getOutwardList().getOutward().get(0)
								.getSegmentList().getSegment().get(fromSegCount).getDepartDate().getValue()));

				segment.setArrivalTime(TravelFusionUtils.getResponseDate(
						reviewResponse.getRouter().getGroupList().getGroup().get(0).getOutwardList().getOutward().get(0)
								.getSegmentList().getSegment().get(fromSegCount++).getArriveDate().getValue()));
			}
		}
		features = reviewResponse.getRouter().getFeatures();
		if (updateFareDetails(reviewResponse, selectedTrip)) {
			isReviewSuccess = true;
			isSuccess = true;
		}
		return isSuccess;
	}

	private RouterRequiredParameterType isMealPresent(RouterRequiredParameterListType requiredParameterList) {
		if (Objects.nonNull(requiredParameterList)) {
			for (RouterRequiredParameterType parameter : requiredParameterList.getRequiredParameter()) {
				if (MEALTYPE.equals(parameter.getName())) {
					return parameter;
				}
			}
		}
		return null;
	}

	private List<? extends SSRInformation> getMealSsr(String mealOptions) {
		List<MealSSRInformation> mealSSRInformations = new ArrayList<>();
		// retrieving meal options from string.
		Pattern pattern1 = Pattern.compile("[A-Za-z]+[\\s]");
		Pattern pattern2 = Pattern.compile("[\\d{1,9}]+[\\.]+[\\d{1,5}]+[\\s]+[A-Za-z]+");
		String[] breaked = mealOptions.split(":")[1].split(",");
		for (String option : breaked) {
			MealSSRInformation mealSSRInformation = new MealSSRInformation();
			mealSSRInformation.setCode(option.trim().split(" ")[0]);
			Matcher matcher1 = pattern1.matcher(option);
			Matcher matcher2 = pattern2.matcher(option);
			StringBuffer desc = new StringBuffer();
			while (matcher1.find()) {
				desc.append(matcher1.group());
			}
			mealSSRInformation.setDesc(desc.toString());
			while (matcher2.find()) {
				mealSSRInformation.setAmount(getAmountBasedOnCurrency(
						Double.parseDouble(matcher2.group().split(" ")[0]), matcher2.group().split(" ")[1]));
			}
			mealSSRInformations.add(mealSSRInformation);
		}
		return mealSSRInformations;
	}

	private List<? extends SSRInformation> getSsrInfo(LuggageFeesInfoType luggageSsr,
			RouterRequiredParameterListType parameterList, SegmentInfo segment, boolean isReturnSegment) {
		String paramLabel = null;
		for (RouterRequiredParameterType parameter : parameterList.getRequiredParameter()) {
			if (parameter.getName().equals(LUGGAGEOPTIONS)) {
				paramLabel = LUGGAGEOPTIONS;
				return getLuggageSSr(segment, parameter, paramLabel, isReturnSegment);
			}
			if (parameter.getName().equals(OUTWARDLUGGAGEOPTIONS) && !isReturnSegment) {
				paramLabel = OUTWARDLUGGAGEOPTIONS;
				return getLuggageSSr(segment, parameter, paramLabel, isReturnSegment);
			}
			if (parameter.getName().equals(RETURNLUGGAGEOPTIONS) && isReturnSegment) {
				paramLabel = OUTWARDLUGGAGEOPTIONS;
				return getLuggageSSr(segment, parameter, paramLabel, isReturnSegment);
			}

		}
		return null;
	}

	private List<? extends SSRInformation> getLuggageSSr(SegmentInfo segmentInfo, RouterRequiredParameterType parameter,
			String paramLabel, boolean isReturnSegment) {
		List<BaggageSSRInformation> ssrInformations = new ArrayList<>();

		Pattern pattern1 = Pattern.compile("[\\d{1,3}]");
		Pattern pattern2 = Pattern.compile("[\\d{1,9}]+[\\.]+[\\d{1,5}]+[\\s]+[A-Za-z]+");

		String[] breaked = parameter.getDisplayText().split(":")[1].split(",");
		for (String option : breaked) {
			// breaking the string to get details of SSR
			String testEnvDesc = option.trim().split(" ", 2)[1].replace("(", "").replace(")", "");
			String[] parseElements = option.trim().split(" ", 2)[1].replace("(", "").replace(")", "").split("-");
			BaggageSSRInformation ssrInformation = new BaggageSSRInformation();

			Matcher matcher1 = pattern1.matcher(parseElements[0]);
			Matcher matcher2 = pattern2.matcher(parseElements[2]);

			ssrInformation.setCode(option.trim().split(" ", 2)[0]);
			ssrInformation.getMiscInfo().setSeatCodeType(paramLabel);
			while (matcher1.find()) {
				ssrInformation.setQuantity(Integer.parseInt(matcher1.group()));
			}
			String quantity = parseElements[0].replaceAll(" ", "").toLowerCase();
			String desc = parseElements[1].replaceAll(" ", "").toLowerCase().replaceAll("total", "");
			if (supplierConfiguration.isTestCredential()) {
				ssrInformation.setDesc(testEnvDesc);
			} else {
				ssrInformation.setDesc(quantity + " " + desc);
			}
			if (isReturnSegment && paramLabel.equals(LUGGAGEOPTIONS))
				ssrInformation.setAmount(null);
			else {
				if (segmentInfo.getSegmentNum().intValue() == 0) {
					while (matcher2.find()) {
						ssrInformation.setAmount(getAmountBasedOnCurrency(
								Double.parseDouble(matcher2.group().split(" ")[0]), matcher2.group().split(" ")[1]));
					}
				} else {
					ssrInformation.setAmount(null);
				}
			}
			ssrInformations.add(ssrInformation);
		}
		return ssrInformations;

	}

	private boolean updateFareDetails(ProcessDetailsResponseType reviewResponse, TripInfo selectedTrip) {
		boolean isSuccess = false;
		CommonPrice price = null;
		double additionalCharges = 0.0;
		List<TripInfo> trips = selectedTrip.splitTripInfo(false);
		int paxCount = AirUtils.getPaxCount(searchQuery, true);
		List<FlightLegData> outwardLeg = new ArrayList<FlightLegData>();
		List<FlightLegData> returnLeg = new ArrayList<FlightLegData>();
		price = reviewResponse.getRouter().getGroupList().getGroup().get(0).getPrice();


		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
				for (PaxType paxFd : priceInfo.getFareDetails().keySet()) {
					priceInfo.getFareDetail(paxFd).setFareComponents(new HashMap<>());
				}
			}
		}

		if (Objects.nonNull(price)) {
			additionalCharges = getAdditionalTaxOnBookingPerPassenger(price, paxCount);
		}


		/**
		 * 
		 * We are keeping the price applicable on whole trip each passenger wise In case of onward simply divide by
		 * number of pax but In case of round trip we have to divide by (paxCount*2) as this price is for whole trip So
		 * to keep it as each pax wise used this
		 */
		if ((searchQuery.isIntlReturn() || searchQuery.isDomesticReturn())) {
			roundTrip = true;
			additionalCharges = additionalCharges / DIVIDING_FACTOR_2;
		}
		if (Objects.nonNull(reviewResponse.getRouter()) && Objects.nonNull(reviewResponse.getRouter().getGroupList())) {
			if (Objects.nonNull(
					reviewResponse.getRouter().getGroupList().getGroup().get(0).getOutwardList().getOutward())) {
				outwardLeg = reviewResponse.getRouter().getGroupList().getGroup().get(0).getOutwardList().getOutward();
			}

			if (Objects.nonNull(reviewResponse.getRouter().getGroupList().getGroup().get(0).getReturnList()) && Objects
					.nonNull(reviewResponse.getRouter().getGroupList().getGroup().get(0).getReturnList().getReturn())) {
				returnLeg = reviewResponse.getRouter().getGroupList().getGroup().get(0).getReturnList().getReturn();
			}
		}

		double taxOnTrip = getTotalTaxOnEachLeg(outwardLeg, returnLeg, paxCount);
		if (!(reviewResponse.getRouter().getGroupList().getGroup().isEmpty() && reviewResponse.getRouter()
				.getGroupList().getGroup().get(0).getOutwardList().getOutward().isEmpty())) {
			int segCount = 0;
			for (FlightLegData leg : reviewResponse.getRouter().getGroupList().getGroup().get(0).getOutwardList()
					.getOutward()) {
				populatePriceInfo(leg, reviewResponse.getRoutingId().getValue(), additionalCharges + taxOnTrip,
						getPriceList(price, leg), leg.getSegmentList().getSegment().get(segCount),
						trips.get(0).getSegmentInfos().get(0), segCount);
				segCount++;
			}

			segCount = 0;
			if (searchQuery.isIntlReturn() || searchQuery.isDomesticReturn()) {
				for (FlightLegData leg : reviewResponse.getRouter().getGroupList().getGroup().get(0).getReturnList()
						.getReturn()) {
					populatePriceInfo(leg, reviewResponse.getRoutingId().getValue(), additionalCharges + taxOnTrip,
							getPriceList(price, leg), leg.getSegmentList().getSegment().get(segCount),
							trips.get(1).getSegmentInfos().get(0), segCount);
					segCount++;
				}
			}
			isSuccess = true;
		}
		return isSuccess;
	}

	private void setCardNameInfo(RouterRequiredParameterListType requiredParameterList, PriceMiscInfo miscInfo) {
		requiredParameterList.getRequiredParameter().forEach(parameter -> {
			if (parameter.getName().equals(FULLCARDNAMEBREAKDOWN)) {
				miscInfo.setLinkavailablity(true);
			}
		});
	}
}
