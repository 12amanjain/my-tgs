package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.ADULT_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.BROWSER;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CHILD_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.END_USER_BROWSER_AGENT;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.END_USER_IP_ADDRESS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INCLUDE_FEATURES;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INC_FEATURES;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INFANT_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.IP;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.ORIGIN;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.POINT_OF_SALE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.POS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.REQUEST_ORIGIN;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.SEARCH_COUNT;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.SEARCH_SLEEP_TIME;
import java.io.IOException;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.xml.bind.JAXBException;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.BookingProfileType;
import TravelFusionRequests.CheckRoutingType;
import TravelFusionRequests.OperatorFilterType;
import TravelFusionRequests.StartRoutingOperatorListType;
import TravelFusionRequests.CommonCustomSupplierParameterList;
import TravelFusionRequests.CommonId;
import TravelFusionRequests.CommonString;
import TravelFusionRequests.StartRoutingDatesType;
import TravelFusionRequests.StartRoutingLocationTypeType;
import TravelFusionRequests.StartRoutingTraveller;
import TravelFusionRequests.StartRoutingTravellerList;
import TravelFusionRequests.StartRoutingType;
import TravelFusionResponses.PricePassengerPriceListType;
import TravelFusionRequests.TfClassEnumerationType;
import TravelFusionResponses.CheckRoutingIDPairType;
import TravelFusionResponses.CheckRoutingResponseType;
import TravelFusionResponses.CommonPrice;
import TravelFusionResponses.CommonRouter;
import TravelFusionResponses.CommonRouterList;
import TravelFusionResponses.FlightLegData;
import TravelFusionResponses.RouterGroupType;
import TravelFusionResponses.RouterSegmentType;
import TravelFusionResponses.TaxItemType;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@NoArgsConstructor
@Slf4j
final class TravelFusionSearchManager extends TravelFusionServiceManager {

	AirSearchResult airSearchResult;
	HttpUtils searchRouting;
	HttpUtils checkRouting;

	public AirSearchResult doSearch() {
		boolean searchStatus = false;
		searchRouting = null;
		checkRouting = null;
		CheckRoutingResponseType checkRoutingresponse = null;
		try {
			airSearchResult = new AirSearchResult();
			initSearchRoute();
			String searchRouteResponse = searchRouting.getResponse(null).orElse("").toString();
			String routingId = bindingService.searchRouteUnmarshaller(searchRouteResponse);
			checkRouting = buildCheckRouteRequest(routingId);
			int tryCount = 0;
			do {
				Thread.sleep(SEARCH_SLEEP_TIME);
				String checkRoutingResponse = checkRouting.getResponse(null).orElse("").toString();
				checkRoutingresponse = bindingService.checkRoutingUnmarshaller(checkRoutingResponse);
				if (checkRoutingresponse != null)
					searchStatus = getSearchStatus(checkRoutingresponse.getRouterList());
				tryCount++;
			} while (!searchStatus && tryCount < SEARCH_COUNT);
			if (checkRoutingresponse != null) {
				parseSearchResponse(checkRoutingresponse);
			}
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (InterruptedException e) {
			throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(searchRouting.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(searchRouting.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
					.type("TF-SearchRouting").build());
			if (checkRouting != null) {
				endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(checkRouting.getPostData(), "Request"),
						TravelFusionUtils.formatRQRS(checkRouting.getResponseString(), "Response"));
				listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(endPointRQRS)
						.type("TF-CheckRouting").build());
			}
			if (StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getClientId()))
				storeSession(loginId);
		}
		return airSearchResult;
	}

	private HttpUtils buildCheckRouteRequest(String routingId) {
		try {
			CheckRoutingType checkRoutingRequest = checkRoutingRequestBody(routingId, loginId);
			checkRouting = HttpUtils.builder().urlString(getURL())
					.postData(bindingService.checkRoutingMarshaller(checkRoutingRequest))
					.headerParams(getHeaderParams()).proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			checkRouting.setTimeout(7 * 1000);
		} catch (JAXBException e) {
			throw new NoSearchResultException(e.getMessage());
		}
		return checkRouting;
	}

	private HttpUtils initSearchRoute() {
		try {
			StartRoutingType searchRequest = startRoutingGenerateRequestBody(loginId);
			searchRouting = HttpUtils.builder().urlString(getURL())
					.postData(bindingService.searchRouteMarshaller(searchRequest)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			searchRouting.setTimeout(4 * 1000);
		} catch (JAXBException e) {
			throw new NoSearchResultException(e.getMessage());
		}
		return searchRouting;
	}

	private Boolean getSearchStatus(CommonRouterList routerList) throws InterruptedException {
		boolean searchStatus = false;
		if (routerList == null || CollectionUtils.isEmpty(routerList.getRouter())) {
			// SearchResult Not available
			searchStatus = true;
		} else {
			for (CommonRouter router : routerList.getRouter()) {
				if (router.isComplete())
					searchStatus = true;
				else {
					searchStatus = false;
					break;
				}
			}
		}
		return searchStatus;
	}

	private void parseSearchResponse(CheckRoutingResponseType response) {
		if (response != null && response.getRouterList() != null
				&& CollectionUtils.isNotEmpty(response.getRouterList().getRouter())) {
			populateTripInfo(response);
		}
	}

	private void populateTripInfo(CheckRoutingResponseType response) {
		CommonRouterList routerList = response.getRouterList();
		String JourneyKey = response.getRoutingId().getValue();
		List<TripInfo> onwardTripInfos = new ArrayList<>();
		List<TripInfo> returnTripInfos = new ArrayList<>();
		Map<String, List<TripInfo>> tripInfosMap = new HashMap<>();
		try {
			for (CommonRouter route : routerList.getRouter()) {
				if (route != null && route.getGroupList() != null
						&& CollectionUtils.isNotEmpty(route.getGroupList().getGroup())) {
					features = route.getFeatures();
					for (RouterGroupType group : route.getGroupList().getGroup()) {
						if (group != null) {
							for (FlightLegData outwardLeg : group.getOutwardList().getOutward()) {
								if (outwardLeg != null) {
									// if combo make combinations of outward leg with return leg
									if (searchQuery.isIntlReturn()) {
										populateIntlReturnTrip(group, response, JourneyKey, outwardLeg,
												onwardTripInfos);
									} else {
										populateOneWayOrDomesticReturnTrip(group, response, JourneyKey, outwardLeg,
												onwardTripInfos);
									}
								}
							}
							if (searchQuery.isDomesticReturn()) {
								for (FlightLegData returnLeg : group.getReturnList().getReturn()) {
									if (returnLeg != null) {
										populateOneWayOrDomesticReturnTrip(group, response, JourneyKey, returnLeg,
												returnTripInfos);
									}
								}
							}
						}
					}
				}
			}
			if (searchQuery.isIntlReturn())
				tripInfosMap.put(TripInfoType.COMBO.getName(), onwardTripInfos);
			else {
				tripInfosMap.put(TripInfoType.ONWARD.getName(), onwardTripInfos);
				if (searchQuery.isDomesticReturn()) {
					tripInfosMap.put(TripInfoType.RETURN.getName(), returnTripInfos);
				}
			}
		} catch (Exception e) {
			log.error("Error occured while populating trip {}", searchQuery.getSearchId(), e);
		}
		airSearchResult.setTripInfos(tripInfosMap);
	}

	private void populateOneWayOrDomesticReturnTrip(RouterGroupType group, CheckRoutingResponseType response,
			String JourneyKey, FlightLegData outwardLeg, List<TripInfo> tripInfos) {
		int segCount = 0;
		TripInfo tripInfo = null;
		try {
			List<SegmentInfo> segmentInfos = new ArrayList<>();
			for (RouterSegmentType outwardSegment : outwardLeg.getSegmentList().getSegment()) {
				SegmentInfo segmentInfo = populateSegmentInfo(outwardLeg, JourneyKey, outwardSegment,
						outwardLeg.getPrice(), false, segCount++);
				segmentInfos.add(segmentInfo);
			}
			tripInfo = new TripInfo();
			tripInfo.setSegmentInfos(segmentInfos);
			tripInfos.add(tripInfo);
		} catch (Exception e) {
			log.error("Error occured while populating trip {}", searchQuery.getSearchId(), e);
		}
	}

	private void populateIntlReturnTrip(RouterGroupType group, CheckRoutingResponseType response, String JourneyKey,
			FlightLegData outwardLeg, List<TripInfo> onwardTripInfos) {
		for (FlightLegData returnLeg : group.getReturnList().getReturn()) {
			Boolean isCombinationAllowed = true;
			if (response.getDisallowedPairingList() != null) {
				for (CheckRoutingIDPairType idPair : response.getDisallowedPairingList().getDisallowedPairing()) {
					if (idPair.getOutwardId().getValue().equals(outwardLeg.getId().getValue())
							&& idPair.getReturnId().getValue().equals(returnLeg.getId().getValue()))
						isCombinationAllowed = false;
					break;
				}
			}
			if (isCombinationAllowed && isSameClassToCombine(outwardLeg, returnLeg)) {
				boolean isGroupPricing = Objects.nonNull(group.getPrice());
				TripInfo onwardTrip = null;
				try {
					onwardTrip = new TripInfo();
					SegmentInfo segment = new SegmentInfo();
					List<SegmentInfo> onwardsegments = new ArrayList<>();
					if (returnLeg != null) {
						int segCount = 0;
						// all segments of outward
						for (RouterSegmentType outwardSegment : outwardLeg.getSegmentList().getSegment()) {
							if (outwardSegment != null) {
								segment = populateSegmentInfo(outwardLeg, JourneyKey, outwardSegment,
										outwardLeg.getPrice(), false, segCount++);
								onwardsegments.add(segment);
							}
						}
						segCount = 0;
						// all segments of return
						for (RouterSegmentType returnSegment : returnLeg.getSegmentList().getSegment()) {
							if (returnSegment != null) {
								segment = populateSegmentInfo(returnLeg, JourneyKey, returnSegment,
										returnLeg.getPrice(), true, segCount++);
								onwardsegments.add(segment);
							}
						}
						if (CollectionUtils.isNotEmpty(onwardsegments)) {
							onwardTrip.getSegmentInfos().addAll(onwardsegments);
							if (isGroupPricing) {
								CommonPrice groupPrice = group.getPrice();
								// keep group price to first segment of entire journey
								SegmentInfo segmentInfo = onwardsegments.get(0);
								buildGroupPriceInfo(groupPrice, segmentInfo);
							}
							if (onwardTrip != null) {
								onwardTripInfos.add(onwardTrip);
							}
						}
					}
				} catch (Exception e) {
					log.error(AirSourceConstants.SEGMENT_INFO_PARSING_ERROR, searchQuery.getSearchId(), e);
				}
			}
		}
	}

	private PriceInfo buildGroupPriceInfo(CommonPrice groupPrice, SegmentInfo segmentInfo) {
		PriceInfo priceInfo = null;
		if (groupPrice.getPassengerPriceList() != null
				&& CollectionUtils.isNotEmpty(groupPrice.getPassengerPriceList().getPassengerPrice())) {
			double additonalAmount =
					getAdditionalTaxOnBookingPerPassenger(groupPrice, AirUtils.getPaxCount(searchQuery, true));
			priceInfo = segmentInfo.getPriceInfo(0);
			List<CommonPrice> commonPrices = groupPrice.getPassengerPriceList().getPassengerPrice();
			Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();
			for (CommonPrice commonPrice : commonPrices) {
				if (fareDetails.get(PaxType.ADULT) != null
						&& TravelFusionConstants.ADULT_AGE == commonPrice.getAge().intValue()) {
					fareDetails.get(PaxType.ADULT).setFareComponents(getFareComponents(additonalAmount,
							commonPrice.getAge().intValue(), groupPrice.getPassengerPriceList()));
				} else if (fareDetails.get(PaxType.CHILD) != null
						&& TravelFusionConstants.CHILD_AGE == commonPrice.getAge().intValue()) {
					fareDetails.get(PaxType.CHILD).setFareComponents(getFareComponents(additonalAmount,
							commonPrice.getAge().intValue(), groupPrice.getPassengerPriceList()));
				} else if (fareDetails.get(PaxType.INFANT) != null
						&& TravelFusionConstants.INFANT_AGE == commonPrice.getAge().intValue()) {
					fareDetails.get(PaxType.INFANT).setFareComponents(getFareComponents(additonalAmount,
							commonPrice.getAge().intValue(), groupPrice.getPassengerPriceList()));
				}
			}
			priceInfo.setFareDetails(fareDetails);
		}
		return priceInfo;
	}

	private FareDetail buidGroupFareDetil(Double additonalAmount, CommonPrice commonPrice, FareDetail fareDetail,
			int age) {
		fareDetail.setFareComponents(getFareComponents(additonalAmount, age, commonPrice.getPassengerPriceList()));
		return fareDetail;
	}

	protected boolean isSameClassToCombine(FlightLegData outwardLeg, FlightLegData returnLeg) {
		if (ObjectUtils.allNotNull(outwardLeg.getSegmentList(), outwardLeg.getSegmentList().getSegment(),
				returnLeg.getSegmentList(), returnLeg.getSegmentList().getSegment())) {
			String outwardSegmentCabinClass = TravelFusionCabinClass.getEnumByString(
					outwardLeg.getSegmentList().getSegment().get(0).getTravelClass().getTfClass().value());
			String returnSegmentCabinClass = TravelFusionCabinClass.getEnumByString(
					returnLeg.getSegmentList().getSegment().get(0).getTravelClass().getTfClass().value());
			String outwardSupplierClass =
					outwardLeg.getSegmentList().getSegment().get(0).getTravelClass().getSupplierClass();
			String returnSupplierClass =
					returnLeg.getSegmentList().getSegment().get(0).getTravelClass().getSupplierClass();
			if (StringUtils.equalsIgnoreCase(outwardSegmentCabinClass, returnSegmentCabinClass)
					&& StringUtils.equalsIgnoreCase(outwardSupplierClass, returnSupplierClass)) {
				return true;
			}
		}
		return false;
	}

	private SegmentInfo populateSegmentInfo(FlightLegData leg, String journeyKey, RouterSegmentType segment,
			CommonPrice price, boolean isReturnSegment, int segCount) {
		SegmentInfo segmentInfo = new SegmentInfo();
		segmentInfo.setDepartAirportInfo(AirportHelper.getAirport(segment.getOrigin().getCode()));
		segmentInfo.setArrivalAirportInfo(AirportHelper.getAirport(segment.getDestination().getCode()));
		segmentInfo.setDepartTime(TravelFusionUtils.getResponseDate(segment.getDepartDate().getValue()));
		segmentInfo.setArrivalTime(TravelFusionUtils.getResponseDate(segment.getArriveDate().getValue()));
		segmentInfo.setFlightDesignator(createFlightDesignator(segment));
		segmentInfo.getDepartAirportInfo().setTerminal(AirUtils.getTerminalInfo(segment.getOrigin().getTerminal()));
		segmentInfo.getArrivalAirportInfo()
				.setTerminal(AirUtils.getTerminalInfo(segment.getDestination().getTerminal()));
		segmentInfo.setIsReturnSegment(isReturnSegment);
		if (segment.getStopList() != null) {
			segmentInfo.setStops(segment.getStopList().getStop().size());
			segmentInfo.setStopOverAirports(TravelFusionUtils.getStopOverAirports(segment.getStopList()));
		}
		if (segment.getDuration() != null) {
			segmentInfo.setDuration(segment.getDuration().longValue());
		} else {
			segmentInfo.calculateDuration();
		}
		segmentInfo.setSegmentNum(segCount);

		// Bank handling charges
		double additionalTaxableAmountOnBooking =
				getAdditionalTaxOnBookingPerPassenger(price, AirUtils.getPaxCount(searchQuery, true));
		PricePassengerPriceListType priceListType = price != null ? price.getPassengerPriceList() : null;
		segmentInfo.setPriceInfoList(populatePriceInfo(leg, journeyKey, additionalTaxableAmountOnBooking, priceListType,
				segment, segmentInfo, segCount));
		return segmentInfo;
	}

	private CheckRoutingType checkRoutingRequestBody(String routingId, String loginId) {
		CommonId routingid = bindingService.objReq.createCommonId();
		routingid.setValue(routingId);
		CheckRoutingType checkRoutingRequest = bindingService.objReq.createCheckRoutingType();
		checkRoutingRequest.setLoginId(loginId);
		checkRoutingRequest.setXmlLoginId(loginId);
		checkRoutingRequest.setRoutingId(routingid);
		return checkRoutingRequest;
	}

	private StartRoutingType startRoutingGenerateRequestBody(String loginId) {
		StartRoutingType searchRequest = bindingService.objReq.createStartRoutingType();
		RouteInfo onwardRoute = searchQuery.getRouteInfos().get(0);
		searchRequest.setXmlLoginId(loginId);
		searchRequest.setLoginId(loginId);
		searchRequest.setMode(TravelFusionConstants.MODE);
		searchRequest.setOrigin(buildLocation(onwardRoute.getFromCityAirportCode()));
		searchRequest.setDestination(buildLocation(onwardRoute.getToCityAirportCode()));
		searchRequest.setOutwardDates(buildTravelDate(onwardRoute.getTravelDate()));
		searchRequest.setTravelClass(TravelFusionUtils.getCabinType(searchQuery));
		searchRequest.setTravellerList(buildTravellerInfo());
		searchRequest.setIncrementalResults(true);
		searchRequest.setMaxChanges(TravelFusionConstants.MAX_CHANGES);
		searchRequest.setMaxHops(TravelFusionConstants.MAX_HOPS);
		searchRequest.setBookingProfile(buildProfile());
		searchRequest.setVendingOperatorFilter(buildOperatorFilter());
		searchRequest.setRealOperatorFilter(buildOperatorFilter());
		if (searchQuery.isReturn()) {
			searchRequest.setReturnDates(buildTravelDate(searchQuery.getRouteInfos().get(1).getTravelDate()));
		}
		return searchRequest;
	}

	private OperatorFilterType buildOperatorFilter() {
		OperatorFilterType filterType = null;
		if (supplierConfiguration.isTestCredential() && CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			filterType = new OperatorFilterType();
			StartRoutingOperatorListType oprList = new StartRoutingOperatorListType();
			oprList.getOperator().add(searchQuery.getPrefferedAirline());
			filterType.setOperatorList(oprList);
		}
		return filterType;
	}

	private TravelFusionRequests.StartRoutingLocation buildLocation(String airportCode) {
		CommonString commonString = bindingService.objReq.createCommonString();
		commonString.setValue(airportCode);
		TravelFusionRequests.StartRoutingLocation location = new TravelFusionRequests.StartRoutingLocation();
		location.setType(StartRoutingLocationTypeType.AIRPORTGROUP);
		location.setDescriptor(commonString);
		return location;
	}

	private StartRoutingDatesType buildTravelDate(LocalDate searchDate) {
		CommonString date = bindingService.objReq.createCommonString();
		date.setValue(TravelFusionUtils.getRequestDate(searchDate));
		StartRoutingDatesType travelDate = bindingService.objReq.createStartRoutingDatesType();
		travelDate.setDateOfSearch(date);
		return travelDate;
	}

	private BookingProfileType buildProfile() {
		BookingProfileType bookingProfile = bindingService.objReq.createBookingProfileType();
		CommonCustomSupplierParameterList commonCustomParameter =
				bindingService.objReq.createCommonCustomSupplierParameterList();
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(INCLUDE_FEATURES, INC_FEATURES));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(END_USER_IP_ADDRESS, IP));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(END_USER_BROWSER_AGENT, BROWSER));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(REQUEST_ORIGIN, ORIGIN));
		commonCustomParameter.getCustomSupplierParameter().add(getCustomParameter(POINT_OF_SALE, POS));
		bookingProfile.setCustomSupplierParameterList(commonCustomParameter);
		return bookingProfile;
	}

	private StartRoutingTravellerList buildTravellerInfo() {
		StartRoutingTravellerList travellers = bindingService.objReq.createStartRoutingTravellerList();
		for (int paxCount = 0; paxCount < searchQuery.getPaxInfo().get(PaxType.ADULT).intValue(); paxCount++) {
			StartRoutingTraveller adultTraveller = bindingService.objReq.createStartRoutingTraveller();
			adultTraveller.setAge(BigInteger.valueOf(ADULT_AGE));
			travellers.getTraveller().add(adultTraveller);
		}
		if (searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0) != 0) {
			for (int paxCount = 0; paxCount < searchQuery.getPaxInfo().get(PaxType.CHILD).intValue(); paxCount++) {
				StartRoutingTraveller ChildTraveller = bindingService.objReq.createStartRoutingTraveller();

				ChildTraveller.setAge(BigInteger.valueOf(CHILD_AGE));
				travellers.getTraveller().add(ChildTraveller);
			}
		}
		if (searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0) != 0) {
			for (int paxCount = 0; paxCount < searchQuery.getPaxInfo().get(PaxType.INFANT).intValue(); paxCount++) {
				StartRoutingTraveller InfantTraveller = bindingService.objReq.createStartRoutingTraveller();
				InfantTraveller.setAge(BigInteger.valueOf(INFANT_AGE));
				travellers.getTraveller().add(InfantTraveller);
			}
		}
		return travellers;
	}
}
