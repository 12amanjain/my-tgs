package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.ADULT_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.BANK_CONVERSION_CHARGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.BANK_HANDLING_CHARGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CANCELLATION;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CGST;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CHD;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CHECKED_IN;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CHILD_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.CREDIT_CARD_SURCHARGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.DIVIDING_FACTOR_1;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.DIVIDING_FACTOR_2;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.HOLD_BAG;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INCLUDES;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.INFANT_AGE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.LARGE_CABIN_BAG;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MAX_WEIGHT;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MR;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.PROVINCE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.SGST;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.SUPPLIER_CLASS;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.TRAVELLER_TYPE;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.WEIGHT;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.YQ4;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Enums;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.SessionMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BaggageInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.BookingProfileAddressType;
import TravelFusionRequests.CommonId;
import TravelFusionRequests.CommonName;
import TravelFusionRequests.CommonNamePartListType;
import TravelFusionRequests.CommonString;
import TravelFusionRequests.CustomSupplierParameterType;
import TravelFusionRequests.GetBookingDetailsType;
import TravelFusionResponses.CheckBookingResponseType;
import TravelFusionResponses.CommonPrice;
import TravelFusionResponses.FlightLegData;
import TravelFusionResponses.GetBookingDetailsResponseType;
import TravelFusionResponses.PricePassengerPriceListType;
import TravelFusionResponses.RouterSegmentType;
import TravelFusionResponses.RouterSupplierFeatureConditionType;
import TravelFusionResponses.RouterSupplierFeatureListType;
import TravelFusionResponses.RouterSupplierFeatureOptionType;
import TravelFusionResponses.RouterSupplierFeatureType;
import TravelFusionResponses.RouterTravelClassType;
import TravelFusionResponses.TaxItemType;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@SuperBuilder
@Slf4j
@NoArgsConstructor
abstract class TravelFusionServiceManager {

	protected SupplierConfiguration supplierConfiguration;
	protected AirSearchQuery searchQuery;
	protected String traceId, tokenId, bookingId;
	protected String bookingOrderNo;
	protected RestAPIListener listener;
	protected FlightAPIURLRuleCriteria apiURLS;

	protected String bookingStatus;
	protected String listenerKey;
	protected GeneralCachingCommunicator cachingComm;

	public RouterSupplierFeatureListType features;

	public String loginId;

	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected TravelFusionBindingService bindingService;

	protected DeliveryInfo deliveryInfo;

	protected User bookingUser;

	protected boolean roundTrip;

	public Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "text/xml");
		headerParams.put("Accept-Encoding", "application/gzip");
		return headerParams;
	}

	public String getURL() {
		return supplierConfiguration.getSupplierCredential().getUrl();
	}

	public void initLoginId(boolean isSearch, String listenerKey) {
		if (!StringUtils.isBlank(supplierConfiguration.getSupplierCredential().getClientId()))
			loginId = supplierConfiguration.getSupplierCredential().getClientId();
		else
			loginId = fetchSessionToken(isSearch);
		if (StringUtils.isBlank(loginId))
			loginId = getLoginId(listenerKey);
	}

	protected List<PriceInfo> populatePriceInfo(FlightLegData leg, String journeyKey,
			double additionalTaxableAmountOnBooking, PricePassengerPriceListType passengerPriceList,
			RouterSegmentType segment, SegmentInfo segmentInfo, int segCount) {
		String groupId = leg.getId().getValue();
		List<PriceInfo> priceInfoList = segmentInfo.getPriceInfoList();
		String plattingCarrier = segment.getVendingOperator().getCode();
		PriceInfo priceInfo = priceInfoList.isEmpty() ? PriceInfo.builder().build() : priceInfoList.get(0);
		priceInfo.setSupplierBasicInfo(supplierConfiguration.getBasicInfo());
		Map<PaxType, FareDetail> fareDetails = priceInfo.getFareDetails();

		fareDetails.put(PaxType.ADULT, createFareDetail(leg.getSeatsRemaining(), additionalTaxableAmountOnBooking,
				passengerPriceList, segment.getTravelClass(), segCount, ADULT_AGE, features, PaxType.ADULT));

		if (searchQuery.getPaxInfo().getOrDefault(PaxType.CHILD, 0) > 0) {
			fareDetails.put(PaxType.CHILD, createFareDetail(leg.getSeatsRemaining(), additionalTaxableAmountOnBooking,
					passengerPriceList, segment.getTravelClass(), segCount, CHILD_AGE, features, PaxType.CHILD));
		}

		if (searchQuery.getPaxInfo().getOrDefault(PaxType.INFANT, 0) > 0) {
			fareDetails.put(PaxType.INFANT, createFareDetail(leg.getSeatsRemaining(), additionalTaxableAmountOnBooking,
					passengerPriceList, segment.getTravelClass(), segCount, INFANT_AGE, features, PaxType.INFANT));
		}

		priceInfo.setFareIdentifier(FareType.PUBLISHED);
		priceInfo.getMiscInfo().setFareKey(groupId);
		priceInfo.getMiscInfo().setJourneyKey(journeyKey);
		if (StringUtils.isNotBlank(plattingCarrier)) {
			priceInfo.getMiscInfo().setPlatingCarrier(AirlineHelper.getAirlineInfo(plattingCarrier));
		}
		if (priceInfoList.isEmpty())
			priceInfoList.add(priceInfo);
		return priceInfoList;
	}

	protected FareDetail createFareDetail(BigInteger seatsRemaining, double amount,
			PricePassengerPriceListType passengerPriceList, RouterTravelClassType travelClass, int segmentNum, int age,
			RouterSupplierFeatureListType features, PaxType paxType) {
		FareDetail fareDetail = new FareDetail();
		String cabinEnum = travelClass.getTfClass().value().equals(TravelFusionConstants.ECONOMY_WITHOUT_RESTRICTION)
				? TravelFusionCabinClass.ECONOMY.getValue()
				: travelClass.getTfClass().value();

		fareDetail.setCabinClass(CabinClass.valueOf(TravelFusionCabinClass.getEnumByString(cabinEnum)));
		if (seatsRemaining != null)
			fareDetail.setSeatRemaining(seatsRemaining.intValue());
		fareDetail.setClassOfBooking(travelClass.getSupplierRBDCode());
		setRefundableType(travelClass, fareDetail, features, paxType);
		fareDetail.setFareType(FareType.getEnumFromName(travelClass.getSupplierClass()).getName());
		fareDetail.setBaggageInfo(createBaggageInfo(travelClass, features, paxType));
		fareDetail.setFareBasis(travelClass.getSupplierFareBasisCode());
		fareDetail.setFareComponents(null);
		if (segmentNum == 0) {
			fareDetail.setFareComponents(getFareComponents(amount, age, passengerPriceList));
		} else {
			fareDetail.setFareComponents(new HashMap<>());
		}
		return fareDetail;
	}

	protected void setRefundableType(RouterTravelClassType travelClass, FareDetail fareDetail,
			RouterSupplierFeatureListType features, PaxType paxType) {
		String travellerType = PaxType.CHILD.equals(paxType) ? CHD : paxType.getType();
		if (features != null && CollectionUtils.isNotEmpty(features.getFeature())) {
			for (RouterSupplierFeatureType feature : features.getFeature()) {
				if (CANCELLATION.equals(feature.getType())) {
					for (RouterSupplierFeatureOptionType option : feature.getOption()) {
						for (RouterSupplierFeatureConditionType condition : option.getCondition()) {
							if (SUPPLIER_CLASS.equals(condition.getType())) {
								boolean isApplicableOnPax = isApplicableOnPax(option, travellerType);
								if (TravelFusionUtils.isPresent(condition.getValue(), travelClass.getSupplierClass())
										&& isApplicableOnPax) {
									if (option.getValue() != null && option.getValue().doubleValue() > 0.0) {
										fareDetail.setRefundableType(RefundableType.REFUNDABLE.getRefundableType());
									}
								}
							}
						}
					}
				}
			}
		}
		if (Objects.isNull(fareDetail.getRefundableType())) {
			fareDetail.setRefundableType(RefundableType.NON_REFUNDABLE.getRefundableType());
		}
	}

	protected BaggageInfo createBaggageInfo(RouterTravelClassType travelClass, RouterSupplierFeatureListType features,
			PaxType paxType) {
		BaggageInfo baggage = new BaggageInfo();
		if (features != null && CollectionUtils.isNotEmpty(features.getFeature())) {
			for (RouterSupplierFeatureType feature : features.getFeature()) {

				if (HOLD_BAG.equals(feature.getType())) {
					populateBagInfo(feature, travelClass, baggage, HOLD_BAG, paxType);
				}
				if (LARGE_CABIN_BAG.equals(feature.getType())) {
					populateBagInfo(feature, travelClass, baggage, LARGE_CABIN_BAG, paxType);
				}
			}
		}
		if (travelClass.getSupplierClassDescription() != null && baggage.getAllowance() == null) {
			String baggageDetails = travelClass.getSupplierClassDescription();
			String before = StringUtils.substringBefore(baggageDetails, CHECKED_IN);
			String checkInBagggage = StringUtils.substringAfter(before, INCLUDES);
			if (!checkInBagggage.isEmpty())
				baggage.setAllowance(checkInBagggage);
		}
		return baggage;
	}

	protected void populateBagInfo(RouterSupplierFeatureType feature, RouterTravelClassType travelClass,
			BaggageInfo baggage, String type, PaxType paxType) {
		String travellerType = PaxType.CHILD.equals(paxType) ? CHD : paxType.getType();
		for (RouterSupplierFeatureOptionType option : feature.getOption()) {
			if (option != null && option.getValue() != null && option.getValue().doubleValue() == 0.0) {
				for (RouterSupplierFeatureConditionType condition : option.getCondition()) {
					if (SUPPLIER_CLASS.equals(condition.getType())) {
						boolean isApplicableOnPax = isApplicableOnPax(option, travellerType);
						if (TravelFusionUtils.isPresent(condition.getValue(), travelClass.getSupplierClass())
								&& isApplicableOnPax) {
							for (RouterSupplierFeatureConditionType Condition : option.getCondition()) {
								if (type.equals(HOLD_BAG) && (MAX_WEIGHT.equals(Condition.getType())
										|| WEIGHT.equals(Condition.getType()))) {
									baggage.setAllowance(Condition.getValue());
								}
								if (type.equals(LARGE_CABIN_BAG) && (MAX_WEIGHT.equals(Condition.getType())
										|| WEIGHT.equals(Condition.getType()))) {
									baggage.setCabinBaggage(Condition.getValue());
								}
							}
						}
					}
				}
			}
		}
	}

	protected boolean isApplicableOnPax(RouterSupplierFeatureOptionType option, String travellerType) {
		for (RouterSupplierFeatureConditionType condition : option.getCondition()) {
			if (TRAVELLER_TYPE.equals(condition.getType())) {
				return TravelFusionUtils.isPresent(condition.getValue(), travellerType);
			}
		}
		return false;
	}


	protected Map<FareComponent, Double> getFareComponents(double additionalAmount, int age,
			PricePassengerPriceListType passengerPriceList) {
		Map<FareComponent, Double> fc = new HashMap<>();
		Double totalTax = Double.valueOf(0);
		Double totalFare = Double.valueOf(additionalAmount);
		fc.put(FareComponent.AT, 0.0);
		if (passengerPriceList != null) {
			for (CommonPrice passengerPrice : passengerPriceList.getPassengerPrice()) {
				if (passengerPrice.getTaxItemList() != null && passengerPrice.getAge().intValue() == age) {
					for (TaxItemType tax : passengerPrice.getTaxItemList().getTaxItem()) {
						double taxValue = getAmountBasedOnCurrency(tax.getAmount().doubleValue(), tax.getCurrency());
						if (roundTrip) {
							taxValue = taxValue / 2;
						}
						totalTax += taxValue;
						if (tax.getName().contains(YQ4)) {
							fc.put(FareComponent.YQ, taxValue);

						} else if (tax.getName().contains(CGST)) {
							fc.put(FareComponent.AGST, taxValue);

						} else if (tax.getName().contains(SGST)) {
							fc.put(FareComponent.AGST, taxValue);
						} else {
							FareComponent fareComponent =
									Enums.getIfPresent(GDSFareComponentMapper.class, tax.getName())
											.or(GDSFareComponentMapper.TX).getFareComponent();
							fc.put(fareComponent, fc.getOrDefault(fareComponent, 0.0) + taxValue);
						}
					}
				}
				double passengerPricePrice = 0.0;
				if (Objects.nonNull(passengerPrice.getAmount()) && passengerPrice.getAge().intValue() == age) {

					passengerPricePrice = getAmountBasedOnCurrency(passengerPrice.getAmount().doubleValue(),
							passengerPrice.getCurrency());
					if (roundTrip) {
						passengerPricePrice = passengerPricePrice / 2;
					}
					totalFare = additionalAmount + passengerPricePrice;
					break;
				}
			}

		}

		fc.put(FareComponent.BF, Double.valueOf(totalFare - totalTax));
		fc.put(FareComponent.TF, totalFare);
		return fc;
	}

	protected CommonName getName(String agentName) {
		CommonName name = new CommonName();
		name.setTitle(MR);
		CommonNamePartListType nameList = new CommonNamePartListType();
		nameList.getNamePart().add(agentName);
		name.setNamePartList(nameList);
		return name;
	}

	protected BookingProfileAddressType getAddress(String bookingUserId, boolean isBilling) {
		ClientGeneralInfo companyInformation = ServiceCommunicatorHelper.getClientInfo();
		AddressInfo contactAddress = ServiceUtils.getContactAddressInfo(bookingUserId, companyInformation);
		BookingProfileAddressType address = new BookingProfileAddressType();
		// Should be sent empty for tags having no value
		String street = isBilling ? companyInformation.getAddress1() : contactAddress.getAddress();
		String city = isBilling ? companyInformation.getAddress2() + " " + companyInformation.getCity()
				: contactAddress.getCityInfo().getName();
		String postCode = isBilling ? companyInformation.getPostalCode() : contactAddress.getPincode();
		address.setStreet(getCommonString(street));
		address.setCity(getCommonString(city));
		address.setCountryCode(getCommonString(companyInformation.getNationality()));
		address.setPostcode(getCommonString(postCode));
		address.setProvince(getCommonString(PROVINCE));
		address.setBuildingName(getCommonString(""));
		address.setBuildingNumber(getCommonString(""));
		address.setCompany(getCommonString(""));
		address.setFlat(getCommonString(""));
		address.setLocality(getCommonString(""));
		return address;
	}

	protected GetBookingDetailsResponseType getBookingDetails(String loginId, CheckBookingResponseType checkBooking) {
		HttpUtils httpUtils = null;
		GetBookingDetailsResponseType getBookingDetailsResponse = null;
		try {
			GetBookingDetailsType request = buildGetBookingDetailsRequest(loginId, checkBooking);
			httpUtils = HttpUtils.builder().urlString(getURL())
					.postData(bindingService.getBookingDetailsMarshaller(request)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			String response = httpUtils.getResponse(null).orElse("").toString();
			getBookingDetailsResponse = bindingService.getBookingDetailsUnmarshaller(response);
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(bookingId).logData(endPointRQRS).type("BookingDetail").build());
		}
		return getBookingDetailsResponse;
	}

	protected GetBookingDetailsType buildGetBookingDetailsRequest(String loginId,
			CheckBookingResponseType checkBooking) {
		GetBookingDetailsType request = new GetBookingDetailsType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		request.setTFBookingReference(checkBooking.getTFBookingReference());
		request.setSupplierReference(checkBooking.getSupplierReference().getValue());
		return request;
	}

	protected CommonString getCommonString(String string) {
		CommonString value = new CommonString();
		value.setValue(string);
		return value;
	}

	protected CustomSupplierParameterType getCustomParameter(String type, String val) {
		CustomSupplierParameterType parameterType = new CustomSupplierParameterType();
		parameterType.setName(type);
		CustomSupplierParameterType.Value value = new CustomSupplierParameterType.Value();
		value.setValue(val);
		parameterType.setValue(value);
		return parameterType;
	}

	protected CommonId getRouteId(String id) {
		CommonId routeId = new CommonId();
		routeId.setValue(id);
		return routeId;
	}

	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.type(AirSourceType.TRAVELFUSION.name().toUpperCase()).toCurrency(toCurrency).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	public String fetchSessionToken(boolean isSearch) {
		if (isSearch) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder()
					.key(supplierConfiguration.getBasicInfo().getRuleId().toString()).index(0).compress(false).build();
			metaInfo = cachingComm.fetchFromQueue(metaInfo);
			if (metaInfo == null || StringUtils.isBlank((String) metaInfo.getValue())) {
				metaInfo = SessionMetaInfo.builder().key(supplierConfiguration.getBasicInfo().getRuleId().toString())
						.index(-1).compress(false).build();
				metaInfo = cachingComm.fetchFromQueue(metaInfo);
			}
			if (metaInfo != null && metaInfo.getValue() != null) {
				return (String) metaInfo.getValue();
			}
		}
		return null;
	}

	public void storeSession(String sessionToken) {
		if (StringUtils.isNotBlank(sessionToken)) {
			SessionMetaInfo metaInfo = SessionMetaInfo.builder()
					.key(supplierConfiguration.getBasicInfo().getRuleId().toString()).compress(false).build();
			metaInfo.setValue(sessionToken);
			cachingComm.storeInQueue(metaInfo);
		}
	}

	private String getLoginId(String listenerKey) {
		TravelFusionLoginManager loginManager = TravelFusionLoginManager.builder()
				.supplierConfiguration(supplierConfiguration).bindingService(bindingService).listener(listener)
				.listenerKey(listenerKey).apiURLS(apiURLS).bookingUser(bookingUser).build();
		if (StringUtils.isBlank(listener.getKey()))
			listener.setKey(
					bookingId != null ? bookingId : searchQuery != null ? searchQuery.getSearchId() : listenerKey);
		String loginId = loginManager.login();
		return loginId;
	}


	protected FlightDesignator createFlightDesignator(RouterSegmentType segment) {
		FlightDesignator flightDesign = new FlightDesignator();
		flightDesign.setAirlineInfo(AirlineHelper.getAirlineInfo(segment.getOperator().getCode()));
		flightDesign.setFlightNumber(segment.getFlightId().getNumber().toString());
		return flightDesign;
	}


	protected HttpUtils getHttpUtil() {
		HttpUtils httpUtils = HttpUtils.builder().urlString(getURL()).proxy(AirSupplierUtils.getProxy(bookingUser))
				.headerParams(getHeaderParams()).build();
		return httpUtils;
	}

	protected PricePassengerPriceListType getPriceList(CommonPrice price, FlightLegData leg) {
		PricePassengerPriceListType passengerPriceList =
				price != null ? price.getPassengerPriceList() : leg.getPrice().getPassengerPriceList();
		return passengerPriceList;
	}

	protected Double getAdditionalTaxOnBookingPerPassenger(CommonPrice price, int paxCount) {
		double amount = 0.0;
		if (Objects.nonNull(price) && Objects.nonNull(price.getTaxItemList())) {
			for (TaxItemType taxItem : price.getTaxItemList().getTaxItem()) {
				if (considerTax(taxItem) && taxItem.getAmount() != null) {
					amount += getAmountBasedOnCurrency(taxItem.getAmount().doubleValue(), taxItem.getCurrency());
				}
			}
			return amount / paxCount;
		}
		return 0d;
	}

	protected boolean considerTax(TaxItemType taxItem) {
		// all tax components are included for payable
		if (taxItem.getName().equals(BANK_HANDLING_CHARGE) || taxItem.getName().equals(BANK_CONVERSION_CHARGE)) {
			return true;
		}
		if (taxItem.getName().equalsIgnoreCase(CREDIT_CARD_SURCHARGE)) {
			return true;
		}
		return true;
	}

	protected Double getRoundValue(double amount) {
		return amount;
		// return Double.valueOf(Math.round((amount) * 100) / 100);
	}

	protected double getTotalTaxOnEachLeg(List<FlightLegData> outwardLeg, List<FlightLegData> returnLeg,
			int passengerNumber) {

		Double totalTax = 0.0;
		for (FlightLegData outward : outwardLeg) {
			if (outward.getPrice() != null && Objects.nonNull(outward.getPrice().getTaxItemList())) {
				for (TaxItemType tax : outward.getPrice().getTaxItemList().getTaxItem()) {
					if (considerTax(tax)) {
						totalTax += getAmountBasedOnCurrency(tax.getAmount().doubleValue(), tax.getCurrency());
					}
				}
			}
		}

		if (returnLeg != null && !returnLeg.isEmpty()) {
			for (FlightLegData returnleg : returnLeg) {
				if (returnleg.getPrice() != null && Objects.nonNull(returnleg.getPrice().getTaxItemList())) {
					for (TaxItemType tax : returnleg.getPrice().getTaxItemList().getTaxItem()) {
						if (considerTax(tax)) {
							totalTax += getAmountBasedOnCurrency(tax.getAmount().doubleValue(), tax.getCurrency());
						}
					}
				}
			}
		}
		int trips = roundTrip ? DIVIDING_FACTOR_2 : DIVIDING_FACTOR_1;
		return totalTax / (passengerNumber * trips);
	}

	public static Map<FareComponent, Double> getZeroFareComponents() {
		Map<FareComponent, Double> newFareComponents = new HashMap<FareComponent, Double>();
		newFareComponents.put(FareComponent.TF, 0.0);
		newFareComponents.put(FareComponent.BF, 0.0);
		newFareComponents.put(FareComponent.AT, 0.0);
		return newFareComponents;
	}

}
