package com.tgs.services.fms.sources.travelfusion;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.JAXBException;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.LogData;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import TravelFusionRequests.GetBranchSupplierListType;
import TravelFusionRequests.ListSupplierRoutesType;
import TravelFusionResponses.GetBranchSupplierListResponseType;
import TravelFusionResponses.ListSupplierRoutesResponseType;
import lombok.experimental.SuperBuilder;

@SuperBuilder
final class TravelFusionSupplierRoutesManager extends TravelFusionServiceManager {

	private HttpUtils httpUtils;

	public void addRouteInfoToSystem(List<SourceRouteInfo> routes) {
		HashSet<String> routesSet = new HashSet<>();
		getSuppliersOnBranch().getBranchSupplierList().getSupplier().forEach(supplier -> {
			try {
				ListSupplierRoutesType request = buildSupplierRoutesRequest(supplier.getValue());
				httpUtils = HttpUtils.builder().urlString(getURL())
						.postData(bindingService.supplierRouteMarshaller(request)).headerParams(getHeaderParams())
						.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
				String response = httpUtils.getResponse(null).orElse("").toString();
				ListSupplierRoutesResponseType supplierRoutesList =
						bindingService.getSupplierRoutesListUnmarshaller(response);
				parseResponse(routes, supplierRoutesList, routesSet);
			} catch (JAXBException e) {
				throw new SupplierUnHandledFaultException(e.getMessage());
			} catch (IOException e) {
				throw new SupplierRemoteException(e);
			} finally {
				String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
						TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
				listener.addLog(
						LogData.builder().key(listenerKey).logData(endPointRQRS).type("SupplierRoutes").build());
			}
		});
	}

	private GetBranchSupplierListResponseType getSuppliersOnBranch() {
		try {
			GetBranchSupplierListType request = buildGetBranchSupplierListTypereq();
			httpUtils = HttpUtils.builder().urlString(getURL())
					.postData(bindingService.getBranchSupplierListMarshaller(request)).headerParams(getHeaderParams())
					.proxy(AirSupplierUtils.getProxy(bookingUser)).build();
			String response = httpUtils.getResponse(null).orElse("").toString();
			GetBranchSupplierListResponseType supplierList = bindingService.getBranchSupplierListUnmarshaller(response);
			return supplierList;
		} catch (JAXBException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} catch (IOException e) {
			throw new SupplierRemoteException(e);
		} finally {
			String endPointRQRS = StringUtils.join(TravelFusionUtils.formatRQRS(httpUtils.getPostData(), "Request"),
					TravelFusionUtils.formatRQRS(httpUtils.getResponseString(), "Response"));
			listener.addLog(LogData.builder().key(listenerKey).logData(endPointRQRS).type("SuppliersList").build());
		}
	}

	private GetBranchSupplierListType buildGetBranchSupplierListTypereq() {
		GetBranchSupplierListType request = new GetBranchSupplierListType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		return request;
	}

	private ListSupplierRoutesType buildSupplierRoutesRequest(String supplier) {
		ListSupplierRoutesType request = new ListSupplierRoutesType();
		request.setLoginId(loginId);
		request.setXmlLoginId(loginId);
		request.setOneWayOnlyAirportRoutes(false);
		// Supplier name returned in GetBranchSupplierList response
		request.setSupplier(supplier);
		return request;
	}

	private void parseResponse(List<SourceRouteInfo> routes, ListSupplierRoutesResponseType supplierRoutesList,
			HashSet<String> routesSet) {
		supplierRoutesList.getRouteList().getAirportRoutes().forEach(routeInfo -> {

			Pattern pattern = Pattern.compile("[A-Za-z]{1,6}");
			Matcher matcher = pattern.matcher(routeInfo);
			while (matcher.find()) {
				if (!routesSet.contains(matcher.group().toString())) {
					SourceRouteInfo route = new SourceRouteInfo();
					route.setDest(matcher.group().substring(3, matcher.group().length()));
					route.setSrc(matcher.group().substring(0, 3));
					route.setEnabled(true);
					route.setSourceId(supplierConfiguration.getSourceId());
					routes.add(route);
					routesSet.add(matcher.group().toString());
				}
			}
		});
	}
}
