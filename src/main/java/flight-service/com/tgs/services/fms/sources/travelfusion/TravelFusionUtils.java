package com.tgs.services.fms.sources.travelfusion;

import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MASTER;
import static com.tgs.services.fms.sources.travelfusion.TravelFusionConstants.MISS;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.CardType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.helper.AirportHelper;
import TravelFusionRequests.TfClassEnumerationType;
import TravelFusionResponses.RouterStopListType;
import TravelFusionResponses.RouterStopType;

public class TravelFusionUtils {

	public static String getTitle(String value) {
		if (value.equalsIgnoreCase(MASTER)) {
			return ("Mr");
		} else if (value.equalsIgnoreCase(MISS)) {
			return ("Miss");
		}
		return value;
	}

	public static Period getAge(LocalDate dob, LocalDate travelDate) {
		Period age = Period.between(dob, travelDate);
		return age;
	}

	public static TfClassEnumerationType getCabinType(AirSearchQuery searchQuery) {
		return (TravelFusionCabinClass.valueOf(searchQuery.getCabinClass().getName().toUpperCase())).getCabin();
	}

	public static String getCardType(CardType type) {
		String cardType = null;
		if (CardType.VISA.equals(type)) {
			cardType = TravelFusionConstants.VISA;
		} else if (CardType.AMERICAN_EXPRESS.equals(type)) {
			cardType = TravelFusionConstants.AMERICAN_EXPRESS;
		} else if (CardType.DINERS_CLUB.equals(type)) {
			cardType = TravelFusionConstants.DINERS_CLUB;
		} else if (CardType.MASTERCARD.equals(type)) {
			cardType = TravelFusionConstants.MASTERCARD;
		}
		return cardType;
	}

	public static String getRequestDate(LocalDate date) {
		return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) + "-00:00";
	}

	public static String getDate(LocalDate date) {
		return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	public static LocalDate getTravellerDOB(FlightTravellerInfo travellerInfo) {
		if (travellerInfo.getDob() == null) {
			return TgsDateUtils.calenderToLocalDate(TgsDateUtils.getDefaultDOB());
		}
		return travellerInfo.getDob();
	}

	public static LocalDateTime getResponseDate(String date) {
		return LocalDateTime.parse(date, DateTimeFormatter.ofPattern("dd/MM/yyyy-HH:mm"));
	}

	public static String formatRQRS(String message, String type) {
		StringBuffer buffer = new StringBuffer();
		if (StringUtils.isNotEmpty(message)) {
			buffer.append(type + " : " + LocalDateTime.now().toString()).append("\n\n").append(message).append("\n\n");
		}
		return buffer.toString();
	}

	protected static List<AirportInfo> getStopOverAirports(RouterStopListType stopList) {
		List<AirportInfo> airportStops = new ArrayList<>();
		for (RouterStopType routerStop : stopList.getStop()) {
			if (StringUtils.isNotBlank(routerStop.getLocation().getCode())) {
				AirportInfo stopOver = AirportHelper.getAirport(routerStop.getLocation().getCode());
				stopOver.setTerminal(routerStop.getLocation().getTerminal());
				airportStops.add(stopOver);
			}
		}
		return airportStops;
	}

	protected static String getTripId(List<SegmentInfo> segmentInfos, boolean isReturn) {
		String tripId = null;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getSegmentNum().intValue() == 0 && isReturn
					&& BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
				tripId = segmentInfo.getPriceInfo(0).getMiscInfo().getFareKey();
			} else {
				if (segmentInfo.getSegmentNum().intValue() == 0 && !isReturn
						&& !BooleanUtils.isTrue(segmentInfo.getIsReturnSegment())) {
					tripId = segmentInfo.getPriceInfo(0).getMiscInfo().getFareKey();
				}
			}
		}
		return tripId;
	}

	public static boolean isPresent(String values, String value) {
		if (StringUtils.isNotBlank(values)) {
			List<String> valueList = Arrays.asList(values.split(","));
			if (valueList.contains(value))
				return true;
		}
		return false;
	}

}
