package com.tgs.services.fms.sources.travelport;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;


@Slf4j
@Service
public class AtlasTravelPortAirBookingFactory extends TravelPortAirBookingFactory {

	public AtlasTravelPortAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments,
			Order order) throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	@Override
	protected void additionalAncillaries() {
		try {
			bookingManager.addContactDetails("VIEWTRIPITIN");
			bookingManager.pnrRetention();
		} finally {
			bookingManager.resetKeys();
		}

	}

	@Override
	protected void airPNRElement() {
		bookingManager.bookingAirPnrElement(true);
	}

}
