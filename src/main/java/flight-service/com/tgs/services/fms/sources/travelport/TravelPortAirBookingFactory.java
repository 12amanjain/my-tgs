package com.tgs.services.fms.sources.travelport;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.manager.AirBookingEngine;
import com.tgs.services.fms.sources.AbstractAirBookingFactory;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Objects;


@Slf4j
@Service
public class TravelPortAirBookingFactory extends AbstractAirBookingFactory {

	private TravelPortBindingService bindingService;

	protected TravelPortBookingManager bookingManager;

	protected SoapRequestResponseListner listener;

	public TravelPortAirBookingFactory(SupplierConfiguration supplierConf, BookingSegments bookingSegments, Order order)
			throws Exception {
		super(supplierConf, bookingSegments, order);
	}

	public void initialize() {
		bindingService = TravelPortBindingService.builder().configuration(supplierConf).build();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		bookingManager = TravelPortBookingManager.builder().configuration(supplierConf).order(order)
				.deliveryInfo(deliveryInfo).segmentInfos(bookingSegments.getSegmentInfos()).bookingId(bookingId)
				.bindingService(bindingService).listener(listener).sourceConfiguration(sourceConfiguration)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(bookingUser).build();
		String traceId = bookingSegments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		bookingManager.setTraceId(traceId);
	}


	@Override
	public boolean doBooking() {
		boolean isSuccess = false;
		boolean ignoreSession = true;
		isTkRequired = true;
		try {
			if (Objects.nonNull(supplierSession)) {
				List<SegmentInfo> bookingSegmentInfos = bookingSegments.getSegmentInfos();
				String sessionKey = supplierSession.getSupplierSessionInfo().getSessionToken();
				initialize();
				bookingManager.init(bookingSegments);
				bookingManager.setSessionkey(sessionKey);
				bookingManager.addPassengerDetails(bookingSegmentInfos);
				bookingManager.addPrice(bookingSegmentInfos);
				if (!isFareDiff(bookingManager.getSupplierAmount())) {
					bookingManager.ticketArrangement();
					// bookingTerminal -> Ruler PCC Terminal Command
					// bookingManager.bookingTerminal();
					// commission ,tour code,corporate codes -> bookingAirPnrElement
					// bookingManager.bookingAirPnrElement();
					String crsPnr = bookingManager.commitBooking();
					ignoreSession = false;
					log.info("CRS pnr {} generated for bookingid {}", crsPnr, bookingId);
					BookingUtils.updateSupplierBookingId(bookingSegmentInfos, crsPnr);
					bookingSegments.setSupplierBookingId(crsPnr);
					if (StringUtils.isNotBlank(crsPnr) && bookingManager.isAirlinePNRAvailable(crsPnr)) {
						bookingManager.addSpecialServiceAndAncillaries(crsPnr, gstInfo);
						isSuccess = true;
						this.additionalAncillaries();
						pnr = crsPnr;
					}
				}
			}
		} finally {
			if (isHoldBooking && isSuccess) {
				updateTimeLimit(TgsDateUtils.getCalendar(bookingManager.getTicketingTimeLimit()));
			}
			bookingManager.resetKeys();
			if (ignoreSession) {
				bookingManager.endSession(true, "Ignore Session");
			}
		}
		return isSuccess;
	}

	@Override
	public boolean confirmBeforeTicket() {
		boolean isFareDiff = false;
		try {
			initialize();
			String crsPNR = bookingSegments.getSupplierBookingId();
			bookingManager.init(bookingSegments);
			boolean isSegmentsActive = bookingManager.isSegmentsActive(crsPNR);
			if (isSegmentsActive) {
				bookingManager.setNewSupplierFare(bookingSegments);
				isFareDiff = isFareDiff(bookingManager.getSupplierAmount());
			}
			if (!isSameDayTicketing() && isFareDiff) {
				bookingManager.cancelFiledFare(crsPNR);
				bookingManager.fileNewFare(crsPNR);
				isFareDiff = isFareDiff(bookingManager.getSupplierAmount());
			}
		} finally {
			bookingManager.resetKeys();
		}
		return isFareDiff;
	}

	@Override
	public boolean doConfirmBooking() {
		boolean isBookingSuccess = false;
		try {
			initialize();
			List<SegmentInfo> bookingSegmentInfos = bookingSegments.getSegmentInfos();
			bookingManager.init(bookingSegments);
			bookingManager.setReservationPNR(TravelPortUtils.getReservationPNR(bookingSegments.getSegmentInfos()));
			if (bookingManager.isPNRStatusActive()) {
				boolean isFareDiff = updateSupplierAndChangePCC(bookingSegmentInfos);
				if (!isFareDiff) {
					this.airPNRElement();
					isBookingSuccess = bookingManager.issueTicketAlongWithFOP(getSupplierBookingCreditCard());
				} else {
					throw new SupplierUnHandledFaultException("Found Fare Diff while ticketing pcc");
				}
			}
		} finally {
			bookingManager.resetKeys();
		}
		return isBookingSuccess;
	}

	private boolean updateSupplierAndChangePCC(List<SegmentInfo> segmentInfos) {
		String sessionId = null;
		boolean isFareDiff = false;
		TravelPortSessionManager sessionManager = null;
		String ticketingSupplierId = AirBookingEngine.getTicketingSupplierId(supplierConf.getSupplierAdditionalInfo(),
				segmentInfos, bookingUser);
		if (StringUtils.isNotEmpty(ticketingSupplierId)
				&& !supplierConf.getBasicInfo().getSupplierId().equals(ticketingSupplierId)) {
			SupplierInfo updatedSupplierInfo = SupplierConfigurationHelper.getSupplierInfo(ticketingSupplierId);
			SupplierBasicInfo updatedBasicInfo = SupplierBasicInfo.builder().sourceId(updatedSupplierInfo.getSourceId())
					.supplierId(updatedSupplierInfo.getSupplierId()).supplierName(updatedSupplierInfo.getName())
					.build();
			supplierConf.setBasicInfo(updatedBasicInfo);
			supplierConf.setSupplierCredential(updatedSupplierInfo.getCredentialInfo());
			updateSupplierId(bookingSegments.getSegmentInfos());
			log.info("Ticketing Pcc changed for booking id {} new supplier id {}", order.getBookingId(),
					ticketingSupplierId);
			sessionManager = TravelPortSessionManager.builder().bindingService(bindingService).searchQuery(null)
					.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
					.configuration(supplierConf).providerCode(bookingManager.getProviderCode())
					.traceId(bookingManager.getTraceId()).listener(listener).bookingId(bookingId)
					.bookingUser(bookingUser).build();
			sessionManager.init();
			sessionId = sessionManager.createSession();
			bookingManager.setSessionkey(sessionId);
			if (bookingManager.isSegmentsActive(bookingSegments.getSupplierBookingId())) {
				bookingManager.setNewSupplierFare(bookingSegments);
				bookingManager.cancelFiledFare(bookingManager.getCsrPnr());
				bookingManager.fileNewFare(bookingManager.getCsrPnr());
				isFareDiff = isFareDiff(bookingManager.getSupplierAmount());
				sessionManager.endSession(sessionId);
			}
		}
		return isFareDiff;
	}

	protected void additionalAncillaries() {}

	protected void airPNRElement() {
		bookingManager.bookingAirPnrElement(false);
	}

}
