package com.tgs.services.fms.sources.travelport;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirBookingRetrieveFactory;
import org.springframework.stereotype.Service;


@Service
public class TravelPortAirBookingRetrieveFactory extends AbstractAirBookingRetrieveFactory {

	private TravelPortBindingService bindingService;
	private TravelPortBookingRetrieveManager retrieveManager;
	protected SoapRequestResponseListner listener;

	public void initialize() {
		listener = new SoapRequestResponseListner(pnr, null, supplierConf.getBasicInfo().getSupplierName());
		bindingService = TravelPortBindingService.builder().configuration(supplierConf).build();
		retrieveManager = TravelPortBookingRetrieveManager.builder().pnr(pnr).listener(listener)
				.bindingService(bindingService).configuration(supplierConf).bookingUser(bookingUser).build();
		String traceId = TravelPortUtils.keyGenerator();
		retrieveManager.setTraceId(traceId);

	}

	public TravelPortAirBookingRetrieveFactory(SupplierConfiguration supplierConfiguration, String pnr) {
		super(supplierConfiguration, pnr);
	}


	@Override
	public AirImportPnrBooking retrievePNRBooking() {
		initialize();
		retrieveManager.init();
		pnrBooking = retrieveManager.retrieveBooking(retrieveManager.getUnivRecordImportRS(pnr));
		return pnrBooking;
	}
}
