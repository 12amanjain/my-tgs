package com.tgs.services.fms.sources.travelport;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.datamodel.supplier.SupplierSessionInfo;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.sources.AbstractAirInfoFactory;
import org.springframework.stereotype.Service;
import java.util.Objects;

@Service
public class TravelPortAirInfoFactory extends AbstractAirInfoFactory {

	protected TravelPortBindingService bindingService;
	protected TravelPortSearchManager searchManager;

	protected SoapRequestResponseListner listener;

	public TravelPortAirInfoFactory(AirSearchQuery searchQuery, SupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	public TravelPortAirInfoFactory(SupplierConfiguration supplierConf) {
		super(null, supplierConf);
	}

	public void initialize() {
		bindingService = TravelPortBindingService.builder().configuration(supplierConf).build();
		searchManager = TravelPortSearchManager.builder().searchQuery(searchQuery).moneyExchnageComm(moneyExchnageComm)
				.sourceConfiguration(sourceConfiguration).configuration(supplierConf).bindingService(bindingService)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
		bindingService.setSupplierAnalyticsInfos(supplierAnalyticsInfos);
	}

	@Override
	protected void searchAvailableSchedules() {
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		initialize();
		listener = new SoapRequestResponseListner(searchQuery.getSearchId(), null,
				supplierConf.getBasicInfo().getSupplierName());
		searchManager.setListener(listener);
		searchManager.setTraceId(TravelPortUtils.keyGenerator());
		searchManager.init();
		searchManager.setListener(listener);
		searchResult = searchManager.doSearch();
	}

	@Override
	protected TripInfo review(TripInfo selectedTrip, String bookingId) {
		initialize();
		boolean isReviewSuccess = false;
		String sessionId = null;
		SupplierSession session = null;
		boolean isNewToken = false;
		TripInfo reviewedTrip = null;
		TravelPortSessionManager sessionManager = null;
		String traceId = selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		String providerCode = TravelPortUtils.getProviderCode(selectedTrip, supplierConf);
		try {
			listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
			TravelPortReviewManager reviewManager = TravelPortReviewManager.builder().searchQuery(searchQuery)
					.sourceConfiguration(sourceConfiguration).configuration(supplierConf).bindingService(bindingService)
					.traceId(traceId).moneyExchnageComm(moneyExchnageComm).bookingId(bookingId)
					.providerCode(providerCode).listener(listener).bookingUser(user).build();
			reviewManager.init();
			reviewedTrip = reviewManager.reviewFlight(selectedTrip, false);
			if (reviewedTrip != null) {
				SupplierSessionInfo sessionInfo =
						SupplierSessionInfo.builder().tripKey(selectedTrip.getSegmentsBookingKey()).build();
				session = getSessionIfExists(bookingId, sessionInfo);
				sessionManager =
						TravelPortSessionManager.builder().bindingService(bindingService).searchQuery(searchQuery)
								.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
								.configuration(supplierConf).providerCode(providerCode).traceId(traceId)
								.bookingId(bookingId).listener(listener).bookingUser(user).build();
				sessionManager.init();
				if (Objects.isNull(session)) {
					isNewToken = true;
					sessionId = sessionManager.createSession();
				} else {
					sessionId = session.getSupplierSessionInfo().getSessionToken();
				}
				reviewManager.setSessionkey(sessionId);
				if (reviewManager.sellSegment(reviewedTrip, sessionId)) {
					isReviewSuccess = true;
					reviewManager.setSSRInfos(reviewedTrip);
				}
			}
		} finally {
			if (isNewToken && reviewedTrip != null && isReviewSuccess) {
				SupplierSessionInfo sessionInfo = SupplierSessionInfo.builder().sessionToken(sessionId).build();
				storeBookingSession(bookingId, sessionInfo, session, selectedTrip);
			} else {
				if (sessionManager != null)
					sessionManager.endSession(sessionId);
			}
		}
		return reviewedTrip;
	}


	protected TripFareRule getFareRule(TripInfo selectedTrip, String bookingId, boolean isDetailed) {
		initialize();
		TripFareRule tripFareRule = null;
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		if (isDetailed) {
			TravelPortFareRuleManager ruleManager = TravelPortFareRuleManager.builder().configuration(supplierConf)
					.bookingId(bookingId).listener(listener).bindingService(bindingService).bookingUser(user).build();
			ruleManager.init();
			tripFareRule = ruleManager.getFareRule(selectedTrip, bookingId);
		} else {
			SupplierConfiguration supplierConfiguration =
					SupplierConfigurationHelper.getSupplierConfiguration("Travelport-FareRule");
			TravelPortMiniRuleManager ruleManager =
					TravelPortMiniRuleManager.builder().configuration(supplierConfiguration).bookingId(bookingId)
							.bookingId(bookingId).bookingUser(user).build();
			ruleManager.init();
			tripFareRule = ruleManager.getFareRule(selectedTrip, bookingId);
		}
		return tripFareRule;
	}

	@Override
	protected TripSeatMap getSeatMap(TripInfo tripInfo, String bookingId) {
		initialize();
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		TravelPortSSRManager ssrManager = null;
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		String providerCode = TravelPortUtils.getProviderCode(tripInfo, supplierConf);
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		ssrManager = TravelPortSSRManager.builder().bindingService(bindingService).searchQuery(searchQuery)
				.sourceConfiguration(sourceConfiguration).criticalMessageLogger(criticalMessageLogger)
				.configuration(supplierConf).listener(listener).providerCode(providerCode).traceId(traceId)
				.bookingId(bookingId).segmentInfos(tripInfo.getSegmentInfos()).bookingUser(user).build();
		ssrManager.init();
		tripSeatMap = ssrManager.getSeatMap(tripInfo);
		return tripSeatMap;
	}

	@Override
	public TripInfo getChangeClass(TripInfo tripInfo, String searchId) {
		initialize();
		listener = new SoapRequestResponseListner(searchId, null, supplierConf.getBasicInfo().getSupplierName());
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		TravelPortAlternateClassSearchManager alternateClassSearchManager = null;
		alternateClassSearchManager = TravelPortAlternateClassSearchManager.builder().searchQuery(searchQuery)
				.moneyExchnageComm(moneyExchnageComm).sourceConfiguration(sourceConfiguration)
				.configuration(supplierConf).listener(listener).bindingService(bindingService).traceId(traceId)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
		alternateClassSearchManager.init();
		alternateClassSearchManager.searchAlternateClass(tripInfo, searchId);
		return tripInfo;
	}

	@Override
	public TripInfo getChangeClassFare(TripInfo tripInfo, String searchId) {
		initialize();
		listener = new SoapRequestResponseListner(searchId, null, supplierConf.getBasicInfo().getSupplierName());
		String traceId = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getTraceId();
		TravelPortReviewManager reviewManager = TravelPortReviewManager.builder().searchQuery(searchQuery)
				.searchId(searchId).sourceConfiguration(sourceConfiguration).configuration(supplierConf)
				.bindingService(bindingService).listener(listener).moneyExchnageComm(moneyExchnageComm).traceId(traceId)
				.criticalMessageLogger(criticalMessageLogger).bookingUser(user).build();
		reviewManager.init();
		tripInfo = reviewManager.reviewFlight(tripInfo, true);
		return tripInfo;
	}
}
