package com.tgs.services.fms.sources.travelport;


import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.fms.utils.AirUtils;
import com.travelport.www.schema.common_v47_0.BaseSearchReqChoice_type0;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.alternateclass.AlternateClass;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.travelport.www.schema.air_v47_0.AirSearchModifiers_type0;
import com.travelport.www.schema.air_v47_0.AvailabilitySearchReq;
import com.travelport.www.schema.air_v47_0.AvailabilitySearchRsp;
import com.travelport.www.schema.air_v47_0.BookingCodeInfo_type0;
import com.travelport.www.schema.air_v47_0.PreferredProviders_type0;
import com.travelport.www.schema.air_v47_0.SearchSpecificAirSegment_type0;
import com.travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import com.travelport.www.schema.common_v47_0.Provider_type0;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
final class TravelPortAlternateClassSearchManager extends TravelPortServiceManager {

	public void searchAlternateClass(TripInfo tripInfo, String searchId) {
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("AvailabilitySearch",configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		AvailabilitySearchReq availabilitySearchReq = buildAvailabilitySearchReq(tripInfo);
		AvailabilitySearchRsp availabilitySearchRsp;
		try {
			bindingService.setProxyAndAuthentication(airService, "AvailabilitySearch");
			availabilitySearchRsp = airService.service(availabilitySearchReq, getSessionContext(false));
			if (!checkAnyErrors(availabilitySearchRsp)) {
				buildAvailableClasses(availabilitySearchRsp, tripInfo);
			}
		} catch (AirFaultMessage | RemoteException airFaultMessage) {
			logCriticalMessage(airFaultMessage.getMessage());
			throw new SupplierUnHandledFaultException(airFaultMessage.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

	}

	private void buildAvailableClasses(AvailabilitySearchRsp availabilitySearchRsp, TripInfo tripInfo) {
		List<TypeBaseAirSegment> airSegmentList = getList(availabilitySearchRsp.getAirSegmentList().getAirSegment());
		Map<String, TypeBaseAirSegment> typeBaseAirSegmentMap = new HashMap<>();
		for (TypeBaseAirSegment typeBaseAirSegment : airSegmentList) {
			String segmentKey = typeBaseAirSegment.getOrigin() + "_"
					+ TravelPortUtils.getIsoDateTime(typeBaseAirSegment.getDepartureTime()).toString();
			typeBaseAirSegmentMap.put(segmentKey, typeBaseAirSegment);
		}
		List<SegmentInfo> segmentInfoList = tripInfo.getSegmentInfos();
		List<AlternateClass> classAvailable = null;
		for (int segmentIndex = 0; segmentIndex < segmentInfoList.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfoList.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			String segmentKey = segmentInfo.getDepartureAirportCode() + "_" + segmentInfo.getDepartTime().toString();
			TypeBaseAirSegment typeBaseAirSegment = typeBaseAirSegmentMap.get(segmentKey);
			// Updating the key as this key is responsible for doing airPricing
			if (typeBaseAirSegment != null) {
				segmentInfo.getPriceInfoList().get(0).getMiscInfo()
						.setSegmentKey(typeBaseAirSegment.getKey().getTypeRef());
				if (typeBaseAirSegment.getAirAvailInfo() != null
						&& getList(typeBaseAirSegment.getAirAvailInfo()).get(0).getBookingCodeInfo() != null) {
					classAvailable = new ArrayList<>();
					for (BookingCodeInfo_type0 bookingCodeInfo : getList(typeBaseAirSegment.getAirAvailInfo()).get(0)
							.getBookingCodeInfo()) {
						String bookingCounts = bookingCodeInfo.getBookingCounts();
						parseBookingCounts(classAvailable, bookingCounts);
					}
				}
			}
			if (legNum == 0 && CollectionUtils.isNotEmpty(classAvailable))
				segmentInfo.getAlternateClass().addAll(classAvailable);
		}
	}

	private void parseBookingCounts(List<AlternateClass> classAvailable, String bookingCounts) {
		String[] bookingCountsArray = bookingCounts.split("\\|");
		for (int index = 0; index < bookingCountsArray.length; index++) {
			String classOfService = String.valueOf(bookingCountsArray[index].charAt(0));
			int availableSeats = getSeatCounts(bookingCountsArray, index);
			if (availableSeats > 0) {
				AlternateClass alternateClass =
						AlternateClass.builder().classOfBook(classOfService).seatCount(availableSeats).build();
				classAvailable.add(alternateClass);
			}

		}
	}

	private int getSeatCounts(String[] bookingCountsArray, int index) {
		return Integer.valueOf(
				StringUtils.isNumeric(bookingCountsArray[index].substring(1)) ? bookingCountsArray[index].substring(1)
						: "0");
	}

	protected AvailabilitySearchReq buildAvailabilitySearchReq(TripInfo tripInfo) {
		AvailabilitySearchReq availabilitySearchReq = new AvailabilitySearchReq();
		BaseSearchReqChoice_type0 baseReq = new BaseSearchReqChoice_type0();
		buildBaseCoreRequest(availabilitySearchReq);
		List<SegmentInfo> segmentInfoList = tripInfo.getSegmentInfos();
		SearchSpecificAirSegment_type0 searchSpecificAirSegment = null;
		for (int segmentIndex = 0; segmentIndex < segmentInfoList.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfoList.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = TravelPortUtils.getNextSegmentNumber(segmentIndex, segmentInfoList);
			if (legNum == 0) {
				searchSpecificAirSegment = new SearchSpecificAirSegment_type0();
				searchSpecificAirSegment.setOrigin(getIATACode(segmentInfo.getDepartureAirportCode()));
				searchSpecificAirSegment.setDepartureTime(segmentInfo.getDepartTime().toString());
				searchSpecificAirSegment
						.setCarrier(getTypeCarrierCode(segmentInfo.getFlightDesignator().getAirlineCode()));
				searchSpecificAirSegment.setFlightNumber(getTypeFlightNumber(segmentInfo.getFlightNumber()));
			}
			searchSpecificAirSegment.setDestination(getIATACode(segmentInfo.getArrivalAirportCode()));
			if (nextSegmentLegNum == 0)
				baseReq.addSearchSpecificAirSegment(searchSpecificAirSegment);
		}
		AirSearchModifiers_type0 airSearchModifiers = new AirSearchModifiers_type0();
		airSearchModifiers.setPreferredProviders(getPreferredProviders(tripInfo));
		availabilitySearchReq.setBaseSearchReqChoice_type0(baseReq);
		availabilitySearchReq.setAirSearchModifiers(airSearchModifiers);
		return availabilitySearchReq;
	}

	protected PreferredProviders_type0 getPreferredProviders(TripInfo tripInfo) {
		PreferredProviders_type0 preferredProviders = new PreferredProviders_type0();
		Provider_type0 provider = new Provider_type0();
		provider.setCode(getProviderCode(TravelPortUtils.getProviderCode(tripInfo, configuration)));
		preferredProviders.addProvider(provider);
		return preferredProviders;
	}


}
