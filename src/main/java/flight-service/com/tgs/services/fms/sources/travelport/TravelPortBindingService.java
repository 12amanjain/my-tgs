package com.tgs.services.fms.sources.travelport;

import static com.tgs.services.fms.sources.travelport.TravelPortConstants.*;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import com.travelport.www.service.universal_v47_0.PassiveServiceStub;
import com.travelport.www.service.universal_v47_0.UniversalRecordCancelServiceStub;
import com.travelport.www.service.universal_v47_0.UniversalRecordImportServiceStub;
import com.travelport.www.service.universal_v47_0.UniversalRecordModifyServiceStub;
import com.travelport.www.service.universal_v47_0.UniversalRecordServiceStub;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.Arrays;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.axis2.AxisFault;
import org.apache.axis2.Constants;
import org.apache.axis2.client.Options;
import org.apache.axis2.client.ServiceClient;
import org.apache.axis2.client.Stub;
import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.impl.httpclient4.HttpTransportPropertiesImpl;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.auth.AuthPolicy;
import org.apache.commons.lang3.StringUtils;

@Slf4j
@Setter
@Getter
@Builder
public class TravelPortBindingService {

	private SupplierConfiguration configuration;

	protected List<SupplierAnalyticsQuery> supplierAnalyticsInfos;

	public AirServiceStub getAirService() {
		AirServiceStub airServiceStub = null;
		try {
			String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), AIR_SERVICE);
			airServiceStub = new AirServiceStub(url);
		} catch (AxisFault e) {
			throw new CustomGeneralException("Error creating AirService Stub " + e);
		}
		// setProxyAndAuthentication(airServiceStub);
		return airServiceStub;
	}


	public UniversalRecordServiceStub getUniversalRecordService() {
		UniversalRecordServiceStub universalRecordService = null;
		if (universalRecordService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordService = new UniversalRecordServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordServiceStub " + e);

			}
		}
		// setProxyAndAuthentication(universalRecordService);
		return universalRecordService;
	}

	public SharedBookingServiceStub getSharedBookingService() {
		SharedBookingServiceStub sharedBookingService = null;
		if (sharedBookingService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), SHARED_BOOKING_SERVICE);
				sharedBookingService = new SharedBookingServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating SharedBookingServiceStub " + e);

			}
		}
		// setProxyAndAuthentication(sharedBookingService);
		return sharedBookingService;
	}

	public UniversalRecordImportServiceStub getUniversalRecordImportService() {
		UniversalRecordImportServiceStub universalRecordImportService = null;
		if (universalRecordImportService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordImportService = new UniversalRecordImportServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordImportServiceStub " + e);
			}
		}
		// setProxyAndAuthentication(universalRecordImportService);
		return universalRecordImportService;
	}

	public UniversalRecordModifyServiceStub getUniversalRecordModifyService() {
		UniversalRecordModifyServiceStub universalRecordModifyService = null;
		if (universalRecordModifyService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordModifyService = new UniversalRecordModifyServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordModifyServiceStub " + e);
			}
		}
		// setProxyAndAuthentication(universalRecordModifyService);
		return universalRecordModifyService;
	}

	public UniversalRecordCancelServiceStub getUniversalRecordCancelService() {
		UniversalRecordCancelServiceStub universalRecordCancelService = null;
		if (universalRecordCancelService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), UNIVERSAL_RECORD_SERVICE);
				universalRecordCancelService = new UniversalRecordCancelServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating UniversalRecordCancelServiceStub " + e);
			}
		}
		// setProxyAndAuthentication(universalRecordCancelService);
		return universalRecordCancelService;
	}

	public PassiveServiceStub getPassiveService() {
		PassiveServiceStub passiveService = null;
		if (passiveService == null) {
			try {
				String url = StringUtils.join(configuration.getSupplierCredential().getUrl(), PASSIVE_SERVICE);
				passiveService = new PassiveServiceStub(url);
			} catch (AxisFault e) {
				throw new CustomGeneralException("Error creating PassiveServiceStub " + e);
			}
		}
		// setProxyAndAuthentication(passiveService);
		return passiveService;
	}

	public void setProxyAndAuthentication(Stub stub, String service) {

		/*
		 * String proxyAddress = null; AirGeneralPurposeOutput configuratorInfo =
		 * AirConfiguratorHelper.getGeneralPurposeOutput(); String proxy = null; if (configuratorInfo != null) { proxy =
		 * configuratorInfo.getProxyAddress(); }
		 * 
		 * if (StringUtils.isNotBlank(proxy)) { int proxyPort = 3128; if (stub != null && stub._getServiceClient() !=
		 * null && stub._getServiceClient().getOptions() != null) { ServiceClient client = stub._getServiceClient();
		 * Options options = client.getOptions(); options.setProperties(null); proxyAddress = proxy.split(":")[0];
		 * proxyPort = Integer.valueOf(proxy.split(":")[1]); if (StringUtils.isNotBlank(proxyAddress)) {
		 * HttpTransportProperties.ProxyProperties proxyProperties = new HttpTransportProperties.ProxyProperties();
		 * proxyProperties.setProxyName(proxyAddress); // 10.10.16.165 proxyProperties.setProxyPort(proxyPort); // 3128
		 * options.setProperty(org.apache.axis2.transport.http.HTTPConstants.PROXY, proxyProperties);
		 * options.setProperty(HTTPConstants.CHUNKED, false); } client.setOptions(options); } }
		 */

		if (stub != null && stub._getServiceClient() != null && stub._getServiceClient().getOptions() != null) {
			ServiceClient serviceClient = stub._getServiceClient();
			serviceClient.getOptions().setProperty(HTTPConstants.SO_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setProperty(HTTPConstants.CONNECTION_TIMEOUT,
					AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setTimeOutInMilliSeconds(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
			serviceClient.getOptions().setUserName(configuration.getSupplierCredential().getUserName());
			serviceClient.getOptions().setPassword(configuration.getSupplierCredential().getPassword());
			HttpTransportPropertiesImpl.Authenticator auth = new HttpTransportPropertiesImpl.Authenticator();
			auth.setUsername(configuration.getSupplierCredential().getUserName());
			auth.setPassword(configuration.getSupplierCredential().getPassword());
			auth.setAuthSchemes(Arrays.asList(AuthPolicy.BASIC));
			auth.setPreemptiveAuthentication(true);
			serviceClient.getOptions().setManageSession(true);
			auth.setAllowedRetry(true);
			serviceClient.getOptions().setProperty(org.apache.axis2.transport.http.HTTPConstants.AUTHENTICATE, auth);
			setConnectionManager(stub);
			log.debug("BasicAuth Added for Service {}", service);
		}
	}

	public void updateOptions(Stub stub) {
		if (stub != null) {
			ServiceClient client = stub._getServiceClient();
			Options options = client.getOptions();
			options.setProperty(Constants.Configuration.DISABLE_SOAP_ACTION, Boolean.TRUE);
			client.setOptions(options);
		}
	}

	public void setConnectionManager(Stub stub) {
		MultiThreadedHttpConnectionManager conmgr = new MultiThreadedHttpConnectionManager();
		conmgr.getParams().setDefaultMaxConnectionsPerHost(30);
		conmgr.getParams().setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		conmgr.getParams().setSoTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		HttpClient client = new HttpClient(conmgr);
		stub._getServiceClient().getOptions().setProperty(HTTPConstants.CACHED_HTTP_CLIENT, client);
	}
}
