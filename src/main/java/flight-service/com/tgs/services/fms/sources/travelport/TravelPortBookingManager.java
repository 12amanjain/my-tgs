package com.tgs.services.fms.sources.travelport;

import static com.tgs.services.fms.sources.travelport.TravelPortConstants.BSP;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.CHILD_AGE;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.CREDIT_CARD_MODE;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.PASSIVE_REMARK_TYPE;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.SEGMENT_TYPE;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.SESSIONED;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.SSRINFO;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.TERMINAL_COMMAND;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.TICKET_DATE;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.travelport.www.schema.common_v47_0.*;
import com.travelport.www.schema.sharedbooking_v47_0.*;
import com.travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import org.apache.axis2.databinding.types.YearMonth;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirlineTimeLimitData;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.tgs.utils.string.TgsStringUtils;
import com.travelport.www.schema.air_v47_0.AirItinerary_type0;
import com.travelport.www.schema.air_v47_0.AirPriceReq;
import com.travelport.www.schema.air_v47_0.AirPriceRsp;
import com.travelport.www.schema.air_v47_0.AirPricingInfoRef_type0;
import com.travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import com.travelport.www.schema.air_v47_0.AirPricingModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirPricingSolution_type0;
import com.travelport.www.schema.air_v47_0.AirPricingTicketingModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirReservationLocatorCode_type0;
import com.travelport.www.schema.air_v47_0.AirReservation_type0;
import com.travelport.www.schema.air_v47_0.AirTicketingModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirTicketingReq;
import com.travelport.www.schema.air_v47_0.AirTicketingRsp;
import com.travelport.www.schema.air_v47_0.BookingInfo_type0;
import com.travelport.www.schema.air_v47_0.FareInfo_type0;
import com.travelport.www.schema.air_v47_0.PassengerType_type0;
import com.travelport.www.schema.air_v47_0.SpecificSeatAssignment_type0;
import com.travelport.www.schema.air_v47_0.TicketEndorsement_type0;
import com.travelport.www.schema.air_v47_0.TicketingModifiers_type0;
import com.travelport.www.schema.air_v47_0.TourCode_type0;
import com.travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import com.travelport.www.schema.air_v47_0.TypeEticketability;
import com.travelport.www.schema.air_v47_0.TypePricingMethod;
import com.travelport.www.schema.air_v47_0.TypeTicketModifierAccountingType;
import com.travelport.www.schema.passive_v47_0.BasePassiveRemarkGroup;
import com.travelport.www.schema.passive_v47_0.PassiveRemark_type0;
import com.travelport.www.schema.passive_v47_0.PassiveSegment_type0;
import com.travelport.www.schema.universal_v47_0.AirAdd_type0;
import com.travelport.www.schema.universal_v47_0.AirDelete_type0;
import com.travelport.www.schema.universal_v47_0.PassiveCreateReservationReq;
import com.travelport.www.schema.universal_v47_0.PassiveCreateReservationRsp;
import com.travelport.www.schema.universal_v47_0.RecordIdentifier_type0;
import com.travelport.www.schema.universal_v47_0.UniversalAdd_type0;
import com.travelport.www.schema.universal_v47_0.UniversalModifyCmd_type0;
import com.travelport.www.schema.universal_v47_0.UniversalRecordModifyReq;
import com.travelport.www.schema.universal_v47_0.UniversalRecordModifyRsp;
import com.travelport.www.schema.universal_v47_0.UniversalRecordRetrieveRsp;
import com.travelport.www.schema.universal_v47_0.UniversalRecord_type0;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirService;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import com.travelport.www.service.sharedbooking_v47_0.SystemFaultMessage;
import com.travelport.www.service.universal_v47_0.AvailabilityFaultMessage;
import com.travelport.www.service.universal_v47_0.PassiveFaultMessage;
import com.travelport.www.service.universal_v47_0.PassiveServiceStub;
import com.travelport.www.service.universal_v47_0.UniversalModifyFaultMessage;
import com.travelport.www.service.universal_v47_0.UniversalRecordFaultMessage;
import com.travelport.www.service.universal_v47_0.UniversalRecordModifyService;
import com.travelport.www.service.universal_v47_0.UniversalRecordModifyServiceStub;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@SuperBuilder
final class TravelPortBookingManager extends TravelPortServiceManager {

	protected Double supplierAmount;

	protected static final String TRAVEL_DOCUMENTS_INFORMATION = "DOCS";
	protected static final String PASSPORT_ACTON_CODE = "HK";

	protected List<FlightTravellerInfo> travellerInfos;

	protected String plattingCarrier;

	protected static final Integer SLEEP_TIME_IN_SECS = 5;

	protected GeneralServiceCommunicator gmsCommunicator;

	// Commission if already loaded on Supplier PCC then we dont send any commisson
	private boolean isCommissionAlreadyAdded;

	public void init(BookingSegments segments) {
		init();
		travellerInfos = segments.getSegmentInfos().get(0).getTravellerInfo();
		plattingCarrier = segments.getSegmentInfos().get(0).getPriceInfo(0).getPlattingCarrier();
		providerCode = segments.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getProviderCode();
		segmentInfos = segments.getSegmentInfos();
		csrPnr = segments.getSupplierBookingId();
	}

	public boolean addPassengerDetails(List<SegmentInfo> bookingSegmentInfos) {
		boolean isSuccess = false;
		AddTraveler_type0 addTraveler = new AddTraveler_type0();
		listener.setType(AirUtils.getLogType("BookingTraveler", configuration));
		SharedBookingServiceStub bookingServiceStub = bindingService.getSharedBookingService();
		bookingServiceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
		BookingTravelerReq bookingTravelerReq = new BookingTravelerReq();
		BookingBaseReqChoice_type0 baseReq = new BookingBaseReqChoice_type0();
		buildBaseCoreRequest(bookingTravelerReq);
		bookingTravelerReq.setSessionKey(getTypeSessionKey(sessionkey));
		buildPassengerDetails(addTraveler, travellerInfos);
		baseReq.setAddTraveler(addTraveler);
		bookingTravelerReq.setBookingBaseReqChoice_type0(baseReq);
		try {
			bindingService.setProxyAndAuthentication(bookingServiceStub, "PassengerDetails");
			BookingTravelerRsp bookingTravelerRsp =
					bookingServiceStub.service(bookingTravelerReq, getSessionContext(true));
			if (!checkAnyErrors(bookingTravelerRsp.getBookingTravelerRsp()) && bookingTravelerRsp
					.getBookingTravelerRsp().getUniversalRecord().getStatus().getTypeURStatus().equals(SESSIONED)) {
				setTravellerMiscInfo(
						getList(bookingTravelerRsp.getBookingTravelerRsp().getUniversalRecord().getBookingTraveler()));
				isSuccess = true;
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (SystemFaultMessage systemFaultMessage) {
			logCriticalMessage(systemFaultMessage.toString());
			throw new SupplierUnHandledFaultException(systemFaultMessage.getMessage());
		} finally {
			bookingServiceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	private void buildPassengerDetails(AddTraveler_type0 addTraveler, List<FlightTravellerInfo> travellerInfos) {
		int passengerNumber = 1;
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			BaseBookingTravelerInfoA bookingTravelerInfo = new BaseBookingTravelerInfoA();
			BookingTraveler_type0 bookingTraveler = new BookingTraveler_type0();
			bookingTraveler.setKey(getTypeRef(String.valueOf(passengerNumber)));
			bookingTraveler.setTravelerType(getTypePTC(travellerInfo.getPaxType().getType()));
			BookingTravelerName_type0 bookingTravelerName = new BookingTravelerName_type0();
			bookingTravelerName.setFirst(getFirst_type2(TravelPortUtils.getFirstName(travellerInfo)));
			bookingTravelerName.setLast(getTravellerLastname(TravelPortUtils.getLastName(travellerInfo)));
			bookingTravelerName.setPrefix(getPrefix(TravelPortUtils.getPaxPrefix(travellerInfo)));
			bookingTravelerInfo.setBookingTravelerName(bookingTravelerName);
			PhoneNumber_type0 phoneNumber = new PhoneNumber_type0();
			phoneNumber.setNumber(getNumberType0(AirSupplierUtils.getContactNumber(deliveryInfo)));
			bookingTravelerInfo.addPhoneNumber(phoneNumber);
			Email_type0 email = new Email_type0();
			email.setEmailID(AirSupplierUtils.getEmailId(deliveryInfo));
			bookingTravelerInfo.addEmail(email);
			if (travellerInfo.getPaxType() == PaxType.INFANT && travellerInfo.getDob() != null) {
				NameRemark_type0 dobRemark = new NameRemark_type0();
				dobRemark.setRemarkData(TravelPortUtils.createDate(travellerInfo.getDob()));
				bookingTraveler.addNameRemark(dobRemark);
			}
			if (travellerInfo.getPaxType() == PaxType.CHILD) {
				NameRemark_type0 dobRemark = new NameRemark_type0();
				String age = TravelPortUtils.getAge(travellerInfo);
				bookingTraveler.setAge(new BigInteger(age));
				dobRemark.setRemarkData(StringUtils.join("P-C", age));
				bookingTraveler.addNameRemark(dobRemark);
			}
			bookingTraveler.setBaseBookingTravelerInfoA(bookingTravelerInfo);
			bookingTraveler.setBaseBookingTravelerInfoB(new BaseBookingTravelerInfoB());
			addTraveler.addBookingTraveler(bookingTraveler);
			passengerNumber++;
		}
	}

	protected Number_type0 getNumberType0(String contactNumber) {
		Number_type0 number = new Number_type0();
		number.setNumber_type0(contactNumber);
		return number;
	}

	public void addPrice(List<SegmentInfo> bookingSegmentInfos) {
		BookingPricingReq bookingPricingReq = null;
		SharedBookingServiceStub bookingServiceStub = bindingService.getSharedBookingService();
		BookingBaseReqChoice_type5 reqChoice = new BookingBaseReqChoice_type5();
		try {
			bookingPricingReq = new BookingPricingReq();
			listener.setType(AirUtils.getLogType("BookingPricing", configuration));
			bookingServiceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			buildBaseCoreRequest(bookingPricingReq);
			bookingPricingReq.setSessionKey(getTypeSessionKey(sessionkey));
			Map<PaxType, FareDetail> fareDetails = bookingSegmentInfos.get(0).getPriceInfo(0).getFareDetails();
			AddPricing_type0 addPricing = buildAddPricing(fareDetails, bookingSegmentInfos);
			reqChoice.setAddPricing(addPricing);
			bookingPricingReq.setBookingBaseReqChoice_type5(reqChoice);
			bindingService.setProxyAndAuthentication(bookingServiceStub, "AddPrice");
			BookingPricingRsp bookingPricingRsp =
					bookingServiceStub.service(bookingPricingReq, getSessionContext(true));
			supplierAmount = getSupplierAmount(bookingSegmentInfos, bookingPricingRsp);
		} catch (SystemFaultMessage | RemoteException e) {
			log.error("Error in Updating pricing for bookingId {} cause  ", bookingId, e.getMessage());
			logCriticalMessage(e.getMessage());
		} finally {
			bookingServiceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private double getSupplierAmount(List<SegmentInfo> bookingSegmentInfos, BookingPricingRsp bookingPricingRsp) {
		double airlinePaidAmount = 0.0;

		createAirPricingInfoMap(bookingPricingRsp.getBookingPricingRsp().getUniversalRecord()
				.getUniversalRecordChoice_type7().getAirReservation()[0].getAirPricingInfo());
		if (bookingPricingRsp.getBookingPricingRsp().getUniversalRecord() != null
				&& ArrayUtils.isNotEmpty(bookingPricingRsp.getBookingPricingRsp().getUniversalRecord()
						.getUniversalRecordChoice_type7().getAirReservation())
				&& ArrayUtils.isNotEmpty(bookingPricingRsp.getBookingPricingRsp().getUniversalRecord()
						.getUniversalRecordChoice_type7().getAirReservation()[0].getAirPricingInfo())) {
			for (AirPricingInfo_type0 pricingInfo : bookingPricingRsp.getBookingPricingRsp().getUniversalRecord()
					.getUniversalRecordChoice_type7().getAirReservation()[0].getAirPricingInfo()) {
				Double airlineTotal = getAmountBasedOnCurrency(pricingInfo.getTotalPrice().getTypeMoney(),
						pricingInfo.getTotalPrice().getTypeMoney().substring(0, 3), 1.0);
				airlinePaidAmount += (airlineTotal * pricingInfo.getPassengerType().length);
			}
		}

		return airlinePaidAmount;
	}

	public void bookingAirPnrElement(boolean netRemittanceCode) {
		UniversalRecordModifyReq universalRecordModifyReq = new UniversalRecordModifyReq();
		buildBaseUniversalModifyRequest(universalRecordModifyReq);
		Integer totalExecutedFQ = TravelPortUtils.getFQCountFromPax(travellerInfos);
		ArrayList<String> airPricingInfoKeys = new ArrayList<String>();
		airPricingInfoPaxMap.forEach((k, v) -> {
			airPricingInfoKeys.addAll(v);
		});
		for (int fqIndex = 0; fqIndex < totalExecutedFQ; fqIndex++) {
			UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
			universalModifyCmd.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
			buildAirPricingTicketModifiers(universalModifyCmd, fqIndex, airPricingInfoKeys, netRemittanceCode);
			universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		}
		modifyUniversalRecord(universalRecordModifyReq, "Add Commission");
	}


	private void buildAirPricingTicketModifiers(UniversalModifyCmd_type0 universalModifyCmd, int fqIndex,
			ArrayList<String> airPricingInfoKeys, boolean netRemittanceCode) {
		AirAdd_type0 airAdd = new AirAdd_type0();
		airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
		AirPricingTicketingModifiers_type0 airPricingTicketingModifiers = new AirPricingTicketingModifiers_type0();
		AirPricingInfoRef_type0 airPricingInfoRef = new AirPricingInfoRef_type0();
		airPricingInfoRef.setKey(getTypeRef(airPricingInfoKeys.get(fqIndex)));
		airPricingTicketingModifiers.addAirPricingInfoRef(airPricingInfoRef);
		TicketingModifiers_type0 ticketingModifiers = new TicketingModifiers_type0();
		String accountCode = TravelPortUtils.getAccountCodeFromTrip(segmentInfos);
		if (StringUtils.isNotBlank(accountCode)) {
			TypeTicketModifierAccountingType accountInformation = new TypeTicketModifierAccountingType();
			accountInformation.setValue(accountCode);
			// ticketingModifiers.setAccountingInfo(accountInformation);
		}
		if (netRemittanceCode) {
			String netRemittance = getNetRemittanceCode();
			if (StringUtils.isNotBlank(netRemittance)) {
				TypeTicketModifierAccountingType accountInformation = new TypeTicketModifierAccountingType();
				accountInformation.setValue(netRemittance);
				ticketingModifiers.setAccountingInfo(accountInformation);
			}
		}
		if (!isCommissionAlreadyAdded) {
			String iataCommission = TravelPortUtils.getIataCommission(segmentInfos);
			if (StringUtils.isNotBlank(iataCommission)) {
				Commission_type0 commission = new Commission_type0();
				commission.setPercentage(getTypePercWithDec(iataCommission));
				ticketingModifiers.setCommission(getCommissionType1(commission.getPercentage()));
			}
		}
		String tourCodeMisc = TravelPortUtils.getTourCode(segmentInfos);
		if (StringUtils.isNotBlank(tourCodeMisc)) {
			TourCode_type0 tourCode = new TourCode_type0();
			tourCode.setValue(getTypeTourCode(tourCodeMisc));
			ticketingModifiers.setTourCode(tourCode);
		}
		TicketEndorsement_type0 ticketEndorsement = new TicketEndorsement_type0();
		ticketEndorsement.setValue(getTicketEndorsement("FARE WILL BE PRINTABLE"));
		ticketingModifiers.addTicketEndorsement(ticketEndorsement);
		airPricingTicketingModifiers.setTicketingModifiers(ticketingModifiers);
		airAdd.addAirPricingTicketingModifiers(airPricingTicketingModifiers);
		universalModifyCmd.setAirAdd(airAdd);
	}

	private boolean validateFactForMandatoryAccountCode() {
		ClientGeneralInfo companyInfo = ServiceCommunicatorHelper.getClientInfo();
		for (SegmentInfo segment : segmentInfos) {
			// Domestic only
			if (!AirType.DOMESTIC.equals(segment.getAirType(companyInfo.getCountry()))) {
				return false;
			}
			// AirIndia only airline
			if (!"AI".equals(segment.getPlatingCarrier(null))) {
				return false;
			}
			// S, T booking class
			for (Entry<PaxType, FareDetail> entry : segment.getPriceInfo(0).getFareDetails().entrySet()) {
				if (!Arrays.asList("S", "T").contains(entry.getValue().getClassOfBooking())) {
					return false;
				}
			}
			// Flight number
			if (segment.getFlightNumber().matches("9[0-9][0-9][0-9]")) {
				return false;
			}
			// Travel period
			if (!(segment.getDepartTime().isAfter(LocalDateTime.of(2020, 9, 20, 23, 59))
					&& segment.getArrivalTime().isBefore(LocalDateTime.of(2020, 11, 25, 00, 00)))) {
				return false;
			}
			// Booking Period
			if (!(LocalDateTime.now().isAfter(LocalDateTime.of(2020, 9, 20, 23, 59))
					&& LocalDateTime.now().isBefore(LocalDateTime.of(2020, 11, 01, 00, 00)))) {
				return false;
			}
		}
		return true;
	}

	private FareInfo_type0 getFareInfoFromBookingInfo(AirPricingInfo_type0 airPricingInfo,
			List<TypeBaseAirSegment> airSegmentList, SegmentInfo segmentInfo) {
		for (TypeBaseAirSegment typeBaseAirSegment : airSegmentList) {
			if (typeBaseAirSegment.getOrigin().getTypeIATACode().equals(segmentInfo.getDepartureAirportCode())
					&& segmentInfo.getDepartureAirportCode().equals(typeBaseAirSegment.getOrigin().getTypeIATACode())
					&& TravelPortUtils.getStringDateTime(segmentInfo.getDepartTime())
							.equals(TravelPortUtils.getStringDateTime(
									TravelPortUtils.getIsoDateTime(typeBaseAirSegment.getDepartureTime())))) {
				String segmentRefKey = typeBaseAirSegment.getKey().getTypeRef();
				for (BookingInfo_type0 bookingInfo : airPricingInfo.getBookingInfo()) {
					if (bookingInfo.getSegmentRef().getTypeRef().equals(segmentRefKey)) {
						String fareInfoRefKey = bookingInfo.getFareInfoRef().getTypeRef();
						for (FareInfo_type0 fareInfoItem : airPricingInfo.getFareInfo()) {
							if (fareInfoItem.getKey().getTypeRef().equals(fareInfoRefKey)) {
								return fareInfoItem;
							}
						}
					}
				}
			}
		}
		return null;
	}

	private AddPricing_type0 buildAddPricing(Map<PaxType, FareDetail> fareDetails, List<SegmentInfo> segments) {
		AddPricing_type0 addPricing = new AddPricing_type0();
		List<AirPricingInfo_type0> airPricingInfoList = buildAirPricingInfoList(fareDetails, segments);
		for (AirPricingInfo_type0 type0 : airPricingInfoList) {
			addPricing.addAirPricingInfo(type0);
		}
		return addPricing;
	}

	public void ticketArrangement() {
		BookingPnrElementReq bookingPnrElementReq = null;
		SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
		try {
			listener.setType(AirUtils.getLogType("BookingPnrElement", configuration));
			bookingPnrElementReq = buildTicketArrangementRequest(bookingPnrElementReq);
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(serviceStub, "BookingPnrElement");
			BookingPnrElementRsp bookingPnrElementRsp =
					serviceStub.service(bookingPnrElementReq, getSessionContext(true));
			if (isBookingPnrError(bookingPnrElementRsp)) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (RemoteException re) {
			throw new SupplierRemoteException(re);
		} catch (SystemFaultMessage systemFaultMessage) {
			logCriticalMessage(systemFaultMessage.toString());
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}

	private BookingPnrElementReq buildTicketArrangementRequest(BookingPnrElementReq bookingPnrElementReq) {
		if (bookingPnrElementReq == null) {
			bookingPnrElementReq = new BookingPnrElementReq();
		}
		BookingBaseReqChoice_type3 reqChoice = new BookingBaseReqChoice_type3();
		buildBaseCoreRequest(bookingPnrElementReq);
		AddPnrElement_type0 addPnrElement = new AddPnrElement_type0();
		ActionStatus_type0 actionStatus = new ActionStatus_type0();
		actionStatus.setProviderCode(getProviderCode(providerCode));
		actionStatus.setTicketDate(TICKET_DATE);
		actionStatus.setType(Type_type9.ACTIVE);
		actionStatus.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
		addPnrElement.setActionStatus(actionStatus);
		reqChoice.setAddPnrElement(addPnrElement);
		bookingPnrElementReq.setBookingBaseReqChoice_type3(reqChoice);
		bookingPnrElementReq.setSessionKey(getTypeSessionKey(sessionkey));
		return bookingPnrElementReq;
	}

	private boolean isBookingPnrError(BookingPnrElementRsp bookingPnrElementRsp) {
		return false;
	}

	public void bookingTerminal() {
		SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
		listener.setType(AirUtils.getLogType("BookingTerminal", configuration));
		serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
		BookingTerminalReq bookingTerminalReq = new BookingTerminalReq();
		buildBaseCoreRequest(bookingTerminalReq);
		bookingTerminalReq.setSessionKey(getTypeSessionKey(sessionkey));
		bookingTerminalReq.setCommand(TERMINAL_COMMAND);
		try {
			bindingService.setProxyAndAuthentication(serviceStub, "BookingTerminal");
			BookingTerminalRsp bookingTerminalRsp = serviceStub.service(bookingTerminalReq, getSessionContext(true));
			if (checkAnyErrors(bookingTerminalRsp)) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (SystemFaultMessage | RemoteException systemFaultMessage) {
			logCriticalMessage(systemFaultMessage.getMessage());
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}


	public boolean issueTicketAlongWithFOP(CreditCardInfo supplierBookingCreditCard) {
		boolean isSuccess = false;
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("AirTicketing", configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		AirTicketingReq airTicketingReq = null;
		AirTicketingRsp airTicketingRsp = null;
		try {
			airTicketingReq = buildTicketingReq(airTicketingReq, supplierBookingCreditCard);
			bindingService.setProxyAndAuthentication(airService, "IssueTicket");
			airTicketingRsp = airService.service(airTicketingReq);
			if (!checkAnyErrors(airTicketingRsp)) {
				commitTicketNumbers();
				isSuccess = true;
			}
		} catch (AirFaultMessage airFaultMessage) {
			logCriticalMessage(airFaultMessage.getMessage());
			throw new SupplierUnHandledFaultException(airFaultMessage.getMessage());
		} catch (Exception e) {
			logCriticalMessage(e.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSuccess;
	}

	private AirTicketingReq buildTicketingReq(AirTicketingReq airTicketingReq,
			CreditCardInfo supplierBookingCreditCard) {
		if (airTicketingReq == null) {
			airTicketingReq = new AirTicketingReq();
		}
		buildBaseCoreRequest(airTicketingReq);
		AirReservationLocatorCode_type0 airReservationLocatorCode = new AirReservationLocatorCode_type0();
		airReservationLocatorCode.setTypeLocatorCode(reservationPNR);
		buildFOPDetails(airTicketingReq, supplierBookingCreditCard);
		airTicketingReq.setAirReservationLocatorCode(airReservationLocatorCode);
		return airTicketingReq;
	}

	private boolean checkAnyErrors(UniversalRecordModifyRsp recordModifyRsp) {
		boolean isError = false;
		StringJoiner message = new StringJoiner("");
		isError = super.checkAnyErrors(recordModifyRsp);
		if (!isError && Objects.nonNull(recordModifyRsp.getUniversalModifyFailureInfo()) && ArrayUtils
				.isNotEmpty(recordModifyRsp.getUniversalModifyFailureInfo().getUniversalModifyCommandError())) {
			message.add(
					recordModifyRsp.getUniversalModifyFailureInfo().getUniversalModifyCommandError()[0].getString());
		}
		if (StringUtils.isNotBlank(message.toString())) {
			isError = true;
			logCriticalMessage(message.toString());
		}
		return isError;
	}

	private boolean checkAnyErrors(AirTicketingRsp airTicketingRsp) {
		boolean isError = false;
		StringJoiner message = new StringJoiner("");
		isError = super.checkAnyErrors(airTicketingRsp);
		if (!isError && ArrayUtils.isNotEmpty(airTicketingRsp.getBaseRspSequence_type0().getTicketFailureInfo())) {
			message.add(airTicketingRsp.getBaseRspSequence_type0().getTicketFailureInfo()[0].getMessage());
		}
		if (StringUtils.isNotBlank(message.toString())) {
			isError = true;
			logCriticalMessage(message.toString());
		}
		return isError;
	}

	private void commitTicketNumbers() {
		UniversalRecordRetrieveRsp univRecordRetrieveRS = this.getUnivRecordRetrieveRS(csrPnr);
		setTravellerMiscInfo(getList(univRecordRetrieveRS.getUniversalRecord().getBookingTraveler()));
		addTicketNumbers(univRecordRetrieveRS.getUniversalRecord());
	}

	private void addTicketNumbers(UniversalRecord_type0 universalRecord) {
		Map<String, String> passengerWiseTicketNumbers = new LinkedHashMap<>();

		for (AirReservation_type0 airReservation : universalRecord.getUniversalRecordChoice_type7()
				.getAirReservation()) {
			for (com.travelport.www.schema.air_v47_0.TicketInfo_type0 ticketInfo : airReservation.getDocumentInfo()
					.getTicketInfo()) {
				passengerWiseTicketNumbers.put(ticketInfo.getBookingTravelerRef().getTypeRef(), ticketInfo.getNumber());
			}
		}

		setTicketNumbers(passengerWiseTicketNumbers);
	}

	private void setTicketNumbers(Map<String, String> passengerWiseTicketNumbers) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
				String ticketNumber =
						passengerWiseTicketNumbers.get(flightTravellerInfo.getMiscInfo().getPassengerKey());
				flightTravellerInfo.setTicketNumber(ticketNumber);
			}
		}
	}

	private void buildFOPDetails(AirTicketingReq airTicketingReq, CreditCardInfo creditCardInfo) {
		AirTicketingModifiers_type0 airTicketingModifiers = new AirTicketingModifiers_type0();
		FormOfPayment_type0 formOfPayment = new FormOfPayment_type0();
		FormOfPaymentChoice_type1 ccChoice = new FormOfPaymentChoice_type1();
		if (creditCardInfo != null && StringUtils.isNotBlank(creditCardInfo.getCardNumber())) {
			formOfPayment.setType(getType_Type6(CREDIT_CARD_MODE));
			formOfPayment.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
			CreditCard_type0 creditCard = new CreditCard_type0();
			String cardNumber = creditCardInfo.getCardNumber();
			String cardHolderName = creditCardInfo.getHolderName();
			String cardType = creditCardInfo.getCardType().getVendorCode();
			creditCard.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
			creditCard.setExpDate(getExpiryDate(creditCardInfo));
			creditCard.setNumber(getTypeCreditCard(cardNumber));
			if (StringUtils.isNotBlank(cardHolderName))
				creditCard.setName(getNameType(cardHolderName));
			creditCard.setType(getTypeMerchant(cardType));
			setBillingAddress(creditCard);
			ccChoice.setCreditCard(creditCard);
		} else {
			formOfPayment.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
			formOfPayment.setType(getType_Type6(BSP));
		}
		formOfPayment.setFormOfPaymentChoice_type1(ccChoice);
		airTicketingModifiers.addFormOfPayment(formOfPayment);
		airTicketingReq.addAirTicketingModifiers(airTicketingModifiers);
	}


	private YearMonth getExpiryDate(CreditCardInfo creditCardInfo) {
		YearMonth yearMonth = null;
		try {
			if (StringUtils.isNotBlank(creditCardInfo.getExpiry())) {
				String[] monthYear = creditCardInfo.getExpiry().split("/");
				yearMonth = new YearMonth(Integer.parseInt(monthYear[1]), Integer.parseInt(monthYear[0]));
			}
		} catch (Exception e) {
			log.error("Failed to parse credit card expiry date {}", bookingId, e);
		}
		return yearMonth;
	}

	private void setBillingAddress(CreditCard_type0 creditCard) {
		ClientGeneralInfo companyInfo = ServiceCommunicatorHelper.getClientInfo();
		if (companyInfo != null && StringUtils.isNotBlank(companyInfo.getNationality())) {
			TypeStructuredAddress billingAddress = new TypeStructuredAddress();
			billingAddress.setAddressName(getAddressName(companyInfo.getAddress1()));
			billingAddress.setCity(getCityType(companyInfo.getCity()));
			State_type0 state = new State_type0();
			state.setString(companyInfo.getState());
			billingAddress.setState(state);
			billingAddress.setPostalCode(getPostalCode(companyInfo.getPostalCode()));
			billingAddress.setCountry(getCountryType(companyInfo.getNationality()));
			creditCard.setBillingAddress(billingAddress);
		}
	}

	public boolean isAirlinePNRAvailable(String crsPnr) {
		AtomicBoolean isAirlinePNRUpdated = new AtomicBoolean();
		int tryCount = 1;
		while (tryCount <= 3 && !isAirlinePNRUpdated.get()) {
			try {
				Thread.sleep(1000 * SLEEP_TIME_IN_SECS);
				UniversalRecordRetrieveRsp univRecordRetrieveRS = this.getUnivRecordRetrieveRS(crsPnr);
				if (Objects.nonNull(univRecordRetrieveRS)) {

					reservationPNR = univRecordRetrieveRS.getUniversalRecord().getUniversalRecordChoice_type7()
							.getAirReservation()[0].getLocatorCode().getTypeLocatorCode();
					setReservetionPNRForSegment(reservationPNR);
					for (SupplierLocator_type0 supplierLocator : univRecordRetrieveRS.getUniversalRecord()
							.getUniversalRecordChoice_type7().getAirReservation()[0].getSupplierLocator()) {
						setAirlinePnrForSegment(supplierLocator.getSupplierCode().getTypeCarrier(),
								supplierLocator.getSupplierLocatorCode());
						isAirlinePNRUpdated.set(true);
					}

					pnr = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
					log.info("Reservation PNR {} for bookingid {} ", reservationPNR, bookingId);
					log.info("AirLine Pnr {} for bookingid {} ", pnr, bookingId);
					if (univRecordRetrieveRS.getUniversalRecord() != null
							&& univRecordRetrieveRS.getUniversalRecord().getGeneralRemark() != null) {
						try {
							ticketingTimeLimit = parseTimelimit(
									getList(univRecordRetrieveRS.getUniversalRecord().getGeneralRemark()));
						} catch (ParseException e) {
							log.error("Exception occurred while parsing hold time limit for booking id {} excep {}",
									bookingId, e);
						}
					}
				}
			} catch (InterruptedException e) {
				log.error("Thread Intrepputed for booking {} ,error ", bookingId, e);
				throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
			} finally {
				tryCount++;
			}
		}
		if (!isAirlinePNRUpdated.get()) {
			log.info("Airline PNR Not Generated For BookingId {}", bookingId);
		}
		return isAirlinePNRUpdated.get();
	}

	public boolean isPNRStatusActive() {
		boolean isActive = false;
		isCommissionAlreadyAdded = false;
		UniversalRecordRetrieveRsp univRecordRetrieveRS = this.getUnivRecordRetrieveRS(csrPnr);
		createAirPricingInfoMap(
				univRecordRetrieveRS.getUniversalRecord().getUniversalRecordChoice_type7().getAirReservation()[0]
						.getAirPricingInfo());
		if (org.apache.commons.collections4.MapUtils.isNotEmpty(airPricingInfoPaxMap)
				&& Objects.nonNull(univRecordRetrieveRS)) {
			isActive = getList(univRecordRetrieveRS.getUniversalRecord().getActionStatus()).get(0).getType().getValue()
					.equals("ACTIVE") ? true : false;
			isCommissionAlreadyAdded = TravelPortUtils.checkIsCommissionAlreadyAdded(bookingId, univRecordRetrieveRS);
			buildSegmentKeys(univRecordRetrieveRS);
		}
		return isActive;
	}

	private void setReservetionPNRForSegment(String reservationPNR) {
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.updateReservationPnr(reservationPNR);
		});
	}

	private LocalDateTime parseTimelimit(List<GeneralRemark_type0> generalRemarkList) throws ParseException {
		AirlineTimeLimitData airlineTimeLimitData = AirUtils
				.getAirlineTimeLimitData(segmentInfos.get(0).getAirlineCode(false), sourceConfiguration, bookingUser);
		for (GeneralRemark_type0 generalRemark : generalRemarkList) {
			String textTimelimitData = generalRemark.getRemarkData();
			if (airlineTimeLimitData != null) {
				String rejexPattern = airlineTimeLimitData.getRegexPattern();
				String timeFormat = airlineTimeLimitData.getTimeFormat();
				List<String> matchedStrings = TgsStringUtils.parseStringByRejex(textTimelimitData, rejexPattern);
				if (CollectionUtils.isNotEmpty(matchedStrings)) {
					// Selecting first matched pattern of hold time limit.
					String timeLimitData = matchedStrings.get(0);
					LocalDateTime ticketingTimeLimit = null;
					ticketingTimeLimit = TgsDateUtils.convertStringToDate(timeLimitData, timeFormat);
					if (ticketingTimeLimit != null) {
						LocalDateTime now = LocalDateTime.now();
						// To handle without year case. TimeLimit without year will set the minimum yearof
						// LocalDateTime.
						if (ticketingTimeLimit.isBefore(now)) {
							ticketingTimeLimit = ticketingTimeLimit.withYear(now.getYear());
							// Booking at the last day of the year & getting time limit of next year.
							if (ticketingTimeLimit.isBefore(now)) {
								ticketingTimeLimit = ticketingTimeLimit.plusYears(1);
							}
						}
						log.info("Ticketing time limit {} for bookingId {} ", bookingId, ticketingTimeLimit);
						return ticketingTimeLimit;
					}
				}

			}
		}
		return null;
	}


	public double getSupplierAmount(AirPriceRsp airPriceRsp, BookingSegments bookingSegments) {
		double supplierAmount = 0.0;
		AirPricingSolution_type0 validAirPricingSolution = null;
		boolean isFareBasisCodeChanged = false;
		for (AirPricingSolution_type0 airPricingSolution : airPriceRsp.getAirPriceResult()[0].getAirPricingSolution()) {
			for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
				SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
				Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
				if (legNum == 0) {
					FareInfo_type0 fareInfo =
							getFareInfoFromBookingInfo(getList(airPricingSolution.getAirPricingInfo()).get(0),
									getList(airPriceRsp.getAirItinerary().getAirSegment()), segmentInfo);
					String fareBasisCode = fareInfo.getFareBasis();
					if (!fareBasisCode.equals(segmentInfo.getPriceInfo(0).getFareBasis(PaxType.ADULT))) {
						log.error("Fare Basis code changed for this bookingID {}", bookingId);
						isFareBasisCodeChanged = true;
						break;
					}
				}
			}
			if (!isFareBasisCodeChanged) {
				validAirPricingSolution = airPricingSolution;
				break;
			}
		}
		if (validAirPricingSolution != null) {
			supplierAmount = getSupplierAmount(validAirPricingSolution.getAirPricingInfo());
		}
		return supplierAmount;
	}

	private double getSupplierAmount(AirPricingInfo_type0[] airPricingInfoList) {
		double supplierAmount = 0.0;
		for (AirPricingInfo_type0 airPricingInfo : airPricingInfoList) {
			Double airlineTotal = getAmountBasedOnCurrency(airPricingInfo.getTotalPrice().getTypeMoney(),
					airPricingInfo.getApproximateBasePrice().getTypeMoney().substring(0, 3), 1.0);
			supplierAmount += (airlineTotal * airPricingInfo.getPassengerType().length);
		}
		return supplierAmount;
	}


	public void cancelFiledFare(String crsPNR) {
		UniversalRecordModifyReq universalRecordModifyReq = new UniversalRecordModifyReq();
		buildBaseUniversalModifyRequest(universalRecordModifyReq);
		buildAirdelete(universalRecordModifyReq);
		modifyUniversalRecord(universalRecordModifyReq, "DELETEFILEDFARE");
	}

	private void buildAirdelete(UniversalRecordModifyReq universalRecordModifyReq) {
		// String airlinePnr = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
		// int paxTypesNumber = segmentInfos.get(0).getPriceInfoList().get(0).getFareDetails().size();
		ArrayList<String> airPricingInfoKeys = new ArrayList<String>();
		airPricingInfoPaxMap.forEach((k, v) -> {
			airPricingInfoKeys.addAll(v);
		});
		for (int index = 0; index < airPricingInfoKeys.size(); index++) {
			UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
			universalModifyCmd.setKey(getTypeRef(String.valueOf(index)));
			AirDelete_type0 airDelete = new AirDelete_type0();
			airDelete.setElement(TypeElement.AirPricingInfo);
			airDelete.setKey(getTypeRef(airPricingInfoKeys.get(index)));
			airDelete.setReservationLocatorCode(getTypeLocatorCode(TravelPortUtils.getReservationPNR(segmentInfos)));
			universalModifyCmd.setAirDelete(airDelete);
			universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		}

	}

	private void buildRecordIdentifier(UniversalRecordModifyReq universalRecordModifyReq, String crsPNR) {
		RecordIdentifier_type0 recordIdentifier = new RecordIdentifier_type0();
		if (CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList()) && StringUtils
				.isNotBlank(segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getProviderCode())) {
			recordIdentifier.setProviderCode(getProviderCode(providerCode));
		}
		recordIdentifier.setProviderLocatorCode(getTypeProviderLocatorCode(crsPNR));
		recordIdentifier.setUniversalLocatorCode(getTypeLocatorCode(universalLocatorCode));
		universalRecordModifyReq.setRecordIdentifier(recordIdentifier);
	}

	public void fileNewFare(String crsPNR) {
		UniversalRecordModifyRsp universalRecordModifyRsp = getNewFileFareRsp(crsPNR);
		supplierAmount = getSupplierAmount(
				universalRecordModifyRsp.getUniversalRecord().getUniversalRecordChoice_type7().getAirReservation()[0]
						.getAirPricingInfo());
	}

	public String commitBooking() {
		return endSession(false, "Commit");
	}


	public boolean ancillaries(GstInfo gstInfo, UniversalRecordModifyReq universalRecordModifyReq,
			AtomicInteger keyCount) {
		boolean modifyUniversalRecord = false;
		if (TravelPortUtils.isPassportAvailable(travellerInfos)) {
			buildPassportInformation(universalRecordModifyReq, segmentInfos, keyCount);
			modifyUniversalRecord = true;
		}
		if (TravelPortUtils.isFqFlierAvailable(travellerInfos)) {
			buildFrequentFlierInformation(universalRecordModifyReq, keyCount);
			modifyUniversalRecord = true;
		}
		if (StringUtils.isNotBlank(gstInfo.getGstNumber())) {
			buildGstInformation(universalRecordModifyReq, gstInfo, keyCount, plattingCarrier);
			modifyUniversalRecord = true;
		}
		if (TravelPortUtils.isSeatAddedInTrip(segmentInfos)) {
			buildSeatInformation(universalRecordModifyReq, segmentInfos, keyCount);
			modifyUniversalRecord = true;
		}
		return modifyUniversalRecord;
	}


	private void buildSeatInformation(UniversalRecordModifyReq universalRecordModifyReq,
			List<SegmentInfo> bookingSegmentInfos, AtomicInteger keyCount) {
		UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
		universalModifyCmd.setKey(getTypeRef(String.valueOf(keyCount.get())));
		AirAdd_type0 airAdd = new AirAdd_type0();
		airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
		for (SegmentInfo segmentInfo : bookingSegmentInfos) {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				List<FlightTravellerInfo> travellerInfoList = segmentInfo.getTravellerInfo();
				for (FlightTravellerInfo flightTravellerInfo : travellerInfoList) {
					if (StringUtils.isNotBlank(flightTravellerInfo.getSsrSeatInfo().getCode())) {
						SpecificSeatAssignment_type0 specificSeatAssignment = new SpecificSeatAssignment_type0();
						specificSeatAssignment
								.setBookingTravelerRef(getTypeRef(flightTravellerInfo.getMiscInfo().getPassengerKey()));
						specificSeatAssignment
								.setSegmentRef(getTypeRef(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey()));
						specificSeatAssignment.setSeatId(flightTravellerInfo.getSsrSeatInfo().getCode());
						airAdd.addSpecificSeatAssignment(specificSeatAssignment);
					}
				}
			}
		}
		universalModifyCmd.setAirAdd(airAdd);
		universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		keyCount.getAndIncrement();
	}


	private UniversalRecordModifyRsp modifyUniversalRecord(UniversalRecordModifyReq modifyReq, String key) {
		UniversalRecordModifyServiceStub modifyServicde = bindingService.getUniversalRecordModifyService();
		UniversalRecordModifyRsp recordModifyRsp = null;
		try {
			listener.setType(AirUtils.getLogType(key, configuration));
			modifyServicde._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(modifyServicde, key);
			recordModifyRsp = modifyServicde.service(modifyReq, getSessionContext(false), getSupportedVersion());
			version = recordModifyRsp.getUniversalRecord().getVersion().getTypeURVersion();
			if (checkAnyErrors(recordModifyRsp)) {
				log.error("Universal Modify failed {} type {} cause {}", bookingId, key,
						String.join(",", criticalMessageLogger));
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
			setTravellerMiscInfo(getList(recordModifyRsp.getUniversalRecord().getBookingTraveler()));
		} catch (AvailabilityFaultMessage | UniversalModifyFaultMessage | UniversalRecordFaultMessage
				| RemoteException fault) {
			throw new SupplierUnHandledFaultException(fault.getMessage());
		} finally {
			modifyServicde._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return recordModifyRsp;
	}

	private void buildBaseUniversalModifyRequest(UniversalRecordModifyReq universalRecordModifyReq) {
		buildBaseCoreRequest(universalRecordModifyReq);
		universalRecordModifyReq.setReturnRecord(Boolean.TRUE);
		universalRecordModifyReq.setVersion(getTypeURversion(version));
		buildRecordIdentifier(universalRecordModifyReq, csrPnr);
	}


	public void addContactDetails(String... remarks) {
		UniversalRecordModifyReq universalRecordModifyReq = new UniversalRecordModifyReq();
		buildBaseUniversalModifyRequest(universalRecordModifyReq);
		UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
		universalModifyCmd.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
		buildGeneralRemark(universalModifyCmd, remarks);
		AtomicInteger atomicInteger = new AtomicInteger();
		AirAdd_type0 airAdd = new AirAdd_type0();
		airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
		airAdd.setBookingTravelerRef(
				getTypeRef(segmentInfos.get(0).getTravellerInfo().get(0).getMiscInfo().getPassengerKey()));
		buildSSRComponents(airAdd, "CTCM", segmentInfos.get(0).getPlatingCarrier(null),
				AirSupplierUtils.getContactNumber(deliveryInfo), PASSPORT_ACTON_CODE, atomicInteger);
		buildSSRComponents(airAdd, "CTCE", segmentInfos.get(0).getPlatingCarrier(null),
				AirSupplierUtils.getEmailId(deliveryInfo), PASSPORT_ACTON_CODE, atomicInteger);
		universalModifyCmd.setAirAdd(airAdd);
		universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		modifyUniversalRecord(universalRecordModifyReq, "ContactDetails");
	}


	private void buildGeneralRemark(UniversalModifyCmd_type0 universalModifyCmd, String... remarks) {
		UniversalAdd_type0 universalAdd = new UniversalAdd_type0();
		universalAdd.addGeneralRemark(buildAdditionalGeneralRemark(bookingId));
		if (ArrayUtils.isNotEmpty(remarks)) {
			for (String additionalRmk : remarks) {
				universalAdd.addGeneralRemark(buildAdditionalGeneralRemark(additionalRmk));
			}
		}
		universalModifyCmd.setUniversalAdd(universalAdd);
	}

	private GeneralRemark_type0 buildAdditionalGeneralRemark(String cmd) {
		GeneralRemark_type0 generalRemark = new GeneralRemark_type0();
		generalRemark.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
		generalRemark.setTypeInGds(getTypeGDS("Basic"));
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
			generalRemark.setProviderCode(getProviderCode(configuration.getSupplierCredential().getProviderCode()));
		}
		generalRemark.setRemarkData(cmd);
		return generalRemark;
	}

	private void buildGstInformation(UniversalRecordModifyReq universalRecordModifyReq, GstInfo gstInfo,
			AtomicInteger atomicInteger, String platingCarrier) {
		AtomicInteger index = new AtomicInteger(1);

		if (false && StringUtils.isNotBlank(gstInfo.getAddress())) {
			// Its not mandatory to send and address are complicated fields by "/" in state,city,pincode
			String freeText = "IND/" + gstInfo.getAddress() + "/" + gstInfo.getCityName() + "/" + gstInfo.getState()
					+ "/" + gstInfo.getPincode();
			buildSSRComponents(universalRecordModifyReq, "GSTA", platingCarrier, freeText, "NN", index, atomicInteger);
		}

		if (StringUtils.isNotBlank(gstInfo.getGstNumber()) && StringUtils.isNotBlank(gstInfo.getRegisteredName())) {
			String registeredName = BaseUtils.getCleanGstName(gstInfo.getRegisteredName()).replaceAll("[^\\w\\s]", "")
					.replaceAll(" ", "");
			registeredName = TravelPortUtils.getValidRegisteredName(registeredName);
			String freeText = "IND/" + gstInfo.getGstNumber() + "/" + registeredName;
			buildSSRComponents(universalRecordModifyReq, "GSTN", platingCarrier, freeText, "NN", index, atomicInteger);
		}

		if (StringUtils.isNotBlank(gstInfo.getMobile())) {
			String freeText = "IND/" + gstInfo.getMobile();
			buildSSRComponents(universalRecordModifyReq, "GSTP", platingCarrier, freeText, "NN", index, atomicInteger);
		}

		if (StringUtils.isNotBlank(gstInfo.getEmail())) {
			String email = AirSupplierUtils.removeIllegalCharacters(gstInfo.getEmail());
			email = gstInfo.getEmail().replaceAll("[@]", "//");
			email = email.replaceAll("[-]", "./");
			email = email.replaceAll("[_]", "..");
			String freeText = "IND/" + email;
			buildSSRComponents(universalRecordModifyReq, "GSTE", platingCarrier, freeText, "NN", index, atomicInteger);
		}
	}


	private void buildSSRComponents(UniversalRecordModifyReq universalRecordModifyReq, String type,
			String platingCarrier, String freeText, String status, AtomicInteger index, AtomicInteger keyCount) {
		UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
		universalModifyCmd.setKey(getTypeRef(String.valueOf(keyCount.get())));
		AirAdd_type0 airAdd = new AirAdd_type0();
		airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
		buildSSRComponents(airAdd, type, platingCarrier, freeText, status, index);
		universalModifyCmd.setAirAdd(airAdd);
		universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		keyCount.getAndIncrement();
	}

	private void buildSSRComponents(AirAdd_type0 airAdd, String type, String platingCarrier, String freeText,
			String status, AtomicInteger index) {
		SSR_type0 ssr = new SSR_type0();
		ssr.setKey(getTypeRef(String.valueOf(index.get())));
		ssr.setType(getTypeSSrCode(type));
		ssr.setCarrier(getTypeCarrierCode(platingCarrier));
		ssr.setStatus(status);
		ssr.setFreeText(getTypeSSrFreeText(freeText));
		airAdd.addSSR(ssr);
		index.getAndIncrement();
	}


	private void buildFrequentFlierInformation(UniversalRecordModifyReq universalRecordModifyReq,
			AtomicInteger keyCount) {
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			if (travellerInfo.getMiscInfo() != null
					&& StringUtils.isNotBlank(travellerInfo.getMiscInfo().getPassengerKey())
					&& MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())
					&& travellerInfo.getFrequentFlierMap().get(plattingCarrier) != null) {
				UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
				universalModifyCmd.setKey(getTypeRef(String.valueOf(keyCount.get())));
				AirAdd_type0 airAdd = new AirAdd_type0();
				airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
				for (Entry<String, String> entrySet : travellerInfo.getFrequentFlierMap().entrySet()) {
					LoyaltyCard_type0 loyaltyCard = new LoyaltyCard_type0();
					loyaltyCard.setKey(getTypeRef(travellerInfo.getMiscInfo().getPassengerKey()));
					loyaltyCard.setSupplierType(TypeProduct.Air);
					loyaltyCard.setSupplierCode(getTypeCarrierCode(entrySet.getKey()));
					loyaltyCard.setCardNumber(getTypeCardNumber(entrySet.getValue().replaceAll("[^\\w\\s]", "")));
					airAdd.addLoyaltyCard(loyaltyCard);
				}
				universalModifyCmd.setAirAdd(airAdd);
				universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
				keyCount.getAndIncrement();
			}
		}
	}


	private void buildPassportInformation(UniversalRecordModifyReq universalRecordModifyReq,
			List<SegmentInfo> bookingSegmentInfos, AtomicInteger keyCount) {
		Set<String> uniqueCarriers = TravelPortUtils.getUniqueAirlines(bookingSegmentInfos);
		for (String carrier : uniqueCarriers) {
			for (FlightTravellerInfo travellerInfo : travellerInfos) {
				if (StringUtils.isNotBlank(travellerInfo.getPassportNumber()) && travellerInfo.getExpiryDate() != null
						&& travellerInfo.getMiscInfo() != null
						&& StringUtils.isNotBlank(travellerInfo.getMiscInfo().getPassengerKey())
						&& travellerInfo.getDob() != null) {
					UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
					universalModifyCmd.setKey(getTypeRef(String.valueOf(keyCount.get())));
					AirAdd_type0 airAdd = new AirAdd_type0();
					airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
					airAdd.setBookingTravelerRef(getTypeRef(travellerInfo.getMiscInfo().getPassengerKey()));
					SSR_type0 ssr = new SSR_type0();
					ssr.setType(getTypeSSrCode(TRAVEL_DOCUMENTS_INFORMATION));
					ssr.setStatus(PASSPORT_ACTON_CODE);
					ssr.setCarrier(getTypeCarrierCode(carrier));
					ssr.setFreeText(getTypeSSrFreeText(TravelPortUtils.buildPassportFreeText(travellerInfo)));
					airAdd.addSSR(ssr);
					universalModifyCmd.setAirAdd(airAdd);
					keyCount.getAndIncrement();
					universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
				}
			}
		}
	}

	protected List<AirPricingInfo_type0> buildAirPricingInfoList(Map<PaxType, FareDetail> fareDetails,
			List<SegmentInfo> segmentInfos) {
		List<AirPricingInfo_type0> airPricingInfoList = new ArrayList<>();
		String fareInfoKey = TravelPortUtils.keyGenerator();
		Integer paxUniqueGroup = 1;
		Map<PaxType, List<String>> airPricingInfoMap =
				segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getPaxPricingInfo();
		for (Map.Entry<PaxType, FareDetail> entry : fareDetails.entrySet()) {
			/**
			 * @implNote : PaxAirPricingInfoCount - How Much Pricing Info needs to be created <br>
			 *           (Special Case if child has different age then new pricing info has to<br>
			 *           be created
			 */
			int airPricingInfoCount = TravelPortUtils.getPaxAirPricingInfoCount(
					AirUtils.getParticularPaxTravellerInfo(travellerInfos, entry.getKey()), entry.getKey());
			List<String> paxKeyList = airPricingInfoMap.get(entry.getKey());
			for (int airPricingIndex = 0; airPricingIndex < airPricingInfoCount; airPricingIndex++) {
				AirPricingInfo_type0 airPricingInfo = new AirPricingInfo_type0();
				airPricingInfo.setKey(getTypeRef(paxKeyList.get(airPricingIndex)));
				String totalAirlineFare = TravelPortUtils.getTotalAirlineFare(currencyCode, entry.getValue());
				String baseFare = TravelPortUtils.getBaseFare(currencyCode, entry.getValue());
				String taxFare = TravelPortUtils.getTaxFare(currencyCode, entry.getValue());
				airPricingInfo.setTotalPrice(getTypeMoney(totalAirlineFare));
				airPricingInfo.setBasePrice(getTypeMoney(baseFare));
				airPricingInfo.setTaxes(getTypeMoney(taxFare));
				airPricingInfo.setPricingMethod(TypePricingMethod.Guaranteed);
				airPricingInfo.setPlatingCarrier(getTypeCarrierCode(plattingCarrier));
				if (StringUtils.isNotBlank(providerCode)) {
					airPricingInfo.setProviderCode(getTypeProviderCode(providerCode));
				}
				airPricingInfo.setAirPricingInfoGroup(paxUniqueGroup);
				airPricingInfo.setETicketability(TypeEticketability.Yes);
				String accountCode = TravelPortUtils.getAccountCodeFromTrip(segmentInfos);
				AirPricingModifiers_type0 airPricingModifiers = getAirPricingModifiers(segmentInfos);
				airPricingInfo.setAirPricingModifiers(airPricingModifiers);
				buildFareInfo(segmentInfos, accountCode, airPricingInfo, fareInfoKey, entry.getKey());
				buildBookingTravellerAirPricing(segmentInfos, airPricingInfo, entry.getKey(), airPricingIndex,
						airPricingInfoCount);
				airPricingInfoList.add(airPricingInfo);
				paxUniqueGroup++;
			}
		}
		return airPricingInfoList;
	}


	private void buildBookingTravellerAirPricing(List<SegmentInfo> segmentInfos, AirPricingInfo_type0 airPricingInfo,
			PaxType paxType, int airPricingIndex, int airPricingInfoCount) {
		List<FlightTravellerInfo> particularPaxTravellerInfo =
				AirUtils.getParticularPaxTravellerInfo(segmentInfos.get(0).getTravellerInfo(), paxType);
		if (airPricingInfoCount == 1) {
			for (FlightTravellerInfo travellerInfo : particularPaxTravellerInfo) {
				PassengerType_type0 passengerType = new PassengerType_type0();
				passengerType.setCode(getTypePTC(paxType.getType()));
				passengerType.setBookingTravelerRef(travellerInfo.getMiscInfo().getPassengerKey());
				if (PaxType.INFANT.getType().equals(travellerInfo.getPaxType().getType())) {
					passengerType.setPricePTCOnly(Boolean.TRUE);
					passengerType.setAge(new BigInteger(TravelPortUtils.getInfantAge(travellerInfo)));
				} else if (PaxType.CHILD.getType().equals(travellerInfo.getPaxType().getType())) {
					passengerType.setAge(new BigInteger(CHILD_AGE));
				}
				airPricingInfo.addPassengerType(passengerType);
			}
		} else {
			// Handling the Special case of Child when two different age passed - refer wiki
			FlightTravellerInfo travellerInfo = particularPaxTravellerInfo.get(airPricingIndex);
			PassengerType_type0 passengerType = new PassengerType_type0();
			passengerType.setCode(getTypePTC(paxType.getType()));
			passengerType.setBookingTravelerRef(travellerInfo.getMiscInfo().getPassengerKey());
			passengerType.setAge(new BigInteger(TravelPortUtils.getAge(travellerInfo)));
			airPricingInfo.addPassengerType(passengerType);
		}
	}

	private void buildFareInfo(List<SegmentInfo> segmentInfos, String accountCode, AirPricingInfo_type0 airPricingInfo,
			String fareInfoKey, PaxType paxType) {
		FareInfo_type0 fareInfo = null;
		BookingInfo_type0 bookingInfo = null;
		for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = TravelPortUtils.getNextSegmentNumber(segmentIndex, segmentInfos);
			FareDetail fareDetail = segmentInfo.getPriceInfo(0).getFareDetails().get(paxType);
			if (legNum == 0) {
				fareInfo = new FareInfo_type0();
				fareInfo.setKey(getTypeRef(String.valueOf(fareInfoKey)));
				fareInfo.setFareBasis(fareDetail.getFareBasis());
				fareInfo.setPassengerTypeCode(getTypePTC(paxType.getType()));
				fareInfo.setOrigin(getIATACode(segmentInfo.getDepartureAirportCode()));
				fareInfo.setKey(getTypeRef(String.valueOf(fareInfoKey)));
				fareInfo.setEffectiveDate(LocalDateTime.now().toLocalDate().toString());
				if (StringUtils.isNotEmpty(accountCode)) {
					AccountCode_type0 accountCode1 = new AccountCode_type0();
					accountCode1.setCode(getCode(accountCode));
					fareInfo.addAccountCode(accountCode1);
				}
				bookingInfo = new BookingInfo_type0();
				bookingInfo.setFareInfoRef(getTypeRef(String.valueOf(fareInfoKey)));
				bookingInfo
						.setSegmentRef(getTypeRef(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey()));
				bookingInfo.setBookingCode(fareDetail.getClassOfBooking());
				bookingInfo.setCabinClass(TravelPortUtils.getCabinClass(fareDetail.getCabinClass().getName()));
			}
			fareInfo.setDestination(getIATACode(segmentInfo.getArrivalAirportCode()));
			fareInfoKey = TravelPortUtils.keyGenerator();
			if (nextSegmentLegNum == 0) {
				airPricingInfo.addFareInfo(fareInfo);
				airPricingInfo.addBookingInfo(bookingInfo);
			}
		}
	}


	public void setNewSupplierFare(BookingSegments bookingSegments) {
		AirPriceRsp airPriceRsp = getAirPricingResponse(bookingSegments);;
		supplierAmount = getSupplierAmount(airPriceRsp, bookingSegments);
	}

	public AirPriceRsp getAirPricingResponse(BookingSegments bookingSegments) {
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("AirPrice", configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		AirPriceReq airPriceReq = buildAirPriceRequest(bookingSegments, false);
		AirPriceRsp airPriceRsp = null;
		try {
			bindingService.setProxyAndAuthentication(airService, "AirPrice");
			airPriceRsp = airService.service(airPriceReq, getSessionContext(true));
			if (checkAnyErrors(airPriceRsp)) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (AirFaultMessage | RemoteException airFaultMessage) {
			logCriticalMessage(airFaultMessage.getMessage());
			throw new SupplierUnHandledFaultException(airFaultMessage.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return airPriceRsp;
	}

	private AirPriceReq buildAirPriceRequest(BookingSegments bookingSegments, boolean isFareBasisCodeReq) {
		AirPriceReq airPriceReq = new AirPriceReq();
		buildBaseCoreRequest(airPriceReq);
		AirItinerary_type0 airItinerary = new AirItinerary_type0();
		List<TypeBaseAirSegment> typeBaseAirSegmentList = new ArrayList<>();
		buildAirSegmentList(typeBaseAirSegmentList, bookingSegments.getSegmentInfos(), true, true);
		airItinerary.setAirSegment(typeBaseAirSegmentList.toArray(new TypeBaseAirSegment[0]));
		airPriceReq.setAirItinerary(airItinerary);
		AirPricingModifiers_type0 airPricingModifiers = getAirPricingModifiers(bookingSegments.getSegmentInfos());
		airPriceReq.setAirPricingModifiers(airPricingModifiers);
		buildSearchPassengers(getPaxCountList(), airPriceReq);
		buildAirPricingCommand(bookingSegments.getSegmentInfos(), airPriceReq, isFareBasisCodeReq);
		return airPriceReq;
	}


	protected void buildSearchPassengers(List<FlightTravellerInfo> travellerInfos, AirPriceReq airPriceReq) {
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			SearchPassenger_type0 searchPassenger = new SearchPassenger_type0();
			searchPassenger.setCode(getTypePTC(travellerInfo.getPaxType().getType()));
			searchPassenger.setBookingTravelerRef(travellerInfo.getMiscInfo().getPassengerKey());
			if (PaxType.CHILD.getType().equals(travellerInfo.getPaxType().getType())) {
				searchPassenger.setAge(new BigInteger(TravelPortConstants.CHILD_AGE));
			}
			if (PaxType.INFANT.getType().equals(travellerInfo.getPaxType().getType())) {
				searchPassenger.setAge(new BigInteger(TravelPortConstants.INFANT_AGE));
				searchPassenger.setPricePTCOnly(Boolean.TRUE);
			}
			airPriceReq.addSearchPassenger(searchPassenger);
		}
	}

	protected UniversalRecordModifyRsp getNewFileFareRsp(String crsPnr) {
		UniversalRecordModifyReq universalRecordModifyReq = new UniversalRecordModifyReq();
		buildBaseUniversalModifyRequest(universalRecordModifyReq);
		// String airlinePnr = segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo().get(0).getPnr();
		UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
		universalModifyCmd.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
		AirAdd_type0 airAdd = new AirAdd_type0();
		airAdd.setReservationLocatorCode(getTypeLocatorCode(TravelPortUtils.getReservationPNR(segmentInfos)));
		Map<PaxType, FareDetail> fareDetails = segmentInfos.get(0).getPriceInfo(0).getFareDetails();
		List<AirPricingInfo_type0> airPricingInfoList = buildAirPricingInfoList(fareDetails, segmentInfos);
		airAdd.setAirPricingInfo(airPricingInfoList.toArray(new AirPricingInfo_type0[0]));
		universalModifyCmd.setAirAdd(airAdd);
		universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
		return modifyUniversalRecord(universalRecordModifyReq, "FileNewFare");
	}

	public void addSpecialServiceAndAncillaries(String crspnr, GstInfo gstInfo) {
		List<FlightTravellerInfo> bookingTravellerInfos =
				segmentInfos.get(0).getBookingRelatedInfo().getTravellerInfo();
		UniversalRecordModifyReq universalRecordModifyReq = new UniversalRecordModifyReq();
		buildBaseUniversalModifyRequest(universalRecordModifyReq);
		boolean isSSRAvailable = false;
		AtomicInteger universalCmdIndex = new AtomicInteger(1);
		if (AirUtils.isSSRAddedInTrip(segmentInfos)) {
			isSSRAvailable = true;
			for (FlightTravellerInfo traveller : bookingTravellerInfos) {
				UniversalModifyCmd_type0 universalModifyCmd = new UniversalModifyCmd_type0();
				universalModifyCmd.setKey(getTypeRef(String.valueOf(universalCmdIndex.get())));
				AirAdd_type0 airAdd = new AirAdd_type0();
				airAdd.setReservationLocatorCode(getTypeLocatorCode(reservationPNR));
				int keyIndex = 1;
				for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
					SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
					List<FlightTravellerInfo> travellerInfoList = segmentInfo.getTravellerInfo();
					FlightTravellerInfo flightTravellerInfo = null;
					for (FlightTravellerInfo travellerInfoItem : travellerInfoList) {
						if (TravelPortUtils.isSameTravellerInfo(traveller, travellerInfoItem)) {
							flightTravellerInfo = travellerInfoItem;
							break;
						}
					}
					if (Objects.nonNull(flightTravellerInfo.getSsrMealInfo())
							|| CollectionUtils.isNotEmpty(flightTravellerInfo.getExtraServices())) {
						airAdd.setBookingTravelerRef(getTypeRef(traveller.getMiscInfo().getPassengerKey()));
						if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
							if (Objects.nonNull(flightTravellerInfo.getSsrMealInfo())) {
								SSR_type0 ssr = new SSR_type0();
								ssr.setKey(getTypeRef(String.valueOf(keyIndex)));
								ssr.setSegmentRef(
										getTypeRef(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey()));
								ssr.setType(getTypeSSrCode(flightTravellerInfo.getSsrMealInfo().getCode()));
								if (SSRINFO.containsKey(flightTravellerInfo.getSsrMealInfo().getCode().toUpperCase())) {
									ssr.setFreeText(getTypeSSrFreeText(
											SSRINFO.get(flightTravellerInfo.getSsrMealInfo().getCode().toUpperCase())));
								}
								ssr.setStatus("NN");
								ssr.setCarrier(getTypeCarrierCode(segmentInfo.getAirlineCode(false)));
								airAdd.addSSR(ssr);
								keyIndex++;
							}
							if (CollectionUtils.isNotEmpty(flightTravellerInfo.getExtraServices())) {
								for (SSRInformation travellerSSRInfo : flightTravellerInfo.getExtraServices()) {
									SSR_type0 ssr = new SSR_type0();
									ssr.setKey(getTypeRef(String.valueOf(keyIndex)));
									ssr.setSegmentRef(
											getTypeRef(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey()));
									ssr.setType(getTypeSSrCode(travellerSSRInfo.getCode()));
									if (SSRINFO.containsKey(travellerSSRInfo.getCode().toUpperCase())) {
										ssr.setFreeText(getTypeSSrFreeText(
												SSRINFO.get(travellerSSRInfo.getCode().toUpperCase())));
									}
									ssr.setStatus("NN");
									ssr.setCarrier(getTypeCarrierCode(segmentInfo.getAirlineCode(false)));
									airAdd.addSSR(ssr);
									keyIndex++;
								}
							}
							universalModifyCmd.setAirAdd(airAdd);
						}
					}
				}
				universalRecordModifyReq.addUniversalModifyCmd(universalModifyCmd);
				universalCmdIndex.getAndIncrement();
			}
		}
		boolean ancillariesAvailable = ancillaries(gstInfo, universalRecordModifyReq, universalCmdIndex);
		if (isSSRAvailable || ancillariesAvailable)
			modifyUniversalRecord(universalRecordModifyReq, "ADD SSR");
	}

	public void pnrRetention() {
		PassiveServiceStub passiveService = bindingService.getPassiveService();
		listener.setType(AirUtils.getLogType("PassiveCreateReservation", configuration));
		passiveService._getServiceClient().getAxisService().addMessageContextListener(listener);
		PassiveCreateReservationReq passiveCreateReservationReq = new PassiveCreateReservationReq();
		buildBaseCoreRequest(passiveCreateReservationReq);
		passiveCreateReservationReq.setVersion(version);
		passiveCreateReservationReq.setUniversalRecordLocatorCode(getTypeLocatorCode(universalLocatorCode));
		passiveCreateReservationReq.setProviderLocatorCode(getTypeLocatorCode(csrPnr));
		setOSI(passiveCreateReservationReq);
		if (CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList())
				&& StringUtils.isNotBlank(providerCode)) {
			passiveCreateReservationReq.setProviderCode(providerCode);
		}
		buildBookingTraveler(passiveCreateReservationReq);
		buildPassiveSegment(passiveCreateReservationReq);
		buildPassiveRemark(passiveCreateReservationReq);
		PassiveCreateReservationRsp passiveCreateReservationRsp = null;
		try {
			bindingService.setProxyAndAuthentication(passiveService, "PnrRetention");
			passiveCreateReservationRsp = passiveService.service(passiveCreateReservationReq, getSupportedVersion());
			if (checkAnyErrors(passiveCreateReservationRsp)) {
				throw new SupplierUnHandledFaultException(String.join(",", criticalMessageLogger));
			}
		} catch (PassiveFaultMessage | RemoteException passiveFaultMessage) {
			logCriticalMessage(passiveFaultMessage.getMessage());
			throw new SupplierUnHandledFaultException(passiveFaultMessage.getMessage());
		} finally {
			passiveService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

	}

	private void setOSI(PassiveCreateReservationReq passiveCreateReservationReq) {
		com.tgs.services.base.datamodel.DeliveryInfo orderDeliveryInfo = deliveryInfo;

		OSI_type0 ctcm = new OSI_type0();
		ctcm.setCarrier(getTypeCarrierCode(segmentInfos.get(0).getPriceInfo(0).getPlattingCarrier()));
		String mobile = AirSupplierUtils.getContactNumberWithCountryCode(orderDeliveryInfo);
		mobile = mobile.replace("+", "");
		ctcm.setText(getText_Type1(getOSIText("CTCM", mobile)));
		passiveCreateReservationReq.addOSI(ctcm);

		OSI_type0 ctce = new OSI_type0();
		String email = AirSupplierUtils.getEmailId(orderDeliveryInfo);
		ctce.setCarrier(getTypeCarrierCode(segmentInfos.get(0).getPriceInfo(0).getPlattingCarrier()));
		ctce.setText(
				getText_Type1(getOSIText("CTCE", StringUtils.join(email.split("@")[0], "//", email.split("@")[1]))));
		passiveCreateReservationReq.addOSI(ctce);

	}


	private String getOSIText(String code, String freeText) {
		return StringUtils.join(code, " ", freeText);
	}

	private void buildPassiveRemark(PassiveCreateReservationReq passiveCreateReservationReq) {
		PassiveRemark_type0 passiveRemark = new PassiveRemark_type0();
		passiveRemark.setPassiveSegmentRef(passiveCreateReservationReq.getPassiveSegment()[0].getKey());
		LocalDateTime lastSegmentFlightTime = segmentInfos.get(segmentInfos.size() - 1).getArrivalTime();
		String retainTill = DateFormatterHelper.format(lastSegmentFlightTime.plusDays(180), "ddMMMYYYY");
		String passiveText = StringUtils.join("KEEP THE PNR LIVE TILL ", retainTill);
		BasePassiveRemarkGroup group = new BasePassiveRemarkGroup();
		group.setText(passiveText);
		group.setType(PASSIVE_REMARK_TYPE);
		passiveRemark.setBasePassiveRemarkGroup(group);
		passiveCreateReservationReq.addPassiveRemark(passiveRemark);
	}

	private void buildPassiveSegment(PassiveCreateReservationReq passiveCreateReservationReq) {
		PassiveSegment_type0 passiveSegment = new PassiveSegment_type0();
		int numberOfPassengers = segmentInfos.get(0).getTravellerInfo().size();
		passiveSegment.setOrigin(getIATACode(segmentInfos.get(0).getDepartureAirportCode()));
		passiveSegment.setSegmentType(SEGMENT_TYPE);
		LocalDateTime lastSegmentFlightTime = segmentInfos.get(segmentInfos.size() - 1).getArrivalTime().plusDays(180);
		passiveSegment.setStartDate(lastSegmentFlightTime.toLocalDate().toString());
		passiveSegment.setStatus("AK");
		passiveSegment.setNumberOfItems(new BigInteger(numberOfPassengers + ""));
		passiveSegment.setKey(getTypeRef(TravelPortUtils.keyGenerator()));
		passiveCreateReservationReq.addPassiveSegment(passiveSegment);
	}

	private void buildBookingTraveler(PassiveCreateReservationReq passiveCreateReservationReq) {
		for (int passengerIndex = 0; passengerIndex < travellerInfos.size(); passengerIndex++) {
			BaseBookingTravelerInfoA travelerInfoA = new BaseBookingTravelerInfoA();
			FlightTravellerInfo flightTravellerInfo = travellerInfos.get(passengerIndex);
			BookingTraveler_type0 bookingTraveler = new BookingTraveler_type0();
			bookingTraveler.setKey(getTypeRef(String.valueOf(passengerIndex)));
			bookingTraveler.setTravelerType(getTypePTC(flightTravellerInfo.getPaxType().getType()));
			BookingTravelerName_type0 bookingTravelerName = new BookingTravelerName_type0();
			bookingTravelerName.setFirst(getFirst_type2(TravelPortUtils.getFirstName(flightTravellerInfo)));
			bookingTravelerName.setLast(getTravellerLastname(TravelPortUtils.getLastName(flightTravellerInfo)));
			bookingTravelerName.setPrefix(getPrefix(TravelPortUtils.getPaxPrefix(flightTravellerInfo)));
			travelerInfoA.setBookingTravelerName(bookingTravelerName);
			bookingTraveler.setBaseBookingTravelerInfoA(travelerInfoA);
			// this just instance required for axis2 to avois OMException
			bookingTraveler.setBaseBookingTravelerInfoB(new BaseBookingTravelerInfoB());
			passiveCreateReservationReq.addBookingTraveler(bookingTraveler);
		}
	}

	private String getNetRemittanceCode() {
		Set<String> flightNumbers = new HashSet<>();
		Set<String> bookingClasses = new HashSet<>();
		Set<String> fareBasisSet = new HashSet<>();
		List<RouteInfo> routeInfos = new ArrayList<>();
		for (SegmentInfo segment : segmentInfos) {
			flightNumbers.add(segment.getFlightNumber());
			fareBasisSet.add(segment.getPriceInfo(0).getFareBasis(PaxType.ADULT));
			bookingClasses.add(segment.getPriceInfo(0).getBookingClass(PaxType.ADULT));
			RouteInfo route = RouteInfo.builder().fromCityOrAirport(segment.getDepartAirportInfo())
					.toCityOrAirport(segment.getArrivalAirportInfo()).travelDate(segment.getDepartTime().toLocalDate())
					.build();
			routeInfos.add(route);
		}
		TimePeriod travelPeriod = new TimePeriod();
		travelPeriod.setStartTime(segmentInfos.get(0).getDepartTime());
		travelPeriod.setEndTime(segmentInfos.get(segmentInfos.size() - 1).getArrivalTime());
		TripInfo trip = new TripInfo();
		trip.setSegmentInfos(segmentInfos);
		AirType airType = AirUtils.getAirType(trip);
		String airline = segmentInfos.get(0).getPlatingCarrier(null);
		FlightBasicFact flightFact = FlightBasicFact.builder().flightNumbers(flightNumbers).travelPeriod(travelPeriod)
				.bookingClasses(bookingClasses).airType(airType).airline(airline).fareBasisSet(fareBasisSet)
				.routeInfos(routeInfos).build();
		return AirSupplierUtils.getAirlinePromotionCode(flightFact);
	}

}
