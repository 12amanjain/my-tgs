package com.tgs.services.fms.sources.travelport;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.sources.AbstractAirCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import org.springframework.stereotype.Service;

@Service
public class TravelPortCancellationFactory extends AbstractAirCancellationFactory {

	private TravelPortBindingService bindingService;
	private TravelPortCancellationManager cancellationManager;
	protected SoapRequestResponseListner listener;

	public TravelPortCancellationFactory(BookingSegments bookingSegments, Order order,
			SupplierConfiguration supplierConf) {
		super(bookingSegments, order, supplierConf);
	}

	public void initialize() {
		listener = new SoapRequestResponseListner(bookingId, null, supplierConf.getBasicInfo().getSupplierName());
		bindingService = TravelPortBindingService.builder().configuration(supplierConf).build();
		cancellationManager = TravelPortCancellationManager.builder().bookingId(bookingId).configuration(supplierConf)
				.order(order).pnr(pnr).listener(listener).segmentInfos(bookingSegments.getSegmentInfos())
				.sourceConfiguration(sourceConfiguration).bindingService(bindingService).bookingUser(bookingUser)
				.build();
	}


	@Override
	public boolean releaseHoldPNR() {
		boolean isAbortSuccess = false;
		try {
			initialize();
			cancellationManager.init();
			boolean isSegmentsActive = cancellationManager.isSegmentsActive(bookingSegments.getSupplierBookingId());
			if (isSegmentsActive) {
				isAbortSuccess = cancellationManager.cancelPNR(bookingSegments.getSupplierBookingId());
			}
		} finally {
			cancellationManager.resetKeys();
		}
		return isAbortSuccess;
	}
}
