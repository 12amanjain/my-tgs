package com.tgs.services.fms.sources.travelport;

import static com.tgs.services.fms.sources.travelport.TravelPortConstants.FARERULE_CATEGORY;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.fms.utils.AirUtils;
import com.travelport.www.schema.air_v47_0.*;
import com.travelport.www.schema.common_v47_0.BaseReqChoice_type0;
import org.apache.axiom.om.OMAbstractFactory;
import org.apache.axiom.om.OMFactory;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.utils.exception.air.FareRuleUnHandledFaultException;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class TravelPortFareRuleManager extends TravelPortServiceManager {

	public TripFareRule getFareRule(TripInfo selectedTrip, String bookingId) {
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("AirFareRules",configuration));
		AirFareRulesReq airFareRulesReq = null;
		AirFareRulesRsp airFareRulesRsp = null;
		try {
			airService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(airService, "TripFareRule");
			airFareRulesReq = buildAirFareRulesReq(selectedTrip);
			airFareRulesRsp = airService.service(airFareRulesReq, getSessionContext(false));
			if (!checkAnyErrors(airFareRulesRsp)) {
				return parseFareRuleRsp(airFareRulesRsp, selectedTrip);
			}
		} catch (AirFaultMessage | RemoteException e) {
			throw new FareRuleUnHandledFaultException(e.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return null;
	}

	private TripFareRule parseFareRuleRsp(AirFareRulesRsp airFareRulesRsp, TripInfo selectedTrip) {
		TripFareRule tripfareRule = TripFareRule.builder().build();
		FareRuleInformation information = new FareRuleInformation();
		Map<String, FareRule_type0> fareRuleMap = new HashMap<>();
		if (CollectionUtils.isNotEmpty(getList(airFareRulesRsp.getFareRule()))) {
			getList(airFareRulesRsp.getFareRule()).forEach(fareRule -> {
				fareRuleMap.put(fareRule.getFareInfoRef(), fareRule);
			});
			FareRule_type0 fareRule = null;
			for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
				if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
					fareRule = fareRuleMap.get(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef());
				}
				Map<String, String> rules = new HashMap<String, String>();
				getList(fareRule.getFareRuleLong()).forEach(rule -> {
					if (rules.containsKey(rule.getCategory().toString())) {
						String value = rules.get(rule.getCategory().toString());
						value = value + " " + rule.getString();
						rules.put(FARERULE_CATEGORY.get(rule.getCategory().toString()), value);
					} else {
						rules.put(FARERULE_CATEGORY.get(rule.getCategory().toString()), rule.getString());
					}

				});
				information.setMiscInfo(rules);
				tripfareRule.getFareRule().put(segmentInfo.getSegmentKey(), information);
			}
		}
		return tripfareRule;
	}

	private AirFareRulesReq buildAirFareRulesReq(TripInfo selectedTrip) {
		AirFareRulesReq airFareRulesReq = new AirFareRulesReq();
		buildBaseCoreRequest(airFareRulesReq);
		BaseReqChoice_type0 baseReq = new BaseReqChoice_type0();
		List<AirFareRuleCategory_type0> airFareRuleCategoryList = new ArrayList<AirFareRuleCategory_type0>();
		AirFareRulesModifier_type0 airFareRulesModifier = new AirFareRulesModifier_type0();
		List<FareRuleKey_type0> fareRuleKey_type0s = new ArrayList<>();
		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {

				FareRuleKey_type0 ruleKeyType0 = new FareRuleKey_type0();
				ruleKeyType0.setFareInfoRef(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef());
				ruleKeyType0.setProviderCode(
						getTypeProviderCode(segmentInfo.getPriceInfo(0).getMiscInfo().getProviderCode()));
				ruleKeyType0.setTypeNonBlanks(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoValue());
				fareRuleKey_type0s.add(ruleKeyType0);

				AirFareRuleCategory_type0 airFareRuleCategory = new AirFareRuleCategory_type0();
				airFareRuleCategory
						.setFareInfoRef(getTypeRef(segmentInfo.getPriceInfo(0).getMiscInfo().getFareRuleInfoRef()));
				airFareRuleCategory.addCategoryCode(TypeFareRuleCategoryCode.CHG);
				airFareRuleCategoryList.add(airFareRuleCategory);
				airFareRulesModifier.addAirFareRuleCategory(airFareRuleCategory);

			}
		}

		baseReq.setFareRuleKey(fareRuleKey_type0s.toArray(new FareRuleKey_type0[0]));
		airFareRulesReq.setBaseReqChoice_type0(baseReq);
		airFareRulesReq.setAirFareRulesModifier(airFareRulesModifier);
		return airFareRulesReq;
	}
}
