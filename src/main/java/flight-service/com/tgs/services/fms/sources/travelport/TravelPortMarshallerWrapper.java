package com.tgs.services.fms.sources.travelport;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.tempuri.GetPenalties;
import org.tempuri.GetPenaltiesResponse;
import lombok.extern.slf4j.Slf4j;
import minirule.response.ArrayOfFareRule;

@Slf4j
public final class TravelPortMarshallerWrapper {

	static Jaxb2Marshaller marshaller;

	public static void init(Jaxb2Marshaller jaxb2) {
		Jaxb2Marshaller jaxb2Marshaller = new Jaxb2Marshaller();
		jaxb2Marshaller.setClassesToBeBound(minirule.penalities.GetMiniRules.class, GetPenalties.class,
				ArrayOfFareRule.class, GetPenaltiesResponse.class);
		jaxb2Marshaller.setMarshallerProperties(new HashMap<String, Object>() {
			{
				put(javax.xml.bind.Marshaller.JAXB_FORMATTED_OUTPUT, true);
			}
		});
		marshaller = jaxb2Marshaller;
	}

	public static <T> String marshallXml(final T obj) {
		try {
			StringWriter sw = new StringWriter();
			Result result = new StreamResult(sw);
			marshaller.marshal(obj, result);
			return sw.toString();
		} catch (Exception jaxbException) {
			log.error("Unable to marshall xml to string {}", obj, jaxbException);
		}
		return null;
	}

	/**
	 * (tries to) unmarshall(s) an InputStream to the desired object.
	 */
	public static GetPenaltiesResponse unmarshallXml(String xml) throws JAXBException {
		xml = xml.replace("<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\"><s:Body>", "");
		xml = xml.replace("</s:Body></s:Envelope>", "");
		xml = xml.replace("<GetPenaltiesResponse xmlns=\"http://tempuri.org/\"><GetPenaltiesResult>",
				"<GetPenaltiesResponse xmlns=\"http://tempuri.org/\"><GetPenaltiesResult><![CDATA[");
		xml = xml.replace("</GetPenaltiesResult></GetPenaltiesResponse>",
				"]]></GetPenaltiesResult> </GetPenaltiesResponse>");
		InputStream targetStream = new ByteArrayInputStream(xml.getBytes());
		return (GetPenaltiesResponse) marshaller.unmarshal(new StreamSource(targetStream));
		// return null;
	}

	/**
	 * (tries to) unmarshall(s) an InputStream to the desired object.
	 */
	public static ArrayOfFareRule unmarshallFareRule(String xml) throws JAXBException {
		InputStream targetStream = new ByteArrayInputStream(xml.getBytes());
		return (ArrayOfFareRule) marshaller.unmarshal(new StreamSource(targetStream));
	}

	/**
	 * (tries to) unmarshall(s) an InputStream to the desired object.
	 */
	public static void unmarshallXml2(final String xml, @SuppressWarnings("rawtypes") Class tClassOf)
			throws JAXBException {
		InputStream targetStream = new ByteArrayInputStream(xml.getBytes());
		JAXBContext jaxbContext = JAXBContext.newInstance(tClassOf.getClass());
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		jaxbUnmarshaller.unmarshal(targetStream);
	}
}
