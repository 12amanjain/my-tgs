package com.tgs.services.fms.sources.travelport;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import com.tgs.services.base.SpringContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.tempuri.GetPenalties;
import org.tempuri.GetPenaltiesResponse;
import org.tempuri.IMiniService;
import org.tempuri.Service1;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.farerule.FareRuleInformation;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyContent;
import com.tgs.services.fms.datamodel.farerule.FareRulePolicyType;
import com.tgs.services.fms.datamodel.farerule.FareRuleTimeWindow;
import com.tgs.services.fms.datamodel.farerule.TripFareRule;
import com.tgs.services.fms.manager.FareRuleManager;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import minirule.penalities.GetMiniRules;
import minirule.penalities.ObjectFactory;
import minirule.response.ArrayOfFareRule;


@Slf4j
@SuperBuilder
public class TravelPortMiniRuleManager extends TravelPortServiceManager {

	static IMiniService iMiniService;

	static Service1 service1;

	static String END_POINT = "https://minirules.itq.in/MiniService.svc/Service1.svc";

	static String GET_PENALITIES = "http://tempuri.org/IMiniService/GetPenalties";

	static ObjectFactory factory = new ObjectFactory();

	static org.tempuri.ObjectFactory factoryUri = new org.tempuri.ObjectFactory();

	static String MR_CANCELLATION = "CANCELLATION";

	static String MR_DATA_CHANGE = "CHANGE";

	// static List<String> ALLOWED_TYPES = Arrays.asList("AFTER DEPARTURE", "BEFORE DEPARTURE", "NO-SHOW");

	static List<String> ALLOWED_TYPES = Arrays.asList("AFTER DEPARTURE", "BEFORE DEPARTURE");

	protected RestAPIListener listener;

	public TripFareRule getFareRule(TripInfo selectedTrip, String bookingId) {
		ArrayOfFareRule penaltiesResponse = null;
		listener = new RestAPIListener(bookingId);
		String penaltiesRq = null;
		String response = null;
		try {
			if (service1 == null) {
				service1 = new Service1();
			}
			iMiniService = service1.getBasicHttpBindingIMiniService();
			bindInterceptors(iMiniService, "MiniRule", bookingId, END_POINT);
			GetMiniRules miniRules = getMiniRuleRequest(selectedTrip);
			String request = TravelPortMarshallerWrapper.marshallXml(miniRules);
			GetPenalties getPenalties = factoryUri.createGetPenalties();
			request = processRequest(request, "<![CDATA[", "]]>");
			getPenalties.setXmlFile(factoryUri.createGetPenaltiesXmlFile(request));
			penaltiesRq = TravelPortMarshallerWrapper.marshallXml(getPenalties);
			penaltiesRq = processRequest(penaltiesRq, TravelPortXMLConstants.MINI_RULE_PREFIX,
					TravelPortXMLConstants.MINI_RULE_SUFFIX);
			response = getResponseByRequest(penaltiesRq, "GetPenalties", GET_PENALITIES, END_POINT);
			GetPenaltiesResponse response1 = TravelPortMarshallerWrapper.unmarshallXml(response);
			penaltiesResponse =
					TravelPortMarshallerWrapper.unmarshallFareRule(response1.getGetPenaltiesResult().getValue());
			return parseFareRules(penaltiesResponse, selectedTrip);
		} catch (Exception e) {
			log.error("Error Occured on fetching fare rule {}", bookingId, e);
		} finally {
			listener.addLog(LogData.builder().key(bookingId).logData(penaltiesRq).type("FareRuleReq-").build());
			listener.addLog(LogData.builder().key(bookingId).logData(response).type("FareRuleRes-").build());
		}
		return null;
	}

	private TripFareRule parseFareRules(ArrayOfFareRule penaltiesResponse, TripInfo selectedTrip) {
		if (penaltiesResponse != null) {
			List<ArrayOfFareRule.FareRule> cancellationRule = penaltiesResponse.getFareRule().stream()
					.filter(rule -> rule.getRule().equalsIgnoreCase(MR_CANCELLATION)).collect(Collectors.toList());
			List<ArrayOfFareRule.FareRule> datChangeRule = penaltiesResponse.getFareRule().stream()
					.filter(rule -> rule.getRule().equalsIgnoreCase(MR_DATA_CHANGE)).collect(Collectors.toList());
			return addRuleToTripFareRule(selectedTrip, cancellationRule, datChangeRule);
		}
		return null;
	}

	private TripFareRule addRuleToTripFareRule(TripInfo selectedTrip, List<ArrayOfFareRule.FareRule> cancellationRule,
			List<ArrayOfFareRule.FareRule> dateChangeRule) {
		TripFareRule fareRule = TripFareRule.builder().build();
		FareRuleInformation information = new FareRuleInformation();
		addPolicyRule(selectedTrip, information, cancellationRule, FareRulePolicyType.CANCELLATION);
		addPolicyRule(selectedTrip, information, dateChangeRule, FareRulePolicyType.DATECHANGE);
		fareRule.getFareRule().put(FareRuleManager.getRuleKey(selectedTrip), information);
		return fareRule;
	}

	private void addPolicyRule(TripInfo selectedTrip, FareRuleInformation information,
			List<ArrayOfFareRule.FareRule> rules, FareRulePolicyType policyType) {
		Map<FareRuleTimeWindow, FareRulePolicyContent> ruleContent = new HashMap<>();
		Map<String, ArrayList<ArrayOfFareRule.FareRule>> fareRuleMap = new TreeMap<>();
		for (ArrayOfFareRule.FareRule policy : rules) {
			if (policy.getPaxType().equals("CHD")) {
				break;
			}
			if (ALLOWED_TYPES.contains(policy.getDepartureType())) {
				ArrayList<ArrayOfFareRule.FareRule> list = null;
				if (fareRuleMap.containsKey(policy.getDepartureType())) {
					list = fareRuleMap.get(policy.getDepartureType());
					list.add(policy);
				} else {
					list = new ArrayList<>();
					list.add(policy);
				}
				fareRuleMap.put(policy.getDepartureType(), list);
			}
		}
		for (Map.Entry<String, ArrayList<ArrayOfFareRule.FareRule>> entry : fareRuleMap.entrySet()) {
			FareRuleTimeWindow timeWindow = TravelPortUtils.getTimeWindow(entry.getKey());
			String ruleType = entry.getKey();
			ArrayList<ArrayOfFareRule.FareRule> policylist = entry.getValue();
			FareRulePolicyContent policyContent = new FareRulePolicyContent();
			Double amount = getMaxAmount(policylist, selectedTrip);
			if (amount > 0) {
				policyContent.setAmount(amount);
				policyContent.setPolicyInfo(ruleType + " Available");
			} else {
				if (ruleContent.get(timeWindow) == null)
					policyContent.setPolicyInfo(policylist.get(0).getRule() + " Not Available");
			}
			ruleContent.put(timeWindow, policyContent);
		}
		information.getFareRuleInfo().put(policyType, ruleContent);
	}

	private Double getMaxAmount(ArrayList<ArrayOfFareRule.FareRule> policylist, TripInfo selectedTrip) {
		Double amount = 0.0;
		for (ArrayOfFareRule.FareRule policy : policylist) {
			if (policy.getFeeAmount().matches("NR")) {
				return 0.0;
			}
			if (!policy.getFeeAmount().isEmpty() && policy.getFeeAmount().matches("[0-9.]*")
					&& amount < Double.valueOf(policy.getFeeAmount())) {
				double baseFare = 0.0;
				for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
					if (segmentInfo.getSegmentNum() == 0 && segmentInfo.getPriceInfo(0).getFareBasis(PaxType.ADULT)
							.equalsIgnoreCase(policy.getFareBasis())) {
						for (PaxType paxType : PaxType.values()) {
							if (segmentInfo.getPriceInfo(0).getFareDetails().get(paxType) != null) {
								baseFare = segmentInfo.getPriceInfo(0).getFareDetails().get(paxType).getFareComponents()
										.getOrDefault(FareComponent.BF, 0.0);
							}
						}
					}
				}
				if (policy.getFeeCurrency().equalsIgnoreCase("PERCENT")) {
					amount += (Double.valueOf(policy.getFeeAmount()) / 100) * baseFare;
				} else {
					amount += Double.valueOf(policy.getFeeAmount());
				}
			}
		}
		return amount;
	}

	private String processRequest(String request, String begin, String end) {
		request = removeXMLEncode(request);
		request = request.replaceAll("&lt;", "<").replaceAll("&gt;", ">");
		return StringUtils.join(begin, request, end);
	}

	private static String removeXMLEncode(String content) {
		content = content.replace("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n", "");
		return content;
	}

	public GetMiniRules getMiniRuleRequest(TripInfo selectedTrip) {
		GetMiniRules miniRules = factory.createGetMiniRules();
		miniRules.getContent().add(factory.createGetMiniRulesPaxType(getPaxType(selectedTrip)));
		for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
			if (StringUtils.isNotBlank(segmentInfo.getPriceInfo(0).getMiscInfo().getAccountCode())) {
				miniRules.getContent().add(factory
						.createGetMiniRulesAccountCode(segmentInfo.getPriceInfo(0).getMiscInfo().getAccountCode()));
				break;
			}
		}
		miniRules.getContent().add(factory.createGetMiniRulesPCC(configuration.getSupplierCredential().getPcc()));
		miniRules.getContent().add(
				factory.createGetMiniRulesTargetBranch(configuration.getSupplierCredential().getOrganisationCode()));
		// miniRules.getContent().add(factory.createGetMiniRulesFareBasis(buildFareBasis(selectedTrip.getSegmentInfos());
		buildFareBasis(selectedTrip.getSegmentInfos(), miniRules);
		return miniRules;
	}

	private void buildFareBasis(List<SegmentInfo> segmentInfoList, GetMiniRules miniRules) {
		GetMiniRules.FareBasis rqfareBasis = null;
		for (int segmentIndex = 0; segmentIndex < segmentInfoList.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfoList.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = (segmentIndex + 1 < segmentInfoList.size())
					? segmentInfoList.get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			Integer nextSegmentNumber =
					(segmentIndex + 1 < segmentInfoList.size()) ? segmentInfoList.get(segmentIndex + 1).getSegmentNum()
							: 0;
			if (legNum == 0 && segmentInfo.getSegmentNum() == 0) {
				rqfareBasis = factory.createGetMiniRulesFareBasis();
				PriceInfo tempPrice = segmentInfo.getPriceInfo(0);
				String fareBasisPrice = tempPrice.getFareBasis(PaxType.ADULT);
				rqfareBasis.setFBC(fareBasisPrice);
				rqfareBasis.getContent()
						.add(factory.createGetMiniRulesFareBasisOrigin(segmentInfo.getDepartureAirportCode()));
				rqfareBasis.getContent().add(factory.createGetMiniRulesFareBasisAirlines(getAirlines(segmentInfo)));
				rqfareBasis.getContent().add(factory.createGetMiniRulesFareBasisDepartureDate(getDate(segmentInfo)));
			}
			if (nextSegmentLegNum == 0 && nextSegmentNumber == 0) {
				rqfareBasis.getContent()
						.add(factory.createGetMiniRulesFareBasisDestination(segmentInfo.getArrivalAirportCode()));
				miniRules.getContent().add(factory.createGetMiniRulesFareBasis(rqfareBasis));
			}
		}
	}

	private String getDate(SegmentInfo segmentInfo) {
		LocalDate localDate = segmentInfo.getDepartTime().toLocalDate();
		return DateFormatterHelper.format(localDate, "ddMMMyy");
	}

	private GetMiniRules.FareBasis.Airlines getAirlines(SegmentInfo segmentInfo) {
		GetMiniRules.FareBasis.Airlines airlines = new GetMiniRules.FareBasis.Airlines();
		airlines.getAirline().add(segmentInfo.getAirlineCode(false));
		return airlines;
	}


	private GetMiniRules.PaxType getPaxType(TripInfo selectedTrip) {
		GetMiniRules.PaxType pax = factory.createGetMiniRulesPaxType();
		selectedTrip.getPaxInfo().forEach((paxType, count) -> {
			if (count > 0) {
				pax.getPTC().add(paxType.getType());
			}
		});
		return pax;
	}

	public List<String> getUniqueFareBasis(List<SegmentInfo> segmentInfos) {
		List<String> tripFareBasis = new ArrayList<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getBookingRelatedInfo() != null) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				if (CollectionUtils.isNotEmpty(travellerInfos)) {
					travellerInfos.forEach(travellerInfo -> {
						if (!tripFareBasis.contains(travellerInfo.getFareDetail().getFareBasis())) {
							tripFareBasis.add(travellerInfo.getFareDetail().getFareBasis());
						}
					});
				}
			} else if (segmentInfo.getPriceInfo(0) != null) {
				PriceInfo tempPrice = segmentInfo.getPriceInfo(0);
				if (MapUtils.isNotEmpty(tempPrice.getFareDetails())) {
					tempPrice.getFareDetails().forEach(((paxType, fareDetail) -> {
						if (!tripFareBasis.contains(fareDetail.getFareBasis())) {
							tripFareBasis.add(fareDetail.getFareBasis());
						}
					}));
				}
			}
		});
		return tripFareBasis;
	}

}
