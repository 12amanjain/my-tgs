package com.tgs.services.fms.sources.travelport;

import com.tgs.services.base.enums.PaxType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.Arrays;

@AllArgsConstructor
@Getter
public enum TravelPortPaxType {
	ADULT(PaxType.ADULT, "ADT"), CHILD(PaxType.CHILD, "CNN"), INFANT(PaxType.INFANT, "INF");

	private PaxType paxType;
	private String paxCode;

	public static PaxType getPaxType(String paxcode) {
		return Arrays.stream(TravelPortPaxType.values()).filter(rdPaxType -> {
			return rdPaxType.getPaxCode().equals(paxcode);
		}).findFirst().get().getPaxType();
	}
}
