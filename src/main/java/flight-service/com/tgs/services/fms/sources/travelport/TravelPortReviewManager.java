package com.tgs.services.fms.sources.travelport;


import com.tgs.services.fms.utils.AirUtils;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.*;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.travelport.www.schema.air_v47_0.*;
import com.travelport.www.schema.common_v47_0.SearchPassenger;
import com.travelport.www.schema.common_v47_0.SearchPassenger_type0;
import com.travelport.www.schema.sharedbooking_v47_0.AddAirSegment_type0;
import com.travelport.www.schema.sharedbooking_v47_0.BookingAirSegmentReq;
import com.travelport.www.schema.sharedbooking_v47_0.BookingAirSegmentRsp;
import com.travelport.www.schema.sharedbooking_v47_0.BookingBaseReqChoice_type4;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import com.travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import com.travelport.www.service.sharedbooking_v47_0.SystemFaultMessage;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.AIR_SERVICE;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.SHARED_BOOKING_SERVICE;


@Slf4j
@Getter
@Setter
@SuperBuilder
final class TravelPortReviewManager extends TravelPortServiceManager {

	private List<TripInfo> tripInfoList;
	private List<TypeBaseAirSegment> airSegment;

	public TripInfo reviewFlight(TripInfo selectedTrip, boolean isAirPricingCommandRequired) {
		bookingId = bookingId != null ? bookingId : searchId;
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("Air Price", configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		TripInfo reviewedTrip = null;
		tripInfoList = selectedTrip.splitTripInfo(false);
		AirPriceReq airPriceReq = buildAirPriceRequest(selectedTrip, isAirPricingCommandRequired);
		AirPriceRsp airPriceRsp = null;
		try {
			bindingService.setProxyAndAuthentication(airService, "AirPrice");
			airPriceRsp = airService.service(airPriceReq, getSessionContext(false));
			airSegment = Arrays.asList(airPriceRsp.getAirItinerary().getAirSegment());
			if (!checkAnyErrors(airPriceRsp)) {
				reviewedTrip = parseAirPriceResponse(airPriceRsp, selectedTrip);
			} else {
				throw new NoSeatAvailableException(String.join(",", criticalMessageLogger));
			}
		} catch (AirFaultMessage | RemoteException airFaultMessage) {
			throw new NoSeatAvailableException(airFaultMessage.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return reviewedTrip;
	}

	private TripInfo parseAirPriceResponse(AirPriceRsp airPriceRsp, TripInfo selectedTrip) {
		boolean isDomReturn = searchQuery.isDomesticReturn() && AirUtils.splitTripInfo(selectedTrip, false).size() == 2;
		TripInfo reviewedTrip = null;
		reviewedTrip = new GsonMapper<>(selectedTrip, TripInfo.class).convert();
		AirPricingSolution_type0 airPricingSolution = getAppropriateAirPricingSolution(airPriceRsp, selectedTrip);
		boolean isFareAvailable = checkFareAvailable(airPricingSolution, reviewedTrip);
		if (!isFareAvailable && isDomReturn) {
			log.info("Fare Difference on booking . search again and sell {} and isdomReturn {}", bookingId,
					isDomReturn);
			reviewedTrip = null;
		} else {
			updateTripInfo(airPricingSolution, reviewedTrip);
		}
		return reviewedTrip;
	}

	private void updateTripInfo(AirPricingSolution_type0 airPricingSolution, TripInfo reviewedTrip) {
		SegmentInfo firstSegment = new GsonMapper<>(reviewedTrip.getSegmentInfos().get(0), SegmentInfo.class).convert();
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(configuration.getBasicInfo());
		priceInfo.setMiscInfo(firstSegment.getPriceInfoList().get(0).getMiscInfo());
		resetSegmentPriceFares(reviewedTrip);
		String segmentRefKey = null;
		for (int segmentNumber = 0; segmentNumber < reviewedTrip.getSegmentInfos().size(); segmentNumber++) {
			SegmentInfo segment = reviewedTrip.getSegmentInfos().get(segmentNumber);
			PriceInfo priceInfo1 = getPriceInfo(segment, priceInfo);
			if (segment.getPriceInfoList().get(0).getMiscInfo().getLegNum() == 0) {
				segmentRefKey = getAndUpdateSegmentKey(segment, airSegment, priceInfo1);
			}
			Map<PaxType, FareDetail> fareDetails = new HashMap<>();
			HashMap<PaxType, List<String>> paxPricingInfoKeyMap = new LinkedHashMap<>();
			for (AirPricingInfo_type0 airPricingInfo : airPricingSolution.getAirPricingInfo()) {
				String key = airPricingInfo.getKey().getTypeRef();
				PassengerType_type0 passengerType = Arrays.asList(airPricingInfo.getPassengerType()).get(0);
				PaxType paxType = TravelPortPaxType.getPaxType(passengerType.getCode().getTypePTC());
				List<String> paxKeyList = null;
				if (paxPricingInfoKeyMap.containsKey(paxType)) {
					paxKeyList = paxPricingInfoKeyMap.get(paxType);
				} else {
					paxKeyList = new ArrayList<>();
				}
				paxKeyList.add(key);
				paxPricingInfoKeyMap.put(paxType, paxKeyList);
				List<BookingInfo_type0> bookingInfoList = Arrays.asList(airPricingInfo.getBookingInfo());
				List<FareInfo_type0> fareInfoList = Arrays.asList(airPricingInfo.getFareInfo());
				BookingInfo_type0 bookingInfo = getBookingInfoFromSegment(segmentRefKey, bookingInfoList);
				FareInfo_type0 fareInfo =
						getFareInfoFromBookingInfo(fareInfoList, bookingInfo.getFareInfoRef().getTypeRef());
				setFareIdentifier(fareInfo, priceInfo1);
				setFareRuleKeys(priceInfo1.getMiscInfo(), fareInfo);
				if (Objects.nonNull(bookingInfo)) {
					FareDetail fareDetail =
							getFareDetail(airPricingInfo, bookingInfo, segment, segmentNumber, fareInfo);
					fareDetails.put(paxType, fareDetail);
				}
			}
			PriceMiscInfo miscInfo = priceInfo1.getMiscInfo();
			miscInfo.setPaxPricingInfo(paxPricingInfoKeyMap);
			priceInfo1.setFareDetails(fareDetails);
			segment.setPriceInfoList(Arrays.asList(priceInfo1));
		}
	}

	public PriceInfo getPriceInfo(SegmentInfo segmentInfo, PriceInfo defaultPriceInfo) {
		if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
			return segmentInfo.getPriceInfo(0);
		}
		return new GsonMapper<>(defaultPriceInfo, PriceInfo.class).convert();
	}

	private FareInfo_type0 getFareInfo(List<FareInfo_type0> fareInfoList, TripInfo tripInfo) {
		for (FareInfo_type0 fareInfo : fareInfoList) {
			if (tripInfo.getDepartureAirportCode().equals(fareInfo.getOrigin().getTypeIATACode())
					&& tripInfo.getArrivalAirportCode().equals(fareInfo.getDestination().getTypeIATACode())) {
				return fareInfo;
			}
		}
		return null;
	}

	private FareDetail getFareDetail(AirPricingInfo_type0 airPricingInfo, BookingInfo_type0 bookingInfo,
			SegmentInfo segment, int segmentNumber, FareInfo_type0 fareInfo) {
		double dividingFactor = 1.0;
		String classOfBook = bookingInfo.getBookingCode();
		FareDetail fare = new FareDetail();
		fare.setCabinClass(TravelPortUtils.getCabinClass(bookingInfo));
		fare.setClassOfBooking(classOfBook);
		fare.setFareBasis(fareInfo.getFareBasis());
		fare.setRefundableType(TravelPortUtils.getRefundableType(airPricingInfo));
		if (segmentNumber == 0) {
			fare.getFareComponents().put(FareComponent.BF, getEquivalentBaseFare(airPricingInfo, dividingFactor));
			fare.getFareComponents().put(FareComponent.TF, getEquivalentTotalfare(airPricingInfo, dividingFactor));
			if (Objects.nonNull(airPricingInfo.getTaxInfo())) {
				setTaxDetails(fare, Arrays.asList(airPricingInfo.getTaxInfo()), dividingFactor);
			}
		}
		if (StringUtils.isNotBlank(bookingInfo.getBookingCount())) {
			fare.setSeatRemaining(Integer.valueOf(bookingInfo.getBookingCount()));
		}
		setBaggageAllowance(fare, airPricingInfo, segment);
		if (StringUtils.isNotBlank(bookingInfo.getBookingCount())) {
			fare.setSeatRemaining(Integer.valueOf(bookingInfo.getBookingCount()));
		}
		return fare;
	}

	/*
	 * This logic will fail for nearbyAirport as there general purpose output don't have all the nearby airport
	 * configuration.
	 */
	private void setBaggageAllowance(FareDetail fare, AirPricingInfo_type0 airPricingInfo, SegmentInfo segment) {
		try {
			Map<String, String> baggageMap = new LinkedHashMap<String, String>();
			if (!Objects.isNull(airPricingInfo.getBaggageAllowances())) {
				for (int index =
						0; index < airPricingInfo.getBaggageAllowances().getBaggageAllowanceInfo().length; index++) {
					BaggageAllowanceInfo_type0 baggageAllowanceInfo =
							Arrays.asList(airPricingInfo.getBaggageAllowances().getBaggageAllowanceInfo()).get(index);
					String baggageKey =
							baggageAllowanceInfo.getOrigin() + "_" + baggageAllowanceInfo.getDestination() + index;
					baggageMap.put(baggageKey, getBaggageAllowance(baggageAllowanceInfo));
				}

				String tripInfoKey = "";
				for (int index = 0; index < tripInfoList.size(); index++) {
					TripInfo tripInfo = tripInfoList.get(index);
					for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
						if (segmentInfo.getSegmentKey().equals(segment.getSegmentKey())
								&& segmentInfo.getDepartTime().equals(segment.getDepartTime())) {
							tripInfoKey =
									tripInfo.getDepartureAirportCode() + "_" + tripInfo.getArrivalAirportCode() + index;

						}
					}
					if (StringUtils.isNotEmpty(tripInfoKey)) {
						break;
					}
				}
				BaggageInfo baggageInfo = new BaggageInfo();
				baggageInfo.setAllowance(baggageMap.get(tripInfoKey));
				fare.setBaggageInfo(baggageInfo);
			}
		} catch (Exception e) {
			log.error("Baggage Allowance Failed for bookingId {}", bookingId);
			setDefaultBaggage(airPricingInfo, fare);
		} finally {
			if (fare.getBaggageInfo() == null || StringUtils.isBlank(fare.getBaggageInfo().getAllowance())) {
				setDefaultBaggage(airPricingInfo, fare);
			}
		}
	}

	private void setDefaultBaggage(AirPricingInfo_type0 airPricingInfo, FareDetail fare) {
		try {
			if (!Objects.isNull(airPricingInfo.getBaggageAllowances())) {
				BaggageInfo baggageInfo = new BaggageInfo();
				baggageInfo.setAllowance(getBaggageAllowance(airPricingInfo.getBaggageAllowances()));
				fare.setBaggageInfo(baggageInfo);
			}
		} catch (Exception e) {
			log.error("Baggage Allowance Failed for bookingId {}", bookingId);
		}
	}


	private String getBaggageAllowance(BaggageAllowances_type0 baggageAllowances) {
		if (getList(getList(getList(baggageAllowances.getBaggageAllowanceInfo()).get(0).getTextInfo()).get(0).getText())
				.get(0).getTypeGeneralText().contains("P")) {
			return getList(
					getList(getList(baggageAllowances.getBaggageAllowanceInfo()).get(0).getTextInfo()).get(0).getText())
							.get(0).getTypeGeneralText()
					+ "iece";
		}
		return getList(
				getList(getList(baggageAllowances.getBaggageAllowanceInfo()).get(0).getTextInfo()).get(0).getText())
						.get(0).getTypeGeneralText()
				+ "G";
	}

	private String getBaggageAllowance(BaggageAllowanceInfo_type0 baggageAllowanceInfo) {
		if (getList(getList(baggageAllowanceInfo.getTextInfo()).get(0).getText()).get(0).getTypeGeneralText()
				.contains("P")) {
			return getList(getList(baggageAllowanceInfo.getTextInfo()).get(0).getText()).get(0).getTypeGeneralText()
					+ "iece";
		}
		return getList(getList(baggageAllowanceInfo.getTextInfo()).get(0).getText()).get(0).getTypeGeneralText() + "G";
	}

	public void setSSRInfos(TripInfo tripInfo) {
		if (tripInfo != null) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				FlightBasicFact fact = FlightBasicFact.builder().build().generateFact(segmentInfo);
				BaseUtils.createFactOnUser(fact, bookingUser);
				Map<SSRType, List<? extends SSRInformation>> ssrInfos = AirUtils.getPreSSR(fact);
				if (MapUtils.isNotEmpty(ssrInfos)) {
					segmentInfo.setSsrInfo(ssrInfos);
				}
			}
		}
	}

	private BookingInfo_type0 getBookingInfoFromSegment(List<BookingInfo_type0> bookingInfoList, SegmentInfo segment,
			List<TypeBaseAirSegment> airSegment, FareInfo_type0 fareInfo) {
		String segmentRefKey = null;
		for (TypeBaseAirSegment typeBaseAirSegment : airSegment) {
			if (segment.getDepartureAirportCode().equals(typeBaseAirSegment.getOrigin().getTypeIATACode())
					&& segment.getArrivalAirportCode().equals(typeBaseAirSegment.getDestination().getTypeIATACode())) {
				segmentRefKey = typeBaseAirSegment.getKey().getTypeRef();
				break;
			}
		}

		for (BookingInfo_type0 bookingInfo : bookingInfoList) {
			if (bookingInfo.getFareInfoRef().equals(fareInfo.getKey())
					&& bookingInfo.getSegmentRef().getTypeRef().equals(segmentRefKey)) {
				return bookingInfo;
			}
		}
		return null;
	}

	private void resetSegmentPriceFares(TripInfo selectedTrip) {
		selectedTrip.getSegmentInfos().forEach(segmentInfo -> {
			segmentInfo.getPriceInfoList().get(0).setFareDetails(null);
		});
	}

	private FareType getOldFareType(TripInfo selectedTrip) {
		return FareType.getEnumFromName(selectedTrip.getSegmentInfos().get(0).getPriceInfo(0).getFareType());
	}

	private boolean checkFareAvailable(AirPricingSolution_type0 airPricingSolution, TripInfo reviewedTrip) {
		boolean isFareAvailable = true;
		AtomicDouble newFare = new AtomicDouble(0);
		AtomicDouble tripTotalFare = AirUtils.getAirlineTotalFare(reviewedTrip);
		double totalAirlineFare = getTotalAirlineFare(airPricingSolution);
		newFare.set(totalAirlineFare);
		double differenceFare = Math.abs(newFare.get() - tripTotalFare.get());
		if (newFare.get() >= 0
				&& (newFare.get() == tripTotalFare.get() || (differenceFare >= 0 && differenceFare <= 2))) {
			log.info("Sell fare Available for booking {} trip fare {}", bookingId, newFare.get());
		} else {
			log.info("Supplier Fare {} , Selected trip fare {} for bookingid {} sell found fare difference  ",
					newFare.get(), tripTotalFare.get(), bookingId);
			isFareAvailable = false;
		}
		return isFareAvailable;
	}

	private double getTotalAirlineFare(AirPricingSolution_type0 airPricingSolution) {
		double totalAirlineFare = 0.0;
		for (AirPricingInfo_type0 airPricingInfo : airPricingSolution.getAirPricingInfo()) {
			totalAirlineFare += Double.valueOf(getEquivalentTotalfare(airPricingInfo, 1));
		}
		return totalAirlineFare;
	}

	private AirPricingSolution_type0 getAppropriateAirPricingSolution(AirPriceRsp airPriceRsp, TripInfo selectedTrip) {
		AirPricingSolution_type0 appropriateAirPricingSolution = null;
		Double minimumPrice = Double.valueOf(Integer.MAX_VALUE);
		List<AirPricingSolution_type0> filteredGuratedSolutionList =
				getFilteredPricingSolution(getList(airPriceRsp.getAirPriceResult()), selectedTrip);
		boolean isClassChanged = false;
		// select lowest price RBD in case of chosen RBD is not available.
		for (AirPricingSolution_type0 filteredGuratedSolution : filteredGuratedSolutionList) {
			List<BookingInfo_type0> bookingInfoList =
					getList(getList(filteredGuratedSolution.getAirPricingInfo()).get(0).getBookingInfo());
			for (SegmentInfo segmentInfo : selectedTrip.getSegmentInfos()) {
				if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
					if (StringUtils.isNotEmpty(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey())) {
						String segmentRefKey =
								getAndUpdateSegmentKey(segmentInfo, airSegment, segmentInfo.getPriceInfoList().get(0));
						BookingInfo_type0 bookingInfo = getBookingInfoFromSegment(segmentRefKey, bookingInfoList);
						// FareInfo fareInfo = getFareInfoFromBookingInfo(fareInfoList, bookingInfo.getFareInfoRef());
						FareDetail fareDetail = TravelPortUtils.getFareDetail(segmentInfo.getPriceInfo(0));
						if (!isClassChanged && bookingInfo.getBookingCode().equals(fareDetail.getClassOfBooking())) {
							appropriateAirPricingSolution = filteredGuratedSolution;
						} else {
							Double totalFare = getEquivalentTotalfare(filteredGuratedSolution, 1.0);
							isClassChanged = true;
							log.info("Class Changed from " + fareDetail.getClassOfBooking() + " to "
									+ bookingInfo.getBookingCode() + " bookingId " + bookingId);
							if (totalFare < minimumPrice) {
								appropriateAirPricingSolution = filteredGuratedSolution;
								minimumPrice = totalFare;
							}
							break;
						}
					}
				}
			}
			if (!isClassChanged) {
				break;
			}
		}
		return appropriateAirPricingSolution;
	}

	private List<AirPricingSolution_type0> getFilteredPricingSolution(List<AirPriceResult_type0> airPriceResult,
			TripInfo selectedTrip) {
		List<AirPricingSolution_type0> pricingSolutionSelectedList = new ArrayList<>();
		String accountCode = TravelPortUtils.getAccountCodeFromTrip(selectedTrip.getSegmentInfos());
		boolean isPrivateFare = TravelPortUtils.isPrivateFare(selectedTrip.getSegmentInfos());
		if (isPrivateFare && CollectionUtils.isNotEmpty(airPriceResult)
				&& CollectionUtils.isNotEmpty(Arrays.asList(airPriceResult.get(0).getAirPricingSolution()))) {
			for (AirPricingSolution_type0 pricingSolution : airPriceResult.get(0).getAirPricingSolution()) {
				TypePricingMethod pricingMethod =
						Arrays.asList(pricingSolution.getAirPricingInfo()).get(0).getPricingMethod();
				if (TypePricingMethod._GuaranteedUsingAirlinePrivateFare.equals(pricingMethod.getValue())
						|| TypePricingMethod._GuaranteedUsingAgencyPrivateFare.equals(pricingMethod.getValue())
						|| TypePricingMethod._AutoUsingPrivateFare.equals(pricingMethod.getValue())) {
					pricingSolutionSelectedList.add(pricingSolution);
				}
			}
		} else if (CollectionUtils.isNotEmpty(airPriceResult)
				&& CollectionUtils.isNotEmpty(Arrays.asList(airPriceResult.get(0).getAirPricingSolution()))) {
			for (AirPricingSolution_type0 pricingSolution : airPriceResult.get(0).getAirPricingSolution()) {
				if (Arrays.asList(pricingSolution.getAirPricingInfo()).get(0).getPricingMethod().getValue()
						.equals(TypePricingMethod._Guaranteed)) {
					pricingSolutionSelectedList.add(pricingSolution);
				}
			}
		}
		return pricingSolutionSelectedList;

	}

	private FareInfo_type0 getFareInfoFromBookingInfo(List<FareInfo_type0> fareInfoList, String fareInfoRef) {
		FareInfo_type0 fareInfo = null;
		for (FareInfo_type0 fareInfoItem : fareInfoList) {
			if (fareInfoItem.getKey().getTypeRef().equals(fareInfoRef)) {
				fareInfo = fareInfoItem;
				break;
			}
		}
		return fareInfo;
	}

	private BookingInfo_type0 getBookingInfoFromSegment(String segmentRefKey, List<BookingInfo_type0> bookingInfoList) {
		BookingInfo_type0 bookingInfo = null;
		for (BookingInfo_type0 bookingInfoItem : bookingInfoList) {
			if (bookingInfoItem.getSegmentRef().getTypeRef().equals(segmentRefKey)) {
				bookingInfo = bookingInfoItem;
				break;
			}
		}
		return bookingInfo;
	}

	private String getAndUpdateSegmentKey(SegmentInfo segmentInfo, List<TypeBaseAirSegment> airSegmentList,
			PriceInfo priceInfo) {
		String segmentRefKey = "";
		for (TypeBaseAirSegment typeBaseAirSegment : airSegmentList) {
			if (segmentInfo.getDepartureAirportCode().equals(typeBaseAirSegment.getOrigin().getTypeIATACode())
					&& TravelPortUtils.getStringDateTime(segmentInfo.getDepartTime())
							.equals(TravelPortUtils.getStringDateTime(
									TravelPortUtils.getIsoDateTime(typeBaseAirSegment.getDepartureTime())))) {
				segmentRefKey = typeBaseAirSegment.getKey().getTypeRef();
				priceInfo.getMiscInfo().setSegmentKey(segmentRefKey);
				break;
			}
		}
		return segmentRefKey;
	}


	private AirPriceReq buildAirPriceRequest(TripInfo selectedTrip, boolean isAirPricingCommandReq) {
		AirPriceReq airPriceReq = new AirPriceReq();
		buildBaseCoreRequest(airPriceReq);
		buildAirItinerary(airPriceReq, selectedTrip, isAirPricingCommandReq);
		return airPriceReq;
	}

	private void buildSearchPassengerValues(AirPriceReq airPriceReq) {
		for (Map.Entry<PaxType, Integer> paxInfo : searchQuery.getPaxInfo().entrySet()) {
			int paxCount = paxInfo.getValue();
			int tempChildAge = Integer.valueOf(TravelPortConstants.CHILD_AGE);
			while (paxCount > 0) {
				SearchPassenger_type0 passenger = new SearchPassenger_type0();
				PaxType paxType = paxInfo.getKey();
				passenger.setCode(getTypePTC(paxType.getType()));
				passenger.setBookingTravelerRef(TravelPortUtils.keyGenerator());
				if (PaxType.CHILD.getType().equals(paxType.getType())) {
					passenger.setAge(new BigInteger(String.valueOf(tempChildAge)));
					tempChildAge++;
				}
				if (PaxType.INFANT.getType().equals(paxType.getType())) {
					passenger.setAge(new BigInteger(TravelPortConstants.INFANT_AGE));
					passenger.setPricePTCOnly(Boolean.TRUE);
				}
				airPriceReq.addSearchPassenger(passenger);
				paxCount--;
			}
		}
	}


	private void buildAirItinerary(AirPriceReq airPriceReq, TripInfo selectedTrip, boolean isAirPricingCommandReq) {
		AirItinerary_type0 airItinerary = new AirItinerary_type0();
		List<TypeBaseAirSegment> typeBaseAirSegmentList = new ArrayList<>();
		buildAirSegmentList(typeBaseAirSegmentList, selectedTrip.getSegmentInfos(), false, true);
		airItinerary.setAirSegment(typeBaseAirSegmentList.toArray(new TypeBaseAirSegment[0]));
		airPriceReq.setAirItinerary(airItinerary);
		AirPricingModifiers_type0 airPricingModifiers = getAirPricingModifiers(selectedTrip.getSegmentInfos());
		buildSearchPassengerValues(airPriceReq);
		AirPricingCommand_type0 airPricingCommand = new AirPricingCommand_type0();
		if (isAirPricingCommandReq || (AirFlowType.ALTERNATE_CLASS).equals(searchQuery.getFlowType())) {
			airPricingModifiers.setInventoryRequestType(TypeInventoryRequest.DirectAccess);
			buildAirPricingCommand(selectedTrip.getSegmentInfos(), airPriceReq, false);
		} else {
			airPriceReq.addAirPricingCommand(airPricingCommand);
		}
		airPriceReq.setAirPricingModifiers(airPricingModifiers);
	}


	public boolean sellSegment(TripInfo tripInfo, String sessionKey) {
		boolean isSegmentsAvailable = false;
		SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
		List<TripInfo> trips = AirUtils.splitTripInfo(tripInfo, false);
		BookingAirSegmentReq bookingAirSegmentReq = buildSellSegmentRequest(trips, sessionKey);
		BookingAirSegmentRsp bookingAirSegmentRsp = null;
		try {
			listener.setType(AirUtils.getLogType("SellSegment", configuration));
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(serviceStub, "SellSegment");
			bookingAirSegmentRsp = serviceStub.service(bookingAirSegmentReq, getSessionContext(true));
			if (bookingAirSegmentRsp != null && !checkAnyError(bookingAirSegmentRsp).get()) {
				isSegmentsAvailable = checkAirSegmentsValues(trips, bookingAirSegmentRsp);
			}
		} catch (SystemFaultMessage | RemoteException systemFaultMessage) {
			logCriticalMessage(systemFaultMessage.getMessage());
			throw new SupplierUnHandledFaultException(systemFaultMessage.getMessage());
		} finally {
			if (!isSegmentsAvailable) {
				throw new NoSeatAvailableException("Sell Segment Failed");
			}
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return isSegmentsAvailable;
	}

	private BookingAirSegmentReq buildSellSegmentRequest(List<TripInfo> trips, String sessionKey) {
		BookingAirSegmentReq bookingAirSegmentReq = new BookingAirSegmentReq();
		buildBaseCoreRequest(bookingAirSegmentReq);
		bookingAirSegmentReq.setSessionKey(getTypeSessionKey(sessionKey));
		buildAddAirSegmentRequest(trips, bookingAirSegmentReq);
		return bookingAirSegmentReq;
	}

	private void buildAddAirSegmentRequest(List<TripInfo> trips, BookingAirSegmentReq bookingAirSegmentReq) {
		BookingBaseReqChoice_type4 baseReq = new BookingBaseReqChoice_type4();
		AddAirSegment_type0 addAirSegment = new AddAirSegment_type0();
		List<TypeBaseAirSegment> typeBaseAirSegmentList = new ArrayList<>();
		for (TripInfo tripInfo : trips) {
			buildAirSegmentList(typeBaseAirSegmentList, tripInfo.getSegmentInfos(), true, false);
		}
		addAirSegment.setAirSegment(typeBaseAirSegmentList.toArray(new TypeBaseAirSegment[0]));
		baseReq.setAddAirSegment(addAirSegment);
		bookingAirSegmentReq.setBookingBaseReqChoice_type4(baseReq);
	}

	private AtomicBoolean checkAnyError(BookingAirSegmentRsp bookingAirSegmentRsp) {
		// Handle this for errors in bookingAirSegmentRsp
		AtomicBoolean isError = new AtomicBoolean();
		if (bookingAirSegmentRsp.getAirSegmentSellFailureInfo() != null) {
			getList(bookingAirSegmentRsp.getAirSegmentSellFailureInfo().getAirSegmentError())
					.forEach(airSegmentError -> {
						log.error("Air Segment is not Available", airSegmentError.getErrorMessage());
						logCriticalMessage(airSegmentError.getErrorMessage());
						isError.set(true);
					});
		}
		if (!isError.get() && checkAnyErrors(bookingAirSegmentRsp)) {
			isError.set(true);
		}
		return isError;
	}

	private boolean checkAirSegmentsValues(List<TripInfo> trips, BookingAirSegmentRsp bookingAirSegmentRsp) {
		boolean isSimilar = false;
		Map<String, TypeBaseAirSegment> typeBaseAirSegmentMap = new HashMap<>();

		// type7 is for Air, rest for other product like hotel,rail
		getList(bookingAirSegmentRsp.getUniversalRecord().getUniversalRecordChoice_type7().getAirReservation())
				.forEach(airReservation -> {
					for (TypeBaseAirSegment baseAirSegment : airReservation.getAirSegment()) {
						typeBaseAirSegmentMap.put(baseAirSegment.getKey().getTypeRef(), baseAirSegment);
					}
				});

		for (TripInfo tripInfo : trips) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				FareDetail fareDetail = TravelPortUtils.getFareDetail(segmentInfo.getPriceInfo(0));
				if (StringUtils.isNotEmpty(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey())) {
					TypeBaseAirSegment typeBaseAirSegment = typeBaseAirSegmentMap
							.get(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey());
					if (typeBaseAirSegment.getOrigin().getTypeIATACode().equals(segmentInfo.getDepartureAirportCode())
							&& TravelPortUtils.getStringDateTime(segmentInfo.getDepartTime())
									.equals(TravelPortUtils.getStringDateTime(
											TravelPortUtils.getIsoDateTime(typeBaseAirSegment.getDepartureTime())))
							&& typeBaseAirSegment.getClassOfService().getTypeClassOfService().equals(
									fareDetail.getClassOfBooking())
							&& typeBaseAirSegment.getStatus().equals("HS")) {
						isSimilar = true;
					} else {
						return false;
					}
				}
			}
		}
		return isSimilar;
	}
}
