package com.tgs.services.fms.sources.travelport;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.TripSeatMap;
import com.tgs.services.fms.datamodel.ssr.SeatInformation;
import com.tgs.services.fms.datamodel.ssr.SeatSSRInformation;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.travelport.www.schema.air_v47_0.Characteristic_type1;
import com.travelport.www.schema.air_v47_0.Characteristic_type2;
import com.travelport.www.schema.air_v47_0.Facility_type0;
import com.travelport.www.schema.air_v47_0.OptionalService_type0;
import com.travelport.www.schema.air_v47_0.Row_type0;
import com.travelport.www.schema.air_v47_0.Rows_type0;
import com.travelport.www.schema.air_v47_0.SeatMapReq;
import com.travelport.www.schema.air_v47_0.SeatMapRsp;
import com.travelport.www.schema.air_v47_0.TypeSeatAvailability;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import com.travelport.www.service.air_v47_0.AirServiceStub;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class TravelPortSSRManager extends TravelPortServiceManager {


	public TripSeatMap getSeatMap(TripInfo tripInfo) {
		TripSeatMap tripSeatMap = TripSeatMap.builder().build();
		AirServiceStub airService = bindingService.getAirService();
		listener.setType(AirUtils.getLogType("SeatMap", configuration));
		airService._getServiceClient().getAxisService().addMessageContextListener(listener);
		SeatMapReq seatMapReq = buildSeatMapReq(tripInfo);
		try {
			bindingService.setProxyAndAuthentication(airService, "SeatMap");
			SeatMapRsp seatMapRsp = airService.service(seatMapReq, getSessionContext(false));
			if (!checkAnyErrors(seatMapRsp)) {
				parseSeatMapResponse(seatMapRsp, tripSeatMap);
			}
		} catch (AirFaultMessage | RemoteException e) {
			throw new SupplierUnHandledFaultException(e.getMessage());
		} finally {
			airService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return tripSeatMap;
	}

	private void parseSeatMapResponse(SeatMapRsp seatMapRsp, TripSeatMap tripSeatMap) {
		Map<String, List<Row_type0>> seatMap = new LinkedHashMap<>();
		Map<String, OptionalService_type0> optionalServiceMap = new HashMap<String, OptionalService_type0>();
		if (Objects.nonNull(seatMapRsp.getOptionalServices())
				&& CollectionUtils.isNotEmpty(getList(seatMapRsp.getOptionalServices().getOptionalService()))) {
			for (OptionalService_type0 optionalService : seatMapRsp.getOptionalServices().getOptionalService()) {
				optionalServiceMap.put(optionalService.getKey().getTypeRef(), optionalService);
			}
		}
		for (Rows_type0 rows : seatMapRsp.getRows()) {
			seatMap.put(rows.getSegmentRef().getTypeRef(), getList(rows.getRow()));
		}
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum() == 0) {
				List<Row_type0> rows = seatMap.get(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey());
				List<SeatSSRInformation> seatInfos = new ArrayList<>();
				for (Row_type0 row : rows) {
					for (Facility_type0 facility : row.getFacility()) {
						if (StringUtils.isNotEmpty(facility.getSeatCode())) {
							SeatSSRInformation seatSSRInformation = new SeatSSRInformation();
							// seatSSRInformation.setSeatNo(row.getNumber().toString());
							seatSSRInformation.setSeatNo(facility.getSeatCode().replace("-", ""));
							seatSSRInformation.setCode(facility.getSeatCode().replace("-", ""));
							if (Objects.nonNull(facility.getPaid()) && facility.getPaid()) {
								seatSSRInformation.setAmount(getAmount(facility, optionalServiceMap));
								seatSSRInformation.setIsBooked(true);
							} else {
								seatSSRInformation.setIsBooked(isSeatBooked(facility));
							}
							if (CollectionUtils.isNotEmpty(getList(facility.getCharacteristic()))) {
								for (Characteristic_type1 characteristic : facility.getCharacteristic()) {
									if (characteristic.getValue().equalsIgnoreCase("AISLE")) {
										seatSSRInformation.setIsAisle(true);
										break;
									}
								}
							}
							seatInfos.add(seatSSRInformation);
						}
					}

				}
				if (CollectionUtils.isNotEmpty(seatInfos)) {
					SeatInformation seatInformation = SeatInformation.builder().seatsInfo(seatInfos).build();
					tripSeatMap.getTripSeat().put(segmentInfo.getId(), seatInformation);
				}
			}
		}

	}

	private Boolean isSeatBooked(Facility_type0 facility) {
		TypeSeatAvailability availability = facility.getAvailability();
		if (availability.getValue() == TypeSeatAvailability._Available) {
			return false;
		}
		return true;
	}

	private Double getAmount(Facility_type0 facility, Map<String, OptionalService_type0> optionalServiceMap) {
		OptionalService_type0 optionalService = optionalServiceMap.get(facility.getOptionalServiceRef());
		if (optionalService != null) {
			Double amount = Double.valueOf(optionalService.getTotalPrice().getTypeMoney().substring(3));
			return amount;
		}
		return 0.0;
	}

	private SeatMapReq buildSeatMapReq(TripInfo tripInfo) {
		SeatMapReq seatMapReq = new SeatMapReq();
		buildBaseCoreRequest(seatMapReq);
		buildAirSegmentList(getList(seatMapReq.getAirSegment()), tripInfo.getSegmentInfos(), true, false);
		return seatMapReq;
	}


}
