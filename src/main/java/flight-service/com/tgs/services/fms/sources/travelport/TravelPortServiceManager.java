package com.tgs.services.fms.sources.travelport;

import static com.tgs.services.fms.sources.travelport.TravelPortConstants.ORIGIN_APPLICATION;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.TRACE_ID;
import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.zip.GZIPInputStream;
import javax.xml.ws.BindingProvider;
import com.tgs.services.base.SoapRequestResponseListner;
import com.travelport.www.schema.common_v47_0.*;
import com.travelport.www.schema.sharedbooking_v47_0.*;
import com.travelport.www.soa.common.security.sessioncontext_v1.SessProp_type0;
import com.travelport.www.soa.common.security.sessioncontext_v1.SessTok_type0;
import org.apache.axis2.databinding.types.PositiveInteger;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.cxf.endpoint.Client;
import org.apache.cxf.frontend.ClientProxy;
import org.apache.cxf.jaxws.JaxWsClientProxy;
import org.apache.cxf.message.Message;
import org.apache.cxf.transport.common.gzip.GZIPInInterceptor;
import org.apache.cxf.transport.common.gzip.GZIPOutInterceptor;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.springframework.stereotype.Service;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CxfRequestLogInterceptor;
import com.tgs.services.base.CxfResponseLogInterceptor;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerMiscInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierCredential;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.GDSFareComponentMapper;
import com.tgs.services.fms.sources.AirSourceConstants;
import com.tgs.services.fms.utils.AirSupplierUtils;
import com.tgs.services.fms.utils.AirUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.air.SupplierUnHandledFaultException;
import com.travelport.www.schema.air_v47_0.AccountCodes_type0;
import com.travelport.www.schema.air_v47_0.AirPriceReq;
import com.travelport.www.schema.air_v47_0.AirPricingCommand_type0;
import com.travelport.www.schema.air_v47_0.AirPricingInfo_type0;
import com.travelport.www.schema.air_v47_0.AirPricingModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirPricingSolution_type0;
import com.travelport.www.schema.air_v47_0.AirSearchModifiers_type0;
import com.travelport.www.schema.air_v47_0.AirSegmentPricingModifiers_type0;
import com.travelport.www.schema.air_v47_0.BookingCode_type0;
import com.travelport.www.schema.air_v47_0.CodeshareInfo_type0;
import com.travelport.www.schema.air_v47_0.Connection_type0;
import com.travelport.www.schema.air_v47_0.FareInfo_type0;
import com.travelport.www.schema.air_v47_0.LowFareSearchReq;
import com.travelport.www.schema.air_v47_0.NumberInParty_type0;
import com.travelport.www.schema.air_v47_0.PassengerType_type0;
import com.travelport.www.schema.air_v47_0.PermittedBookingCodes_type0;
import com.travelport.www.schema.air_v47_0.PermittedCabins_type0;
import com.travelport.www.schema.air_v47_0.PreferredCarriers_type0;
import com.travelport.www.schema.air_v47_0.PreferredProviders_type0;
import com.travelport.www.schema.air_v47_0.TypeAvailabilitySource;
import com.travelport.www.schema.air_v47_0.TypeBaseAirSegment;
import com.travelport.www.schema.air_v47_0.TypeFaresIndicator;
import com.travelport.www.schema.air_v47_0.TypeTourCode;
import com.travelport.www.schema.universal_v47_0.SupportedVersions;
import com.travelport.www.schema.universal_v47_0.UniversalRecordRetrieveReq;
import com.travelport.www.schema.universal_v47_0.UniversalRecordRetrieveRsp;
import com.travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import com.travelport.www.service.sharedbooking_v47_0.SystemFaultMessage;
import com.travelport.www.service.universal_v47_0.UniversalRecordArchivedFaultMessage;
import com.travelport.www.service.universal_v47_0.UniversalRecordFaultMessage;
import com.travelport.www.service.universal_v47_0.UniversalRecordServiceStub;
import com.travelport.www.soa.common.security.sessioncontext_v1.SessionContext;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.AGENT_USER_NAME;


@Getter
@Setter
@Slf4j
@Service
@SuperBuilder
abstract class TravelPortServiceManager {

	protected Order order;

	protected List<SegmentInfo> segmentInfos;

	protected SupplierConfiguration configuration;

	protected AirSearchQuery searchQuery;

	protected String bookingId;

	protected String pnr;

	protected AirSourceConfigurationOutput sourceConfiguration;

	protected Client client;

	protected TravelPortBindingService bindingService;

	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected String sessionkey;

	protected LocalDateTime ticketingTimeLimit;

	protected String providerCode;

	protected String csrPnr;

	protected String universalLocatorCode;

	protected List<String> criticalMessageLogger;

	protected Map<PaxType, String> bookingTravelerRefMap;

	protected String reservationPNR;

	protected String searchId;

	protected BigInteger version;

	protected String currencyCode;

	protected String traceId;

	protected DeliveryInfo deliveryInfo;

	protected User bookingUser;

	protected Map<PaxType, List<String>> airPricingInfoPaxMap;

	protected SoapRequestResponseListner listener;

	protected static final String UNAUTHORIZED_CRED = "Transport error: 401 Error: Unauthorized";

	// protected DateTimeFormatter dateFormat =
	// DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.ENGLISH);

	public void init() {
		currencyCode = AirSourceConstants.getSupplierInfoCurrencyCode(configuration);
	}


	public void bindInterceptors(Object port, String type, String key, String endPointAction) {
		bindInterceptors(port, type, key, endPointAction, false);
	}

	public void bindInterceptors(Object port, String type, String key, String endPointAction, boolean logVisibility) {
		String endpointURL =
				StringUtils.isBlank(endPointAction) ? configuration.getSupplierCredential().getUrl() : endPointAction;
		BindingProvider provider = (BindingProvider) port;
		provider.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointURL);
		provider.getRequestContext().put(Message.PROTOCOL_HEADERS, addUserHeaders());
		client = ClientProxy.getClient(port);

		HTTPConduit http = (HTTPConduit) client.getConduit();
		HTTPClientPolicy httpClientPolicy = new HTTPClientPolicy();
		httpClientPolicy.setConnectionTimeout(AirSourceConstants.TIME_OUT_IN_MILLI_SECONDS);
		http.setClient(httpClientPolicy);

		List<String> visibilityGroups = logVisibility ? AirSupplierUtils.getLogVisibility() : new ArrayList<>();

		client.getInInterceptors().add(new CxfResponseLogInterceptor(type, key, true, visibilityGroups));
		client.getOutInterceptors().add(new CxfRequestLogInterceptor(type, key, true, visibilityGroups));
	}


	private Map<String, String> addUserHeaders() {
		SupplierCredential credential = configuration.getSupplierCredential();
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Securitykey", credential.getPassword());
		headers.put("Username", credential.getUserName());
		headers.put("Branchcode", credential.getOrganisationCode());
		return headers;
	}


	public String getResponseByRequest(String requestXML, String methodName, String soapAction, String requestURL)
			throws Exception {
		String sbStr = null;
		int numTry = 0;
		URLConnection urlCon = null;
		boolean doRetry = false;
		if (StringUtils.isNotEmpty(requestXML)) {
			do {
				try {
					URL url = new URL(requestURL);
					urlCon = url.openConnection();
					urlCon.setDoInput(true);
					urlCon.setDoOutput(true);
					urlCon.setUseCaches(false);
					urlCon.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
					urlCon.setRequestProperty("User-Agent", "Apache-HttpClient/4.1.1");
					urlCon.setRequestProperty("Accept-Encoding", "gzip,deflate");
					urlCon.setRequestProperty("SOAPAction", soapAction);
					urlCon.setRequestProperty("Host", "minirules.itq.in");
					urlCon.setRequestProperty("Securitykey", configuration.getSupplierCredential().getPassword());
					urlCon.setRequestProperty("Username", configuration.getSupplierCredential().getUserName());
					urlCon.setRequestProperty("Branchcode",
							configuration.getSupplierCredential().getOrganisationCode());
					HttpURLConnection httpUrlCon = (HttpURLConnection) url.openConnection();
					httpUrlCon.setRequestMethod(HttpUtils.REQ_METHOD_POST);

					DataOutputStream printout = new DataOutputStream(urlCon.getOutputStream());
					printout.writeBytes(requestXML);
					printout.flush();
					printout.close();

					InputStream rawInStream = urlCon.getInputStream();
					InputStream dataInput = null;
					String encoding = urlCon.getContentEncoding();
					if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
						dataInput = new GZIPInputStream(new BufferedInputStream(rawInStream));
					} else {
						dataInput = new BufferedInputStream(rawInStream);
					}
					StringBuffer sb = new StringBuffer();
					int rc;
					while ((rc = dataInput.read()) != -1) {
						sb.append((char) rc);
					}
					dataInput.close();
					sbStr = sb.toString();
					sbStr = sbStr.replaceAll("&lt;", "<").replaceAll("&gt;", ">").replaceAll("&#xD;", "");
				} catch (IOException e) {
					log.error("Not able to connect for methodName {}", methodName);
					doRetry = false;
				} catch (Exception e) {
					log.error("Exception Occured while trying  methodName {}", methodName, e);
					doRetry = false;
				} finally {

				}
			} while (numTry++ < 3 && doRetry);
		}
		return sbStr;
	}

	public double getAmountBasedOnCurrency(String price, String fromCurrency, double dividingFactor) {
		Double amount = Double.valueOf(price.replaceAll(fromCurrency, ""));
		String toCurrency = AirSourceConstants.getClientCurrencyCode();
		if (StringUtils.equalsIgnoreCase(fromCurrency, toCurrency)) {
			return amount.doubleValue() / dividingFactor;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency).type(getType())
				.toCurrency(toCurrency).build();
		return moneyExchnageComm.getExchangeValue((amount.doubleValue() / dividingFactor), filter, true);
	}

	public String getType() {
		AirSourceType sourceType = AirSourceType.getAirSourceType(configuration.getSourceId());
		return StringUtils.join(sourceType.name(), "_", configuration.getSupplierCredential().getPcc()).toUpperCase();
	}

	public UniversalRecordRetrieveRsp getUnivRecordRetrieveRS(String csrPnr) {
		String id = StringUtils.isNotBlank(bookingId) ? bookingId : csrPnr;
		UniversalRecordServiceStub universalRecordService = bindingService.getUniversalRecordService();
		UniversalRecordRetrieveReq universalRecordRetrieveReq = buildUniversalRequest(csrPnr);
		UniversalRecordRetrieveRsp universalRecordRetrieveRsp = null;
		listener.setType("RecordRetrieve");
		try {
			universalRecordService._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(universalRecordService, "UniversalRecordRetrieve");
			universalRecordRetrieveRsp = universalRecordService.service(universalRecordRetrieveReq,
					getSessionContext(false), getSupportedVersion());
			if (StringUtils.isNotBlank(universalRecordRetrieveRsp.getTraceId())) {
				traceId = universalRecordRetrieveRsp.getTraceId();
			} else {
				traceId = TravelPortUtils.keyGenerator();
			}
			version = universalRecordRetrieveRsp.getUniversalRecord().getVersion().getTypeURVersion();
			universalLocatorCode =
					universalRecordRetrieveRsp.getUniversalRecord().getLocatorCode().getTypeLocatorCode();
		} catch (UniversalRecordArchivedFaultMessage | UniversalRecordFaultMessage | RemoteException faultMessage) {
			logCriticalMessage(faultMessage.getMessage());
			throw new SupplierUnHandledFaultException(faultMessage.getMessage());
		} finally {
			universalRecordService._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return universalRecordRetrieveRsp;
	}

	protected SupportedVersions getSupportedVersion() {
		SupportedVersions version = new SupportedVersions();
		return version;
	}


	private UniversalRecordRetrieveReq buildUniversalRequest(String blockedPnr) {
		UniversalRecordRetrieveReq universalRecordRetrieveReq = new UniversalRecordRetrieveReq();
		buildBaseCoreRequest(universalRecordRetrieveReq);
		com.travelport.www.schema.universal_v47_0.ProviderReservationInfo_type1 providerReservationInfo =
				new com.travelport.www.schema.universal_v47_0.ProviderReservationInfo_type1();
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(segmentInfos.get(0).getPriceInfoList())
				&& StringUtils
						.isNotBlank(segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getProviderCode())) {
			providerReservationInfo.setProviderCode(
					getProviderCode(segmentInfos.get(0).getPriceInfoList().get(0).getMiscInfo().getProviderCode()));
		}
		providerReservationInfo.setProviderLocatorCode(getTypeProviderLocatorCode(blockedPnr));
		universalRecordRetrieveReq.setProviderReservationInfo(providerReservationInfo);
		return universalRecordRetrieveReq;
	}

	protected void buildCredentialValues(LowFareSearchReq lowFareSearchRequest) {
		lowFareSearchRequest.setAuthorizedBy(AGENT_USER_NAME);
		lowFareSearchRequest
				.setTargetBranch(getTypeBranchCode(configuration.getSupplierCredential().getOrganisationCode()));
		lowFareSearchRequest.setTraceId(TRACE_ID);
		lowFareSearchRequest.setSolutionResult(false);
		lowFareSearchRequest.setBillingPointOfSaleInfo(buildBillingPointOfSale());
	}

	protected BillingPointOfSaleInfo_type0 buildBillingPointOfSale() {
		BillingPointOfSaleInfo_type0 billingPointOfSaleInfo = new BillingPointOfSaleInfo_type0();
		billingPointOfSaleInfo.setOriginApplication(ORIGIN_APPLICATION);
		return billingPointOfSaleInfo;
	}

	protected void buildPassengerDetails(LowFareSearchReq lowFareSearchRequest) {
		List<SearchPassenger_type0> searchPassenger_type0s = new ArrayList<>();
		for (Map.Entry<PaxType, Integer> paxInfo : searchQuery.getPaxInfo().entrySet()) {
			int paxCount = paxInfo.getValue();
			while (paxCount > 0) {
				SearchPassenger_type0 type0 = new SearchPassenger_type0();
				PaxType paxType = paxInfo.getKey();
				TypePTC type = new TypePTC();
				type.setTypePTC(paxType.getType());
				type0.setCode(type);
				type0.setBookingTravelerRef(TravelPortUtils.keyGenerator());
				if (PaxType.CHILD.getType().equals(paxType.getType())) {
					type0.setAge(new BigInteger(TravelPortConstants.CHILD_AGE));
				}
				if (PaxType.INFANT.getType().equals(paxType.getType())) {
					type0.setPricePTCOnly(Boolean.TRUE);
					type0.setAge(new BigInteger(TravelPortConstants.INFANT_AGE));
				}
				searchPassenger_type0s.add(type0);
				paxCount--;
			}
		}
		lowFareSearchRequest.setSearchPassenger(searchPassenger_type0s.toArray(new SearchPassenger_type0[0]));
	}

	protected void buildAirSearchModifiers(LowFareSearchReq lowFareSearchRequest) {
		AirSearchModifiers_type0 airSearchModifiers = new AirSearchModifiers_type0();
		airSearchModifiers.setMaxSolutions(new BigInteger(getItinCount()));
		if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
			PreferredProviders_type0 preferredProviders = new PreferredProviders_type0();
			Provider_type0 provider = new Provider_type0();
			provider.setCode(getTypeProviderCode(configuration.getSupplierCredential().getProviderCode()));
			preferredProviders.addProvider(provider);
			airSearchModifiers.setPreferredProviders(preferredProviders);
		}
		if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			PreferredCarriers_type0 preferredCarriers = new PreferredCarriers_type0();
			airSearchModifiers.setPreferredCarriers(preferredCarriers);
			Carrier_type0[] carrier_type0s = new Carrier_type0[searchQuery.getPreferredAirline().size()];
			int index = 0;
			for (AirlineInfo airlineInfo : searchQuery.getPreferredAirline()) {
				Carrier_type0 carrier_type0 = new Carrier_type0();
				TypeCarrier carrier = getTypeCarrierCode(airlineInfo.getCode());
				carrier_type0.setCode(carrier);
				carrier_type0s[index++] = carrier_type0;
			}
			airSearchModifiers.getPreferredCarriers().setCarrier(carrier_type0s);
		}
		lowFareSearchRequest.setAirSearchModifiers(airSearchModifiers);
	}

	protected TypeProviderCode getTypeProviderCode(String providerCode2) {
		TypeProviderCode code = new TypeProviderCode();
		code.setTypeProviderCode(providerCode2);
		return code;
	}


	public String getItinCount() {
		int flightCount = 100;
		if (sourceConfiguration != null) {
			if (searchQuery.getIsDomestic()) {
				flightCount = ObjectUtils.firstNonNull(sourceConfiguration.getDomflightsCount(),
						sourceConfiguration.getFlightsCount(), flightCount);
			} else {
				flightCount = ObjectUtils.firstNonNull(sourceConfiguration.getFlightsCount(),
						sourceConfiguration.getDomflightsCount(), flightCount);
			}
		}
		return String.valueOf(flightCount);
	}


	protected void setAccountCodesInReq(List<String> accountCodeList, AirPricingModifiers_type0 airPricingModifiers) {
		if (CollectionUtils.isNotEmpty(accountCodeList)) {
			AccountCodes_type0 accountCodes = new AccountCodes_type0();
			airPricingModifiers.setAccountCodes(accountCodes);
			for (String accountCodeEntry : accountCodeList) {
				AccountCode_type0 accountCode = new AccountCode_type0();
				accountCode.setCode(getCode(accountCodeEntry));
				if (StringUtils.isNotBlank(configuration.getSupplierCredential().getProviderCode())) {
					accountCode
							.setProviderCode(getProviderCode(configuration.getSupplierCredential().getProviderCode()));
				}
				accountCodes.addAccountCode(accountCode);
			}
			airPricingModifiers.setAccountCodes(accountCodes);
		}
	}

	private AccountCode_type0[] addAccountCode(AccountCode_type0[] accountCode, AccountCode_type0 code) {
		List<AccountCode_type0> acCodes = new ArrayList<AccountCode_type0>(Arrays.asList(accountCode));
		acCodes.add(code);
		accountCode = acCodes.toArray(accountCode);
		return accountCode;
	}


	protected TypeProviderCode getProviderCode(String code) {
		TypeProviderCode providerCode = new TypeProviderCode();
		providerCode.setTypeProviderCode(code);
		return providerCode;
	}


	protected Code_type1 getCode(String accountCodeEntry) {
		Code_type1 code = new Code_type1();
		code.setCode_type1(accountCodeEntry);
		return code;
	}


	protected void buildBaseCoreRequest(BaseCoreReq baseCoreReq) {
		baseCoreReq.setAuthorizedBy(AGENT_USER_NAME);
		baseCoreReq.setTargetBranch(getTypeBranchCode(configuration.getSupplierCredential().getOrganisationCode()));
		baseCoreReq.setTraceId(traceId);
		baseCoreReq.setBillingPointOfSaleInfo(buildBillingPointOfSale());
	}


	protected void setAirlinePnrForSegment(String supplierCode, String supplierLocatorCode) {
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getAirlineCode(false).equals(supplierCode)) {
				segmentInfo.updateAirlinePnr(supplierLocatorCode);
			}
		});
	}

	protected List<FlightTravellerInfo> getPaxCountList() {
		List<FlightTravellerInfo> travellerInfoList = segmentInfos.get(0).getTravellerInfo();
		return travellerInfoList;
	}

	protected void logCriticalMessage(String message) {
		if (criticalMessageLogger != null && StringUtils.isNotBlank(message)) {
			criticalMessageLogger.add(message);
		}
	}

	protected void buildAirSegmentList(List<TypeBaseAirSegment> typeBaseAirSegmentList, List<SegmentInfo> segmentInfos,
			boolean isBookingClassRequired, boolean isInfantRequired) {
		TypeBaseAirSegment typeBaseAirSegment = null;
		int groupId = 0;
		for (int segmentIndex = 0; segmentIndex < segmentInfos.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfos.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			Integer nextSegmentLegNum = (segmentIndex + 1 < segmentInfos.size())
					? segmentInfos.get(segmentIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			if (legNum == 0) {
				FareDetail fareDetail = TravelPortUtils.getFareDetail(segmentInfo.getPriceInfo(0));
				PriceMiscInfo miscInfo = segmentInfo.getPriceInfo(0).getMiscInfo();
				typeBaseAirSegment = new TypeBaseAirSegment();
				typeBaseAirSegment.setKey(getTypeRef(miscInfo.getSegmentKey()));
				typeBaseAirSegment.setGroup(groupId);
				if (StringUtils.isNotBlank(miscInfo.getAvailablitySource())) {
					typeBaseAirSegment.setAvailabilitySource(getTypeAvailability(miscInfo.getAvailablitySource()));
				}
				if (StringUtils.isNotBlank(miscInfo.getAvailabilityDisplayType())) {
					typeBaseAirSegment.setAvailabilityDisplayType(miscInfo.getAvailabilityDisplayType());
				}
				if (StringUtils.isNotBlank(miscInfo.getPolledAvailabilityOption())) {
					typeBaseAirSegment.setPolledAvailabilityOption(miscInfo.getPolledAvailabilityOption());
				}
				if (StringUtils.isNotBlank(miscInfo.getParticipationLevel())) {
					typeBaseAirSegment.setParticipantLevel(miscInfo.getParticipationLevel());
				}
				if (Objects.nonNull(miscInfo.getLinkavailablity())) {
					typeBaseAirSegment.setLinkAvailability(miscInfo.getLinkavailablity());
				}
				typeBaseAirSegment.setCarrier(getTypeCarrierCode(segmentInfo.getAirlineCode(false)));
				typeBaseAirSegment.setFlightNumber(getTypeFlightNumber(segmentInfo.getFlightNumber()));
				if (isBookingClassRequired)
					typeBaseAirSegment.setClassOfService(getTypeClassOfService(fareDetail.getClassOfBooking()));
				typeBaseAirSegment.setOrigin(getIATACode(segmentInfo.getDepartureAirportCode()));
				if (searchQuery != null) {
					typeBaseAirSegment.setNumberInParty(getNumberInParty(
							AirUtils.getPaxCountFromPaxInfo(searchQuery.getPaxInfo(), isInfantRequired)));
				} else {
					typeBaseAirSegment.setNumberInParty(getNumberInParty(
							AirUtils.getPaxCountFromTravellerInfo(segmentInfo.getTravellerInfo(), isInfantRequired)));
				}
				typeBaseAirSegment.setDepartureTime(TravelPortUtils.getStringDateTime(segmentInfo.getDepartTime()));
				if (StringUtils.isNotBlank(segmentInfo.getPriceInfo(0).getMiscInfo().getProviderCode())) {
					typeBaseAirSegment.setProviderCode(getProviderCode(miscInfo.getProviderCode()));
				}
				if (segmentInfo.getOperatedByAirlineInfo() != null) {
					CodeshareInfo_type0 codeshareInfo = new CodeshareInfo_type0();
					codeshareInfo
							.setOperatingCarrier(getTypeCarrierCode(segmentInfo.getOperatedByAirlineInfo().getCode()));
					codeshareInfo.setString(segmentInfo.getOperatedByAirlineInfo().getCode());
					typeBaseAirSegment.setCodeshareInfo(codeshareInfo);
				}
				typeBaseAirSegment.setCabinClass(TravelPortUtils.getCabinClass(fareDetail.getCabinClass().getName()));
			}
			typeBaseAirSegment.setDestination(getIATACode(segmentInfo.getArrivalAirportCode()));
			typeBaseAirSegment.setArrivalTime(TravelPortUtils.getStringDateTime(segmentInfo.getArrivalTime()));
			if (nextSegmentLegNum == 0) {
				if (segmentIndex + 1 < segmentInfos.size()) {
					SegmentInfo nextSegmentInfo = segmentInfos.get(segmentIndex + 1);
					if (nextSegmentInfo.getSegmentNum() != 0) {
						Connection_type0 connection = new Connection_type0();
						typeBaseAirSegment.setConnection(connection);
					} else {
						groupId++;
					}
				}
				typeBaseAirSegmentList.add(typeBaseAirSegment);
			}
		}
	}


	private NumberInParty_type0 getNumberInParty(int paxCountFromPaxInfo) {
		NumberInParty_type0 numberInParty = new NumberInParty_type0();
		PositiveInteger positiveInteger = new PositiveInteger(String.valueOf(paxCountFromPaxInfo), 36);
		numberInParty.setNumberInParty_type0(positiveInteger);
		return numberInParty;
	}


	protected TypeAvailabilitySource getTypeAvailability(String availablitySource) {
		TypeAvailabilitySource avaSource = new TypeAvailabilitySource();
		avaSource.setTypeAvailabilitySource(availablitySource);
		return avaSource;
	}


	protected TypeClassOfService getTypeClassOfService(String classOfBooking) {
		TypeClassOfService service = new TypeClassOfService();
		service.setTypeClassOfService(classOfBooking);
		return service;
	}


	protected void buildAirPricingCommand(List<SegmentInfo> segmentInfoList, AirPriceReq airPriceReq,
			boolean isFareBasisCodeReq) {
		AirPricingCommand_type0 airPricingCommand = new AirPricingCommand_type0();
		for (int segmentIndex = 0; segmentIndex < segmentInfoList.size(); segmentIndex++) {
			SegmentInfo segmentInfo = segmentInfoList.get(segmentIndex);
			Integer legNum = segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum();
			if (legNum == 0) {
				if (StringUtils.isNotEmpty(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey())) {
					AirSegmentPricingModifiers_type0 airSegmentPricingModifiers =
							new AirSegmentPricingModifiers_type0();
					airSegmentPricingModifiers.setAirSegmentRef(
							getTypeRef(segmentInfo.getPriceInfoList().get(0).getMiscInfo().getSegmentKey()));

					/*
					 * Check if fareBasisCode is required in case of change class, or it can be compromise.
					 */
					if (isFareBasisCodeReq)
						airSegmentPricingModifiers
								.setFareBasisCode(segmentInfo.getPriceInfoList().get(0).getFareBasis(PaxType.ADULT));

					PermittedBookingCodes_type0 permittedBookingCodes = new PermittedBookingCodes_type0();

					// Considering the booking code of Adult as of now
					BookingCode_type0 bookingCode = new BookingCode_type0();
					bookingCode.setCode(getTypeClassOfService(
							segmentInfo.getPriceInfoList().get(0).getBookingClass(PaxType.ADULT)));
					permittedBookingCodes.addBookingCode(bookingCode);
					airSegmentPricingModifiers.setPermittedBookingCodes(permittedBookingCodes);
					airPricingCommand.addAirSegmentPricingModifiers(airSegmentPricingModifiers);
				}
			}
		}
		airPriceReq.addAirPricingCommand(airPricingCommand);
	}

	protected AirPricingModifiers_type0 getAirPricingModifiers(List<SegmentInfo> segmentInfos) {
		AirPricingModifiers_type0 airPricingModifiers = new AirPricingModifiers_type0();
		boolean isPrivateFare = TravelPortUtils.isPrivateFare(segmentInfos);
		if (isPrivateFare) {
			airPricingModifiers.setFaresIndicator(TypeFaresIndicator.PrivateFaresOnly);
			String accountCode = TravelPortUtils.getAccountCodeFromTrip(segmentInfos);
			if (StringUtils.isNotBlank(accountCode))
				setAccountCodesInReq(Arrays.asList(accountCode), airPricingModifiers);
		} else {
			airPricingModifiers.setFaresIndicator(TypeFaresIndicator.PublicFaresOnly);
		}
		PermittedCabins_type0 permittedCabins = new PermittedCabins_type0();
		com.travelport.www.schema.common_v47_0.CabinClass_type0 cabinClass =
				new com.travelport.www.schema.common_v47_0.CabinClass_type0();
		cabinClass.setType(TravelPortUtils.getCabinClass(
				segmentInfos.get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass().getName()));
		permittedCabins.addCabinClass(cabinClass);
		airPricingModifiers.setPermittedCabins(permittedCabins);
		// airPricingModifiers.setInventoryRequestType(value);
		return airPricingModifiers;
	}

	protected boolean checkAnyErrors(BookingStartRsp baseRsp) {
		AtomicBoolean isAnyError = new AtomicBoolean();
		StringJoiner message = new StringJoiner("");
		if (baseRsp.getBookingStartRsp().getResponseMessage() != null) {
			getList(baseRsp.getBookingStartRsp().getResponseMessage()).forEach(responseMessage -> {
				if (StringUtils.isNotBlank(responseMessage.getType().getValue())
						&& responseMessage.getType().getValue().toLowerCase().equals(TravelPortConstants.ERROR)) {
					message.add(responseMessage.getType().getValue());
					isAnyError.set(true);
				}
				if (StringUtils.isNotBlank(responseMessage.getType().getValue())
						&& responseMessage.getType().getValue().toLowerCase().equals(TravelPortConstants.WARNING)) {
					message.add(responseMessage.getType().getValue());
				}
			});
			if (StringUtils.isNotBlank(message.toString()))
				logCriticalMessage(message.toString());
		}
		if (StringUtils.isNotBlank(message.toString())) {
			logCriticalMessage(message.toString());
		}
		return isAnyError.get();
	}

	public boolean isSegmentsActive(String crsPNR) {
		UniversalRecordRetrieveRsp univRecordRetrieveRS = this.getUnivRecordRetrieveRS(crsPNR);
		version = univRecordRetrieveRS.getUniversalRecord().getVersion().getTypeURVersion();
		universalLocatorCode = univRecordRetrieveRS.getUniversalRecord().getLocatorCode().getTypeLocatorCode();

		for (TypeBaseAirSegment typeBaseAirSegment : univRecordRetrieveRS.getUniversalRecord()
				.getUniversalRecordChoice_type7().getAirReservation()[0].getAirSegment()) {
			if (!typeBaseAirSegment.getStatus().equals("HK")) {
				return false;
			}
		}

		buildSegmentKeys(univRecordRetrieveRS);
		createAirPricingInfoMap(
				univRecordRetrieveRS.getUniversalRecord().getUniversalRecordChoice_type7().getAirReservation()[0]
						.getAirPricingInfo());
		if (MapUtils.isEmpty(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getPaxPricingInfo())) {
			segmentInfos.get(0).getPriceInfo(0).getMiscInfo().setPaxPricingInfo(airPricingInfoPaxMap);
		}
		if (MapUtils.isEmpty(airPricingInfoPaxMap)) {
			return false;
		}
		setTravellerMiscInfo(getList(univRecordRetrieveRS.getUniversalRecord().getBookingTraveler()));
		return true;
	}


	public void buildSegmentKeys(UniversalRecordRetrieveRsp univRecordRetrieveRS) {
		HashMap<String, String> travelportSegmentMap = new HashMap<String, String>();

		for (TypeBaseAirSegment typeBaseAirSegment : univRecordRetrieveRS.getUniversalRecord()
				.getUniversalRecordChoice_type7().getAirReservation()[0].getAirSegment()) {
			String segmentKey = typeBaseAirSegment.getOrigin() + "_" + typeBaseAirSegment.getDestination();
			travelportSegmentMap.put(segmentKey, typeBaseAirSegment.getKey().getTypeRef());
		}

		int index = 0;
		while (index < segmentInfos.size()) {
			SegmentInfo segmentInfo = segmentInfos.get(index);
			String origin = segmentInfo.getDepartureAirportCode();
			String destination = segmentInfo.getArrivalAirportCode();
			Integer nextSegmentLegNum = (index + 1 < segmentInfos.size())
					? segmentInfos.get(index + 1).getPriceInfo(0).getMiscInfo().getLegNum()
					: 0;
			int tempIndex = index;
			while (nextSegmentLegNum != 0) {
				segmentInfo = segmentInfos.get(tempIndex);
				destination = segmentInfo.getArrivalAirportCode();
				nextSegmentLegNum = (tempIndex + 1 < segmentInfos.size())
						? segmentInfos.get(tempIndex + 1).getPriceInfo(0).getMiscInfo().getLegNum()
						: 0;
				tempIndex++;
			}
			String segmentKey = travelportSegmentMap.get(origin + "_" + destination);
			if (index == tempIndex) {
				segmentInfo = segmentInfos.get(index);
				segmentInfo.getPriceInfo(0).getMiscInfo().setSegmentKey(segmentKey);
				index++;
			}
			while (index < tempIndex && index < segmentInfos.size()) {
				segmentInfo = segmentInfos.get(index);
				segmentInfo.getPriceInfo(0).getMiscInfo().setSegmentKey(segmentKey);
				index++;
			}
		}
	}

	protected void setTravellerMiscInfo(List<BookingTraveler_type0> bookingTravelers) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			List<FlightTravellerInfo> travellerInfoList = segmentInfo.getTravellerInfo();
			bookingTravelers.forEach(bookingTraveler -> {
				for (FlightTravellerInfo travellerInfo : travellerInfoList) {
					if (travellerInfo.getPaxType().getCode().equals(
							TravelPortPaxType.getPaxType(bookingTraveler.getTravelerType().getTypePTC()).getCode())) {
						TravellerMiscInfo travellerMiscInfo = travellerInfo.getMiscInfo();
						if (Objects.isNull(travellerMiscInfo)) {
							travellerMiscInfo = new TravellerMiscInfo();
							travellerInfo.setMiscInfo(travellerMiscInfo);
						}
						if (StringUtils.isBlank(travellerMiscInfo.getPassengerKey())) {
							travellerMiscInfo.setPassengerKey(bookingTraveler.getKey().getTypeRef());
							break;
						}
					}
				}
			});
		}
	}


	public String endSession(boolean ignoreSession, String type) {
		String pnr = null;
		BookingEndReq bookingEndReq = new BookingEndReq();
		buildBaseCoreRequest(bookingEndReq);
		bookingEndReq.setSessionKey(getTypeSessionKey(sessionkey));
		listener.setType(type);
		if (ignoreSession) {
			bookingEndReq.setSessionActivity(SessionActivity_type1.Ignore);
		} else {
			bookingEndReq.setSessionActivity(SessionActivity_type1.End);
		}
		SharedBookingServiceStub serviceStub = null;
		try {
			serviceStub = bindingService.getSharedBookingService();
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			bindingService.setProxyAndAuthentication(serviceStub, "EndSession");
			BookingEndRsp bookingEndRsp = serviceStub.service(bookingEndReq, getSessionContext(true));
			if (!Objects.isNull(bookingEndRsp.getBookingEndRsp().getUniversalRecord())) {
				pnr = getList(bookingEndRsp.getBookingEndRsp().getUniversalRecord().getProviderReservationInfo()).get(0)
						.getLocatorCode().getTypeProviderLocatorCode();
				version = bookingEndRsp.getBookingEndRsp().getUniversalRecord().getVersion().getTypeURVersion();
				csrPnr = pnr;
			}
		} catch (SystemFaultMessage systemFaultMessage) {
			logCriticalMessage(systemFaultMessage.toString());
		} catch (Exception e) {
			logCriticalMessage(e.getMessage());
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return pnr;
	}

	public Double getEquivalentTotalfare(AirPricingInfo_type0 airPricingInfo, double dividingFactor) {
		Double totalFare = new Double(0);
		if (StringUtils.isNotBlank(airPricingInfo.getApproximateTotalPrice().toString())) {
			totalFare = getAmountBasedOnCurrency(airPricingInfo.getApproximateTotalPrice().getTypeMoney(),
					airPricingInfo.getApproximateTotalPrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else {
			totalFare = getAmountBasedOnCurrency(airPricingInfo.getTotalPrice().getTypeMoney(),
					airPricingInfo.getBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return totalFare;
	}

	public Double getEquivalentTotalfare(AirPricingSolution_type0 airPricingSolution, double dividingFactor) {
		Double totalFare = new Double(0);
		if (StringUtils.isNotBlank(airPricingSolution.getApproximateTotalPrice().getTypeMoney())) {
			totalFare = getAmountBasedOnCurrency(airPricingSolution.getApproximateTotalPrice().getTypeMoney(),
					airPricingSolution.getApproximateTotalPrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else {
			totalFare = getAmountBasedOnCurrency(airPricingSolution.getTotalPrice().getTypeMoney(),
					airPricingSolution.getBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return totalFare;
	}

	public Double getEquivalentBaseFare(AirPricingInfo_type0 airPricingInfo, double dividingFactor) {
		Double baseFare = new Double(0);
		if (airPricingInfo.getEquivalentBasePrice() != null
				&& StringUtils.isNotBlank(airPricingInfo.getEquivalentBasePrice().getTypeMoney())) {
			baseFare = getAmountBasedOnCurrency(airPricingInfo.getEquivalentBasePrice().getTypeMoney(),
					airPricingInfo.getEquivalentBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else if (airPricingInfo.getApproximateBasePrice() != null
				&& StringUtils.isNotBlank(airPricingInfo.getApproximateBasePrice().getTypeMoney())) {
			baseFare = getAmountBasedOnCurrency(airPricingInfo.getApproximateBasePrice().getTypeMoney(),
					airPricingInfo.getApproximateBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		} else if (airPricingInfo.getBasePrice() != null) {
			baseFare = getAmountBasedOnCurrency(airPricingInfo.getBasePrice().getTypeMoney(),
					airPricingInfo.getBasePrice().getTypeMoney().substring(0, 3), dividingFactor);
		}
		return baseFare;
	}

	public void setTaxDetails(FareDetail fareDetail, List<TypeTaxInfo> taxInfoList, double dividingFactor) {
		if (CollectionUtils.isNotEmpty(taxInfoList)) {
			Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
			Map<String, GDSFareComponentMapper> gdsFares = GDSFareComponentMapper.getGdsFareComponents();
			taxInfoList.forEach(typeTaxInfo -> {
				Double amount = Double.valueOf(getAmountBasedOnCurrency(typeTaxInfo.getAmount().toString(),
						typeTaxInfo.getAmount().getTypeMoney().substring(0, 3), dividingFactor));
				if (gdsFares.keySet().contains(typeTaxInfo.getCategory())) {
					FareComponent fareComponent = gdsFares.get(typeTaxInfo.getCategory()).getFareComponent();
					Double previousAmount = fareComponents.getOrDefault(fareComponent, 0.0);
					fareComponents.put(fareComponent, previousAmount + amount);
				} else {
					FareComponent otherCharges = FareComponent.OC;
					Double previousAmount = fareComponents.getOrDefault(otherCharges, 0.0);
					fareComponents.put(otherCharges, previousAmount + amount);
				}
			});
		}
	}


	public void createAirPricingInfoMap(AirPricingInfo_type0[] airPricingInfoList) {
		airPricingInfoPaxMap = new LinkedHashMap<PaxType, List<String>>();
		for (AirPricingInfo_type0 airPricingInfo : airPricingInfoList) {
			String key = airPricingInfo.getKey().getTypeRef();
			PassengerType_type0 passengerType = getList(airPricingInfo.getPassengerType()).get(0);
			PaxType paxType = TravelPortPaxType.getPaxType(passengerType.getCode().getTypePTC());
			List<String> paxKeyList = null;
			if (airPricingInfoPaxMap.containsKey(paxType)) {
				paxKeyList = airPricingInfoPaxMap.get(paxType);
			} else {
				paxKeyList = new ArrayList<>();
			}
			paxKeyList.add(key);
			airPricingInfoPaxMap.put(paxType, paxKeyList);
		}
	}

	public void setFareRuleKeys(PriceMiscInfo miscInfo, FareInfo_type0 fareInfo) {
		if (Objects.nonNull(fareInfo.getFareRuleKey())) {
			if (StringUtils.isNotBlank(fareInfo.getFareRuleKey().getFareInfoRef())) {
				miscInfo.setFareRuleInfoRef(fareInfo.getFareRuleKey().getFareInfoRef());
				miscInfo.setFareRuleInfoValue(fareInfo.getFareRuleKey().getTypeNonBlanks());
			}
		}
	}

	protected void resetKeys() {
		for (SegmentInfo segmentInfo : segmentInfos) {
			for (FlightTravellerInfo flightTravellerInfo : segmentInfo.getTravellerInfo()) {
				if (Objects.nonNull(flightTravellerInfo.getMiscInfo()))
					flightTravellerInfo.getMiscInfo().setPassengerKey(null);
			}
			segmentInfo.getPriceInfo(0).getMiscInfo().setSegmentKey(null);
		}
	}

	protected void setFareIdentifier(FareInfo_type0 fareInfo, PriceInfo priceInfo1) {
		if (fareInfo != null && Objects.nonNull(fareInfo.getPrivateFare())) {
			if (ArrayUtils.isNotEmpty(fareInfo.getAccountCode())) {
				priceInfo1.getMiscInfo()
						.setAccountCode(Arrays.asList(fareInfo.getAccountCode()).get(0).getCode().getCode_type1());
				priceInfo1.setFareIdentifier(FareType.CORPORATE);
			} else {
				priceInfo1.setFareIdentifier(FareType.PUBLISHED);
			}
			priceInfo1.getMiscInfo().setIsPrivateFare(TravelPortUtils.isPrivateFare(fareInfo));
		} else {
			priceInfo1.setFareIdentifier(FareType.PUBLISHED);
		}
	}

	protected static <T> List<T> getList(T[] array) {
		if (array != null && ArrayUtils.isNotEmpty(array)) {
			List<T> list = new ArrayList<T>(Arrays.asList(array));
			return list;
		}
		return new ArrayList<>();
	}


	protected TypePTC getTypePTC(String type) {
		TypePTC code = new TypePTC();
		code.setTypePTC(type);
		return code;
	}

	protected TypeIATACode getIATACode(String code) {
		TypeIATACode airportCode = new TypeIATACode();
		airportCode.setTypeIATACode(code);
		return airportCode;
	}

	protected TypeFlightNumber getTypeFlightNumber(String flightNumber) {
		TypeFlightNumber flight = new TypeFlightNumber();
		flight.setTypeFlightNumber(flightNumber);
		return flight;
	}

	protected TypeCarrier getTypeCarrierCode(String airlineCode) {
		TypeCarrier carrierCode = new TypeCarrier();
		carrierCode.setTypeCarrier(airlineCode);
		return carrierCode;
	}

	protected TypeRef getTypeRef(String value) {
		TypeRef ref = new TypeRef();
		ref.setTypeRef(value);
		return ref;
	}

	protected TypeBranchCode getTypeBranchCode(String pcc) {
		TypeBranchCode branch = new TypeBranchCode();
		branch.setTypeBranchCode(pcc);
		return branch;
	}

	protected TypeSessionKey getTypeSessionKey(String sessionId) {
		TypeSessionKey sessionKey = new TypeSessionKey();
		sessionKey.setTypeRef(sessionId);
		return sessionKey;
	}

	protected TypeProviderLocatorCode getTypeProviderLocatorCode(String pnr) {
		TypeProviderLocatorCode locatorCode = new TypeProviderLocatorCode();
		locatorCode.setTypeProviderLocatorCode(pnr);
		return locatorCode;
	}

	protected TypeLocatorCode getTypeLocatorCode(String reservationPNR) {
		TypeLocatorCode locatorCode = new TypeLocatorCode();
		locatorCode.setTypeLocatorCode(reservationPNR);
		return locatorCode;
	}

	protected City_type1 getCityType(String city) {
		City_type1 cityType = new City_type1();
		cityType.setCity_type0(city);
		return cityType;
	}

	protected Type_type6 getType_Type6(String creditCardMode) {
		Type_type6 type = new Type_type6();
		type.setType_type6(creditCardMode);
		return type;
	}

	protected PostalCode_type1 getPostalCode(String postalCode) {
		PostalCode_type1 postal = new PostalCode_type1();
		postal.setPostalCode_type0(postalCode);
		return postal;
	}

	protected AddressName_type1 getAddressName(String address1) {
		AddressName_type1 address = new AddressName_type1();
		address.setAddressName_type0(address1);
		return address;
	}

	protected Country_type1 getCountryType(String nationality) {
		Country_type1 country = new Country_type1();
		country.setCountry_type0(nationality);
		return country;
	}

	protected TypeURVersion getTypeURversion(BigInteger version) {
		TypeURVersion ver = new TypeURVersion();
		ver.setTypeURVersion(version);
		return ver;
	}

	protected TypeEndorsement getTicketEndorsement(String string) {
		TypeEndorsement ticket = new TypeEndorsement();
		ticket.setTypeEndorsement(string);
		return ticket;
	}

	protected TypeTourCode getTypeTourCode(String tourCodeMisc) {
		TypeTourCode code = new TypeTourCode();
		code.setTypeTourCode(tourCodeMisc);
		return code;
	}

	protected Commission_type1 getCommissionType1(TypePercentageWithDecimal percentage) {
		Commission_type1 commission = new Commission_type1();
		commission.setLevel(TypeCommissionLevel.Fare);
		commission.setType(TypeCommissionType.PercentBase);
		commission.setPercentage(percentage);
		return commission;
	}

	protected TypePercentageWithDecimal getTypePercWithDec(String iataCommission) {
		TypePercentageWithDecimal per = new TypePercentageWithDecimal();
		per.setTypePercentageWithDecimal(iataCommission);
		return per;
	}

	protected TypeCardMerchantType getTypeMerchant(String cardType) {
		TypeCardMerchantType card = new TypeCardMerchantType();
		card.setTypeCardMerchantType(cardType);
		return card;
	}

	protected Name_type0 getNameType(String cardHolderName) {
		Name_type0 name = new Name_type0();
		name.setName_type0(cardHolderName);
		return name;
	}

	protected TypeCreditCardNumber getTypeCreditCard(String cardNumber) {
		TypeCreditCardNumber card = new TypeCreditCardNumber();
		card.setTypeCreditCardNumber(cardNumber);
		return card;
	}

	protected TypeGdsRemark getTypeGDS(String remarks) {
		TypeGdsRemark remark = new TypeGdsRemark();
		remark.setTypeGdsRemark(remarks);
		return remark;
	}

	protected TypeCardNumber getTypeCardNumber(String card) {
		TypeCardNumber cardNumber = new TypeCardNumber();
		cardNumber.setTypeCardNumber(card);
		return cardNumber;
	}

	protected TypeMoney getTypeMoney(String totalAirlineFare) {
		TypeMoney money = new TypeMoney();
		money.setTypeMoney(totalAirlineFare);
		return money;
	}

	protected TypeSSRFreeText getTypeSSrFreeText(String text) {
		TypeSSRFreeText freeText = new TypeSSRFreeText();
		freeText.setTypeSSRFreeText(text);
		return freeText;
	}

	protected TypeSSRCode getTypeSSrCode(String code) {
		TypeSSRCode ssrCode = new TypeSSRCode();
		ssrCode.setTypeSSRCode(code);
		return ssrCode;
	}

	protected Prefix_type1 getPrefix(String paxPrefix) {
		Prefix_type1 prefix = new Prefix_type1();
		prefix.setPrefix_type1(paxPrefix);
		return prefix;
	}

	protected TypeTravelerLastName getTravellerLastname(String lastName) {
		TypeTravelerLastName name = new TypeTravelerLastName();
		name.setTypeTravelerLastName(lastName);
		return name;
	}

	protected First_type2 getFirst_type2(String firstName) {
		First_type2 first = new First_type2();
		first.setFirst_type2(firstName);
		return first;
	}

	protected Text_type1 getText_Type1(String osiText) {
		Text_type1 text = new Text_type1();
		text.setText_type1(osiText);
		return text;
	}

	protected boolean checkAnyErrors(BaseRsp baseRsp) {
		boolean isError = false;
		ResponseMessage_type0[] responseMessgaes = baseRsp.getResponseMessage();
		if (ArrayUtils.isNotEmpty(responseMessgaes)) {
			for (ResponseMessage_type0 message : responseMessgaes)
				if (isError(message, "ERROR")) {
					isError = true;
					logCriticalMessage(message.getString());
				} else if (isWarning(message, "WARNING")) {
					// logCriticalMessage(message.getString());
				}
		}
		return isError;
	}


	protected boolean isError(ResponseMessage_type0 responseMessgae, String error) {
		return responseMessgae != null && error.equalsIgnoreCase(responseMessgae.getType().getValue().toUpperCase());
	}

	protected boolean isWarning(ResponseMessage_type0 responseMessgae, String warning) {
		return responseMessgae != null && warning.equalsIgnoreCase(responseMessgae.getType().getValue().toUpperCase());
	}

	protected SessionContext getSessionContext(boolean isRequired) {
		SessionContext session = new SessionContext();
		SessTok_type0 sessTok_type0 = new SessTok_type0();
		SessProp_type0[] props = new SessProp_type0[2];
		props[0] = new SessProp_type0();
		props[0].setNm("Username");
		props[0].setVal(configuration.getSupplierCredential().getUserName());
		props[1] = new SessProp_type0();
		props[1].setNm("Password");
		props[1].setVal(configuration.getSupplierCredential().getPassword());
		sessTok_type0.setId(sessionkey);
		if (isRequired) {
			session.setSessProp(props);
			session.setSessTok(sessTok_type0);
		} else {
			session.setSessProp(null);
			session.setSessTok(null);
		}
		return session;
	}

}
