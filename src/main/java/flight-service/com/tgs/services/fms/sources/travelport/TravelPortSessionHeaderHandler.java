package com.tgs.services.fms.sources.travelport;

import lombok.extern.slf4j.Slf4j;

import java.util.Set;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPHeader;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPHeaderElement;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;


@Slf4j
public class TravelPortSessionHeaderHandler implements SOAPHandler<SOAPMessageContext> {

	private String sessionKey;

	public TravelPortSessionHeaderHandler(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	@Override
	public Set<QName> getHeaders() {
		return null;
	}

	@Override
	public boolean handleMessage(SOAPMessageContext context) {
		Boolean outboundProperty = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);

		if (outboundProperty.booleanValue()) {
			try {
				SOAPEnvelope envelope = context.getMessage().getSOAPPart().getEnvelope();
				SOAPHeader header = envelope.getHeader();
				if (header == null) {
					header = envelope.addHeader();
				}
				String prefix = "ses";
				Name name = envelope.createName("SessionContext", prefix,
						"http://www.travelport.com/soa/common/security/SessionContext_v1");
				SOAPHeaderElement soapHeaderElement = header.addHeaderElement(name);
				soapHeaderElement.addChildElement("SessTok", prefix).setAttribute("id", sessionKey);
			} catch (Exception e) {
				log.error("Error on Session Context Travelport ", e);
			}
		}
		return outboundProperty;
	}

	@Override
	public boolean handleFault(SOAPMessageContext context) {
		return false;
	}

	@Override
	public void close(MessageContext context) {

	}
}
