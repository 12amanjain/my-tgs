package com.tgs.services.fms.sources.travelport;

import com.tgs.utils.exception.air.SupplierSessionException;
import com.travelport.www.schema.sharedbooking_v47_0.BookingStartReq;
import com.travelport.www.schema.sharedbooking_v47_0.BookingStartRsp;
import com.travelport.www.schema.sharedbooking_v47_0.SessionActivity_type1;
import com.travelport.www.schema.sharedbooking_v47_0.TypeSessionKey;
import com.travelport.www.schema.sharedbooking_v47_0.BookingBaseRsp;
import com.travelport.www.schema.sharedbooking_v47_0.BookingEndReq;
import com.travelport.www.schema.sharedbooking_v47_0.BookingEndRsp;
import com.travelport.www.service.sharedbooking_v47_0.SharedBookingServiceStub;
import com.travelport.www.service.sharedbooking_v47_0.SystemFaultMessage;
import com.travelport.www.service.air_v47_0.AirFaultMessage;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;
import java.rmi.RemoteException;
import java.util.*;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.IGNORE_SESSION_COMMAND;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.SHARED_BOOKING_SERVICE;
import static com.tgs.services.fms.sources.travelport.TravelPortConstants.AGENT_USER_NAME;

@Slf4j
@SuperBuilder
final class TravelPortSessionManager extends TravelPortServiceManager {

	public String createSession() throws SupplierSessionException {
		String sessionKey = StringUtils.EMPTY;
		SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
		listener.setType("Session");
		BookingStartRsp bookingBaseRsp = null;
		try {
			serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
			BookingStartReq bookingStartReq = new BookingStartReq();
			bookingStartReq.setAuthorizedBy(AGENT_USER_NAME);
			bookingStartReq
					.setTargetBranch(getTypeBranchCode(configuration.getSupplierCredential().getOrganisationCode()));
			bookingStartReq.setTraceId(traceId);
			bookingStartReq.setBillingPointOfSaleInfo(buildBillingPointOfSale());
			bookingStartReq.setProviderCode(getProviderCode(providerCode));
			bindingService.setProxyAndAuthentication(serviceStub, "CreateSession");
			bookingBaseRsp = serviceStub.service(bookingStartReq);
			if (bookingBaseRsp != null && !checkAnyErrors(bookingBaseRsp)) {
				sessionKey = bookingBaseRsp.getBookingStartRsp().getSessionKey().getTypeRef();
			}
		} catch (RemoteException | SystemFaultMessage fault) {
			logCriticalMessage(fault.getMessage());
			throw new SupplierSessionException(fault.getMessage());
		} finally {
			serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return sessionKey;
	}

	public void endSession(String sessionId) {
		if (StringUtils.isNotBlank(sessionId)) {
			SharedBookingServiceStub serviceStub = bindingService.getSharedBookingService();
			BookingEndReq bookingEndReq = new BookingEndReq();
			buildBaseCoreRequest(bookingEndReq);
			bookingEndReq.setSessionKey(getTypeSessionKey(sessionId));
			bookingEndReq.setSessionActivity(SessionActivity_type1.Ignore);
			listener.setType("EndSession");
			try {
				serviceStub._getServiceClient().getAxisService().addMessageContextListener(listener);
				bindingService.setProxyAndAuthentication(serviceStub, "EndSession");
				BookingEndRsp bookingEndRsp = serviceStub.service(bookingEndReq, getSessionContext(true));
				if (!Objects.isNull(bookingEndRsp.getBookingEndRsp().getResponseMessage())) {
					log.info(getList(bookingEndRsp.getBookingEndRsp().getResponseMessage()).get(0).getString());
				}
			} catch (SystemFaultMessage systemFaultMessage) {
				logCriticalMessage(systemFaultMessage.toString());
			} catch (Exception e) {
				logCriticalMessage(e.getMessage());
			} finally {
				serviceStub._getServiceClient().getAxisService().removeMessageContextListener(listener);
			}
		}
	}
}
