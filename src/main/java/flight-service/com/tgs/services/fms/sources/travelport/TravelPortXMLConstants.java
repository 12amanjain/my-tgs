package com.tgs.services.fms.sources.travelport;


class TravelPortXMLConstants {

	// MINI RULE
	public static final String MINI_RULE_PREFIX =
			"<s:Envelope xmlns:s=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:tem=\"http://tempuri.org/\"> <s:Body>";
	public static final String MINI_RULE_SUFFIX = "</s:Body> </s:Envelope>";
}
