package com.tgs.services.fms.utils;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.RefundableType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.ssr.BaggageSSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierSession;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.manager.TripBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirAnalyticsUtils {

	static String DELIMTER = ";";

	public static String getAirlines(List<SegmentInfo> segmentInfos) {
		Set<String> airlines = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			airlines.add(segmentInfo.getAirlineCode(false));
		});
		return airlines.stream().distinct().collect(Collectors.toList()).toString();
	}

	public static String getBookingClass(List<SegmentInfo> segmentInfos) {
		StringJoiner joiner = new StringJoiner(DELIMTER);
		segmentInfos.forEach(segmentInfo -> {
			StringJoiner segmentJoiner = new StringJoiner("-");
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
				segmentJoiner.add(fareDetail.getClassOfBooking());
			});
			joiner.add(segmentJoiner.toString());
		});
		return joiner.toString();
	}

	public static String getFareBasis(List<SegmentInfo> segmentInfos) {
		StringJoiner joiner = new StringJoiner(DELIMTER);
		segmentInfos.forEach(segmentInfo -> {
			StringJoiner segmentJoiner = new StringJoiner("-");
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			priceInfo.getFareDetails().forEach((paxType, fareDetail) -> {
				segmentJoiner.add(fareDetail.getFareBasis());
			});
			joiner.add(segmentJoiner.toString());
		});
		return joiner.toString();
	}

	public static String getFlightNumbers(List<SegmentInfo> segmentInfos) {
		Set<String> flightNumbers = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			flightNumbers.add(segmentInfo.getFlightNumber());
		});
		return flightNumbers.stream().distinct().collect(Collectors.toList()).toString();
	}

	public static String getFareType(List<SegmentInfo> segmentInfos) {
		Set<String> fareTypes = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getSegmentNum().intValue() == 0) {
				fareTypes.add(segmentInfo.getPriceInfo(0).getFareType());
			}
		});
		return fareTypes.toString();
	}

	public static String getSourceName(List<SegmentInfo> segmentInfos) {
		Set<String> sourceTypes = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getSupplierInfo().getSourceId().intValue() > 0) {
				sourceTypes.add(AirSourceType.getAirSourceType(segmentInfo.getSupplierInfo().getSourceId()).name());
			}
		});
		if (CollectionUtils.isNotEmpty(sourceTypes)) {
			return sourceTypes.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getSupplierName(List<SegmentInfo> segmentInfos) {
		Set<String> supplierName = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			supplierName.add(segmentInfo.getSupplierInfo().getSupplierId());
		});
		if (CollectionUtils.isNotEmpty(supplierName)) {
			return supplierName.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getSupplierDesc(List<SegmentInfo> segmentInfos) {
		Set<String> supplierDesc = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			supplierDesc.add(segmentInfo.getSupplierInfo().getDescription());
		});
		if (CollectionUtils.isNotEmpty(supplierDesc)) {
			return supplierDesc.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getCabinClass(List<SegmentInfo> segmentInfos) {
		Set<String> cabinClass = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.getPriceInfo(0).getFareDetails().forEach(((paxType, fareDetail) -> {
				if (fareDetail.getCabinClass() != null) {
					cabinClass.add(fareDetail.getCabinClass().getName());
				}
			}));
		});
		if (CollectionUtils.isNotEmpty(cabinClass)) {
			return cabinClass.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getCabinClass(AirSearchQuery searchQuery) {
		if (searchQuery != null && searchQuery.getCabinClass() != null) {
			return searchQuery.getCabinClass().name();
		}
		return null;
	}

	public static String getSearchId(List<SegmentInfo> segmentInfos) {
		Set<String> searchIds = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			if (StringUtils.isNotBlank(segmentInfo.getPriceInfo(0).getId())) {
				searchIds.add(FlightCacheHandler.getSearchQueryKey(segmentInfo.getPriceInfo(0).getId()));
			}
		});
		if (CollectionUtils.isNotEmpty(searchIds)) {
			return searchIds.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static Double getTotalAirlineFare(List<SegmentInfo> segmentInfos) {
		AtomicDouble totalAirlineFare = new AtomicDouble(0);
		segmentInfos.forEach(segmentInfo -> {
			totalAirlineFare.getAndAdd(segmentInfo.getPriceInfo(0).getTotalAirlineFare());
		});
		return totalAirlineFare.get();
	}

	public static String getSerializedRouteInfo(AirSearchQuery searchQuery) {
		StringJoiner routeJoiner = new StringJoiner(DELIMTER);
		searchQuery.getRouteInfos().forEach(routeInfo -> {
			routeJoiner.add(routeInfo.getRouteKey());
		});
		return routeJoiner.toString();
	}

	public static String getSerializedRouteInfoFromSegmentList(List<SegmentInfo> segmentInfos) {
		StringJoiner routeJoiner = new StringJoiner(DELIMTER);
		segmentInfos.forEach(segmentInfo -> {
			routeJoiner.add(segmentInfo.getSegmentKey());
		});
		return routeJoiner.toString();
	}

	public static Integer getDayDiff(LocalDate travelDate) {
		return Long.valueOf(ChronoUnit.DAYS.between(LocalDate.now(), travelDate)).intValue();
	}

	public static Double getTotalCommission(List<SegmentInfo> segmentInfos) {
		AtomicDouble commission = new AtomicDouble(0);
		segmentInfos.forEach(segmentInfo -> {
			if (MapUtils.isNotEmpty(segmentInfo.getPriceInfo(0).getFareDetails())) {
				segmentInfo.getPriceInfo(0).getFareDetails().forEach(((paxType, fareDetail) -> {
					if (MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
						for (FareComponent component : FareComponent.getAllCommisionComponents()) {
							commission.getAndAdd(fareDetail.getFareComponents().getOrDefault(component, 0d));
						}
					}
				}));
			}
		});
		return commission.get();
	}

	public static String getCommissionId(List<SegmentInfo> segmentInfos) {
		Set<String> commissionIds = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			if (segmentInfo.getPriceInfo(0).getMiscInfo().getCommericialRuleId() != null) {
				commissionIds.add(segmentInfo.getPriceInfo(0).getMiscInfo().getCommericialRuleId().toString());
			}
		});
		if (CollectionUtils.isNotEmpty(commissionIds)) {
			return commissionIds.toString();
		}
		return null;
	}

	public static String getSupplierSession(String bookingId, User bookingUser) {
		List<String> sessions = new ArrayList<>();
		List<SupplierSession> supplierSessions = TripBookingUtils.getSupplierSession(bookingId, bookingUser);
		supplierSessions.forEach(sessionId -> {
			if (sessionId.getSupplierSessionInfo() != null
					&& StringUtils.isNotBlank(sessionId.getSupplierSessionInfo().getSessionToken())) {
				sessions.add(sessionId.getSupplierSessionInfo().getSessionToken());
			}
		});
		if (CollectionUtils.isNotEmpty(sessions)) {
			return sessions.toString();
		}
		return null;
	}

	public static String getBaggages(List<SegmentInfo> segmentInfos) {
		Set<String> baggageInfo = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			StringJoiner segmentJoiner = new StringJoiner("-");
			if (CollectionUtils.isNotEmpty(segmentInfo.getTravellerInfo())) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
				travellerInfos.forEach(travellerInfo -> {
					if (travellerInfo.getSsrBaggageInfo() != null
							&& StringUtils.isNotBlank(travellerInfo.getSsrBaggageInfo().getDesc())) {
						baggageInfo.add(travellerInfo.getSsrBaggageInfo().getDesc());
					}
				});
			} else if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.BAGGAGE))) {
				StringJoiner segmentBaggages = new StringJoiner(",");
				List<BaggageSSRInformation> ssrInformations =
						(List<BaggageSSRInformation>) segmentInfo.getSsrInfo().get(SSRType.BAGGAGE);
				for (BaggageSSRInformation baggage : ssrInformations) {
					segmentBaggages.add(StringUtils.join(baggage.getCode(), "@", baggage.getAmount()));
				}
				segmentJoiner.add(StringUtils.join(segmentInfo.getSegmentNum(), "-", segmentBaggages.toString()));
				baggageInfo.add(segmentJoiner.toString());
			}
		});
		if (CollectionUtils.isNotEmpty(baggageInfo)) {
			return baggageInfo.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getMeal(List<SegmentInfo> segmentInfos) {
		Set<String> mealInfo = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			StringJoiner segmentJoiner = new StringJoiner("-");
			if (CollectionUtils.isNotEmpty(segmentInfo.getTravellerInfo())) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
				travellerInfos.forEach(travellerInfo -> {
					if (travellerInfo.getSsrMealInfo() != null
							&& StringUtils.isNotBlank(travellerInfo.getSsrMealInfo().getDesc())) {
						mealInfo.add(travellerInfo.getSsrMealInfo().getDesc());
					}
				});
			} else if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.MEAL))) {
				StringJoiner segmentMeals = new StringJoiner(",");
				List<SSRInformation> ssrInformations =
						(List<SSRInformation>) segmentInfo.getSsrInfo().get(SSRType.MEAL);
				ssrInformations
						.forEach(meal -> segmentMeals.add(StringUtils.join(meal.getCode(), "@", meal.getAmount())));
				segmentJoiner.add(StringUtils.join(segmentInfo.getSegmentNum(), "-", segmentMeals.toString()));
				mealInfo.add(segmentJoiner.toString());
			}
		});
		if (CollectionUtils.isNotEmpty(mealInfo)) {
			return mealInfo.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getSeat(List<SegmentInfo> segmentInfos) {
		Set<String> seatInfo = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			StringJoiner segmentJoiner = new StringJoiner("-");
			if (CollectionUtils.isNotEmpty(segmentInfo.getTravellerInfo())) {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
				travellerInfos.forEach(travellerInfo -> {
					if (travellerInfo.getSsrSeatInfo() != null
							&& StringUtils.isNotBlank(travellerInfo.getSsrSeatInfo().getCode())) {
						seatInfo.add(travellerInfo.getSsrSeatInfo().getCode());
					}
				});
			} else if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getSsrInfo().get(SSRType.SEAT))) {
				StringJoiner segmentSeats = new StringJoiner(",");
				List<SSRInformation> ssrInformations =
						(List<SSRInformation>) segmentInfo.getSsrInfo().get(SSRType.SEAT);
				ssrInformations
						.forEach(seat -> segmentSeats.add(StringUtils.join(seat.getCode(), "@", seat.getAmount())));
				segmentJoiner.add(StringUtils.join(segmentInfo.getSegmentNum(), "-", segmentSeats.toString()));
				seatInfo.add(segmentJoiner.toString());
			}
		});
		if (CollectionUtils.isNotEmpty(seatInfo)) {
			return seatInfo.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getFFlier(List<SegmentInfo> segmentInfos) {
		Set<String> fflier = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			List<FlightTravellerInfo> travellerInfos = segmentInfo.getTravellerInfo();
			travellerInfos.forEach(travellerInfo -> {
				if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
					travellerInfo.getFrequentFlierMap().forEach((airline, memeberShipId) -> {
						fflier.add(StringUtils.join(airline, "-", memeberShipId));
					});
				}
			});
		});
		if (CollectionUtils.isNotEmpty(fflier)) {
			return fflier.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static String getRefundableType(List<SegmentInfo> segmentInfos) {
		Set<String> refundableTypes = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.getPriceInfo(0).getFareDetails().forEach(((paxType, fareDetail) -> {
				RefundableType refundableType = RefundableType.getEnumFromCode(fareDetail.getRefundableType());
				if (refundableType != null) {
					refundableTypes.add(refundableType.name());
				}
			}));
		});
		if (CollectionUtils.isNotEmpty(refundableTypes)) {
			return refundableTypes.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	public static Integer getSearchResultCount(AirSearchResult searchResult, AirSearchQuery searchQuery) {
		if (searchResult != null && org.apache.commons.collections4.MapUtils.isNotEmpty(searchResult.getTripInfos())) {
			return searchResult.getTripInfos().getOrDefault(searchQuery.getTripType().getName(), new ArrayList<>())
					.size();
		}
		return 0;
	}

	public static String getAirlines(AirSearchQuery searchQuery) {
		if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			List<String> preferredAirline = new ArrayList<>();
			searchQuery.getPreferredAirline().forEach(airlineInfo -> {
				preferredAirline.add(airlineInfo.getCode());
			});
			return preferredAirline.toString();
		}
		return null;
	}

	public static String getAlternateClass(List<SegmentInfo> segmentInfos) {
		StringJoiner joiner = new StringJoiner(DELIMTER);
		segmentInfos.forEach(segmentInfo -> {
			if (CollectionUtils.isNotEmpty(segmentInfo.getAlternateClass())) {
				StringJoiner segmentJoiner = new StringJoiner("-");
				segmentJoiner.add(
						StringUtils.join(segmentInfo.getSegmentNum(), "-", segmentInfo.getAlternateClass().toString()));
				joiner.add(segmentJoiner.toString());
			}
		});
		if (StringUtils.isNotBlank(joiner.toString()) && !joiner.toString().equals(DELIMTER)) {
			return joiner.toString();
		}
		return null;
	}

	public static String getItemStatus(List<SegmentInfo> segmentInfos) {
		Set<AirItemStatus> itemStatus = new LinkedHashSet<>();
		segmentInfos.forEach(segmentInfo -> {
			itemStatus.add(segmentInfo.getBookingRelatedInfo().getStatus());
		});
		if (CollectionUtils.isNotEmpty(itemStatus)) {
			return itemStatus.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}
}
