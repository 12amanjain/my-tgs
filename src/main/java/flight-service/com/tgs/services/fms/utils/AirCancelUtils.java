package com.tgs.services.fms.utils;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceCancelConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirCancelUtils {


	public static List<TripInfo> createTripListFromCancelledSegmentList(List<SegmentInfo> cancelledSegmentList,
			List<TripInfo> tripInfos) {
		List<TripInfo> tripList = new ArrayList<>();
		boolean isMatched = false;
		for (TripInfo tripInfo : tripInfos) {
			TripInfo cancelledTripInfo = new TripInfo();
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (SegmentInfo cancelledSegmentInfo : cancelledSegmentList) {
					if (segmentInfo.getId().equals(cancelledSegmentInfo.getId())) {
						isMatched = true;
						if (!cancelledTripInfo.getSegmentInfos().contains(cancelledSegmentInfo))
							cancelledTripInfo.getSegmentInfos().add(cancelledSegmentInfo);
						break;
					}
				}
			}
			if (isMatched)
				tripList.add(cancelledTripInfo);
			isMatched = false;
		}
		return tripList;
	}

	public static List<TripInfo> createTripListFromSegmentListCancellation(List<SegmentInfo> segmentList) {
		List<TripInfo> tripList = new ArrayList<>();
		TripInfo tripInfo = null;
		SegmentInfo previousSegmentInfo = null;
		for (SegmentInfo segmentInfo : segmentList) {
			if (segmentInfo.getSegmentNum() == 0) {
				tripInfo = new TripInfo();
				tripList.add(tripInfo);
			}
			if (segmentInfo.getSegmentNum() > 0) {
				if (previousSegmentInfo == null) {
					log.error("Segment number is " + segmentInfo.getSegmentNum() + "segmentInfo is"
							+ segmentInfo.toString());
					tripInfo = new TripInfo();
					tripList.add(tripInfo);
				}

				if (previousSegmentInfo != null && segmentInfo.getDepartTime() != null
						&& previousSegmentInfo.getArrivalTime() != null)
					previousSegmentInfo.setConnectingTime(Duration
							.between(previousSegmentInfo.getArrivalTime(), segmentInfo.getDepartTime()).toMinutes());
			}
			tripInfo.getSegmentInfos().add(segmentInfo);
			previousSegmentInfo = segmentInfo;
		}
		return tripList;
	}

	public static AirSourceCancelConfiguration getAirSourceCancelConfiguration(SupplierBasicInfo basicInfo,
			BookingSegments bookingSegments, User bookingUser) {
		FlightBasicFact flightfact = FlightBasicFact.builder().build();
		if (basicInfo != null) {
			flightfact.setSourceId(basicInfo.getSourceId());
			flightfact.setSupplierId(basicInfo.getSupplierId());
		}
		if (CollectionUtils.isNotEmpty(bookingSegments.getCancelSegments())) {
			flightfact.generateFact(
					createTripListFromSegmentListCancellation(bookingSegments.getCancelSegments()).get(0));
		}
		BaseUtils.createFactOnUser(flightfact, bookingUser);
		return AirConfiguratorHelper.getAirConfigRule(flightfact, AirConfiguratorRuleType.CANCELCONFIG);
	}

	public static boolean isBasedOnFareTypeAllowed(AirSourceCancelConfiguration cancelConfiguration, String fareType,
			List<String> criticalMessageLogger) {
		Boolean isAllowed = true;
		if (MapUtils.isNotEmpty(cancelConfiguration.getCancellationAllowed())) {
			isAllowed = MapUtils.getBooleanValue(cancelConfiguration.getCancellationAllowed(), fareType, true);
		}
		if (!isAllowed) {
			criticalMessageLogger.add("AutoCancellation not allowed for fareType " + fareType);
		}
		return isAllowed;
	}

	public static boolean isDependsOnDepartureTime(AirSourceCancelConfiguration cancelConfiguration, AirType airType,
			LocalDateTime departureTime, String fareType, List<String> criticalMessageLogger) {
		boolean isAllowed = true;
		if (MapUtils.isNotEmpty(cancelConfiguration.getCancellationRestrictions())
				&& MapUtils.isNotEmpty(cancelConfiguration.getCancellationRestrictions().get(airType))) {
			Integer allowedBeforeHours =
					MapUtils.getInteger(cancelConfiguration.getCancellationRestrictions().get(airType), fareType, null);
			if (allowedBeforeHours != null) {
				isAllowed = !(Duration.between(LocalDateTime.now(), departureTime).toMinutes() <= allowedBeforeHours);
			}
		}
		if (!isAllowed) {
			criticalMessageLogger.add("AutoCancellation not allowed due to less departure time");
		}
		return isAllowed;
	}

	public static boolean isTravellerAllowed(FlightTravellerInfo traveller) {
		return (traveller.getStatus() == null
				|| traveller.getStatus() != null && !traveller.getStatus().equals(TravellerStatus.CANCELLED)
						&& !traveller.getStatus().equals(TravellerStatus.VOIDED));
	}

	public static double getAirCancellationFees(BookingSegments bookingSegments) {
		double airlineCancellationFee = 0.0;
		for (SegmentInfo segmentInfo : bookingSegments.getCancelSegments()) {
			for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
				if (travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.ACF, 0.0) != null) {
					airlineCancellationFee +=
							travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.ACF, 0.0);
				}

			}
		}
		return airlineCancellationFee;
	}
}
