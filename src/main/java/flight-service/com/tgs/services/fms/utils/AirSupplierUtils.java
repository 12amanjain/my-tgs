package com.tgs.services.fms.utils;

import com.google.common.collect.Lists;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.LogDataVisibilityGroup;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.analytics.SupplierAnalyticsQuery;
import com.tgs.services.fms.datamodel.AirSearchModifiers;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.SupplierFlowType;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.NetRemittanceOutput;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class AirSupplierUtils {

	public static final String CONTACT_PREFIX = "+";

	public static String removeIllegalCharacters(String text) {
		return text.replaceAll("[^a-zA-Z0-9!#$%&@'*+-/=?^_`{|}~.]+", "");
	}

	public static String getEmailId(DeliveryInfo deliveryInfo) {
		if (deliveryInfo == null || CollectionUtils.isEmpty(deliveryInfo.getEmails())) {
			return null;
		}
		String emailId = deliveryInfo.getEmails().get(0);
		return removeIllegalCharacters(emailId);
	}

	public static String getContactNumber(DeliveryInfo deliveryInfo) {
		if (deliveryInfo == null || CollectionUtils.isEmpty(deliveryInfo.getContacts())) {
			return null;
		}
		String contactNumber = deliveryInfo.getContacts().get(0);
		return contactNumber.replaceAll("[^0-9]", "");
	}

	public static String getContactNumberWithCountryCode(String contactNumber) {
		String countryCode = ServiceCommunicatorHelper.getClientInfo().getCountryCode();
		if (contactNumber.startsWith(StringUtils.join(CONTACT_PREFIX, countryCode))) {
			// contact number has info with proper country code
			contactNumber = contactNumber.replace(CONTACT_PREFIX, "");
			return contactNumber.replaceAll("\\s+", "");
		}
		if (!contactNumber.startsWith(countryCode)) {
			contactNumber = StringUtils.join(countryCode, contactNumber);
		}
		if (contactNumber.startsWith(CONTACT_PREFIX)) {
			contactNumber = contactNumber.replace(CONTACT_PREFIX, "");
		}
		// if (!contactNumber.startsWith(CONTACT_PREFIX)) {
		// contactNumber = StringUtils.join(CONTACT_PREFIX, contactNumber);
		// }
		return contactNumber.replaceAll("\\s+", "");
	}

	public static String getContactNumberWithCountryCode(DeliveryInfo deliveryInfo) {
		if (deliveryInfo == null || CollectionUtils.isEmpty(deliveryInfo.getContacts())) {
			return null;
		}
		return getContactNumberWithCountryCode(deliveryInfo.getContacts().get(0));
	}


	public static String getCompanyName(User bookingUser, ClientGeneralInfo clientInfo) {
		if (StringUtils.isNotBlank(bookingUser.getName())) {
			return bookingUser.getName();
		}
		return clientInfo.getCompanyName();
	}

	/**
	 * This will filter critical messages which we are getting frequently from the supplier
	 * 
	 * @param criticalMessages
	 * @return
	 */
	public static List<String> filterCriticalMessage(List<String> criticalMessages, boolean isSuccess, User user) {
		if (isSuccess && CollectionUtils.isNotEmpty(criticalMessages)) {
			AirGeneralPurposeOutput generalOutput =
					AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
			if (CollectionUtils.isNotEmpty(generalOutput.getCriticalLoggerExclusions())) {
				Iterator<String> criticalMessageIterator = criticalMessages.iterator();
				List<String> exclusions = generalOutput.getCriticalLoggerExclusions();
				while (criticalMessageIterator.hasNext()) {
					String criticalMessage = criticalMessageIterator.next();

					if (exclusions.contains(criticalMessage.trim())) {
						criticalMessageIterator.remove();
					}
				}
			}
		}
		return criticalMessages;
	}

	public static void addToAnalytics(List<SupplierAnalyticsQuery> analyticsInfos, long timeToCreate, String stubName) {
		if (analyticsInfos != null) {
			SupplierAnalyticsQuery stubQuery = SupplierAnalyticsQuery.builder()
					.flowtype(SupplierFlowType.STUB_CREATION.name())
					.searchtimeinsec(Long.valueOf(TimeUnit.MILLISECONDS.toSeconds(timeToCreate)).intValue()).build();
			stubQuery.setStubname(stubName);
			stubQuery.setGenerationtime(LocalDateTime.now());
			stubQuery.setCreatedtime(LocalDateTime.now().toString());
			analyticsInfos.add(stubQuery);
		}
	}

	public static void isStoreLogRequired(AirSearchQuery searchQuery) {
		// Temp code to disable search log
		if (!UserUtils.isMidOfficeRole(
				UserUtils.getEmulatedUserRoleOrUserRoleCode(SystemContextHolder.getContextData().getUser()))) {
			if (searchQuery.getSearchModifiers() == null) {
				searchQuery.setSearchModifiers(new AirSearchModifiers());
			}
			searchQuery.getSearchModifiers().setStoreSearchLog(Boolean.FALSE);
		}
	}

	public static Proxy getProxy(User user) {
		Proxy proxy = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		if (configuratorInfo != null && StringUtils.isNotBlank(configuratorInfo.getProxyAddress())) {
			String proxyAddress = configuratorInfo.getProxyAddress();
			proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
					new InetSocketAddress(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1])));
		}
		return proxy;
	}

	public static boolean isGSTMandatoryFareType(TripInfo trip) {
		Set<String> fareTypes = trip.getFareType(0);
		Optional<String> allowedTypes = fareTypes.stream().filter(ft -> ft.equalsIgnoreCase(FareType.SME.getName())
				|| ft.equalsIgnoreCase(FareType.CORPORATE_FLEX.getName())).findFirst();
		return allowedTypes.isPresent();
	}

	public static DeliveryInfo getDeliveryDetails(DeliveryInfo customerDeliveryInfo) {
		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		if (BooleanUtils.isTrue(clientInfo.getUseClientDeliveryInfo())) {
			DeliveryInfo deliveryInfo = new DeliveryInfo();
			deliveryInfo.setContacts(Arrays.asList(clientInfo.getWorkPhone()));
			deliveryInfo.setEmails(Arrays.asList(clientInfo.getEmail()));
			return deliveryInfo;
		}
		return customerDeliveryInfo;
	}

	public static List<String> getLogVisibility() {
		return new ArrayList<>(Lists.newArrayList(LogDataVisibilityGroup.DEVELOPER.name()));
	}

	public static String getAirlinePromotionCode(IFact fact) {
		NetRemittanceOutput configuratorRule =
				AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.NETREMITTANCE);
		if (Objects.nonNull(configuratorRule)) {
			return configuratorRule.getNetRemittanceCode();
		}
		return null;
	}

}
