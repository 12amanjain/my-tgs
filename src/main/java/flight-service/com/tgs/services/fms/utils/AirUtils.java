package com.tgs.services.fms.utils;

import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.FareChangeAlert;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirCustomPathOutput;
import com.tgs.services.fms.datamodel.AirPassportConditions;
import com.tgs.services.fms.datamodel.AirPath;
import com.tgs.services.fms.datamodel.AirSearchGrpOutput;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirSearchResult;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.AirlineTimeLimitData;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.ProcessedPriceInfo;
import com.tgs.services.fms.datamodel.ProcessedTripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirClientFeeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSSRInfoOutput;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;
import com.tgs.services.fms.datamodel.airconfigurator.DobOutput;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.datamodel.ssr.SSRType;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.helper.AirConfiguratorHelper;
import com.tgs.services.fms.helper.AirSourceType;
import com.tgs.services.fms.helper.AirlineHelper;
import com.tgs.services.fms.helper.AirportHelper;
import com.tgs.services.fms.helper.ServiceCommunicatorHelper;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;
import com.tgs.services.fms.manager.TripPriceEngine;
import com.tgs.services.fms.mapper.PriceInfoToProcessedPriceInfo;
import com.tgs.services.fms.mapper.SegmentPriceInfoToTripPriceInfo;
import com.tgs.services.fms.mapper.TripInfoToProccessedTripInfoMapper;
import com.tgs.services.fms.mapper.TripPriceInfoUserMapper;
import com.tgs.services.fms.ruleengine.FlightAPIURLRuleCriteria;
import com.tgs.services.fms.ruleengine.FlightBasicAirConfigOutput;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AirUtils {

	public static void setProcessedTripInfo(TripInfo tripInfo, AirSearchQuery searchQuery, User bookingUser) {
		for (TripInfo tempInfo : splitTripInfo(tripInfo, false)) {
			if (tripInfo.getProcessedTripInfo() != null) {
				tripInfo.getProcessedTripInfo().getConnectingTripInfo().add(getProcessedTripInfo(tempInfo));
			} else {
				tripInfo.setProcessedTripInfo(getProcessedTripInfo(tempInfo));
				tripInfo.getProcessedTripInfo().setPriceInfo(getBasicPriceInfoList(tripInfo, bookingUser));
			}
		}
		checkAndUpdateNearByAirport(tripInfo, searchQuery);
	}

	public static ProcessedTripInfo getProcessedTripInfo(TripInfo tripInfo) {
		return TripInfoToProccessedTripInfoMapper.builder().tripInfo(tripInfo).build().convert();
	}

	public static void checkAndUpdateNearByAirport(TripInfo tripInfo, AirSearchQuery searchQuery) {
		ProcessedTripInfo processedTripInfo = tripInfo.getProcessedTripInfo();
		updateNearByAirport(searchQuery.getRouteInfos().get(0), processedTripInfo);
		if (CollectionUtils.isNotEmpty(processedTripInfo.getConnectingTripInfo())) {
			AtomicInteger routeIndex = new AtomicInteger(1);
			processedTripInfo.getConnectingTripInfo().forEach(processedTripInfo1 -> {
				RouteInfo routeInfo = searchQuery.getRouteInfos().get(routeIndex.get());
				updateNearByAirport(routeInfo, processedTripInfo1);
				routeIndex.getAndIncrement();
			});
		}
	}

	public static void updateNearByAirport(RouteInfo routeInfo, ProcessedTripInfo processedTripInfo) {
		String orgSourceAirport = routeInfo.originalFromCityOrAirport().getCode();
		String orgDestinationAirport = routeInfo.originalToCityOrAirport().getCode();
		if (!orgSourceAirport.equals(processedTripInfo.getDepartureAirport())) {
			processedTripInfo.setOrgDepartureAirport(orgSourceAirport);
		}
		if (!orgDestinationAirport.equals(processedTripInfo.getArrivalAirport())) {
			processedTripInfo.setOrgArrivalAirport(orgDestinationAirport);
		}
	}

	public static int daysDiff(TripInfo tripInfo) {
		List<TripInfo> trips = tripInfo.splitTripInfo(false);
		int days = 0;
		for (TripInfo trip : trips) {
			days = Math.max(days, ((int) ChronoUnit.DAYS.between(trip.getDepartureTime().toLocalDate(),
					trip.getArrivalTime().toLocalDate())));
		}
		return days;
	}

	/**
	 * Validates trips using <code>searchQuery</code>, to sort trips, and Allowed Combination Minutes fetched from
	 * <code>AirConfiguratorRuleType.COMBOMINUTES</code>
	 *
	 * @param tripInfos Trips to be validated
	 * @param searchQueries Search query to get the order of trips
	 */

	public static void validateTrips(List<TripInfo> tripInfos, List<AirSearchQuery> searchQueries, User bookingUser) {
		AirSearchQuery searchQuery = combineSearchQuery(searchQueries);

		if (CollectionUtils.isEmpty(tripInfos)) {
			return;
		}

		if (searchQuery.isIntl() && (searchQuery.isReturn() || searchQuery.isMultiCity())) {
			tripInfos = AirUtils.splitTripInfo(tripInfos.get(0), false);
		}
		long allowedMinutes =
				getCombinationAllowedMinutes(tripInfos.get(0).getSupplierInfo().getSourceId(), bookingUser);

		/**
		 * If tripInfos are not in order, then it is required to re-arrange them in the order of routes that were
		 * searched.<br>
		 * For example, priceIds could be swapped resulting in re-ordering of trips which will affect trip-time
		 * validation.
		 */
		tripInfos = syncTripInfosWithRouteInfos(tripInfos, searchQuery.getRouteInfos());

		for (int i = 0; i < tripInfos.size() - 1; i++) {
			long minutes = tripInfos.get(i).getArrivalTime().until(tripInfos.get(i + 1).getDepartureTime(),
					ChronoUnit.MINUTES);
			if (minutes < allowedMinutes) {
				/**
				 * "\n\t" will mark the beginning of tripInfo making log easily readable.
				 *
				 * <pre>
				 * For example,<br>
				 * |    tripInfo=(.... <br>
				 * |departTime=....... <br>
				 * |totalPriceList=..........)<br>
				 * |    tripInfo=(.... <br>
				 * |departTime=....... <br>
				 * |totalPriceList=..........)
				 * </pre>
				 */
				log.error("Invalid trip timings for trips:\n\t{} \n\t{}", tripInfos.get(i), tripInfos.get(i + 1));
				throw new CustomGeneralException(SystemError.INVALID_TRIP_TIMINGS, "A flight should arrive at least "
						+ LocalTime.MIN.plusMinutes(allowedMinutes) + " hrs before upcoming flight's departure.");
			}
		}

		Set<String> fareTypes = new HashSet<>();
		tripInfos.forEach(tripInfo -> {
			fareTypes.addAll(tripInfo.getFareType(0));
		});

		if (searchQuery.isDomesticReturn() && fareTypes.contains(FareType.SPECIAL_RETURN.getName())
				&& fareTypes.size() > 1) {
			throw new CustomGeneralException(SystemError.INVALID_SPECIAL_RETURN_SELECTION);
		}
	}

	/**
	 * Syncing: Trips in <code>tripInfos</code> are re-arranged using the order of routes (origin, destination) in
	 * <code>routeInfos</code>, and returned.<br>
	 * Syncing is done only for domestic trips because it doesn't apply to international trips.
	 *
	 * @param tripInfos List of trips to be synced
	 * @param routeInfos List of routes to be used for sync
	 * @return TripInfos synced with routeInfos
	 */
	public static List<TripInfo> syncTripInfosWithRouteInfos(List<TripInfo> tripInfos, List<RouteInfo> routeInfos) {
		if (tripInfos.size() == 1 || !AirUtils.isDomesticTrip(tripInfos.get(0))) {
			return tripInfos;
		}

		List<TripInfo> sortedTrips = new ArrayList<>(), duplicateTripList = new ArrayList<>(tripInfos);
		for (RouteInfo route : routeInfos) {
			for (int i = 0; i < duplicateTripList.size(); i++) {
				TripInfo trip = duplicateTripList.get(i);
				if (trip.getDepartureAirport().equals(route.getFromCityOrAirport())
						&& trip.getArrivalAirport().equals(route.getToCityOrAirport())) {
					sortedTrips.add(trip);
					duplicateTripList.remove(i--);
				}
			}
		}
		return sortedTrips;
	}

	public static List<ProcessedPriceInfo> getBasicPriceInfoList(TripInfo tripInfo, User bookingUser) {
		List<ProcessedPriceInfo> basicPriceInfoList = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(tripInfo.getTripPriceInfos())) {
			for (PriceInfo tripPriceInfo : tripInfo.getTripPriceInfos()) {
				ProcessedPriceInfo basicPriceInfo = PriceInfoToProcessedPriceInfo.builder().priceInfo(tripPriceInfo)
						.paxInfo(tripInfo.getPaxInfo()).build().convert();
				basicPriceInfo.setIsChangeClassAllowed(
						AirUtils.getIsChangeClassAllowed(tripInfo, tripPriceInfo, bookingUser));
				basicPriceInfoList.add(basicPriceInfo);
			}
		}
		Collections.sort(basicPriceInfoList, new Comparator<ProcessedPriceInfo>() {

			@Override
			public int compare(ProcessedPriceInfo price1, ProcessedPriceInfo price2) {
				if (price1.getTotalPrice() > price2.getTotalPrice()) {
					return 1;
				} else if (price1.getTotalPrice() < price2.getTotalPrice()) {
					return -1;
				}
				return 0;
			}
		});
		return basicPriceInfoList;
	}

	public static double getTotalFare(Map<PaxType, Integer> paxTypeMap, PriceInfo priceInfo,
			FareComponent targetComponent) {
		double totalPrice = 0;
		for (PaxType paxType : paxTypeMap.keySet()) {
			if (priceInfo.getFareDetail(paxType) != null) {
				totalPrice += priceInfo.getFareDetail(paxType).getFareComponents().getOrDefault(targetComponent,
						Objects.nonNull(priceInfo.getFareDetail(paxType).getAddlFareComponents()) ? priceInfo
								.getFareDetail(paxType).getAddlFareComponents()
								.getOrDefault(FareComponent.TAF, new HashMap<>()).getOrDefault(targetComponent, 0.0)
								: 0.0)
						* paxTypeMap.get(paxType);
			}
		}
		return totalPrice;
	}

	public static double getTotalFare(Map<PaxType, Integer> paxTypeMap, PriceInfo priceInfo,
			List<FareComponent> fareComponents) {
		double totalPrice = 0;
		for (FareComponent fc : fareComponents) {
			totalPrice += getTotalFare(paxTypeMap, priceInfo, fc);
		}
		return totalPrice;
	}

	public static List<PriceInfo> getPriceInfos(List<TripInfo> tripInfos) {
		return BaseUtils.getPriceInfos(tripInfos);
	}

	public static void unsetRedudantPriceInfoBasedUponId(TripInfo tripInfo, String id) {
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			List<PriceInfo> filteredPriceList = segmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
				return StringUtils.isNotEmpty(priceInfo.getId()) && priceInfo.getId().equals(id);
			}).collect(Collectors.toList());
			segmentInfo.setPriceInfoList(filteredPriceList);
		});
		tripInfo.setTripPriceInfos(null);
	}

	public static long getTimeOutInMilliSecond(User user, SupplierBasicInfo supplierInfo, AirSearchQuery searchQuery) {
		if (searchQuery.getTimeout() != null) {
			return searchQuery.getTimeout() * 1000;
		}
		long timeOut = getTimeOutInSecond(user);
		AirSourceConfigurationOutput airSourceOutput = getAirSourceConfiguration(searchQuery, supplierInfo, user);
		if (airSourceOutput != null && Objects.nonNull(airSourceOutput.getThreadTimeout())) {
			timeOut = Long.valueOf(airSourceOutput.getThreadTimeout());
		}
		return timeOut * 1000;
	}

	public static long getTimeOutInSecond(User user) {
		AirGeneralPurposeOutput generalPurposeOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		if (generalPurposeOutput != null && generalPurposeOutput.getThreadTimeout() != null) {
			return generalPurposeOutput.getThreadTimeout();
		}
		return 15;
	}

	public static Map<SSRType, List<? extends SSRInformation>> getPreSSR(SupplierBasicInfo basicInfo, String airline,
			User user) {
		Map<SSRType, List<? extends SSRInformation>> ssrMap = new HashMap<>();
		AirSSRInfoOutput ssrOutput = AirUtils.getAirSSRInfos(basicInfo, airline, user);
		if (ssrOutput != null && (CollectionUtils.isNotEmpty(ssrOutput.getBaggageSsrList())
				|| CollectionUtils.isNotEmpty(ssrOutput.getMealSsrList())
				|| CollectionUtils.isNotEmpty(ssrOutput.getExtraSsrList())
				|| CollectionUtils.isNotEmpty(ssrOutput.getSeatSsrList()))) {
			ssrMap.put(SSRType.SEAT, ssrOutput.getSeatSsrList());
			ssrMap.put(SSRType.MEAL, ssrOutput.getMealSsrList());
			ssrMap.put(SSRType.BAGGAGE, ssrOutput.getBaggageSsrList());
			ssrMap.put(SSRType.EXTRASERVICES, ssrOutput.getExtraSsrList());
			return ssrMap;
		}
		return null;
	}

	public static Map<SSRType, List<? extends SSRInformation>> getPreSSR(FlightBasicFact flightFact) {
		Map<SSRType, List<? extends SSRInformation>> ssrMap = new HashMap<>();
		AirSSRInfoOutput ssrOutput = AirUtils.getAirSSRInfos(flightFact);
		if (ssrOutput != null && (CollectionUtils.isNotEmpty(ssrOutput.getBaggageSsrList())
				|| CollectionUtils.isNotEmpty(ssrOutput.getMealSsrList())
				|| CollectionUtils.isNotEmpty(ssrOutput.getExtraSsrList())
				|| CollectionUtils.isNotEmpty(ssrOutput.getSeatSsrList()))) {
			ssrMap.put(SSRType.SEAT, ssrOutput.getSeatSsrList());
			ssrMap.put(SSRType.MEAL, ssrOutput.getMealSsrList());
			ssrMap.put(SSRType.BAGGAGE, ssrOutput.getBaggageSsrList());
			ssrMap.put(SSRType.EXTRASERVICES, ssrOutput.getExtraSsrList());
			return ssrMap;
		}
		return null;
	}

	public static SSRInformation getSSRInfo(List<? extends SSRInformation> ssrList, String ssrCode) {
		if (CollectionUtils.isEmpty(ssrList)) {
			return null;
		}
		return ssrList.stream().filter(ssrInfo -> ssrInfo.getCode().toUpperCase().equals(ssrCode)).findFirst()
				.orElse(null);
	}

	/*
	 * @return - total paxCount
	 */
	public static int getPaxCount(AirSearchQuery searchQuery, boolean isInfantRequired) {
		AtomicInteger atomicInteger = new AtomicInteger(0);
		searchQuery.getPaxInfo().forEach((paxType, count) -> {
			if (count > 0 && isInfantRequired) {
				atomicInteger.getAndAdd(count);
			} else if (count > 0 && !paxType.equals(PaxType.INFANT)) {
				atomicInteger.getAndAdd(count);
			}
		});
		return atomicInteger.intValue();
	}

	/*
	 * @return - total paxCount
	 */
	public static int getPaxCountFromTravellerInfo(List<FlightTravellerInfo> travellerInfoList,
			boolean isInfantRequired) {
		AtomicInteger atomicInteger = new AtomicInteger(0);
		travellerInfoList.forEach(travellerInfo -> {
			if ((travellerInfo.getPaxType().equals(PaxType.INFANT) && isInfantRequired)
					|| !travellerInfo.getPaxType().equals(PaxType.INFANT)) {
				atomicInteger.getAndIncrement();
			}
		});
		return atomicInteger.intValue();
	}

	/*
	 * @return - total paxCount This assumes there are same num of pxns in all segments of this trip. It fetches pxn
	 * Count from 1st segment
	 */
	public static int getPaxCountFromTravellerInfo(TripInfo tripInfo, boolean isInfantRequired) {
		List<FlightTravellerInfo> travellerInfoList =
				tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
		return getPaxCountFromTravellerInfo(travellerInfoList, isInfantRequired);
	}

	public static int getPaxCountFromPaxInfo(Map<PaxType, Integer> paxInfo, boolean isInfantRequired) {
		int paxCount = 0;
		for (Entry<PaxType, Integer> entryMap : paxInfo.entrySet()) {
			if (!entryMap.getKey().equals(PaxType.INFANT) || isInfantRequired) {
				paxCount += entryMap.getValue();
			}
		}
		return paxCount;
	}

	/*
	 * @return - total paxCount of Particular Pax Type
	 */
	public static int getParticularPaxCount(AirSearchQuery searchQuery, PaxType pax) {
		AtomicInteger atomicInteger = new AtomicInteger(0);
		searchQuery.getPaxInfo().forEach((paxType, count) -> {
			if (count > 0 && pax.equals(paxType)) {
				atomicInteger.getAndAdd(count);
			}
		});
		return atomicInteger.intValue();
	}

	public static String getTerminalInfo(String terminal) {
		if (StringUtils.isBlank(terminal)) {
			return null;
		}
		return terminal.toUpperCase().contains("TERMINAL") ? terminal : String.join(" ", "Terminal", terminal);
	}

	public static TripInfo unsetRedudantPrice(TripInfo newTrip, TripInfo oldTrip) {
		for (int i = 0; i < newTrip.getSegmentInfos().size(); i++) {
			SegmentInfo newSegmentInfo = newTrip.getSegmentInfos().get(i);
			SegmentInfo oldSegmentInfo = oldTrip.getSegmentInfos().get(i);
			String fareIdentifier = oldSegmentInfo.getPriceInfo(0).getFareType();
			CabinClass cabinClass = oldSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass();
			Integer sourceId = oldSegmentInfo.getPriceInfo(0).getSourceId();
			List<PriceInfo> filteredPriceList = newSegmentInfo.getPriceInfoList().stream().filter(priceInfo -> {
				return (StringUtils.isNotEmpty(priceInfo.getFareType())
						&& priceInfo.getFareIdentifier().getName().equals(fareIdentifier))
						&& (priceInfo.getFareDetail(PaxType.ADULT).getCabinClass().equals(cabinClass)
								&& priceInfo.getSupplierBasicInfo().getSourceId() == sourceId);
			}).collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(filteredPriceList) && matchSegmentInfo(newSegmentInfo, oldSegmentInfo)) {
				newSegmentInfo.setPriceInfoList(filteredPriceList);
			}
		}
		return newTrip;
	}

	public static TripInfo filterTripFromAirSearchResult(AirSearchResult searchResult, TripInfo oldTrip) {
		if (searchResult == null || MapUtils.isEmpty(searchResult.getTripInfos())) {
			return null;
		}
		for (Entry<String, List<TripInfo>> entry : searchResult.getTripInfos().entrySet()) {
			for (TripInfo newTripInfo : entry.getValue()) {
				if (newTripInfo.getSegmentInfos().size() == oldTrip.getSegmentInfos().size()) {
					int i = 0;
					for (; i < newTripInfo.getSegmentInfos().size(); i++) {
						if (!matchSegmentInfo(newTripInfo.getSegmentInfos().get(i), oldTrip.getSegmentInfos().get(i))) {
							break;
						}
					}
					if (i == newTripInfo.getSegmentInfos().size()) {
						return unsetRedudantPrice(newTripInfo, oldTrip);
					}
				}
			}
		}
		return null;
	}

	public static Double getTaxFare(FareDetail fareDetail) {
		Double taxFare = 0.0;
		if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
			for (Map.Entry<FareComponent, Double> entry : fareDetail.getFareComponents().entrySet()) {
				if (entry.getKey().airlineComponent() && !entry.getKey().equals(FareComponent.BF)) {
					taxFare += entry.getValue();
				}
			}
		}
		return taxFare;
	}

	public static boolean matchSegmentInfo(SegmentInfo segment1, SegmentInfo segment2) {
		return segment1.getFlightDesignator().getFlightNumber().equals(segment2.getFlightDesignator().getFlightNumber())
				&& segment1.getArrivalAirportInfo().getCode().equals(segment2.getArrivalAirportInfo().getCode())
				&& segment1.getDepartAirportInfo().getCode().equals(segment2.getDepartAirportInfo().getCode())
				// && segment1.getIsReturnSegment().equals(segment2.getIsReturnSegment())
				&& segment1.getDepartTime().isEqual(segment2.getDepartTime())
				&& segment1.getArrivalTime().equals(segment2.getArrivalTime())
				&& segment1.getStops() == segment2.getStops();
	}

	public static List<FlightTravellerInfo> getParticularPaxTravellerInfo(List<FlightTravellerInfo> travellerInfos,
			PaxType paxType) {
		return BaseUtils.getParticularPaxTravellerInfo(travellerInfos, paxType);
	}

	public static int getParticularPaxCount(TripInfo tripInfo, PaxType paxType) {
		List<FlightTravellerInfo> travellerInfos =
				tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
		return (int) travellerInfos.stream().filter(t -> t.getPaxType().equals(paxType)).count();
	}

	public static FlightTravellerInfo getFirstAdultTravellerInfo(TripInfo tripInfo) {
		return getParticularPaxTravellerInfo(
				tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), PaxType.ADULT).get(0);
	}

	public static Optional<FlightTravellerInfo> findTravellerByNameDob(TripInfo tripInfo, String firstName,
			String lastName, LocalDate dob) {

		List<FlightTravellerInfo> pxns = getParticularPaxTravellerInfo(
				tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), PaxType.ADULT);
		pxns.addAll(getParticularPaxTravellerInfo(
				tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo(), PaxType.CHILD));
		return pxns.stream()
				.filter(px -> StringUtils.equalsIgnoreCase(px.getFirstName(), firstName)
						&& StringUtils.equalsIgnoreCase(px.getLastName(), lastName)
						&& (dob == null || dob.equals(px.getDob())))
				.findFirst();
	}

	public static List<TripInfo> buildCombination(List<TripInfo> onwardTrips, List<TripInfo> returnTrips, int sourceId,
			User bookingUser) {

		if (CollectionUtils.isEmpty(onwardTrips) || CollectionUtils.isEmpty(returnTrips)) {
			return null;
		}
		List<TripInfo> result = new ArrayList<TripInfo>();
		for (TripInfo onwardTrip : onwardTrips) {
			for (TripInfo returnTrip : returnTrips) {
				LocalDateTime onwardArrivalTime = onwardTrip.getArrivalTime();
				LocalDateTime returnDepartureTime = returnTrip.getDepartureTime();
				if (Duration.between(onwardArrivalTime, returnDepartureTime)
						.toMinutes() > getCombinationAllowedMinutes(sourceId, bookingUser)) {
					TripInfo copyOnwardTrip = new GsonMapper<>(onwardTrip, TripInfo.class).convert();
					TripInfo copyReturnTrip = new GsonMapper<>(returnTrip, TripInfo.class).convert();
					TripPriceEngine.filterComboPriceInfos(copyOnwardTrip, copyReturnTrip);
					if (copyOnwardTrip.isPriceInfosNotEmpty() && copyReturnTrip.isPriceInfosNotEmpty()) {
						copyReturnTrip.getSegmentInfos().get(0).setIsReturnSegment(true);
						copyOnwardTrip.getSegmentInfos().addAll(copyReturnTrip.getSegmentInfos());
						result.add(copyOnwardTrip);
					}
				}
			}
		}
		return result;
	}

	/**
	 * @param sourceId
	 * @return Allowed combination minutes between two flights. Returns 180 minutes,by default.
	 */
	public static Long getCombinationAllowedMinutes(int sourceId, User bookinguser) {
		AirSourceConfigurationOutput airSourceOutput = AirUtils.getAirSourceConfiguration(null, sourceId, bookinguser);
		if (airSourceOutput != null && Objects.nonNull(airSourceOutput.getComboMinutes())) {
			return Long.valueOf(airSourceOutput.getComboMinutes());
		}
		return 180L;
	}

	/**
	 * @return true or false - if any SSR Added to atleast one Pax
	 */
	public static boolean isSSRAddedInTrip(List<SegmentInfo> segmentInfos) {
		boolean isSSRAdded = false;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (Objects.nonNull(segmentInfo.getBookingRelatedInfo())
					&& CollectionUtils.isNotEmpty(segmentInfo.getBookingRelatedInfo().getTravellerInfo())) {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					if (Objects.nonNull(travellerInfo.getSsrBaggageInfo())
							|| Objects.nonNull(travellerInfo.getSsrMealInfo())) {
						isSSRAdded = true;
						break;
					}
				}
			}
		}
		return isSSRAdded;
	}

	public static String getSearchTypeKey(AirSearchQuery airSearchQuery) {
		return String.join("_", airSearchQuery.getSearchType().name(),
				(airSearchQuery.getIsDomestic() ? "DOM" : "INTL"));
	}

	public static List<Integer[]> getSearchGroupingList(AirSearchQuery searchQuery, User user) {
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(searchQuery);
		BaseUtils.createFactOnUser(flightFact, user);
		AirSearchGrpOutput ruleOutput =
				AirConfiguratorHelper.getAirConfigRule(flightFact, AirConfiguratorRuleType.SEARCHGRPNG);
		if (ruleOutput != null) {
			Map<String, List<Integer[]>> groupMap = ruleOutput.getValue();
			return groupMap.get(getSearchTypeKey(searchQuery));
		}
		return null;
	}

	public static List<AirPath> getCustomPaths(AirSearchQuery airSearchQuery, User user) {
		FlightBasicFact flightFact = FlightBasicFact.createFact().generateFact(airSearchQuery);
		BaseUtils.createFactOnUser(flightFact, user);
		AirCustomPathOutput ruleOutput =
				AirConfiguratorHelper.getAirConfigRule(flightFact, AirConfiguratorRuleType.CUSTOM_PATH);
		if (ruleOutput != null) {
			Map<String, List<AirPath>> paths = ruleOutput.getValue();
			return paths.get(airSearchQuery.getRouteInfos().get(0).getFromCityOrAirport().getCode()
					+ airSearchQuery.getRouteInfos().get(0).getToCityOrAirport().getCode());
		}
		return null;
	}

	public static String getNearByAirport(String code, Integer sourceId, AirSearchQuery searchQuery, User bookingUser) {

		AirSourceConfigurationOutput airSourceOutput =
				AirUtils.getAirSourceConfiguration(searchQuery, sourceId, bookingUser);
		if ((airSourceOutput != null) && Objects.nonNull(airSourceOutput.getNearByAirports())) {

			List<KeyValue> pathList = airSourceOutput.getNearByAirports();
			if (CollectionUtils.isNotEmpty(pathList)) {
				for (KeyValue path : pathList) {
					if (path.getKey().contains(code)) {
						return path.getValue();
					}
				}
			}
		}
		return null;
	}

	public static Double getTotalFare(TripInfo tripInfo) {
		double totalFare = 0;
		for (int segmentIndex = 0; segmentIndex < tripInfo.getSegmentInfos().size(); segmentIndex++) {
			PriceInfo priceInfo = tripInfo.getSegmentInfos().get(segmentIndex).getPriceInfo(0);
			totalFare += getTotalFare(tripInfo.getPaxInfo(), priceInfo, FareComponent.TF);
		}
		return totalFare;
	}

	public static double getTotalFareComponentAmount(TripInfo tripInfo, FareComponent targetFareComponent) {
		double totalFare = 0;
		for (SegmentInfo segment : tripInfo.getSegmentInfos()) {
			if (segment.getBookingRelatedInfo() != null) {
				List<FlightTravellerInfo> travellerInfos = segment.getBookingRelatedInfo().getTravellerInfo();
				for (FlightTravellerInfo travellerInfo : travellerInfos) {
					totalFare +=
							travellerInfo.getFareDetail().getFareComponents().getOrDefault(targetFareComponent, 0.0);
				}
			} else {
				PriceInfo priceInfo = segment.getPriceInfo(0);
				totalFare += getTotalFare(tripInfo.getPaxInfo(), priceInfo, targetFareComponent);
			}
		}
		return totalFare;
	}

	public static boolean logTripWiseFareChange(TripInfo oldTrip, TripInfo newTrip, String bookingId) {
		if (oldTrip == null || newTrip == null) {
			throw new CustomGeneralException(SystemError.FLIGHT_SOLD_OUT);
		}
		boolean foundFareDiff = false;
		double oldFare = BigDecimal.valueOf(getTotalFare(oldTrip)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		double newFare = BigDecimal.valueOf(getTotalFare(newTrip)).setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
		if (oldFare != newFare) {
			log.info("Found fare difference for bookingId {} , Old Fare {}, New Fare {} for tripInfo {}", bookingId,
					oldFare, newFare, oldTrip);
			foundFareDiff = true;
		}
		return foundFareDiff;
	}

	public static FareChangeAlert getFareChangeAlert(List<TripInfo> oldTrips, List<TripInfo> newTrips, String bookingId,
			boolean sendAlert) {
		if (oldTrips.size() != newTrips.size()) {
			throw new CustomGeneralException(SystemError.FLIGHT_SOLD_OUT);
		}
		double oldFare = 0;
		double newFare = 0;
		double discount = 0;
		for (int i = 0; i < oldTrips.size(); i++) {
			oldFare += BigDecimal.valueOf(getTotalFare(oldTrips.get(i))).setScale(2, BigDecimal.ROUND_HALF_UP)
					.doubleValue() + AirUtils.getTotalFareComponentAmount(oldTrips.get(i), FareComponent.MU);
			newFare += BigDecimal.valueOf(getTotalFare(newTrips.get(i))).setScale(2, BigDecimal.ROUND_HALF_UP)
					.doubleValue() + AirUtils.getTotalFareComponentAmount(newTrips.get(i), FareComponent.MU);
			if (UserRole.corporate(SystemContextHolder.getContextData().getUser().getRole())) {
				discount += BaseUtils.getGrossCommission(newTrips.get(i), 0, null, true);
			}
		}
		log.info("Old Fare {}, New Fare {} , for bookingId {}", oldFare, newFare, bookingId);
		if (oldFare != newFare) {
			log.info("Found fare difference for bookingId {} , Old Fare {}, New Fare {} ", bookingId, oldFare, newFare);
			return FareChangeAlert.builder().newFare(newFare).oldFare(oldFare).discount(discount)
					.type(AlertType.FAREALERT.name()).build();
		}
		/**
		 * This is purely from testing standpoint of view to replicate fare jump alert scenario
		 */
		if (sendAlert) {
			return FareChangeAlert.builder().newFare(newFare + newFare * 0.01).oldFare(oldFare).discount(discount)
					.type(AlertType.FAREALERT.name()).build();
		}
		return null;
	}

	/**
	 * It fetches <i>booking fee</i> details (in the form of <code>AirClientFeeOutput</code> instance) from booking-fee
	 * rule and returns the same. If updating <i>booking fee</i> fails, these details can be used.
	 *
	 * @param searchQuery Search Query to generate facts
	 * @param tripInfo
	 * @return instance with <i>booking fee</i> details
	 */
	public static AirClientFeeOutput getClientFeeOutput(AirSearchQuery searchQuery, PaxType paxType,
			SegmentInfo segmentInfo, User user, TripInfo tripInfo, Integer priceIndex) {
		SupplierBasicInfo basicInfo = segmentInfo.getPriceInfo(priceIndex).getSupplierBasicInfo();
		FlightBasicFact flightBasicFact = FlightBasicFact.builder().paxType(paxType).build().generateFact(searchQuery)
				.generateFact(segmentInfo).generateFact(basicInfo);
		flightBasicFact.setRouteInfos(tripInfo.getRouteInfos());
		BaseUtils.createFactOnUser(flightBasicFact, user);
		return getClientFeeOutput(flightBasicFact);
	}


	public static AirClientFeeOutput getClientFeeOutput(FlightBasicFact flightBasicFact) {
		AirClientFeeOutput info =
				AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.CLIENTFEE);
		if (info != null && flightBasicFact != null) {
			if (info.getConditions() != null && BooleanUtils.isTrue(info.getConditions().isNotApplicableOnInfant())
					&& flightBasicFact.getPaxType() == PaxType.INFANT) {
				return null;
			}

			if (flightBasicFact.getSegmentNumber() == null || flightBasicFact.getSegmentNumber() == 0
					|| (info.getConditions() != null
							&& BooleanUtils.isTrue(info.getConditions().getIsApplicableOnAllSegment()))) {
				return info;
			}
		}
		return null;
	}

	/**
	 * It fetches list of cache-disabled suppliers from general-purpose rule, and checks if <code>sourceId</code> is
	 * contained in the list.
	 *
	 * @param searchQuery Search query to generate fact
	 * @param basicInfo Source-Id for which caching-ability is queried
	 * @return <code>true</code> if caching is allowed for given <code>sourceId</code>. Returns <code>false</code> if
	 *         nothing is found.
	 */
	public static boolean isCacheDisabled(AirSearchQuery searchQuery, SupplierBasicInfo basicInfo, User bookingUser) {
		AirGeneralPurposeOutput output =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (output != null && CollectionUtils.isNotEmpty(output.getCachedDisabledSources())) {
			return output.getCachedDisabledSources().contains(basicInfo.getSourceId());
		}
		return false;
	}

	/**
	 * It fetches list of airlines, which have disabled Frequent Flier, from SOURCECONFIG rule. if List is not there in
	 * sourceconfig rule then it will fetch from GNPURPOSE Rule
	 *
	 * @param tripInfo Trip info to generate fact
	 * @return List of airlines which have disabled Frequent Flier. Returns <i>empty</i> list if nothing is found.
	 */
	public static List<String> getFfDisabledAirlines(TripInfo tripInfo, User bookingUser) {
		AirSourceConfigurationOutput airSourceOutput =
				getAirSourceConfiguration(null, tripInfo.getSupplierInfo(), bookingUser);
		if (airSourceOutput != null && CollectionUtils.isNotEmpty(airSourceOutput.getFfDisabledAirlines())) {
			return airSourceOutput.getFfDisabledAirlines();
		}
		AirGeneralPurposeOutput output =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (output != null && CollectionUtils.isNotEmpty(output.getFfDisabledAirlines())) {
			return output.getFfDisabledAirlines();
		}
		return new ArrayList<>();
	}

	public static AirSearchQuery combineSearchQuery(List<AirSearchQuery> searchQueries) {
		/**
		 * Only in case domestic multiCity we will have separate Queries and will have to combine into Route List.
		 * Otherwise it will be single query.<br>
		 * Also, searchQueries need to be sorted according to their route's travel date, so they can be used for
		 * ordering tripsInfos wherever needed.
		 */
		AirSearchQuery combineQuery = null;
		if ((searchQueries.get(0).isMultiCity() && searchQueries.get(0).getIsDomestic())
				|| (searchQueries.get(0).getRouteInfos().size() == 1)) {
			searchQueries.sort(new Comparator<AirSearchQuery>() {
				@Override
				public int compare(AirSearchQuery o1, AirSearchQuery o2) {
					return o1.getRouteInfos().get(0).getTravelDate()
							.compareTo(o2.getRouteInfos().get(0).getTravelDate());
				}
			});
			combineQuery = new GsonMapper<>(searchQueries.get(0), AirSearchQuery.class).convert();
			combineQuery.setRouteInfos(new ArrayList<>());
			for (int i = 0; i < searchQueries.size(); i++) {
				combineQuery.getRouteInfos().add(searchQueries.get(i).getRouteInfos().get(0));
			}
			combineQuery.isSetSearchType();
		} else {
			combineQuery = new GsonMapper<>(searchQueries.get(0), AirSearchQuery.class).convert();
			return combineQuery;
		}

		return combineQuery;
	}

	public static List<AirSearchQuery> splitSearchQueryRoutesIntoSearchQueryList(AirSearchQuery airSearchQuery) {
		List<AirSearchQuery> searchQueryList = new ArrayList<>();
		for (int i = 0; i < airSearchQuery.getRouteInfos().size(); i++) {
			AirSearchQuery copyQuery = new GsonMapper<>(airSearchQuery, AirSearchQuery.class).convert();
			copyQuery.setRouteInfos(new ArrayList<>());
			copyQuery.getRouteInfos().add(airSearchQuery.getRouteInfos().get(i));
			searchQueryList.add(copyQuery);
		}
		return searchQueryList;
	}

	public static List<TripInfo> splitTripInfo(TripInfo tripInfo, boolean shouldConsiderManualCombination) {
		return tripInfo.splitTripInfo(shouldConsiderManualCombination);
	}

	public static String getAirTypeCode(boolean isDomestic) {
		return isDomestic ? AirType.DOMESTIC.getCode() : AirType.INTERNATIONAL.getCode();
	}

	public static List<AirSearchQuery> getSearchQueryFromTripInfos(List<TripInfo> tripInfos) {
		List<AirSearchQuery> searchQueries = new ArrayList<>();
		tripInfos.forEach(tripInfo -> {
			AirSearchQuery sQuery = getSearchQueryFromTripInfo(tripInfo);
			searchQueries.add(sQuery);
		});
		return searchQueries;
	}

	public static Map<PaxType, Integer> getPaxInfo(TripInfo tripInfo) {
		return BaseUtils.getPaxInfo(tripInfo);
	}

	public static OrderType getOrderType(AirSearchQuery searchQuery) {
		return OrderType.AIR;
	}

	public static long getConnectingTime(SegmentInfo segmentInfo1, SegmentInfo segmentInfo2) {
		LocalDateTime nextDeparture = segmentInfo2.getDepartTime();
		long connectingTime = segmentInfo1.getArrivalTime().until(nextDeparture, ChronoUnit.MINUTES);
		return connectingTime;
	}

	/*
	 * @param tripInfo Trip info to generate fact
	 *
	 * @param searchQuery
	 *
	 * @param previousValue - Previous Trip Value
	 *
	 * @return true or false - if dob required or not
	 */
	public static AirPassportConditions getPassportConditions(TripInfo tripInfo, AirPassportConditions previousValue,
			User bookingUser, AtomicBoolean isPANRequired) {
		AirPassportConditions passportConditions = null;
		/*
		 * First priority for trip as trip will have the conditions received from suppliers. If trip doesn't have
		 * condition then fall back to Configurator.
		 */
		if (tripInfo.getBookingConditions() != null
				&& tripInfo.getBookingConditions().getPassportConditions() != null) {
			return updatePassportConditions(tripInfo.getBookingConditions().getPassportConditions(), previousValue,
					isPANRequired);
		}
		FlightBasicFact flightBasicFact = FlightBasicFact.builder().build();
		flightBasicFact.generateFact(tripInfo);
		flightBasicFact.setAirType(AirUtils.getAirType(tripInfo));
		BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
		passportConditions = AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.PASSPORT);
		if (Objects.nonNull(passportConditions)) {
			return updatePassportConditions(passportConditions, previousValue, isPANRequired);
		} else if (BooleanUtils.isTrue(isPANRequired.get())) {
			previousValue.setPassportEligible(true);
		}
		return previousValue;
	}

	private static AirPassportConditions updatePassportConditions(AirPassportConditions passportConditions,
			AirPassportConditions previousValue, AtomicBoolean isPANRequired) {
		passportConditions
				.setPassportMandatory(passportConditions.isPassportMandatory() || previousValue.isPassportMandatory());
		passportConditions.setPassportExpiryDate(
				passportConditions.isPassportExpiryDate() || previousValue.isPassportExpiryDate());
		passportConditions
				.setPassportIssueDate(passportConditions.isPassportIssueDate() || previousValue.isPassportIssueDate());
		// If pan is mandatory then we need to verify PASSPORT nationality.
		passportConditions.setPassportEligible(
				passportConditions.isPassportEligible() || previousValue.isPassportEligible() || isPANRequired.get());
		passportConditions.setDobEligible(passportConditions.isDobEligible() || previousValue.isDobEligible());
		return passportConditions;
	}

	public static DobOutput getDobConditions(TripInfo tripInfo, DobOutput previousValue, User bookingUser) {
		DobOutput dobConditions = null;
		/*
		 * First priority for trip as trip will have the conditions received from suppliers. If trip doesn't have
		 * condition then fall back to Configurator.
		 */
		if (tripInfo.getBookingConditions() != null && tripInfo.getBookingConditions().getDobOutput() != null) {
			dobConditions = updateDobConditions(tripInfo.getBookingConditions().getDobOutput(), previousValue);
			return dobConditions;
		}
		FlightBasicFact flightBasicFact = FlightBasicFact.builder().build();
		flightBasicFact.generateFact(tripInfo);
		flightBasicFact.setAirType(AirUtils.getAirType(tripInfo));
		BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
		dobConditions = AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.DOB);
		if (Objects.isNull(dobConditions)) {
			dobConditions = DobOutput.builder().build();
		}
		updateDobConditions(dobConditions, previousValue);
		return dobConditions;
	}

	private static DobOutput updateDobConditions(DobOutput dobConditions, DobOutput previousValue) {
		dobConditions.setAdultDobRequired(dobConditions.isAdultDobRequired() || previousValue.isAdultDobRequired());
		dobConditions.setChildDobRequired(dobConditions.isChildDobRequired() || previousValue.isChildDobRequired());
		dobConditions.setInfantDobRequired(dobConditions.isInfantDobRequired() || previousValue.isInfantDobRequired());
		return dobConditions;
	}

	/**
	 * isDefault -> in Case Same Supplier For Domestic Should be false & Intl should be true<br>
	 * 1.In Case if any Supplier API Attributes Specifies Hold - them handled from Segment#SegmentMiscInfo<br>
	 * 2.If Not handled from SupplierRule then will be checking if any rule specified on airconfigurator
	 *
	 * @param tripInfo :Trip info to generate fact
	 * @param searchQuery
	 * @param previousValue : Previous Trip Block Allowed or Not
	 * @return true or false - if hold Supports
	 */
	public static boolean isBlockingAllowed(TripInfo tripInfo, AirSearchQuery searchQuery,
			AtomicBoolean previousValue) {
		AtomicBoolean isHoldAllowed = new AtomicBoolean();
		if (searchQuery.isPNRCreditSearch() || UserUtils.isB2CUser(SystemContextHolder.getContextData().getUser())) {
			return false;
		}
		if (isAllowedFromSupplier(tripInfo, previousValue)) {
			isHoldAllowed.set(isBAFromConfig(tripInfo));
		}
		return isHoldAllowed.get() && previousValue.get();
	}

	public static boolean isBAFromConfig(TripInfo tripInfo) {
		AirSourceConfigurationOutput airSourceOutput =
				getAirSourceConfigOnTripInfo(tripInfo, SystemContextHolder.getContextData().getUser());
		LocalDateTime depatureTime = tripInfo.getDepartureTime();
		if (airSourceOutput != null && BooleanUtils.isNotFalse(airSourceOutput.getIsSupplierBlockAllowed())) {
			if (airSourceOutput.getBlockMinutes() != null && Duration.between(LocalDateTime.now(), depatureTime)
					.toMinutes() < airSourceOutput.getBlockMinutes()) {
				return false;
			}
			return true;
		}
		return false;
	}

	public static boolean isAllowedFromSupplier(TripInfo tripInfo, AtomicBoolean previousValue) {
		if (tripInfo.getBookingConditions() == null || tripInfo.getBookingConditions().getIsBlockingAllowed() == null) {
			return true;
		}
		return BooleanUtils.isTrue(tripInfo.getBookingConditions().getIsBlockingAllowed()) && previousValue.get();
	}

	public static String getSupplierPromotionCode(IFact fact) {
		FlightBasicAirConfigOutput configuratorRule =
				AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.APIPROMOTION);
		if (Objects.nonNull(configuratorRule)) {
			return configuratorRule.getValue();
		}
		return null;
	}

	public static SegmentInfo findSegment(List<SegmentInfo> segmentInfos, String departCode, String arrCode,
			LocalDate depDate, String flightNum) {

		for (SegmentInfo segmentInfo : segmentInfos) {
			if (segmentInfo.getDepartAirportInfo().getCode().equals(departCode)
					&& segmentInfo.getArrivalAirportInfo().getCode().equals(arrCode)
					&& segmentInfo.getDepartTime().toLocalDate().isEqual(depDate)
					&& segmentInfo.getFlightDesignator().getFlightNumber().equals(flightNum)) {
				return segmentInfo;
			}
		}
		return null;
	}

	public static void populateMissingParametersInAirSearchQuery(AirSearchQuery searchQuery) {
		try {
			searchQuery.getRouteInfos().forEach(routeInfo -> {
				// Might Getting Possibility only Airport Code
				routeInfo.setFromCityOrAirport(
						AirportHelper.getAirportInfo(routeInfo.getFromCityOrAirport().getCode().toUpperCase()));
				routeInfo.setToCityOrAirport(
						AirportHelper.getAirportInfo(routeInfo.getToCityOrAirport().getCode().toUpperCase()));
			});
		} catch (Exception e) {
			throw new CustomGeneralException(SystemError.INVALID_AIRPORT);
		}
		searchQuery.setOrigRouteInfoSize(searchQuery.getRouteInfos().size());

		if (searchQuery.getCabinClass() == null) {
			searchQuery.setCabinClass(CabinClass.ECONOMY);
		}

		ClientGeneralInfo clientInfo = ServiceCommunicatorHelper.getClientInfo();
		searchQuery.populateMissingParametersInAirSearchQuery(clientInfo.getCountry(),
				clientInfo.getClientDepentCountriesAir());
		if (CollectionUtils.isNotEmpty(searchQuery.getPreferredAirline())) {
			Set<AirlineInfo> airlineInfos = searchQuery.getPreferredAirline();
			searchQuery.setPreferredAirline(new HashSet<>());
			airlineInfos.forEach(airlineInfo -> {
				searchQuery.getPreferredAirline().add(AirlineHelper.getAirlineInfo(airlineInfo.getCode()));
			});
		}

		if (searchQuery.getSearchModifiers() != null
				&& BooleanUtils.isTrue(searchQuery.getSearchModifiers().getIsDirectFlight())
				&& BooleanUtils.isTrue(searchQuery.getSearchModifiers().getIsConnectingFlight())) {
			searchQuery.getSearchModifiers().setIsDirectFlight(null);
			searchQuery.getSearchModifiers().setIsConnectingFlight(null);
		}
	}

	// This requires bookingRelatedInfo in TripInfo to fill PaxInfo in search Query
	public static AirSearchQuery getSearchQueryFromTripInfo(TripInfo tripInfo) {
		return BaseUtils.getSearchQueryFromTripInfo(tripInfo);
	}

	public static List<SegmentInfo> getSegmentInfos(List<TripInfo> tripInfos) {
		return BookingUtils.getSegmentInfos(tripInfos);
	}

	public static String generatePaxKey(TravellerInfo travellerInfo) {
		return StringUtils.join(travellerInfo.getFirstName(), travellerInfo.getLastName());
	}

	public static int getSessionTime(SupplierBasicInfo basicInfo, AirSourceConfigurationOutput airSourceOutput,
			User bookingUser) {
		if (airSourceOutput == null) {
			FlightBasicFact flightBasicFact = FlightBasicFact.createFact();
			flightBasicFact.generateFact(basicInfo);
			BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
			airSourceOutput =
					AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.SOURCECONFIG);
		}
		if (airSourceOutput != null && Objects.nonNull(airSourceOutput.getSessionTime())) {
			return airSourceOutput.getSessionTime();
		} else {
			AirGeneralPurposeOutput output =
					AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
			if (output != null && output.getSessionTime() != null) {
				return output.getSessionTime();
			}
		}
		return 14;
	}

	public static int getSessionTime(TripInfo trip, AirSourceConfigurationOutput airSourceOutput, User bookingUser) {
		/*
		 * First priority for trip as trip will have the conditions received from suppliers. If trip doesn't have
		 * condition then fall back to Configurator.
		 */
		if (trip.getBookingConditions() != null && trip.getBookingConditions().getSessionTimeInSecond() != null) {
			// convert to minutes , cause supplier is already giving in seconds
			return trip.getBookingConditions().getSessionTimeInSecond() / 60;
		}
		return getSessionTime(trip.getSupplierInfo(), airSourceOutput, bookingUser);
	}

	public static AirSourceConfigurationOutput getAirSourceConfiguration(AirSearchQuery searchQuery,
			SupplierBasicInfo basicInfo, User bookingUser) {
		FlightBasicFact flightfact = FlightBasicFact.builder().build();
		if (searchQuery != null) {
			flightfact.generateFact(searchQuery);
		}
		if (basicInfo != null) {
			flightfact.setSourceId(basicInfo.getSourceId());
			flightfact.setSupplierId(basicInfo.getSupplierId());
		}
		BaseUtils.createFactOnUser(flightfact, bookingUser);
		return AirConfiguratorHelper.getAirConfigRule(flightfact, AirConfiguratorRuleType.SOURCECONFIG);
	}

	/**
	 * This method return properties associated with the sourceType which are fixed. For eg(flights count,included
	 * airlines,excluded airlines)
	 *
	 * @param sourceId
	 * @return
	 */
	public static AirSourceConfigurationOutput getAirSourceConfiguration(AirSearchQuery searchQuery, Integer sourceId,
			User bookingUser) {
		FlightBasicFact flightfact = FlightBasicFact.builder().build();
		if (searchQuery != null) {
			flightfact.generateFact(searchQuery);
		}
		if (sourceId != null) {
			flightfact.setSourceId(sourceId);
		}
		BaseUtils.createFactOnUser(flightfact, bookingUser);
		return AirConfiguratorHelper.getAirConfigRule(flightfact, AirConfiguratorRuleType.SOURCECONFIG);
	}

	/**
	 * This method will give the SourceConfig based on TripInfo
	 *
	 * @return
	 */

	public static AirSourceConfigurationOutput getAirSourceConfigOnTripInfo(TripInfo tripInfo, User bookingUser) {
		FlightBasicFact flightfact = FlightBasicFact.builder().build();
		flightfact.generateFact(tripInfo);
		flightfact.setAirType(AirUtils.getAirType(tripInfo));
		BaseUtils.createFactOnUser(flightfact, bookingUser);
		return AirConfiguratorHelper.getAirConfigRule(flightfact, AirConfiguratorRuleType.SOURCECONFIG);
	}

	public static AirSSRInfoOutput getAirSSRInfos(SupplierBasicInfo basicInfo, String airline, User user) {
		FlightBasicFact flightfact =
				FlightBasicFact.builder().airline(airline).sourceId(basicInfo.getSourceId()).build();
		flightfact.setSupplierId(basicInfo.getSupplierId());
		BaseUtils.createFactOnUser(flightfact, user);
		return AirConfiguratorHelper.getAirConfigRule(flightfact, AirConfiguratorRuleType.SSR_INFO);
	}

	public static AirSSRInfoOutput getAirSSRInfos(FlightBasicFact flightFact) {
		return AirConfiguratorHelper.getAirConfigRule(flightFact, AirConfiguratorRuleType.SSR_INFO);
	}

	/**
	 * @param routeInfos : to be sorted according to their travel date
	 */
	public static List<RouteInfo> sortRouteList(List<RouteInfo> routeInfos) {
		if (CollectionUtils.isEmpty(routeInfos)) {
			return routeInfos;
		}
		routeInfos.sort(new Comparator<RouteInfo>() {
			@Override
			public int compare(RouteInfo o1, RouteInfo o2) {
				return o1.getTravelDate().compareTo(o2.getTravelDate());
			}
		});
		return routeInfos;
	}

	public static NameLengthLimit getAirNameLengthLimit(FlightBasicFact fact) {
		AirSourceConfigurationOutput info =
				AirConfiguratorHelper.getAirConfigRule(fact, AirConfiguratorRuleType.SOURCECONFIG);
		if (info != null && MapUtils.isNotEmpty(info.getAirNameLengthMap())) {
			NameLengthLimit airNameLengthLimitFromRule =
					info.getAirNameLengthMap().getOrDefault(fact.getAirline(), info.getAirNameLengthMap().get("ALL"));
			if (airNameLengthLimitFromRule != null) {
				return airNameLengthLimitFromRule;
			}
		} else {
			AirGeneralPurposeOutput airGeneralOutput = AirConfiguratorHelper.getGeneralPurposeOutput(fact);
			if (airGeneralOutput != null && MapUtils.isNotEmpty(airGeneralOutput.getAirNameLengthMap())) {
				NameLengthLimit airNameLengthLimitFromRule = airGeneralOutput.getAirNameLengthMap()
						.getOrDefault(fact.getAirline(), airGeneralOutput.getAirNameLengthMap().get("ALL"));
				if (airNameLengthLimitFromRule != null) {
					return airNameLengthLimitFromRule;
				}
			}
		}
		return null;
	}

	public static Map<String, List<Integer>> getPreferredAirlineSourceList(User bookingUser) {
		AirGeneralPurposeOutput info =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (info != null && MapUtils.isNotEmpty(info.getPrefferedAirlineMap())) {
			return info.getPrefferedAirlineMap();
		}
		return null;
	}

	public static TripInfo mergeTripInfo(List<TripInfo> tripInfos) {
		TripInfo copyTripInfo = new GsonMapper<>(tripInfos.get(0), TripInfo.class).convert();
		copyTripInfo.getSegmentInfos().addAll(tripInfos.get(1).getSegmentInfos());
		return copyTripInfo;
	}

	public static boolean getSeatApplicable(TripInfo tripInfo, AtomicBoolean previousValue) {

		AirSourceConfigurationOutput sourceConfiguration =
				getAirSourceConfigOnTripInfo(tripInfo, SystemContextHolder.getContextData().getUser());
		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsSeatApplicable())) {
			previousValue.set(true);
			return true;
		}

		return previousValue.get();
	}

	public static boolean isAddressRequired(TripInfo tripInfo, AtomicBoolean previousValue, User bookingUser) {
		AirSourceConfigurationOutput sourceConfiguration = getAirSourceConfigOnTripInfo(tripInfo, bookingUser);
		if (sourceConfiguration != null && BooleanUtils.isTrue(sourceConfiguration.getIsAddressRequired())) {
			previousValue.set(true);
			return true;
		}

		return previousValue.get();
	}

	/**
	 * This will validate whether there is sufficient time gap between the departure time and review time.
	 *
	 *
	 * @param restrictionMinutes
	 * @param departureTime
	 * @param now
	 * @return
	 */

	public static boolean isSSRValidForTrip(Integer restrictionMinutes, LocalDateTime departureTime,
			LocalDateTime now) {
		if (restrictionMinutes == null || departureTime.minusMinutes(restrictionMinutes).isAfter(now)) {
			return true;
		}
		return false;
	}

	public static boolean isSameSupplier(SupplierBasicInfo onwardInfo, SupplierBasicInfo returnInfo) {

		return onwardInfo.getSupplierId().equals(returnInfo.getSupplierId());
	}

	public static boolean isSameAirline(AirlineInfo onwardAirline, AirlineInfo returnAirline) {
		if (onwardAirline != null && returnAirline != null) {
			return onwardAirline.getCode().equals(returnAirline.getCode());
		}
		return false;
	}

	public static boolean isSameCabinClass(CabinClass onwardClass, CabinClass returnClass) {
		return onwardClass.equals(returnClass);
	}

	public static AtomicDouble getAirlineTotalFare(TripInfo tripInfo) {
		AtomicDouble totalFare = new AtomicDouble(0);
		tripInfo.getSegmentInfos().forEach(segmentInfo -> {
			PriceInfo priceInfo = segmentInfo.getPriceInfo(0);
			if (priceInfo != null) {
				totalFare.getAndAdd(priceInfo.getTotalAirlineFare());
			}
		});
		return totalFare;
	}

	public static boolean isNearByAirport(String code, Integer sourceId, AirSearchQuery searchQuery,
			AirSourceConfigurationOutput airSourceOutput, User bookingUser) {
		if (airSourceOutput == null) {
			airSourceOutput = AirUtils.getAirSourceConfiguration(searchQuery, sourceId, bookingUser);
		}
		if (CollectionUtils.isNotEmpty(airSourceOutput.getNearByAirports())) {
			List<KeyValue> pathList = airSourceOutput.getNearByAirports();
			if (CollectionUtils.isNotEmpty(pathList)) {
				for (KeyValue path : pathList) {
					if (path.getKey().contains(code)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static boolean isGNNearByAirport(String code, Integer sourceId, AirSearchQuery searchQuery,
			User bookingUser) {
		AirGeneralPurposeOutput airGeneralPurposeOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (airGeneralPurposeOutput != null
				&& CollectionUtils.isNotEmpty(airGeneralPurposeOutput.getNearByAirports())) {
			List<KeyValue> pathList = airGeneralPurposeOutput.getNearByAirports();
			if (CollectionUtils.isNotEmpty(pathList)) {
				for (KeyValue path : pathList) {
					if (path.getKey().contains(code)) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public static FareDetail copyStaticInfo(FareDetail newFareDetail, FareDetail oldFareDetail) {
		newFareDetail.setCabinClass(oldFareDetail.getCabinClass());
		newFareDetail.setClassOfBooking(oldFareDetail.getClassOfBooking());
		newFareDetail.setFareBasis(oldFareDetail.getFareBasis());
		newFareDetail.setFareType(oldFareDetail.getFareType());
		newFareDetail.setIsHandBaggage(oldFareDetail.getIsHandBaggage());
		newFareDetail.setIsMealIncluded(oldFareDetail.getIsMealIncluded());
		newFareDetail.setRefundableType(oldFareDetail.getRefundableType());
		newFareDetail.setSeatRemaining(oldFareDetail.getSeatRemaining());
		newFareDetail.setBaggageInfo(oldFareDetail.getBaggageInfo());
		return newFareDetail;
	}

	// isSameBaggageInfo(oldPriceInfo, newPriceInfo) for same price
	public static boolean isSamePriceInfo(PriceInfo oldPriceInfo, PriceInfo newPriceInfo) {
		if (oldPriceInfo != null && newPriceInfo != null
				&& Objects.equals(oldPriceInfo.getFareBasis(PaxType.ADULT), newPriceInfo.getFareBasis(PaxType.ADULT))
				&& Objects.equals(oldPriceInfo.getCabinClass(PaxType.ADULT), newPriceInfo.getCabinClass(PaxType.ADULT))
				&& Objects.equals(oldPriceInfo.getFareDetail(PaxType.ADULT).getRefundableType(),
						newPriceInfo.getFareDetail(PaxType.ADULT).getRefundableType())
				&& isSameFareIdentifier(oldPriceInfo, newPriceInfo)) {
			return true;
		}
		return false;
	}

	public static boolean isSameBaggageInfo(PriceInfo oldPriceInfo, PriceInfo newPriceInfo) {
		boolean isBaggageSame = false;
		if (oldPriceInfo.getFareDetail(PaxType.ADULT).getBaggageInfo() != null
				&& newPriceInfo.getFareDetail(PaxType.ADULT).getBaggageInfo() != null) {
			if (StringUtils.isEmpty(oldPriceInfo.getFareDetail(PaxType.ADULT).getBaggageInfo().getAllowance())
					&& StringUtils.isEmpty(newPriceInfo.getFareDetail(PaxType.ADULT).getBaggageInfo().getAllowance())) {
				isBaggageSame = true;
			} else if (Objects.equals(oldPriceInfo.getFareDetail(PaxType.ADULT).getBaggageInfo().getAllowance(),
					newPriceInfo.getFareDetail(PaxType.ADULT).getBaggageInfo().getAllowance())) {
				isBaggageSame = true;
			}
		}
		return isBaggageSame;
	}

	public static boolean isSameFareIdentifier(PriceInfo oldPriceInfo, PriceInfo newpriceInfo) {
		if (oldPriceInfo != null && newpriceInfo != null) {
			return oldPriceInfo.getFareType().equals(newpriceInfo.getFareType());
		}
		return true;
	}

	public static boolean isSameBaseFare(PriceInfo oldPriceInfo, PriceInfo newPriceInfo) {
		if (oldPriceInfo != null && newPriceInfo != null) {
			double oldBaseFare =
					oldPriceInfo.getFareDetail(PaxType.ADULT).getFareComponents().getOrDefault(FareComponent.BF, 0d);
			double newBaseFare =
					newPriceInfo.getFareDetail(PaxType.ADULT).getFareComponents().getOrDefault(FareComponent.BF, 0d);
			return oldBaseFare == newBaseFare ? true : false;
		}
		return true;
	}

	public static FlightAPIURLRuleCriteria getAirEndPointURL(AirSourceConfigurationOutput configurationOutput) {
		if (configurationOutput != null && CollectionUtils.isNotEmpty(configurationOutput.getFlightUrl())) {
			List<String> urlList = configurationOutput.getFlightUrl();
			FlightAPIURLRuleCriteria flightUrls = new FlightAPIURLRuleCriteria();
			flightUrls.setSecurityURL(getURL(urlList, 0));
			flightUrls.setPricingURL(getURL(urlList, 1));
			flightUrls.setSsrURL(getURL(urlList, 2));
			flightUrls.setReservationURL(getURL(urlList, 3));
			flightUrls.setAssesfeeURL(getURL(urlList, 4));
			flightUrls.setPnrPaymentURL(getURL(urlList, 5));
			flightUrls.setTravelAgent(getURL(urlList, 6));
			flightUrls.setHost(getURL(urlList, 7));
			flightUrls.setFareRuleURL(getURL(urlList, 8));
			flightUrls.setLogOutURL(getURL(urlList, 9));
			flightUrls.setFlightURL(getURL(urlList, 10));
			flightUrls.setRetrieveURL(getURL(urlList, 11));
			flightUrls.setFareValidateURL(getURL(urlList, 12));
			flightUrls.setFetchCreditShellURL(getURL(urlList, 13));
			flightUrls.setValidateCreditShellURL(getURL(urlList, 14));
			flightUrls.setCreditShellPaymentURL(getURL(urlList, 15));
			return flightUrls;
		}
		return null;
	}

	public static String getURL(List<String> urlList, int index) {
		try {
			return urlList.get(index);
		} catch (IndexOutOfBoundsException e) {
			log.debug("URL is Empty for Index {}", index);
			return "";
		}
	}

	public static boolean isGSTApplicable(TripInfo tripInfo, AtomicBoolean previousValue, User bookingUser) {
		AtomicBoolean isGSTApplicable = new AtomicBoolean(previousValue.get());

		/**
		 * For Client which don't support GST- if isGstApplicable is False/true, then directly return false/true
		 * 
		 * And if isGstApplicable is null in configurator then we have to check further conditions.
		 **/
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));

		if (configuratorInfo != null && Objects.nonNull(configuratorInfo.getIsGstApplicable())) {
			return configuratorInfo.getIsGstApplicable();
		}
		/*
		 * Priority for trip as trip will have the conditions received from suppliers. If trip doesn't have condition
		 * then fall back to Configurator.
		 */
		if (tripInfo.getBookingConditions() != null && tripInfo.getBookingConditions().getGstInfo() != null
				&& BooleanUtils.isTrue(tripInfo.getBookingConditions().getGstInfo().getGstApplicable())) {
			return true;
		}
		AirUtils.splitTripInfo(tripInfo, true).forEach(trip -> {
			trip.getSegmentInfos().forEach(segmentInfo -> {
				if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
					if (segmentInfo.getPriceInfo(0).getSourceId() == AirSourceType.MYSTIFLY.getSourceId()) {
						// even if any supplier involved make GST applicable to false
						isGSTApplicable.set(Boolean.FALSE);
					} else if (segmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getFareComponents()
							.getOrDefault(FareComponent.AGST, 0d) > 0) {
						isGSTApplicable.set(Boolean.TRUE);
					}
				}
			});
		});
		return isGSTApplicable.get();
	}

	public static boolean isGSTMandatory(TripInfo tripInfo, AtomicBoolean previousValue,
			AtomicBoolean isGSTApplicable) {
		AtomicBoolean isGSTMandatory = new AtomicBoolean(previousValue.get());
		/*
		 * First priority for trip as trip will have the conditions received from suppliers. If trip doesn't have
		 * condition then fall back to Configurator.
		 */
		if (isGSTApplicable.get() && tripInfo.getBookingConditions() != null
				&& tripInfo.getBookingConditions().getGstInfo() != null
				&& BooleanUtils.isTrue(tripInfo.getBookingConditions().getGstInfo().getIsGSTMandatory())) {
			return true;
		}
		if (isGSTApplicable.get()) {
			AirUtils.splitTripInfo(tripInfo, true).forEach(trip -> {
				SupplierBasicInfo basicInfo = trip.getSupplierInfo();
				SupplierInfo supplierInfo = SupplierConfigurationHelper.getSupplierInfo(basicInfo.getSupplierId());
				if (isGSTApplicable.get() && (BooleanUtils.isTrue(supplierInfo.getCredentialInfo().getIsGSTMandatory())
						&& AirSupplierUtils.isGSTMandatoryFareType(trip))) {
					isGSTMandatory.set(Boolean.TRUE);
				}
			});
		}
		return isGSTMandatory.get();
	}

	public static Proxy getProxy(User user) {
		Proxy proxy = null;
		AirGeneralPurposeOutput configuratorInfo =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, user));
		if (configuratorInfo != null && StringUtils.isNotBlank(configuratorInfo.getProxyAddress())) {
			String proxyAddress = configuratorInfo.getProxyAddress();
			proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
					new InetSocketAddress(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1])));
		}
		return proxy;
	}

	public static boolean isSupplierSessionCachingEnabled(User bookingUser) {
		AirGeneralPurposeOutput generalPurposeOutput =
				AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
		if (generalPurposeOutput != null && BooleanUtils.isTrue(generalPurposeOutput.getSessionCachingEnabled())) {
			return true;
		}
		return false;
	}

	public static boolean isAllFareComponentPresent(TripInfo tripInfo, List<FareComponent> fareComponentList,
			boolean considerAllSegment) {
		if (fareComponentList == null || CollectionUtils.isEmpty(fareComponentList)) {
			return false;
		}
		List<SegmentInfo> segmentInfos = tripInfo.getSegmentInfos();
		if (!considerAllSegment) {
			segmentInfos = Arrays.asList(segmentInfos.get(0));
		}
		for (SegmentInfo segmentInfo : segmentInfos) {
			for (PriceInfo priceInfo : segmentInfo.getPriceInfoList()) {
				FareDetail fareDetail = priceInfo.getFareDetails().get(PaxType.ADULT);
				for (FareComponent component : fareComponentList) {
					if (fareDetail.getFareComponents().get(component) == null) {
						return false;
					}
				}
			}
		}
		return true;
	}

	public static Optional<FlightTravellerInfo> findTraveller(List<FlightTravellerInfo> travellerInfos,
			FlightTravellerInfo travellerInfo) {
		Optional<FlightTravellerInfo> flightTravellerInfo =
				travellerInfos.stream()
						.filter(t1 -> StringUtils.equalsIgnoreCase(t1.getFirstName(), travellerInfo.getFirstName())
								&& StringUtils.equalsIgnoreCase(t1.getLastName(), travellerInfo.getLastName()))
						.findFirst();
		return flightTravellerInfo;
	}

	/**
	 * this function will return true if the fare type is corporate.
	 *
	 * @param priceInfo
	 * @return
	 */
	public static boolean isCorporateFare(PriceInfo priceInfo) {
		if (priceInfo.getFareType().equals(FareType.CORPORATE.getName())) {
			return true;
		}
		return false;
	}

	/**
	 * Converts all the paxType of child to Adult in travellerInfo.
	 *
	 * @param segmentList
	 * @return
	 */
	public static List<SegmentInfo> convertChildTravellerToAdultTraveller(List<SegmentInfo> segmentList) {
		List<SegmentInfo> copyList = new ArrayList<>();
		segmentList.forEach(segment -> {
			copyList.add(new GsonMapper<>(segment, new SegmentInfo(), SegmentInfo.class).convert());
		});
		if (CollectionUtils.isNotEmpty(copyList)) {
			for (SegmentInfo segment : copyList) {
				if (segment.getBookingRelatedInfo() != null
						&& CollectionUtils.isNotEmpty(segment.getBookingRelatedInfo().getTravellerInfo())) {
					for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
						if (traveller.getPaxType().equals(PaxType.CHILD)) {
							traveller.setPaxType(PaxType.ADULT);
						}
					}
				}
			}
		}
		return copyList;
	}

	/**
	 * This will identify whether corporate fare is applied and converts child passenger to adult passenger if corporate
	 * fare is applied.
	 * <p>
	 * Used in case of GDS, like sabre, amadeus.
	 *
	 * @param segmentList
	 * @return
	 */
	public static List<SegmentInfo> checkAndApplyPrivateFareLogic(List<SegmentInfo> segmentList) {
		if (isCorporateFare(segmentList.get(0).getPriceInfo(0))) {
			return convertChildTravellerToAdultTraveller(segmentList);
		}
		return segmentList;
	}

	public static boolean isDomesticTrip(TripInfo tripInfo) {
		ClientGeneralInfo generalInfo = ServiceCommunicatorHelper.getClientInfo();
		return tripInfo.isDomesticTrip(generalInfo.getCountry());
	}

	public static AirType getAirType(TripInfo tripInfo) {
		return isDomesticTrip(tripInfo) ? AirType.DOMESTIC : AirType.INTERNATIONAL;
	}

	public static Boolean getIsChangeClassAllowed(TripInfo tripInfo, PriceInfo priceInfo, User bookingUser) {
		List<TripInfo> tripInfos = tripInfo.splitTripInfo(true);
		AtomicBoolean isChangeClassAllowed = new AtomicBoolean();
		tripInfos.forEach(trip -> {
			if (priceInfo.getFareType().equals(FareType.SPECIAL_RETURN.getName())) {
				isChangeClassAllowed.set(false);
			} else {
				AirType airType = AirUtils.getAirType(trip);
				AirSourceConfigurationOutput output = getSourceConfig(trip, airType, bookingUser);
				if (!isChangeClassAllowed.get() && output != null
						&& MapUtils.isNotEmpty(output.getIsChangeClassAllowed())) {
					isChangeClassAllowed.set(output.getIsChangeClassAllowed().getOrDefault(airType, false));
				}
			}
		});
		return isChangeClassAllowed.get();
	}

	public static AirSourceConfigurationOutput getSourceConfig(TripInfo trip, AirType airType, User bookingUser) {
		ContextData contextData = SystemContextHolder.getContextData();
		SupplierBasicInfo basicInfo = trip.getSupplierInfo();
		if (MapUtils.getObject(contextData.getValueMap(), basicInfo.getSupplierId()) == null) {
			FlightBasicFact flightBasicFact = FlightBasicFact.createFact().generateFact(trip.getSupplierInfo());
			flightBasicFact.setAirType(airType);
			BaseUtils.createFactOnUser(flightBasicFact, bookingUser);
			AirSourceConfigurationOutput output =
					AirConfiguratorHelper.getAirConfigRule(flightBasicFact, AirConfiguratorRuleType.SOURCECONFIG);
			contextData.setValue(basicInfo.getSupplierId(), output);
			return output;
		}
		return (AirSourceConfigurationOutput) contextData.getValue(basicInfo.getSupplierId()).get();
	}

	public static boolean isDomesticTrips(List<TripInfo> tripInfos) {
		AtomicBoolean isDomestic = new AtomicBoolean(true);
		for (TripInfo trip : tripInfos) {
			if (!AirUtils.isDomesticTrip(trip)) {
				isDomestic.set(false);
				break;
			}
		}
		return isDomestic.get();
	}

	// To identify SearchType from tripInfos post booking fetched by bookingId.
	public static SearchType getSearchTypeFromTrips(List<TripInfo> tripInfos) {
		SearchType searchType = tripInfos.get(0).getSegmentInfos().get(0).getMiscInfo().getSearchType();
		if (searchType != null) {
			return searchType;
		} else {
			/**
			 * This Will handle the old Booking case in which we had not persisted the SearchType.
			 */
			if (tripInfos.size() == 1) {
				return SearchType.ONEWAY;
			} else if (tripInfos.size() == 2
					&& tripInfos.get(0).getDepartureAirportCode().equals(tripInfos.get(1).getArrivalAirportCode())) {
				return SearchType.RETURN;
			}
			return SearchType.MULTICITY;
		}
	}

	public static void generateMissingFactsOnSegmentInfo(SegmentInfo segmentInfo, FlightBasicFact fact) {
		fact.generateFact(segmentInfo);
		if (segmentInfo.getFlightDesignator() != null) {
			fact.setEquipTypes(new HashSet<String>(Arrays.asList(segmentInfo.getFlightDesignator().getEquipType())));
		}
		fact.setFlightNumbers(new HashSet<String>(Arrays.asList(segmentInfo.getFlightNumber())));
		RouteInfo routeInfo = new RouteInfo();
		routeInfo.setFromCityOrAirport(segmentInfo.getDepartAirportInfo());
		routeInfo.setToCityOrAirport(segmentInfo.getArrivalAirportInfo());
		routeInfo.setTravelDate(segmentInfo.getDepartTime().toLocalDate());
		fact.setRouteInfos(Arrays.asList(routeInfo));
		if (Objects.nonNull(segmentInfo.getConnectingTime())) {
			fact.setLayovers(Arrays.asList(segmentInfo.getConnectingTime()));
		}
	}

	public static AirlineTimeLimitData getAirlineTimeLimitData(String airline,
			AirSourceConfigurationOutput sourceConfigurationOutput, User bookingUser) {
		if (StringUtils.isNotEmpty(airline) && sourceConfigurationOutput != null
				&& sourceConfigurationOutput.getAirTimeLimit() != null
				&& CollectionUtils.isNotEmpty(sourceConfigurationOutput.getAirTimeLimit().getAirlineTimeLimits())) {
			for (AirlineTimeLimitData timeLimitData : sourceConfigurationOutput.getAirTimeLimit()
					.getAirlineTimeLimits()) {
				if (CollectionUtils.isNotEmpty(timeLimitData.getAirlines())
						&& timeLimitData.getAirlines().contains(airline)) {
					return timeLimitData;
				}
			}
		} else {
			AirGeneralPurposeOutput gnOutput =
					AirConfiguratorHelper.getGeneralPurposeOutput(BaseUtils.createFactOnUser(null, bookingUser));
			if (StringUtils.isNotEmpty(airline) && gnOutput != null && gnOutput.getAirTimeLimit() != null
					&& CollectionUtils.isNotEmpty(gnOutput.getAirTimeLimit().getAirlineTimeLimits())) {
				for (AirlineTimeLimitData timeLimitData : gnOutput.getAirTimeLimit().getAirlineTimeLimits()) {
					if (CollectionUtils.isNotEmpty(timeLimitData.getAirlines())
							&& timeLimitData.getAirlines().contains(airline)) {
						return timeLimitData;
					}
				}
			}
		}
		return null;
	}

	public static double getTotalMappedComponentAmount(Map<PaxType, Integer> paxTypeMap, PriceInfo priceInfo,
			FareComponent mapComponent, FareComponent targetComponent) {
		double totalAmount = 0;
		for (PaxType paxType : paxTypeMap.keySet()) {
			if (priceInfo.getFareDetail(paxType) != null
					&& priceInfo.getFareDetail(paxType).getAddlFareComponents() != null) {
				totalAmount += priceInfo.getFareDetail(paxType).getAddlFareComponents()
						.getOrDefault(mapComponent, new HashMap<>()).getOrDefault(targetComponent, 0.0)
						* paxTypeMap.get(paxType);
			}
		}
		return totalAmount;
	}

	public static int getFlightRestrictionMinutes(TripInfo tripInfo,
			AirSourceConfigurationOutput airSourceConfigurationOutput, AirGeneralPurposeOutput gnSourceRule) {
		int flightRestrictionMinutes = 0;

		if (airSourceConfigurationOutput != null
				&& airSourceConfigurationOutput.getFlightRestrictionMinutes() != null) {
			flightRestrictionMinutes = airSourceConfigurationOutput.getFlightRestrictionMinutes();
		} else if (gnSourceRule != null) {
			flightRestrictionMinutes = gnSourceRule.getFlightRestrictionMinutes();

		}
		return flightRestrictionMinutes;
	}

	public static String getLogType(String prefix, SupplierConfiguration configuration) {
		return (prefix + "-" + configuration.getBasicInfo().getDescription() + "-"
				+ configuration.getBasicInfo().getRuleId());
	}

	public static void setPlatingCarrier(List<TripInfo> tripInfos) {
		for (TripInfo tripInfo : tripInfos) {
			setPlatingCarrier(tripInfo);
		}
	}

	public static void setPlatingCarrier(TripInfo tripInfo) {
		if (CollectionUtils.isEmpty(tripInfo.getTripPriceInfos())) {
			return;
		}
		for (PriceInfo priceInfo : tripInfo.getTripPriceInfos()) {
			String platingCarrier = tripInfo.getAirlineCode();
			if (priceInfo.getMiscInfo().getPlatingCarrier() != null)
				platingCarrier = priceInfo.getMiscInfo().getPlatingCarrier().getCode();
			priceInfo.setPlatingCarrier(AirlineHelper.getAirlineInfo(platingCarrier));
		}
	}

	public static void unsetExtraServicesForApiUser(TripInfo trip) {
		for (SegmentInfo segmentInfo : trip.getSegmentInfos()) {
			if (MapUtils.isNotEmpty(segmentInfo.getSsrInfo())) {
				segmentInfo.getSsrInfo().remove(SSRType.EXTRASERVICES);
				segmentInfo.getSsrInfo().remove(SSRType.SEAT);
			}
		}
	}

	public static double calculateAvailablePNRCredit(List<SegmentInfo> segments) {
		double totalCreditBalance = 0;
		if (CollectionUtils.isNotEmpty(segments)) {
			for (SegmentInfo segment : segments) {
				totalCreditBalance +=
						BaseUtils.calculateAvailablePNRCredit(segment.getBookingRelatedInfo().getTravellerInfo());
			}
		}
		return totalCreditBalance;
	}

	public static List<PriceInfo> getTripTotalPriceInfoList(TripInfo tripInfo, User user) {
		SegmentPriceInfoToTripPriceInfo toTripPriceInfo =
				SegmentPriceInfoToTripPriceInfo.builder().tripInfo(tripInfo).user(user).build();
		return toTripPriceInfo.convert();
	}

	public static void setTripPriceInfoFromSegmentPriceInfo(TripInfo tripInfo, User user,
			FareBreakUpConfigOutput configOutput) {
		TripPriceInfoUserMapper infoUserMapper =
				TripPriceInfoUserMapper.builder().tripInfo(tripInfo).user(user).configOutput(configOutput).build();
		infoUserMapper.convert();
	}

	public static boolean isInfantPresentInBooking(List<TripInfo> tripInfoList) {
		for (TripInfo tripInfo : tripInfoList) {
			if (getPaxInfo(tripInfo).get(PaxType.INFANT) > 0) {
				return true;
			}
		}
		return false;
	}

	public static boolean getIsPANRequired(TripInfo trip, AtomicBoolean isPANRequired) {
		if (trip.getBookingConditions() != null
				&& BooleanUtils.isTrue(trip.getBookingConditions().getIsPANRequired())) {
			isPANRequired.set(true);
		}
		return isPANRequired.get();
	}
}
