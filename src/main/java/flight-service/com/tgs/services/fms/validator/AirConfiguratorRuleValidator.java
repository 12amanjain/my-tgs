package com.tgs.services.fms.validator;

import org.springframework.validation.Errors;
import org.springframework.stereotype.Service;
import org.springframework.validation.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;

@Service
public class AirConfiguratorRuleValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator flightBasicRuleCriteriaValidator;

	@Autowired
	AirGeneralPurposeOutputValidator generalPurposeValidator;

	@Autowired
	AirSourceConfigurationOutputValidator sourceConfigValidator;

	@Autowired
	AirSSRInfoOutputValidator ssrInfoValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		AirConfiguratorInfo airconfiguratorInfo = (AirConfiguratorInfo) target;

		if (airconfiguratorInfo.getRuleType() == null) {

			rejectValue(errors, "ruleType", SystemError.INVALID_RULE);

		} else {
			if (airconfiguratorInfo.getOutput() == null) {
				rejectValue(errors, "output", SystemError.EMPTY_CONFIGURATOR_OUTPUT);
			}

			if (AirConfiguratorRuleType.GNPUPROSE.equals(airconfiguratorInfo.getRuleType())) {

				generalPurposeValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());

			} else if (AirConfiguratorRuleType.SOURCECONFIG.equals(airconfiguratorInfo.getRuleType())) {

				sourceConfigValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());

			} else if (AirConfiguratorRuleType.SSR_INFO.equals(airconfiguratorInfo.getRuleType())) {

				ssrInfoValidator.validateOutput(errors, "output", airconfiguratorInfo.getOutput());

			}
		}
		flightBasicRuleCriteriaValidator.validateCriteria(errors, "inclusionCriteria",
				(BasicRuleCriteria) airconfiguratorInfo.getInclusionCriteria());
		flightBasicRuleCriteriaValidator.validateCriteria(errors, "exclusionCriteria",
				(BasicRuleCriteria) airconfiguratorInfo.getExclusionCriteria());

	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

}
