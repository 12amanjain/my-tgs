package com.tgs.services.fms.validator;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;

@Service
public class AirGeneralPurposeOutputValidator extends AbstractAirConfigRuleValidator {

	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		AirGeneralPurposeOutput output = (AirGeneralPurposeOutput) iRuleOutput;
		if (output == null) {
			return;
		}

		List<Integer> cacheDisabledSources = output.getCachedDisabledSources();
		if (CollectionUtils.isNotEmpty(cacheDisabledSources)) {
			validateSourceIds(cacheDisabledSources, fieldName + ".cachedDisabledSources", errors,
					SystemError.INVALID_FBRC_SOURCEID);
		}

		List<String> ffDisabledAirlines = output.getFfDisabledAirlines();
		validateAirlines(ffDisabledAirlines, fieldName + ".ffDisabledAirlines", errors,
				SystemError.INVALID_AIRLINE_CODE);

		Map<String, List<Integer>> prefferedAirlineMap = output.getPrefferedAirlineMap();
		if (MapUtils.isNotEmpty(prefferedAirlineMap)) {
			Set<String> airlines = new HashSet<>();
			Set<Integer> sourceIds = new HashSet<>();
			for (Entry<String, List<Integer>> entry : prefferedAirlineMap.entrySet()) {
				airlines.add(entry.getKey());
				sourceIds.addAll(entry.getValue());
			}
			validateAirlines(new ArrayList<String>(airlines), fieldName + ".prefferedAirlineMap", errors,
					SystemError.INVALID_AIRLINE_CODE);
			validateSourceIds(new ArrayList<>(sourceIds), fieldName + ".prefferedAirlineMap", errors,
					SystemError.INVALID_FBRC_SOURCEID);
		}

		Map<String, NameLengthLimit> airNameLengthMap = output.getAirNameLengthMap();
		if (MapUtils.isNotEmpty(airNameLengthMap)) {
			for (Entry<String, NameLengthLimit> entry : airNameLengthMap.entrySet()) {
				validateAirlines(Arrays.asList(entry.getKey()), fieldName + ".airNameLengthMap", errors,
						SystemError.INVALID_AIRLINE_CODE);
			}
		}
	}

}
