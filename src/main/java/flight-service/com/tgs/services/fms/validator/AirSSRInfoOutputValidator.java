package com.tgs.services.fms.validator;

import java.util.List;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.fms.datamodel.airconfigurator.AirSSRInfoOutput;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;

@Service
public class AirSSRInfoOutputValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		AirSSRInfoOutput output = (AirSSRInfoOutput) iRuleOutput;
		if (output == null) {
			return;
		}

		if (CollectionUtils.isNotEmpty(output.getBaggageSsrList())) {
			validateSSRInfo(errors, fieldName + ".baggageSsrList", output.getBaggageSsrList());
		}

		if (CollectionUtils.isNotEmpty(output.getExtraSsrList())) {
			validateSSRInfo(errors, fieldName + ".extraSsrList", output.getExtraSsrList());
		}

		if (CollectionUtils.isNotEmpty(output.getMealSsrList())) {
			validateSSRInfo(errors, fieldName + ".mealSsrList", output.getMealSsrList());
		}

		if (CollectionUtils.isNotEmpty(output.getSeatSsrList())) {
			validateSSRInfo(errors, fieldName + ".seatSsrList", output.getSeatSsrList());
		}
	}

	private void validateSSRInfo(Errors errors, String fieldName, List<SSRInformation> ssrInfos) {
		for (SSRInformation ssr : ssrInfos) {
			if (ssr.getCode() == null || ssr.getDesc() == null) {
				rejectValue(errors, fieldName, SystemError.NULL_SSR_CODE_DESC);
			}
		}
	}

}
