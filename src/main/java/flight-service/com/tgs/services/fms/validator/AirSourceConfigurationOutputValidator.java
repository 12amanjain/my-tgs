package com.tgs.services.fms.validator;

import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.util.Map.Entry;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.datamodel.airconfigurator.AirSourceConfigurationOutput;

@Service
public class AirSourceConfigurationOutputValidator extends AbstractAirConfigRuleValidator {

	@Override
	public <T extends IRuleOutPut> void validateOutput(Errors errors, String fieldName, T iRuleOutput) {
		AirSourceConfigurationOutput output = (AirSourceConfigurationOutput) iRuleOutput;
		if (output == null) {
			return;
		}

		List<String> includedAirlines = output.getIncludedAirlines();
		validateAirlines(includedAirlines, fieldName + ".includedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

		List<String> excludedAirlines = output.getExcludedAirlines();
		validateAirlines(excludedAirlines, fieldName + ".excludedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

		List<KeyValue> nearByAirports = output.getNearByAirports();
		if (CollectionUtils.isNotEmpty(nearByAirports)) {
			List<String> airports = new ArrayList<>();
			for (KeyValue nearByAirport : nearByAirports) {
				airports.add(nearByAirport.getKey());
				airports.add(nearByAirport.getValue());
			}
			validateAirports(airports, fieldName + ".nearByAirports", errors, SystemError.INVALID_AIRPORT_CODE);
		}

		List<String> ffDisabledAirlines = output.getFfDisabledAirlines();
		validateAirlines(ffDisabledAirlines, fieldName + ".ffDisabledAirlines", errors,
				SystemError.INVALID_FBRC_AIRLINE);

		Map<String, NameLengthLimit> airNameLengthMap = output.getAirNameLengthMap();
		if (MapUtils.isNotEmpty(airNameLengthMap)) {
			List<String> airlines = new ArrayList<>();
			for (Entry<String, NameLengthLimit> entry : airNameLengthMap.entrySet()) {
				airlines.add(entry.getKey());
			}
			validateAirlines(airlines, fieldName + ".airNameLengthMap", errors, SystemError.INVALID_FBRC_AIRLINE);
		}

	}

}
