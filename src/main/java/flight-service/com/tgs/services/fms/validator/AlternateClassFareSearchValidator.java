package com.tgs.services.fms.validator;


import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.helper.FlightCacheHandler;
import com.tgs.services.fms.restmodel.alternateclass.AlternateClassFareSearchRequest;
import com.tgs.services.fms.utils.AirUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Service
public class AlternateClassFareSearchValidator implements Validator {

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	private FlightCacheHandler cacheHandler;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof AlternateClassFareSearchRequest) {
			AlternateClassFareSearchRequest request = (AlternateClassFareSearchRequest) target;
			request.getFareClasses().forEach(tripRequest -> {
				TripInfo tripInfo = cachingService.fetchValue(tripRequest.getPriceId(), TripInfo.class,
						CacheSetName.PRICE_INFO.getName(), BinName.PRICEINFOBIN.getName());
				if (tripInfo != null && MapUtils.isNotEmpty(tripRequest.getClassData())) {
					AirUtils.unsetRedudantPriceInfoBasedUponId(tripInfo, tripRequest.getPriceId());
					tripRequest.getClassData().forEach((segmentNum, newClasses) -> {
						if (!(segmentNum < tripInfo.getSegmentInfos().size())) {
							rejectValue(errors, "fareClasses", SystemError.INVALID_ALTERNATE_CLASS_SEGMENT_COUNT);
						}
					});
				}
			});

		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
