package com.tgs.services.fms.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;

@Service
public class FareRuleValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator criteriaValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof FareRuleInfo) {
			FareRuleInfo rule = (FareRuleInfo) target;
			criteriaValidator.validateCriteria(errors, "inclusionCriteria", rule.getInclusionCriteria());
			criteriaValidator.validateCriteria(errors, "exclusionCriteria", rule.getExclusionCriteria());
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
