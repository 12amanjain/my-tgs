package com.tgs.services.fms.validator;

import java.util.function.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.datamodel.KeyValue;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.validator.ListValidator;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.fms.datamodel.supplier.SupplierAdditionalInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.helper.SupplierConfigurationHelper;

@Service
public class SupplierRuleValidator implements org.springframework.validation.Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator criteriaValidator;

	@Autowired
	ListValidator listValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof SupplierRule) {
			SupplierRule rule = (SupplierRule) target;
			if (SupplierConfigurationHelper.getSupplierInfo(rule.getSupplierId()) == null) {
				rejectValue(errors, "supplierId", SystemError.INVALID_FBRC_SUPPLIERID, rule.getSupplierId());
			}
			SupplierAdditionalInfo supplierAdditionalInfo = rule.getSupplierAdditionalInfo();
			listValidator.validateAirlines(supplierAdditionalInfo.getIncludedAirlines(),
					"supplierAdditionalInfo.includedAirlines", errors, SystemError.INVALID_FBRC_AIRLINE);

			Predicate<KeyValue> isKeySupplierIdValid = val -> StringUtils.isNotBlank(val.getKey())
					&& SupplierConfigurationHelper.getSupplierInfo(val.getValue()) != null;

			listValidator.validateList(supplierAdditionalInfo.getTicketingSupplierIds(),
					"supplierAdditionalInfo.ticketingSupplierIds", errors, SystemError.INVALID_TICKETING_SUPPLIER_ID,
					isKeySupplierIdValid);

			listValidator.validateList(supplierAdditionalInfo.getBookingSupplierIds(),
					"supplierAdditionalInfo.bookingSupplierIds", errors, SystemError.INVALID_BOOKING_SUPPLIER_ID,
					isKeySupplierIdValid);

			criteriaValidator.validateCriteria(errors, "inclusionCriteria", rule.getInclusionCriteria());
			criteriaValidator.validateCriteria(errors, "exclusionCriteria", rule.getExclusionCriteria());

		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}

}
