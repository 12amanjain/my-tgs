package com.tgs.services.gms.communicator.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.GstInfoFilter;
import com.tgs.filters.IncidenceFilter;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.CountryInfo;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.PolicyFilter;
import com.tgs.services.gms.datamodel.PolicyInfo;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorFilter;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;
import com.tgs.services.gms.dbmodel.DbBillingEntity;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.services.gms.dbmodel.DbSystemAudit;
import com.tgs.services.gms.dbmodel.DbTravellerInfo;
import com.tgs.services.gms.helper.ConfiguratorHelper;
import com.tgs.services.gms.helper.HolidayHelper;
import com.tgs.services.gms.helper.UIConfigurationHelper;
import com.tgs.services.gms.jparepository.BackendCollectionService;
import com.tgs.services.gms.jparepository.BillingEntityService;
import com.tgs.services.gms.jparepository.CollectionService;
import com.tgs.services.gms.jparepository.CountryInfoService;
import com.tgs.services.gms.jparepository.IncidenceService;
import com.tgs.services.gms.jparepository.NoteService;
import com.tgs.services.gms.jparepository.PolicyInfoService;
import com.tgs.services.gms.jparepository.TravellerInfoService;
import com.tgs.services.gms.jparepository.configurator.ConfiguratorService;
import com.tgs.services.gms.jparepository.systemaudit.SystemAuditService;
import com.tgs.services.gms.restmodel.PolicyInfoResponse;
import com.tgs.services.ums.datamodel.User;

@Service
public class GeneralServiceCommunicatorImpl implements GeneralServiceCommunicator {

	@Autowired
	private BackendCollectionService bkcollService;

	@Autowired
	private CollectionService collService;

	@Autowired
	private PolicyInfoService plyService;

	@Autowired
	private HolidayHelper holidayHelper;

	@Autowired
	private BillingEntityService billingEntityService;

	@Autowired
	private TravellerInfoService travellerService;

	@Autowired
	private ConfiguratorService configuratorService;

	@Autowired
	private NoteService noteService;

	@Autowired
	private SystemAuditService auditService;

	@Autowired
	private CountryInfoService countryInfoService;

	@Autowired
	private IncidenceService incidenceService;

	@Autowired
	private UIConfigurationHelper uiConfHelper;

	@Override
	public List<Document> fetchDocument(CollectionServiceFilter filter) {
		List<Document> docList = bkcollService.find(filter);
		if (CollectionUtils.isNotEmpty(docList)) {
			return docList;
		}
		return null;
	}

	@Override
	public List<Document> fetchGeneralDocument(CollectionServiceFilter filter) {
		List<Document> docList = collService.find(filter);
		if (CollectionUtils.isNotEmpty(docList)) {
			return docList;
		}
		return null;
	}

	@Override
	public LocalDateTime nextWorkingDay(LocalDateTime dateTime) {
		return holidayHelper.nextWorkingDay(dateTime);
	}

	@Override
	public PolicyInfoResponse fetchPolicies(PolicyFilter filter) {
		PolicyInfoResponse res = new PolicyInfoResponse();
		res.setPolicyInfos(new ArrayList<>());
		plyService.findAll(filter)
				.forEach(policy -> res.getPolicyInfos().add(new GsonMapper<>(policy, PolicyInfo.class).convert()));
		return res;
	}

	@Override
	public List<ConfiguratorInfo> getConfiguratorInfos(ConfiguratorRuleType ruleType) {
		List<ConfiguratorInfo> configuratorInfos = ConfiguratorHelper.getConfiguratorRules(ruleType);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			return ConfiguratorHelper.getConfiguratorRules(ruleType).stream().filter(info -> info.getEnabled())
					.collect(Collectors.toList());
		}
		return null;
	}

	@Override
	public ConfiguratorInfo getConfiguratorInfo(ConfiguratorRuleType ruleType) {
		return ConfiguratorHelper.getConfiguratorRule(ruleType);
	}

	@Override
	public List<ConfiguratorInfo> getConfiguratorInfosFromDB(ConfiguratorRuleType ruleType) {
		List<DbConfiguratorRule> ruleList = configuratorService.findAll(
				ConfiguratorFilter.builder().ruleType(ruleType.getName()).enabled(true).isDeleted(false).build());
		return DbConfiguratorRule.toDomainList(ruleList);
	}

	@Override
	public void addNote(Note note) {
		noteService.save(note);
	}

	@Override
	public List<Note> getNotes(NoteFilter filter) {
		return noteService.findAll(filter);
	}

	@Override
	public void addToSystemAudit(SystemAudit systemAudit) {
		DbSystemAudit audit = auditService.save(new DbSystemAudit().from(systemAudit));
		auditService.save(audit);
	}

	@Override
	public <T> T getConfigRule(ConfiguratorRuleType ruleType, IFact fact) {
		return ConfiguratorHelper.getConfigRule(ruleType, fact);
	}

	@Override
	public CountryInfo getCountryInfo(String code) {
		return countryInfoService.getCountryInfo(code);
	}

	@Override
	public String getCountryCode(String countryName) {
		return countryInfoService.getCountryCode(countryName);
	}

	@Override
	public List<Incidence> getIncidences(IncidenceFilter filter) {
		return incidenceService.search(filter);
	}

	@Override
	public List<Document> fetchGeneralRoleSpecificDocument(CollectionServiceFilter filter) {
		List<Document> docList = new ArrayList<>();
		docList.addAll(collService.find(filter));
		return collService.getServiceResponse(docList, filter).getDocuments();
	}

	@Override
	public List<Document> fetchRoleSpecificDocument(CollectionServiceFilter filter) {
		List<Document> docList = new ArrayList<>();
		docList.addAll(bkcollService.find(filter));
		return bkcollService.getServiceResponse(docList, filter).getDocuments();
	}

	@Override
	public void saveBillingEntity(GstInfo billingEntity) {
		DbBillingEntity dbBillingEntity = billingEntityService.findByUserIdAndGstNumberAndBillingCompanyName(
				billingEntity.getBookingUserId(), billingEntity.getGstNumber(), billingEntity.getBillingCompanyName());
		if (dbBillingEntity != null)
			return;
		billingEntity.setUserId(billingEntity.getBookingUserId());
		billingEntityService.save(billingEntity);
	}

	@Override
	public void saveTravellerInfo(TravellerInfo travellerInfo, DeliveryInfo deliveryInfo, User user) {

		String travellerName = StringUtils.join(travellerInfo.getFirstName(), " ", travellerInfo.getLastName());
		Optional<DbTravellerInfo> dbTravellerInfo = Optional.ofNullable((travellerService
				.findByUserIdAndNameAndEmail(user.getParentUserId(), travellerName, deliveryInfo.getEmails().get(0))));
		if (dbTravellerInfo.isPresent()) {
			TravellerInfo newtravellerInfo = new GsonMapper<>(travellerInfo, dbTravellerInfo.get().getTravellerData(),
					TravellerInfo.class).convert();

			if (MapUtils.isNotEmpty(travellerInfo.getFrequentFlierMap())) {
				if (dbTravellerInfo.get().getTravellerData().getFrequentFlierMap() == null) {
					dbTravellerInfo.get().getTravellerData().setFrequentFlierMap(new HashMap<>());
				}
				dbTravellerInfo.get().getTravellerData().getFrequentFlierMap()
						.putAll(travellerInfo.getFrequentFlierMap());
			}
			dbTravellerInfo.get().setTravellerData(newtravellerInfo);
		} else {
			dbTravellerInfo = Optional
					.of(DbTravellerInfo.builder().travellerData(travellerInfo).email(deliveryInfo.getEmails().get(0))
							.userId(user.getParentUserId()).enabled(true).name(travellerName).build());
		}
		travellerService.save(dbTravellerInfo.get());

	}

	@Override
	public String getUIConf() {
		return uiConfHelper.getUIConf();
	}

	@Override
	public List<GstInfo> searchBillingEntity(GstInfoFilter filter) {
		return billingEntityService.searchGst(filter);
	}

}
