package com.tgs.services.gms.communicator.impl;

import java.time.LocalDateTime;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.restmodel.OtpTokenResponse;
import com.tgs.services.base.gms.OtpValidateRequest;
import com.tgs.services.ums.restmodel.ResetPasswordRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.OTPCommunicator;
import com.tgs.services.gms.base.ITokenStore;
import com.tgs.services.gms.dbmodel.OtpToken;
import com.tgs.services.gms.manager.OtpServiceManager;
import com.tgs.services.gms.servicehandler.GenericOtpServiceHandler;
import com.tgs.services.gms.servicehandler.OtpValidateHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OtpServiceCommunicatorImpl implements OTPCommunicator {

	@Autowired
	OtpValidateHandler validateHandler;

	@Autowired
	OtpServiceManager otpManager;

	@Autowired
	ITokenStore tokenstore;


	@Override
	public BaseResponse vaidateOtp(ResetPasswordRequest resetRequest) throws Exception {
		validateHandler.initData(
				OtpValidateRequest.builder().requestId(resetRequest.getRequestId()).otp(resetRequest.getOtp()).build(),
				new BaseResponse());
		return validateHandler.getResponse();

	}

	@Override
	public BaseResponse validateOtp(String requestId, String otp, String email, String mobile) {
		LocalDateTime time = LocalDateTime.now();
		time = time.minusMinutes(30);
		OtpToken otpToken = tokenstore.validateToken(requestId, otp, time);
		if (otpToken != null) {
			if (otpToken.getEmail() != null && otpToken.getEmail().equals(email)
					|| otpToken.getMobile() != null && otpToken.getMobile().equals(mobile)) {
				return new BaseResponse();
			}
		}
		return null;

	}

	@Override
	public void updateConsumedOtp(String requestId) {
		validateHandler.updateConsumedOtp(requestId);
	}

	@Override
	public void generateOtp(com.tgs.services.gms.datamodel.OtpToken otpToken) {
		try {
			GenericOtpServiceHandler otpHandler =
					SpringContext.getApplicationContext().getBean(GenericOtpServiceHandler.class);
			otpHandler.initData(otpToken, new OtpTokenResponse());
			otpHandler.getResponse();
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to generate otp for request {}", otpToken);
		}
	}

	@Override
	public com.tgs.services.gms.datamodel.OtpToken validateOtp(String requestId, String otp, int attemptCount) {
		com.tgs.services.gms.datamodel.OtpToken otpToken = null;
		try {
			otpManager.validateOtp(requestId, otp, attemptCount, 30);
			otpToken = new GsonMapper<>(tokenstore.findTokenByRequestId(requestId),
					com.tgs.services.gms.datamodel.OtpToken.class).convert();
		} catch (Exception e) {
			log.error("Validation failed for request id {} and otp {} ", requestId, otp, e);
		}
		return otpToken;
	}

}
