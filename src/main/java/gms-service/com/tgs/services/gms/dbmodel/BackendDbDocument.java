package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.base.gson.JsonStringSerializer;
import com.tgs.services.gms.datamodel.Document;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@Entity
@Audited
@Table(name = "backend_collection_documents")
public class BackendDbDocument extends SuperBaseModel<BackendDbDocument, Document> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "backend_collection_documents_generator")
	@SequenceGenerator(name="backend_collection_documents_generator", sequenceName = "backend_collection_documents_id_seq",allocationSize=1)
	private Long id;
	
	@Column
	private String key;

	@Column(length = 10000)
	@JsonAdapter(JsonStringSerializer.class)
	private String data;

	@Column
	@CreationTimestamp
	private LocalDateTime created_on;
	
	@Column
	@UpdateTimestamp
	private LocalDateTime processed_on;

	@Column
	private Boolean enabled;

	@Column
	private LocalDateTime expiry;

	@Column
	private String type;
	
	public static BackendDbDocument create(Document document) {
		return new BackendDbDocument().from(document);
	}

	@Override
	public Document toDomain() {
		return new GsonMapper<>(this, Document.class).convert();
	}

	public Document toDomain(Document document) {
		return new GsonMapper<>(this, document, Document.class).convert();
	}

	@Override
	public BackendDbDocument from(Document document) {
		return new GsonMapper<>(document, this, BackendDbDocument.class).convert();
	}
}
