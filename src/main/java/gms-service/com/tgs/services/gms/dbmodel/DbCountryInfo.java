package com.tgs.services.gms.dbmodel;

import lombok.Getter;
import lombok.Setter;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.gms.datamodel.CountryInfo;

@Entity
@Setter
@Getter
@Table(name = "countryinfo")
public class DbCountryInfo extends BaseModel<DbCountryInfo, CountryInfo> {

	@Column
	private String name;

	@Column
	private String code;

	@Column
	private String isoCode;
	
	@Column
	private String mobileCode;

	@Override
	public CountryInfo toDomain() {
		return new GsonMapper<>(this, CountryInfo.class).convert();
	}

	@Override
	public DbCountryInfo from(CountryInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbCountryInfo.class).convert();
	}
}
