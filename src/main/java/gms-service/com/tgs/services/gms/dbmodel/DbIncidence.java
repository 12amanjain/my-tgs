package com.tgs.services.gms.dbmodel;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity(name = "incidence")
@Table(name = "incidence")
@Audited
public class DbIncidence extends BaseModel<DbIncidence, Incidence> {

    @Column(updatable = false)
    private String incidenceId;

    @Column(updatable = false)
    private String name;

    @Column
    private String incidenceType;

    @Column
    @JsonAdapter(JsonStringSerializer.class)
    private String incidenceData;

    @Column
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Override
    public Incidence toDomain() {
        return new GsonMapper<>(this, Incidence.class).convert();
    }

    @Override
    public DbIncidence from(Incidence dataModel) {
        return new GsonMapper<>(dataModel, this, DbIncidence.class).convert();
    }
}
