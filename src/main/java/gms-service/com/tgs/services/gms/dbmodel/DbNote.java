package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;
import javax.annotation.Nonnull;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteAdditionalInfo;
import com.tgs.services.gms.hibernate.NoteAdditionalInfoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Table(name = "note")
@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@TypeDef(name = "NoteAdditionalInfoType", typeClass = NoteAdditionalInfoType.class)
public class DbNote extends SuperBaseModel<DbNote, Note> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "note_generator")
	@SequenceGenerator(name = "note_generator", sequenceName = "note_id_seq", allocationSize = 1)
	private Long id;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private String userId;

	@Column
	private String bookingId;

	@Column
	private String amendmentId;

	@Column
	@Nonnull
	private String noteType;

	@Column
	@Nonnull
	private String noteMessage;

	@Column
	@Type(type = "NoteAdditionalInfoType")
	private NoteAdditionalInfo additionalInfo;

	@Override
	public Note toDomain() {
		return new GsonMapper<>(this, Note.class).convert();
	}

	@Override
	public DbNote from(Note dataModel) {
		return new GsonMapper<>(dataModel, this, DbNote.class).convert();
	}
}
