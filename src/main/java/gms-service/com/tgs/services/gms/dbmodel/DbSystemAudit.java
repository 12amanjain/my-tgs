package com.tgs.services.gms.dbmodel;


import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@Entity
@Table(name="systemaudit")
public class DbSystemAudit extends SuperBaseModel<DbSystemAudit, SystemAudit> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "system_audit_generator")
	@SequenceGenerator(name="system_audit_generator", sequenceName = "systemaudit_id_seq",allocationSize=1)
	private Long id;
	
    @Column
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Column
    private String userId;

    @Column
    private String auditType;

    @Column
    private String ip;

    @Column
    private String bookingId;

    @Column
    private String loggedInUserId;

    @Override
    public SystemAudit toDomain() {
    	return new GsonMapper<>(this, SystemAudit.class).convert();
    }
    
    @Override
    public DbSystemAudit from(SystemAudit dataModel) {
    	return new GsonMapper<>(dataModel, this, DbSystemAudit.class).convert();
    }
}