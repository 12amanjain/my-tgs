package com.tgs.services.gms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.gson.JsonStringSerializer;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(uniqueConstraints = { @UniqueConstraint(columnNames = { "key", "type" }, name = "policyinfo_key_type_idx") })
@Getter
@Setter
@Audited
public class PolicyInfo {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column
	private String type;

	@Column
	private String key;

	@CreationTimestamp
	@Column
	private LocalDateTime created_on;

	@Column
	private Boolean enabled;

	@Column(length = 1000)
	@JsonAdapter(JsonStringSerializer.class)
	private String data;

	@Column
	private String name;

	@Column
	private String description;

	@Column
	private String groupName;

}
