package com.tgs.services.gms.helper;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Map.Entry;
import com.aerospike.client.query.Filter;
import com.tgs.services.gms.dbmodel.CityInfo;
import org.springframework.stereotype.Service;
import org.apache.commons.collections4.MapUtils;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.gms.datamodel.CityInfoFilter;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.gms.jparepository.CityInfoService;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;

@Service
public class CityHelper extends InMemoryInitializer {

	@Autowired
	public static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private CityInfoService cityService;

	@Autowired
	public CityHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		CityHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		List<CityInfo> cityList = cityService.findAll(CityInfoFilter.builder().build());
		for (CityInfo cityInfo : cityList) {
			saveInCache(cityInfo);
		}
	}

	private void saveInCache(CityInfo cityInfo) {
		int expiration = -1;
		if (!cityInfo.getEnabled()) {
			expiration = 1;
		}
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.COUNTRY.getName(), cityInfo.getCountry());
		binMap.put(BinName.STATE.getName(), cityInfo.getState());
		binMap.put(BinName.CITY.getName(), cityInfo.getName());
		cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.CITY_INFO.getName())
				.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).key(String.valueOf(cityInfo.getId())).build(), binMap,
				false, true, expiration);
	}

	public static List<com.tgs.services.base.datamodel.CityInfo> searchInCache(CityInfoFilter filter) {
		List<com.tgs.services.base.datamodel.CityInfo> cityList = null;
		String binName;
		String searchKey;
		if (filter.getName() != null) {
			searchKey = filter.getName();
			binName = BinName.CITY.getName();
		} else if (filter.getState() != null) {
			searchKey = filter.getState();
			binName = BinName.STATE.getName();
		} else {
			searchKey = filter.getCountry();
			binName = BinName.COUNTRY.getName();
		}
		Map<String, Map<String, String>> cityMap = cachingCommunicator.getResultSet(
				CacheMetaInfo.builder().set(CacheSetName.CITY_INFO.getName())
						.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).build(),
				String.class, Filter.equal(binName, searchKey));
		if (MapUtils.isNotEmpty(cityMap)) {
			cityList = new ArrayList<>();
			for (Entry<String, Map<String, String>> entry : cityMap.entrySet()) {
				com.tgs.services.base.datamodel.CityInfo cityInfo = com.tgs.services.base.datamodel.CityInfo.builder().build();
				cityInfo.setCountry(entry.getValue().get(BinName.COUNTRY.getName()));
				cityInfo.setState(entry.getValue().get(BinName.STATE.getName()));
				cityInfo.setName(entry.getValue().get(BinName.CITY.getName()));
				cityList.add(cityInfo);
			}
		}
		return cityList;
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.CITY_INFO.getName())
				.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).build());
	}

}
