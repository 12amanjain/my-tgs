package com.tgs.services.gms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.dbmodel.DbDocument;
import com.tgs.services.gms.jparepository.CollectionService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CollectionHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private CollectionService collService;

	public CollectionHelper(CustomInMemoryHashMap map, GeneralCachingCommunicator cachingCommunicator) {
		super(configurationHashMap);
		CollectionHelper.cachingCommunicator = cachingCommunicator;

	}

	@Override
	public void process() {
		CollectionServiceFilter filter = CollectionServiceFilter.builder().build();
		List<DbDocument> docList = collService.findAll(filter);
		for (DbDocument dbDocument : docList) {
			try {
				saveInCache(dbDocument);
			} catch (Exception e) {
				log.error("Unable to save collection document in cache for key {}", dbDocument.getKey());
			}
		}
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.DOC_COLL.getName())
				.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).build());
	}

	public void updateCache(DbDocument dbDocument) {
		saveInCache(dbDocument);
	}

	private void saveInCache(DbDocument dbDocument) {

		Document document = dbDocument.toDomain();
		Map<String, String> binMap = new HashMap<>();

		binMap.put(BinName.DOCKEY.getName(), dbDocument.getKey());
		binMap.put(BinName.DOCTYPE.getName(), dbDocument.getType());
		binMap.put(BinName.DOCDATA.getName(), GsonUtils.getGson().toJson(document));

		int expirationTime = (BooleanUtils.isTrue(document.getEnabled())) ? -1 : 1;
		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.DOC_COLL.getName())
						.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).key(document.getKey()).build(),
				binMap, false, true, expirationTime);
	}

	/**
	 * This method return the list of collection documents from cache by passing either the collection_key or
	 * collection_type or both.
	 * 
	 * @param filter
	 * @return
	 */

	public static List<Document> getDocuments(CollectionServiceFilter filter) {

		List<Document> docList = new ArrayList<>();
		Map<String, Map<String, String>> docMap = null;
		CacheMetaInfo metaInfo = null;
		if (Objects.nonNull(filter.getKey())) {

			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.DOC_COLL.getName()).keys(Arrays.asList(filter.getKey()).toArray(new String[0]))
					.build();
			docMap = cachingCommunicator.get(metaInfo, String.class);

			for (Entry<String, Map<String, String>> entrySet : docMap.entrySet()) {
				docList.add(GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.DOCDATA.getName()),
						Document.class));
			}
		} else if (Objects.nonNull(filter.getKeys())) {

			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.DOC_COLL.getName()).keys(filter.getKeys().toArray(new String[0])).build();
			docMap = cachingCommunicator.get(metaInfo, String.class);

			for (Entry<String, Map<String, String>> entrySet : docMap.entrySet()) {
				docList.add(GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.DOCDATA.getName()),
						Document.class));
			}
		} else if (CollectionUtils.isNotEmpty(filter.getTypes())) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.DOC_COLL.getName()).build();

			for (String type : filter.getTypes()) {

				docMap = cachingCommunicator.getResultSet(metaInfo, String.class,
						Filter.equal(BinName.DOCTYPE.getName(), type));
				if (MapUtils.isNotEmpty(docMap)) {

					for (Entry<String, Map<String, String>> entrySet : docMap.entrySet()) {
						docList.add(GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.DOCDATA.getName()),
								Document.class));
					}
				}
			}
		}

		return docList;
	}

}
