package com.tgs.services.gms.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import com.tgs.services.base.ruleengine.GeneralRuleField;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorFilter;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.services.gms.jparepository.configurator.ConfiguratorService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ConfiguratorHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap configRules;

	private static Map<ConfiguratorRuleType, List<ConfiguratorInfo>> inMemoryConfigRules;

	private static final String FIELD = "config_rules";

	private static ConfiguratorHelper SINGLETON;

	@Autowired
	ConfiguratorService configuratorService;

	public ConfiguratorHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap airConfigRules) {
		super(configurationHashMap);
		ConfiguratorHelper.configRules = airConfigRules;
	}

	@PostConstruct
	void init() {
		SINGLETON = this;
	}

	@Override
	public void process() {
		log.debug("Initializing ConfiguratorHelper");
		processInMemory();

		if (MapUtils.isEmpty(inMemoryConfigRules)) {
			return;
		}

		inMemoryConfigRules.forEach((ruleType, rules) -> {
			configRules.put(ruleType.name(), FIELD, rules, CacheMetaInfo.builder().compress(false)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.GN_CONF.getName()).build());
		});
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<DbConfiguratorRule> ruleList = configuratorService.findAll(ConfiguratorFilter.builder().build());
		Map<ConfiguratorRuleType, List<ConfiguratorInfo>> ruleMap = null;
		if (CollectionUtils.isNotEmpty(ruleList)) {
			List<ConfiguratorInfo> configuratorInfos = DbConfiguratorRule.toDomainList(ruleList);
			ruleMap = configuratorInfos.stream().collect(Collectors.groupingBy(ConfiguratorInfo::getRuleType));
		}
		inMemoryConfigRules =
				Optional.<Map<ConfiguratorRuleType, List<ConfiguratorInfo>>>ofNullable(ruleMap).orElseGet(HashMap::new);
	}

	@Override
	public void deleteExistingInitializer() {
		if (inMemoryConfigRules != null) {
			inMemoryConfigRules.clear();
		}
		configRules.truncate(CacheSetName.GN_CONF.getName());
	}

	public static ConfiguratorInfo getValidConfigRule(IFact ifact, ConfiguratorRuleType ruleType) {

		List<ConfiguratorInfo> matchingRules = getApplicableRules(ruleType, ifact);

		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules.get(0);
		}
		return null;
	}

	public static <T> T getConfigRule(ConfiguratorRuleType ruleType, IFact fact) {
		List<ConfiguratorInfo> matchingRules = getApplicableRules(ruleType, fact);
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return (T) matchingRules.get(0).getOutput();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static List<ConfiguratorInfo> getApplicableRules(ConfiguratorRuleType ruleType, IFact iFact) {

		List<ConfiguratorInfo> airconfigRules = getConfiguratorRules(ruleType);

		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(GeneralRuleField.class);

		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(airconfigRules, iFact, fieldResolverMap);

		List<ConfiguratorInfo> rules = (List<ConfiguratorInfo>) ruleEngine.fireAllRules();
		return rules;
	}

	@SuppressWarnings("serial")
	public static List<ConfiguratorInfo> getConfiguratorRules(ConfiguratorRuleType ruleType) {
		if (inMemoryConfigRules == null)
			SINGLETON.processInMemory();

		/**
		 * Not able to use GsonMapper with generic types.
		 */
		Gson gson = GsonUtils.getGson();
		String json = gson.toJson(inMemoryConfigRules.get(ruleType));
		return gson.fromJson(json, new TypeToken<List<ConfiguratorInfo>>() {}.getType());
		// return configRules.get(ruleType.getName(), FIELD, null, CacheMetaInfo.builder()
		// .set(CacheSetName.GN_CONF.getName()).typeOfT(new TypeToken<List<ConfiguratorInfo>>() {
		// }.getType()).build());
	}

	public static ConfiguratorInfo getConfiguratorRule(ConfiguratorRuleType ruleType) {
		List<ConfiguratorInfo> ruleList = inMemoryConfigRules.get(ruleType);
		if (CollectionUtils.isNotEmpty(ruleList)) {
			return new GsonMapper<ConfiguratorInfo>(ruleList.get(0), ConfiguratorInfo.class).convert();
		}
		return null;
	}
}
