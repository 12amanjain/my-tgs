package com.tgs.services.gms.helper;

import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.PostConstruct;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.gms.datamodel.CountryInfo;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.gms.jparepository.CountryInfoService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CountryHelper extends InMemoryInitializer {

	@Autowired
	CountryInfoService countryInfoService;

	@Autowired
	public CountryHelper(CustomInMemoryHashMap configurationHashMap) {
		super(configurationHashMap);
	}

	private static Map<String, CountryInfo> countryHashMap;

	public static CountryInfo getCountryInfo(String code) {
		return countryHashMap.get(code);
	}

	public static String getCountryCode(String countryName) {
		return countryHashMap.entrySet().stream().filter(e -> e.getValue().getName().equals(countryName))
				.map(Map.Entry::getKey).findFirst().orElse(null);
	}

	@Override
	@PostConstruct
	public void process() {
		countryHashMap = new HashMap<>();
		countryInfoService.findAll().forEach(country -> {
			countryHashMap.put(country.getCode(), country);
		});
	}

	@Override
	public void deleteExistingInitializer() {
		countryHashMap = new HashMap<>();
	}

}
