package com.tgs.services.gms.hibernate;

import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;

public class TravellerDataType extends CustomUserType{

	 @Override
	    public Class returnedClass() {
	    	return TravellerInfo.class;	
	    }
}
