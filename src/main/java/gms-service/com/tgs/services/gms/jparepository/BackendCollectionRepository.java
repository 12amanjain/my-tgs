package com.tgs.services.gms.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.gms.dbmodel.BackendDbDocument;

public interface BackendCollectionRepository extends JpaRepository<BackendDbDocument, Long>, JpaSpecificationExecutor<BackendDbDocument> {

	public List<BackendDbDocument> findByTypeAndExpiryGreaterThan(String type, LocalDateTime expiry);

	public Optional<BackendDbDocument> findByKey(String key);

	public Optional<BackendDbDocument> findById(Long id);
}
