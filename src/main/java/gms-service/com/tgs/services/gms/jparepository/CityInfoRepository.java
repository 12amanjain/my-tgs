package com.tgs.services.gms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.gms.dbmodel.CityInfo;

public interface CityInfoRepository extends JpaRepository<CityInfo, Long>, JpaSpecificationExecutor<CityInfo> {

	public List<CityInfo> findByEnabled(Boolean enabled);

}
