package com.tgs.services.gms.jparepository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.gms.datamodel.CityInfoFilter;
import com.tgs.services.gms.dbmodel.CityInfo;

@Service
public class CityInfoService {

	@Autowired
	CityInfoRepository cityRepo;

	public List<CityInfo> findEnabled(Boolean enabled) {
		return cityRepo.findByEnabled(enabled);
	}

	@SuppressWarnings("serial")
	public List<CityInfo> findAll(CityInfoFilter filter) {
		return cityRepo.findAll(new Specification<CityInfo>() {
			@Override
			public Predicate toPredicate(Root<CityInfo> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter.getName() != null) {
					predicates.add(criteriaBuilder.equal(root.get("name"), filter.getName()));
				}
				if (filter.getEnabled() != null) {
					predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
				}
				if (filter.getState() != null) {
					predicates.add(criteriaBuilder.equal(root.get("state"), filter.getState()));
				}
				if(filter.getCountry()!=null){
					predicates.add(criteriaBuilder.equal(root.get("country"),filter.getCountry()));
				}
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		});

	}
}
