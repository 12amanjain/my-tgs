package com.tgs.services.gms.jparepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.validation.Valid;

import com.tgs.services.gms.restmodel.CollectionServiceResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.dbmodel.DbDocument;
import com.tgs.services.gms.helper.CollectionHelper;

@Service
public class CollectionService {

	@Autowired
	private CollectionRepository collectionRepo;
	
	@Autowired
	private CollectionHelper collHelper;

	public Optional<DbDocument> findById(Long id) {
		return collectionRepo.findById(id);
	}

	public Optional<DbDocument> findByKey(String key) {
		return collectionRepo.findByKey(key);
	}

	public List<DbDocument> findByTypeAndExpiryGreaterThan(String type, LocalDateTime expiry) {
		return collectionRepo.findByTypeAndExpiryGreaterThan(type, expiry);
	}

	public DbDocument save(DbDocument doc) {
		DbDocument dbDocument=collectionRepo.save(doc);
		collHelper.updateCache(dbDocument);
		return  dbDocument;
	}

	public List<DbDocument> findAll(@Valid CollectionServiceFilter filter) {
		Specification<DbDocument> specification = new Specification<DbDocument>() {
			@Override
			public Predicate toPredicate(Root<DbDocument> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				if (filter != null && StringUtils.isNotBlank(filter.getKey())) {
					predicates.add(cb.equal(root.<String>get("key"), filter.getKey()));
				}
				if (filter != null && filter.getType() != null) {
					predicates.add(cb.equal(root.<Integer>get("type"), filter.getType()));
				}
				if (filter != null && CollectionUtils.isNotEmpty(filter.getTypes())) {
					Expression<String> exp = root.get("type");
					predicates.add(exp.in(filter.getTypes()));
				}
				if (filter != null && CollectionUtils.isNotEmpty(filter.getKeys())) {
					Expression<String> exp = root.get("key");
					predicates.add(exp.in(filter.getKeys()));
				}
				if (filter != null && filter.getEnabled() != null) {
					predicates.add(cb.equal(root.<Integer>get("enabled"), filter.getEnabled()));
				}
				if (filter != null && (filter.getCreatedOnAfterDate() != null)
						&& (filter.getCreatedOnBeforeDateTime() != null)) {
					predicates.add(cb.between(root.get("createdOn"), filter.getCreatedOnAfterDateTime(),
							filter.getCreatedOnBeforeDateTime()));
				}
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};
		return collectionRepo.findAll(specification);
	}

	public List<Document> findFromCache(CollectionServiceFilter filter) {
		return collHelper.getDocuments(filter);
	}
	
	public List<Document> findLiveData(CollectionServiceFilter filter){
		return DbDocument.toDomainList(findAll(filter));
	}

	public List<Document> find(CollectionServiceFilter filter) {
		if(FetchType.LIVE.equals(filter.getFetchType())) {
			return findLiveData(filter);
		}
		else {
			return findFromCache(filter);
		}
	}
	
	public CollectionServiceResponse getServiceResponse(List<Document> docList, CollectionServiceFilter filter) {
		CollectionServiceResponse serviceResponse= new CollectionServiceResponse();
		serviceResponse.setDocuments(new ArrayList<>());
		if (CollectionUtils.isNotEmpty(docList)) {
			if (BooleanUtils.isTrue(filter.getIsConsiderHierarchy())) {
				Map<String, List<Document>> keyWiseDocuments = docList.stream()
						.collect(Collectors.groupingBy(Document::getKey));
				outer:
				for (String hierarchy : UserUtils.getUserHierarchy(SystemContextHolder.getContextData().getUser())) {
					for (Entry<String, List<Document>> entry : keyWiseDocuments.entrySet()) {
						if (entry.getKey().contains(hierarchy)) {
							docList = entry.getValue();
							break outer;
						}
					}
				}
			}
			docList.forEach(document -> {
				serviceResponse.getDocuments().add(document);
			});
		}
		return serviceResponse;		
	}
}
