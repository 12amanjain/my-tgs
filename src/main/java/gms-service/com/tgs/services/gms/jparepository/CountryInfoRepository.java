package com.tgs.services.gms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.gms.dbmodel.DbCountryInfo;

public interface CountryInfoRepository extends JpaRepository<DbCountryInfo, Long>, JpaSpecificationExecutor<DbCountryInfo>{
	

}
