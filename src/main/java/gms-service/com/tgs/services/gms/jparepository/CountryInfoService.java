package com.tgs.services.gms.jparepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.gms.datamodel.CountryInfo;
import com.tgs.services.gms.dbmodel.DbCountryInfo;
import com.tgs.services.gms.helper.CountryHelper;

@Service
public class CountryInfoService {

	@Autowired
	CountryInfoRepository countryInfoRepo;
	
	public List<CountryInfo> findAll() {
		return DbCountryInfo.toDomainList(countryInfoRepo.findAll());
	}
	
	public CountryInfo getCountryInfo(String code) {
		return CountryHelper.getCountryInfo(code);
	}
	
	public String getCountryCode(String countryName) {
		return CountryHelper.getCountryCode(countryName);
	}
}
