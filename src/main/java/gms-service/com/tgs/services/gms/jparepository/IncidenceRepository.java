package com.tgs.services.gms.jparepository;

import com.tgs.services.gms.dbmodel.DbIncidence;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.Collection;

public interface IncidenceRepository extends JpaRepository<DbIncidence, Long>, JpaSpecificationExecutor<DbIncidence> {

    DbIncidence findByIncidenceId(String incidenceId);

    Collection<DbIncidence> findByIncidenceType(String incidenceType);
}
