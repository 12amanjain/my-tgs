package com.tgs.services.gms.jparepository;

import com.tgs.filters.IncidenceFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import com.tgs.services.gms.dbmodel.DbIncidence;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class IncidenceService extends SearchService<DbIncidence> {

    @Autowired
    private IncidenceRepository repository;

    private static final String INCIDENCE_PREFIX = "20";

    public Incidence getByIncidenceId(String incidenceId) {
        DbIncidence incidence = repository.findByIncidenceId(incidenceId);
        if (incidence == null) {
            throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
        }
        return incidence.toDomain();
    }

    public List<Incidence> search(IncidenceFilter filter) {
        return BaseModel.toDomainList(search(filter, repository));
    }

    public void save(Incidence incidence) {
        validate(incidence);
        if (StringUtils.isEmpty(incidence.getIncidenceId()))
            create(incidence);
        else
            update(incidence);
    }

    private void create(Incidence incidence) {
        incidence.setIncidenceId(generateId());
        repository.saveAndFlush(new DbIncidence().from(incidence));
    }

    private void update(Incidence incidence) {
        Incidence old = getByIncidenceId(incidence.getIncidenceId());
        incidence.setId(old.getId());
        repository.saveAndFlush(new DbIncidence().from(incidence));
    }

    private static void validate(Incidence incidence) {
        try {
            incidence.getIncidenceData();
        } catch (Exception ex) {
            throw new CustomGeneralException(SystemError.INCIDENCE_DATA_MISMATCH);
        }
    }

    private static String generateId() {
        return new StringBuilder().append(INCIDENCE_PREFIX).append(RandomStringUtils.random(8, false, true))
                .toString();
    }
}
