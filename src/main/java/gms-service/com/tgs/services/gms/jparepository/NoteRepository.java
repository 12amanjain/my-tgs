package com.tgs.services.gms.jparepository;

import com.tgs.services.gms.dbmodel.DbNote;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface NoteRepository extends JpaRepository<DbNote, String>, JpaSpecificationExecutor<DbNote> {
	
	public DbNote findById(Long id);

}
