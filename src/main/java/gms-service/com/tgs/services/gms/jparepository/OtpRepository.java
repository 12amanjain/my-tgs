package com.tgs.services.gms.jparepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.gms.dbmodel.OtpToken;

public interface OtpRepository extends JpaRepository<OtpToken, Long>, JpaSpecificationExecutor<OtpToken> {

	public OtpToken findByRequestIdAndOtpAndAttemptCountLessThanAndCreatedOnGreaterThanAndIsConsumed(String requestId, String otp,
			int attemptCount, LocalDateTime createdOn, Boolean isConsumed);

	public OtpToken findByRequestId(String requestId);
	
	public List<OtpToken> findByEmailAndMobileAndCreatedOnGreaterThan(String email, String mobile, LocalDateTime time);

}
