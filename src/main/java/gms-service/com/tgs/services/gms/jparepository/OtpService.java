package com.tgs.services.gms.jparepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.gms.dbmodel.OtpToken;

@Service
public class OtpService {

	@Autowired
	OtpRepository otpRepo;

	//
	@PersistenceContext
	private EntityManager em;

	public OtpToken save(OtpToken otpToken) {
		// List<OtpToken> objectList = em
		// .createQuery("select email,otp from OtpToken where otp = '387566'",
		// OtpToken.class).getResultList();
		return otpRepo.saveAndFlush(otpToken);
	}

	public OtpToken findByRequestIdAndOTP(String requestId, String otp, LocalDateTime time) {

		// OtpToken should have valid requestId and OTP combination.
		// Maximum allowed try is 3 for each requestId and OTP combination.

		OtpToken token = otpRepo.findByRequestIdAndOtpAndAttemptCountLessThanAndCreatedOnGreaterThanAndIsConsumed(
				requestId, otp, 3, time, false);
		if (token == null) {
			OtpToken tempToken = otpRepo.findByRequestId(requestId);
			if (tempToken != null) {
				tempToken.setAttemptCount(tempToken.getAttemptCount() + 1);
				otpRepo.save(tempToken);
			}
		}
		if (token != null && token.isConsumed()) {
			return null;
		}
		return token;
	}

	public OtpToken findByRequestId(String requestId) {
		return otpRepo.findByRequestId(requestId);
	}

	public OtpToken findUnutilizedOtpForUser(String email, String mobile, LocalDateTime time) {
		List<OtpToken> otpTokens = otpRepo.findByEmailAndMobileAndCreatedOnGreaterThan(email, mobile, time);
		OtpToken otpToken = null;
		Collections.sort(otpTokens, new Comparator<OtpToken>() {

			@Override
			public int compare(OtpToken o1, OtpToken o2) {
				if (o1.getCreatedOn().isBefore(o2.getCreatedOn())) {
					return 1;
				}
				return -1;
			}
		});
		if (CollectionUtils.isNotEmpty(otpTokens)) {
			if (!otpTokens.get(0).isConsumed()) {
				otpToken = otpTokens.get(0);
			}
		}
		return otpToken;
	}
}
