package com.tgs.services.gms.jparepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;

import com.tgs.services.gms.datamodel.TravellerInfoFilter;
import com.tgs.services.gms.dbmodel.DbTravellerInfo;

@Service
public class TravellerInfoService {

    @Autowired
    TravellerInfoRepository travellerRepo;

    public DbTravellerInfo save(DbTravellerInfo dbTravellerInfo) {
        return travellerRepo.saveAndFlush(dbTravellerInfo);
    }

    public DbTravellerInfo findByUserIdAndNameAndEmail(String userId, String travellerName, String email) {
        return travellerRepo.findByUserIdAndNameAndEmail(userId,travellerName,email);
    }

    public List<DbTravellerInfo> find(TravellerInfoFilter filter,@Param("userid") String userid){
        return travellerRepo.findByNameAndUserIdAndPaxType(filter.getName(),userid,filter.getPt().getCode());
    }

}
