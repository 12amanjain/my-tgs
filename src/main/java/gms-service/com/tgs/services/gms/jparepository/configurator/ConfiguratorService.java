package com.tgs.services.gms.jparepository.configurator;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorFilter;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.services.gms.helper.ConfiguratorHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class ConfiguratorService {

	@Autowired
	ConfiguratorRepository configRepository;

	@Autowired
	ConfiguratorHelper confHelper;

	public DbConfiguratorRule save(DbConfiguratorRule airConfigRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			airConfigRule.setProcessedOn(LocalDateTime.now());
			airConfigRule = configRepository.saveAndFlush(airConfigRule);
		}
		confHelper.process();
		return airConfigRule;
	}

	public List<DbConfiguratorRule> findAll() {
		Sort sort = new Sort(Direction.DESC, Arrays.asList("processedOn"));
		return configRepository.findAll(sort);
	}

	public List<DbConfiguratorRule> findByRuleType(ConfiguratorRuleType ruleType) {
		return configRepository.findByRuleType(ruleType.getName());
	}

	public DbConfiguratorRule findById(long id) {
		return configRepository.findOne(id);
	}

	public List<DbConfiguratorRule> findAll(ConfiguratorFilter queryFilter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(queryFilter);
		Specification<DbConfiguratorRule> specification = new Specification<DbConfiguratorRule>() {
			@Override
			public Predicate toPredicate(Root<DbConfiguratorRule> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates = (ConfiguratorSearchPredicate.getPredicateListBasedOnConfigFilter(root, query, cb,
						queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		return configRepository.findAll(specification, request).getContent();
	}
}
