package com.tgs.services.gms.jparepository.systemaudit;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.gms.datamodel.systemaudit.SystemAuditFilter;
import com.tgs.services.gms.dbmodel.DbSystemAudit;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum SystemAuditPredicate {

	CREATED_DATE_ON {
		@Override
		public void addPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SystemAuditFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	USER_IDS {
		@Override
		public void addPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
					SystemAuditFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotBlank(filter.getUserId())) {
				predicates.add(criteriaBuilder.equal(root.get("userId"), filter.getUserId()));
			}

		}
	},
	AUDIT_TYPE {
		@Override
		public void addPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SystemAuditFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getAuditTypes())) {
				Expression<String> exp = root.get("auditType");
				predicates.add(exp.in(AuditAction.getCodes(filter.getAuditTypes())));
			}
		}
	},
	BOOKING_ID {
		@Override
		public void addPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SystemAuditFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotBlank(filter.getBookingId())) {
				predicates.add(criteriaBuilder.equal(root.get("bookingId"), filter.getBookingId()));
			}
		}
	},
	LOGGED_USER_ID {
		@Override
		public void addPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				SystemAuditFilter filter, List<Predicate> predicates) {
			if (StringUtils.isNotBlank(filter.getLoggedInUserId())) {
				predicates.add(criteriaBuilder.equal(root.get("loggedInUserId"), filter.getLoggedInUserId()));
			}
		}
	};

	public abstract void addPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
			SystemAuditFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnFareRuleFilter(Root<DbSystemAudit> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, SystemAuditFilter filter) {

		List<Predicate> predicates = new ArrayList<>();
		for (SystemAuditPredicate ruleSearchPredicate : SystemAuditPredicate.values()) {
			ruleSearchPredicate.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}