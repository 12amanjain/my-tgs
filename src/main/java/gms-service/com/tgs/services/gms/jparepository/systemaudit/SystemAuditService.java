package com.tgs.services.gms.jparepository.systemaudit;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.gms.datamodel.systemaudit.SystemAuditFilter;
import com.tgs.services.gms.dbmodel.DbSystemAudit;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class SystemAuditService {

	@Autowired
	SystemAuditRepository auditRepository;

	public DbSystemAudit save(DbSystemAudit audit) {
		return auditRepository.save(audit);
	}

	public List<DbSystemAudit> findAll() {
		return auditRepository.findAll();
	}

	public List<DbSystemAudit> findAll(SystemAuditFilter queryFilter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(queryFilter);
		Specification<DbSystemAudit> specification = new Specification<DbSystemAudit>() {
			@Override
			public Predicate toPredicate(Root<DbSystemAudit> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				List<Predicate> predicates = new ArrayList<>();
				predicates = (SystemAuditPredicate.getPredicateListBasedOnFareRuleFilter(root, query, cb, queryFilter));
				return cb.and(predicates.toArray(new Predicate[0]));
			}
		};

		return auditRepository.findAll(specification, request).getContent();
	}

	public DbSystemAudit findById(long id) {
		return auditRepository.findOne(id);
	}

}