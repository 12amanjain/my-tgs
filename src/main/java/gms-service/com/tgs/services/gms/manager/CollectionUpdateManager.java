package com.tgs.services.gms.manager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.gms.restmodel.CollectionUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.gms.datamodel.CollectionUpdateQuery;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.dbmodel.DbDocument;
import com.tgs.services.gms.jparepository.CollectionService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CollectionUpdateManager {

	@Autowired
	private CollectionService collService;

	private static List<String> validDocTypes = Arrays.asList("Offers", "Notification");

	public BulkUploadResponse uploadDocument(CollectionUpdateRequest collectionUpdateRequest) throws Exception {
		if (collectionUpdateRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(collectionUpdateRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable saveDocumentTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (CollectionUpdateQuery updateQuery : collectionUpdateRequest.getUpdateQuery()) {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(updateQuery.getRowId());
					try {
						saveDocument(getDocumentFromRequest(updateQuery));
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to update document with key {} type {} and data {} ", updateQuery.getKey(),
								updateQuery.getType(), updateQuery.getData(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, "collectionHelper");
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting document for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(saveDocumentTask, collectionUpdateRequest.getUploadType(),
				jobId);
	}

	private Document getDocumentFromRequest(CollectionUpdateQuery updateQuery) {
		if (!validDocTypes.contains(updateQuery.getType()))
			throw new CustomGeneralException("Invalid type for updating document");
		Document document = Document.builder().data(updateQuery.getData()).enabled(updateQuery.getEnabled())
				.key(updateQuery.getKey()).type(updateQuery.getType()).build();
		return document;
	}

	private void saveDocument(Document document) {
		DbDocument dbDocument = collService.findByKey(document.getKey()).orElse(null);
		dbDocument = new GsonMapper<>(document, dbDocument, DbDocument.class).convert();
		LocalDateTime time = LocalDateTime.now();
		dbDocument.setExpiry(time.plusYears(10));
		collService.save(dbDocument);
	}


}
