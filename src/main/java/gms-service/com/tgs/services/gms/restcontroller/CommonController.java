package com.tgs.services.gms.restcontroller;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.base.security.AbuseControlFilter;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.gms.datamodel.CityInfoFilter;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.TravellerInfoFilter;
import com.tgs.services.gms.datamodel.UIConfiguration;
import com.tgs.services.gms.datamodel.WhatsAppConsentInfo;
import com.tgs.services.gms.datamodel.systemaudit.SystemAuditFilter;
import com.tgs.services.gms.dbmodel.BackendDbDocument;
import com.tgs.services.gms.dbmodel.DbDocument;
import com.tgs.services.gms.dbmodel.DbTravellerInfo;
import com.tgs.services.gms.helper.UIConfigurationHelper;
import com.tgs.services.gms.jparepository.BackendCollectionService;
import com.tgs.services.gms.jparepository.BillingEntityService;
import com.tgs.services.gms.jparepository.CollectionService;
import com.tgs.services.gms.jparepository.CountryInfoService;
import com.tgs.services.gms.jparepository.TravellerInfoService;
import com.tgs.services.gms.manager.CollectionUpdateManager;
import com.tgs.services.gms.restmodel.BillingEntityResponse;
import com.tgs.services.gms.restmodel.BulkUploadRequest;
import com.tgs.services.gms.restmodel.CityResponse;
import com.tgs.services.gms.restmodel.CollectionServiceResponse;
import com.tgs.services.gms.restmodel.CollectionUpdateRequest;
import com.tgs.services.gms.restmodel.CountryResponse;
import com.tgs.services.gms.restmodel.TravellerInfoResponse;
import com.tgs.services.gms.restmodel.systemaudit.SystemAuditResponse;
import com.tgs.services.gms.servicehandler.CityHandler;
import com.tgs.services.gms.servicehandler.SystemAuditHandler;
import com.tgs.services.gms.servicehandler.WhatsAppConsentHandler;

@RestController
@RequestMapping("/gms/v1")
public class CommonController {

	@Autowired
	CityHandler cityHandler;

	@Autowired
	CollectionService collService;

	@Autowired
	BackendCollectionService backendCollService;

	@Autowired
	BillingEntityService billingEntityService;

	@Autowired
	TravellerInfoService travellerService;

	@Autowired
	SystemAuditHandler auditHandler;

	@Autowired
	CountryInfoService countryInfoService;

	@Autowired
	CollectionUpdateManager collectionManager;

	@Autowired
	private FiltersValidator validator;

	@Autowired
	private AbuseControlFilter abuseControlFilter;

	@Autowired
	private UIConfigurationHelper uiHelper;

	@Autowired
	WhatsAppConsentHandler consentHandler;

	@Autowired
	protected AuditsHandler auditsHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@RequestMapping(value = "/cities", method = RequestMethod.POST)
	protected CityResponse getCityList(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody CityInfoFilter cityFilter) throws Exception {
		cityHandler.initData(cityFilter, new CityResponse());
		return cityHandler.getResponse();
	}

	@RequestMapping(value = "/save-document", method = RequestMethod.POST)
	protected BaseResponse saveDocument(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody Document document) throws Exception {
		DbDocument dbDocument = collService.findByKey(document.getKey()).orElse(null);
		dbDocument = new GsonMapper<>(document, dbDocument, DbDocument.class).convert();
		LocalDateTime time = LocalDateTime.now();
		if (((Integer) ObjectUtils.defaultIfNull(document.getTtl(), 0)) <= 0) {
			dbDocument.setExpiry(time.plusYears(10));
		} else {
			dbDocument.setExpiry(time.plusSeconds(document.getTtl()));
		}

		collService.save(dbDocument);
		return new BaseResponse();
	}

	@RequestMapping(value = "/save-backend-document", method = RequestMethod.POST)
	protected BaseResponse saveBackendDocument(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody Document document) throws Exception {
		BackendDbDocument dbDocument = backendCollService.findByKey(document.getKey()).orElse(null);
		dbDocument = new GsonMapper<>(document, dbDocument, BackendDbDocument.class).convert();
		LocalDateTime time = LocalDateTime.now();
		if (((Integer) ObjectUtils.defaultIfNull(document.getTtl(), 0)) <= 0) {
			dbDocument.setExpiry(time.plusYears(10));
		} else {
			dbDocument.setExpiry(time.plusSeconds(document.getTtl()));
		}

		backendCollService.save(dbDocument);
		return new BaseResponse();
	}

	@RequestMapping(value = "/fetch-documents/{query}", method = RequestMethod.GET)
	protected CollectionServiceResponse saveDocument(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("query") String type) throws Exception {
		CollectionServiceResponse serviceResponse = new CollectionServiceResponse();
		serviceResponse.setDocuments(new ArrayList<>());
		collService.findByTypeAndExpiryGreaterThan(type, LocalDateTime.now()).forEach(document -> {
			serviceResponse.getDocuments().add(new GsonMapper<>(document, Document.class).convert());
		});
		return serviceResponse;
	}

	@RequestMapping(value = "/fetch-backend-documents/{query}", method = RequestMethod.GET)
	protected CollectionServiceResponse saveBackendDocument(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("query") String type) throws Exception {
		CollectionServiceResponse serviceResponse = new CollectionServiceResponse();
		serviceResponse.setDocuments(new ArrayList<>());
		backendCollService.findByTypeAndExpiryGreaterThan(type, LocalDateTime.now()).forEach(document -> {
			serviceResponse.getDocuments().add(new GsonMapper<>(document, Document.class).convert());
		});
		return serviceResponse;
	}

	@RequestMapping(value = "/fetch-documents", method = RequestMethod.POST)
	protected CollectionServiceResponse fetchDocumentFromCache(HttpServletRequest request, HttpServletResponse response,
			@RequestBody CollectionServiceFilter filter) throws Exception {
		List<Document> docList = new ArrayList<>();
		docList.addAll(collService.find(filter));
		return collService.getServiceResponse(docList, filter);
	}

	@RequestMapping(value = "/fetch-backend-documents", method = RequestMethod.POST)
	protected CollectionServiceResponse fetchBackendDocumentFromCache(HttpServletRequest request,
			HttpServletResponse response, @RequestBody CollectionServiceFilter filter) throws Exception {
		List<Document> docList = new ArrayList<>();
		docList.addAll(backendCollService.find(filter));
		return backendCollService.getServiceResponse(docList, filter);
	}


	@RequestMapping(value = "/audit", method = RequestMethod.POST)
	protected SystemAuditResponse doAuditAnalysis(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid SystemAuditFilter auditFilter, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			SystemAuditResponse auditResponse = new SystemAuditResponse();
			auditResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return auditResponse;
		} else {
			auditHandler.initData(auditFilter, new SystemAuditResponse());
			return auditHandler.getResponse();
		}

	}

	@RequestMapping(value = "/countries", method = RequestMethod.POST)
	protected CountryResponse getCountries(HttpServletRequest request, HttpServletResponse response) throws Exception {
		CountryResponse countryResponse = new CountryResponse();
		countryResponse.setCountryInfos(countryInfoService.findAll());
		return countryResponse;
	}

	@RequestMapping(value = "/fetch-gstinfo", method = RequestMethod.POST)
	protected BillingEntityResponse fetchGstInfo() throws Exception {
		List<GstInfo> gstInfo =
				billingEntityService.findByUserId(SystemContextHolder.getContextData().getUser().getParentUserId());
		BillingEntityResponse response = new BillingEntityResponse();
		response.setGstInfo(gstInfo);
		return response;
	}

	@RequestMapping(value = "/fetch-traveller", method = RequestMethod.POST)
	protected TravellerInfoResponse fetchTravellerInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody TravellerInfoFilter filter) throws Exception {

		TravellerInfoResponse travellerInfoResponse = new TravellerInfoResponse();
		List<DbTravellerInfo> travellerList =
				travellerService.find(filter, SystemContextHolder.getContextData().getUser().getParentUserId());
		for (DbTravellerInfo dbTravellerInfo : travellerList) {
			if (BooleanUtils.isTrue(dbTravellerInfo.getEnabled())) {
				travellerInfoResponse.getTravellerInfos().add(dbTravellerInfo.getTravellerData());
			}
		}
		return travellerInfoResponse;
	}

	@RequestMapping(value = "/bulk-upload-response", method = RequestMethod.POST)
	protected BulkUploadResponse getBulkUploadResponse(HttpServletRequest request, HttpServletResponse response,
			@RequestBody BulkUploadRequest uploadRequest) throws Exception {
		BulkUploadResponse bulkResponse = BulkUploadUtils.getCachedBulkUploadResponse(uploadRequest.getUploadId());
		return bulkResponse;
	}

	@RequestMapping(value = "/upload-documents", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadDocuments(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody CollectionUpdateRequest updateRequest) throws Exception {
		return collectionManager.uploadDocument(updateRequest);
	}

	@RequestMapping(value = "/reload-abusefilter", method = RequestMethod.GET)
	protected @ResponseBody BaseResponse reloadAbuseControlFilter(HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		abuseControlFilter.setConfigInfoList();
		return new BaseResponse();
	}

	@RequestMapping(value = "/ui-conf", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse reloadAbuseControlFilter(HttpServletRequest request,
			@Valid @RequestBody UIConfiguration uiConfiguration) throws Exception {
		uiHelper.updateInfo(new Gson().toJson(uiConfiguration));
		return new BaseResponse();
	}

	@RequestMapping(value = "/whatsapp/consent", method = RequestMethod.POST)
	protected BaseResponse getUserConsent(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody WhatsAppConsentInfo consentRequest) throws Exception {
		consentHandler.initData(consentRequest, new BaseResponse());
		return consentHandler.getResponse();
	}

	@RequestMapping(value = "/whatsapp/consent/status", method = RequestMethod.POST)
	protected BaseResponse getNumberStatus(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody WhatsAppConsentInfo consentRequest) throws Exception {
		return consentHandler.getNumberStatus(consentRequest);
	}

	@RequestMapping(value = "/document-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditsHandler.fetchAudits(auditsRequestData, DbDocument.class, "key"));
		return auditResponse;
	}


}
