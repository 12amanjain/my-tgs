package com.tgs.services.gms.restcontroller;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.ruleengine.GeneralBasicFact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.ruleengine.IRuleOutPut;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorFilter;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.services.gms.helper.ConfiguratorHelper;
import com.tgs.services.gms.jparepository.configurator.ConfiguratorService;
import com.tgs.services.gms.restmodel.ConfigResponse;
import com.tgs.services.gms.restmodel.ConfiguratorRuleTypeResponse;
import com.tgs.services.gms.servicehandler.ConfiguratorHandler;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.User;

@RestController
@RequestMapping("gms/v1")
public class ConfiguratorController {

	@Autowired
	ConfiguratorHandler configHandler;

	@Autowired
	ConfiguratorService configService;

	@RequestMapping(value = "/config/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected ConfiguratorRuleTypeResponse saveOrUpdateConfigRule(HttpServletRequest request,
			HttpServletResponse response, @RequestBody @Valid ConfiguratorInfo configuratorInfo) throws Exception {
		configHandler.initData(configuratorInfo, new ConfiguratorRuleTypeResponse());
		return configHandler.getResponse();
	}

	@RequestMapping(value = "/config/list", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected ConfiguratorRuleTypeResponse getConfigRuleType(HttpServletRequest request,
			HttpServletResponse httpResoponse, @RequestBody @Valid ConfiguratorFilter configuratorFilter)
			throws Exception {
		ConfiguratorRuleTypeResponse ruleTypeResponse = new ConfiguratorRuleTypeResponse();
		configService.findAll(configuratorFilter).forEach(config -> {
			ruleTypeResponse.getConfiguratorInfos().add(new GsonMapper<>(config, ConfiguratorInfo.class).convert());
		});
		return ruleTypeResponse;
	}

	@RequestMapping(value = "/configuration/{type}", method = RequestMethod.GET)
	protected ConfigResponse getConfigRule(HttpServletRequest request, HttpServletResponse httpResoponse,
			@PathVariable("type") String type) throws Exception {
		ConfigResponse ruleTypeResponse = new ConfigResponse();
		ConfiguratorRuleType ruleType = ConfiguratorRuleType.getRuleType(type);
		if (ruleType != null) {
			User loggedInUser = SystemContextHolder.getContextData().getUser();
			GeneralBasicFact fact = GeneralBasicFact.builder().role(loggedInUser.getRole())
					.userId(loggedInUser.getUserId()).partnerId(loggedInUser.getPartnerId()).build();
			IRuleOutPut ruleOutput = ConfiguratorHelper.getConfigRule(ruleType, fact);
			ruleTypeResponse.setConfig(ruleOutput);
		}
		return ruleTypeResponse;
	}

	@RequestMapping(value = "/config/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse deleteConfigRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		BaseResponse baseResponse = new BaseResponse();
		
		DbConfiguratorRule configuratorRule = configService.findById(id);
		if (Objects.nonNull(configuratorRule)) {
			configuratorRule.setDeleted(true);
			configService.save(configuratorRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
