package com.tgs.services.gms.restcontroller;

import com.tgs.filters.IncidenceFilter;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import com.tgs.services.gms.jparepository.IncidenceService;
import com.tgs.services.pms.restmodel.IncidenceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.PathParam;

@RestController
@RequestMapping("/gms/v1/incidence/")
public class IncidenceController {

    @Autowired
    private IncidenceService incidenceService;

    @RequestMapping(value = "{incidenceId}", method = RequestMethod.GET)
    protected IncidenceResponse getById(@PathParam("incidenceId") String incidenceId) throws Exception {
        return new IncidenceResponse(incidenceService.getByIncidenceId(incidenceId));
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    protected BaseResponse save(HttpServletRequest request, HttpServletResponse response,
            @Valid @RequestBody Incidence incidence) throws Exception {
        incidenceService.save(incidence);
        return new BaseResponse();
    }

    @RequestMapping(value = "search", method = RequestMethod.POST)
    protected IncidenceResponse search(HttpServletRequest request, HttpServletResponse response,
             @Valid @RequestBody IncidenceFilter filter) throws Exception {
        return new IncidenceResponse(incidenceService.search(filter));
    }
}
