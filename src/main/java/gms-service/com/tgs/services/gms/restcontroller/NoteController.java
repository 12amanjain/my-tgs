package com.tgs.services.gms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.NoteAdditionalInfoFilter;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.jparepository.NoteService;
import com.tgs.services.gms.restmodel.NoteResponse;
import com.tgs.services.gms.servicehandler.NoteHandler;

@RestController
@RequestMapping("/gms/v1")
public class NoteController {

	@Autowired
	NoteService noteService;

	@Autowired
	NoteHandler noteHandler;

	@RequestMapping(value = "/note/fetch", method = RequestMethod.POST)
	protected NoteResponse getOrderNotes(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody NoteFilter noteFilterRequest) throws Exception {
		if (noteFilterRequest.getAdditionalInfo() == null && !UserUtils.isMidOfficeRole(
				UserUtils.getEmulatedUserRoleOrUserRoleCode(SystemContextHolder.getContextData().getUser()))) {
            if(noteFilterRequest.getAdditionalInfo()==null) {
            	noteFilterRequest.setAdditionalInfo(NoteAdditionalInfoFilter.builder().build());
            }
            noteFilterRequest.getAdditionalInfo().setVisibleToAgent(true);
		}
		noteHandler.initData(noteFilterRequest, new NoteResponse());
		return noteHandler.getResponse();
	}

	@RequestMapping(value = "/note/add", method = RequestMethod.POST)
	protected NoteResponse saveOrderNote(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody Note note) throws Exception {
		note.setUserId(UserUtils.getUserId(SystemContextHolder.getContextData().getUser()));
		NoteResponse noteResponse = new NoteResponse();
		Note savedNotes = noteService.save(note);
		noteResponse.getOrderNotes().add(savedNotes);
		return noteResponse;
	}

}
