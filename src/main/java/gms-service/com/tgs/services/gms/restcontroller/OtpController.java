package com.tgs.services.gms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.gms.restmodel.OtpTokenResponse;
import com.tgs.services.base.gms.OtpValidateRequest;
import com.tgs.services.gms.servicehandler.GenericOtpServiceHandler;
import com.tgs.services.gms.servicehandler.OtpServiceHandler;
import com.tgs.services.gms.servicehandler.OtpValidateHandler;

@RestController
@RequestMapping("/gms/v1")
public class OtpController {

	@Autowired
	OtpServiceHandler otpHandler;

	@Autowired
	GenericOtpServiceHandler signInOtpHandler;

	@Autowired
	OtpValidateHandler validateHandler;
	
	@RequestMapping(value = "/otp-generate", method = RequestMethod.POST)
	protected OtpTokenResponse generateOtp(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody OtpToken otpRequest) throws Exception {
		otpHandler.initData(otpRequest, new OtpTokenResponse());
		return otpHandler.getResponse();
	}

	@RequestMapping(value = "/generate-otp", method = RequestMethod.POST)
	protected OtpTokenResponse generateSignInOtp(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody OtpToken otpRequest) throws Exception {
		signInOtpHandler.initData(otpRequest, new OtpTokenResponse());
		return signInOtpHandler.getResponse();
	}

	@RequestMapping(value = "/otp/validate", method = RequestMethod.POST)
	protected BaseResponse validateOtp(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody OtpValidateRequest otpValidateRequest) throws Exception {
		validateHandler.initData(otpValidateRequest, new BaseResponse());
		return validateHandler.getResponse();
	}
	
}
