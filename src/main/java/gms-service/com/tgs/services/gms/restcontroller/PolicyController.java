package com.tgs.services.gms.restcontroller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.gms.datamodel.PolicyFilter;
import com.tgs.services.gms.datamodel.PolicyInfo;
import com.tgs.services.gms.jparepository.PolicyInfoService;
import com.tgs.services.gms.restmodel.PolicyInfoResponse;

@RestController
@RequestMapping("/gms/v1")
public class PolicyController {

	@Autowired
	PolicyInfoService plyService;

	@Autowired
	private AuditsHandler auditHandler;
	 
	@RequestMapping(value = "/policy-save", method = RequestMethod.POST)
	protected PolicyInfoResponse addPolicy(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody PolicyInfo policyInfo) throws Exception {
		com.tgs.services.gms.dbmodel.PolicyInfo dbInfo = null;
		if (policyInfo.getId() != null) {
			dbInfo = plyService.findById(policyInfo.getId()).orElse(null);
		}
		plyService.save(
				new GsonMapper<>(policyInfo, dbInfo, com.tgs.services.gms.dbmodel.PolicyInfo.class).convert());
		PolicyInfoResponse res = new PolicyInfoResponse();
		res.setPolicyInfos(new ArrayList<>());
		res.getPolicyInfos().add(policyInfo);
		return res;
	}

	@RequestMapping(value = "/policies", method = RequestMethod.POST)
	protected PolicyInfoResponse list(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody PolicyFilter policyFilter) throws Exception {
		PolicyInfoResponse res = new PolicyInfoResponse();
		res.setPolicyInfos(new ArrayList<>());
		plyService.findAll(policyFilter)
				.forEach(policy -> res.getPolicyInfos().add(new GsonMapper<>(policy, PolicyInfo.class).convert()));
		return res;
	}
	
	@RequestMapping(value = "/policies-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = { UserIdResponseProcessor.class })
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse =  new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData,com.tgs.services.gms.dbmodel.PolicyInfo.class,""));
		return auditResponse;
	}

}
