package com.tgs.services.gms.runtime;

import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.gms.base.ITokenStore;
import com.tgs.services.gms.dbmodel.OtpToken;
import com.tgs.services.gms.jparepository.OtpService;

@Service
public class RdsTokenStore implements ITokenStore {

	@Autowired
	private OtpService otpService;

	@Override
	public OtpToken generateToken(OtpToken otpToken) {
		return otpService.save(otpToken);
	}

	@Override
	public OtpToken validateToken(String requestId, String otp, LocalDateTime time) {
		return otpService.findByRequestIdAndOTP(requestId, otp, time);
	}

	@Override
	public OtpToken updateOtp(OtpToken otpToken) {
		return otpService.save(otpToken);
	}

	@Override
	public OtpToken findTokenByRequestId(String requestId) {
		return otpService.findByRequestId(requestId);
	}

	/**
	 * This will return the unused otp for the user with email @param email and
	 * mobile @param mobile, based on the constraints OtpToken.isconsumed is false
	 * and OtpToken.attemptcount < 3 and should be less than 30 minutes old.
	 * 
	 * @param email
	 * @param mobile
	 * @return
	 */
	@Override
	public OtpToken getUnUtilizedOtpForUser(String email, String mobile, LocalDateTime time) {
		return otpService.findUnutilizedOtpForUser(email, mobile, time);
	}

}
