package com.tgs.services.gms.servicehandler;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.gms.datamodel.CityInfoFilter;
import com.tgs.services.gms.helper.CityHelper;
import com.tgs.services.gms.restmodel.CityResponse;

@Service
public class CityHandler extends ServiceHandler<CityInfoFilter, CityResponse> {

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		List<CityInfo> cityInfoList = CityHelper.searchInCache(request);
		if (CollectionUtils.isNotEmpty(cityInfoList)) {
			response.setCityInfos(new ArrayList<>());
			for (CityInfo cityInfo : cityInfoList) {
				response.getCityInfos()
						.add(new GsonMapper<>(cityInfo, com.tgs.services.base.datamodel.CityInfo.class).convert());
			}
		}

	}

}
