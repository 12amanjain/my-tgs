package com.tgs.services.gms.servicehandler;

import java.util.Objects;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.dbmodel.DbConfiguratorRule;
import com.tgs.services.gms.jparepository.configurator.ConfiguratorService;
import com.tgs.services.gms.restmodel.ConfiguratorRuleTypeResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ConfiguratorHandler extends ServiceHandler<ConfiguratorInfo, ConfiguratorRuleTypeResponse> {

	@Autowired
	ConfiguratorService configuratorService;

	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		DbConfiguratorRule rule = null;
		if (Objects.nonNull(request.getId())) {
			rule = configuratorService.findById(request.getId());
		}
		rule = Optional.ofNullable(rule).orElseGet(() -> new DbConfiguratorRule());
		if (rule.getRuleType() != null && request.getRuleType() != null
				&& !rule.getRuleType().equals(request.getRuleType().getName())) {
			throw new CustomGeneralException(SystemError.RULE_TYPE_MISMATCH);
		}
		try {
			rule = rule.from(request);
			rule = configuratorService.save(rule);
			request.setId(rule.getId());
			syncService.sync("gms", rule.toDomain());
		} catch (Exception e) {
			log.error("Unable to save configuration", e);
			throw new Exception("Save or Update failed", e);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		response.getConfiguratorInfos().add(request);
	}
}
