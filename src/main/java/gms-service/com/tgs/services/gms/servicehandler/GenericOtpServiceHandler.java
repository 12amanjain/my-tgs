package com.tgs.services.gms.servicehandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.gms.base.ITokenStore;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.gms.manager.OtpServiceManager;
import com.tgs.services.gms.restmodel.OtpTokenResponse;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GenericOtpServiceHandler extends ServiceHandler<OtpToken, OtpTokenResponse> {

	@Autowired
	ITokenStore tokenstore;

	com.tgs.services.gms.dbmodel.OtpToken otpToken;

	@Autowired
	MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	UserServiceCommunicator userService;

	@Autowired
	OtpServiceManager otpManager;

	@Override
	public void beforeProcess() throws Exception {
		otpToken = new GsonMapper<>(request, com.tgs.services.gms.dbmodel.OtpToken.class).convert();
	}

	@Override
	public void process() throws Exception {
		try {
			String partnerId = SystemContextHolder.getContextData().getHttpHeaders().getPartnerId();
			User user = null;
			if (StringUtils.isNotEmpty(request.getKey())) {
				user = otpManager.fetchUserForSendingOtp(request);
			}
			String email = request.getEmail() != null ? request.getEmail() : user != null ? user.getEmail() : null;
			String mobile = request.getMobile() != null ? request.getMobile() : user != null ? user.getMobile() : null;
			String suffix = user != null ? user.getUserId() : null;
			otpToken.setEmail(email);
			otpToken.setMobile(mobile);
			generateOtp(suffix);
			sendEmail(partnerId);
			sendSms(partnerId);

		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to generate otp token for request {}", request, e);
			if (Objects.nonNull(e.getCause()) && e.getCause().getCause() instanceof PSQLException) {
				throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST_ID);
			}
			throw e;
		}
	}

	/**
	 * @implspec System will generate same otp for the user if the user has any unused otp. This logic should be removed
	 *           once the everything is stable from client side as this logic has some security issues.
	 * 
	 * @param suffix
	 */
	private void generateOtp(String suffix) {
		com.tgs.services.gms.dbmodel.OtpToken storedOtp = tokenstore.findTokenByRequestId(request.getRequestId());
		if (storedOtp == null && suffix != null) {
			storedOtp = tokenstore.findTokenByRequestId(request.getRequestId() + "_" + suffix);
		}

		if (storedOtp == null) {
			otpToken.setOtp(TgsStringUtils.generateOTP(6));
			otpToken = tokenstore.generateToken(otpToken);
			return;
		}
		if (storedOtp.isConsumed()) {
			log.error("Duplicate Request Id received for consumed otp, request id is {}", request.getRequestId());
			throw new CustomGeneralException(SystemError.OTP_ALREADY_USED);
		}

		if (storedOtp.getAttemptCount() < ObjectUtils.firstNonNull(request.getMaxAttemptCount(), 3)
				&& !storedOtp.getCreatedOn().isBefore(LocalDateTime.now()
						.minusMinutes(ObjectUtils.firstNonNull(request.getExpiryTimeInMinutes(), 30)))) {
			otpToken = storedOtp;
		} else {
			throw new CustomGeneralException(SystemError.OTP_LIMIT_EXCEEDED);
		}
	}

	private void sendEmail(String partnerId) {
		if (EmailValidator.getInstance().isValid(otpToken.getEmail())) {
			AbstractMessageSupplier<EmailAttributes> emailAttributeSupplier =
					new AbstractMessageSupplier<EmailAttributes>() {
						@Override
						public EmailAttributes get() {
							EmailAttributes msgAttr = EmailAttributes.builder().toEmailId(otpToken.getEmail())
									.key(request.getType() + "_EMAIL").value(otpToken.getOtp()).partnerId(partnerId)
									.role(request.getRole()).addDateTimeInSubject(true).build();
							return msgAttr;
						}
					};
			msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
		}
	}

	private void sendSms(String partnerId) {
		if (StringUtils.isNotBlank(otpToken.getMobile())) {
			AbstractMessageSupplier<SmsAttributes> smsAttributeSupplier = new AbstractMessageSupplier<SmsAttributes>() {
				@Override
				public SmsAttributes get() {
					Map<String, String> attributes = new HashMap<>();
					attributes.put("otp", otpToken.getOtp());
					SmsAttributes smsAttr = SmsAttributes.builder().key(request.getType() + "_SMS")
							.recipientNumbers(Arrays.asList(otpToken.getMobile())).attributes(attributes)
							.partnerId(partnerId).role(request.getRole()).build();
					return smsAttr;
				}
			};
			msgSrvCommunicator.sendMessage(smsAttributeSupplier.getAttributes());
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.setOtpToken(request);
	}

}
