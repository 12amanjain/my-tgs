package com.tgs.services.gms.servicehandler;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.jparepository.NoteService;
import com.tgs.services.gms.restmodel.NoteResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;

@Service
public class NoteHandler extends ServiceHandler<NoteFilter, NoteResponse> {

	@Autowired
	NoteService noteService;

	@Autowired
	UserServiceCommunicator userComm;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		List<Note> notes = noteService.findAll(request);
		List<String> userIds = notes.stream().map(Note::getUserId).collect(Collectors.toList());
		Map<String, User> userMap =
				userComm.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(userIds)).build());

		for (Note note : notes) {
			if (userMap.get(note.getUserId()) != null)
				note.setUserName(userMap.get(note.getUserId()).getName());
		}
		response.setNotes(notes);
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
