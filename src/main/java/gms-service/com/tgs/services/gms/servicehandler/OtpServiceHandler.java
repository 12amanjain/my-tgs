package com.tgs.services.gms.servicehandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.base.ITokenStore;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.gms.manager.OtpServiceManager;
import com.tgs.services.gms.restmodel.OtpTokenResponse;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OtpServiceHandler extends ServiceHandler<OtpToken, OtpTokenResponse> {

	@Autowired
	ITokenStore tokenstore;

	com.tgs.services.gms.dbmodel.OtpToken otpToken;

	@Autowired
	MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	OtpServiceManager otpManager;

	@Override
	public void beforeProcess() throws Exception {
		otpToken = new GsonMapper<>(request, com.tgs.services.gms.dbmodel.OtpToken.class).convert();
	}

	@Override
	public void process() throws Exception {
		try {
			User user = otpManager.fetchUserForSendingOtp(request);
			if (Objects.isNull(user)) {
				throw new CustomGeneralException(SystemError.INVALID_EMAIL_OR_MOBILE);
			}
			otpToken.setEmail(user.getEmail());
			otpToken.setMobile(user.getMobile());
			generateOtp(user);
			sendEmail(user);
			sendSms(user);
		} catch (Exception e) {
			log.error("Unable to generate otp token for request {}", request, e);
			if (Objects.nonNull(e.getCause()) && e.getCause().getCause() instanceof PSQLException) {
				throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST_ID);
			}
			throw e;
		}
	}

	/**
	 * @implspec System will generate same otp for the user if the user has any unused otp. This logic should be removed
	 *           once the everything is stable from client side as this logic has some security issues.
	 * 
	 * @param user
	 */
	private void generateOtp(User user) {
		com.tgs.services.gms.dbmodel.OtpToken unutilizedOtp = getUnutilizedOTP(user);
		otpToken.setOtp((unutilizedOtp != null) ? unutilizedOtp.getOtp() : TgsStringUtils.generateOTP(6));
		otpToken = tokenstore.generateToken(otpToken);
	}

	private com.tgs.services.gms.dbmodel.OtpToken getUnutilizedOTP(User user) {
		LocalDateTime time = LocalDateTime.now();
		time = time.minusMinutes(30);
		return tokenstore.getUnUtilizedOtpForUser(user.getEmail(), user.getMobile(), time);
	}

	private void sendEmail(User user) {
		if (EmailValidator.getInstance().isValid(otpToken.getEmail())) {
			AbstractMessageSupplier<EmailAttributes> emailAttributeSupplier =
					new AbstractMessageSupplier<EmailAttributes>() {
						@Override
						public EmailAttributes get() {
							EmailAttributes msgAttr = EmailAttributes.builder().toEmailId(otpToken.getEmail())
									.key(EmailTemplateKey.OTPGENERATION_EMAIL.name()).value(otpToken.getOtp())
									.partnerId(UserUtils.getPartnerUserId(user)).role(user.getRole())
									.addDateTimeInSubject(true).build();
							return msgAttr;
						}
					};
			msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
		}
	}

	private void sendSms(User user) {
		if (StringUtils.isNotBlank(otpToken.getMobile())) {
			AbstractMessageSupplier<SmsAttributes> smsAttributeSupplier = new AbstractMessageSupplier<SmsAttributes>() {
				@Override
				public SmsAttributes get() {
					Map<String, String> attributes = new HashMap<>();
					attributes.put("type", otpToken.getType());
					attributes.put("otp", otpToken.getOtp());
					SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.OTP_SMS.name())
							.recipientNumbers(Arrays.asList(otpToken.getMobile())).attributes(attributes)
							.partnerId(UserUtils.getPartnerUserId(user)).role(user.getRole()).build();
					return smsAttr;
				}
			};
			msgSrvCommunicator.sendMessage(smsAttributeSupplier.getAttributes());
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.setOtpToken(request);
	}

}
