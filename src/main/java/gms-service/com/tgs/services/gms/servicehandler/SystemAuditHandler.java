package com.tgs.services.gms.servicehandler;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.gms.datamodel.systemaudit.SystemAuditFilter;
import com.tgs.services.gms.dbmodel.DbSystemAudit;
import com.tgs.services.gms.jparepository.systemaudit.SystemAuditService;
import com.tgs.services.gms.restmodel.systemaudit.SystemAuditResponse;

@Service
public class SystemAuditHandler extends ServiceHandler<SystemAuditFilter, SystemAuditResponse> {


    @Autowired
    SystemAuditService auditService;

    @Override
    public void beforeProcess() throws Exception {

    }

    @Override
    public void process() throws Exception {
    	response.getAuditList().addAll(DbSystemAudit.toDomainList(auditService.findAll(request)));
    }

    @Override
    public void afterProcess() throws Exception {

    }
}