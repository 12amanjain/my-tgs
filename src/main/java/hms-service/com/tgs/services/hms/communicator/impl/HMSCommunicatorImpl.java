package com.tgs.services.hms.communicator.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.analytics.HotelBookingAnalyticsInfo;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.helper.HotelBookingCancellation;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.manager.HMSVoucherManager;
import com.tgs.services.hms.manager.HotelBookingEngine;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.manager.HotelUserFeeManager;
import com.tgs.services.hms.manager.mapping.HotelRoomInfoSaveManager;
import com.tgs.services.hms.mapper.HotelBookToAnalyticsHotelBookMapper;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.points.datamodel.HotelRedeemPointData;
import com.tgs.services.points.restmodel.HotelRedeemPointResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;
import com.tgs.services.vms.restmodel.HotelVoucherValidateRequest;
import com.tgs.services.vms.restmodel.HotelVoucherValidateResponse;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class HMSCommunicatorImpl implements HMSCommunicator {

	@Autowired
	HotelBookingEngine bookingEngine;

	@Autowired
	HotelBookingCancellation bookingCancellation;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	@Autowired
	OrderServiceCommunicator orderCommunicator;

	@Autowired
	HotelUserFeeManager userFeeManager;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HotelRoomInfoSaveManager roomInfoSaveManager;

	@Autowired
	@Lazy
	HotelPointManager pointManager;

	@Autowired
	@Lazy
	HMSVoucherManager voucherManager;

	@Override
	public HotelInfo getHotelInfo(String bookingId) {
		return bookingEngine.getHotelInfo(bookingId);
	}

	@Override
	public double getMarkup(String bookingId) {
		HotelInfo hInfo = getHotelInfo(bookingId);
		double totalMarkup = 0;
		for (RoomInfo rInfo : hInfo.getOptions().get(0).getRoomInfos()) {
			totalMarkup += rInfo.getTotalFareComponents().getOrDefault(FareComponent.MU, 0d);
		}
		return totalMarkup;
	}

	@Override
	public HotelSearchResult getCrossSellHotelSearchResult(HotelSearchQuery searchQuery) {
		HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
		searchManager.setCountryAndCityFromIATA(searchQuery);
		if (StringUtils.isBlank(searchQuery.getSearchCriteria().getCityId()))
			return null;
		// searchQuery.getSearchCriteria().setCityName("New Delhi");
		// searchQuery.getSearchCriteria().setCityId("110203");
		// searchQuery.getSearchCriteria().setCountryId("106");
		cacheHandler.storeCrossSellSearchQueryInCache(searchQuery);
		searchManager.search(searchQuery);
		return null;
	}

	@Override
	public void callSupplierBook(HotelInfo hInfo, String bookingId) {
		Order order = orderCommunicator.findByBookingId(bookingId);
		bookingEngine.callSupplierBook(hInfo, order);
	}

	@Override
	public boolean callSupplierCancelBooking(HotelInfo hInfo, String bookingId) {
		Order order = orderCommunicator.findByBookingId(bookingId);
		if (hInfo.getMiscInfo().getSupplierBookingReference() == null
				|| hInfo.getMiscInfo().getSupplierBookingReference().equals("HTESTREF")) {
			return true;
		}
		return bookingCancellation.cancelBooking(order, hInfo);
	}

	@Override
	public HotelImportedBookingInfo retrieveBooking(HotelImportBookingParams importBookingParams, User user)
			throws Exception {
		return bookingEngine.retrieveBooking(importBookingParams, user);
	}

	@Override
	public HotelConfiguratorInfo getHotelConfigRule(HotelConfiguratorRuleType ruleType) {
		List<HotelConfiguratorInfo> configuratorInfo = HotelConfiguratorHelper.getHotelConfiguratorRules(ruleType);
		if (CollectionUtils.isNotEmpty(configuratorInfo)) {
			return configuratorInfo.get(0);
		}
		return null;
	}

	@Override
	public HotelSupplierInfo getHotelSupplierInfo(String supplierId) {
		return HotelSupplierConfigurationHelper.getSupplierInfo(supplierId);
	}

	@Override
	public void addBookingToAnalytics(HotelBookingAnalyticsInfo analyticsInfo) {
		try {

			String supplierId = analyticsInfo.getHInfo().getOptions().get(0).getMiscInfo().getSupplierId();
			analyticsInfo.getSearchQuery().setMiscInfo(HotelSearchQueryMiscInfo.builder().supplierId(supplierId)
					.sourceId(
							String.valueOf(HotelSupplierConfigurationHelper.getSupplierInfo(supplierId).getSourceId()))
					.build());


			HotelBookToAnalyticsHotelBookMapper queryMapper =
					HotelBookToAnalyticsHotelBookMapper.builder().user(SystemContextHolder.getContextData().getUser())
							.contextData(SystemContextHolder.getContextData())
							.searchQuery(analyticsInfo.getSearchQuery()).bookingInfo(analyticsInfo).build();
			analyticsHelper.storeData(queryMapper);

		} catch (Exception e) {
			log.error("Failed to push hotel book info ", e);
		}
	}

	@Override
	public boolean callSupplierConfirmBook(HotelInfo hInfo, String bookingId) {
		Order order = orderCommunicator.findByBookingId(bookingId);
		return bookingEngine.callSupplierConfirmBook(hInfo, order);

	}

	@Override
	public HotelReviewResponse getHotelReviewResponse(String bookingId) {
		return bookingEngine.getHotelReviewResponse(bookingId);
	}

	@Override
	public void updateUserFee(HotelInfo hInfo, UserFeeAmount userFeeAmount) {
		userFeeManager.updateUserFee(hInfo, userFeeAmount);
	}

	@Override
	public HotelConfiguratorInfo getHotelConfigRule(HotelConfiguratorRuleType ruleType, HotelBasicFact fact) {
		return HotelConfiguratorHelper.getHotelConfigRule(fact, ruleType);
	}

	@Override
	public List<HotelRoomCategory> fetchRoomCategoriesById(Set<Long> ids) {
		return roomInfoSaveManager.fetchRoomCategoryListByIdList(new ArrayList<>(ids));
	}

	@Override
	public List<HotelRoomTypeInfo> fetchRoomTypesById(Set<Long> ids) {
		return roomInfoSaveManager.fetchRoomTypeListByIdList(new ArrayList<>(ids));
	}

	@Override
	public HotelRedeemPointResponse calcOrApplyPoints(HotelRedeemPointData request, boolean modifyfareDetail) {
		return pointManager.validatePoints(request, modifyfareDetail);
	}

	public HotelVoucherValidateResponse applyVoucher(HotelVoucherValidateRequest request, boolean modifyFareDetail) {
		return voucherManager.calculateVoucherDiscount(request, modifyFareDetail);
	}
	
	@Override
	public void updateBookingStatus(HotelInfo hInfo, Order order) {
	 bookingEngine.updateBookingStatus(hInfo, order);
	}

	@Override
	public void updateHotelConfirmationNumber(HotelInfo hInfo, Order order) {
		 bookingEngine.updateHotelConfirmationNumber(hInfo, order);
	}

	
}
