package com.tgs.services.hms.communicator.impl;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.PointsConfiguration;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.HotelBasicRuleField;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import com.tgs.services.points.datamodel.HotelRedeemPointData;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.datamodel.PointsOutputCriteria;
import com.tgs.services.points.restmodel.HotelRedeemPointResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelPointManager {

	@Autowired
	HMSCommunicator hmsCommunicator;

	@Autowired
	HotelOrderItemCommunicator orderItemCommunicator;

	private static Map<String, ? extends IRuleField> resolverMap = EnumUtils.getEnumMap(HotelBasicRuleField.class);

	private HotelInfo fetchHotelInfoByBookingId(String bookingId) {
		HotelInfo hInfo = null;
		try {
			HotelReviewResponse reviewResponse = hmsCommunicator.getHotelReviewResponse(bookingId);
			if (reviewResponse != null && Objects.nonNull(reviewResponse.getHInfo())) {
				hInfo = reviewResponse.getHInfo();
			}
		} catch (Exception e) {
			log.info("HotelReviewResponse not avilable for the booking id {} ", bookingId, e);
		}
		if (Objects.isNull(hInfo)) {
			hInfo = orderItemCommunicator.getHotelInfo(bookingId);
			if (Objects.isNull(hInfo)) {
				throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
			}
		}
		return hInfo;
	}

	public void calculatePointsOnHotel(HotelInfo hotelInfo, RedeemPointsResponse response, boolean modifyFareDetail,
			PointsOutputCriteria outputCriteria, HotelRedeemPointData request) {
		PointsConfiguration clientPointConfig = request.getPointsConfiguration();
		Option option = hotelInfo.getOptions().get(0);
		int totalRoomNights = BaseHotelUtils.getTotalRoomNights(option);
		Double perNightPoint = request.getPoints() / totalRoomNights;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double perNightPointAmount = 0;
				if (outputCriteria != null && StringUtils.isNotBlank(outputCriteria.getExpression())) {
					Double points =
							BaseHotelUtils.evaluateHotelExpression(outputCriteria.getExpression(), priceInfo);
					if (isMaxAllowed(request.getPoints(), points)) {
						perNightPointAmount = perNightPoint * clientPointConfig.getAmountOfOnePoint();
					}
				} else if (outputCriteria == null) {
					perNightPointAmount = perNightPoint * clientPointConfig.getAmountOfOnePoint();
				}
				if (modifyFareDetail) {
					calcOrValidateRedeemPoint(priceInfo, perNightPointAmount);
				}
				if (perNightPointAmount > 0) {
					response.setAmount(perNightPointAmount);
				}
			}
		}
	}

	private void calcOrValidateRedeemPoint(PriceInfo priceInfo, Double perPaxPointAmount) {

		priceInfo.getFareComponents().put(HotelFareComponent.RP, perPaxPointAmount);
	}

	public HotelRedeemPointResponse validatePoints(HotelRedeemPointData request, boolean modifyFareDetail) {
		ContextData contextData = SystemContextHolder.getContextData();
		HotelRedeemPointResponse response = new HotelRedeemPointResponse();
		if (StringUtils.isNotBlank(request.getBookingId())) {
			HotelInfo hotelInfo = Objects.nonNull(request.getHotelInfo()) ? request.getHotelInfo()
					: fetchHotelInfoByBookingId(request.getBookingId());
			PointsConfiguration clientPointConfig = request.getPointsConfiguration();
			HotelSearchType searchType = hotelInfo.getSearchType();
			HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromHotelInfo(hotelInfo);
			hotelFact.setUserId(UserUtils.getUserId(contextData.getUser()));
			hotelFact.setRole(contextData.getUser().getRole());
			CustomisedRuleEngine ruleEngine =
					new CustomisedRuleEngine(request.getPointRules(), hotelFact, resolverMap);
			List<PointsConfigurationRule> rules = (List<PointsConfigurationRule>) ruleEngine.fireAllRules();
			if (CollectionUtils.isNotEmpty(rules)) {
				PointsConfigurationRule pointRule = rules.get(0);
				log.info("Redeem Points Applied for {} PointConfig Rule {} ", request.getBookingId(), pointRule);
				PointsOutputCriteria outputCriteria = pointRule.getOutput();
				calculatePointsOnHotel(hotelInfo, response, modifyFareDetail, outputCriteria, request);
				log.info("Reward Point Converted to Amount for {} amount {}", request.getBookingId(),
						response.getAmount());
			}
			if (isMaxAllowed(request.getPoints(), clientPointConfig.getHotelMaxRedeemablePoints(searchType))) {
				log.info("Redeem Points Applied for {} ClientConfig Rule", request.getBookingId());
				calculatePointsOnHotel(hotelInfo, response, modifyFareDetail, null, request);
				log.info("Reward Point Converted to Amount for {} amount {}", request.getBookingId(),
						response.getAmount());
			}
			if (Objects.nonNull(hotelInfo)) {
				response.setHotelInfo(hotelInfo);
			}
			log.info("Redeem Point Applied to Hotel for {} amount {} ", request.getBookingId(), response.getAmount());
		}
		return response;
	}

	public boolean isMaxAllowed(Double userReqPoint, Double maxPoints) {
		if (userReqPoint > maxPoints) {
			String message = SystemError.MAX_ALLOWED_POINTS.getErrorDetail(String.valueOf(maxPoints)).getMessage();
			throw new CustomGeneralException(SystemError.MAX_ALLOWED_POINTS, message);
		}
		return true;
	}
}
