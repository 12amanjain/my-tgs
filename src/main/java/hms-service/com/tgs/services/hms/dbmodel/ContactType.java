package com.tgs.services.hms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.Contact;

public class ContactType extends CustomUserType {
	
	@Override
	public Class returnedClass() {
		return Contact.class;
	}

}
