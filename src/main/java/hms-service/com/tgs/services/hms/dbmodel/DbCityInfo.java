package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.CityInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@Table(name = "hotelcityinfo", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "cityName", "countryName" }) })
public class DbCityInfo extends BaseModel<DbCityInfo, CityInfo> {

	
	private String cityName;
	private String countryId;
	private String countryName;
	private String iataCode;
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Override
	public CityInfo toDomain() {
		return new GsonMapper<>(this, CityInfo.class).convert();
	}

	@Override
	public DbCityInfo from(CityInfo dataModel) {

		dataModel.setCityName(dataModel.getCityName().toUpperCase());
		dataModel.setCountryName(dataModel.getCountryName().toUpperCase());
		return new GsonMapper<>(dataModel, this, DbCityInfo.class).convert();
	}

}
