package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.CityInfoMapping;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Builder
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "hotelcityinfomapping", uniqueConstraints = {
		@UniqueConstraint(columnNames = { "cityId", "supplierName" }) })
public class DbCityInfoMapping extends BaseModel<DbCityInfoMapping, CityInfoMapping> {

	private Long cityId;
	private String supplierName;
	private String supplierCity;
	private String supplierCountry;
	private String supplierCityName;
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	
	
	@Override
	public CityInfoMapping toDomain() {
		return new GsonMapper<>(this, CityInfoMapping.class).convert();
	}

	@Override
	public DbCityInfoMapping from(CityInfoMapping dataModel) {
		return new GsonMapper<>(dataModel, this, DbCityInfoMapping.class).convert();
	}

}
