package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.CustomSerializedName;
import com.tgs.services.base.helper.FieldName;
import com.tgs.services.base.runtime.database.CustomTypes.StringArrayUserType;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@TypeDefs({@TypeDef(name = "GeoLocationType", typeClass = GeoLocationType.class),
		@TypeDef(name = "AddressType", typeClass = AddressType.class),
		@TypeDef(name = "StringArrayUserType", typeClass = StringArrayUserType.class),
		@TypeDef(name = "ImageType", typeClass = ImageType.class),
		@TypeDef(name = "InstructionType", typeClass = InstructionType.class),
		@TypeDef(name = "ContactType", typeClass = ContactType.class)})
@Table(name = "hotelinfo", uniqueConstraints = {@UniqueConstraint(columnNames = {"name", "rating"})})
public class DbHotelInfo extends BaseModel<DbHotelInfo, HotelInfo> {

	private String name;

	@SerializedName("des")
	private String description;

	@Type(type = "ImageType")
	@CustomSerializedName(key = FieldName.HOTEL_IMAGES)
	private List<Image> images;

	@SerializedName("rt")
	private String rating;

	@Type(type = "GeoLocationType")
	@SerializedName("gl")
	private GeoLocation geolocation;

	@Type(type = "AddressType")
	@SerializedName("ad")
	private Address address;

	@Type(type = "StringArrayUserType")
	@SerializedName("fl")
	private String[] facility = {};

	@SerializedName("ldes")
	private String longDescription;
	
	@SerializedName("wb")
	private String website;
	private String type;
	@CreationTimestamp
	private LocalDateTime createdOn;

	@SerializedName("gId")
	private String giataId;

	@SerializedName("pt")
	private String propertyType;
	
	@Type(type = "InstructionType")
	@SerializedName("inst")
	private List<Instruction> instructions;
	
	@Type(type = "ContactType")
	@SerializedName("cnt")
	private Contact contact;
	
	@SerializedName("taid")
	private String tripadvisorid;
	
	private String cityName;
	
	private String countryName;

	@Override
	public HotelInfo toDomain() {
		return new GsonMapper<>(this, HotelInfo.class).convert();
	}

	@Override
	public DbHotelInfo from(HotelInfo dataModel) {
		
		Address address = dataModel.getAddress();
		cityName = address.getCity().getName().toUpperCase();
		countryName = address.getCountry().getName().toUpperCase();
		dataModel.setName(dataModel.getName().toUpperCase());
		return new GsonMapper<>(dataModel, this, DbHotelInfo.class).convert();
	}
}
