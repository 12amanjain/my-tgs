package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.supplier.HotelMealBasis;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "hotelmealbasis", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
public class DbHotelMealBasis extends BaseModel<DbHotelMealBasis, HotelMealBasis> {

	
	private String fMealBasis;
	private String supplier;
	private String sMealBasis;
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	@Override
	public HotelMealBasis toDomain() {
		return new GsonMapper<>(this, HotelMealBasis.class).convert();
	}

	@Override
	public DbHotelMealBasis from(HotelMealBasis dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelMealBasis.class).convert();
	}
}
