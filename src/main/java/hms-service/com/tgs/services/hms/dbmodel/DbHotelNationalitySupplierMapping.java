package com.tgs.services.hms.dbmodel;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.HotelNationalityInfo;
import com.tgs.services.hms.datamodel.HotelNationalitySupplierMapping;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Getter
@Setter
@ToString
@TypeDefs({ @TypeDef(name = "NationalityDataType", typeClass = NationalityDataType.class)})
@Table(name = "hotelnationalitymapping", uniqueConstraints = { @UniqueConstraint(columnNames = { "supplier"}) })
public class DbHotelNationalitySupplierMapping extends BaseModel<DbHotelNationalitySupplierMapping, HotelNationalitySupplierMapping> {
	
	private String supplier;
	
	@Type(type = "NationalityDataType")
	private List<HotelNationalityInfo> data;
	
	@Override
	public HotelNationalitySupplierMapping toDomain() {
		return new GsonMapper<>(this, HotelNationalitySupplierMapping.class).convert();
	}

	@Override
	public DbHotelNationalitySupplierMapping from(HotelNationalitySupplierMapping dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelNationalitySupplierMapping.class).convert();
	}

}
