package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.CreationTimestamp;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;

import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
@Table(name = "hotelsuppliermapping", uniqueConstraints = { @UniqueConstraint(columnNames = { "hotelId", "supplierName" }) })
public class DbHotelSupplierMapping extends BaseModel<DbHotelSupplierMapping, HotelSupplierMappingInfo> {

	private Long hotelId;
	private String supplierHotelId;
	private String sourceName;
	private String supplierName;
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	@Override
	public HotelSupplierMappingInfo toDomain() {
		return new GsonMapper<>(this, HotelSupplierMappingInfo.class).convert();
	}

	@Override
	public DbHotelSupplierMapping from(HotelSupplierMappingInfo dataModel) {
		return new GsonMapper<>(dataModel, this, DbHotelSupplierMapping.class).convert();
	}
	
}
