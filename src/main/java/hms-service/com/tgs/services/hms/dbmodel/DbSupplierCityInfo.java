package com.tgs.services.hms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.hms.datamodel.SupplierCityInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@ToString
@Getter
@Setter
@Table(name = "hotelsuppliercity",
		uniqueConstraints = {@UniqueConstraint(columnNames = {"cityName", "countryName", "supplierName"})})
public class DbSupplierCityInfo extends BaseModel<DbSupplierCityInfo, SupplierCityInfo> {

	private String cityName;
	private String countryName;
	private String cityId;
	private String countryId;
	private String supplierName;
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Override
	public SupplierCityInfo toDomain() {
		return new GsonMapper<>(this, SupplierCityInfo.class).convert();
	}

	@Override
	public DbSupplierCityInfo from(SupplierCityInfo dataModel) {

		return new GsonMapper<>(dataModel, this, DbSupplierCityInfo.class).convert();
	}
}