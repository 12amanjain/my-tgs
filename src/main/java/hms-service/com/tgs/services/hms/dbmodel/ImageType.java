package com.tgs.services.hms.dbmodel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.Image;

public class ImageType extends CustomUserType {
	
	@Override
	public Class returnedClass() {
		List<Image> imageList = new ArrayList<>();
		return imageList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<Image>>() {
		}.getType();
	}
}
