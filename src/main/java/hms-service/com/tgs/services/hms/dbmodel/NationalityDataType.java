package com.tgs.services.hms.dbmodel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.HotelNationalityInfo;

public class NationalityDataType extends CustomUserType {
	
	@Override
	public Class returnedClass() {
		List<HotelNationalityInfo> nationalityList = new ArrayList<>();
		return nationalityList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<HotelNationalityInfo>>() {
		}.getType();
	}

}
