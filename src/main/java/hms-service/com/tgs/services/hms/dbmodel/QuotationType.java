package com.tgs.services.hms.dbmodel;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.quotation.HotelQuotation;

public class QuotationType extends CustomUserType {
	
	@Override
	public Class returnedClass() {
		List<HotelQuotation> quotationList = new ArrayList<>();
		return quotationList.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<HotelQuotation>>() {
		}.getType();
	}
}
