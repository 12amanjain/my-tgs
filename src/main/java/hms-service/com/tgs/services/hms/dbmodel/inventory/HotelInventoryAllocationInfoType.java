package com.tgs.services.hms.dbmodel.inventory;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryAllocationInfo;

public class HotelInventoryAllocationInfoType extends CustomUserType {
	
	 @Override
    public Class returnedClass() {
       return HotelInventoryAllocationInfo.class;
   }
}
