package com.tgs.services.hms.dbmodel.tripadvisor;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.tripadvisor.ReviewData;

public class ReviewDataType extends CustomUserType {
	
	@Override
	public Class returnedClass() {
		List<ReviewData> reviewData = new ArrayList<>();
		return reviewData.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<ReviewData>>() {
		}.getType();
	}

}
