package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.mapper.HotelCancellationToAnalyticsHotelCancellationMapper;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;

@Service
public abstract class AbstractHotelCancellationPolicy {

	@Autowired
	HotelAnalyticsHelper analyticsHelper;
	
	public void getCancellationPolicy(HotelSearchQuery query, HotelInfo hInfo, String optionId, String logKey) {
		AbstractHotelInfoFactory factory = null;
		try {
			factory =
					HotelSourceType.getFactoryInstance(query, hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
			factory.getCancellationPolicy(query, hInfo, logKey);
		} finally {
			sendCancellationDataToAnalytics(factory, query, hInfo);
		}
	}

	private void sendCancellationDataToAnalytics(AbstractHotelInfoFactory hotelInfofactory, HotelSearchQuery searchQuery, HotelInfo hInfo) {
		SystemContextHolder.getContextData().getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
				new ArrayList<>(Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
						.time(System.currentTimeMillis()).build())));
		HotelCancellationToAnalyticsHotelCancellationMapper cancellationQueryMapper =
				HotelCancellationToAnalyticsHotelCancellationMapper.builder().searchQuery(searchQuery).user(SystemContextHolder.getContextData().getUser())
						.contextData(SystemContextHolder.getContextData()).hInfo(hInfo).build();
		analyticsHelper.storeData(cancellationQueryMapper);
	}
}
