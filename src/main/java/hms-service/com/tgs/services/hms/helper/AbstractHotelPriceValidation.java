package com.tgs.services.hms.helper;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.datamodel.FareChangeAlert;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.manager.HotelUserFeeManager;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.AmountType;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AbstractHotelPriceValidation {

	@Autowired
	HotelSearchResultProcessingManager searchResultProcessingManager;

	@Autowired
	HotelUserFeeManager userFeeManager;
	
	@Autowired
	private CommercialCommunicator commericialCommunicator;
	
	public FareChangeAlert updatePriceIfChanged(HotelSearchQuery searchQuery, HotelInfo hInfo,
			boolean isToSendFareDifference, String bookingId) throws Exception {
		Option oldOption = hInfo.getOptions().get(0);
		double oldAmount = oldOption.getTotalPrice();
		AbstractHotelPriceValidationFactory priceValidationFactory =
				HotelSourceType.getHotelPriceValidationFactoryInstance(searchQuery, hInfo, bookingId);
		Option updatedOption = priceValidationFactory.getUpdatedOption();
		if (ObjectUtils.isEmpty(updatedOption))
			throw new CustomGeneralException("Unable to validate price");
		boolean isOptionsEqual =
				HotelUtils.compareIfTwoOptionsEqual(oldOption.getRoomInfos(), updatedOption.getRoomInfos());
		User user = SystemContextHolder.getContextData().getUser();
		FareChangeAlert fareAlert = null;
		double totalMarkup = 0.0;
		if (!isOptionsEqual) {
			totalMarkup = HotelUtils.getTotalMarkupOfOption(oldOption);
			logIfRoomsWithDuplicateUniqueId(updatedOption.getRoomInfos(), searchQuery);
			updateRoomInfoWithUpdatedPriceANDCancellationPolicy(oldOption.getRoomInfos(), updatedOption.getRoomInfos());
			searchResultProcessingManager.processHotelFareComponents(hInfo, searchQuery, user);
			if (oldOption.getMiscInfo().isMarkupUpdatedByUser()) {
				userFeeManager.updateUserFeeForHotelList(hInfo, totalMarkup, AmountType.FIXED.name());
			}
			searchResultProcessingManager.updateManagementFees(hInfo);
			searchResultProcessingManager.buildProcessOptions(hInfo, searchQuery);
			BaseHotelUtils.updateTotalFareComponents(hInfo);
		} else if (isToSendFareDifference) {
			oldAmount = oldAmount - oldOption.getRoomInfos().get(0).getTotalAddlFareComponents()
					.get(HotelFareComponent.TAF).getOrDefault(HotelFareComponent.MU, 0.0);
			fareAlert = FareChangeAlert.builder().newFare(oldAmount + oldAmount * 0.01).oldFare(oldAmount)
					.type(AlertType.FAREALERT.name()).build();
		}

		checkIfExpectedBookingAmountSame(searchQuery, hInfo);

		if (!isOptionsEqual) {
			applyCommission(hInfo, user, searchQuery);
			double newAmount = hInfo.getOptions().get(0).getTotalPrice();
			fareAlert = FareChangeAlert.builder().newFare(newAmount - totalMarkup).oldFare(oldAmount - totalMarkup)
					.type(AlertType.FAREALERT.name()).build();
		}
		return fareAlert;
	}

	private void applyCommission(HotelInfo hInfo, User user, HotelSearchQuery searchQuery) {
		hInfo.getOptions().get(0).getMiscInfo().setIsCommissionApplied(false);
		commericialCommunicator.processUserCommissionInHotel(hInfo, user, searchQuery);
	}

	private void checkIfExpectedBookingAmountSame(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		OptionMiscInfo optionMiscInfo = hInfo.getOptions().get(0).getMiscInfo();
		if (optionMiscInfo != null && optionMiscInfo.getExpectedOptionBookingPrice() != null) {
			BigDecimal expectedSupplierAmount = new BigDecimal(optionMiscInfo.getExpectedOptionBookingPrice());
			BigDecimal totalAmount = BaseHotelUtils.getTotalSupplierBookingAmount(hInfo.getOptions().get(0));
			if (Math.abs(totalAmount.doubleValue() - expectedSupplierAmount.doubleValue()) >= 1) {
				log.error(
						"Expected Supplier Booking Amount {} Does Not Match With Amount To Be Paid {} For SearchId {}",
						expectedSupplierAmount, totalAmount, searchQuery.getSearchId());
				throw new CustomGeneralException("Price Mismatch Between Expected Price & Amount to Be Paid");
			}
		}
	}

	private void updateRoomInfoWithUpdatedPriceANDCancellationPolicy(List<RoomInfo> oldRoomInfoList, List<RoomInfo> newRoomInfoList) {
		

		Map<String, RoomInfo> newRoomPriceInfoMap = newRoomInfoList.stream()
				.collect(Collectors.toMap(RoomInfo :: getId, Function.identity()));

		for (RoomInfo roomInfo : oldRoomInfoList) {
			RoomInfo updatedRoom = newRoomPriceInfoMap.get(roomInfo.getId());
			List<PriceInfo> priceInfo = updatedRoom.getPerNightPriceInfos();
			if (CollectionUtils.isEmpty(priceInfo)) {
				throw new CustomGeneralException("This room is no longer available");
			}
			roomInfo.setPerNightPriceInfos(priceInfo);
			roomInfo.setCancellationPolicy(updatedRoom.getCancellationPolicy());
		}
	}

	private void logIfRoomsWithDuplicateUniqueId(List<RoomInfo> roomInfos, HotelSearchQuery searchQuery) {
		Set<String> uniqueRoomIds = new HashSet<>();
		if (roomInfos.size() > 0) {
			for (RoomInfo roomInfo : roomInfos) {
				uniqueRoomIds.add(roomInfo.getId());
			}
		}
		if (uniqueRoomIds.size() != roomInfos.size()) {
			log.info("Duplicate room ids after price change for search id {}", searchQuery.getSearchId());
		}
	}
}
