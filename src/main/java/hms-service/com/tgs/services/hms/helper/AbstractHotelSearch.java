package com.tgs.services.hms.helper;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.hms.datamodel.HotelDetailResult;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.mapper.HotelDetailToAnalyticsHotelDetailMapper;
import com.tgs.services.hms.mapper.HotelSearchToAnalyticsHotelSearchMapper;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public abstract class AbstractHotelSearch {

	@Autowired
	HotelSearchResultProcessingManager hotelSearchResultProcessingManager;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	public HotelSearchResult search(HotelSearchQuery searchQuery, ContextData contextData) {

		HotelSearchResult searchResult = null;
		AbstractHotelInfoFactory factory = null;
		Integer searchResultCount = 0;
		try {

			// To get configuration of a supplier to be used in their respective
			// implementations.
			factory = HotelSourceType.getFactoryInstance(searchQuery, searchQuery.getMiscInfo().getSupplierId());
			searchResult = factory.getAvailableHotels();
			log.info("Total hotel count before processing for supplier {} and searchId {} is {}",
					searchQuery.getMiscInfo().getSupplierId(), searchQuery.getSearchId(),
					searchResult != null ? searchResult.getNoOfHotelOptions() : 0);
			LogUtils.log(LogTypes.HOTELSEARCH_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			if (searchResult != null && CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
				searchResultCount = searchResult.getHotelInfos().size();
				hotelSearchResultProcessingManager.processSearchResult(searchResult, searchQuery);
			}
			LogUtils.log(LogTypes.HOTELSEARCH_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.supplierId(searchQuery.getMiscInfo().getSupplierId())
							.trips(searchResult != null ? searchResult.getNoOfHotelOptions() : 0).build(),
					LogTypes.HOTELSEARCH_PROCESS_START);
			log.info("Total hotel count after processing for supplier {} and searchId {} is {}",
					searchQuery.getMiscInfo().getSupplierId(), searchQuery.getSearchId(),
					searchResult != null ? searchResult.getNoOfHotelOptions() : 0);
			return searchResult;
		} finally {
			if (contextData != null) {
				sendSearchDataToAnalytics(factory, contextData, searchQuery, searchResultCount);
			}
		}
	}

	private void sendSearchDataToAnalytics(AbstractHotelInfoFactory hotelInfofactory, ContextData contextData,
			HotelSearchQuery searchQuery, Integer searchResultCount) {
		contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
				new ArrayList<>(Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
						.time(System.currentTimeMillis()).build())));
		HotelSearchToAnalyticsHotelSearchMapper queryMapper =
				HotelSearchToAnalyticsHotelSearchMapper.builder().searchQuery(searchQuery).user(contextData.getUser())
						.contextData(contextData).searchResultCount(searchResultCount)
						.basicInfo(hotelInfofactory.getSupplierConf().getBasicInfo()).build();
		analyticsHelper.storeData(queryMapper);
	}

	public void generateRandomHotels(HotelSearchResult result, HotelSearchQuery searchQuery) {
		if (searchQuery.getMinHotel() != null) {
			for (int i = result.getHotelInfos().size(); i < searchQuery.getMinHotel(); i++) {
				int randomIndex = (int) ((result.getHotelInfos().size() - 1) * Math.random());
				HotelInfo copyInfo = GsonUtils.getGson()
						.fromJson(GsonUtils.getGson().toJson(result.getHotelInfos().get(randomIndex)), HotelInfo.class);
				// copyInfo.setHotelId((RandomStringUtils.random(10, true, false)));
				copyInfo.setName(RandomStringUtils.random(20, true, false));
				copyInfo.setDescription(RandomStringUtils.random(200, true, false));
				copyInfo.setRating((int) (Math.random() * 5));
				result.getHotelInfos().add(copyInfo);
			}
		}
	}

	public void fetchDetails(HotelSearchQuery searchQuery, HotelInfo hInfo) {
		AbstractHotelInfoFactory factory = null;
		try {

			String supplier = hInfo.getOptions().get(0).getMiscInfo().getSupplierId();
			factory = HotelSourceType.getFactoryInstance(searchQuery, supplier);
			factory.fetchHotelDetails(hInfo);
			hotelSearchResultProcessingManager.processDetailResult(hInfo, searchQuery);
			hotelSearchResultProcessingManager.updateStaticData(hInfo, searchQuery, OperationType.DETAIL_SEARCH);
		} finally {
			setIsDetailHitTrue(hInfo);
			if (Objects.nonNull(factory))
				sendDetailDataToAnalytics(factory, hInfo, searchQuery);
		}
	}

	private void sendDetailDataToAnalytics(AbstractHotelInfoFactory factory, HotelInfo hInfo,
			HotelSearchQuery searchQuery) {
		ContextData contextData = SystemContextHolder.getContextData();
		contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
				new ArrayList<>(Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
						.time(System.currentTimeMillis()).build())));

		HotelDetailToAnalyticsHotelDetailMapper queryMapper = HotelDetailToAnalyticsHotelDetailMapper.builder()
				.user(SystemContextHolder.getContextData().getUser()).contextData(SystemContextHolder.getContextData())
				.detailResult(HotelDetailResult.builder().searchQuery(searchQuery).hotel(hInfo).build())
				.basicInfo(factory.getSupplierConf().getBasicInfo()).build();
		analyticsHelper.storeData(queryMapper);
	}

	private void setIsDetailHitTrue(HotelInfo hInfo) {

		for (Option option : hInfo.getOptions()) {
			option.getMiscInfo().setIsDetailHit(true);
		}
	}
}
