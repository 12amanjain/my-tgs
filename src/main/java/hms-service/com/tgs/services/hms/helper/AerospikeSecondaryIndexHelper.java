package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.aerospike.client.query.Filter;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AerospikeSecondaryIndexHelper {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	public AerospikeSecondaryIndexHelper(GeneralCachingCommunicator cachingCommunicator) {
		AerospikeSecondaryIndexHelper.cachingCommunicator = cachingCommunicator;
	}

	public static List<Map<String, String>> getDocuments(CollectionServiceFilter filter, CacheMetaInfo metaInfo) {

		Map<String, Map<String, String>> docMap = null;
		List<Map<String, String>> documents = new ArrayList<>();
		try {
			if (CollectionUtils.isNotEmpty(filter.getTypes())) {
				for (String type : filter.getTypes()) {
					docMap = cachingCommunicator.getResultSet(metaInfo, String.class,
							Filter.equal(metaInfo.getBins()[0], "\"" + type + "\""));
					if (MapUtils.isNotEmpty(docMap)) {
						for (Entry<String, Map<String, String>> entrySet : docMap.entrySet()) {
							documents.add(entrySet.getValue());
						}
					}
				}
			}
		} catch (Exception e) {
			log.error("Error in fetching documents from aerospike", e);
		}
		return documents;
	}

}
