package com.tgs.services.hms.helper;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.hms.sources.agoda.AgodaMarshaller;
import com.tgs.services.hms.sources.cleartrip.CleartripUtils;
import com.tgs.services.hms.sources.dotw.DotwMarshallerWrapper;
import com.tgs.services.hms.sources.expedia.ExpediaUtils;

@Service
public class HMSStaticContextInitializer {

	@Autowired
	Jaxb2Marshaller marshaller;
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	GeneralServiceCommunicator gnComm;

	@PostConstruct
	public void init() {
		AgodaMarshaller.init(marshaller);
		DotwMarshallerWrapper.init(marshaller);
		ExpediaUtils.init(cacheHandler, gnComm);
		CleartripUtils.init(cacheHandler);
	}
}
