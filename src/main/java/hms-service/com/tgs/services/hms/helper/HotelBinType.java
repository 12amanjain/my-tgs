package com.tgs.services.hms.helper;

import lombok.Getter;

@Getter
public enum HotelBinType {

	HOTEL_RESULT("hotel_result");

	
	private String binName;
	private HotelBinType(String binName) {
		this.binName = binName;
	}
}
