package com.tgs.services.hms.helper;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class HotelBookingCancellation {

	public boolean cancelBooking(Order order, HotelInfo hInfo) {

		AbstractHotelBookingCancellationFactory factory = HotelSourceType
				.getHotelBookingCancellationFactoryInstance(hInfo, order);
		return factory.cancelBooking();
	}

}
