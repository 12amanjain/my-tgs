package com.tgs.services.hms.helper;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.query.Filter;
import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelNationalityInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryCacheData;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelCacheHandler {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HotelInfoService hotelInfoService;

	public void cacheHotelSearchResults(HotelSearchResult searchResult, HotelSearchQuery searchQuery,
			HotelSupplierConfiguration supplierConf) {
		if (false && searchResult != null && CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
			try {
				Map<String, Object> binMap = new HashMap<>();
				binMap.put(BinName.SEARCHQUERYBIN.getName(), searchResult);

				CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
						.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(generateKey(searchQuery, supplierConf))
						.expiration(CacheType.HOTELSEARCHCACHING.getTtl()).binValues(binMap).kyroCompress(true).build();
				cacheService.store(metaInfo);

			} catch (Exception e) {
				log.error("Unable to store result in cache", e);
			}
		} else {
			log.info("Hotel search result is empty, hence can't be cached.");
		}
	}

	public HotelSearchResult getCachedSearchResult(HotelSearchQuery searchQuery,
			HotelSupplierConfiguration supplierConf) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(generateKey(searchQuery, supplierConf))
				.bins(new String[] {BinName.SEARCHQUERYBIN.getName()}).kyroCompress(true).build();
		Optional<HotelSearchResult> searchResult =
				Optional.ofNullable(cacheService.fetchValue(HotelSearchResult.class, metaInfo));
		return searchResult.orElse(null);
	}

	private String generateKey(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {

		String key = "";
		key = StringUtils.join(key, searchQuery.getRoomInfo().size(), "S");
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			key = StringUtils.join(key, room.getNumberOfAdults(), "A", room.getNumberOfChild(), "C");
		}
		key = StringUtils.join(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate(), key,
				searchQuery.getSearchCriteria().getCityId() != null ? searchQuery.getSearchCriteria().getCityId()
						: searchQuery.getSearchCriteria().getCityName(),
				"-",
				searchQuery.getSearchCriteria().getCountryId() != null ? searchQuery.getSearchCriteria().getCountryId()
						: searchQuery.getSearchCriteria().getCountryName(),
				"-", searchQuery.getSearchPreferences().getCurrency(), "-",
				searchQuery.getSearchPreferences().getAvailableonly());
		if (searchQuery.getSearchPreferences().getRatings() != null) {
			for (Integer rating : searchQuery.getSearchPreferences().getRatings()) {
				key = StringUtils.join(key, rating);
			}
		}
		return key;
	}

	public HotelInfo getCachedHotelById(String id) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(id)
				.bins(new String[] {BinName.HOTELDETAILS.getName()}).compress(true).build();
		HotelInfo hotel = cacheService.fetchValue(HotelInfo.class, metaInfo);
		if (hotel == null) {
			return null;
		}
		return hotel;
	}

	public void persistHotelInCache(HotelInfo hotel, HotelSearchQuery searchQuery) {
		if (hotel == null) {
			log.info("Hotel Can't be cached as it is empty for searchQuery {}", searchQuery);
			return;
		}
		// hotel.setId(HotelUtils.generateKey(hotel, searchQuery));
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.HOTELSEARCHCACHING.getTtl())
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_API_CACHE_SET.getName())
				.key(hotel.getId()).binValues(ImmutableMap.of(BinName.HOTELDETAILS.getName(), hotel)).compress(true)
				.build();
		cacheService.store(metaInfo);
	}

	public HotelSearchQuery getHotelSearchQueryFromCache(String key) {

		if (StringUtils.isBlank(key)) {
			return null;
		}
		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
						.key(key).bins(new String[] {BinName.SEARCHQUERYBIN.getName()}).compress(true).build();
		HotelSearchQuery searchQuery = cacheService.fetchValue(HotelSearchQuery.class, metaInfo);
		return searchQuery;
	}

	public List<HotelInfo> getHotelInfoFromAerospike(List<String> hotelIdList) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).keys(hotelIdList.toArray(new String[0])).compress(true)
				.bins(new String[] {BinName.HOTELDETAILS.getName()}).build();

		Map<String, Map<String, HotelInfo>> hInfoListMap = cachingCommunicator.get(metaInfo, HotelInfo.class);
		List<HotelInfo> hInfoList = new ArrayList<>();
		for (String key : hInfoListMap.keySet()) {
			Map<String, HotelInfo> hInfoMap = hInfoListMap.get(key);
			HotelInfo hInfo = hInfoMap.get("HOTELDETAILS");
			hInfoList.add(hInfo);
		}
		return hInfoList;

	}

	public void storeSearchResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		log.debug("No of results found for supplier {} are {}", searchQuery.getMiscInfo().getSupplierId(),
				searchResult.getHotelInfos().size());
		if (searchResult.getHotelInfos().size() == 0) {
			return;
		}

		try {
			CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(searchQuery.getSearchId())
					.expiration(CacheType.HOTELSEARCHCACHING.getTtl())
					.binValues(ImmutableMap.of(BinName.HOTELRESULTS.getName(), searchResult)).kyroCompress(true)
					.build();

			int n = getHotelInfoSizeLimit();
			if (searchResult.getHotelInfos().size() > n) {
				HotelSearchResult newSearchResult = new HotelSearchResult();
				ExecutorService storeSearchResultExecutor = Executors.newFixedThreadPool(4);
				try {
					List<List<HotelInfo>> smallerLists = TgsCollectionUtils.chunkList(searchResult.getHotelInfos(), n);
					metaInfoToStore.setBinValues(ImmutableMap.of(BinName.HOTELRESULTS.getName(), newSearchResult));
					cacheService.store(metaInfoToStore);
					AtomicInteger searchIdSuffix = new AtomicInteger(0);
					List<Future<?>> storeSearchResultTasks = new ArrayList<Future<?>>();
					for (List<HotelInfo> hInfos : smallerLists) {
						Future<?> storeSearchResultTask = storeSearchResultExecutor.submit(() -> {
							HotelSearchResult searchResultInChunk =
									new GsonMapper<>(newSearchResult, HotelSearchResult.class).convert();
							searchResultInChunk.setHotelInfos(hInfos);
							metaInfoToStore.setKey(searchQuery.getSearchId() + searchIdSuffix.get())
									.setBinValues(ImmutableMap.of(BinName.HOTELRESULTS.getName(), searchResultInChunk));
							searchIdSuffix.getAndIncrement();
							cacheService.store(metaInfoToStore);
						});
						storeSearchResultTasks.add(storeSearchResultTask);
					}

					try {
						for (Future<?> storeSearchResultTask : storeSearchResultTasks)
							storeSearchResultTask.get();
					} catch (InterruptedException | ExecutionException e) {
						log.error("Interrupted exception while persisting search result in cache for search query {}"
								+ GsonUtils.getGson().toJson(searchQuery), e);
					}
				} catch (AerospikeException e) {
					Map<String, Object> basicInfoToLog = HotelUtils.getHotelBasisInfo(newSearchResult, searchQuery);
					log.error("Unable to store search result for search query {} and hotel info {}",
							GsonUtils.getGson().toJson(searchQuery), basicInfoToLog, e);
				}
			} else {
				cacheService.store(metaInfoToStore);
			}
		} catch (AerospikeException e) {
			Map<String, Object> basicInfoToLog = HotelUtils.getHotelBasisInfo(searchResult, searchQuery);
			log.error("Unable to store search result for search query {} and hotel info {} in cache",
					GsonUtils.getGson().toJson(searchQuery), basicInfoToLog, e);
		}
	}

	private Integer getHotelInfoSizeLimit() {

		int sizeLimit = 1000;
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		if (configuratorInfo != null) {
			sizeLimit = configuratorInfo.getHotelListSizeLimit();
		}
		return sizeLimit;
	}

	public HotelSearchResult getSearchResult(String searchId) {

		log.info("Fetching search result from cache for search id {}, search result size is {}", searchId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(searchId)
				.bins(new String[] {BinName.HOTELRESULTS.getName()}).kyroCompress(true).build();

		HotelSearchResult searchResult = cacheService.fetchValue(HotelSearchResult.class, metaInfo);

		if (searchResult != null && searchResult.getHotelInfos().isEmpty()) {

			List<String> keys = new ArrayList<>();
			for (int i = 0; i < 10; i++) {
				keys.add(searchId + i);
			}
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
					.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).keys(keys.toArray(new String[0]))
					.bins(new String[] {BinName.HOTELRESULTS.getName()}).kyroCompress(true).build();

			Map<String, Map<String, HotelSearchResult>> hotelSearchResultMap =
					cachingCommunicator.get(metaInfo, HotelSearchResult.class);

			for (String key : keys) {
				Map<String, HotelSearchResult> hotelSearchResult = hotelSearchResultMap.get(key);
				if (hotelSearchResult == null) {
					break;
				} else {
					searchResult.getHotelInfos()
							.addAll(hotelSearchResult.get(BinName.HOTELRESULTS.getName()).getHotelInfos());
				}
			}
		}
		log.info("Fetched search result from cache for search id {}, search result size {}", searchId,
				searchResult == null ? "no result" : searchResult.getNoOfHotelOptions());
		return searchResult;
	}


	public HotelUserReview getHotelUserReviewById(String locationId) {

		if (StringUtils.isBlank(locationId)) {
			return null;
		}
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(locationId)
				.bins(new String[] {BinName.HOTELRATING.getName()}).kyroCompress(true).build();
		HotelUserReview hReview = cacheService.fetchValue(HotelUserReview.class, metaInfo);
		return hReview;
	}

	public void cacheHotelUserReview(HotelUserReview hReview) {

		if (hReview == null) {
			return;
		}

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(hReview.getId())
				.expiration(CacheType.HOTELRATINGCACHING.getTtl())
				.binValues(ImmutableMap.of(BinName.HOTELRATING.getName(), hReview)).kyroCompress(true).build();
		cacheService.store(metaInfo);

	}

	public void storeSearchSpiltCount(String key, Integer count) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key).expiration(300)
				.binValues(ImmutableMap.of(BinName.SPLITCOUNT.getName(), count)).build();
		cacheService.store(metaInfo);
	}

	public int getSearchSplitCount(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key)
				.bins(new String[] {BinName.SPLITCOUNT.getName()}).build();
		Integer searchcount = cacheService.fetchValue(Integer.class, metaInfo);
		return ObjectUtils.firstNonNull(searchcount, 0);
	}

	public void storeSearchCountReturned(String key, Integer count) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key).expiration(300)
				.binValues(ImmutableMap.of(BinName.SEARCHCOUNT.getName(), count)).build();
		cacheService.store(metaInfo);
	}

	public int getSearchCountReturned(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(key)
				.bins(new String[] {BinName.SEARCHCOUNT.getName()}).build();
		Integer searchcount = cacheService.fetchValue(Integer.class, metaInfo);
		return ObjectUtils.firstNonNull(searchcount, 0);
	}

	public String getLocationId(String id, String binName) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(id).bins(new String[] {binName}).kyroCompress(true)
				.build();
		String reviewId = cacheService.fetchValue(String.class, metaInfo);
		return reviewId;
	}

	public CityInfoMapping getSupplierCityInfoFromCityId(String tgsCityId, String sourceName) {
		if (tgsCityId == null)
			return null;

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.CITY_MAPPING.getName()).key(tgsCityId).bins(new String[] {sourceName}).build();
		String supplierCityMappingJson = cacheService.fetchValue(String.class, metaInfo);
		CityInfoMapping supplierCityMapping =
				GsonUtils.getGson().fromJson(supplierCityMappingJson, CityInfoMapping.class);
		return supplierCityMapping;
	}

	public CityInfo getCityInfoFromCityId(String tgsCityId) {

		if (StringUtils.isBlank(tgsCityId))
			return null;
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.CITY_MAPPING.getName()).key(tgsCityId).bins(new String[] {"CITY_INFO"}).build();
		String cityInfoJson = cacheService.fetchValue(String.class, metaInfo);
		CityInfo cityInfo = GsonUtils.getGson().fromJson(cityInfoJson, CityInfo.class);
		return cityInfo;

	}

	public void storeSearchQueryInCache(HotelSearchQuery query) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.SEARCHQUERYBIN.getName(), query);
		binMap.put(BinName.STOREAT.getName(), LocalDateTime.now().toString());
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.SEARCHQUERYBINCACHING.getTtl())
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(query.getSearchId()).binValues(binMap).compress(true).build();
		cacheService.store(metaInfo);

	}

	public void cacheSearchResultCompleteAt(HotelSearchQuery searchQuery) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.expiration(CacheType.HOTELSEARCHCACHING.getTtl()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(searchQuery.getSearchId())
				.binValues(ImmutableMap.of(BinName.COMPLETEAT.getName(), LocalDateTime.now().toString())).compress(true)
				.build();
		cacheService.store(metaInfo);
	}

	public LocalDateTime getSearchResultCompleteAt(String searchId) {

		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
						.key(searchId).bins(new String[] {BinName.COMPLETEAT.getName()}).compress(true).build();

		String completeAt = cacheService.fetchValue(String.class, metaInfo);
		return LocalDateTime.parse(completeAt);
	}

	public boolean isSearchCompleted(String searchId) {
		int splitCount = getSearchSplitCount(searchId);
		if (splitCount > 0) {
			return false;
		}
		return true;

	}

	public void removeSearchResult(HotelSearchQuery searchQuery) {

		CacheMetaInfo metaInfoToDelete = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_API_CACHE_SET.getName()).key(searchQuery.getSearchId()).build();
		cacheService.delete(metaInfoToDelete);

	}

	public void cacheHotelReviewAgainstBookingId(HotelReviewResponse reviewResponse) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(reviewResponse.getBookingId())
				.binValues(ImmutableMap.of(BinName.HOTELINFO.getName(), reviewResponse)).compress(false).build();
		cacheService.store(metaInfo);
	}

	public HotelReviewResponse getHotelReviewFromCache(String bookingId) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).bins(new String[] {BinName.HOTELINFO.getName()})
				.typeOfT(HotelReviewResponse.class).key(bookingId).compress(false).build();
		return cacheService.fetchValue(HotelReviewResponse.class, metaInfo);

	}

	public void storeCrossSellSearchQueryInCache(HotelSearchQuery searchQuery) {
		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.SEARCHQUERYBIN.getName(), searchQuery);
		binMap.put(BinName.STOREAT.getName(), LocalDateTime.now().toString());
		int expirySeconds =
				(int) Duration.between(LocalDateTime.now(), searchQuery.getCheckoutDate().atStartOfDay()).getSeconds();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(expirySeconds)
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(searchQuery.getSearchId()).binValues(binMap).compress(true).build();
		cacheService.store(metaInfo);
	}

	public void storeSearchQueryAndStartAt(HotelSearchQuery query) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.SEARCHQUERYBIN.getName(), query);
		binMap.put(BinName.STOREAT.getName(), LocalDateTime.now().toString());
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.HOTELSEARCHCACHING.getTtl())
				.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
				.key(query.getSearchId()).binValues(binMap).compress(true).build();
		cacheService.store(metaInfo);

	}


	public LocalDateTime getSearchStartAt(String searchId) {

		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_SEARCH.getName())
						.key(searchId).bins(new String[] {BinName.STOREAT.getName()}).compress(true).build();

		String startAt = cacheService.fetchValue(String.class, metaInfo);
		log.info("StartAt is {}", startAt);
		return LocalDateTime.parse(startAt);

	}


	public void storeUserReviewIdWithKey(String key, String userReviewId) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.USERREVIEWID.getName(), userReviewId);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(key).binValues(binMap).kyroCompress(true).build();
		cacheService.store(metaInfo);

	}

	public Map<String, String> getKeyHotelUserReviewSupplierIdMap(List<String> keyList) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).keys(keyList.toArray(new String[0]))
				.bins(new String[] {BinName.USERREVIEWID.getName()}).kyroCompress(true).build();
		Map<String, String> tempMap = new HashMap<>();
		Map<String, Map<String, String>> userReviewSupplierIdMap = cachingCommunicator.get(metaInfo, String.class);
		for (String key : userReviewSupplierIdMap.keySet()) {
			Map<String, String> map = userReviewSupplierIdMap.get(key);
			String userReviewSupplierId = map.get("USERREVIEWID");
			tempMap.put(key, userReviewSupplierId);
		}
		return tempMap;
	}


	public void storeBlacklistedUserReviewHotelDetail(String key) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(BinName.UNMAPPEDUR.getName(), key);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(key).binValues(binMap).kyroCompress(true).build();
		cacheService.store(metaInfo);

	}

	public CityInfo getCityAndCountryInfo(String key) {
		if (StringUtils.isBlank(key))
			return null;
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.CITY_MAPPING.getName()).bins(new String[] {"CITY_INFO"}).build();

		Map<String, Map<String, String>> cityIataMappings = cachingCommunicator.getResultSet(metaInfo, String.class,
				Filter.equal(BinName.IATA.getName(), "\"" + key + "\""));
		for (Map.Entry<String, Map<String, String>> cityIataMapping : cityIataMappings.entrySet()) {
			Map<String, String> cityInfoMap = cityIataMapping.getValue();
			CityInfo cityInfo = GsonUtils.getGson().fromJson(cityInfoMap.get("CITY_INFO"), CityInfo.class);
			return cityInfo;
		}
		return null;
	}

	public String getBlacklistedUserReviewHotelDetail(String key) {

		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_USER_REVIEW.getName()).key(key)
				.bins(new String[] {BinName.UNMAPPEDUR.getName()}).kyroCompress(true).build();

		String value = cacheService.fetchValue(String.class, metaInfo);
		return value;
	}

	public void storeSupplierNationalityMapping(String supplier, HotelNationalityInfo mappingInfo) {

		HashMap<String, Object> binMap = new HashMap<>();
		binMap.put(supplier, mappingInfo.getSupplierCountryId());
		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.NATIONALITY.getName())
						.key(mappingInfo.getCountryId()).binValues(binMap).kyroCompress(false).build();
		cacheService.store(metaInfo);

	}

	public String fetchSupplierNationalityMapping(String supplier, String countryId) {

		CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.NATIONALITY.getName())
						.key(countryId).bins(new String[] {supplier}).kyroCompress(false).build();

		String value = cacheService.fetchValue(String.class, metaInfo);
		return value;
	}

	public Map<String, String> fetchMealInfoFromAerospike(HotelSearchQuery searchQuery,
			List<String> supplierMealInfoIds) {
		Map<String, String> mealInfos = new HashMap<>();
		if (CollectionUtils.isEmpty(supplierMealInfoIds)) {
			return mealInfos;
		}
		// Max batch request limit of aerospike is 5000
		int partitionSize = 2000;
		String binName = BinName.MEALBASIS.getName();
		Set<String> supplierMealKeys =
				supplierMealInfoIds.stream().map(String::toUpperCase).collect(Collectors.toSet());
		try {
			mealInfos = fetchAggregatedChunkedMealFetch(supplierMealKeys, binName, partitionSize);
		} catch (Exception e) {
			log.debug("Unable to find meal mapping for searchQuery {}", searchQuery, e);
		}
		return mealInfos;
	}

	private Map<String, String> fetchAggregatedChunkedMealFetch(Set<String> supplierMealKeys, String binName,
			int partitionSize) {
		Map<String, String> mealInfos = new HashMap<>();
		List<Set<String>> partitionedSupplierMealKeys = HotelUtils.partitionSet(supplierMealKeys, partitionSize);
		partitionedSupplierMealKeys.stream().forEach(mealKeys -> {
			CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(CacheSetName.HOTEL_MEAL_BASIS.getName()).keys(supplierMealKeys.toArray(new String[0]))
					.bins(new String[] {binName}).build();
			Map<String, Map<String, String>> supplierhotelIdMap =
					cachingCommunicator.get(supplierMappingMetaInfo, String.class);
			for (Map.Entry<String, Map<String, String>> supplierhotelIdMapEntry : supplierhotelIdMap.entrySet()) {
				mealInfos.put(supplierhotelIdMapEntry.getKey(), supplierhotelIdMapEntry.getValue().get(binName));
			}
		});
		return mealInfos;
	}


	public void updateInventoryRatePlans(List<HotelRatePlan> ratePlanList) {

		Map<String, List<HotelRatePlan>> supplierHotelIdDateSupplierIdKeyRatePlanListValue =
				HotelUtils.getSupplierHotelIdRatePlanListValue(ratePlanList);
		List<String> keys = new ArrayList<>(supplierHotelIdDateSupplierIdKeyRatePlanListValue.keySet());
		List<HotelInventoryCacheData> cachedInventoryData = getHotelInventoryCachedDatafromKeys(keys);

		if (CollectionUtils.isNotEmpty(cachedInventoryData)) {
			List<HotelRatePlan> cachedRatePlanList = new ArrayList<>();
			cachedInventoryData.forEach(invData -> cachedRatePlanList.addAll(invData.getRatePlanList()));
			Map<String, List<HotelRatePlan>> cachedRatePlanMap =
					cachedRatePlanList.stream().collect(Collectors.groupingBy(ratePlan -> ratePlan.getSupplierHotelId()
							+ String.valueOf(ratePlan.getValidOn()) + ratePlan.getSupplierId()));

			supplierHotelIdDateSupplierIdKeyRatePlanListValue.keySet().forEach((key -> {

				List<HotelRatePlan> cachedListForKey = cachedRatePlanMap.get(key);
				if (CollectionUtils.isEmpty(cachedListForKey)) {
					storeInventoryRatePlansForKey(key, supplierHotelIdDateSupplierIdKeyRatePlanListValue.get(key));
				} else {

					Map<Long, HotelRatePlan> cachedMap = cachedListForKey.stream()
							.collect(Collectors.toMap(HotelRatePlan::getId, Function.identity()));
					List<HotelRatePlan> ratePlans = supplierHotelIdDateSupplierIdKeyRatePlanListValue.get(key);
					ratePlans.forEach(ratePlan -> {
						cachedMap.put(ratePlan.getId(), ratePlan);
					});
					List<HotelRatePlan> finalRatePlanList = new ArrayList<>();
					cachedMap.keySet().forEach(mapKey -> {
						finalRatePlanList.add(cachedMap.get(mapKey));
					});
					storeInventoryRatePlansForKey(key, finalRatePlanList);
				}
			}));

		} else {
			supplierHotelIdDateSupplierIdKeyRatePlanListValue.keySet().forEach(key -> {
				storeInventoryRatePlansForKey(key, supplierHotelIdDateSupplierIdKeyRatePlanListValue.get(key));
			});
		}
	}

	public void deleteRatePlanFromCache(HotelRatePlan ratePlan) {

		String key = HotelUtils.getRatePlanCachingKey(ratePlan);
		List<HotelInventoryCacheData> cachedInventoryData =
				getHotelInventoryCachedDatafromKeys(new ArrayList<>(Arrays.asList(key)));
		List<HotelRatePlan> cachedRatePlanList = new ArrayList<>();
		cachedInventoryData.forEach(invData -> cachedRatePlanList.addAll(invData.getRatePlanList()));
		List<HotelRatePlan> updatedRatePlanList =
				cachedRatePlanList.stream().filter(rp -> rp.getId() != ratePlan.getId()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(updatedRatePlanList)) {
			deleteKeyFromCache(key);
			return;
		}
		storeInventoryRatePlansForKey(key, updatedRatePlanList);

	}

	private void deleteKeyFromCache(String key) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_INVENTORY.getName()).key(key).build();
		cacheService.delete(metaInfo);

	}

	public void storeInventoryRatePlansForKey(String key, List<HotelRatePlan> ratePlanList) {

		String supplierHotelId = ratePlanList.get(0).getSupplierHotelId();
		LocalDate validOn = ratePlanList.get(0).getValidOn();
		String supplierId = ratePlanList.get(0).getSupplierId();
		Map<String, Object> binValues = new HashMap<>();
		HotelInventoryCacheData data = HotelInventoryCacheData.builder().ratePlanList(ratePlanList)
				.supplierHotelId(supplierHotelId).validOn(validOn).supplierId(supplierId).build();
		binValues.put(BinName.H_RATEPLAN.name(), GsonUtils.getGson().toJson(data));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_INVENTORY.getName()).key(key).expiration(-1).binValues(binValues).build();
		cacheService.store(metaInfoToStore);

	}

	public List<HotelInventoryCacheData> getHotelInventoryCachedDatafromKeys(List<String> keys) {

		List<HotelInventoryCacheData> hotelInventoryCacheDataList = new ArrayList<>();

		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_INVENTORY.getName()).keys(keys.toArray(new String[0]))
				.bins(new String[] {BinName.H_RATEPLAN.name()}).build();
		Map<String, Map<String, String>> hotelInventoryCacheMap =
				cachingCommunicator.get(supplierMappingMetaInfo, String.class);

		for (Map.Entry<String, Map<String, String>> hotelInventoryCacheMapEntry : hotelInventoryCacheMap.entrySet()) {
			hotelInventoryCacheDataList.add(
					GsonUtils.getGson().fromJson(hotelInventoryCacheMapEntry.getValue().get(BinName.H_RATEPLAN.name()),
							HotelInventoryCacheData.class));
		}
		return hotelInventoryCacheDataList;
	}

	public void storeRoomCategoryList(List<HotelRoomCategory> rcList) {

		rcList.forEach(rcInfo -> {
			Long key = rcInfo.getId();
			Map<String, Object> binValues = new HashMap<>();
			binValues.put(BinName.ROOMCATEGORY.name(), rcInfo.getRoomCategory());
			CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(CacheSetName.HOTEL_INVENTORY.getName()).key(String.valueOf(key)).expiration(-1)
					.binValues(binValues).build();
			cacheService.store(metaInfoToStore);
		});
	}

	public void storeSupplierMappingInCache(HotelSupplierMappingInfo supplierMapping) {

		DbHotelInfo hotelInfo = hotelInfoService.findById(supplierMapping.getHotelId());
		storeSupplierMappingInCache(supplierMapping, hotelInfo);
	}

	public void storeSupplierMappingInCache(HotelSupplierMappingInfo supplierMapping, DbHotelInfo hotelInfo) {

		try {
			String cityName = "";
			String countryName = "";
			String starRating = "";
			if (!Objects.isNull(hotelInfo)) {
				cityName = hotelInfo.getAddress().getCity().getName().toLowerCase();
				if (!Objects.isNull(hotelInfo.getAddress().getCountry()))
					countryName = hotelInfo.getAddress().getCountry().getName().toLowerCase();
				starRating = hotelInfo.getRating();
			}

			String binName = HotelUtils.getSupplierBinName(supplierMapping.getSourceName());
			String setName = HotelUtils.getSupplierSetName(supplierMapping.getSourceName());
			Map<String, Object> binValues = new HashMap<>();
			binValues.put(binName, supplierMapping.getHotelId());
			binValues.put(BinName.CITY.name(), cityName);
			binValues.put(BinName.COUNTRY.name(), countryName);
			binValues.put(BinName.RATING.name(), starRating);
			binValues.put(BinName.HOTELNAME.name(), hotelInfo.getName());
			CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
					.set(setName).key(String.valueOf(supplierMapping.getSupplierHotelId())).expiration(-1)
					.binValues(binValues).build();
			cacheService.store(metaInfoToStore);
		} catch (Exception e) {
			log.error("Unable to save hotel static data mapping into aerospike", e);
		}
	}


	private static HotelInfo getBasicInfo(HotelInfo hotelInfo) {

		HotelInfo basicHotelInfo = HotelInfo.builder().id(hotelInfo.getId()).address(hotelInfo.getAddress())
				.name(hotelInfo.getName()).rating(hotelInfo.getRating()).giataId(hotelInfo.getGiataId())
				.userReviewSupplierId(hotelInfo.getUserReviewSupplierId())
				.images(CollectionUtils.isEmpty(hotelInfo.getImages()) ? null
						: new ArrayList<>(Arrays.asList(hotelInfo.getImages().get(0))))
				.geolocation(hotelInfo.getGeolocation()).build();
		return basicHotelInfo;
	}

	private static HotelInfo getDetailInfo(HotelInfo hotelInfo) {

		HotelInfo detailHotelInfo = HotelInfo.builder().id(hotelInfo.getId()).description(hotelInfo.getDescription())
				.longDescription(hotelInfo.getLongDescription()).instructions(hotelInfo.getInstructions())
				.facilities(hotelInfo.getFacilities()).images(hotelInfo.getImages()).website(hotelInfo.getWebsite())
				.propertyType(hotelInfo.getPropertyType()).contact(hotelInfo.getContact()).build();
		return detailHotelInfo;
	}

	public void storeMasterHotel(HotelInfo hotelInfo) {

		Map<String, Object> binValues = new HashMap<>();
		binValues.put(BinName.HOTEL_INFO.getName(), GsonUtils.getGson().toJson(getBasicInfo(hotelInfo)));
		binValues.put(BinName.HOTEL_DETAIL.getName(), GsonUtils.getGson().toJson(getDetailInfo(hotelInfo)));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_MAPPING.getName()).key(String.valueOf(hotelInfo.getId())).expiration(-1)
				.binValues(binValues).build();
		cacheService.store(metaInfoToStore);
	}
}
