package com.tgs.services.hms.helper;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.utils.HotelUtils;

@Service
public class HotelCancellationPolicySearch extends AbstractHotelCancellationPolicy {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Override
	public void getCancellationPolicy(HotelSearchQuery searchQuery, HotelInfo hInfo, String optionId, String logKey) {
		HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
		Option option = HotelUtils.filterOptionById(hInfo, optionId);
		searchQuery.setSourceId(option.getMiscInfo().getSourceId());
		if (option.getCancellationPolicy() != null) return;
		else {
			option.getRoomInfos().get(0).setCheckInDate(searchQuery.getCheckinDate());
			copyHInfo.setOptions(Arrays.asList(option));
			super.getCancellationPolicy(searchQuery, copyHInfo, optionId, logKey);
			for (int i = 0; i < hInfo.getOptions().size(); i++) {
				Option opt = hInfo.getOptions().get(i);
				if (opt.getId().equals(option.getId())) {
					hInfo.getOptions().set(i, copyHInfo.getOptions().get(0));
				}
			}
			cacheHandler.persistHotelInCache(hInfo, searchQuery);
		}
	}

}
