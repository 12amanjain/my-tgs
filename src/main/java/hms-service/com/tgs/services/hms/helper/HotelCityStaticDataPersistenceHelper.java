
package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.dbmodel.DbCityInfo;
import com.tgs.services.hms.dbmodel.DbCityInfoMapping;
import com.tgs.services.hms.jparepository.HotelCityInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelCityStaticDataPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelCityInfoService cityInfoService;


	public HotelCityStaticDataPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		List<CityInfo> cityInfoList = new ArrayList<>();
		List<CityInfoMapping> cityMappingList = new ArrayList<>();
		Runnable fetchCityInfoTask = () -> {
			log.info("Fetching hotel city info from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<CityInfo> cityInfoListChunk = DbCityInfo.toDomainList(cityInfoService.findAll(page));
				if (CollectionUtils.isEmpty(cityInfoListChunk))
					break;
				cityInfoList.addAll(cityInfoListChunk);
				log.debug("Fetched hotel static info from database, info list size is {}", cityInfoList.size());
			}

			log.info("Fetching hotel city mapping info from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<CityInfoMapping> cityInfoMappingChunk =
						DbCityInfoMapping.toDomainList(cityInfoService.findAllMapping(page));
				if (CollectionUtils.isEmpty(cityInfoMappingChunk))
					break;
				cityMappingList.addAll(cityInfoMappingChunk);
				log.debug("Fetched hotel static mapping info from database, mapping list size is {}",
						cityMappingList.size());
			}

			Map<Long, List<CityInfoMapping>> cityMap =
					cityMappingList.stream().collect(Collectors.groupingBy(CityInfoMapping::getCityId));
			for (int i = 0; i < cityInfoList.size(); i++) {
				try {
					List<CityInfoMapping> cityInfoMappingList = cityMap.get(cityInfoList.get(i).getId());
					Map<String, Object> cityInfoBinMap = new HashMap<>();
					cityInfoBinMap.put("CITY_INFO", GsonUtils.getGson().toJson(cityInfoList.get(i)));
					cityInfoBinMap.put(BinName.IATA.name(),
							cityInfoList.get(i).getIataCode() == null ? "" : cityInfoList.get(i).getIataCode());
					if (CollectionUtils.isNotEmpty(cityInfoMappingList)) {
						for (CityInfoMapping supplier : cityInfoMappingList) {
							cityInfoBinMap.put(supplier.getSupplierName(), GsonUtils.getGson().toJson(supplier));
						}
						CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder()
								.namespace(CacheNameSpace.HOTEL.getName()).set(CacheSetName.CITY_MAPPING.getName())
								.key(String.valueOf(cityInfoList.get(i).getId())).expiration(-1)
								.binValues(cityInfoBinMap).build();
						cacheService.store(metaInfoToStore);
					}
				} catch (Exception e) {
					log.error("Error while storing city mapping in aerospike for city {} , {}",
							cityInfoList.get(i).getId(), cityInfoList.get(i).getCityName(), e);
				}
			}
		};

		Thread fetchCityInfoThread = new Thread(fetchCityInfoTask);
		fetchCityInfoThread.start();

	}

	@Override
	public void deleteExistingInitializer() {}

}
