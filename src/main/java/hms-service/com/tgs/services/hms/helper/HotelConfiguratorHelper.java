package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.filters.HotelConfiguratorInfoFilter;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.HotelBasicRuleField;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.dbmodel.DbHotelConfiguratorRule;
import com.tgs.services.hms.jparepository.HotelConfiguratorService;

@Service
public class HotelConfiguratorHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap hotelConfigRules;

	private static Map<HotelConfiguratorRuleType, List<HotelConfiguratorInfo>> inMemoryHotelConfigRules;

	private static final String FIELD = "hotel_config";

	static Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(HotelBasicRuleField.class);

	private static HotelConfiguratorHelper SINGLETON;

	@Autowired
	HotelConfiguratorService hotelConfiguratorService;

	public HotelConfiguratorHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap hotelConfigRules) {
		super(configurationHashMap);
		HotelConfiguratorHelper.hotelConfigRules = hotelConfigRules;
	}

	@PostConstruct
	void init() {
		SINGLETON = this;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getRuleOutPut(IFact hotelFact, List<HotelConfiguratorInfo> hotelconfigrules) {
		List<HotelConfiguratorInfo> configuratorInfos = getApplicableRules(hotelFact, hotelconfigrules);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			return (T) configuratorInfos.get(0).getOutput();
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public static <T> T getRule(IFact hotelFact, List<HotelConfiguratorInfo> hotelconfigrules) {
		List<HotelConfiguratorInfo> configuratorInfos = getApplicableRules(hotelFact, hotelconfigrules);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			return (T) configuratorInfos.get(0);
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	private static List<HotelConfiguratorInfo> getApplicableRules(IFact hotelFact,
			List<HotelConfiguratorInfo> hotelconfigrules) {
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(hotelconfigrules, hotelFact, fieldResolverMap);
		List<HotelConfiguratorInfo> rules = (List<HotelConfiguratorInfo>) ruleEngine.fireAllRules();
		return rules;
	}

	public static List<HotelConfiguratorInfo> getHotelConfiguratorRules(HotelConfiguratorRuleType ruleType) {
		if(inMemoryHotelConfigRules==null)
			SINGLETON.processInMemory();

		List<HotelConfiguratorInfo> rules = new ArrayList<>();
		List<HotelConfiguratorInfo> configuratorInfos = inMemoryHotelConfigRules.get(ruleType);
		if (CollectionUtils.isNotEmpty(configuratorInfos)) {
			inMemoryHotelConfigRules.get(ruleType).forEach(rule -> {
				rules.add(rule.toBuilder().build());
			});
		}
		return rules;

		// Gson gson = GsonUtils.getGson();
		// String json = gson.toJson(inMemoryHotelConfigRules.get(ruleType));
		// return gson.fromJson(json, new TypeToken<List<HotelConfiguratorInfo>>() {}.getType());
	}

	@SuppressWarnings("unchecked")
	public static <T> T getHotelConfigRuleOutput(IFact hotelFact, HotelConfiguratorRuleType ruleType) {
		List<HotelConfiguratorInfo> matchingRules = getApplicableRules(hotelFact, getHotelConfiguratorRules(ruleType));
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return (T) matchingRules.get(0).getOutput();
		}
		return null;
	}

	public static HotelConfiguratorInfo getHotelConfigRule(IFact hotelFact, HotelConfiguratorRuleType ruleType) {
		List<HotelConfiguratorInfo> matchingRules = getApplicableRules(hotelFact, getHotelConfiguratorRules(ruleType));
		if (CollectionUtils.isNotEmpty(matchingRules)) {
			return matchingRules.get(0);
		}
		return null;
	}
	
	@Override
	public void process() {
		processInMemory();

		if (MapUtils.isEmpty(inMemoryHotelConfigRules)) {
			return;
		}

		inMemoryHotelConfigRules.forEach((ruleType, rules) -> {
			hotelConfigRules.put(ruleType.getName(), FIELD, rules,
					CacheMetaInfo.builder().set(CacheSetName.HOTEL_CONFIGURATOR.getName())
							.expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
		});
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<DbHotelConfiguratorRule> configRules = hotelConfiguratorService
				.findAll(HotelConfiguratorInfoFilter.builder().isDeleted(false).build());
		Map<HotelConfiguratorRuleType, List<HotelConfiguratorInfo>> ruleMap = null;
		if (CollectionUtils.isNotEmpty(configRules)) {
			List<HotelConfiguratorInfo> configuratorInfos = DbHotelConfiguratorRule.toDomainList(configRules);

			 ruleMap = configuratorInfos.stream()
						.collect(Collectors.groupingBy(HotelConfiguratorInfo::getRuleType));
		}
		inMemoryHotelConfigRules =
				Optional.<Map<HotelConfiguratorRuleType, List<HotelConfiguratorInfo>>>ofNullable(ruleMap)
						.orElseGet(HashMap::new);
	}
	
	@Override
	public void deleteExistingInitializer() {
		if (inMemoryHotelConfigRules != null) {
			inMemoryHotelConfigRules.clear();
		}
		hotelConfigRules.truncate(CacheSetName.HOTEL_CONFIGURATOR.getName());
	}

}
