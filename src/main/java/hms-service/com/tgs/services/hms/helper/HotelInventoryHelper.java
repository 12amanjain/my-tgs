package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryCacheData;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierMappingInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.dbmodel.inventory.DbHotelRatePlan;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.jparepository.inventory.HotelRatePlanService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelInventoryHelper extends InMemoryInitializer {
	
	
	@Autowired
	HotelInfoService hotelInfoService;
	
	@Autowired
	HotelRatePlanService ratePlanService;
	
	@Autowired
	HMSCachingServiceCommunicator cacheService;
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	
	public HotelInventoryHelper() {
		super(null);
		
	}

	@Override
	public void process() {
		
		Runnable fetchSupplierMappingTask = () -> {
			log.info("Fetching hotelsuppliermapping from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 500, Direction.ASC, "id");
				List<HotelSupplierMappingInfo> supplierMappingList =
						DbHotelSupplierMapping.toDomainList(hotelInfoService.findAllHotelSupplierMappingBySource(page,
								HotelSourceType.OFFLINEINVENTORY.name()));
				log.debug("Fetched hotelsuppliermapping from database , mapping size is {}",
						supplierMappingList.size());
				if (CollectionUtils.isEmpty(supplierMappingList)) break;
				List<String> supplierHotelIdList = new ArrayList<>();
				supplierMappingList.forEach((mappingInfo) ->{
					supplierHotelIdList.add(mappingInfo.getSupplierHotelId());
				});
				
				HotelRatePlanFilter ratePlanFilter = HotelRatePlanFilter.builder()
						.supplierHotelIds(supplierHotelIdList).isDeleted(false).enabled(true).build();
				List<HotelRatePlan> ratePlanList = DbHotelRatePlan
						.toDomainList(ratePlanService.findAll(ratePlanFilter));
				cacheHandler.updateInventoryRatePlans(ratePlanList);
				
			}
		};
		
		new Thread(fetchSupplierMappingTask).start();
	}

	@Override
	public void deleteExistingInitializer() {
	
	}

}
