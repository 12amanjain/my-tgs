package com.tgs.services.hms.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.supplier.HotelMealBasis;
import com.tgs.services.hms.dbmodel.DbHotelMealBasis;
import com.tgs.services.hms.jparepository.HotelInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelMealPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HotelInfoService hotelInfoService;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	public HotelMealPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		Runnable fetchHotelMealTask = () -> {
			log.info("Fetching hotel meal basis from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<HotelMealBasis> hotelMealBasisList =
						DbHotelMealBasis.toDomainList(hotelInfoService.findAllHotelMeals(page));
				if (CollectionUtils.isEmpty(hotelMealBasisList))
					break;
				log.debug("Fetched hotel meal basis from database , mapping size is {}", hotelMealBasisList.size());
				hotelMealBasisList.stream().forEach(hotelMeal -> {
					try {
						if (StringUtils.isNotEmpty(hotelMeal.getSMealBasis())) {
							Map<String, Object> binValues = new HashMap<>();
							binValues.put(BinName.MEALBASIS.getName(), hotelMeal.getFMealBasis().toUpperCase());
							CacheMetaInfo metaInfoToStore =
									CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
											.set(CacheSetName.HOTEL_MEAL_BASIS.getName())
											.key(hotelMeal.getSMealBasis().toUpperCase()).expiration(-1)
											.binValues(binValues).build();
							cacheService.store(metaInfoToStore);
						}
					} catch (Exception e) {
						log.error("Unable to save hotel meal basis into aerospike", e);
					}
				});
			}
		};

		Thread fetchHotelMealThread = new Thread(fetchHotelMealTask);
		fetchHotelMealThread.start();
	}

	@Override
	public void deleteExistingInitializer() {}
}
