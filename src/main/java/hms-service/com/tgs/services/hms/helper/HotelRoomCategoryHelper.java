package com.tgs.services.hms.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.dbmodel.inventory.DbRoomCategory;
import com.tgs.services.hms.jparepository.inventory.HotelRoomInfoService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRoomCategoryHelper extends InMemoryInitializer{

	private static CustomInMemoryHashMap roomCategoryValues;
	private static  Map<Long, String> inMemoryRoomCategoryIdValueMap;
	private static HotelRoomCategoryHelper SINGLETON;
	private static final String FIELD = "room_category";
	
	@Autowired
	HotelRoomInfoService roomService;
	
	public HotelRoomCategoryHelper(CustomInMemoryHashMap roomCategoryInfoHashMap) {
		super(roomCategoryInfoHashMap);
		HotelRoomCategoryHelper.roomCategoryValues = roomCategoryInfoHashMap;
	}
	
	@PostConstruct
	void init() {
		SINGLETON = this;
	}

	@Override
	public void process() {
		
		log.debug("Initializing HotelRoomCategoryHelper");
		processInMemory();
		if (MapUtils.isEmpty(inMemoryRoomCategoryIdValueMap)) {
			return;
		}
		
		inMemoryRoomCategoryIdValueMap.forEach((rcId, rcName) -> {
			String roomCategoryId = String.valueOf(rcId);
			roomCategoryValues.put(roomCategoryId, FIELD, rcName, CacheMetaInfo.builder().compress(false)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.ROOM_CATEGORY.getName()).build());
		});
 		
	}
	
	public static Map<Long, String> getRoomCategoryIdRoomCategoryNameMap(){
		
		if(inMemoryRoomCategoryIdValueMap==null)
			SINGLETON.processInMemory();
		
		return inMemoryRoomCategoryIdValueMap;
	}
	
	private void processInMemory() {
		
		List<HotelRoomCategory> rcList = DbRoomCategory.toDomainList(roomService.findAll());
		if(CollectionUtils.isEmpty(rcList)) {
			log.info("Fetched Empty RoomCategory List while running HotelRoomCategoryHelper");
			return;
		}
	
		inMemoryRoomCategoryIdValueMap = rcList.stream().collect(Collectors.toMap(HotelRoomCategory :: getId, 
				HotelRoomCategory :: getRoomCategory));
		
	}
	
	

	@Override
	public void deleteExistingInitializer() {
		if (inMemoryRoomCategoryIdValueMap != null) {
			inMemoryRoomCategoryIdValueMap.clear();
		}
		roomCategoryValues.truncate(CacheSetName.ROOM_CATEGORY.getName());
		
	}

}
