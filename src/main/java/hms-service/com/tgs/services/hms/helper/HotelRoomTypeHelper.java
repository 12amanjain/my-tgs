package com.tgs.services.hms.helper;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.dbmodel.inventory.DbRoomTypeInfo;
import com.tgs.services.hms.jparepository.inventory.HotelRoomInfoService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRoomTypeHelper extends InMemoryInitializer{

	private static CustomInMemoryHashMap roomTypeValues;
	private static  Map<Long, HotelRoomTypeInfo> inMemoryRoomTypeIdValueMap;
	private static HotelRoomTypeHelper SINGLETON;
	private static final String FIELD = "room_type";
	
	@Autowired
	HotelRoomInfoService roomService;
	
	public HotelRoomTypeHelper(CustomInMemoryHashMap roomTypeInfoHashMap) {
		super(roomTypeInfoHashMap);
		HotelRoomTypeHelper.roomTypeValues = roomTypeInfoHashMap;
	}
	
	@PostConstruct
	void init() {
		SINGLETON = this;
	}

	@Override
	public void process() {
		
		log.debug("Initializing HotelRoomTypeHelper");
		processInMemory();
		if (MapUtils.isEmpty(inMemoryRoomTypeIdValueMap)) {
			return;
		}
		
		inMemoryRoomTypeIdValueMap.forEach((rtId, rtInfo) -> {
			String roomTypeId = String.valueOf(rtId);
			String roomTypeInfo = GsonUtils.getGson().toJson(rtInfo);
			roomTypeValues.put(roomTypeId, FIELD, roomTypeInfo, CacheMetaInfo.builder().compress(false)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.ROOM_TYPE.getName()).build());
		});
 		
	}
	
	public static Map<Long, HotelRoomTypeInfo> getRoomTypeIdRoomTypeInfoMap(){
		
		if(inMemoryRoomTypeIdValueMap==null)
			SINGLETON.processInMemory();
		
		return inMemoryRoomTypeIdValueMap;
	}
	
	private void processInMemory() {
		
		List<HotelRoomTypeInfo> rtList = DbRoomTypeInfo.toDomainList(roomService.findAllRoomTypeInfo());
		if(CollectionUtils.isEmpty(rtList)) {
			log.info("Fetched Empty RoomType List while running HotelRoomTypeHelper");
			return;
		}
	
		inMemoryRoomTypeIdValueMap = rtList.stream().collect(Collectors.toMap(HotelRoomTypeInfo :: getId, 
				Function.identity()));
	}
	
	

	@Override
	public void deleteExistingInitializer() {
		if (inMemoryRoomTypeIdValueMap != null) {
			inMemoryRoomTypeIdValueMap.clear();
		}
		roomTypeValues.truncate(CacheSetName.ROOM_TYPE.getName());
		
	}

}
