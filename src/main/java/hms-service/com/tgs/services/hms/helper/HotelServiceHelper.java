package com.tgs.services.hms.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;

@Service
public class HotelServiceHelper {

	@Autowired
	private static HMSCachingServiceCommunicator cacheService;

	@Autowired
	public HotelServiceHelper(HMSCachingServiceCommunicator cachingService) {
		HotelServiceHelper.cacheService = cachingService;
	}
}
