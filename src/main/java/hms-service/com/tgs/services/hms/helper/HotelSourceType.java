package com.tgs.services.hms.helper;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.sources.CleartripPriceValidationFactory;
import com.tgs.services.hms.sources.agoda.AgodaHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.agoda.AgodaHotelBookingFactory;
import com.tgs.services.hms.sources.agoda.AgodaHotelInfoFactory;
import com.tgs.services.hms.sources.agoda.AgodaPriceValidationFactory;
import com.tgs.services.hms.sources.agoda.AgodaStaticDataFactory;
import com.tgs.services.hms.sources.cleartrip.CleartripBookingCancellationFactory;
import com.tgs.services.hms.sources.cleartrip.CleartripHotelBookingFactory;
import com.tgs.services.hms.sources.cleartrip.CleartripHotelInfoFactory;
import com.tgs.services.hms.sources.cleartrip.CleartripRetrieveBookingFactory;
import com.tgs.services.hms.sources.cleartrip.CleartripStaticDataInfoFactory;
import com.tgs.services.hms.sources.desiya.DesiyaHotelBookingFactory;
import com.tgs.services.hms.sources.desiya.DesiyaHotelCancellationFactory;
import com.tgs.services.hms.sources.desiya.DesiyaHotelInfoFactory;
import com.tgs.services.hms.sources.desiya.DesiyaPriceValidationFactory;
import com.tgs.services.hms.sources.desiya.DesiyaStaticDataFactory;
import com.tgs.services.hms.sources.dotw.DotwHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.dotw.DotwHotelBookingFactory;
import com.tgs.services.hms.sources.dotw.DotwHotelInfoFactory;
import com.tgs.services.hms.sources.dotw.DotwPriceValidationFactory;
import com.tgs.services.hms.sources.dotw.DotwRetrieveBookingFactory;
import com.tgs.services.hms.sources.dotw.DotwStaticDataFactory;
import com.tgs.services.hms.sources.expedia.ExpediaBookingCancellationFactory;
import com.tgs.services.hms.sources.expedia.ExpediaCrossSellFactory;
import com.tgs.services.hms.sources.expedia.ExpediaHotelBookingFactory;
import com.tgs.services.hms.sources.expedia.ExpediaHotelInfoFactory;
import com.tgs.services.hms.sources.expedia.ExpediaPriceValidationFactory;
import com.tgs.services.hms.sources.expedia.ExpediaRetrieveBookingFactory;
import com.tgs.services.hms.sources.expedia.ExpediaStaticDataInfoFactory;
import com.tgs.services.hms.sources.hotelbeds.HotelBedsBookingCancellationFactory;
import com.tgs.services.hms.sources.hotelbeds.HotelBedsBookingFactory;
import com.tgs.services.hms.sources.hotelbeds.HotelBedsInfoFactory;
import com.tgs.services.hms.sources.hotelbeds.HotelBedsPriceValidationFactory;
import com.tgs.services.hms.sources.hotelbeds.HotelBedsStaticDataFactory;
import com.tgs.services.hms.sources.hotelbeds.hotelBedsRetrieveBookingFactory;
import com.tgs.services.hms.sources.inventory.HotelInventoryBookingCancellationFactory;
import com.tgs.services.hms.sources.inventory.HotelInventoryBookingFactory;
import com.tgs.services.hms.sources.inventory.HotelInventoryPriceValidationFactory;
import com.tgs.services.hms.sources.inventory.InventoryHotelInfoFactory;
import com.tgs.services.hms.sources.qtech.QTechHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.qtech.QTechHotelBookingFactory;
import com.tgs.services.hms.sources.qtech.QTechHotelInfoFactory;
import com.tgs.services.hms.sources.qtech.QTechHotelPriceValidationFactory;
import com.tgs.services.hms.sources.qtech.QTechRetrieveHotelBookingFactory;
import com.tgs.services.hms.sources.tbo.TravelBoutiqueHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.tbo.TravelBoutiqueHotelBookingFactory;
import com.tgs.services.hms.sources.tbo.TravelBoutiqueHotelInfoFactory;
import com.tgs.services.hms.sources.tbo.TravelBoutiquePriceValidationFactory;
import com.tgs.services.hms.sources.tripjack.TripjackBookingCancellationFactory;
import com.tgs.services.hms.sources.tripjack.TripjackBookingFactory;
import com.tgs.services.hms.sources.tripjack.TripjackInfoFactory;
import com.tgs.services.hms.sources.tripjack.TripjackPriceValidationFactory;
import com.tgs.services.hms.sources.tripjack.TripjackRetrieveBookingFactory;
import com.tgs.services.hms.sources.tripjack.TripjackStaticDataFactory;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import lombok.Getter;

@Getter
public enum HotelSourceType {

	QTECH(1) {
		@Override
		public AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(QTechHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		public AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(QTechHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		public AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(QTechRetrieveHotelBookingFactory.class,
					hotelSupplierConf, importBookingParams);
		}

		@Override
		public AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(QTechHotelBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest catalogRequest) {
			return null;
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(QTechHotelPriceValidationFactory.class, searchQuery,
					hotel, hotelSupplierConf, bookingId);
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},

	TBO(2) {

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(TravelBoutiqueHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(TravelBoutiqueHotelBookingFactory.class,
					hotelSupplierConf, hotel, order);
		}

		/*
		 * @Override protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
		 * HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingInfo importBookingInfo) { // TODO
		 * Auto-generated method stub return null; }
		 */

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(TravelBoutiqueHotelBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		public int getExpirationDuration() {
			// TODO Auto-generated method stub
			return 20 * 60;
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(TravelBoutiquePriceValidationFactory.class,
					searchQuery, hotel, hotelSupplierConf, bookingId);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest catalogRequest) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},
	EXPEDIA(3) {

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(ExpediaHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(ExpediaHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(ExpediaRetrieveBookingFactory.class, hotelSupplierConf,
					importBookingParams);
		}

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(ExpediaBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(ExpediaPriceValidationFactory.class, searchQuery,
					hotel, hotelSupplierConf, bookingId);
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
			return SpringContext.getApplicationContext().getBean(ExpediaStaticDataInfoFactory.class, supplierConf,
					staticDataRequest);
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},
	DOTW(4) {

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(DotwHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(DotwHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(DotwRetrieveBookingFactory.class, hotelSupplierConf,
					importBookingParams);
		}

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(DotwHotelBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);

		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticRequest) {
			return SpringContext.getApplicationContext().getBean(DotwStaticDataFactory.class, supplierConf,
					staticRequest);
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(DotwPriceValidationFactory.class, searchQuery, hotel,
					hotelSupplierConf, bookingId);
		}

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}

	},
	EXPEDIACROSSSELL(5) {

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(ExpediaCrossSellFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(ExpediaHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(ExpediaRetrieveBookingFactory.class, hotelSupplierConf,
					importBookingParams);
		}

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(ExpediaBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(ExpediaPriceValidationFactory.class, searchQuery,
					hotel, hotelSupplierConf, bookingId);
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
			return null;
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},
	OFFLINEINVENTORY(6) {

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(InventoryHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(HotelInventoryBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return null;
		}

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(HotelInventoryBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(HotelInventoryPriceValidationFactory.class,
					searchQuery, hotel, hotelSupplierConf, bookingId);
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
			return null;
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			return true;
		}

	},
	CLEARTRIP(7) {

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(CleartripHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(CleartripHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(CleartripRetrieveBookingFactory.class,
					hotelSupplierConf, importBookingParams);
		}

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(CleartripBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(CleartripPriceValidationFactory.class, searchQuery,
					hotel, hotelSupplierConf, bookingId);
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
			return SpringContext.getApplicationContext().getBean(CleartripStaticDataInfoFactory.class, supplierConf,
					staticDataRequest);
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},

	AGODA(8) {

		@Override
		protected AgodaHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(AgodaHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}

		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(AgodaHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}

		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(AgodaHotelBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}

		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticRequest) {
			return SpringContext.getApplicationContext().getBean(AgodaStaticDataFactory.class, supplierConf,
					staticRequest);
		}

		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(AgodaPriceValidationFactory.class, searchQuery, hotel,
					hotelSupplierConf, bookingId);
		}

		@Override
		public int getExpirationDuration() {
			return 20 * 60;
		}

		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},
	DESIYA(9) {

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(DesiyaHotelInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}


		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(DesiyaHotelBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}


		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			// TODO Auto-generated method stub
			return null;
		}


		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(DesiyaHotelCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}


		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticRequest) {
			return SpringContext.getApplicationContext().getBean(DesiyaStaticDataFactory.class, supplierConf,
					staticRequest);
		}


		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(DesiyaPriceValidationFactory.class, searchQuery, hotel,
					hotelSupplierConf, bookingId);
		}


		@Override
		public int getExpirationDuration() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	},
	HOTELBEDS(10) {

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(HotelBedsInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}


		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(HotelBedsBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}


		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(hotelBedsRetrieveBookingFactory.class,
					hotelSupplierConf, importBookingParams);
		}


		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(HotelBedsBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}


		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticRequest) {
			return SpringContext.getApplicationContext().getBean(HotelBedsStaticDataFactory.class, supplierConf,
					staticRequest);
		}


		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(HotelBedsPriceValidationFactory.class, searchQuery,
					hotel, hotelSupplierConf, bookingId);
		}


		@Override
		public int getExpirationDuration() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			return true;
		}

	},
	TRIPJACK(11) {

		@Override
		protected AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
				HotelSupplierConfiguration hotelSupplierConf) {
			return SpringContext.getApplicationContext().getBean(TripjackInfoFactory.class, searchQuery,
					hotelSupplierConf);
		}


		@Override
		protected AbstractHotelBookingFactory getBookingFactoryInstance(HotelSupplierConfiguration hotelSupplierConf,
				HotelInfo hotel, Order order) {
			return SpringContext.getApplicationContext().getBean(TripjackBookingFactory.class, hotelSupplierConf,
					hotel, order);
		}


		@Override
		protected AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
				HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams) {
			return SpringContext.getApplicationContext().getBean(TripjackRetrieveBookingFactory.class,
					hotelSupplierConf, importBookingParams);
}


		@Override
		protected AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
				HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order) {
			return SpringContext.getApplicationContext().getBean(TripjackBookingCancellationFactory.class,
					hotelSupplierConf, hInfo, order);
		}


		@Override
		protected AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
				HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticRequest) {
			return SpringContext.getApplicationContext().getBean(TripjackStaticDataFactory.class, supplierConf,
					staticRequest);
		}


		@Override
		protected AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(HotelSearchQuery searchQuery,
				HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
			return SpringContext.getApplicationContext().getBean(TripjackPriceValidationFactory.class, searchQuery, hotel,
					hotelSupplierConf, bookingId);
		}


		@Override
		public int getExpirationDuration() {
			// TODO Auto-generated method stub
			return 0;
		}


		@Override
		public boolean getisValidSourceIfCityMappingNull() {
			// TODO Auto-generated method stub
			return false;
		}
	};
	;

	private int sourceId;

	private HotelSourceType(int sourceId) {
		this.sourceId = sourceId;
	}

	public static HotelSourceType getHotelSourceType(int id) {
		for (HotelSourceType sourceType : HotelSourceType.values()) {
			if (sourceType.getSourceId() == id) {
				return sourceType;

			}
		}
		return null;
	}

	public static HotelSourceType getHotelSourceTypeFromSourceName(String sourceName) {
		for (HotelSourceType sourceType : HotelSourceType.values()) {
			if (sourceType.name().equals(sourceName)) {
				return sourceType;
			}
		}
		return null;
	}

	public static List<Integer> getAllSourceIds() {

		return Stream.of(HotelSourceType.values()).mapToInt(HotelSourceType::getSourceId).boxed()
				.collect(Collectors.toList());
	}

	public boolean isCityWiseHotelStaticDataSupported() {

		Set<HotelSourceType> sourceType = new HashSet<>();
		sourceType.add(HotelSourceType.EXPEDIA);
		sourceType.add(HotelSourceType.QTECH);
		sourceType.add(HotelSourceType.EXPEDIACROSSSELL);
		return !sourceType.contains(this);
	}

	public static AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery query, String supplierId) {

		HotelSupplierConfiguration hotelSupplierConf =
				HotelSupplierConfigurationHelper.getSupplierConfiguration(supplierId);
		if (ObjectUtils.isEmpty(query.getSourceId())) {
			query.setSourceId(hotelSupplierConf.getBasicInfo().getSourceId());
		}
		HotelSourceType hotelSourceType =
				HotelSourceType.getHotelSourceType(hotelSupplierConf.getBasicInfo().getSourceId());
		return hotelSourceType.getFactoryInstance(query, hotelSupplierConf);
	}

	public static AbstractHotelBookingFactory getBookingFactoryInstance(HotelInfo hInfo, Order order) {
		HotelSupplierConfiguration hotelSupplierConf = HotelSupplierConfigurationHelper
				.getSupplierConfiguration(hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
		HotelSourceType sourceType = HotelSourceType.getHotelSourceType(hotelSupplierConf.getBasicInfo().getSourceId());
		return sourceType.getBookingFactoryInstance(hotelSupplierConf, hInfo, order);
	}

	public static AbstractRetrieveHotelBookingFactory getRetrieveBookingFactoryInstance(
			HotelImportBookingParams importBookingParams) {
		HotelSupplierConfiguration hotelSupplierConf =
				HotelSupplierConfigurationHelper.getSupplierConfiguration(importBookingParams.getSupplierId());
		HotelSourceType sourceType = HotelSourceType.getHotelSourceType(hotelSupplierConf.getBasicInfo().getSourceId());
		return sourceType.getRetrieveHotelBookingFactory(hotelSupplierConf, importBookingParams);
	}

	public static AbstractHotelBookingCancellationFactory getHotelBookingCancellationFactoryInstance(HotelInfo hInfo,
			Order order) {
		HotelSupplierConfiguration hotelSupplierConf = HotelSupplierConfigurationHelper
				.buildSupplierConfiguration(hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
		HotelSourceType hotelSourceType =
				HotelSourceType.getHotelSourceType(hotelSupplierConf.getBasicInfo().getSourceId());
		return hotelSourceType.getBookingCancellationFactoryInstance(hotelSupplierConf, hInfo, order);
	}

	public static AbstractHotelPriceValidationFactory getHotelPriceValidationFactoryInstance(
			HotelSearchQuery searchQuery, HotelInfo hInfo, String bookingId) {

		HotelSupplierConfiguration hotelSupplierConf = HotelSupplierConfigurationHelper
				.buildSupplierConfiguration(hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
		HotelSourceType hotelSourceType =
				HotelSourceType.getHotelSourceType(hotelSupplierConf.getBasicInfo().getSourceId());
		return hotelSourceType.getPriceValidationFactoryInstance(searchQuery, hInfo, hotelSupplierConf, bookingId);
	}

	public static AbstractStaticDataInfoFactory getStaticDataFactoryInstance(String supplierId,
			HotelStaticDataRequest staticDataRequest) {
		HotelSupplierConfiguration hotelSupplierConf =
				HotelSupplierConfigurationHelper.getSupplierConfiguration(supplierId);

		HotelSourceType hotelSourceType =
				HotelSourceType.getHotelSourceType(hotelSupplierConf.getBasicInfo().getSourceId());
		return hotelSourceType.getStaticDataInfoFactoryInstance(hotelSupplierConf, staticDataRequest);
	}

	protected abstract AbstractHotelInfoFactory getFactoryInstance(HotelSearchQuery searchQuery,
			HotelSupplierConfiguration hotelSupplierConf);

	protected abstract AbstractHotelBookingFactory getBookingFactoryInstance(
			HotelSupplierConfiguration hotelSupplierConf, HotelInfo hotel, Order order);

	protected abstract AbstractRetrieveHotelBookingFactory getRetrieveHotelBookingFactory(
			HotelSupplierConfiguration hotelSupplierConf, HotelImportBookingParams importBookingParams);

	protected abstract AbstractHotelBookingCancellationFactory getBookingCancellationFactoryInstance(
			HotelSupplierConfiguration hotelSupplierConf, HotelInfo hInfo, Order order);

	protected abstract AbstractStaticDataInfoFactory getStaticDataInfoFactoryInstance(
			HotelSupplierConfiguration supplierConf, HotelStaticDataRequest catalogRequest);

	protected abstract AbstractHotelPriceValidationFactory getPriceValidationFactoryInstance(
			HotelSearchQuery searchQuery, HotelInfo hotel, HotelSupplierConfiguration hotelSupplierConf,
			String bookingId);

	// public abstract String getSupplierId();

	public abstract int getExpirationDuration();

	public abstract boolean getisValidSourceIfCityMappingNull();

}

