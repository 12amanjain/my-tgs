package com.tgs.services.hms.helper;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.jparepository.HotelInfoService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelStaticDataPersistenceHelper extends InMemoryInitializer {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelInfoService hotelInfoService;

	public HotelStaticDataPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		Runnable fetchStaticDataTask = () -> {
			log.info("Fetching hotel static data from database");
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<HotelInfo> hotelInfoList = DbHotelInfo.toDomainList(hotelInfoService.findAllHotels(page));
				log.debug("Fetched hotel static info from database, info list size is {}", hotelInfoList.size());
				if (CollectionUtils.isEmpty(hotelInfoList))
					break;
				hotelInfoList.forEach(hotelInfo -> {
					try {
						cacheHandler.storeMasterHotel(hotelInfo);
					} catch (Exception e) {
						log.error("Unable to save hotel static data into aerospike", e);
					}
				});
			}
		};

		Thread fetchStaticDataThread = new Thread(fetchStaticDataTask);
		fetchStaticDataThread.start();
	}

	@Override
	public void deleteExistingInitializer() {}

}
