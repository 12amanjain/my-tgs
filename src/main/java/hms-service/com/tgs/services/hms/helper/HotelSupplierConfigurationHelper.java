package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.annotation.PostConstruct;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.filters.HotelSupplierInfoFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.TgsSecurityUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierBasicInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierInfo;
import com.tgs.services.hms.jparepository.HotelSupplierInfoService;

@Service
public class HotelSupplierConfigurationHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap supplierInfoHashMap;
	private static final String SUPPLIERS = "suppliers";
	private static final String SUPPLIER_INFO = "supplierinfo";

	private static Map<String,HotelSupplierInfo> inMemoryHotelSuppliers = new HashMap<>();
	private static Map<Integer, List<HotelSupplierInfo>> inMemorySupplierRuleMap = new HashMap<>();
	@Autowired
	HotelSupplierInfoService hotelSupplierInfoService;

	public HotelSupplierConfigurationHelper(CustomInMemoryHashMap supplierInfoHashMap,
			GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		HotelSupplierConfigurationHelper.supplierInfoHashMap = supplierInfoHashMap;
	}

	private static HotelSupplierConfigurationHelper SINGLETON;

	@PostConstruct
	void init() {
		SINGLETON = this;
	}
	
	/*
	 * This will get called at the time of application loads
	 * 
	 * @see com.tgs.services.base.InMemoryInitializer#process()
	 */

	@Override
	public void process() {
		processInMemory();
		if (MapUtils.isEmpty(inMemorySupplierRuleMap) || MapUtils.isEmpty(inMemoryHotelSuppliers)) {
			return;
		}
		inMemoryHotelSuppliers.forEach((key,value) -> {
			supplierInfoHashMap.put(key, SUPPLIER_INFO, value,
					CacheMetaInfo.builder().compress(false).expiration(InMemoryInitializer.NEVER_EXPIRE)
							.set(CacheSetName.HOTEL_SUPPLIER.getName()).build());
		});
		
		inMemorySupplierRuleMap.forEach((key, value) -> {
			supplierInfoHashMap.put(SUPPLIERS, key.toString(), value, CacheMetaInfo.builder().compress(false)
					.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.HOTEL_SUPPLIER.getName()).build());
		});

	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	private void processInMemory() {
		List<DbHotelSupplierInfo> supplierInfos = hotelSupplierInfoService
				.findAll(HotelSupplierInfoFilter.builder().isDeleted(false).build());
		List<HotelSupplierInfo> hotelSuppliers = new ArrayList<>();
		supplierInfos.forEach(si -> {
			HotelSupplierInfo supplierInfo = si.toDomain(false);
			inMemoryHotelSuppliers.put(supplierInfo.getSupplierId(),supplierInfo);
			hotelSuppliers.add(supplierInfo);
		}); 
		inMemorySupplierRuleMap = hotelSuppliers.stream().collect(Collectors.groupingBy(HotelSupplierInfo::getSourceId));
	}

	public static List<HotelSupplierInfo> getSupplierList(Integer sourceId) {
		if(inMemorySupplierRuleMap==null)
			SINGLETON.processInMemory();
		Gson gson = GsonUtils.getGson();
		String json = gson.toJson(inMemorySupplierRuleMap.get(sourceId));
		List<HotelSupplierInfo>  rules  = gson.fromJson(json, new TypeToken<List<HotelSupplierInfo>>(){}.getType());
		if (CollectionUtils.isNotEmpty(rules)) {
			for (Iterator<HotelSupplierInfo> iter = rules.iterator(); iter.hasNext();) {
				HotelSupplierInfo rule = iter.next();
				if (BooleanUtils.isFalse(rule.getEnabled())) {
					iter.remove();
				} else {
					HotelSupplierCredential creds = rule.getCredentialInfo();
					try {
						creds.setPassword(TgsSecurityUtils.decryptData(creds.getPassword()));
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}

		return rules;
	}
	
	public static List<HotelSupplierInfo> getSupplierListFromSourceIds(List<Integer> sourceIds) {
		List<HotelSupplierInfo> finalSupplierInfos =  new ArrayList<>();
		for(Integer sourceId : sourceIds) {
			List<HotelSupplierInfo> supplierInfos =  getSupplierList(sourceId);
			if(CollectionUtils.isNotEmpty(supplierInfos)) {
				finalSupplierInfos.addAll(supplierInfos);
			}
		}
		return finalSupplierInfos;
	}

	public static List<HotelSupplierInfo> getSupplierListFromSupplierIds(List<String> supplierIds) {
		if(inMemoryHotelSuppliers==null)
			SINGLETON.processInMemory();
		List<HotelSupplierInfo> hotelSupplierInfoList = new ArrayList<>();
		Gson gson = GsonUtils.getGson();
		supplierIds.forEach(supplierId -> {
			String json = gson.toJson(inMemoryHotelSuppliers.get(supplierId));
			HotelSupplierInfo supplierInfo = gson.fromJson(json, HotelSupplierInfo.class);
			if(!ObjectUtils.isEmpty(supplierInfo)) {
				hotelSupplierInfoList.add(supplierInfo);
			}
		});
		if (CollectionUtils.isNotEmpty(hotelSupplierInfoList)) {
			for (Iterator<HotelSupplierInfo> iter = hotelSupplierInfoList.iterator(); iter.hasNext();) {
				HotelSupplierInfo rule = iter.next();
				if (BooleanUtils.isFalse(rule.getEnabled())) {
					iter.remove();
				} else {
					HotelSupplierCredential creds = rule.getCredentialInfo();
					try {
						creds.setPassword(TgsSecurityUtils.decryptData(creds.getPassword()));
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
		}
		return hotelSupplierInfoList;
	}

	public static HotelSupplierInfo getSupplierInfo(String supplierId) {
		List<HotelSupplierInfo> supplierInfo = getSupplierListFromSupplierIds(Arrays.asList(supplierId));
		if (!supplierInfo.isEmpty()) {
			return supplierInfo.get(0);
		}
		return null;
	}

	public static HotelSupplierConfiguration getSupplierConfiguration(String supplierId) {
		return buildSupplierConfiguration(supplierId);
	}

	public static HotelSupplierConfiguration buildSupplierConfiguration(String supplierId) {
		HotelSupplierInfo hotelSupplierInfo = getSupplierInfo(supplierId);
		if (hotelSupplierInfo != null && BooleanUtils.isTrue(hotelSupplierInfo.getEnabled())) {
			HotelSupplierBasicInfo basicInfo = HotelSupplierBasicInfo.builder()
					.supplierId(hotelSupplierInfo.getSupplierId()).supplierName(hotelSupplierInfo.getName())
					.sourceId(hotelSupplierInfo.getSourceId()).build();
			return HotelSupplierConfiguration.builder().hotelSupplierCredentials(hotelSupplierInfo.getCredentialInfo())
					.basicInfo(basicInfo).build();
		}
		return null;
	}

	@Override
	public void deleteExistingInitializer() {
		if(inMemoryHotelSuppliers!=null)
			inMemoryHotelSuppliers.clear();
		if(inMemorySupplierRuleMap!=null)
			inMemorySupplierRuleMap.clear();
		supplierInfoHashMap.truncate(CacheSetName.HOTEL_SUPPLIER.getName());
	}

}
