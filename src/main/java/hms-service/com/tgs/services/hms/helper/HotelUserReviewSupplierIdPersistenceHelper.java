package com.tgs.services.hms.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelUserReviewIdInfo;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;
import com.tgs.services.hms.jparepository.HotelUserReviewService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelUserReviewSupplierIdPersistenceHelper extends InMemoryInitializer{

	
	@Autowired
	HotelUserReviewService userReviewService;
	
	@Autowired
	HMSCachingServiceCommunicator cacheService;
	
	public HotelUserReviewSupplierIdPersistenceHelper() {
		super(null);
	}

	@Override
	public void process() {
		List<HotelUserReviewIdInfo> list = new ArrayList<>();
		Runnable fetchUserReviewInfoTask = () -> {
			for (int i = 0; i < 500; i++) {
				Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
				List<HotelUserReviewIdInfo> userReviewInfoListChunk = 
						DbHotelUserReviewIdInfo.toDomainList(userReviewService.findAll(page));
				if (CollectionUtils.isEmpty(userReviewInfoListChunk))
					break;
				list.addAll(userReviewInfoListChunk);
			}
			
			List<HotelUserReviewIdInfo> blackListedHotelList = list.stream()
					.filter((info) -> info.getReviewId() == null).collect(Collectors.toList());
			List<HotelUserReviewIdInfo> whiteListedHotelList = list.stream()
					.filter((info) -> info.getReviewId() != null).collect(Collectors.toList());
			
			storeBlackListedHotelsInCache(blackListedHotelList);
			storeWhiteListedHotelsInCache(whiteListedHotelList);
		};
		Thread fetchUserReviewInfoThread = new Thread(fetchUserReviewInfoTask);
		fetchUserReviewInfoThread.start();
		
	}

	private void storeWhiteListedHotelsInCache(List<HotelUserReviewIdInfo> whiteListedHotelList) {
		
		whiteListedHotelList.forEach(userReviewInfo ->{
			try {
				HashMap<String, Object> binMap = new HashMap<>();
				binMap.put(BinName.USERREVIEWID.getName(), userReviewInfo.getReviewId());
				CacheMetaInfo metaInfo = CacheMetaInfo.builder()
						.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_USER_REVIEW.getName())
						.key(userReviewInfo.getKey()).binValues(binMap).kyroCompress(true).build();
				cacheService.store(metaInfo);
			} catch (Exception e) {
				log.error("Unable to save hotel static data into aerospike", e);
			}
		});
		
		
	}

	private void storeBlackListedHotelsInCache(List<HotelUserReviewIdInfo> blackListedHotelList) {
		
		blackListedHotelList.forEach(userReviewInfo ->{
			try {
				HashMap<String, Object> binMap = new HashMap<>();
				binMap.put(BinName.UNMAPPEDUR.getName(), userReviewInfo.getKey());
				CacheMetaInfo metaInfo = CacheMetaInfo.builder()
						.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_USER_REVIEW.getName())
						.key(userReviewInfo.getKey()).binValues(binMap).kyroCompress(true).build();
				cacheService.store(metaInfo);
			}catch(Exception e) {
				log.error("Unable to save hotel static data into aerospike", e);
			}
		});
	}

	@Override
	public void deleteExistingInitializer() {
		// TODO Auto-generated method stub
		
	}
	
	

}
