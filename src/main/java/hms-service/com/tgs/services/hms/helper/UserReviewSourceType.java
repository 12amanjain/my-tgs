package com.tgs.services.hms.helper;

import com.tgs.services.base.SpringContext;
import com.tgs.services.hms.sources.AbstractHotelUserReviewFactory;
import com.tgs.services.hms.sources.tripadvisor.TripAdvisorReviewFactory;

import lombok.Getter;

@Getter
public enum UserReviewSourceType {

	TRIP_ADVISOR(1){

		@Override
		public AbstractHotelUserReviewFactory getReviewFactoryInstance() {
			return SpringContext.getApplicationContext().getBean(TripAdvisorReviewFactory.class);
		}
		
		@Override
		public String getSupplierName() {
			return "TRIP_ADVISOR";
		}
		
	};
	
	private int sourceId;
	
	private UserReviewSourceType(int sourceId) {
		this.sourceId = sourceId;
	}
	
	
	public static UserReviewSourceType getReviewSourceType(int id) {
		for(UserReviewSourceType type : UserReviewSourceType.values()) {
			if(type.getSourceId() == id) {
				return type;
			}
		}
		return null;
	}
	
	public abstract AbstractHotelUserReviewFactory getReviewFactoryInstance();
	public abstract String getSupplierName();
	
}
