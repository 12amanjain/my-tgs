package com.tgs.services.hms.helper.analytics;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelAnalyticsHelper {

	@Autowired
	KafkaServiceCommunicator kafkaService;

	public void storeData(Mapper<AnalyticsHotelQuery> mapper) {
		ExecutorUtils.getAnalyticsThreadPool().submit(() -> {
			AnalyticsHotelQuery analyticsQuery = null;
			try {
				analyticsQuery = mapper.convert();
				QueueData queueData = QueueData.builder().key(ESMetaInfo.HOTEL.getIndex())
						.value(GsonUtils.getGson().toJson(analyticsQuery)).build();
				List<QueueDataType> queueTypes = Arrays.asList(QueueDataType.ELASTICSEARCH);
				kafkaService.queue(queueTypes, queueData);
			} catch (Exception e) {

				log.error("Failed to store hotel analytics data", e);

			}
		});
	}
}
