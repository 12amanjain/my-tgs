package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import com.tgs.services.hms.dbmodel.DbCityInfoMapping;

public interface DbCityInfoMappingRepository
		extends JpaRepository<DbCityInfoMapping, Long>, JpaSpecificationExecutor<DbCityInfoMapping> {

	public DbCityInfoMapping findByCityIdAndSupplierName(Long cityId, String supplierName);

	public DbCityInfoMapping findBySupplierCity(String supplierCity);
	
	public Page<DbCityInfoMapping> findBySupplierName(String supplierName, Pageable pageable);

	@Query(value = "SELECT * FROM hotelcityinfomapping h WHERE h.cityid = :cityid and lower(h.suppliername) in :supplierNames",
			nativeQuery = true)
	public List<DbCityInfoMapping> findByCityIdAndSupplierList(@Param("cityid") Long cityId,
			@Param("supplierNames") List<String> supplierNames);
	
	public List<DbCityInfoMapping> findBySupplierName(String supplierName);
	
	public List<DbCityInfoMapping> findByCityId(Long cityId);

	public DbCityInfoMapping findBySupplierCityAndSupplierName(String supplierCityId, String supplierName);
}
