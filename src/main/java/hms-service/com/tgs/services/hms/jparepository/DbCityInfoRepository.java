package com.tgs.services.hms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbCityInfo;

@Repository
public interface DbCityInfoRepository extends JpaRepository<DbCityInfo, Long>, JpaSpecificationExecutor<DbCityInfo> {

	public DbCityInfo findById(Long id);
	public DbCityInfo findByCityNameAndCountryName(String cityName , String countryName);
	
	@Query(value = "Select * from hotelcityinfo where id > :cursor order by id asc limit :limit" , nativeQuery = true)
	public List<DbCityInfo> findByIdGreaterThanOrderByIdAscLimit(@Param("cursor")Long cursor , @Param("limit") Long limit);
	
}
