package com.tgs.services.hms.jparepository;



import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelInfo;


@Repository
public interface DbHotelInfoRepository extends JpaRepository<DbHotelInfo, Long> {
	
	public DbHotelInfo findByNameAndRatingAndCityNameAndCountryName(String name, String rating, String cityName, String countryName);
	public List<DbHotelInfo> findAll();
	public List<DbHotelInfo> findAllByOrderByIdAsc();
    public List<DbHotelInfo> findByIdGreaterThanOrderByIdAsc(Long id);
    public DbHotelInfo findById(Long id);

    @Query("FROM DbHotelInfo h WHERE h.id NOT IN (SELECT h.id FROM DbHotelInfo h RIGHT JOIN DbHotelSupplierMapping hs ON h.id = hs.hotelId WHERE supplierName = ?1)")
    public List<DbHotelInfo> findHotelsRightJoinHotelSupplierMapping(String supplierName);

    @Query(value = "Select * from hotelinfo where id > :cursor order by id asc limit :limit" , nativeQuery = true)
    public List<DbHotelInfo> findByIdGreaterThanOrderByIdAscLimit(@Param("cursor")Long cursor , @Param("limit") Long limit);
    
    @Query(value = "SELECT h.id, h.name, h.address, h.rating from DbHotelInfo h order by h.id asc")
    public List<Object[]> findHotelNameIdAddress(Pageable pageable);
    
    @Query(value = "SELECT h.id, h.name, h.address, h.rating from DbHotelInfo h WHERE id IN :idList")
    public List<Object[]> findNameRatingAddressByIdIn(@Param("idList")List<Long> idList);

}