package com.tgs.services.hms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelMealBasis;

@Repository
public interface DbHotelMealMappingRepository extends JpaRepository<DbHotelMealBasis, Long> {
	
	public DbHotelMealBasis findBySMealBasisAndSupplier(String sMealBasis, String supplier);
}
