package com.tgs.services.hms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelNationalitySupplierMapping;

@Repository
public interface DbHotelNationalityMappingRepository extends JpaRepository<DbHotelNationalitySupplierMapping, Long>, JpaSpecificationExecutor<DbHotelNationalitySupplierMapping> {

	
	
	
}
