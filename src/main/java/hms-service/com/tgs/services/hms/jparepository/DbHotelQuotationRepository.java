package com.tgs.services.hms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelQuotation;

@Repository
public interface DbHotelQuotationRepository extends JpaRepository<DbHotelQuotation, Long>, JpaSpecificationExecutor<DbHotelQuotation> {

	public List<DbHotelQuotation> findByUserId(String userId);
}
