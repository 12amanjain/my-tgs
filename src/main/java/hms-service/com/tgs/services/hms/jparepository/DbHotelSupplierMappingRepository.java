package com.tgs.services.hms.jparepository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;

@Repository
public interface DbHotelSupplierMappingRepository extends JpaRepository<DbHotelSupplierMapping, Long> {

	List<DbHotelSupplierMapping> findAll();
	public DbHotelSupplierMapping findFirstBySupplierNameOrderByHotelIdDesc(String supplierName);
	public DbHotelSupplierMapping findById(Long id);
	public DbHotelSupplierMapping findBySupplierNameAndHotelId(String supplierName , Long hotelId);
	
	@Query("From DbHotelSupplierMapping h where h.sourceName = :sourceName order by h.id asc")
	public List<DbHotelSupplierMapping> findAllBySourceName(Pageable pageable, @Param("sourceName") String sourceName);
	
	public List<DbHotelSupplierMapping> findAllBySupplierNameAndSourceName(String supplierName, String sourceName);

}
