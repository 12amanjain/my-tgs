package com.tgs.services.hms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;

public interface DbHotelUserReviewRepository extends JpaRepository<DbHotelUserReviewIdInfo, String>, JpaSpecificationExecutor<DbHotelUserReviewIdInfo>{
	
}
