package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.hms.dbmodel.DbSupplierCityInfo;

public interface DbSupplierCityInfoRepository
		extends JpaRepository<DbSupplierCityInfo, Long>, JpaSpecificationExecutor<DbSupplierCityInfo> {

	DbSupplierCityInfo findByCityNameAndCountryNameAndSupplierName(String cityName, String countryName,
			String supplierName);

	List<DbSupplierCityInfo> findByCityNameAndSupplierName(String cityName, String supplierName);

	List<DbSupplierCityInfo> findByCityIdAndSupplierName(String cityId, String supplierName);
}
