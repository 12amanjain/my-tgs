package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbCityInfo;
import com.tgs.services.hms.dbmodel.DbCityInfoMapping;
import com.tgs.services.hms.dbmodel.DbSupplierCityInfo;

@Service
public class HotelCityInfoService {

	@Autowired
	DbCityInfoRepository repository;

	@Autowired
	DbCityInfoMappingRepository mappingRepository;

	@Autowired
	DbSupplierCityInfoRepository supplierCityInfoRepository;

	public DbCityInfo saveCityInfoIntoDB(DbCityInfo cityInfo) {
		
		cityInfo.setCityName(cityInfo.getCityName().toUpperCase());
		cityInfo.setCountryName(cityInfo.getCountryName().toUpperCase());
		return repository.save(cityInfo);
	}

	public List<DbCityInfo> findAll(Pageable pageable) {
		List<DbCityInfo> cityInfoList = new ArrayList<>();
		repository.findAll(pageable).forEach(cityInfo -> cityInfoList.add(cityInfo));
		return cityInfoList;
	}

	public DbCityInfo findById(Long id) {
		return repository.findById(id);
	}

	public DbCityInfoMapping saveSupplierMapping(DbCityInfoMapping mappingObj) {
		return mappingRepository.save(mappingObj);
	}

	public DbCityInfoMapping findByCityIdAndSupplierName(Long cityId, String supplierName) {
		return mappingRepository.findByCityIdAndSupplierName(cityId, supplierName);
	}

	public List<DbCityInfoMapping> findByCityId(Long cityId) {
		return mappingRepository.findByCityId(cityId);
	}

	public List<DbCityInfoMapping> findBySupplierName(String supplierName, Pageable pageable) {
		List<DbCityInfoMapping> cityInfoMappings = new ArrayList<>();
		mappingRepository.findBySupplierName(supplierName, pageable)
				.forEach(cityInfo -> cityInfoMappings.add(cityInfo));
		return cityInfoMappings;
	}

	public DbCityInfoMapping findBySupplierCity(String city) {
		return mappingRepository.findBySupplierCity(city);
	}

	public List<DbCityInfoMapping> findAllMapping(Pageable pageable) {
		List<DbCityInfoMapping> cityInfoMappingList = new ArrayList<>();
		mappingRepository.findAll(pageable).forEach(hotelInfo -> cityInfoMappingList.add(hotelInfo));
		return cityInfoMappingList;
	}

	public DbCityInfo findByCityNameAndCountryName(String cityName, String countryName) {
		return repository.findByCityNameAndCountryName(cityName.toUpperCase(), countryName.toUpperCase());
	}

	public List<DbCityInfo> findByIdGreaterThanOrderByIdAscLimit(Long limit, Long cursor) {
		return repository.findByIdGreaterThanOrderByIdAscLimit(cursor, limit);
	}

	public List<DbCityInfoMapping> findByCityIdAndSupplierList(Long cityId, List<String> supplierNames) {
		return mappingRepository.findByCityIdAndSupplierList(cityId, supplierNames);
	}
	
	public List<DbCityInfoMapping> findBySupplierName(String supplierName) {
		return mappingRepository.findBySupplierName(supplierName);
	}
	
	public DbCityInfo findCityBySupplierNameSupplierCityId(String supplierCityId, String supplierName) {
		DbCityInfoMapping cityMapping = mappingRepository.findBySupplierCityAndSupplierName(supplierCityId, supplierName);
		if(cityMapping == null) return null;
		Long hotelCityId = cityMapping.getCityId();
		return findById(hotelCityId);
	}
	
	public DbCityInfoMapping findBySupplierNameSupplierCityId(String supplierCityId, String supplierName) {
		return mappingRepository.findBySupplierCityAndSupplierName(supplierCityId, supplierName);
	}

	public DbSupplierCityInfo findByCityNameAndCountryNameAndSupplierName(String cityName, String countryName,
			String supplierName) {
		return supplierCityInfoRepository.findByCityNameAndCountryNameAndSupplierName(cityName, countryName,
				supplierName);
	}

	public DbSupplierCityInfo saveSupplierCityInfo(DbSupplierCityInfo mappingObj) {
		return supplierCityInfoRepository.save(mappingObj);
	}

	public List<DbSupplierCityInfo> findByCityNameAndSupplierName(String cityName, String supplierName) {
		return supplierCityInfoRepository.findByCityNameAndSupplierName(cityName, supplierName);
	}

	public List<DbSupplierCityInfo> findByCityIdAndSupplierName(String cityName, String supplierName) {
		return supplierCityInfoRepository.findByCityIdAndSupplierName(cityName, supplierName);
	}
}
