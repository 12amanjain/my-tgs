package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorFilter;
import com.tgs.services.hms.dbmodel.DbHotelConfiguratorRule;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum HotelConfiguratorSearchPredicate {

	CREATED_ON {
		@Override
		public void addPredicate(Root<DbHotelConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	ENABLED {
		@Override
		public void addPredicate(Root<DbHotelConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getEnabled()))
				predicates.add(criteriaBuilder.equal(root.get("enabled"), filter.getEnabled()));
		}
	},
	RULETYPE {
		@Override
		public void addPredicate(Root<DbHotelConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getRuleType()))
				predicates.add(criteriaBuilder.equal(root.get("ruleType"), filter.getRuleType().getName()));
		}
	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DbHotelConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter, List<Predicate> predicates) {
			predicates.add(criteriaBuilder.equal(root.get("isDeleted"), filter.isDeleted()));
		}
	},
	EXITONMATCH {
		@Override
		public void addPredicate(Root<DbHotelConfiguratorRule> root, CriteriaQuery<?> query,
				CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter, List<Predicate> predicates) {
			if (Objects.nonNull(filter.getExitOnMatch()))
				predicates.add(criteriaBuilder.equal(root.get("exitOnMatch"), filter.getExitOnMatch()));
		}
	};

	public abstract void addPredicate(Root<DbHotelConfiguratorRule> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnHotelConfigFilter(Root<DbHotelConfiguratorRule> root,
			CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder, HotelConfiguratorFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (HotelConfiguratorSearchPredicate configuratorFilter : HotelConfiguratorSearchPredicate.values()) {
			configuratorFilter.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
