package com.tgs.services.hms.jparepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.tgs.filters.HotelConfiguratorInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.dbmodel.DbHotelConfiguratorRule;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;

@Service
public class HotelConfiguratorService extends SearchService<DbHotelConfiguratorRule> {

	@Autowired
	HotelConfiguratorRepository hotelConfiguratorRepository;

	@Autowired
	HotelConfiguratorHelper hotelConfiguratorHelper;

	public List<DbHotelConfiguratorRule> findAll(HotelConfiguratorInfoFilter queryFilter) {
		return super.search(queryFilter, hotelConfiguratorRepository);
	}

	public DbHotelConfiguratorRule findById(long id) {
		return hotelConfiguratorRepository.findOne(id);
	}

	public DbHotelConfiguratorRule save(DbHotelConfiguratorRule hotelConfigRule) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			if (!ObjectUtils.isEmpty(hotelConfigRule.getInclusionCriteria()))
				hotelConfigRule.getInclusionCriteria().cleanData();
			if (!ObjectUtils.isEmpty(hotelConfigRule.getExclusionCriteria()))
				hotelConfigRule.getExclusionCriteria().cleanData();
			hotelConfigRule.setProcessedOn(LocalDateTime.now());
			hotelConfigRule = hotelConfiguratorRepository.saveAndFlush(hotelConfigRule);
		}
		hotelConfiguratorHelper.process();
		return hotelConfigRule;
	}
}
