package com.tgs.services.hms.jparepository;

import java.util.List;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelMealBasis;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;

@Service
public interface HotelInfoService {

	public DbHotelInfo save(DbHotelInfo hotel);

	public DbHotelSupplierMapping save(DbHotelSupplierMapping supplier);

	public DbHotelInfo findByNameAndRatingAndCityNameAndCountryName(String name, String rating, String cityName, String countryName);

	public List<DbHotelInfo> findAllHotels(Pageable pageable);
	
	public List<Object[]> findHotelNameIdAddress(Pageable pageable);

	public List<DbHotelSupplierMapping> findAllHotelSupplierMapping(Pageable pageable);

	public DbHotelSupplierMapping findFirstBySupplierNameOrderByHotelIdDesc(String supplierName);

	public List<DbHotelInfo> findAllByOrderByIdAsc();

	public List<DbHotelInfo> findByIdGreaterThanOrderByIdAsc(Long id);

	public List<DbHotelInfo> findUnmappedhotels(String supplierName);

	public DbHotelInfo findById(Long id);
	
	public List<Object[]> findHotelBasicInfoByIdIn(List<Long> idList);

	public DbHotelSupplierMapping findMapping(String supplierName, Long hotelId);
	
	public List<DbHotelSupplierMapping> findAllBySupplierNameAndSourceName(String supplierName, String sourceName);

	public DbHotelMealBasis save(DbHotelMealBasis mealMapping);
	
	public DbHotelMealBasis findBySMealBasisAndSupplier(String smealBasis, String supplier);

	public List<DbHotelMealBasis> findAllHotelMeals(Pageable pageable);
	
	public List<DbHotelInfo> findByIdGreaterThanOrderByIdAscLimit(Long limit , Long cursor);
	
	public List<DbHotelSupplierMapping> findAllHotelSupplierMappingBySource(Pageable pageable, String sourceName);
	
}
