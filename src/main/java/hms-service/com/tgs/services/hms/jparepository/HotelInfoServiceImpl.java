package com.tgs.services.hms.jparepository;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelMealBasis;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;

@Service
public class HotelInfoServiceImpl implements HotelInfoService {

	@Autowired
	DbHotelInfoRepository hotelRepository;

	@Autowired
	DbHotelSupplierMappingRepository hotelSupplierMappingRepository;

	@Autowired
	DbHotelMealMappingRepository hotelMealMappingRepository;

	@Autowired
	DbHotelSupplierRepository supplierRepository;

	@Override
	public DbHotelInfo save(DbHotelInfo hotel) {
		return hotelRepository.save(hotel);
	}

	@Override
	public DbHotelSupplierMapping save(DbHotelSupplierMapping supplier) {
		return supplierRepository.save(supplier);
	}

	@Override
	public DbHotelInfo findByNameAndRatingAndCityNameAndCountryName(String name, String rating, String cityName, String countryName) {
		return hotelRepository.findByNameAndRatingAndCityNameAndCountryName(name.toUpperCase(), rating,
				cityName.toUpperCase(), countryName.toUpperCase());
	}

	@Override
	public List<DbHotelInfo> findAllHotels(Pageable pageable) {
		List<DbHotelInfo> hotelInfoList = new ArrayList<>();
		hotelRepository.findAll(pageable).forEach(hotelInfo -> hotelInfoList.add(hotelInfo));
		return hotelInfoList;
	}

	@Override
	public List<DbHotelSupplierMapping> findAllHotelSupplierMapping(Pageable pageable) {
		List<DbHotelSupplierMapping> mappingList = new ArrayList<>();
		hotelSupplierMappingRepository.findAll(pageable).forEach(mapping -> mappingList.add(mapping));
		return mappingList;
	}

	@Override
	public List<DbHotelInfo> findAllByOrderByIdAsc() {
		return hotelRepository.findAllByOrderByIdAsc();
	}

	@Override
	public DbHotelSupplierMapping findFirstBySupplierNameOrderByHotelIdDesc(String supplierName) {
		return hotelSupplierMappingRepository.findFirstBySupplierNameOrderByHotelIdDesc(supplierName);
	}

	@Override
	public List<DbHotelInfo> findByIdGreaterThanOrderByIdAsc(Long id) {
		return hotelRepository.findByIdGreaterThanOrderByIdAsc(id);
	}

	@Override

	public List<DbHotelInfo> findUnmappedhotels(String supplierName) {
		return hotelRepository.findHotelsRightJoinHotelSupplierMapping(supplierName);
	}

	@Override
	public DbHotelInfo findById(Long id) {
		return hotelRepository.findById(id);
	}

	@Override
	public DbHotelSupplierMapping findMapping(String supplierName, Long hotelId) {
		return hotelSupplierMappingRepository.findBySupplierNameAndHotelId(supplierName, hotelId);
	}

	@Override
	public DbHotelMealBasis findBySMealBasisAndSupplier(String smealBasis, String supplier) {
		return hotelMealMappingRepository.findBySMealBasisAndSupplier(smealBasis, supplier);
	}
	
	@Override
	public DbHotelMealBasis save(DbHotelMealBasis mealBasis) {
		return hotelMealMappingRepository.save(mealBasis);
	}

	@Override
	public List<DbHotelMealBasis> findAllHotelMeals(Pageable pageable) {
		List<DbHotelMealBasis> hotelMealBasisList = new ArrayList<>();
		hotelMealMappingRepository.findAll(pageable).forEach(hotelMealBasis -> hotelMealBasisList.add(hotelMealBasis));
		return hotelMealBasisList;
	}

	@Override
	public List<DbHotelInfo> findByIdGreaterThanOrderByIdAscLimit(Long limit, Long cursor) {
		return hotelRepository.findByIdGreaterThanOrderByIdAscLimit(cursor, limit);
	}

	@Override
	public List<Object[]> findHotelNameIdAddress(Pageable pageable) {
		return hotelRepository.findHotelNameIdAddress(pageable);
	}

	@Override
	public List<DbHotelSupplierMapping> findAllHotelSupplierMappingBySource(Pageable pageable, String sourceName) {
		return hotelSupplierMappingRepository.findAllBySourceName(pageable, sourceName);
	}

	public List<DbHotelSupplierMapping> findAllBySupplierNameAndSourceName(String supplierName, String sourceName) {
		return hotelSupplierMappingRepository.findAllBySupplierNameAndSourceName(supplierName, sourceName);
	}

	@Override
	public List<Object[]> findHotelBasicInfoByIdIn(List<Long> idList) {
		return hotelRepository.findNameRatingAddressByIdIn(idList);
	}
}
