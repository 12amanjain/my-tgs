package com.tgs.services.hms.jparepository;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.dbmodel.DbHotelNationalitySupplierMapping;

@Service
public interface HotelNationalityInfoService {
	
	public DbHotelNationalitySupplierMapping save(DbHotelNationalitySupplierMapping mapping);

}
