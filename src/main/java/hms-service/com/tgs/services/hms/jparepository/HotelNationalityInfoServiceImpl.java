package com.tgs.services.hms.jparepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.dbmodel.DbHotelNationalitySupplierMapping;

@Service
public class HotelNationalityInfoServiceImpl implements HotelNationalityInfoService{

	@Autowired
	DbHotelNationalityMappingRepository repository;
	
	@Override
	public DbHotelNationalitySupplierMapping save(DbHotelNationalitySupplierMapping mapping) {
		return repository.save(mapping);
	}

}
