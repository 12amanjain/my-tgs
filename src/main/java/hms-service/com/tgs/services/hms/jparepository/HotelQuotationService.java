package com.tgs.services.hms.jparepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.dbmodel.DbHotelQuotation;

@Service
public class HotelQuotationService {

	@Autowired
	DbHotelQuotationRepository dbHotelQuotationRepository;
	
	public DbHotelQuotation saveQuotation(DbHotelQuotation hotelQuotation) {
		return dbHotelQuotationRepository.save(hotelQuotation);
	}
	
	public DbHotelQuotation fetchQuotationFromId(Long id) {
		return dbHotelQuotationRepository.findOne(id);
	}
	
	public List<DbHotelQuotation> fetchAllUserQuotations(String userId){
		return dbHotelQuotationRepository.findByUserId(userId);
	}
	
	public void deleteQuotationFromId(Long id) {
		dbHotelQuotationRepository.delete(id);
	}
	
	public void deleteAllAgentsQuotations(Iterable<? extends DbHotelQuotation> quotations) {
		dbHotelQuotationRepository.delete(quotations);
	}
}
