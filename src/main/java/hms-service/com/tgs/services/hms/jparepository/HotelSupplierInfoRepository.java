package com.tgs.services.hms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.hms.dbmodel.DbHotelSupplierInfo;

public interface HotelSupplierInfoRepository
		extends JpaRepository<DbHotelSupplierInfo, Long>, JpaSpecificationExecutor<DbHotelSupplierInfo> {

	public List<DbHotelSupplierInfo> findByIsDeletedOrderBySourceIdAscProcessedOnDesc(boolean isDeleted);

}
