package com.tgs.services.hms.jparepository;

import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.HotelSupplierInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.hms.dbmodel.DbHotelSupplierInfo;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;

@Service
public class HotelSupplierInfoService extends SearchService<DbHotelSupplierInfo> {

	@Autowired
	HotelSupplierInfoRepository supplierInfoRepo;

	@Autowired
	HotelSupplierConfigurationHelper confHelper;

	public List<DbHotelSupplierInfo> findAll(boolean isDeleted) {
		return supplierInfoRepo.findByIsDeletedOrderBySourceIdAscProcessedOnDesc(isDeleted);
	}

	public List<DbHotelSupplierInfo> findAll(@Valid HotelSupplierInfoFilter filter) {
		return super.search(filter, supplierInfoRepo);
	}

	public DbHotelSupplierInfo findById(Long id) {
		return supplierInfoRepo.findOne(id);
	}

	public DbHotelSupplierInfo save(DbHotelSupplierInfo supplierInfo) {
		supplierInfo.getCredentialInfo().cleanData();
		supplierInfo.setProcessedOn(LocalDateTime.now());
		DbHotelSupplierInfo sInfo = supplierInfoRepo.saveAndFlush(supplierInfo);
		confHelper.process();
		return sInfo;
	}

}
