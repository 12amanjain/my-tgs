package com.tgs.services.hms.jparepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.helper.HotelSourceType;

@Service
public class HotelSupplierMappingService {

	@Autowired
	HotelInfoService hotelInfoService;

	public DbHotelSupplierMapping save(DbHotelSupplierMapping supplierMappingInfo) {
		if (isValidSourceName(supplierMappingInfo)) {
			return hotelInfoService.save(supplierMappingInfo);
		}
		return null;
	}

	private boolean isValidSourceName(DbHotelSupplierMapping supplierMappingInfo) {
		return HotelSourceType.getHotelSourceTypeFromSourceName(supplierMappingInfo.getSourceName()) != null ? true : false;
	}

}
