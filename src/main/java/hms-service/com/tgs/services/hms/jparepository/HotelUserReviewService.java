package com.tgs.services.hms.jparepository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;

@Service
public interface HotelUserReviewService {
	
	public void save(DbHotelUserReviewIdInfo hotelUserReviewIdInfo);
	public List<DbHotelUserReviewIdInfo> findAll(Pageable pageable);
}