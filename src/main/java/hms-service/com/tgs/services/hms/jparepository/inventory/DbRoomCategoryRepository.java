package com.tgs.services.hms.jparepository.inventory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.inventory.DbRoomCategory;

@Repository
public interface DbRoomCategoryRepository extends JpaRepository<DbRoomCategory, Long>, JpaSpecificationExecutor<DbRoomCategory> {
	
	public DbRoomCategory findById(Long id);
	public List<DbRoomCategory> findByIdIn(List<Long> ids);

}
