package com.tgs.services.hms.jparepository.inventory;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import com.tgs.services.hms.dbmodel.inventory.DbRoomTypeInfo;

@Repository
public interface DbRoomTypeRepository extends JpaRepository<DbRoomTypeInfo, Long>, JpaSpecificationExecutor<DbRoomTypeInfo> {
	
	public DbRoomTypeInfo findById(Long id);
	public List<DbRoomTypeInfo> findByIdIn(List<Long> ids);
}