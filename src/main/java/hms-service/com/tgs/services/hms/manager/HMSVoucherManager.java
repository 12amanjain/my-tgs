package com.tgs.services.hms.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.communicator.VoucherServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.GeneralRuleField;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.HotelBasicRuleField;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.vms.restmodel.HotelVoucherValidateRequest;
import com.tgs.services.vms.restmodel.HotelVoucherValidateResponse;
import com.tgs.services.voucher.datamodel.HotelVoucherRuleCriteriaOutput;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.datamodel.VoucherConfigurationRule;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HMSVoucherManager {

	@Autowired
	HotelOrderItemCommunicator orderItemCommunicator;

	@Autowired
	VoucherServiceCommunicator voucherService;

	@Autowired
	HMSCommunicator hmsCommunicator;

	private static Map<String, ? extends IRuleField> gnResolverMap = EnumUtils.getEnumMap(GeneralRuleField.class);
	private static Map<String, ? extends IRuleField> hotelResolvedMap = EnumUtils.getEnumMap(HotelBasicRuleField.class);

	private static Map<String, IRuleField> resolverMap = new HashMap<>();;

	static {
		resolverMap.putAll(gnResolverMap);
		resolverMap.putAll(hotelResolvedMap);
	}

	private HotelInfo fetchHotelInfoByBookingId(String bookingId) {
		HotelInfo hInfo = null;
		try {
			HotelReviewResponse reviewResponse = hmsCommunicator.getHotelReviewResponse(bookingId);
			if (reviewResponse != null && Objects.nonNull(reviewResponse.getHInfo())) {
				hInfo = reviewResponse.getHInfo();
			}
		} catch (Exception e) {
			log.info("HotelReviewResponse not avilable for the booking id {} ", bookingId, e);
		}
		if (Objects.isNull(hInfo)) {
			hInfo = orderItemCommunicator.getHotelInfo(bookingId);
			if (Objects.isNull(hInfo)) {
				throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
			}
		}
		return hInfo;
	}

	public HotelVoucherValidateResponse calculateVoucherDiscount(HotelVoucherValidateRequest request,
			boolean isApplyDiscountOnHotel) {
		ContextData contextData = SystemContextHolder.getContextData();
		HotelVoucherValidateResponse response = new HotelVoucherValidateResponse();
		if (StringUtils.isNotBlank(request.getBookingId()) && CollectionUtils.isNotEmpty(request.getConfigurations())) {
			HotelInfo hotelInfo =
					ObjectUtils.firstNonNull(request.getHotelInfo(), fetchHotelInfoByBookingId(request.getBookingId()));
			VoucherConfiguration configuration = request.getConfigurations().get(0);

			HotelVoucherRuleCriteriaOutput voucherCriteria = null;
			if (!hotelInfo.isVoucherApplied()) {
				VoucherConfigurationRule voucherRule =
						getVoucherRuleOnHotel(request, contextData, hotelInfo, configuration);
				if (voucherRule != null) {
					voucherCriteria =
							new GsonMapper<>(voucherRule.getOutput(), HotelVoucherRuleCriteriaOutput.class).convert();
					HotelVoucherValidateResponse validateResponse =
							applyDiscountOnHotel(voucherCriteria, hotelInfo, isApplyDiscountOnHotel);
					response.setDiscountedAmount(
							response.getDiscountedAmount() + validateResponse.getDiscountedAmount());
					if (isApplyDiscountOnHotel) {
						voucherRule.setQuantityConsumed(voucherRule.getQuantityConsumed() + 1);
					}
				}
			}
			if (isApplyDiscountOnHotel) {
				voucherService.saveVoucherConfiguration(configuration);
			}
			response.setHotelInfo(hotelInfo);
		}
		return response;
	}

	public HotelVoucherValidateResponse applyDiscountOnHotel(HotelVoucherRuleCriteriaOutput voucherCriteria,
			HotelInfo hotelInfo, boolean isModifyFD) {
		HotelVoucherValidateResponse voucherResponse = new HotelVoucherValidateResponse();
		AtomicDouble totalDiscount = new AtomicDouble();
		Option option = hotelInfo.getOptions().get(0);
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {

				if (validateVoucherConditions(voucherCriteria, hotelInfo)) {
					double voucherDiscount =
							BaseHotelUtils.evaluateHotelExpression(voucherCriteria.getExpression(), priceInfo);
					Double discount = voucherCriteria.getDiscountAmount(voucherDiscount);
					calcOrApplyVoucherDiscount(priceInfo, isModifyFD, discount);
					totalDiscount.getAndAdd(discount);
				}
			}
		}
		voucherResponse.setDiscountedAmount(totalDiscount.get());
		return voucherResponse;
	}

	private void calcOrApplyVoucherDiscount(PriceInfo priceInfo, boolean isModify, Double totalDiscount) {

		if (isModify) {
			priceInfo.getFareComponents().put(HotelFareComponent.VD, totalDiscount);
		}
	}

	private VoucherConfigurationRule getVoucherRuleOnHotel(HotelVoucherValidateRequest request, ContextData contextData,
			HotelInfo hotelInfo, VoucherConfiguration configuration) {
		List<VoucherConfigurationRule> configurationRules = configuration.getVoucherRules().stream()
				.filter(rule -> (Product.HOTEL.equals(rule.getProduct()) || Product.NA.equals(rule.getProduct()))
						&& rule.getRemainingQty() > 0)
				.collect(Collectors.toList());
		User user = contextData.getUser();
		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromHotelInfo(hotelInfo)
				.generateFactFromUserId(UserUtils.getUserId(user));
		hotelFact.setRole(user.getRole());
		hotelFact.setPaymentMediums(request.getMediums());
		hotelFact.setChannelType(contextData.getHttpHeaders().getChannelType());
		hotelFact.setRole(user.getRole());
		hotelFact.setPartnerId(UserUtils.getPartnerUserId(user));
		hotelFact.setPaxCount(hotelInfo.getOptions().get(0).getPaxCount());
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(configurationRules, hotelFact, resolverMap);
		List<VoucherConfigurationRule> rules = (List<VoucherConfigurationRule>) ruleEngine.fireAllRules();
		if (CollectionUtils.isNotEmpty(rules)) {
			return rules.get(0);
		}
		return null;
	}

	public static boolean validateVoucherConditions(HotelVoucherRuleCriteriaOutput condition, HotelInfo hInfo) {
		boolean isApplicable = true;
		/*
		 * Currently, we don't apply any booking condition while applying promotional code.
		 */
		return isApplicable;
	}
}
