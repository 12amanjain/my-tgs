package com.tgs.services.hms.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchCriteria;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.ums.datamodel.User;

@Service
public class HotelBookingEngine {

	@Autowired
	HMSCachingServiceCommunicator cachingService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelSearchResultProcessingManager searchResultProcessingManager;

	@Autowired
	HotelOrderItemCommunicator itemComm;

	@Autowired
	private CommercialCommunicator commericialCommunicator;

	public HotelInfo getHotelInfo(String bookingId) {

		HotelReviewResponse reviewResponse = cacheHandler.getHotelReviewFromCache(bookingId);
		return reviewResponse.getHInfo();
	}


	public HotelReviewResponse getHotelReviewResponse(String bookingId) {
		return cacheHandler.getHotelReviewFromCache(bookingId);
	}

	public void callSupplierBook(HotelInfo hInfo, Order order) {
		ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			AbstractHotelBookingFactory factory = HotelSourceType.getBookingFactoryInstance(hInfo, order);
			factory.doBooking();
			itemComm.updateOrderItem(hInfo, order, null);
		});
	}


	public HotelImportedBookingInfo retrieveBooking(HotelImportBookingParams importBookingParams, User user)
			throws Exception {

		AbstractRetrieveHotelBookingFactory factory =
				HotelSourceType.getRetrieveBookingFactoryInstance(importBookingParams);
		HotelImportedBookingInfo bookingDetailResponse = factory.retrieveBookingDetails();
		populateMissingInfo(bookingDetailResponse.getHInfo(), bookingDetailResponse.getSearchQuery());
		searchResultProcessingManager.updateManagementFees(bookingDetailResponse.getHInfo());
		searchResultProcessingManager.buildProcessOptions(bookingDetailResponse.getHInfo(),
				bookingDetailResponse.getSearchQuery());
		commericialCommunicator.processUserCommissionInHotel(bookingDetailResponse.getHInfo(), user,
				bookingDetailResponse.getSearchQuery());
		return bookingDetailResponse;
	}

	public void populateMissingInfo(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		if (searchQuery.getSourceId() != null) {
			searchResultProcessingManager.updateStaticData(hInfo, searchQuery, OperationType.BOOKING_DETAILS);
			HotelSearchCriteria searchCriteria = new HotelSearchCriteria();
			searchCriteria.setCityName(hInfo.getAddress().getCity().getName());
			searchCriteria.setCountryName(hInfo.getAddress().getCountry().getName());
			searchQuery.setSearchCriteria(searchCriteria);

		}
	}

	public boolean callSupplierConfirmBook(HotelInfo hInfo, Order order) {

		AbstractHotelBookingFactory factory = HotelSourceType.getBookingFactoryInstance(hInfo, order);
		return factory.doConfirmBooking();
	}

	public void updateBookingStatus(HotelInfo hInfo, Order order) {

		AbstractHotelBookingFactory factory = HotelSourceType.getBookingFactoryInstance(hInfo, order);
		factory.updateBookingStatus();
	}
	
	public void updateHotelConfirmationNumber(HotelInfo hInfo, Order order) {

		AbstractHotelBookingFactory factory = HotelSourceType.getBookingFactoryInstance(hInfo, order);
		factory.updateHotelConfirmationNumber();
	}
	
}
