package com.tgs.services.hms.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSearch;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelDetailManager {

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HotelSearch hotelSearch;

	@Autowired
	HMSCachingServiceCommunicator cachingService;

	@Autowired
	HotelCacheHandler cacheHandler;

	public void fetchHotelDetails(HotelSearchQuery searchQuery, HotelInfo hInfo, ContextData contextData)
			throws Exception {
		if (hInfo == null) {
			log.error("HInfo is null for searchQuery {}", searchQuery);
			throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
		}
		try {
			searchManager.fetchHotelDetails(searchQuery, hInfo);
		} catch (Exception e) {
			log.error("Unable to fetch hotel details directly from API , hence doing search again", e);

			hInfo = searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
			searchManager.fetchHotelDetails(searchQuery, hInfo);
		}
	}
	
}
