package com.tgs.services.hms.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.dbmodel.inventory.DbHotelRatePlan;
import com.tgs.services.hms.jparepository.inventory.HotelRatePlanService;

@Service
public class HotelRatePlanManager {
	
	@Autowired
	private HotelRatePlanService ratePlanService;
	
	public List<HotelRatePlan> fetchHotelRatePlan(HotelRatePlanFilter ratePlanFilter) {

		List<HotelRatePlan> ratePlans = new ArrayList<>();
		ratePlanService.findAll(ratePlanFilter).forEach(supplier -> {
			ratePlans.add(supplier.toDomain());
		});
		return ratePlans;
	}
	
	public void storeRatePlanList(List<HotelRatePlan> ratePlanList) {
		List<DbHotelRatePlan> dbRatePlanList = new ArrayList<>();
		ratePlanList.forEach(ratePlan -> {
			dbRatePlanList.add(new DbHotelRatePlan().from(ratePlan));
		});
		ratePlanService.saveRatePlans(dbRatePlanList);
	}

}
