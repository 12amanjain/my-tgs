package com.tgs.services.hms.manager;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.HotelMissingInfoLoggingUtil;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbCityInfoMapping;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelSearch;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.jparepository.HotelCityInfoService;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSearchManager {

	@Autowired
	HotelSearch hotelSearch;

	@Autowired
	HotelInfoService hotelInfoService;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelCityInfoService cityInfoService;

	@Autowired
	HotelSearchResultProcessingManager searchResultProcessingManager;

	public void search(HotelSearchQuery searchQuery) {

		try {
			ContextData contextData = SystemContextHolder.getContextData();
			if (CollectionUtils.isNotEmpty(searchQuery.getSupplierIds())) {
				/*
				 * Search for specific suppliers mentioned in Search Query
				 */
				List<String> supplierIds = new ArrayList<>(searchQuery.getSupplierIds());
				List<HotelSupplierInfo> hotelSupplierInfoList =
						HotelSupplierConfigurationHelper.getSupplierListFromSupplierIds(supplierIds);
				doSearch(searchQuery, contextData, hotelSupplierInfoList,
						new AtomicInteger(hotelSupplierInfoList.size()));
				return;
			}

			/*
			 * Search for each source in parallel
			 */
			searchQuery.setSourceIds(HotelSourceType.getAllSourceIds());
			List<HotelSupplierInfo> supplierList = HotelUtils.getHotelSupplierInfo(searchQuery, contextData);

			if (CollectionUtils.isEmpty(supplierList)) {
				log.info("No supplier is enabled for search id {}", searchQuery.getSearchId());
				return;
			}

			for (Iterator<HotelSupplierInfo> supplierInfoIterator = supplierList.iterator(); supplierInfoIterator
					.hasNext();) {
				HotelSupplierInfo supplierInfo = supplierInfoIterator.next();
				if (!isSearchRequestValidForSource(searchQuery,
						HotelSourceType.getHotelSourceType(supplierInfo.getSourceId()))) {
					supplierInfoIterator.remove();
				}
			}

			if (CollectionUtils.isEmpty(supplierList)) {
				log.info("No city mapping is configured for any supplier for search id {}", searchQuery.getSearchId());
				return;
			}
			log.info("Started hotel search with search id {} for suppliers {}", searchQuery.getSearchId(),
					supplierList.stream().map(HotelSupplierInfo::getName).collect(Collectors.toList()));

			AtomicInteger searchCount = new AtomicInteger(supplierList.size());
			cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
			doSearch(searchQuery, contextData, supplierList, searchCount);
		} finally {
			cacheHandler.cacheSearchResultCompleteAt(searchQuery);
			LogUtils.clearLogList();
		}
	}

	public HotelSearchResponse getSearchResult(String searchId) throws Exception {

		HotelSearchResult searchResult = null;
		boolean isSearchCompleted = false;
		HotelSearchResponse searchResponse = new HotelSearchResponse();
		int attemptCount = 0;
		do {
			isSearchCompleted = cacheHandler.isSearchCompleted(searchId);
			if (isSearchCompleted) {
				searchResult = cacheHandler.getSearchResult(searchId);
				if (searchResult != null) {
					// filterHotelsBasedOnLimit(searchQuery, searchResult);
					searchResponse.setSearchResult(searchResult);
					return searchResponse;
				} else {
					SystemContextHolder.getContextData().getErrorMessages().add("Cross sell search result is empty");
					log.info("Cross sell search result is empty for search query {}", searchId);
					throw new CustomGeneralException(SystemError.EMPTY_SEARCH_RESULT);
				}
			}
			attemptCount++;
			Thread.sleep(3 * 1000);
		} while (!isSearchCompleted && attemptCount < 20);
		return null;
	}

	public boolean isSearchRequestValidForSource(HotelSearchQuery searchQuery, HotelSourceType sourceType) {

		String sourceName = sourceType.name();
		if (HotelUtils.isCrossSell(searchQuery.getSearchId())) {
			return true;
		}
		boolean isValid = true;

		CityInfoMapping supplierCityInfo =
				cacheHandler.getSupplierCityInfoFromCityId(searchQuery.getSearchCriteria().getCityId(), sourceName);
		if (supplierCityInfo == null && !sourceType.getisValidSourceIfCityMappingNull()) {
			isValid = false;
		}
		return isValid;


	}

	public HotelSearchResult doSearch(HotelSearchQuery searchQuery, ContextData contextData,
			List<HotelSupplierInfo> supplierList, AtomicInteger searchCount) {

		ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				List<Future<?>> suppliersResultToProcess = new ArrayList<Future<?>>();
				supplierList.forEach(supplierInfo -> {
					Future<?> supplierWiseSearchResult = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
						HotelSearchResponse searchResponse = new HotelSearchResponse();
						HotelSearchResult searchResult = null;
						HotelSearchQuery copyQuery = null;
						try {
							log.info("Search started with search id {} for supplier {}", searchQuery.getSearchId(),
									supplierInfo.getName());
							copyQuery = new GsonMapper<>(searchQuery, HotelSearchQuery.class).convert();
							copyQuery.setMiscInfo(
									HotelSearchQueryMiscInfo.builder().supplierId(supplierInfo.getSupplierId())
											.sourceId(supplierInfo.getSourceId().toString()).build());
							copyQuery.setSourceId(supplierInfo.getSourceId());
							ContextData copyContextData = contextData.deepCopy();
							SystemContextHolder.setContextData(copyContextData);
							searchResult = hotelSearch.search(copyQuery, copyContextData);
							processAndPopulateCheckpointInfo(contextData);
						} catch (Exception e) {
							log.error("Error while searching for searchId {} , supplier {}", copyQuery.getSearchId(),
									copyQuery.getMiscInfo().getSupplierId(), e);
						} finally {
							log.info("Search completed with search id {} for supplier {}", searchQuery.getSearchId(),
									supplierInfo.getName());
							searchResponse.setSearchQuery(copyQuery);
							searchResponse.setSearchResult(searchResult);
						}
						return searchResponse;
					});
					suppliersResultToProcess.add(supplierWiseSearchResult);
				});

				processSupplierResult(suppliersResultToProcess, searchQuery, supplierList, searchCount);
				log.info("Finished hotel search with search id {} for suppliers {}", searchQuery.getSearchId(),
						supplierList.stream().map(HotelSupplierInfo::getName).collect(Collectors.toList()));
			} catch (ExecutionException | InterruptedException e) {
				log.error("Error while searching for hotels due to {} for searchId {} ", e.getMessage(),
						searchQuery.getSearchId(), e);
			} catch (Exception e) {
				log.error("Error while searching for hotels for searchId {} ", searchQuery.getSearchId(), e);
			}
		});
		return null;
	}

	private void processSupplierResult(List<Future<?>> suppliersResultToProcess, HotelSearchQuery searchQuery,
			List<HotelSupplierInfo> supplierList, AtomicInteger searchCount) throws Exception {

		Set<String> processedTasks = new HashSet<>(); // To make sure no supplier will get reprocessed.
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		int searchRetryCount = ObjectUtils.isEmpty(configuratorInfo.getSearchRetryCount()) ? 60
				: configuratorInfo.getSearchRetryCount();
		do {
			AtomicInteger supplierResponseFetchedCount = new AtomicInteger();
			try {
				List<HotelSearchResponse> searchResultFromMultipleSuppliers = fetchTasksIfNotProcessedYet(
						suppliersResultToProcess, processedTasks, supplierResponseFetchedCount);
				log.info(
						"Search result from multiple suppliers for search id {}, processed tasks {} and size {}, fetch count {} and supplier list size {}, searchRetryCount {}",
						searchQuery.getSearchId(), processedTasks, processedTasks.size(),
						supplierResponseFetchedCount.get(), supplierList.size(), searchRetryCount);
				if (CollectionUtils.isNotEmpty(searchResultFromMultipleSuppliers)) {
					processHotelSearchResponses(searchResultFromMultipleSuppliers);
					searchCount.set(searchCount.get() - supplierResponseFetchedCount.get());
					cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
				} else {
					Thread.sleep(1000);
				}
			} catch (Exception e) {
				searchCount.set(0);
				cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
				log.error("Search failed while searching for hotels for searchId {} ", searchQuery.getSearchId(), e);
				throw e;
			}
			if (--searchRetryCount < 0 || processedTasks.size() == supplierList.size()) {
				log.info(
						"Either search processing retry threshold reached or all suppliers are processed for searchId {}. {}",
						searchQuery.getSearchId(), processedTasks.size() == supplierList.size());
				return;
			}
		} while (true);
	}

	private List<HotelSearchResponse> fetchTasksIfNotProcessedYet(List<Future<?>> suppliersResultToProcess,
			Set<String> processedTasks, AtomicInteger supplierResponseFetchedCount)
			throws ExecutionException, InterruptedException {

		List<HotelSearchResponse> searchResultFromMultipleSuppliers = new ArrayList<>();
		for (Future<?> singleSupplierResult : suppliersResultToProcess) {
			if (singleSupplierResult.isDone()) {
				Object searchResult = singleSupplierResult.get();
				HotelSearchResponse searchResponse = (HotelSearchResponse) searchResult;
				if (!processedTasks.contains(searchResponse.getSearchQuery().getMiscInfo().getSupplierId())) {
					searchResultFromMultipleSuppliers.add(searchResponse);
					supplierResponseFetchedCount.getAndIncrement();
					processedTasks.add(searchResponse.getSearchQuery().getMiscInfo().getSupplierId());
				}
			}
		}
		return searchResultFromMultipleSuppliers;
	}

	private void processHotelSearchResponses(List<HotelSearchResponse> searchResponses) {

		HotelSearchQuery searchQuery = searchResponses.get(0).getSearchQuery();
		List<String> supplierIds = new ArrayList<>();

		/*
		 * Created single hotel search result from multiple search result
		 */
		HotelSearchResult searchResult = new HotelSearchResult();
		for (HotelSearchResponse searchResponse : searchResponses) {
			if (searchResponse.getSearchResult().getNoOfHotelOptions() > 0) {
				searchResult.getHotelInfos().addAll(searchResponse.getSearchResult().getHotelInfos());
				supplierIds.add(searchResponse.getSearchQuery().getMiscInfo().getSupplierId());
			} else {
				log.info("No result found for search id {} from supplier {}", searchQuery.getSearchId(),
						searchResponse.getSearchQuery().getMiscInfo().getSupplierId());
			}
		}

		if (CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
			searchQuery.getMiscInfo().setSupplierIds(supplierIds); // This is required for better logging
			log.info("Processing of merged hotels for search id {} and suppliers {} are {}", searchQuery.getSearchId(),
					supplierIds, searchResult.getNoOfHotelOptions());
			searchResultProcessingManager.combinePreviousResult(searchResult, searchQuery);
			log.info("Processing completed for merged hotels for search id {} and suppliers {} are {}",
					searchQuery.getSearchId(), supplierIds, searchResult.getNoOfHotelOptions());
		}
	}

	private void processAndPopulateCheckpointInfo(ContextData contextData) {

		ContextData copyContextData = SystemContextHolder.getContextData();
		Map<String, List<CheckPointData>> copyCheckPointInfoMap =
				copyContextData.getCheckPointInfo().entrySet().stream()
						.filter(map -> !(map.getKey().equals(SystemCheckPoint.REQUEST_STARTED.name())
								|| map.getKey().equals(SystemCheckPoint.REQUEST_FINISHED.name())))
						.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

		copyCheckPointInfoMap.entrySet().forEach(copyCheckPointInfo -> {
			copyCheckPointInfo.getValue().stream().forEach(checkpoint -> {
				checkpoint.setParallel(true);
				checkpoint.setSubType(HotelFlowType.SEARCH.name());
			});
			contextData.addCheckPoints(copyCheckPointInfo.getValue());
		});
	}

	public void fetchHotelDetails(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		hotelSearch.fetchDetails(searchQuery, hInfo);

	}

	public static void setSearchId(HotelSearchQuery searchQuery) {
		String sourceIdStr = RandomStringUtils.random(10, false, true);
		String searchId = String.join("", "hsid", sourceIdStr);
		searchQuery.setSearchId(searchId);
	}

	public void setCountryAndCityName(HotelSearchQuery searchQuery) {
		CityInfo cityInfo = cacheHandler.getCityInfoFromCityId(searchQuery.getSearchCriteria().getCityId());
		if (!ObjectUtils.isEmpty(cityInfo)) {
			searchQuery.getSearchCriteria().setCityName(cityInfo.getCityName());
			searchQuery.getSearchCriteria().setCountryName(cityInfo.getCountryName());
		}
		logMissingCityInfo(searchQuery);
	}

	public void setCountryAndCityFromIATA(HotelSearchQuery searchQuery) {
		CityInfo cityInfo = cacheHandler
				.getCityAndCountryInfo(searchQuery.getSearchPreferences().getCrossSellParameter().getIataCode());
		if (!ObjectUtils.isEmpty(cityInfo)) {
			searchQuery.getSearchCriteria().setCityName(cityInfo.getCityName());
			searchQuery.getSearchCriteria().setCityId(String.valueOf(cityInfo.getId()));
			searchQuery.getSearchCriteria().setCountryId(cityInfo.getCountryId());
		}
	}

	public HotelInfo doSearchAgainWithSameSuppliers(HotelSearchQuery searchQuery, HotelInfo hInfo,
			ContextData contextData) throws Exception {


		List<String> supplierIds = HotelUtils.getSupplierIdsFromHotelInfo(hInfo);
		List<HotelSupplierInfo> hotelSupplierInfoList =
				HotelSupplierConfigurationHelper.getSupplierListFromSupplierIds(supplierIds);
		AtomicInteger searchCount = new AtomicInteger(hotelSupplierInfoList.size());
		cacheHandler.storeSearchSpiltCount(searchQuery.getSearchId(), searchCount.get());
		doSearch(searchQuery, contextData, hotelSupplierInfoList, searchCount);
		HotelSearchResult searchResult = null;
		boolean isSearchCompleted = false;
		String hotelName = hInfo.getName();
		int attemptCount = 0;
		do {
			isSearchCompleted = cacheHandler.isSearchCompleted(searchQuery.getSearchId());
			if (isSearchCompleted) {
				searchResult = cacheHandler.getSearchResult(searchQuery.getSearchId());
				if (searchResult != null) {
					hInfo = HotelUtils.filterHotel(searchResult, hInfo);
					if (hInfo == null) {
						contextData.getErrorMessages().add("Not available in new search result for searchId");
						log.info("Hotel {} Not available in new search result for new searchId {} ", hotelName,
								searchQuery.getSearchId());
						throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
					}
				} else {
					contextData.getErrorMessages().add("Search Result Empty while we do search again on review page");
					log.info(
							"Search result empty while we do search again on review page for hotel {} , new searchId {}",
							hotelName, searchQuery.getSearchId());
					throw new CustomGeneralException(SystemError.EMPTY_SEARCH_RESULT);
				}
				break;
			}
			attemptCount++;
			Thread.sleep(3 * 1000);
		} while (!isSearchCompleted && attemptCount < 20);
		hInfo = cacheHandler.getCachedHotelById(hInfo.getId());
		return hInfo;
	}

	public HotelSearchQuery getSearchQuery(String id) {
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(id);
		if (HotelUtils.isCrossSell(id) && ObjectUtils.isEmpty(searchQuery)) {
			String cleanedId = HotelUtils.removeCrossSellFromSearchId(id);
			searchQuery = HotelUtils.getHotelSearchQuery(cleanedId);
			if (ObjectUtils.isEmpty(searchQuery)) {
				throw new CustomGeneralException(SystemError.INVALID_ID);
			}
			HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
		}
		return searchQuery;
	}

	public HotelSearchResponse searchCrossSellHotels(HotelSearchQuery searchQuery) {
		HotelSearchResponse searchResponse = new HotelSearchResponse();
		HotelSearchResult searchResult = cacheHandler.getSearchResult(searchQuery.getSearchId());
		if (ObjectUtils.isEmpty(searchResult)) {
			search(searchQuery);
		} else {
			HotelUtils.filterCrossSellHotels(searchQuery, searchResult);
			HotelUtils.filterHotelsBasedOnLimit(searchQuery, searchResult);
		}
		searchResponse.setSearchResult(searchResult);
		searchResponse.setSearchQuery(searchQuery);
		return searchResponse;
	}

	@SuppressWarnings("unchecked")
	private void logMissingCityInfo(HotelSearchQuery searchQuery) {
		ExecutorService executor = ExecutorUtils.getHotelSearchThreadPool();
		executor.submit(() -> {
			try {
				List<String> supplierNames = Arrays.asList(HotelSourceType.values()).stream()
						.map(x -> x.name().toLowerCase()).collect(Collectors.toList());
				supplierNames.remove("expediacrosssell");
				List<DbCityInfoMapping> cityInfoMapping = cityInfoService.findByCityIdAndSupplierList(
						Long.parseLong(searchQuery.getSearchCriteria().getCityId()), supplierNames);
				List<String> supplierNames2 = cityInfoMapping.stream().map(x -> x.getSupplierName().toLowerCase())
						.collect(Collectors.toList());
				Set<String> missingCityInfoSuppliers =
						new HashSet<>((ArrayList<String>) CollectionUtils.disjunction(supplierNames, supplierNames2));

				HotelMissingInfo missingInfo = new HotelMissingInfo();
				missingInfo.setCountryName(searchQuery.getSearchCriteria().getCountryName());
				missingInfo.setCityName(searchQuery.getSearchCriteria().getCityName());
				missingInfo.setSupplierNames(missingCityInfoSuppliers);
				missingInfo.setOperationType(OperationType.CITY_MAPPING);
				HotelMissingInfoLoggingUtil.addToLogList(missingInfo, searchQuery);
			} catch (Exception e) {
				log.info("Unable to generate search query for bookingId {}", e);
			} finally {
				HotelMissingInfoLoggingUtil.clearLog(LogTypes.MISSING_INFO);
			}
		});
	}

	public void updateSearchQueryForNewSearch(HotelSearchQuery searchQuery) {

		HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
		setCountryAndCityName(searchQuery);

	}

	public void doFullSearchAgain(HotelSearchQuery searchQuery, ContextData contextData) {
		search(searchQuery);
	}

}
