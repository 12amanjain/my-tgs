package com.tgs.services.hms.manager;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.HotelMissingInfoLoggingUtil;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelKeyInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelPromotion;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelTag;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.ProcessedOption;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPromotionOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelPropertyCategoryOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.PropertyCriteria;
import com.tgs.services.hms.datamodel.hotelconfigurator.PropertyType;
import com.tgs.services.hms.helper.CacheType;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelConstants;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSearchResultProcessingManager {

	@Autowired
	private HotelUserFeeManager userFeeManager;

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	private CommercialCommunicator commericialCommunicator;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	public void processSearchResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		User user = SystemContextHolder.getContextData().getUser();
		CityInfo cityInfo = cacheHandler.getCityInfoFromCityId(searchQuery.getSearchCriteria().getCityId());
		ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		searchResult.getHotelInfos().stream().forEach(hotelInfo -> {
			populateCityAndCountry(hotelInfo, cityInfo, searchQuery);
			filterOption(hotelInfo, searchQuery, clientInfo);
			processHotelFareComponents(hotelInfo, searchQuery, user);
		});
		process(searchResult, searchQuery);
	}

	public void processDetailResult(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		User user = SystemContextHolder.getContextData().getUser();
		processHotelFareComponents(hInfo, searchQuery, user);
		for (Option option : hInfo.getOptions()) {
			option.setIsPanRequired(isPanRequired(option));
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				BaseHotelUtils.updateManagementFee(roomInfo);
			}
		}
		updateTotalFareComponents(hInfo);
		commericialCommunicator.processUserCommissionInHotel(hInfo, user, searchQuery);
	}

	private void populateCityAndCountry(HotelInfo hInfo, CityInfo cityInfo, HotelSearchQuery searchQuery) {


		City city = null;
		Country country = null;
		if (cityInfo != null || searchQuery.getSearchCriteria().getCityName() != null) {
			city = City.builder()
					.name(cityInfo != null ? cityInfo.getCityName() : searchQuery.getSearchCriteria().getCityName())
					.build();
			country = Country.builder().name(
					cityInfo != null ? cityInfo.getCountryName() : searchQuery.getSearchCriteria().getCountryName())
					.build();
			if (ObjectUtils.isEmpty(hInfo.getAddress())) {
				hInfo.setAddress(Address.builder().build());
			}
		}
		hInfo.getAddress().setCity(city);
		hInfo.getAddress().setCountry(country);
	}

	private void filterOption(HotelInfo hotelInfo, HotelSearchQuery searchQuery, ClientGeneralInfo clientInfo) {

		if (!ObjectUtils.isEmpty(hotelInfo)) {
			for (Iterator<Option> optionIterator = hotelInfo.getOptions().iterator(); optionIterator.hasNext();) {
				Option option = optionIterator.next();
				if (!isValidOption(hotelInfo, option, searchQuery, clientInfo))
					optionIterator.remove();
			}
		}
	}

	private boolean isValidOption(HotelInfo hotelInfo, Option option, HotelSearchQuery searchQuery,
			ClientGeneralInfo clientInfo) {

		Integer roomCountInSearchQuery = searchQuery.getRoomInfo().size();
		if (CollectionUtils.isEmpty(option.getRoomInfos()) || option.getRoomInfos().size() != roomCountInSearchQuery) {
			log.debug(
					"Removed Option for hotel {} due to either no room available or roomCount in option doesn't match "
							+ "with user criteria",
					hotelInfo.getName());
			return false;
		} else if (option.getIsOptionOnRequest()) {
			if (clientInfo.getIsOnRequestAllowed() != null && !clientInfo.getIsOnRequestAllowed())
				return false;
			if (clientInfo.getOnRequestHours() != null) {
				Integer requiredHours = clientInfo.getOnRequestHours();
				Long actualHours = Duration
						.between(LocalDateTime.now(), searchQuery.getCheckinDate().atTime(LocalTime.now())).toHours();
				if (requiredHours != null && actualHours < requiredHours) {
					log.debug(
							"Removed Option for hotel {} due to On Request Not Allowed"
									+ " for targetHours {}, actualHours {}",
							hotelInfo.getName(), requiredHours, actualHours);
					return false;
				}
			}
		}
		return true;
	}

	private void process(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {
		try {
			LogUtils.log(LogTypes.HOTEL_SINGLE_SUPPLIER_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			List<String> supplierHotelIdList = new ArrayList<>();
			List<String> supplierMealInfoIdList = new ArrayList<>();
			populateHotelAndMealIds(searchResult, supplierHotelIdList, supplierMealInfoIdList);
			Map<String, HotelInfo> hotelStaticInfoMap =
					fetchHotelStaticInfoMap(searchQuery, supplierHotelIdList, OperationType.LIST_SEARCH);
			Map<String, String> hotelStaticMealInfoMap = fetchMealInfoData(searchQuery, supplierMealInfoIdList);
			for (Iterator<HotelInfo> hotelIterator = searchResult.getHotelInfos().iterator(); hotelIterator
					.hasNext();) {
				HotelInfo hInfo = hotelIterator.next();
				HotelMissingInfo missingInfo = new HotelMissingInfo(OperationType.LIST_SEARCH);
				populateMissingHotelInfo(hotelStaticInfoMap, hInfo, missingInfo, searchQuery);
				if (!isValidHotelInfo(hInfo, searchQuery)) {
					hotelIterator.remove();
				} else {
					try {
						processOptions(hInfo, searchQuery);
						sortOptions(hInfo);
						mapHotelMealBasis(hotelStaticMealInfoMap, hInfo, searchQuery, missingInfo);
						updateManagementFees(hInfo);
						applyPropertyType(hInfo, searchQuery);
						applyPromotionIfApplicable(hInfo, searchQuery);
					} finally {
						HotelMissingInfoLoggingUtil.addToLogList(missingInfo, hInfo, searchQuery,
								hInfo.getMiscInfo().getSupplierStaticHotelId());
					}
				}
			}
			LogUtils.log(LogTypes.HOTEL_SINGLE_SUPPLIER_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.trips(searchResult == null ? 0 : searchResult.getNoOfHotelOptions())
							.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
					LogTypes.HOTEL_SINGLE_SUPPLIER_PROCESS_START);
		} finally {
			HotelMissingInfoLoggingUtil.clearLog(LogTypes.MISSING_INFO);
		}
	}

	public void aggregateHotels(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		log.info("Inside aggregate hotels for search id {} and supplier {}", searchQuery.getSearchId(), supplierIds);
		LogUtils.log(LogTypes.HOTEL_AGGREGATE_HOTEL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		List<HotelInfo> mappedHotels = new ArrayList<>();
		List<HotelInfo> unmappedHotels = new ArrayList<>();
		for (HotelInfo hInfo : searchResult.getHotelInfos()) {
			if (hInfo.getGiataId() != null) {
				mappedHotels.add(hInfo);
			} else {
				unmappedHotels.add(hInfo);
			}
		}
		log.debug("Number of Unmapped Hotels are {} , Mapped Hotels are {}", unmappedHotels.size(),
				mappedHotels.size());
		mappedHotels = aggregateByUniqueId(mappedHotels, searchQuery);
		mappedHotels.addAll(unmappedHotels);
		mappedHotels = aggregateByHotelNameAndRating(mappedHotels, searchQuery);
		searchResult.setHotelInfos(mappedHotels);
		LogUtils.log(LogTypes.HOTEL_AGGREGATE_HOTEL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(searchResult.getNoOfHotelOptions()).supplierId(supplierIds.toString())
						.build(),
				LogTypes.HOTEL_AGGREGATE_HOTEL_PROCESS_START);
	}


	public List<HotelInfo> aggregateByUniqueId(List<HotelInfo> hotelList, HotelSearchQuery searchQuery) {

		LogUtils.log(LogTypes.HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		Map<String, HotelInfo> hotelInfoMap = new HashMap<>();
		for (HotelInfo hInfo : hotelList) {
			String key = hInfo.getGiataId();
			if (hotelInfoMap.isEmpty() || !hotelInfoMap.containsKey(key)) {
				hotelInfoMap.put(key, hInfo);
			} else {
				hotelInfoMap.get(key).getOptions().addAll(hInfo.getOptions());
				mergeOptions(hotelInfoMap.get(key), searchQuery);
			}
		}
		hotelList = new ArrayList<>(hotelInfoMap.values());
		LogUtils.log(LogTypes.HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(HotelUtils.getHotelSupplierIds(searchQuery).toString()).build(),
				LogTypes.HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_START);
		return hotelList;
	}

	public List<HotelInfo> aggregateByHotelNameAndRating(List<HotelInfo> hotelList, HotelSearchQuery searchQuery) {

		log.info("Inside aggregate by constraint for search id {} and supplier {}", searchQuery.getSearchId(),
				HotelUtils.getHotelSupplierIds(searchQuery));
		LogUtils.log(LogTypes.HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		Map<String, HotelInfo> hotelInfoMap = new HashMap<>();
		for (HotelInfo hInfo : hotelList) {
			Address address = hInfo.getAddress();
			String key =
					HotelUtils.getKey(HotelKeyInfo.builder().hotelName(hInfo.getName()).hotelRating(hInfo.getRating())
							.cityName(address.getCity().getName()).countryName(address.getCountry().getName()).build());
			if (hotelInfoMap.isEmpty() || !hotelInfoMap.containsKey(key)) {
				hotelInfoMap.put(key, hInfo);
			} else {
				int noOfOptions = hotelInfoMap.get(key).getOptions().size();
				int mergedHotelOptions = hInfo.getOptions().size();
				hotelInfoMap.get(key).getOptions().addAll(hInfo.getOptions());
				mergeOptions(hotelInfoMap.get(key), searchQuery);
				// log.debug("Count of removed option of merged hotel is {}, no of options was {}",
				// hInfo.getOptions().size() - noOfOptions, mergedHotelOptions);
			}
		}
		hotelList = new ArrayList<>(hotelInfoMap.values());
		LogUtils.log(LogTypes.HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(hotelList.size()).supplierId(HotelUtils.getHotelSupplierIds(searchQuery).toString())
						.build(),
				LogTypes.HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_START);
		return hotelList;
	}

	public void updateStaticData(HotelInfo hotelInfo, HotelSearchQuery searchQuery, OperationType operationType) {

		HotelMissingInfo missingInfo = new HotelMissingInfo(operationType);
		String supplierHotelId = hotelInfo.getMiscInfo().getSupplierStaticHotelId();
		try {
			List<String> supplierHotelIdList = Arrays.asList(supplierHotelId);
			Map<String, HotelInfo> hotelStaticInfoMap =
					fetchHotelStaticInfoMap(searchQuery, supplierHotelIdList, operationType);
			if (MapUtils.isNotEmpty(hotelStaticInfoMap)) {
				populateMissingHotelBasicInfo(hotelStaticInfoMap.get(supplierHotelId), hotelInfo);
				populateMissingHotelDetailInfo(hotelStaticInfoMap.get(supplierHotelId), hotelInfo);
				logMissingHotelDetails(hotelInfo, missingInfo);
			}
		} finally {
			HotelMissingInfoLoggingUtil.addToLogList(missingInfo, hotelInfo, searchQuery, supplierHotelId);
		}
	}

	public HotelInfo mergeSameHotelOptions(List<HotelInfo> hInfoList) {

		HotelInfo hInfo = hInfoList.get(0);
		for (int i = 1; i < hInfoList.size(); i++) {
			hInfo.getOptions().addAll(hInfoList.get(i).getOptions());
		}
		return hInfo;

	}

	public void mergeSortedOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		LogUtils.log(LogTypes.HOTEL_MERGE_SORTED_OPTIONS_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		Map<String, Option> optionMap = new HashMap<>();
		List<Option> updatedOptionList = new ArrayList<>();
		for (Option option : hInfo.getOptions()) {
			String key = "";
			for (RoomInfo room : option.getRoomInfos()) {
				key = StringUtils.join(key, HotelUtils.getKey(room.getRoomCategory()),
						HotelUtils.getKey(room.getMealBasis()));
			}
			if (optionMap.isEmpty() || !optionMap.containsKey(key)) {
				optionMap.put(key, option);
				updatedOptionList.add(option);
			}
		}
		hInfo.setOptions(updatedOptionList);
		LogUtils.log(LogTypes.HOTEL_MERGE_SORTED_OPTIONS_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(updatedOptionList.size()).supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_MERGE_SORTED_OPTIONS_PROCESS_START);
	}

	public HotelSearchResult combinePreviousResult(HotelSearchResult hotelSearchResult,
			HotelSearchQuery searchQuery) {
		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		log.info("Inside combine previous result for search id {} and supplier {}", searchQuery.getSearchId(),
				supplierIds);

		if (hotelSearchResult != null) {
			LogUtils.log(LogTypes.HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			HotelSearchResult oldHotelResults = cacheHandler.getSearchResult(searchQuery.getSearchId());
			if (oldHotelResults != null) {
				hotelSearchResult.getHotelInfos().addAll(oldHotelResults.getHotelInfos());
			}
			LogUtils.log(LogTypes.HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.trips(hotelSearchResult.getNoOfHotelOptions())
							.supplierId(supplierIds.toString()).build(),
					LogTypes.HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_START);
			aggregateHotels(hotelSearchResult, searchQuery);
			sortSearchResult(hotelSearchResult, searchQuery);
			applyCommercial(hotelSearchResult, searchQuery);
			processAndStore(hotelSearchResult, searchQuery);
			storeSearchResult(hotelSearchResult, searchQuery);
		}
		return hotelSearchResult;
	}

	private void applyCommercial(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		log.info("Inside apply commercial for search id {} and supplier {}", searchQuery.getSearchId(),
				supplierIds);
		LogUtils.log(LogTypes.HOTEL_APPLY_COMMERCIAL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		User user = SystemContextHolder.getContextData().getUser();
		commericialCommunicator.processUserCommissionInHotels(searchResult.getHotelInfos(), user, searchQuery);
		postApplyCommercial(searchResult, searchQuery);
		LogUtils.log(LogTypes.HOTEL_APPLY_COMMERCIAL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(supplierIds.toString()).trips(searchResult.getNoOfHotelOptions())
						.build(),
				LogTypes.HOTEL_APPLY_COMMERCIAL_PROCESS_START);
	}

	private void postApplyCommercial(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		log.info("Inside post apply commercial for search id {} and supplier {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId());
		LogUtils.log(LogTypes.HOTEL_POST_APPLY_COMMERCIAL_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		for (Iterator<HotelInfo> hotelIterator = searchResult.getHotelInfos().iterator(); hotelIterator.hasNext();) {
			HotelInfo hInfo = hotelIterator.next();
			if (CollectionUtils.isEmpty(hInfo.getOptions())) {
				hotelIterator.remove();
			} else {
				buildProcessOptions(hInfo, searchQuery);
			}
		}
		LogUtils.log(LogTypes.HOTEL_POST_APPLY_COMMERCIAL_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).trips(searchResult.getNoOfHotelOptions())
						.build(),
				LogTypes.HOTEL_POST_APPLY_COMMERCIAL_PROCESS_START);
	}

	private void processAndStore(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
			log.info("Inside process and store for search id {} and supplier {}", searchQuery.getSearchId(),
					supplierIds);
			LogUtils.log(LogTypes.HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

			for (HotelInfo hotel : searchResult.getHotelInfos()) {

				CacheMetaInfo metaInfo = CacheMetaInfo.builder().expiration(CacheType.HOTELSEARCHCACHING.getTtl())
						.namespace(CacheNameSpace.HOTEL.name()).set(CacheSetName.HOTEL_API_CACHE_SET.getName())
						.key(hotel.getId()).binValues(ImmutableMap.of(BinName.HOTELDETAILS.getName(), hotel))
						.compress(true).build();
				cacheService.store(metaInfo);
			}
			LogUtils.log(LogTypes.HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_END,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
							.supplierId(supplierIds.toString())
							.trips(searchResult.getNoOfHotelOptions()).build(),
					LogTypes.HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_START);
		});
	}

	private void storeSearchResult(HotelSearchResult hotelSearchResult, HotelSearchQuery searchQuery) {
		List<String> supplierIds = HotelUtils.getHotelSupplierIds(searchQuery);
		log.info("Inside store search result for search id {} and supplier {}", searchQuery.getSearchId(),
				supplierIds);
		LogUtils.log(LogTypes.HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		cacheHandler.storeSearchResult(hotelSearchResult, searchQuery);

		LogUtils.log(LogTypes.HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(supplierIds.toString())
						.trips(hotelSearchResult.getNoOfHotelOptions()).build(),
				LogTypes.HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_START);
	}

	public void mergeOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		LogUtils.log(LogTypes.HOTEL_MERGE_OPTIONS_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		sortOptions(hInfo);
		mergeSortedOptions(hInfo, searchQuery);
		buildProcessOptions(hInfo, searchQuery);

		LogUtils.log(LogTypes.HOTEL_MERGE_OPTIONS_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_MERGE_OPTIONS_PROCESS_START);
	}

	public void applyPropertyType(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		LogUtils.log(LogTypes.HOTEL_APPLY_PROPERTY_TYPE_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		List<HotelConfiguratorInfo> hotelpropertyrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.PROPERTY);
		if (!CollectionUtils.isEmpty(hotelpropertyrules)) {
			HotelPropertyCategoryOutput configuratorOutput =
					HotelConfiguratorHelper.getRuleOutPut(null, hotelpropertyrules);
			for (Map.Entry<PropertyType, PropertyCriteria> entry : configuratorOutput.getPropertyCategory()
					.entrySet()) {
				PropertyCriteria propertyCriteria = entry.getValue();
				if (propertyCriteria.isPropertyCriteriaMatches(hInfo)) {
					hInfo.setPropertyType(entry.getKey().name());
					break;
				}
			}
			if (StringUtils.isEmpty(hInfo.getPropertyType())) {
				hInfo.setPropertyType(PropertyType.HOTEL.name());
			}
		}
		LogUtils.log(LogTypes.HOTEL_APPLY_PROPERTY_TYPE_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_APPLY_PROPERTY_TYPE_PROCESS_START);
	}

	private void mapHotelMealBasis(Map<String, String> hotelStaticMealInfoMap, HotelInfo hInfo,
			HotelSearchQuery hotelSearchQuery, HotelMissingInfo missingInfo) {

		if (MapUtils.isNotEmpty(hotelStaticMealInfoMap)
				&& BooleanUtils.isNotTrue(hInfo.getMiscInfo().getIsMealAlreadyMapped())) {
			hInfo.getOptions().stream().forEach(option -> {
				option.getRoomInfos().stream().forEach(roomInfo -> {
					String mappedMeal = StringUtils.isBlank(roomInfo.getMealBasis()) ? null
							: hotelStaticMealInfoMap.get(roomInfo.getMealBasis().toUpperCase());
					if (mappedMeal == null) {
						missingInfo.getMealBasis().add(roomInfo.getMealBasis());
					}
					roomInfo.setMealBasis(mappedMeal == null ? HotelConstants.ROOM_ONLY : mappedMeal);
				});
			});
		}
	}

	public Map<String, String> fetchMealInfoData(HotelSearchQuery searchQuery, List<String> supplierMealInfoIdList) {
		LogUtils.log(LogTypes.HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		Map<String, String> hotelStaticMealInfoMap =
				cacheHandler.fetchMealInfoFromAerospike(searchQuery, supplierMealInfoIdList);

		LogUtils.log(LogTypes.HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(supplierMealInfoIdList.size()).supplierId(searchQuery.getMiscInfo().getSupplierId())
						.build(),
				LogTypes.HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_START);
		return hotelStaticMealInfoMap;
	}

	public Map<String, HotelInfo> fetchHotelStaticInfoMap(HotelSearchQuery searchQuery,
			List<String> supplierHotelIdList, OperationType operationType) {
		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		log.info("Number of static info is getting fetched for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIdList.size());
		if (CollectionUtils.isEmpty(supplierHotelIdList)) {
			return new HashMap<>();
		}

		Map<String, HotelInfo> hotelStaticDataMap =
				fetchHotelInfoFromAerospike(HotelUtils.getSourceNameFromSourceId(searchQuery.getSourceId()),
						supplierHotelIdList, searchQuery, operationType);

		log.info("Number of static is fetched for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), supplierHotelIdList.size());

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(supplierHotelIdList.size()).supplierId(searchQuery.getMiscInfo().getSupplierId())
						.build(),
				LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_START);
		return hotelStaticDataMap;
	}

	private void populateHotelAndMealIds(HotelSearchResult searchResult, List<String> supplierHotelIdList,
			List<String> supplierMealInfoIdList) {

		searchResult.getHotelInfos().stream().forEach(hotelInfo -> {
			if (StringUtils.isNotBlank(hotelInfo.getMiscInfo().getSupplierStaticHotelId()))
				supplierHotelIdList.add(hotelInfo.getMiscInfo().getSupplierStaticHotelId());
			hotelInfo.getOptions().forEach(option -> {
				option.getRoomInfos().stream().forEach(roomInfo -> {
					if (StringUtils.isNotBlank(roomInfo.getMealBasis())) {
						supplierMealInfoIdList.add(roomInfo.getMealBasis());
					}
				});
			});
		});
	}

	public void populateMissingHotelInfo(Map<String, HotelInfo> hotelStaticDataMap, HotelInfo hInfo,
			HotelMissingInfo missingInfo, HotelSearchQuery searchQuery) {
		String supplierHotelMapper = hInfo.getMiscInfo().getSupplierStaticHotelId();

		if (hInfo.getMiscInfo().getSearchKeyExpiryTime() == null) {
			hInfo.getMiscInfo().setSearchKeyExpiryTime(LocalDateTime.now().plusMinutes(20));
		}

		if (StringUtils.isBlank(supplierHotelMapper)) {
			hInfo.setId(HotelUtils.generateKey(hInfo, searchQuery));
			return;
		}
		HotelInfo staticHotel = hotelStaticDataMap.get(supplierHotelMapper);
		if (!ObjectUtils.isEmpty(staticHotel)) {

			populateMissingHotelBasicInfo(staticHotel, hInfo);
		}

		if (hInfo.getName() == null)
			return;

		hInfo.setId(HotelUtils.generateKey(hInfo, searchQuery));
		logMissingHotelDetails(hInfo, missingInfo);
	}

	public void populateMissingHotelBasicInfo(HotelInfo staticHotel, HotelInfo hInfo) {

		if (staticHotel.isAddressPresent()) {
			populateMissingAddressDetails(hInfo, staticHotel);
		}

		if (CollectionUtils.isNotEmpty(staticHotel.getImages())) {
			hInfo.setImages(staticHotel.getImages());
		}

		if (ObjectUtils.isEmpty(hInfo.getRating())) {
			hInfo.setRating(staticHotel.getRating());
		}

		if (StringUtils.isBlank(hInfo.getName())) {
			hInfo.setName(staticHotel.getName());
		}
		hInfo.setName(StringUtils.isNotBlank(hInfo.getName()) ? StringEscapeUtils.unescapeHtml4(hInfo.getName())
				: hInfo.getName());

		if (!ObjectUtils.isEmpty(staticHotel.getGeolocation())) {
			hInfo.setGeolocation(staticHotel.getGeolocation());
		}

		if (!ObjectUtils.isEmpty(staticHotel.getGiataId())) {
			hInfo.setGiataId(staticHotel.getGiataId());
		}

		if (!ObjectUtils.isEmpty(staticHotel.getUserReviewSupplierId())) {
			hInfo.setUserReviewSupplierId(staticHotel.getUserReviewSupplierId());
		}
		hInfo.setLocalHotelCode(staticHotel.getId());
}

	public void populateMissingHotelDetailInfo(HotelInfo staticHotel, HotelInfo hInfo) {

		if (StringUtils.isNotBlank(staticHotel.getDescription())) {
			hInfo.setDescription(staticHotel.getDescription());
		}

		if (!ObjectUtils.isEmpty(staticHotel.getInstructions())) {
			hInfo.setInstructions(staticHotel.getInstructions());
		}

		if (CollectionUtils.isNotEmpty(staticHotel.getFacilities())) {
			hInfo.setFacilities(staticHotel.getFacilities());
		}

		if (StringUtils.isNotBlank(staticHotel.getWebsite())) {
			hInfo.setWebsite(staticHotel.getWebsite());
		}

		if (!ObjectUtils.isEmpty(staticHotel.getContact())) {
			Contact contact = new Contact();
			contact.setPhone(staticHotel.getContact().getPhone());
			contact.setEmail(staticHotel.getContact().getEmail());
			hInfo.setContact(contact);
		}
	}

	public void populateMissingAddressDetails(HotelInfo hInfo, HotelInfo staticHotel) {

		if (hInfo.isAddressPresent()) {
			Address adr = hInfo.getAddress();

			if (!adr.isAddressLinePresent()) {
				adr.setAddressLine1(staticHotel.getAddress().getAddressLine1());
			}
			if (!adr.isCityPresent()) {
				adr.setCity(staticHotel.getAddress().getCity());
			}
			if (!adr.isCountryPresent()) {
				adr.setCountry(staticHotel.getAddress().getCountry());
			}
		} else {
			hInfo.setAddress(staticHotel.getAddress());
		}
	}


	public void logMissingHotelDetails(HotelInfo hInfo, HotelMissingInfo missingInfo) {
		if (ObjectUtils.isEmpty(hInfo.getAddress())) {
			missingInfo.setAddress(true);
		}

		if (ObjectUtils.isEmpty(hInfo.getImages())) {
			missingInfo.setImages(true);
		}

		if (ObjectUtils.isEmpty(hInfo.getGeolocation())) {
			missingInfo.setGeolocation(true);
		}
		if (ObjectUtils.isEmpty(hInfo.getRating())) {
			missingInfo.setRating(true);
		}

		if (missingInfo.getOperationType().equals(OperationType.DETAIL_SEARCH)) {
			if (ObjectUtils.isEmpty(hInfo.getFacilities())) {
				missingInfo.setHotelFacilities(true);
			}

			missingInfo.setRoomFacilities(true);
			for (Option option : hInfo.getOptions()) {
				if (!CollectionUtils.isEmpty(option.getRoomInfos().get(0).getRoomAmenities())) {
					missingInfo.setRoomFacilities(false);
					break;
				}
			}
		}
	}

	public void sortSearchResult(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		List<HotelInfo> hotelInfoList =
				searchResult.getHotelInfos().stream().filter(hInfo -> hInfo.getOptions().size() > 0)
						.sorted(Comparator.comparing(hInfo -> hInfo.getOptions().get(0).getTotalPrice()))
						.collect(Collectors.toCollection(ArrayList::new));
		searchResult.setHotelInfos(hotelInfoList);
	}

	public void sortOptions(HotelInfo hInfo) {
		List<Option> options = hInfo.getOptions().stream().sorted(Comparator.comparing(op -> op.getTotalPrice()))
				.collect(Collectors.toCollection(ArrayList::new));
		hInfo.setOptions(options);
	}

	public boolean isValidHotelInfo(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		/**
		 * Checking Star Rating as per search criteria, In case Star rating is Blank then system won't filter those
		 * results
		 */

		if (hInfo.getName() == null)
			return false;

		if ((BooleanUtils.isTrue(searchQuery.getSearchPreferences().getFetchSpecialCategory())
				&& hInfo.getRating() == null))
			return true;

		if (ObjectUtils.isEmpty(hInfo.getOptions())
				|| !searchQuery.getSearchPreferences().getRatings().contains(hInfo.getRating()))
			return false;


		return true;
	}

	public void updateManagementFees(HotelInfo hInfo) {
		for (Option option : hInfo.getOptions()) {
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				BaseHotelUtils.updateManagementFee(roomInfo);
			}
		}
	}


	public void buildProcessOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		LogUtils.log(LogTypes.HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		List<ProcessedOption> processOptions = new ArrayList<>();
		Map<String, Double> processedOptionMap = new HashMap<>();
		Map<String, Double> processedOptionDiscountMap = new HashMap<>();
		for (Option option : hInfo.getOptions()) {
			double totalPrice = 0.0;
			double discountedPrice = 0.0;
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				totalPrice += roomInfo.getTotalPrice();
				Map<HotelFareComponent, Double> addlFareComponents =
						roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF);
				discountedPrice += addlFareComponents.getOrDefault(HotelFareComponent.DS, 0.0);
			}

			String mealBasis = option.getRoomInfos().get(0).getMealBasis();
			if (!processedOptionMap.isEmpty() && processedOptionMap.containsKey(mealBasis)) {
				if (totalPrice < processedOptionMap.get(mealBasis)) {
					processedOptionMap.put(mealBasis, totalPrice);
					processedOptionDiscountMap.put(mealBasis, discountedPrice);
				}
			} else {
				processedOptionMap.put(mealBasis, totalPrice);
				processedOptionDiscountMap.put(mealBasis, discountedPrice);
			}
			if (option.getMiscInfo() != null)
				hInfo.getSuppliers().add(option.getMiscInfo().getSupplierId());
			option.setTotalPrice(totalPrice);
		}
		for (String mealBasis : processedOptionMap.keySet()) {
			ProcessedOption po = ProcessedOption.builder().facilities(Arrays.asList(mealBasis)).build();
			if (processedOptionDiscountMap.get(mealBasis) > 0) {
				po.setTotalPrice(processedOptionDiscountMap.get(mealBasis) + processedOptionMap.get(mealBasis));
				po.setDiscountedPrice(processedOptionMap.get(mealBasis));
			} else {
				po.setTotalPrice(processedOptionMap.get(mealBasis));
			}
			processOptions.add(po);
		}
		processOptions = processOptions.stream().sorted(Comparator.comparing(po -> po.getTotalPrice()))
				.collect(Collectors.toCollection(ArrayList::new));
		hInfo.setProcessedOptions(processOptions);
		LogUtils.log(LogTypes.HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(processOptions.size()).supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_START);
	}

	public void processOptions(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		for (Iterator<Option> optionIterator = hInfo.getOptions().iterator(); optionIterator.hasNext();) {
			Option option = optionIterator.next();
			if (!arrangeRoomAccordingToSearchCriteria(option, searchQuery)) {
				log.debug("Removing optionId {} from hotel {} due to mismatch of search criteria", option.getId(),
						hInfo.getName());
				optionIterator.remove();
			}
		}
	}

	private boolean arrangeRoomAccordingToSearchCriteria(Option option, HotelSearchQuery searchQuery) {
		List<RoomInfo> roomInfoList = option.getRoomInfos();
		List<RoomSearchInfo> sRoomInfo = searchQuery.getRoomInfo();
		int index = 0;
		for (RoomInfo room : roomInfoList) {
			RoomSearchInfo sRoom = sRoomInfo.get(index);
			int i = search(roomInfoList, sRoom, index);
			if (i == -1) {
				return false;
			}
			Collections.swap(roomInfoList, i, index);
			index++;
		}
		return true;
	}

	private int search(List<RoomInfo> roomInfoList, RoomSearchInfo sRoom, int startIndex) {
		for (int i = startIndex; i < roomInfoList.size(); i++) {
			RoomInfo roomInfo = roomInfoList.get(i);
			if (roomInfo.getNumberOfAdults() == sRoom.getNumberOfAdults()
					&& (sRoom.getNumberOfChild() == null || ObjectUtils.isEmpty(roomInfo.getNumberOfChild())
							|| roomInfo.getNumberOfChild() == sRoom.getNumberOfChild())) {
				return i;
			}
		}
		return -1;
	}

	public void applyPromotionIfApplicable(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		LogUtils.log(LogTypes.HOTEL_APPLY_PROMOTION_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery);
		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.PROMOTION);

		if (!CollectionUtils.isEmpty(hotelconfigrules)) {
			hotelFact = hotelFact.generateFactFromHotelInfo(hInfo);
			HotelPromotionOutput promotionDetails = HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);
			if (promotionDetails != null) {
				HotelPromotion promotion =
						HotelPromotion.builder().isFlexibleTimingAllowed(promotionDetails.getFlexibleTiming())
								.isLowestPriceAllowed(promotionDetails.getLowestPrice()).msg(promotionDetails.getMsg())
								.preferredHotel(promotionDetails.getPreferredHotel()).build();
				hInfo.getTags().add(HotelTag.PREFERRED);
				hInfo.setPromotionInfo(promotion);
			}
		}
		LogUtils.log(LogTypes.HOTEL_APPLY_PROMOTION_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_APPLY_PROMOTION_PROCESS_END);
	}

	public void updateTotalFareComponents(HotelInfo hotelInfo) {

		for (Option option : hotelInfo.getOptions()) {
			Double totalOptionPrice = 0.0;
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
				totalOptionPrice += roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.TF, 0.0);
			}
			option.setTotalPrice(totalOptionPrice);
		}
	}

	public void resetMarkupComponent(HotelInfo hInfo, HotelSearchQuery searchQuery, User user) {
		for (Option option : hInfo.getOptions()) {
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				for (PriceInfo pInfo : roomInfo.getPerNightPriceInfos()) {
					if (MapUtils.isNotEmpty(pInfo.getAddlFareComponents().get(HotelFareComponent.TAF)))
						pInfo.getAddlFareComponents().get(HotelFareComponent.TAF).remove(HotelFareComponent.MU);
				}
				if (MapUtils.isNotEmpty(roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)))
					roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF).remove(HotelFareComponent.MU);
			}
		}
		processHotelFareComponents(hInfo, searchQuery, user);
	}

	public void processHotelFareComponents(HotelInfo hotelInfo, HotelSearchQuery searchQuery, User user) {
		try {
			if (!(user != null && user.getAdditionalInfo() != null
					&& BooleanUtils.isTrue(user.getAdditionalInfo().getDisableMarkup()))) {
				userFeeManager.processUserFee(user, hotelInfo, UserFeeType.MARKUP, HotelFareComponent.MU);
			}
			updateTotalFareComponents(hotelInfo);
		} catch (Exception e) {
			log.error("Exception occured while processing fare components for hotel {} , query {} ",
					hotelInfo.getName(), searchQuery, e);
		}
	}

	private Map<String, HotelInfo> fetchHotelInfoFromAerospike(String supplierName, List<String> supplierHotelIds,
			HotelSearchQuery searchQuery, OperationType operationType) {

		String supplierMappingBinName = BaseHotelUtils.getSupplierBinName(supplierName);
		String supplierMappingSetName = BaseHotelUtils.getSupplierSetName(supplierName);

		Map<String, Map<String, String>> supplierhotelIdMap = fetchSupplierIdToHotelIdMapping(supplierMappingSetName,
				supplierMappingBinName, supplierHotelIds, searchQuery);

		Map<String, String> hotelIdAsKeySupplierIdAsValueMap = getHotelIdAsKeySupplierIdAsValue(supplierhotelIdMap);
		return processAndGenerateHotelInfo(hotelIdAsKeySupplierIdAsValueMap, searchQuery, operationType);
	}

	private Map<String, HotelInfo> processAndGenerateHotelInfo(Map<String, String> hotelIdAsKeySupplierIdAsValueMap,
			HotelSearchQuery searchQuery,
			OperationType operationType) {
		log.info("Processing static info for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), hotelIdAsKeySupplierIdAsValueMap.size());
		Map<String, HotelInfo> hotelInfoList = new HashMap<>();
		if (operationType.equals(OperationType.LIST_SEARCH)) {
			Map<String, Map<String, String>> hotelIdWithHotelInfoMap =
					fetchHotelIdToHotelStaticData(hotelIdAsKeySupplierIdAsValueMap, searchQuery,
							new String[] {BinName.HOTEL_INFO.name()});
			log.info(
					"Processing static info after fetching hotel id to hotel static data for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
					hotelIdWithHotelInfoMap.size());

			Gson gson = GsonUtils.getGson();
			for (Map.Entry<String, String> hotelIdAsKeySupplierIdAsValueEntry : hotelIdAsKeySupplierIdAsValueMap
					.entrySet()) {
				try {
					Map<String, String> hotelInfo =
							hotelIdWithHotelInfoMap.get(hotelIdAsKeySupplierIdAsValueEntry.getKey());
					if (!ObjectUtils.isEmpty(hotelInfo)) {
						hotelInfoList.put(hotelIdAsKeySupplierIdAsValueEntry.getValue(),
								gson.fromJson(hotelInfo.get(BinName.HOTEL_INFO.name()), HotelInfo.class));
					}
				} catch (Exception e) {
					log.debug("Unable to find mapping for hotelId {} and supplierId {}",
							hotelIdAsKeySupplierIdAsValueEntry.getKey(), hotelIdAsKeySupplierIdAsValueEntry.getValue(), e);
				}
			}
			log.info(
					"Processed static info after fetching hotel id to hotel static data for search id {} and supplier {} are {}",
					searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
					hotelIdWithHotelInfoMap.size());
		} else {
			Map<String, Map<String, String>> hotelIdWithHotelInfoMap =
					fetchHotelIdToHotelStaticData(hotelIdAsKeySupplierIdAsValueMap, searchQuery,
							new String[] {BinName.HOTEL_INFO.name(), BinName.HOTEL_DETAIL.name()});
			Gson gson = GsonUtils.getGson();
			for (Map.Entry<String, String> hotelIdAsKeySupplierIdAsValueEntry : hotelIdAsKeySupplierIdAsValueMap
					.entrySet()) {
				try {
					Map<String, String> hotelInfo =
							hotelIdWithHotelInfoMap.get(hotelIdAsKeySupplierIdAsValueEntry.getKey());
					if (!ObjectUtils.isEmpty(hotelInfo)) {
						HotelInfo basicInfo =
								gson.fromJson(hotelInfo.get(BinName.HOTEL_INFO.name()), HotelInfo.class);
						if (StringUtils.isNotBlank(hotelInfo.get(BinName.HOTEL_DETAIL.name()))) {
							HotelInfo detailInfo =
									gson
									.fromJson(hotelInfo.get(BinName.HOTEL_DETAIL.name()), HotelInfo.class);
							hotelInfoList.put(hotelIdAsKeySupplierIdAsValueEntry.getValue(),
									new GsonMapper<>(detailInfo, basicInfo, HotelInfo.class).convert());
						} else {
							hotelInfoList.put(hotelIdAsKeySupplierIdAsValueEntry.getValue(), basicInfo);
						}
					}
				} catch (Exception e) {
					log.debug("Unable to find mapping for hotelId {} and supplierId {}",
							hotelIdAsKeySupplierIdAsValueEntry.getKey(), hotelIdAsKeySupplierIdAsValueEntry.getValue(),
							e);
				}
			}
		}
		log.info("Processed static info for search id {} and supplier {} are {}", searchQuery.getSearchId(),
				searchQuery.getMiscInfo().getSupplierId(), hotelIdAsKeySupplierIdAsValueMap.size());
		return hotelInfoList;
	}

	private Map<String, Map<String, String>> fetchSupplierIdToHotelIdMapping(String supplierMappingSetName,
			String supplierMappingBinName, List<String> supplierHotelIds, HotelSearchQuery searchQuery) {
		log.info("Fetching supplier id to hotel id mapping for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIds.size());

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(supplierMappingSetName).keys(supplierHotelIds.toArray(new String[0]))
				.bins(new String[] {supplierMappingBinName}).build();
		Map<String, Map<String, String>> supplierhotelIdMap =
				cachingCommunicator.get(supplierMappingMetaInfo, String.class);

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(supplierHotelIds.size()).supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_START);

		log.info("Fetched supplier id to hotel id mapping for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(), supplierHotelIds.size());
		return supplierhotelIdMap;
	}

	private Map<String, Map<String, String>> fetchHotelIdToHotelStaticData(
			Map<String, String> hotelIdAsKeySupplierIdAsValueMap, HotelSearchQuery searchQuery, String[] bins) {

		log.info("Fetching hotel id to hotel static data for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
				hotelIdAsKeySupplierIdAsValueMap.size());

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		CacheMetaInfo hotelMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.HOTEL_MAPPING.getName()).bins(bins)
				.keys(hotelIdAsKeySupplierIdAsValueMap.keySet().toArray(new String[0])).build();
		Map<String, Map<String, String>> hotelIdWithHotelInfoMap =
				cachingCommunicator.get(hotelMappingMetaInfo, String.class);

		LogUtils.log(LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_END,
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
						.trips(hotelIdAsKeySupplierIdAsValueMap.size())
						.supplierId(searchQuery.getMiscInfo().getSupplierId()).build(),
				LogTypes.HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START);
		log.info("Fetched hotel id to hotel static data for search id {} and supplier {} are {}",
				searchQuery.getSearchId(), searchQuery.getMiscInfo().getSupplierId(),
				hotelIdAsKeySupplierIdAsValueMap.size());
		return hotelIdWithHotelInfoMap;
	}

	private Map<String, String> getHotelIdAsKeySupplierIdAsValue(Map<String, Map<String, String>> supplierIdMap) {
		Map<String, String> supplierIdHotelIdMap = new HashMap<>();
		for (Map.Entry<String, Map<String, String>> supplierIdEntryMap : supplierIdMap.entrySet()) {
			Iterator<Map.Entry<String, String>> supplierIdIterator =
					supplierIdEntryMap.getValue().entrySet().iterator();
			if (supplierIdIterator.hasNext()) {
				Map.Entry<String, String> supplierIdEntry = supplierIdIterator.next();
				supplierIdHotelIdMap.put(supplierIdEntry.getValue(), supplierIdEntryMap.getKey());
			}
		}
		return supplierIdHotelIdMap;
	}
	
	private boolean isPanRequired(Option option) {

		if (option.getIsPanRequired() != null)
			return option.getIsPanRequired();

		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);

		if (configuratorInfo != null) {
			return configuratorInfo.getSuppliersPanCardNotReqd() != null
					&& !configuratorInfo.getSuppliersPanCardNotReqd().contains(option.getMiscInfo().getSupplierId());
		}
		return true;
	}
}
