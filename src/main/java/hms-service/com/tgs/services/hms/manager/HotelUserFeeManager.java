package com.tgs.services.hms.manager;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import com.tgs.services.base.ruleengine.UserHotelFeeRuleCriteria;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.AmountType;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;
import com.tgs.services.ums.datamodel.fee.UserFeeType;

@Service
public class HotelUserFeeManager {

	@Autowired
	UserServiceCommunicator userComm;

	public void processUserFee(User user, HotelInfo hotelInfo, UserFeeType feeType,
			HotelFareComponent targetComponent) {

		List<UserFee> userFeeRequest = userComm.getUserFee(user.getUserId(), Product.HOTEL, feeType);
		if (CollectionUtils.isEmpty(userFeeRequest) && StringUtils.isNotBlank(user.getParentUserId())) {
			userFeeRequest = userComm.getUserFee(user.getParentUserId(), Product.HOTEL, feeType);
		}
		if (CollectionUtils.isNotEmpty(userFeeRequest)) {
			Map<String, List<UserFee>> userFeeMap =
					userFeeRequest.stream().collect(Collectors.groupingBy(userRequest -> getKey(userRequest)));
			UserFee userFee = getUserFee(hotelInfo, userFeeMap);
			if (Objects.nonNull(userFee)) {
				UserFeeAmount feeAmountRequest = userFee.getUserFee();
				UserFeeAmount copyFeeAmountRequest =
						GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(feeAmountRequest), UserFeeAmount.class);
				if (Objects.nonNull(copyFeeAmountRequest)) {
					updateUserFee(hotelInfo, copyFeeAmountRequest);
				}
			}
		}
	}

	public AmountType getUserFeeType(User user, HotelInfo hotelInfo, UserFeeType feeType) {
		List<UserFee> userFeeRequest = userComm.getUserFee(user.getUserId(), Product.HOTEL, feeType);
		if (CollectionUtils.isEmpty(userFeeRequest) && StringUtils.isNotBlank(user.getParentUserId())) {
			userFeeRequest = userComm.getUserFee(user.getParentUserId(), Product.HOTEL, feeType);
		}
		if (CollectionUtils.isNotEmpty(userFeeRequest)) {
			Map<String, List<UserFee>> userFeeMap =
					userFeeRequest.stream().collect(Collectors.groupingBy(userRequest -> getKey(userRequest)));
			UserFee userFee = getUserFee(hotelInfo, userFeeMap);
			if (Objects.nonNull(userFee)) {
				return AmountType.valueOf(userFee.getUserFee().getAmountType());
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public UserFee getUserFee(HotelInfo hotelInfo, Map<String, List<UserFee>> userFeeMap) {

		if (MapUtils.isNotEmpty(userFeeMap)) {
			List<String> hotelSearchTypeKeys = new ArrayList<>();
			String hotel = hotelInfo.getSearchType().getCode();
			hotelSearchTypeKeys.add(hotelInfo.getSearchType().getCode());
			hotelSearchTypeKeys.add(HotelSearchType.ALL.getName());
			List<UserFee> feeRequests = new ArrayList<>();
			for (String key : hotelSearchTypeKeys) {
				feeRequests = (List<UserFee>) MapUtils.getObject(userFeeMap, key, null);
				List<UserFee> tempList = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(feeRequests)) {
					for (UserFee userFee : feeRequests) {
						UserHotelFeeRuleCriteria ruleCriteria =
								(UserHotelFeeRuleCriteria) userFee.getInclusionCriteria();
						if (Objects.nonNull(ruleCriteria)) {
							if (ruleCriteria.getIsDomestic() == null) {
								return userFee;
							} else if ((ruleCriteria.getIsDomestic() && hotel.equals("D"))
									|| (!ruleCriteria.getIsDomestic() && hotel.equals("I"))) {
								return userFee;
							} else if (ruleCriteria.getSearchType().getCode().equals("A")
									|| ruleCriteria.getSearchType().getCode().equals(hotel)) {
								return userFee;
							}
						}
					}
				}
				if (CollectionUtils.isNotEmpty(tempList)) {
					return tempList.get(0);
				}
			}
		}
		return null;
	}

	private String getKey(UserFee userRequest) {
		UserHotelFeeRuleCriteria ruleCriteria = ((UserHotelFeeRuleCriteria) userRequest.getInclusionCriteria());
		if (ruleCriteria != null) {
			String hotelType = ruleCriteria.getIsDomestic() != null
					? HotelUtils.getHotelTypeCode(BooleanUtils.isTrue(ruleCriteria.getIsDomestic()))
					: HotelSearchType.ALL.getCode();
			return StringUtils.join(hotelType);
		}
		return StringUtils.EMPTY;
	}

	public void updateUserFee(HotelInfo hInfo, UserFeeAmount feeAmount) {
		double optionMarkUp = feeAmount.getValue();
		for (Option option : hInfo.getOptions()) {
			if (feeAmount.getAmountType().equals(AmountType.PERCENTAGE.name())) {
				optionMarkUp = getTotalOptionPrice(option);
				optionMarkUp = optionMarkUp * feeAmount.getValue() / 100;
				feeAmount.setValue(optionMarkUp);
				feeAmount.setAmountType(AmountType.FIXED.name());
			}
			feeAmount.setValue(getMarkUpPerRoomNight(option, optionMarkUp));
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
					setUserFeeOnFareDetail(feeAmount, priceInfo);
				}
			}
		}
	}

	protected void setUserFeeOnFareDetail(UserFeeAmount feeAmount, PriceInfo priceInfo) {

		AmountType amountType = AmountType.valueOf((feeAmount.getAmountType()));
		double supplierPrice = priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
				+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.SP, 0.0);
		double userFee = 0d;
		if (amountType.equals(AmountType.FIXED)) {
			// Fixed User Fee
			userFee = feeAmount.getValue();

		} else if (amountType.equals(AmountType.PERCENTAGE)) {
			// Percentage User Fee - Calculate Percentage on Total fare
			userFee = (feeAmount.getValue() * supplierPrice / 100);
		}
		if (userFee >= 0d) {
			priceInfo.getFareComponents().put(HotelFareComponent.MU, userFee);
		}
	}

	private Double getTotalOptionPrice(Option option) {

		Double totalOptionSupplierPrice = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				totalOptionSupplierPrice += priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0);
			}
		}
		return totalOptionSupplierPrice;
	}


	public void updateUserFeeForHotelList(HotelInfo hInfo, Double markUpValue, String feeType) {

		UserFeeAmount userFee = new UserFeeAmount();
		userFee.setAmountType(feeType);
		userFee.setValue(markUpValue);
		updateUserFee(hInfo, userFee);
		BaseHotelUtils.updateTotalFareComponents(hInfo);
	}

	private double getMarkUpPerRoomNight(Option option, double totalMarkUpAmount) {

		double markUpPerRoomNight = 0.0;
		int roomNights = option.getRoomInfos().get(0).getPerNightPriceInfos().size();
		int roomCount = option.getRoomInfos().size();
		markUpPerRoomNight = totalMarkUpAmount / (roomNights * roomCount);
		return markUpPerRoomNight;
	}

}
