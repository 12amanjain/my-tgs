package com.tgs.services.hms.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelUserReviewSearch;
import com.tgs.services.hms.jparepository.HotelUserReviewService;
import com.tgs.services.hms.sources.tripadvisor.TripAdvisorConstants;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelUserReviewManager {

	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	HotelUserReviewSearch search;
	
	@Autowired
	HotelUserReviewService userReviewService;
	
	public List<HotelUserReview> getHotelUserReviewList(List<HotelInfo> hInfoList) throws Exception {
		
		List<Future<HotelUserReview>> userReviewFutureList = new ArrayList<>();
		for(HotelInfo hInfo : hInfoList) {
			Future<HotelUserReview> future = ExecutorUtils.getHotelUserReviewSearchThreadPool()
				.submit(() -> getHotelUserReview(hInfo));
			userReviewFutureList.add(future);
		}
		return getHotelUserReviewListFromFutures(userReviewFutureList);
	}
	
	public HotelUserReview getHotelUserReview(HotelInfo hInfo) {
		
		return search.reviewSearch(Integer.valueOf(TripAdvisorConstants.SOURCE.getValue()), hInfo);
	}
	
	public List<HotelUserReview> getHotelUserReviewListFromFutures(List<Future<HotelUserReview>> userReviewFutureList)
			throws Exception {
		
		List<HotelUserReview> userReviewList = new ArrayList<>();
		for(Future<HotelUserReview> userReviewFuture : userReviewFutureList) {
			try {
				HotelUserReview userReview = null;
				if((userReview = userReviewFuture.get(1,TimeUnit.SECONDS)) != null) {
					userReviewList.add(userReview);
				}
			}catch(TimeoutException e) {
				log.error("Timeout Exception While Fetching Hotel User Review For Supplier TripAdvisor", e);
			}
		}
		return userReviewList;
	}
	
	public void getUserReviewSupplierIdFromHotel(List<HotelInfo> hInfoList) {
		
		for(HotelInfo hInfo : hInfoList) {
			search.userReviewIdSearch(Integer.valueOf(TripAdvisorConstants.SOURCE.getValue()), hInfo);
		}
	}
}
