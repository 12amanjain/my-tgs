package com.tgs.services.hms.manager.mapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelCityMappingQuery;
import com.tgs.services.hms.datamodel.HotelUpdateCityInfoMappingQuery;
import com.tgs.services.hms.datamodel.TopSuggestion;
import com.tgs.services.hms.dbmodel.DbCityInfo;
import com.tgs.services.hms.dbmodel.DbCityInfoMapping;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelCityInfoService;
import com.tgs.services.hms.restmodel.HotelCityInfoMappingResponse;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.restmodel.HotelCityInfoRequest;
import com.tgs.services.hms.restmodel.HotelCityMappingRequest;
import com.tgs.services.hms.restmodel.HotelDeleteMappingRequest;
import com.tgs.services.hms.restmodel.HotelUpdateCityInfoMappingRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelCityInfoMappingManager {

	@Autowired
	HotelCityInfoService infoService;

	@Autowired
	HotelCityInfoSaveManager cityInfoSaveManger;

	@Autowired
	HotelInfoSaveManager hotelInfoManager;

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	public void process(HotelCityInfoRequest hotelCityInfos) throws Exception {
		saveCityInfoMappingList(hotelCityInfos.getCityInfoQuery());
	}

	public void saveCityInfoMappingList(List<HotelCityInfoQuery> hotelCityInfos) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		Runnable saveHotelMealInfoTask = () -> {
			List<Future<?>> futures = new ArrayList<>();
			for (HotelCityInfoQuery hotelCityInfo : hotelCityInfos) {
				Future<?> future = executor.submit(() -> {
					CityInfo cityInfo = processCityInfo(hotelCityInfo);
					Long cityId = null;
					if ((cityId = isCityExists(cityInfo)) == null)
						return;
					CityInfoMapping cityInfoMapping = processCityInfoMapping(hotelCityInfo);
					saveCityMapping(cityInfoMapping, cityId);
				});
				futures.add(future);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				InMemoryInitializer inMemoryInitializer = (InMemoryInitializer) SpringContext.getApplicationContext()
						.getBean("hotelCityStaticDataPersistenceHelper");
				inMemoryInitializer.initialize("hotelCityStaticDataPersistenceHelper");
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting meal mappings for {} ", e);
			}
		};
		saveHotelMealInfoTask.run();
	}

	public Long isCityExists(CityInfo cityInfo) {
		DbCityInfo dbCityInfo =
				infoService.findByCityNameAndCountryName(cityInfo.getCityName(), cityInfo.getCountryName());
		if (dbCityInfo == null) {
			log.debug("Master Db entry not found for city {} , country {}", cityInfo.getCityName(),
					cityInfo.getCountryName());
			return null;
		}
		return dbCityInfo.getId();
	}

	public void saveCityMapping(CityInfoMapping cityInfoMapping, Long cityId) {
		DbCityInfoMapping dbCityInfoMapping = new DbCityInfoMapping().from(cityInfoMapping);
		dbCityInfoMapping.setCityId(cityId);
		try {
			dbCityInfoMapping = infoService.saveSupplierMapping(dbCityInfoMapping);
			log.debug("New City Info Mapping object saved with id {}", dbCityInfoMapping.getId());
		} catch (Exception e) {
			DbCityInfoMapping oldMappingObject = infoService.findByCityIdAndSupplierName(dbCityInfoMapping.getCityId(),
					dbCityInfoMapping.getSupplierName());
			Long id = oldMappingObject.getId();
			oldMappingObject = new GsonMapper<>(dbCityInfoMapping, oldMappingObject, DbCityInfoMapping.class).convert();
			oldMappingObject.setId(id);
			log.info("Updated City Info Mapping Object with cityId {} ,supplierName{} ", oldMappingObject.getCityId(),
					oldMappingObject.getSupplierName());
			infoService.saveSupplierMapping(oldMappingObject);

		}
	}

	public static CityInfoMapping processCityInfoMapping(HotelCityInfoQuery cityInfoQuery) {

		CityInfoMapping cityInfoMapping = CityInfoMapping.builder().supplierName(cityInfoQuery.getSuppliername())
				.supplierCity(cityInfoQuery.getSuppliercityid()).supplierCountry(cityInfoQuery.getSuppliercountryid())
				.build();
		return cityInfoMapping;
	}

	public static CityInfo processCityInfo(HotelCityInfoQuery cityInfoQuery) {

		CityInfo cityInfo = CityInfo.builder().cityName(cityInfoQuery.getCityname())
				.countryName(cityInfoQuery.getCountryname()).build();
		return cityInfo;
	}

	public void saveHotelCityMappingRequest(HotelCityMappingRequest unmappedCityRequest) {

		try {
			
			List <HotelCityMappingQuery> cityMappingQueryList = unmappedCityRequest.getCityMappingQuery();
			for(HotelCityMappingQuery cityMappingQuery: cityMappingQueryList) {
				
				boolean updateStaticHotel = false;
				CityInfoMapping cityInfoMapping = getCityMappingRequest(cityMappingQuery);
				
				DbCityInfoMapping dbCityInfoMapping  = infoService.findByCityIdAndSupplierName(cityInfoMapping.getCityId(),cityMappingQuery.getSupplierName());
				if(dbCityInfoMapping != null && dbCityInfoMapping.getSupplierCityName() != null) {
					updateStaticHotel = !(dbCityInfoMapping.getSupplierCityName().equals(cityInfoMapping.getSupplierCityName()));
				} else {
					updateStaticHotel = true;
				}
				
				saveCityMapping(cityInfoMapping, cityInfoMapping.getCityId());
				saveCityInfoCache(cityInfoMapping, cityInfoMapping.getCityId());
				if(updateStaticHotel) {
					hotelInfoManager.fetchAndSaveHotelInfo(Arrays.asList(cityMappingQuery.getSupplierCityId()),
							cityInfoMapping.getSupplierName());
				}
			}
			
			
		} catch (Exception e) {
			log.error("Unable to save city mapping for request {}", GsonUtils.getGson().toJson(unmappedCityRequest), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	private void saveCityInfoCache(CityInfoMapping cityInfoMapping, Long cityId) {
		Map<String, Object> cityInfoBinMap = new HashMap<>();
		cityInfoBinMap.put(cityInfoMapping.getSupplierName(), GsonUtils.getGson().toJson(cityInfoMapping));
		CacheMetaInfo metaInfoToStore = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(CacheSetName.CITY_MAPPING.getName()).key(String.valueOf(cityId)).expiration(-1)
				.binValues(cityInfoBinMap).build();
		cacheService.store(metaInfoToStore);
	}

	private CityInfoMapping getCityMappingRequest(HotelCityMappingQuery cityMappingQuery) {
		String cityName = "";
		String countryName = "";
		String supplierName = "";
		if (StringUtils.isBlank(cityName = cityMappingQuery.getCityName())) {
			throw new CustomGeneralException(SystemError.INVALID_CITY_NAME);
		}

		if (StringUtils.isBlank(countryName = cityMappingQuery.getCountryName())) {
			throw new CustomGeneralException(SystemError.INVALID_COUNTRY_NAME);
		}

		HotelSourceType supplierNameType =
				HotelSourceType.getHotelSourceTypeFromSourceName(cityMappingQuery.getSupplierName().toUpperCase());
		if (Objects.nonNull(supplierNameType)) {
			supplierName = supplierNameType.name();
		} else {
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_NAME);
		}

		DbCityInfo dbCityInfo = infoService.findByCityNameAndCountryName(cityName, countryName);
		if (ObjectUtils.isEmpty(dbCityInfo))
			throw new CustomGeneralException(SystemError.INVALID_CITY_COUNTRY_COMBINATION);

		CityInfoMapping cityMapping = CityInfoMapping.builder().supplierCity(cityMappingQuery.getSupplierCityId())
				.supplierCountry(cityMappingQuery.getSupplierCountryId()).supplierName(supplierName)
				.cityId(dbCityInfo.getId()).supplierCityName(cityMappingQuery.getSupplierCityName()).build();
		return cityMapping;
	}

	public void updateMapping(HotelUpdateCityInfoMappingRequest cityInfoMappingRequest) {

		for (HotelUpdateCityInfoMappingQuery cityInfoMappingQuery : cityInfoMappingRequest.getCityInfoMappingQuery()) {
			TopSuggestion topSuggestion = cityInfoMappingQuery.getTopSuggestionList().get(0);
			String masterCityName = topSuggestion.getCityName();
			String masterCountryName = topSuggestion.getCountryName();
			try {
				DbCityInfo dbCityInfo = infoService.findByCityNameAndCountryName(masterCityName, masterCountryName);
				Long masterId = dbCityInfo.getId();
				CityInfoMapping cityMapping = CityInfoMapping.builder().supplierCity(cityInfoMappingQuery.getCityId())
						.supplierCountry(cityInfoMappingQuery.getCountryId())
						.supplierCityName(cityInfoMappingQuery.getCityName())
						.supplierName(cityInfoMappingRequest.getSupplierName()).cityId(masterId).build();
				saveCityMapping(cityMapping, cityMapping.getCityId());
			} catch (Exception e) {
				log.debug("Unable to update city mapping for cityname {} and country name {}", masterCityName,
						masterCountryName);
			}
		}
	}

	public List<CityInfoMapping> getHotelCityMappingForSupplier(String supplierName) {

		List<DbCityInfoMapping> dbCityMapping = infoService.findBySupplierName(supplierName);
		return DbCityInfoMapping.toDomainList(dbCityMapping);
	}

	public HotelCityInfoMappingResponse getHotelCityMappingForCityId(String cityId) {
		HotelCityInfoMappingResponse cityMappingResponse = new HotelCityInfoMappingResponse();
		try {
			List<DbCityInfoMapping> dbCityMapping = infoService.findByCityId(Long.parseLong(cityId));
			cityMappingResponse.setCityInfos(DbCityInfoMapping.toDomainList(dbCityMapping));
		} catch (Exception e) {
			throw new CustomGeneralException(SystemError.INVALID_CITY);
		}
		return cityMappingResponse;
	}

	public CityInfo getHotelCityInfoFromSupplierCityId(String supplierCityId, String supplierName) {
		DbCityInfo cityInfo = infoService.findCityBySupplierNameSupplierCityId(supplierCityId, supplierName);
		return cityInfo != null ? cityInfo.toDomain() : null;
	}

	public List<CityInfoMapping> findBySupplierName(String supplierName) {
		List<CityInfoMapping> cityMappingList = new ArrayList<>();
		for (int i = 0; i < 500; i++) {
			Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
			List<CityInfoMapping> cityInfoMappingChunk =
					DbCityInfoMapping.toDomainList(infoService.findBySupplierName(supplierName, page));
			if (CollectionUtils.isEmpty(cityInfoMappingChunk))
				break;
			cityMappingList.addAll(cityInfoMappingChunk);
			log.debug("Fetched hotel city mapping info from database, mapping list size is {}", cityMappingList.size());
		}

		return cityMappingList;

	}
	
	public void deleteHotelCityMapping(HotelDeleteMappingRequest mappedCityRequest) {

		try {
			List <CityInfoMapping> cityMappingQueryList = mappedCityRequest.getCityMappingQuery();
			for(CityInfoMapping cityMappingQuery: cityMappingQueryList) {
				
				if (StringUtils.isBlank(cityMappingQuery.getCityId().toString())) {
					throw new CustomGeneralException(SystemError.INVALID_CITY);
				}

				if (StringUtils.isBlank(cityMappingQuery.getSupplierName())) {
					throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_NAME);
				}
				
				DbCityInfoMapping oldMappingObject  = infoService.findByCityIdAndSupplierName(cityMappingQuery.getCityId(),cityMappingQuery.getSupplierName());
				
				if(oldMappingObject != null) {
					oldMappingObject.setSupplierCityName("");
					
					CityInfoMapping cityMapping = CityInfoMapping.builder().supplierCity(oldMappingObject.getSupplierCity())
							.supplierCountry(oldMappingObject.getSupplierCountry()).supplierName(oldMappingObject.getSupplierName())
							.cityId(cityMappingQuery.getCityId()).supplierCityName(oldMappingObject.getSupplierCityName()).build();
					
					infoService.saveSupplierMapping(oldMappingObject);
					saveCityInfoCache(cityMapping, cityMappingQuery.getCityId());
				}
			}
			
		} catch (Exception e) {
			log.error("Unable to delete city mapping for request {}", GsonUtils.getGson().toJson(mappedCityRequest), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}
	
}
