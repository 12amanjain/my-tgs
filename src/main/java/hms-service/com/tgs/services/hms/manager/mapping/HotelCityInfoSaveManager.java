package com.tgs.services.hms.manager.mapping;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.es.datamodel.ESCityInfo;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.IataCodeCityMappingQuery;
import com.tgs.services.hms.dbmodel.DbCityInfo;
import com.tgs.services.hms.jparepository.HotelCityInfoService;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.restmodel.HotelCityInfoRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelCityInfoSaveManager {

	@Autowired
	HotelCityInfoService infoService;
	
	@Autowired
	ElasticSearchCommunicator esCommunicator;

	public void process(HotelCityInfoRequest hotelCityInfo) throws Exception {
		saveCityInfoList(hotelCityInfo.getCityInfoQuery());
	}

	public void saveIataInfo(List<IataCodeCityMappingQuery> iataCodeCityMappings) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		for (IataCodeCityMappingQuery iataCodeCityMapping : iataCodeCityMappings) {
			executor.submit(() -> {
				try {
					DbCityInfo dbCityInfo = infoService.findByCityNameAndCountryName(iataCodeCityMapping.getCityName(),
							iataCodeCityMapping.getCountryName());
					if (!ObjectUtils.isEmpty(dbCityInfo)) {
						dbCityInfo.setIataCode(iataCodeCityMapping.getIataCode());
						dbCityInfo = infoService.saveCityInfoIntoDB(dbCityInfo);
						log.debug("New City Info object saved with cityid {}", dbCityInfo.getId());
					} else {
						log.info("Unable to find mapping for {}, id {}", iataCodeCityMapping.getCityName(),
								iataCodeCityMapping.getCountryName());
					}
				} catch (Exception e) {
					log.info("Error while updating city info for city name {} , id {}",
							iataCodeCityMapping.getCityName(), iataCodeCityMapping.getCountryName(), e);
				}
			});
		}
	}

	private void saveCityInfoList(List<HotelCityInfoQuery> hotelCityInfos) {
		ExecutorService executor = Executors.newFixedThreadPool(10);
		List<Future<?>> futures = new ArrayList<>();
		for (HotelCityInfoQuery hotelCityInfo : hotelCityInfos) {
			Future<?> future = executor.submit(() -> {
				try {
					CityInfo cityInfo = processCityInfo(hotelCityInfo);
					saveCity(cityInfo);
				} catch (Exception e) {
					log.error("Error while processing city info for city name {} and country name {} due to {}",
							hotelCityInfo.getCityname(),
							hotelCityInfo.getCountryname(), e.getMessage());
				}
			});
			futures.add(future);
		}
	}

	public void saveCity(CityInfo cityInfo) {
		DbCityInfo dbCityInfo = new DbCityInfo().from(cityInfo);
		try {
			dbCityInfo = infoService.saveCityInfoIntoDB(dbCityInfo);
			log.debug("New City Info object saved with cityid {}", dbCityInfo.getId());
		} catch (DataIntegrityViolationException e) {
			DbCityInfo oldDbObject =
					infoService.findByCityNameAndCountryName(cityInfo.getCityName(), cityInfo.getCountryName());
			oldDbObject = new GsonMapper<>(dbCityInfo, oldDbObject, DbCityInfo.class).convert();
			log.debug("Updated City Info Object with cityName {}, countryName{} ", oldDbObject.getCityName(),
					oldDbObject.getCountryName());
			infoService.saveCityInfoIntoDB(oldDbObject);
		} catch (Exception e) {
			log.debug("Unable to update city info object for cityName {}, countryName {}", dbCityInfo.getCityName(),
					dbCityInfo.getCountryName(), e.getMessage());
		}
	}

	public Map<String, String> getMapFromCityList(List<CityInfo> cityInfoList) {
		Map<String, String> map = new HashMap<>();
		for (CityInfo cityInfo : cityInfoList) {
			if (map.isEmpty() || !map.containsKey(cityInfo.getCountryName())) {
				map.put(cityInfo.getCountryName(), cityInfo.getCountryId());
			}
		}
		return map;
	}

	public static CityInfo processCityInfo(HotelCityInfoQuery cityInfoQuery) {

		CityInfo cityInfo = CityInfo.builder().cityName(cityInfoQuery.getCityname())
				.countryName(cityInfoQuery.getCountryname()).build();

		return cityInfo;
	}

	public Long isCityExists(CityInfo cityInfo) {
		DbCityInfo dbCityInfo =
				infoService.findByCityNameAndCountryName(cityInfo.getCityName(), cityInfo.getCountryName());
		if (dbCityInfo == null) {
			log.info("Master Db entry not found for city {} , country {}", cityInfo.getCityName(),
					cityInfo.getCountryName());
			return null;
		}
		return dbCityInfo.getId();
	}
	

	public void storeCityMasterListInElasticSearch(){
		
		for (int i = 0; i < 500; i++) {
			Pageable page = new PageRequest(i, 10000, Direction.ASC, "id");
			List<CityInfo> cityInfoChunk = DbCityInfo.toDomainList(infoService.findAll(page));
			if (CollectionUtils.isEmpty(cityInfoChunk)) break;
			List<ESCityInfo> esCityInfoList =  getElasticSearchCityInfo(cityInfoChunk);
			esCommunicator.addBulkDocuments(esCityInfoList, ESMetaInfo.HOTEL_CITY_MAPPING);
			log.debug("Fetched Master City List from database, info list size is {}", cityInfoChunk.size());
		}
	}
	

	private List<ESCityInfo> getElasticSearchCityInfo(List<CityInfo> cityInfoChunk) {
		
		List<ESCityInfo> cityInfoList = new ArrayList<>();
		cityInfoChunk.forEach(cityInfo -> {
			ESCityInfo esCity = ESCityInfo.builder().code(String.valueOf(cityInfo.getId()))
					.name(cityInfo.getCityName()).country_code(cityInfo.getCountryId())
					.country_name(cityInfo.getCountryName()).build();
			cityInfoList.add(esCity);
		});
		return cityInfoList;
	}


}
