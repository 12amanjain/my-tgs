package com.tgs.services.hms.manager.mapping;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.dbmodel.inventory.DbRoomCategory;
import com.tgs.services.hms.dbmodel.inventory.DbRoomTypeInfo;
import com.tgs.services.hms.helper.HotelRoomCategoryHelper;
import com.tgs.services.hms.jparepository.inventory.HotelRoomInfoService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRoomInfoSaveManager {
	
	@Autowired
	HotelRoomInfoService roomService;
	
	@Autowired
	HotelRoomCategoryHelper roomCategoryHelper;
	
	public void storeRoomCategory(String roomCategory) {
		try {
			
			roomCategory = roomCategory.toUpperCase();
			List<String> tokenList = new ArrayList<>(Arrays.asList(roomCategory.split(" ")));
			for(Iterator<String> itr = tokenList.iterator(); itr.hasNext();) {
				String token = itr.next();
				if(!token.matches("^[A-Z]*$")){
					throw new CustomGeneralException(SystemError.APLHABET_ALLOWED_ONLY);
				}
				
			}
			
			roomCategory = tokenList.stream().collect(Collectors.joining(" "));
			DbRoomCategory rc = new DbRoomCategory();
			rc.setRoomCategory(roomCategory);
			roomService.storeRoomCategory(rc);
			roomCategoryHelper.process();
		}catch(DataIntegrityViolationException exception) {
			log.error("Error while storing same Room Category for {}", roomCategory);
			throw new CustomGeneralException(SystemError.SAME_ROOM_CATEGORY);
		}
	}
	
	public void storeRoomType(HotelRoomTypeInfo roomTypeInfo) {
		try {
			DbRoomTypeInfo dbRoomTypeInfo = new DbRoomTypeInfo().from(roomTypeInfo);
			roomService.storeRoomTypeInfo(dbRoomTypeInfo);
		}catch(DataIntegrityViolationException exception) {
			log.error("Error while storing same Room Type for {}", roomTypeInfo);
			throw new CustomGeneralException(SystemError.ERROR_STORING_ROOM_TYPE_INFO);
		}
	}
	
	
	public List<HotelRoomCategory> fetchRoomCategoryList(){
		List<DbRoomCategory> rcList = roomService.findAll();
		return DbRoomCategory.toDomainList(rcList);
	}
	
	public List<HotelRoomTypeInfo> fetchRoomTypeList(){
		List<DbRoomTypeInfo> rcList = roomService.findAllRoomTypeInfo();
		return DbRoomTypeInfo.toDomainList(rcList);
	}
	
	public HotelRoomCategory fetchRoomById(String id) {
		DbRoomCategory rc = roomService.findById(id);
		return rc.toDomain();
	}
	
	public HotelRoomTypeInfo fetchRoomTypeInfoById(String id) {
		DbRoomTypeInfo rc = roomService.findRoomTypeInfoById(id);
		return rc.toDomain();
	}
	
	public List<HotelRoomCategory> fetchRoomCategoryListByIdList(List<Long> ids){
		List<DbRoomCategory> rcList = roomService.findAllByIds(ids);
		return DbRoomCategory.toDomainList(rcList);
	}
	
	public List<HotelRoomTypeInfo> fetchRoomTypeListByIdList(List<Long> ids){
		List<DbRoomTypeInfo> rcList = roomService.findAllRoomTypeInfoByIds(ids);
		return DbRoomTypeInfo.toDomainList(rcList);
	}
}
