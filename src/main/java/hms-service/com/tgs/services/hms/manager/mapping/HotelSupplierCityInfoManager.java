package com.tgs.services.hms.manager.mapping;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelSupplierCityQuery;
import com.tgs.services.hms.datamodel.SupplierCityInfo;
import com.tgs.services.hms.dbmodel.DbSupplierCityInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelCityInfoService;
import com.tgs.services.hms.restmodel.HotelMappedSupplierCityResponse;
import com.tgs.services.hms.restmodel.HotelSupplierCityResponse;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelSupplierCityInfoManager {

	@Autowired
	HotelCityInfoService infoService;

	public void fetchAndSaveSupplierCityInfo(HotelStaticDataRequest staticDataRequest) {
		try {
			staticDataRequest.setLanguage(HotelUtils.getLanguage());
			if (StringUtils.isBlank(staticDataRequest.getSupplierId())) {
				throw new CustomGeneralException(SystemError.INVALID_SUPPLIERID);
			}
			AbstractStaticDataInfoFactory staticDataInfoFactory =
					HotelSourceType.getStaticDataFactoryInstance(staticDataRequest.getSupplierId(), staticDataRequest);
			staticDataInfoFactory.getCityMappingInfo();
		} catch (Exception e) {
			log.error("Unable to handle supplier city info for {}", GsonUtils.getGson().toJson(staticDataRequest), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}
	
	public void saveSupplierCityInfo(List<SupplierCityInfo> cityInfos) {

		cityInfos.forEach(cityInfo -> {
			DbSupplierCityInfo dbCityInfo = new DbSupplierCityInfo().from(cityInfo);
			try {
				dbCityInfo = infoService.saveSupplierCityInfo(dbCityInfo);
				log.debug("New supplier city info object saved with cityid {}", dbCityInfo.getId());
			} catch (DataIntegrityViolationException e) {
				DbSupplierCityInfo oldCityInfo = infoService.findByCityNameAndCountryNameAndSupplierName(
						cityInfo.getCityName(), cityInfo.getCountryName(), cityInfo.getSupplierName());
				oldCityInfo = new GsonMapper<>(dbCityInfo, oldCityInfo, DbSupplierCityInfo.class).convert();
				log.debug("Updated City Info Object with cityName {}, countryName{} and supplier {} ",
						oldCityInfo.getCityName(), oldCityInfo.getCountryName(), oldCityInfo.getSupplierName());
				infoService.saveSupplierCityInfo(oldCityInfo);
			} catch (Exception e) {
				log.debug("Unable to update city info object for cityName {}, countryName {}", dbCityInfo.getCityName(),
						dbCityInfo.getCountryName(), e.getMessage());
			}
		});
	}
	
	public HotelSupplierCityResponse getSupplierCityInfo(HotelSupplierCityQuery supplierCityQuery) {

		HotelSupplierCityResponse supplierCityResponse = new HotelSupplierCityResponse();
		if (StringUtils.isBlank(supplierCityQuery.getCityId()) && StringUtils.isBlank(supplierCityQuery.getCityName()))
			throw new CustomGeneralException(SystemError.INVALID_CITY);

		if (StringUtils.isBlank(supplierCityQuery.getSupplierName()))
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIERID);

		List<SupplierCityInfo> supplierCityInfos = null;
		if(StringUtils.isNotBlank(supplierCityQuery.getCityName())) {
			supplierCityInfos = DbSupplierCityInfo.toDomainList(infoService
				.findByCityNameAndSupplierName(supplierCityQuery.getCityName(), supplierCityQuery.getSupplierName()));
		}

		if(CollectionUtils.isEmpty(supplierCityInfos) && StringUtils.isNotBlank(supplierCityQuery.getCityId())) {
			supplierCityInfos = DbSupplierCityInfo.toDomainList(infoService
					.findByCityIdAndSupplierName(supplierCityQuery.getCityId(), supplierCityQuery.getSupplierName()));
		}
		supplierCityResponse.setCityInfos(supplierCityInfos);
		return supplierCityResponse;
	}

	public SupplierCityInfo findByCityNameAndSupplierName(String cityName, HotelSourceType supplierName) {

		List<SupplierCityInfo> cityInfo =
				DbSupplierCityInfo
						.toDomainList(infoService.findByCityNameAndSupplierName(cityName, supplierName.name()));
		return cityInfo.get(0);
	}
	
	public HotelMappedSupplierCityResponse getMappedSupplierCityInfo(CityInfo cityInfoQuery) {

		HotelMappedSupplierCityResponse supplierCityResponse = new HotelMappedSupplierCityResponse();
		List<CityInfoMapping> cityInfos = null;
		List<SupplierCityInfo> supplierCityInfos = new ArrayList<>();
		
		if (StringUtils.isBlank(Long.toString(cityInfoQuery.getId())))
			throw new CustomGeneralException(SystemError.INVALID_CITY);
		
		cityInfos = DbSupplierCityInfo.toDomainList(infoService.findByCityId(cityInfoQuery.getId()));
		
		for(CityInfoMapping cityInfo: cityInfos) {
			if(!StringUtils.isBlank(cityInfo.getSupplierCityName())) {
				supplierCityInfos.addAll(DbSupplierCityInfo.toDomainList(infoService.
						findByCityIdAndSupplierName(cityInfo.getSupplierCity(),cityInfo.getSupplierName())));
			}
		}
		
		supplierCityResponse.setCityInfos(supplierCityInfos);
		return supplierCityResponse;
	}
}
