package com.tgs.services.hms.mapper;

import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.datamodel.HotelDetailResult;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierBasicInfo;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelCancellationToAnalyticsHotelCancellationMapper extends Mapper<AnalyticsHotelQuery> {

	private HotelInfo hInfo;
	private HotelSearchQuery searchQuery;
	private User user;
	private ContextData contextData;
	private boolean isRoomFacilityAvailable;
	@Builder.Default
	private Set<String> mealBasis = new HashSet<>();
	@Builder.Default
	private Set<String> roomCategory = new HashSet<>();

	@Override
	protected void execute() throws CustomGeneralException {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		output = output != null ? output : AnalyticsHotelQuery.builder().build();
		HotelSupplierBasicInfo basicInfo = null;
		if (hInfo != null) {
			Integer sourceId = hInfo.getOptions().get(0).getMiscInfo().getSourceId();
			String supplierName = hInfo.getOptions().get(0).getMiscInfo().getSupplierId();
			if (ObjectUtils.isEmpty(sourceId)) {
				sourceId = HotelSupplierConfigurationHelper.getSupplierInfo(supplierName).getSourceId();
			}
			basicInfo = HotelSupplierBasicInfo.builder().sourceId(sourceId).supplierName(supplierName).build();
			output.setNoofroom(hInfo.getOptions().get(0).getRoomInfos().size());
			Option option = hInfo.getOptions().get(0);
			output.setIsonrequest(option.getIsOptionOnRequest());
			if (!ObjectUtils.isEmpty(option.getCancellationPolicy())) {
				output.setIszerocancellationallowed(option.getCancellationPolicy().getIsFullRefundAllowed());
			}
			if (!ObjectUtils.isEmpty(option.getDeadlineDateTime()))
				output.setExpirationdate(option.getDeadlineDateTime().format(formatter));
			populateOptionSpecificInfo(option);
			output.setMealbasis(mealBasis.toString());
			output.setRoomcategory(roomCategory.toString());
		}
		output.setRoomfacilityavail(BooleanUtils.isTrue(isRoomFacilityAvailable));
		output = HotelDetailToAnalyticsHotelDetailMapper.builder()
				.detailResult(HotelDetailResult.builder().hotel(hInfo).searchQuery(searchQuery).build()).user(user)
				.contextData(contextData).basicInfo(basicInfo).build().setOutput(output).convert();
		output.setAnalyticstype(HotelFlowType.CANCELLATION.name());
		filterCancellationOutput();
	}

	private void filterCancellationOutput() {
		output.setSearchresultcount(null);
		output.setNumberofadults(null);
		output.setNumberofchild(null);
		output.setOptionscount(null);
	}

	private void populateOptionSpecificInfo(Option option) {
		if (!ObjectUtils.isEmpty(option)) {
			option.getRoomInfos().stream().forEach(roomInfo -> {
				if (CollectionUtils.isNotEmpty(roomInfo.getRoomAmenities())) {
					isRoomFacilityAvailable = true;
				}
				roomCategory.add(roomInfo.getRoomCategory());
				mealBasis.add(roomInfo.getMealBasis());
			});
		}
	}
}
