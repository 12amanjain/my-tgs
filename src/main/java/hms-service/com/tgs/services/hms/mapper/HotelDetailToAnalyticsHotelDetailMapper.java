package com.tgs.services.hms.mapper;

import java.util.Arrays;
import java.util.Comparator;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.datamodel.HotelDetailResult;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelTag;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierBasicInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelDetailToAnalyticsHotelDetailMapper extends Mapper<AnalyticsHotelQuery> {

	private HotelDetailResult detailResult;
	private HotelSupplierBasicInfo basicInfo;
	private User user;
	private ContextData contextData;

	@Override
	protected void execute() throws CustomGeneralException {
		HotelInfo hotelInfo = detailResult.getHotel();
		output = output != null ? output : AnalyticsHotelQuery.builder().build();
		output = HotelSearchToAnalyticsHotelSearchMapper.builder().searchQuery(detailResult.getSearchQuery()).user(user)
				.contextData(contextData).basicInfo(basicInfo).build().setOutput(output).convert();
		output.setAnalyticstype(HotelFlowType.DETAIL.name());
		if (!ObjectUtils.isEmpty(hotelInfo)) {
			output.setHotelname(hotelInfo.getName());
			output.setHotelid(hotelInfo.getId());
			output.setGiataid(hotelInfo.getGiataId());
			output.setOptionscount(hotelInfo.getOptions().size());
			output.setHotelfacilityavail(CollectionUtils.isNotEmpty(hotelInfo.getFacilities()));
			output.setImagesavail(CollectionUtils.isNotEmpty(hotelInfo.getImages()));
			if (!ObjectUtils.isEmpty(hotelInfo.getGeolocation())) {
				output.setLatitude(hotelInfo.getGeolocation().getLatitude());
				output.setLongitude(hotelInfo.getGeolocation().getLongitude());
			}
			output.setPropertytype(hotelInfo.getPropertyType());
			output.setIspreferredhotel(hotelInfo.getTags().contains(HotelTag.PREFERRED));
			output.setTripadvisorid(hotelInfo.getUserReviewSupplierId());
			output.setTotalprice(hotelInfo.getOptions().stream().min(Comparator.comparing(op -> op.getTotalPrice()))
					.get().getTotalPrice());

			output.setCity(output.getCity() != null ? output.getCity() : hotelInfo.getAddress().getCity().getName());
			output.setCountry(
					output.getCountry() != null ? output.getCountry() : hotelInfo.getAddress().getCountry().getName());
			output.setRatings(output.getRatings() != null ? output.getRatings() : Arrays.asList(hotelInfo.getRating()));
		}

		output.setExternalapiflow(contextData.calculateApiWiseTime(SystemCheckPoint.EXTERNAL_API_STARTED.name(),
				SystemCheckPoint.EXTERNAL_API_FINISHED.name()));
		
	}
}
