package com.tgs.services.hms.mapper;

import java.util.HashSet;
import java.util.Set;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.hms.analytics.AnalyticsHotelQuery;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelReviewToAnalyticsHotelReviewMapper extends Mapper<AnalyticsHotelQuery> {

	private User user;
	private ContextData contextData;
	private HotelInfo hInfo;
	private HotelSearchQuery searchQuery;
	private String bookingId;
	private boolean isRoomFacilityAvailable;
	@Builder.Default
	private Set<String> mealBasis = new HashSet<>();
	@Builder.Default
	private Set<String> roomCategory = new HashSet<>();

	@Override
	protected void execute() throws CustomGeneralException {
		output = output != null ? output : AnalyticsHotelQuery.builder().build();
		output.setBookingid(bookingId);
		output = HotelCancellationToAnalyticsHotelCancellationMapper.builder().hInfo(hInfo).searchQuery(searchQuery)
				.user(user).contextData(contextData).build().setOutput(output).convert();
		output.setAnalyticstype(HotelFlowType.REVIEW.name());
	}
}
