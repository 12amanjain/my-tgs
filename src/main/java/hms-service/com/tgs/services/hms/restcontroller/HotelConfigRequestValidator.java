package com.tgs.services.hms.restcontroller;

import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;

@Component
public class HotelConfigRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof HotelConfiguratorInfo) {
			HotelConfiguratorInfo configuratorInfo = (HotelConfiguratorInfo) target;

			if (configuratorInfo.getRuleType() == null)
				rejectValue("ruleType", SystemError.INVALID_HOTEL_CONFIGURATOR_RULE_TYPE, errors);
			
			if(!ObjectUtils.isEmpty(configuratorInfo.getInclusionCriteria())) {
				registerErrors(errors, "inclusionCriteria", (HotelBasicRuleCriteria)configuratorInfo.getInclusionCriteria());
			}
			
			if(!ObjectUtils.isEmpty(configuratorInfo.getExclusionCriteria())) {
				registerErrors(errors, "exclusionCriteria", (HotelBasicRuleCriteria)configuratorInfo.getExclusionCriteria());
			}
			
		}
	}

	private static void rejectValue(String field, SystemError error, Errors errors) {
		errors.rejectValue("ruleType", error.errorCode(), error.getMessage());
	}

}
