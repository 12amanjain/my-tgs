package com.tgs.services.hms.restcontroller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.quotation.HotelQuotationSearchResult;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationAddRequest;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationAddResponse;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationDeleteRequest;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationSearchRequest;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationSearchResponse;
import com.tgs.services.hms.servicehandler.HotelQuotationHandler;


@RestController
@RequestMapping("/hms/v1")
public class HotelQuotationController {

	@Autowired
	HotelQuotationHandler hotelQuotationHandler;
	
	@RequestMapping(value = "/add-quotation", method = RequestMethod.POST)
	protected HotelQuotationAddResponse addQuotation(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelQuotationAddRequest quotationAddRequest) throws Exception {
		hotelQuotationHandler.initData(quotationAddRequest, new HotelQuotationAddResponse());
		return hotelQuotationHandler.getResponse();
	}

	@RequestMapping(value = "/search-quotation", method = RequestMethod.POST)
	protected HotelQuotationSearchResponse searchQuotation(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelQuotationSearchRequest quotationSearchRequest) throws Exception {
		ContextData contextData= SystemContextHolder.getContextData();
		HotelQuotationSearchResponse hotelQuotationSearchResponse = new HotelQuotationSearchResponse();
		List<HotelQuotationSearchResult> hotelQuotationSearchResult = hotelQuotationHandler
				.searchQuotation(quotationSearchRequest, contextData);
		hotelQuotationSearchResponse.setHotelQuotationSearchResult(hotelQuotationSearchResult);
		return hotelQuotationSearchResponse;
	}

	@RequestMapping(value = "/delete-quotation", method = RequestMethod.DELETE)
	protected BaseResponse deleteQuotation(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelQuotationDeleteRequest quotationDeleteRequest) throws Exception {
		ContextData contextData= SystemContextHolder.getContextData();
		return hotelQuotationHandler.deleteQuotation(quotationDeleteRequest, contextData);
	}

}
