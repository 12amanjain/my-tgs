package com.tgs.services.hms.restcontroller;


import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.restmodel.HotelDetailRequest;
import com.tgs.services.hms.restmodel.HotelDetailResponse;
import com.tgs.services.hms.restmodel.HotelSearchQueryListResponse;
import com.tgs.services.hms.restmodel.HotelSearchRequest;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import com.tgs.services.hms.servicehandler.HotelDetailHandler;
import com.tgs.services.hms.servicehandler.HotelSearchHandler;
import com.tgs.services.hms.servicehandler.HotelSearchResultValidateHandler;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.hms.validators.HotelSearchQueryValidator;

import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/hms/v1")
@Api(value = "hotelsearch", description = "Operations pertaining to Hotel search")
@Slf4j
public class HotelSearchController {

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HotelSearchQueryValidator validator;

	@Autowired
	HotelSearchHandler searchHandler;

	@Autowired
	HMSCachingServiceCommunicator cachingService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelDetailHandler detailHandler;

	@Autowired
	HotelSearchResultValidateHandler validateHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@RequestMapping(value = "/hotel-searchquery-list", method = RequestMethod.POST)
	protected HotelSearchQueryListResponse getSearchList(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid HotelSearchRequest searchRequest) throws Exception {

		HotelSearchQuery searchQuery = null;
		HotelSearchQueryListResponse searchQueryListResponse = new HotelSearchQueryListResponse();
		HotelSearchResponse searchResponse = null;
		String searchId = null;
		if (BooleanUtils.isTrue(searchRequest.getCrossSell())) {
			searchId = BaseUtils.appendCrossSellInId(searchRequest.getBookingId());
			searchQuery = searchManager.getSearchQuery(searchId);
			HotelUtils.populateOldSearchQueryWithNewSearchQueryParam(searchQuery, searchRequest.getSearchQuery());
			searchResponse = searchManager.searchCrossSellHotels(searchQuery);
		} else {
			searchId = searchRequest.getSearchId();
			HotelSearchQuery cachedSearchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			searchQuery = searchRequest.getSearchQuery();
			searchManager.updateSearchQueryForNewSearch(searchQuery);
			boolean isSame = Objects.equals(cachedSearchQuery, searchQuery);

			if (StringUtils.isBlank(searchId) || !isSame) {
				/*
				 * Assuming that it is new search
				 */
				HotelSearchManager.setSearchId(searchQuery);
				log.info("New SearchId {} Generated", searchQuery.getSearchId());
				searchQueryListResponse.addSearchId(searchQuery.getSearchId());
				cacheHandler.storeSearchQueryAndStartAt(searchQuery);

			} else {
				searchQueryListResponse.addSearchId(cachedSearchQuery.getSearchId());
				searchQuery = cachedSearchQuery;
			}
			searchManager.search(searchQuery);
		}

		if (BooleanUtils.isTrue(searchRequest.getSync())) {
			if (BooleanUtils.isTrue(searchRequest.getCrossSell())) {
				searchResponse = searchManager.getSearchResult(searchId);
			} else {
				searchResponse = searchManager.getSearchResult(searchQuery.getSearchId());
			}
			searchQueryListResponse.setSearchQuery(searchQuery);
			searchQueryListResponse.setSearchResult(searchResponse.getSearchResult());
		}
		return searchQueryListResponse;
	}

	@RequestMapping(value = "/hotel-search", method = RequestMethod.POST)
	protected HotelSearchResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelSearchRequest searchRequest) throws Exception {
		searchHandler.initData(searchRequest, new HotelSearchResponse());
		return searchHandler.getResponse();
	}

	@RequestMapping(value = "/hotelDetail-search", method = RequestMethod.POST)
	protected HotelDetailResponse searchHotelDetail(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelDetailRequest searchRequest) throws Exception {
		detailHandler.initData(searchRequest, new HotelDetailResponse());
		return detailHandler.getResponse();
	}

	@RequestMapping(value = "/valid", method = RequestMethod.POST)
	protected BaseResponse isValid(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelSearchRequest searchRequest) throws Exception {
		validateHandler.initData(searchRequest, new BaseResponse());
		return validateHandler.getResponse();
	}

}
