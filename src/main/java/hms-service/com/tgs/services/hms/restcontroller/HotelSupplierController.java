package com.tgs.services.hms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.hms.restmodel.HotelSourceInfoResponse;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.HotelSupplierInfoFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierInfo;
import com.tgs.services.hms.restmodel.HotelSupplierInfoResponse;
import com.tgs.services.hms.servicehandler.HotelSupplierCredentialHandler;


@RequestMapping("/hms/v1/supplier")
@RestController
public class HotelSupplierController {

	@Autowired
	HotelSupplierInfoRequestValidator supplierInfoValidator;

	@Autowired
	HotelSupplierCredentialHandler supplierHandler;
	
	@Autowired
	AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(supplierInfoValidator);
	}

	@RequestMapping(value = "/info/save", method = RequestMethod.POST)
	protected HotelSupplierInfoResponse saveOrUpdateSupplierInfo(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody HotelSupplierInfo supplierInfo) throws Exception {
		supplierHandler.initData(supplierInfo, new HotelSupplierInfoResponse());
		return supplierHandler.getResponse();
	}

	@RequestMapping(value = "/info/save/list", method = RequestMethod.POST)
	protected HotelSupplierInfoResponse saveOrUpdateSupplierInfos(HttpServletRequest request,
			HttpServletResponse response, @RequestBody List<HotelSupplierInfo> supplierInfos) throws Exception {
		HotelSupplierInfoResponse supplierInfoResponse = new HotelSupplierInfoResponse();
		for (HotelSupplierInfo supplierInfo : supplierInfos) {
			supplierInfoResponse.getSupplierInfos()
					.addAll(saveOrUpdateSupplierInfo(request, response, supplierInfo).getSupplierInfos());
		}
		return supplierHandler.getResponse();
	}

	@RequestMapping(value = "/info/list", method = RequestMethod.POST)
	protected HotelSupplierInfoResponse listSupplierInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelSupplierInfoFilter supplierInfoFilter) throws Exception {
		return supplierHandler.getSupplierInfo(supplierInfoFilter);
	}

	@RequestMapping(value = "/info/list/src", method = RequestMethod.GET)
	protected HotelSourceInfoResponse listSourceInfo(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		return supplierHandler.getSourceInfo();
	}

	@RequestMapping(value = "/info/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse updateInfoStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return supplierHandler.updateStatusSupplierInfo(id, status);
	}
	
	@RequestMapping(value = "/info/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchSupplierInfoAudits(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbHotelSupplierInfo.class, ""));
		return auditResponse;
	}
}
