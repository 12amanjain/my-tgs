package com.tgs.services.hms.restcontroller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;

@Component
public class HotelSupplierInfoRequestValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof HotelSupplierInfo) {
			HotelSupplierInfo supplierInfo = (HotelSupplierInfo) target;
			if (supplierInfo.getEnabled() == null) {
				errors.rejectValue("enabled", "", "Supplier enabled field is not present");
			}
			if (supplierInfo.getSourceId() == null && supplierInfo.getSourceId() <= 0) {
				errors.rejectValue("sourceId", SystemError.INVALID_SOURCE_ID.errorCode(),
						SystemError.INVALID_SOURCE_ID.getMessage());
			}
			if (StringUtils.isEmpty(supplierInfo.getName())) {
				errors.rejectValue("name", SystemError.INVALID_SUPPLIER_NAME.errorCode(),
						SystemError.INVALID_SUPPLIER_NAME.getMessage());
			}
			HotelSupplierCredential supplierCredential = supplierInfo.getCredentialInfo();
			if (StringUtils.isNotBlank(supplierCredential.getUserName())
					&& StringUtils.isNotBlank(supplierCredential.getPassword())) {
				if (StringUtils.isEmpty(supplierCredential.getUserName())) {
					errors.rejectValue("credentialInfo.userName", SystemError.INVALID_SUPPLIER_NAME.errorCode(),
							SystemError.INVALID_SUPPLIER_NAME.getMessage());
				}
				if (StringUtils.isEmpty(supplierCredential.getPassword())
						|| supplierCredential.getPassword().length() < 3) {
					errors.rejectValue("credentialInfo.password", SystemError.INVALID_SUPPLIER_PASSWORD.errorCode(),
							SystemError.INVALID_SUPPLIER_PASSWORD.getMessage());
				}
			}
		}
	}
}
