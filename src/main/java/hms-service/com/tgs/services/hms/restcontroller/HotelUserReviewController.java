package com.tgs.services.hms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.hms.restmodel.HotelUserReviewRequest;
import com.tgs.services.hms.restmodel.HotelUserReviewResponse;
import com.tgs.services.hms.servicehandler.HotelUserReviewHandler;

@RestController
@RequestMapping("/hms/v1")
public class HotelUserReviewController {
	
	@Autowired
	HotelUserReviewHandler reviewHandler;
	
	@RequestMapping(value = "/user-review" , method = RequestMethod.POST)
	protected HotelUserReviewResponse getHotelReview(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelUserReviewRequest reviewRequest) throws Exception {
		
		reviewHandler.initData(reviewRequest, new HotelUserReviewResponse());
		return reviewHandler.getResponse();
	}
	
}
