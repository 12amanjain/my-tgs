
package com.tgs.services.hms.restcontroller.inventory;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.filters.HotelRatePlanFilter;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.restmodel.inventory.HotelRatePlanRequest;

@Component
public class HotelRatePlanRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target == null) {
			return;
		}

		if (target instanceof HotelRatePlanRequest) {
			HotelRatePlanRequest ratePlanRequest = (HotelRatePlanRequest) target;
			if (CollectionUtils.isNotEmpty(ratePlanRequest.getRatePlans())) {
				registerErrors(errors, "", ratePlanRequest);
			} else {
				errors.rejectValue(SystemError.INVALID_REQUEST.getMessage(), SystemError.INVALID_REQUEST.getMessage());
			}
		}

		if (target instanceof HotelRatePlanFilter) {
			int allowedDateDifference = 90;
			HotelRatePlanFilter ratePlanFilter = (HotelRatePlanFilter) target;
			if (ratePlanFilter.getValidOnLessThan() != null && ratePlanFilter.getValidOnGreaterThan() == null) {
				errors.rejectValue("validOnGreaterThan", SystemError.VALID_ON_DATE_CHECK.errorCode(),
						SystemError.VALID_ON_DATE_CHECK.getMessage());
			} else if (ratePlanFilter.getValidOnLessThan() == null && ratePlanFilter.getValidOnGreaterThan() != null) {
				errors.rejectValue("validOnLessThan", SystemError.VALID_ON_DATE_CHECK.errorCode(),
						SystemError.VALID_ON_DATE_CHECK.getMessage());
			} else if (ratePlanFilter.getValidOnLessThan() != null && ratePlanFilter.getValidOnGreaterThan() != null) {
				long gap = ChronoUnit.DAYS.between(ratePlanFilter.getValidOnLessThan(),
						ratePlanFilter.getValidOnGreaterThan());
				if (gap > allowedDateDifference) {
					errors.rejectValue("validOnLessThan", SystemError.VALID_ON_DATE_DIFFERENCE.errorCode(),
							SystemError.VALID_ON_DATE_DIFFERENCE.getMessage() + ", Max allowed is :" + allowedDateDifference);
				}
			}
		}
	}
}
