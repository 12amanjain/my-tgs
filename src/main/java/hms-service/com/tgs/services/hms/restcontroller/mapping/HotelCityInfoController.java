package com.tgs.services.hms.restcontroller.mapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.hms.manager.mapping.HotelCityInfoSaveManager;
import com.tgs.services.hms.restmodel.FetchHotelCityInfoResponse;
import com.tgs.services.hms.restmodel.HotelCityInfoRequest;
import com.tgs.services.hms.restmodel.HotelIataCodeMappingRequest;
import com.tgs.services.hms.servicehandler.HotelCityInfoFetchHandler;

@RestController
@RequestMapping("/hms/v1")
public class HotelCityInfoController {

	@Autowired
	HotelCityInfoSaveManager saveManager;
	
	@Autowired
	HotelCityInfoFetchHandler cityInfoHandler;

	@RequestMapping(value = "/city-save", method = RequestMethod.POST)
	protected String saveCityInfoIntoDB(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelCityInfoRequest cityInfoRequest) throws Exception {
		saveManager.process(cityInfoRequest);
		return "Record Saved Successfully";
	}

	@RequestMapping(value = "/city-iata-save", method = RequestMethod.POST)
	protected String saveIataInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelIataCodeMappingRequest iataCodeMappingRequest) throws Exception {
		saveManager.saveIataInfo(iataCodeMappingRequest.getIataCodeCityMappings());
		return "Record Saved Successfully";
	}
	
	@RequestMapping(value = {"/static-cities" , "/static-cities/{next}"}, method = RequestMethod.GET)
	protected FetchHotelCityInfoResponse getCityInfo(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(required = false) String next) throws Exception {
		cityInfoHandler.initData(next, new FetchHotelCityInfoResponse());
		return cityInfoHandler.getResponse();
	}
	
	@RequestMapping(value = "/city-save-es", method = RequestMethod.POST)
	protected String saveCityInfoIntoES(HttpServletRequest request, HttpServletResponse response) throws Exception{
		saveManager.storeCityMasterListInElasticSearch();
		return "Record Saved Successfully";
	}
}
