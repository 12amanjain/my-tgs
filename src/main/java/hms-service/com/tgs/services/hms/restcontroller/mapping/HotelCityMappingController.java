package com.tgs.services.hms.restcontroller.mapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.restmodel.HotelCityInfoRequest;
import com.tgs.services.hms.restmodel.HotelCityMappingRequest;
import com.tgs.services.hms.restmodel.HotelDeleteMappingRequest;
import com.tgs.services.hms.restmodel.HotelUpdateCityInfoMappingRequest;

@RestController
@RequestMapping("/hms/v1")
public class HotelCityMappingController {

	@Autowired
	HotelCityInfoMappingManager mappingManager;


	@RequestMapping(value = "/city-mapping", method = RequestMethod.POST)
	protected String saveSupplierCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelCityInfoRequest cityInfoRequest) throws Exception {
		mappingManager.process(cityInfoRequest);
		return "Record Saved Successfully";
	}
	
	@RequestMapping(value = "/upload-mapping", method = RequestMethod.POST)
	protected BaseResponse uploadSupplierCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelCityMappingRequest cityInfoRequest) throws Exception {
		mappingManager.saveHotelCityMappingRequest(cityInfoRequest);
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/fetch-mapping/{cityId}", method = RequestMethod.POST)
	protected BaseResponse uploadSupplierCityMapping(HttpServletRequest request,
			HttpServletResponse response,
			@PathVariable("cityId") String cityId) throws Exception {
		return mappingManager.getHotelCityMappingForCityId(cityId);
	}
	
	@RequestMapping(value = "/city-mapping-elasticsearch", method = RequestMethod.POST)
	protected BaseResponse updateCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelUpdateCityInfoMappingRequest cityInfoRequest) throws Exception {
		mappingManager.updateMapping(cityInfoRequest);
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/delete-mapping", method = RequestMethod.POST)
	protected BaseResponse deleteSupplierCityMapping(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelDeleteMappingRequest cityInfoRequest) throws Exception {
		mappingManager.deleteHotelCityMapping(cityInfoRequest);
		return new BaseResponse();
	}

}
