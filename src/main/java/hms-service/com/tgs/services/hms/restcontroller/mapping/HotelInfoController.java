package com.tgs.services.hms.restcontroller.mapping;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.hms.datamodel.ESHotel;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.FetchHotelInfoResponse;
import com.tgs.services.hms.restmodel.HotelInfoStaticDataRequest;
import com.tgs.services.hms.restmodel.HotelMasterDataResponse;
import com.tgs.services.hms.restmodel.HotelMealRequest;
import com.tgs.services.hms.servicehandler.HotelInfoFetchHandler;

@RestController
@RequestMapping("/hms/v1")
public class HotelInfoController {

	@Autowired
	HotelInfoSaveManager saveManager;
	
	@Autowired
	HotelInfoFetchHandler hotelInfoHandler;

	@RequestMapping(value = "/hotel-save", method = RequestMethod.POST)
	protected String saveHotelInfoIntoDB(HttpServletRequest request, HttpServletResponse response,
			@RequestBody String filePath) throws Exception {
		saveManager.process(filePath);
		return "Record Saved Successfully";
	}

	@RequestMapping(value = "/upload-meal-mapping", method = RequestMethod.POST)
	protected BulkUploadResponse saveSupplierHotelMealBasis(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelMealRequest unmappedMealRequest) throws Exception {
		return saveManager.saveHotelMealInfoList(unmappedMealRequest);
	}

	@Deprecated
	@RequestMapping(value = "/hotel-meal-save", method = RequestMethod.POST)
	protected String saveSupplierHotelMealBasis(HttpServletRequest request, HttpServletResponse response,
			@RequestBody String filePath) throws Exception {
		saveManager.processHotelMealInfo(filePath);
		return "Record Saved Successfully";
	}
	
	@RequestMapping(value = {"/static-hotels" , "/static-hotels/{next}"}, method = RequestMethod.GET)
	protected FetchHotelInfoResponse getCityInfo(HttpServletRequest request, HttpServletResponse response,
			@PathVariable(required = false) String next) throws Exception {
		hotelInfoHandler.initData(next, new FetchHotelInfoResponse());
		return hotelInfoHandler.getResponse();
	}
	
	/*
	 * API to add master hotel 
	 */
	@RequestMapping(value = "/master/hotel" , method = RequestMethod.POST)
	protected HotelMasterDataResponse saveHotelInfo(HttpServletRequest request, HttpServletResponse response
			, @RequestBody HotelInfoStaticDataRequest staticDataRequest) {
		HotelInfo hInfo = saveManager.saveMasterHotel(staticDataRequest.getStaticDataRequest());
		return HotelMasterDataResponse.builder().hInfo(hInfo).build();
	}
	
	/*
	 * API to do modifications to master data.
	 * Some improvisation like only allowed users should be able to 
	 * modify master data
	 * 
	 */
	@RequestMapping(value = "/inventory/modify-hotel", method = RequestMethod.POST)
	protected HotelMasterDataResponse modifyHotelInfo(HttpServletRequest request, HttpServletResponse response
			, @RequestBody HotelInfoStaticDataRequest staticDataRequest) {
		HotelInfo hInfo =  saveManager.modifyMasterHotel(staticDataRequest.getStaticDataRequest());
		return HotelMasterDataResponse.builder().hInfo(hInfo).build();
	}
	
	/*
	 * API to sync master hotel data with elastic search. Currently Only hotelName & Id
	 * are being stored in ElasticSearch.
	 * Some improvisation required i.e. fetching address also.
	 */
	@RequestMapping(value = "/sync-es/hotels" , method = RequestMethod.POST)
	protected BaseResponse saveHotelMasterDataIntoElasticSearch(HttpServletRequest request, HttpServletResponse response) {
		saveManager.syncMasterHotelsWithElasticSearch();
		return new BaseResponse();
	}
	
	/*
	 * API to fetch master hotel by id.
	 * This could be later moved to call on cache
	 */
	@RequestMapping(value = "/master/hotel/{id}" , method = RequestMethod.GET)
	protected HotelMasterDataResponse getHotelInfo(HttpServletRequest request, HttpServletResponse response
			, @PathVariable String id) {
		HotelInfo hInfo = saveManager.fetchFromDb(id);
		return HotelMasterDataResponse.builder().hInfo(hInfo).build();
	}
	
	@RequestMapping(value = "/es/hotel/search", method = RequestMethod.POST)
	protected HotelMasterDataResponse getHotelDataFromElasticSearch(HttpServletRequest request, HttpServletResponse response
			, @RequestBody HotelInfoStaticDataRequest staticDataRequest){
		List<ESHotel> esHotels = saveManager.fetchHotelDataFromElasticSearch(staticDataRequest.getStaticDataRequest().getHInfo());
		return HotelMasterDataResponse.builder().esHotel(esHotels).build();
	}
	
	/* API to add list of master hotels*/
	@RequestMapping(value = "/master/hotellist" , method = RequestMethod.POST)
	protected String saveHotelInfos(HttpServletRequest request, HttpServletResponse response
			, @RequestBody HotelInfoStaticDataRequest staticDataRequest) {
		
		saveManager.saveHotelInfos(staticDataRequest.getStaticDataRequest());
		return  "Record Saved Successfully";
	}

}
