package com.tgs.services.hms.restcontroller.mapping;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.manager.mapping.HotelRoomInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelFetchRoomCategoryResponse;
import com.tgs.services.hms.restmodel.inventory.HotelRoomTypeInfoRequest;

@RestController
@RequestMapping("/hms/v1")
public class HotelRoomStaticInfoController {
	
	@Autowired
	HotelRoomInfoSaveManager roomManager;
	
	@RequestMapping(value = "/room-category/{category}", method = RequestMethod.POST)
	protected BaseResponse saveMasterRoomCategoryIntoDB(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String category) throws Exception {
		
		roomManager.storeRoomCategory(category);
		return new BaseResponse();
	}

	@RequestMapping(value = "/room-categories", method = RequestMethod.GET)
	protected HotelFetchRoomCategoryResponse fetchMasterRoomCategories(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		List<HotelRoomCategory> roomCategoryList =  roomManager.fetchRoomCategoryList();
		HotelFetchRoomCategoryResponse roomCategoryListResponse = new HotelFetchRoomCategoryResponse();
		roomCategoryListResponse.setRoomCategoryList(roomCategoryList);
		return roomCategoryListResponse;
		
	}
	
	@RequestMapping(value = "/room-type", method = RequestMethod.POST)
	protected BaseResponse saveRoomTypeIntoDB(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelRoomTypeInfoRequest roomInfoRequest) throws Exception {
		
		roomManager.storeRoomType(roomInfoRequest.getRoomTypeInfo());
		return new BaseResponse();
	}

	@RequestMapping(value = "/room-types", method = RequestMethod.GET)
	protected HotelFetchRoomCategoryResponse fetcRoomTypeInfo(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		List<HotelRoomTypeInfo> roomTypeInfoList =  roomManager.fetchRoomTypeList();
		HotelFetchRoomCategoryResponse roomCategoryListResponse = new HotelFetchRoomCategoryResponse();
		roomCategoryListResponse.setRoomTypeInfoList(roomTypeInfoList);
		return roomCategoryListResponse;
		
	}
}

