package com.tgs.services.hms.restcontroller.mapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.manager.mapping.HotelSupplierCityInfoManager;
import com.tgs.services.hms.restmodel.HotelMappedSupplierCityRequest;
import com.tgs.services.hms.restmodel.HotelMappedSupplierCityResponse;
import com.tgs.services.hms.restmodel.HotelSupplierCityRequest;
import com.tgs.services.hms.restmodel.HotelSupplierCityResponse;

@RestController
@RequestMapping("/hms/v1/supplier")
public class HotelSupplierCityInfoController {

	@Autowired
	HotelSupplierCityInfoManager supplierCityManager;

	@RequestMapping(value = "/city-save", method = RequestMethod.POST)
	protected BaseResponse saveSupplierCityInfoIntoDB(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelStaticDataRequest staticDataRequest) throws Exception {
		supplierCityManager.fetchAndSaveSupplierCityInfo(staticDataRequest);
		return new BaseResponse();
	}

	@RequestMapping(value = "/fetch-city", method = RequestMethod.POST)
	protected HotelSupplierCityResponse getSupplierCityInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelSupplierCityRequest supplierCityRequest) throws Exception {
		return supplierCityManager.getSupplierCityInfo(supplierCityRequest.getSupplierCityQuery());
	}
	
	@RequestMapping(value = "/mapped-city", method = RequestMethod.POST)
	protected HotelMappedSupplierCityResponse getMappedSupplierCityInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelMappedSupplierCityRequest supplierCityRequest) throws Exception {
		return supplierCityManager.getMappedSupplierCityInfo(supplierCityRequest.getSupplierCityQuery());
	}
}
