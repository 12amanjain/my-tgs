package com.tgs.services.hms.restcontroller.staticdata;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.restmodel.HotelCityStaticDataRequest;
import com.tgs.services.hms.restmodel.HotelInfoStaticDataRequest;
import com.tgs.services.hms.restmodel.HotelNationalityStaticDataRequest;
import com.tgs.services.hms.servicehandler.HotelCityStaticDataHandler;
import com.tgs.services.hms.servicehandler.HotelInfoStaticDataHandler;
import com.tgs.services.hms.servicehandler.HotelNationalityStaticDataHandler;

@RestController
@RequestMapping("/hms/v1/job")
public class HotelStaticDataController {

	@Autowired
	HotelCityStaticDataHandler cityStaticDataHandler;
	
	@Autowired
	HotelInfoStaticDataHandler hotelStaticDataHandler;
	
	@Autowired
	HotelNationalityStaticDataHandler hotelNationalityHandler;
	
	
	@RequestMapping(value = "/static-cities", method = RequestMethod.POST)
	protected BaseResponse getCities(HttpServletRequest request, HttpServletResponse response
			, @RequestBody HotelCityStaticDataRequest cityRequest) throws Exception {
		cityStaticDataHandler.initData(cityRequest.getStaticDataRequest(), new BaseResponse());
		cityStaticDataHandler.process();
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/static-hotels", method = RequestMethod.POST)
	protected BaseResponse getHotels(HttpServletRequest request, HttpServletResponse response , 
			@RequestBody HotelInfoStaticDataRequest hotelRequest) throws Exception {
		hotelStaticDataHandler.initData(hotelRequest.getStaticDataRequest(), new BaseResponse());
		hotelStaticDataHandler.process();
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/static-nationality", method = RequestMethod.POST)
	protected BaseResponse saveNationality(HttpServletRequest request, HttpServletResponse response , 
			@RequestBody HotelNationalityStaticDataRequest staticRequest) throws Exception {
		hotelNationalityHandler.initData(staticRequest, new BaseResponse());
		hotelNationalityHandler.process();
		return new BaseResponse();
	}
	
}
