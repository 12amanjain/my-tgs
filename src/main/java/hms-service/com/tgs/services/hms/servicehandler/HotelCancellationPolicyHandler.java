package com.tgs.services.hms.servicehandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelCancellationPolicySearch;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyRequest;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyResponse;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelCancellationPolicyHandler extends ServiceHandler<HotelCancellationPolicyRequest, HotelCancellationPolicyResponse> {

	@Autowired
	HotelCancellationPolicySearch search;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {

		HotelInfo hInfo = null;
		HotelSearchQuery searchQuery = null;
		String searchId = null;
		try {
			hInfo = cacheHandler.getCachedHotelById(request.getId());
			searchId = HotelUtils.getSearchId(request.getId());
			searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			search.getCancellationPolicy(searchQuery, hInfo, request.getOptionId(), searchId);
			Option option = HotelUtils.filterOptionById(hInfo, request.getOptionId());
			HotelCancellationPolicy cp = option.getCancellationPolicy();
			response.setCancellationPolicy(cp);
			response.setId(request.getId());

		} catch (Exception e) {
			log.error("Error while searching for cancellation policy for query {}", searchQuery, e);
		} finally {
			SystemContextHolder.getContextData().getReqIds().add(searchId);
		}
	}

	@Override
	public void afterProcess() throws Exception {
	}

}
