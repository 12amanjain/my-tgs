package com.tgs.services.hms.servicehandler;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.utils.HibernateUtils;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.HotelStaticCityInfo;
import com.tgs.services.hms.dbmodel.DbCityInfo;
import com.tgs.services.hms.jparepository.HotelCityInfoService;
import com.tgs.services.hms.restmodel.FetchHotelCityInfoResponse;

@Service
public class HotelCityInfoFetchHandler extends ServiceHandler<String,FetchHotelCityInfoResponse> {
	
	private final static long LIMIT = 1000;
	
	@Autowired
	HotelCityInfoService cityInfoService;
	
	@Override
	public void beforeProcess() throws Exception {
		if(StringUtils.isBlank(request))
			updateForFirstRequest();
		
	}

	private void updateForFirstRequest() {
		request = HibernateUtils.getNextDataSetCursorInfo(LIMIT, 0l);
	}
	
	
	@Override
	public void process() throws Exception {
		
		
		String next = null;
		long[] data = HibernateUtils.getDataFromRequest(request);
		List<DbCityInfo> dbCityList = cityInfoService.findByIdGreaterThanOrderByIdAscLimit(data[0], data[1]);
		List<CityInfo> cityList =  DbCityInfo.toDomainList(dbCityList);
		if(cityList.size() == LIMIT) {
			Long lastAccessedId = cityList.get(cityList.size()-1).getId();
			next = HibernateUtils.getNextDataSetCursorInfo(LIMIT, lastAccessedId);
		}
		HotelStaticCityInfo fetchCityInfo = HotelStaticCityInfo.builder().next(next)
				.cityInfoList(cityList).build();
		response.setResponse(fetchCityInfo);
		
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
