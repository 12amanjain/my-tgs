package com.tgs.services.hms.servicehandler;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.HotelConfiguratorInfoFilter;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.hms.HotelBasicRuleCriteria;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.dbmodel.DbHotelConfiguratorRule;
import com.tgs.services.hms.jparepository.HotelConfiguratorService;
import com.tgs.services.hms.restmodel.HotelConfigRuleTypeResponse;


@Service
public class HotelConfigHandler extends ServiceHandler<HotelConfiguratorInfo, HotelConfigRuleTypeResponse> {

	@Autowired
	private HotelConfiguratorService configService;

	@Autowired
	SyncService syncService;
	
	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {

		DbHotelConfiguratorRule rule = null;
		if (Objects.nonNull(request.getId())) {
			rule = configService.findById(request.getId());
		}
		
		if (rule != null) {
			HotelConfiguratorInfo configuratorInfo = rule.toDomain();
			patchUpdate(configuratorInfo, request);
		}
		request.cleanData();
		rule = new DbHotelConfiguratorRule().from(request);
		rule = configService.save(rule);
		request.setId(rule.getId());
		syncService.sync("hms", rule.toDomain());

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	public HotelConfigRuleTypeResponse getConfiguratorInfo(HotelConfiguratorInfoFilter configuratorInfoFilter) {
		HotelConfigRuleTypeResponse configResponse = new HotelConfigRuleTypeResponse();
		configService.findAll(configuratorInfoFilter).forEach(configurator -> {
			configResponse.getHotelConfiguratorInfos().add(configurator.toDomain());
		});
		return configResponse;
	}

	public BaseResponse deleteConfiguratorInfo(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbHotelConfiguratorRule configuratorInfo = configService.findById(id);
		if (Objects.nonNull(configuratorInfo)) {
			configuratorInfo.setEnabled(Boolean.FALSE);
			configuratorInfo.setDeleted(Boolean.TRUE);
			configService.save(configuratorInfo);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateConfigStatus(Long id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbHotelConfiguratorRule configuratorInfo = configService.findById(id);
		if (Objects.nonNull(configuratorInfo)) {
			configuratorInfo.setEnabled(status);
			configService.save(configuratorInfo);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
	
	public void patchUpdate(HotelConfiguratorInfo oldConfig, HotelConfiguratorInfo newConfig) {
		if (oldConfig == null) {
			return;
		}
		HotelBasicRuleCriteria oldInclusionCriteria = (HotelBasicRuleCriteria) oldConfig.getInclusionCriteria();
		HotelBasicRuleCriteria oldExclusionCriteria = (HotelBasicRuleCriteria) oldConfig.getExclusionCriteria();

		HotelBasicRuleCriteria newInclusionCriteria = (HotelBasicRuleCriteria) newConfig.getInclusionCriteria();
		HotelBasicRuleCriteria newExclusionCriteria = (HotelBasicRuleCriteria) newConfig.getExclusionCriteria();

		if (!Objects.isNull(oldInclusionCriteria) && !Objects.isNull(newInclusionCriteria)
				&& (!Objects.isNull(oldInclusionCriteria.getIsCrossSellPreferred())
						&& Objects.isNull(newInclusionCriteria.getIsCrossSellPreferred()))) {
			oldInclusionCriteria.setIsCrossSellPreferred(null);
		}

		if (!Objects.isNull(oldExclusionCriteria) && !Objects.isNull(newExclusionCriteria)
				&& !Objects.isNull(oldExclusionCriteria.getIsCrossSellPreferred())
				&& Objects.isNull(newExclusionCriteria.getIsCrossSellPreferred())) {
			oldExclusionCriteria.setIsCrossSellPreferred(null);
		}
		
		newConfig.setInclusionCriteria(
				new GsonMapper<>(newInclusionCriteria, oldInclusionCriteria, HotelBasicRuleCriteria.class).convert());
		newConfig.setExclusionCriteria(
				new GsonMapper<>(newExclusionCriteria, oldExclusionCriteria, HotelBasicRuleCriteria.class).convert());
		newConfig.cleanData();
	}

}
