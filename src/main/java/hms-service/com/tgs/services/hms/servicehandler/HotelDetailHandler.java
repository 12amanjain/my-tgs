package com.tgs.services.hms.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.manager.HotelDetailManager;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.restmodel.HotelDetailRequest;
import com.tgs.services.hms.restmodel.HotelDetailResponse;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelDetailHandler extends ServiceHandler<HotelDetailRequest, HotelDetailResponse> {

	@Autowired
	HMSCachingServiceCommunicator cacheService;

	@Autowired
	HotelDetailManager detailManager;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HotelSearchResultProcessingManager processingManager;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {

		HotelSearchQuery searchQuery = null;
		HotelInfo hInfo = null;
		String searchId = null;
		try {

			searchId = HotelUtils.getSearchId(request.getId());
			HotelSearchQuery oldSearchQuery = searchManager.getSearchQuery(searchId);

			boolean isSearchCompleted = cacheHandler.isSearchCompleted(searchId);
			if (isSearchCompleted) {
				oldSearchQuery.setSearchCompleted(true);
			}
			hInfo = getHotelDetails(request.getId());
			if (ObjectUtils.isEmpty(hInfo)) {
				log.info("Hotel details not found in cache for hotel id {}", request.getId());
			}

			searchQuery = request.getSearchQuery();
			if (searchQuery != null && !Objects.equals(oldSearchQuery, searchQuery)) {
				log.info("Doing search for same supplier for search id {} and hotel id {}", searchQuery.getSourceId(),
						hInfo.getId());
				HotelSearchManager.setSearchId(searchQuery);
				HotelUtils.populateMissingParametersInHotelSearchQuery(searchQuery);
				cacheHandler.storeSearchQueryInCache(searchQuery);
				hInfo = searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
				searchQuery.setSearchCompleted(true);

			} else {
				searchQuery = oldSearchQuery;
				/**
				 * Keeping result for additional time to decrease response time. In future we can move 2 hour source
				 * type.
				 */
				if (hInfo != null && (!HotelUtils.allOptionDetailHit(hInfo) && hInfo.isExpired())
						|| (!ObjectUtils.isEmpty(hInfo.getMiscInfo().getSearchKeyExpiryTime()) && hInfo.getMiscInfo()
								.getSearchKeyExpiryTime().plusHours(2).isBefore(LocalDateTime.now()))) {
					log.info(
							"Doing new search for suppliers {} because of expiration of key for search id {} and hotel id {}",
							HotelUtils.getSupplierIdsFromHotelInfo(hInfo), searchQuery.getSearchId(), hInfo.getId());
					hInfo = searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
					searchManager.doFullSearchAgain(searchQuery, contextData);
				}
			}
			if (!HotelUtils.allOptionDetailHit(hInfo)) {
				fetchHotelDetails(searchQuery, hInfo, contextData);
			}

			SystemContextHolder.getContextData().setExclusionStrategys(new ArrayList<>(
					Arrays.asList(new FieldExclusionStrategy(null, new ArrayList<>(Arrays.asList("rmi", "omi"))))));

			response.setId(hInfo.getId());
			response.setSearchQuery(searchQuery);
			response.setHotel(hInfo);
			updateForRetry(hInfo, searchQuery);
		} catch (CustomGeneralException e) {
			log.info("Unable to fetch hotel detail for searchQuery {}, hInfo {} ", searchQuery, hInfo, e);
		} catch (Exception e) {
			log.error("Unable to fetch hotel detail for searchQuery {}, hInfo {} ", searchQuery, hInfo, e);
		} finally {
			if (response.getHotel() == null) {
				throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
			}
			SystemContextHolder.getContextData().getReqIds().add(searchId);
		}
	}

	private void fetchHotelDetails(HotelSearchQuery searchQuery, HotelInfo hInfo, ContextData contextData)
			throws Exception {

		log.info("Fetching hotel details from supplier for search id {} and hotel id {}", searchQuery.getSearchId(),
				hInfo.getId());
		Map<String, List<Option>> supplierOptionMap =
				hInfo.getOptions().stream().collect(Collectors.groupingBy(o -> o.getMiscInfo().getSupplierId()));
		Map<String, Option> optionIdOptionMap = getMap(hInfo);
		if (!searchQuery.isSearchCompleted()) {
			fetchDetailFromFirstSupplier(hInfo, searchQuery, supplierOptionMap, optionIdOptionMap);
			searchQuery.setSourceId(hInfo.getOptions().get(0).getMiscInfo().getSourceId());
			searchQuery.setMiscInfo(HotelSearchQueryMiscInfo.builder()
					.supplierId(hInfo.getOptions().get(0).getMiscInfo().getSupplierId()).build());
			processingManager.updateStaticData(hInfo, searchQuery, OperationType.DETAIL_SEARCH);
		} else {
			if (noOptionDetailHit(hInfo)) {
				/*
				 * First case after search completion
				 */

				fetchDetailFromFirstSupplier(hInfo, searchQuery, supplierOptionMap, optionIdOptionMap);
			} else {
				fetchDetailFromAllSuppliers(hInfo, searchQuery, supplierOptionMap, optionIdOptionMap);
			}
			cacheHandler.persistHotelInCache(hInfo, searchQuery);
		}
		log.info("Fetched hotel details from supplier for search id {} and hotel id {}", searchQuery.getSearchId(),
				hInfo.getId());
	}

	private Map<String, Option> getMap(HotelInfo hInfo) {

		Map<String, Option> map = new HashMap<>();
		for (Option option : hInfo.getOptions()) {
			map.put(option.getId(), option);
		}
		return map;
	}

	private void fetchDetailFromFirstSupplier(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierOptionMap, Map<String, Option> map) {

		HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
		copyHInfo.setOptions(supplierOptionMap.get(hInfo.getOptions().get(0).getMiscInfo().getSupplierId()));
		HotelSearchQuery copyQuery = getHotelSearchQuery(searchQuery, copyHInfo);
		fetchHotelDetailForOneSupplier(copyQuery, copyHInfo);
		mergeOptions(hInfo, copyHInfo, copyQuery, map);
	}

	private void fetchDetailFromAllSuppliers(HotelInfo hInfo, HotelSearchQuery searchQuery,
			Map<String, List<Option>> supplierOptionMap, Map<String, Option> map) throws Exception {

		List<Future<?>> futures = new ArrayList<>();
		for (String supplier : supplierOptionMap.keySet()) {
			Future<?> future = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
				HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
				copyHInfo.setOptions(supplierOptionMap.get(supplier));
				HotelSearchQuery copyQuery = getHotelSearchQuery(searchQuery, copyHInfo);
				ContextData copyContextData = contextData.deepCopy();
				SystemContextHolder.setContextData(copyContextData);
				fetchHotelDetailForOneSupplier(copyQuery, copyHInfo);
				mergeOptions(hInfo, copyHInfo, copyQuery, map);
			});
			futures.add(future);
		}
		for (Future<?> future : futures) {
			future.get();
		}

	}

	private HotelSearchQuery getHotelSearchQuery(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		HotelSearchQuery copyQuery = new GsonMapper<>(searchQuery, HotelSearchQuery.class).convert();
		copyQuery.setSourceId(hInfo.getOptions().get(0).getMiscInfo().getSourceId());
		copyQuery.setMiscInfo(HotelSearchQueryMiscInfo.builder()
				.supplierId(hInfo.getOptions().get(0).getMiscInfo().getSupplierId()).build());
		return copyQuery;
	}

	private void fetchHotelDetailForOneSupplier(HotelSearchQuery searchQuery, HotelInfo hInfo) {

		HotelSearchQuery copyQuery = new GsonMapper<>(searchQuery, HotelSearchQuery.class).convert();
		copyQuery.setSourceId(hInfo.getOptions().get(0).getMiscInfo().getSourceId());
		try {
			detailManager.fetchHotelDetails(copyQuery, hInfo, contextData);
		} catch (Exception e) {
			log.error("Error while fetching hotel detail for searchId {} , supplier {}", searchQuery.getSearchId(),
					hInfo.getOptions().get(0).getMiscInfo().getSupplierId(), e);
		}
	}

	private boolean noOptionDetailHit(HotelInfo hInfo) {

		boolean noOptionDetailHit = true;
		for (Option option : hInfo.getOptions()) {
			if (option.getMiscInfo().getIsDetailHit()) {
				noOptionDetailHit = false;
				break;
			}
		}
		return noOptionDetailHit;
	}


	@SuppressWarnings("unchecked")
	private void mergeOptions(HotelInfo hInfo, HotelInfo copyHInfo, HotelSearchQuery searchQuery,
			Map<String, Option> map) {

		removeExtraOptions(hInfo, map, searchQuery);
		if (!CollectionUtils.isEmpty(copyHInfo.getImages())) {
			if (CollectionUtils.isEmpty(hInfo.getImages()) || (!CollectionUtils.isEmpty(hInfo.getImages())
					&& copyHInfo.getImages().size() > hInfo.getImages().size())) {
				hInfo.setImages(copyHInfo.getImages());
			}
		}

		if (!CollectionUtils.isEmpty(copyHInfo.getFacilities())) {
			if (CollectionUtils.isEmpty(hInfo.getFacilities()) || (!CollectionUtils.isEmpty(hInfo.getFacilities())
					&& copyHInfo.getFacilities().size() > hInfo.getFacilities().size())) {
				hInfo.setFacilities(copyHInfo.getFacilities());
			}
		}

		if (!StringUtils.isEmpty(copyHInfo.getDescription())) {
			hInfo.setDescription(copyHInfo.getDescription());
		}
		List<Option> newOptions = new ArrayList<>();
		for (Option option : copyHInfo.getOptions()) {
			if (!map.containsKey(option.getId()))
				newOptions.add(option);
		}
		hInfo.getOptions().addAll(newOptions);
		removeDummyOption(hInfo);
		processingManager.sortOptions(hInfo);
		processingManager.mergeSortedOptions(hInfo, searchQuery);
	}

	public void removeDummyOption(HotelInfo hInfo) {

		for (Iterator<Option> optionIterator = hInfo.getOptions().iterator(); optionIterator.hasNext();) {
			Option option = optionIterator.next();
			if (option.getRoomInfos().get(0).getRoomCategory().equals("Dummy Room Category")) {
				optionIterator.remove();
			}
		}
	}

	public void removeExtraOptions(HotelInfo hInfo, Map<String, Option> map, HotelSearchQuery searchQuery) {

		for (Iterator<Option> optionIterator = hInfo.getOptions().iterator(); optionIterator.hasNext();) {
			Option option = optionIterator.next();
			String optionSupplierId = option.getMiscInfo().getSupplierId();
			if (BooleanUtils.isTrue(option.getMiscInfo().getIsNotRequiredOnDetail())
					&& optionSupplierId.equals(searchQuery.getMiscInfo().getSupplierId())) {
				optionIterator.remove();
				map.remove(option.getId());
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {

		SystemContextHolder.getContextData().setExclusionStrategys(
				new ArrayList<>(
						Arrays.asList(new FieldExclusionStrategy(null, Arrays.asList("cnp", "links")))));
	}

	private void updateForRetry(HotelInfo hInfo, HotelSearchQuery searchQuery) {

		LocalDateTime searchStartAt = cacheHandler.getSearchStartAt(searchQuery.getSearchId());
		LocalDateTime currentTime = LocalDateTime.now();
		if (!searchQuery.isSearchCompleted() && searchStartAt.plusSeconds(30).isAfter(currentTime)) {
			response.setRetryInSecond(5);
		} else {
			for (Option option : hInfo.getOptions()) {
				if (!option.getMiscInfo().getIsDetailHit()) {
					response.setRetryInSecond(5);
					break;
				}
			}
		}
	}

	private HotelInfo getHotelDetails(String hotelId) throws InterruptedException {

		HotelInfo hInfo = cacheHandler.getCachedHotelById(hotelId);
		if (ObjectUtils.isEmpty(hInfo)) {
			HotelGeneralPurposeOutput configuratorInfo =
					HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
			int attemptCount = ObjectUtils.isEmpty(configuratorInfo.getDetailRetryCount()) ? 3
					: configuratorInfo.getDetailRetryCount();
			while (ObjectUtils.isEmpty(hInfo) && attemptCount-- > 0) {
				Thread.sleep(2 * 1000);
				hInfo = cacheHandler.getCachedHotelById(hotelId);
				log.debug(
						"Retrying to fetch hotel details from cache for hotel id {}. Attempts remaining : {}, Found : {}",
						hotelId, attemptCount, !ObjectUtils.isEmpty(hInfo));
			}
		}
		return hInfo;
	}

}
