package com.tgs.services.hms.servicehandler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelNationalityInfo;
import com.tgs.services.hms.datamodel.HotelNationalitySupplierMapping;
import com.tgs.services.hms.dbmodel.DbHotelNationalitySupplierMapping;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.jparepository.HotelNationalityInfoService;
import com.tgs.services.hms.restmodel.HotelNationalityStaticDataRequest;

@Service
public class HotelNationalityStaticDataHandler extends ServiceHandler<HotelNationalityStaticDataRequest, BaseResponse>{

	@Autowired
	HotelNationalityInfoService service;
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		
		HotelNationalitySupplierMapping mappingInfo = request.getNationalityMapping();
		DbHotelNationalitySupplierMapping dbMappingInfo = new DbHotelNationalitySupplierMapping().from(mappingInfo);
		service.save(dbMappingInfo);
		String supplier = mappingInfo.getSupplier();
		
		
		/*
		 * Need to move this code to staticInitializers
		 */
		List<HotelNationalityInfo> data = mappingInfo.getData();
		for(HotelNationalityInfo info : data) {
			cacheHandler.storeSupplierNationalityMapping(supplier, info);
		}
		
		
		
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
