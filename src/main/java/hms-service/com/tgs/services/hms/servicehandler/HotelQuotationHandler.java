package com.tgs.services.hms.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.quotation.HotelQuotation;
import com.tgs.services.hms.datamodel.quotation.HotelQuotationAddResult;
import com.tgs.services.hms.datamodel.quotation.HotelQuotationSearchResult;
import com.tgs.services.hms.dbmodel.DbHotelQuotation;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelCancellationPolicySearch;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.jparepository.HotelQuotationService;
import com.tgs.services.hms.mapper.HotelInfoToHotelQuotationMapper;
import com.tgs.services.hms.mapper.HotelQuotationToAnalyticsHotelQuotationMapper;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationAddRequest;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationAddResponse;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationDeleteRequest;
import com.tgs.services.hms.restmodel.quotation.HotelQuotationSearchRequest;
import com.tgs.services.hms.utils.HotelUtils;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelQuotationHandler extends ServiceHandler<HotelQuotationAddRequest, HotelQuotationAddResponse> {

	@Autowired
	HotelQuotationService hotelQuotationService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelCancellationPolicySearch cancellationPolicy;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void process() throws Exception {
		HotelInfoToHotelQuotationMapper hotelInfoToHotelQuotationMapper = null;
		HotelQuotationAddResponse hotelQuotationAddResponse = new HotelQuotationAddResponse();
		DbHotelQuotation dbHotelQuotation = null;
		try {
			if (request.getQuotationId() != null) {
				dbHotelQuotation = fetchQuotationFromDB(request.getQuotationId());
				if (!isOptionIdAlreadyExist(dbHotelQuotation, request.getOptionId())) {
					if (dbHotelQuotation != null) {
						hotelInfoToHotelQuotationMapper = getHotelQuotations(request);
						dbHotelQuotation.getQuotationInfo().add(hotelInfoToHotelQuotationMapper.convert());
						dbHotelQuotation = hotelQuotationService.saveQuotation(dbHotelQuotation);
					} else {
						throw new CustomGeneralException(SystemError.INVALID_QUOTATION_ID);
					}
				}
			} else {
				List<HotelQuotation> hotelQuotations = new ArrayList<>();
				hotelInfoToHotelQuotationMapper = getHotelQuotations(request);
				hotelQuotations.add(hotelInfoToHotelQuotationMapper.convert());
				HotelQuotationAddResult hotelQuotationAddResult =
						HotelQuotationAddResult.builder().quotationInfo(hotelQuotations)
								.name(request.getQuotationName()).userId(contextData.getUser().getUserId()).build();
				hotelQuotationAddResult.setCreatedOn(LocalDateTime.now());
				dbHotelQuotation =
						hotelQuotationService.saveQuotation(new DbHotelQuotation().from(hotelQuotationAddResult));
			}
			hotelQuotationAddResponse.setHotelQuotationAddResult(dbHotelQuotation.toDomain());
			response = hotelQuotationAddResponse;
		} catch (CustomGeneralException e) {
			log.error("Failed to add quotation {}", request.getQuotationName(), e);
			contextData.getErrorMessages().add("Failed to add quotation because hotel not found");
			throw new CustomGeneralException(e.getError(), SystemError.HOTEL_NOT_FOUND.getMessage());
		} catch (Exception e) {
			log.error("Failed to add quotation {}", request.getQuotationName(), e);
			contextData.getErrorMessages().add("Failed to add quotation because of service exception");
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		} finally {
			sendQuotationDataToAnalytics(hotelInfoToHotelQuotationMapper.getHotelInfo(),
					hotelInfoToHotelQuotationMapper.getSearchQuery());
		}
	}

	private void sendQuotationDataToAnalytics(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
				new ArrayList<>(Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
						.time(System.currentTimeMillis()).build())));

		HotelQuotationToAnalyticsHotelQuotationMapper queryMapper = HotelQuotationToAnalyticsHotelQuotationMapper
				.builder().user(SystemContextHolder.getContextData().getUser())
				.contextData(SystemContextHolder.getContextData()).hInfo(hInfo).searchQuery(searchQuery)
				.quotationName(request.getQuotationName()).optionId(request.getOptionId()).build();
		analyticsHelper.storeData(queryMapper);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	private boolean isOptionIdAlreadyExist(DbHotelQuotation quotations, String optionId) {
		HotelQuotation opt = quotations.getQuotationInfo().stream()
				.filter(quotation -> optionId.equals(quotation.getOptionId())).findAny().orElse(null);
		if (opt != null) {
			return true;
		}
		return false;
	}

	public List<HotelQuotationSearchResult> searchQuotation(HotelQuotationSearchRequest searchRequest,
			ContextData contextData) {
		Boolean shouldShowAll = searchRequest.getShowAll();
		List<HotelQuotationSearchResult> hotelQuotationSearchResultList = new ArrayList<>();
		HotelQuotationSearchResult hotelQuotationSearchResult = new HotelQuotationSearchResult();
		if (shouldShowAll != null && searchRequest.getShowAll()) {
			List<DbHotelQuotation> dbHotelQuotationList =
					hotelQuotationService.fetchAllUserQuotations(contextData.getUser().getUserId());
			for (DbHotelQuotation hotelQuotation : dbHotelQuotationList) {
				HotelQuotationSearchResult quotationResult = new HotelQuotationSearchResult();
				quotationResult.setQuotationId(hotelQuotation.getId());
				quotationResult.setQuotationName(hotelQuotation.getName());
				quotationResult.setCreatedOn(hotelQuotation.getCreatedOn());
				quotationResult.setHotelQuotationList(hotelQuotation.getQuotationInfo());
				hotelQuotationSearchResultList.add(quotationResult);
			}
		} else {
			DbHotelQuotation dbHotelQuotation =
					hotelQuotationService.fetchQuotationFromId(searchRequest.getQuotationId());
			hotelQuotationSearchResult.setQuotationId(dbHotelQuotation.getId());
			hotelQuotationSearchResult.setQuotationName(dbHotelQuotation.getName());
			hotelQuotationSearchResult.setCreatedOn(dbHotelQuotation.getCreatedOn());
			hotelQuotationSearchResult.setHotelQuotationList(dbHotelQuotation.getQuotationInfo());
			hotelQuotationSearchResultList.add(hotelQuotationSearchResult);
		}
		return hotelQuotationSearchResultList;
	}

	public BaseResponse deleteQuotation(HotelQuotationDeleteRequest deleteRequest, ContextData contextData) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			if (deleteRequest.getQuotationId() != null) {
				if (deleteRequest.getOptionId() == null)
					hotelQuotationService.deleteQuotationFromId(deleteRequest.getQuotationId());
				else if (deleteRequest.getOptionId() != null) {
					DbHotelQuotation dbHotelQuotation =
							hotelQuotationService.fetchQuotationFromId(deleteRequest.getQuotationId());

					dbHotelQuotation.setQuotationInfo(dbHotelQuotation.getQuotationInfo().stream()
							.filter(quotation -> !quotation.getOptionId().equals(deleteRequest.getOptionId()))
							.collect(Collectors.toList()));
					dbHotelQuotation = hotelQuotationService.saveQuotation(dbHotelQuotation);
				}
			} else if (deleteRequest.getDeleteAll()) {
				List<DbHotelQuotation> dbHotelQuotations =
						hotelQuotationService.fetchAllUserQuotations(contextData.getUser().getUserId());
				hotelQuotationService.deleteAllAgentsQuotations(dbHotelQuotations);
			}
		} catch (Exception e) {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	private DbHotelQuotation fetchQuotationFromDB(Long quotationId) {
		return hotelQuotationService.fetchQuotationFromId(quotationId);
	}

	private HotelInfoToHotelQuotationMapper getHotelQuotations(HotelQuotationAddRequest addRequest) {
		try {
			HotelInfo hInfo = cacheHandler.getCachedHotelById(addRequest.getHotelId());

			String searchId = HotelUtils.getSearchId(addRequest.getHotelId());
			HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			if (hInfo.getOptions().get(0).getCancellationPolicy() == null) {
				cancellationPolicy.getCancellationPolicy(searchQuery, hInfo, addRequest.getOptionId(), searchId);
			}
			return HotelInfoToHotelQuotationMapper.builder().hotelInfo(hInfo).searchQuery(searchQuery)
					.optionId(addRequest.getOptionId()).build();
		} catch (Exception e) {
			log.error("Error while searching for hotel search request  {}", addRequest, e);
			throw new CustomGeneralException(SystemError.HOTEL_NOT_FOUND);
		}
	}

}
