package com.tgs.services.hms.servicehandler;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelBookingConditions;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.OperationType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSoldOut;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelCancellationPolicySearch;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelPriceValidation;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.helper.analytics.HotelAnalyticsHelper;
import com.tgs.services.hms.manager.HotelDetailManager;
import com.tgs.services.hms.manager.HotelSearchManager;
import com.tgs.services.hms.manager.HotelSearchResultProcessingManager;
import com.tgs.services.hms.mapper.HotelReviewToAnalyticsHotelReviewMapper;
import com.tgs.services.hms.restmodel.HotelReviewRequest;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.sources.tbo.TravelBoutiqueUtil;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelReviewHandler extends ServiceHandler<HotelReviewRequest, HotelReviewResponse> {

	@Autowired
	HotelDetailManager detailManager;

	@Autowired
	HotelSearchManager searchManager;

	@Autowired
	HMSCachingServiceCommunicator cachingService;

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelCancellationPolicySearch search;

	@Autowired
	HotelAnalyticsHelper analyticsHelper;

	@Autowired
	HotelSearchResultProcessingManager processingManager;

	@Autowired
	HotelPriceValidation priceValidation;


	@Override
	public void beforeProcess() throws Exception {
		String bookingId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.HOTEL).build());
		HotelInfo hInfo = null;
		HotelSearchQuery searchQuery = null;
		Alert fareChange = null;
		try {
			hInfo = cacheHandler.getCachedHotelById(request.getHotelId());
			String searchId = HotelUtils.getSearchId(request.getHotelId());
			searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			Option oldOption = HotelUtils.filterOptionById(hInfo, request.getOptionId());
			searchQuery.setSourceId(oldOption.getMiscInfo().getSourceId());
			searchQuery.setMiscInfo(
					HotelSearchQueryMiscInfo.builder().supplierId(oldOption.getMiscInfo().getSupplierId()).build());
			oldOption.setCancellationPolicy(null);
			boolean isToSendFareDifference = isToShowFareDiff(searchQuery);

			if (hInfo != null && hInfo.getMiscInfo().getSearchKeyExpiryTime() != null
					&& hInfo.getMiscInfo().getSearchKeyExpiryTime().isBefore(LocalDateTime.now().plusMinutes(6))) {
				HotelSearchManager.setSearchId(searchQuery);
				log.info(
						"Doing new search  for source {} because  key will expire in 10 minutes on review page for hotel {} "
								+ " searchId {}",
						oldOption.getMiscInfo().getSourceId(), hInfo.getName(), searchQuery.getSearchId());

				hInfo.setOptions(new ArrayList<>(Arrays.asList(oldOption)));
				HotelInfo newHInfo = searchManager.doSearchAgainWithSameSuppliers(searchQuery, hInfo, contextData);
				fetchHotelDetailsIfRequired(newHInfo, searchQuery);
				Option filteredOption = HotelUtils.filterOptionByPriceAndRoomCategory(newHInfo, oldOption);

				if (filteredOption == null) {
					log.error("Requested Option not available now for hotel {}", hInfo);
					contextData.getErrorMessages().add("Option Now Available In New Search After Filtering");
					throw new CustomGeneralException(SystemError.OPTION_NO_LONGER_AVAILABLE,
							StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
				}

				detailManager.fetchHotelDetails(searchQuery, newHInfo, contextData);
				newHInfo.setOptions(new ArrayList<>(Arrays.asList(filteredOption)));
				search.getCancellationPolicy(searchQuery, newHInfo, filteredOption.getId(), bookingId);
				fareChange =
						priceValidation.updatePriceIfChanged(searchQuery, newHInfo, isToSendFareDifference, bookingId);
				hInfo = newHInfo;
			} else {
				search.getCancellationPolicy(searchQuery, hInfo, oldOption.getId(), bookingId);
				hInfo.setOptions(new ArrayList<>(Arrays.asList(oldOption)));
				fareChange =
						priceValidation.updatePriceIfChanged(searchQuery, hInfo, isToSendFareDifference, bookingId);
			}

			CancellationMiscInfo mInfo = null;
			if (hInfo.getOptions().get(0).getCancellationPolicy() == null
					|| ((mInfo = hInfo.getOptions().get(0).getCancellationPolicy().getMiscInfo()) != null
							&& (BooleanUtils.isFalse(mInfo.getIsBookingAllowed())
									|| BooleanUtils.isTrue(mInfo.getIsSoldOut()))
							&& !hInfo.getOptions().get(0).getIsOptionOnRequest())) {
				log.error("Request room is no longer available for hotel {}", hInfo);
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT,
						StringUtils.join(" You can use booking Id ", bookingId, " for reference"));
			}

			SystemContextHolder.getContextData().setExclusionStrategys(new ArrayList<>(
					Arrays.asList(new FieldExclusionStrategy(null, new ArrayList<>(Arrays.asList("rmi"))))));

			// setBookingNotes(hInfo);
			processingManager.updateStaticData(hInfo, searchQuery, OperationType.REVIEW);
			updateDeadlineDateTimeForOnRequstBooking(hInfo);
			updateTermsAndConditionsIfApplicable(hInfo, searchQuery);
			updateFareComponents(hInfo);
			response.setBookingId(bookingId);
			setBookingConditions(response, hInfo);

			response.setHInfo(hInfo);
			response.setQuery(searchQuery);
			processHInfo(hInfo);
			storeInCache();
			if (!ObjectUtils.isEmpty(fareChange)) {
				response.addAlert(fareChange);
				response.setPriceChanged(true);
			}
		} catch (CustomGeneralException e) {
			String message = StringUtils.join("Failed to review hotelId", request.getHotelId(), ", optionId ",
					request.getOptionId(), ", bookingId ", bookingId);
			RoomSoldOut soldOut = RoomSoldOut.builder().type(AlertType.SOLDOUT.name()).message(message).build();
			response.addAlert(soldOut);
			response.setStatus(new Status(HttpStatusCode.HTTP_200));
			response.setQuery(searchQuery);
			log.error("Failed to review hotelId {}, optionId {},error msg is {}", request.getHotelId(),
					request.getOptionId(), contextData.getErrorMessages(), e);
			contextData.getErrorMessages().add("Failed to review hotel because hotel is sold out");
		} catch (Exception e) {
			String message = StringUtils.join("Unable to review hotel for hotelId ", request.getHotelId(), " optionId ",
					request.getOptionId(), " hInfo ", hInfo);
			RoomSoldOut soldOut =
					RoomSoldOut.builder().type(SystemError.SERVICE_EXCEPTION.getMessage()).message(message).build();
			contextData.getErrorMessages().add("Failed to review hotel due to service exception");
			log.error("Failed to review hotelId {}, optionId {},due to service exception", request.getHotelId(),
					request.getOptionId(), ", bookingId ", bookingId, e);
			response.addAlert(soldOut);
			response.addError(new ErrorDetail(SystemError.FAILED_TO_REVIEW));
		} finally {
			contextData.getReqIds().add(bookingId);
			storeSearchLogs(searchQuery, bookingId);
			sendReviewDataToAnalytics(hInfo, searchQuery, bookingId);
		}
	}

	/**
	 * This is primarily used for testing purpose. In order to test fare jump case we can define value in DB.
	 *
	 * @param searchQuery
	 * @return
	 */
	public boolean isToShowFareDiff(HotelSearchQuery searchQuery) {
		HotelGeneralPurposeOutput rule = HotelConfiguratorHelper.getHotelConfigRuleOutput(
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery),
				HotelConfiguratorRuleType.GNPURPOSE);
		if (rule != null) {
			return BooleanUtils.toBoolean(rule.getShowFareDiff());
		}
		return false;
	}

	private void sendReviewDataToAnalytics(HotelInfo hInfo, HotelSearchQuery searchQuery, String bookingId) {
		contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
				new ArrayList<>(Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
						.time(System.currentTimeMillis()).build())));
		HotelReviewToAnalyticsHotelReviewMapper queryMapper =
				HotelReviewToAnalyticsHotelReviewMapper.builder().user(SystemContextHolder.getContextData().getUser())
						.contextData(contextData).hInfo(hInfo).searchQuery(searchQuery).bookingId(bookingId).build();
		analyticsHelper.storeData(queryMapper);
	}

	private void updateFareComponents(HotelInfo hInfo) {
		hInfo.getOptions().get(0).getRoomInfos().stream().forEach(roomInfo -> {
			BaseHotelUtils.updatePerNightTotalFareComponents(roomInfo);
		});
	}

	private void updateTermsAndConditionsIfApplicable(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		HotelSourceConfigOutput rule = HotelConfiguratorHelper.getHotelConfigRuleOutput(
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery),
				HotelConfiguratorRuleType.SOURCECONFIG);
		if (Objects.isNull(rule))
			return;
		hInfo.setTermsAndConditions(rule.getTermsAndConditions());
	}

	private void updateDeadlineDateTimeForOnRequstBooking(HotelInfo hInfo) {
		if (hInfo.getOptions().get(0).getIsOptionOnRequest()
				&& hInfo.getOptions().get(0).getDeadlineDateTime().isBefore(LocalDateTime.now())) {
			hInfo.getOptions().get(0).setDeadlineDateTime(LocalDateTime.now().plusHours(6));
		}
	}

	/*
	 * private void setBookingNotes(HotelInfo hInfo) {
	 * 
	 * String bookingNotes = hInfo.getOptions().get(0).getInstructionFromType(InstructionType.BOOKINGNOTES);
	 * hInfo.getOptions().get(0).setNotes(hInfo.getOptions().get(0).getCancellationPolicy().getNotes()); }
	 */

	private void setBookingConditions(HotelReviewResponse response, HotelInfo hInfo) {
		HotelBookingConditions cond = HotelBookingConditions.builder().build();

		boolean isBlockingAllowed = false;

		if (BooleanUtils.isTrue(hInfo.getOptions().get(0).getCancellationPolicy().getIsFullRefundAllowed())) {
			if (hInfo.getOptions().get(0).getDeadlineDateTime().isAfter(LocalDateTime.now().plusHours(48))) {
				isBlockingAllowed = true;
			}
		}
		cond.setIsBlockingAllowed(isBlockingAllowed);
		cond.setSessionTimeInSecond(getSessionTimeInSeconds(hInfo));
		response.setConditions(cond);
	}

	private void processHInfo(HotelInfo hInfo) {
		String searchId = HotelUtils.getSearchId(request.getHotelId());
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
		for (RoomInfo roomInfo : hInfo.getOptions().get(0).getRoomInfos()) {

			/*
			 * Frontend is pickingup CheckInDate & CheckOutDate from 0th room
			 */
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
		}
	}

	private int getSessionTimeInSeconds(HotelInfo hInfo) {

		int remainingTime =
				(int) Duration.between(LocalDateTime.now(), hInfo.getMiscInfo().getSearchKeyExpiryTime()).getSeconds();
		return remainingTime;
	}

	private void storeSearchLogs(HotelSearchQuery searchQuery, String bookingId) {
		if (searchQuery != null) {
			SystemContextHolder.getContextData().addReqIds(bookingId, searchQuery.getSearchId());
		}
	}

	public void storeInCache() {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(response.getBookingId())
				.binValues(ImmutableMap.of(BinName.HOTELINFO.getName(), response)).compress(false).build();
		cachingService.store(metaInfo);
	}

	@Override
	public void afterProcess() throws Exception {
		SystemContextHolder.getContextData().setExclusionStrategys(
				new ArrayList<>(Arrays.asList(new FieldExclusionStrategy(null, Arrays.asList("links")))));
	}

	private void fetchHotelDetailsIfRequired(HotelInfo hInfo, HotelSearchQuery searchQuery) {
		HotelSourceConfigOutput sourceConfig = HotelUtils.getHotelSourceConfigOutput(searchQuery);
		if (BooleanUtils.isTrue(sourceConfig.getIsHotelDetailsRequiredAtReview())) {
			searchManager.fetchHotelDetails(searchQuery, hInfo);
		}
	}

	@Override
	public void process() throws Exception {
		// TODO Auto-generated method stub

	}
}
