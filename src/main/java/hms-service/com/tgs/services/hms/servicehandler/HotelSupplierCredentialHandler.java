package com.tgs.services.hms.servicehandler;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.HotelSupplierInfoFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.FieldTransform;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.supplier.CredentialsInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSourceInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.jparepository.HotelSupplierInfoService;
import com.tgs.services.hms.restmodel.HotelSourceInfoResponse;
import com.tgs.services.hms.restmodel.HotelSupplierInfoResponse;
import com.tgs.utils.common.UpdateMaskedField;

@Service
public class HotelSupplierCredentialHandler extends ServiceHandler<HotelSupplierInfo, HotelSupplierInfoResponse> {

	@Autowired
	HotelSupplierInfoService supplierService;

	@Override
	public void beforeProcess() throws Exception {
		DbHotelSupplierInfo supplierDbModel = null;
		if (request.getId() != null) {
			supplierDbModel = supplierService.findById(request.getId());
			DbHotelSupplierInfo newSupplierDbModel = new DbHotelSupplierInfo().from(request);
			updateHotelSupplierInfo(supplierDbModel, newSupplierDbModel);
		} else {
			supplierDbModel = new DbHotelSupplierInfo().from(request);
		}
		try {
			supplierDbModel = supplierService.save(supplierDbModel);
			request = supplierDbModel.toDomain();
		} catch (DataIntegrityViolationException e) {
			throw new CustomGeneralException(SystemError.DUPLICATE_SUPPLIER_NAME);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		FieldTransform.mask(request);
		response.getSupplierInfos().add(request);
	}

	public HotelSupplierInfoResponse getSupplierInfo(HotelSupplierInfoFilter configurationFilter) {
		HotelSupplierInfoResponse supplierInfoResponse = new HotelSupplierInfoResponse();
		supplierService.findAll(configurationFilter).forEach(supplier -> {
			supplierInfoResponse.getSupplierInfos().add(supplier.toDomain());
		});
		if (CollectionUtils.isNotEmpty(supplierInfoResponse.getSupplierInfos())) {
			supplierInfoResponse.getSupplierInfos().forEach(supplierInfo -> {
				FieldTransform.mask(supplierInfo);
			});
		}
		return supplierInfoResponse;
	}

	public HotelSourceInfoResponse getSourceInfo() {
		HotelSourceInfoResponse sourceInfoResponse = new HotelSourceInfoResponse();
		for (HotelSourceType sourceType : HotelSourceType.values()) {
			sourceInfoResponse.getSourceInfos()
					.add(HotelSourceInfo.builder().id(sourceType.getSourceId()).name(sourceType.name()).build());
		}
		return sourceInfoResponse;
	}

	public BaseResponse updateStatusSupplierInfo(Long id, boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbHotelSupplierInfo supplierInfo = supplierService.findById(id);
		if (Objects.nonNull(supplierInfo)) {
			supplierInfo.setEnabled(status);
			supplierService.save(supplierInfo);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	private static void updateHotelSupplierInfo(DbHotelSupplierInfo oldSupplierInfo,
			DbHotelSupplierInfo newSupplierInfo) throws Exception {

		List<CredentialsInfo> oldExtranetCredentialsInfoBeforeMap =
				Objects.isNull(oldSupplierInfo.getCredentialInfo()) ? null
						: oldSupplierInfo.getCredentialInfo().getExtranetCredentials();

		String oldSupplierPassword = ObjectUtils.isEmpty(oldSupplierInfo.getCredentialInfo()) ? null
				: oldSupplierInfo.getCredentialInfo().getPassword();

		oldSupplierInfo = new GsonMapper<>(newSupplierInfo, oldSupplierInfo, DbHotelSupplierInfo.class).convert();
		HotelSupplierCredential oldSupplierCredential = oldSupplierInfo.getCredentialInfo();
		HotelSupplierCredential newSupplierCredential = newSupplierInfo.getCredentialInfo();

		if (Objects.nonNull(oldSupplierCredential) && Objects.nonNull(newSupplierCredential)) {

			if (Objects.isNull(newSupplierCredential.getUserName())
					&& Objects.nonNull(oldSupplierCredential.getUserName())) {
				oldSupplierCredential.setUserName(null);
			}

			if (Objects.isNull(newSupplierCredential.getPassword()) && !Objects.nonNull(oldSupplierPassword)) {
				oldSupplierCredential.setPassword(null);
			} else if (Objects.nonNull(oldSupplierPassword)
					&& UpdateMaskedField.isAlreadyMasked(newSupplierCredential.getPassword(), '*')) {
				oldSupplierCredential.setPassword(oldSupplierPassword);
			}

			if (Objects.isNull(newSupplierCredential.getIsTestCredential())
					&& Objects.nonNull(oldSupplierCredential.getUserName())) {
				oldSupplierCredential.setIsTestCredential(null);
			}
			if (Objects.isNull(newSupplierCredential.getCurrency())
					&& Objects.nonNull(oldSupplierCredential.getCurrency())) {
				oldSupplierCredential.setCurrency(null);
			}
			if (Objects.isNull(newSupplierCredential.getTimeZone())
					&& Objects.nonNull(oldSupplierCredential.getTimeZone())) {
				oldSupplierCredential.setTimeZone(null);
			}
			if (Objects.isNull(newSupplierCredential.getRemarks())
					&& Objects.nonNull(oldSupplierCredential.getRemarks())) {
				oldSupplierCredential.setRemarks(null);
			}

			Address newAddress = newSupplierCredential.getAddress();
			Address oldAddress = oldSupplierCredential.getAddress();

			if (Objects.nonNull(oldAddress) && Objects.nonNull(newAddress)) {

				if (Objects.isNull(newAddress.getAddress()) && Objects.nonNull(oldAddress.getAddress())) {
					oldAddress.setAddress(null);
				}

				City newCity = newAddress.getCity();
				City oldCity = oldAddress.getCity();

				if (Objects.nonNull(oldCity) && Objects.nonNull(newCity)) {

					if (Objects.isNull(newCity.getName()) && Objects.nonNull(oldCity.getName())) {
						oldCity.setName(null);
					}
				}

				Country newCountry = newAddress.getCountry();
				Country oldCountry = oldAddress.getCountry();

				if (Objects.nonNull(oldCountry) && Objects.nonNull(newCountry)) {

					if (Objects.isNull(newCountry.getName()) && Objects.nonNull(oldCountry.getName())) {
						oldCountry.setName(null);
					}
				}
			}

			List<CredentialsInfo> newExtranetCredentials = newSupplierCredential.getExtranetCredentials();
			List<CredentialsInfo> oldExtranetCredentials = oldExtranetCredentialsInfoBeforeMap;

			if (CollectionUtils.isNotEmpty(oldExtranetCredentials)
					&& CollectionUtils.isNotEmpty(newExtranetCredentials)) {

				Map<String, List<CredentialsInfo>> newExtranetCredentialsMap =
						newExtranetCredentials.stream().collect(Collectors.groupingBy(CredentialsInfo::getId));

				Map<String, List<CredentialsInfo>> oldExtranetCredentialsMap =
						oldExtranetCredentials.stream().collect(Collectors.groupingBy(CredentialsInfo::getId));

				newExtranetCredentialsMap.forEach((id, newCredentialInfoList) -> {
					CredentialsInfo oldCredentialInfo = oldExtranetCredentialsMap.get(id).get(0);
					CredentialsInfo newCredentialInfo = newCredentialInfoList.get(0);

					if (Objects.nonNull(oldCredentialInfo) && Objects.nonNull(newCredentialInfo)) {

						if (Objects.isNull(newCredentialInfo.getUsername())
								&& Objects.nonNull(oldCredentialInfo.getUsername())) {
							oldCredentialInfo.setUsername(null);
						} else if (Objects.nonNull(newCredentialInfo.getUsername())) {
							oldCredentialInfo.setUsername(newCredentialInfo.getUsername());
						}
						if (Objects.isNull(oldCredentialInfo.getPassword())
								|| !UpdateMaskedField.isAlreadyMasked(newCredentialInfo.getPassword(), '*')) {
							oldCredentialInfo.setPassword(newCredentialInfo.getPassword());
						}
					}
				});
				oldSupplierCredential.setExtranetCredentials(oldExtranetCredentials);
			} else if (CollectionUtils.isNotEmpty(newExtranetCredentials)) {
				oldSupplierCredential.setExtranetCredentials(newExtranetCredentials);
			} else if (CollectionUtils.isEmpty(newExtranetCredentials)) {
				oldSupplierCredential.setExtranetCredentials(null);
			}
		}
	}
}
