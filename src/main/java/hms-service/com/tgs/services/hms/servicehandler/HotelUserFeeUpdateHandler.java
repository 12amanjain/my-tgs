package com.tgs.services.hms.servicehandler;

import java.util.ArrayList;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.HotelUserFeeManager;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.hms.restmodel.HotelUserFeeUpdateRequest;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelUserFeeUpdateHandler extends ServiceHandler<HotelUserFeeUpdateRequest, BaseResponse> {


	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelUserFeeManager userFeeManager;

	@Autowired
	HotelOrderItemCommunicator itemCommunicator;

	@Autowired
	OrderServiceCommunicator orderCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {

		String id = request.getId();
		HotelFlowType flowType = request.getFlowType();
		HotelInfo hInfo = null;

		if (request.getValue() < 0) {
			throw new CustomGeneralException(SystemError.BAD_REQUEST, "User Fee must be non-negative");
		}

		try {
			switch (flowType) {

				case SEARCH:
					hInfo = cacheHandler.getCachedHotelById(id);
					updateMarkUpForSearch(hInfo);
					break;

				case DETAIL:
					hInfo = cacheHandler.getCachedHotelById(id);
					updateMarkUpForDetail(hInfo);
					break;

				case REVIEW:
					HotelReviewResponse reviewResponse = cacheHandler.getHotelReviewFromCache(id);
					updateMarkUpForReview(reviewResponse);
					break;

				case BOOKING_DETAIL:
					hInfo = itemCommunicator.getHotelInfo(id);
					updateMarkUpForBooking(hInfo, id);
					break;
			}

		} catch (Exception e) {
			log.error("Updating User Fee failed for id {}", id, e);
			throw e;
		}
	}


	private void updateMarkUpForSearch(HotelInfo hInfo) {

		updateUserFee(hInfo, request.getValue(), request.getFeeType());
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(HotelUtils.getSearchId(hInfo.getId()));
		cacheHandler.persistHotelInCache(hInfo, searchQuery);

	}

	private void updateMarkUpForDetail(HotelInfo hInfo) {

		HotelInfo copyHInfo = new GsonMapper<>(hInfo, HotelInfo.class).convert();
		Option filteredOption = HotelUtils.filterOptionById(hInfo, request.getOptionId());
		copyHInfo.setOptions(new ArrayList<>(Arrays.asList(filteredOption)));
		updateUserFee(copyHInfo, request.getValue(), request.getFeeType());
		copyHInfo.getOptions().get(0).getMiscInfo().setMarkupUpdatedByUser(true);
		int i = 0;
		for (Option option : hInfo.getOptions()) {
			if (option.getId().equals(filteredOption.getId())) {
				hInfo.getOptions().set(i, filteredOption);
				break;
			}
			i++;
		}
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(HotelUtils.getSearchId(hInfo.getId()));
		cacheHandler.persistHotelInCache(hInfo, searchQuery);
	}

	private void updateMarkUpForReview(HotelReviewResponse reviewResponse) {

		updateUserFee(reviewResponse.getHInfo(), request.getValue(), request.getFeeType());
		reviewResponse.getHInfo().getOptions().get(0).getMiscInfo().setMarkupUpdatedByUser(true);
		cacheHandler.cacheHotelReviewAgainstBookingId(reviewResponse);

	}

	private void updateMarkUpForBooking(HotelInfo hInfo, String bookingId) {

		updateUserFee(hInfo, request.getValue(), request.getFeeType());
		Order order = orderCommunicator.findByBookingId(bookingId);
		Double totalMarkUp = getTotalOptionMarkUp(hInfo);
		order.setMarkup(totalMarkUp);
		itemCommunicator.updateOrderItem(hInfo, order, null);
	}

	public double getTotalOptionMarkUp(HotelInfo hInfo) {

		Double markUp = 0.0;
		for (Option option : hInfo.getOptions()) {
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				markUp += roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF).get(HotelFareComponent.MU);
			}
		}
		return markUp;
	}

	private void updateUserFee(HotelInfo hInfo, Double markUpValue, String feeType) {
		userFeeManager.updateUserFeeForHotelList(hInfo, markUpValue, feeType);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
