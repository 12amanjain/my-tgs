package com.tgs.services.hms.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.HotelUserReviewManager;
import com.tgs.services.hms.restmodel.HotelUserReviewRequest;
import com.tgs.services.hms.restmodel.HotelUserReviewResponse;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelUserReviewHandler extends ServiceHandler<HotelUserReviewRequest, HotelUserReviewResponse> {

	
	@Autowired
	HotelUserReviewManager reviewManager;
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws Exception {
		
		if(request.getIds().size() > 20) {
			response.addError(new ErrorDetail(SystemError.MAXIMUM_LIMIT_EXCEEDED));
			return;
		}
		List<HotelInfo> hotelListWithReviewId = new ArrayList<>();
		List<HotelInfo> hInfoList = cacheHandler.getHotelInfoFromAerospike(request.getIds());
		Map<String , String> tempMap = getKeyUserReviewSupplierIdMap(hInfoList);
		List<HotelInfo> hotelListWithoutReviewId = new ArrayList<>();
		for(HotelInfo hInfo : hInfoList) {
			String key = HotelUtils.getKeyForUserReview(hInfo);
			String userReviewSupplierId = tempMap.get(key);
			if(StringUtils.isNotBlank(userReviewSupplierId)) {
				hInfo.setUserReviewSupplierId(userReviewSupplierId);
				hotelListWithReviewId.add(hInfo);
			}else hotelListWithoutReviewId.add(hInfo);
		}
		ExecutorUtils.getHotelUserReviewSearchThreadPool().submit(() -> reviewManager
				.getUserReviewSupplierIdFromHotel(hotelListWithoutReviewId));
		response.setReviews(reviewManager.getHotelUserReviewList(hotelListWithReviewId));
	}
	
	
	private Map<String, String> getKeyUserReviewSupplierIdMap(List<HotelInfo> hInfoList){
		
		List<String> keyList = hInfoList.stream().map((hInfo) -> HotelUtils.getKeyForUserReview(hInfo))
				.collect(Collectors.toList());
		return cacheHandler.getKeyHotelUserReviewSupplierIdMap(keyList);
	}
	
	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
