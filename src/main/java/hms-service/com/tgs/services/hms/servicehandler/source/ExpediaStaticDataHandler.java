package com.tgs.services.hms.servicehandler.source;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelCityInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExpediaStaticDataHandler extends ServiceHandler<HotelStaticDataRequest, BaseResponse> {

	@Autowired
	HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	HotelCityInfoSaveManager cityInfoSaveManager;

	@Autowired
	HotelCityInfoMappingManager cityInfoMappingManager;

	@Override
	public void beforeProcess() throws Exception {
		request.setLanguage(HotelUtils.getLanguage());
	}

	@Override
	public void process() throws Exception {
		try {
			String supplierName = StringUtils.isNotBlank(request.getSupplierId()) ? request.getSupplierId() :  HotelSourceType.EXPEDIA.name();
			AbstractStaticDataInfoFactory staticDataInfoFactory =
					HotelSourceType.getStaticDataFactoryInstance(supplierName, request);
			staticDataInfoFactory.getStaticHotelInfo();
		} catch (Exception e) {
			log.error("Unable to handle static info", e);
		}
	}

	public void syncCities(HotelStaticDataRequest staticDataRequest) {
		try {
			staticDataRequest.setLanguage(HotelUtils.getLanguage());
			String supplierName = StringUtils.isNotBlank(staticDataRequest.getSupplierId()) ? staticDataRequest.getSupplierId() :  HotelSourceType.EXPEDIA.name();
			AbstractStaticDataInfoFactory staticDataInfoFactory = HotelSourceType
					.getStaticDataFactoryInstance(supplierName, staticDataRequest);
			staticDataInfoFactory.getCityMappingInfo();
		} catch (Exception e) {
			log.error("Unable to handle city info", e);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}
}
