package com.tgs.services.hms.sources;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
public abstract class AbstractHotelBookingCancellationFactory {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelInfo hInfo;
	protected Order order;
	protected HotelSourceConfigOutput sourceConfigOutput;
	@Value("${env}")
	private String env;
	@Value("${pbuid}")
	private String prodBookingUserId;
	
	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public AbstractHotelBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo,
			Order order) {
		this.supplierConf = supplierConf;
		this.hInfo = hInfo;
		this.order = order;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(
				HotelSearchQuery.builder().sourceId(supplierConf.getBasicInfo().getSourceId()).build());
	}

	public abstract boolean cancelHotel() throws IOException, JAXBException;

	public abstract boolean getCancelHotelStatus() throws IOException;

	public boolean cancelBooking() {
		boolean isBookingCancelled = false;
		try {
			if (!isCancellationAllowed()) {
				log.info("Supplier Cancellation is not allowed for bookingId {}", order.getBookingId());
				isBookingCancelled = true;
			} else {
				isBookingCancelled = this.cancelHotel();
			}
		} catch (IOException e) {
			log.error("Exception while cancelling booking due to IOException for supplier {}  , bookingId {} ",
					supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(), e);
		} catch (Exception e) {
			log.error("Exception while cancelling booking for supplier {}  , bookingId {} ",
					supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(), e);
		} finally {
			LogUtils.clearLogList();
		}
		return isBookingCancelled;
	}


	public boolean getCancelBookingStatus() {

		boolean isBookingCancelled = false;
		try {
			isBookingCancelled = this.getCancelHotelStatus();
		} catch (Exception e) {
			log.error("Exception while getting booking cancellation status for bookingId {} ", order.getBookingId(), e);
		} finally {
			LogUtils.clearLogList();
		}
		return isBookingCancelled;
	}

	public boolean isCancellationAllowed() {
		return ((BooleanUtils.isTrue(this.getSupplierConf().getHotelSupplierCredentials().getIsTestCredential())
				|| "prod".equalsIgnoreCase(env) || order.getBookingUserId().equals(prodBookingUserId)));
	}

}
