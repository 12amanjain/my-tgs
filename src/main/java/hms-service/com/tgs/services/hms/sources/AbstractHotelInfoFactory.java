package com.tgs.services.hms.sources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.base.utils.thread.UnboundedAsynchronousExecutor;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public abstract class AbstractHotelInfoFactory {

	protected HotelSearchResult searchResult;
	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfigOutput;
	protected CityInfoMapping supplierCityInfo;

	@Autowired
	protected HotelCacheHandler cacheHandler;

	@Autowired
	protected GeneralServiceCommunicator gsCommunicator;

	@Autowired
	protected GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;


	public AbstractHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		this.searchQuery = searchQuery;
		this.supplierConf = supplierConf;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(searchQuery);

	}

	private void init() {

		this.supplierCityInfo = cacheHandler.getSupplierCityInfoFromCityId(searchQuery.getSearchCriteria().getCityId(),
				HotelSourceType.getHotelSourceType(supplierConf.getBasicInfo().getSourceId()).name());
	}

	protected abstract void searchAvailableHotels() throws IOException, JAXBException, InterruptedException;

	protected abstract void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery)
			throws IOException, JAXBException;

	protected abstract void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException;

	public HotelSearchResult getAvailableHotels() {
		try {
			LogUtils.log(LogTypes.HOTEL_SUPPLIER_SEARCH_START,
					LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);
			init();
			searchResult = this.getHotelSearchDetailsFromCache();
			if (searchResult == null) {
				// if(!validateSearchCriteria()) return searchResult;
				log.info("Doing search with search id {} for supplier {}", searchQuery.getSearchId(),
						supplierConf.getBasicInfo().getSupplierName());
				this.searchAvailableHotels();
				cacheHandler.cacheHotelSearchResults(searchResult, searchQuery, supplierConf);
				LogUtils.log(LogTypes.HOTEL_SUPPLIER_SEARCH_END,
						LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).searchId(searchQuery.getSearchId())
								.trips(searchResult == null ? 0 : searchResult.getNoOfHotelOptions())
								.supplierId(supplierConf.getBasicInfo().getSupplierName()).build(),
						LogTypes.HOTEL_SUPPLIER_SEARCH_START);
			}
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search result due to I/O exception");
			log.info("Available hotel search failed because of I/O exception for searchQuery {}", searchQuery, e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search result due to " + e.getCause());
			log.error("Available hotel search failed for searchQuery {}", searchQuery, e);
		} finally {
			LogUtils.clearLogList();
		}
		return searchResult;
	}


	public void fetchHotelDetails(HotelInfo hInfo) {
		try {
			String searchId = hInfo.getMiscInfo().getSearchId();
			HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(searchId);
			this.searchHotel(hInfo, searchQuery);
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get search hotel details to I/O exception");
			log.info("Available hotel search failed because of I/O exception for searchQuery {}", searchQuery, e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to get hotel details due to " + e.getCause());
			log.error("Fetch hotel Details failed for hInfo {}", hInfo, e);
		} finally {
			LogUtils.clearLogList();
		}
	}

	private HotelSearchResult getHotelSearchDetailsFromCache() {
		return cacheHandler.getCachedSearchResult(searchQuery, supplierConf);
	}

	public void getCancellationPolicy(HotelSearchQuery query, HotelInfo hInfo, String logKey) {
		try {
			this.searchCancellationPolicy(hInfo, logKey);
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to fetch cancellation policy due to I/O exception");
			log.info("Unable to fetch cancellation policy due to I/O exception for searchQuery {}", searchQuery, e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to fetch cancellation policy due to " + e.getCause());
			log.error("Unable to fetch cancellation policy for searchQuery {}, hInfo {}", query, hInfo, e);
		} finally {
			LogUtils.clearLogList();
		}
	}

	private boolean validateSearchCriteria() {

		boolean isValid = true;
		Integer maxAdultsInRoom = sourceConfigOutput.getMaxAdultInRoom();
		if (maxAdultsInRoom == null)
			return isValid;
		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			if (roomSearchInfo.getNumberOfAdults() > maxAdultsInRoom) {
				isValid = false;
				break;
			}
		}
		return isValid;

	}

	protected Set<String> getPropertyIdsOfCity(String supplierName) {
		String cityName = getCityName();
		Set<String> propertyIds = new HashSet<>();
		String supplierMappingSetName = HotelUtils.getSupplierSetName(supplierName);
		CacheMetaInfo supplierMappingMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName())
				.set(supplierMappingSetName).bins(new String[] {BinName.CITY.name(), BinName.RATING.name()}).build();

		Map<String, Map<String, String>> supplierhotelIdMap = cachingCommunicator.getResultSet(supplierMappingMetaInfo,
				String.class, Filter.equal(BinName.CITY.getName(), "\"" + cityName.toLowerCase() + "\""));
		for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
			propertyIds.add(supplierHotel.getKey());
		}

		return propertyIds;
	}


	protected HotelSearchResult getResultOfAllPropertyIds(Set<String> propertyIds)
			throws IOException, InterruptedException {

		HotelSearchResult finalSearchResult = new HotelSearchResult();
		List<Set<String>> listOfPartitionedIdsSet =
				HotelUtils.partitionSet(propertyIds, sourceConfigOutput.getHotelHitCount());
		int hotelSearchExecBatchSize = sourceConfigOutput.getHotelBatchSize();
		List<Callable<?>> taskList = new ArrayList<>();
		listOfPartitionedIdsSet.forEach(set -> {
			Callable<HotelSearchResult> callable = () -> {
				try {
					HotelSearchResult searchResult = doSearchForSupplier(set);
					if (searchResult != null && CollectionUtils.isNotEmpty(searchResult.getHotelInfos()))
						finalSearchResult.getHotelInfos().addAll(searchResult.getHotelInfos());
					return searchResult;
				} catch (IOException | JAXBException e) {
					log.error("Error while search for hotels for {} Supplier",
							supplierConf.getBasicInfo().getSupplierName(), e);
					throw e;
				}
			};
			taskList.add(callable);
		});
		if (CollectionUtils.isEmpty(taskList)) {
			log.info("Task List Is Empty So Returning for Supplier {}", supplierConf.getBasicInfo().getSupplierName());
			return null;
		}

		UnboundedAsynchronousExecutor asyncExecutor = new UnboundedAsynchronousExecutor(
				ExecutorUtils.getHotelSearchThreadPool(), hotelSearchExecBatchSize, 5000, 5, null);
		asyncExecutor.executeAsynchronously(taskList.iterator());
		synchronized (asyncExecutor) {
			asyncExecutor.wait();
		}
		return finalSearchResult;
	}

	protected HotelSearchResult doSearchForSupplier(Set<String> set) throws IOException, JAXBException {
		return null;
	}

	private String getCityName() {

		if (supplierCityInfo != null) {
			return ObjectUtils.firstNonNull(supplierCityInfo.getSupplierCityName(),
					searchQuery.getSearchCriteria().getCityName());
		}
		return searchQuery.getSearchCriteria().getCityName();
	}


}
