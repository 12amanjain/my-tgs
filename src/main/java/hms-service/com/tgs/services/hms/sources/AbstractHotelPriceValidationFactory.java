package com.tgs.services.hms.sources;

import java.io.IOException;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public abstract class AbstractHotelPriceValidationFactory {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelInfo hInfo;
	protected HotelSearchQuery searchQuery;
	protected String bookingId;
	protected HotelSourceConfigOutput sourceConfigOutput;

	public AbstractHotelPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		this.searchQuery = query;
		this.hInfo = hotel;
		this.supplierConf = hotelSupplierConf;
		this.bookingId = bookingId;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(searchQuery);
	}

	public abstract Option getOptionWithUpdatedPrice() throws Exception;

	public Option getUpdatedOption() {
		try {
			return this.getOptionWithUpdatedPrice();
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to fetch price changes due to I/O exception");
			log.info("Unable to fetch price changes because of I/O exception for hotel {} for searchId {} and bookingId {}",
					hInfo.getName(), searchQuery.getSearchId(), bookingId, e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to fetch price changes due to " + e.getCause());
			log.error("Unable to fetch price changes for hotel {}, for searchId {} and bookingId {}", hInfo.getName(),
					searchQuery.getSearchId(), bookingId, e);
		} finally {
			LogUtils.clearLogList();
		}
		return null;
	}

}
