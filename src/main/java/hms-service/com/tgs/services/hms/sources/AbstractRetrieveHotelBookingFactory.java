package com.tgs.services.hms.sources;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Service;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Service
@Slf4j
public abstract class AbstractRetrieveHotelBookingFactory {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelImportBookingParams importBookingParams;
	protected HotelImportedBookingInfo bookingDetailResponse;
	protected HotelSourceConfigOutput sourceConfigOutput;

	public AbstractRetrieveHotelBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		this.supplierConf = supplierConf;
		this.importBookingParams = importBookingInfo;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(
				HotelSearchQuery.builder().sourceId(supplierConf.getBasicInfo().getSourceId()).build());
	}

	public abstract void retrieveBooking() throws IOException , JAXBException;

	public HotelImportedBookingInfo retrieveBookingDetails() {
		try {
			this.retrieveBooking();
		} catch (IOException e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to retrieve booking details due to I/O exception");
			log.error(
					"Unable to retrieve booking details due to I/O exception for boooking id {} and supplier booking id {}",
					importBookingParams.getBookingId(), importBookingParams.getSupplierBookingId(), e);
		} catch (Exception e) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Unable to retrieve booking details due to " + e.getCause());
			log.error("Unable to retrieve booking details for boooking id {} and supplier booking id {}",
					importBookingParams.getBookingId(), importBookingParams.getSupplierBookingId(), e);
		} finally {
			LogUtils.clearLogList();
		}
		return bookingDetailResponse;
	}
}
