package com.tgs.services.hms.sources;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
public abstract class AbstractStaticDataInfoFactory {

	@Autowired
	protected GeneralServiceCommunicator gsCommunicator;

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSourceConfigOutput sourceConfigOutput;
	protected HotelStaticDataRequest staticDataRequest;

	public AbstractStaticDataInfoFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		this.staticDataRequest = staticDataRequest;
		this.supplierConf = supplierConf;
		this.sourceConfigOutput = HotelUtils.getHotelSourceConfigOutput(
				HotelSearchQuery.builder().sourceId(supplierConf.getBasicInfo().getSourceId()).build());
	}

	protected abstract void getHotelStaticData() throws IOException, JAXBException;

	protected abstract void getCityMappingData() throws IOException;

	public void getStaticHotelInfo() {
		try {
			this.getHotelStaticData();
		} catch (IOException e) {
			log.error("Exception while fetching static info due to IOException for supplier {}",
					supplierConf.getBasicInfo().getSupplierName(), e);
		} catch (Exception e) {
			log.error("Exception while fetching static info for supplier {}",
					supplierConf.getBasicInfo().getSupplierName(), e);
		} finally {
			LogUtils.clearLogList();
		}
	}

	public void getCityMappingInfo() {
		try {
			this.getCityMappingData();
		} catch (IOException e) {
			log.error("Exception while fetching city mapping info due to IOException for supplier {}",
					supplierConf.getBasicInfo().getSupplierName(), e);
		} catch (Exception e) {
			log.error("Exception while fetching city mapping info for supplier {}",
					supplierConf.getBasicInfo().getSupplierName(), e);
		} finally {
			LogUtils.clearLogList();
		}
	}
}
