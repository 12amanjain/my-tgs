package com.tgs.services.hms.sources.agoda;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.util.StringUtils;

import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.RoomSurcharge;
import com.tgs.services.hms.datamodel.RoomSurcharge.Rate;
import com.tgs.services.hms.datamodel.agoda.precheck.ChildrenAgesRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.HotelRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.RateRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.RoomRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.RoomsRequest;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Benefits.Benefit;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyDates;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.Cancellation.PolicyDates.PolicyDate;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.RateType;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room.RateInfo.Surcharges.Surcharge;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.utils.common.HttpUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class AgodaBaseService {

	protected HotelSearchQuery searchQuery;
	protected HotelSupplierConfiguration supplierConf;
	protected HttpUtils httpUtils;
	protected RestAPIListener listener;
	protected HotelSourceConfigOutput sourceConfigOutput;
	protected Integer cancellationBuffer;

	protected LocalDateTime getFormattedDate(String date) {
		DateTimeFormatter DATEFORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate ld = LocalDate.parse(date, DATEFORMATTER);
		LocalDateTime formattedDate = LocalDateTime.of(ld, LocalDateTime.MIN.toLocalTime());
		return formattedDate;

	}

	protected String getMealBasis(Benefits benefits) {

		String mealBasis = "";
		if (Objects.isNull(benefits)) {
			mealBasis = "Room Only";
		} else {
			for (Benefit benefit : benefits.getBenefit()) {
				if (benefit.getId() == 1)
					mealBasis += "Breakfast";
				else if (benefit.getId() == 2)
					mealBasis += "Lunch";
				else if (benefit.getId() == 3)
					mealBasis += "Dinner";
				// else if (benefit.getId() == 4)
				// mealBasis += "Beverages";
			}
			if (mealBasis.equals("BreakfastLunchDinner") || mealBasis.equals("BreakfastDinnerLunch"))
				return "Full Board";
			else if (mealBasis.equals("BreakfastLunch") || mealBasis.equals("BreakfastDinner"))
				return "Half Board";
			return mealBasis;
		}
		return mealBasis;
	}

	protected List<RoomInfo> getRooms(Room agodaRoom) {

		List<RoomInfo> roomList = new ArrayList<RoomInfo>();
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setNumberOfAdults(room.getNumberOfAdults());
			roomInfo.setNumberOfChild(room.getNumberOfChild());
			populateRoomInfo(roomInfo, agodaRoom);
			roomList.add(roomInfo);
		}
		updatePriceWithClientCommission(roomList);
		return roomList;
	}


	protected void populateRoomInfo(RoomInfo roomInfo, Room agodaRoom) {

		roomInfo.setRoomCategory(agodaRoom.getName());
		roomInfo.setRoomType(agodaRoom.getName());
		roomInfo.setMealBasis(getMealBasis(agodaRoom.getBenefits()));
		roomInfo.setId(agodaRoom.getId());
		roomInfo.setCheckInDate(searchQuery.getCheckinDate());
		roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
		RateInfo rateInfo = agodaRoom.getRateInfo();
		RateType rate = rateInfo.getRate();

		boolean isExtraBedIncluded = getExtraBedInfo(agodaRoom);
		roomInfo.setIsextraBedIncluded(isExtraBedIncluded);
		RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomIndex(Integer.valueOf(agodaRoom.getId()))
				.ratePlanCode(agodaRoom.getRateplanid() + "").roomTypeName(agodaRoom.getRatecategoryid() + "")
				.roomBlockId(agodaRoom.getBlockid()).lineItemId(agodaRoom.getLineitemid())
				.rateChannel(agodaRoom.getRatechannel()).exclusive(rate.getExclusive()).fees(rate.getFees())
				.inclusive(rate.getInclusive()).tax(rate.getTax()).rateType(agodaRoom.getRatetype())
				.ratePlan(agodaRoom.getRateplan()).model(agodaRoom.getModel()).build();
		roomInfo.setMiscInfo(roomMiscInfo);
		setPerNightRoomPrice(roomInfo, agodaRoom);


	}

	private boolean getExtraBedInfo(Room agodaRoom) {

		if (!StringUtils.isEmpty(agodaRoom.getRateInfo().getIncluded())) {
			if (agodaRoom.getRateInfo().getIncluded().contains("Extra Bed"))
				return true;
		}
		return false;
	}


	protected List<RoomSurcharge> getSurchargeList(RoomInfo room, Room agodaRoom) {

		List<RoomSurcharge> surcharges = new ArrayList<>();
		if (!Objects.isNull(agodaRoom.getRateInfo().getSurcharges())
				&& CollectionUtils.isNotEmpty(agodaRoom.getRateInfo().getSurcharges().getSurcharge())) {
			for (Surcharge agodaSurcharge : agodaRoom.getRateInfo().getSurcharges().getSurcharge()) {

				RoomSurcharge roomSurcharge = RoomSurcharge.builder().id(agodaSurcharge.getId())
						.charge(agodaSurcharge.getCharge()).name(agodaSurcharge.getName()).build();

				Rate rate = roomSurcharge.new Rate();
				rate.setExclusive(agodaSurcharge.getRate().getExclusive());
				rate.setInclusive(agodaSurcharge.getRate().getInclusive());
				rate.setFees(agodaSurcharge.getRate().getFees());
				rate.setTax(agodaSurcharge.getRate().getTax());
				roomSurcharge.setRate(rate);

				surcharges.add(roomSurcharge);
			}
		}

		return surcharges;
	}

	private void setPerNightRoomPrice(RoomInfo room, Room agodaRoom) {

		RoomMiscInfo roomMiscInfo = room.getMiscInfo();
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		List<PriceInfo> priceInfoList = new ArrayList<>();
		double totalRoomSurcharge = 0.0;
		List<RoomSurcharge> surcharges = getSurchargeList(room, agodaRoom);

		if (surcharges.size() > 0) {
			roomMiscInfo.setSurcharges(surcharges);
			totalRoomSurcharge = surcharges.stream().filter(surcharge -> surcharge.getCharge().equals("Mandatory"))
					.mapToDouble(surcharge -> surcharge.getRate().getInclusive()).sum();
		}
		double pernightRoomSurcharge = totalRoomSurcharge / (numberOfNights * searchQuery.getRoomInfo().size());

		for (int j = 1; j <= numberOfNights; j++) {
			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF,
					agodaRoom.getRateInfo().getRate().getInclusive() + pernightRoomSurcharge);
			priceInfo.setDay(j);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		}
		room.setPerNightPriceInfos(priceInfoList);
	}

	protected boolean isValid() {

		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		if (totalNumberOfChildren() > 79 || totalNumberOfAdult() > 80 || searchQuery.getRoomInfo().size() > 20
				|| numberOfNights > 14)
			return false;
		if (searchQuery.getCheckinDate().getYear() != searchQuery.getCheckoutDate().getYear())
			return false;
		if (totalNumberOfAdult() < searchQuery.getRoomInfo().size())
			return false;

		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				for (int age : room.getChildAge()) {
					if (age > 17)
						return false;
				}
			}
		}
		return true;
	}

	protected boolean validRoomOcuupancy(Room agodaRoom) {

		int maxRoomOccupancy = 0;

		if (agodaRoom.getRemainingRooms() < searchQuery.getRoomInfo().size())
			return false;
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			int noOfChild = room.getNumberOfChild() != null ? room.getNumberOfChild() : 0;
			int occupancy = room.getNumberOfAdults() + noOfChild;
			maxRoomOccupancy = maxRoomOccupancy < occupancy ? occupancy : maxRoomOccupancy;
		}
		long agodaRoomOccupancy = agodaRoom.getMaxRoomOccupancy().getNormalbedding()
				+ ObjectUtils.firstNonNull(agodaRoom.getMaxRoomOccupancy().getExtrabeds(), 0l);

		return agodaRoomOccupancy < maxRoomOccupancy ? false : true;
	}

	protected void setOptionCancellationPolicy(Room agodaRoom, Option option) {

		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		List<PenaltyDetails> pds = new ArrayList<>();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		boolean isFullRefundAllowed = false;
		String cancellationPolicyText = agodaRoom.getCancellation().getPolicyText().getValue();
		cancellationBuffer = sourceConfigOutput.getCancellationPolicyBuffer() != null
				? sourceConfigOutput.getCancellationPolicyBuffer()
				: 48;

		PolicyDates policyDates = agodaRoom.getCancellation().getPolicyDates();
		LocalDateTime fromDate = now;
		LocalDateTime toDate = checkInDate;
		for (PolicyDate policyDate : policyDates.getPolicyDate()) {
			if (pds.size() > 0) {
				if (policyDate.getBefore() != null) {
					toDate = getFormattedDate(policyDate.getBefore());
					fromDate = pds.get(pds.size() - 1).getToDate();
				}
				if (policyDate.getAfter() != null) {
					fromDate = getFormattedDate(policyDate.getAfter());
					toDate = checkInDate;
				}
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(fromDate).toDate(toDate)
						.penaltyAmount(policyDate.getRate().getInclusive()).build();
				pds.add(penaltyDetails1);
			} else {
				toDate = policyDate.getBefore() != null ? getFormattedDate(policyDate.getBefore()) : toDate;
				fromDate = policyDate.getAfter() != null ? getFormattedDate(policyDate.getAfter()) : fromDate;
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(now).toDate(toDate)
						.penaltyAmount(policyDate.getRate().getInclusive()).build();
				pds.add(penaltyDetails1);
			}
		}
		Iterator<PenaltyDetails> i = pds.iterator();
		PenaltyDetails pd = null;
		while (i.hasNext()) {
			pd = (PenaltyDetails) i.next();
			LocalDateTime tDate = pd.getToDate().minus(cancellationBuffer, ChronoUnit.HOURS);
			if (tDate.isBefore(now)) {
				i.remove();
			} else {
				LocalDateTime fDate = pd.getFromDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				if (fDate.isBefore(now))
					fDate = now;
				pd.setFromDate(fDate);
				pd.setToDate(tDate);
				if (pd.getPenaltyAmount() == 0.0) {
					deadlineDateTime = tDate;
					isFullRefundAllowed = true;
				}
			}
		}
		if (pds.size() == 0) {
			PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(now).toDate(checkInDate).penaltyAmount(
					policyDates.getPolicyDate().get(policyDates.getPolicyDate().size() - 1).getRate().getInclusive())
					.build();

			pds.add(penaltyDetails1);
		}
		if (pds.get(0).getPenaltyAmount() > 0.0) {
			isFullRefundAllowed = false;
		}
		pds.get(pds.size() - 1).setToDate(checkInDate);

		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(option.getId())
				.cancellationPolicy(cancellationPolicyText).isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();
		option.setDeadlineDateTime(deadlineDateTime);
		option.setCancellationPolicy(cancellationPolicy);

	}

	protected void setRooms(HotelRequest hotel, HotelInfo hInfo) {

		RoomInfo selectedRoomType = hInfo.getOptions().get(0).getRoomInfos().get(0);
		Option option = hInfo.getOptions().get(0);
		RoomsRequest roomsRequest = new RoomsRequest();
		RoomRequest roomRequest = new RoomRequest();
		setOccupancy(roomRequest);
		roomRequest.setBlockid(selectedRoomType.getMiscInfo().getRoomBlockId());
		roomRequest.setCount(option.getRoomInfos().size());
		roomRequest.setCurrency("INR");
		roomRequest.setId(selectedRoomType.getId());
		roomRequest.setLineitemid(selectedRoomType.getMiscInfo().getLineItemId() + "");
		roomRequest.setModel(selectedRoomType.getMiscInfo().getModel());
		roomRequest.setName(selectedRoomType.getRoomType());
		roomRequest.setRatechannel(selectedRoomType.getMiscInfo().getRateChannel());

		RateRequest rate = new RateRequest();
		rate.setExclusive(selectedRoomType.getMiscInfo().getExclusive());
		rate.setInclusive(selectedRoomType.getMiscInfo().getInclusive());
		rate.setFees(selectedRoomType.getMiscInfo().getFees());
		rate.setTax(selectedRoomType.getMiscInfo().getTax());
		roomRequest.setRate(rate);

		roomRequest.setRatecategoryid(selectedRoomType.getMiscInfo().getRoomTypeName());
		String RatePlanId = selectedRoomType.getMiscInfo().getRatePlanCode();
		if (!RatePlanId.equals("null")) {
			roomRequest.setRateplanid(selectedRoomType.getMiscInfo().getRatePlanCode());
		}

		roomRequest.setRateplan(selectedRoomType.getMiscInfo().getRatePlan());
		roomRequest.setRatetype(selectedRoomType.getMiscInfo().getRateType());
		roomsRequest.setRoom(Arrays.asList(roomRequest));
		hotel.setRooms(roomsRequest);

	}

	protected void setOccupancy(RoomRequest roomRequest) {

		ChildrenAgesRequest childrenAgesRequest = new ChildrenAgesRequest();
		List<Integer> ages = new ArrayList<Integer>();
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				for (Integer age : room.getChildAge()) {
					ages.add(age);
				}
			}
		}
		roomRequest.setAdults(totalNumberOfAdult());
		roomRequest.setChildren(totalNumberOfChildren());
		if (ages.size() > 0) {
			childrenAgesRequest.setAge(ages);
			roomRequest.setChildrenAges(childrenAgesRequest);
		}

	}

	protected void setOptionSurcharge(Option option, RoomInfo room) {

		if (!CollectionUtils.isEmpty(room.getMiscInfo().getSurcharges())
				&& room.getMiscInfo().getSurcharges().size() > 0) {

			List<RoomSurcharge> roomSurchargeList = room.getMiscInfo().getSurcharges();
			Instruction instruction = Instruction.builder().type(InstructionType.INCLUSION_EXCLUSION)
					.msg(GsonUtils.getGson().toJson(roomSurchargeList)).build();
			option.getInstructions().add(instruction);
		}
	}

	protected void setOptionBenefits(Option option, Room agodaRoom) {

		String benefits = getBenefits(agodaRoom);
		if (!StringUtils.isEmpty(benefits)) {
			Instruction instruction = Instruction.builder().type(InstructionType.BENEFITS).msg(benefits).build();
			option.getInstructions().add(instruction);
		}
	}

	protected Integer totalNumberOfAdult() {

		int totalNumberOfAdults = 0;
		for (RoomSearchInfo room : searchQuery.getRoomInfo())
			totalNumberOfAdults += room.getNumberOfAdults();

		return totalNumberOfAdults;
	}

	protected Integer totalNumberOfChildren() {

		int totalNumberOfchildren = 0;
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			if (room.getNumberOfChild() != null)
				totalNumberOfchildren += room.getNumberOfChild();
		}
		return totalNumberOfchildren;
	}

	public String getBenefits(Room agodaRoom) {

		String instruction = "";
		if (!Objects.isNull(agodaRoom.getBenefits())
				&& CollectionUtils.isNotEmpty(agodaRoom.getBenefits().getBenefit())) {
			for (Benefit benefit : agodaRoom.getBenefits().getBenefit()) {
				instruction += benefit.getName() + ", ";
			}
		}
		return instruction;
	}

	private void updatePriceWithClientCommission(List<RoomInfo> rInfoList) {

		double supplierMarkup =
				sourceConfigOutput.getSupplierMarkup() == null ? 0.0 : sourceConfigOutput.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.MUP, (supplierGrossPrice * supplierMarkup) / 100);
				Double supplierGrossPriceWithMarkup =
						supplierGrossPrice + priceInfo.getFareComponents().getOrDefault(HotelFareComponent.MUP, 0.0);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPriceWithMarkup);

			}
		}
	}

	public void storeLogs(String identifier, String key) {

		Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
				.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getPostData())
				.type(identifier + "-Req" + supplierConf.getHotelSupplierCredentials().getUserName())
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());

		listener.addLog(LogData.builder().key(key).logData(httpUtils.getResponseString())
				.type(identifier + "-Res" + supplierConf.getHotelSupplierCredentials().getUserName())
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());

	}

}
