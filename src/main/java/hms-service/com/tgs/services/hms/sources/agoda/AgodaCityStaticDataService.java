package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.SupplierCityInfo;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaCity;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaCountry;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaStaticDataRequest;
import com.tgs.services.hms.datamodel.agoda.statc.CityFeed;
import com.tgs.services.hms.datamodel.agoda.statc.CountryFeed;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierCityInfoManager;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;

@Builder
public class AgodaCityStaticDataService {

	protected HotelSupplierConfiguration supplierConf;
	private HotelStaticDataRequest staticDataRequest;

	private HotelSupplierCityInfoManager supplierCityInfoManager;
	private HotelCityInfoMappingManager cityInfoMappingManager;

	public void init() {

		cityInfoMappingManager = (HotelCityInfoMappingManager) SpringContext.getApplicationContext()
				.getBean("hotelCityInfoMappingManager");

		supplierCityInfoManager = (HotelSupplierCityInfoManager) SpringContext.getApplicationContext()
				.getBean("hotelSupplierCityInfoManager");
	}

	public List<HotelCityInfoQuery> getAgodaCityList() throws IOException {

		List<HotelCityInfoQuery> cityList = new ArrayList<>();
		List<AgodaCountry> countryList = getAgodaCountryList();
		for (AgodaCountry agodaCountry : countryList) {
			List<AgodaCity> cityResult = getAgodaCityListForCountry(agodaCountry.getCountryId());
			if (CollectionUtils.isNotEmpty(cityResult)) {
				List<HotelCityInfoQuery> cities = getCitiesFromAgodaCities(cityResult, agodaCountry.getCountryName());
				cityList.addAll(cities);
			}
		}
		return cityList;
	}

	private List<HotelCityInfoQuery> getCitiesFromAgodaCities(List<AgodaCity> cityResult, String countryName) {

		List<HotelCityInfoQuery> cityList = new ArrayList<>();
		cityResult.forEach((agodaCity) -> {
			HotelCityInfoQuery cityInfo = new HotelCityInfoQuery();
			cityInfo.setCityname(agodaCity.getCityName());
			cityInfo.setCountryname(countryName);
			cityInfo.setSuppliercityid(String.valueOf(agodaCity.getCityId()));
			cityInfo.setSuppliercountryid(String.valueOf(agodaCity.getCountryId()));
			cityInfo.setSuppliername(AgodaConstants.NAME.getValue());
			cityList.add(cityInfo);
		});
		return cityList;
	}

	public List<AgodaCity> getAgodaCityListForCountry(Long countryId) throws IOException {

		AgodaStaticDataRequest staticRequest =
				AgodaStaticDataRequest.builder().feed_id("3").ocountry_id(String.valueOf(countryId)).build();
		HttpUtils httpUtils =
				AgodaUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, staticRequest, supplierConf);
		httpUtils.setRequestMethod("GET");
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);

		CityFeed cityFeed = AgodaMarshaller.unmarshallCityFeed((xmlResponse));
		if (cityFeed != null)
			return cityFeed.getCities();
		return null;
	}

	private List<AgodaCountry> getAgodaCountryList() throws IOException {

		AgodaStaticDataRequest staticRequest = AgodaStaticDataRequest.builder().feed_id("2").build();
		HttpUtils httpUtils =
				AgodaUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, staticRequest, supplierConf);
		httpUtils.setRequestMethod("GET");
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		CountryFeed countryFeed = AgodaMarshaller.unmarshallCountryFeed(xmlResponse);
		if (countryFeed != null)
			return countryFeed.getCountry();
		return null;

	}

	public void saveIntoDB(List<HotelCityInfoQuery> cityData) {

		if (BooleanUtils.isTrue(staticDataRequest.getIsMasterData())) {
			supplierCityInfoManager.saveSupplierCityInfo(getSupplierCityInfo(cityData));
		} else {
			cityInfoMappingManager.saveCityInfoMappingList(cityData);
		}
	}

	private List<SupplierCityInfo> getSupplierCityInfo(List<HotelCityInfoQuery> cityData) {

		List<SupplierCityInfo> supplierCityInfos = new ArrayList<>();
		cityData.forEach(cityMapping -> {

			SupplierCityInfo supplierCityInfo = SupplierCityInfo.builder().cityId(cityMapping.getSuppliercityid())
					.cityName(cityMapping.getCityname()).countryId(cityMapping.getSuppliercountryid())
					.countryName(cityMapping.getCountryname()).supplierName(HotelSourceType.AGODA.name()).build();
			supplierCityInfos.add(supplierCityInfo);
		});
		return supplierCityInfos;
	}
}
