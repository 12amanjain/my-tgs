package com.tgs.services.hms.sources.agoda;

public enum AgodaConstants {
	NAME("AGODA"),
	CURRENCY("INR"),
	LANGUAGECODE("en-us"),
	SHORTSEARCH("6"),
	LONGSEARCH("4");
	private String value;

	private AgodaConstants(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
