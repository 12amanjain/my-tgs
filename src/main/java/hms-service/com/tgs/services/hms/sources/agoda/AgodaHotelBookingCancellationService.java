package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Service;

import com.tgs.services.base.RestAPIListener;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.agoda.precheck.CancellationRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.CancellationResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.CancellationResponseV2.CancellationSummary.Refund.RefundRateInclusive;
import com.tgs.services.hms.datamodel.agoda.precheck.ConfirmCancellationRequestV2;
import com.tgs.services.hms.datamodel.agoda.precheck.ConfirmCancellationRequestV2.Refund;
import com.tgs.services.hms.datamodel.agoda.precheck.ConfirmCancellationResponseV2;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;

import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class AgodaHotelBookingCancellationService extends AgodaBaseService {
	
	private Order order;
	private HotelInfo hInfo;

	public boolean cancelBooking() throws IOException, JAXBException {


		CancellationRequestV2 cancellationRequest = null;
		try {
			listener = new RestAPIListener("");
			cancellationRequest = getHotelBookingCancellationRequest();
			String xmlRequest = AgodaMarshaller.marshallXml(cancellationRequest);

			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.CANCEL_BOOKING_URL, null, supplierConf);
			log.info("Cancellation Request is {}", httpUtils.getPostData());
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			log.info("Cancellation Response string is ", xmlResponse);
			
			if (xmlResponse == null) {
				log.info("Unable to get response {}", xmlRequest, xmlResponse);
			}
			CancellationResponseV2 result = AgodaMarshaller.unmarshallCancellationResponse(xmlResponse);
			storeLogs("Agoda-Booking-Cancellation", order.getBookingId());
			if (result.getStatus().equals("200")) {
				return confirmCancellation(result, listener);
			}
		}finally {
			
		}
		
		return false;
	}

	private boolean confirmCancellation(CancellationResponseV2 result, RestAPIListener listener2) throws JAXBException, IOException {

		ConfirmCancellationRequestV2 confirmCancellationRequest = getConfirmCancellationRequest(result);
		HttpUtils httpUtils = null;
		try {
			String xmlRequest = AgodaMarshaller.marshallXml(confirmCancellationRequest);

			httpUtils = AgodaUtil.getHttpUtils(xmlRequest,HotelUrlConstants.CANCEL_BOOKING_STATUS_URL, null, supplierConf);
			log.info("Cancellation Request is {}", httpUtils.getPostData());
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (xmlResponse == null) {
				log.error("Unable to get response {}", xmlRequest, xmlResponse);
			}
			log.info("Cancellation Response is {}", xmlResponse);
			ConfirmCancellationResponseV2 confirmCancelResult =
					AgodaMarshaller.unmarshallConfirmCancellationResponse(xmlResponse);
			storeLogs("Agoda-Booking-Cancellation-Confirm", order.getBookingId());
			if (confirmCancelResult.getStatus().equals("200")) return true;
			
		} finally {
		}
		
		return false;
	}

	private ConfirmCancellationRequestV2 getConfirmCancellationRequest(CancellationResponseV2 result) {

		List<RefundRateInclusive> refundRate = null;
		ConfirmCancellationRequestV2 request = new ConfirmCancellationRequestV2();
		if (result.getCancellationSummary().getRefund() != null) {
			refundRate = result.getCancellationSummary().getRefund().getRefundRateInclusive();
		}
		request.setCancelReason("0");
		Refund refund = new Refund();
		if (refundRate != null) {
			refund.setRefundRateInclusive(refundRate);
			request.setRefund(refund);
		}
		request.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		request.setReference(result.getCancellationSummary().getReference());
		request.setBookingID(result.getCancellationSummary().getBookingID());
		request.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		return request;
	}

	private CancellationRequestV2 getHotelBookingCancellationRequest() {

		CancellationRequestV2 cancelRequest = new CancellationRequestV2();
		cancelRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		cancelRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		cancelRequest.setBookingID(hInfo.getMiscInfo().getSupplierBookingId());
		return cancelRequest;

	}
}
