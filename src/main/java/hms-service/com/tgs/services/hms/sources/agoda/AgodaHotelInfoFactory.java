package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AgodaHotelInfoFactory extends AbstractHotelInfoFactory {

	public AgodaHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException, InterruptedException {
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.AGODA.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);	
	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> set) throws IOException, JAXBException {
		
		AgodaSearchService searchService = AgodaSearchService.builder()
				.supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
				.sourceConfigOutput(sourceConfigOutput).hotelIds(set).build();
		searchService.doSearch();
		HotelSearchResult searchResult = (HotelSearchResult)searchService.getSearchResult();
		return searchResult;
	}
	

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});
		
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		if(CollectionUtils.isNotEmpty(hInfo.getOptions())) {
			for(Option option : hInfo.getOptions()) {
				option.getMiscInfo().setIsDetailHit(true);
				option.getMiscInfo().setSecondarySupplier(supplierConf.getBasicInfo().getSupplierId());
			}
		}
		
	}

}
