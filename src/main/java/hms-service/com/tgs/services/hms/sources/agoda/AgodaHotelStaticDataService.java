package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaChildAndExtraBedPolicy;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotel;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelAddress;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelDescription;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFacility;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFeed;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFeedFull;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelImage;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaPolicy;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaPolicy.AgodaPolicyBuilder;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaRoomFacility;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaRoomFacilityFeed;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaRoomType;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaStaticDataRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Builder
@Slf4j
public class AgodaHotelStaticDataService {

	protected HotelInfoSaveManager hotelInfoSaveManager;
	private HotelStaticDataRequest staticDataRequest;
	protected HotelSupplierConfiguration supplierConf;
	private static final ExecutorService executor = Executors.newFixedThreadPool(25);
	private HotelCityInfoMappingManager cityInfoMappingManager;
	private HotelSourceConfigOutput sourceConfigOutput;

	public void process() throws IOException {

		int hotelCount = 0;
		List<String> cityIds = null;
		if (CollectionUtils.isNotEmpty(sourceConfigOutput.getCityList())) {
			cityIds = sourceConfigOutput.getCityList();
			for (String city : cityIds) {
				try {
					List<HotelInfo> hInfoList = getMasterHotelForCity(city);
					hotelCount += hInfoList.size();
					if (CollectionUtils.isEmpty(hInfoList))
						continue;
					log.info("Number Of Results For City {} is {}", city, hInfoList.size());
					saveHotelInfoList(hInfoList);
				} catch (Exception e) {
					log.error("Error While Fetching Static Hotels For supplier AGODA  City {}", city);
				} finally {
					log.info("Total no of hotels fetched are {}", hotelCount);
				}
			}
		} else {
			cityIds = getAgodaCityIdList();
			log.info("Total number of cityIds are {}", cityIds.size());
			try {
				for (String city : cityIds) {
					try {
						List<HotelInfo> hInfoList = getMasterHotelForCity(city);
						hotelCount += hInfoList.size();
						if (CollectionUtils.isEmpty(hInfoList))
							continue;
						log.info("Number Of Results For City {} is {}", city, hInfoList.size());
						saveHotelInfoList(hInfoList);
					} catch (Exception e) {
						log.error("Error While Fetching Static Hotels For supplier AGODA  City {}", city);
					}
				}
			} finally {
				log.info("Total no of hotels fetched from all cities are {}", hotelCount);
			}
		}
	}

	private void saveHotelInfoList(List<HotelInfo> hInfoList) {

		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				dbHotelInfo.setId(null);
				if (BooleanUtils.isTrue(staticDataRequest.getCacheStaticData())) {
					hotelInfoSaveManager.saveAndCacheHotelInfo(dbHotelInfo, hInfo.getId(), HotelSourceType.AGODA.name(),
							HotelSourceType.AGODA);
				} else {
					hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hInfo.getId(), HotelSourceType.AGODA.name(),
							HotelSourceType.AGODA);
				}
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {}", hInfo.getName(), hInfo.getRating(),
						e);
			}
		}
	}

	private List<HotelInfo> getMasterHotelForCity(String city) throws IOException {

		CityInfoMapping cityMapping = CityInfoMapping.builder().supplierCity(city).build();
		AgodaHotelFeed hotelFeed = fetchHotelFeedFromCityId(cityMapping);
		List<AgodaHotel> agodaHotelList = hotelFeed.getHotels();
		if (hotelFeed == null || CollectionUtils.isEmpty(agodaHotelList))
			return new ArrayList<>();
		List<Future<HotelInfo>> futures = new ArrayList<>();
		for (AgodaHotel agodaHotel : agodaHotelList) {
			Future<HotelInfo> future = executor.submit(() -> {
				HotelInfo hInfo = null;
				try {
					AgodaHotelFeedFull fullHotelFeed = fetchFullHotelFeedFromHotelId(agodaHotel.getHotelId());
					if (!(fullHotelFeed == null || CollectionUtils.isEmpty(fullHotelFeed.getHotels()))) {
						hInfo = fetchHotelFromAgodaData(agodaHotel, fullHotelFeed);
					}
				} catch (Exception e) {
					log.error("Error while fetching static hotel info ", e);
					throw e;
				}
				return hInfo;
			});
			futures.add(future);
		}

		return getHotelListFromFutureList(futures);

	}

	private List<HotelInfo> getHotelListFromFutureList(List<Future<HotelInfo>> futures) {

		List<HotelInfo> hInfoList = new ArrayList<>();
		for (Future<HotelInfo> future : futures) {
			try {
				HotelInfo hInfo = future.get(20, TimeUnit.SECONDS);
				if (!Objects.isNull(hInfo))
					hInfoList.add(hInfo);
			} catch (Exception e) {
				log.error("Error while fetching static hotel info ", e);
			}
		}
		return hInfoList;
	}

	private AgodaHotelFeedFull fetchFullHotelFeedFromHotelId(String hotelId) throws IOException {

		AgodaStaticDataRequest staticRequest = AgodaStaticDataRequest.builder().feed_id("19").build();
		AgodaHotelFeedFull fullHotelFeed = null;
		try {
			staticRequest.setMhotel_id(hotelId);
			staticRequest.setFeed_id("19");
			staticRequest.setOlanguage_id("1");
			HttpUtils httpUtils =
					AgodaUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, staticRequest, supplierConf);
			httpUtils.setRequestMethod("GET");
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (StringUtils.isBlank(xmlResponse))
				return null;
			fullHotelFeed = AgodaMarshaller.unmarshallAgodaFullFeed(xmlResponse);

		} catch (Exception e) {
			log.error("Error while getting full hotel feed from agoda for hotelId {}", hotelId, e);
		}

		return fullHotelFeed;
	}

	private AgodaHotelFeed fetchHotelFeedFromCityId(CityInfoMapping city) throws IOException {

		AgodaStaticDataRequest staticRequest = AgodaStaticDataRequest.builder().feed_id("5").build();
		AgodaHotelFeed hotelFeed = null;
		try {
			staticRequest.setMcity_id(String.valueOf(city.getSupplierCity()));
			HttpUtils httpUtils =
					AgodaUtil.getHttpUtils(null, HotelUrlConstants.HOTEL_STATIC_DATA, staticRequest, supplierConf);
			httpUtils.setRequestMethod("GET");
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (StringUtils.isBlank(xmlResponse))
				return null;
			hotelFeed = AgodaMarshaller.unmarshallAgodaFeed(xmlResponse);
		} catch (Exception e) {
			log.error("Exception While fetching Agoda Feed From City Id {}", city.getSupplierCity(), e);
		}

		return hotelFeed;
	}

	private List<RoomInfo> fetchRoomInfoListFromFacilityFeed(AgodaRoomFacilityFeed facilityFeed) {

		List<RoomInfo> rInfoList = new ArrayList<>();
		Map<String, List<String>> map = new HashMap<>();
		List<AgodaRoomFacility> arfs = facilityFeed.getRoomFacilities();
		if (CollectionUtils.isEmpty(arfs))
			return null;
		for (AgodaRoomFacility arf : arfs) {
			String roomId = arf.getHotel_room_type_id();
			if (!map.containsKey(roomId))
				map.put(roomId, new ArrayList<String>());
			map.get(roomId).add(arf.getProperty_name());
		}

		for (String key : map.keySet()) {
			RoomInfo room = new RoomInfo();
			room.setId(key);
			room.setRoomAmenities(map.get(key));
			rInfoList.add(room);
		}
		return rInfoList;
	}

	private HotelInfo fetchHotelFromAgodaData(AgodaHotel agodaHotel, AgodaHotelFeedFull feed) {

		AgodaHotel agodaFullFeedHotel = feed.getHotels().get(0);

		AgodaPolicy childPolicy = getChildPolicyFromAgodaHotel(agodaHotel);

		List<Instruction> instructions = new ArrayList<>();
		if (StringUtils.isNotEmpty(agodaHotel.getRemark())) {
			Instruction instruction =
					Instruction.builder().type(InstructionType.BOOKING_NOTES).msg(agodaHotel.getRemark()).build();
			instructions.add(instruction);
		}
		String agodaChildPolicy = GsonUtils.getGson().toJson(childPolicy);

		if (StringUtils.isNotEmpty(agodaChildPolicy)) {
			Instruction instruction2 = Instruction.builder().type(InstructionType.CHILD_AND_BED_POLICY)
					.msg(GsonUtils.getGson().toJson(childPolicy)).build();
			instructions.add(instruction2);
		}

		List<Image> images = getImagesFromAgodaData(feed);
		Address hotelAddress = getHotelAddressFromAgodaHotel(feed);
		String shortDescription = getHotelShortDescriptionFromAgodaHotel(feed);
		String longDescription = getHotelLongDescriptionFromAgodaHotel(feed);
		List<String> facilities = getHotelAmenitiesFromAgodaHotel(feed);
		Integer rating =
				agodaFullFeedHotel.getStarRating() != null ? agodaFullFeedHotel.getStarRating().intValue() : null;
		GeoLocation geoLocation = GeoLocation.builder().latitude(agodaFullFeedHotel.getLatitude())
				.longitude(agodaFullFeedHotel.getLongitude()).build();
		List<Option> optionList = fetchOptionFromAgodaRoomTypes(feed);

		HotelInfo hInfo = HotelInfo.builder().name(agodaFullFeedHotel.getHotelName()).id(agodaHotel.getHotelId())
				.images(images).rating(rating).geolocation(geoLocation).address(hotelAddress)
				.description(shortDescription).longDescription(longDescription).facilities(facilities)
				.instructions(instructions).options(optionList).build();
		return hInfo;
	}

	private List<Option> fetchOptionFromAgodaRoomTypes(AgodaHotelFeedFull feed) {

		if (CollectionUtils.isEmpty(feed.getHotelRoomTypeList()))
			return null;
		List<RoomInfo> rInfoList = new ArrayList<>();
		for (AgodaRoomType agodaRoom : feed.getHotelRoomTypeList()) {
			RoomInfo rInfo = getRoomFromAgodaRoom(agodaRoom);
			rInfoList.add(rInfo);
		}
		Option option = Option.builder().roomInfos(rInfoList).build();
		List<Option> optionList = new ArrayList<>(Arrays.asList(option));
		return optionList;
	}

	private RoomInfo getRoomFromAgodaRoom(AgodaRoomType agodaRoom) {
		RoomInfo roomInfo = new RoomInfo();
		roomInfo.setId(agodaRoom.getHotel_room_type_id());
		roomInfo.setRoomType(agodaRoom.getStandard_caption());
		List<String> images = new ArrayList<>(Arrays.asList(agodaRoom.getHotel_room_type_picture()));
		roomInfo.setImages(images);
		return roomInfo;

	}

	private AgodaPolicy getChildPolicyFromAgodaHotel(AgodaHotel agodaHotel) {

		AgodaChildAndExtraBedPolicy childPolicy = agodaHotel.getChildAndExtraBedPolicy();
		AgodaPolicyBuilder policyBuilder = AgodaPolicy.builder();
		if (childPolicy == null)
			return policyBuilder.build();
		AgodaPolicy policy = policyBuilder.children_age_from(childPolicy.getChildrenAgeFrom())
				.children_age_to(childPolicy.getChildrenAgeTo()).children_stay_free(childPolicy.getChildrenStayFree())
				.infant_age(childPolicy.getInfantAge()).min_guest_age(childPolicy.getMinGuestAge()).build();

		return policy;
	}

	private List<String> getHotelAmenitiesFromAgodaHotel(AgodaHotelFeedFull feed) {
		List<String> hotelAmenities = new ArrayList<>();
		List<AgodaHotelFacility> agodaFacilityList = feed.getHotelFacilityList();
		if (CollectionUtils.isEmpty(agodaFacilityList))
			return null;
		for (AgodaHotelFacility agodaFacility : agodaFacilityList) {
			hotelAmenities.add(agodaFacility.getPropertyName());
		}
		return hotelAmenities;
	}

	private String getHotelShortDescriptionFromAgodaHotel(AgodaHotelFeedFull feed) {

		String shortDescription = null;
		List<AgodaHotelDescription> agodaDescriptionList = feed.getHotelDescriptionList();
		if (CollectionUtils.isEmpty(agodaDescriptionList))
			return null;
		for (AgodaHotelDescription agodaHotelDescription : agodaDescriptionList) {
			shortDescription = agodaHotelDescription.getSnippet();
		}
		return shortDescription;
	}


	private String getHotelLongDescriptionFromAgodaHotel(AgodaHotelFeedFull feed) {

		String longDescription = null;
		List<AgodaHotelDescription> agodaDescriptionList = feed.getHotelDescriptionList();
		if (CollectionUtils.isEmpty(agodaDescriptionList))
			return null;
		for (AgodaHotelDescription agodaHotelDescription : agodaDescriptionList) {
			longDescription = agodaHotelDescription.getOverview();
		}
		return longDescription;
	}

	private Address getHotelAddressFromAgodaHotel(AgodaHotelFeedFull feed) {
		Address address = null;
		List<AgodaHotelAddress> agodaAddressList = feed.getAddressList();
		if (CollectionUtils.isEmpty(agodaAddressList))
			return address;
		for (AgodaHotelAddress agodaAddress : agodaAddressList) {
			String addressType = agodaAddress.getAddressType();
			if ("English address".equalsIgnoreCase(addressType)) {
				City city = City.builder().name(agodaAddress.getCity()).build();
				Country country = Country.builder().name(agodaAddress.getCountry()).build();
				address = Address.builder().addressLine1(agodaAddress.getAddressLine1()).city(city).country(country)
						.postalCode(agodaAddress.getPostalCode()).build();
			}
		}
		return address;
	}

	private List<Image> getImagesFromAgodaData(AgodaHotelFeedFull feed) {
		List<Image> imageList = new ArrayList<>();
		List<AgodaHotelImage> agodaImages = feed.getHotelImageList();
		if (CollectionUtils.isEmpty(agodaImages))
			return imageList;
		for (AgodaHotelImage agodaImage : agodaImages) {
			String imageUrl = agodaImage.getUrl().replace("http:", "https:");
			Image image = Image.builder().bigURL(imageUrl).thumbnail(imageUrl).build();
			imageList.add(image);
		}
		return imageList;
	}

	private List<String> getAgodaCityIdList() {
		if (CollectionUtils.isEmpty(staticDataRequest.getCityIds())) {
			List<CityInfoMapping> cityInfoMappings =
					cityInfoMappingManager.findBySupplierName(HotelSourceType.AGODA.name());
			return cityInfoMappings.stream().map(CityInfoMapping::getSupplierCity).collect(Collectors.toList());
		} else {
			return staticDataRequest.getCityIds();
		}
	}

}

