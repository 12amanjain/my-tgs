package com.tgs.services.hms.sources.agoda;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.context.annotation.Bean;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingDetailsResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.BookingResponseV3;
import com.tgs.services.hms.datamodel.agoda.precheck.CancellationResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.ConfirmCancellationResponseV2;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckResponse;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchResponse;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFeed;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaHotelFeedFull;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaRoomFacilityFeed;
import com.tgs.services.hms.datamodel.agoda.statc.CityFeed;
import com.tgs.services.hms.datamodel.agoda.statc.CountryFeed;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class AgodaMarshaller {

	static Jaxb2Marshaller marshaller2;

	public static void init(Jaxb2Marshaller jaxb2) {
		marshaller2 = jaxb2;

	}

	public static <T> String marshallXml(final T obj) throws JAXBException {
		StringWriter sw = new StringWriter();
		StreamResult result = new StreamResult(sw);
		marshaller2.marshal(obj, result);
		return sw.toString();
	}

	@SuppressWarnings("unchecked")
	public static <T> T unmarshallXML(String data, Class<T> T) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (T) marshaller2.unmarshal(new StreamSource(targetStream));

	}


	@SuppressWarnings("unchecked")
	public static CountryFeed unmarshallCountryFeed(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (CountryFeed) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static CityFeed unmarshallCityFeed(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (CityFeed) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static AgodaSearchResponse unmarshallLongSearchResponse(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (AgodaSearchResponse) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static AgodaHotelFeed unmarshallAgodaFeed(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (AgodaHotelFeed) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static AgodaHotelFeedFull unmarshallAgodaFullFeed(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (AgodaHotelFeedFull) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static AgodaRoomFacilityFeed unmarshallAgodaFacilityFeed(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (AgodaRoomFacilityFeed) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static PrecheckResponse unmarshallPreCheckResponse(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (PrecheckResponse) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static BookingResponseV3 unmarshallBookingResponse(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (BookingResponseV3) marshaller2.unmarshal(new StreamSource(targetStream));

	}

	@SuppressWarnings("unchecked")
	public static BookingDetailsResponseV2 unmarshallBookingDetailResponse(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (BookingDetailsResponseV2) marshaller2.unmarshal(new StreamSource(targetStream));

	}
	@SuppressWarnings("unchecked")
	public static CancellationResponseV2 unmarshallCancellationResponse(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (CancellationResponseV2) marshaller2.unmarshal(new StreamSource(targetStream));

	}
	@SuppressWarnings("unchecked")
	public static ConfirmCancellationResponseV2 unmarshallConfirmCancellationResponse(String data) {

		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (ConfirmCancellationResponseV2) marshaller2.unmarshal(new StreamSource(targetStream));

	}
}
