package com.tgs.services.hms.sources.agoda;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;
@Service
public class AgodaPriceValidationFactory extends AbstractHotelPriceValidationFactory {
	
	@Autowired
	HotelCacheHandler cacheHandler;

	public AgodaPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		
		AgodaPriceValidationService priceValidationService = AgodaPriceValidationService.builder()
				.searchQuery(this.searchQuery).bookingId(bookingId)
				.supplierConf(this.getSupplierConf()).build();
		priceValidationService.validate(this.getHInfo());
		return this.getHInfo().getOptions().get(0);
		
	}

}
