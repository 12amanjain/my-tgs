package com.tgs.services.hms.sources.agoda;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.agoda.precheck.HotelRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckDetailsRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckRequest;
import com.tgs.services.hms.datamodel.agoda.precheck.PrecheckResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Getter
@Setter
@Slf4j
@SuperBuilder
public class AgodaPriceValidationService extends AgodaBaseService {

	private String bookingId;

	public void validate(HotelInfo hInfo) throws Exception {

		try {
			listener = new RestAPIListener("");
			PrecheckRequest preCheckRequest = createPreCheckRequest(hInfo);
			String xmlRequest = AgodaMarshaller.marshallXml(preCheckRequest);

			httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.BLOCK_ROOM_URL, null, supplierConf);
			log.info("Price Validation Request is {}", httpUtils.getPostData());
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			if (xmlResponse == null) {
				log.error("Unable to get response {}", preCheckRequest, xmlResponse);
			}
			PrecheckResponse result = AgodaMarshaller.unmarshallPreCheckResponse(xmlResponse);
			if (!(result.getStatus().equals(200))) {
				log.error(result.getMessage());
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
			}
		}catch(Exception e) {
			log.error("Error While validation of price {}",bookingId, e);
			throw e;
		}finally {
			storeLogs("Agoda-PriceCheck", bookingId);
		}

	}

	private PrecheckRequest createPreCheckRequest(HotelInfo hInfo) {

		PrecheckRequest precheckRequest = new PrecheckRequest();
		HotelRequest hotel = new HotelRequest();

		hotel.setId(hInfo.getMiscInfo().getSupplierStaticHotelId());
		setRooms(hotel, hInfo);
		precheckRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		precheckRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		PrecheckDetailsRequest precheckDetailRequest = new PrecheckDetailsRequest();
		precheckDetailRequest.setSearchid(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		precheckDetailRequest.setTag(bookingId);
		precheckDetailRequest.setAllowDuplication(true);
		precheckDetailRequest.setUserCountry("IN");
		precheckDetailRequest.setCheckIn(searchQuery.getCheckinDate().toString());
		precheckDetailRequest.setCheckOut(searchQuery.getCheckoutDate().toString());
		precheckDetailRequest.setHotel(hotel);
		precheckRequest.setPrecheckDetails(precheckDetailRequest);
		return precheckRequest;
	}


}
