package com.tgs.services.hms.sources.agoda;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchRequest;
import com.tgs.services.hms.datamodel.agoda.search.AgodaSearchResponse;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel;
import com.tgs.services.hms.datamodel.agoda.search.AvailabilityLongResponseV2.Hotels.Hotel.Rooms.Room;
import com.tgs.services.hms.datamodel.agoda.search.ChildrenAges;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;

import lombok.Getter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Slf4j
@SuperBuilder
public class AgodaSearchService extends AgodaBaseService {

	private HotelSearchResult searchResult;
	private Set<String> hotelIds;

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public void doSearch() throws IOException, JAXBException {
		if (isValid()) {
			listener = new RestAPIListener("");
			AgodaSearchRequest searchRequest = createSearchRequest();
			try {

				String xmlRequest = AgodaMarshaller.marshallXml(searchRequest);
				log.info("Request of Agoda {}, {}", xmlRequest);
				httpUtils = AgodaUtil.getHttpUtils(xmlRequest, HotelUrlConstants.HOTEL_SEARCH_URL, null, supplierConf);
				String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
				if (xmlResponse == null) {
					log.error("Unable to get response {}", searchQuery, xmlResponse);
				}

				AgodaSearchResponse Result = AgodaMarshaller.unmarshallLongSearchResponse(xmlResponse);
				log.info("Response of Agoda {}, {}", searchQuery, Result);
				List<HotelInfo> hotelList = getHotelListFromAgodaResult(Result);
				searchResult = new HotelSearchResult();
				searchResult.setHotelInfos(hotelList);

				SystemContextHolder.getContextData().addCheckPoint(
						CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
								.time(System.currentTimeMillis()).build());

			} finally {

				storeLogs("Agoda-Search", searchQuery.getSearchId());
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (ObjectUtils.isEmpty(searchResult)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}

			}
		}
	}

	private AgodaSearchRequest createSearchRequest() {

		AgodaSearchRequest searchRequest = new AgodaSearchRequest();
		searchRequest.setApikey(supplierConf.getHotelSupplierCredentials().getApiKey());
		searchRequest.setSiteid(supplierConf.getHotelSupplierCredentials().getClientId());
		setOccupancy(searchRequest);
		String hotelIdList = String.join(",", hotelIds);
		searchRequest.setId(hotelIdList);
		searchRequest.setCheckIn(searchQuery.getCheckinDate().format(dateTimeFormatter));
		searchRequest.setCheckOut(searchQuery.getCheckoutDate().format(dateTimeFormatter));
		searchRequest.setCurrency(AgodaConstants.CURRENCY.getValue());
		searchRequest.setRooms(searchQuery.getRoomInfo().size() + "");
		searchRequest.setType("6");
		searchRequest.setLanguage(AgodaConstants.LANGUAGECODE.getValue());
		searchRequest.setUserCountry("IN");
		return searchRequest;
	}


	private void setOccupancy(AgodaSearchRequest searchRequest) {

		int totalNumberOfAdults = 0;
		int totalNumberOfchildren = 0;
		List<Integer> age = new ArrayList<Integer>();
		ChildrenAges childrenAges = new ChildrenAges();
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			totalNumberOfAdults += room.getNumberOfAdults();
			if (CollectionUtils.isNotEmpty(room.getChildAge()) && !Objects.isNull(room.getNumberOfChild())
					&& room.getNumberOfChild() > 0) {
				totalNumberOfchildren += room.getNumberOfChild();
				for (Integer childAge : room.getChildAge()) {
					age.add(childAge);
				}
			}
		}
		searchRequest.setAdults(totalNumberOfAdults + "");
		searchRequest.setChildren(totalNumberOfchildren + "");
		if (age.size() > 0) {
			childrenAges.setAge(age);
			searchRequest.setChildrenAges(childrenAges);
		}

	}

	protected List<HotelInfo> getHotelListFromAgodaResult(AgodaSearchResponse result) {

		List<HotelInfo> hInfos = new ArrayList<>();
		if (result == null || CollectionUtils.isEmpty(result.getHotels())) {
			log.error("Empty Response From Supplier {} for searchId {}", supplierConf.getBasicInfo().getSupplierId(),
					searchQuery.getSearchId());
			return null;
		}
		for (Hotel hotel : result.getHotels()) {
			List<Option> optionList = new ArrayList<>();
			for (Room agodaRoom : hotel.getRooms().getRoom()) {
				if (validRoomOcuupancy(agodaRoom)) {
					List<RoomInfo> roomList = getRooms(agodaRoom);
					Option option = Option.builder().roomInfos(roomList)
							.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
									.supplierSearchId(result.getSearchid()).supplierHotelId(hotel.getId())
									.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
							.id(RandomStringUtils.random(20, true, true)).build();
					setOptionCancellationPolicy(agodaRoom, option);
					setOptionSurcharge(option, roomList.get(0));
					setOptionBenefits(option, agodaRoom);
					optionList.add(option);
				}
			}
			HotelInfo hInfo = HotelInfo.builder().miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
					.supplierStaticHotelId(hotel.getId()).build()).options(optionList).build();

			hInfos.add(hInfo);

		}
		return hInfos;

	}

}

