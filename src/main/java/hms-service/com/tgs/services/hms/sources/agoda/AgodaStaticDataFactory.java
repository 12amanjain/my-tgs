package com.tgs.services.hms.sources.agoda;


import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class AgodaStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	protected HotelCacheHandler hotelCacheHandler;

	@Autowired
	private HotelCityInfoMappingManager cityInfoMappingManager;

	public AgodaStaticDataFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException {

		AgodaHotelStaticDataService staticDataService = AgodaHotelStaticDataService.builder()
				.cityInfoMappingManager(cityInfoMappingManager).hotelInfoSaveManager(hotelInfoSaveManager)
				.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).staticDataRequest(staticDataRequest)
				.build();
		staticDataService.process();
	}

	@Override
	protected void getCityMappingData() throws IOException {
		AgodaCityStaticDataService staticCityService = AgodaCityStaticDataService.builder().supplierConf(supplierConf)
				.staticDataRequest(staticDataRequest).build();
		staticCityService.init();
		List<HotelCityInfoQuery> cityData = staticCityService.getAgodaCityList();
		log.info("agoda city size : " + cityData.size());
		staticCityService.saveIntoDB(cityData);
	}

}
