package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.cleartrip.CleartripConfirmBookingRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripCreateBookingResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripCreateBookingSuccessResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripPaymentInfo;
import com.tgs.services.hms.datamodel.cleartrip.CleartripProvisionalBookingRequest;
import com.tgs.services.hms.datamodel.cleartrip.Customer;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Builder
@Slf4j
public class CleartripBookingService {

	private GeneralServiceCommunicator gnComm;
	private HotelInfo hInfo;
	private Order order;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private CleartripCreateBookingResponse bookingResponse;
	protected RestAPIListener listener;

	private static final String PROVISIONAL_BOOKING_SUFFIX = "/hotels/api/v2/provisional-book";
	private static final String CONFIRM_BOOKING_SUFFIX = "/hotels/api/v2/book";

	private static final DateTimeFormatter dateTimeFormatter_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public boolean book() throws IOException {
		HttpUtilsV2 httpUtils = null;
		CleartripProvisionalBookingRequest provisionalBookingRequest = null;
		CleartripCreateBookingResponse bookingResponse = null;
		String requestURL = "";
		try {
			listener = new RestAPIListener("");
			provisionalBookingRequest = createProvisionalBookingRequest();
			httpUtils = CleartripUtils.getResponseURLWithRequestBody(provisionalBookingRequest, supplierConf);
			requestURL = httpUtils.getUrlString() + "/n" + httpUtils.getPostData();
			httpUtils.setPrintResponseLog(true);
			bookingResponse = httpUtils.getResponse(CleartripCreateBookingResponse.class).orElseGet(null);
			log.info("Response for provisional booking for request {} is {}", requestURL,
					GsonUtils.getGson().toJson(bookingResponse));
			boolean bookingStatus = confirmBooking(bookingResponse);

			return bookingStatus;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(bookingResponse)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			listener.addLog(LogData.builder().key(order.getBookingId())
					.logData(requestURL)
					.type("Cleartrip-ProvisionalBookingReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Cleartrip-ProvisionalBookingRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private boolean confirmBooking(CleartripCreateBookingResponse bookingResponse) throws IOException {

		boolean bookingStatus = false;
		if (bookingResponse == null) {
			SystemContextHolder.getContextData().getErrorMessages().add("Booking is failed due to empty response");
			log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
					this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(), "empty Response");
			return bookingStatus;
		}

		if (ObjectUtils.isEmpty(bookingResponse.getError())) {

			HttpUtilsV2 httpUtils = null;
			CleartripConfirmBookingRequest confirmBookingRequest = null;
			String requestURL = "";
			try {
				listener = new RestAPIListener("");
				confirmBookingRequest = createConfirmBookingRequest(bookingResponse.getSuccess());
				httpUtils = CleartripUtils.getResponseURLWithRequestBody(confirmBookingRequest, supplierConf);
				requestURL = httpUtils.getUrlString() + "/n" + httpUtils.getPostData();
				httpUtils.setPrintResponseLog(true);
				this.bookingResponse = httpUtils.getResponse(CleartripCreateBookingResponse.class).orElseGet(null);
				log.info("Response for booking confirmation for request {} is {}", requestURL,
						GsonUtils.getGson().toJson(this.bookingResponse));
				bookingStatus = updateBookingStatus();
				return bookingStatus;
			} finally {
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (ObjectUtils.isEmpty(bookingResponse)) {
					SystemContextHolder.getContextData().getErrorMessages()
							.add(httpUtils.getResponseString() + httpUtils.getPostData());
				}
				listener.addLog(
						LogData.builder().key(order.getBookingId())
								.logData(httpUtils.getUrlString() + "/n" + httpUtils.getPostData())
								.type("Cleartrip-ConfirmBookingReq")
								.generationTime(Instant
										.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
								.build());
				listener.addLog(
						LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
								.type("Cleartrip-ConfirmBookingRes")
								.generationTime(Instant
										.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
								.build());
			}
		} else {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Booking is failed due to " + bookingResponse.getError().getDetailedMessage());
			log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
					this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(),
					bookingResponse.getError().getDetailedMessage());
		}
		return bookingStatus;
	}

	private boolean updateBookingStatus() throws IOException {

		boolean bookingStatus = false;
		if (bookingResponse == null) {
			SystemContextHolder.getContextData().getErrorMessages().add("Booking is failed due to empty response");
			log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
					this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(), "empty Response");
			return bookingStatus;
		}

		if (!ObjectUtils.isEmpty(bookingResponse.getError())) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Booking is failed due to " + bookingResponse.getError().getDetailedMessage());
			log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
					this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(),
					bookingResponse.getError().getDetailedMessage());
			return bookingStatus;
		}

		if (!ObjectUtils.isEmpty(bookingResponse.getSuccess())) {
			updateSupplierBookingReferenceId();
			bookingStatus = true;
		}

		return bookingStatus;
	}

	private void updateSupplierBookingReferenceId() {

		CleartripCreateBookingSuccessResponse bookingSuccessResponse = bookingResponse.getSuccess();
		hInfo.getMiscInfo().setSupplierBookingId(order.getBookingId());
		hInfo.getMiscInfo().setSupplierBookingConfirmationNo(bookingSuccessResponse.getConfirmationNumber());
		hInfo.getMiscInfo().setSupplierBookingReference(bookingSuccessResponse.getTripId());
	}

	private CleartripConfirmBookingRequest createConfirmBookingRequest(
			CleartripCreateBookingSuccessResponse bookingSuccessResponse) {
		CleartripPaymentInfo paymentInfo =
				CleartripPaymentInfo.builder().depositAccountId(sourceConfigOutput.getDepositId()).build();
		return CleartripConfirmBookingRequest.builder().affiliateTripRef(order.getBookingId())
				.itineraryId(bookingSuccessResponse.getItineraryId()).suffixOfURL(CONFIRM_BOOKING_SUFFIX)
				.payment(paymentInfo).build();
	}

	private CleartripProvisionalBookingRequest createProvisionalBookingRequest() {
		Option option = hInfo.getOptions().get(0);
		BigDecimal totalAmount = new BigDecimal(0);
		DeliveryInfo deliveryInfo = order.getDeliveryInfo();
		String mobile = deliveryInfo.getContacts().get(0);
		String email = deliveryInfo.getEmails().get(0);
		List<Customer> customers = new ArrayList<>();
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			totalAmount = totalAmount.add(roomInfo.getMiscInfo().getTotalBaseAmount());
			TravellerInfo travellerInfo = roomInfo.getTravellerInfo().get(0);
			Customer customer =
					Customer.builder().title(travellerInfo.getTitle()).firstName(travellerInfo.getFirstName())
							.lastName(travellerInfo.getLastName()).mobile(mobile).email(email).build();
			customers.add(customer);
		}

		RoomInfo firstRoomInfo = option.getRoomInfos().get(0);
		return CleartripProvisionalBookingRequest.builder().affiliateTxnId(order.getBookingId())
				.checkInDate(firstRoomInfo.getCheckInDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkOutDate(firstRoomInfo.getCheckOutDate().format(dateTimeFormatter_YYYY_MM_DD))
				.roomTypeCode(option.getMiscInfo().getRoomTypeCode()).hotelId(option.getMiscInfo().getSupplierHotelId())
				.bookingCode(option.getMiscInfo().getBookingCode()).nri("false")
				.occupancy(CleartripUtils.createOccupancy(option.getRoomInfos())).customerDetails(customers)
				.suffixOfURL(PROVISIONAL_BOOKING_SUFFIX).bookingAmount(totalAmount.toString()).build();
	}
}
