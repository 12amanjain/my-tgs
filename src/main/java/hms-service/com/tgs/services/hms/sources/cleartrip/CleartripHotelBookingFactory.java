package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class CleartripHotelBookingFactory extends AbstractHotelBookingFactory {

	public CleartripHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException {
		CleartripBookingService bookingService = CleartripBookingService.builder().hInfo(this.getHotel()).order(order).supplierConf(supplierConf)
				.sourceConfigOutput(sourceConfigOutput).build();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
	}

}