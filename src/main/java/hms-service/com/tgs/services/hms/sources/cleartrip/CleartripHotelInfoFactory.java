package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;

@Service
public class CleartripHotelInfoFactory extends AbstractHotelInfoFactory {

	public CleartripHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	public void searchAvailableHotels() throws IOException {
		CityInfoMapping supplierCityInfo = cacheHandler.getSupplierCityInfoFromCityId(
				searchQuery.getSearchCriteria().getCityId(), CleartripConstants.CLEARTRIP_SOURCE_NAME);
		CleartripSearchService searchService = CleartripSearchService.builder().supplierConfig(this.getSupplierConf())
				.searchQuery(this.getSearchQuery()).sourceConfig(sourceConfigOutput).supplierCityInfo(supplierCityInfo)
				.build();
		searchService.init();
		searchService.doSearch();
		searchResult = searchService.getSearchResult();
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {
		CityInfoMapping supplierCityInfo = cacheHandler.getSupplierCityInfoFromCityId(
				searchQuery.getSearchCriteria().getCityId(), CleartripConstants.CLEARTRIP_SOURCE_NAME);
		CleartripSearchService searchService = CleartripSearchService.builder().sourceConfig(sourceConfigOutput)
				.searchQuery(this.getSearchQuery()).supplierConfig(this.getSupplierConf())
				.supplierCityInfo(supplierCityInfo).hotelInfo(hInfo).build();
		searchService.init();
		searchService.doDetailSearch();
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		boolean isCancellationPolicyAlreadySet = false;
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option cachedOption = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		for (Option op : hotel.getOptions()) {
			if (op.getId().equals(optionId) && !ObjectUtils.isEmpty(cachedOption.getCancellationPolicy())) {
				op.setCancellationPolicy(cachedOption.getCancellationPolicy());
				isCancellationPolicyAlreadySet = true;
			}
		}
		if (!isCancellationPolicyAlreadySet) {
			CleartripCancellationPolicyService cancellationPolicyService =
					CleartripCancellationPolicyService.builder().hInfo(hotel).searchQuery(searchQuery)
							.supplierConf(this.getSupplierConf()).sourceConfigOutput(sourceConfigOutput).build();
			cancellationPolicyService.searchCancellationPolicy(logKey);
		}
	}
}

