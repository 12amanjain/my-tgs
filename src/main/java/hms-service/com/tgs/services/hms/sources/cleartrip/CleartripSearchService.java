package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.cleartrip.CleartripHotelDetailRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripHotelInfo;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRoomRate;
import com.tgs.services.hms.datamodel.cleartrip.CleartripRoomType;
import com.tgs.services.hms.datamodel.cleartrip.CleartripSearchRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripSearchResponse;
import com.tgs.services.hms.datamodel.cleartrip.HotelBasicInfo;
import com.tgs.services.hms.datamodel.cleartrip.Locality;
import com.tgs.services.hms.datamodel.cleartrip.PricingElement;
import com.tgs.services.hms.datamodel.cleartrip.RateBreakDown;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class CleartripSearchService {

	protected RestAPIListener listener;
	private GeneralServiceCommunicator gsCommunicator;
	private HotelSearchQuery searchQuery;
	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConfig;
	private CleartripSearchResponse searchResponse;
	private HotelSearchResult searchResult;
	private HotelInfo hotelInfo;
	private CityInfoMapping supplierCityInfo;

	private static final DateTimeFormatter dateTimeFormatter_YYYY_MM_DD = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final String HOTEL_SEARCH_PREFIX = "/hotels/api/v2/search";
	private static final String HOTEL_DETAIL_PREFIX = "/hotels/api/v2/detail";

	// Possible values are basic, detail and no
	private static String HOTEL_SEARCH_GRANULARITY;
	private static String BASE_URL;

	public void init() {

		gsCommunicator = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
		BASE_URL = supplierConfig.getHotelSupplierCredentials().getUrl();
		HOTEL_SEARCH_GRANULARITY = sourceConfig.getHotelInfoGranularity();
	}

	@SuppressWarnings({"unchecked"})
	public void doSearch() throws IOException {
		HttpUtilsV2 httpUtils = null;
		try {
			listener = new RestAPIListener("");
			CleartripSearchRequest searchRequest = createSearchRequest();
			httpUtils = CleartripUtils.getResponseURL(searchRequest, supplierConfig);
			httpUtils.setPrintResponseLog(false);
			searchResponse = httpUtils.getResponse(CleartripSearchResponse.class).orElseGet(null);
			searchResult = createSearchResult(searchResponse);
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getUrlString())
					.type("Cleartrip-SearchReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());

			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("Cleartrip-SearchRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	@SuppressWarnings({"unchecked"})
	public void doDetailSearch() throws IOException {

		HttpUtilsV2 httpUtils = null;
		try {
			listener = new RestAPIListener("");
			CleartripSearchRequest searchRequest = createDetailRequest();
			httpUtils = CleartripUtils.getResponseURL(searchRequest, supplierConfig);
			httpUtils.setPrintResponseLog(false);
			searchResponse = httpUtils.getResponse(CleartripSearchResponse.class).orElseGet(null);
			createDetailResult(searchResponse);
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getUrlString())
					.type("Cleartrip-DetailReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());

			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("Cleartrip-DetailRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private CleartripSearchRequest createSearchRequest() {

		String countryCode =
				gsCommunicator.getCountryCode(HotelUtils.toCamelCase(searchQuery.getSearchCriteria().getCountryName()));
		return CleartripSearchRequest.builder().country(countryCode).hotelInfo(HOTEL_SEARCH_GRANULARITY)
				.checkInDate(searchQuery.getCheckinDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkOutDate(searchQuery.getCheckoutDate().format(dateTimeFormatter_YYYY_MM_DD))
				.city(supplierCityInfo.getSupplierCityName()).suffixOfURL(HOTEL_SEARCH_PREFIX)
				.occupancy(CleartripUtils.getOccupancies(searchQuery)).build();
	}

	private CleartripHotelDetailRequest createDetailRequest() {

		String countryCode =
				gsCommunicator.getCountryCode(HotelUtils.toCamelCase(searchQuery.getSearchCriteria().getCountryName()));
		return CleartripHotelDetailRequest.builder().country(countryCode).sellingCountry(countryCode)
				.sellingCurrency(searchQuery.getSearchPreferences().getCurrency()).hotelInfo(HOTEL_SEARCH_GRANULARITY)
				.checkInDate(searchQuery.getCheckinDate().format(dateTimeFormatter_YYYY_MM_DD))
				.checkOutDate(searchQuery.getCheckoutDate().format(dateTimeFormatter_YYYY_MM_DD))
				.city(supplierCityInfo.getSupplierCityName()).suffixOfURL(HOTEL_DETAIL_PREFIX)
				.hotelId(hotelInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId())
				.occupancy(CleartripUtils.getOccupancies(searchQuery)).build();
	}

	private void createDetailResult(CleartripSearchResponse searchResponse) {
		searchResult = new HotelSearchResult();
		if (searchResponse == null)
			return;

		if (!ObjectUtils.isEmpty(searchResponse.getError())) {
			SystemContextHolder.getContextData().getErrorMessages().add(searchResponse.getError().getErrorMessage());
			log.error("Unable to get search result from Cleartrip due to {} for supplier {} with username {}",
					searchResponse.getError().getErrorMessage(), supplierConfig.getBasicInfo().getSupplierName(),
					supplierConfig.getHotelSupplierCredentials().getUserName());
			return;
		}

		CleartripHotelInfo cHotelInfo = null;
		Set<String> amenities = new HashSet<>();
		try {
			cHotelInfo = searchResponse.getSuccess().getHotels().get(0);
			HotelBasicInfo basicInfo = cHotelInfo.getBasicInfo();
			Locality locality = basicInfo.getLocality();
			Address address = Address.builder().addressLine1(locality.getAddress()).postalCode(locality.getZip())
					.city(City.builder().name(locality.getCity()).build())
					.country(Country.builder().name(locality.getCountry()).build())
					.state(State.builder().name(locality.getState()).build()).build();

			GeoLocation geolocation = GeoLocation.builder().latitude(locality.getLocalityLatitude())
					.longitude(locality.getLocalityLongitude()).build();

			hotelInfo.setAddress(address).setGeolocation(geolocation).setDescription(basicInfo.getOverview())
					.setName(basicInfo.getHotelName())
					.setImages(
							Arrays.asList(Image.builder().thumbnail(BASE_URL + basicInfo.getThumbNailImage()).build()));
			Integer rating = NumberUtils.isParsable(basicInfo.getStarRating())
					? (int) Double.parseDouble(basicInfo.getStarRating())
					: null;
			hotelInfo.setRating(rating);
			List<Option> optionList = populateRoomInfo(cHotelInfo, amenities);
			for (Option option : optionList) {
				option.getMiscInfo().setSecondarySupplier(supplierConfig.getBasicInfo().getSupplierId());
				option.getMiscInfo().setIsDetailHit(true);
			}
			hotelInfo.setOptions(optionList);
			CleartripUtils.setMealBasis(Arrays.asList(hotelInfo), amenities, searchQuery);
		} catch (Exception e) {
			log.info("Unable to parse hotel detail info for hotel {} for search id {}",
					GsonUtils.getGson().toJson(cHotelInfo), searchQuery.getSearchId(), e);
		}
	}

	private HotelSearchResult createSearchResult(CleartripSearchResponse searchResponse) {
		searchResult = new HotelSearchResult();
		if (searchResponse == null)
			return searchResult;

		if (!ObjectUtils.isEmpty(searchResponse.getError())) {
			SystemContextHolder.getContextData().getErrorMessages().add(searchResponse.getError().getErrorMessage());
			log.error("Unable to get search result from Cleartrip due to {} for supplier {} with username {}",
					searchResponse.getError().getErrorMessage(), supplierConfig.getBasicInfo().getSupplierName(),
					supplierConfig.getHotelSupplierCredentials().getUserName());
			return searchResult;
		}

		List<HotelInfo> hotels = new ArrayList<>();
		Set<String> amenities = new HashSet<>();
		for (CleartripHotelInfo cHotelInfo : searchResponse.getSuccess().getHotels()) {
			try {
				HotelBasicInfo basicInfo = cHotelInfo.getHotelBasicInfo();
				Locality locality = basicInfo.getLocality();
				Address address = Address.builder().addressLine1(locality.getName()).postalCode(locality.getZip())
						.city(City.builder().name(locality.getCity()).build())
						.country(Country.builder().name(locality.getCountry()).build())
						.state(State.builder().name(locality.getState()).build()).build();

				GeoLocation geolocation = GeoLocation.builder().latitude(locality.getLatitude())
						.longitude(locality.getLongitude()).build();

				HotelInfo hInfo = HotelInfo.builder().address(address).geolocation(geolocation)
						.description(basicInfo.getOverview()).name(basicInfo.getHotelName())
						.images(Arrays
								.asList(Image.builder().thumbnail(BASE_URL + basicInfo.getThumbNailImage()).build()))
						.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
								.supplierStaticHotelId(cHotelInfo.getHotelId())
								.searchKeyExpiryTime(
										LocalDateTime.now().plusMinutes(sourceConfig.getSearchKeyExpirationTime()))
								.build())
						.build();
				Integer rating = NumberUtils.isParsable(basicInfo.getStarRating())
						? (int) Double.parseDouble(basicInfo.getStarRating())
						: null;
				hInfo.setRating(rating);
				List<Option> optionList = populateRoomInfo(cHotelInfo, amenities);
				for (Option option : optionList) {
					option.getMiscInfo().setIsNotRequiredOnDetail(true);
				}
				hInfo.setOptions(optionList);
				hotels.add(hInfo);
			} catch (Exception e) {
				log.info("Unable to parse hotel search info for hotel {} for search id {}",
						GsonUtils.getGson().toJson(cHotelInfo), searchQuery.getSearchId(), e);
			}
		}
		CleartripUtils.setMealBasis(hotels, amenities, searchQuery);
		searchResult.setHotelInfos(hotels);
		return searchResult;
	}

	private List<Option> populateRoomInfo(CleartripHotelInfo cHotelInfo, Set<String> amenities) {
		List<CleartripRoomRate> cRoomRates = cHotelInfo.getRoomRates();
		List<Option> options = new ArrayList<>();

		for (CleartripRoomRate roomRate : cRoomRates) {
			amenities.addAll(roomRate.getInclusions());
			Option option = Option.builder().build();
			List<RoomInfo> roomInfos = new ArrayList<>();
			RoomInfo roomInfo = new RoomInfo();
			CleartripRoomType roomType = roomRate.getRoomType();
			roomInfo.setRoomCategory(roomType.getRoomTypeName());
			roomInfo.setRoomType(roomType.getRoomTypeName());
			roomInfo.setRoomAmenities(roomRate.getInclusions());
			roomInfo.setId(roomType.getRoomTypeCode());
			populatePriceInRoomInfo(roomInfo, roomRate.getRateBreakdown(), searchQuery.getRoomInfo().size());
			roomInfo.getMiscInfo().setAmenities(roomRate.getInclusions());
			roomInfos.add(roomInfo);
			CleartripUtils.updatePriceWithClientCommissionComponents(roomInfos, sourceConfig);
			roomInfos = CleartripUtils.createMultipleRoomsOfSameRoomType(roomInfo, searchQuery);
			option.setMiscInfo(OptionMiscInfo.builder().supplierId(supplierConfig.getBasicInfo().getSupplierId())
					.supplierHotelId(cHotelInfo.getHotelId()).sourceId(supplierConfig.getBasicInfo().getSourceId())
					.bookingCode(roomRate.getBookingCode()).roomTypeCode(roomType.getRoomTypeCode()).build());

			option.setRoomInfos(roomInfos);
			option.setId(roomType.getRoomTypeCode().replaceAll(":", "_"));
			options.add(option);
		}
		return options;
	}

	public void populatePriceInRoomInfo(RoomInfo roomInfo, List<RateBreakDown> rateBreakDowns,
			int noOfRooms) {
		int i = 1;
		Double total = 0.0;
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (RateBreakDown rateBreakDown : rateBreakDowns) {
			PriceInfo priceInfo = new PriceInfo();
			List<PricingElement> pricingElements = rateBreakDown.getPricingElements();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			for (PricingElement pricingElement : pricingElements) {
				if (pricingElement.getCategory().equals("BF")) {
					fareComponents.put(HotelFareComponent.BF, pricingElement.getAmount() / noOfRooms);
					total += fareComponents.get(HotelFareComponent.BF);
				}

				if (pricingElement.getCategory().equals("TAX")) {
					fareComponents.put(HotelFareComponent.TSF, pricingElement.getAmount() / noOfRooms);
					total += fareComponents.get(HotelFareComponent.TSF);
				}

				if (pricingElement.getCategory().equals("DIS")) {
					fareComponents.put(HotelFareComponent.SDS, pricingElement.getAmount() / noOfRooms);
					total += fareComponents.get(HotelFareComponent.SDS);
				}
			}

			priceInfo.setDay(i);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
			i++;
		}

		roomInfo.setMiscInfo(RoomMiscInfo.builder().totalBaseAmount(BigDecimal.valueOf(total)).build());
		roomInfo.setPerNightPriceInfos(priceInfoList);
	}
}
