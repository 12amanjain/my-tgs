package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;

@Service
public class CleartripStaticDataInfoFactory extends AbstractStaticDataInfoFactory {

	public CleartripStaticDataInfoFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	public void getCityMappingData() throws IOException {
		CleartripStaticDataInfoService cleartripStaticDataInfoService = CleartripStaticDataInfoService.builder()
				.supplierConf(supplierConf).staticDataRequest(staticDataRequest).sourceConfig(this.sourceConfigOutput)
				.build();
		cleartripStaticDataInfoService.init();
		cleartripStaticDataInfoService.getCityList();
	}

	@Override
	protected void getHotelStaticData() throws IOException {
		CleartripStaticDataInfoService cleartripStaticDataInfoService = CleartripStaticDataInfoService.builder()
				.supplierConf(supplierConf).sourceConfig(this.sourceConfigOutput).build();
		cleartripStaticDataInfoService.init();
		cleartripStaticDataInfoService.handleHotelStaticContent();
	}
}
