package com.tgs.services.hms.sources.cleartrip;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.SupplierCityInfo;
import com.tgs.services.hms.datamodel.cleartrip.CityList;
import com.tgs.services.hms.datamodel.cleartrip.CityRequest;
import com.tgs.services.hms.datamodel.cleartrip.CityResponse;
import com.tgs.services.hms.datamodel.cleartrip.CleartripBaseRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripStaticResponseData;
import com.tgs.services.hms.datamodel.cleartrip.HotelList;
import com.tgs.services.hms.datamodel.cleartrip.HotelListRequest;
import com.tgs.services.hms.datamodel.cleartrip.HotelListResponse;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfileData;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfileLocationInfo;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfilePolicyInfo;
import com.tgs.services.hms.datamodel.cleartrip.HotelProfileResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelCityInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierCityInfoManager;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class CleartripStaticDataInfoService {

	private HotelInfoSaveManager hotelInfoSaveManager;
	private HotelCityInfoSaveManager cityInfoSaveManager;
	private HotelSupplierCityInfoManager supplierCityInfoManager;
	private HotelCityInfoMappingManager cityInfoMappingManager;
	private HotelSourceConfigOutput sourceConfig;
	private HotelSupplierConfiguration supplierConf;
	private HotelStaticDataRequest staticDataRequest;

	private static final String CITY_LIST_SUFFIX = "/hotels/api/v2/content/city-list";
	private static final String HOTEL_LIST_SUFFIX = "/hotels/api/v2/content/hotel-list-city-based";
	private static final String HOTEL_PROFILE_SUFFIX = "/hotels/api/v2/content/hotel-profile/";
	private static final Integer MAX_RETRY_COUNT = 5;

	private static ExecutorService executor = Executors.newFixedThreadPool(20);

	public void init() {

		hotelInfoSaveManager =
				(HotelInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelInfoSaveManager");

		cityInfoSaveManager =
				(HotelCityInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelCityInfoSaveManager");

		cityInfoMappingManager = (HotelCityInfoMappingManager) SpringContext.getApplicationContext()
				.getBean("hotelCityInfoMappingManager");

		supplierCityInfoManager = (HotelSupplierCityInfoManager) SpringContext.getApplicationContext()
				.getBean("hotelSupplierCityInfoManager");
	}

	public void getCityList() throws IOException {

		int page = 1;
		HttpUtilsV2 httpUtils = null;
		int size = !ObjectUtils.isEmpty(sourceConfig.getSize()) ? Integer.parseInt(sourceConfig.getSize()) : 1000;
		int maxCityHitCount =
				!ObjectUtils.isEmpty(sourceConfig.getCityHitCount()) ? sourceConfig.getCityHitCount() : 200;
		int retryCount = 0;
		try {
			while (page < maxCityHitCount) {
				try {
					CityRequest cityRequest = createRequestToFetchCityList(page, size);
					httpUtils = CleartripUtils.getResponseURL(cityRequest, supplierConf);
					httpUtils.setPrintResponseLog(false);
					CityResponse cityResponse = httpUtils.getResponse(CityResponse.class).orElse(null);
					CleartripStaticResponseData cityResponseData = cityResponse.getData();
					List<CityList> supplierCityList = null;
					if (ObjectUtils.isEmpty(cityResponseData)
							|| CollectionUtils.isEmpty((supplierCityList = cityResponseData.getCityList()))) {
						break;
					}
					if (page == 1) {
						log.info("City Fetched are {} ", httpUtils.getResponseString());
					}
					saveCityResponse(supplierCityList);
					retryCount = 0;
					log.info("No of cities fetched are {}", page * supplierCityList.size());
					page++;
				} catch (IOException e) {
					retryCount++;
					log.info("Unable to fetch city info for url {}, headers {}, link {}", httpUtils.getUrlString(),
							httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
					if (retryCount > MAX_RETRY_COUNT)
						break;
				}
			}
		} finally {
			log.info("Total no of city fetched are {}, final response {}, headers {}", page * size,
					httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
		}
	}

	public void handleHotelStaticContent() throws IOException {

		List<String> cities = getCleartripCityIds();
		int totalFetchedHotels = 0;
		for(String city: cities) {
			int page = 0;
			int totalFetchedHotelsCityWise = 0;
			HttpUtilsV2 httpUtils = null;
			int size = !ObjectUtils.isEmpty(sourceConfig.getSize()) ? Integer.parseInt(sourceConfig.getSize()) : 500;
			int maxHotelHitCount =
					!ObjectUtils.isEmpty(sourceConfig.getCityHitCount()) ? sourceConfig.getHotelHitCount() : 20;
			int retryCount = 0;
			try {
				while (page < maxHotelHitCount) {
					try {
						page++;
						HotelListRequest hotelRequest = createRequestToFetchHotelList(page, size, city);
						httpUtils = CleartripUtils.getResponseURL(hotelRequest, supplierConf);
						httpUtils.setPrintResponseLog(false);
						HotelListResponse hotelListResponse =
								httpUtils.getResponse(HotelListResponse.class).orElse(null);
						CleartripStaticResponseData hotelListResponseData = hotelListResponse.getData();
						List<HotelList> supplierHotelList = null;
						if (!ObjectUtils.isEmpty(hotelListResponseData) && CollectionUtils
								.isNotEmpty((supplierHotelList = hotelListResponseData.getHotels()))) {
							fetchAndSaveHotelProfile(supplierHotelList);
						} else {
							break;
						}
						retryCount = 0;
						totalFetchedHotelsCityWise += supplierHotelList.size();
					} catch (IOException e) {
						retryCount++;
						log.info("Unable to fetch hotel info for url {}, headers {}, link {}", httpUtils.getUrlString(),
								httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
						if (retryCount > MAX_RETRY_COUNT) {
							break;
						}
					} catch (Exception e) {
						log.info("Unable to fetch hotel info for url {}, headers {}, link {}", httpUtils.getUrlString(),
								httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
					}
				}
			} finally {
				totalFetchedHotels += totalFetchedHotelsCityWise;
				log.info("Total no of hotels fetched for city {} are {}, final response {}, headers {}", city,
						totalFetchedHotelsCityWise,
						httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
			}
		}
		log.info("Total no of hotels fetched are {}", totalFetchedHotels);
	}

	private void fetchAndSaveHotelProfile(List<HotelList> supplierHotelList) throws IOException {
		supplierHotelList.forEach(supplierHotel -> {

			try {
				HotelProfileData profileData = fetchHotelProfile(supplierHotel);
				if (!ObjectUtils.isEmpty(profileData)) {

					HotelInfo hotelInfo = convertHotelProfileDataIntoHotelInfo(profileData);
					saveOrUpdateHotelInfo(hotelInfo);
				}
			} catch (Exception e) {
				log.info("Unable to fetch hotel profile for {}", GsonUtils.getGson().toJson(supplierHotel), e);
			}
		});
	}

	private void saveOrUpdateHotelInfo(HotelInfo hotelInfo) {
		try {
			DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hotelInfo);
			dbHotelInfo.setId(null);
			String supplierName = HotelSourceType.CLEARTRIP.name();
			hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hotelInfo.getId(), supplierName, HotelSourceType.CLEARTRIP);
		} catch (Exception e) {
			log.info("Error while processing hotel {} due to {} ", hotelInfo.getId(), e);
		}
	}

	private HotelInfo convertHotelProfileDataIntoHotelInfo(HotelProfileData profileData) {

		try {
			HotelProfileLocationInfo locationInfo = profileData.getLocationInfo();
			Address address = Address.builder().addressLine1(profileData.getLocationInfo().getAddress())
					.city(City.builder().name(locationInfo.getCityName()).code(String.valueOf(locationInfo.getCityId()))
							.build())
					.state(State.builder().name(locationInfo.getStateName()).code(locationInfo.getStateCode()).build())
					.country(Country.builder().name(locationInfo.getCountryName()).code(locationInfo.getCountryCode())
							.build())
					.postalCode(locationInfo.getZip()).build();

			GeoLocation geolocation = GeoLocation.builder().latitude(locationInfo.getLatitude())
					.longitude(locationInfo.getLongitude()).build();

			List<Image> images = new ArrayList<>();
			if (CollectionUtils.isNotEmpty(profileData.getImages())) {
				profileData.getImages().forEach(image -> {
					Image img = Image.builder().build();
					img.setBigURL(image.getOriginalImage());
					img.setThumbnail(image.getThumbNailImage());
					images.add(img);
				});
			}

			String propertyType = null;
			if (!ObjectUtils.isEmpty(profileData.getProperty())) {
				propertyType = profileData.getProperty().getPropertyName();
			}

			HotelInfo hotelInfo = HotelInfo.builder().id(String.valueOf(profileData.getId()))
					.name(profileData.getHotelName()).propertyType(propertyType).address(address).images(images)
					.geolocation(geolocation).build();

			if (CollectionUtils.isNotEmpty(profileData.getAmenities())) {
				List<String> facilities = new ArrayList<>();
				profileData.getAmenities().forEach(amenity -> {
					facilities.add(amenity.getAmenityNameEn());
				});
				hotelInfo.setFacilities(facilities);
			}

			if (!ObjectUtils.isEmpty(profileData.getRatings())
					&& !ObjectUtils.isEmpty(profileData.getRatings().getStarRating())) {
				hotelInfo.setRating((int) Double.parseDouble(profileData.getRatings().getStarRating()));
				hotelInfo.setRating(hotelInfo.getRating() != 0 ? hotelInfo.getRating() : null);
			}
			populateCheckinInstructions(profileData.getPolicyInfo(), hotelInfo);

			if (!ObjectUtils.isEmpty(profileData.getPolicyInfo())) {
				hotelInfo.setDescription(StringEscapeUtils.unescapeHtml4(profileData.getBasicInfo().getDescription()));
			}
			return hotelInfo;
		} catch (Exception e) {
			log.info("Unable to convert hotel profile data into hotel info {}", GsonUtils.getGson().toJson(profileData),
					e);

		}
		return null;
	}

	public HotelProfileData fetchHotelProfile(HotelList supplierHotel) {

		HttpUtilsV2 httpUtils = null;
		try {
			CleartripBaseRequest hotelProfileRequest = createRequestToFetchHotelProfile(supplierHotel.getHotelId());
			httpUtils = CleartripUtils.getResponseURL(hotelProfileRequest, supplierConf);
			httpUtils.setPrintResponseLog(false);
			HotelProfileResponse hotelProfileResponse = httpUtils.getResponse(HotelProfileResponse.class).orElse(null);
			if (ObjectUtils.isEmpty(hotelProfileResponse)) {
				log.debug("Empty hotel profile info for url {}, headers {}, link {}", httpUtils.getUrlString(),
						httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams());
			}
			return hotelProfileResponse.getData();
		} catch (IOException e) {
			log.info("Unable to fetch hotel profile info for url {}, headers {}, link {}", httpUtils.getUrlString(),
					httpUtils.getHeaderParams(), httpUtils.getResponseHeaderParams(), e);
		}
		return null;
	}

	private CityRequest createRequestToFetchCityList(int pageNo, int size) {

		return CityRequest.builder().suffixOfURL(CITY_LIST_SUFFIX).pageNo(String.valueOf(pageNo))
				.size(String.valueOf(size)).build();
	}

	private HotelListRequest createRequestToFetchHotelList(int pageNo, int size, String cityId) {
		return HotelListRequest.builder().suffixOfURL(HOTEL_LIST_SUFFIX).pageNo(String.valueOf(pageNo))
				.size(String.valueOf(size)).cityId(cityId).build();
	}

	private CleartripBaseRequest createRequestToFetchHotelProfile(int supplierHotelId) {
		return CleartripBaseRequest.builder().suffixOfURL(HOTEL_PROFILE_SUFFIX + supplierHotelId).build();
	}

	private void saveCityResponse(List<CityList> supplierCityList) {
		if (BooleanUtils.isTrue(staticDataRequest.getIsMasterData())) {
			supplierCityInfoManager.saveSupplierCityInfo(getSupplierCityInfos(supplierCityList));
		} else {
			for (CityList supplierCity : supplierCityList) {
				executor.submit(() -> {
					CityInfo cityInfo = getCityInfo(supplierCity);
					Long cityId = null;
					if ((cityId = cityInfoSaveManager.isCityExists(cityInfo)) == null)
						return;
					CityInfoMapping cityInfoMapping = getCityInfoMapping(supplierCity);
					cityInfoMappingManager.saveCityMapping(cityInfoMapping, cityId);
				});
			}
		}
	}

	private CityInfoMapping getCityInfoMapping(CityList city) {
		return CityInfoMapping.builder().supplierName(HotelSourceType.CLEARTRIP.name())
				.supplierCity(String.valueOf(city.getId())).supplierCountry(city.getCountryId())
				.supplierCityName(city.getCityName()).build();
	}

	private CityInfo getCityInfo(CityList city) {
		return CityInfo.builder().cityName(city.getCityName()).countryId(city.getCountryId())
				.countryName(city.getCountry().getCountryName()).build();
	}

	private List<SupplierCityInfo> getSupplierCityInfos(List<CityList> cityList) {
		List<SupplierCityInfo> supplierCityInfos = new ArrayList<>();
		cityList.forEach(cityMapping -> {

			SupplierCityInfo supplierCityInfo = SupplierCityInfo.builder().cityId(cityMapping.getId().toString())
					.cityName(cityMapping.getCityName()).countryId(cityMapping.getCountryId())
					.countryName(cityMapping.getCountry().getCountryName())
					.supplierName(HotelSourceType.CLEARTRIP.name()).build();
			supplierCityInfos.add(supplierCityInfo);
		});
		return supplierCityInfos;
	}

	private List<String> getCleartripCityIds() {

		if (CollectionUtils.isEmpty(staticDataRequest.getCityIds())) {
			List<CityInfoMapping> cityInfoMappings =
					cityInfoMappingManager.findBySupplierName(HotelSourceType.CLEARTRIP.name());
			return cityInfoMappings.stream().map(CityInfoMapping::getSupplierCity).collect(Collectors.toList());
		} else {
			return staticDataRequest.getCityIds();
		}
	}

	private void populateCheckinInstructions(HotelProfilePolicyInfo policyInfo, HotelInfo hotelInfo) {

		List<Instruction> instructions = new ArrayList<>();
		if (StringUtils.isNotBlank(policyInfo.getCheckInInstructions())) {
			instructions.add(Instruction.builder().type(InstructionType.CHECKIN_INSTRUCTIONS)
					.msg(policyInfo.getCheckInInstructions()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getSpecialCheckInInstructions())) {
			instructions.add(Instruction.builder().type(InstructionType.SPECIAL_INSTRUCTIONS)
					.msg(policyInfo.getSpecialCheckInInstructions()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getMandatoryFees())) {
			instructions.add(Instruction.builder().type(InstructionType.MANDATORY_FEES)
					.msg(policyInfo.getMandatoryFees()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getMandatoryFees())) {
			instructions.add(Instruction.builder().type(InstructionType.OPTIONAL_FEES)
					.msg(policyInfo.getOptionalExtra()).build());
		}

		if (StringUtils.isNotBlank(policyInfo.getMandatoryFees())) {
			instructions.add(Instruction.builder().type(InstructionType.KNOW_BEFORE_YOU_GO)
					.msg(policyInfo.getOtherInformation()).build());
		}

		hotelInfo.setInstructions(instructions);
	}
}
