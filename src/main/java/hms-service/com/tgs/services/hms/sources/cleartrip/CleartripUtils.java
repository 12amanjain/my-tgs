package com.tgs.services.hms.sources.cleartrip;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.cleartrip.CleartripBaseRequest;
import com.tgs.services.hms.datamodel.cleartrip.CleartripCancellationPolicySuccessResponse;
import com.tgs.services.hms.datamodel.cleartrip.Occupancy;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConstants;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CleartripUtils {

	static HotelCacheHandler cacheHandler;
	static final DateFormat dateFormatter_d_MMMM_YYYYY = new SimpleDateFormat("d MMMM yyyy", Locale.ENGLISH);

	public static void init(HotelCacheHandler cacheHandler) {
		CleartripUtils.cacheHandler = cacheHandler;
	}

	public static HttpUtilsV2 getResponseURLWithRequestBody(CleartripBaseRequest request,
			HotelSupplierConfiguration supplierConf) {
		HotelSupplierCredential supplierCredential = supplierConf.getHotelSupplierCredentials();
		request.getHeaderParams().put("X-CT-API-KEY", supplierCredential.getPassword());

		HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(supplierCredential.getUrl() + request.getSuffixOfURL())
				.proxy(HotelUtils.getProxyFromConfigurator())
				.postData(GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(request))
				.headerParams(request.getHeaderParams()).build();
		return httpUtils;
	}

	@SuppressWarnings("unchecked")
	public static HttpUtilsV2 getResponseURL(CleartripBaseRequest request, HotelSupplierConfiguration supplierConf) {
		HotelSupplierCredential supplierCredential = supplierConf.getHotelSupplierCredentials();
		request.getHeaderParams().put("X-CT-API-KEY", supplierCredential.getPassword());
		Map<String, String> queryParams = convertToMap(request);
		MultiMap recurringQueryParams = new MultiValueMap();
		recurringQueryParams.putAll(queryParams);
		HttpUtilsV2 httpUtils = HttpUtilsV2.builder().urlString(supplierCredential.getUrl() + request.getSuffixOfURL())
				.proxy(HotelUtils.getProxyFromConfigurator()).recurringQueryParams(recurringQueryParams)
				.headerParams(request.getHeaderParams()).build();
		return httpUtils;
	}

	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

	public static String getOccupancies(HotelSearchQuery searchQuery) {

		List<RoomSearchInfo> roomInfos = searchQuery.getRoomInfo();
		StringBuilder occupancy = new StringBuilder();
		for (RoomSearchInfo roomSearchInfo : roomInfos) {

			occupancy.append(roomSearchInfo.getNumberOfAdults());
			if (CollectionUtils.isNotEmpty(roomSearchInfo.getChildAge())) {
				occupancy.append("-").append(
						roomSearchInfo.getChildAge().stream().map(String::valueOf).collect(Collectors.joining(":")));
			}
			occupancy.append(",");
		}
		return occupancy.substring(0, occupancy.length() - 1).toString();
	}

	public static void setMealBasis(List<HotelInfo> hotelInfos, Set<String> amenities, HotelSearchQuery searchQuery) {

		Map<String, String> mealBasis =
				CleartripUtils.cacheHandler.fetchMealInfoFromAerospike(searchQuery, new ArrayList<>(amenities));

		hotelInfos.forEach(hInfo -> hInfo.getOptions().forEach(option -> {
			option.getRoomInfos().forEach(roomInfo -> {
				if (CollectionUtils.isNotEmpty(roomInfo.getMiscInfo().getAmenities())) {
					for (String amenity : roomInfo.getMiscInfo().getAmenities()) {
						if (!ObjectUtils.isEmpty(roomInfo.getMealBasis())
								&& !roomInfo.getMealBasis().equals(HotelConstants.ROOM_ONLY)) {
							return;
						}
						roomInfo.setMealBasis(mealBasis.getOrDefault(amenity.toUpperCase(), HotelConstants.ROOM_ONLY));
					}
				} else {
					roomInfo.setMealBasis("Room Only");
				}
			});
		}));
	}

	public static List<RoomInfo> createMultipleRoomsOfSameRoomType(RoomInfo roomInfo, HotelSearchQuery searchQuery) {

		List<RoomInfo> roomInfos = new ArrayList<>();
		List<RoomSearchInfo> searchInfos = searchQuery.getRoomInfo();
		int roomCount = 0;
		for (RoomSearchInfo searchInfo : searchInfos) {
			int noOfAdults = searchInfo.getNumberOfAdults();
			int noOfChilds = CollectionUtils.isEmpty(searchInfo.getChildAge()) ? 0 : searchInfo.getChildAge().size();

			RoomInfo copyRoomInfo = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(roomInfo), RoomInfo.class);
			copyRoomInfo.setNumberOfAdults(noOfAdults);
			copyRoomInfo.setNumberOfChild(noOfChilds);
			copyRoomInfo.setId(roomInfo.getId().replaceAll(":", "_").concat("_" + roomCount++));
			roomInfos.add(copyRoomInfo);
		}
		return roomInfos;
	}

	public static List<Occupancy> createOccupancy(HotelSearchQuery searchQuery) {

		List<Occupancy> occupancies = new ArrayList<>();
		for (RoomSearchInfo roomInfo : searchQuery.getRoomInfo()) {
			Occupancy occupancy = Occupancy.builder().numberOfAdults(roomInfo.getNumberOfAdults())
					.childAges(roomInfo.getChildAge()).build();
			occupancies.add(occupancy);
		}
		return occupancies;
	}

	public static List<Occupancy> createOccupancy(List<RoomInfo> roomInfos) {

		List<Occupancy> occupancies = new ArrayList<>();
		for (RoomInfo roomInfo : roomInfos) {
			List<TravellerInfo> travellers = roomInfo.getTravellerInfo();
			List<Integer> ages = travellers.stream().filter(traveller -> traveller.getPaxType().equals(PaxType.CHILD))
					.map(t -> t.getAge()).collect(Collectors.toList());
			Occupancy occupancy =
					Occupancy.builder().numberOfAdults(roomInfo.getNumberOfAdults()).childAges(ages).build();
			occupancies.add(occupancy);
		}

		return occupancies;
	}

	public static void updatePriceWithClientCommissionComponents(List<RoomInfo> roomInfos,
			HotelSourceConfigOutput sourceConfig) {

		double sAgentCommssion =
				ObjectUtils.isEmpty(sourceConfig.getSAgentCommssion()) ? 0.0 : sourceConfig.getSAgentCommssion();
		double supplierMarkup =
				ObjectUtils.isEmpty(sourceConfig.getSupplierMarkup()) ? 0.0 : sourceConfig.getSupplierMarkup();

		for (RoomInfo roomInfo : roomInfos) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				
				double baseMinusDiscount = priceInfo.getFareComponents().get(HotelFareComponent.BF)
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.SDS, 0.0);
				double baseMinusDiscountCommission = (baseMinusDiscount * sAgentCommssion) / 100;

				double supplierNetPrice = baseMinusDiscount - baseMinusDiscountCommission
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.TSF, 0.0);
				double supplierGrossPrice =
						baseMinusDiscount + priceInfo.getFareComponents().getOrDefault(HotelFareComponent.TSF, 0.0);

				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.MUP, (supplierNetPrice * supplierMarkup) / 100);
				fareComponents.put(HotelFareComponent.SAC, baseMinusDiscountCommission);
				fareComponents.put(HotelFareComponent.SNP, supplierNetPrice);
				fareComponents.put(HotelFareComponent.SGP, supplierGrossPrice);

				Double supplierNetPriceWithMarkup =
						supplierNetPrice + fareComponents.getOrDefault(HotelFareComponent.MUP, 0.0);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierNetPriceWithMarkup);
			}
		}
	}

	public static boolean isValidCancellationPolicyType(String type) {

		return StringUtils.isNotBlank(type)
				&& (type.equals("REFUNDABLE") || type.equals("PARTIALLY_REFUNDABLE") || type.equals("NON_REFUNDABLE"));
	}

	public static void setCancellationPolicyInOption(
			Option option, CleartripCancellationPolicySuccessResponse cancellationPolicyResponse,
			HotelSourceConfigOutput sourceConfigOutput, HotelSearchQuery searchQuery) {

		Integer cancellationBuffer = !ObjectUtils.isEmpty(sourceConfigOutput.getCancellationPolicyBuffer())
				? sourceConfigOutput.getCancellationPolicyBuffer()
				: 24;
		LocalDate targetDate = searchQuery.getCheckinDate();
		LocalDateTime currentTime = LocalDateTime.now();
		String cancellationPolicyType = cancellationPolicyResponse.getRefundable();
		boolean isRefundable = cancellationPolicyType.equals("NON_REFUNDABLE") ? false : true;
		boolean isFreeCancellationAllowed = false;
		LocalDateTime deadlineDateTime = currentTime;
		List<PenaltyDetails> penaltyList = new ArrayList<>();
		LocalDateTime nonRefundableStartDate = currentTime.toLocalDate().atTime(12, 00);

		try {
			if (isRefundable) {

				LocalDateTime toDate = cancellationPolicyType.equals("REFUNDABLE")
						? getEndTimeOfFullyRefundableCancellationPolicy(cancellationPolicyResponse.getCancelPolicy(),
								searchQuery.getCheckinDate())
						: getEndTimeOfPartiallyRefundableCancellationPolicy(
								cancellationPolicyResponse.getCancelPolicy());
				LocalDateTime bufferedToDate = toDate.minus(cancellationBuffer, ChronoUnit.HOURS);
				if (bufferedToDate.isAfter(currentTime)) {
					PenaltyDetails penaltyDetail = PenaltyDetails.builder().fromDate(currentTime).toDate(bufferedToDate)
							.penaltyAmount(0.0).build();
					penaltyList.add(penaltyDetail);
					deadlineDateTime = bufferedToDate;
					isFreeCancellationAllowed = true;
				}
				nonRefundableStartDate = bufferedToDate;
			}
			PenaltyDetails penaltyDetails = PenaltyDetails.builder().fromDate(nonRefundableStartDate)
					.toDate(targetDate.atTime(12, 00)).penaltyAmount(option.getTotalPrice()).build();

			if (cancellationPolicyResponse.getRefundable().equals("REFUNDABLE")) {
				penaltyDetails.setIsCancellationRestricted(true);
			}
			penaltyList.add(penaltyDetails);

			HotelCancellationPolicy cancellationPolicy =
					HotelCancellationPolicy.builder().id(option.getId()).isFullRefundAllowed(isFreeCancellationAllowed)
							.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build())
							.penalyDetails(penaltyList).cancellationPolicy(cancellationPolicyResponse.getCancelPolicy())
							.build();
			option.setDeadlineDateTime(deadlineDateTime);


			if (StringUtils.isNotBlank(cancellationPolicyResponse.getCheckInInstructions())) {
				option.getInstructions().add(Instruction.builder().type(InstructionType.CHECKIN_INSTRUCTIONS)
						.msg(cancellationPolicyResponse.getCheckInInstructions().replaceAll("\\<.*?\\>", "")).build());
			}

			if (StringUtils.isNotBlank(cancellationPolicyResponse.getSpecialCheckInInstructions())) {
				option.getInstructions()
						.add(Instruction.builder().type(InstructionType.SPECIAL_INSTRUCTIONS)
								.msg(cancellationPolicyResponse.getSpecialCheckInInstructions().replaceAll("\\<.*?\\>",
										""))
								.build());
			}

			if (StringUtils.isNotBlank(cancellationPolicyResponse.getHotelPolicy())) {
				option.getInstructions().add(Instruction.builder().type(InstructionType.KNOW_BEFORE_YOU_GO)
						.msg(cancellationPolicyResponse.getHotelPolicy().replaceAll("\\<.*?\\>", "")).build());
			}
			option.setCancellationPolicy(cancellationPolicy);

		} catch (ParseException e) {
			option.setCancellationPolicy(null);
			log.info("Unable to parse cancellation policy {} ", cancellationPolicyResponse.getCancelPolicy(), e);
		}
	}

	private static LocalDateTime getEndTimeOfPartiallyRefundableCancellationPolicy(String cancellationPolicy)
			throws ParseException {

		cancellationPolicy = cancellationPolicy.substring(0, cancellationPolicy.indexOf(","))
				.substring(cancellationPolicy.indexOf("before") + 7);

		LocalDateTime endTime = dateFormatter_d_MMMM_YYYYY.parse(cancellationPolicy).toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate().atTime(12, 00);
		return endTime;
	}

	private static LocalDateTime getEndTimeOfFullyRefundableCancellationPolicy(String cancellationPolicy,
			LocalDate checkinDate) throws ParseException {

		cancellationPolicy = cancellationPolicy.substring(cancellationPolicy.indexOf(",") + 2);
		cancellationPolicy =
				cancellationPolicy.substring(0, cancellationPolicy.indexOf("(") - 1) + " " + checkinDate.getYear();

		LocalDateTime endTime = dateFormatter_d_MMMM_YYYYY.parse(cancellationPolicy).toInstant()
				.atZone(ZoneId.systemDefault()).toLocalDate().atTime(12, 00);
		return endTime;
	}
}
