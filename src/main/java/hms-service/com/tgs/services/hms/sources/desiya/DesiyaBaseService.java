package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.MathContext;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.UnknownHostException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.axis2.transport.http.HTTPConstants;
import org.apache.axis2.transport.http.HttpTransportProperties;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.utils.common.HttpUtils;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.CompanyNameType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RequestorID_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AdditionalGuestAmountType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AdditionalGuestAmounts_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AdditionalGuest_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AdditionalGuests_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AddressInfoType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Amenities_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.BasicPropertyInfoType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.CancelPenaltyType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.FormattedTextTextType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.GuestCount_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.GuestCounts_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.HotelBasicInformation_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Occupancy_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.ParagraphType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.ParagraphTypeChoice_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Position_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RatePlanInclusions_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RatePlanType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RateType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Rate_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomRate_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomRates_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomStay_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomTypeType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.TPA_ExtensionsType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.TaxType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.TaxesType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.TotalType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.UserAuthentication_type0;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@SuperBuilder
@Slf4j
public class DesiyaBaseService {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfig;


	protected HotelInfo getHotelInfoFromRoomStay(RoomStay_type1 roomStay, boolean isDetailHit) {

		RoomTypeType[] roomTypeArray = roomStay.getRoomTypes().getRoomType();
		RatePlanType[] ratePlanArray = getRatePlanArrayFromRoomStay(roomStay);

		RoomRates_type0 roomRates = roomStay.getRoomRates();
		if (roomRates == null) {
			log.error("Empty Room Rates for roomStay {}", roomStay);
			return null;
		}
		RoomRate_type0[] roomRateArray = roomRates.getRoomRate();

		Map<String, RoomTypeType> roomCodeRoomTypeMap = Arrays.stream(roomTypeArray)
				.collect(Collectors.toMap(RoomTypeType::getRoomTypeCode, Function.identity()));
		Map<String, RatePlanType> ratePlanCodeRatePlanType = Arrays.stream(ratePlanArray)
				.collect(Collectors.toMap(RatePlanType::getRatePlanCode, Function.identity()));
		Map<String, List<RoomRate_type0>> roomTypeRatePlanAsKeyRateTypeList = new HashMap<>();

		Arrays.stream(roomRateArray).forEach(roomRate -> {
			String key = roomRate.getRatePlanCode() + "" + roomRate.getRoomID();
			roomTypeRatePlanAsKeyRateTypeList.computeIfAbsent(key, (x) -> new ArrayList<>()).add(roomRate);
		});
		// .collect(Collectors.groupingBy(RoomRate_type0 :: getRatePlanCode));

		int numberOfDays = (int) Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		List<Option> optionList = new ArrayList<>();
		roomTypeRatePlanAsKeyRateTypeList.keySet().forEach(key -> {
			List<RoomRate_type0> roomRateList = roomTypeRatePlanAsKeyRateTypeList.get(key);
			RoomRate_type0 roomRate = roomRateList.get(0);
			RoomTypeType roomType = roomCodeRoomTypeMap.get(roomRate.getRoomID());
			RatePlanType ratePlanType = ratePlanCodeRatePlanType.get(roomRate.getRatePlanCode());
			if (!Objects.isNull(roomType) && !(Objects.isNull(ratePlanType)) && roomRateList.size() == numberOfDays) {
				Option option = getOptionFromRoomRate(roomRateList, roomType, ratePlanType);
				option.getMiscInfo().setIsDetailHit(isDetailHit);
				optionList.add(option);
			}
		});

		BasicPropertyInfoType basicPropertyInfo = roomStay.getBasicPropertyInfo();
		HotelInfo hInfo = HotelInfo.builder().miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
				.supplierStaticHotelId(basicPropertyInfo.getHotelCode()).build()).build();
		
		if (!isDetailHit && CollectionUtils.isNotEmpty(optionList) && optionList.get(0) != null) {
			setDummyData(optionList.get(0));
			hInfo.setOptions(new ArrayList<>(Arrays.asList(optionList.get(0))));
		} else
			hInfo.setOptions(optionList);
		return hInfo;

	}

	private void setDummyData(Option option) {

		for (RoomInfo room : option.getRoomInfos()) {
			room.setRoomCategory("Dummy Room Category");
			room.setRoomType("Dummy Room Type");
		}

	}


	private Option getOptionFromRoomRate(List<RoomRate_type0> roomRateList, RoomTypeType roomTypeType,
			RatePlanType ratePlanType) {

		String mealBasis = fetchMealBasisFromRatePlan(ratePlanType);

		String roomCategory = ratePlanType.getRatePlanName();
		String roomType = roomTypeType.getRoomType();
		RoomMiscInfo rmi = RoomMiscInfo.builder().ratePlanCode(ratePlanType.getRatePlanCode())
				.roomTypeCode(roomTypeType.getRoomTypeCode()).build();
		RoomInfo rInfo = getRoomInfo(roomCategory, roomType, rmi, "Room Only");
		rInfo.setMealBasis(mealBasis);
		List<RoomInfo> rInfoList = new ArrayList<>();

		for (int i = 0; i < searchQuery.getRoomInfo().size(); i++) {
			/*
			 * Try to Use clone here(Deep Cloning)
			 */
			RoomInfo copyRInfo = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(rInfo), RoomInfo.class);
			rInfoList.add(copyRInfo);
		}

		int day = 1;
		for (RoomRate_type0 roomRateType : roomRateList) {

			BigDecimal baseAmount = fetchBaseAmount(roomRateType);
			BigDecimal totalTax = fetchBaseTax(roomRateType);
			updateBaseRateInAllRoomsForThisDay(rInfoList, baseAmount, totalTax, day);

			Rate_type1 rate = fetchRateFromRoomRateType(roomRateType);
			AdditionalGuestAmounts_type1 additionalGuestType = rate.getAdditionalGuestAmounts();
			if (additionalGuestType != null) {
				AdditionalGuestAmountType[] additionalGuestInfoList = additionalGuestType.getAdditionalGuestAmount();
				for (int i = 0; i < additionalGuestInfoList.length; i++) {
					AdditionalGuestAmountType additionalGuestInfo = additionalGuestInfoList[i];
					int index = Integer.valueOf(additionalGuestInfo.getRPH());
					RoomInfo room = rInfoList.get(index - 1);

					TotalType additionalAmountInfo = additionalGuestInfo.getAmount();
					BigDecimal totalAdditionalAmountBeforeTax = additionalAmountInfo.getAmountBeforeTax();
					BigDecimal totalAdditionalTaxAmount = fetchTaxesFromTotalType(additionalAmountInfo);
					BigDecimal totalAdditonalAmount = totalAdditionalAmountBeforeTax.add(totalAdditionalTaxAmount);

					/*
					 * Add logic of additional taxes here
					 */

					PriceInfo perNightPriceInfo = room.getPerNightPriceInfos().get(day - 1);
					perNightPriceInfo.getFareComponents().put(HotelFareComponent.BF,
							perNightPriceInfo.getFareComponents().get(HotelFareComponent.BF)
									+ totalAdditonalAmount.doubleValue());

					room.getTotalFareComponents().put(HotelFareComponent.BF,
							room.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0d)
									+ totalAdditonalAmount.doubleValue());

					room.getMiscInfo().setTotalBaseAmount(
							room.getMiscInfo().getTotalBaseAmount().add(totalAdditionalAmountBeforeTax));
					room.getMiscInfo().setTotalTaxes(room.getMiscInfo().getTotalTaxes().add(totalAdditionalTaxAmount));
				}
			}
			day++;
		}

		Set<Integer> set = new HashSet<>();
		AdditionalGuests_type0 additionalGuestInfoType = ratePlanType.getTPA_Extensions().getAdditionalGuests();
		if (additionalGuestInfoType != null && additionalGuestInfoType.getAdditionalGuest() != null) {
			AdditionalGuest_type0[] additionalGuestArray = additionalGuestInfoType.getAdditionalGuest();

			for (int i = 0; i < additionalGuestArray.length; i++) {
				AdditionalGuest_type0 additonalGuestCountInfo = additionalGuestArray[i];
				RoomInfo room = rInfoList.get(additonalGuestCountInfo.getRoomNo() - 1);
				set.add(additonalGuestCountInfo.getRoomNo() - 1);
				RoomSearchInfo roomSearchInfo = searchQuery.getRoomInfo().get(additonalGuestCountInfo.getRoomNo() - 1);
				int searchAdult = roomSearchInfo.getNumberOfAdults();
				int searchChild = roomSearchInfo.getNumberOfChild() != null ? roomSearchInfo.getNumberOfChild() : 0;
				room.setNumberOfAdults(searchAdult);
				room.setNumberOfChild(searchChild);
			}
		}

		int index = 0;
		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {

			if (!set.contains(index)) {
				RoomInfo room = rInfoList.get(index);
				int searchAdult = roomSearchInfo.getNumberOfAdults();
				int searchChild = roomSearchInfo.getNumberOfChild() != null ? roomSearchInfo.getNumberOfChild() : 0;
				room.setNumberOfAdults((searchAdult));
				room.setNumberOfChild(searchChild);
			}
			index++;
		}

		String inclusions = fetchInclusionsFromRatePlan(ratePlanType);
		Instruction instruction = Instruction.builder().type(InstructionType.INCLUSIONS).msg(inclusions).build();

		updatePriceWithClientCommission(rInfoList);
		OptionMiscInfo miscInfo =
				OptionMiscInfo.builder().secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
						.supplierId(supplierConf.getBasicInfo().getSupplierId())
						.sourceId(supplierConf.getBasicInfo().getSourceId()).build();
		Option option = Option.builder().roomInfos(rInfoList).miscInfo(miscInfo)
				.instructions(new ArrayList<>(Arrays.asList(instruction)))
				.id((RandomStringUtils.random(20, true, true))).build();
		return option;

		/*
		 * Case Of Changed OCcupancy
		 */
		/*
		 * for(RoomSearchInfo roomSearchInfo : roomSearchCriteria) {
		 * 
		 * Integer numberOfAdults = roomSearchInfo.getNumberOfAdults(); Integer numberOfChild =
		 * roomSearchInfo.getNumberOfChild() != null ? roomSearchInfo.getNumberOfChild() : 0; List<Integer> childAgeList
		 * = roomSearchInfo.getChildAge();
		 * 
		 * if(CollectionUtils.isNotEmpty(childAgeList)) { for(Integer childAge : childAgeList) { if(minChildAgeFromRoom
		 * > childAge) numberOfChild-- ; else if(childAge > maxChildAgeFromRoom) { numberOfChild--; numberOfAdults++; }
		 * } } }
		 */
	}

	private void updatePriceWithClientCommission(List<RoomInfo> rInfoList) {

		int numberOfNights = (int) Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		double sAgentCommssion =
				ObjectUtils.isEmpty(sourceConfig.getSAgentCommssion()) ? 0.0 : sourceConfig.getSAgentCommssion();

		for (RoomInfo roomInfo : rInfoList) {

			double totalAmount = roomInfo.getMiscInfo().getTotalBaseAmount().doubleValue();
			double totalSAgentCommssion = (totalAmount * sAgentCommssion) / 100;

			double newRoomTotalFare =
					totalAmount - totalSAgentCommssion + roomInfo.getMiscInfo().getTotalTaxes().doubleValue();
			double pernightNewBaseAmount = newRoomTotalFare / numberOfNights;
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				priceInfo.getFareComponents().put(HotelFareComponent.BF, pernightNewBaseAmount);

			}
		}
	}

	private void updateBaseRateInAllRoomsForThisDay(List<RoomInfo> rInfoList, BigDecimal baseAmountForBaseOccupancy,
			BigDecimal taxForBaseOccupancy, int day) {

		/*
		 * Have to do like this, need to keep a watch over difference in booking amount, if it's exceeding 1Rs then we
		 * need to look into this as an issue .
		 */
		double numberOfRoom = rInfoList.size();
		Double totalOptionBasePricePerNight = baseAmountForBaseOccupancy.add(taxForBaseOccupancy).doubleValue();
		BigDecimal totalRoomBasePrice =
				baseAmountForBaseOccupancy.divide(BigDecimal.valueOf(numberOfRoom), MathContext.DECIMAL64);
		BigDecimal totalRoomTaxPrice =
				(taxForBaseOccupancy.divide(BigDecimal.valueOf(numberOfRoom), MathContext.DECIMAL64));

		Double totalRoomBasePricePerNight = totalOptionBasePricePerNight / rInfoList.size();
		rInfoList.stream().forEach((room -> {

			PriceInfo priceInfo = new PriceInfo();
			priceInfo.setDay(day);
			priceInfo.getFareComponents().put(HotelFareComponent.BF, totalRoomBasePricePerNight);
			room.getPerNightPriceInfos().add(priceInfo);
			room.getTotalFareComponents().put(HotelFareComponent.BF,
					room.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
							+ totalRoomBasePricePerNight);
			room.getMiscInfo().setTotalBaseAmount(room.getMiscInfo().getTotalBaseAmount().add(totalRoomBasePrice));
			room.getMiscInfo().setTotalTaxes(room.getMiscInfo().getTotalTaxes().add(totalRoomTaxPrice));

		}));

	}


	private String fetchInclusionsFromRatePlan(RatePlanType ratePlanType) {


		StringBuilder sb = new StringBuilder();
		String inclusions = "";
		RatePlanInclusions_type0 ratePlanInclusion = ratePlanType.getRatePlanInclusions();
		if (!Objects.isNull(inclusions)) {
			ParagraphType paragraphType = ratePlanInclusion.getRatePlanInclusionDesciption();
			if (!Objects.isNull(paragraphType)) {
				ParagraphTypeChoice_type0[] paragraphTypeChoiceArray = paragraphType.getParagraphTypeChoice_type0();
				if (!Objects.isNull(paragraphTypeChoiceArray) && paragraphTypeChoiceArray.length > 0) {
					for (ParagraphTypeChoice_type0 paragraphTypeChoice : paragraphTypeChoiceArray) {
						FormattedTextTextType text = paragraphTypeChoice.getText();
						if (!Objects.isNull(text))
							sb.append(text.getString());
					}
				}
			}
		}

		String allInclusions = sb.toString();
		return allInclusions;
	}

	private String fetchMealBasisFromRatePlan(RatePlanType ratePlanType) {

		String defaultMealBasis = "Room Only";
		List<String> possibleMealBasis = DesiyaConstants.MEALS;
		String allInclusions = fetchInclusionsFromRatePlan(ratePlanType);
		if (StringUtils.isNotBlank(allInclusions)) {
			allInclusions = allInclusions.toLowerCase();
			for (String str : possibleMealBasis) {
				str = str.toLowerCase();
				if (allInclusions.contains(str))
					return str;
			}
		}

		return defaultMealBasis;
	}

	private BigDecimal fetchBaseTax(RoomRate_type0 roomRateType) {

		RateType roomRate = roomRateType.getRates();
		Rate_type1[] rateList = roomRate.getRate();
		/*
		 * Need to consider cases of multiple rate Currently considering only first
		 */
		Rate_type1 rate = rateList[0];
		TotalType type = rate.getBase();
		return fetchTaxesFromTotalType(type);

	}

	private BigDecimal fetchTaxesFromTotalType(TotalType type) {

		TaxesType taxesType = type.getTaxes();
		BigDecimal totalTax = BigDecimal.ZERO;
		if (taxesType != null) {
			TaxType[] taxTypeList = taxesType.getTax();
			if (taxTypeList != null && taxTypeList.length > 0) {
				totalTax = Arrays.stream(taxTypeList).map(TaxType::getAmount).reduce(BigDecimal.ZERO, BigDecimal::add);
			}
		}

		return totalTax;


	}

	private Rate_type1 fetchRateFromRoomRateType(RoomRate_type0 roomRateType) {

		RateType roomRate = roomRateType.getRates();
		Rate_type1[] rateList = roomRate.getRate();
		return rateList[0];
	}


	private BigDecimal fetchBaseAmount(RoomRate_type0 roomRateType) {

		RateType roomRate = roomRateType.getRates();
		Rate_type1[] rateList = roomRate.getRate();
		/*
		 * Need to consider cases of multiple rate Currently considering only first
		 */
		Rate_type1 rate = rateList[0];
		TotalType type = rate.getBase();
		return type.getAmountBeforeTax();

	}


	private RoomInfo getRoomInfo(String roomCategory, String roomType, RoomMiscInfo rmi, String mealBasis) {

		RoomInfo rInfo = new RoomInfo();
		rInfo.setRoomCategory(roomCategory);
		rInfo.setRoomType(roomType);
		rInfo.setMiscInfo(rmi);
		rInfo.setMealBasis(mealBasis);
		rInfo.getTotalFareComponents().put(HotelFareComponent.BF, 0d);
		return rInfo;
	}


	protected void setAuthentication(TPA_ExtensionsType extension, HotelSupplierConfiguration sConf)
			throws UnknownHostException {

		UserAuthentication_type0 authRequest = new UserAuthentication_type0();
		authRequest.setPassword(sConf.getHotelSupplierCredentials().getPassword());
		authRequest.setUsername(sConf.getHotelSupplierCredentials().getUserName());
		authRequest.setPropertyId(sConf.getHotelSupplierCredentials().getClientId());

		extension.setUserAuthentication(authRequest);
	}


	protected void setOptionCancellationPolicy(RatePlanType ratePlan, HotelSearchQuery searchQuery, Option option) {
	
		RoomInfo room = option.getRoomInfos().get(0);
		Integer totalNumberOfRooms = option.getRoomInfos().size();
		List<PenaltyDetails> penaltyDetails = new ArrayList<>();
		Boolean isFullRefundableAllowed = false;
		String penaltyText = "";
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDate = now;
		LocalDateTime CheckinTime = searchQuery.getCheckinDate().atStartOfDay();

		CancelPenaltyType cancelPenaltyDescrptions = ratePlan.getCancelPenalties().getCancelPenalty()[0];


		// isFullRefundableAllowed = !cancelPenaltyDescrptions.getNonRefundable();
		for (ParagraphType penalty : cancelPenaltyDescrptions.getPenaltyDescription()) {
			if (penalty.getName().equals("FREE_CANCELLATION")) {
				if (penalty.getParagraphTypeChoice_type0()[0].getText().getString().equals("Y")) {
					isFullRefundableAllowed = true;
				}
			}
			penaltyText += "     " + penalty.getName() + " : "
					+ penalty.getParagraphTypeChoice_type0()[0].getText().getString();
		}

		if (isFullRefundableAllowed) {

			for (CancelPenaltyType penalty : ratePlan.getCancelPenalties().getCancelPenalty()) {
				if (penalty.getAmountPercent() != null && penalty.getDeadline().getOffsetUnitMultiplier() != 9999) {

					int day = (int) TimeUnit.HOURS.toDays(penalty.getDeadline().getOffsetUnitMultiplier());
					deadlineDate = searchQuery.getCheckinDate().minusDays(day).atStartOfDay();
					PenaltyDetails pd = PenaltyDetails.builder().build();
					pd.setFromDate(deadlineDate);
					pd.setToDate(CheckinTime);
					pd.setPenaltyRoomNights(penalty.getAmountPercent().getNmbrOfNights().intValue());
					pd.setPenaltyAmount(
							room.getPerNightPriceInfos().get(0).getFareComponents().get(HotelFareComponent.BF)
									* pd.getPenaltyRoomNights() * totalNumberOfRooms);
					penaltyDetails.add(pd);
				}
			}

			PenaltyDetails pd = PenaltyDetails.builder().build();
			pd.setPenaltyAmount(0.0);
			pd.setFromDate(now);
			pd.setToDate(deadlineDate);
			penaltyDetails.add(pd);


			/*
			 * deadlineDate = searchQuery.getCheckinDate().minusDays(1).atStartOfDay(); for (CancelPenaltyType penalty :
			 * ratePlan.getCancelPenalties().getCancelPenalty()) {
			 * 
			 * if (penalty.getDeadline().getOffsetUnitMultiplier() == 9999) { PenaltyDetails pd =
			 * PenaltyDetails.builder().build();
			 * pd.setPenaltyRoomNights(penalty.getAmountPercent().getNmbrOfNights().intValue()); pd.setPenaltyAmount(
			 * room.getPerNightPriceInfos().get(0).getFareComponents().get(HotelFareComponent.BF)
			 * pd.getPenaltyRoomNights() * totalNumberOfRooms); pd.setFromDate(now); pd.setToDate(deadlineDate);
			 * penaltyDetails.add(pd); } else if (penalty.getDeadline().getOffsetUnitMultiplier() == 24) {
			 * PenaltyDetails pd = PenaltyDetails.builder().build(); pd.setFromDate(deadlineDate);
			 * pd.setToDate(CheckinTime);
			 * pd.setPenaltyRoomNights(penalty.getAmountPercent().getNmbrOfNights().intValue()); pd.setPenaltyAmount(
			 * room.getPerNightPriceInfos().get(0).getFareComponents().get(HotelFareComponent.BF)
			 * pd.getPenaltyRoomNights() * totalNumberOfRooms); penaltyDetails.add(pd); } }
			 */
		}
		if (!isFullRefundableAllowed) {
			CancelPenaltyType penalty = ratePlan.getCancelPenalties().getCancelPenalty()[1];
			if (penalty != null) {
				int day = (int) TimeUnit.HOURS.toDays(penalty.getDeadline().getOffsetUnitMultiplier());
				deadlineDate = searchQuery.getCheckinDate().minusDays(day).atStartOfDay();
				if (deadlineDate.isBefore(now)) {
					deadlineDate = now;
				}
				PenaltyDetails pd = PenaltyDetails.builder().build();
				pd.setFromDate(deadlineDate);
				pd.setToDate(CheckinTime);
				pd.setPenaltyRoomNights(penalty.getAmountPercent().getNmbrOfNights().intValue());
				pd.setPenaltyAmount(room.getPerNightPriceInfos().get(0).getFareComponents().get(HotelFareComponent.BF)
						* pd.getPenaltyRoomNights() * totalNumberOfRooms);
				penaltyDetails.add(pd);
			}
		}
		Collections.reverse(penaltyDetails);
		HotelCancellationPolicy hotelCancellationPolicy = HotelCancellationPolicy.builder()
				.penalyDetails(penaltyDetails).id(option.getId()).isFullRefundAllowed(isFullRefundableAllowed)
				.cancellationPolicy(penaltyText)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();

		option.setCancellationPolicy(hotelCancellationPolicy);
		option.setDeadlineDateTime(deadlineDate);
	}

	protected RatePlanType[] getRatePlanArrayFromRoomStay(RoomStay_type1 roomStay) {
		return roomStay.getRatePlans().getRatePlan();
	}

	protected List<String> getHotelAmenities(RoomStay_type1 desiyaHotelInfo) {

		List<String> hotelAmenityList = new ArrayList<>();
		if (desiyaHotelInfo.getTPA_Extensions() != null) {
			HotelBasicInformation_type0 basicInfo = desiyaHotelInfo.getTPA_Extensions().getHotelBasicInformation();
			Amenities_type0 amenities = basicInfo.getAmenities();
			if (amenities != null && amenities.getPropertyAmenities()!=null) {
				Arrays.stream(amenities.getPropertyAmenities()).forEach(amenity -> {
					hotelAmenityList.add(amenity.getDescription());
				});
			}
		}

		return hotelAmenityList;
	}

	protected Address getAddressFromDesiyaAddress(AddressInfoType desiyaAddress) {

		if (Objects.isNull(desiyaAddress))
			return null;
		String[] addressLineArray = desiyaAddress.getAddressLine();
		String addressLine1 = addressLineArray != null && addressLineArray.length > 0 ? addressLineArray[0] : null;
		City city = City.builder().name(desiyaAddress.getCityName()).build();
		Country country = Country.builder().name(desiyaAddress.getCountryName().getString()).build();

		return Address.builder().city(city).country(country).addressLine1(addressLine1).build();
	}

	protected GeoLocation getGeoLocationFromDesiyaPosition(Position_type1 pos) {

		if (pos == null)
			return null;
		return GeoLocation.builder().latitude(pos.getLatitude()).longitude(pos.getLongitude()).build();
	}

	protected RequestorID_type0 getRequestId() {

		RequestorID_type0 requestId = new RequestorID_type0();
		requestId.setMessagePassword(supplierConf.getHotelSupplierCredentials().getPassword());
		requestId.setID(supplierConf.getHotelSupplierCredentials().getClientId());
		CompanyNameType company = new CompanyNameType();
		company.setCode(supplierConf.getHotelSupplierCredentials().getUserName());
		company.setString(supplierConf.getHotelSupplierCredentials().getUserName());
		requestId.setCompanyName(company);
		return requestId;
	}

	protected String getLogType(String prefix) {
		return (prefix + "-" + supplierConf.getHotelSupplierCredentials().getUserName());
	}
	
	protected HttpURLConnection getHttpURLConnection(String zipFolderUrl) throws IOException {

		URL url = new URL(zipFolderUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		// temp
		String username = supplierConf.getHotelSupplierCredentials().getExtranetCredentials().get(0).getUsername();
		String password = supplierConf.getHotelSupplierCredentials().getExtranetCredentials().get(0).getPassword();

		String basicAuth = Base64.getEncoder().encodeToString((username + ":" + password).getBytes());
		connection.setRequestProperty("Authorization", "Basic " + basicAuth);
		connection.setRequestMethod(HttpUtils.REQ_METHOD_GET);
		connection.setRequestProperty("Accept", "*/*");
		connection.setRequestProperty("Accept-Encoding", "gzip, deflate, br");
		connection.setRequestProperty("Connection", "keep-alive");
		connection.setUseCaches(false);
		connection.setDoInput(true);
		connection.setDoOutput(true);
		return connection;
	}
}
