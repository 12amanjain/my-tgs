package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import org.apache.axiom.om.OMException;
import org.apache.axis2.AxisFault;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.CancelDates_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.EmailType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.OTA_CancelRQ;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.OTA_CancelRS;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.POS_Type;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.PersonName_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.SourceType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.TPA_ExtensionsType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.TransactionActionType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.TransactionStatusType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.UniqueID_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.VerificationType;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@SuperBuilder
@Slf4j
public class DesiyaBookingCancellationService extends DesiyaBaseService {

	private HotelSourceConfigOutput sourceConfigOutput;
	private OTA_CancelRS response;
	private Order order;
	private HotelInfo hInfo;
	protected SoapRequestResponseListner listener;

	public boolean cancelBooking() throws IOException {

		boolean isCancelled = false;
		TGBookingServiceEndPointImplServiceStub stub = null;
		try {
			stub = DesiyaBindingService.getBookingStub(supplierConf);
			OTA_CancelRQ request = new OTA_CancelRQ();
			request = createInitiateBookingCancellationRequest();
			try {
				listener.setType("Desiya Initiate Cancellation");
				stub._getServiceClient().getAxisService().addMessageContextListener(listener);
				OTA_CancelRS response1 = stub.cancelBooking(request);
				if (response1.getErrors() == null && response1.getStatus() != null
						&& response1.getStatus().equals(TransactionStatusType.value8)) {
					OTA_CancelRQ cancelRequest = new OTA_CancelRQ();
					cancelRequest = createBookingCancellationRequest();
					listener.setType("Desiya Confirm Cancellation");
					stub._getServiceClient().getAxisService().addMessageContextListener(listener);
					response = stub.cancelBooking(cancelRequest);
					isCancelled = isBookingCancelled();
				}
			} catch (OMException om) {
				throw new NoSeatAvailableException(om.getMessage());
			} catch (AxisFault e) {
				throw new NoSearchResultException(e.getMessage());
			} catch (RemoteException e) {
				throw new SupplierRemoteException(e);
			}
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);

		}
		return isCancelled;
	}

	private OTA_CancelRQ createCancellationRequest() {

		OTA_CancelRQ cancelRequest = new OTA_CancelRQ();
		cancelRequest.setVersion(BigDecimal.valueOf(1.0));

		POS_Type posType = new POS_Type();
		SourceType[] sourceList = new SourceType[1];
		SourceType source = new SourceType();

		source.setRequestorID(getRequestId());
		sourceList[0] = source;
		posType.setSource(sourceList);
		cancelRequest.setPOS(posType);

		UniqueID_type0[] uniqueIDs = new UniqueID_type0[1];
		UniqueID_type0 uniqueId = new UniqueID_type0();
		uniqueId.setID(hInfo.getMiscInfo().getSupplierBookingId());
		uniqueIDs[0] = uniqueId;
		cancelRequest.setUniqueID(uniqueIDs);

		cancelRequest.setVerification(null);
		VerificationType[] verificationType = new VerificationType[1];
		VerificationType verfication = new VerificationType();

		PersonName_type0 personName = new PersonName_type0();
		personName.setSurname(hInfo.getOptions().get(0).getRoomInfos().get(0).getTravellerInfo().get(0).getLastName());
		verfication.setPersonName(personName);

		EmailType email = new EmailType();
		email.setString(order.getDeliveryInfo().getEmails().get(0));
		verfication.setEmail(email);

		verificationType[0] = verfication;
		cancelRequest.setVerification(verificationType);

		TPA_ExtensionsType extension = new TPA_ExtensionsType();

		CancelDates_type0 dates = new CancelDates_type0();
		
		extension.setCancelDates(dates);
		cancelRequest.setTPA_Extensions(extension);
		return cancelRequest;
	}

	private void setInitiateCancel(OTA_CancelRQ request) {

		TransactionActionType transactionType = TransactionActionType.Initiate;
		request.setCancelType(transactionType);
	}

	private void setConfirmCancel(OTA_CancelRQ request) {

		TransactionActionType transactionType = TransactionActionType.Cancel;
		request.setCancelType(transactionType);
	}

	private OTA_CancelRQ createInitiateBookingCancellationRequest() {

		OTA_CancelRQ request = createCancellationRequest();
		setInitiateCancel(request);
		return request;
	}

	private OTA_CancelRQ createBookingCancellationRequest() {

		OTA_CancelRQ request = createCancellationRequest();
		setConfirmCancel(request);
		return request;
	}


	private boolean isBookingCancelled() {

		boolean isBookingCancelled = false;
		if (response == null) {
			log.error("Error while booking cancellation , response is null for bookingId {} ", order.getBookingId());
			return isBookingCancelled;
		} else if (response.getErrors() != null) {
			log.error(
					"Error while booking cancellation on Qtech for bookingId {}, message {}, supplier {}, username {} ",
					order.getBookingId(), response.getErrors().getError()[0].getString(),
					this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
		} else if (response.getStatus() != null && response.getStatus().equals(TransactionStatusType.value2)) {
			log.info("Booking cancellation successful on Desiya for bookingId {} messageInfo {} ", order.getBookingId(),
					response.getComment().getParagraphTypeChoice_type0()[0].getText().toString());
			isBookingCancelled = true;
		}
		return isBookingCancelled;

	}

}
