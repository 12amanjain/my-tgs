package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DesiyaCitySaxParser extends DefaultHandler {

	List<HotelCityInfoQuery> cityList;
	InputStream cityXmlFileName;
	String tmpValue;
	HotelCityInfoQuery cityTmp;

	private HotelCityInfoMappingManager cityManager;

	public DesiyaCitySaxParser(InputStream cityXmlFileName,HotelCityInfoMappingManager cityManager) {

		this.cityXmlFileName = cityXmlFileName;
		cityList=new ArrayList<>();
		this.cityManager=cityManager;
		parseDocument();
		log.info("desiya city size : "+cityList.size());
		cityManager.saveCityInfoMappingList(cityList);
	}

	public void parseDocument() {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(cityXmlFileName, this);
		} catch (ParserConfigurationException e) {
			log.error("ParserConfig error");
		} catch (SAXException e) {
			log.error("SAXException : xml not well formed");
		} catch (IOException e) {
			log.error("IO error");
		}
	}

	@Override
	public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {

		if (elementName.equalsIgnoreCase("City")) {
			cityTmp = new HotelCityInfoQuery();
			
		}
	}

	@Override
	public void endElement(String s, String s1, String element) throws SAXException {

		if (element.equals("City")) {
			cityTmp.setCityname(tmpValue);
			cityTmp.setCountryname("INDIA");
			cityTmp.setSuppliercityid(tmpValue);
			cityTmp.setSuppliername("DESIYA");
			cityTmp.setSuppliercountryid("INDIA");
			cityList.add(cityTmp);
		}
	}

	@Override
	public void characters(char[] ac, int i, int j) throws SAXException {
		tmpValue = new String(ac, i, j);
	}

}
