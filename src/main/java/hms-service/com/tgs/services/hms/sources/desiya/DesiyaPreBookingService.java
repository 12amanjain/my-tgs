package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.rmi.RemoteException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.apache.axiom.om.OMException;
import org.apache.axis2.AxisFault;

import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;

import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.Address_type1;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.BasicPropertyInfoType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.CountryNameType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.CustomerType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.DateTimeSpanType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.Email_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.GuaranteeType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.GuestCountType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.GuestCount_type1;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.HotelReservation_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.HotelReservationsType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.OTA_HotelResRQ;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.OTA_HotelResRS;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.POS_Type;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.PersonNameType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ProfileInfo_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ProfileType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ProfilesType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RatePlanType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RatePlans_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ResGlobalInfoType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ResGuestType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.ResGuestsType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RoomStay_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RoomStaysType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RoomTypeType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.RoomTypes_type0;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.SourceType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.StateProvType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.TaxesType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.Telephone_type2;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.TotalType;
import com.travelguru.services.endpoints.TGBookingServiceEndPointImplServiceStub.UniqueID_Type;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DesiyaPreBookingService extends DesiyaBaseService {

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private SoapRequestResponseListner listener;
	private LocalDateTime currentTime;
	private HotelSearchQuery searchQuery;

	private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
	private static final int maxTrial = 3;

	public boolean book() throws IOException, JAXBException {

		OTA_HotelResRS bookResponse = null;
		TGBookingServiceEndPointImplServiceStub stub = DesiyaBindingService.getBookingStub(supplierConf);
		OTA_HotelResRQ reviewrequest = createPreBookRequest(hInfo);
		listener.setType(getLogType("Desiya Pre-Book Service"));
		stub._getServiceClient().getAxisService().addMessageContextListener(listener);
		try {

			int trial = 0;
			while (trial < maxTrial) {

				bookResponse = stub.createBooking(reviewrequest);
				if (bookResponse.getErrors() == null) {
					createReviewResponse(bookResponse, hInfo);
					return true;
				} else if (bookResponse.getErrors().getError()!=null && !bookResponse.getErrors().getError()[0].getCode().equals("038"))
					return false;
				trial++;
			}

		} catch (OMException om) {
			throw new NoSeatAvailableException(om.getMessage());
		} catch (AxisFault e) {
			throw new NoSearchResultException(e.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		}
		finally {

			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
		return false;
	}


	private void createReviewResponse(OTA_HotelResRS bookResponse, HotelInfo hInfo) {

		if (bookResponse.getHotelReservations() != null) {
			if (bookResponse.getHotelReservations().getHotelReservation()[0].getUniqueID()[0] != null) {
				hInfo.getMiscInfo().setSupplierBookingId(
						bookResponse.getHotelReservations().getHotelReservation()[0].getUniqueID()[0].getID());
			}
		}
	}

	private OTA_HotelResRQ createPreBookRequest(HotelInfo hInfo) {

		OTA_HotelResRQ reviewrequest = new OTA_HotelResRQ();
		reviewrequest.setCorrelationID(hInfo.getMiscInfo().getCorrelationId());
	//	reviewrequest.setCorrelationID(order.getBookingId());
	//	reviewrequest.setTransactionIdentifier(order.getBookingId());
		setSource(reviewrequest);

		UniqueID_Type[] uniqueIDs = new UniqueID_Type[1];
		UniqueID_Type uniqueId = new UniqueID_Type();
		uniqueId.setType("");
		uniqueId.setID("");
		uniqueIDs[0] = uniqueId;
		reviewrequest.setUniqueID(uniqueIDs);

		setHotelReservation(reviewrequest, hInfo);
		return reviewrequest;
	}


	private void setHotelReservation(OTA_HotelResRQ reviewrequest, HotelInfo hInfo) {

		HotelReservationsType hotelRes = new HotelReservationsType();
		HotelReservation_type0[] hotelResList = new HotelReservation_type0[1];
		HotelReservation_type0 hotelReservation = new HotelReservation_type0();
		ResGlobalInfoType globalInfo = new ResGlobalInfoType();
		GuaranteeType guaranteeType = new GuaranteeType();

		hotelReservation.setRoomStays(getRoomStays(hInfo));
		hotelReservation.setResGuests(getResGuests(hInfo));

		guaranteeType.setGuaranteeType("PrePay");
		globalInfo.setGuarantee(guaranteeType);
		hotelReservation.setResGlobalInfo(globalInfo);

		hotelResList[0] = hotelReservation;
		hotelRes.setHotelReservation(hotelResList);
		reviewrequest.setHotelReservations(hotelRes);
		reviewrequest.setVersion(new BigDecimal("0"));

	}


	private RoomStaysType getRoomStays(HotelInfo hInfo) {

		int numberOfRooms = hInfo.getOptions().get(0).getRoomInfos().size();

		RoomStaysType roomStays = new RoomStaysType();
		RoomStay_type0[] roomStayList = new RoomStay_type0[1];
		RoomStay_type0 roomStay = new RoomStay_type0();
		RatePlans_type0 ratePlans = new RatePlans_type0();
		RatePlanType[] ratePlanList = new RatePlanType[1];
		RoomTypeType[] roomTypeList = new RoomTypeType[1];
		RoomTypes_type0 roomTypes = new RoomTypes_type0();

		RoomMiscInfo roomMiscInfo = hInfo.getOptions().get(0).getRoomInfos().get(0).getMiscInfo();
		RatePlanType ratePlan = new RatePlanType();
		ratePlan.setRatePlanCode(roomMiscInfo.getRatePlanCode());
		ratePlanList[0] = ratePlan;
		ratePlans.setRatePlan(ratePlanList);

		RoomTypeType roomType = new RoomTypeType();
		roomType.setNumberOfUnits(new BigInteger(String.valueOf(numberOfRooms)));
		roomType.setRoomTypeCode(roomMiscInfo.getRoomTypeCode());
		roomTypeList[0] = roomType;
		roomTypes.setRoomType(roomTypeList);


		DateTimeSpanType timeSpan = new DateTimeSpanType();
		timeSpan.setEnd(searchQuery.getCheckoutDate().format(dateTimeFormatter));
		timeSpan.setStart(searchQuery.getCheckinDate().format(dateTimeFormatter));


		BasicPropertyInfoType basicPropertyInfo = new BasicPropertyInfoType();
		basicPropertyInfo.setHotelCode(hInfo.getMiscInfo().getSupplierStaticHotelId());

		roomStay.setRatePlans(ratePlans);
		roomStay.setRoomTypes(roomTypes);
		roomStay.setGuestCounts(setGuestCount(hInfo));
		roomStay.setTimeSpan(timeSpan);
		roomStay.setTotal(setAmount(hInfo));
		roomStay.setBasicPropertyInfo(basicPropertyInfo);
		roomStayList[0] = roomStay;
		roomStays.setRoomStay(roomStayList);
		return roomStays;
	}

	private TotalType setAmount(HotelInfo hInfo) {

		BigDecimal totalBaseAmount = BigDecimal.ZERO;
		BigDecimal totalTaxAmount = BigDecimal.ZERO;

		for (RoomInfo room : hInfo.getOptions().get(0).getRoomInfos()) {
			RoomMiscInfo rMiscInfo = room.getMiscInfo();
			BigDecimal roomBaseAmount = rMiscInfo.getTotalBaseAmount();
			BigDecimal roomTaxAmount = rMiscInfo.getTotalTaxes();
			totalBaseAmount = totalBaseAmount.add(roomBaseAmount);
			totalTaxAmount = totalTaxAmount.add(roomTaxAmount);
		}

		TotalType total = new TotalType();
		total.setAmountBeforeTax(totalBaseAmount);
		total.setCurrencyCode(DesiyaConstants.CURRENCY);

		TaxesType taxes = new TaxesType();
		taxes.setAmount(totalTaxAmount);
		taxes.setCurrencyCode(DesiyaConstants.CURRENCY);
		total.setTaxes(taxes);

		return total;
	}

	/*
	 * private RoomTypes_type0 setRoomType(HotelInfo hInfo, int numberOfRoom) {
	 * 
	 * 
	 * HashMap<String, Integer> roomTypeMap = new HashMap<>(); for (RoomInfo room :
	 * hInfo.getOptions().get(0).getRoomInfos()) { String key = room.getMiscInfo().getRoomTypeCode(); if
	 * (roomTypeMap.containsKey(key)) { roomTypeMap.put(key, roomTypeMap.get(key) + 1); } else roomTypeMap.put(key, 1);
	 * } RoomTypeType[] roomTypeList = new RoomTypeType[hInfo.getOptions().get(0).getRoomInfos().size()];
	 * RoomTypes_type0 roomTypes = new RoomTypes_type0();
	 * 
	 * for (String key : roomTypeMap.keySet()) {
	 * 
	 * index++;
	 * 
	 * }
	 * 
	 * return roomTypes; }
	 */


	private ResGuestsType getResGuests(HotelInfo hInfo) {

		ResGuestsType resGuests = new ResGuestsType();
		ResGuestType[] guestList = new ResGuestType[1];
		ResGuestType resGuest = new ResGuestType();
		ProfilesType profiles = new ProfilesType();
		ProfileInfo_type0[] profileinfoList = new ProfileInfo_type0[1];
		ProfileInfo_type0 profileInfo = new ProfileInfo_type0();
		ProfileType profile = new ProfileType();

		profile.setProfileType("1");
		profile.setCustomer(getCustomer());

		profileInfo.setProfile(profile);
		profileinfoList[0] = profileInfo;
		profiles.setProfileInfo(profileinfoList);
		resGuest.setProfiles(profiles);

		guestList[0] = resGuest;
		resGuests.setResGuest(guestList);
		return resGuests;
	}


	private CustomerType getCustomer() {

		CustomerType customer = new CustomerType();
		Telephone_type2[] telephoneList = new Telephone_type2[1];
		Telephone_type2 telephone = new Telephone_type2();
		// telephone.setExtension("0");
		telephone.setPhoneNumber(order.getDeliveryInfo().getContacts().get(0));
		telephone.setCountryAccessCode("91");
		telephone.setPhoneTechType("1");
		telephoneList[0] = telephone;
		Email_type0[] emailList = new Email_type0[1];
		Email_type0 email = new Email_type0();
		email.setString(order.getDeliveryInfo().getEmails().get(0));
		emailList[0] = email;
		customer.setEmail(emailList);
		customer.setTelephone(telephoneList);
		customer.setAddress(getaddress());
		setPersonName(customer);

		return customer;
	}


	private Address_type1[] getaddress() {

		Address_type1[] addresses = new Address_type1[1];
		Address_type1 address = new Address_type1();
		String[] addressLine = new String[1];
		String add1 = "Eureka towers, Mindspace";
		addressLine[0] = add1;
		address.setAddressLine(addressLine);

		address.setCityName("Delhi");
		address.setPostalCode("10059");

		StateProvType stateProv = new StateProvType();
		stateProv.setStateCode("DL");
		stateProv.setString("DELHI");
		address.setStateProv(stateProv);
		CountryNameType countryName = new CountryNameType();
		countryName.setCode("IN");
		countryName.setString("IN");
		address.setCountryName(countryName);
		addresses[0] = address;
		return addresses;

	}


	private void setPersonName(CustomerType customer) {

		List<PersonNameType> persons = new ArrayList<>();
		for (RoomInfo room : hInfo.getOptions().get(0).getRoomInfos()) {
			List<String> namePrefix = new ArrayList<String>();
			List<String> givenName = new ArrayList<String>();
			List<String> surName = new ArrayList<String>();
			for (TravellerInfo traveller : room.getTravellerInfo()) {
				namePrefix.add(traveller.getTitle());
				givenName.add(traveller.getFirstName());
				surName.add(traveller.getLastName());
			}
			PersonNameType person = new PersonNameType();
			String[] namePre = new String[namePrefix.size()];
			String[] givenNam = new String[surName.size()];
			String[] surNam = new String[givenName.size()];
			person.setNamePrefix(namePrefix.toArray(namePre));
			person.setGivenName(givenName.toArray(givenNam));
			person.setNameSuffix(surName.toArray(surNam));
			person.setSurname(surName.get(0));
			persons.add(person);
		}
		PersonNameType[] arr = new PersonNameType[persons.size()];
		arr = persons.toArray(arr);

		customer.setPersonName(arr);
	}


	private GuestCountType setGuestCount(HotelInfo hInfo) {

		GuestCountType guestCounts = new GuestCountType();
		guestCounts.setIsPerRoom(false);
		List<GuestCount_type1> guestList = new ArrayList<>();
		int roomCount = 0;
		
		for (RoomInfo room : hInfo.getOptions().get(0).getRoomInfos()) {
			int adultCount = 0;
			for (TravellerInfo traveller : room.getTravellerInfo()) {
				if (traveller.getPaxType().equals(PaxType.ADULT)) {
					adultCount++;
				} else {
					GuestCount_type1 guest = new GuestCount_type1();
					guest.setResGuestRPH("" + roomCount);
					guest.setAgeQualifyingCode("8");
					guest.setCount(1);
					guest.setAge(traveller.getAge());
					guestList.add(guest);
				}
			}
			GuestCount_type1 guest = new GuestCount_type1();
			guest.setResGuestRPH("" + roomCount);
			guest.setAgeQualifyingCode("10");
			guest.setCount(adultCount);
			guestList.add(guest);
			roomCount++;
		}
		GuestCount_type1[] arr = new GuestCount_type1[guestList.size()];
		arr = guestList.toArray(arr);
		guestCounts.setGuestCount(arr);
		return guestCounts;
	}


	private void setSource(OTA_HotelResRQ reviewrequest) {

		POS_Type posType = new POS_Type();
		SourceType[] sourceList = new SourceType[1];
		SourceType source = new SourceType();
		source.setISOCurrency("INR");

		source.setRequestorID(getRequestId());
		sourceList[0] = source;
		posType.setSource(sourceList);
		reviewrequest.setPOS(posType);

	}

}
