package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DesiyaSaxParser extends DefaultHandler {

	List<HotelInfo> hotelInfoL;
	InputStream hotelInfoXmlFileName;
	String tmpValue;
	HotelInfo hotelInfoTmp;
	List<Image> imageList;
	Address address;
	List<String> amenities;

	private HotelInfoSaveManager hotelInfoSaveManager;

	public DesiyaSaxParser(InputStream hotelInfoXmlFileName, HotelInfoSaveManager hotelInfoSaveManager) {

		this.hotelInfoXmlFileName = hotelInfoXmlFileName;
		this.hotelInfoSaveManager = hotelInfoSaveManager;
		hotelInfoL = new ArrayList<HotelInfo>();
	//	parseDocument();
	}

	public void parseDocument() {

		SAXParserFactory factory = SAXParserFactory.newInstance();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(hotelInfoXmlFileName, this);
		} catch (ParserConfigurationException e) {
			log.error("ParserConfig error");
		} catch (SAXException e) {
			log.error("SAXException : xml not well formed");
		} catch (IOException e) {
			log.error("IO error");
		}
	}

	@Override
	public void startElement(String s, String s1, String elementName, Attributes attributes) throws SAXException {

		if (elementName.equalsIgnoreCase("HotelDescriptiveContent")) {
			hotelInfoTmp = HotelInfo.builder().build();
			hotelInfoTmp.setId(attributes.getValue("HotelCode"));
			hotelInfoTmp.setName(attributes.getValue("HotelName"));
			imageList = new ArrayList<>();
			address = Address.builder().build();
			amenities = new ArrayList<>();
		}

		if (elementName.equalsIgnoreCase("Position")) {
			String longitude = attributes.getValue("Longitude");
			String latitude = attributes.getValue("Latitude");
			GeoLocation geoLocation = GeoLocation.builder().latitude(latitude).longitude(longitude).build();
			hotelInfoTmp.setGeolocation(geoLocation);
		}
	}

	@Override
	public void endElement(String s, String s1, String element) throws SAXException {

		if (element.equals("HotelDescriptiveContent")) {
			hotelInfoTmp.setImages(imageList);
			hotelInfoL.add(hotelInfoTmp);
			hotelInfoTmp.setAddress(address);
			hotelInfoTmp.setFacilities(amenities);
			if (hotelInfoL.size() == 100) {
				saveHotelInfoList(hotelInfoL);
				hotelInfoL.clear();
			}
		}
		if (element.equals("HotelDescriptiveContents")) {
			if (hotelInfoL.size() > 0) {
				saveHotelInfoList(hotelInfoL);
			}
		}
		if (element.equalsIgnoreCase("URL")) {
			if (tmpValue.contains("https:"))
				imageList.add(Image.builder().bigURL(tmpValue).thumbnail(tmpValue).build());
		}
		if (element.equalsIgnoreCase("DescriptiveText")) {
			hotelInfoTmp.setDescription(tmpValue);
		}
		if (element.equalsIgnoreCase("AddressLine")) {
			if (tmpValue.contains("Address:")) {
				String add = tmpValue.replace("Address:", "");
				address.setAddress(add);
			}
		}
		if (element.equalsIgnoreCase("CityName")) {
			address.setCity(City.builder().name(tmpValue).build());
		}
		if (element.equalsIgnoreCase("County")) {
			address.setCountry(Country.builder().name(tmpValue).build());
		}
		if (element.equalsIgnoreCase("Hotel_Star")) {
			hotelInfoTmp.setRating(Integer.parseInt(tmpValue));
		}
		if (element.equalsIgnoreCase("HotelPinCode")) {
			address.setPostalCode(tmpValue);
		}
		if (element.equalsIgnoreCase("Description")) {
			amenities.add(tmpValue);
		}
	}

	@Override
	public void characters(char[] ac, int i, int j) throws SAXException {
		tmpValue = new String(ac, i, j);
	}

	public void saveHotelInfoList(List<HotelInfo> hInfoList) {

		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				dbHotelInfo.setId(null);
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hInfo.getId(), HotelSourceType.DESIYA.name(),
						HotelSourceType.DESIYA);
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {}", hInfo.getName(), hInfo.getRating(),
						e);
			}
		}
	}
}
