package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.math.BigDecimal;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.apache.axiom.om.OMException;
import org.apache.axis2.AxisFault;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SoapRequestResponseListner;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.exception.air.NoSearchResultException;
import com.tgs.utils.exception.air.NoSeatAvailableException;
import com.tgs.utils.exception.air.SupplierRemoteException;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AddressInfoType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Address_type3;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AvailRequestSegment_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.AvailRequestSegments_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.BasicPropertyInfoType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.CountryNameType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Criterion_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.DateTimeSpanType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.GuestCountType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.GuestCount_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.HotelBasicInformation_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.HotelRef_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.HotelSearchCriteria_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.OTA_HotelAvailRQ;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.OTA_HotelAvailRS;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Pagination_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Position_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.Promotion_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RatePlanType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomStayCandidateType;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomStayCandidates_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomStay_type1;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.RoomStays_type0;
import com.travelguru.services.endpoints.TGServiceEndPointImplServiceStub.TPA_ExtensionsType;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@SuperBuilder
public class DesiyaSearchService extends DesiyaBaseService {

	private CityInfoMapping supplierCityInfo;
	private HotelInfo hInfo;
	private HotelSearchResult searchResult;
	private SoapRequestResponseListner listener;
	private TGServiceEndPointImplServiceStub stub;
	private static final int maxattempt = 3;
	private Set<String> propertyIds;

	public void doSearch() throws IOException {

		searchResult = new HotelSearchResult();
		OTA_HotelAvailRQ searchRequest = null;
		OTA_HotelAvailRS searchResponse = null;
		searchQuery.getSearchPreferences().setRatings(new ArrayList<>(Arrays.asList(0, 1, 2, 3, 4, 5)));
		stub = DesiyaBindingService.getSearchStub(supplierConf);
		try {
			searchRequest = createSearchRequest();
			listener.setType(getLogType("Desiya City-Search"));
			stub._getServiceClient().getAxisService().addMessageContextListener(listener);

			searchResponse = stub.fetchResponse(searchRequest);
			if (searchResponse != null && searchResponse.getErrors() == null)
				searchResult = createSearchResponse(searchResponse);
		} catch (OMException om) {
			throw new NoSearchResultException(om.getMessage());
		} catch (AxisFault e) {
			log.error("Error While fetching Search Result", e);
			throw new NoSearchResultException(e.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}


	private OTA_HotelAvailRQ createSearchRequest() throws UnknownHostException {

		OTA_HotelAvailRQ searchReq = new OTA_HotelAvailRQ();
		searchReq.setRequestedCurrency(DesiyaConstants.CURRENCY);
		searchReq.setPrimaryLangID("en");
		searchReq.setSortOrder(DesiyaConstants.SORT_ORDER);
		searchReq.setVersion(new BigDecimal("0.0"));
		setAvailRequestSegment(searchReq);
		return searchReq;
	}

	private void setAvailRequestSegment(OTA_HotelAvailRQ searchReq) throws UnknownHostException {

		AvailRequestSegment_type0[] reqSegment = new AvailRequestSegment_type0[1];
		AvailRequestSegments_type0 reqSegments = new AvailRequestSegments_type0();

		HotelSearchCriteria_type0 hotelSearchcriteria = new HotelSearchCriteria_type0();
		Criterion_type0[] criterias = new Criterion_type0[1];
		Criterion_type0 criteria = new Criterion_type0();
		DateTimeSpanType stayDate = new DateTimeSpanType();
		TPA_ExtensionsType extension = new TPA_ExtensionsType();
		AvailRequestSegment_type0 reqSeg = new AvailRequestSegment_type0();

		stayDate.setEnd(searchQuery.getCheckoutDate() + "");
		stayDate.setStart(searchQuery.getCheckinDate() + "");
		criteria.setStayDateRange(stayDate);

		setAddress(criteria);

		setAuthentication(extension, supplierConf);
		setPagination(extension);
		setPromotion(extension);
		criteria.setTPA_Extensions(extension);

		createRoomCandidate(criteria);

		criterias[0] = criteria;
		hotelSearchcriteria.setCriterion(criterias);
		reqSeg.setHotelSearchCriteria(hotelSearchcriteria);
		reqSegment[0] = reqSeg;
		reqSegments.setAvailRequestSegment(reqSegment);
		searchReq.setAvailRequestSegments(reqSegments);
	}


	private void setPromotion(TPA_ExtensionsType extension) {

		Promotion_type0 promotion = new Promotion_type0();
		promotion.setType("HOTEL");
		promotion.setName("StayPeriod");
		extension.setPromotion(promotion);
	}


	private void setPagination(TPA_ExtensionsType extension) {

		Pagination_type0 pagination = new Pagination_type0();
		pagination.setEnabled(false);
		extension.setPagination(pagination);
	}


	private void setAddress(Criterion_type0 criteria) {

		Address_type3 address = new Address_type3();
		address.setCityName(supplierCityInfo.getSupplierCity());
		CountryNameType country = new CountryNameType();
		country.setCode(supplierCityInfo.getSupplierCountry());
		country.setString(supplierCityInfo.getSupplierCountry());
		address.setCountryName(country);
		criteria.setAddress(address);
	}


	private HotelSearchResult createSearchResponse(OTA_HotelAvailRS searchResponse) {

		RoomStays_type0 roomStayType = searchResponse.getRoomStays();
		RoomStay_type1[] roomStayArray = roomStayType.getRoomStay();

		List<HotelInfo> hInfoList = new ArrayList<>();
		Arrays.stream(roomStayArray).forEach((roomStay) -> {

			HotelInfo hInfo = getHotelInfoFromRoomStay(roomStay, false);
			hInfo.getMiscInfo().setCorrelationId(searchResponse.getCorrelationID());
			List<Instruction> instructions = new ArrayList<>();
			StringBuilder bookingNotes = new StringBuilder();

			if (!ObjectUtils.isEmpty(sourceConfig) && !ObjectUtils.isEmpty(sourceConfig.getBookingNotes())) {
				sourceConfig.getBookingNotes().forEach(bookingNote -> {
					bookingNotes.append(bookingNote);
				});
			}
			Instruction instruction =
					Instruction.builder().type(InstructionType.BOOKING_NOTES).msg(bookingNotes.toString()).build();
			instructions.add(instruction);

			hInfo.setInstructions(instructions);
			hInfoList.add(hInfo);
		});

		if (CollectionUtils.isEmpty(hInfoList)) {
			log.error("No Hotel Found for SearchID {}", searchQuery.getSearchId());
			throw new CustomGeneralException(SystemError.NO_HOTEL_FOUND);
		}
		searchResult.setHotelInfos(hInfoList);
		return searchResult;
	}


	private OTA_HotelAvailRQ createDetailRequest() throws IOException {

		OTA_HotelAvailRQ detailRequest = new OTA_HotelAvailRQ();
		detailRequest.setRequestedCurrency(DesiyaConstants.CURRENCY);
		detailRequest.setSortOrder(DesiyaConstants.SORT_ORDER);
		detailRequest.setCorrelationID(searchQuery.getSearchId());
		detailRequest.setSearchCacheLevel("Live");
		detailRequest.setVersion(new BigDecimal("0.0"));
		setDetailAvailRequestSegment(detailRequest);
		return detailRequest;
	}


	private void setDetailAvailRequestSegment(OTA_HotelAvailRQ detailRequest) throws UnknownHostException {

		AvailRequestSegment_type0[] reqSegment = new AvailRequestSegment_type0[1];
		AvailRequestSegments_type0 reqSegments = new AvailRequestSegments_type0();
		HotelSearchCriteria_type0 hotelSearchcriteria = new HotelSearchCriteria_type0();
		Criterion_type0[] criterias = new Criterion_type0[1];
		HotelRef_type0[] hotelRefs = new HotelRef_type0[1];
		List<HotelRef_type0> hotelRefList = new ArrayList<>();
		Criterion_type0 criteria = new Criterion_type0();
		TPA_ExtensionsType extension = new TPA_ExtensionsType();
		AvailRequestSegment_type0 reqSeg = new AvailRequestSegment_type0();

		if (propertyIds != null && CollectionUtils.isNotEmpty(propertyIds)) {

			propertyIds.forEach(propertyId -> {
				HotelRef_type0 hotelref = new HotelRef_type0();
				hotelref.setHotelCode(propertyId);
				hotelRefList.add(hotelref);

			});
			hotelRefs = hotelRefList.toArray(new HotelRef_type0[0]);
			criteria.setHotelRef(hotelRefs);
		} else {
			HotelRef_type0 hotelref = new HotelRef_type0();
			hotelref.setHotelCode(hInfo.getMiscInfo().getSupplierStaticHotelId());
			hotelRefs[0] = hotelref;
			criteria.setHotelRef(hotelRefs);
		}
		createRoomCandidate(criteria);

		DateTimeSpanType stayDate = getDateSpanTimeFromSearchQuery();
		criteria.setStayDateRange(stayDate);

		setAuthentication(extension, supplierConf);
		criteria.setTPA_Extensions(extension);
		criterias[0] = criteria;
		hotelSearchcriteria.setCriterion(criterias);
		reqSeg.setHotelSearchCriteria(hotelSearchcriteria);
		reqSegment[0] = reqSeg;
		reqSegments.setAvailRequestSegment(reqSegment);
		detailRequest.setAvailRequestSegments(reqSegments);
	}

	private DateTimeSpanType getDateSpanTimeFromSearchQuery() {

		DateTimeSpanType stayDate = new DateTimeSpanType();
		stayDate.setEnd(searchQuery.getCheckoutDate().toString());
		stayDate.setStart(searchQuery.getCheckinDate().toString());
		return stayDate;
	}


	public void createRoomCandidate(Criterion_type0 criteria) {

		RoomStayCandidates_type0 candidates = new RoomStayCandidates_type0();
		RoomStayCandidateType[] candidateList = new RoomStayCandidateType[searchQuery.getRoomInfo().size()];


		for (int roomCount = 0; roomCount < searchQuery.getRoomInfo().size(); roomCount++) {
			RoomSearchInfo roomSearchInfo = searchQuery.getRoomInfo().get(roomCount);
			int guestIndex = 0, noOfChild = 0;
			RoomStayCandidateType candidate = new RoomStayCandidateType();
			GuestCountType guests = new GuestCountType();
			if (!Objects.isNull(roomSearchInfo.getNumberOfChild()))
				noOfChild = roomSearchInfo.getNumberOfChild();

			GuestCount_type1[] guestList = new GuestCount_type1[noOfChild + 1];
			GuestCount_type1 adultGuest = new GuestCount_type1();
			adultGuest.setAgeQualifyingCode("10");
			adultGuest.setCount(roomSearchInfo.getNumberOfAdults());
			guestList[guestIndex++] = adultGuest;

			for (int i = 0; i < noOfChild; i++) {
				GuestCount_type1 childGuest = new GuestCount_type1();
				childGuest.setAgeQualifyingCode("8");
				childGuest.setAge(roomSearchInfo.getChildAge().get(i));
				childGuest.setCount(1);
				guestList[guestIndex++] = childGuest;
			}
			guests.setGuestCount(guestList);
			candidate.setGuestCounts(guests);
			candidateList[roomCount] = candidate;
		}

		candidates.setRoomStayCandidate(candidateList);
		criteria.setRoomStayCandidates(candidates);
	}

	public void doDetailSearch(HotelInfo hInfo) throws IOException {

		OTA_HotelAvailRQ detailRequest = createDetailRequest();
		String searchId = HotelUtils.getSearchId(hInfo.getId());
		int attempt = 0;
		try {
			stub = DesiyaBindingService.getDetailStub(supplierConf);
			listener.setType(getLogType("Desiya Detail-Search"));

			stub._getServiceClient().getAxisService().addMessageContextListener(listener);
			do {
				// have added the loop as the detail api gives result in two or 3 hits
				OTA_HotelAvailRS detailResponse = stub.fetchResponse(detailRequest);
				if (detailResponse != null && detailResponse.getErrors() == null) {
					if (detailResponse.getRoomStays() != null && detailResponse.getRoomStays().getRoomStay() != null) {
						createDetailResponse(detailResponse, hInfo, searchId);
						break;
					}
					attempt++;
				}
			} while (attempt < maxattempt);

			SystemContextHolder.getContextData()
					.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
							.subType(HotelFlowType.DETAIL.name()).time(System.currentTimeMillis()).build());

		} catch (OMException om) {
			throw new NoSeatAvailableException(om.getMessage());
		} catch (AxisFault e) {
			throw new NoSearchResultException(e.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {

			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}
	}


	public String getRatePlan(int ratePlanId) {

		if (ratePlanId == 24)
			return "public rate plan";
		else if (ratePlanId == 10)
			return "opaque rate plan";
		return null;

	}

	private void createDetailResponse(OTA_HotelAvailRS detailResponse, HotelInfo hInfo, String searchId) {

		if (detailResponse == null || detailResponse.getRoomStays() == null) {
			log.info("Error in getting Room Response for searchId {} , hotel {}", searchQuery.getSearchId(),
					hInfo.getName());
			return;
		}
		hInfo.getMiscInfo().setCorrelationId(detailResponse.getCorrelationID());

		RoomStay_type1 desiyaHotelInfo = detailResponse.getRoomStays().getRoomStay()[0];
		HotelInfo hotelInfo = getHotelInfoFromRoomStay(desiyaHotelInfo, true);

		hInfo.setOptions(hotelInfo.getOptions());

		populateHotelDetails(desiyaHotelInfo, hInfo);
	}

	private void populateHotelDetails(RoomStay_type1 desiyaHotelInfo, HotelInfo hInfo) {

		if (desiyaHotelInfo.getTPA_Extensions() != null) {
			HotelBasicInformation_type0 basicInfo = desiyaHotelInfo.getTPA_Extensions().getHotelBasicInformation();
			hInfo.setDescription(basicInfo.getAmenityDescription());
			hInfo.setFacilities(getHotelAmenities(desiyaHotelInfo));
		}

		if (desiyaHotelInfo.getBasicPropertyInfo() != null) {
			BasicPropertyInfoType basicPropertyInfo = desiyaHotelInfo.getBasicPropertyInfo();
			AddressInfoType desiyaAddress = basicPropertyInfo.getAddress();
			hInfo.setAddress(getAddressFromDesiyaAddress(desiyaAddress));

			Position_type1 pos = basicPropertyInfo.getPosition();
			hInfo.setGeolocation(getGeoLocationFromDesiyaPosition(pos));
		}
		for (Option selectedOption : hInfo.getOptions()) {
			for (RatePlanType ratePlan : desiyaHotelInfo.getRatePlans().getRatePlan()) {
				if (selectedOption.getRoomInfos().get(0).getMiscInfo().getRatePlanCode()
						.equals(ratePlan.getRatePlanCode())) {
					setOptionCancellationPolicy(ratePlan, searchQuery, selectedOption);
					break;
				}
			}
		}
	}

	public void doPropertyIdBasedSearch() throws IOException {

		searchResult = new HotelSearchResult();
		OTA_HotelAvailRQ searchRequest = null;
		OTA_HotelAvailRS searchResponse = null;
		stub = DesiyaBindingService.getSearchStub(supplierConf);

		try {
			searchRequest = createDetailRequest();
			listener.setType(getLogType("Desiya HotelId Based-Search"));

			stub._getServiceClient().getAxisService().addMessageContextListener(listener);

			searchResponse = stub.fetchResponse(searchRequest);
			if (searchResponse != null && searchResponse.getErrors() == null)
				searchResult = createSearchResponse(searchResponse);

		} catch (OMException om) {
			throw new NoSearchResultException(om.getMessage());
		} catch (AxisFault e) {
			log.error("Error While fetching Search Result", e);
			throw new NoSearchResultException(e.getMessage());
		} catch (RemoteException e) {
			throw new SupplierRemoteException(e);
		} finally {
			stub._getServiceClient().getAxisService().removeMessageContextListener(listener);
		}

	}
}
