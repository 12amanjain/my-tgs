package com.tgs.services.hms.sources.desiya;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DesiyaStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	private HotelCityInfoMappingManager cityManager;

	public DesiyaStaticDataFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getCityMappingData() throws IOException {
		DesiyaCitySaveService citySaveService = DesiyaCitySaveService.builder().supplierConf(supplierConf).cityManager(cityManager)
				.build();
		citySaveService.saveDesiyaCities();
	}

	@Override
	protected void getHotelStaticData() throws IOException {

		DesiyaHotelStaticDataSaveService hotelSaveService =
				DesiyaHotelStaticDataSaveService.builder().hotelInfoSaveManager(hotelInfoSaveManager).supplierConf(supplierConf).build();
		try {
			hotelSaveService.process();
		}catch(Exception e) {
			log.error("Error While fetching static data" , e);
		}
		

	}

}
