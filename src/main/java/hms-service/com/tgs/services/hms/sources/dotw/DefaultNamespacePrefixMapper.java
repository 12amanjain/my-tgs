package com.tgs.services.hms.sources.dotw;

import java.util.HashMap;
import java.util.Map;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;

public class DefaultNamespacePrefixMapper extends NamespacePrefixMapper{

	private Map<String, String> namespaceMap = new HashMap<>();
	
	public DefaultNamespacePrefixMapper() {
		namespaceMap.put("http://us.dotwconnect.com/xsd/atomicCondition", "a");
		namespaceMap.put("http://us.dotwconnect.com/xsd/complexCondition", "c");
	}
	
	@Override
	public String getPreferredPrefix(String namespaceUri, String suggestion, boolean requirePrefix) {
		return namespaceMap.getOrDefault(namespaceUri, suggestion);
	}
	
	
	

}
