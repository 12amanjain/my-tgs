package com.tgs.services.hms.sources.dotw;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwCancellationRule;
import com.tgs.services.hms.datamodel.dotw.DotwChild;
import com.tgs.services.hms.datamodel.dotw.DotwChildren;
import com.tgs.services.hms.datamodel.dotw.DotwHotel;
import com.tgs.services.hms.datamodel.dotw.DotwPerDayRoomPrice;
import com.tgs.services.hms.datamodel.dotw.DotwRoomList;
import com.tgs.services.hms.datamodel.dotw.DotwRoomRequest;
import com.tgs.services.hms.datamodel.dotw.DotwRoomResponse;
import com.tgs.services.hms.datamodel.dotw.DotwRoomType;
import com.tgs.services.hms.datamodel.dotw.DotwSearchCriteria;
import com.tgs.services.hms.datamodel.dotw.RateBasis;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConstants;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.utils.common.HttpUtils;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
public class DotwBaseService {


	protected HotelInfo hInfo;
	protected HotelSearchQuery searchQuery;
	protected Results result;
	protected HotelSupplierConfiguration supplierConf;
	protected RestAPIListener listener;
	protected HttpUtils httpUtils;
	protected HotelCacheHandler cacheHandler;
	protected MoneyExchangeCommunicator moneyExchnageComm;

	protected Map<String, List<RoomInfo>> getRoomInfoMap(DotwHotel hotel) {

		Map<String, List<RoomInfo>> roomInfoMap = new HashMap<>();
		for (DotwRoomResponse room : hotel.getRooms()) {
			int numberOfAdults = Integer.valueOf(room.getAdults());
			int numberOfChilds = Integer.valueOf(room.getChild());
			for (DotwRoomType roomType : room.getRoomType()) {
				String key = roomType.getRoomtypecode();
				RoomInfo roomInfo = new RoomInfo();
				roomInfo.setNumberOfAdults(numberOfAdults);
				roomInfo.setNumberOfChild(numberOfChilds);
				RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomTypeCode(roomType.getRoomtypecode()).build();
				roomInfo.setMiscInfo(roomMiscInfo);
				roomInfo.setRoomType(roomType.getName());
				roomInfo.setRoomCategory(roomType.getName());
				if(!(roomType.getRateBases() != null 
						&& CollectionUtils.isNotEmpty(roomType.getRateBases().getRateBasis())))
					continue;
				
				for (RateBasis rateBasis : roomType.getRateBases().getRateBasis()) {
					if (!Objects.isNull(rateBasis.getValidForOccupancy()))
						continue;
					if (rateBasis.getLeftToSell() != null
							&& rateBasis.getLeftToSell() < searchQuery.getRoomInfo().size())
						continue;
					RoomInfo rInfo = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(roomInfo), RoomInfo.class);
					rInfo.getMiscInfo().setNotes(rateBasis.getTariffNotes());
					key += "-" + rateBasis.getId();
					rInfo.getMiscInfo().setRatePlanCode(rateBasis.getId());
					if (StringUtils.isNotBlank(rateBasis.getStatus())) {
						if (rateBasis.getStatus().equalsIgnoreCase("checked"))
							rInfo.getMiscInfo().setIsRoomBlocked(true);
						else
							rInfo.getMiscInfo().setIsRoomBlocked(false);
					}
					if (StringUtils.isNotBlank(rateBasis.getPassengerNamesRequiredForBooking())) {
						rInfo.getMiscInfo().setNumberOfPassengerNameRequiredForbooking(
								Integer.parseInt(rateBasis.getPassengerNamesRequiredForBooking()));
					}
					rInfo.setMealBasis(getMealBasis(rateBasis.getId()));
					rInfo.getMiscInfo().setAllocationDetails(rateBasis.getAllocationDetails());
					rInfo.setCheckInDate(searchQuery.getCheckinDate());
					rInfo.setCheckOutDate(searchQuery.getCheckoutDate());
					HotelCancellationPolicy cancellationPolicy =
							getRoomCancellationPolicy(rateBasis, searchQuery, rInfo);
					rInfo.setCancellationPolicy(cancellationPolicy);
					if (cancellationPolicy.getIsFullRefundAllowed())
						key += "-" + 0;
					else
						key += "-" + 1;
					if (rateBasis.getOnRequest() != null && rateBasis.getOnRequest().equals("1")) {
						rInfo.setIsOptionOnRequest(true);
						key += "-" + 1;
					}

					rInfo.setId(key);
					rInfo.getCancellationPolicy().setId(key);
					Double price = getTotalRoomPrice(rateBasis);
					rInfo.setTotalPrice(price);
					List<PriceInfo> priceInfoList = null;
					if (CollectionUtils.isNotEmpty(rateBasis.getDate())) {
						priceInfoList = getDailyPriceInfo(rateBasis.getDate());
					} else {
						priceInfoList = new ArrayList<>();
						long numberOfNights = Duration.between(searchQuery.getCheckinDate().atStartOfDay(),
								searchQuery.getCheckoutDate().atStartOfDay()).toDays();
						numberOfNights = Math.max(numberOfNights, 0);
						double perNightPrice = (rInfo.getTotalPrice()) / numberOfNights;
						for (int j = 1; j <= numberOfNights; j++) {
							PriceInfo priceInfo = new PriceInfo();
							Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
							fareComponents.put(HotelFareComponent.BF, perNightPrice);
							priceInfo.setDay(j);
							priceInfo.setFareComponents(fareComponents);
							priceInfoList.add(priceInfo);
						}
					}
					rInfo.setPerNightPriceInfos(priceInfoList);
					if (roomInfoMap.containsKey(key)) {
						roomInfoMap.get(key).add(rInfo);
					} else {
						List<RoomInfo> newRoomInfoList = new ArrayList<>();
						newRoomInfoList.add(rInfo);
						roomInfoMap.put(key, newRoomInfoList);
					}
				}
			}
		}
		return roomInfoMap;
	}

	protected List<Option> getOptionList(DotwHotel hotel) {

		Map<String, List<RoomInfo>> roomInfoMap = getRoomInfoMap(hotel);
		List<Option> optionList = new ArrayList<>();
		for (String keySet : roomInfoMap.keySet()) {
			List<RoomInfo> roomInfos = roomInfoMap.get(keySet);
			if (searchQuery.getRoomInfo().size() != roomInfos.size())
				continue;
			boolean isOptionOnRequest = checkIfOptionOnRequest(roomInfos);

			// HotelCancellationPolicy optionCancellationPolicy = getOptionCancellationPolicyFromRooms(roomInfos);
			Option option = Option.builder()
					.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
							.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
							.supplierHotelId(hotel.getHotelid()).sourceId(supplierConf.getBasicInfo().getSourceId())
							.build())
					.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true))
					// .cancellationPolicy(optionCancellationPolicy)
					.instructions(
							new ArrayList<>(Arrays.asList(Instruction.builder().type(InstructionType.BOOKING_NOTES)
									.msg(roomInfos.get(0).getMiscInfo().getNotes()).build())))
					.build();
			setCancellationDeadlineFromRooms(roomInfos, option);
			String optionId = option.getId();
			option.getCancellationPolicy().setId(optionId);
			// roomInfos.stream().forEach(ri -> ri.setCancellationPolicy(optionCancellationPolicy));
			if (isOptionOnRequest)
				option.setIsOptionOnRequest(true);
			optionList.add(option);
		}
		return optionList;
	}

	// public HotelCancellationPolicy getOptionCancellationPolicyFromRooms(List<RoomInfo> roomInfoList) {
	//
	// LocalDateTime earliestDeadlineDateTime = null;
	// LocalDateTime earliestCancellationRestrictedDateTime = null;
	// LocalDateTime checkInDate = null;
	// Double totalOptionPrice = 0.0;
	// boolean isFullRefundAllowed = true;
	// // String tariffNotes = null;
	// for (RoomInfo roomInfo : roomInfoList) {
	// checkInDate = roomInfo.getCheckInDate().atTime(12, 00);
	// // tariffNotes = roomInfo.getCancellationPolicy().getNotes();
	// totalOptionPrice += roomInfo.getTotalPrice();
	// if (earliestDeadlineDateTime == null)
	// earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
	// else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
	// earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
	//
	// if (earliestCancellationRestrictedDateTime == null)
	// earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
	// else if (earliestCancellationRestrictedDateTime.isAfter(roomInfo.getCancellationRestrictedDateTime()))
	// earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
	//
	// if (!roomInfo.getCancellationPolicy().getIsFullRefundAllowed())
	// isFullRefundAllowed = false;
	// }
	//
	// List<PenaltyDetails> pds = new ArrayList<>();
	// if (isFullRefundAllowed) {
	// PenaltyDetails pd1 = PenaltyDetails.builder().fromDate(LocalDateTime.now()).penaltyAmount(0.0)
	// .toDate(earliestDeadlineDateTime).build();
	// pds.add(pd1);
	// }
	// PenaltyDetails pd2 = PenaltyDetails.builder().fromDate(earliestDeadlineDateTime).penaltyAmount(totalOptionPrice)
	// .toDate(checkInDate).build();
	// pds.add(pd2);
	//
	// if (earliestCancellationRestrictedDateTime != null) {
	// PenaltyDetails pd3 = PenaltyDetails.builder().fromDate(earliestCancellationRestrictedDateTime)
	// .toDate(checkInDate).isCancellationRestricted(true).build();
	// pds.add(pd3);
	// }
	//
	//
	// HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
	// .isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
	// .miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();
	// return cancellationPolicy;
	//
	// }

	public void setCancellationDeadlineFromRooms(List<RoomInfo> roomInfoList, Option option) {

		LocalDateTime earliestDeadlineDateTime = null;
		LocalDateTime earliestCancellationRestrictedDateTime = null;

		for (RoomInfo roomInfo : roomInfoList) {

			if (earliestDeadlineDateTime == null)
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();

			if (earliestCancellationRestrictedDateTime == null)
				earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();
			else if (earliestCancellationRestrictedDateTime.isAfter(roomInfo.getCancellationRestrictedDateTime()))
				earliestCancellationRestrictedDateTime = roomInfo.getCancellationRestrictedDateTime();

		}

		option.setDeadlineDateTime(earliestDeadlineDateTime);
		if (earliestCancellationRestrictedDateTime != null) {
			option.setCancellationRestrictedDateTime(earliestCancellationRestrictedDateTime);
		}

	}


	protected boolean checkIfOptionOnRequest(List<RoomInfo> roomInfoList) {

		boolean isOptionOnRequest =
				roomInfoList.stream().map(ri -> ri.getIsOptionOnRequest()).reduce(false, (a, b) -> a | b);
		return isOptionOnRequest;

	}


	protected Double getTotalSupplierPrice(List<RoomInfo> roomInfoList) {

		Double totalSupplierOptionPrice =
				roomInfoList.stream().map(ri -> ri.getTotalPrice()).reduce(0.0, (a, b) -> a + b);
		return totalSupplierOptionPrice;
	}


	protected String getMealBasis(String rateBasisId) {

		if (rateBasisId.equals("0"))
			return HotelConstants.ROOM_ONLY;
		else if (rateBasisId.equals("1331"))
			return "Breakfast";
		else if (rateBasisId.equals("1334"))
			return "Half Board";
		else if (rateBasisId.equals("1335"))
			return "Full Board";
		else if (rateBasisId.equals("1336"))
			return "All Inclusive";
		return "Default";
	}

	protected String getTitleFromSaluationCode(String code) {

		if (code.equals("14632"))
			return "Child";
		else if (code.equals("558"))
			return "Dr.";
		else if (code.equals("1671"))
			return "Madam";
		else if (code.equals("9234"))
			return "Messrs.";
		else if (code.equals("15134"))
			return "Miss";
		else if (code.equals("149"))
			return "Mrs.";
		else if (code.equals("148"))
			return "Ms.";
		else if (code.equals("1328"))
			return "Sir";
		else if (code.equals("3801"))
			return "Sir/Madam";
		return "Mr.";


	}

	protected LocalDateTime getFormattedDate(String localDateTime) {

		String input = localDateTime.replace(" ", "T");
		return LocalDateTime.parse(input);

	}

	protected HotelCancellationPolicy getRoomCancellationPolicy(RateBasis rateBasis, HotelSearchQuery searchQuery,
			RoomInfo rInfo) {

		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		List<PenaltyDetails> pds = new ArrayList<>();
		List<DotwCancellationRule> dcps = rateBasis.getCancellationRules();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		LocalDateTime cancellationRestrictedDatetime = null;
		boolean isFullRefundAllowed = false;
		for (DotwCancellationRule dcp : dcps) {

			if (dcp.getCancelCharge() != null && dcp.getCancelCharge() == 0) {
				isFullRefundAllowed = true;
				deadlineDateTime = getFormattedDate(dcp.getToDate());
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(now).toDate(deadlineDateTime)
						.penaltyAmount(dcp.getCancelCharge()).build();
				pds.add(penaltyDetails1);
			} else if (dcp.getCancelRestricted() != null && dcp.getCancelRestricted()) {
				if (pds.size() > 0)
					cancellationRestrictedDatetime = pds.get(pds.size() - 1).getToDate();
				else
					cancellationRestrictedDatetime = LocalDateTime.now();
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(cancellationRestrictedDatetime)
						.toDate(checkInDate).isCancellationRestricted(true).build();
				pds.add(penaltyDetails1);
				break;

			} else if (dcp.getAmendRestricted() != null && dcp.getAmendRestricted()
					|| dcp.getNoShowPolicy() != null && dcp.getNoShowPolicy())
				continue;
			else {
				LocalDateTime fromDate = null;
				if (pds.size() > 0)
					fromDate = pds.get(pds.size() - 1).getToDate();
				else
					fromDate = LocalDateTime.now();
				Double totalBookingAmount = getTotalRoomPrice(rateBasis);
				PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
						.penaltyAmount(totalBookingAmount).build();
				pds.add(penaltyDetails1);
			}
		}

		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds).miscInfo(CancellationMiscInfo.builder()
						.isBookingAllowed(true).isSoldOut(false).isCancellationPolicyBelongToRoom(true).build())
				.build();
		rInfo.setDeadlineDateTime(deadlineDateTime);
		rInfo.setCancellationRestrictedDateTime(cancellationRestrictedDatetime);
		return cancellationPolicy;
	}

	protected Double getTotalRoomPrice(RateBasis rateBasis) {

		Double amount = rateBasis.getTotalInRequestedCurrency() != null ? rateBasis.getTotalInRequestedCurrency()
				: rateBasis.getTotal();
		return getAmountBasedOnCurrency(amount, "USD");

	}


	protected Customer getCustomer(HotelSupplierConfiguration conf) {

		Customer customer = new Customer();
		HotelSupplierCredential supplierCredentials = conf.getHotelSupplierCredentials();
		customer.setUsername(supplierCredentials.getUserName());
		customer.setPassword(supplierCredentials.getPassword());
		customer.setId(supplierCredentials.getClientId());
		customer.setSource("1");
		return customer;
	}

	protected Map<String, String> getHeaderParams() {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "text/xml");
		headerParams.put("Accept-Encoding", "gzip");
		headerParams.put("Connection", "close");
		return headerParams;

	}

	protected HttpUtils getHttpRequest(String xmlRequest, HotelSupplierConfiguration supplierConf) {

		HttpUtils httpUtils = HttpUtils.builder().urlString(supplierConf.getHotelSupplierCredentials().getUrl())
				.postData(xmlRequest).headerParams(getHeaderParams()).requestMethod(HttpUtils.REQ_METHOD_POST)
				.timeout(400 * 1000).build();
		return httpUtils;
	}


	protected DotwSearchCriteria getSearchCriteriaFromSearchQuery(HotelSearchQuery searchQuery) {

		String country = searchQuery.getSearchCriteria().getCountryOfResidence() != null
				? searchQuery.getSearchCriteria().getCountryOfResidence()
				: "IN";
		String countryOfResidence = cacheHandler.fetchSupplierNationalityMapping("DOTW", country);
		String nationalityCode =
				cacheHandler.fetchSupplierNationalityMapping("DOTW", searchQuery.getSearchCriteria().getNationality());
		List<DotwRoomRequest> rooms = new ArrayList<>();
		DotwRoomList roomList = new DotwRoomList();
		roomList.setNo(searchQuery.getRoomInfo().size());
		DotwSearchCriteria searchCriteria = new DotwSearchCriteria();
		String formattedFromDate = searchQuery.getCheckinDate().format(DateTimeFormatter.ISO_DATE);
		String formattedToDate = searchQuery.getCheckoutDate().format(DateTimeFormatter.ISO_DATE);
		searchCriteria.setFromDate(String.valueOf(formattedFromDate));
		searchCriteria.setToDate(String.valueOf(formattedToDate));
		searchCriteria.setCurrency(DotwConstants.CURRENCY.getValue());
		int index = 0;
		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			DotwRoomRequest room = new DotwRoomRequest();
			room.setNumber(String.valueOf(index));
			room.setAdultsCode(roomSearchInfo.getNumberOfAdults());
			room.setPassengerCountryOfResidence(countryOfResidence);
			room.setPassengerNationality(nationalityCode);
			DotwChildren children = getRoomChildrenRequest(roomSearchInfo);
			room.setChildren(children);
			room.setRateBasis(-1);
			rooms.add(room);
			index++;
		}
		roomList.setRoom(rooms);
		searchCriteria.setRooms(roomList);
		return searchCriteria;
	}


	public DotwChildren getRoomChildrenRequest(RoomSearchInfo roomSearchInfo) {

		DotwChildren children = new DotwChildren();
		children.setNo(0);
		if (roomSearchInfo.getNumberOfChild() != null && roomSearchInfo.getNumberOfChild() > 0) {
			children.setNo(roomSearchInfo.getNumberOfChild());
			List<DotwChild> dotwChild = new ArrayList<>();
			int index = 0;
			for (int childAge : roomSearchInfo.getChildAge()) {
				DotwChild child = new DotwChild();
				child.setRunno(index++);
				child.setChild(childAge);
				dotwChild.add(child);
			}
			children.setChild(dotwChild);
		}
		return children;
	}

	public List<PriceInfo> getDailyPriceInfo(List<DotwPerDayRoomPrice> dates) {

		List<PriceInfo> priceInfoList = new ArrayList<>();
		int day = 1;
		for (DotwPerDayRoomPrice perDayPriceInfo : dates) {

			PriceInfo priceInfo = new PriceInfo();
			priceInfo.setDay(day);
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			Double perDayPrice = perDayPriceInfo.getPriceInRequestedCurrency() != null
					? perDayPriceInfo.getPriceInRequestedCurrency().getPriceInRequestedCurrency()
					: perDayPriceInfo.getPrice().getPrice();
			perDayPrice = getAmountBasedOnCurrency(perDayPrice, "USD");
			fareComponents.put(HotelFareComponent.BF, perDayPrice);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
			day++;

		}
		return priceInfoList;

	}

	public void storeLogs(String identifier, String key) {

		Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
				.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getPostData())
				.type(identifier + "-Req" + supplierConf.getHotelSupplierCredentials().getUserName())
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());

		listener.addLog(LogData.builder().key(key).logData(httpUtils.getResponseString())
				.type(identifier + "-Res" + supplierConf.getHotelSupplierCredentials().getUserName())
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());

	}

	public double getAmountBasedOnCurrency(double amount, String fromCurrency) {

		String toCurrency = "INR";
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(HotelSourceType.DOTW.name()).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);

	}

}
