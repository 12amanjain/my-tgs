package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import org.springframework.util.ObjectUtils;

import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwActualChild;
import com.tgs.services.hms.datamodel.dotw.DotwActualChildren;
import com.tgs.services.hms.datamodel.dotw.DotwBooking;
import com.tgs.services.hms.datamodel.dotw.DotwBookingCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwChildren;
import com.tgs.services.hms.datamodel.dotw.DotwPassenger;
import com.tgs.services.hms.datamodel.dotw.DotwRoomList;
import com.tgs.services.hms.datamodel.dotw.DotwRoomRequest;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.oms.datamodel.Order;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwBookingService extends DotwBaseService{

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	
	
	public boolean book() throws IOException, JAXBException {
		
		try {
			listener = new RestAPIListener("");
			Customer customer = getRequestForHotelBooking();
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			boolean bookingStatus = updateBookingStatus();
			return bookingStatus;
		}finally {
			
			storeLogs("Dotw-Booking" , order.getBookingId());
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(result)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			
			}
			
		}
	}
	
	private boolean updateBookingStatus() {
		
		boolean isBooked = false;
		if(result.getSuccessful().equalsIgnoreCase("TRUE")) {
			isBooked = true;
			updateSupplierBookingReferenceId();
		}
		return isBooked;
	}
	
	
	public void updateSupplierBookingReferenceId() {

		hInfo.getMiscInfo().setSupplierBookingId(result.getReturnedCode());
		hInfo.getMiscInfo().setSupplierBookingReference(result.getReturnedCode());
		int index = 0;
		Option option = hInfo.getOptions().get(0);
		for(DotwBooking bookingDetails : result.getBooking()) {
			RoomInfo roomInfo = option.getRoomInfos().get(index);
			roomInfo.getMiscInfo().setRoomBookingId(bookingDetails.getBookingCode());
			index++;
		}

	}
	
	private Customer getRequestForHotelBooking() {
		
		Customer customer = getCustomer(supplierConf);
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("confirmbooking");
		DotwBookingCriteria bookingCriteria = getBookingCriteria();
		request.setBookingRequest(bookingCriteria);
		customer.setRequest(request);
		return customer;
		
	}
	
	private DotwBookingCriteria getBookingCriteria() {
		
		DotwBookingCriteria bookingCriteria = new DotwBookingCriteria();
		String formattedFromDate = searchQuery.getCheckinDate()
				.format(DateTimeFormatter.ISO_DATE);
		String formattedToDate = searchQuery.getCheckoutDate()
				.format(DateTimeFormatter.ISO_DATE);
		bookingCriteria.setFromDate(String.valueOf(formattedFromDate));
		bookingCriteria.setToDate(String.valueOf(formattedToDate));
		bookingCriteria.setCurrency(DotwConstants.CURRENCY.getValue());
		bookingCriteria.setProductId(hInfo.getMiscInfo().getSupplierStaticHotelId());
		bookingCriteria.setCustomerReference(order.getBookingId());
		DotwRoomList roomList = getRoomList();
		bookingCriteria.setRooms(roomList);
		return bookingCriteria;
		
	}
	
	public DotwRoomList getRoomList() {
		
		List<DotwRoomRequest> rooms = new ArrayList<>();
		DotwRoomList roomList = new DotwRoomList();
		roomList.setNo(searchQuery.getRoomInfo().size());
		List<RoomInfo> roomInfos = hInfo.getOptions().get(0).getRoomInfos();
		int i = 0;
		for(RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			RoomInfo roomInfo = roomInfos.get(i);
			DotwRoomRequest room = new DotwRoomRequest();
			room.setRoomTypeCode(roomInfo.getMiscInfo().getRoomTypeCode());
			room.setSelectedRateBasis(roomInfo.getMiscInfo().getRatePlanCode());
			room.setAllocationDetails(roomInfo.getMiscInfo().getAllocationDetails());
			room.setAdultsCode(roomSearchInfo.getNumberOfAdults());
			room.setActualAdults(roomSearchInfo.getNumberOfAdults());
			room.setPassengerCountryOfResidence("20");
			room.setPassengerNationality("20");
			DotwChildren children = getRoomChildrenRequest(roomSearchInfo);
			DotwActualChildren actualChildren = getActualRoomChildrenRequest(roomSearchInfo);
			
			room.setChildren(children);
			room.setActualChildren(actualChildren);
			List<DotwPassenger> passengers = getRoomPassengers(roomInfo);
			room.setPassenger(passengers);
			List<String> specialRequests = new ArrayList<>();
			room.setReq(specialRequests);
			room.setBeddingPreference(0);
			rooms.add(room);
			i++;
		}
		roomList.setRoom(rooms);
		return roomList;
		
	}
	
	
	private DotwActualChildren getActualRoomChildrenRequest(RoomSearchInfo roomSearchInfo) {
		
		DotwActualChildren children = new DotwActualChildren();
		children.setNo(0);
		if(roomSearchInfo.getNumberOfChild() != null && roomSearchInfo.getNumberOfChild() > 0) {
			children.setNo(roomSearchInfo.getNumberOfChild());
			List<DotwActualChild> dotwChild = new ArrayList<>();
			int index = 0;
			for(int childAge : roomSearchInfo.getChildAge()) {
				DotwActualChild actualChild = new DotwActualChild();
				actualChild.setRunno(index++);
				actualChild.setActualChild(childAge);
				dotwChild.add(actualChild);
			}
			children.setActualChild(dotwChild);
		}
		return children;
		
	}
	
	public List<DotwPassenger> getRoomPassengers(RoomInfo roomInfo){
		
		boolean isLeading = true;
		List<DotwPassenger> passengerList = new ArrayList<>();
		for(TravellerInfo traveller : roomInfo.getTravellerInfo()) {
			
			DotwPassenger passenger = new DotwPassenger();
			passenger.setFirstName(traveller.getFirstName().replaceAll("\\s", ""));
			passenger.setLastName(traveller.getLastName().replaceAll("\\s", ""));
			passenger.setLeading("no");
			if(isLeading) {
				passenger.setLeading("yes");
				isLeading = false;
			}
			passenger.setSalutation(getSalutationCode(traveller.getTitle(), traveller.getPaxType()));
			passengerList.add(passenger);
		}
		return passengerList;
	}
	
	private String getSalutationCode(String tgsSalutation, PaxType paxType) {
		
		if(paxType != null && paxType.equals(PaxType.CHILD)) return "14632";
		else if(tgsSalutation.equalsIgnoreCase("Mr")) return "147";
		else if(tgsSalutation.equalsIgnoreCase("Mrs")) return "149";
		else if(tgsSalutation.equalsIgnoreCase("Ms")) return "148";
		return "3801";
 		
	}
	
}
