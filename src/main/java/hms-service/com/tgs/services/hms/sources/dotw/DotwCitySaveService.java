package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import com.opencsv.CSVWriter;
import com.tgs.services.hms.datamodel.agoda.statc.AgodaCity;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwCity;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.tbo.mapping.CityCountryMapping;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.sources.agoda.AgodaConstants;
import com.tgs.utils.common.HttpUtils;

import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class DotwCitySaveService extends DotwBaseService {

	public List<HotelCityInfoQuery> getCityMapppings() throws IOException {

		List<HotelCityInfoQuery> dotwCityMappingList = new ArrayList<>();
		try {
			List<DotwCity> cityList = getCityList();
			Map<String, List<DotwCity>> countryCityListMap =
					cityList.stream().collect(Collectors.groupingBy(DotwCity::getCountryName));
			for (String country : countryCityListMap.keySet()) {
				List<DotwCity> cities = countryCityListMap.get(country);
				if (CollectionUtils.isNotEmpty(cities)) {
					List<HotelCityInfoQuery> dotwCityMapping = getCitiesFromDotwCities(cities, country);
					dotwCityMappingList.addAll(dotwCityMapping);
				}
			}
		} catch (Exception e) {
			log.error("Error while mapping cities for DOTW", e);
		}
		return dotwCityMappingList;
	}

	private List<DotwCity> getCityList() throws JAXBException, IOException {

		Customer customer = getRequestForStaticDataCity();
		String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
		HttpUtils httpUtils = getHttpRequest(xmlRequest, supplierConf);
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
		return result.getCities();
	}

	private List<HotelCityInfoQuery> getCitiesFromDotwCities(List<DotwCity> cityResult, String countryName) {

		List<HotelCityInfoQuery> cityList = new ArrayList<>();
		cityResult.forEach((dotwCity) -> {
			HotelCityInfoQuery cityInfo = new HotelCityInfoQuery();
			cityInfo.setCityname(dotwCity.getName());
			cityInfo.setCountryname(dotwCity.getCountryName());
			cityInfo.setSuppliercityid(String.valueOf(dotwCity.getCode()));
			cityInfo.setSuppliercountryid(String.valueOf(dotwCity.getCountryCode()));
			cityInfo.setSuppliername("DOTW");
			cityList.add(cityInfo);
		});
		return cityList;
	}


	private Customer getRequestForStaticDataCity() {

		Customer customer = getCustomer(supplierConf);
		Request request = new Request();
		request.setCommand("getallcities");
		request.getRet().getFields().setFields(new ArrayList<>(Arrays.asList("countryName", "countryCode")));
		customer.setRequest(request);
		return customer;

	}

}
