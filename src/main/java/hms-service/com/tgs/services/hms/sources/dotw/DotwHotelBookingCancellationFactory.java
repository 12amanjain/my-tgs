package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.LocalDateTime;

import javax.xml.bind.JAXBException;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class DotwHotelBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public DotwHotelBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException, JAXBException {

		checkIfSupplierCancellationAllowed();
		HotelImportBookingParams importBookingParams =
				HotelImportBookingParams.builder().bookingId(order.getBookingId()).supplierId("DOTW")
						.supplierBookingId(hInfo.getMiscInfo().getSupplierBookingId()).build();
		DotwRetrieveBookingService bookingService = DotwRetrieveBookingService.builder().supplierConf(supplierConf)
				.importBookingParams(importBookingParams).moneyExchnageComm(moneyExchnageComm).build();
		DotwHotelBookingCancellationService cancellationService = DotwHotelBookingCancellationService.builder()
				.supplierConf(supplierConf).retrieveBookingService(bookingService).hInfo(hInfo).order(order)
				.moneyExchnageComm(moneyExchnageComm).build();
		return cancellationService.cancelBooking();
	}

	private void checkIfSupplierCancellationAllowed() {
		
		HotelCancellationPolicy cp = hInfo.getOptions().get(0).getCancellationPolicy();
		if(cp == null) return;
		LocalDateTime currentTime = LocalDateTime.now();
		for(PenaltyDetails pd : cp.getPenalyDetails()) {
			if(pd.getFromDate().isBefore(currentTime) && pd.getToDate() != null 
					&& pd.getToDate().isAfter(currentTime)) {
				if(pd.getIsCancellationRestricted() != null && pd.getIsCancellationRestricted()) {
					throw new CustomGeneralException("Cancellation Restricted So Can't cancel");
				}
			}
		}
		
		
	}

	@Override
	public boolean getCancelHotelStatus() throws IOException {
		// TODO Auto-generated method stub
		return false;
	}

}
