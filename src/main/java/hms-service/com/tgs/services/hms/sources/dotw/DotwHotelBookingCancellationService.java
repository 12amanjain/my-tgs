package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBException;

import com.ibm.icu.math.BigDecimal;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwCancellationCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwRoomBookingDetailResponse;
import com.tgs.services.hms.datamodel.dotw.DotwServiceRequest;
import com.tgs.services.hms.datamodel.dotw.RateBasis;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;

import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
public class DotwHotelBookingCancellationService extends DotwBaseService {

	private DotwRetrieveBookingService retrieveBookingService;
	private Order order;

	public boolean cancelBooking() throws IOException, JAXBException {

		Customer customer = getCustomer(supplierConf);
		Request request = getBaseRequest();
		customer.setRequest(request);
		return cancelBookingItineary(customer);
	}

	private List<DotwRoomBookingDetailResponse> getRetrievedBookingDetails() throws IOException, JAXBException {

		retrieveBookingService.retrieveBooking();
		Results results = retrieveBookingService.getResult();
		if (results == null || results.getBookingDetailResponse() == null)
			throw new CustomGeneralException("Error While Fetching Cancellation Policy");
		return results.getBookingDetailResponse().getRoomDetails();

	}

	private Request getBaseRequest() {

		Request request = new Request();
		request.setCommand("deleteitinerary");
		DotwCancellationCriteria cancellationCriteria = new DotwCancellationCriteria();
		cancellationCriteria.setBookingType("1");
		request.setBookingCancellationRequest(cancellationCriteria);
		return request;

	}

	private boolean cancelBookingItineary(Customer customer) throws JAXBException, IOException {

		updateRequest(customer);
		String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
		listener = new RestAPIListener("");
		httpUtils = getHttpRequest(xmlRequest, supplierConf);
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
		storeLogs("Dotw-BookingCancelCheck" ,order.getBookingId());
		if (result.getSuccessful().equalsIgnoreCase("TRUE")) {
			updateRequestForCancel(customer);
			String xmlRequest1 = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest1, supplierConf);
			String xmlResponse1 = (String) httpUtils.getResponse(null).orElse(null);
			Results result1 = DotwMarshallerWrapper.unmarshallXML(xmlResponse1);
			storeLogs("Dotw-BookingCancelConfirm" ,order.getBookingId());
			if (result1.getSuccessful().equalsIgnoreCase("TRUE")) {
				log.info("Booking {} Supplier {} cancelled Successfully", 
						order.getBookingId(), "DOTW");
				return true;
			}
			
			log.error("Booking Cancellation Failed For BookingId {}, Supplier {}, NumberOfProductsLeft {}"
					,order.getBookingId(), "DOTW", result1.getProductsLeftOnItinerary());
		}
		return false;
	}

	private void updateRequestForCancel(Customer customer) throws IOException, JAXBException {
		List<RoomInfo> roomInfoList = hInfo.getOptions().get(0).getRoomInfos();
		List<DotwRoomBookingDetailResponse> roomResponseList = getRetrievedBookingDetails();
		List<DotwServiceRequest> services = new ArrayList<>();
		int index = 0;
		for (RoomInfo roomInfo : roomInfoList) {
			DotwRoomBookingDetailResponse dotwRoom = roomResponseList.get(index);
			RateBasis rateBasis = new RateBasis();
			rateBasis.setCancellationRules(dotwRoom.getCancellationRules());
			rateBasis.setTotalInRequestedCurrency(getAmountBasedOnCurrency(dotwRoom.getServicePrice().getServicePrice(),"USD"));
			HotelSearchQuery searchQuery = HotelSearchQuery.builder().checkinDate(roomInfo.getCheckInDate())
					.checkoutDate(roomInfo.getCheckOutDate()).build();
			HotelCancellationPolicy roomCancellationPolicy = getRoomCancellationPolicy(rateBasis, searchQuery, roomInfo);
			customer.getRequest().getBookingCancellationRequest().setConfirm("yes");
			DotwServiceRequest serviceRequest = new DotwServiceRequest();

			serviceRequest.setPenaltyApplied(getPenaltyApplied(roomCancellationPolicy));
			serviceRequest.setReferencenumber(roomInfo.getMiscInfo().getRoomBookingId());
			services.add(serviceRequest);
			index++;
		}
		customer.getRequest().getBookingCancellationRequest().setService(services);
	}

	private String getPenaltyApplied(HotelCancellationPolicy dotwCancellationRules) {

		for (PenaltyDetails pd : dotwCancellationRules.getPenalyDetails()) {
			LocalDateTime currentTime = LocalDateTime.now();
			if (pd.getFromDate().isBefore(currentTime) && pd.getToDate().isAfter(currentTime)) {
				BigDecimal value = BigDecimal.valueOf(pd.getPenaltyAmount());
				return value.toString();
			}
		}
		return "";
	}

	private void updateRequest(Customer customer) {

		customer.getRequest().getBookingCancellationRequest()
				.setBookingCode(hInfo.getMiscInfo().getSupplierBookingId());
		customer.getRequest().getBookingCancellationRequest().setConfirm("no");

	}
}
