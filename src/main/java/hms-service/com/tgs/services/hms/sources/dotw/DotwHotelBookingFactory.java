package com.tgs.services.hms.sources.dotw;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class DotwHotelBookingFactory extends AbstractHotelBookingFactory{

	@Autowired
	HotelCacheHandler cacheHandler;
	
	public DotwHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException, InterruptedException, JAXBException {
		
		HotelInfo hInfo = this.getHotel();
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(hInfo.getMiscInfo().getSearchId());
		DotwBookingService bookingService = DotwBookingService.builder().hInfo(this.getHotel())
				.order(order).supplierConf(supplierConf)
				.sourceConfigOutput(sourceConfigOutput).searchQuery(searchQuery)
				.moneyExchnageComm(moneyExchnageComm).build();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
	}
	
	

}
