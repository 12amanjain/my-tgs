package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DotwHotelInfoFactory extends AbstractHotelInfoFactory {


	public DotwHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException, InterruptedException {
		Set<String> propertyIds = getPropertyIdsOfCity(HotelSourceType.DOTW.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);

	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		DotwHotelSearchService searchService = DotwHotelSearchService.builder().sourceConfigOutput(sourceConfigOutput)
				.searchQuery(searchQuery).supplierConf(this.getSupplierConf()).cacheHandler(cacheHandler)
				.moneyExchnageComm(moneyExchnageComm).hInfo(hInfo).build();
		searchService.doDetailSearch();

	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {

		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});

	}

	@Override
	protected HotelSearchResult doSearchForSupplier(Set<String> set) throws IOException, JAXBException {
		
		DotwHotelSearchService searchService = DotwHotelSearchService.builder().supplierConf(this.getSupplierConf())
				.searchQuery(this.getSearchQuery()).cacheHandler(cacheHandler)
				.sourceConfigOutput(sourceConfigOutput).propertyIds(set)
				.moneyExchnageComm(moneyExchnageComm).build();
		searchService.doSearch();
		HotelSearchResult searchResult = (HotelSearchResult)searchService.getSearchResult();
		return searchResult;
	}
}
