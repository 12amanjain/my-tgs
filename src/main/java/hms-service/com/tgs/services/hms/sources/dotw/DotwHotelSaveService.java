package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwAddress;
import com.tgs.services.hms.datamodel.dotw.DotwChildren;
import com.tgs.services.hms.datamodel.dotw.DotwCity;
import com.tgs.services.hms.datamodel.dotw.DotwHotel;
import com.tgs.services.hms.datamodel.dotw.DotwHotelImage;
import com.tgs.services.hms.datamodel.dotw.DotwHotelLocation;
import com.tgs.services.hms.datamodel.dotw.DotwRoomList;
import com.tgs.services.hms.datamodel.dotw.DotwRoomRequest;
import com.tgs.services.hms.datamodel.dotw.DotwRoomType;
import com.tgs.services.hms.datamodel.dotw.DotwSearchCriteria;
import com.tgs.services.hms.datamodel.dotw.Filter;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.utils.common.HttpUtils;

import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@SuperBuilder
public class DotwHotelSaveService extends DotwBaseService {

	protected HotelInfoSaveManager hotelInfoSaveManager;
	private HotelCityInfoMappingManager cityInfoMappingManager;

	private static final ExecutorService executor = Executors.newFixedThreadPool(30);
	private int totalHotelCount;
	private int cityCount;

	public void process() throws IOException {

		try {
			cityCount = 0;
			totalHotelCount = 0;
			List<String> cityIds = getDotwCityIdList();
			log.info("Total number of DOTW cities fetched are " + cityIds.size());
			for (String city : cityIds) {

				executor.submit(() -> {
					List<HotelInfo> hotels = new ArrayList<>();
					try {
						hotels = getHotelList(city, supplierConf);

						saveHotels(hotels, city);
					} catch (Exception e1) {
						log.error("Error while fetching hotels data {} ", e1);
					}
				});
			}


		} catch (Exception e) {
			log.error("Error while mapping static data ", e);
		}
	}

	private void saveHotels(List<HotelInfo> hotels, String city) {

		for (HotelInfo hInfo : hotels) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				String supplierName = HotelSourceType.DOTW.name();
				dbHotelInfo.setId(null);
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hInfo.getId(), supplierName, HotelSourceType.DOTW);
			} catch (Exception e) {
				log.error("Error while processing hotel {} ", hInfo, e);
			}
		}
		totalHotelCount += hotels.size();
		cityCount++;
		log.info("Total Hotel Count {} for {} Dotw cities are fetched ", totalHotelCount, cityCount);
		log.info("Total Hotels of dotw city {} are {}", city, hotels.size());

	}


	public String getRoomInfos(HotelInfo hInfo) {

		if (hInfo != null && CollectionUtils.isNotEmpty(hInfo.getOptions()) && hInfo.getOptions().get(0) != null) {
			return GsonUtils.getGson().toJson(hInfo.getOptions().get(0).getRoomInfos());
		}
		return null;
	}


	public String getAmenities(List<String> amenities) {

		String amenitie = "";
		if (CollectionUtils.isNotEmpty(amenities)) {
			for (String amenity : amenities) {
				amenitie += amenity + ",";
			}
		}
		return amenitie;
	}

	public String getImages(List<Image> images) {

		String image = "";
		if (CollectionUtils.isNotEmpty(images)) {
			for (Image img : images) {
				image += img.getBigURL() + ",";
			}
		}
		return image;
	}

	public List<HotelInfo> getHotelList(String city, HotelSupplierConfiguration supplierConf) throws Exception {

		Customer customer = getRequestForStaticDataHotel(supplierConf, city);
		String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
		HttpUtils httpUtils = getHttpRequest(xmlRequest, supplierConf);
		String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
		Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
		List<HotelInfo> hotelList = getHotelListFromDotwResult(result);

		return hotelList;

	}

	public Customer getRequestForStaticDataHotel(HotelSupplierConfiguration conf, String city) {

		Customer customer = getCustomer(conf);
		customer.setProduct("hotel");
		customer.setLanguage("en");
		Request request = new Request();
		List<String> fields = new ArrayList<>(Arrays.asList("hotelName", "fullAddress", "description1", "description2",
				"amenitie", "hotelPhone", "rating", "images", "geoPoint"));
		request.getRet().getFields().setFields(fields);
		List<String> roomFields = new ArrayList<>(Arrays.asList("name", "roomInfo", "roomAmenities"));
		request.getRet().getFields().setRoomFields(roomFields);
		request.setCommand("searchhotels");
		request.setSearchRequest(getSearchCriteria());
		Filter filter = new Filter();
		filter.setCity(city);
		filter.setNoPrice(true);
		request.getRet().setFilters(filter);
		request.getRet().setGetRooms(true);
		customer.setRequest(request);
		return customer;

	}

	public DotwSearchCriteria getSearchCriteria() {

		List<DotwRoomRequest> rooms = new ArrayList<>();
		DotwRoomList roomList = new DotwRoomList();
		roomList.setNo(1);
		DotwSearchCriteria searchCriteria = new DotwSearchCriteria();
		LocalDate from = LocalDate.now().plusDays(30);
		LocalDate to = LocalDate.now().plusDays(30);
		String formattedFromDate = from.format(DateTimeFormatter.ISO_DATE);
		String formattedToDate = to.format(DateTimeFormatter.ISO_DATE);
		searchCriteria.setFromDate(String.valueOf(formattedFromDate));
		searchCriteria.setToDate(String.valueOf(formattedToDate));
		searchCriteria.setCurrency("373");
		DotwRoomRequest room = new DotwRoomRequest();
		room.setAdultsCode(1);
		DotwChildren children = new DotwChildren();
		children.setNo(0);
		room.setChildren(children);
		rooms.add(room);
		room.setRateBasis(-1);
		roomList.setRoom(rooms);
		searchCriteria.setRooms(roomList);
		return searchCriteria;
	}


	public List<HotelInfo> getHotelListFromDotwResult(Results result) {

		List<HotelInfo> hotelInfos = new ArrayList<>();
		List<DotwHotel> supplierHotels = result.getHotels();
		for (DotwHotel hotel : supplierHotels) {

			Address hotelAddress = getHotelAddress(hotel.getAddress());
			Contact contact = new Contact();
			String shortDescription = getShortDescription(hotel);
			String longDescription = getLongDescription(hotel);
			contact.setPhone(hotel.getHotelPhone());
			List<Image> images = getImages(hotel);
			List<String> amenities = getAmenities(hotel);
			GeoLocation location = getGeoLocation(hotel);
			// Option option = getOptionWithRoomStaticData(hotel);
			// List<Option> optionList = new ArrayList<>();
			// optionList.add(option);
			HotelInfo hInfo = HotelInfo.builder().name(hotel.getHotelName()).id(hotel.getHotelid())
					.address(hotelAddress).contact(contact).longDescription(longDescription)
					.description(shortDescription).images(images).geolocation(location).facilities(amenities)
					.rating(getRating(hotel.getRating())).build();
			hotelInfos.add(hInfo);
		}
		return hotelInfos;

	}


	private Integer getRating(String dotwRatingCode) {

		if (dotwRatingCode.equals("559"))
			return 1;
		if (dotwRatingCode.equals("560"))
			return 2;
		if (dotwRatingCode.equals("561"))
			return 3;
		if (dotwRatingCode.equals("562"))
			return 4;
		if (dotwRatingCode.equals("563"))
			return 5;
		if (dotwRatingCode.equals("55835"))
			return null;
		return null;

	}

	public Address getHotelAddress(DotwAddress adr) {

		if (adr != null) {
			City city = City.builder().name(adr.getHotelCity()).build();
			Country country = Country.builder().name(adr.getHotelCountry()).build();
			Address adrress = Address.builder().addressLine1(adr.getAddressLine1()).city(city).country(country)
					.postalCode(adr.getHotelZipCode()).build();
			return adrress;
		}
		return null;
	}

	public String getShortDescription(DotwHotel hotel) {

		if (hotel != null && hotel.getDescription2() != null)
			return hotel.getDescription2().getLanguage();
		return null;

	}

	public String getLongDescription(DotwHotel hotel) {

		if (hotel != null && hotel.getDescription1() != null)
			return hotel.getDescription1().getLanguage();
		return null;

	}

	public List<String> getAmenities(DotwHotel hotel) {

		if (hotel != null && hotel.getAmenitie() != null && hotel.getAmenitie().getLanguage() != null) {
			List<String> amenities = hotel.getAmenitie().getLanguage().getAmenitieItem();
			if (CollectionUtils.isNotEmpty(hotel.getAmenitie().getLanguage().getLeisureItem())) {
				amenities.addAll(hotel.getAmenitie().getLanguage().getLeisureItem());
			}
			return amenities;
		}
		return null;
	}

	public List<Image> getImages(DotwHotel hotel) {

		List<Image> imageList = new ArrayList<>();
		if (hotel != null && hotel.getImages() != null && hotel.getImages().getHotelImages() != null) {
			for (DotwHotelImage dotwImage : hotel.getImages().getHotelImages().getImage()) {
				Image image = Image.builder().bigURL(dotwImage.getUrl()).thumbnail(dotwImage.getUrl()).build();
				imageList.add(image);
			}
		}
		return imageList;
	}

	public GeoLocation getGeoLocation(DotwHotel hotel) {

		if (hotel != null && hotel.getGeoPoint() != null) {
			DotwHotelLocation geo = hotel.getGeoPoint();
			GeoLocation geoLocation = GeoLocation.builder().latitude(geo.getLat()).longitude(geo.getLng()).build();
			return geoLocation;
		}
		return null;
	}

	public Option getOptionWithRoomStaticData(DotwHotel hotel) {

		if (hotel != null && CollectionUtils.isNotEmpty(hotel.getRooms())
				&& CollectionUtils.isNotEmpty(hotel.getRooms().get(0).getRoomType())) {
			List<RoomInfo> roomInfos = new ArrayList<>();
			List<DotwRoomType> roomTypes = hotel.getRooms().get(0).getRoomType();
			for (DotwRoomType roomType : roomTypes) {

				RoomInfo roomInfo = new RoomInfo();
				if (roomType.getRoomAmenities() != null
						&& CollectionUtils.isNotEmpty(roomType.getRoomAmenities().getAmenity())) {
					roomInfo.setRoomAmenities(roomType.getRoomAmenities().getAmenity());
				}
				roomInfo.setId(roomType.getRoomtypecode());
				roomInfo.setRoomType(roomType.getName());
				roomInfos.add(roomInfo);
			}
			Option option = Option.builder().roomInfos(roomInfos).build();
			return option;
		}
		return null;

	}

	private List<String> getDotwCityIdList() {

		List<CityInfoMapping> cityInfoMappings = cityInfoMappingManager.findBySupplierName(HotelSourceType.DOTW.name());
		return cityInfoMappings.stream().map(CityInfoMapping::getSupplierCity).collect(Collectors.toList());
	}

}
