package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.xml.bind.JAXBException;

import org.apache.cxf.common.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwFilterComplexCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwFilterCriteria;
import com.tgs.services.hms.datamodel.dotw.DotwHotel;
import com.tgs.services.hms.datamodel.dotw.Filter;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwHotelSearchService extends DotwBaseService {
	
	private HotelSearchResult searchResult;
	private HotelSourceConfigOutput sourceConfigOutput;
	private Set<String> propertyIds;
	
	public void doSearch() throws IOException, JAXBException {
		
		try {
			listener = new RestAPIListener("");
			Customer customer = getRequestForHotelSearch();
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);	
			List<HotelInfo> hotelList = getHotelListFromDotwResult(result);
			updateHotelResult(hotelList);
			searchResult = new HotelSearchResult();
			searchResult.setHotelInfos(hotelList);
		}finally {
			
			storeLogs("Dotw-Search" , searchQuery.getSearchId());
			httpUtils.getCheckPoints().stream()
					.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.SEARCH.name()));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(result)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			}
		}
		
	}
	
	private void updateHotelResult(List<HotelInfo> hotelInfoList) {
		
		for(HotelInfo hInfo : hotelInfoList) {
			for(Option option : hInfo.getOptions()) {
				for(RoomInfo roomInfo : option.getRoomInfos()) {
					roomInfo.setRoomCategory("Dummy Room Category");
					roomInfo.setRoomType("Dummy Room Type");
				}
			}
		}
	}
	
	private Customer getRequestForHotelSearch() {
		
		Customer customer = getCustomer(supplierConf);
		customer.setLanguage("en");
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("searchhotels");
		request.setSearchRequest(getSearchCriteriaFromSearchQuery(searchQuery));
		
		Filter filter = new Filter();
		updateRatingFilterInRequest(filter);
		updatePropertyIdFilterInRequest(filter);
	//	filter.setCity(supplierCityInfo.getSupplierCity());
		request.getRet().setFilters(filter);
		customer.setRequest(request);
		return customer;
		
	}

	private void updateRatingFilterInRequest(Filter filter) {

		DotwFilterComplexCriteria complexCondition = new DotwFilterComplexCriteria();
		HotelSearchPreferences hotelSearchPreferences = searchQuery.getSearchPreferences();
		
		if(!CollectionUtils.isEmpty(hotelSearchPreferences.getRatings())) {
			DotwFilterCriteria filterCriteria = new DotwFilterCriteria();
			filterCriteria.setFieldName("rating");
			filterCriteria.setFieldTest("in");
			filterCriteria.setFieldValue(getFieldValues(hotelSearchPreferences.getRatings()));
			if(hotelSearchPreferences.getFetchSpecialCategory() != null 
					&& hotelSearchPreferences.getFetchSpecialCategory()) {
				filterCriteria.getFieldValue().add("55835");
			}
			complexCondition.getFilterCriteria().add(filterCriteria);
			filter.setCondition(complexCondition);
		}
	}
	
	private void updatePropertyIdFilterInRequest(Filter filter) {

		DotwFilterComplexCriteria complexCondition = new DotwFilterComplexCriteria();
		
		if(!CollectionUtils.isEmpty(propertyIds)) {
			DotwFilterCriteria filterCriteria=new DotwFilterCriteria();
			filterCriteria.setFieldName("hotelId");
			filterCriteria.setFieldTest("in");
			filterCriteria.setFieldValue(propertyIds);
			complexCondition.getFilterCriteria().add(filterCriteria);
			filter.setCondition(complexCondition);
		}
	}
	
	private Set<String> getFieldValues(List<Integer> input){
		
		Set<String> list = new HashSet<>();
		for(Integer rating : input) {
			String dotwCodeForRating = getDotwCodeForRating(String.valueOf(rating));
			if(dotwCodeForRating != null) list.add(dotwCodeForRating);	
		}
		return list;	
	}
	
	private String getDotwCodeForRating(String rating) {
		
		if(rating.equals("1")) return "559";
		else if(rating.equals("2")) return "560";
		else if(rating.equals("3")) return "561";
		else if(rating.equals("4")) return "562";
		else if(rating.equals("5")) return "563";
		return null;
		
	}
	
	private List<HotelInfo> getHotelListFromDotwResult(Results result){
		List<HotelInfo> hInfos = new ArrayList<>();
		for(DotwHotel hotel : result.getHotels()) {
			HotelInfo hInfo = HotelInfo.builder().name(hotel.getHotelName()).miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
					.supplierStaticHotelId(hotel.getHotelid()).build()).build();
			List<Option> optionList = getOptionList(hotel);
			if (!CollectionUtils.isEmpty(optionList)) {
				hInfo.setOptions(optionList);
				hInfos.add(hInfo);
			}
		}
		return hInfos;
	}
	
	public void doDetailSearch() throws IOException, JAXBException{
		
		String searchId = HotelUtils.getSearchId(hInfo.getId());
		try {
			listener = new RestAPIListener("");
			Customer customer = createDetailRequest(hInfo);
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			createDetailResponse(result , hInfo);
		}finally {
			try {
				
				storeLogs("Dotw-DetailSearch" , searchId);
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (ObjectUtils.isEmpty(result)) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
			} catch (Exception e) {
				log.error("Unable to set rest api logs for hotel detail search {}", searchQuery, e);
			}
		}
	}
	
	private void createDetailResponse(Results result , HotelInfo hInfo) {
		
		DotwHotel hotel = result.getHotel();
		List<Option> optionList = getOptionList(hotel);
		if (!CollectionUtils.isEmpty(optionList)) {
			for(Option option : optionList) option.getMiscInfo().setIsDetailHit(true);
			hInfo.setOptions(optionList);
		}
	}
	
	private Customer createDetailRequest(HotelInfo hInfo) {
		
		Customer customer = getCustomer(supplierConf);
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("getrooms");
		request.setSearchRequest(getSearchCriteriaFromSearchQuery(searchQuery));
		request.getSearchRequest().setProductId(hInfo.getMiscInfo().getSupplierStaticHotelId());
		customer.setRequest(request);
		return customer;
	}
	
}
