package com.tgs.services.hms.sources.dotw;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import com.sun.xml.bind.marshaller.NamespacePrefixMapper;
import com.tgs.services.hms.datamodel.dotw.Results;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DotwMarshallerWrapper {

	static Jaxb2Marshaller marshaller;

	public static void init(Jaxb2Marshaller jaxb2) {
		marshaller = jaxb2;
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("com.sun.xml.bind.namespacePrefixMapper",
				new DefaultNamespacePrefixMapper());
		marshaller.setMarshallerProperties(map);
	}
	
	public static <T> String marshallXml(final T obj) throws JAXBException {
	      StringWriter sw = new StringWriter();
	      StreamResult result = new StreamResult(sw);
	      marshaller.marshal(obj, result);
	      return sw.toString();
	}
	
	
	@SuppressWarnings("unchecked")
	public static Results unmarshallXML(String data) {
		
		InputStream targetStream = new ByteArrayInputStream(data.getBytes());
		return (Results)marshaller.unmarshal(new StreamSource(targetStream));
		
	}
}
