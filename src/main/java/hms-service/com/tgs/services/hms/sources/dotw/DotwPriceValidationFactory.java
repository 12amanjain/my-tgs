package com.tgs.services.hms.sources.dotw;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class DotwPriceValidationFactory extends AbstractHotelPriceValidationFactory {
	
	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public DotwPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		
		DotwPriceValidationService priceValidationService = DotwPriceValidationService.builder()
				.searchQuery(this.searchQuery).bookingId(bookingId)
				.supplierConf(this.getSupplierConf()).cacheHandler(cacheHandler)
				.moneyExchnageComm(moneyExchnageComm).build();
		priceValidationService.validate(this.getHInfo());
		return this.getHInfo().getOptions().get(0);
		
	}
	
	

}
