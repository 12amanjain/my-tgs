package com.tgs.services.hms.sources.dotw;

import java.time.LocalDateTime;
import java.util.List;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwRoomRequest;
import com.tgs.services.hms.datamodel.dotw.DotwSelectedRoom;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwPriceValidationService extends DotwBaseService {

	private String bookingId;
	private Option updatedOption;
	
	public void validate(HotelInfo hInfo) throws Exception {
		
		try {
			listener = new RestAPIListener("");
			Customer customer = createPriceValidationRequest(hInfo, supplierConf);
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			log.info("Price Validation Request is {}" , httpUtils.getPostData());
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			Results result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			validateResponse(result , hInfo , supplierConf);
			hInfo.getMiscInfo().setSearchKeyExpiryTime(LocalDateTime.now().plusMinutes(15));
		}finally {
			storeLogs("Dotw-PriceCheck" ,bookingId);
		}
		
	}

	private Customer createPriceValidationRequest(HotelInfo hInfo , HotelSupplierConfiguration supplierConf) {
		
		Customer customer = getCustomer(supplierConf);
		customer.setProduct("hotel");
		Request request = new Request();
		request.setCommand("getrooms");
		request.setSearchRequest(getSearchCriteriaFromSearchQuery(searchQuery));
		updateRequestParametersForPriceValidationRequest(request , hInfo);
		request.getSearchRequest().setProductId(hInfo.getMiscInfo().getSupplierStaticHotelId());
		customer.setRequest(request);
		return customer;
	}
	
	
	private void updateRequestParametersForPriceValidationRequest(Request request , HotelInfo hInfo) {
		
		List<DotwRoomRequest> roomRequestList = request.getSearchRequest().getRooms().getRoom();
		int index = 0;
		Option option = hInfo.getOptions().get(0);
		for(DotwRoomRequest roomRequest : roomRequestList) {
			RoomInfo roomInfo = option.getRoomInfos().get(index);
			DotwSelectedRoom selectedRoom = new DotwSelectedRoom();
			selectedRoom.setAllocationDetails(roomInfo.getMiscInfo().getAllocationDetails());
			selectedRoom.setCode(roomInfo.getMiscInfo().getRoomTypeCode());
			selectedRoom.setSelectedRateBasis(roomInfo.getMiscInfo().getRatePlanCode());
			roomRequest.setSelectedRoom(selectedRoom);
			index++;
		}
		
	}
	
	public void validateResponse(Results result , HotelInfo hInfo , HotelSupplierConfiguration supplierConf) {
		
		String selectedRoomId = hInfo.getOptions().get(0).getRoomInfos().get(0).getId();	
		List<Option> optionList = getOptionList(result.getHotel());
		Option selectedOption = null;
		for(Option option : optionList) {
			for(RoomInfo roomInfo : option.getRoomInfos()) {
				if(roomInfo.getId().equalsIgnoreCase(selectedRoomId)) {
					RoomMiscInfo roomMiscInfo = roomInfo.getMiscInfo();
					if(roomMiscInfo.getIsRoomBlocked() != null 
							&& !roomMiscInfo.getIsRoomBlocked()) {
						throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
					}
					selectedOption = option;
					break;
				}
			}
			if(selectedOption != null) break;
		}
		
		if(selectedOption == null) throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		checkIfAllRoomsBlocked(selectedOption);
		updatedOption = selectedOption;
	}
	
	
	private void checkIfAllRoomsBlocked(Option selectedOption) {
		
		for(RoomInfo roomInfo : selectedOption.getRoomInfos()) {
			RoomMiscInfo roomMiscInfo = roomInfo.getMiscInfo();
			if(!(roomMiscInfo.getIsRoomBlocked() != null && roomMiscInfo.getIsRoomBlocked()))
				throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
		}
		
	}
}
