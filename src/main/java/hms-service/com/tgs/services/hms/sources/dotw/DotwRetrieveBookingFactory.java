package com.tgs.services.hms.sources.dotw;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class DotwRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	public DotwRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException, JAXBException {
		DotwRetrieveBookingService bookingService = DotwRetrieveBookingService.builder().supplierConf(supplierConf)
				.importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();
		
	}

}
