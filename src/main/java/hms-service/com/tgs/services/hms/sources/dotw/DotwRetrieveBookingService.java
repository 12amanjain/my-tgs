package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.dotw.Customer;
import com.tgs.services.hms.datamodel.dotw.DotwBookingDetailRequest;
import com.tgs.services.hms.datamodel.dotw.DotwBookingDetailResponse;
import com.tgs.services.hms.datamodel.dotw.DotwPassenger;
import com.tgs.services.hms.datamodel.dotw.DotwRoomBookingDetailResponse;
import com.tgs.services.hms.datamodel.dotw.RateBasis;
import com.tgs.services.hms.datamodel.dotw.Request;
import com.tgs.services.hms.datamodel.dotw.Results;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class DotwRetrieveBookingService extends DotwBaseService{

	private HotelImportBookingParams importBookingParams;
	private HotelSupplierConfiguration supplierConf;
	private RestAPIListener listener;
	private HotelImportedBookingInfo bookingInfo;
	private Results result;
	
	
	public void retrieveBooking() throws IOException, JAXBException{
		
		HttpUtils httpUtils = null;
		try {
			listener = new RestAPIListener("");
			Customer customer = createRetrieveBookingRequest();
			String xmlRequest = DotwMarshallerWrapper.marshallXml(customer);
			httpUtils = getHttpRequest(xmlRequest, supplierConf);
			String xmlResponse = (String) httpUtils.getResponse(null).orElse(null);
			result = DotwMarshallerWrapper.unmarshallXML(xmlResponse);
			bookingInfo = createBookingDetailResponse();
		}finally {
			try {
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
				httpUtils.getCheckPoints().stream()
						.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOKING_DETAIL.name()));
				SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
				if (result == null) {
					SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
				}
				listener.addLog(LogData.builder().key(importBookingParams.getBookingId()).logData(httpUtils.getUrlString())
						.type("Dotw-GetBookingReq")
						.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
								.atZone(ZoneId.systemDefault()).toLocalDateTime())
						.build());
				listener.addLog(LogData.builder().key(importBookingParams.getBookingId())
						.logData(httpUtils.getResponseString()).type("Dotw-GetBookingRes")
						.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
								.atZone(ZoneId.systemDefault()).toLocalDateTime())
						.build());
			} catch (Exception e) {
				log.error("Unable to set rest api logs for hotel detail search {}", searchQuery, e);
			}
		}
		
		
	}


	private HotelImportedBookingInfo createBookingDetailResponse() {
		
		if(result == null || result.getBookingDetailResponse() == null)
			throw new CustomGeneralException("Error While Trying To Fetch Booking Details");
		DotwBookingDetailResponse response = result.getBookingDetailResponse();
		HotelInfo hInfo = getHotelDetails(response);
		HotelSearchQuery searchQuery = getHotelSearchQuery(response);
		List<RoomInfo> roomInfos  = new ArrayList<>();
		String currencyCode = null;
		for(DotwRoomBookingDetailResponse roomResponse : response.getRoomDetails()) {
			currencyCode = roomResponse.getCurrency();
			RoomInfo roomInfo = new RoomInfo();
			RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().roomBookingId(roomResponse.getRoomBookingId())
					.roomTypeCode(roomResponse.getRoomTypeCode()).build();
			roomInfo.setMiscInfo(roomMiscInfo);
			roomInfo.setNumberOfAdults(roomResponse.getAdults());
			roomInfo.setRoomCategory(roomResponse.getRoomCategory());
			roomInfo.setRoomType(roomResponse.getRoomName());
			roomInfo.setMealBasis(getMealBasis(roomResponse.getRateBasis()));
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomInfo.setTotalPrice(roomResponse.getServicePrice().getServicePrice());
			if(roomResponse.getChildren() != null) roomInfo.setNumberOfChild(roomResponse.getChildren().getNo());
			RateBasis rateBasis = new RateBasis();
			rateBasis.setCancellationRules(roomResponse.getCancellationRules());
			rateBasis.setTotal(getAmountBasedOnCurrency(roomResponse.getServicePrice().getServicePrice(),"USD"));
			HotelCancellationPolicy cp = getRoomCancellationPolicy(rateBasis , searchQuery,roomInfo);
			roomInfo.setCancellationPolicy(cp);
			
			
			List<TravellerInfo> travellerInfoList = getTravellerList(roomResponse.getPassengerList());
			roomInfo.setTravellerInfo(travellerInfoList);
			
			List<PriceInfo> priceInfoList = getDailyPriceInfo(roomResponse.getDate());
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomInfos.add(roomInfo);
			
		}
		
		Double totalPrice = getTotalSupplierPrice(roomInfos);
	//	HotelCancellationPolicy optionCancellationPolicy = getOptionCancellationPolicyFromRooms(roomInfos);
		Option option = Option.builder().totalPrice(totalPrice)
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo()
						.getSupplierName()).secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(hInfo.getMiscInfo().getSupplierStaticHotelId()).sourceId(supplierConf.getBasicInfo().getSourceId())
						.build())
				.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true))
			//	.cancellationPolicy(optionCancellationPolicy)
				.build();
		setCancellationDeadlineFromRooms(roomInfos,  option);
		//String optionId = option.getId();
		//option.getCancellationPolicy().setId(optionId);
		List<Option> options = new ArrayList<>();
		options.add(option);
		hInfo.setOptions(options);
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		
		HotelImportedBookingInfo importedBookingInfo = HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery)
				.orderStatus(OrderStatus.ON_HOLD.getStatus()).deliveryInfo(deliveryInfo).bookingCurrencyCode(currencyCode).build();
		return importedBookingInfo;
	}
	
	private List<TravellerInfo> getTravellerList(List<DotwPassenger> passengers){
		
		List<TravellerInfo> travellerInfoList = new ArrayList<>();
		for(DotwPassenger passenger : passengers) {
			TravellerInfo ti = new TravellerInfo();
			ti.setFirstName(passenger.getFirstName());
			ti.setLastName(passenger.getLastName());
			ti.setTitle(getTitleFromSaluationCode(passenger.getSalutation()));
			travellerInfoList.add(ti);
		}
		return travellerInfoList;
	}
	
	private HotelSearchQuery getHotelSearchQuery(DotwBookingDetailResponse response) {
		
		List<DotwRoomBookingDetailResponse> roomResponseList = response.getRoomDetails();
		DotwRoomBookingDetailResponse roomResponse = roomResponseList.get(0);
		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
				searchQueryBuilder.checkinDate(LocalDate.parse(roomResponse.getFrom()));
				searchQueryBuilder.checkoutDate(LocalDate.parse(roomResponse.getTo()));
		return searchQueryBuilder.build();
	}
	
	private HotelInfo getHotelDetails(DotwBookingDetailResponse response) {
		
		List<DotwRoomBookingDetailResponse> roomResponseList = response.getRoomDetails();
		DotwRoomBookingDetailResponse roomResponse = roomResponseList.get(0);
		String hotelId = roomResponse.getHotelId();
		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelId).supplierBookingReference(response.getBookingId()).build();
		HotelInfo hInfo = HotelInfo.builder().miscInfo(hMiscInfo).name(roomResponse.getHotelName()).build();
		return hInfo;
		
	}


	private Customer createRetrieveBookingRequest() {
		
		Customer customer = getCustomer(supplierConf);
		Request request = new Request();
		request.setCommand("getbookingdetails");
		DotwBookingDetailRequest bookingDetailRequest = new DotwBookingDetailRequest();
		bookingDetailRequest.setBookingCode(importBookingParams.getSupplierBookingId());
		bookingDetailRequest.setBookingType(1);
		request.setBookingDetails(bookingDetailRequest);
		customer.setRequest(request);
		return customer;
	}
	
}
