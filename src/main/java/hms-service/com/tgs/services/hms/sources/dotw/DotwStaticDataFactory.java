package com.tgs.services.hms.sources.dotw;

import java.io.IOException;
import java.util.List;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DotwStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	private HotelCityInfoMappingManager cityInfoMappingManager;

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	private HotelCityInfoMappingManager cityManager;

	public DotwStaticDataFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getCityMappingData() throws IOException {
		DotwCitySaveService citySaveService =
				DotwCitySaveService.builder().supplierConf(this.getSupplierConf()).build();
		List<HotelCityInfoQuery> cityData = citySaveService.getCityMapppings();
		log.info("dotw city size : " + cityData.size());
		cityManager.saveCityInfoMappingList(cityData);

	}

	@Override
	protected void getHotelStaticData() throws IOException, JAXBException {

		DotwHotelSaveService hotelSaveService = DotwHotelSaveService.builder().supplierConf(this.getSupplierConf())
				.cityInfoMappingManager(cityInfoMappingManager).hotelInfoSaveManager(hotelInfoSaveManager).build();
		hotelSaveService.process();

	}

}
