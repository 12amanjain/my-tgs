package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingCancellationFactory;
import com.tgs.services.hms.sources.expedia.ExpediaBookingCancellationService.ExpediaBookingCancellationServiceBuilder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;

@Service
public class ExpediaBookingCancellationFactory extends AbstractHotelBookingCancellationFactory {

	public ExpediaBookingCancellationFactory(HotelSupplierConfiguration supplierConf, HotelInfo hInfo, Order order) {
		super(supplierConf, hInfo, order);
	}

	@Override
	public boolean cancelHotel() throws IOException {
		ExpediaBookingCancellationServiceBuilder cancellationServiceBuilder = ExpediaBookingCancellationService
				.builder().supplierConf(supplierConf).sourceConf(sourceConfigOutput).order(order).hInfo(getHInfo());
		if (order.getAdditionalInfo().getFlowType().equals(OrderFlowType.IMPORT_PNR))
			cancellationServiceBuilder.isImportedBooking(true);
		ExpediaBookingCancellationService cancellationService = cancellationServiceBuilder.build();
		cancellationService.init();
		return cancellationService.cancelBooking();
	}

	@Override
	public boolean getCancelHotelStatus() {
		return false;
	}
}