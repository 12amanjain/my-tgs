package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.expedia.BookingCancelResponse;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingRequest;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingResponse;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveHoldBookingResponse;
import com.tgs.services.hms.datamodel.expedia.Room;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
public class ExpediaBookingCancellationService {

	private GeneralServiceCommunicator gnComm;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConf;
	private Order order;
	private HotelInfo hInfo;
	protected RestAPIListener listener;
	private Boolean isImportedBooking;

	private static final String RETREIVE_BOOKING_SUFFIX = "/itineraries";
	protected static final Integer SLEEP_TIME_IN_SECS = 5;

	public void init() {
		gnComm = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
	}

	public boolean cancelBooking() throws IOException {
		boolean cancellationResponse = false;
		ExpediaRetrieveBookingResponse response = retrieveBookingDetails();
		cancellationResponse = cancelBookingRoomWise(response);
		return cancellationResponse;
	}

	private ExpediaRetrieveBookingResponse retrieveBookingDetails() throws IOException {

		HttpUtilsV2 httpUtils = null;
		ExpediaRetrieveBookingResponse response = null;
		try {
			listener = new RestAPIListener("");
			ExpediaRetrieveBookingRequest retrieveBookingRequest = ExpediaUtils.createRetrieveBookingRequest(
					order.getDeliveryInfo(), hInfo.getMiscInfo().getSupplierBookingReference());
			httpUtils = ExpediaUtils.getResponseURL(retrieveBookingRequest, sourceConf, supplierConf);
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			httpUtils.getHeaderParams().putAll(headerParam);
			response = httpUtils.getResponse(ExpediaRetrieveBookingResponse.class).orElse(null);
			log.info("Expedia retrieve booking response is :{}", response);
			return response;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
					.type("Expedia-BookingRetrieveReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Expedia-BookingRetrieveRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private ExpediaRetrieveHoldBookingResponse retrieveHoldBookingDetails() throws IOException {

		HttpUtilsV2 httpUtils = null;
		ExpediaRetrieveHoldBookingResponse response = null;
		try {
			listener = new RestAPIListener("");
			ExpediaRetrieveBookingRequest retrieveBookingRequest = ExpediaUtils.createRetrieveBookingRequest(
					order.getDeliveryInfo(), hInfo.getMiscInfo().getSupplierBookingReference());
			httpUtils = ExpediaUtils.getResponseURL(retrieveBookingRequest, sourceConf, supplierConf);
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			httpUtils.getHeaderParams().putAll(headerParam);
			response = httpUtils.getResponse(ExpediaRetrieveHoldBookingResponse.class).orElse(null);
			log.info("Expedia retrieve booking response is :{}", response);
			return response;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
					.type("Expedia-BookingRetrieveReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Expedia-BookingRetrieveRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private boolean cancelHoldBooking(ExpediaRetrieveHoldBookingResponse retrieveHoldBookingResponse)
			throws IOException {
		if (!ObjectUtils.isEmpty(retrieveHoldBookingResponse.getType())) {
			return retrieveHoldBookingResponse.getType().equals("resource_not_found") ? true : false;
		}

		HttpUtilsV2 httpUtils = null;
		try {
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			headerParam.put("Accept", "application/json");

			httpUtils = HttpUtilsV2.builder().headerParams(headerParam).timeout(ExpediaConstants.TIMEOUT)
					.urlString(supplierConf.getHotelSupplierCredentials().getUrl()
							+ retrieveHoldBookingResponse.getLinks().get("cancel").getHref())
					.requestMethod("DELETE").build();
			BookingCancelResponse cancellationResponse =
					httpUtils.getResponse(BookingCancelResponse.class).orElse(null);
			if (cancellationResponse == null
					&& httpUtils.getResponseHeaderParams().get("status").get(0).equals("204")) {
				return true;
			} else if (cancellationResponse == null
					&& httpUtils.getResponseHeaderParams().get("status").get(0).equals("202")) {
				retrieveHoldBookingResponse = retrieveHoldBookingDetails();
				if (!ObjectUtils.isEmpty(retrieveHoldBookingResponse.getType())) {
					return retrieveHoldBookingResponse.getType().equals("resource_not_found") ? true : false;
				}
			}
		} finally {
			if (!ObjectUtils.isEmpty(httpUtils)) {
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

				listener.addLog(
						LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
								.type("Expedia-HoldBookingCancellationReq")
								.generationTime(Instant
										.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
								.build());
				listener.addLog(LogData.builder().key(order.getBookingId())
						.logData(StringUtils.isNotBlank(httpUtils.getResponseString()) ? httpUtils.getResponseString()
								: GsonUtils.getGson().toJson(httpUtils.getResponseHeaderParams()))
						.type("Expedia-HoldBookingCancellationRes")
						.generationTime(
								Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
						.build());
			}
		}
		return false;
	}


	private boolean cancelBookingRoomWise(ExpediaRetrieveBookingResponse response) throws IOException {

		if (StringUtils.isNotEmpty(response.getType())) {
			log.error("Error during booking cancellation, response is {} for bookingId {} ", response.getMessage(),
					order.getBookingId());
			return false;
		}

		int successfulCancellationCount = 0;
		List<Room> unknownStatusRooms = new ArrayList<>();
		for (Room room : response.getRooms()) {
			int retryCount = 3;
			HttpUtilsV2 httpUtils = null;
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			headerParam.put("Accept", "application/json");
			while (retryCount > 0) {
				try {
					headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
					if (room.getStatus().equals("canceled")) {
						log.info("Booking with booking id {} and room id {} is already cancelled", order.getBookingId(),
								room.getId());
						successfulCancellationCount++;
						retryCount = 0;
					} else {
						httpUtils =
								HttpUtilsV2.builder().headerParams(headerParam).timeout(ExpediaConstants.TIMEOUT)
										.urlString(supplierConf.getHotelSupplierCredentials().getUrl()
												+ room.getLinks().get("cancel").getHref())
										.requestMethod("DELETE").build();
						BookingCancelResponse cancellationResponse =
								httpUtils.getResponse(BookingCancelResponse.class).orElse(null);

						if (!ObjectUtils.isEmpty(cancellationResponse)
								&& !ObjectUtils.isEmpty(cancellationResponse.getType())
								&& cancellationResponse.getType().equals("unknown_internal_error")) {
							log.info("Unable to cancel booking id {} and room id {} due to {}, retry count {}",
									order.getBookingId(), room.getId(), cancellationResponse.getMessage(), retryCount);

							log.info("Thread is sleeping for {} seconds", SLEEP_TIME_IN_SECS);
							Thread.sleep(1000 * SLEEP_TIME_IN_SECS);
							retryCount--;
						} else {
							if (cancellationResponse == null
									&& httpUtils.getResponseHeaderParams().get("status").get(0).equals("204")) {
								successfulCancellationCount++;
							} else if (cancellationResponse == null
									&& httpUtils.getResponseHeaderParams().get("status").get(0).equals("202")) {
								unknownStatusRooms.add(room);
							}
							retryCount = 0;
						}
					}
				} catch (InterruptedException e) {
					log.error("Thread Intrepputed for booking {} ,error ", order.getBookingId(), e);
					throw new CustomGeneralException(SystemError.INTERRUPRT_EXPECTION);
				} finally {
					if (!ObjectUtils.isEmpty(httpUtils)) {
						Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
								.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

						listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
								.type("Expedia-BookingCancellationReq")
								.generationTime(Instant
										.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
								.build());
						listener.addLog(LogData.builder().key(order.getBookingId())
								.logData(StringUtils.isNotBlank(httpUtils.getResponseString())
										? httpUtils.getResponseString()
										: GsonUtils.getGson().toJson(httpUtils.getResponseHeaderParams()))
								.type("Expedia-BookingCancellationRes")
								.generationTime(Instant
										.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
								.build());
					}
				}
			}
		}

		if (CollectionUtils.isNotEmpty(unknownStatusRooms)) {
			successfulCancellationCount += checkStatus(unknownStatusRooms);
		}
		if (successfulCancellationCount == response.getRooms().size()) {
			return true;
		}
		return false;
	}

	private int checkStatus(List<Room> roomList) {
		int successfulCancellationCount = 0;
		try {
			ExpediaRetrieveBookingResponse response = retrieveBookingDetails();
			for (Room room : response.getRooms()) {
				for (Room unknownStatusRoom : roomList) {
					if (unknownStatusRoom.getBed_group_id().equals(room.getBed_group_id())
							&& unknownStatusRoom.getId().equals(room.getId()) && room.getStatus().equals("canceled")) {
						successfulCancellationCount++;
					}
				}
			}
		} catch (IOException e) {
			return 0;
		}

		return successfulCancellationCount;
	}

	private boolean isConfirmedBooking() {

		boolean isConfirmedBooking = true;
		if (order.getAdditionalInfo() == null || order.getAdditionalInfo().getPaymentStatus() == null) {
			isConfirmedBooking = false;
		}
		return isConfirmedBooking;
	}
}
