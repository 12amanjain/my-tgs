package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.OptionLinkType;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.expedia.Address;
import com.tgs.services.hms.datamodel.expedia.BillingContact;
import com.tgs.services.hms.datamodel.expedia.BookingCancelResponse;
import com.tgs.services.hms.datamodel.expedia.ExpediaBookingRequest;
import com.tgs.services.hms.datamodel.expedia.ExpediaBookingResponse;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingRequest;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingResponse;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveHoldBookingResponse;
import com.tgs.services.hms.datamodel.expedia.ExpediaRoom;
import com.tgs.services.hms.datamodel.expedia.Payment;
import com.tgs.services.hms.datamodel.expedia.Phone;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Builder
public class ExpediaBookingService {

	private GeneralServiceCommunicator gnComm;
	private HotelInfo hotel;
	private Order order;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private ExpediaBookingResponse response;
	protected RestAPIListener listener;

	public void init() {
		gnComm = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
	}

	public boolean book() throws IOException {
		HttpUtilsV2 httpUtils = null;
		ExpediaBookingRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = createBookingRequest();
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			headerParam.put("Accept", "application/json");

			httpUtils = HttpUtilsV2.builder().headerParams(headerParam).timeout(ExpediaConstants.TIMEOUT)
					.urlString(supplierConf.getHotelSupplierCredentials().getUrl()
							+ hotel.getOptions().get(0).getMiscInfo().getLinks().get(OptionLinkType.ITINERARY))
					.postData(GsonUtils.getGson().toJson(request)).build();
			httpUtils.setPrintResponseLog(true);
			httpUtils.getResponse(ExpediaBookingResponse.class);
			response = GsonUtils.getGson().fromJson(httpUtils.getResponseString(), ExpediaBookingResponse.class);
			log.info("Expedia Booking Response is {}", GsonUtils.getGson().toJson(response));
			boolean bookingStatus = updateBookingStatus();
			return bookingStatus;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(response)) {
				SystemContextHolder.getContextData().getErrorMessages()
						.add(httpUtils.getResponseString() + httpUtils.getPostData());
			}
			listener.addLog(LogData.builder().key(order.getBookingId())
					.logData(httpUtils.getUrlString() + "/n" + httpUtils.getPostData()).type("Expedia-BookingReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Expedia-BookingRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private ExpediaRetrieveBookingResponse retrieveBookingDetails() throws IOException {

		HttpUtilsV2 httpUtils = null;
		ExpediaRetrieveBookingResponse response = null;
		try {
			listener = new RestAPIListener("");
			ExpediaRetrieveBookingRequest retrieveBookingRequest = ExpediaUtils
					.createRetrieveBookingRequest(order.getDeliveryInfo(), hotel.getMiscInfo().getSupplierBookingId());
			httpUtils = ExpediaUtils.getResponseURL(retrieveBookingRequest, sourceConfigOutput, supplierConf);
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			httpUtils.getHeaderParams().putAll(headerParam);
			httpUtils.getResponse(ExpediaRetrieveBookingResponse.class);
			response =
					GsonUtils.getGson().fromJson(httpUtils.getResponseString(), ExpediaRetrieveBookingResponse.class);
			log.info("Expedia retrieve booking response is {}", GsonUtils.getGson().toJson(response));
			return response;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
					.type("Expedia-BookingRetrieveReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Expedia-BookingRetrieveRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private ExpediaRetrieveHoldBookingResponse retrieveHoldBookingDetails() throws IOException {

		HttpUtilsV2 httpUtils = null;
		ExpediaRetrieveHoldBookingResponse response = null;
		try {
			listener = new RestAPIListener("");
			ExpediaRetrieveBookingRequest retrieveBookingRequest = ExpediaUtils
					.createRetrieveBookingRequest(order.getDeliveryInfo(), hotel.getMiscInfo().getSupplierBookingId());
			httpUtils = ExpediaUtils.getResponseURL(retrieveBookingRequest, sourceConfigOutput, supplierConf);
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			httpUtils.getHeaderParams().putAll(headerParam);
			httpUtils.getResponse(ExpediaRetrieveHoldBookingResponse.class);
			response = GsonUtils.getGson().fromJson(httpUtils.getResponseString(),
					ExpediaRetrieveHoldBookingResponse.class);
			log.info("Expedia retrieve hold booking response is :{}", response);
			return response;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
					.type("Expedia-BookingRetrieveReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Expedia-BookingRetrieveRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	public boolean confirmHoldBooking() throws IOException {

		ExpediaRetrieveHoldBookingResponse retrieveHoldBookingResponse = retrieveHoldBookingDetails();
		if (!ObjectUtils.isEmpty(retrieveHoldBookingResponse.getType())
				|| MapUtils.isEmpty(retrieveHoldBookingResponse.getLinks())) {
			return false;
		}
		HttpUtilsV2 httpUtils = null;
		try {
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			headerParam.put("Accept", "application/json");

			httpUtils =
					HttpUtilsV2.builder().headerParams(headerParam).timeout(ExpediaConstants.TIMEOUT)
							.urlString(supplierConf.getHotelSupplierCredentials().getUrl()
									+ retrieveHoldBookingResponse.getLinks().get("resume").getHref())
							.putData("{}").build();

			httpUtils.getResponse(BookingCancelResponse.class);
			BookingCancelResponse cancellationResponse =
					GsonUtils.getGson().fromJson(httpUtils.getResponseString(), BookingCancelResponse.class);

			if (cancellationResponse == null
					&& httpUtils.getResponseHeaderParams().get("status").get(0).equals("204")) {
				return true;
			} else if (cancellationResponse == null
					&& httpUtils.getResponseHeaderParams().get("status").get(0).equals("202")) {
				ExpediaRetrieveBookingResponse retrieveBookingResponse = retrieveBookingDetails();
				return retrieveBookingResponse.getRooms().get(0).getStatus().equals("booked") ? true : false;
			}
		} finally {
			if (!ObjectUtils.isEmpty(httpUtils)) {
				Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
						.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

				listener.addLog(
						LogData.builder().key(order.getBookingId())
								.logData(httpUtils.getUrlString() + httpUtils.getPutData())
								.type("Expedia-ConfirmHoldBookingReq")
								.generationTime(Instant
										.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
								.build());
				listener.addLog(LogData.builder().key(order.getBookingId())
						.logData(StringUtils.isNotBlank(httpUtils.getResponseString()) ? httpUtils.getResponseString()
								: GsonUtils.getGson().toJson(httpUtils.getResponseHeaderParams()))
						.type("Expedia-ConfirmHoldBookingRes")
						.generationTime(
								Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
										.atZone(ZoneId.systemDefault()).toLocalDateTime())
						.build());
			}
		}
		return false;
	}

	private ExpediaBookingRequest createBookingRequest() {

		ClientGeneralInfo cInfo = gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		ExpediaBookingRequest bookingRequest = new ExpediaBookingRequest();
		bookingRequest.setEmail(cInfo.getAltEmailIds().get(0));
		bookingRequest
				.setPhone(Phone.builder().number(cInfo.getGstInfo().getPhone().replaceAll("\\s+", ""))
						.country_code(cInfo.getCountryCode())
						.build());
		List<ExpediaRoom> expediaRoomList = new ArrayList<>();
		for (RoomInfo roomInfo : hotel.getOptions().get(0).getRoomInfos()) {
			TravellerInfo travellerInfo = roomInfo.getTravellerInfo().get(0);
			ExpediaRoom expediaRoom = ExpediaRoom.builder().title(travellerInfo.getTitle())
					.given_name(travellerInfo.getFirstName()).family_name(travellerInfo.getLastName()).build();
			expediaRoomList.add(expediaRoom);
		}
		
		List<Payment> paymentList = new ArrayList<>();
		Payment payment = Payment.builder().type("affiliate_collect").billing_contact(BillingContact.builder()
				.given_name(cInfo.getName()).family_name(cInfo.getName())
				.address(Address.builder().line_1(cInfo.getAddress1()).city(cInfo.getCity())
						.postal_code(cInfo.getPostalCode()).country_code(cInfo.getNationality()).build())
				.build()).build();
		
		paymentList.add(payment);
		// bookingRequest.setHold(!isConfirmedBooking());
		bookingRequest.setHold(false);
		bookingRequest.setAffiliate_reference_id(order.getBookingId());
		bookingRequest.setRooms(expediaRoomList);
		bookingRequest.setPayments(paymentList);
		return bookingRequest;
	}

	public boolean updateBookingStatus() throws IOException {

		boolean isBooked = false;
		if (StringUtils.isEmpty(response.getItinerary_id())) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Booking is failed due to " + response.getMessage());
			log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
					this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(), response.getMessage());
		} else {
			updateSupplierBookingReferenceId();
			isBooked = true;
		}
		return isBooked;
	}

	public void updateSupplierBookingReferenceId() {

		hotel.getMiscInfo().setSupplierBookingId(order.getBookingId());
		hotel.getMiscInfo().setSupplierBookingReference(response.getItinerary_id());
	}

	private boolean isConfirmedBooking() {

		boolean isConfirmedBooking = true;
		if (order.getAdditionalInfo() == null || order.getAdditionalInfo().getPaymentStatus() == null) {
			isConfirmedBooking = false;
		}
		return isConfirmedBooking;
	}

}
