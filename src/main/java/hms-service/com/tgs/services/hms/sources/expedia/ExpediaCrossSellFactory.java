package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelTag;
import com.tgs.services.hms.datamodel.expedia.ExpediaAdditionalSearchParams;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ExpediaCrossSellFactory extends AbstractHotelInfoFactory {

	ExpediaHotelInfoFactory exhotelInfoFactory;

	public ExpediaCrossSellFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
		exhotelInfoFactory =
				SpringContext.getApplicationContext().getBean(ExpediaHotelInfoFactory.class, searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException {

		Future<?> hotelCrossSellSearchFutureTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				return searchCrossSellHotels();
			} catch (IOException cause) {
				throw new CustomGeneralException("Unable to fetch search result due to " + cause.getMessage());
			}
		});

		if (!ObjectUtils.isEmpty(searchQuery) && !ObjectUtils.isEmpty(searchQuery.getSearchPreferences())
				&& !ObjectUtils.isEmpty(searchQuery.getSearchPreferences().getCrossSellParameter())
				&& BooleanUtils.isTrue(searchQuery.getSearchPreferences().getCrossSellParameter().getCrossSellOnly())) {
			try {
				searchResult = (HotelSearchResult) hotelCrossSellSearchFutureTask.get();
			} catch (ExecutionException | InterruptedException e) {
				log.error("Interrupted exception while fetching cross sell search result {} ", this.searchQuery, e);
			}
			return;
		}

		Future<?> hotelSearchFutureTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				exhotelInfoFactory.searchAvailableHotels();
				return exhotelInfoFactory.getSearchResult();
			} catch (IOException cause) {
				throw new CustomGeneralException("Unable to fetch search result due to " + cause.getMessage());
			}
		});

		try {
			HotelSearchResult rawHotelsSearchResult = (HotelSearchResult) hotelSearchFutureTask.get();
			HotelSearchResult crossSellHotelsSearchResult = (HotelSearchResult) hotelCrossSellSearchFutureTask.get();
			searchResult = mergeCrossSellHotelInRawHotels(rawHotelsSearchResult, crossSellHotelsSearchResult);
		} catch (ExecutionException | InterruptedException e) {
			log.error("Interrupted exception while fetching search result {} ", this.searchQuery, e);
		}
	}

	private static HotelSearchResult mergeCrossSellHotelInRawHotels(HotelSearchResult rawHotelsSearchResult,
			HotelSearchResult crossSellHotelSearchResult) {
		Map<String, HotelInfo> rawHotelsSearchResultMap = rawHotelsSearchResult.getHotelInfos().stream()
				.collect(Collectors.toMap(hInfo -> hInfo.getMiscInfo().getSupplierStaticHotelId(), hInfo -> hInfo));
		log.info("No of raw hotels are {}", rawHotelsSearchResultMap.size());

		Map<String, HotelInfo> crossSellHotelSearchResultMap = crossSellHotelSearchResult.getHotelInfos().stream()
				.collect(Collectors.toMap(hInfo -> hInfo.getMiscInfo().getSupplierStaticHotelId(), hInfo -> hInfo));

		log.info("No of cross sell hotels are {}", crossSellHotelSearchResultMap.size());
		crossSellHotelSearchResultMap.keySet().forEach(hotelId -> {
			HotelInfo rawHotel = rawHotelsSearchResultMap.get(hotelId);
			if (!ObjectUtils.isEmpty(rawHotel)) {
				rawHotelsSearchResultMap.put(hotelId, crossSellHotelSearchResultMap.get(hotelId));
			}
		});
		
		HotelSearchResult finalSearchResult = new HotelSearchResult();
		List<HotelInfo> hotelInfos = rawHotelsSearchResultMap.values().stream().sorted(
				Comparator.comparing(HotelInfo::isCrossSellHotel, Comparator.nullsLast(Collections.reverseOrder())))
				.collect(Collectors.toList());
		log.info("Merged hotels are {}", hotelInfos.size());
		
		for (int i = 0; i < 10; i++) {
			hotelInfos.get(i).getTags().add(HotelTag.CROSS_SELL);
		}
		finalSearchResult.getHotelInfos().addAll(hotelInfos);

		return finalSearchResult;
	}

	private HotelSearchResult searchCrossSellHotels() throws IOException {
		ExpediaAdditionalSearchParams searchParams = ExpediaAdditionalSearchParams.builder()
				.country_code(gsCommunicator.getCountryCode(searchQuery.getSearchCriteria().getCountryName()))
				.language(HotelUtils.getLanguage()).build();

		ExpediaSearchService searchService =
				ExpediaSearchService.builder().supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
						.crossSellParameter(searchQuery.getSearchPreferences().getCrossSellParameter())
						.additionalSearchParams(searchParams).sourceConfigOutput(sourceConfigOutput).build();
		searchService.doSearch();

		HotelSearchResult crossSellHotelsSearchResult = searchService.getSearchResult();
		crossSellHotelsSearchResult.getHotelInfos().forEach(hInfo -> {
			hInfo.getTags().add(HotelTag.CROSS_SELL);
		});
		return crossSellHotelsSearchResult;
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		exhotelInfoFactory.searchCancellationPolicy(hotel, logKey);
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		// TODO Auto-generated method stub
		
	}

}
