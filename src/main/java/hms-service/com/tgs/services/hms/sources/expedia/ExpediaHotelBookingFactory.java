package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class ExpediaHotelBookingFactory extends AbstractHotelBookingFactory {

	public ExpediaHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException {

		ExpediaBookingService bookingService = ExpediaBookingService.builder().hotel(this.getHotel()).order(order)
				.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).build();
		bookingService.init();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
//		if(order.getAdditionalInfo().getFlowType().equals(OrderFlowType.IMPORT_PNR))
//			return true;
//		ExpediaBookingService bookingService = ExpediaBookingService.builder().hotel(this.getHotel()).order(order)
//				.supplierConf(supplierConf).sourceConfigOutput(sourceConfigOutput).build();
//		bookingService.init();
//		return bookingService.confirmHoldBooking();
	}

}

