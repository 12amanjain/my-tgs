package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.expedia.ExpediaAdditionalSearchParams;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Setter
@Slf4j
public class ExpediaHotelInfoFactory extends AbstractHotelInfoFactory {

	private Set<String> propertyIds;

	public ExpediaHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException {
		searchResult = new HotelSearchResult();
		String cityName = org.apache.commons.lang3.ObjectUtils.firstNonNull(supplierCityInfo.getSupplierCityName(),
				searchQuery.getSearchCriteria().getCityName());
		propertyIds = getPropertyIdsOfCity(cityName,
				searchQuery.getSearchCriteria().getCountryName());

		SystemContextHolder.getContextData().addCheckPoint(CheckPointData.builder()
				.type(SystemCheckPoint.EXTERNAL_API_STARTED.name()).time(System.currentTimeMillis())
				.subType(HotelFlowType.SEARCH.name()).build());
		Future<?> hotelOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				HotelSearchResult hotelOnlySearchResult = getResultOfAllPropertyIds(propertyIds, null);
				return hotelOnlySearchResult;
			} catch (IOException e) {
				log.error("IOException while fetching hotel only search result for search query {} ", this.searchQuery,
						e);
				throw new CustomGeneralException("Unable to fetch hotel only search result due to " + e.getMessage());
			}
		});

		Future<?> packageOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				HotelSearchResult packageOnlySearchResult = searchPackageOnlyAvailableHotels();
				return packageOnlySearchResult;
			} catch (IOException e) {
				log.error("IOException while fetching package only search result for search query {} ",
						this.searchQuery, e);
				throw new CustomGeneralException("Unable to fetch package only search result due to " + e.getMessage());
			}

		});

		try {
			Object hotelOnlySearchResultObj = hotelOnlySearchResultTask.get();
			Object packageOnlySearchResultObj = packageOnlySearchResultTask.get();
			if (Objects.nonNull(hotelOnlySearchResultObj) && Objects.nonNull(packageOnlySearchResultObj)) {
				HotelSearchResult hotelOnlySearchResult = (HotelSearchResult) hotelOnlySearchResultObj;
				HotelSearchResult packageOnlySearchResult = (HotelSearchResult) packageOnlySearchResultObj;
				SystemContextHolder.getContextData().addCheckPoint(CheckPointData.builder()
						.type(SystemCheckPoint.EXTERNAL_API_FINISHED.name()).time(System.currentTimeMillis())
						.subType(HotelFlowType.SEARCH.name()).build());
				searchResult = mergePackageAndNonPackageResult(hotelOnlySearchResult, packageOnlySearchResult);
				SystemContextHolder.getContextData().addCheckPoint(
						CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
								.subType(HotelFlowType.SEARCH.name()).time(System.currentTimeMillis()).build());
				log.debug("Total number of hotels after merging for {} are {}",
						supplierConf.getBasicInfo().getSupplierName(), searchResult.getNoOfHotelOptions());
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Interrupted exception while fetching search result {} ", this.searchQuery, e);
			return;
		}
	}

	public HotelSearchResult searchPackageOnlyAvailableHotels() throws IOException {
		propertyIds =
				CollectionUtils.isEmpty(propertyIds)
						? getPropertyIdsOfCity(searchQuery.getSearchCriteria().getCityName(),
								searchQuery.getSearchCriteria().getCountryName())
						: propertyIds;
		return getResultOfAllPropertyIds(propertyIds, true);
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String bookingId) throws IOException {
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});
	}

	private HotelSearchResult getResultOfAllPropertyIds(Set<String> propertyIds, Boolean isPackageOnly)
			throws IOException {
		List<Set<String>> listOfPartitionedIdsSet = HotelUtils.partitionSet(propertyIds, 250);
		Integer maxLimit = ObjectUtils.isEmpty(sourceConfigOutput.getPartitionHotelIdsLimit()) ? 30
				: sourceConfigOutput.getPartitionHotelIdsLimit();
		if (listOfPartitionedIdsSet.size() > maxLimit) {
			throw new CustomGeneralException(SystemError.MAXIMUM_PARTITIONSET_LIMIT_EXCEEDED);
		}

		HotelSearchResult finalSearchResult = new HotelSearchResult();
		List<Future<?>> hotelSearchFutureTaskList = new ArrayList<Future<?>>();
		listOfPartitionedIdsSet.forEach(partitionedIdsSet -> {
			Future<?> hotelSearchFutureTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
				try {
					ExpediaAdditionalSearchParams searchParams = ExpediaAdditionalSearchParams.builder()
							.country_code(HotelUtils.getCountryCode()).language(HotelUtils.getLanguage())
							.property_id(partitionedIdsSet).isPackageRate(isPackageOnly).build();

					ExpediaSearchService searchService = ExpediaSearchService.builder()
							.supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
							.additionalSearchParams(searchParams).sourceConfigOutput(sourceConfigOutput).build();
					searchService.doSearch();
					return searchService.getSearchResult();
				} catch (IOException e) {
					log.error("IOException while fetching search result for search query {} ", this.searchQuery, e);
					throw new CustomGeneralException("Unable to fetch search result due to " + e.getMessage());
				} finally {
					LogUtils.clearLogList();
				}
			});
			hotelSearchFutureTaskList.add(hotelSearchFutureTask);
		});

		try {
			for (Future<?> future : hotelSearchFutureTaskList) {
				Object result = future.get();
				if (!ObjectUtils.isEmpty(result)) {
					HotelSearchResult searchResult = (HotelSearchResult) result;
					finalSearchResult.getHotelInfos().addAll(searchResult.getHotelInfos());
				}
			}
			return finalSearchResult;
		} catch (InterruptedException | ExecutionException e) {
			log.error("Interrupted exception while fetching search result {} ", this.searchQuery, e);
			return null;
		}
	}

	protected Set<String> getPropertyIdsOfCity(String cityName, String countryName) {

		log.info("Fetching property id from cache for city {}, country {} and searchid {}", cityName, countryName,
				searchQuery.getSearchId());
		Set<String> propertyIds = new HashSet<>();
		try {
			String supplierMappingSetName = BaseHotelUtils.getSupplierSetName(HotelSourceType.EXPEDIA.name());
			CacheMetaInfo supplierMappingMetaInfo =
					CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.getName()).set(supplierMappingSetName)
							.bins(new String[] {BinName.CITY.name(), BinName.COUNTRY.name(), BinName.RATING.name()})
							.build();
			Map<String, Map<String, String>> supplierhotelIdMap =
					cachingCommunicator.getResultSet(supplierMappingMetaInfo, String.class,
							Filter.equal(BinName.CITY.getName(), "\"" + cityName.toLowerCase() + "\""));
			for (Map.Entry<String, Map<String, String>> supplierHotel : supplierhotelIdMap.entrySet()) {
				Map<String, String> supplierInfo = supplierHotel.getValue();
				if (StringUtils.isNotBlank(supplierInfo.get(BinName.RATING.name()))
						&& searchQuery.getSearchPreferences().getRatings()
								.contains(Integer.parseInt(supplierInfo.get(BinName.RATING.name())))) {
					if (StringUtils.isNotBlank(supplierInfo.get(BinName.COUNTRY.name()))) {
						if (countryName.equalsIgnoreCase(supplierInfo.get(BinName.COUNTRY.name()))) {
							propertyIds.add(supplierHotel.getKey());
						}
					} else {
						propertyIds.add(supplierHotel.getKey());
					}
				}
			}
			return propertyIds;
		} catch (Exception e) {
			log.info("Unable to fetch property ids of city {} and country {}", cityName, countryName, e);
			return null;
		} finally {
			log.info("Fetched property id from cache for city {} and country {} for searchid {}. Size is {}", cityName,
					countryName,
					searchQuery.getSearchId(), propertyIds.size());
		}
	}

	private HotelSearchResult mergePackageAndNonPackageResult(HotelSearchResult hotelOnlySearchResult,
			HotelSearchResult packageOnlySearchResult) {
		HotelSearchResult mergedHotelSearchResult = new HotelSearchResult();
		List<HotelInfo> mergedHotels = new ArrayList<>();

		Map<String, HotelInfo> groupByPackageOnlyResult =
				hotelOnlySearchResult.getHotelInfos().stream().collect(Collectors.toMap(
						hotelInfo -> hotelInfo.getMiscInfo().getSupplierStaticHotelId(), hotelInfo -> hotelInfo));
		Map<String, HotelInfo> groupByHotelOnlyResult =
				packageOnlySearchResult.getHotelInfos().stream().collect(Collectors.toMap(
						hotelInfo -> hotelInfo.getMiscInfo().getSupplierStaticHotelId(), hotelInfo -> hotelInfo));

		Set<String> hotelOnlyCombinedHotels = new HashSet<>();
		groupByPackageOnlyResult.forEach((key, hInfo) -> {
			HotelInfo hotelOnlyResult = groupByHotelOnlyResult.get(key);
			if (!ObjectUtils.isEmpty(hotelOnlyResult)) {
				hInfo.getOptions().addAll(hotelOnlyResult.getOptions());
				hotelOnlyCombinedHotels.add(key);
			}
		});
		mergedHotels.addAll(groupByPackageOnlyResult.values());

		groupByHotelOnlyResult.forEach((key, hInfo) -> {
			if (!hotelOnlyCombinedHotels.contains(key)) {
				mergedHotels.add(hInfo);
			}
		});

		mergedHotelSearchResult.getHotelInfos().addAll(mergedHotels);
		return mergedHotelSearchResult;
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {

		Set<String> propertyIds = new HashSet<>();
		propertyIds.add(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());

		Future<?> hotelOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				ExpediaAdditionalSearchParams searchParams =
						ExpediaAdditionalSearchParams.builder().country_code(HotelUtils.getCountryCode())
								.language(HotelUtils.getLanguage()).property_id(propertyIds).build();

				ExpediaSearchService searchService = ExpediaSearchService.builder().supplierConf(this.getSupplierConf())
						.searchQuery(this.getSearchQuery()).additionalSearchParams(searchParams)
						.sourceConfigOutput(sourceConfigOutput).build();
				searchService.doDetailSearch();
				return searchService.getSearchResult();
			} catch (IOException e) {
				log.error("IOException while fetching detail hotel info for search query {} ", this.searchQuery, e);
				throw new CustomGeneralException("Unable to fetch detail hotel info due to " + e.getMessage());
			} finally {
				LogUtils.clearLogList();
			}
		});

		Future<?> packageOnlySearchResultTask = ExecutorUtils.getHotelSearchThreadPool().submit(() -> {
			try {
				ExpediaAdditionalSearchParams searchParams = ExpediaAdditionalSearchParams.builder()
						.country_code(HotelUtils.getCountryCode()).language(HotelUtils.getLanguage())
						.property_id(propertyIds).isPackageRate(true).build();

				ExpediaSearchService searchService = ExpediaSearchService.builder().supplierConf(this.getSupplierConf())
						.searchQuery(this.getSearchQuery()).additionalSearchParams(searchParams)
						.sourceConfigOutput(sourceConfigOutput).build();
				searchService.doDetailSearch();
				return searchService.getSearchResult();
			} catch (IOException e) {
				log.error("IOException while fetching package only search result for search query {} ",
						this.searchQuery, e);
				throw new CustomGeneralException("Unable to fetch package only search result due to " + e.getMessage());
			}
		});

		try {
			Object hotelOnlySearchResultObj = hotelOnlySearchResultTask.get();
			Object packageOnlySearchResultObj = packageOnlySearchResultTask.get();
			if (Objects.nonNull(hotelOnlySearchResultObj) && Objects.nonNull(packageOnlySearchResultObj)) {
				HotelSearchResult hotelOnlySearchResult = (HotelSearchResult) hotelOnlySearchResultObj;
				HotelSearchResult packageOnlySearchResult = (HotelSearchResult) packageOnlySearchResultObj;
				searchResult = mergePackageAndNonPackageResult(hotelOnlySearchResult, packageOnlySearchResult);
				log.debug("Total number of hotels after merging for {} are {}",
						supplierConf.getBasicInfo().getSupplierName(), searchResult.getNoOfHotelOptions());
				hInfo.setOptions(searchResult.getHotelInfos().get(0).getOptions());
			}
		} catch (InterruptedException | ExecutionException e) {
			log.error("Interrupted exception while fetching detail hotel info {} ", this.searchQuery, e);
			return;
		}
	}
}
