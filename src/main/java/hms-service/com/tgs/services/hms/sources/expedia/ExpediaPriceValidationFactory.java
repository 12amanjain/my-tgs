package com.tgs.services.hms.sources.expedia;

import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class ExpediaPriceValidationFactory extends AbstractHotelPriceValidationFactory{

	public ExpediaPriceValidationFactory(HotelSearchQuery query, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(query, hotel, hotelSupplierConf, bookingId);
		
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		Option oldOption = hInfo.getOptions().get(0);
		ExpediaPriceValidationService priceValidationService = ExpediaPriceValidationService.builder()
				.searchQuery(this.searchQuery).option(oldOption).sourceConfigOutput(sourceConfigOutput)
				.bookingId(bookingId).supplierConf(this.getSupplierConf()).build();
		priceValidationService.fetchPriceChanges();
		return priceValidationService.getUpdatedOption();
	}
}
