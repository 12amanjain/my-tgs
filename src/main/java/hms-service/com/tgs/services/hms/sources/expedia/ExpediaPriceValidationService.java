package com.tgs.services.hms.sources.expedia;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionLinkType;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.expedia.PriceCheckResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ExpediaPriceValidationService {

	private HotelSearchQuery searchQuery;
	private Option option;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private String bookingId;
	private Option updatedOption;
	private RestAPIListener listener;
	private LocalDateTime currentTime;

	public void fetchPriceChanges() throws Exception {
		HttpUtilsV2 httpUtils = null;
		try {
			listener = new RestAPIListener("");
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
			headerParam.put("Accept", "application/json");
			httpUtils = HttpUtilsV2.builder().headerParams(headerParam)
					.urlString(supplierConf.getHotelSupplierCredentials().getUrl()
							+ option.getMiscInfo().getLinks().get(OptionLinkType.PRICE_CHECK))
					.build();
			httpUtils.setPrintResponseLog(true);
			httpUtils.getResponse(PriceCheckResponse.class);
			PriceCheckResponse priceCheckResponse =
					GsonUtils.getGson().fromJson(httpUtils.getResponseString(), PriceCheckResponse.class);

			if (CollectionUtils.isNotEmpty(priceCheckResponse.getFields())) {
				throw new CustomGeneralException("Unable to retreive info from price check api for booking id "
						+ bookingId + "due to " + GsonUtils.getGson().toJson(priceCheckResponse));
			} else {
				updatedOption = createUpdatedOption(priceCheckResponse);
			}
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			listener.addLog(LogData.builder().key(bookingId).logData(httpUtils.getUrlString())
					.type("Expedia-PriceCheckReq-" + supplierConf.getHotelSupplierCredentials().getUserName())
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());

			listener.addLog(LogData.builder().key(bookingId).logData(httpUtils.getResponseString())
					.type("Expedia-PriceCheckRes-" + supplierConf.getHotelSupplierCredentials().getUserName())
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	@SuppressWarnings("unchecked")
	private Option createUpdatedOption(PriceCheckResponse priceCheckResponse) {
		if (priceCheckResponse.getStatus().equals("sold_out")) {
			return null;
		} else if (priceCheckResponse.getStatus().equals("available")) {
			option.getMiscInfo().getLinks().put(OptionLinkType.ITINERARY,
					priceCheckResponse.getLinks().get("book").getHref());
			return option;
		} else {
			Option updatedOption = Option.builder().build();
			updatedOption.setId(option.getId());
			MultiMap occupancies = ExpediaUtils.getOccupancies(searchQuery);
			Map<String, Integer> occupancyMap =
					ExpediaUtils.getOccupancyMap((ArrayList<String>) occupancies.get("occupancy"));
			Option newOption = Option.builder().build();
			ExpediaUtils.setRoomPricing(newOption, null, priceCheckResponse.getOccupancy_pricing(), occupancyMap);

			for (RoomInfo roomInfo : option.getRoomInfos()) {
				String occupancyPattern = roomInfo.getId().split("--")[1];
				for (RoomInfo updatedRoomInfo : newOption.getRoomInfos()) {
					if (StringUtils.isBlank(updatedRoomInfo.getId())
							&& occupancyPattern.equals(updatedRoomInfo.getOccupancyPattern())) {
						updatedRoomInfo.setId(roomInfo.getId());
						updatedRoomInfo.setMealBasis(roomInfo.getMealBasis());
						break;
					}
				}
			}
			option.getMiscInfo().getLinks().put(OptionLinkType.ITINERARY,
					priceCheckResponse.getLinks().get("book").getHref());
			option.setRoomInfos(newOption.getRoomInfos());
			return option;
		}
	}
}
