package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;


@Service
public class ExpediaRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	public ExpediaRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException {
		ExpediaRetrieveBookingService bookingService = ExpediaRetrieveBookingService.builder().supplierConf(supplierConf)
				.sourceConf(sourceConfigOutput).importBookingParams(importBookingParams).build();
		bookingService.init();
		bookingService.importBookingDetails();
		bookingDetailResponse = bookingService.getBookingInfo();
	}
}
