package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.collect.Lists;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelInfo.HotelInfoBuilder;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo.HotelMiscInfoBuilder;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.expedia.ExpediaCancellationDeadline;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingRequest;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingResponse;
import com.tgs.services.hms.datamodel.expedia.ImportedRoomRate;
import com.tgs.services.hms.datamodel.expedia.NightlyPrice;
import com.tgs.services.hms.datamodel.expedia.Room;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelConstants;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class ExpediaRetrieveBookingService {

	private GeneralServiceCommunicator gnComm;
	private HotelImportBookingParams importBookingParams;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConf;
	private HotelImportedBookingInfo bookingInfo;

	public void init() {
		gnComm = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");
	}

	public void importBookingDetails() throws IOException {

		ExpediaRetrieveBookingResponse bookingResponse = retrieveBookingDetails();
		bookingInfo = createBookingResponse(bookingResponse);
	}

	public ExpediaRetrieveBookingResponse retrieveBookingDetails() throws IOException {

		HttpUtilsV2 httpUtils = null;
		ExpediaRetrieveBookingResponse response = null;
		RestAPIListener listener = null;
		try {
			listener = new RestAPIListener("");
			ExpediaRetrieveBookingRequest retrieveBookingRequest = ExpediaUtils.createRetrieveBookingRequest(
					importBookingParams.getDeliveryInfo(), importBookingParams.getSupplierBookingId());
			httpUtils = ExpediaUtils.getResponseURL(retrieveBookingRequest, sourceConf, supplierConf);
			Map<String, String> headerParam = new HashMap<>();
			headerParam.put("Content-Type", "application/json");
			headerParam.put("Accept-Encoding", "application/gzip");
			headerParam.put("Customer-Ip", ExpediaConstants.CUSTOMER_IP);
			httpUtils.getHeaderParams().putAll(headerParam);
			httpUtils.getResponse(ExpediaRetrieveBookingResponse.class);
			response =
					GsonUtils.getGson().fromJson(httpUtils.getResponseString(), ExpediaRetrieveBookingResponse.class);
			log.info("Expedia Booking Response is :{}", GsonUtils.getGson().toJson(response));
			return response;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

			listener.addLog(LogData.builder().key(importBookingParams.getBookingId()).logData(httpUtils.getUrlString())
					.type("Expedia-BookingRetrieveReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(importBookingParams.getBookingId())
					.logData(httpUtils.getResponseString()).type("Expedia-BookingRetrieveRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private HotelImportedBookingInfo createBookingResponse(ExpediaRetrieveBookingResponse bookingResponse) {
		if (!ObjectUtils.isEmpty(bookingResponse.getMessage())
				&& bookingResponse.getMessage().equals("record_not_found")) {
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_BOOKING_ID);
		}

		if (StringUtils.isNotEmpty(bookingResponse.getMessage())) {
			log.error("Unable to get booking result from Expedia due to {} for supplier {} with username {}",
					bookingResponse.getMessage(), this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
		}

		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(LocalDate.parse(bookingResponse.getRooms().get(0).getCheckin()));
		searchQueryBuilder.checkoutDate(LocalDate.parse(bookingResponse.getRooms().get(0).getCheckout()));
		searchQueryBuilder.sourceId(HotelSourceType.EXPEDIA.getSourceId());
		searchQueryBuilder.miscInfo(
				HotelSearchQueryMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName()).build());
		HotelSearchPreferences searchPreferences = new HotelSearchPreferences();
		String currency = bookingResponse.getRooms().get(0).getRate().getCancel_refund().getCurrency();
		searchPreferences.setCurrency(currency);
		searchQueryBuilder.searchPreferences(searchPreferences);

		HotelInfo hotelInfo = setHotelInfo(bookingResponse, searchQueryBuilder.build());

		DeliveryInfo deliveryInfo = new DeliveryInfo();
		deliveryInfo.setContacts(Lists.newArrayList(bookingResponse.getPhone().getNumber()));
		deliveryInfo.setEmails(Lists.newArrayList(bookingResponse.getEmail()));
		// deliveryInfo.cleanData();
		String orderStatus = "";
		if (StringUtils.isNotBlank(bookingResponse.getRooms().get(0).getStatus())) {
			try {
				orderStatus = ExpediaOrderMapping.valueOf(bookingResponse.getRooms().get(0).getStatus().toUpperCase())
						.getCode();
			} catch (IllegalArgumentException e) {
				log.error("Unable to get correct order status for bookingid {}", importBookingParams.getBookingId());
			}
		}
		return HotelImportedBookingInfo.builder().hInfo(hotelInfo).searchQuery(searchQueryBuilder.build())
				.orderStatus(orderStatus).bookingCurrencyCode(currency)
				.affiliateRefId(bookingResponse.getAffiliate_reference_id())
				.bookingDate(OffsetDateTime.parse(bookingResponse.getCreation_date_time())
						.withOffsetSameInstant(ZoneOffset.ofHoursMinutes(5, 30)).toLocalDate())
				.deliveryInfo(deliveryInfo).orderStatus(orderStatus).bookingCurrencyCode(currency).build();
	}

	private HotelInfo setHotelInfo(ExpediaRetrieveBookingResponse bookingResponse, HotelSearchQuery searchQuery) {
		HotelInfoBuilder hotelInfoBuilder = HotelInfo.builder();
		HotelMiscInfoBuilder hotelMiscInfoBuilder = HotelMiscInfo.builder();
		hotelMiscInfoBuilder.supplierBookingId(bookingResponse.getItinerary_id());
		hotelMiscInfoBuilder.supplierStaticHotelId(bookingResponse.getProperty_id());
		hotelMiscInfoBuilder.supplierBookingReference(bookingResponse.getItinerary_id());
		hotelInfoBuilder.miscInfo(hotelMiscInfoBuilder.build());

		Set<String> amenities = new HashSet<>();
		List<Option> optionList = new ArrayList<>();
		optionList.add(populateOptionInfo(bookingResponse, searchQuery, amenities));
		hotelInfoBuilder.options(optionList);

		HotelInfo hotelInfo = hotelInfoBuilder.build();
		setMealBasis(hotelInfo, amenities, searchQuery);
		return hotelInfo;
	}

	private void setMealBasis(HotelInfo hInfo, Set<String> amenities, HotelSearchQuery searchQuery) {

		List<HotelInfo> hotelInfos = new ArrayList<>();
		hotelInfos.add(hInfo);
		ExpediaUtils.setMealBasis(hotelInfos, amenities, searchQuery);
	}

	public Option populateOptionInfo(ExpediaRetrieveBookingResponse bookingResponse, HotelSearchQuery searchQuery,
			Set<String> amenities) {

		Option option = Option.builder().id(
				bookingResponse.getRooms().get(0).getId() + "_" + bookingResponse.getRooms().get(0).getBed_group_id())
				.build();

		List<RoomInfo> roomInfoList = setRoomPricing(bookingResponse.getRooms(), searchQuery, amenities);
		option.setRoomInfos(roomInfoList);
		option.setMiscInfo(OptionMiscInfo.builder().secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
				.sourceId(supplierConf.getBasicInfo().getSourceId()).supplierId(importBookingParams.getSupplierId())
				.build());

		Double totalPrice = roomInfoList.stream().mapToDouble(RoomInfo::getTotalPrice).sum();
		ExpediaCancellationDeadline cancellationDeadline = new ExpediaCancellationDeadline();
		ImportedRoomRate roomRate = bookingResponse.getRooms().get(0).getRate();

		HotelCancellationPolicy cancellationPolicy = ExpediaUtils.getCancellationPolicy(searchQuery,
				roomRate.getRefundable(), roomRate.getCancel_penalties(), totalPrice,
				option.getRoomInfos().get(0).getPerNightPriceInfos().size(), cancellationDeadline,
				option.getRoomInfos(), sourceConf.getCancellationPolicyBuffer());
		option.setCancellationPolicy(cancellationPolicy);

		option.setDeadlineDateTime(cancellationDeadline.getDeadlineDateTime());
		option.setTotalPrice(totalPrice);
		ExpediaUtils.updatePriceWithClientCommissionComponents(option, sourceConf);
		option.getRoomInfos().stream().forEach(ri -> {
			ri.setCancellationPolicy(cancellationPolicy);
			ri.setDeadlineDateTime(cancellationDeadline.getDeadlineDateTime());
		});
		return option;
	}

	public List<RoomInfo> setRoomPricing(List<Room> roomList, HotelSearchQuery searchQuery, Set<String> amenities) {

		List<RoomInfo> roomInfoList = new ArrayList<>();
		AtomicInteger roomCount = new AtomicInteger(1);
		for (Room room : roomList) {
			RoomInfo roomInfo = new RoomInfo();
			String roomType = ExpediaUtils.getRoomType(room.getNumber_of_adults());
			roomInfo.setRoomCategory(roomType);
			roomInfo.setRoomType(roomType);
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			ImportedRoomRate roomRate = room.getRate();
			String roomId = ExpediaUtils.getRoomId(room.getId(), roomRate.getId(), room.getBed_group_id());
			roomInfo.setId(String.join("--", roomId, String.valueOf(roomCount.getAndAdd(1))));

			int noOfNights = roomRate.getPricing().getNightly().size();
			List<NightlyPrice> stayPriceList = roomRate.getPricing().getStay();
			double stayTaxAndServiceFee = 0.0;
			double stayExtraPersonFee = 0.0;
			double staySalesTax = 0.0;
			double stayPropertyFee = 0.0;
			if (CollectionUtils.isNotEmpty(stayPriceList)) {
				for (NightlyPrice stayPrice : stayPriceList) {
					if (stayPrice.getType().equals("tax_and_service_fee")) {
						stayTaxAndServiceFee = Double.parseDouble(stayPrice.getValue());
					}

					if (stayPrice.getType().equals("extra_person_fee")) {
						stayExtraPersonFee = Double.parseDouble(stayPrice.getValue());
					}

					if (stayPrice.getType().equals("sales_tax")) {
						staySalesTax = Double.parseDouble(stayPrice.getValue());
					}

					if (stayPrice.getType().equals("property_fee")) {
						stayPropertyFee = Double.parseDouble(stayPrice.getValue());
					}
				}
			}

			
			int day = 1;
			Double totalRoomPrice = 0.0;
			List<PriceInfo> priceInfoList = new ArrayList<>();
			List<List<NightlyPrice>> priceListOfList = roomRate.getPricing().getNightly();
			for (List<NightlyPrice> priceList : priceListOfList) {
				double totalPerNightPrice = 0.0;
				double taxAndServiceFeePrice = 0.0;
				double salesTax = 0.0;
				double extraPersonFee = 0.0;
				double propertyFee = 0.0;
				for (NightlyPrice nightlyPrice : priceList) {
					if (nightlyPrice.getType().equals("base_rate")) {
						totalPerNightPrice += Double.parseDouble(nightlyPrice.getValue());
					}

					if (nightlyPrice.getType().equals("tax_and_service_fee")) {
						taxAndServiceFeePrice += Double.parseDouble(nightlyPrice.getValue());
					}

					if (nightlyPrice.getType().equals("sales_tax")) {
						salesTax += Double.parseDouble(nightlyPrice.getValue());
					}

					if (nightlyPrice.getType().equals("extra_person_fee")) {
						extraPersonFee += Double.parseDouble(nightlyPrice.getValue());
					}

					if (nightlyPrice.getType().equals("property_fee")) {
						propertyFee += Double.parseDouble(nightlyPrice.getValue());
					}
				}
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, totalPerNightPrice);

				if (salesTax > 0 || staySalesTax > 0)
					fareComponents.put(HotelFareComponent.ST, salesTax + (staySalesTax / noOfNights));

				if (taxAndServiceFeePrice > 0 || stayTaxAndServiceFee > 0)
					fareComponents.put(HotelFareComponent.TSF,
							taxAndServiceFeePrice + (stayTaxAndServiceFee / noOfNights));

				if (extraPersonFee > 0 || stayExtraPersonFee > 0)
					fareComponents.put(HotelFareComponent.EPF, extraPersonFee + (stayExtraPersonFee / noOfNights));

				if (propertyFee > 0 || stayPropertyFee > 0)
					fareComponents.put(HotelFareComponent.PRF, propertyFee + (stayPropertyFee / noOfNights));

				if (importBookingParams.getMarketingFees() > 0) {
					fareComponents.put(HotelFareComponent.TMF, importBookingParams.getMarketingFees());
				}

				priceInfo.setDay(day);
				priceInfo.setFareComponents(fareComponents);
				totalRoomPrice += totalPerNightPrice;
				priceInfoList.add(priceInfo);
				day++;
			}
			List<TravellerInfo> travellerInfos = new ArrayList<>();
			TravellerInfo travellerInfo = new TravellerInfo();
			travellerInfo.setTitle("");
			travellerInfo.setPaxType(PaxType.ADULT);
			travellerInfo.setFirstName(room.getGiven_name());
			travellerInfo.setLastName(room.getFamily_name());
			travellerInfos.add(travellerInfo);
			roomInfo.setTravellerInfo(travellerInfos);
			roomInfo.setTotalPrice(totalRoomPrice);
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomInfo.setNumberOfAdults(room.getNumber_of_adults());
			roomInfo.setNumberOfChild(
					CollectionUtils.isNotEmpty(room.getChild_ages()) ? room.getChild_ages().size() : 0);

			List<Long> importedAmenities = roomRate.getAmenities();
			if (CollectionUtils.isEmpty(importedAmenities)) {
				roomInfo.setMealBasis(HotelConstants.ROOM_ONLY);
				roomInfo.setMiscInfo(RoomMiscInfo.builder().build());
			} else {
				List<String> amenitiesInStr =
						importedAmenities.stream().map(Object::toString).collect(Collectors.toList());
				roomInfo.setMiscInfo(RoomMiscInfo.builder().amenities(amenitiesInStr).build());
				amenities.addAll(amenitiesInStr);
			}

			roomInfoList.add(roomInfo);
		}
		return roomInfoList;
	}
}
