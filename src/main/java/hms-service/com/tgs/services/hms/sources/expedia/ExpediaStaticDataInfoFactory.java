package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;

@Service
public class ExpediaStaticDataInfoFactory extends AbstractStaticDataInfoFactory {

	public ExpediaStaticDataInfoFactory(HotelSupplierConfiguration supplierConf, HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	public void getCityMappingData() throws IOException {
		ExpediaStaticDataInfoService expediaStaticDataInfoService = ExpediaStaticDataInfoService.builder()
				.sourceConfig(this.sourceConfigOutput).supplierConf(supplierConf).staticDataRequest(this.staticDataRequest).build();
		expediaStaticDataInfoService.init();
		expediaStaticDataInfoService.getRegionWiseCityMapping();
	}

	@Override
	protected void getHotelStaticData() throws IOException {
		ExpediaStaticDataInfoService expediaStaticDataInfoService = ExpediaStaticDataInfoService.builder()
				.sourceConfig(this.sourceConfigOutput).supplierConf(supplierConf).staticDataRequest(this.staticDataRequest).build();
		expediaStaticDataInfoService.init();
		expediaStaticDataInfoService.handleHotelStaticContent();
		
	}
}