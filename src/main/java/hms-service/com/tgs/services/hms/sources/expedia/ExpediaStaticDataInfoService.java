package com.tgs.services.hms.sources.expedia;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.gms.datamodel.CountryInfo;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.CityInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelCityMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.SupplierCityInfo;
import com.tgs.services.hms.datamodel.expedia.Descriptions;
import com.tgs.services.hms.datamodel.expedia.PreparePropertyCatalogRequest;
import com.tgs.services.hms.datamodel.expedia.PropertyCatalog;
import com.tgs.services.hms.datamodel.expedia.RegionWiseDataRequest;
import com.tgs.services.hms.datamodel.expedia.RegionWiseDataResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelCityInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.manager.mapping.HotelSupplierCityInfoManager;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Getter
public class ExpediaStaticDataInfoService {

	private HotelInfoSaveManager hotelInfoSaveManager;
	private HotelCityInfoSaveManager cityInfoSaveManager;
	private HotelSupplierCityInfoManager supplierCityInfoManager;
	private HotelCityInfoMappingManager cityInfoMappingManager;
	private GeneralServiceCommunicator gsCommunicator;
	private HotelSourceConfigOutput sourceConfig;
	private HotelStaticDataRequest staticDataRequest;
	private HotelSupplierConfiguration supplierConf;

	private static final String PROPERTY_CATALOG_SUFFIX = "/properties/content";
	private static final String REGION_WISE_CITY_MAPPING_SUFFIX = "/regions";

	private static ExecutorService executor = Executors.newFixedThreadPool(20);


	public void init() {

		hotelInfoSaveManager =
				(HotelInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelInfoSaveManager");

		cityInfoSaveManager =
				(HotelCityInfoSaveManager) SpringContext.getApplicationContext().getBean("hotelCityInfoSaveManager");

		supplierCityInfoManager = (HotelSupplierCityInfoManager) SpringContext.getApplicationContext()
				.getBean("hotelSupplierCityInfoManager");

		cityInfoMappingManager = (HotelCityInfoMappingManager) SpringContext.getApplicationContext()
				.getBean("hotelCityInfoMappingManager");

		gsCommunicator = (GeneralServiceCommunicator) SpringContext.getApplicationContext()
				.getBean("generalServiceCommunicatorImpl");


	}

	@SuppressWarnings({"unchecked", "serial"})
	public void getRegionWiseCityMapping() throws IOException {
		RegionWiseDataRequest regionWiseDataRequest = createRequestToFetchRegionWiseCityMapping();
		HttpUtilsV2 httpUtils = ExpediaUtils.getResponseURL(regionWiseDataRequest, sourceConfig, supplierConf);
		httpUtils.setPrintResponseLog(false);
		ArrayList<RegionWiseDataResponse> regionWiseDataResponse = (ArrayList<RegionWiseDataResponse>) httpUtils
				.getResponseFromType(new TypeToken<ArrayList<RegionWiseDataResponse>>() {}.getType()).get();
		List<HotelCityMapping> cityInfoMappingList = createCityMappingResponse(regionWiseDataResponse);
		setCountryNameFromCountryCode(cityInfoMappingList);
		saveOrUpdateCityInfo(cityInfoMappingList);
		handleRestRegionWiseCityMapping(httpUtils.getResponseHeaderParams());
	}

	@SuppressWarnings({"serial"})
	private void handleRestRegionWiseCityMapping(Map<String, List<String>> headerParams) throws IOException {
		int i = 0;
		int retryCount = 0;
		HttpUtilsV2 httpUtils = null;
		try {
		while (ObjectUtils.isEmpty(sourceConfig.getCityHitCount())
				|| (!ObjectUtils.isEmpty(sourceConfig.getCityHitCount()) && i < sourceConfig.getCityHitCount())) {
			try {
				List<String> listOfLink = headerParams.get("Link");
				if (CollectionUtils.isEmpty(listOfLink))
					break;
				String link = listOfLink.get(0);
				link = link.substring(1, link.indexOf(">"));
				Map<String, String> headerParam = new HashMap<>();
				headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
				headerParam.put("Accept", "application/json");
				httpUtils = HttpUtilsV2.builder().headerParams(headerParam).urlString(link).build();
				httpUtils.setPrintResponseLog(false);
				httpUtils.getResponseFromType(new TypeToken<ArrayList<RegionWiseDataResponse>>() {}.getType());
				String responseString = httpUtils.getResponseString();
				try {
					ArrayList<RegionWiseDataResponse> regionWiseDataResponse = GsonUtils.getGson()
							.fromJson(responseString, new TypeToken<ArrayList<RegionWiseDataResponse>>() {}.getType());
					List<HotelCityMapping> cityInfoMappingList = createCityMappingResponse(regionWiseDataResponse);
					setCountryNameFromCountryCode(cityInfoMappingList);
					saveOrUpdateCityInfo(cityInfoMappingList);
					headerParams = httpUtils.getResponseHeaderParams();
					/*
					 * Temporary Logs
					 */
					log.info("No of cities fetched are {}", i * 250);
					retryCount = 0;
					i++;
				} catch (Exception e) {
					RegionWiseDataResponse errorInPropertyCatalog =
							GsonUtils.getGson().fromJson(responseString, RegionWiseDataResponse.class);
					if (errorInPropertyCatalog.getType().equals("unknown_internal_error")) {
						retryCount++;
					}
					log.info("Unable to fetch complete city static dump due to type {} and message {}",
							errorInPropertyCatalog.getType(), errorInPropertyCatalog.getMessage(), e);
					if (retryCount > 3)
						break;
				}
			} catch (IOException e) {
				log.error("Unable to fetch hotel city mapping for url {}, headers {}, link {}",
						httpUtils.getUrlString(), httpUtils.getHeaderParams(), headerParams, e);
				}
			}
		} finally {
			log.info("Total no of city fetched are {}, final response {}, headers {}", i * 250,
					httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
		}
	}

	@SuppressWarnings({"unchecked", "serial"})
	public void handleHotelStaticContent() throws IOException {
		PreparePropertyCatalogRequest preparePropertyCatalogRequest = createRequestToPreparePropertyCatalogURL();
		HttpUtilsV2 httpUtils = ExpediaUtils.getResponseURL(preparePropertyCatalogRequest, sourceConfig, supplierConf);

		if (CollectionUtils.isNotEmpty(staticDataRequest.getAddlParams())) {
			MultiMap propertyIdMap = getCountryCodes(staticDataRequest.getAddlParams());
			httpUtils.getRecurringQueryParams().putAll(propertyIdMap);
		}
		httpUtils.setPrintResponseLog(false);
		httpUtils.getResponseFromType(new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
		String responseString = httpUtils.getResponseString();
		try {
			Map<String, PropertyCatalog> propertyCatalogs = GsonUtils.getGson().fromJson(responseString,
					new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
			List<HotelInfo> hotelInfos = convertPropertyCatalogsIntoHotelInfos(propertyCatalogs);
			saveOrUpdateHotelInfos(hotelInfos);
			handleRestHotelStaticContent(httpUtils.getResponseHeaderParams());
		} catch (Exception e) {
			PropertyCatalog errorInPropertyCatalog =
					GsonUtils.getGson().fromJson(responseString, PropertyCatalog.class);
			log.info("Unable to fetch complete hotel static dump due to type {} and message {}",
					errorInPropertyCatalog.getType(), errorInPropertyCatalog.getMessage(), e);
		}
	}

	@SuppressWarnings({"unchecked", "serial"})
	private void handleRestHotelStaticContent(Map<String, List<String>> headerParams) throws IOException {
		int i = 1;
		String link = "";
		HttpUtilsV2 httpUtils = null;
		int retryCount = 0;
		try {
			while (ObjectUtils.isEmpty(sourceConfig.getHotelHitCount())
					|| (!ObjectUtils.isEmpty(sourceConfig.getHotelHitCount()) && i < sourceConfig.getHotelHitCount())) {
				try {

					List<String> listOfLink = headerParams.get("Link");
					if (CollectionUtils.isEmpty(listOfLink))
						break;
					link = listOfLink.get(0);
					link = link.substring(1, link.indexOf(">"));
					Map<String, String> headerParam = new HashMap<>();
					headerParam.put("Authorization", ExpediaUtils.getAuthorizationSignature(supplierConf));
					headerParam.put("Accept", "application/json");
					httpUtils = HttpUtilsV2.builder().headerParams(headerParam).urlString(link).build();
					httpUtils.setPrintResponseLog(false);
					httpUtils.getResponseFromType(new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
					String responseString = httpUtils.getResponseString();
					try {
						Map<String, PropertyCatalog> propertyCatalogs = GsonUtils.getGson().fromJson(responseString,
								new TypeToken<HashMap<String, PropertyCatalog>>() {}.getType());
						List<HotelInfo> hotelInfos = convertPropertyCatalogsIntoHotelInfos(propertyCatalogs);
						saveOrUpdateHotelInfos(hotelInfos);
						headerParams = httpUtils.getResponseHeaderParams();
						/*
						 * Temporary Logs
						 */
						log.info("No of hotels fetched are {}", i * 250);
						retryCount = 0;
						i++;
					} catch (Exception e) {
						PropertyCatalog errorInPropertyCatalog =
								GsonUtils.getGson().fromJson(responseString, PropertyCatalog.class);
						if (errorInPropertyCatalog.getType().equals("unknown_internal_error")) {
							retryCount++;
						}
						log.info("Unable to fetch complete hotel static dump due to type {} and message {}",
								errorInPropertyCatalog.getType(), errorInPropertyCatalog.getMessage(), e);

						if (retryCount > 3)
							break;
					}
				} catch (IOException e) {
					log.error("Unable to fetch hotel mapping for url {}, headers {}, link {}", httpUtils.getUrlString(),
							httpUtils.getHeaderParams(), headerParams, e);
				}
			}
		} finally {
			log.info("Total no of hotels fetched are {}, final response {}, headers {}", i * 250,
					httpUtils.getResponseString(), httpUtils.getResponseHeaderParams());
		}
	}

	private List<HotelCityMapping> createCityMappingResponse(List<RegionWiseDataResponse> regionWiseDataResponse) {

		List<HotelCityMapping> hotelCityMapping = new ArrayList<>();
		regionWiseDataResponse.forEach(region -> {
			if (region.getType().equals("city") || region.getType().equals("province_state")) {
				HotelCityMapping cityMapping = HotelCityMapping.builder().build();
				cityMapping.setCityName(region.getName());
				cityMapping.setSupplierCityId(region.getId());
				cityMapping.setCountryCode(region.getCountry_code());
				hotelCityMapping.add(cityMapping);
			}
		});
		return hotelCityMapping;
	}

	private List<HotelInfo> convertPropertyCatalogsIntoHotelInfos(Map<String, PropertyCatalog> propertyContent) {
		List<HotelInfo> hotelInfos = new ArrayList<>();

		for (Map.Entry<String, PropertyCatalog> propertyContentMap : propertyContent.entrySet()) {
			try {
				PropertyCatalog propertyCatalog = propertyContentMap.getValue();
				CountryInfo countryInfo = getCountryInfo(propertyCatalog.getAddress().getCountry_code());
				String countryName = !ObjectUtils.isEmpty(countryInfo) ? countryInfo.getName()
						: propertyCatalog.getAddress().getCountry_code() + "--Unknown";
				Address address = Address.builder().addressLine1(propertyCatalog.getAddress().getLine_1())
						.addressLine2(propertyCatalog.getAddress().getLine_2())
						.city(City.builder().name(propertyCatalog.getAddress().getCity()).build())
						.state(State.builder().name(propertyCatalog.getAddress().getState_province_name()).build())
						.country(Country.builder().name(countryName)
								.code(propertyCatalog.getAddress().getCountry_code()).build())
						.postalCode(propertyCatalog.getAddress().getPostal_code()).build();
				GeoLocation geolocation = GeoLocation.builder()
						.latitude(String.valueOf(propertyCatalog.getLocation().getCoordinates().getLatitude()))
						.longitude(String.valueOf(propertyCatalog.getLocation().getCoordinates().getLongitude()))
						.build();


				List<Image> images = new ArrayList<>();
				if (CollectionUtils.isNotEmpty(propertyCatalog.getImages())) {
					propertyCatalog.getImages().forEach(image -> {
						Image img = Image.builder().build();

						if (image.getLinks().get("350px") != null) {
							img.setBigURL(image.getLinks().get("350px").getHref());
						}

						if (image.getLinks().get("70px") != null) {
							img.setThumbnail(image.getLinks().get("70px").getHref());
						}
						images.add(img);
					});
				}

				// temp_logs, star rating should be in string rather than integer
				HotelInfo hotelInfo = HotelInfo.builder().id(propertyCatalog.getProperty_id())
						.name(propertyCatalog.getName()).propertyType(propertyCatalog.getCategory().getName())
						.address(address).images(images).geolocation(geolocation).build();

				if (MapUtils.isNotEmpty(propertyCatalog.getAmenities())) {
					List<String> facilities = new ArrayList<>();
					propertyCatalog.getAmenities().forEach((id, amenity) -> {
						facilities.add(amenity.getName());
					});
					hotelInfo.setFacilities(facilities);
				}

				if (!ObjectUtils.isEmpty(propertyCatalog.getRatings())
						&& !ObjectUtils.isEmpty(propertyCatalog.getRatings().getProperty())) {
					hotelInfo.setRating(
							(int) Double.parseDouble(propertyCatalog.getRatings().getProperty().getRating()));
				} else {
					hotelInfo.setRating(null);
				}

				if (!ObjectUtils.isEmpty(propertyCatalog.getCheckin())) {
					populateCheckinInstructions(propertyCatalog, hotelInfo);
				}

				if (!ObjectUtils.isEmpty(propertyCatalog.getDescriptions())) {
					hotelInfo.setDescription(createDescription(propertyCatalog.getDescriptions()));
				}

				hotelInfos.add(hotelInfo);
			} catch (Exception e) {
				log.info("Unable to convert property catalog into hotel info {}",
						GsonUtils.getGson().toJson(propertyContentMap.getValue()), e);

			}
		}

		return hotelInfos;
	}

	private void populateCheckinInstructions(PropertyCatalog propertyCatalog, HotelInfo hotelInfo) {

		List<Instruction> instructions = new ArrayList<>();

		if (StringUtils.isNotBlank(propertyCatalog.getCheckin().getInstructions())) {
			instructions.add(Instruction.builder().type(InstructionType.BOOKING_NOTES)
					.msg(propertyCatalog.getCheckin().getInstructions().replaceAll("\\<.*?\\>", "")).build());
		}

		if (StringUtils.isNotBlank(propertyCatalog.getCheckin().getSpecial_instructions())) {
			instructions.add(Instruction.builder().type(InstructionType.SPECIAL_INSTRUCTIONS)
					.msg(propertyCatalog.getCheckin().getSpecial_instructions().replaceAll("\\<.*?\\>", "")).build());
		}

		if (!ObjectUtils.isEmpty(propertyCatalog.getFees())) {
			if (StringUtils.isNotBlank(propertyCatalog.getFees().getMandatory())) {
				instructions.add(Instruction.builder().type(InstructionType.MANDATORY_FEES)
						.msg(propertyCatalog.getFees().getMandatory().replaceAll("\\<.*?\\>", "")).build());
			}

			if (StringUtils.isNotBlank(propertyCatalog.getFees().getOptional())) {
				instructions.add(Instruction.builder().type(InstructionType.OPTIONAL_FEES)
						.msg(propertyCatalog.getFees().getOptional().replaceAll("\\<.*?\\>", "")).build());
			}
		}

		if (!ObjectUtils.isEmpty(propertyCatalog.getPolicies())
				&& StringUtils.isNotBlank(propertyCatalog.getPolicies().getKnow_before_you_go())) {
			instructions.add(Instruction.builder().type(InstructionType.KNOW_BEFORE_YOU_GO)
					.msg(propertyCatalog.getPolicies().getKnow_before_you_go().replaceAll("\\<.*?\\>", "")).build());
		}

		hotelInfo.setInstructions(instructions);
	}


	private String createDescription(Descriptions descriptions) {

		StringBuilder builder = new StringBuilder();
		if (!ObjectUtils.isEmpty(descriptions.getHeadline())) {
			builder.append(descriptions.getHeadline());
		}
		if (!ObjectUtils.isEmpty(descriptions.getLocation())) {
			builder.append(descriptions.getLocation());
		}
		if (!ObjectUtils.isEmpty(descriptions.getAmenities())) {
			builder.append(descriptions.getAmenities());
		}
		if (!ObjectUtils.isEmpty(descriptions.getAttractions())) {
			builder.append(descriptions.getAttractions());
		}
		return builder.toString().replaceAll("\\<.*?\\>", "");
	}

	private void saveOrUpdateHotelInfos(List<HotelInfo> hotelInfos) {
		for (HotelInfo hotelInfo : hotelInfos) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hotelInfo);
				dbHotelInfo.setId(null);
				String supplierName = HotelSourceType.EXPEDIA.name();
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hotelInfo.getId(), supplierName, HotelSourceType.EXPEDIA);
			} catch (Exception e) {
				log.info("Error while processing hotel {} due to {} ", hotelInfo.getId(), e.getMessage());
			}
		}
	}

	private void saveOrUpdateCityInfo(List<HotelCityMapping> cityMappings) {

		if (BooleanUtils.isTrue(staticDataRequest.getIsMasterData())) {
			supplierCityInfoManager.saveSupplierCityInfo(convertIntoSupplierCityInfo(cityMappings));
		} else {
			for (HotelCityMapping cityMapping : cityMappings) {
				executor.submit(() -> {
					CityInfo cityInfo = getCityInfo(cityMapping);
					Long cityId = null;
					if ((cityId = cityInfoSaveManager.isCityExists(cityInfo)) == null)
						return;
					CityInfoMapping cityInfoMapping = getCityInfoMapping(cityMapping);
					cityInfoMappingManager.saveCityMapping(cityInfoMapping, cityId);
				});
			}
		}
	}

	private CityInfo getCityInfo(HotelCityMapping cityMapping) {
		return CityInfo.builder().cityName(cityMapping.getCityName()).countryName(cityMapping.getCountryName()).build();
	}

	private CityInfoMapping getCityInfoMapping(HotelCityMapping cityMapping) {
		return CityInfoMapping.builder().supplierName(HotelSourceType.EXPEDIA.name())
				.supplierCity(cityMapping.getSupplierCityId()).build();
	}

	private RegionWiseDataRequest createRequestToFetchRegionWiseCityMapping() {
		return RegionWiseDataRequest.builder().suffixOfURL(REGION_WISE_CITY_MAPPING_SUFFIX)
				.include(sourceConfig.getCityInfoGranularity()).language(staticDataRequest.getLanguage()).build();
	}

	private PreparePropertyCatalogRequest createRequestToPreparePropertyCatalogURL() {
		return PreparePropertyCatalogRequest.builder().suffixOfURL(PROPERTY_CATALOG_SUFFIX)
				.language(staticDataRequest.getLanguage()).build();
	}

	private MultiMap getCountryCodes(List<String> countryIds) {

		MultiMap countryIdMap = new MultiValueMap();
		for (String countryId : countryIds) {
			countryIdMap.put("country_code", countryId);
		}
		return countryIdMap;
	}

	private void setCountryNameFromCountryCode(List<HotelCityMapping> cityMappingList) {
		for (HotelCityMapping cityMapping : cityMappingList) {
			CountryInfo country = getCountryInfo(cityMapping.getCountryCode());
			if (!ObjectUtils.isEmpty(country)) {
				cityMapping.setCountryName(country.getCountry());
			}
		}
	}

	private CountryInfo getCountryInfo(String countryCode) {
		CountryInfo country = gsCommunicator.getCountryInfo(countryCode);
		return country;
	}

	private List<SupplierCityInfo> convertIntoSupplierCityInfo(List<HotelCityMapping> cityMappings) {

		List<SupplierCityInfo> supplierCityInfos = new ArrayList<>();
		cityMappings.forEach(cityMapping -> {

			SupplierCityInfo supplierCityInfo = SupplierCityInfo.builder().cityId(cityMapping.getSupplierCityId())
					.cityName(cityMapping.getCityName()).countryId(cityMapping.getSupplierCountryId())
					.countryName(cityMapping.getCountryName()).supplierName(HotelSourceType.EXPEDIA.name()).build();
			supplierCityInfos.add(supplierCityInfo);
		});
		return supplierCityInfos;
	}
}
