package com.tgs.services.hms.sources.expedia;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.collections.map.MultiValueMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.expedia.CancelPenalties;
import com.tgs.services.hms.datamodel.expedia.CurrencyParams;
import com.tgs.services.hms.datamodel.expedia.ExpediaBaseRequest;
import com.tgs.services.hms.datamodel.expedia.ExpediaCancellationDeadline;
import com.tgs.services.hms.datamodel.expedia.ExpediaRetrieveBookingRequest;
import com.tgs.services.hms.datamodel.expedia.NightlyPrice;
import com.tgs.services.hms.datamodel.expedia.OccupancyPricing;
import com.tgs.services.hms.datamodel.expedia.OccupancyPricingTotal;
import com.tgs.services.hms.datamodel.expedia.ValueAndCurrency;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelConstants;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExpediaUtils {

	static HotelCacheHandler cacheHandler;
	static GeneralServiceCommunicator gnComm;

	public static void init(HotelCacheHandler cacheHandler, GeneralServiceCommunicator gnComm) {
		ExpediaUtils.cacheHandler = cacheHandler;
		ExpediaUtils.gnComm = gnComm;
	}

	public static String getAuthorizationSignature(HotelSupplierConfiguration supplierConfig) {
		Date date = new java.util.Date();
		Long timestamp = (date.getTime() / 1000);
		String signature = null;
		try {
			String toBeHashed = supplierConfig.getHotelSupplierCredentials().getUserName()
					+ supplierConfig.getHotelSupplierCredentials().getPassword() + timestamp;
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			byte[] bytes = md.digest(toBeHashed.getBytes("UTF-8"));
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			signature = sb.toString();
		} catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
			log.error("Unable to generate signature", e);
		}
		String authHeaderValue = "EAN APIKey=" + supplierConfig.getHotelSupplierCredentials().getUserName()
				+ ",Signature=" + signature + ",timestamp=" + timestamp;
		return authHeaderValue;
	}

	@SuppressWarnings("unchecked")
	public static <T> HttpUtilsV2 getResponseURL(ExpediaBaseRequest request, HotelSourceConfigOutput sourceConfigOutput,
			HotelSupplierConfiguration supplierConfig) {
		if (sourceConfigOutput.isGzipEnabled()) {
			request.setGzipHeader();
		}
		request.setAuthHeader(getAuthorizationSignature(supplierConfig));
		Map<String, String> queryParams = convertToMap(request);
		MultiMap recurringQueryParams = new MultiValueMap();
		recurringQueryParams.putAll(queryParams);
		HttpUtilsV2 httpUtils = HttpUtilsV2.builder()
				.urlString(supplierConfig.getHotelSupplierCredentials().getUrl()
						+ sourceConfigOutput.getHotelUrls().get(0) + request.getSuffixOfURL())
				.recurringQueryParams(recurringQueryParams).headerParams(request.getHeaderParams())
				.timeout(ExpediaConstants.TIMEOUT).build();
		return httpUtils;
	}

	public static MultiMap getOccupancies(HotelSearchQuery searchQuery) {
		MultiMap propertyIdMap = new MultiValueMap();
		List<RoomSearchInfo> roomInfos = searchQuery.getRoomInfo();
		for (RoomSearchInfo roomSearchInfo : roomInfos) {
			StringBuilder occupancy = new StringBuilder();
			occupancy.append(roomSearchInfo.getNumberOfAdults());
			if (CollectionUtils.isNotEmpty(roomSearchInfo.getChildAge())) {
				occupancy.append("-").append(
						roomSearchInfo.getChildAge().stream().map(String::valueOf).collect(Collectors.joining(",")));
			}
			propertyIdMap.put("occupancy", occupancy.toString());
		}
		return propertyIdMap;
	}

	public static Map<String, Integer> getOccupancyMap(List<String> occupancyList) {
		Map<String, Integer> occupancyMap = new HashMap<>();
		for (String occupancy : occupancyList) {
			occupancyMap.put(occupancy, occupancyMap.getOrDefault(occupancy, 0) + 1);
		}
		return occupancyMap;
	}

	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

	public static void setRoomPricing(Option option, RoomInfo roomInfo, Map<String, OccupancyPricing> occupancies,
			Map<String, Integer> occupancyCountMap) {

		List<RoomInfo> roomInfoList = new ArrayList<>();
		Map<String, Double> taxInstructions = new HashMap<>();
		String taxCurrency = "INR";
		for (Map.Entry<String, OccupancyPricing> occupancyPricingMap : occupancies.entrySet()) {
			RoomInfo copyRoomInfo =
					new GsonMapper<>(roomInfo == null ? new RoomInfo() : roomInfo, RoomInfo.class).convert();
			copyRoomInfo.setOccupancyPattern(occupancyPricingMap.getKey());
			Integer noOfRooms = occupancyCountMap.get(occupancyPricingMap.getKey());
			OccupancyPricing occupancyPricing = occupancyPricingMap.getValue();
			int noOfNights = occupancyPricing.getNightly().size();
			double stayTaxAndServiceFee = 0.0;
			double stayExtraPersonFee = 0.0;
			double staySalesTax = 0.0;
			double stayPropertyFee = 0.0;
			if (CollectionUtils.isNotEmpty(occupancyPricing.getStay())) {
				for (NightlyPrice stayPrice : occupancyPricing.getStay()) {
					if (stayPrice.getType().equals("tax_and_service_fee")) {
						stayTaxAndServiceFee = Double.parseDouble(stayPrice.getValue());
					}

					if (stayPrice.getType().equals("extra_person_fee")) {
						stayExtraPersonFee = Double.parseDouble(stayPrice.getValue());
					}

					if (stayPrice.getType().equals("sales_tax")) {
						staySalesTax = Double.parseDouble(stayPrice.getValue());
					}

					if (stayPrice.getType().equals("property_fee")) {
						stayPropertyFee = Double.parseDouble(stayPrice.getValue());
					}
				}
			}

			int day = 1;
			Double totalRoomPrice = 0.0;
			List<PriceInfo> priceInfoList = new ArrayList<>();
			List<List<NightlyPrice>> priceListOfList = occupancyPricing.getNightly();
			for (List<NightlyPrice> priceList : priceListOfList) {
				double totalPerNightPrice = 0.0;
				double taxAndServiceFeePrice = 0.0;
				double extraPersonFee = 0.0;
				double salesTax = 0.0;
				double propertyFee = 0.0;
				for (NightlyPrice nightlyPrice : priceList) {
					if (nightlyPrice.getType().equals("base_rate")) {
						totalPerNightPrice += Double.parseDouble(nightlyPrice.getValue());
					}

					if (nightlyPrice.getType().equals("tax_and_service_fee")) {
						taxAndServiceFeePrice += (Double.parseDouble(nightlyPrice.getValue()));
					}

					if (nightlyPrice.getType().equals("extra_person_fee")) {
						extraPersonFee += (Double.parseDouble(nightlyPrice.getValue()));
					}

					if (nightlyPrice.getType().equals("sales_tax")) {
						salesTax += (Double.parseDouble(nightlyPrice.getValue()));
					}

					if (nightlyPrice.getType().equals("property_fee")) {
						propertyFee += (Double.parseDouble(nightlyPrice.getValue()));
					}
				}
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, totalPerNightPrice);
				if (salesTax > 0 || staySalesTax > 0)
					fareComponents.put(HotelFareComponent.ST, salesTax + (staySalesTax / noOfNights));

				if (taxAndServiceFeePrice > 0 || stayTaxAndServiceFee > 0)
					fareComponents.put(HotelFareComponent.TSF,
							taxAndServiceFeePrice + (stayTaxAndServiceFee / noOfNights));

				if (extraPersonFee > 0 || stayExtraPersonFee > 0)
					fareComponents.put(HotelFareComponent.EPF, extraPersonFee + (stayExtraPersonFee / noOfNights));

				if (propertyFee > 0 || stayPropertyFee > 0)
					fareComponents.put(HotelFareComponent.PRF, propertyFee + (stayPropertyFee / noOfNights));

				priceInfo.setDay(day);
				priceInfo.setFareComponents(fareComponents);
				totalRoomPrice += totalPerNightPrice;
				priceInfoList.add(priceInfo);
				day++;
			}
			copyRoomInfo.setPerNightPriceInfos(priceInfoList);


			OccupancyPricingTotal occupancyPricingTotal = occupancyPricing.getTotals();
			if (!ObjectUtils.isEmpty(occupancyPricingTotal.getMarketing_fee())) {
				CurrencyParams currencyParams = occupancyPricingTotal.getMarketing_fee();
				ValueAndCurrency valueAndCurrency = currencyParams.getRequest_currency();
				int totalNights = copyRoomInfo.getPerNightPriceInfos().size();
				double perNightMarketingFee = Double.parseDouble(valueAndCurrency.getValue()) / totalNights;
				for (PriceInfo priceInfo : copyRoomInfo.getPerNightPriceInfos()) {
					priceInfo.getFareComponents().put(HotelFareComponent.TMF, perNightMarketingFee);
				}
			}

			if (!ObjectUtils.isEmpty(occupancyPricing.getFees())
					&& !ObjectUtils.isEmpty(occupancyPricing.getFees().getMandatory_tax())) {
				taxCurrency = occupancyPricing.getFees().getMandatory_tax().getRequest_currency().getCurrency();
				if (taxInstructions.containsKey("Mandatory Tax")) {
					taxInstructions.put("Mandatory Tax", taxInstructions.get("Mandatory Tax") + Double.parseDouble(
							occupancyPricing.getFees().getMandatory_tax().getRequest_currency().getValue()));
				} else {
					taxInstructions.put("Mandatory Tax", Double.parseDouble(
							occupancyPricing.getFees().getMandatory_tax().getRequest_currency().getValue()));
				}
			}

			if (!ObjectUtils.isEmpty(occupancyPricing.getFees())
					&& !ObjectUtils.isEmpty(occupancyPricing.getFees().getMandatory_fee())) {
				if (taxInstructions.containsKey("Mandatory Fee")) {
					taxInstructions.put("Mandatory Fee", taxInstructions.get("Mandatory Fee") + Double.parseDouble(
							occupancyPricing.getFees().getMandatory_fee().getRequest_currency().getValue()));
				} else {
					taxInstructions.put("Mandatory Fee", Double.parseDouble(
							occupancyPricing.getFees().getMandatory_fee().getRequest_currency().getValue()));
				}
			}

			if (!ObjectUtils.isEmpty(occupancyPricing.getFees())
					&& !ObjectUtils.isEmpty(occupancyPricing.getFees().getResort_fee())) {
				if (taxInstructions.containsKey("Resort Fee")) {
					taxInstructions.put("Resort Fee", taxInstructions.get("Resort Fee") + Double
							.parseDouble(occupancyPricing.getFees().getResort_fee().getRequest_currency().getValue()));
				} else {
					taxInstructions.put("Resort Fee", Double
							.parseDouble(occupancyPricing.getFees().getResort_fee().getRequest_currency().getValue()));
				}
			}

			copyRoomInfo.setTotalPrice(totalRoomPrice);

			String passengers = occupancyPricingMap.getKey();
			copyRoomInfo.setNumberOfAdults(
					Integer.parseInt(passengers.contains("-") ? passengers.split("-")[0] : passengers));
			copyRoomInfo.setNumberOfChild(passengers.contains("-") ? passengers.split("-")[1].split(",").length : 0);
			roomInfoList.add(copyRoomInfo);

			if (noOfRooms > 1) {
				IntStream.range(1, noOfRooms).forEach(roomCount -> {
					RoomInfo roomInfoWithSameOccupancy = new GsonMapper<>(copyRoomInfo, RoomInfo.class).convert();
					roomInfoList.add(roomInfoWithSameOccupancy);
				});
			}
		}
		for (Map.Entry<String, Double> taxInstruction : taxInstructions.entrySet()) {
			Instruction mandatoryTax = Instruction.builder().type(InstructionType.MANDATORY_TAX)
					.msg(String.valueOf(taxInstruction.getValue()) + " " + taxCurrency).build();
			option.getInstructions().add(mandatoryTax);
		}
		option.setRoomInfos(roomInfoList);
	}

	public static HotelCancellationPolicy getCancellationPolicy(HotelSearchQuery searchQuery, boolean isRefundable,
			List<CancelPenalties> cancelPenalties, double optionAmount, int totalNights,
			ExpediaCancellationDeadline cancellationDeadline, List<RoomInfo> roomInfos, Integer cancellationBuffer) {
		cancellationBuffer = ObjectUtils.isEmpty(cancellationBuffer) ? 24 : cancellationBuffer;
		LocalDateTime currentTime = LocalDateTime.now();
		LocalDate targetDate = searchQuery.getCheckinDate();
		List<PenaltyDetails> penaltyList = new ArrayList<PenaltyDetails>();
		Boolean isRefundAlreadyAllowed = false;
		Boolean isFullRefundAllowed = true;
		Boolean isNoRefundAllowed = null;
		boolean isFreeCancellationAllowed = true;
		if (isRefundable) {
			LocalDateTime initialDate = OffsetDateTime.parse(cancelPenalties.get(0).getStart())
					.withOffsetSameInstant(ZoneOffset.ofHoursMinutes(5, 30)).toLocalDateTime();
			LocalDateTime bufferedDate = initialDate.minus(cancellationBuffer, ChronoUnit.HOURS);
			if (bufferedDate.isAfter(currentTime)) {
				PenaltyDetails penaltyDetail =
						PenaltyDetails.builder().fromDate(currentTime).toDate(bufferedDate).penaltyAmount(0.0).build();
				penaltyList.add(penaltyDetail);
				isRefundAlreadyAllowed = true;
			} else if (bufferedDate.isBefore(initialDate)) {
				isFreeCancellationAllowed = false;
			}

			if (isFreeCancellationAllowed) {
				for (CancelPenalties cancelPenalty : cancelPenalties) {
					LocalDateTime startDate = OffsetDateTime.parse(cancelPenalty.getStart())
							.withOffsetSameInstant(ZoneOffset.ofHoursMinutes(5, 30)).toLocalDateTime()
							.minus(cancellationBuffer, ChronoUnit.HOURS);
					LocalDateTime endDate = OffsetDateTime.parse(cancelPenalty.getEnd())
							.withOffsetSameInstant(ZoneOffset.ofHoursMinutes(5, 30)).toLocalDateTime()
							.minus(cancellationBuffer, ChronoUnit.HOURS);

					Double penaltyAmount = null;
					if (StringUtils.isNotEmpty(cancelPenalty.getAmount())) {
						double cancellationAmount = Double.parseDouble(cancelPenalty.getAmount());
						if (optionAmount - cancellationAmount > 0.01) {
							if (!isRefundAlreadyAllowed)
								isFullRefundAllowed = false;
							else
								cancellationDeadline.setDeadlineDateTime(startDate);
						} else {
							cancellationDeadline.setDeadlineDateTime(startDate);
						}
						penaltyAmount = cancellationAmount;
					} else if (StringUtils.isNotEmpty(cancelPenalty.getPercent())) {
						if (!cancelPenalty.getPercent().equals("100%")) {
							if (!isRefundAlreadyAllowed)
								isFullRefundAllowed = false;
							else
								cancellationDeadline.setDeadlineDateTime(startDate);
						} else {
							cancellationDeadline.setDeadlineDateTime(startDate);
						}
						double percent = (int) Double.parseDouble(
								cancelPenalty.getPercent().substring(0, cancelPenalty.getPercent().indexOf("%")));
						penaltyAmount = (optionAmount * percent) / 100;
					} else if (StringUtils.isNotEmpty(cancelPenalty.getNights())) {
						Integer cancellationNights = Integer.parseInt(cancelPenalty.getNights());
						if (cancellationNights != totalNights) {
							if (!isRefundAlreadyAllowed)
								isFullRefundAllowed = false;
							else
								cancellationDeadline.setDeadlineDateTime(startDate);
						} else {
							cancellationDeadline.setDeadlineDateTime(startDate);
						}
						penaltyAmount = (optionAmount / totalNights) * cancellationNights;
					}

					if (!ObjectUtils.isEmpty(penaltyAmount)) {
						PenaltyDetails penaltyDetails = PenaltyDetails.builder().fromDate(startDate).toDate(endDate)
								.penaltyAmount(penaltyAmount).build();
						penaltyList.add(penaltyDetails);
					}
				}
			}
		} else {
			isFreeCancellationAllowed = false;
		}

		if (!isFreeCancellationAllowed) {
			cancellationDeadline.setDeadlineDateTime(currentTime);
			isFullRefundAllowed = false;
			isNoRefundAllowed = true;
			PenaltyDetails penaltyDetails = PenaltyDetails.builder().fromDate(currentTime.toLocalDate().atTime(12, 00))
					.toDate(targetDate.atTime(12, 00)).penaltyAmount(optionAmount).build();
			penaltyList.add(penaltyDetails);
		}

		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(penaltyList)
				.isNoRefundAllowed(isNoRefundAllowed)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();
		return cancellationPolicy;
	}

	public static void updatePriceWithClientCommissionComponents(Option option, HotelSourceConfigOutput sourceConfig) {
		double sRetentionCommission = ObjectUtils.isEmpty(sourceConfig.getSRetentionCommission()) ? 0.0
				: sourceConfig.getSRetentionCommission();
		double sAgentCommssion =
				ObjectUtils.isEmpty(sourceConfig.getSAgentCommssion()) ? 0.0 : sourceConfig.getSAgentCommssion();
		double supplierMarkup =
				ObjectUtils.isEmpty(sourceConfig.getSupplierMarkup()) ? 0.0 : sourceConfig.getSupplierMarkup();

		for (RoomInfo roomInfo : option.getRoomInfos()) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double totalMarketingFees = priceInfo.getFareComponents().getOrDefault(HotelFareComponent.TMF, -1.0);
				if (totalMarketingFees <= 0) {
					break;
				}
				double perNightSRetentionCommission = (totalMarketingFees * sRetentionCommission) / 100;
				double perNightSAgentCommssion = (totalMarketingFees * sAgentCommssion) / 100;
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.TMF, totalMarketingFees);
				fareComponents.put(HotelFareComponent.SRC, perNightSRetentionCommission);
				fareComponents.put(HotelFareComponent.SAC, perNightSAgentCommssion);
				double totalTax = priceInfo.getFareComponents().getOrDefault(HotelFareComponent.TSF, 0.0)
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.ST, 0.0)
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.EPF, 0.0)
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.PRF, 0.0);
				Double supplierGrossPrice =
						(priceInfo.getFareComponents().get(HotelFareComponent.BF) + totalTax) - perNightSAgentCommssion;
				fareComponents.put(HotelFareComponent.MUP, (supplierGrossPrice * supplierMarkup) / 100);
				Double supplierGrossPriceWithMarkup =
						supplierGrossPrice + fareComponents.getOrDefault(HotelFareComponent.MUP, 0.0);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPriceWithMarkup);

			}
		}
	}

	private static double getTotalTax(List<RoomInfo> roomInfos) {

		return roomInfos.stream().mapToDouble(roomInfo -> {
			return roomInfo.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				return priceInfo.getFareComponents().getOrDefault(HotelFareComponent.TSF, 0.0)
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.ST, 0.0);
			}).sum();
		}).sum();
	}

	public static void setMealBasis(List<HotelInfo> hotelInfos, Set<String> amenities, HotelSearchQuery searchQuery) {

		Map<String, String> mealBasis =
				ExpediaUtils.cacheHandler.fetchMealInfoFromAerospike(searchQuery, new ArrayList<>(amenities));

		hotelInfos.forEach(hInfo -> hInfo.getOptions().forEach(option -> {
			option.getRoomInfos().forEach(roomInfo -> {
				if (CollectionUtils.isNotEmpty(roomInfo.getMiscInfo().getAmenities())) {
					for (String amenity : roomInfo.getMiscInfo().getAmenities()) {
						if (!ObjectUtils.isEmpty(roomInfo.getMealBasis())
								&& !roomInfo.getMealBasis().equals(HotelConstants.ROOM_ONLY)) {
							return;
						}
						roomInfo.setMealBasis(mealBasis.getOrDefault(amenity.toUpperCase(), HotelConstants.ROOM_ONLY));
					}
				} else {
					roomInfo.setMealBasis("Room Only");
				}
			});
		}));
	}

	public static ExpediaRetrieveBookingRequest createRetrieveBookingRequest(DeliveryInfo deliveryInfo,
			String bookingId) {
		ClientGeneralInfo cInfo = ExpediaUtils.gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		ExpediaRetrieveBookingRequest retrieveBookingRequest =
				ExpediaRetrieveBookingRequest.builder().email(cInfo.getAltEmailIds().get(0))
						.suffixOfURL(ExpediaConstants.RETREIVE_BOOKING_SUFFIX + "/" + bookingId).build();

		return retrieveBookingRequest;
	}

	public static String getRoomId(String expediaRoomId, String roomRateId, String bedGroupId) {

		return String.join("_", expediaRoomId, roomRateId, bedGroupId);
	}

	public static String getRoomType(int numberOfAdults) {
		String roomType = "";
		switch (numberOfAdults) {
			case 1:
				roomType = ExpediaRoomCategoryConstant.SINGLE.getRoomType();
				break;
			case 2:
				roomType = ExpediaRoomCategoryConstant.DOUBLE.getRoomType();
				break;
			case 3:
				roomType = ExpediaRoomCategoryConstant.TRIPLE.getRoomType();
				break;
			case 4:
				roomType = ExpediaRoomCategoryConstant.QUADRUPLE.getRoomType();
				break;
			case 5:
				roomType = ExpediaRoomCategoryConstant.QUINTUPLE.getRoomType();
				break;
			default:
				return "Undefined Room Type";
		}
		return roomType;
	}
}
