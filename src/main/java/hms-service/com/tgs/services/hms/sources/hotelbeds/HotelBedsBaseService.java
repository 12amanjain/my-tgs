package com.tgs.services.hms.sources.hotelbeds;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelBeds.CancellationPolicy;
import com.tgs.services.hms.datamodel.hotelBeds.Hotel;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBaseResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Rate;
import com.tgs.services.hms.datamodel.hotelBeds.RoomResponse;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class HotelBedsBaseService {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfig;
	protected HttpUtilsV2 httpUtils;
	protected RestAPIListener listener;
	protected MoneyExchangeCommunicator moneyExchnageComm;
	protected HotelCacheHandler cacheHandler;
	protected String currency;

	protected double getAmountBasedOnCurrency(double amount, String fromCurrency) {

		String toCurrency = "INR";
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type(HotelSourceType.HOTELBEDS.name()).build();
		return moneyExchnageComm.getExchangeValue(amount, filter, true);
	}

	protected LocalDateTime getFormattedDate(String supplierDate) {

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ssXXX");
		return LocalDateTime.parse(supplierDate, formatter);
	}

	protected HotelCancellationPolicy getRoomCancellationPolicy(Rate rate, HotelSearchQuery searchQuery,
			RoomInfo rInfo) {

		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		List<PenaltyDetails> pds = new ArrayList<>();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		boolean isFullRefundAllowed = false;
		for (CancellationPolicy cnp : rate.getCancellationPolicies()) {
			if (pds.size() < 1) {
				LocalDateTime fromDate = getFormattedDate(cnp.getFrom());
				if (fromDate.isBefore(now)) {
					PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(now).toDate(checkInDate)
							.penaltyAmount(getAmountBasedOnCurrency(cnp.getAmount(), currency)).build();
					pds.add(penaltyDetails2);
				} else {
					PenaltyDetails penaltyDetails1 =
							PenaltyDetails.builder().fromDate(now).toDate(fromDate).penaltyAmount(0.0).build();
					isFullRefundAllowed = true;
					pds.add(penaltyDetails1);
					deadlineDateTime = fromDate;
					PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
							.penaltyAmount(getAmountBasedOnCurrency(cnp.getAmount(), currency)).build();
					pds.add(penaltyDetails2);
				}
			} else {
				LocalDateTime fromDate = getFormattedDate(cnp.getFrom());
				pds.get(pds.size() - 1).setToDate(fromDate);
				PenaltyDetails penaltyDetails = PenaltyDetails.builder().fromDate(fromDate).toDate(checkInDate)
						.penaltyAmount(getAmountBasedOnCurrency(cnp.getAmount(), currency)).build();
				pds.add(penaltyDetails);
			}
		}
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(rInfo.getId())
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds).miscInfo(CancellationMiscInfo.builder()
						.isBookingAllowed(true).isSoldOut(false).isCancellationPolicyBelongToRoom(true).build())
				.build();
		rInfo.setDeadlineDateTime(deadlineDateTime);
		return cancellationPolicy;
	}

	public void setCancellationDeadlineFromRooms(Option option) {

		LocalDateTime earliestDeadlineDateTime = null;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			if (earliestDeadlineDateTime == null)
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
		}
		option.setDeadlineDateTime(earliestDeadlineDateTime);
	}

	protected void populateRoomPerNightPrice(RoomInfo rInfo, Rate rate) {

		long numberOfNights = Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		double netPrice =
				rate.getHotelMandatory() != null && rate.getHotelMandatory() ? rate.getSellingRate() : rate.getNet();
		if (rate.getHotelMandatory() != null && rate.getHotelMandatory())
			netPrice = rate.getSellingRate();
		netPrice = rate.getNet();

		double perNightRoomPrice = getAmountBasedOnCurrency(netPrice / numberOfNights, currency);
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (int i = 0; i < numberOfNights; i++) {
			PriceInfo priceInfo = new PriceInfo();
			priceInfo.setDay(i);
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, perNightRoomPrice);
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		}
		rInfo.setPerNightPriceInfos(priceInfoList);
		rInfo.setTotalPrice(getAmountBasedOnCurrency(rate.getNet(), currency));
	}


	protected List<Option> createOptionFromHotel(Hotel hotelResponse) {

		List<Option> optionList = new ArrayList<>();
		currency = hotelResponse.getCurrency();
		Map<String, List<RoomInfo>> roomOccupancyKeyRoomInfosValues = getroomInfoMap(hotelResponse.getRooms());
		roomOccupancyKeyRoomInfosValues.entrySet().stream().forEach(entry -> {
			entry.getValue().stream().sorted((p1, p2) -> p1.getTotalPrice().compareTo(p2.getTotalPrice()))
					.collect(Collectors.toList());
		});

		while (!roomOccupancyKeyRoomInfosValues.isEmpty()) {
			List<RoomInfo> optionRoomInfos = new ArrayList<>();
			for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
				String key = getKeyFromSearchRoom(room);
				List<RoomInfo> roomList = roomOccupancyKeyRoomInfosValues.get(key);
				if (roomList != null && roomList.size() > 0) {
					optionRoomInfos.add(roomList.get(0));
					roomOccupancyKeyRoomInfosValues.get(key).remove(0);
				}
			}
			if (optionRoomInfos.size() == searchQuery.getRoomInfo().size()) {
				Option option = Option.builder().roomInfos(optionRoomInfos)
						.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
								.supplierHotelId(hotelResponse.getCode() + "")
								.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
						.id(RandomStringUtils.random(20, true, true)).build();
				setCancellationDeadlineFromRooms(option);
				optionList.add(option);
			} else
				break;
		}
		return optionList;
	}

	private String getKeyFromSearchRoom(RoomSearchInfo room) {

		StringBuilder key = new StringBuilder();
		key.append(room.getNumberOfAdults());
		key.append(room.getNumberOfChild() != null ? room.getNumberOfChild() : 0);
		if (room.getChildAge() != null && CollectionUtils.isNotEmpty(room.getChildAge())) {
			String childAge = room.getChildAge().stream().map(String::valueOf).collect(Collectors.joining(","));
			key.append(childAge);
		}
		return key.toString();
	}

	private Map<String, List<RoomInfo>> getroomInfoMap(List<RoomResponse> rooms) {

		Map<String, List<RoomInfo>> roomOccupancyKeyRoomInfosValues = new ConcurrentHashMap<>();
		for (RoomResponse hotelBedsRoom : rooms) {
			RoomInfo room = new RoomInfo();
			room.setId(hotelBedsRoom.getCode());
			room.setRoomCategory(hotelBedsRoom.getName());
			room.setCheckInDate(searchQuery.getCheckinDate());
			room.setCheckOutDate(searchQuery.getCheckinDate());
			String code = hotelBedsRoom.getCode().replace(".", "_");
			room.setRoomType(code.split("_")[0]);
			hotelBedsRoom.getRates().forEach(rate -> {
				if (rate.getPaymentType().equals("AT_WEB")) {
					StringBuilder key = new StringBuilder();
					key.append(rate.getAdults());
					key.append(rate.getChildren());
					if (rate.getChildrenAges() != null) {
						key.append(rate.getChildrenAges());
					}
					RoomInfo rInfo = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(room), RoomInfo.class);
					populateRoomInfoFromRate(rInfo, rate);
					rInfo.getMiscInfo().setRoomTypeCode(rInfo.getId());
					roomOccupancyKeyRoomInfosValues.computeIfAbsent(key.toString(), (x) -> new LinkedList<>())
							.add(rInfo);
				}
			});
		}
		return roomOccupancyKeyRoomInfosValues;
	}

	protected void populateRoomInfoFromRate(RoomInfo rInfo, Rate rate) {

		rInfo.setNumberOfAdults(rate.getAdults());
		rInfo.setNumberOfChild(rate.getChildren());
		populateRoomPerNightPrice(rInfo, rate);
		rInfo.setCancellationPolicy(getRoomCancellationPolicy(rate, searchQuery, rInfo));
		rInfo.setMiscInfo(RoomMiscInfo.builder().ratePlanCode(rate.getRateKey()).rateType(rate.getRateType())
				.rateChannel(rate.getRateClass()).build());
		// setMealBasis(rInfo, rate.getBoardName());
		rInfo.setMealBasis(rate.getBoardCode());
	}

	// public void setMealBasis(RoomInfo roomInfo, String boardCode) {
	//
	// if (ObjectUtils.isEmpty(boardCode)) {
	// roomInfo.setMealBasis(HotelConstants.ROOM_ONLY);
	// return;
	// }
	// Map<String, String> mealBasis = cacheHandler.fetchMealInfoFromAerospike(searchQuery, Arrays.asList(boardCode));
	// if (!ObjectUtils.isEmpty(roomInfo.getMealBasis())
	// && !roomInfo.getMealBasis().equals(HotelConstants.ROOM_ONLY)) {
	// return;
	// }
	// roomInfo.setMealBasis(mealBasis.getOrDefault(boardCode.toLowerCase(), HotelConstants.ROOM_ONLY));
	// }

	public void storeLogs(String identifier, String key, HotelBedsBaseResponse response) {

		Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
				.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

		SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
		if (ObjectUtils.isEmpty(response)) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add(httpUtils.getResponseString() + httpUtils.getPostData());
		}
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getPostData()).type(identifier)
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getResponseString()).type(identifier)
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());
	}


}
