package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class HotelBedsBookingCancellationService extends HotelBedsBaseService {

	private Order order;
	private HotelInfo hInfo;
	HotelBedsBookResponse cancellationResponse;

	public boolean cancelBooking() throws IOException {

		try {
			listener = new RestAPIListener("");
			String supplierBookingReference = hInfo.getMiscInfo().getSupplierBookingReference();
			httpUtils = HotelBedsUtils.getPostBookingHttpUtils(supplierBookingReference, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.CANCEL_BOOKING_URL),HttpUtilsV2.REQ_METHOD_DELETE);
			cancellationResponse = httpUtils.getResponse(HotelBedsBookResponse.class).orElse(null);

			if (cancellationResponse.getBooking() != null) {
				return cancellationResponse.getBooking().getStatus().equals("CANCELLED");
			}
		} finally {
			storeLogs("HotelBeds-Cancellation", order.getBookingId(), cancellationResponse);
		}
		return false;
	}
}

