package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service

public class HotelBedsBookingFactory extends AbstractHotelBookingFactory {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	HotelOrderItemCommunicator itemComm;

	public HotelBedsBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException, InterruptedException, JAXBException {

		HotelInfo hInfo = this.getHotel();
		HotelSearchQuery searchQuery = cacheHandler.getHotelSearchQueryFromCache(hInfo.getMiscInfo().getSearchId());
		HotelBedsBookingService bookingService =
				HotelBedsBookingService.builder().hInfo(this.getHotel()).order(order).supplierConf(supplierConf)
						.sourceConfigOutput(sourceConfigOutput).searchQuery(searchQuery).itemComm(itemComm).build();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
	}


}
