package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.BookRequestRoom;
import com.tgs.services.hms.datamodel.hotelBeds.Holder;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedPaxType;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Paxes;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.oms.datamodel.Order;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelBedsBookingService extends HotelBedsBaseService {

	private Order order;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelInfo hInfo;
	private HotelOrderItemCommunicator itemComm;
	HotelBedsBookResponse bookingResponse;

	public boolean book() throws IOException, JAXBException, InterruptedException {

		boolean isBookingSuccessful = false;
		try {
			listener = new RestAPIListener("");
			HotelBedsBookRequest bookingRequest = getBookingRequest(hInfo.getOptions().get(0));
			httpUtils = HotelBedsUtils.getHttpUtils(bookingRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOK_URL));
			bookingResponse = httpUtils.getResponse(HotelBedsBookResponse.class).orElse(null);
			if (bookingResponse.getBooking() != null) {
				isBookingSuccessful = updateBookingStatus(bookingResponse);
			}
		} finally {
			storeLogs("HotelBeds-Booking", order.getBookingId(), bookingResponse);
		}
		return isBookingSuccessful;
	}

	private boolean updateBookingStatus(HotelBedsBookResponse bookingResponse) {

		hInfo.getMiscInfo().setSupplierBookingReference(bookingResponse.getBooking().getReference());
		hInfo.getMiscInfo().setSupplierBookingId(bookingResponse.getBooking().getReference());
		return bookingResponse.getBooking().getStatus().equals("CONFIRMED");
	}

	private HotelBedsBookRequest getBookingRequest(Option option) {

		List<BookRequestRoom> rooms = new ArrayList<>();
		double tolerance = sourceConfigOutput.getTolerance();
		int roomIndex = 1;
		option.getRoomInfos().forEach(rInfo -> {
			BookRequestRoom room = new BookRequestRoom();
			room.setRateKey(rInfo.getMiscInfo().getRatePlanCode());
			List<Paxes> paxes = new ArrayList<>();
			for (TravellerInfo traveller : rInfo.getTravellerInfo()) {
				Paxes pax = Paxes.builder().name(traveller.getFirstName()).surname(traveller.getLastName())
						.type(traveller.getPaxType().getCode().equals(PaxType.ADULT.getCode())
								? HotelBedPaxType.ADULT.getCode()
								: HotelBedPaxType.CHILD.getCode())
						.roomId(roomIndex).build();
				paxes.add(pax);
			}
			room.setPaxes(paxes);
			rooms.add(room);
		});
		TravellerInfo traveller = option.getRoomInfos().get(0).getTravellerInfo().get(0);
		Holder holder = Holder.builder().name(traveller.getFirstName()).surname(traveller.getLastName()).build();

		HotelBedsBookRequest bookingRequest = HotelBedsBookRequest.builder().clientReference(order.getBookingId())
				.holder(holder).remark("").rooms(rooms).tolerance(tolerance).build();
		return bookingRequest;
	}

}

