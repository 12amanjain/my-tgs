package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.hotelBeds.CountryResponse;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsStaticDataRequest;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;

@Builder
public class HotelBedsCityStaticDataService {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSourceConfigOutput sourceConfig;
	protected HotelStaticDataRequest staticDataRequest;

	public List<HotelCityInfoQuery> getHotelBedsCityList() throws IOException {

		List<HotelCityInfoQuery> cityList = new ArrayList<>();
		HotelBedsStaticDataRequest staticRequest = HotelBedsStaticDataRequest.builder().fields("all")
				.language(HotelBedsConstant.LANGUAGE.value).from(1).to(sourceConfig.getCityHitCount()).build();
		HttpUtilsV2 httpUtils = HotelBedsUtils.getHttpUtils(null, staticRequest, supplierConf,
				supplierConf.getHotelAPIUrl(HotelUrlConstants.CITY_INFO));
		CountryResponse response = httpUtils.getResponse(CountryResponse.class).orElse(null);
		if (response != null) {
			cityList = getCityListFromHotelBedsCountry(response);
		}
		return cityList;
	}

	private List<HotelCityInfoQuery> getCityListFromHotelBedsCountry(CountryResponse response) {

		List<HotelCityInfoQuery> cityList = new ArrayList<>();
		response.getCountries().forEach(country -> {
			country.getStates().forEach((hotelBedcity) -> {
				HotelCityInfoQuery cityInfo = new HotelCityInfoQuery();
				cityInfo.setCityname(hotelBedcity.getName());
				cityInfo.setCountryname(country.getDescription().getContent());
				cityInfo.setSuppliercityid(String.valueOf(hotelBedcity.getCode()));
				cityInfo.setSuppliercountryid(String.valueOf(country.getCode()));
				cityInfo.setSuppliername(HotelSourceType.HOTELBEDS.name());
				cityList.add(cityInfo);
			});
		});
		return cityList;
	}
}
