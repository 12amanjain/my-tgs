package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Contact;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelBeds.Facility;
import com.tgs.services.hms.datamodel.hotelBeds.FacilityList;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsStaticData;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsStaticDataRequest;
import com.tgs.services.hms.datamodel.hotelBeds.Phone;
import com.tgs.services.hms.datamodel.hotelBeds.Room;
import com.tgs.services.hms.datamodel.hotelBeds.RoomFacility;
import com.tgs.services.hms.datamodel.hotelBeds.StaticHotel;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.utils.common.HttpUtilsV2;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Builder
@Slf4j
public class HotelBedsHotelStaticDataService {

	protected HotelInfoSaveManager hotelInfoSaveManager;
	protected HotelSupplierConfiguration supplierConf;
	private static final ExecutorService executor = Executors.newFixedThreadPool(1);
	private GeneralServiceCommunicator generalServiceCommunicator;
	private Map<Integer, List<String>> facilityCodeFacilityDescMap;
	protected HotelSourceConfigOutput sourceConfig;

	public void init() {

		facilityCodeFacilityDescMap = getFacilityCodeMap();
	}

	public void process() throws IOException {

		Integer startIndex = sourceConfig.getStartIndex() != null ? sourceConfig.getStartIndex() : 0;
		AtomicInteger index = new AtomicInteger(startIndex);
		Integer hotelHitCount = Integer.parseInt(sourceConfig.getSize());
		Integer staticHotelPageSize = sourceConfig.getHotelBatchSize();

		IntStream.range(0, staticHotelPageSize).forEach(i -> {
			executor.submit(() -> {
				try {
					HotelBedsStaticDataRequest staticRequest = HotelBedsStaticDataRequest.builder()
							.fields(HotelBedsConstant.FIELDS.value).language(HotelBedsConstant.LANGUAGE.value)
							.from(index.get() + 1).to(index.updateAndGet(n -> n + hotelHitCount)).build();
					HttpUtilsV2 httpUtils = HotelBedsUtils.getHttpUtils(null, staticRequest, supplierConf,
							supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DATA));
					HotelBedsStaticData response = httpUtils.getResponse(HotelBedsStaticData.class).orElse(null);
					List<HotelInfo> hotelList = getHotelListFromResponse(response);
					saveHotelInfoList(hotelList);
				} catch (Exception e) {
					log.error("Error while fetching static hotel info ", e);
				}
			});
		});
	}

	private List<HotelInfo> getHotelListFromResponse(HotelBedsStaticData response) {

		List<HotelInfo> hotelList = new ArrayList<>();
		if (response.getHotels() != null && response.getHotels().size() > 0) {
			response.getHotels().forEach(hotel -> {
				String description = hotel.getDescription() != null ? hotel.getDescription().getContent() : "";
				HotelInfo hInfo = HotelInfo.builder().name(hotel.getName().getContent()).description(description)
						.geolocation(GetGeoLocation(hotel)).address(getaddress(hotel)).contact(getContact(hotel))
						.images(getImages(hotel, HotelBedsConstant.IMAGEBASE_URL.value))
						.facilities(getFacilities(hotel))
						.miscInfo(HotelMiscInfo.builder().supplierStaticHotelId(hotel.getCode() + "").build())
						//.options(Arrays.asList(getOptionRoomsAmenities(hotel)))
						.rating(getRatingoutofCategory(hotel.getCategoryCode())).build();
				hotelList.add(hInfo);
			});
		}
		return hotelList;
	}

	private Option getOptionRoomsAmenities(StaticHotel hotel) {

		if (hotel != null && CollectionUtils.isNotEmpty(hotel.getRooms())) {
			List<RoomInfo> roomInfos = new ArrayList<>();
			List<Room> roomCodes = hotel.getRooms();
			for (Room roomCode : roomCodes) {
				RoomInfo roomInfo = new RoomInfo();
				if (roomCode.getRoomFacilities() != null && CollectionUtils.isNotEmpty(roomCode.getRoomFacilities())) {
					roomInfo.setRoomAmenities(getRoomFacilities(roomCode.getRoomFacilities()));
				}
				roomInfo.setId(roomCode.getRoomCode());
				roomInfo.setRoomType(roomCode.getRoomType());
				roomInfos.add(roomInfo);
			}
			Option option = Option.builder().roomInfos(roomInfos).build();
			return option;
		}
		return null;
	}

	private List<String> getFacilities(StaticHotel hotel) {
		List<String> facilities = new ArrayList<>();
		if (hotel.getFacilities() != null) {
			List<Facility> availableFacilities =
					hotel.getFacilities().stream()
							.filter(facility -> facility.getIndYesOrNo() != null && facility.getIndYesOrNo()
									|| facility.getIndFee() != null && facility.getIndFee())
							.collect(Collectors.toList());

			availableFacilities.forEach(facility -> {
				if (facilityCodeFacilityDescMap.get(facility.getFacilityCode()) != null) {
					String facilitiesDescription = facilityCodeFacilityDescMap.get(facility.getFacilityCode()).stream()
							.collect(Collectors.joining(","));
					facilities.add(StringUtils.join(facilitiesDescription, " : ",
							facility.getIndFee() != null && facility.getIndFee() ? "" : "Paid Facility"));
				}
			});
		}
		return facilities;
	}

	private List<String> getRoomFacilities(List<RoomFacility> roomFacilities) {

		List<RoomFacility> availableFacilities =
				roomFacilities.stream().filter(facility -> facility.getIndYesOrNo() != null && facility.getIndYesOrNo()
						|| facility.getIndFee() != null && facility.getIndFee()).collect(Collectors.toList());
		List<String> facilities = new ArrayList<>();
		availableFacilities.forEach(facility -> {
			String facilitiesDescription = facilityCodeFacilityDescMap.get(facility.getFacilityCode()).stream()
					.collect(Collectors.joining(","));
			facilities.add(StringUtils.join(facilitiesDescription, " : ",
					facility.getIndFee() != null && facility.getIndFee() ? "" : "Paid Facility"));
		});
		return facilities;
	}


	private List<Image> getImages(StaticHotel hotel, String baseUrl) {

		List<Image> images = new ArrayList<>();
		long imageSize = sourceConfig.getImageListSize();
		if (hotel.getImages() != null && CollectionUtils.isNotEmpty(hotel.getImages())) {
			for (com.tgs.services.hms.datamodel.hotelBeds.Image img : hotel.getImages()) {
				Image image = Image.builder().bigURL(StringUtils.join(baseUrl, img.getPath()))
						.thumbnail(StringUtils.join(baseUrl, "small/", img.getPath())).build();
				images.add(image);
				if (images.size() > imageSize)
					break;
			}
		}
		return images;
	}

	private Contact getContact(StaticHotel hotel) {

		Contact contact = new Contact();
		contact.setEmail(hotel.getEmail());
		if (hotel.getPhones() != null && CollectionUtils.isNotEmpty(hotel.getPhones())) {
			Map<String, String> phoneTypePhoneNumberMap =
					hotel.getPhones().stream().collect(Collectors.toMap(Phone::getPhoneType, Phone::getPhoneNumber));
			String phoneNumbers = phoneTypePhoneNumberMap.entrySet().stream()
					.map(entry -> entry.getKey() + " - " + entry.getValue()).collect(Collectors.joining(", "));
			contact.setPhone(phoneNumbers);
		}
		return contact;
	}

	private Address getaddress(StaticHotel hotel) {

		City city = City.builder().name(hotel.getCity().getContent()).build();
		String countryName = generalServiceCommunicator.getCountryInfo(hotel.getCountryCode()) != null
				? generalServiceCommunicator.getCountryInfo(hotel.getCountryCode()).getName()
				: hotel.getCountryCode();
		Country country = Country.builder().name(countryName).build();
		String add = "Street:" + hotel.getAddress().getStreet() + ",Number- " + hotel.getAddress().getNumber();
		Address address = Address.builder().addressLine1(add).city(city).country(country)
				.postalCode(hotel.getPostalCode()).build();
		return address;
	}

	private GeoLocation GetGeoLocation(StaticHotel hotel) {

		GeoLocation geoLocation = null;
		if (hotel.getCoordinates() != null) {
			geoLocation = GeoLocation.builder().latitude(hotel.getCoordinates().getLatitude().toString())
					.longitude(hotel.getCoordinates().getLongitude().toString()).build();
		}
		return geoLocation;
	}

	private void saveHotelInfoList(List<HotelInfo> hInfoList) {

		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				// String name = new StringBuilder(dbHotelInfo.getName()).append("_").append("HOTELBEDS").toString();
				// dbHotelInfo.setName(name);
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hInfo.getMiscInfo().getSupplierStaticHotelId(),
						HotelSourceType.HOTELBEDS.name(), HotelSourceType.HOTELBEDS);
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {}", hInfo.getName(), hInfo.getRating(),
						e);
			}
		}
	}

	private Map<Integer, List<String>> getFacilityCodeMap() {

		FacilityList facilityList = null;
		try {
			HotelBedsStaticDataRequest facilityStaticRequest =
					HotelBedsStaticDataRequest.builder().fields("all").language("ENG").from(1).to(500).build();
			HttpUtilsV2 httpUtils1 = HotelBedsUtils.getHttpUtils(null, facilityStaticRequest, supplierConf,
					HotelBedsConstant.FACILITY_URL.value);
			facilityList = httpUtils1.getResponse(FacilityList.class).orElse(null);
		} catch (IOException e) {
			log.error("error while fetching facility list");
		}
		Map<Integer, List<String>> facilityCodeKeyDescVal = new HashMap<>();
		facilityList.getFacilities().stream().forEach(facility -> {
			Integer key = facility.getCode();
			facilityCodeKeyDescVal.computeIfAbsent(key, (x) -> new ArrayList<>())
					.add(facility.getDescription() != null ? facility.getDescription().getContent() : "");
		});
		return facilityCodeKeyDescVal;
	}

	private Integer getRatingoutofCategory(String categoryName) {

		List<Integer> ratings = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5));
		Integer hotelRating = 0;
		for (Integer rating : ratings) {
			if (categoryName.contains(rating + "")) {
				hotelRating = rating;
				break;
			}
		}
		return hotelRating;
	}
}

