package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBException;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBedsInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	protected HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public HotelBedsInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {

		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException {

		Set<String> propertyIds =
				getPropertyIdsOfCity(HotelSourceType.HOTELBEDS.name());
		searchResult = getResultOfAllPropertyIds(propertyIds);
	}

	protected HotelSearchResult getResultOfAllPropertyIds(Set<String> propertyIds) throws IOException {

		HotelSearchResult finalSearchResult = new HotelSearchResult();
		int hotelHitCount = sourceConfigOutput.getHotelHitCount();
		List<Set<String>> listOfPartitionedIdsSet = HotelUtils.partitionSet(propertyIds, hotelHitCount);
		listOfPartitionedIdsSet.forEach(partition -> {
			List<Integer> propertyIdList =
					partition.stream().map(s -> Integer.parseInt(s)).collect(Collectors.toList());
			HotelBedsSearchService searchService = HotelBedsSearchService.builder().sourceConfig(sourceConfigOutput)
					.supplierConf(this.getSupplierConf()).propertyIds(propertyIdList).searchQuery(searchQuery)
					.cacheHandler(cacheHandler).moneyExchnageComm(moneyExchnageComm).build();
			try {
				searchService.doSearch();
			} catch (IOException e) {
				log.error("error while fetching HotelBeds search response");
			}
			HotelSearchResult partialSearchResult = searchService.getSearchResult();
			if (partialSearchResult != null) {
				List<HotelInfo> hInfo = partialSearchResult.getHotelInfos();
				finalSearchResult.getHotelInfos().addAll(hInfo);
			}
		});
		return finalSearchResult;
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		if (CollectionUtils.isNotEmpty(hInfo.getOptions())) {
			for (Option option : hInfo.getOptions()) {
				option.getMiscInfo().setIsDetailHit(true);
			}
		}
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) {

		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});
	}
}
