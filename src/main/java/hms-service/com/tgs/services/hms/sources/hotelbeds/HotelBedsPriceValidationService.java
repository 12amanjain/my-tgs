package com.tgs.services.hms.sources.hotelbeds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsRateCheckResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Rate;
import com.tgs.services.hms.datamodel.hotelBeds.RateCheckRoomRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@SuperBuilder
public class HotelBedsPriceValidationService extends HotelBedsBaseService {

	private String bookingId;
	HotelBedsRateCheckResponse priceValidationResponse;

	public void validate(HotelInfo hInfo) throws Exception {

		listener = new RestAPIListener("");
		try {
			HotelBedsRateCheckRequest priceValidationRequest = getRateCheckRequest(hInfo.getOptions().get(0));
			httpUtils = HotelBedsUtils.getHttpUtils(priceValidationRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BLOCK_ROOM_URL));
			priceValidationResponse = httpUtils.getResponse(HotelBedsRateCheckResponse.class).orElse(null);
			updateOptionWithNewPrice(priceValidationResponse, hInfo.getOptions().get(0));
		} finally {
			storeLogs("HotelBeds-PriceValidation", bookingId, priceValidationResponse);
		}
	}

	private void updateOptionWithNewPrice(HotelBedsRateCheckResponse priceValidationResponse, Option option) {

		Map<String, Rate> rateKeyMap = new HashMap<>();
		currency = priceValidationResponse.getHotel().getCurrency();
		priceValidationResponse.getHotel().getRooms().forEach(room -> {
			room.getRates().forEach(rate -> {
				rateKeyMap.put(rate.getRateKey(), rate);
			});
		});
		List<Instruction> instructions = new ArrayList<>();
		List<String> rateComments = new ArrayList<>();
		option.getRoomInfos().forEach(room -> {
			Rate rate = rateKeyMap.get(room.getMiscInfo().getRatePlanCode());
			{
				if (rate == null) {
					throw new CustomGeneralException(SystemError.ROOM_SOLD_OUT);
				}
				if (rate.getRateComments() != null) {
					rateComments.add(rate.getRateComments());
				}
				if (!room.getMiscInfo().getRateType().equals("BOOKABLE")) {
					double oldRoomPrice = room.getTotalFareComponents().get(HotelFareComponent.BF);
					double newRoomPrice = getAmountBasedOnCurrency(rate.getNet(), currency);
					if ((Math.abs(oldRoomPrice - newRoomPrice) > 5)) {
						populateRoomInfoFromRate(room, rateKeyMap.get(room.getMiscInfo().getRatePlanCode()));
					}
				}
			}
		});
		String rateComment = rateComments.stream().collect(Collectors.joining("/n")).toString();
		instructions.add(Instruction.builder().type(InstructionType.BOOKING_NOTES).msg(rateComment).build());
		option.setInstructions(instructions);
	}

	private HotelBedsRateCheckRequest getRateCheckRequest(Option selectedOption) {

		List<RateCheckRoomRequest> rateKeys = new ArrayList<>();
		selectedOption.getRoomInfos().forEach(room -> {
			rateKeys.add(RateCheckRoomRequest.builder().rateKey(room.getMiscInfo().getRatePlanCode()).build());
		});
		HotelBedsRateCheckRequest rateCheckrequest = HotelBedsRateCheckRequest.builder().rooms(rateKeys).build();
		return rateCheckrequest;
	}
}
