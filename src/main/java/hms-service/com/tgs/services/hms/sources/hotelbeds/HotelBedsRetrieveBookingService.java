package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelBeds.Booking;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedPaxType;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsBookResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Paxes;
import com.tgs.services.hms.datamodel.hotelBeds.RoomResponse;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelBedsRetrieveBookingService extends HotelBedsBaseService {

	private HotelImportBookingParams importBookingParams;
	private HotelSupplierConfiguration supplierConf;
	private HotelImportedBookingInfo bookingInfo;
	HotelBedsBookResponse bookingDetailResponse;

	public void retrieveBooking() throws IOException {

		try {
			listener = new RestAPIListener("");
			String supplierBookingReference = importBookingParams.getSupplierBookingId();
			httpUtils = HotelBedsUtils.getPostBookingHttpUtils(supplierBookingReference, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOK_URL), HttpUtils.REQ_METHOD_GET);
			bookingDetailResponse = httpUtils.getResponse(HotelBedsBookResponse.class).orElse(null);
			bookingInfo = createBookingDetailResponse();
		} finally {
			storeLogs("HotelBeds-RetrieveBooking", importBookingParams.getBookingId(), bookingDetailResponse);
		}
	}

	private HotelImportedBookingInfo createBookingDetailResponse() {

		if (bookingDetailResponse == null || bookingDetailResponse.getBooking() == null)
			throw new CustomGeneralException("Error While Trying To Fetch Booking Details");
		Booking response = bookingDetailResponse.getBooking();
		HotelInfo hInfo = getHotelDetails(response);
		searchQuery = getHotelSearchQuery(response);
		List<RoomInfo> roomInfos = new ArrayList<>();
		currency = response.getCurrency();
		for (RoomResponse roomResponse : response.getHotel().getRooms()) {
			RoomInfo room = new RoomInfo();
			room.setId(roomResponse.getCode());
			room.setRoomCategory(roomResponse.getName());
			room.setCheckInDate(response.getHotel().getCheckIn());
			room.setCheckOutDate(response.getHotel().getCheckIn());
			String code = roomResponse.getCode().replace(".", "_");
			room.setRoomType(code.split("_")[0]);
			roomResponse.getRates().forEach(rate -> {
				populateRoomInfoFromRate(room, rate);
				room.getMiscInfo().setRoomTypeCode(room.getId());
			});
			List<TravellerInfo> travellerInfoList = getTravellerList(response.getHotel().getRooms());
			room.setTravellerInfo(travellerInfoList);
			roomInfos.add(room);
		}
		double totalPrice = roomInfos.stream().mapToDouble(RoomInfo::getTotalPrice).sum();
		Option option = Option.builder().totalPrice(totalPrice)
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(hInfo.getMiscInfo().getSupplierStaticHotelId())
						.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
				.roomInfos(roomInfos).id(RandomStringUtils.random(20, true, true)).build();

		setCancellationDeadlineFromRooms(option);
		List<Option> options = new ArrayList<>();
		options.add(option);
		hInfo.setOptions(options);
		String orderStatus = response.getHotel().getRooms().get(0).getStatus();
		DeliveryInfo deliveryInfo = new DeliveryInfo();

		return HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery).orderStatus(orderStatus)
				.deliveryInfo(deliveryInfo).bookingCurrencyCode(currency).bookingDate(response.getCreationDate())
				.build();
	}

	private List<TravellerInfo> getTravellerList(List<RoomResponse> rooms) {

		List<TravellerInfo> travellerInfoList = new ArrayList<>();
		for (RoomResponse room : rooms) {
			for (Paxes pax : room.getPaxes()) {
				TravellerInfo ti = new TravellerInfo();
				ti.setFirstName(pax.getName());
				ti.setLastName(pax.getSurname());
				ti.setPaxType(pax.getType() == HotelBedPaxType.ADULT.getCode() ? PaxType.ADULT : PaxType.CHILD);
				travellerInfoList.add(ti);
			}
		}
		return travellerInfoList;
	}

	private HotelSearchQuery getHotelSearchQuery(Booking response) {

		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(response.getHotel().getCheckIn());
		searchQueryBuilder.checkoutDate(response.getHotel().getCheckOut());
		searchQueryBuilder.sourceId(10);
		searchQuery.setMiscInfo(HotelSearchQueryMiscInfo.builder().supplierId("HOTELBEDS").build());
		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(Booking response) {

		String hotelId = response.getHotel().getCode().toString();
		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelId)
				.supplierBookingReference(response.getReference()).build();
		HotelInfo hInfo = HotelInfo.builder().miscInfo(hMiscInfo).name(response.getHotel().getName()).build();
		return hInfo;
	}
}
