package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedPaxType;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsSearchRequest;
import com.tgs.services.hms.datamodel.hotelBeds.HotelBedsSearchResponse;
import com.tgs.services.hms.datamodel.hotelBeds.Occupancy;
import com.tgs.services.hms.datamodel.hotelBeds.Paxes;
import com.tgs.services.hms.datamodel.hotelBeds.Stay;
import com.tgs.services.hms.datamodel.hotelBeds.searchRequestHotel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@Service
@SuperBuilder
public class HotelBedsSearchService extends HotelBedsBaseService {

	private HotelSearchResult searchResult;
	private List<Integer> propertyIds;
	HotelBedsSearchResponse response;

	public void doSearch() throws IOException {

		searchResult = new HotelSearchResult();
		listener = new RestAPIListener("");
		try {
			HotelBedsSearchRequest searchRequest = getHotelBedsSearchRequest();
			httpUtils = HotelBedsUtils.getHttpUtils(searchRequest, null, supplierConf,
					supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			response = httpUtils.getResponse(HotelBedsSearchResponse.class).orElse(null);
			searchResult.setHotelInfos(getHotelListFromHotelBedsResult(response));
		} finally {
			storeLogs("HotelBeds-SearchById", searchQuery.getSearchId(), response);
		}
	}

	private List<HotelInfo> getHotelListFromHotelBedsResult(HotelBedsSearchResponse result) {

		List<HotelInfo> hInfos = new ArrayList<>();
		if (result == null || CollectionUtils.isEmpty(result.getHotels().getHotels())) {
			log.error("Empty Response From Supplier {} for searchId {}", supplierConf.getBasicInfo().getSupplierId(),
					searchQuery.getSearchId());
			return null;
		}
		result.getHotels().getHotels().forEach(hotel -> {
			List<Option> optionList = createOptionFromHotel(hotel);
			HotelInfo hInfo = HotelInfo.builder()
					.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
							.supplierBookingReference(result.getAuditData().getToken())
							.supplierStaticHotelId(hotel.getCode() + "").build())
					.options(optionList).build();
			hInfos.add(hInfo);
		});
		return hInfos;
	}

	private HotelBedsSearchRequest getHotelBedsSearchRequest() {

		Stay stay = Stay.builder().checkIn(searchQuery.getCheckinDate().toString())
				.checkOut(searchQuery.getCheckoutDate().toString()).build();
		List<Occupancy> occuancies = getOccupancy();
		HotelBedsSearchRequest request = HotelBedsSearchRequest.builder().stay(stay).occupancies(occuancies)
				.hotels(searchRequestHotel.builder().hotel(propertyIds).build()).build();
		return request;
	}

	private List<Occupancy> getOccupancy() {

		List<Occupancy> occupancyList = new ArrayList<>();
		searchQuery.getRoomInfo().forEach(room -> {
			List<Paxes> paxeList = new ArrayList<>();
			int childrenCount = room.getNumberOfChild() != null ? room.getNumberOfChild() : 0;
			if (childrenCount > 0) {
				room.getChildAge().forEach(age -> {
					Paxes paxes = Paxes.builder().age(age).type(HotelBedPaxType.CHILD.getCode()).build();
					paxeList.add(paxes);
				});
			}
			Occupancy occupancy = Occupancy.builder().adults(room.getNumberOfAdults()).children(childrenCount).rooms(1)
					.paxes(CollectionUtils.isNotEmpty(paxeList) ? paxeList : null).build();
			occupancyList.add(occupancy);
		});
		return occupancyList;
	}

}
