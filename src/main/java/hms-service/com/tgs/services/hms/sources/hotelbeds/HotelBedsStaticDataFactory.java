package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelBedsStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	private HotelCityInfoMappingManager cityManager;

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;

	public HotelBedsStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException {

		HotelBedsHotelStaticDataService hotelBedStaticDataService = HotelBedsHotelStaticDataService.builder()
				.hotelInfoSaveManager(hotelInfoSaveManager).generalServiceCommunicator(generalServiceCommunicator)
				.supplierConf(supplierConf).sourceConfig(sourceConfigOutput).build();
		hotelBedStaticDataService.init();
		hotelBedStaticDataService.process();
	}

	// City mapping not needed for this supplier as there is no city api or city based search supported
	@Override
	protected void getCityMappingData() throws IOException {

		HotelBedsCityStaticDataService staticCityService = HotelBedsCityStaticDataService.builder()
				.supplierConf(supplierConf).staticDataRequest(staticDataRequest).build();
		List<HotelCityInfoQuery> cityData = staticCityService.getHotelBedsCityList();
		cityManager.saveCityInfoMappingList(cityData);
	}
}
