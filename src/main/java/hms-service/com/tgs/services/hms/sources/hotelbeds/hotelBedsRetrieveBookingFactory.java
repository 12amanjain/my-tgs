package com.tgs.services.hms.sources.hotelbeds;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class hotelBedsRetrieveBookingFactory extends AbstractRetrieveHotelBookingFactory {

	@Autowired
	HotelCacheHandler cacheHandler;

	@Autowired
	protected MoneyExchangeCommunicator moneyExchnageComm;

	public hotelBedsRetrieveBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException {
		HotelBedsRetrieveBookingService bookingService = HotelBedsRetrieveBookingService.builder()
				.supplierConf(supplierConf).moneyExchnageComm(moneyExchnageComm).cacheHandler(cacheHandler)
				.importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();

	}

}
