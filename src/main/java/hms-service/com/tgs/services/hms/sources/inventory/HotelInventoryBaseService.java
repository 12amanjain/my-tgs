package com.tgs.services.hms.sources.inventory;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import com.google.api.client.repackaged.com.google.common.base.Objects;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;

import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryAllocationInfo;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryCacheData;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.datamodel.inventory.HotelSellPolicy;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;

import com.tgs.services.hms.helper.HotelRoomCategoryHelper;
import com.tgs.services.hms.helper.HotelRoomTypeHelper;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelInventoryBaseService {

	protected HotelSearchQuery searchQuery;
	protected Map<Integer, HotelRoomTypeInfo> roomOccupancyRoomTypeInfoMap;
	protected Map<String, String> mealBasisIdMealBasisName;
	protected HotelSupplierConfiguration supplierConf;

	private static final int maxOptionSize = 20;
	
	protected List<Option> fetchOptionListForHotel(List<HotelRatePlan> ratePlanList) {

		ratePlanList.forEach(ratePlan -> {
			ratePlan.setRoomTypeInfo(HotelRoomTypeHelper.getRoomTypeIdRoomTypeInfoMap().get(Long.valueOf(ratePlan.getRoomTypeId())));
			ratePlan.setRoomCategoryName(
					HotelRoomCategoryHelper.getRoomCategoryIdRoomCategoryNameMap().get(Long.valueOf(ratePlan.getRoomCategoryId())));
		});
		Map<String, List<RoomInfo>> roomTypeKeyRoomInfoList = new HashMap<>();
		Map<String, List<HotelRatePlan>> roomTypeAsKeyRatePlanListValue =
				ratePlanList.stream().collect(Collectors.groupingBy(HotelRatePlan::getRoomTypeId));
		roomTypeAsKeyRatePlanListValue.keySet().forEach(roomTypeName -> {
			List<HotelRatePlan> ratePlanForRoomType = roomTypeAsKeyRatePlanListValue.get(roomTypeName);
			List<RoomInfo> rooms = getRoomListForRoomType(ratePlanForRoomType);
			roomTypeKeyRoomInfoList.put(roomTypeName, rooms);
		});

		List<Option> optionList = getOptionList(roomTypeKeyRoomInfoList);
		return optionList;
	}


	protected List<RoomInfo> getRoomListForRoomType(List<HotelRatePlan> ratePlanForRoomType) {

		Map<String, List<HotelRatePlan>> roomCategoryKeyRatePlanList =
				ratePlanForRoomType.stream().collect(Collectors.groupingBy(ratePlan -> {
					StringBuilder sb = new StringBuilder();
					sb.append(ratePlan.getRoomCategoryId());
					sb.append(ratePlan.getMealBasisId());
					return sb.toString();
				}));
		List<RoomInfo> rInfoList = new ArrayList<>();
		roomCategoryKeyRatePlanList.keySet().forEach(key -> {
			List<HotelRatePlan> ratePlanForEachRoom = roomCategoryKeyRatePlanList.get(key);
			RoomInfo rInfo = getRoomFromMap(ratePlanForEachRoom);
			if (rInfo == null)
				return;
			rInfoList.add(rInfo);
		});
		return rInfoList;
	}


	protected RoomInfo getRoomFromMap(List<HotelRatePlan> list) {
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		Map<LocalDate, HotelRatePlan> rps =
				list.stream().collect(Collectors.toMap(HotelRatePlan::getValidOn, Function.identity()));
		List<PriceInfo> priceInfoList = new ArrayList<>();
		double totalRoomAmount = 0.0;
		List<Long> ratePlanIds = new ArrayList<>();
		for (int i = 0; i < numberOfNights; i++) {
			LocalDate date = searchQuery.getCheckinDate().plusDays(i);
			if (rps.get(date) != null) {
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, rps.get(date).getRatePlanInfo().getBaseFare());
				totalRoomAmount += rps.get(date).getRatePlanInfo().getBaseFare();
				priceInfo.setDay(i);
				priceInfo.setFareComponents(fareComponents);
				Long ratePlanId = rps.get(date).getId();
				ratePlanIds.add(ratePlanId);
				priceInfoList.add(priceInfo);
			} else
				return null;
		}
		HotelRatePlan roomRatePlan = list.get(0);
		RoomInfo rInfo = new RoomInfo();
		StringBuilder sb = new StringBuilder();
		sb.append(roomRatePlan.getRoomCategoryName());
		sb.append(" ");
		sb.append(roomRatePlan.getRoomTypeInfo().getRoomTypeName());
		rInfo.setRoomCategory(sb.toString());
		rInfo.setRoomType(sb.toString());
		rInfo.setPerNightPriceInfos(priceInfoList);
		rInfo.getTotalFareComponents().put(HotelFareComponent.BF, totalRoomAmount);
		rInfo.getTotalFareComponents().put(HotelFareComponent.TF, totalRoomAmount);
		rInfo.setCheckInDate(searchQuery.getCheckinDate());
		rInfo.setCheckOutDate(searchQuery.getCheckoutDate());
		rInfo.setMealBasis(roomRatePlan.getMealBasisName());
		rInfo.setMiscInfo(RoomMiscInfo.builder().ratePlanIdList(ratePlanIds).secondarySupplier(roomRatePlan.getSupplierId()).build());
		String ratePlanList =
				list.stream().map(HotelRatePlan::getId).map(String::valueOf).collect(Collectors.joining("-"));
		String roomId = String.valueOf(searchQuery.getCheckinDate()) + String.valueOf(numberOfNights) + ratePlanList;
		rInfo.setId(roomId);
		return rInfo;
	}

	protected List<Option> getOptionList(Map<String, List<RoomInfo>> roomTypeKeyRoomInfoList) {

		List<List<RoomInfo>> rInfoList = getRoomTypeRoomListInOrderAsPerSearchQuery(roomTypeKeyRoomInfoList);
		if (CollectionUtils.isEmpty(rInfoList))
			return null;
		if (rInfoList.size() != searchQuery.getRoomInfo().size())
			throw new CustomGeneralException(SystemError.INCORRECT_ROOM_TYPE_COUNT);
		List<Option> optionList = mergeRoomList(rInfoList);

		return optionList;
	}


	private List<Option> mergeRoomList(List<List<RoomInfo>> rInfoList) {


		/*
		 * Logic : Suppose we have searched for 3 roomType i.e. DOUBLE, QUAD, FIVE. rInfoList is like this : 0 index has
		 * list of rooms with roomType as DOUBLE similarly for QUAD, FIVE. Now we have to merge 1 room from each .
		 * 
		 * 
		 * First Inside priority Queue we put index of each list a1[0] & first element of each list a1[1]. Next as this
		 * is minHeap, sorted according to roomPrice, element with minimum price will come out suppose it is first
		 * element of 2nd list. Not we put next element of 2nd list inside queue .
		 * 
		 * This will ensure options with minimum price to come first with minimal space & time complexity. Also we break
		 * if number of options made crosses maxOptionSize
		 * 
		 */


		List<Option> optionList = new ArrayList<>();
		/*
		 * We can optimize it by reducing size of roomList for each category
		 */
		rInfoList.stream().forEach(list -> {
			Collections.sort(list, (room1, room2) -> {
				return room1.getTotalFareComponents().get(HotelFareComponent.TF)
						.compareTo(room2.getTotalFareComponents().get(HotelFareComponent.TF));
			});
		});


		PriorityQueue<int[]> pq = new PriorityQueue<>((a1, a2) -> {

			RoomInfo r1 = rInfoList.get(a1[0]).get(a1[1]);
			RoomInfo r2 = rInfoList.get(a2[0]).get(a2[1]);

			if (r1.getTotalFareComponents().get(HotelFareComponent.TF) < r2.getTotalFareComponents()
					.get(HotelFareComponent.TF))
				return -1;
			else if (r1.getTotalFareComponents().get(HotelFareComponent.TF) == r2.getTotalFareComponents()
					.get(HotelFareComponent.TF))
				return 0;
			return 1;
		});

		for (int i = 0; i < rInfoList.size(); i++) {
			pq.offer(new int[] {i, 0});
		}

		int optionSize = 0;
		while (!pq.isEmpty() && optionSize < maxOptionSize) {

			List<RoomInfo> roomForOneOption = pq.stream().map(a -> {
				return GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(rInfoList.get(a[0]).get(a[1])),
						RoomInfo.class);
			}).collect(Collectors.toList());

			optionList.add(getOptionForRoomList(roomForOneOption));
			int[] polledData = pq.poll();
			if (polledData[1] + 1 == rInfoList.get(polledData[0]).size())
				break;
			else
				pq.offer(new int[] {polledData[0], polledData[1] + 1});
			optionSize++;
		}
		return optionList;
	}


	private Option getOptionForRoomList(List<RoomInfo> roomForOneOption) {

		StringBuilder sb = new StringBuilder();
		int roomNum = 1;
		for (RoomInfo roomInfo : roomForOneOption) {
			roomInfo.setId(roomInfo.getId() + (roomNum++));
			roomInfo.getMiscInfo().getRatePlanIdList().stream().forEach(id -> sb.append(id));
		}

		OptionMiscInfo miscInfo =
				OptionMiscInfo.builder().secondarySupplier(roomForOneOption.get(0).getMiscInfo().getSecondarySupplier())
						.supplierId(supplierConf.getBasicInfo().getSupplierId())
						.sourceId(supplierConf.getBasicInfo().getSourceId()).isDetailHit(true).build();
		Option option = Option.builder().roomInfos(roomForOneOption).miscInfo(miscInfo).id(sb.toString()).build();
		setOptionCancellationPolicy(option);
		return option;
	}


	private List<List<RoomInfo>> getRoomTypeRoomListInOrderAsPerSearchQuery(
			Map<String, List<RoomInfo>> roomTypeKeyRoomInfoList) {

		/*
		 * We can do optimization here for cases like 2 double rooms , in that case it should run only for 1st double &
		 * for second double it should use previously cached data.
		 */
		List<List<RoomInfo>> rInfoList = new ArrayList<>();
		int index = 0;
		log.info("Map is {}", roomOccupancyRoomTypeInfoMap);
		for (RoomSearchInfo roomSearchInfo : searchQuery.getRoomInfo()) {
			int totalRoomOccupancy = getTotalOccupancy(roomSearchInfo);
			do {

				Long roomTypeId = getRoomTypeIdFromOccupancy(totalRoomOccupancy++);
				log.info("RoomTypeId for occupancy {} is {}", roomTypeId, totalRoomOccupancy - 1);
				if (roomTypeId == null)
					continue;
				List<RoomInfo> roomList = roomTypeKeyRoomInfoList.getOrDefault(String.valueOf(roomTypeId), null);
				log.info("RoomList for roomTypeId {} is {}", roomList, roomTypeId);
				if (CollectionUtils.isNotEmpty(roomList)) {
					if (index == rInfoList.size())
						rInfoList.add(index, roomList);
					else
						rInfoList.get(index).addAll(roomList);
				}
			} while (totalRoomOccupancy < 6);
			log.info("RInfoList size is {}, index is {}", rInfoList.size(), index);
			if (rInfoList.size() <= index)
				return null;
			List<RoomInfo> rooms = rInfoList.get(index);
			List<RoomInfo> copyRooms = rooms.stream().map(room -> {
				RoomInfo copyRoom = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(room), RoomInfo.class);
				return copyRoom;
			}).collect(Collectors.toList());
			updateOccupancyInRooms(copyRooms, roomSearchInfo);
			rInfoList.set(index, copyRooms);
			index++;
		}
		return rInfoList;
	}

	private void updateOccupancyInRooms(List<RoomInfo> roomList, RoomSearchInfo roomSearchInfo) {
		roomList.forEach(room -> {
			room.setNumberOfAdults(roomSearchInfo.getNumberOfAdults());
			room.setNumberOfChild(roomSearchInfo.getNumberOfChild());
		});

	}


	public Long getRoomTypeIdFromOccupancy(int totalOccupancy) {
		HotelRoomTypeInfo roomTypeInfo = roomOccupancyRoomTypeInfoMap.get(totalOccupancy);
		if (roomTypeInfo != null)
			return roomTypeInfo.getId();
		return null;
	}

	public String getRoomTypeFromOccupancy(int totalOccupancy) {
		return roomOccupancyRoomTypeInfoMap.get(totalOccupancy).getRoomTypeName();
	}


	private int getTotalOccupancy(RoomSearchInfo room) {
		return room.getNumberOfAdults() + Objects.firstNonNull(room.getNumberOfChild(), 0);
	}

	public int getMinOccupancy() {

		int minOccupancy = getTotalOccupancy(searchQuery.getRoomInfo().get(0));
		for (RoomSearchInfo room : searchQuery.getRoomInfo()) {
			minOccupancy = getTotalOccupancy(room) < minOccupancy ? getTotalOccupancy(room) : minOccupancy;
		}
		return minOccupancy;
	}


	// Adding some random cancellation policy in all options until the cancellation policy requirement gets finalized
	protected void setOptionCancellationPolicy(Option option) {

		LocalDateTime checkInDate = searchQuery.getCheckinDate().atTime(12, 00);
		List<PenaltyDetails> pds = new ArrayList<>();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		boolean isFullRefundAllowed = false;
		String cancellationPolicyText = "Some Text";

		LocalDateTime fromDate = now;
		LocalDateTime toDate = checkInDate;

		PenaltyDetails penaltyDetails1 =
				PenaltyDetails.builder().fromDate(fromDate).toDate(toDate.minusDays(1)).penaltyAmount(0.0).build();
		pds.add(penaltyDetails1);

		PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(fromDate).toDate(toDate.minusDays(1))
				.penaltyAmount(option.getRoomInfos().get(0).getTotalFareComponents().get(HotelFareComponent.BF))
				.build();
		pds.add(penaltyDetails2);
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder().id(option.getId())
				.cancellationPolicy(cancellationPolicyText).isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();
		option.setDeadlineDateTime(deadlineDateTime);
		option.setCancellationPolicy(cancellationPolicy);

	}

	protected void filterRatePlanList(List<HotelRatePlan> ratePlanList) {

		Set<String> excludedRoomType = getExcludedRoomTypeSet();
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		Iterator<HotelRatePlan> ratePlanIterator = ratePlanList.iterator();
		while (ratePlanIterator.hasNext()) {
			HotelRatePlan ratePlan = ratePlanIterator.next();
			ratePlan.setRoomTypeInfo(HotelRoomTypeHelper.getRoomTypeIdRoomTypeInfoMap().get(Long.valueOf(ratePlan.getRoomTypeId())));
			ratePlan.setRoomCategoryName(
					HotelRoomCategoryHelper.getRoomCategoryIdRoomCategoryNameMap().get(Long.valueOf(ratePlan.getRoomCategoryId())));
			int minStayNight = ratePlan.getRatePlanInfo() != null && ratePlan.getRatePlanInfo().getMinStay() != null
					? ratePlan.getRatePlanInfo().getMinStay()
					: 0;
			if (excludedRoomType.contains(ratePlan.getRoomTypeInfo().getRoomTypeName()) || minStayNight > numberOfNights
					|| !availableInventory(ratePlan) || invalidReleaseDate(ratePlan) || !ratePlan.getEnabled()) {
				ratePlanIterator.remove();
			}
		}
	}

	protected void filterRequiredRatePlan(List<HotelInventoryCacheData> data) {

		Iterator<HotelInventoryCacheData> cacheDataIterator = data.iterator();

		while (cacheDataIterator.hasNext()) {

			HotelInventoryCacheData cacheData = cacheDataIterator.next();
			List<HotelRatePlan> ratePlanList = cacheData.getRatePlanList();
			filterRatePlanList(ratePlanList);
			if (ratePlanList.size() == 0)
				cacheDataIterator.remove();
		}
	}

	private boolean availableInventory(HotelRatePlan ratePlan) {

		if (ratePlan.getSellPolicy() != null) {
			if (ratePlan.getSellPolicy().equals(HotelSellPolicy.FREE_SELL))
				return true;
			else if (ratePlan.getSellPolicy().equals(HotelSellPolicy.STOP_SELL))
				return false;
		}

		HotelInventoryAllocationInfo inventoryInfo = ratePlan.getInventoryAllocationInfo();
		if (inventoryInfo == null)
			return false;
		int totalInventory = inventoryInfo.getTotalInventory();
		int soldInventory = inventoryInfo.getSoldInventory();
		if (soldInventory + 1 > totalInventory)
			return false;
		return true;
	}


	private boolean invalidReleaseDate(HotelRatePlan ratePlan) {

		int hoursLeftbeforeCheckin = (int) ChronoUnit.HOURS.between(LocalDate.now().atStartOfDay(),
				searchQuery.getCheckinDate().atStartOfDay());

		Integer releasePeriod =
				ratePlan.getRatePlanInfo() != null ? ratePlan.getRatePlanInfo().getReleasePeriod() : null;
		return releasePeriod != null && releasePeriod > hoursLeftbeforeCheckin ? true : false;
	}


	private Set<String> getExcludedRoomTypeSet() {
		/*
		 * Need To Implement
		 */
		Set<String> exeludedRoomType = new HashSet<>();
		int minRoomOccupancy = getMinOccupancy();
		for (int i = 1; i < minRoomOccupancy && i < 7; i++) {
			exeludedRoomType.add(getRoomTypeFromOccupancy(i));
		}

		return exeludedRoomType;
	}

	protected boolean isSupplierEnabled(String supplierId) {

		HotelSupplierInfo hotelSupplierInfo=HotelSupplierConfigurationHelper.getSupplierInfo(supplierId);
		if(hotelSupplierInfo==null)return false;
		return hotelSupplierInfo.getEnabled();
		
	}
}
