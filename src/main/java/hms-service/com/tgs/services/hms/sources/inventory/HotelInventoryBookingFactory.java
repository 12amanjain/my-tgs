package com.tgs.services.hms.sources.inventory;

import java.io.IOException;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.manager.HotelRatePlanManager;
import com.tgs.services.hms.manager.mapping.HotelRoomInfoSaveManager;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.services.oms.datamodel.Order;

@Service
public class HotelInventoryBookingFactory extends AbstractHotelBookingFactory {

	@Autowired
	HotelCacheHandler cacheHandler;
	
	@Autowired
	HotelRatePlanManager ratePlanManager;
	
	@Autowired
	HotelRoomInfoSaveManager roomInfoManager;
	
	@Autowired
	HotelCacheHandler cachaeHandler;
	
	public HotelInventoryBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException, InterruptedException, JAXBException {
	
		HotelInfo hInfo = this.getHotel();
		HotelSearchQuery searchQuery = cachaeHandler.getHotelSearchQueryFromCache(hotel.getMiscInfo().getSearchId());
		HotelInventoryBookingService bookingService = HotelInventoryBookingService.builder().hInfo(hInfo).order(order).searchQuery(searchQuery)
				.sourceConfigOutput(sourceConfigOutput).supplierConf(this.getSupplierConf()).ratePlanManager(ratePlanManager)
				.roomOccupancyRoomTypeInfoMap(HotelUtils.getRoomOccupancyRoomTypeInfoMap()).build();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		// TODO Auto-generated method stub
		return true;
	}

}
