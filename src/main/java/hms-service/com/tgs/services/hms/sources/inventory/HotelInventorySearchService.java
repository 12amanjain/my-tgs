package com.tgs.services.hms.sources.inventory;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.inventory.HotelInventoryCacheData;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.helper.HotelCacheHandler;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@SuperBuilder
public class HotelInventorySearchService extends HotelInventoryBaseService {

	private HotelCacheHandler hotelCacheHandler;

	public HotelSearchResult getResultOfAllPropertyIds(Set<String> propertyIds) throws IOException {

		HotelSearchResult finalSearchResult = new HotelSearchResult();
		if (CollectionUtils.isEmpty(propertyIds))
			return finalSearchResult;
		try {
			List<String> cacheSearchKeyList = getCacheSearchKeyList(propertyIds);
			List<HotelInventoryCacheData> ratePlans =
					hotelCacheHandler.getHotelInventoryCachedDatafromKeys(cacheSearchKeyList);

			filterRequiredRatePlan(ratePlans);
			Map<String, List<HotelInventoryCacheData>> supplierHotelIdKeyRatePlanListValue =
					ratePlans.stream().collect(Collectors.groupingBy(HotelInventoryCacheData::getSupplierHotelId));
			List<HotelInfo> hInfoList = getHotelList(supplierHotelIdKeyRatePlanListValue);

			finalSearchResult.setHotelInfos(hInfoList);
			return finalSearchResult;
		} finally {
			/*
			 * Logging
			 */
		}
	}


	private List<String> getCacheSearchKeyList(Set<String> propertyIds) {

		List<String> keys = new ArrayList<>();
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());

		for (String hotelId : propertyIds) {
			String supplierId = hotelId.split("-")[0];
			if (isSupplierEnabled(supplierId)) {
				for (int i = 0; i < numberOfNights; i++) {
					LocalDate date = searchQuery.getCheckinDate().plusDays(i);
					String key = hotelId + date.toString() + supplierId;
					keys.add(key);
				}
			}
		}
		return keys;
	}

	private List<HotelInfo> getHotelList(
			Map<String, List<HotelInventoryCacheData>> supplierHotelIdKeyRatePlanListValue) {

		/*
		 * We can make it concurrent also.
		 */
		List<HotelInfo> hInfoList = new ArrayList<>();
		supplierHotelIdKeyRatePlanListValue.keySet().forEach(supplierHotelId -> {

			List<HotelInventoryCacheData> inventoryDataForSupplierHotelId =
					supplierHotelIdKeyRatePlanListValue.get(supplierHotelId);
			List<HotelRatePlan> ratePlanList = new ArrayList<>();
			inventoryDataForSupplierHotelId.forEach(cacheData -> ratePlanList.addAll(cacheData.getRatePlanList()));
			List<Option> optionList = fetchOptionListForHotel(ratePlanList);
			if (CollectionUtils.isEmpty(optionList))
				return;
			LocalDateTime currentTime = LocalDateTime.now();
			HotelMiscInfo miscInfo = HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
					.searchKeyExpiryTime(currentTime.plusMinutes(30l)).supplierStaticHotelId(supplierHotelId)
					.isMealAlreadyMapped(true).build();
			HotelInfo hInfo = HotelInfo.builder().miscInfo(miscInfo).options(optionList).build();
			hInfoList.add(hInfo);
		});
		return hInfoList;

	}
}
