package com.tgs.services.hms.sources.inventory;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;

import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelRoomInfoSaveManager;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import com.tgs.services.hms.utils.HotelUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class InventoryHotelInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	HMSCachingServiceCommunicator cachingCommunicator;
	@Autowired
	HotelCacheHandler hotelCacheHandler;
	
	@Autowired
	HotelRoomInfoSaveManager roomInfoManager;

	public InventoryHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException, JAXBException {

		Set<String> propertyIds =
				getPropertyIdsOfCity(HotelSourceType.OFFLINEINVENTORY.name());
		
		HotelInventorySearchService inventorySearchService =
				HotelInventorySearchService.builder().searchQuery(searchQuery)
						.supplierConf(supplierConf).hotelCacheHandler(hotelCacheHandler)
						.roomOccupancyRoomTypeInfoMap(HotelUtils.getRoomOccupancyRoomTypeInfoMap()).build();
		
		/*
		 * Send only those number of propertyIds which can be efficiently handled in one go
		 * & we can exploit concurrency here
		 */
		searchResult = inventorySearchService.getResultOfAllPropertyIds(propertyIds);

	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException, JAXBException {
		/*
		 * Nothing As currently we are fetching everything in search only.
		 */
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option option = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(option.getCancellationPolicy());
			}
		});
		
	}
}
