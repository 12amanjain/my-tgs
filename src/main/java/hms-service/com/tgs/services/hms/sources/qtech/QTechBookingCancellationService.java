package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.time.Instant;
import java.time.ZoneId;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.HotelBookingCancellationRequest;
import com.tgs.services.hms.datamodel.qtech.HotelBookingCancellationResponse;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Service
public class QTechBookingCancellationService {

	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelBookingCancellationResponse response;
	private Order order;
	private HotelInfo hInfo;
	protected RestAPIListener listener;

	public boolean cancelBooking() throws IOException {

		HttpUtils httpUtils = null;
		HotelBookingCancellationRequest request = null;
		boolean isCancelled = false;
		try {
			listener = new RestAPIListener("");
			request = createBookingCancellationRequest();
			supplierConf.getHotelSupplierCredentials().setUrl(sourceConfigOutput.getHotelUrls().get(0));
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			response = httpUtils.getResponse(HotelBookingCancellationResponse.class).orElse(null);
			isCancelled = isBookingCancelled();
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			if (ObjectUtils.isEmpty(response)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			}
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
					.type("Qtech-BookingCancellation-Req")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Qtech-BookingCancellation-Res")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
		return isCancelled;
	}

	private HotelBookingCancellationRequest createBookingCancellationRequest() {
		HotelBookingCancellationRequest request = new HotelBookingCancellationRequest();
		request.setAction(QTechConstants.ACTION_BC.getValue());
		request.setBooking_id(hInfo.getMiscInfo().getSupplierBookingId());
		request.setGzip("no");
		return request;
	}

	private boolean isBookingCancelled() {

		boolean isBookingCancelled = false;
		if (response == null) {
			log.error("Error while booking cancellation , response is null for bookingId {} ", order.getBookingId());
			return isBookingCancelled;
		} else if (response.getMessage().equalsIgnoreCase("success")) {
			log.info("Booking cancellation successful on Qtech for bookingId {} messageInfo {} ", order.getBookingId(),
					response.getMessageInfo());
			isBookingCancelled = true;
		} else if (response.getMessageInfo()
				.equalsIgnoreCase("Booking Cannot Be Cancelled, As It Is Already Cancelled.")) {
			log.info("Booking already cancelled on Qtech for bookingId {} messageInfo {} ", order.getBookingId(),
					response.getMessageInfo());
			isBookingCancelled = true;
		} else if (response.getMessage().equalsIgnoreCase("fail")) {
			log.error(
					"Error while booking cancellation on Qtech for bookingId {}, message {}, supplier {}, username {} ",
					order.getBookingId(), response.getMessageInfo(), this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
		}
		return isBookingCancelled;

	}

}
