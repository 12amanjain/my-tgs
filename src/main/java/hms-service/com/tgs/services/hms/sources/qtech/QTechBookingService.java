
package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;

import com.google.gson.Gson;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.HotelBookingRequest;
import com.tgs.services.hms.datamodel.qtech.HotelBookingResponse;
import com.tgs.services.hms.datamodel.qtech.RoomDetails;
import com.tgs.services.hms.datamodel.qtech.Travellers;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class QTechBookingService {

	private HotelInfo hotel;
	private Order order;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelBookingResponse response;
	protected RestAPIListener listener;

	public boolean book() throws IOException {
		HttpUtils httpUtils = null;
		HotelBookingRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = createBookingRequest();
			supplierConf.getHotelSupplierCredentials().setUrl(sourceConfigOutput.getHotelUrls().get(0));
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			response = httpUtils.getResponse(HotelBookingResponse.class).orElse(null);
			log.info("Qtech Booking Response is :{}", response);
			boolean bookingStatus = updateBookingStatus();
			return bookingStatus;
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(response)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			}
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getUrlString())
					.type("Qtech-BookingReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("Qtech-BookingRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	public boolean updateBookingStatus() throws IOException {

		boolean isBooked = false;
		if (StringUtils.isNotEmpty(response.getMessage()) && response.getMessage().equals("fail")) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Booking is failed due to " + response.getMessageInfo());
			log.error("Hotel Booking failed from supplier {} for bookingId {} due to {}",
					this.supplierConf.getBasicInfo().getSupplierName(), order.getBookingId(),
					response.getMessageInfo());
		}
		
		if(response == null || response.getBookingDetail() == null 
				|| StringUtils.isEmpty(response.getBookingDetail().getCurrentStatus())) {
			return isBooked;
			}

		if(response == null || response.getBookingDetail() == null 
				|| StringUtils.isEmpty(response.getBookingDetail().getCurrentStatus())) {
			return isBooked;
			}
		
		String bookingStatus = response.getBookingDetail().getCurrentStatus();
		if (bookingStatus.equalsIgnoreCase("vouchered")) {
			updateSupplierBookingReferenceId();
			isBooked = true;
		} else
			updateSupplierBookingReferenceId();

		return isBooked;
	}

	public void updateSupplierBookingReferenceId() {

		hotel.getMiscInfo().setSupplierBookingId(response.getBookingDetail().getId());
		hotel.getMiscInfo().setSupplierBookingReference(response.getBookingDetail().getBookingReference());

	}

	private HotelBookingRequest createBookingRequest() {
		
		String totalBookingAmount = hotel.getOptions().get(0).getMiscInfo().getExpectedOptionBookingPrice();
		HotelBookingRequest request = new HotelBookingRequest();
		request.setAction(QTechConstants.ACTION_B.getValue());
		request.setUnique_id(hotel.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		request.setHotel_id(hotel.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setAgent_ref_no(order.getBookingId());
		request.setSection_unique_id(hotel.getOptions().get(0).getId());
		request.setGzip("no");
		request.setExpected_price(totalBookingAmount);
		request.setRoomDetails(String.valueOf(new Gson().toJson(createRoomDetails())));
		return request;

	}

	private List<RoomDetails> createRoomDetails() {

		List<RoomDetails> roomDetailList = new ArrayList<>();
		for (RoomInfo roomInfo : hotel.getOptions().get(0).getRoomInfos()) {
			List<Travellers> travellerList = new ArrayList<>();
			for (TravellerInfo travellerInfo : roomInfo.getTravellerInfo()) {
				Travellers traveller = Travellers.builder().salutation(travellerInfo.getTitle())
						.first_name(travellerInfo.getFirstName()).last_name(travellerInfo.getLastName())
						.age(travellerInfo.getAge()).build();
				if (travellerInfo.getPaxType().equals(PaxType.CHILD)) {
					traveller.setSalutation("Child");
				}
				travellerList.add(traveller);
			}
			RoomDetails roomDetail = RoomDetails.builder().numberOfChilds(roomInfo.getNumberOfChild().toString())
					.passangers(travellerList).roomClassId(roomInfo.getId()).build();
			roomDetailList.add(roomDetail);
		}
		return roomDetailList;
	}
}
