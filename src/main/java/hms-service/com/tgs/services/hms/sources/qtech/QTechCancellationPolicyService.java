package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelCancellationPolicyResult;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.InstructionType;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.BookingAllowedInfo;
import com.tgs.services.hms.datamodel.qtech.CancellationPolicyRequest;
import com.tgs.services.hms.datamodel.qtech.CancellationPolicyResponse;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class QTechCancellationPolicyService {

	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelSupplierConfiguration supplierConf;
	private HotelCancellationPolicyResult result;
	private CancellationPolicyResponse searchResponse;
	private HotelInfo hInfo;
	private String searchId;
	private LocalDateTime deadlineDateTime;
	private HttpUtils httpUtils;
	protected RestAPIListener listener;

	public void searchCancellationPolicy(String logKey) throws IOException {
		listener = new RestAPIListener("");
		CancellationPolicyRequest request = null;
		searchId = HotelUtils.getSearchId(hInfo.getId());
		try {
			request = createRequest();
			supplierConf.getHotelSupplierCredentials().setUrl(sourceConfigOutput.getHotelUrls().get(0));
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			searchResponse = httpUtils.getResponse(CancellationPolicyResponse.class).orElse(null);
			createResponse();
			SystemContextHolder.getContextData()
					.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
							.subType(HotelFlowType.CANCELLATION.name()).time(System.currentTimeMillis()).build());
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			httpUtils.getCheckPoints().stream()
					.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.CANCELLATION.name()));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(searchResponse)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			}
			listener.addLog(LogData.builder().key(logKey).logData(httpUtils.getUrlString())
					.type("Qtech-CancellationReq-" + hInfo.getOptions().get(0).getId())
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(logKey).logData(httpUtils.getResponseString())
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.type("Qtech-CancellationRes-" + hInfo.getOptions().get(0).getId()).build());
		}
	}

	public CancellationPolicyRequest createRequest() {
		CancellationPolicyRequest request = new CancellationPolicyRequest();
		request.setAction(QTechConstants.ACTION_C.getValue());
		request.setHotel_id(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setUnique_id(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		request.setSection_unique_id(hInfo.getOptions().get(0).getId());
		request.setGzip("no");
		return request;
	}

	public void createResponse() {
		result = new HotelCancellationPolicyResult();
		HotelCancellationPolicy cancellationPolicy = this.getCancellationPolicy();
		if (cancellationPolicy != null) {
			cancellationPolicy.setId(hInfo.getOptions().get(0).getId());
			hInfo.getOptions().get(0).setCancellationPolicy(cancellationPolicy);
			hInfo.getOptions().get(0).setDeadlineDateTime(deadlineDateTime);
			result.setHotel(hInfo);

		}
	}

	private HotelCancellationPolicy getCancellationPolicy() {

		BookingAllowedInfo bookingAllowedInfo = getBookingAllowedInfo();
		boolean isError = false;
		if (searchResponse == null) {
			log.error("Cancellation policy is empty");
			SystemContextHolder.getContextData().getErrorMessages().add("Cancellation policy is empty");
			isError = true;
		}

		if (searchResponse != null && searchResponse.getMessage().equalsIgnoreCase("fail")) {
			if (ObjectUtils.isEmpty(searchResponse.getMessageInfo())) {
				log.error(searchResponse.getBookingAllowedInfo().getMessageInfo());
				SystemContextHolder.getContextData().getErrorMessages()
						.add(searchResponse.getBookingAllowedInfo().getMessageInfo());
			} else {
				log.error(searchResponse.getMessageInfo());
				SystemContextHolder.getContextData().getErrorMessages().add(searchResponse.getMessageInfo());
			}
			isError = true;
		}

		if (searchResponse.getMessage().equalsIgnoreCase("success")
				&& StringUtils.isBlank(searchResponse.getAppliedAgentCharges())) {
			log.info("Applied agent charges are empty");
			SystemContextHolder.getContextData().getErrorMessages().add("Applied agent charges are empty");
			isError = true;
		}

		if (isError) {
			return null;
		}

		boolean isBookingAllowed = true;
		boolean isSoldOut = false;
		boolean isFullRefundAllowed = true;

		if (bookingAllowedInfo != null && bookingAllowedInfo.getBookingAllowed().equalsIgnoreCase("no")) {
			isBookingAllowed = false;
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Qtech booking not allowed due to " + bookingAllowedInfo.getMessageInfo());
			log.info(
					"Qtech booking not allowed Further due to  {}  isBookingAllowed-{} for searchId {} for supplier {}"
							+ " username {}",
					bookingAllowedInfo.getMessageInfo(), isBookingAllowed, searchId,
					this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
			return null;
		}
		if (bookingAllowedInfo != null && bookingAllowedInfo.getSoldOut().equalsIgnoreCase("yes")) {
			isSoldOut = true;
			SystemContextHolder.getContextData().getErrorMessages()
					.add("Qtech booking not allowed due to due to " + bookingAllowedInfo.getMessageInfo());
			log.info(
					"Qtech booking not allowed Further due to {} isSoldOut-{}  for searchId {} for supplier {}"
							+ "for username {}",
					bookingAllowedInfo.getMessageInfo(), isSoldOut, searchId,
					this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
			return null;
		}

		if (Double.valueOf(searchResponse.getAppliedAgentCharges()) <= 0) {
			SystemContextHolder.getContextData().getErrorMessages().add("Got wrong cancellation charges from Qtech");
			log.info("Got wrong cancellation charges from Qtech i.e. {} for searchId {} ",
					searchResponse.getAppliedAgentCharges(), searchId);
		}

		List<PenaltyDetails> list = new ArrayList<PenaltyDetails>();
		int days = (int) Math.ceil(Double.valueOf((searchResponse.getCancellationHours())) / 24);
		LocalDate checkInDate = hInfo.getOptions().get(0).getRoomInfos().get(0).getCheckInDate();
		LocalDate targetDate = checkInDate.minusDays(days + 1);
		LocalDateTime currentTime = LocalDateTime.now();
		Double cancellationCharges = Double.valueOf(searchResponse.getAppliedAgentCharges());
		if (cancellationCharges == 0.0 && CollectionUtils.isNotEmpty(hInfo.getOptions())) {
			cancellationCharges = hInfo.getOptions().get(0).getTotalPrice();
		}
		if (days <= 0 || targetDate.atTime(12, 00).isBefore(currentTime)
				|| targetDate.atTime(12, 00).isEqual(currentTime)) {
			isFullRefundAllowed = false;
			LocalDateTime fromDateTime = currentTime;
			if (currentTime.isAfter(checkInDate.atTime(12, 00))) {
				fromDateTime = checkInDate.atTime(12, 00);
			}
			deadlineDateTime = fromDateTime;
			PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(fromDateTime)
					.toDate(checkInDate.atTime(12, 00)).penaltyAmount(cancellationCharges).build();
			list.add(penaltyDetails1);
		} else {
			deadlineDateTime = targetDate.atTime(12, 00);
			PenaltyDetails penaltyDetails1 = PenaltyDetails.builder().fromDate(currentTime)
					.toDate(targetDate.atTime(12, 00)).penaltyAmount(0.0).build();
			if (cancellationCharges == 0.0) {
				cancellationCharges = hInfo.getOptions().get(0).getTotalPrice();
			}
			PenaltyDetails penaltyDetails2 = PenaltyDetails.builder().fromDate(targetDate.atTime(12, 00))
					.toDate(checkInDate.atTime(12, 00)).penaltyAmount(cancellationCharges).build();
			list.add(penaltyDetails1);
			list.add(penaltyDetails2);
		}
		
		if(StringUtils.isBlank(hInfo.getOptions().get(0).getInstructionFromType(InstructionType.BOOKING_NOTES))) {
			hInfo.getOptions().get(0).getInstructions().add(Instruction.builder()
					.type(InstructionType.BOOKING_NOTES).msg(searchResponse.getContractComment()).build());
		}
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
				.isFullRefundAllowed(isFullRefundAllowed).penalyDetails(list)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(isBookingAllowed).isSoldOut(isSoldOut).build())
				.build();
		updateExpectedBookingAmountInOption();
		return cancellationPolicy;
	}

	private BookingAllowedInfo getBookingAllowedInfo() {

		if (searchResponse != null && searchResponse.getBookingAllowedInfo() != null) {
			return searchResponse.getBookingAllowedInfo();
		}
		return null;

	}
	
	private void updateExpectedBookingAmountInOption() {
		
		OptionMiscInfo optionMiscInfo = hInfo.getOptions().get(0).getMiscInfo();
		optionMiscInfo.setExpectedOptionBookingPrice(searchResponse.getTotalBookingAmount());
	}

}
