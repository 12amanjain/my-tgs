package com.tgs.services.hms.sources.qtech;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class QTechHotelBookingFactory extends AbstractHotelBookingFactory {

	public QTechHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException {
		QTechBookingService bookingService = QTechBookingService.builder().hotel(this.getHotel()).order(order).supplierConf(supplierConf)
				.sourceConfigOutput(sourceConfigOutput).build();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		return true;
	}

}
