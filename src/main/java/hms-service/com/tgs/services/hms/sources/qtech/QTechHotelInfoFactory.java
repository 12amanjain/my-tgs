package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QTechHotelInfoFactory extends AbstractHotelInfoFactory {

	public QTechHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	public void searchAvailableHotels() throws IOException {
		QtechSearchService searchService =
				QtechSearchService.builder().supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
						.supplierCityInfo(supplierCityInfo).sourceConfigOutput(sourceConfigOutput).build();
		searchService.doSearch();
		searchResult = searchService.getSearchResult();
	}

	@Override
	protected void searchHotel(HotelInfo hInfo , HotelSearchQuery searchQuery) throws IOException {
		QtechSearchService searchService = QtechSearchService.builder().sourceConfigOutput(sourceConfigOutput)
				.supplierConf(this.getSupplierConf()).hInfo(hInfo).build();
		searchService.doDetailSearch();
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		QTechCancellationPolicyService cancellationPolicyService = QTechCancellationPolicyService.builder().hInfo(hotel)
				.supplierConf(this.getSupplierConf()).sourceConfigOutput(sourceConfigOutput).build();
		cancellationPolicyService.searchCancellationPolicy(logKey);
		
	}
}
