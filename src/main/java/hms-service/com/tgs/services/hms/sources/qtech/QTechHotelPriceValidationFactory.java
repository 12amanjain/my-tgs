package com.tgs.services.hms.sources.qtech;

import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class QTechHotelPriceValidationFactory extends AbstractHotelPriceValidationFactory {

	public QTechHotelPriceValidationFactory(HotelSearchQuery searchQuery, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String bookingId) {
		super(searchQuery, hotel, hotelSupplierConf, bookingId);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {
		Option oldOption = hInfo.getOptions().get(0);
		QtechHotelPriceValidationService priceValidationService = QtechHotelPriceValidationService.builder()
				.searchQuery(this.searchQuery).option(oldOption).sourceConfigOutput(sourceConfigOutput)
				.bookingId(bookingId).supplierConf(this.getSupplierConf()).build();
		priceValidationService.fetchPriceChanges();
		return priceValidationService.getUpdatedOption();
	}
}
