package com.tgs.services.hms.sources.qtech;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.api.client.repackaged.com.google.common.base.Objects;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.Address.AddressBuilder;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.City.CityBuilder;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.Country.CountryBuilder;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelFlowType;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelInfo.HotelInfoBuilder;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo.HotelMiscInfoBuilder;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.Option.OptionBuilder;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.BookingDetail;
import com.tgs.services.hms.datamodel.qtech.BookingRoomRates;
import com.tgs.services.hms.datamodel.qtech.HotelBookingResponse;
import com.tgs.services.hms.datamodel.qtech.HotelImportBookingRequest;
import com.tgs.services.hms.datamodel.qtech.Passengers;
import com.tgs.services.hms.datamodel.qtech.RoomDetail;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
@Builder
public class QTechRetrieveBookingService {

	private HotelImportBookingParams importBookingParams;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelBookingResponse response;
	private RestAPIListener listener;
	private HotelImportedBookingInfo bookingInfo;
	private LocalDateTime deadlineDateTime;

	public void retrieveBooking() throws IOException {
		HttpUtils httpUtils = null;
		HotelImportBookingRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = createGetBookingRequest();
			supplierConf.getHotelSupplierCredentials().setUrl(sourceConfigOutput.getHotelUrls().get(0));
			httpUtils = QTechUtil.getResponseURL(request, supplierConf, sourceConfigOutput);
			response = httpUtils.getResponse(HotelBookingResponse.class).orElse(null);
			bookingInfo = createBookingResponse(response);
			log.info("Qtech Fetch Booking Response is :{}", response);
			SystemContextHolder.getContextData()
					.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name())
							.subType(HotelFlowType.BOOKING_DETAIL.name()).time(System.currentTimeMillis()).build());
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			httpUtils.getCheckPoints().stream()
					.forEach(checkpoint -> checkpoint.setSubType(HotelFlowType.BOOKING_DETAIL.name()));
			SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
			if (ObjectUtils.isEmpty(response)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			}
			listener.addLog(LogData.builder().key(importBookingParams.getBookingId()).logData(httpUtils.getUrlString())
					.type("Qtech-GetBookingReq")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
			listener.addLog(LogData.builder().key(importBookingParams.getBookingId())
					.logData(httpUtils.getResponseString()).type("Qtech-GetBookingRes")
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());

		}
	}

	private HotelImportBookingRequest createGetBookingRequest() {
		HotelImportBookingRequest request = new HotelImportBookingRequest();
		request.setAction(QTechConstants.ACTION_BD.getValue());
		request.setBooking_id(importBookingParams.getSupplierBookingId());
		request.setGzip("no");
		return request;
	}

	private HotelImportedBookingInfo createBookingResponse(HotelBookingResponse bookingResponse) {
		if (bookingResponse.getMessage().equals("ERR_CODE_NO_RESULT")) {
			throw new CustomGeneralException(SystemError.INVALID_SUPPLIER_BOOKING_ID);
		}

		if (StringUtils.isNotEmpty(bookingResponse.getSuccess()) && bookingResponse.getSuccess().equals("fail")) {
			log.error("Unable to get booking result from QTech due to {} for supplier {} with username {}",
					bookingResponse.getMessage(), this.supplierConf.getBasicInfo().getSupplierName(),
					this.supplierConf.getHotelSupplierCredentials().getUserName());
		}

		BookingDetail bookingDetail = bookingResponse.getBookingDetail();
		HotelInfo hotelInfo = setHotelInfo(bookingDetail);
		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(LocalDate.parse(bookingDetail.getCheckInDate()));
		searchQueryBuilder.checkoutDate(LocalDate.parse(bookingDetail.getCheckOutDate()));
		searchQueryBuilder.sourceId(HotelSourceType.QTECH.getSourceId());
		searchQueryBuilder.miscInfo(
				HotelSearchQueryMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName()).build());
		HotelSearchPreferences searchPreferences = new HotelSearchPreferences();
		searchPreferences.setCurrency(bookingDetail.getCurrencyCode());
		searchQueryBuilder.searchPreferences(searchPreferences);
		DeliveryInfo deliveryInfo = new DeliveryInfo();
		deliveryInfo.setContacts(Arrays.asList(bookingDetail.getPassengerPhone()));
		deliveryInfo.setEmails(Arrays.asList(bookingDetail.getPassengerEmail()));
		deliveryInfo.cleanData();
		String orderStatus = "";
		if (StringUtils.isNotBlank(bookingDetail.getCurrentStatus())) {
			try {
				orderStatus = QTechOrderMapping.valueOf(bookingDetail.getCurrentStatus().toUpperCase()).getCode();
			} catch (IllegalArgumentException e) {
			}
		}
		return HotelImportedBookingInfo.builder().hInfo(hotelInfo).searchQuery(searchQueryBuilder.build())
				.deliveryInfo(deliveryInfo).orderStatus(orderStatus)
				.bookingCurrencyCode(bookingDetail.getCurrencyCode()).build();
	}

	private HotelInfo setHotelInfo(BookingDetail bookingDetail) {
		HotelInfoBuilder hotelInfoBuilder = HotelInfo.builder();
		AddressBuilder addressBuilder = Address.builder();
		addressBuilder.addressLine1(bookingDetail.getHotelAddress1());
		CityBuilder cityBuilder = City.builder();
		cityBuilder.name(bookingDetail.getCityId());
		addressBuilder.city(cityBuilder.build());

		CountryBuilder countryBuilder = Country.builder();
		countryBuilder.name(bookingDetail.getCountryName());
		addressBuilder.country(countryBuilder.build());

		hotelInfoBuilder.address(addressBuilder.build());
		HotelMiscInfoBuilder hotelMiscInfoBuilder = HotelMiscInfo.builder();
		hotelMiscInfoBuilder.supplierBookingId(importBookingParams.getBookingId());
		hotelMiscInfoBuilder.supplierStaticHotelId(bookingDetail.getLocalHotelId());
		hotelMiscInfoBuilder.supplierBookingReference(bookingDetail.getBookingReference());
		hotelInfoBuilder.miscInfo(hotelMiscInfoBuilder.build());
		hotelInfoBuilder.name(bookingDetail.getHotelName());

		List<Option> optionList = new ArrayList<>();
		OptionBuilder optionBuilder = Option.builder();
		List<RoomDetail> roomDetailList = bookingDetail.getRoomDetail();
		HotelCancellationPolicy cancellationPolicy = getCancellationPolicy(bookingDetail);
		List<RoomInfo> roomInfoList = populateRoomInfo(bookingDetail, roomDetailList, cancellationPolicy);
		optionBuilder.deadlineDateTime(deadlineDateTime);
		optionBuilder.miscInfo(OptionMiscInfo.builder().supplierId(importBookingParams.getSupplierId())
				.supplierHotelId(bookingDetail.getLocalHotelId()).build());
		optionBuilder.roomInfos(roomInfoList);
		Double totalPrice = roomInfoList.stream().mapToDouble(RoomInfo::getTotalPrice).sum();
		optionBuilder.totalPrice(totalPrice);
		if (cancellationPolicy.getPenalyDetails().get(0).getPenaltyAmount() == null) {
			cancellationPolicy.getPenalyDetails().get(0).setPenaltyAmount(totalPrice);
		}
		optionBuilder.deadlineDateTime(LocalDateTime.parse(bookingDetail.getExpirationDate(),
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss")));
		optionList.add(optionBuilder.build());
		hotelInfoBuilder.options(optionList);
		return hotelInfoBuilder.build();
	}

	public List<RoomInfo> populateRoomInfo(BookingDetail bookingDetail, List<RoomDetail> roomDetailList,
			HotelCancellationPolicy cancellationPolicy) {
		List<RoomInfo> roomInfoList = new ArrayList<>();

		for (RoomDetail roomDetail : roomDetailList) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setId(Objects.firstNonNull(roomInfo.getId(), TgsStringUtils.generateRandomNumber(8, "id_")));
			roomInfo.setRoomType(roomDetail.getRoomTypeDescription());
			roomInfo.setRoomCategory(roomDetail.getRoomTypeDescription());
			roomInfo.setCancellationPolicy(cancellationPolicy);
			int dayCount = 1;
			Double totalRoomPrice = 0.0;
			List<PriceInfo> priceInfoList = new ArrayList<>();

			for (BookingRoomRates roomRate : roomDetail.getRoomRates()) {
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				Double nightlyPrice = Double.parseDouble(roomRate.getDisplayNightlyRate());
				fareComponents.put(HotelFareComponent.BF, nightlyPrice);
				priceInfo.setDay(dayCount++);
				priceInfo.setFareComponents(fareComponents);
				totalRoomPrice += nightlyPrice;
				priceInfoList.add(priceInfo);
			}
			roomInfo.setCheckInDate(LocalDate.parse(bookingDetail.getCheckInDate()));
			roomInfo.setCheckOutDate(LocalDate.parse(bookingDetail.getCheckOutDate()));
			roomInfo.setTotalPrice(totalRoomPrice);
			roomInfo.setDeadlineDateTime(deadlineDateTime);
			roomInfo.getTotalFareComponents().put(HotelFareComponent.BF, totalRoomPrice);
			roomInfo.setPerNightPriceInfos(priceInfoList);
			int perRoomPassenger = roomDetail.getPassengers().size() / (Integer.parseInt(roomDetail.getNumberOfRoom()));
			int passengerIndexOffset = 0;
			int passengerIndex;
			for (int roomIndex = 0; roomIndex < Integer.parseInt(roomDetail.getNumberOfRoom()); roomIndex++) {
				RoomInfo copyRoomInfo = new GsonMapper<>(roomInfo, RoomInfo.class).convert();
				List<TravellerInfo> travellerInfoList = new ArrayList<>();
				int adultCount = 0;
				int childCount = 0;
				for (passengerIndex = passengerIndexOffset; passengerIndex < (passengerIndexOffset
						+ perRoomPassenger); passengerIndex++) {
					if (roomDetail.getPassengers().size() == passengerIndex) {
						break;
					}
					Passengers passenger = roomDetail.getPassengers().get(passengerIndex);
					TravellerInfo travellerInfo = new TravellerInfo();
					travellerInfo.setTitle(passenger.getSalutation());
					travellerInfo.setFirstName(passenger.getFirstName());
					travellerInfo.setLastName(passenger.getLastName());

					String passengerType = passenger.getPassengerType().trim().toLowerCase();
					if (passengerType.equals("adult")) {
						travellerInfo.setPaxType(PaxType.ADULT);
						adultCount++;
					} else {
						travellerInfo.setPaxType(PaxType.CHILD);
						childCount++;
						if (!StringUtils.isEmpty(passenger.getAge())) {
							travellerInfo.setAge(Integer.parseInt(passenger.getAge()));
						}
					}
					if (!StringUtils.isEmpty(passenger.getAge()))
						travellerInfo.setAge(Integer.parseInt(passenger.getAge()));
					travellerInfoList.add(travellerInfo);
				}
				passengerIndexOffset = passengerIndex;
				copyRoomInfo.setNumberOfAdults(adultCount);
				copyRoomInfo.setNumberOfChild(childCount);
				copyRoomInfo.setTravellerInfo(travellerInfoList);
				roomInfoList.add(copyRoomInfo);

			}

		}
		return roomInfoList;
	}

	public HotelCancellationPolicy getCancellationPolicy(BookingDetail bookingDetail) {
		LocalDateTime expirationDate = LocalDateTime.parse(bookingDetail.getExpirationDate(),
				DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
		LocalDateTime checkinDate = LocalDate.parse(bookingDetail.getCheckInDate()).atTime(12, 00);
		HotelCancellationPolicy cancellationPolicy = null;
		if (expirationDate.isBefore(checkinDate)) {
			List<PenaltyDetails> penaltyDetailsList = new ArrayList<>();
			PenaltyDetails penaltyDetails = PenaltyDetails.builder().build();
			penaltyDetails.setFromDate(expirationDate);
			penaltyDetails.setToDate(checkinDate);
			penaltyDetails.setPenaltyAmount(Double.parseDouble(bookingDetail.getAgentRate()));
			penaltyDetailsList.add(penaltyDetails);
			cancellationPolicy = HotelCancellationPolicy.builder().isFullRefundAllowed(false)
					.penalyDetails(penaltyDetailsList).build();
			deadlineDateTime = expirationDate;
		} else {
			List<PenaltyDetails> penaltyDetailsList = new ArrayList<>();
			PenaltyDetails penaltyDetails = PenaltyDetails.builder().build();
			penaltyDetails.setFromDate(checkinDate);
			penaltyDetails.setToDate(expirationDate);
			penaltyDetails.setPenaltyAmount(0.0);
			penaltyDetailsList.add(penaltyDetails);
			cancellationPolicy = HotelCancellationPolicy.builder().isFullRefundAllowed(true)
					.penalyDetails(penaltyDetailsList).build();
			deadlineDateTime = expirationDate;
		}
		cancellationPolicy.setMiscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build());
		return cancellationPolicy;
	}

}
