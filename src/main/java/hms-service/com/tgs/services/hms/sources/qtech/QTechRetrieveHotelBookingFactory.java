package com.tgs.services.hms.sources.qtech;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractRetrieveHotelBookingFactory;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;

@Service
public class QTechRetrieveHotelBookingFactory extends AbstractRetrieveHotelBookingFactory {

	public QTechRetrieveHotelBookingFactory(HotelSupplierConfiguration supplierConf,
			HotelImportBookingParams importBookingInfo) {
		super(supplierConf, importBookingInfo);
	}

	@Override
	public void retrieveBooking() throws IOException {
		QTechRetrieveBookingService bookingService = QTechRetrieveBookingService.builder().supplierConf(supplierConf)
				.sourceConfigOutput(sourceConfigOutput).importBookingParams(importBookingParams).build();
		bookingService.retrieveBooking();
		bookingDetailResponse = bookingService.getBookingInfo();
	}
}
