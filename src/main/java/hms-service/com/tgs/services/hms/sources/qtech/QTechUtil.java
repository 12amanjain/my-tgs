package com.tgs.services.hms.sources.qtech;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.PriceInfoQtech;
import com.tgs.services.hms.datamodel.qtech.QTechBaseRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierCredential;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;

public class QTechUtil {

	public static <T> HttpUtils getResponseURL(QTechBaseRequest request, HotelSupplierConfiguration supplierConf,
			HotelSourceConfigOutput sourceConfigOutput) {
		populateCommonRequestParameters(request, supplierConf);
		request.setGzip(sourceConfigOutput.isGzipEnabled() ? "yes" : "no");
		HotelSupplierCredential supplierCredential = supplierConf.getHotelSupplierCredentials();
		Map<String, String> queryParams = convertToMap(request);
		HttpUtils httpUtils =
				HttpUtils.builder().urlString(supplierCredential.getUrl()).proxy(HotelUtils.getProxyFromConfigurator())
						.queryParams(queryParams).headerParams(request.getHeaderParams()).build();
		return httpUtils;
	}

	private static void populateCommonRequestParameters(QTechBaseRequest baseRequest,
			HotelSupplierConfiguration supplierConf) {
		baseRequest.setUsername(supplierConf.getHotelSupplierCredentials().getUserName());
		baseRequest.setPassword(supplierConf.getHotelSupplierCredentials().getPassword());
	}

	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

	public static void populatePriceInRoomInfo(RoomInfo roomInfo, List<PriceInfoQtech> priceInfoQtechList) {
		int i = 1;
		Double total = 0.0;
		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (PriceInfoQtech priceInfoQtech : priceInfoQtechList) {
			PriceInfo priceInfo = new PriceInfo();
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, priceInfoQtech.getDisplayNightlyRate());
			priceInfo.setDay(i);
			priceInfo.setFareComponents(fareComponents);
			total += priceInfoQtech.getDisplayNightlyRate();
			priceInfoList.add(priceInfo);
			i++;
		}
		roomInfo.setTotalPrice(total);
		roomInfo.setPerNightPriceInfos(priceInfoList);
	}
	
}
