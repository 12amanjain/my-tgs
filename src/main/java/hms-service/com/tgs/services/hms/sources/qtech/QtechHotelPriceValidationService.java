package com.tgs.services.hms.sources.qtech;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.qtech.CancellationValidationRequest;
import com.tgs.services.hms.datamodel.qtech.PriceChangeResponse;
import com.tgs.services.hms.datamodel.qtech.RoomRates;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.utils.common.HttpUtils;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class QtechHotelPriceValidationService {

	private HotelSearchQuery searchQuery;
	private Option option;
	private HotelSupplierConfiguration supplierConf;
	private HotelSourceConfigOutput sourceConfigOutput;
	private String bookingId;
	private Option updatedOption;
	private RestAPIListener listener;
	private LocalDateTime currentTime;

	public void fetchPriceChanges() throws Exception {
		HttpUtils httpUtils = null;
		PriceChangeResponse priceChangeResponse = null;
		try {
			listener = new RestAPIListener("");
			CancellationValidationRequest validationRequest = createPriceValidationRequest();
			supplierConf.getHotelSupplierCredentials().setUrl(sourceConfigOutput.getHotelUrls().get(0));
			httpUtils = QTechUtil.getResponseURL(validationRequest, supplierConf, sourceConfigOutput);
			httpUtils.setPrintResponseLog(true);
			priceChangeResponse = httpUtils.getResponse(PriceChangeResponse.class).orElse(null);
			currentTime = LocalDateTime.now();
			updatedOption = createUpdatedOption(priceChangeResponse);
		} finally {
			Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
					.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));
			if (ObjectUtils.isEmpty(priceChangeResponse)) {
				SystemContextHolder.getContextData().getErrorMessages().add(httpUtils.getResponseString());
			}
			listener.addLog(LogData.builder().key(bookingId).logData(httpUtils.getUrlString())
					.type("Qtech-PriceCheckReq-" + supplierConf.getHotelSupplierCredentials().getUserName())
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());

			listener.addLog(LogData.builder().key(bookingId).logData(httpUtils.getResponseString())
					.type("Qtech-PriceCheckRes-" + supplierConf.getHotelSupplierCredentials().getUserName())
					.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
							.atZone(ZoneId.systemDefault()).toLocalDateTime())
					.build());
		}
	}

	private Option createUpdatedOption(PriceChangeResponse priceChangeResponse) {
		Option option = Option.builder().build();
		option.setId(priceChangeResponse.getSectionUniqueId());
		List<RoomInfo> roomInfos = new ArrayList<>();
		for (RoomRates roomRates : priceChangeResponse.getRoomRates()) {
			int numberOfRooms = roomRates.getNumberOfRooms();
			RoomInfo roomInfo = new RoomInfo();
			QTechUtil.populatePriceInRoomInfo(roomInfo, roomRates.getRateBreakup());
			roomInfo.setId(roomRates.getClassUniqueId());
			roomInfos.add(roomInfo);
			if (numberOfRooms > 1) {
				for (int r = 1; r < numberOfRooms; r++) {
					RoomInfo copyRoomInfo =
							GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(roomInfo), RoomInfo.class);
					roomInfos.add(copyRoomInfo);
				}
			}
		}
		option.setRoomInfos(roomInfos);
		return option;
	}

	private CancellationValidationRequest createPriceValidationRequest() throws InterruptedException {
		
		Thread.sleep(2000);
		CancellationValidationRequest validationRequest = new CancellationValidationRequest();
		validationRequest.setAction(QTechConstants.ACTION_CV.getValue());
		validationRequest.setIsValuation("yes");
		validationRequest.setHotel_id(option.getMiscInfo().getSupplierHotelId());
		validationRequest.setUnique_id(option.getMiscInfo().getSupplierSearchId());
		validationRequest.setSection_unique_id(option.getId());
		return validationRequest;
	}
}
