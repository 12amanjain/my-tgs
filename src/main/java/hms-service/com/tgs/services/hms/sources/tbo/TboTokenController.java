package com.tgs.services.hms.sources.tbo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.jparepository.HotelSupplierInfoService;

@RestController
@RequestMapping("/hms/v1/job")
public class TboTokenController {
	
	@Autowired
	private HMSCachingServiceCommunicator cacheService;
	
	@Autowired
	private HotelSupplierInfoService supplierService;
	
	@RequestMapping(value = "/tbo/token", method = RequestMethod.POST)
	protected void storeToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
		
		TravelBoutiqueBaseService baseService = TravelBoutiqueBaseService.builder()
				.cacheService(cacheService).supplierService(supplierService).build();
		baseService.storeToken();
	}

}
