package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.booking.ArrivalTransport;
import com.tgs.services.hms.datamodel.tbo.booking.BookingResult;
import com.tgs.services.hms.datamodel.tbo.booking.ConfirmBookingRequest;
import com.tgs.services.hms.datamodel.tbo.booking.ConfirmBookingResponse;
import com.tgs.services.hms.datamodel.tbo.booking.HotelBookingRequest;
import com.tgs.services.hms.datamodel.tbo.booking.HotelBookingResponse;
import com.tgs.services.hms.datamodel.tbo.booking.HotelConfirmBookingResponse;
import com.tgs.services.hms.datamodel.tbo.booking.TravelBoutiqueBookingDetailRequest;
import com.tgs.services.hms.datamodel.tbo.booking.TravelBoutiqueBookingDetailResponse;
import com.tgs.services.hms.datamodel.tbo.booking.Travellers;
import com.tgs.services.hms.datamodel.tbo.search.BedType;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.utils.common.HttpUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Getter
@Setter
@SuperBuilder
public class TravelBoutiqueBookingService extends TravelBoutiqueBaseService {

	HotelInfo hotel;
	Order order;
	HotelSupplierConfiguration supplierConf;
	protected HotelSourceConfigOutput sourceConfigOutput;
	HotelBookingResponse bookingResponse;
	HotelConfirmBookingResponse confirmBookingResponse;
	protected RestAPIListener listener;
	private boolean isPriceChanged;
	
	//private static final DateTimeFormatter formatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
	
	public boolean book() throws IOException, InterruptedException {
		
		if(hotel.getOptions().get(0).getIsOptionOnRequest()) 
			return false;
		HttpUtils httpUtils = null;
		HotelBookingRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = createBookingRequest();
			log.debug("Request is {}" , GsonUtils.getGson().toJson(request));
			httpUtils = TravelBoutiqueUtil.getRequest(request, supplierConf, supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOK_URL));
			bookingResponse = httpUtils.getResponse(HotelBookingResponse.class).orElse(null);
			log.debug("TBO Booking Response is :{}", bookingResponse);
			boolean bookingStatus = updateBookingStatus();
			return bookingStatus;
		}finally {
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(GsonUtils.getGson().toJson(request))
					.type("TBO-BookingReq").build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("TBO-BookingRes").build());
		}
	}

	private boolean updateBookingStatus() throws IOException, InterruptedException {
		
		boolean isBooked = false;
		if(bookingResponse == null || bookingResponse.getBookResult() == null) {
			log.info("Booking Failed for bookingID {}" , order.getBookingId());
			return isBooked;
		}
		
		BookingResult bookingResult = bookingResponse.getBookResult();
		
		if(bookingResult.getStatus() == 1) {
			isBooked = true;
			updateSupplierBookingReferenceId();
		}else if(bookingResult.getStatus() == 3 && !isPriceChanged) {
			log.info("Price or Cancellation Policy Changed for bookingId {}" , order.getBookingId());
			TravelBoutiqueUtil.updateHotelInfo(hotel  , bookingResponse.getBookResult(),sourceConfigOutput);
			isPriceChanged = true;
			return book();
		}else if(bookingResult.getHotelBookingStatus().equalsIgnoreCase("Pending")) {
			log.info("Booking Pending From Supplier for BookingId {} " , order.getBookingId());
			return checkForBookingStatus();
			
		}else{
			log.info("Booking Failed  for bookingId {} , Booking Status {}", order.getBookingId()
					, bookingResponse.getBookResult().getHotelBookingStatus());
			return checkForBookingStatus();
		}
		callBookingDetail();
		return isBooked;	
	}
	
	private boolean checkForBookingStatus() throws IOException, InterruptedException {
		
		String bookingId = bookingResponse.getBookResult().getBookingId(); 
		TravelBoutiqueBookingDetailRequest bookingDetailRequest = getBookingDetailRequest(bookingId);
		log.debug("Request is {}" , GsonUtils.getGson().toJson(bookingDetailRequest));
		
		HttpUtils httpUtils = TravelBoutiqueUtil.getRequest(bookingDetailRequest, supplierConf
				, supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOKING_DETAIL));
		AtomicBoolean isBookingSuccessful = new AtomicBoolean(false);
		Runnable bookStatusTask = () -> {
			
			int attempts = 1;
			boolean bookingStatus = false;
			TravelBoutiqueBookingDetailResponse detailResponse = null;
			try {
				do {
					detailResponse = httpUtils.getResponse(TravelBoutiqueBookingDetailResponse.class).orElse(null);
					if(detailResponse.getGetBookingDetailResult().getStatus() == 1) {
						bookingStatus = true;
						isBookingSuccessful.set(true);
						log.debug("Booking Detail Response is {}" , httpUtils.getResponseString());
					}else if(detailResponse.getGetBookingDetailResult().getHotelBookingStatus().equalsIgnoreCase("Pending")) {
						Thread.sleep(60000);
						attempts++;
					}else {
						log.info("Booking not confirmed for bookingId {} " , order.getBookingId());
						break;
					}
				}while(!bookingStatus && attempts < 10);
			}catch(Exception e) {
				log.error("Error while fetching Booking Details for bookingID {}" , order.getBookingId());
			}
		};
		
		Thread t = new Thread(bookStatusTask);
		t.start();
		t.join();
		return isBookingSuccessful.get();
	}
	
	private void callBookingDetail() throws IOException {
		
		String bookingId = bookingResponse.getBookResult().getBookingId(); 
		TravelBoutiqueBookingDetailRequest bookingDetailRequest = getBookingDetailRequest(bookingId);
		log.debug("Request is {}" , GsonUtils.getGson().toJson(bookingDetailRequest));
		HttpUtils httpUtils = HttpUtils.builder().urlString(supplierConf.getHotelAPIUrl(HotelUrlConstants.BOOKING_DETAIL))
				.postData(GsonUtils.getGson().toJson(bookingDetailRequest)).headerParams(TravelBoutiqueUtil.getHeaderParams())
				.printResponseLog(true).build();
		TravelBoutiqueBookingDetailResponse detailResponse = httpUtils.getResponse(TravelBoutiqueBookingDetailResponse.class).orElse(null);
		
	}
	
	public void updateSupplierBookingReferenceId() {

		hotel.getMiscInfo().setSupplierBookingId(bookingResponse.getBookResult().getBookingId());
		hotel.getMiscInfo().setSupplierBookingReference(bookingResponse.getBookResult().getBookingRefNo());

	}

	private HotelBookingRequest createBookingRequest() throws IOException {
		
		HotelBookingRequest request =  new HotelBookingRequest();
		request.setEndUserIp(TravelBoutiqueUtil.IP);
		request.setTokenId(getCachedToken());
		request.setTraceId(hotel.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		request.setResultIndex(hotel.getOptions().get(0).getMiscInfo().getResultIndex().toString());
		request.setGuestNationality("IN");
		request.setHotelCode(hotel.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setHotelName(hotel.getName());
		request.setNoOfRooms(String.valueOf(hotel.getOptions().get(0).getRoomInfos().size()));
		request.setClientReferenceNo("123");
		request.setIsVoucherBooking(isConfirmedBooking());
		if(BooleanUtils.isTrue(hotel.getOptions().get(0).getMiscInfo().getIsPackageFare())) {
			request.setIsPackageFare(true);
			if(BooleanUtils.isTrue(hotel.getOptions().get(0).getMiscInfo().getIsPackageDetailsMandatory())) {
				ArrivalTransport transport = new ArrivalTransport();
				transport.setArrivalTransportType(1);
				transport.setTransportInfoId("abc");
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
				LocalDateTime checkInTime = hotel.getOptions().get(0).getRoomInfos().get(0).getCheckInDate().atTime(LocalTime.now());
				String checkInTimeInString = checkInTime.format(formatter);
				transport.setTime(LocalDateTime.parse(checkInTimeInString, formatter));
				request.setArrivalTransport(transport);
			}
		}
		List<HotelRoomDetail> roomdetails = new ArrayList<>();
		for(RoomInfo roomInfo : hotel.getOptions().get(0).getRoomInfos()) {
			HotelRoomDetail roomDetail = new HotelRoomDetail();
			RoomMiscInfo roomMiscInfo = roomInfo.getMiscInfo();
			
			roomDetail.setPrice(TravelBoutiqueUtil.getSupplierPrice(roomMiscInfo.getPrice()));
			roomDetail.setRatePlanCode(roomMiscInfo.getRatePlanCode());
			roomDetail.setRoomIndex(roomMiscInfo.getRoomIndex());
			roomDetail.setRoomTypeCode(roomMiscInfo.getRoomTypeCode());
			roomDetail.setRoomTypeName(roomMiscInfo.getRoomTypeName());
			roomDetail.setSupplements("");
			List<BedType> types = new ArrayList<>();
			BedType bedType = new BedType();
			bedType.setBedTypeCode(1);
			if(!CollectionUtils.isEmpty(roomDetail.getBedTypes())) { types = roomDetail.getBedTypes(); }
			roomDetail.setBedTypes(types);
			List<Travellers> travellerList = new ArrayList<>();
			boolean isLeadPassenger = true;
			for (TravellerInfo travellerInfo : roomInfo.getTravellerInfo()) {	
				int paxType = 1;
				String title = travellerInfo.getTitle();
				if(title.equalsIgnoreCase("master")) {
					paxType = 2;
					title = "Mr";
				}
				Travellers traveller = Travellers.builder().Title(title)
						.FirstName(travellerInfo.getFirstName()).Middlename("").LastName(travellerInfo.getLastName())
						.LeadPassenger(isLeadPassenger).PaxType(paxType).Age(travellerInfo.getAge())
						.PAN(travellerInfo.getPanNumber()).build();
				travellerList.add(traveller);
				if(isLeadPassenger) {
					traveller.setEmail(order.getDeliveryInfo().getEmails().get(0));
					traveller.setPhoneno(order.getDeliveryInfo().getContacts().get(0));
				}
				isLeadPassenger = false;
			}
			roomDetail.setHotelPassenger(travellerList);
			roomdetails.add(roomDetail);
		}
		request.setHotelRoomsDetails(roomdetails);
		return request;	
	}	
	
	private boolean isConfirmedBooking() {
		
		boolean isConfirmedBooking = true;
		if(order.getAdditionalInfo()== null || order.getAdditionalInfo().getPaymentStatus() == null) {
			isConfirmedBooking = false;
		}
		return isConfirmedBooking;
	}
	
	
	public boolean confirmHoldBooking() throws IOException {
		
		HttpUtils httpUtils = null;
		ConfirmBookingRequest request = null;
		try {
			listener = new RestAPIListener("");
			request = createConfirmBookingRequest();
			log.info("Request is {}" , GsonUtils.getGson().toJson(request));
			httpUtils = HttpUtils.builder().urlString(supplierConf.getHotelAPIUrl(HotelUrlConstants.CONFIRM_BOOKING_URL))
					.postData(GsonUtils.getGson().toJson(request)).headerParams(TravelBoutiqueUtil.getHeaderParams())
					.printResponseLog(true).build();
			confirmBookingResponse = httpUtils.getResponse(HotelConfirmBookingResponse.class).orElse(null);
			log.debug("TBO Booking Response is :{}", confirmBookingResponse);
			boolean bookingStatus = updateConfirmBookingStatus();
			return bookingStatus;
		}finally {
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(request.toString())
					.type("TBO-Confirm-BookingReq").build());
			listener.addLog(LogData.builder().key(order.getBookingId()).logData(httpUtils.getResponseString())
					.type("TBO-Confirm-BookingRes").build());
		}
		
	}
	
	
	private boolean updateConfirmBookingStatus() {
		
		boolean isConfirmed = false;
		if(confirmBookingResponse == null || confirmBookingResponse.getGenerateVoucherResult() == null) {
			log.info("Booking Failed for bookingID {} as null response from supplier" , order.getBookingId());
			return isConfirmed;
		}
		
		ConfirmBookingResponse response = confirmBookingResponse.getGenerateVoucherResult();
		if(response.getStatus() == 1 && response.getHotelBookingStatus().equalsIgnoreCase("confirmed")) {
			isConfirmed = true;
		}
		
		return isConfirmed;
	}
	
	private ConfirmBookingRequest createConfirmBookingRequest() throws IOException {
		
		String tokenId = getCachedToken();
		ConfirmBookingRequest request = ConfirmBookingRequest.builder().BookingId(hotel.getMiscInfo().getSupplierBookingId())
				.EndUserIp(TravelBoutiqueUtil.IP).TokenId(tokenId).build();
		return request;
	}
	
	
	private TravelBoutiqueBookingDetailRequest getBookingDetailRequest(String bookingId) throws IOException {
		
		TravelBoutiqueBookingDetailRequest bookingDetailRequest = new TravelBoutiqueBookingDetailRequest();
		bookingDetailRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		bookingDetailRequest.setTraceId(hotel.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		String token = getCachedToken();
		bookingDetailRequest.setTokenId(token);
		bookingDetailRequest.setBookingId(bookingId);
		return bookingDetailRequest;
		
	}
	
	
}
