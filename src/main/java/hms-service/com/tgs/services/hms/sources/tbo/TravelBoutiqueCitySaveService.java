package com.tgs.services.hms.sources.tbo;


import java.util.List;

import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.mapping.City;
import com.tgs.services.hms.datamodel.tbo.mapping.CityInfoRequest;
import com.tgs.services.hms.datamodel.tbo.mapping.CityInfoResponse;
import com.tgs.services.hms.datamodel.tbo.mapping.Country;
import com.tgs.services.hms.datamodel.tbo.mapping.CountryInfoRequest;
import com.tgs.services.hms.datamodel.tbo.mapping.CountryInfoResponse;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.utils.common.HttpUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TravelBoutiqueCitySaveService extends TravelBoutiqueBaseService {
	
	
	/*
	 * Not Used
	 */
	
	private HotelSupplierConfiguration supplierConf;

	public void process() throws Exception {
		
		supplierConf = HotelSupplierConfigurationHelper.getSupplierConfiguration("TBO");
		HttpUtils httpUtils = null;
		String tokenId = getCachedToken();
		try {
			
			CountryInfoRequest searchRequest = createSearchRequest(tokenId);
			httpUtils = TravelBoutiqueUtil.getRequest(searchRequest, supplierConf , supplierConf.getHotelAPIUrl(HotelUrlConstants.COUNTRY_INFO));
			CountryInfoResponse searchResponse = httpUtils.getResponse(CountryInfoResponse.class).orElse(null);
			String xmlResponse = searchResponse.getCountryList();
			List<Country> countryList = TravelBoutiqueUtil.getCountryListFromXML(xmlResponse);
			
			for(Country country : countryList) {
				
				CityInfoRequest cityInfoRequest = createCityInfoSearchRequest(tokenId , country.getCode());
				httpUtils = TravelBoutiqueUtil.getRequest(cityInfoRequest , supplierConf , supplierConf.getHotelAPIUrl(HotelUrlConstants.CITY_INFO));
				CityInfoResponse response = httpUtils.getResponse(CityInfoResponse.class).orElse(null);
				String xmlCityResponse = response.getDestinationCityList();
				if(xmlCityResponse.equals("No City Found for the same combination.")) {
					
					log.error("No City Found For Country {} , name {}" , country.getCode() , country.getName());
					continue;
				}
				List<City> cityList = TravelBoutiqueUtil.getCityListFromXML(xmlCityResponse);
				for(City city : cityList) {
					
					HotelCityInfoQuery cityInfo = new HotelCityInfoQuery();
					cityInfo.setCityname(city.getCityName());
					cityInfo.setCountryname(country.getName());
					cityInfo.setSuppliercityid(city.getCityId());
					cityInfo.setSuppliercountryid(country.getCode());
					cityInfo.setSuppliername("TBO");
				}
				
			}
		}catch(Exception e) {
			log.error("Error while fetching cities {}");
		}
	}
	
	
	public CountryInfoRequest createSearchRequest(String tokenId) throws Exception {
		
		CountryInfoRequest request = new CountryInfoRequest();
		request.setClientId(supplierConf.getHotelSupplierCredentials().getClientId());
		request.setEndUserIp("192.168.3.4");
		request.setTokenId(tokenId);
		return request;
	}
	
	public CityInfoRequest createCityInfoSearchRequest(String tokenId , String countryCode) throws Exception {
		
		CityInfoRequest request = new CityInfoRequest();
		request.setClientId(supplierConf.getHotelSupplierCredentials().getClientId());
		request.setEndUserIp("192.168.3.4");
		request.setTokenId(tokenId);
		request.setCountryCode(countryCode);
		return request;
		
	}
	
}
