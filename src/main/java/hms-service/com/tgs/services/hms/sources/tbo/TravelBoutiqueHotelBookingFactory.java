package com.tgs.services.hms.sources.tbo;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelBookingFactory;
import com.tgs.services.oms.datamodel.Order;

@Service
public class TravelBoutiqueHotelBookingFactory extends AbstractHotelBookingFactory {

	@Autowired
	private HMSCachingServiceCommunicator cacheService;
	
	public TravelBoutiqueHotelBookingFactory(HotelSupplierConfiguration supplierConf, HotelInfo hotel, Order order) {
		super(supplierConf, hotel, order);
	}

	@Override
	public boolean bookHotel() throws IOException, InterruptedException {
		TravelBoutiqueBookingService bookingService = TravelBoutiqueBookingService.builder().hotel(this.getHotel()).sourceConfigOutput(sourceConfigOutput)
				.order(order).supplierConf(supplierConf).cacheService(cacheService).build();
		return bookingService.book();
	}

	@Override
	public boolean confirmHotel() throws IOException {
		TravelBoutiqueBookingService bookingService = TravelBoutiqueBookingService.builder().hotel(this.getHotel()).sourceConfigOutput(sourceConfigOutput)
				.order(order).supplierConf(supplierConf).cacheService(cacheService).build();
		return bookingService.confirmHoldBooking();
	}

}
