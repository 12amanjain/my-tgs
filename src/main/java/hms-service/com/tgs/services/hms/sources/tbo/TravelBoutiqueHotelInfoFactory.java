package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.jparepository.HotelSupplierInfoService;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;


@Service
public class TravelBoutiqueHotelInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	private HotelCacheHandler cacheHandler;

	@Autowired
	private HMSCachingServiceCommunicator cacheService;
	
	@Autowired
	private HotelSupplierInfoService supplierService;

	public TravelBoutiqueHotelInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {
		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException {
		
		CityInfoMapping supplierCityInfo = cacheHandler.getSupplierCityInfoFromCityId(
				searchQuery.getSearchCriteria().getCityId(), supplierConf.getBasicInfo().getSupplierId());
		TravelBoutiqueSearchService searchService = TravelBoutiqueSearchService.builder()
				.supplierConf(this.getSupplierConf()).searchQuery(this.getSearchQuery())
				.supplierCityInfo(supplierCityInfo).sourceConfigOutput(sourceConfigOutput)
				.cacheService(cacheService).supplierService(supplierService).build();
		searchService.doSearch();
		searchResult = searchService.getSearchResult();
	}

	@Override

	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {

		TravelBoutiqueSearchService searchService = TravelBoutiqueSearchService.builder()
				.sourceConfigOutput(sourceConfigOutput).supplierConf(this.getSupplierConf()).hInfo(hInfo)
				.searchQuery(searchQuery).cacheService(cacheService).build();
		searchService.doDetailSearch(hInfo);
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) {
		
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option cachedOption = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		
		hotel.getOptions().forEach(op -> {
			if (op.getId().equals(optionId)) {
				op.setCancellationPolicy(cachedOption.getCancellationPolicy());
			}
		});

	}


}
