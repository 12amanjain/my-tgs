package com.tgs.services.hms.sources.tbo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.sources.AbstractHotelPriceValidationFactory;

@Service
public class TravelBoutiquePriceValidationFactory extends AbstractHotelPriceValidationFactory {

	@Autowired
	private HMSCachingServiceCommunicator cacheService;

	public TravelBoutiquePriceValidationFactory(HotelSearchQuery searchQuery, HotelInfo hotel,
			HotelSupplierConfiguration hotelSupplierConf, String logKey) {
		super(searchQuery, hotel, hotelSupplierConf, logKey);
	}

	@Override
	public Option getOptionWithUpdatedPrice() throws Exception {

		TravelBoutiquePriceValidationService priceValidationService = TravelBoutiquePriceValidationService.builder().sourceConfigOutput(sourceConfigOutput)
				.searchQuery(this.searchQuery).cacheService(cacheService).build();
		priceValidationService.validate(this.getHInfo(), this.getSupplierConf());
		return this.getHInfo().getOptions().get(0);
	}
}
