package com.tgs.services.hms.sources.tbo;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.booking.BlockRoomResult;
import com.tgs.services.hms.datamodel.tbo.booking.HotelPriceValidationRequest;
import com.tgs.services.hms.datamodel.tbo.booking.HotelPriceValidationResponse;
import com.tgs.services.hms.datamodel.tbo.search.BedType;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.utils.common.HttpUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
@SuperBuilder
public class TravelBoutiquePriceValidationService extends TravelBoutiqueBaseService {
	
	private HotelSearchQuery searchQuery;
	protected RestAPIListener listener;
	protected HotelSourceConfigOutput sourceConfigOutput;

	public void validate(HotelInfo hInfo , HotelSupplierConfiguration supplierConf) throws Exception {
		
		HttpUtils httpUtils = null;
		HotelPriceValidationRequest validationRequest = null;
		try {
			listener = new RestAPIListener("");
			String token = getCachedToken();
			validationRequest = createRequest(hInfo , token);
			log.debug("Request is {}" , GsonUtils.getGson().toJson(validationRequest));
			httpUtils = TravelBoutiqueUtil.getRequest(validationRequest,supplierConf, supplierConf.getHotelAPIUrl(HotelUrlConstants.BLOCK_ROOM_URL));
			HotelPriceValidationResponse response = httpUtils.getResponse(HotelPriceValidationResponse.class).orElse(null);
			if(response.isSessionExpired()) {
				log.error("TBO Session Expired SearchId {}", searchQuery.getSearchId());
				return;
			}
			validateResponse(response , hInfo);
		}finally {
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(GsonUtils.getGson().toJson(validationRequest))
					.type("TBO-PriceValidation-Req").build());
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("TBO-PriceValidation-Res").build());
		}
	}
	
	private void validateResponse(HotelPriceValidationResponse response , HotelInfo hInfo) {
		
		
		if(response == null || response.getBlockRoomResult() == null) {
			log.info("Got null Block Room Result from supplier {} , for hotelId {}" ,hInfo.getId());
			return;
		}
			
		BlockRoomResult blockRoomResult = response.getBlockRoomResult();
		hInfo.getOptions().get(0).getMiscInfo().setIsPackageFare(blockRoomResult.isIsPackageFare());
		hInfo.getOptions().get(0).getMiscInfo().setIsPackageDetailsMandatory(blockRoomResult.isIsPackageDetailsMandatory());
		
		if(blockRoomResult.getAvailabilityType().equalsIgnoreCase("Available")) {
			/*
			 * Handle Case of Available Booking
			 */
			hInfo.getOptions().get(0).setIsOptionOnRequest(true);
		}
		
		hInfo.setName(blockRoomResult.getHotelName());
		if(blockRoomResult.isCancellationPolicyChanged() || blockRoomResult.isPriceChanged()) {
			TravelBoutiqueUtil.updateHotelInfo(hInfo , response.getBlockRoomResult(),sourceConfigOutput);
			log.info("Price Info/ Cancellation Policy Changed & Updated for searchId {}" , searchQuery.getSearchId());
		}
	}
	
	private HotelPriceValidationRequest createRequest(HotelInfo hInfo , String token) throws UnknownHostException {
		
		HotelPriceValidationRequest request = new HotelPriceValidationRequest();
		request.setEndUserIp(TravelBoutiqueUtil.IP);
		request.setGuestNationality(TravelBoutiqueUtil.NATIONALITY);
		request.setHotelCode(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		request.setHotelName(hInfo.getName());
		request.setIsVoucherBooking(true);
		request.setNoOfRooms(String.valueOf(hInfo.getOptions().get(0).getRoomInfos().size()));
		request.setResultIndex(hInfo.getOptions().get(0).getMiscInfo().getResultIndex().toString());
		request.setTokenId(token);
		request.setTraceId(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		/*
		 * Alphanumeric client Reference not accepted(as they are accepting only integer values
		 */
		request.setClientReferenceNo("123");
		List<HotelRoomDetail> roomdetails = new ArrayList<>();
		for(RoomInfo roomInfo : hInfo.getOptions().get(0).getRoomInfos()) {
			HotelRoomDetail roomDetail = new HotelRoomDetail();
			RoomMiscInfo roomMiscInfo = roomInfo.getMiscInfo();
			roomDetail.setPrice(TravelBoutiqueUtil.getSupplierPrice(roomMiscInfo.getPrice()));
			roomDetail.setRatePlanCode(roomMiscInfo.getRatePlanCode());
			roomDetail.setRoomIndex(roomMiscInfo.getRoomIndex());
			roomDetail.setRoomTypeCode(roomMiscInfo.getRoomTypeCode());
			roomDetail.setRoomTypeName(roomMiscInfo.getRoomTypeName());
			roomDetail.setSupplements("");
			List<BedType> types = new ArrayList<>();
			BedType bedType = new BedType();
			bedType.setBedTypeCode(1);
			if(!CollectionUtils.isEmpty(roomDetail.getBedTypes())) {
				types = roomDetail.getBedTypes();
			}
			roomDetail.setBedTypes(types);
			roomdetails.add(roomDetail);
		}
		request.setHotelRoomsDetails(roomdetails);
		return request;
	}
	
	
	
}
