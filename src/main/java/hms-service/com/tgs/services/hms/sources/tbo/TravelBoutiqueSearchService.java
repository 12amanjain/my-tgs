package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Image;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.bookingcancellation.RoomGuest;
import com.tgs.services.hms.datamodel.tbo.search.HotelDetail;
import com.tgs.services.hms.datamodel.tbo.search.HotelDetailRequest;
import com.tgs.services.hms.datamodel.tbo.search.HotelDetailResponse;
import com.tgs.services.hms.datamodel.tbo.search.HotelResult;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomResponse;
import com.tgs.services.hms.datamodel.tbo.search.HotelSearchRequest;
import com.tgs.services.hms.datamodel.tbo.search.HotelSearchResponse;
import com.tgs.services.hms.datamodel.tbo.search.RoomCombination;
import com.tgs.services.hms.datamodel.tbo.search.RoomCombinations;
import com.tgs.services.hms.helper.HotelConstants;
import com.tgs.utils.common.HttpUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
@Service
@SuperBuilder
public class TravelBoutiqueSearchService extends TravelBoutiqueBaseService {
	
	private CityInfoMapping supplierCityInfo;
	private HotelSourceConfigOutput sourceConfigOutput;
	private HotelSupplierConfiguration supplierConf;
	private HotelSearchQuery searchQuery;
	private HotelInfo hInfo;
	private HotelSearchResult searchResult;
    protected RestAPIListener listener;
    
    private static final Integer maxOptionSize = 15;
    
	
	public void doSearch() throws IOException {
	
		HttpUtils httpUtils = null;
		HotelSearchRequest searchRequest = null;
		try {
			listener = new RestAPIListener("");
			searchRequest = createSearchRequest();
			log.info("SearchRequest is {}", GsonUtils.getGson().toJson(searchRequest));
			httpUtils = TravelBoutiqueUtil.getRequest(searchRequest, supplierConf 
					, supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
			HotelSearchResponse searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
			if(searchResponse.isSessionExpired()) {
				log.error("TBO Session Expired SearchId {}", searchQuery.getSearchId());
				storeToken();
				searchRequest.setTokenId(getCachedToken());
				log.info("SearchRequest is {}", GsonUtils.getGson().toJson(searchRequest));
				httpUtils = TravelBoutiqueUtil.getRequest(searchRequest, supplierConf 
						, supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_SEARCH_URL));
				searchResponse = httpUtils.getResponse(HotelSearchResponse.class).orElse(null);
			}
			searchResult  = createSearchResult(searchResponse);
		}finally{	
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(GsonUtils.getGson().toJson(searchRequest))
					.type("TBO-HotelSearch-Req").build());
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("TBO-HotelSearch-Res").build());
		}
	}
	
	
	private HotelSearchRequest createSearchRequest() throws IOException {
		
		String token = getCachedToken();
		HotelSearchRequest searchRequest = new HotelSearchRequest();
		searchRequest.setCheckInDate(DateTimeFormatter.ofPattern("dd/MM/yyyy").format(searchQuery.getCheckinDate()));
		int numberOfNights = (int)ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		searchRequest.setNoOfNights(numberOfNights);
		
		searchRequest.setCountryCode(supplierCityInfo.getSupplierCountry());
		searchRequest.setCityId(Integer.parseInt(supplierCityInfo.getSupplierCity()));
		
		/*
		 * Nationality for Qtech coming as its own code i.e. 106 . 
		 * Need nationality mapping same as city mapping 
		 */
		searchRequest.setGuestNationality(TravelBoutiqueUtil.NATIONALITY);
		searchRequest.setPreferredCurrency(TravelBoutiqueUtil.CURRENCY);
		
		searchRequest.setNoOfRooms(searchQuery.getRoomInfo().size());
		List<RoomGuest> roomGuests = new ArrayList<>();
		for(RoomSearchInfo roomInfo : searchQuery.getRoomInfo()) {	
			Integer numberOfChild = roomInfo.getNumberOfChild() != null 
					? roomInfo.getNumberOfChild() : 0;
			RoomGuest roomGuest = RoomGuest.builder().NoOfAdults(roomInfo.getNumberOfAdults())
					.NoOfChild(numberOfChild).ChildAge(roomInfo.getChildAge()).build();
			roomGuests.add(roomGuest);
		}
		searchRequest.setRoomGuests(roomGuests);
		
		searchRequest.setTokenId(token);
		searchRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		
		List<Integer> ratings = searchQuery.getSearchPreferences().getRatings();
		int minRating = 0 , maxRating = 5;
		if(CollectionUtils.isNotEmpty(ratings)) {
			Collections.sort(ratings);
			minRating = ratings.get(0);
			maxRating = ratings.get(ratings.size()-1);
		}
		
		searchRequest.setMinRating(minRating);
		searchRequest.setMaxRating(maxRating);
		
		return searchRequest;
	}

	private HotelSearchResult createSearchResult(HotelSearchResponse response) {
		
		searchResult = new HotelSearchResult();
		if(response == null || response.getHotelSearchResult() == null 
				|| CollectionUtils.isEmpty(response.getHotelSearchResult().getHotelResults())) {
			log.error("Empty Response From Supplier {} for searchId {}" 
					,supplierConf.getBasicInfo().getSupplierId() , searchQuery.getSearchId());
			return null;
		}
		
		
		List<HotelResult> hotelResults = response.getHotelSearchResult().getHotelResults();
		List<HotelInfo> hotelInfos = new ArrayList<>();
		for(HotelResult hotelResult : hotelResults) {
			Address address = Address.builder().addressLine1(hotelResult.getHotelAddress()).build();
			GeoLocation geoLocation = null;
			if(StringUtils.isNotBlank(hotelResult.getLatitude()) 
					&& StringUtils.isNotBlank(hotelResult.getLongitude())) {
				geoLocation = GeoLocation.builder().latitude(hotelResult.getLatitude())
						.longitude(hotelResult.getLongitude()).build();
			}
			
			Image image = Image.builder().thumbnail(hotelResult.getHotelPicture()).build();
			HotelMiscInfo miscInfo = HotelMiscInfo.builder()
					.searchId(searchQuery.getSearchId())
					.searchKeyExpiryTime(LocalDateTime.now().plusMinutes(ObjectUtils.firstNonNull
							(sourceConfigOutput.getSearchKeyExpirationTime(), 20l)))
					.build();
			
			/**
			 * Dummy Option to store supplier & miscInfo
			 */
			Option option = Option.builder().totalPrice(hotelResult.getPrice().getRoomPrice())
					.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo()
							.getSupplierName()).resultIndex(hotelResult.getResultIndex())
							.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
							.supplierSearchId(response.getHotelSearchResult().getTraceId())
							.supplierHotelId(hotelResult.getHotelCode())
							.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
					.roomInfos(getRoomInfo(hotelResult)).id(RandomStringUtils.random(20, true, true)).build();
			
			HotelInfo hInfo = HotelInfo.builder().name(hotelResult.getHotelName()).rating(hotelResult.getStarRating())
					.description(hotelResult.getHotelDescription()).address(address).geolocation(geoLocation).images(Arrays.asList(image))
					.miscInfo(miscInfo).options(new ArrayList<>(Arrays.asList(option))).build();
			hotelInfos.add(hInfo);
		}
		searchResult.setHotelInfos(hotelInfos);
		
		return searchResult;	
	}
	
	
	private List<RoomInfo> getRoomInfo(HotelResult hotelResult){
		
		Double totalOptionPrice = hotelResult.getPrice().getPublishedPrice();
		int numberOfNights = (int) ChronoUnit.DAYS.between(searchQuery.getCheckinDate(), searchQuery.getCheckoutDate());
		int numberOfRoom = searchQuery.getRoomInfo().size();
		Double perRoomPerNightPrice = totalOptionPrice/(numberOfNights * numberOfRoom);
		List<RoomInfo> roomInfos = new ArrayList<>(searchQuery.getRoomInfo().size());
		for(int i = 0; i< searchQuery.getRoomInfo().size(); i++) {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setNumberOfAdults(searchQuery.getRoomInfo().get(i).getNumberOfAdults());
			roomInfo.setNumberOfChild(searchQuery.getRoomInfo().get(i).getNumberOfChild());
			roomInfo.setRoomCategory("Dummy Room Category");
			roomInfo.setRoomType("Dummy Room Type");
			roomInfo.setMealBasis(HotelConstants.ROOM_ONLY);
			Double totalRoomPrice = 0.0;
			List<PriceInfo> priceInfoList = new ArrayList<>();
			for (int j = 1; j<= numberOfNights ; j++) {
				PriceInfo priceInfo = new PriceInfo();
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, perRoomPerNightPrice);
				priceInfo.setDay(i);
				priceInfo.setFareComponents(fareComponents);
				totalRoomPrice += perRoomPerNightPrice;
				priceInfoList.add(priceInfo);
			}
			roomInfo.setTotalPrice(totalRoomPrice);
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomInfos.add(roomInfo);
		}
		TravelBoutiqueUtil.updatePriceWithMarkup(roomInfos,sourceConfigOutput);
		return roomInfos;
	}
	
	public void doDetailSearch(HotelInfo hInfo) throws IOException {
		listener = new RestAPIListener("");
		HotelDetailRequest detailRequest = createDetailRequest(hInfo);
	    if(CollectionUtils.isEmpty(hInfo.getFacilities())) {
	    	doStaticDataSearch(hInfo , detailRequest);
	    }
		doRoomInfoSearch(hInfo , detailRequest);
	}
	
	private HotelDetailRequest createDetailRequest(HotelInfo hInfo) throws IOException {
		
		HotelDetailRequest detailRequest = new HotelDetailRequest();
		detailRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		detailRequest.setHotelCode(hInfo.getOptions().get(0).getMiscInfo().getSupplierHotelId());
		detailRequest.setResultIndex(hInfo.getOptions().get(0).getMiscInfo().getResultIndex().toString());
		detailRequest.setTraceId(hInfo.getOptions().get(0).getMiscInfo().getSupplierSearchId());
		detailRequest.setTokenId(getCachedToken());
		return detailRequest;
		
	}
	
	private void doStaticDataSearch(HotelInfo hInfo , HotelDetailRequest detailRequest) throws IOException {
		
		HttpUtils httpUtils = null;
		HotelDetailResponse detailResponse = null;
		try {
			httpUtils = TravelBoutiqueUtil.getRequest(detailRequest, supplierConf , supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_STATIC_DATA));
			detailResponse = httpUtils.getResponse(HotelDetailResponse.class).orElse(null);
			createDetailResponse(detailResponse, hInfo);
		}finally {
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(GsonUtils.getGson().toJson(detailRequest))
					.type("TBO-HotelDetail-Static-Req").build());
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("TBO-HotelDetail-Static-Res").build());
		}
	}
	
	private void doRoomInfoSearch(HotelInfo hInfo , HotelDetailRequest detailRequest) throws IOException {
		
		HttpUtils httpUtils = null;
		HotelRoomResponse roomResponse = null;
		try {
			httpUtils = TravelBoutiqueUtil.getRequest(detailRequest, supplierConf 
					, supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_ROOM_URL));
			
			roomResponse = httpUtils.getResponse(HotelRoomResponse.class).orElse(null);
			if(roomResponse.isSessionExpired()) {
				log.error("TBO Session Expired SearchId {}", searchQuery.getSearchId());
				return;
			}
			createRoomDetailResponse(roomResponse , hInfo);
		}finally {
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(detailRequest.toString())
					.type("TBO-HotelDetail-Room-Req").build());
			listener.addLog(LogData.builder().key(searchQuery.getSearchId()).logData(httpUtils.getResponseString())
					.type("TBO-HotelDetail-Room-Res").build());
		}
	}
	
	private void createDetailResponse(HotelDetailResponse detailResponse , HotelInfo hInfo) {
		
		HotelDetail hotelDetail = detailResponse.getHotelInfoResult().getHotelDetails();
		hInfo.setDescription(hotelDetail.getDescription());
		hInfo.setFacilities(hotelDetail.getHotelFacilities());
		if(!CollectionUtils.isEmpty(hotelDetail.getImages())) {
			List<Image> hotelImages = new ArrayList<>();
			for(String image : hotelDetail.getImages()) {
				Image hotelImage = Image.builder().bigURL(image).build();
				hotelImages.add(hotelImage);
			}
			hInfo.setImages(hotelImages);	
		}
	}
	
	private void createRoomDetailResponse(HotelRoomResponse roomResponse , HotelInfo hInfo) {
		
		List<Option> options = new ArrayList<>();
		Map<String , RoomInfo> roomInfoMap = new HashMap<>();
		
		if(roomResponse == null || roomResponse.getGetHotelRoomResult() == null) {
			log.info("Error in getting Room Response for searchId {} , hotel {}" 
					, searchQuery.getSearchId() , hInfo.getName());
			return;
		}
		
		boolean isUnderCancellationAllowedForAgent = roomResponse.isUnderCancellationAllowedForAgent();
		for(HotelRoomDetail roomDetail : roomResponse.getGetHotelRoomResult().getHotelRoomsDetails()) {
			
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setId(roomDetail.getRoomTypeCode());
			roomInfo.setRoomType(roomDetail.getRoomTypeName());
			roomInfo.setRoomCategory(roomDetail.getRoomTypeName());
			roomInfo.setPerNightPriceInfos(TravelBoutiqueUtil.getPriceInfos(roomDetail));
			roomInfo.setTotalPrice(roomDetail.getPrice().getPublishedPrice());
			roomInfo.setRoomAmenities(roomDetail.getAmenity());
			roomInfo.setCancellationPolicy(TravelBoutiqueUtil.getCancellationPolicy(roomDetail));
			roomInfo.setDeadlineDateTime(roomDetail.getLastCancellationDate());
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			RoomMiscInfo roomMiscInfo = RoomMiscInfo.builder().price(TravelBoutiqueUtil.getRoomPrice(roomDetail.getPrice()))
					.roomIndex(roomDetail.getRoomIndex()).ratePlanCode(roomDetail.getRatePlanCode())
					.roomTypeCode(roomDetail.getRoomTypeCode()).bedTypes(TravelBoutiqueUtil.getBedTypes(roomDetail.getBedTypes()))
					.roomTypeName(roomDetail.getRoomTypeName())
					.isUnderCancellationAllowedForAgent(isUnderCancellationAllowedForAgent)
					.build();
			roomInfo.setMiscInfo(roomMiscInfo);
			
			roomInfoMap.put(roomDetail.getRoomIndex().toString(), roomInfo);
			
		}
		options.addAll(getOptionList(roomInfoMap , roomResponse));
		hInfo.getOptions().get(0).getMiscInfo().setIsDetailHit(true);
		hInfo.setOptions(options);
	}
	
	private List<Option> getOptionList(Map<String,  RoomInfo> roomInfoMap , HotelRoomResponse roomResponse){
		
		
		RoomCombinations combo = roomResponse.getGetHotelRoomResult().getRoomCombinations();
		List<List<RoomInfo>> roomInfosList = new ArrayList<>();
		List<Option> options = new ArrayList<>();
		
		if(combo.getInfoSource().equals("FixedCombination")) {
			
			List<RoomCombination> roomCombos = combo.getRoomCombination();
			for(RoomCombination roomCombo : roomCombos) {
				List<RoomInfo> roomInfos = new ArrayList<>();
				List<String> indexes = roomCombo.getRoomIndex();
				
				for(String index : indexes) {
					roomInfos.add(roomInfoMap.get(index));
				}
				roomInfosList.add(roomInfos);
			}
		}
		else if(combo.getInfoSource().equals("OpenCombination")) {
			/*
			 * Getting Custom Room Combinations
			 */
			roomInfosList = getRoomCombosForOpenCombination(roomResponse, roomInfoMap);
		}
		
		
		
		for(List<RoomInfo> roomInfos : roomInfosList) {
			
			HotelCancellationPolicy optionCancellationPolicy = roomResponse.getGetHotelRoomResult().getIsPolicyPerStay() 
					? roomInfos.get(0).getCancellationPolicy()
					: TravelBoutiqueUtil.getCancellationPolicyForOption(roomInfos);
			
			LocalDateTime fullRefundCancellationDeadlineDatetime = TravelBoutiqueUtil.getFullRefundCancellationDeadlineDatetime(roomInfos);
			OptionMiscInfo dummyOptionMiscInfo = hInfo.getOptions().get(0).getMiscInfo();
			
			OptionMiscInfo miscInfo = OptionMiscInfo.builder()
					.supplierId(supplierConf.getBasicInfo().getSupplierId())
					.secondarySupplier(supplierConf.getBasicInfo().getSupplierId())
					.sourceId(supplierConf.getBasicInfo().getSourceId())
					.resultIndex(dummyOptionMiscInfo.getResultIndex())
					.supplierHotelId(dummyOptionMiscInfo.getSupplierHotelId())
					.supplierSearchId(dummyOptionMiscInfo.getSupplierSearchId())
					.isDetailHit(true).build();
			
			String optionId = RandomStringUtils.random(20, true, true);
			optionCancellationPolicy.setId(optionId);
			TravelBoutiqueUtil.updatePriceWithMarkup(roomInfos,sourceConfigOutput);
			Option option = Option.builder().roomInfos(roomInfos).id(optionId)
					.cancellationPolicy(optionCancellationPolicy).miscInfo(miscInfo)
					.deadlineDateTime(fullRefundCancellationDeadlineDatetime).build();
			TravelBoutiqueUtil.setBufferTimeinCnp(option,optionCancellationPolicy,sourceConfigOutput);
			options.add(option);
		}
		
		return options;
	}
	
	public List<List<RoomInfo>> getRoomCombosForOpenCombination(HotelRoomResponse roomResponse, Map<String,  RoomInfo> roomInfoMap){
		
		List<List<RoomInfo>> roomInfosList = new ArrayList<>();
 		RoomCombinations roomCombos = roomResponse.getGetHotelRoomResult().getRoomCombinations();
		List<RoomCombination> roomComboList = roomCombos.getRoomCombination();
		Map<Integer, List<String>> map = new HashMap<>();
		/*
		 * 2 elements in array 
		 * 1) Index of roomIndex in kth list
		 * 2) Index of list
		 */
		
		PriorityQueue<int[]> pq = new PriorityQueue<>((a1,a2) -> {
			RoomInfo r1 = roomInfoMap.get(map.get(a1[1]).get(a1[0]));
			RoomInfo r2 = roomInfoMap.get(map.get(a2[1]).get(a1[0]));
			
			if(r1.getTotalPrice() < r2.getTotalPrice()) return -1;
			else if (r1.getTotalPrice() == r2.getTotalPrice()) return 0;
			return 1;
		});
		
		for(int i = 0 ; i < roomComboList.size(); i++) {
			map.put(i,roomComboList.get(i).getRoomIndex());
			pq.offer(new int[] {0,i});
		}
		
		int optionSize = 0;
		while(!pq.isEmpty() && optionSize < maxOptionSize) {
			
			List<RoomInfo> rInfoList = pq.stream().map(a -> {
				return roomInfoMap.get(map.get(a[1]).get(a[0]));
			}).collect(Collectors.toList());
			
			roomInfosList.add(rInfoList);
			int[] polledData = pq.poll();
			if(polledData[0]+1 == map.get(polledData[1]).size()) break;
			else pq.offer(new int[] {polledData[0]+1,polledData[1]});
			optionSize++;
		}
		
		return roomInfosList;
		
	}
	
	/*private void getCombos(RoomCombinations roomCombos , List<RoomCombination> list 
			, List<String> indexList , int size){
		
		if(indexList.size() == size) {
			RoomCombination roomCombo = RoomCombination.builder().RoomIndex(indexList).build();
			list.add(roomCombo);
			return;
		}
		List<String> kIndexList = roomCombos.getRoomCombination().get(indexList.size()).getRoomIndex();
		for(int i = 0 ; i < kIndexList.size() ; i++) {
			
			indexList.add(kIndexList.get(i));
			getCombos(roomCombos , list , indexList , size);
			indexList.remove(indexList.size()-1);
		}
	}
	*/
	

	
}
