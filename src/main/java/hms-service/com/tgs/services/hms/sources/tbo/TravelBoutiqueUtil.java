package com.tgs.services.hms.sources.tbo;

import java.io.IOException;
import java.io.StringReader;
import java.net.UnknownHostException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUrlConstants;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PenaltyDetails.PenaltyDetailsBuilder;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomBedType;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomPrice;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.datamodel.tbo.authentication.HotelAuthenticationRequest;
import com.tgs.services.hms.datamodel.tbo.authentication.HotelAuthenticationResponse;
import com.tgs.services.hms.datamodel.tbo.booking.BaseBookingResponse;
import com.tgs.services.hms.datamodel.tbo.mapping.Cities;
import com.tgs.services.hms.datamodel.tbo.mapping.City;
import com.tgs.services.hms.datamodel.tbo.mapping.Countries;
import com.tgs.services.hms.datamodel.tbo.mapping.Country;
import com.tgs.services.hms.datamodel.tbo.search.BedType;
import com.tgs.services.hms.datamodel.tbo.search.CancellationPolicy;
import com.tgs.services.hms.datamodel.tbo.search.DayRate;
import com.tgs.services.hms.datamodel.tbo.search.HotelRoomDetail;
import com.tgs.services.hms.datamodel.tbo.search.Price;
import com.tgs.services.hms.datamodel.tbo.search.TBOBaseRequest;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TravelBoutiqueUtil {

	protected static final String IP = "::1";
	protected static final String CURRENCY = "INR";
	protected static final String NATIONALITY = "IN";
	protected static final String CACHE_KEY = "TBO";
	protected static final String TBO_SUPPLIER_ID = "TBO";
	
	public static HttpUtils getRequest(TBOBaseRequest request , HotelSupplierConfiguration sConf , String url) throws UnknownHostException {
		
		HttpUtils httpUtils = HttpUtils.builder().urlString(url)
				.postData(GsonUtils.getGson().toJson(request)).headerParams(getHeaderParams()).proxy(HotelUtils.getProxyFromConfigurator()).build();
		return httpUtils;
	}
	

	public static Map<String, String> getHeaderParams() {
		
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "application/json");
        headerParams.put("Accept-Encoding", "gzip, deflate");
		return headerParams;
		
	}
	
	public static String fetchNewToken(HotelSupplierConfiguration supplierConf) throws IOException  {
		
		HotelAuthenticationResponse authResponse = null;
		try {
			HotelAuthenticationRequest authRequest = createAuthenticationRequest(supplierConf);
			log.info("Auth Request is {}" , GsonUtils.getGson().toJson(authRequest));
			HttpUtils httpUtils = TravelBoutiqueUtil.
					getRequest(authRequest , supplierConf , supplierConf.getHotelAPIUrl(HotelUrlConstants.HOTEL_AUTH_URL));
			log.info("Auth Response is {}" , GsonUtils.getGson().toJson(httpUtils.getResponseString()));
			 authResponse = httpUtils.getResponse(HotelAuthenticationResponse.class).orElse(null);
			if(authResponse == null || StringUtils.isBlank(authResponse.getTokenId())) { 
				throw new CustomGeneralException(SystemError.AUTHENTICATION_FAILED , authResponse.getError().getErrorMessage());
			}
		}catch(Exception e) {
			log.error("Exception During Authentication for supplier {}", supplierConf.getHotelSupplierCredentials().getUserName());
			throw e;
		}
		return authResponse.getTokenId();
	}
	

	private  static HotelAuthenticationRequest createAuthenticationRequest(HotelSupplierConfiguration sConf) throws UnknownHostException {
		
		HotelAuthenticationRequest authRequest = new HotelAuthenticationRequest();
		authRequest.setPassword(sConf.getHotelSupplierCredentials().getPassword());
		authRequest.setUserName(sConf.getHotelSupplierCredentials().getUserName());
		authRequest.setClientId(sConf.getHotelSupplierCredentials().getClientId());
		authRequest.setEndUserIp(TravelBoutiqueUtil.IP);
		return authRequest;
		
	}


    public static HotelCancellationPolicy getCancellationPolicy(HotelRoomDetail roomDetail) {
		
		List<PenaltyDetails> penaltyDetails = new ArrayList<>();
		for(CancellationPolicy cancellationPolicy : roomDetail.getCancellationPolicies()) {
			PenaltyDetailsBuilder pd = PenaltyDetails.builder();
			if(cancellationPolicy.getChargeType() == 1) {
				pd.penaltyAmount(cancellationPolicy.getCharge());
			}else if(cancellationPolicy.getChargeType() == 2) {
				pd.penaltyPercent(cancellationPolicy.getCharge());
			}else if(cancellationPolicy.getChargeType() == 3) {
				pd.penaltyRoomNights(cancellationPolicy.getCharge().intValue());
			}
			PenaltyDetails penaltyDetail = pd.fromDate(cancellationPolicy.getFromDate())
					.toDate(cancellationPolicy.getToDate()).build();
			penaltyDetails.add(penaltyDetail);
		}
		HotelCancellationPolicy hotelCancellationPolicy = HotelCancellationPolicy.builder().penalyDetails(penaltyDetails)
				.cancellationPolicy(roomDetail.getCancellationPolicy()).build();
		return hotelCancellationPolicy;
	}
    
    
  /*  public static HotelCancellationPolicy getCancellationPolicyForOption(List<RoomInfo> roomInfos) {

		HotelCancellationPolicy cancellationPolicy = null;
		if(CollectionUtils.isNotEmpty(roomInfos)) {
			cancellationPolicy = roomInfos.get(0).getCancellationPolicy();
			for(int i =1; i< roomInfos.size();i++) {
				HotelCancellationPolicy roomCancellationPolicy = roomInfos.get(i).getCancellationPolicy();
				mergeCancellationPolicy(cancellationPolicy , roomCancellationPolicy);
			}
		}
		return cancellationPolicy;
	}
	*/
    
    
    
    
	/*public static void mergeCancellationPolicy(HotelCancellationPolicy cp1 , HotelCancellationPolicy cp2) {
		
		int index = 0;
		for(PenaltyDetails pd1 : cp1.getPenalyDetails()) {
			PenaltyDetails pd2 = cp2.getPenalyDetails().get(index);
			LocalDateTime from = pd1.getFromDate() , to = pd1.getToDate();
			if(from.isAfter(pd2.getFromDate())) { from = pd2.getFromDate(); }
			if(to.isBefore(pd2.getToDate())) { to = pd2.getToDate(); }
			index++;
		}
	}*/
	

    public static HotelCancellationPolicy getCancellationPolicyForOption(List<RoomInfo> roomInfos) {
    	
    	boolean isUnderCancellationAllowedForAgent = roomInfos.get(0).getMiscInfo().isUnderCancellationAllowedForAgent();
    	LocalDateTime deadLineDateTimeForOption = getFullRefundCancellationDeadlineDatetime(roomInfos);
    	List<PenaltyDetails> pds = new ArrayList<>();
    	LocalDateTime currentDateTime = LocalDateTime.now();
    	boolean isFullRefundAllowed = false;
    	if(deadLineDateTimeForOption.isAfter(LocalDateTime.now()) && isUnderCancellationAllowedForAgent) {
    		PenaltyDetails pd1 = PenaltyDetails.builder().fromDate(currentDateTime)
    				.toDate(deadLineDateTimeForOption).penaltyAmount(0.0).build();
    		pds.add(pd1);
    		isFullRefundAllowed = true;
    	}else {
    		deadLineDateTimeForOption = currentDateTime;
    	}
    	PenaltyDetails pd2 = PenaltyDetails.builder().fromDate(deadLineDateTimeForOption)
    			.toDate(roomInfos.get(0).getCheckInDate().atTime(12, 00))
    			.penaltyAmount(geTotalBookingAmount(roomInfos)).build();
    	pds.add(pd2);
    	CancellationMiscInfo miscInfo = CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build();
    	HotelCancellationPolicy optionCancellationPolicy = HotelCancellationPolicy.builder().penalyDetails(pds)
    			.isFullRefundAllowed(isFullRefundAllowed).miscInfo(miscInfo).build();
    	updateNewCancellationPolicyInRooms(roomInfos , optionCancellationPolicy , deadLineDateTimeForOption);
    	
    	return optionCancellationPolicy;
    	
    }
    
    private static void updateNewCancellationPolicyInRooms(List<RoomInfo> roomInfos 
    		, HotelCancellationPolicy cancellationPolicy , LocalDateTime deadlineDateTime) {
    	
    	for(RoomInfo roomInfo : roomInfos) {
    		roomInfo.setCancellationPolicy(cancellationPolicy);
    		roomInfo.setDeadlineDateTime(deadlineDateTime);
    	}
    	
    }
    
    private static Double geTotalBookingAmount(List<RoomInfo> roomInfos) {
    	
    	Double totalBookingAmount = 0.0;
    	for(RoomInfo roomInfo : roomInfos) {
    		
    		for(PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
    			totalBookingAmount += priceInfo.getFareComponents().get(HotelFareComponent.BF);
    		}
    	}
    	return totalBookingAmount;
    }
    
	public static void updateHotelInfo(HotelInfo hInfo , BaseBookingResponse baseResponse,HotelSourceConfigOutput sourceConfigOutput) {
		
		Map<String , RoomInfo> roomInfoMap = new HashMap<>();
		List<RoomInfo> roomInfos = hInfo.getOptions().get(0).getRoomInfos();
		for(RoomInfo roomInfo : roomInfos) {
			roomInfoMap.put(roomInfo.getMiscInfo().getRoomIndex().toString() , roomInfo);
		}
		
		if(baseResponse.isCancellationPolicyChanged()) {
			updateCancellationPolicyInRooms(baseResponse , roomInfoMap);
			HotelCancellationPolicy cancellationPolicy = getCancellationPolicyForOption(hInfo.getOptions().get(0).getRoomInfos());
			hInfo.getOptions().get(0).setCancellationPolicy(cancellationPolicy);
			hInfo.getOptions().get(0).setDeadlineDateTime(getFullRefundCancellationDeadlineDatetime(roomInfos));
			setBufferTimeinCnp(hInfo.getOptions().get(0),hInfo.getOptions().get(0).getCancellationPolicy(),sourceConfigOutput);
			
		}
		if(baseResponse.isPriceChanged()) {
			updateHotelPrice(hInfo , baseResponse , roomInfoMap);
			updatePriceWithMarkup(hInfo.getOptions().get(0).getRoomInfos(),sourceConfigOutput);
		}
	}
	

	public static LocalDateTime getFullRefundCancellationDeadlineDatetime(List<RoomInfo> roomInfos) {
		
		LocalDateTime lastCancellationDateTime = roomInfos.get(0).getDeadlineDateTime(); 
		for(int i = 1 ; i < roomInfos.size(); i++) {
			RoomInfo roomInfo = roomInfos.get(i);
			if(lastCancellationDateTime.isBefore(roomInfo.getDeadlineDateTime())) {
				lastCancellationDateTime = roomInfo.getDeadlineDateTime();
			}
		}
		return lastCancellationDateTime;
		
	}

	private static void updateCancellationPolicyInRooms(BaseBookingResponse priceValidationResponse 
			, Map<String , RoomInfo> roomInfoMap) {
		
		for(HotelRoomDetail roomDetail : priceValidationResponse.getHotelRoomDetails()) {
			
			RoomInfo roomInfo = roomInfoMap.get(roomDetail.getRoomIndex().toString());
			roomInfo.setCancellationPolicy(getCancellationPolicy(roomDetail));
			roomInfo.setDeadlineDateTime(roomDetail.getLastCancellationDate());
		}
	}
	
	public static List<PriceInfo> getPriceInfos(HotelRoomDetail roomDetail){
		
		int i = 1;
		List<PriceInfo> priceInfos = new ArrayList<>();
		Price supplierPriceInfo = roomDetail.getPrice();
		Double totalTaxesAndFees = supplierPriceInfo.getPublishedPriceRoundedOff() - supplierPriceInfo.getRoomPrice();
		Double perRoomNightTaxAndFees = totalTaxesAndFees/roomDetail.getDayRates().size();
		for(DayRate dayRate : roomDetail.getDayRates()) {
			PriceInfo priceInfo = new PriceInfo();
			Double totalPerRoomNightPrice = dayRate.getAmount() + perRoomNightTaxAndFees;
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, totalPerRoomNightPrice);
			priceInfo.setFareComponents(fareComponents);
			priceInfo.setDay(i);
			priceInfo.setTotalPrice(totalPerRoomNightPrice);
			priceInfos.add(priceInfo);
			i++;
		}
		return priceInfos;
	}
	
	

	private static void updateHotelPrice(HotelInfo hInfo , BaseBookingResponse priceValidationResponse
			, Map<String , RoomInfo> roomInfoMap) {
		
		for(HotelRoomDetail roomDetail : priceValidationResponse.getHotelRoomDetails()) {
			RoomInfo roomInfo = roomInfoMap.get(roomDetail.getRoomIndex().toString());
			roomInfo.setPerNightPriceInfos(getPriceInfos(roomDetail));
			roomInfo.setTotalPrice(roomDetail.getPrice().getPublishedPrice());
		}
		
	}
	
	public static List<Country> getCountryListFromXML(String xml) throws Exception{
		
		JAXBContext jaxbContext = JAXBContext.newInstance(Countries.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StringReader reader = new StringReader(xml);
	    Countries countries = (Countries) jaxbUnmarshaller.unmarshal(reader);
	    return countries.getCountry();
		
	}
	

	public static List<City> getCityListFromXML(String xml) throws Exception{
		
		JAXBContext jaxbContext = JAXBContext.newInstance(Cities.class);
	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    StringReader reader = new StringReader(xml);
	    Cities cities = (Cities) jaxbUnmarshaller.unmarshal(reader);
	    return cities.getCity();
		
	}
	
	public static RoomPrice getRoomPrice(Price price) {
		
		return GsonUtils.getGson().fromJson(GsonUtils.getGson()
				.toJson(price), RoomPrice.class);
	}
	
	public static Price getSupplierPrice(RoomPrice price) {
		
		return GsonUtils.getGson().fromJson(GsonUtils.getGson()
				.toJson(price), Price.class);
		
	}
	
	public static List<RoomBedType> getBedTypes(List<BedType> bedTypes){
		
		List<RoomBedType> roomBedTypes = new ArrayList<>();
		for(BedType bedType  : bedTypes) {
			RoomBedType roomBedType = GsonUtils.getGson().fromJson(GsonUtils.getGson()
					.toJson(bedType), RoomBedType.class);
			roomBedTypes.add(roomBedType);
		}
		return roomBedTypes;
		
		
	}
	
	public static void setBufferTimeinCnp(Option option, HotelCancellationPolicy cancellationPolicyResponse,HotelSourceConfigOutput sourceConfigOutput) {

		LocalDateTime checkInDate = option.getRoomInfos().get(0).getCheckInDate().atStartOfDay();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		boolean isFullRefundAllowed = false;
		Integer cancellationBuffer =
				sourceConfigOutput.getCancellationPolicyBuffer() != null ? sourceConfigOutput.getCancellationPolicyBuffer() : 0;

		List<PenaltyDetails> pds = cancellationPolicyResponse.getPenalyDetails();
		if (pds != null) {
			for (PenaltyDetails pd : pds) {
				LocalDateTime fDate = pd.getFromDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				LocalDateTime tDate = pd.getToDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				pd.setFromDate(fDate);
				pd.setToDate(tDate);
				pd.setPenaltyAmount(pd.getPenaltyAmount());
			}
			pds.get(0).setFromDate(now);
			pds.get(pds.size() - 1).setToDate(checkInDate);

			Iterator<PenaltyDetails> i = pds.iterator();
			PenaltyDetails pd = null;
			while (i.hasNext()) {
				pd = (PenaltyDetails) i.next();
				LocalDateTime tDate = pd.getToDate();
				if (tDate.isBefore(now)) {
					i.remove();
				} else {
					LocalDateTime fDate = pd.getFromDate();
					if (fDate.isBefore(now))
						i.remove();
					if (pd.getPenaltyAmount() == 0.0) {
						deadlineDateTime = tDate;
						isFullRefundAllowed = true;
					}
				}
			}
		}
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
				.id(cancellationPolicyResponse.getId()).isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();
		option.setDeadlineDateTime(deadlineDateTime);
		option.setCancellationPolicy(cancellationPolicy);
	}
	
	public static void updatePriceWithMarkup(List<RoomInfo> rInfoList,HotelSourceConfigOutput sourceConfigOutput) {

		double supplierMarkup =
				sourceConfigOutput.getSupplierMarkup() == null ? 0.0 : sourceConfigOutput.getSupplierMarkup();

		for (RoomInfo roomInfo : rInfoList) {
			for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
				double supplierGrossPrice = priceInfo.getFareComponents().get(HotelFareComponent.BF);
				priceInfo.getFareComponents().put(HotelFareComponent.MUP, (supplierGrossPrice * supplierMarkup) / 100);
				Double supplierGrossPriceWithMarkup =
						supplierGrossPrice + priceInfo.getFareComponents().getOrDefault(HotelFareComponent.MUP, 0.0);
				priceInfo.getFareComponents().put(HotelFareComponent.BF, supplierGrossPriceWithMarkup);

			}
		}
	}
}
