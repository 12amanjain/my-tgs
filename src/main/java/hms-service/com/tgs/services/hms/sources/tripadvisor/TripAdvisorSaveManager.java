package com.tgs.services.hms.sources.tripadvisor;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.tripadvisor.HotelReviewMappingResponse;
import com.tgs.services.hms.datamodel.tripadvisor.LocationMapperRequest;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelSupplierMapping;
import com.tgs.services.hms.jparepository.HotelInfoService;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TripAdvisorSaveManager {
	
	@Autowired
	HotelInfoService hotelInfoService;
	
	public void save() {

		List<DbHotelInfo> dbHotelInfoList = hotelInfoService.findUnmappedhotels(TripAdvisorConstants.NAME.getValue());

		for(DbHotelInfo dbHotel : dbHotelInfoList) {
			try {
			LocationMapperRequest request = createLocationMapperRequest(dbHotel);
			String baseUrl = StringUtils.join(TripAdvisorConstants.LOCATION_MAPPER_URL.getValue() , "/" , dbHotel.getGeolocation().getLatitude() , "," , dbHotel.getGeolocation().getLongitude());
			HttpUtils httpUtils = HotelUtils.getReviewURL(request , baseUrl);
			HotelReviewMappingResponse response = httpUtils.getResponse(HotelReviewMappingResponse.class).orElse(null);
			if(response!= null && response.getData() !=null) {
				DbHotelSupplierMapping supplierMapping = new DbHotelSupplierMapping();
				supplierMapping.setHotelId(dbHotel.getId());
				supplierMapping.setSupplierHotelId(response.getData().get(0).getLocation_id());
				supplierMapping.setSupplierName(TripAdvisorConstants.NAME.getValue());
				supplierMapping.setSourceName(TripAdvisorConstants.NAME.getValue());
				hotelInfoService.save(supplierMapping);
			}
			}
			catch(Exception e) {
				log.error("Error while creating mapping for Hotel with id {}",dbHotel.getId(),e);
			}
		}
		
	}
	
	public LocationMapperRequest createLocationMapperRequest(DbHotelInfo dbHotel) {
		
		LocationMapperRequest request = new LocationMapperRequest();
		request.setCategory(TripAdvisorConstants.CATEGORY.getValue());
		request.setKey(StringUtils.join(TripAdvisorConstants.KEY.getValue() , "-" , "mapper"));
		request.setQ(dbHotel.getName());
		return request;
		
	}

}
