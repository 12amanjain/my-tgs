package com.tgs.services.hms.sources.tripadvisor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.City;
import com.tgs.services.hms.datamodel.Country;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelUserReview;
import com.tgs.services.hms.datamodel.HotelUserReviewIdInfo;
import com.tgs.services.hms.datamodel.State;
import com.tgs.services.hms.datamodel.SubReview;
import com.tgs.services.hms.datamodel.UserReviewStaticData;
import com.tgs.services.hms.datamodel.tripadvisor.HotelRatingRequest;
import com.tgs.services.hms.datamodel.tripadvisor.HotelRatingResponse;
import com.tgs.services.hms.datamodel.tripadvisor.HotelReviewMappingResponse;
import com.tgs.services.hms.datamodel.tripadvisor.LocationMapperRequest;
import com.tgs.services.hms.datamodel.tripadvisor.Rating;
import com.tgs.services.hms.datamodel.tripadvisor.ReviewData;
import com.tgs.services.hms.datamodel.tripadvisor.TripAdvisorAddress;
import com.tgs.services.hms.datamodel.tripadvisor.TripType;
import com.tgs.services.hms.utils.HotelUtils;
import com.tgs.utils.common.HttpUtils;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TripAdvisorSearchService {
	
	public void doRatingSearch(HotelUserReview hReview) {
		
		String id = hReview.getId();
		try {
			HotelRatingResponse response = getRatingSearchResponse(id);
			if(response == null) {
				log.info("Received No Response While doing TripAdvisor Rating"
					+ " Search For userReviewSupplierId {}" , id);
				return;
			}
			log.info("Tripadvisor response is {}" , response);
			createSearchResponse(response , hReview);
		}
		catch(Exception e) {
			log.error("Error While Getting Rating From TripAdvisor For userReviewSupplierId {}" , id , e);
		}
		
	}
	
	
	private HotelRatingResponse getRatingSearchResponse(String userReviewSupplierId) throws IOException {
		
		HotelRatingRequest request =  new HotelRatingRequest();
		request.setKey(TripAdvisorConstants.KEY.getValue());
		String baseUrl = StringUtils.join(TripAdvisorConstants.LOCATION_URL.getValue(), "/" , userReviewSupplierId);
		HttpUtils httpUtils = HotelUtils.getReviewURL(request , baseUrl);
		return httpUtils.getResponse(HotelRatingResponse.class).orElse(null);
		
	}
	public void createSearchResponse(HotelRatingResponse response , HotelUserReview hReview) {
		
		hReview.setReview(Double.parseDouble(response.getRating()));
		hReview.setReviewCount(response.getReview_rating_count());
		hReview.setTotalcount(response.getNum_reviews());
		hReview.setImageUrl(response.getRating_image_url());
		hReview.setPercentRecommended(response.getPercent_recommended());
		hReview.setRedirectUrl(response.getWeb_url());
		List<SubReview> subReviews = new ArrayList<>();
		for(Rating supplierSubRating : response.getSubratings()) {
			SubReview subRating = SubReview.builder().name(supplierSubRating.getLocalized_name()).value(Double.
					parseDouble(supplierSubRating.getValue())).imageUrl(supplierSubRating.getRating_image_url()).build();
			subReviews.add(subRating);
		}
		hReview.setSubReviews(subReviews);	
	}
	
	private Address getAddress(HotelRatingResponse response) {
		
		TripAdvisorAddress adr = response.getAddress_obj();
		City city = City.builder().name(adr.getCity()).build();
		State state = State.builder().name(adr.getState()).build();
		Country country = Country.builder().name(adr.getCountry()).build();
		Address address = Address.builder().address(adr.getAddress_string())
				.addressLine1(adr.getStreet1()).addressLine2(adr.getStreet2())
				.city(city).state(state).country(country).postalCode(adr.getPostalcode())
				.build();
		return address;
	}
	
	private Map<String, Integer> getTripTypes(HotelRatingResponse response){
		
		List<TripType> tripTypeList = response.getTrip_types();
		Map<String,Integer> map = new HashMap<>();
		for(TripType tripType : tripTypeList) {
			Integer value = tripType.getValue() != null ? Integer.valueOf(tripType.getValue()) : 0;
			map.put(tripType.getLocalized_name(), value);
		}
		return map;
	}
	
	public void doIdSearch(HotelInfo hInfo, HotelUserReviewIdInfo hUserReviewSupplierIdInfo) throws Exception {
		
		GeoLocation geoLocation = hInfo.getGeolocation();
		String cleanHotelName = cleanData(hInfo);
		try {
			HotelReviewMappingResponse response = getIdSearchResponse(hInfo, cleanHotelName);
			if(response!= null && CollectionUtils.isNotEmpty(response.getData())) {
				ReviewData reviewResponse = response.getData().get(0);
				if(reviewResponse != null && StringUtils.isNotBlank(reviewResponse.getLocation_id())) {
					hInfo.setUserReviewSupplierId(reviewResponse.getLocation_id());
					hUserReviewSupplierIdInfo.setReviewId(reviewResponse.getLocation_id());
					hUserReviewSupplierIdInfo.setStaticData(getStaticData(reviewResponse.getLocation_id()));
					return;
				}
			}
			setSuggestedReviewData(hInfo, hUserReviewSupplierIdInfo);
			log.info("TripAdvisor Id  Not Found For Hotel Name {} , Latitude {} , Longitude {}" 
					, hInfo.getName(), geoLocation.getLatitude(), geoLocation.getLongitude());	
		}
		catch(Exception e) {
			log.error("Error While Creating Mapping For Hotel With Name {},"
					+ " Latitude {} , Longitude {}",hInfo.getName(), geoLocation.getLatitude()
					, geoLocation.getLongitude() , e);
			throw e;
		}	
	}
	
	private UserReviewStaticData getStaticData(String userReviewSupplierId) throws IOException {
		
		HotelRatingResponse ratingResponse = getRatingSearchResponse(userReviewSupplierId);
		String propertyCategory = ratingResponse.getCategory() != null ? ratingResponse.getCategory().getLocalized_name() : null;
		UserReviewStaticData staticData = UserReviewStaticData.builder().address(getAddress(ratingResponse))
				.category(propertyCategory).hotelName(ratingResponse.getName())
				.latitude(ratingResponse.getLatitude()).longitude(ratingResponse.getLongitude())
				.tripTypes(getTripTypes(ratingResponse)).build();
		return staticData;
	}

	
	private HotelReviewMappingResponse getIdSearchResponse(HotelInfo hInfo, String token) throws IOException {
		
		GeoLocation geoLocation = hInfo.getGeolocation();
		LocationMapperRequest request = createLocationMapperRequest(hInfo, token);
		String baseUrl = StringUtils.join(TripAdvisorConstants.LOCATION_MAPPER_URL.getValue() , "/" 
				, geoLocation.getLatitude() , "," , geoLocation.getLongitude());
		HttpUtils httpUtils = HotelUtils.getReviewURL(request , baseUrl);
		return httpUtils.getResponse(HotelReviewMappingResponse.class).orElse(null);
		
	}
	
	private String cleanData(HotelInfo hInfo) {
		
		String hotelName = hInfo.getName();
		hotelName = StringEscapeUtils.unescapeHtml4(hotelName);
		String cityName = hInfo.getAddress().getCity().getName().toLowerCase();
		hotelName = hotelName.toLowerCase().replace("bed", "").replace("and", "")
				.replace("breakfast", "").replace("the", "").replace("hotel", "")
				.replace(cityName, "");
		return hotelName;
	}
	
	private void setSuggestedReviewData(HotelInfo hInfo, 
			HotelUserReviewIdInfo hUserReviewSupplierIdInfo) throws IOException {
		
		GeoLocation geoLocation = hInfo.getGeolocation();
		HotelReviewMappingResponse response = getIdSearchResponse(hInfo, "");
		List<ReviewData> reviewDataList = response.getData();
		if(CollectionUtils.isEmpty(reviewDataList)) {
			log.info("No Suggestions Available For  Latitude {} , Longitude {}"
					,geoLocation.getLatitude(),geoLocation.getLongitude());
			return;
		}
		hUserReviewSupplierIdInfo.setReviewDataList(reviewDataList);
	}

	public LocationMapperRequest createLocationMapperRequest(HotelInfo hInfo, String cleanHotelName) {
		
		LocationMapperRequest request = new LocationMapperRequest();
		request.setCategory(TripAdvisorConstants.CATEGORY.getValue());
		request.setKey(StringUtils.join(TripAdvisorConstants.KEY.getValue() , "-" , "mapper"));
		request.setQ(cleanHotelName);
		return request;
	}
	
	

}
