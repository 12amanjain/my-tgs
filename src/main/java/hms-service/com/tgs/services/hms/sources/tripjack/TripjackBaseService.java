package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.CancellationMiscInfo;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.ProcessedOption;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomMiscInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.utils.common.HttpUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Getter
@Setter
@Slf4j
public class TripjackBaseService {

	protected HotelSupplierConfiguration supplierConf;
	protected HotelSearchQuery searchQuery;
	protected HotelSourceConfigOutput sourceConfig;
	protected HttpUtils httpUtils;
	protected RestAPIListener listener;
	protected HotelCacheHandler cacheHandler;
	protected String currency;
	protected String endpoint;
	protected CityInfoMapping cityInfo;
	protected double tripjckManagementFee;
	protected double curiocityManagementFee;
	protected HMSCommunicator hmsComm;


	protected <T> T getResponseByRequest(String request, String url, Class<T> type) throws IOException {

		httpUtils = HttpUtils.builder().urlString(url).postData(request).requestMethod(HttpUtils.REQ_METHOD_POST)
				.headerParams(getHeaderParams()).build();
		T responseBody = null;
		responseBody = httpUtils.getResponse(type).orElse(null);

		return responseBody;
	}

	public Map<String, String> getHeaderParams() {

		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("content-type", "application/json");
		headerParams.put("Accept-Encoding", "gzip, deflate");
		headerParams.put("Apikey", supplierConf.getHotelSupplierCredentials().getPassword());
		return headerParams;
	}

	public void setCancellationDeadlineFromRooms(Option option) {

		LocalDateTime earliestDeadlineDateTime = null;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			if (earliestDeadlineDateTime == null)
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
			else if (earliestDeadlineDateTime.isAfter(roomInfo.getDeadlineDateTime()))
				earliestDeadlineDateTime = roomInfo.getDeadlineDateTime();
		}
		option.setDeadlineDateTime(earliestDeadlineDateTime);
	}

	protected List<Option> createOptionFromHotel(HotelInfo hotelResponse) {

		List<Option> optionList = new ArrayList<>();

		hotelResponse.getOptions().forEach(optionResponse -> {
			double tripjackFee = getTripjackFees(optionResponse);
			List<RoomInfo> optionRoomInfos = new ArrayList<>();
			optionResponse.getRoomInfos().forEach(roomResponse -> {
				RoomInfo room = new RoomInfo();
				room.setCheckInDate(roomResponse.getCheckInDate());
				room.setCheckOutDate(roomResponse.getCheckOutDate());
				room.setRoomType(roomResponse.getRoomType());
				room.setRoomCategory(roomResponse.getRoomCategory());
				room.setMealBasis(roomResponse.getMealBasis());
				room.setId(roomResponse.getId());
				room.setNumberOfAdults(roomResponse.getNumberOfAdults());
				room.setNumberOfChild(roomResponse.getNumberOfChild());
				setRoomPriceInfo(room, roomResponse);
				RoomMiscInfo roomMiscInfo =
						RoomMiscInfo.builder().fees(tripjackFee / searchQuery.getRoomInfo().size()).build();
				room.setMiscInfo(roomMiscInfo);
				optionRoomInfos.add(room);
			});
			Option option = Option.builder().roomInfos(optionRoomInfos).isPanRequired(optionResponse.getIsPanRequired())
					.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
							.supplierHotelId(hotelResponse.getLocalHotelCode())
							.sourceId(supplierConf.getBasicInfo().getSourceId()).isDetailHit(true).build())
					.id(optionResponse.getId()).build();
			optionList.add(option);
		});
		return optionList;
	}

	protected void setRoomPriceInfo(RoomInfo room, RoomInfo roomResponse) {

		List<PriceInfo> priceInfoList = new ArrayList<>();
		roomResponse.getPerNightPriceInfos().forEach(perNightPriceInfo -> {
			PriceInfo priceInfo = new PriceInfo();
			priceInfo.setDay(perNightPriceInfo.getDay());
			Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
			fareComponents.put(HotelFareComponent.BF, perNightPriceInfo.getFareComponents().get(HotelFareComponent.TF));
			priceInfo.setFareComponents(fareComponents);
			priceInfoList.add(priceInfo);
		});
		room.setPerNightPriceInfos(priceInfoList);
	}

	protected List<HotelInfo> getHotelListFromTripjackResult(List<HotelInfo> hotelResults) {

		List<HotelInfo> hInfos = new ArrayList<>();

		hotelResults.forEach(hotelInfo -> {
			List<Option> optionList = createSearchOptionFromHotel(hotelInfo);
			HotelInfo hInfo = HotelInfo.builder().description(hotelInfo.getDescription()).name(hotelInfo.getName())
					.propertyType(hotelInfo.getPropertyType()).address(hotelInfo.getAddress())
					.geolocation(hotelInfo.getGeolocation()).rating(hotelInfo.getRating())
					.miscInfo(HotelMiscInfo.builder().searchId(searchQuery.getSearchId())
							.supplierStaticHotelId(hotelInfo.getLocalHotelCode()).correlationId(hotelInfo.getId())
							.build())
					.options(optionList).images(hotelInfo.getImages()).instructions(hotelInfo.getInstructions())
					.description(hotelInfo.getDescription()).build();
			hInfos.add(hInfo);
		});
		return hInfos;
	}

	protected List<Option> createSearchOptionFromHotel(HotelInfo hotelInfo) {

		int roomSize = searchQuery.getRoomInfo().size();
		long roomSearchNight = Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		ProcessedOption optionRes = hotelInfo.getProcessedOptions().get(0);
		List<RoomInfo> roomList = new ArrayList<>();
		double perNightRoomDiscountedPrice =
				optionRes.getDiscountedPrice() != null ? optionRes.getDiscountedPrice() / (roomSize * roomSearchNight)
						: 0.0;
		double perNightRoomPrice = perNightRoomDiscountedPrice != 0.0 ? perNightRoomDiscountedPrice
				: optionRes.getTotalPrice() / (roomSize * roomSearchNight);

		searchQuery.getRoomInfo().forEach(room -> {
			RoomInfo roomInfo = new RoomInfo();
			roomInfo.setRoomCategory("Dummy Room Category");
			roomInfo.setRoomType("Dummy Room Type");
			roomInfo.setNumberOfAdults(room.getNumberOfAdults());
			roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomInfo.setNumberOfChild(room.getNumberOfChild() != null ? room.getNumberOfChild() : 0);
			List<PriceInfo> priceInfoList = new ArrayList<>();
			for (int i = 1; i <= roomSearchNight; i++) {
				PriceInfo priceInfo = new PriceInfo();
				priceInfo.setDay(i);
				Map<HotelFareComponent, Double> fareComponents = priceInfo.getFareComponents();
				fareComponents.put(HotelFareComponent.BF, perNightRoomPrice);
				priceInfo.setFareComponents(fareComponents);
				priceInfoList.add(priceInfo);
			}
			roomInfo.setPerNightPriceInfos(priceInfoList);
			roomInfo.setMealBasis(optionRes.getFacilities().get(0));
			roomList.add(roomInfo);
		});
		Option option = Option.builder().roomInfos(roomList)
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(hotelInfo.getLocalHotelCode())
						.sourceId(supplierConf.getBasicInfo().getSourceId()).build())
				.id(RandomStringUtils.random(20, true, true)).build();
		return new ArrayList<>(Arrays.asList(option));
	}

	protected void setCancellationPolicy(Option option, HotelCancellationPolicy cancellationPolicyResponse) {

		option.setCancellationPolicy(cancellationPolicyResponse);
		LocalDateTime deadlineDate = null;
		if (cancellationPolicyResponse != null && cancellationPolicyResponse.getPenalyDetails() != null) {
			deadlineDate = cancellationPolicyResponse.getPenalyDetails().stream()
					.filter(pd -> pd.getPenaltyAmount() > 0).findFirst().get().getFromDate();
		}
		option.setDeadlineDateTime(
				deadlineDate == null ? option.getRoomInfos().get(0).getCheckInDate().atTime(12, 00) : deadlineDate);
		if (cancellationPolicyResponse != null && cancellationPolicyResponse.getRoomCancellationPolicyList() != null
				&& CollectionUtils.isNotEmpty(cancellationPolicyResponse.getRoomCancellationPolicyList())) {
			Map<String, RoomInfo> map =
					option.getRoomInfos().stream().collect(Collectors.toMap(RoomInfo::getId, Function.identity()));

			cancellationPolicyResponse.getRoomCancellationPolicyList().forEach(roomResponse -> {
				RoomInfo room = map.get(roomResponse.getId());
				if (roomResponse.getCancellationPolicy() != null
						&& CollectionUtils.isNotEmpty(roomResponse.getPenalyDetails())) {
					room.setCancellationPolicy(roomResponse);
					room.setDeadlineDateTime(room.getDeadlineDateTime());
				}
			});
		}
		setBufferTimeinCnp(option, cancellationPolicyResponse);
	}

	private void setBufferTimeinCnp(Option option, HotelCancellationPolicy cancellationPolicyResponse) {

		LocalDateTime checkInDate = option.getRoomInfos().get(0).getCheckInDate().atStartOfDay();
		LocalDateTime now = LocalDateTime.now();
		LocalDateTime deadlineDateTime = now;
		boolean isFullRefundAllowed = false;
		Integer cancellationBuffer =
				sourceConfig.getCancellationPolicyBuffer() != null ? sourceConfig.getCancellationPolicyBuffer() : 0;

		List<PenaltyDetails> pds = cancellationPolicyResponse.getPenalyDetails();
		if (pds != null) {
			for (PenaltyDetails pd : pds) {
				LocalDateTime fDate = pd.getFromDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				LocalDateTime tDate = pd.getToDate().minus(cancellationBuffer, ChronoUnit.HOURS);
				pd.setFromDate(fDate);
				pd.setToDate(tDate);
				pd.setPenaltyAmount(pd.getPenaltyAmount() + tripjckManagementFee);
			}
			pds.get(0).setFromDate(now);
			pds.get(pds.size() - 1).setToDate(checkInDate);

			Iterator<PenaltyDetails> i = pds.iterator();
			PenaltyDetails pd = null;
			while (i.hasNext()) {
				pd = (PenaltyDetails) i.next();
				LocalDateTime tDate = pd.getToDate();
				if (tDate.isBefore(now)) {
					i.remove();
				} else {
					LocalDateTime fDate = pd.getFromDate();
					if (fDate.isBefore(now))
						i.remove();
					if (pd.getPenaltyAmount() == 0.0) {
						deadlineDateTime = tDate;
						isFullRefundAllowed = true;
					}
				}
			}
		}
		HotelCancellationPolicy cancellationPolicy = HotelCancellationPolicy.builder()
				.id(cancellationPolicyResponse.getId()).isFullRefundAllowed(isFullRefundAllowed).penalyDetails(pds)
				.miscInfo(CancellationMiscInfo.builder().isBookingAllowed(true).isSoldOut(false).build()).build();
		option.setDeadlineDateTime(deadlineDateTime);
		option.setCancellationPolicy(cancellationPolicy);
	}

	public double getTripjackFees(Option resOption) {
		tripjckManagementFee = 0;
		for (RoomInfo roomInfo : resOption.getRoomInfos()) {
			tripjckManagementFee += roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
					.getOrDefault(HotelFareComponent.MF, 0d)
					+ roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
							.getOrDefault(HotelFareComponent.MFT, 0d)
					+ roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF)
							.getOrDefault(HotelFareComponent.PF, 0d);
		}
		return tripjckManagementFee;
	}

	public void storeLogs(String identifier, String key, BaseResponse response) {

		Map<String, Long> checkPoints = httpUtils.getCheckPoints().stream()
				.collect(Collectors.toMap(CheckPointData::getType, CheckPointData::getTime));

		SystemContextHolder.getContextData().addCheckPoints(httpUtils.getCheckPoints());
		if (ObjectUtils.isEmpty(response)) {
			SystemContextHolder.getContextData().getErrorMessages()
					.add(httpUtils.getResponseString() + httpUtils.getPostData());
		}
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getPostData()).type(identifier)
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_STARTED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getResponseString()).type(identifier)
				.generationTime(Instant.ofEpochMilli(checkPoints.get(SystemCheckPoint.EXTERNAL_API_FINISHED.name()))
						.atZone(ZoneId.systemDefault()).toLocalDateTime())
				.build());
	}

}
