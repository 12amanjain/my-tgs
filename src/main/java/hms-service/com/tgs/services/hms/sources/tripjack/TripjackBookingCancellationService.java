package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.restmodel.tripjack.TJBookingDetailResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@SuperBuilder
@Service
public class TripjackBookingCancellationService extends TripjackBaseService {

	private Order order;
	private HotelInfo hInfo;
	private BaseResponse cancellationResponse;
	private TJBookingDetailResponse bookingDetailResponse;

	public boolean cancelBooking() throws IOException {

		String requestUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING_CANCELLATION.value);
		String supplierBookingId = hInfo.getMiscInfo().getSupplierBookingReference();

		listener = new RestAPIListener("");
		cancellationResponse = getResponseByRequest(null, requestUrl + supplierBookingId, BaseResponse.class);
		if (cancellationResponse.getStatus().getHttpStatus() == 200) {
			storeLogs("Tripjack-Cancellation", order.getBookingId(), cancellationResponse);
			try {
				String bookingDetailRequestUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING_DETAIL.value);
				BookingDetailRequest bookingDetailRequest =
						BookingDetailRequest.builder().bookingId(supplierBookingId).build();
				bookingDetailResponse = getResponseByRequest(GsonUtils.getGson().toJson(bookingDetailRequest),
						bookingDetailRequestUrl, TJBookingDetailResponse.class);
				if (bookingDetailResponse.getOrder().getStatus().equals("CANCELLED")) {
					return true;
				}
			} finally {
				storeLogs("Tripjack-cancelled-Booking-detail", order.getBookingId(), bookingDetailResponse);
			}
		}
		return false;
	}
}

