package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyRequest;
import com.tgs.services.hms.restmodel.HotelCancellationPolicyResponse;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class TripjackCancellationPolicyService extends TripjackBaseService {

	private HotelCancellationPolicyResponse cancellationPolicyResponse;
	private HotelInfo hInfo;

	public void searchCancellationPolicy(String logKey, String endpoint) throws IOException {

		String requestUrl = StringUtils.join(endpoint, TripjackConstant.CANCELLATION_POLICY.value);
		listener = new RestAPIListener("");
		try {
			HotelCancellationPolicyRequest cancellationPolicyRequest = new HotelCancellationPolicyRequest();
			cancellationPolicyRequest.setId(hInfo.getMiscInfo().getCorrelationId());
			cancellationPolicyRequest.setOptionId(hInfo.getOptions().get(0).getId());

			cancellationPolicyResponse = getResponseByRequest(GsonUtils.getGson().toJson(cancellationPolicyRequest),
					requestUrl, HotelCancellationPolicyResponse.class);
			int RoomCount = hInfo.getOptions().get(0).getRoomInfos().size();
			tripjckManagementFee =
					hInfo.getOptions().get(0).getRoomInfos().get(0).getMiscInfo().getFees() * (RoomCount);
			curiocityManagementFee = hInfo.getOptions().get(0).getRoomInfos().get(0).getTotalAddlFareComponents()
					.get(HotelFareComponent.TAF).getOrDefault(HotelFareComponent.MF, 0.0) * (RoomCount);

			if (!ObjectUtils.isEmpty(cancellationPolicyResponse)) {
				setCancellationPolicy(hInfo.getOptions().get(0), cancellationPolicyResponse.getCancellationPolicy());
			}
		} finally {
			storeLogs("Tripjack-cancellation policy", logKey, cancellationPolicyResponse);
		}
	}
}
