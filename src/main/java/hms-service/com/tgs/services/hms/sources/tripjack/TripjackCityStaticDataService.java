package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.restmodel.FetchHotelCityInfoResponse;
import com.tgs.services.hms.restmodel.HotelCityInfoQuery;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;

@SuperBuilder
public class TripjackCityStaticDataService extends TripjackBaseService {

	protected FetchHotelCityInfoResponse staticDataRequest;
	private HotelCityInfoMappingManager cityManager;

	public void saveTripjackCityList() throws IOException {
		String endPoint = supplierConf.getHotelSupplierCredentials().getUrl();
		String next = "";
		do {
			List<HotelCityInfoQuery> cityList = new ArrayList<>();
			String requestUrl = StringUtils.join(endPoint, TripjackConstant.STATIC_CITY_SUFFIX.value, next);
			HttpUtils httpUtils = HttpUtils.builder().urlString(requestUrl).headerParams(getHeaderParams())
					.requestMethod("GET").timeout(90 * 1000).build();
			FetchHotelCityInfoResponse response = httpUtils.getResponse(FetchHotelCityInfoResponse.class).orElse(null);
			response.getResponse().getCityInfoList().forEach(city -> {
				HotelCityInfoQuery cityInfoQuery = new HotelCityInfoQuery();
				cityInfoQuery.setCityname(city.getCityName());
				cityInfoQuery.setCountryname(city.getCountryName());
				cityInfoQuery.setSuppliercityid(city.getId() + "");
				cityInfoQuery.setSuppliername(HotelSourceType.TRIPJACK.name());
				cityInfoQuery.setSuppliercountryid(city.getCountryId());
				cityList.add(cityInfoQuery);
			});
			cityManager.saveCityInfoMappingList(cityList);
			next = response.getResponse().getNext();
		} while (next != null);
	}
}
