package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.dbmodel.DbHotelInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.restmodel.FetchHotelInfoResponse;
import com.tgs.utils.common.HttpUtils;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@SuperBuilder
@Slf4j
public class TripjackHotelStaticDataService extends TripjackBaseService {

	protected HotelInfoSaveManager hotelInfoSaveManager;

	public void process() throws IOException {

		String endPoint = supplierConf.getHotelSupplierCredentials().getUrl();
		String next = "";
		try {
			do {
				List<HotelInfo> hotelList = new ArrayList<>();
				String requestUrl = StringUtils.join(endPoint, TripjackConstant.STATIC_HOTEL_SUFFIX.value, next);
				HttpUtils httpUtils = HttpUtils.builder().urlString(requestUrl).headerParams(getHeaderParams())
						.requestMethod("GET").timeout(90 * 1000).build();
				FetchHotelInfoResponse response = httpUtils.getResponse(FetchHotelInfoResponse.class).orElse(null);
				response.getResponse().getHotelInfoList().forEach(hotel -> {
					HotelInfo hInfo = HotelInfo.builder().address(hotel.getAddress()).facilities(hotel.getFacilities())
							.id(hotel.getId()).name(hotel.getName()).rating(hotel.getRating())
							.contact(hotel.getContact()).description(hotel.getDescription())
							.geolocation(hotel.getGeolocation()).giataId(hotel.getGiataId())
							.instructions(hotel.getInstructions()).build();
					hotelList.add(hInfo);
				});
				saveHotelInfoList(hotelList);
				next = response.getResponse().getNext();
			} while (next != null);
		} catch (Exception e) {
			log.error("Error while fetching static data {}", e);
		}
	}

	private void saveHotelInfoList(List<HotelInfo> hInfoList) {

		for (HotelInfo hInfo : hInfoList) {
			try {
				DbHotelInfo dbHotelInfo = new DbHotelInfo().from(hInfo);
				dbHotelInfo.setId(null);
				hotelInfoSaveManager.saveHotelInfo(dbHotelInfo, hInfo.getId(), HotelSourceType.TRIPJACK.name(),
						HotelSourceType.TRIPJACK);
			} catch (Exception e) {
				log.error("Error While Storing Master Hotel With Name {} Rating {}", hInfo.getName(), hInfo.getRating(),
						e);
			}
		}
	}
}

