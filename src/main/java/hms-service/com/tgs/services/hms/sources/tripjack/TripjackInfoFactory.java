package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.helper.HotelCacheHandler;
import com.tgs.services.hms.sources.AbstractHotelInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TripjackInfoFactory extends AbstractHotelInfoFactory {

	@Autowired
	protected HotelCacheHandler cacheHandler;

	public TripjackInfoFactory(HotelSearchQuery searchQuery, HotelSupplierConfiguration supplierConf) {

		super(searchQuery, supplierConf);
	}

	@Override
	protected void searchAvailableHotels() throws IOException {
		String endpoint = supplierConf.getHotelSupplierCredentials().getUrl();
		CityInfoMapping supplierCityInfo =
				cacheHandler.getSupplierCityInfoFromCityId(searchQuery.getSearchCriteria().getCityId(), "TRIPJACK");

		TripjackSearchService searchService = TripjackSearchService.builder().sourceConfig(sourceConfigOutput)
				.endpoint(endpoint).cityInfo(supplierCityInfo).supplierConf(this.getSupplierConf())
				.searchQuery(searchQuery).build();
		searchService.doSearch();
		searchResult = searchService.getSearchResult();
	}

	@Override
	protected void searchHotel(HotelInfo hInfo, HotelSearchQuery searchQuery) throws IOException {
		String endpoint = supplierConf.getHotelSupplierCredentials().getUrl();
		TripjackSearchService searchService = TripjackSearchService.builder().supplierConf(this.getSupplierConf())
				.endpoint(endpoint).searchQuery(searchQuery).build();
		searchService.doDetailSearch(hInfo);
		for (Option option : hInfo.getOptions()) {
			option.getMiscInfo().setIsDetailHit(true);
		}
	}

	@Override
	protected void searchCancellationPolicy(HotelInfo hotel, String logKey) throws IOException {
		boolean isCancellationPolicyAlreadySet = false;
		HotelInfo cachedHotel = cacheHandler.getCachedHotelById(hotel.getId());
		String optionId = hotel.getOptions().get(0).getId();
		Option cachedOption = cachedHotel.getOptions().stream().filter(opt -> opt.getId().equals(optionId))
				.collect(Collectors.toList()).get(0);
		for (Option op : hotel.getOptions()) {
			if (op.getId().equals(optionId) && !ObjectUtils.isEmpty(cachedOption.getCancellationPolicy())) {
				op.setCancellationPolicy(cachedOption.getCancellationPolicy());
				isCancellationPolicyAlreadySet = true;
			}
		}
		String endpoint = supplierConf.getHotelSupplierCredentials().getUrl();
		if (!isCancellationPolicyAlreadySet) {
			TripjackCancellationPolicyService cancellationPolicyService = TripjackCancellationPolicyService.builder()
					.sourceConfig(sourceConfigOutput).hInfo(hotel).supplierConf(this.getSupplierConf()).build();
			cancellationPolicyService.searchCancellationPolicy(logKey, endpoint);
		}
	}
}
