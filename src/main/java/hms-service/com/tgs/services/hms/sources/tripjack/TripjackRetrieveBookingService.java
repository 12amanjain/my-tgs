package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchQuery.HotelSearchQueryBuilder;
import com.tgs.services.hms.datamodel.HotelSearchQueryMiscInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.restmodel.tripjack.TJBookingDetailResponse;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
@SuperBuilder
public class TripjackRetrieveBookingService extends TripjackBaseService {

	private HotelImportBookingParams importBookingParams;
	private HotelImportedBookingInfo bookingInfo;
	TJBookingDetailResponse bookingDetailResponse;

	public void retrieveBooking() throws IOException {

		try {
			listener = new RestAPIListener("");
			String supplierBookingReference = importBookingParams.getSupplierBookingId();
			String endpoint = supplierConf.getHotelSupplierCredentials().getUrl();
			String requestUrl = StringUtils.join(endpoint, TripjackConstant.BOOKING_DETAIL.value);
			BookingDetailRequest bookingDetailRequest =
					BookingDetailRequest.builder().bookingId(supplierBookingReference).build();

			bookingDetailResponse = getResponseByRequest(GsonUtils.getGson().toJson(bookingDetailRequest), requestUrl,
					TJBookingDetailResponse.class);
			bookingInfo = createBookingDetailResponse();
		} finally {
			storeLogs("Tripjack-RetrieveBooking", importBookingParams.getBookingId(), bookingDetailResponse);
		}
	}

	private HotelImportedBookingInfo createBookingDetailResponse() {

		if (bookingDetailResponse == null)
			throw new CustomGeneralException("Error While Trying To Fetch Booking Details");
		HotelInfo hotelInfo = bookingDetailResponse.getItemInfos().get("HOTEL").getHInfo();

		HotelInfo hInfo = getHotelDetails(hotelInfo);
		searchQuery = getHotelSearchQuery();
		Option optionRes = bookingDetailResponse.getItemInfos().get("HOTEL").getHInfo().getOptions().get(0);

		List<RoomInfo> roomInfos = new ArrayList<>();
		for (RoomInfo roomResponse : optionRes.getRoomInfos()) {
			RoomInfo room = new RoomInfo();
			room.setId(roomResponse.getId());
			room.setRoomCategory(roomResponse.getRoomCategory());
			room.setCheckInDate(roomResponse.getCheckInDate());
			room.setCheckOutDate(roomResponse.getCheckOutDate());
			room.setRoomType(roomResponse.getRoomType());
			setRoomPriceInfo(room, roomResponse);
			room.setTravellerInfo(roomResponse.getTravellerInfo());
			room.setCancellationPolicy(roomResponse.getCancellationPolicy());
			roomInfos.add(room);
		}
		double totalPrice = optionRes.getTotalPrice();
		Option option = Option.builder().totalPrice(totalPrice)
				.miscInfo(OptionMiscInfo.builder().supplierId(supplierConf.getBasicInfo().getSupplierName())
						.secondarySupplier(supplierConf.getBasicInfo().getSupplierName())
						.supplierHotelId(hInfo.getLocalHotelCode()).sourceId(supplierConf.getBasicInfo().getSourceId())
						.build())
				.roomInfos(roomInfos).id(optionRes.getId()).build();
		tripjckManagementFee = getTripjackFees(optionRes);

		setCancellationPolicy(option, optionRes.getCancellationPolicy());
		List<Option> options = new ArrayList<>();
		options.add(option);
		hInfo.setOptions(Arrays.asList(option));
		String orderStatus = bookingDetailResponse.getOrder().getStatus();
		option.getRoomInfos().stream().forEach(ri -> {
			ri.setDeadlineDateTime(optionRes.getDeadlineDateTime());
		});

		return HotelImportedBookingInfo.builder().hInfo(hInfo).searchQuery(searchQuery).orderStatus(orderStatus)
				.bookingCurrencyCode("INR").deliveryInfo(bookingDetailResponse.getOrder().getDeliveryInfo())
				.bookingDate(bookingDetailResponse.getOrder().getCreatedOn().toLocalDate()).build();
	}

	private HotelSearchQuery getHotelSearchQuery() {
		HotelSearchQuery searchQuery = bookingDetailResponse.getItemInfos().get("HOTEL").getQuery();
		HotelSearchQueryBuilder searchQueryBuilder = HotelSearchQuery.builder();
		searchQueryBuilder.checkinDate(searchQuery.getCheckinDate());
		searchQueryBuilder.checkoutDate(searchQuery.getCheckoutDate());
		searchQueryBuilder.sourceId(11);
		searchQueryBuilder
				.miscInfo(HotelSearchQueryMiscInfo.builder().supplierId(HotelSourceType.TRIPJACK.name()).build());
		return searchQueryBuilder.build();
	}

	private HotelInfo getHotelDetails(HotelInfo hotelInfo) {

		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().supplierStaticHotelId(hotelInfo.getLocalHotelCode())
				.hotelBookingReference(hotelInfo.getMiscInfo().getHotelBookingReference())
				.supplierBookingReference(bookingDetailResponse.getOrder().getBookingId()).build();
		HotelInfo hInfo = HotelInfo.builder().miscInfo(hMiscInfo).name(hotelInfo.getName())
				.address(hotelInfo.getAddress()).description(hotelInfo.getDescription()).images(hotelInfo.getImages())
				.rating(hotelInfo.getRating()).instructions(hotelInfo.getInstructions()).contact(hotelInfo.getContact())
				.facilities(hotelInfo.getFacilities()).geolocation(hotelInfo.getGeolocation())
				.propertyType(hotelInfo.getPropertyType()).build();
		return hInfo;
	}
}
