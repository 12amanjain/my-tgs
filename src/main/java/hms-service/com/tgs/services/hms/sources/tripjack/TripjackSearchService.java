package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchCriteria;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.restmodel.HotelDetailRequest;
import com.tgs.services.hms.restmodel.HotelDetailResponse;
import com.tgs.services.hms.restmodel.HotelSearchQueryListResponse;
import com.tgs.services.hms.restmodel.HotelSearchRequest;
import com.tgs.services.hms.restmodel.HotelSearchResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
@SuperBuilder
public class TripjackSearchService extends TripjackBaseService {

	private HotelSearchResult searchResult;
	private List<Integer> propertyIds;
	private HotelSearchResponse searchResponse;
	private HotelSearchQueryListResponse searchListResponse;
	private final static int MAX_SEARCH_HIT = 30;

	public void doSearch() throws IOException {

		searchResult = new HotelSearchResult();
		String requestUrl = StringUtils.join(endpoint, TripjackConstant.SEARCH_LIST.value);
		listener = new RestAPIListener("");
		HotelSearchRequest searchRequest = getTripjackSearchRequest();
		searchListResponse = getResponseByRequest(GsonUtils.getGson().toJson(searchRequest), requestUrl,
				HotelSearchQueryListResponse.class);
		try {
			if (searchListResponse.getStatus().getHttpStatus().equals(HttpURLConnection.HTTP_OK)
					&& searchListResponse.getSearchIds() != null) {
				storeLogs("Tripjack-searchList", searchQuery.getSearchId(), searchListResponse);

				HotelSearchRequest search = new HotelSearchRequest();
				search.setSearchId(searchListResponse.getSearchIds().get(0));
				String searchRequestUrl = StringUtils.join(endpoint, TripjackConstant.SEARCH.value);
				searchResponse = getResponseByRequest(GsonUtils.getGson().toJson(search), searchRequestUrl,
						HotelSearchResponse.class);

				int index = 0;
				if (searchResponse.getStatus().getHttpStatus().equals(200)) {
					while (searchResponse.getRetryInSecond() != null && index < MAX_SEARCH_HIT) {
						try {
							Thread.sleep(2 * 1000);
						} catch (InterruptedException e) {
							log.error("error while searching hotel list {}", e);
						}
						searchResponse = getResponseByRequest(GsonUtils.getGson().toJson(search), searchRequestUrl,
								HotelSearchResponse.class);
						index++;
					}
					if (searchResponse.getSearchResult() != null
							&& CollectionUtils.isNotEmpty(searchResponse.getSearchResult().getHotelInfos())) {
						searchResult.setHotelInfos(
								getHotelListFromTripjackResult(searchResponse.getSearchResult().getHotelInfos()));
					}
				} else {
					log.error("unable to fetch search list response from tripjack");
				}
			} else {
				log.error("unable to fetch search id from tripjack");
			}
		} finally {
			storeLogs("Tripjack-search", searchQuery.getSearchId(), searchResponse);
		}
	}

	private HotelSearchRequest getTripjackSearchRequest() {

		HotelSearchRequest searchRequest = new HotelSearchRequest();
		List<RoomSearchInfo> roomSearchInfoList = new ArrayList<>();
		searchQuery.getRoomInfo().forEach(roomInfo -> {
			RoomSearchInfo roomSearchInfo = RoomSearchInfo.builder().numberOfAdults(roomInfo.getNumberOfAdults())
					.numberOfChild(roomInfo.getNumberOfChild()).build();
			if (roomInfo.getNumberOfChild() != null && roomInfo.getNumberOfChild() > 0) {
				roomSearchInfo.setChildAge(roomInfo.getChildAge());
			}
			roomSearchInfoList.add(roomSearchInfo);
		});
		HotelSearchCriteria searchCriteria = new HotelSearchCriteria();
		searchCriteria.setCityId(cityInfo.getSupplierCity());
		searchCriteria.setCountryId(cityInfo.getSupplierCountry());
		searchCriteria.setNationality(searchQuery.getSearchCriteria().getNationality());
		searchCriteria.setCountryOfResidence(searchQuery.getSearchCriteria().getCountryOfResidence());
		HotelSearchQuery searchReqQuery = HotelSearchQuery.builder().checkinDate(searchQuery.getCheckinDate())
				.checkoutDate(searchQuery.getCheckoutDate()).searchPreferences(searchQuery.getSearchPreferences())
				.roomInfo(roomSearchInfoList).searchCriteria(searchCriteria).build();
		searchRequest.setSearchQuery(searchReqQuery);
		searchRequest.setSync(false);
		return searchRequest;
	}

	public void doDetailSearch(HotelInfo hInfo) throws IOException {

		HotelDetailRequest searchDetailRequest = new HotelDetailRequest();
		searchDetailRequest.setId(hInfo.getMiscInfo().getCorrelationId());
		String detailSearchUrl = StringUtils.join(endpoint, TripjackConstant.DETAIL_SEARCH.value);
		listener = new RestAPIListener("");
		try {
			HotelDetailResponse searchDetailRespone = getResponseByRequest(
					GsonUtils.getGson().toJson(searchDetailRequest), detailSearchUrl, HotelDetailResponse.class);
			createDetailSearchResult(searchDetailRespone, hInfo);
		} finally {
			storeLogs("Tripjack-DetailSearch", hInfo.getMiscInfo().getSearchId(), searchResponse);
		}
	}

	private void createDetailSearchResult(HotelDetailResponse searchDetailRespone, HotelInfo hInfo) {

		if (searchDetailRespone == null)
			return;
		if (searchDetailRespone.getStatus().getHttpStatus().equals(HttpURLConnection.HTTP_OK)
				&& searchDetailRespone.getHotel() != null) {
			HotelInfo hotel = searchDetailRespone.getHotel();
			hInfo.setImages(hotel.getImages());
			hInfo.setDescription(hotel.getDescription());
			hInfo.setFacilities(hotel.getFacilities());
			List<Option> optionList = createOptionFromHotel(searchDetailRespone.getHotel());
			hInfo.setOptions(optionList);
		} else {
			log.error("unable to get detail search response from tripjack, status code {}  due to {}",
					searchDetailRespone.getStatus().getHttpStatus(), searchDetailRespone.getErrorMessage());
		}

	}
}
