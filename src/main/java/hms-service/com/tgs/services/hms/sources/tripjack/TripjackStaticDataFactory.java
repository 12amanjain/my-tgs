package com.tgs.services.hms.sources.tripjack;

import java.io.IOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierConfiguration;
import com.tgs.services.hms.manager.mapping.HotelCityInfoMappingManager;
import com.tgs.services.hms.manager.mapping.HotelInfoSaveManager;
import com.tgs.services.hms.sources.AbstractStaticDataInfoFactory;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TripjackStaticDataFactory extends AbstractStaticDataInfoFactory {

	@Autowired
	private HotelCityInfoMappingManager cityManager;

	@Autowired
	protected HotelInfoSaveManager hotelInfoSaveManager;

	public TripjackStaticDataFactory(HotelSupplierConfiguration supplierConf,
			HotelStaticDataRequest staticDataRequest) {
		super(supplierConf, staticDataRequest);
	}

	@Override
	protected void getHotelStaticData() throws IOException {

		TripjackHotelStaticDataService TripjacktaticDataService = TripjackHotelStaticDataService.builder()
				.hotelInfoSaveManager(hotelInfoSaveManager).supplierConf(supplierConf).build();
		TripjacktaticDataService.process();
	}

	@Override
	protected void getCityMappingData() throws IOException {

		TripjackCityStaticDataService staticCityService =
				TripjackCityStaticDataService.builder().supplierConf(supplierConf).cityManager(cityManager).build();
		staticCityService.saveTripjackCityList();
	}
}
