package com.tgs.services.hms.utils;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.HotelSearchType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.GeoLocation;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelKeyInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.HotelTag;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelClientFeeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSourceConfigOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelSupplierConfigOutput;
import com.tgs.services.hms.datamodel.inventory.HotelRatePlan;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.dbmodel.DbHotelUserReviewIdInfo;
import com.tgs.services.hms.helper.HotelConfiguratorHelper;
import com.tgs.services.hms.helper.HotelRoomTypeHelper;
import com.tgs.services.hms.helper.HotelSourceType;
import com.tgs.services.hms.helper.HotelSupplierConfigurationHelper;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HotelUtils {

	public static final String MASTER_HOTEL_ID_PREPEND = "M";

	private static <T> Map<String, String> convertPojoToMap(T request) {
		ObjectMapper mapper = new ObjectMapper();
		Map<String, String> responseMap = mapper.convertValue(request, new TypeReference<Map<String, String>>() {});
		return responseMap;
	}

	public static <T> HttpUtils getReviewURL(T request, String baseUrl) {
		Map<String, String> queryParams = HotelUtils.convertPojoToMap(request);
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Accept-Encoding", "gzip");
		HttpUtils httpUtils =
				HttpUtils.builder().urlString(baseUrl).headerParams(headerParams).queryParams(queryParams).build();
		return httpUtils;
	}

	public static void populateMissingParametersInHotelSearchQuery(HotelSearchQuery searchQuery) {

		if (searchQuery.getSearchPreferences().getRatings() == null) {
			searchQuery.getSearchPreferences().setRatings(new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5)));
		}
		if (searchQuery.getSearchCriteria().getNationality() == null) {
			searchQuery.getSearchCriteria().setNationality("106");
		}
		if (searchQuery.getSearchPreferences().getCurrency() == null) {
			searchQuery.getSearchPreferences().setCurrency("INR");
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getUniqueHotelKey(T... combinationOfValues) {
		return (T) StringUtils.join(combinationOfValues, "-");
	}

	/**
	 * Intially id consist of datbaseId , this function will generate new Id based on search Id and set new Id.
	 * Otherwise it will generate hashcode and set hashcode.
	 * 
	 * @param hotel
	 * @param searchQuery
	 * @return
	 */
	public static String generateKey(HotelInfo hotel, HotelSearchQuery searchQuery) {

		String key = null;
		if (hotel.getId() != null)
			key = StringUtils.join(searchQuery.getSearchId(), "-", MASTER_HOTEL_ID_PREPEND, hotel.getId());
		else if (StringUtils.isNotBlank(hotel.getName())) {
			key = StringUtils.join(searchQuery.getSearchId(), "-", getKey(getKeyMetaInfo(hotel)));
		}
		return key;
	}

	public static String getSearchId(String id) {
		return id.split("-")[0];
	}

	public static String getHotelId(String id) {
		return id.split("-")[1];
	}

	public static String getSourceNameFromSourceId(Integer sourceId) {
		HotelSourceType hotelSourceType = HotelSourceType.getHotelSourceType(sourceId);
		return hotelSourceType.name();
	}

	public static Option filterOptionById(HotelInfo hotel, String id) {

		if (hotel == null) {
			return null;
		}
		return hotel.getOptions().stream().filter(opts -> {
			return opts.getId().equals(id);
		}).findFirst().orElse(null);
	}

	public static Option filterOptionByPriceAndRoomCategory(HotelInfo hotel, Option oldOption) {

		if (Objects.nonNull(hotel)) {
			for (Option option : hotel.getOptions()) {
				if (option.getTotalPrice() - oldOption.getTotalPrice() == 0) {
					if (checkIfRoomTypeSame(option, oldOption))
						return option;
				}
			}
		}
		return null;
	}

	public static boolean checkIfRoomTypeSame(Option option, Option oldOption) {

		for (int i = 0; i < option.getRoomInfos().size(); i++) {
			RoomInfo newRoomInfo = option.getRoomInfos().get(i);
			RoomInfo oldRoomInfo = oldOption.getRoomInfos().get(i);
			if (!(newRoomInfo.getRoomCategory().equalsIgnoreCase(oldRoomInfo.getRoomCategory()))
					|| !(newRoomInfo.getMealBasis().equalsIgnoreCase(oldRoomInfo.getMealBasis()))) {
				return false;
			}
		}
		return true;
	}

	public static HotelInfo filterHotel(HotelSearchResult result, HotelInfo oldHInfo) {
		if (result == null || CollectionUtils.isEmpty(result.getHotelInfos())) {
			return null;
		}

		Optional<HotelInfo> hotelInfo = result.getHotelInfos().stream().filter(hInfo -> {
			return hInfo.getGiataId() != null ? hInfo.getGiataId().equals(oldHInfo.getGiataId()) : false;
		}).findFirst();

		if (!hotelInfo.isPresent()) {
			hotelInfo = result.getHotelInfos().stream().filter(hInfo -> {
				return hInfo.getName().equalsIgnoreCase((oldHInfo.getName()));
			}).findFirst();
		}

		return hotelInfo.orElse(null);
	}

	public static Proxy getProxyFromConfigurator() {

		Proxy proxy = null;
		String proxyAddress = null;
		HotelGeneralPurposeOutput configuratorInfo =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(null, HotelConfiguratorRuleType.GNPURPOSE);
		if (configuratorInfo != null) {
			proxyAddress = configuratorInfo.getProxyAddress();
			if (StringUtils.isNotBlank(proxyAddress)) {
				proxy = new java.net.Proxy(java.net.Proxy.Type.HTTP,
						new InetSocketAddress(proxyAddress.split(":")[0], Integer.valueOf(proxyAddress.split(":")[1])));
			}
		}
		return proxy;
	}

	public static String getHotelTypeCode(boolean isDomestic) {
		return isDomestic ? HotelSearchType.DOMESTIC.getCode() : HotelSearchType.INTERNATIONAL.getCode();
	}

	public static String getSupplierBinName(String sourceName) {
		/**
		 * Due to the limitation of aerospike binName
		 */

		if (sourceName.length() > 12) {
			sourceName = sourceName.substring(0, 9);
		}
		return "S_" + sourceName + "_ID";
	}

	public static String getSupplierSetName(String supplierName) {
		return "S_" + supplierName;
	}

	public static String getSearchTypeKey(HotelSearchQuery hotelSearchQuery) {
		return "DOM";
	}

	public static HotelSourceType getSourceType(HotelSearchQuery searchQuery) {
		return HotelSourceType.getHotelSourceType(searchQuery.getSourceId());
	}

	public static List<Map<?, ?>> readObjectsFromCsv(File file) throws IOException {
		CsvSchema bootstrap = CsvSchema.emptySchema().withEscapeChar('\\').withHeader();
		CsvMapper csvMapper = new CsvMapper();
		MappingIterator<Map<?, ?>> mappingIterator = csvMapper.reader(Map.class).with(bootstrap).readValues(file);
		return mappingIterator.readAll();
	}

	public static List<Map<?, ?>> getCSVListFromFile(String filePath) throws Exception {
		File inputFile = new File(filePath);
		return HotelUtils.readObjectsFromCsv(inputFile);

	}


	public static List<HotelSupplierInfo> getHotelSupplierInfo(HotelSearchQuery searchQuery, ContextData contextData) {

		List<HotelSupplierInfo> supplierInfos = new ArrayList<>();
		contextData = contextData == null ? SystemContextHolder.getContextData() : contextData;
		String userId = null;
		if (contextData.getUser() != null) {
			User user = contextData.getUser();
			userId = user.getPartnerId().equals(UserUtils.DEFAULT_PARTNERID) ? user.getUserId() : user.getPartnerId();
		}

		HotelBasicFact basicFact =
				HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery).generateFactFromUserId(userId);

		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.SUPPLIERCONFIG);

		/*
		 * Set all enabled suppliers of requested source id(s) if no SUPPLIERCONFIG rule type is present.
		 */
		HotelConfiguratorInfo configuratorInfo = null;
		if (!CollectionUtils.isEmpty(hotelconfigrules)) {

			configuratorInfo = HotelConfiguratorHelper.getRule(basicFact, hotelconfigrules);
			HotelSupplierConfigOutput configuratorRuleOutput =
					!ObjectUtils.isEmpty(configuratorInfo) ? (HotelSupplierConfigOutput) configuratorInfo.getOutput()
							: null;

			/*
			 * If Condition - Set only those enabled suppliers which are configured in the output of the satisfied rule.
			 * Else Condition - Set all enabled suppliers of requested source id(s) if no rule is satisfied.
			 */

			if (!ObjectUtils.isEmpty(configuratorRuleOutput)) {
				supplierInfos =
						CollectionUtils.isNotEmpty(configuratorRuleOutput.getAllowedSuppliers())
								? HotelSupplierConfigurationHelper
										.getSupplierListFromSupplierIds(configuratorRuleOutput.getAllowedSuppliers())
								: new ArrayList<>();
			} else {
				List<HotelSupplierInfo> currSupplierInfos =
						HotelSupplierConfigurationHelper.getSupplierListFromSourceIds(searchQuery.getSourceIds());
				supplierInfos = CollectionUtils.isNotEmpty(currSupplierInfos) ? currSupplierInfos : new ArrayList<>();
			}
		} else {
			List<HotelSupplierInfo> currSupplierInfos =
					HotelSupplierConfigurationHelper.getSupplierListFromSourceIds(searchQuery.getSourceIds());
			supplierInfos = CollectionUtils.isNotEmpty(currSupplierInfos) ? currSupplierInfos : new ArrayList<>();
		}

		if (ObjectUtils.isEmpty(configuratorInfo)) {
			log.info("No rule is applied for search id {} ", searchQuery.getSearchId());
		} else {
			log.info("Rule applied for search id {} is {}", searchQuery.getSearchId(), configuratorInfo.getId());
		}
		return supplierInfos;
	}

	public static String getKey(String name) {
		if (StringUtils.isBlank(name))
			return null;
		return String.valueOf(name.toLowerCase().replaceAll("\\s", "").hashCode());
	}

	public static String getKey(HotelKeyInfo keyInfo) {
		return StringUtils.join(getKey(
				keyInfo.getHotelName() + (ObjectUtils.isEmpty(keyInfo.getHotelRating()) ? "" : keyInfo.getHotelRating())
						+ keyInfo.getCityName() + keyInfo.getCountryName()));
	}

	public static <T> List<Set<T>> partitionSet(Set<T> originalSet, int partitionSize) {
		List<Set<T>> partitionedSetList = new ArrayList<>();
		Iterator<T> iterator = originalSet.iterator();
		while (iterator.hasNext()) {
			Set<T> newSet = new HashSet<>();
			for (int j = 0; j < partitionSize && iterator.hasNext(); j++) {
				T s = iterator.next();
				newSet.add(s);
			}
			partitionedSetList.add(newSet);
		}
		return partitionedSetList;
	}

	public static void updateTotalFareComponents(HotelInfo hotelInfo) {
		hotelInfo.getOptions().forEach(option -> {
			option.getRoomInfos().forEach(roomInfo -> {
				BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
			});
		});
	}

	public static List<String> getSupplierIdsFromHotelInfo(HotelInfo hInfo) {

		Set<String> supplierIds = new HashSet<>();
		for (Option option : hInfo.getOptions()) {
			supplierIds.add(option.getMiscInfo().getSupplierId());
		}
		return new ArrayList<>(supplierIds);
	}

	public static HotelSourceConfigOutput getHotelSourceConfigOutput(HotelSearchQuery searchQuery) {

		HotelBasicFact hotelFact = HotelBasicFact.createFact().generateFactFromSearchQuery(searchQuery);
		List<HotelConfiguratorInfo> hotelconfigrules =
				HotelConfiguratorHelper.getHotelConfiguratorRules(HotelConfiguratorRuleType.SOURCECONFIG);

		if (!CollectionUtils.isEmpty(hotelconfigrules)) {
			return HotelConfiguratorHelper.getRuleOutPut(hotelFact, hotelconfigrules);
		}
		return null;
	}

	public static double getTotalMarkupOfOption(Option option) {
		double totalMarkup = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			totalMarkup += roomInfo.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF))) {
					Map<HotelFareComponent, Double> fareComponent =
							priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
					return fareComponent.getOrDefault(HotelFareComponent.MU, 0.0);
				}
				return 0.0;
			}).sum();
		}
		return totalMarkup;
	}

	public static boolean compareIfTwoOptionsEqual(List<RoomInfo> oldRoomInfo, List<RoomInfo> updatedRoomInfo) {
		double oldOptionPrice = 0.0;
		double updatedOptionPrice = 0.0;
		for (int i = 0; i < oldRoomInfo.size(); i++) {
			updatedOptionPrice += updatedRoomInfo.get(i).getPerNightPriceInfos().stream()
					.mapToDouble(priceInfo -> priceInfo.getFareComponents().get(HotelFareComponent.BF)).sum();
			oldOptionPrice += oldRoomInfo.get(i).getPerNightPriceInfos().stream()
					.mapToDouble(priceInfo -> priceInfo.getFareComponents().get(HotelFareComponent.BF)).sum();
		}
		if ((Math.abs(oldOptionPrice - updatedOptionPrice)) < 5) {
			return true;
		}
		return false;
	}

	public static boolean allOptionDetailHit(HotelInfo hInfo) {

		boolean allOptionDetailHit = true;
		for (Option option : hInfo.getOptions()) {
			if (!option.getMiscInfo().getIsDetailHit()) {
				allOptionDetailHit = false;
				break;
			}
		}
		return allOptionDetailHit;
	}

	public static String getLanguage() {
		return "en-US";
	}

	// origin of booking
	public static String getCountryCode() {
		return "IN";
	}

	public static BigDecimal getTotalSupplierBookingAmount(HotelInfo hInfo) {

		BigDecimal amount = BigDecimal.ZERO;
		List<RoomInfo> roomInfoList = hInfo.getOptions().get(0).getRoomInfos();
		for (RoomInfo roomInfo : roomInfoList) {
			BigDecimal roomAmount = BigDecimal.valueOf(roomInfo.getTotalFareComponents().get(HotelFareComponent.BF));
			amount = amount.add(roomAmount);
		}
		amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return amount;
	}


	public static String getKeyForUserReview(HotelInfo hInfo) {

		GeoLocation geoLocation = hInfo.getGeolocation();
		if (geoLocation != null) {
			String key = String.join("-", hInfo.getName(), geoLocation.getLatitude(), geoLocation.getLongitude());
			return key;
		} else
			return hInfo.getName();

	}


	public static DbHotelUserReviewIdInfo getDbHotelUserReviewInfo(HotelInfo hInfo) {

		DbHotelUserReviewIdInfo userReviewIdInfo = new DbHotelUserReviewIdInfo();
		String key = HotelUtils.getKeyForUserReview(hInfo);

		String hotelId = HotelUtils.getHotelId(hInfo.getId());
		String masterId =
				StringUtils.isNotBlank(hotelId) && hotelId.substring(0, 1).equals(HotelUtils.MASTER_HOTEL_ID_PREPEND)
						? hotelId.substring(1)
						: null;
		String city = hInfo.getAddress().getCity().getName();
		userReviewIdInfo.setKey(key);
		userReviewIdInfo.setReviewId(hInfo.getUserReviewSupplierId());
		userReviewIdInfo.setSupplierId("TRIP_ADVISOR");
		userReviewIdInfo.setHotelId(masterId);
		userReviewIdInfo.setRating(hInfo.getRating());
		userReviewIdInfo.setCity(city);
		return userReviewIdInfo;
	}

	public static HotelClientFeeOutput getClientFeeOutput(HotelBasicFact hotelFact) {
		HotelClientFeeOutput info =
				HotelConfiguratorHelper.getHotelConfigRuleOutput(hotelFact, HotelConfiguratorRuleType.CLIENTFEE);
		return info;
	}


	public static void filterCrossSellHotels(HotelSearchQuery searchQuery, HotelSearchResult searchResult) {

		if (ObjectUtils.isEmpty(searchQuery) || ObjectUtils.isEmpty(searchQuery.getSearchPreferences())
				|| ObjectUtils.isEmpty(searchQuery.getSearchPreferences().getCrossSellParameter()) || BooleanUtils
						.isNotTrue(searchQuery.getSearchPreferences().getCrossSellParameter().getCrossSellOnly())) {
			return;
		}
		List<HotelInfo> hotelInfos = searchResult.getHotelInfos().stream()
				.filter(hotel -> BooleanUtils.isTrue(hotel.getTags().contains(HotelTag.CROSS_SELL)))
				.collect(Collectors.toList());
		searchResult.setHotelInfos(hotelInfos);
	}

	public static void filterHotelsBasedOnLimit(HotelSearchQuery searchQuery, HotelSearchResult searchResult) {
		if (ObjectUtils.isEmpty(searchQuery) || ObjectUtils.isEmpty(searchQuery.getSearchPreferences())
				|| ObjectUtils.isEmpty(searchQuery.getSearchPreferences().getLimitHotels())) {
			return;
		}
		int limit = searchQuery.getSearchPreferences().getLimitHotels();
		if (!ObjectUtils.isEmpty(limit)) {
			int searchResultSize = searchResult.getHotelInfos().size();
			if (limit > searchResultSize) {
				limit = searchResultSize;
			}
			searchResult.setHotelInfos(searchResult.getHotelInfos().subList(0, limit));
		}
	}

	public static HotelSearchQuery getHotelSearchQuery(String bookingId) {
		ProductMetaInfo metaInfo = Product.getProductMetaInfoFromId(bookingId);
		if (metaInfo.getProduct() == Product.AIR) {
			AirOrderItemCommunicator airOrderCommunicator =
					SpringContext.getApplicationContext().getBean(AirOrderItemCommunicator.class);
			HotelSearchQuery searchQuery = airOrderCommunicator.getHotelSearchQuery(bookingId);
			return searchQuery;
		}
		return null;
	}

	public static void populateOldSearchQueryWithNewSearchQueryParam(HotelSearchQuery oldSearchQuery,
			HotelSearchQuery newSearchQuery) {
		Integer hotelLimits = null;
		Boolean crossSellOnly = null;
		if (!ObjectUtils.isEmpty(newSearchQuery) && !ObjectUtils.isEmpty(newSearchQuery.getSearchPreferences())) {
			hotelLimits = newSearchQuery.getSearchPreferences().getLimitHotels();
		}

		if (!ObjectUtils.isEmpty(newSearchQuery) && !ObjectUtils.isEmpty(newSearchQuery.getSearchPreferences())
				&& !ObjectUtils.isEmpty(newSearchQuery.getSearchPreferences().getCrossSellParameter())) {
			crossSellOnly = newSearchQuery.getSearchPreferences().getCrossSellParameter().getCrossSellOnly();
		}
		oldSearchQuery.getSearchPreferences().setLimitHotels(hotelLimits);
		oldSearchQuery.getSearchPreferences().getCrossSellParameter().setCrossSellOnly(crossSellOnly);
	}

	public static String removeCrossSellFromSearchId(String id) {
		return id.lastIndexOf('_') == -1 ? id : id.substring(0, id.lastIndexOf('_'));
	}

	public static boolean isCrossSell(String id) {

		return StringUtils.isBlank(id) ? false
				: id.lastIndexOf("_") == -1 ? false : id.substring(id.lastIndexOf("_") + 1).equals("CS");
	}

	public static String getCityNameFromAddress(Address adr) {

		if (!Objects.isNull(adr) && !Objects.isNull(adr.getCity()))
			return adr.getCity().getName();
		return null;

	}


	public static String getCountryNameFromAddress(Address adr) {

		if (!Objects.isNull(adr) && !Objects.isNull(adr.getCountry()))
			return adr.getCountry().getName();
		return null;

	}

	public static Map<Integer, HotelRoomTypeInfo> getRoomOccupancyRoomTypeInfoMap() {

		Map<Long, HotelRoomTypeInfo> roomTypeIdValueMap = HotelRoomTypeHelper.getRoomTypeIdRoomTypeInfoMap();
		List<HotelRoomTypeInfo> roomTypeList =
				roomTypeIdValueMap.entrySet().stream().map(entry -> entry.getValue()).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(roomTypeList))
			return new HashMap<>();
		Map<Integer, HotelRoomTypeInfo> roomOccupancyRoomTypeMap = roomTypeList.stream()
				.collect(Collectors.toMap(HotelRoomTypeInfo::getMaxOccupancy, Function.identity()));
		return roomOccupancyRoomTypeMap;
	}

	public static Map<String, List<HotelRatePlan>> getSupplierHotelIdRatePlanListValue(
			List<HotelRatePlan> ratePlanList) {
		if (CollectionUtils.isEmpty(ratePlanList))
			return new HashMap<>();
		return ratePlanList.stream().collect(Collectors.groupingBy(ratePlan -> getRatePlanCachingKey(ratePlan)));
	}

	public static String getRatePlanCachingKey(HotelRatePlan ratePlan) {
		return ratePlan.getSupplierHotelId() + String.valueOf(ratePlan.getValidOn()) + ratePlan.getSupplierId();
	}

	public static String toCamelCase(String s) {
		String[] parts = s.split(" ");
		String camelCaseString = "";
		for (String part : parts) {
			camelCaseString = camelCaseString + toProperCase(part) + " ";
		}
		return camelCaseString.trim();
	}

	private static String toProperCase(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	private static HotelKeyInfo getKeyMetaInfo(HotelInfo hInfo) {

		Address address = hInfo.getAddress();
		return HotelKeyInfo.builder().hotelName(hInfo.getName()).hotelRating(hInfo.getRating())
				.cityName(address.getCity().getName()).countryName(address.getCountry().getName()).build();
	}

	public static Map<String, Object> getHotelBasisInfo(HotelSearchResult searchResult, HotelSearchQuery searchQuery) {

		Map<String, Object> basicInfoToLog = new HashMap<>();
		basicInfoToLog.put("Number of hotels", searchResult.getHotelInfos().size());

		int numberOfOptions = getNumberOfOptionsInSearchResult(searchResult);
		basicInfoToLog.put("Number of options", numberOfOptions);

		int noOfRooms = searchQuery.getRoomInfo().size();
		basicInfoToLog.put("Rooms Searched", noOfRooms);

		long noOfNights = Duration
				.between(searchQuery.getCheckinDate().atStartOfDay(), searchQuery.getCheckoutDate().atStartOfDay())
				.toDays();
		basicInfoToLog.put("Number of nights", noOfNights);

		long totalRoomNights = numberOfOptions * noOfRooms * noOfNights;
		basicInfoToLog.put("Total Room Nights", totalRoomNights);
		return basicInfoToLog;
	}

	public static int getNumberOfOptionsInSearchResult(HotelSearchResult searchResult) {

		if (CollectionUtils.isNotEmpty(searchResult.getHotelInfos())) {
			return searchResult.getHotelInfos().stream().mapToInt(hotelInfo -> {
				return hotelInfo.getOptions().size();
			}).sum();
		}
		return 0;
	}

	public static List<String> getHotelSupplierIds(HotelSearchQuery searchQuery) {

		List<String> supplierIds = CollectionUtils.isEmpty(searchQuery.getMiscInfo().getSupplierIds())
				? Arrays.asList(searchQuery.getMiscInfo().getSupplierId())
				: searchQuery.getMiscInfo().getSupplierIds();
		return supplierIds;
	}
}
