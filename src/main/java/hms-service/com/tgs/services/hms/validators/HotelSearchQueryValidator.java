package com.tgs.services.hms.validators;

import java.time.LocalDate;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.restmodel.HotelDetailRequest;
import com.tgs.services.hms.restmodel.HotelSearchRequest;

@Component
public class HotelSearchQueryValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof HotelSearchRequest) {
			HotelSearchRequest searchRequest = (HotelSearchRequest) target;
			if (ObjectUtils.isEmpty(searchRequest.getSync())) {
				errors.rejectValue("sync", SystemError.EMPTY_SYNC_FLAG.errorCode(),
						SystemError.EMPTY_SYNC_FLAG.getMessage());
			}

			if (StringUtils.isNotBlank(searchRequest.getBookingId()))
				return;

			HotelSearchQuery searchQuery = searchRequest.getSearchQuery();
			validateQuery(searchQuery, errors);
		} else if (target instanceof HotelDetailRequest) {
			HotelDetailRequest detailRequest = (HotelDetailRequest) target;
			HotelSearchQuery detailQuery = detailRequest.getSearchQuery();
			validateQuery(detailQuery, errors);
		}
	}

	private void validateQuery(HotelSearchQuery searchQuery, Errors errors) {
		registerErrors(errors, "searchQuery", searchQuery);

		if (!ObjectUtils.isEmpty(searchQuery)) {
			LocalDate previousDate = LocalDate.now();
			LocalDate yearDate = LocalDate.now().plusYears(1);

			if (Objects.isNull(searchQuery.getCheckinDate()) || !(searchQuery.getCheckinDate().isAfter(previousDate)
					|| searchQuery.getCheckinDate().isEqual(previousDate))) {
				errors.rejectValue("searchQuery.checkinDate", SystemError.HOTEL_CHECKINDATE_VALIDATION.errorCode(),
						SystemError.HOTEL_CHECKINDATE_VALIDATION.getMessage());
			}

			if (Objects.isNull(searchQuery.getCheckoutDate()) || !(searchQuery.getCheckoutDate().isAfter(previousDate)
					|| searchQuery.getCheckoutDate().isEqual(previousDate))) {
				errors.rejectValue("searchQuery.checkoutDate", SystemError.HOTEL_CHECKOUTDATE_VALIDATION.errorCode(),
						SystemError.HOTEL_CHECKOUTDATE_VALIDATION.getMessage());
			}

			if (searchQuery.getCheckinDate() != null
					&& !(searchQuery.getCheckoutDate().isAfter(searchQuery.getCheckinDate()))) {
				errors.rejectValue("searchQuery.checkoutDate", SystemError.INVALID_CHECKIN_CHECKOUT_DATE.errorCode(),
						SystemError.INVALID_CHECKIN_CHECKOUT_DATE.getMessage());
			}

			if (searchQuery.getCheckinDate() != null && searchQuery.getCheckinDate().isAfter(yearDate)) {
				errors.rejectValue("searchQuery.checkinDate", SystemError.TRAVEL_DATE_YEAR_VALIDATION.errorCode(),
						SystemError.TRAVEL_DATE_YEAR_VALIDATION.getMessage());
			}

			if (searchQuery.getCheckoutDate() != null && searchQuery.getCheckoutDate().isAfter(yearDate)) {
				errors.rejectValue("searchQuery.checkoutDate", SystemError.TRAVEL_DATE_YEAR_VALIDATION.errorCode(),
						SystemError.TRAVEL_DATE_YEAR_VALIDATION.getMessage());
			}
		}
	}
}
