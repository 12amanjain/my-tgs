package com.tgs.services.ims.communicator.impl;

import java.lang.reflect.Type;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import com.aerospike.client.query.Filter;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.datamodel.air.AirInventoryInfo;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ims.datamodel.air.AirInventoryTripTiming;
import com.tgs.services.ims.datamodel.air.AirRatePlanInfo;
import com.tgs.services.ims.datamodel.air.AirSeatAllocationAdditionalInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.dbmodel.DbInventoryOrder;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.helper.AirInventoryHelper;
import com.tgs.services.ims.helper.RatePlanHelper;
import com.tgs.services.ims.helper.SeatAllocationHelper;
import com.tgs.services.ims.jparepository.InventoryOrderService;
import com.tgs.services.ims.jparepository.RatePlanService;
import com.tgs.services.ims.jparepository.air.AirInventoryService;
import com.tgs.services.ims.jparepository.air.SeatAllocationService;
import com.tgs.services.ims.restmodel.air.AirSeatInventoryFilter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class InventoryServiceCommunicatorImpl implements DealInventoryCommunicator {

	@Autowired
	private SeatAllocationService allocationService;

	@Autowired
	private RatePlanService ratePlanService;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private InventoryOrderService invOrderService;

	@Autowired
	private AirInventoryService airInventoryService;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Override
	public List<TripInfo> findTripInfos(AirSeatInventoryFilter filter) {
		List<TripInfo> tripInfos = new ArrayList<>();
		Map<String, AirInventory> airInventoryMap = new HashMap<>();
		Map<String, AirRatePlan> ratePlanMap = new HashMap<>();
		Map<String, AirSeatAllocationAdditionalInfo> seatAllocationMap =
				SeatAllocationHelper.searchSeatAllocation(filter);
		log.debug("Seat allocations size {}", seatAllocationMap.size());
		List<String> ratePlanIds = new ArrayList<>();
		List<String> inventoryIds = new ArrayList<>();
		for (Entry<String, AirSeatAllocationAdditionalInfo> entrySet : seatAllocationMap.entrySet()) {
			ratePlanIds.add(entrySet.getValue().getRatePlanId());
			inventoryIds.add(entrySet.getValue().getInventoryId());
		}
		log.debug("RatePlan Ids found in cache {}", ratePlanIds.toString());
		log.debug("Inventory Ids found in cache {}", inventoryIds.toString());
		if (CollectionUtils.isNotEmpty(ratePlanIds) && CollectionUtils.isNotEmpty(inventoryIds)) {
			ratePlanMap = RatePlanHelper.searchRatePlan(ratePlanIds);
			airInventoryMap = AirInventoryHelper.searchAirInventory(inventoryIds);
		}
		
		Map<String, TripInfo> tripInfoMap = new ConcurrentHashMap<>();
		for (Entry<String, AirSeatAllocationAdditionalInfo> entrySet : seatAllocationMap.entrySet()) {
			AirSeatAllocationAdditionalInfo allocation = entrySet.getValue();
			AirInventory inventory = airInventoryMap.get(allocation.getInventoryId());
			AirRatePlan ratePlan = ratePlanMap.get(allocation.getRatePlanId());

			if (isValidFlight(inventory, ratePlan, filter.getAirlines())
					&& allocation.getTotalSeats() >= filter.getTotalSeats()
					&& ChronoUnit.HOURS.between(LocalDateTime.now(),
							LocalDateTime.of(allocation.getValidOn(),
									LocalTime.parse(inventory.getSegmentInfos().get(0).getDeparture())))
							- inventory.getDisableBfrDept() >= 0) {

				String searchKey = StringUtils.join(inventory.getFromAirport(), inventory.getToAirport(), "_",
						inventory.getCarrier(), "_", filter.getTravelDate());

				AirInventoryTripTiming tripTiming = getFlightsTiming(searchKey);

				List<AirRatePlanInfo> ratePlanInfo = ratePlan.getRatePlanInfo();
				SegmentInfo prevSegmentInfo = null;
				for (SegmentInfo segment : inventory.getSegmentInfos()) {
					constructMissingSegmentInfo(allocation, segment, prevSegmentInfo,tripTiming);
					prevSegmentInfo = segment;
				}
				TripInfo tripInfo = BookingUtils.createTripListFromSegmentList(inventory.getSegmentInfos()).get(0);
				boolean hasPnr = StringUtils.isNotEmpty(allocation.getAirlinePnr());
				String key = tripInfo.getTripKey() + hasPnr;
				if (tripInfoMap.get(key) != null) {
					tripInfo = tripInfoMap.get(key);
					applyLowestPriceOption(tripInfo, filter, inventory, ratePlanInfo, airInventoryMap,
							allocation.getTotalSeats(), hasPnr);
				} else {
					Map<PaxType, FareDetail> fareDetails = new HashMap<>();
					setPaxFare(filter.getPaxInfo(), fareDetails, ratePlanInfo, allocation.getTotalSeats());
					setFareDetail(tripInfo, fareDetails, getSupplierBasicInfo(inventory.getSupplierId()),
							String.valueOf(inventory.getId()), hasPnr);
					tripInfos.add(tripInfo);
					tripInfoMap.put(key, tripInfo);
				}
				log.debug("TripInfoMap constructed with size {}", tripInfoMap.size());
			}
		}
		log.debug("TripInfo size {}", tripInfos.size());
		return tripInfos;
	}

	public SupplierBasicInfo getSupplierBasicInfo(String supplierId) {
		log.debug("In getSupplierBasicInfo");
		List<SupplierInfo> sInfos = fmsCommunicator.getSupplierForSourceId("11");
		SupplierInfo sInfo = sInfos.stream().filter(supp -> supp.getCredentialInfo().getUserName().equals(supplierId))
				.findFirst().get();
		SupplierBasicInfo suppInfo = new SupplierBasicInfo();
		suppInfo.setSupplierName(sInfo.getName());
		suppInfo.setSupplierId(sInfo.getSupplierId());
		suppInfo.setSourceId(11);
		log.debug("SupplierBasicInfo constructed");
		return suppInfo;
	}

	public void applyLowestPriceOption(TripInfo tripInfo, AirSeatInventoryFilter filter, AirInventory inventory,
			List<AirRatePlanInfo> ratePlanInfo, Map<String, AirInventory> airInventoryMap, Integer seatsRemaining,
			boolean hasPnr) {
		Map<PaxType, FareDetail> newFareDetails = new HashMap<>();
		FareDetail oldFareDetail = tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getFareDetail(PaxType.ADULT);
		setPaxFare(filter.getPaxInfo(), newFareDetails, ratePlanInfo, seatsRemaining);

		if (oldFareDetail.getFareComponents().get(FareComponent.TF) > newFareDetails.get(PaxType.ADULT)
				.getFareComponents().get(FareComponent.TF)) {
			setFareDetail(tripInfo, newFareDetails, getSupplierBasicInfo(inventory.getSupplierId()),
					String.valueOf(inventory.getId()), hasPnr);
		} else if (oldFareDetail.getFareComponents().get(FareComponent.TF)
				.equals(newFareDetails.get(PaxType.ADULT).getFareComponents().get(FareComponent.TF))) {
			AirInventory appliedInventory = airInventoryMap
					.get(tripInfo.getSegmentInfos().get(0).getPriceInfo(0).getMiscInfo().getInventoryId());
			if (appliedInventory.getCreatedOn().isAfter(inventory.getCreatedOn())) {
				setFareDetail(tripInfo, newFareDetails, getSupplierBasicInfo(inventory.getSupplierId()),
						String.valueOf(inventory.getId()), hasPnr);
			}
		}
	}

	public void setFareDetail(TripInfo tripInfo, Map<PaxType, FareDetail> fareDetails, SupplierBasicInfo suppInfo,
			String inventoryId, boolean hasPnr) {
		for (int i = 0; i < tripInfo.getSegmentInfos().size(); i++) {
			SegmentInfo segment = tripInfo.getSegmentInfos().get(i);
			List<PriceInfo> priceInfoList = new ArrayList<>();
			PriceInfo info = PriceInfo.builder().build();
			info.setSupplierBasicInfo(suppInfo);
			info.setFareIdentifier(hasPnr ? FareType.OFFER_FARE_WITH_PNR : FareType.OFFER_FARE_WITHOUT_PNR);
			info.setMiscInfo(PriceMiscInfo.builder().inventoryId(String.valueOf(inventoryId)).build());
			if (i == 0)
				info.setFareDetails(fareDetails);
			// for connecting flights, send dummy faredetail
			else {
				Map<PaxType, FareDetail> fareDetail = new HashMap<>();
				Set<PaxType> keys = fareDetails.keySet();
				keys.forEach(key -> {
					FareDetail oldFd = fareDetails.get(key);
					FareDetail fd = new FareDetail();
					Map<FareComponent, Double> fareComponents = new HashMap<>();
					fareComponents.put(FareComponent.BF, 0.0);
					fareComponents.put(FareComponent.AT, 0.0);
					fareComponents.put(FareComponent.YQ, 0.0);
					fareComponents.put(FareComponent.TF, 0.0);
					fd.setFareComponents(fareComponents);
					fd.setCabinClass(oldFd.getCabinClass());
					fd.setClassOfBooking(oldFd.getClassOfBooking());
					fd.setSeatRemaining(oldFd.getSeatRemaining());
					fareDetail.put(key, fd);
				});
				info.setFareDetails(fareDetail);
			}
			priceInfoList.add(info);
			segment.setPriceInfoList(priceInfoList);

		}
	}

	private void setPaxFare(Map<PaxType, Integer> paxInfo, Map<PaxType, FareDetail> fareDetails,
			List<AirRatePlanInfo> ratePlanInfo, Integer seatsRemaining) {
		for (Entry<PaxType, Integer> entrySet : paxInfo.entrySet()) {
			PaxType paxType = entrySet.getKey();
			if (entrySet.getValue() != null && entrySet.getValue() > 0) {
				for (AirRatePlanInfo rp : ratePlanInfo) {
					if (rp.getPaxType() == paxType) {
						fareDetails.put(paxType, constructFareComponent(rp, seatsRemaining));
					}
				}
			}
		}
	}

	private boolean isValidFlight(AirInventory inventory, AirRatePlan ratePlan, Set<String> prefferedAirlines) {
		boolean isValidFlight = false;
		if (inventory != null && ratePlan != null) {
			isValidFlight = inventory.getIsEnabled() && !inventory.getDeleted() && !ratePlan.getIsDeleted();
			if (CollectionUtils.isNotEmpty(prefferedAirlines))
				isValidFlight = isValidFlight && prefferedAirlines.contains(inventory.getCarrier());
		}
		return isValidFlight;
	}

	private FareDetail constructFareComponent(AirRatePlanInfo rp, Integer seatsRemaining) {
		FareDetail fareDetail = new FareDetail();
		fareDetail.setSeatRemaining(seatsRemaining);
		fareDetail.getFareComponents().put(FareComponent.BF, rp.getBaseFare());
		if (rp.getGst() != 0) {
			fareDetail.getFareComponents().put(FareComponent.AGST, rp.getGst());
		}
		if (rp.getTaxRate() != 0) {
			fareDetail.getFareComponents().put(FareComponent.AT, rp.getTaxRate());
		}
		double totalFare = rp.getBaseFare() + rp.getTaxRate() + rp.getGst();
		fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		fareDetail.setCabinClass(CabinClass.getCabinClass(rp.getCabinClass()).get());
		fareDetail.setClassOfBooking(rp.getBookingClass());
		return fareDetail;
	}

	private void constructMissingSegmentInfo(AirSeatAllocationAdditionalInfo allocation, SegmentInfo segment,
			SegmentInfo prevSegmentInfo,AirInventoryTripTiming tripTiming) {
		AirlineInfo airlineInfo =
				fmsCommunicator.getAirlineInfo(segment.getFlightDesignator().getAirlineInfo().getCode());
		// For the inventory flights ticketing is no required.
		airlineInfo.setIsTkRequired(false);
		String departTerminal = segment.getDepartAirportInfo().getTerminal();
		String arrivalTerminal = segment.getArrivalAirportInfo().getTerminal();
		segment.getFlightDesignator().setAirlineInfo(airlineInfo);
		segment.setDepartTime(processDepartureTime(allocation, segment, prevSegmentInfo));
		segment.setArrivalTime(processArrivalTime(allocation, segment));
		segment.setDepartAirportInfo(fmsCommunicator.getAirportInfo(segment.getDepartAirportInfo().getCode()));
		segment.getDepartAirportInfo().setTerminal(departTerminal);
		segment.setArrivalAirportInfo(fmsCommunicator.getAirportInfo(segment.getArrivalAirportInfo().getCode()));
		segment.getArrivalAirportInfo().setTerminal(arrivalTerminal);
		segment.setDuration(Duration.between(segment.getDepartTime(), segment.getArrivalTime()).toMinutes());
		String segmentKey = StringUtils.join(segment.getAirlineCode(false), segment.getFlightNumber());

		if (tripTiming != null && MapUtils.isNotEmpty(tripTiming.getTripTimings())
				&& tripTiming.getTripTimings().get(segmentKey) != null) {
			SeatAllocationHelper.updateTimingsFromCache(segment, tripTiming.getTripTimings().get(segmentKey));
			segment.setDuration(Duration.between(segment.getDepartTime(), segment.getArrivalTime()).toMinutes());
		}
	}
	
	// case to handle next day departure in case of connecting flights.
	private LocalDateTime processDepartureTime(AirSeatAllocationAdditionalInfo allocation, SegmentInfo segment,
			SegmentInfo prevSegmentInfo) {
		LocalDateTime departureTime =
				LocalDateTime.of(allocation.getValidOn(), LocalTime.parse(segment.getDeparture()));
		if (prevSegmentInfo != null && BooleanUtils.isTrue(prevSegmentInfo.isArrivalNextDay())) {
			departureTime = departureTime.plusDays(1);
		}
		return departureTime;
	}

	// case to handle next day arrival.
	private LocalDateTime processArrivalTime(AirSeatAllocationAdditionalInfo allocation, SegmentInfo segment) {
		LocalDateTime arrivalTime =
				LocalDateTime.of(segment.getDepartTime().toLocalDate(), LocalTime.parse(segment.getArrival()));
		if (arrivalTime.isBefore(segment.getDepartTime()) || BooleanUtils.isTrue(segment.isArrivalNextDay())) {
			arrivalTime = arrivalTime.plusDays(1);
		}
		return arrivalTime;
	}

	@Override
	public void revertSeatsSold(String inventoryId, LocalDate validOn, Integer seatsSold) {
		DbSeatAllocation seatAllocation = allocationService.findByInventoryIdAndValidOn(inventoryId, validOn);
		updateSeatsInSeatAllocation(seatAllocation.getTotalSeats() + seatsSold,
				seatAllocation.getSeatsSold() - seatsSold, seatAllocation);
	}

	@Override
	public String fetchPnrAndUpdateSeatsIfAvailable(String inventoryId, LocalDate validOn, int paxCount) {
		DbSeatAllocation seatAllocation = allocationService.findByInventoryIdAndValidOn(inventoryId, validOn);
		if (seatAllocation.getTotalSeats() >= paxCount) {
			updateSeatsInSeatAllocation(seatAllocation.getTotalSeats() - paxCount,
					seatAllocation.getSeatsSold() + paxCount, seatAllocation);
			return seatAllocation.getAdditionalInfo().getAirlinePnr();
		}
		return "";
	}

	private void updateSeatsInSeatAllocation(int totalSeats, int seatsSold, DbSeatAllocation seatAllocation) {
		seatAllocation.setTotalSeats(totalSeats);
		seatAllocation.setSeatsSold(seatsSold);
		allocationService.save(seatAllocation);
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		AirSeatAllocationAdditionalInfo seatAllocationInfo =
				gson.fromJson(gson.toJson(seatAllocation), AirSeatAllocationAdditionalInfo.class);
		seatAllocationInfo.setAirlinePnr(seatAllocation.getAdditionalInfo().getAirlinePnr());
		SeatAllocationHelper.updateSeatAllocation(seatAllocationInfo);
	}

	@Override
	public Map<PaxType, Double> fetchFareBeforeBooking(String inventoryId, LocalDate validOn) {
		DbRatePlan ratePlan = ratePlanService.fetchRatePlanForInventory(inventoryId, validOn);
		return ratePlan.getRatePlanInfo().stream()
				.collect(Collectors.toMap(x -> x.getPaxType(), x -> x.getBaseFare() + x.getTaxRate() + x.getGst()));
	}

	@Override
	public void saveInventoryOrder(InventoryOrder invOrder) {
		DbInventoryOrder dbInvOrder =
				invOrder.getId() != null ? invOrderService.findById(invOrder.getId()) : new DbInventoryOrder();
		dbInvOrder = dbInvOrder.from(invOrder);
		invOrderService.save(dbInvOrder);
	}

	@Override
	public String getInventoryName(String inventoryId) {
		Map<String, AirInventory> inventoryDetails = AirInventoryHelper.searchAirInventory(Arrays.asList(inventoryId));
		AirInventory inventory = inventoryDetails.get(inventoryId);
		return Optional.ofNullable(inventory.getName()).orElseGet(() -> "");
	}

	@Override
	public List<InventoryOrder> fetchInventoryOrders(InventoryOrderFilter filter) {
		List<DbInventoryOrder> dbInventories = invOrderService.findAll(filter);
		if (CollectionUtils.isNotEmpty(dbInventories))
			return BaseModel.toDomainList(dbInventories);
		return Collections.emptyList();
	}

	@Override
	public AirInventory getAirInventory(Long id) {
		DbAirInventory dbAirInventory = airInventoryService.findById(id);
		if (dbAirInventory != null) {
			return dbAirInventory.toDomain();
		}
		return null;
	}

	public SeatAllocation getSeatAllocationFromDb(String inventoryId, LocalDate validOn) {
		return allocationService.findByInventoryIdAndValidOn(inventoryId, validOn).toDomain();
	}

	public Integer getTotalSeat(Long id) {
		return allocationService.findById(id).getTotalSeats();
	}

	public Map<PaxType, FareDetail> getRatePlanFareDetails(String inventoryId, LocalDate validOn,
			Integer seatRemaining) {
		DbRatePlan ratePlan = ratePlanService.fetchRatePlanForInventory(inventoryId, validOn);
		Map<PaxType, FareDetail> fareMap = new HashMap<>();
		for (AirRatePlanInfo airRatePlanInfo : ratePlan.getRatePlanInfo()) {
			FareDetail fd = constructFareComponent(airRatePlanInfo, seatRemaining);
			fareMap.put(airRatePlanInfo.getPaxType(), fd);
		}
		return fareMap;
	}

	@Override
	public void storeFlightTimings(String tripkey, Map<String, AirInventoryInfo> segmentWiseTimings) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.SEAT_ALLOCATION.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).key(tripkey).build();
		Map<String, Map<String, String>> seatAllocationMap = cachingCommunicator.getResultSet(metaInfo, String.class,
				Filter.equal(BinName.INVENTORYKEY.getName(), tripkey));
		if (MapUtils.isNotEmpty(seatAllocationMap)) {
			CacheMetaInfo inventoryMetaInfo = CacheMetaInfo.builder().set(CacheSetName.INVENTORY_TIMING.getName())
					.namespace(CacheNameSpace.FLIGHT.getName()).key(tripkey).build();
			Map<String, String> binMap = new HashMap<>();
			AirInventoryTripTiming tripTiming = AirInventoryTripTiming.builder().tripTimings(segmentWiseTimings).build();
			binMap.put(BinName.INVENTORYTIME.getName(), GsonUtils.getGson().toJson(tripTiming));
			cachingCommunicator.store(inventoryMetaInfo, binMap, false, true, 259200);
		}
	}

	@Override
	public AirInventoryTripTiming getFlightsTiming(String tripKey) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.FLIGHT.getName())
				.set(CacheSetName.INVENTORY_TIMING.getName()).keys(Arrays.asList(tripKey).toArray(new String[0]))
				.build();
		Map<String, Map<String, String>> resultSet = cachingCommunicator.get(metaInfo, String.class);
		Type typeOfT = new TypeToken<AirInventoryTripTiming>() {}.getType();
		return resultSet.get(tripKey) != null
				? GsonUtils.getGson().fromJson(resultSet.get(tripKey).get(BinName.INVENTORYTIME.getName()), typeOfT)
				: null;
	}


}
