package com.tgs.services.ims.dbmodel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.database.CustomTypes.FlightTravellerInfoType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import com.tgs.services.ims.hibernate.air.AirInventoryOrderAdditionalInfoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "inventoryorders")
@Table(name = "inventoryorders")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({@TypeDef(name = "FlightTravellerInfoType", typeClass = FlightTravellerInfoType.class),
		@TypeDef(name = "InvOrderAdditionalInfoType", typeClass = AirInventoryOrderAdditionalInfoType.class)})
public class DbInventoryOrder extends BaseModel<DbInventoryOrder, InventoryOrder> {

	@Column
	private String inventoryId;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private String referenceId;

	@Column
	private LocalDate validOn;

	@Column
	@Type(type = "FlightTravellerInfoType")
	private List<FlightTravellerInfo> travellerInfo;

	@Column
	private String supplierId;

	@Type(type = "InvOrderAdditionalInfoType")
	@Column
	private AirInventoryOrderInfo additionalInfo;

	@Column
	private String product;

	@Column
	private String bookingUserId;

	@Override
	public InventoryOrder toDomain() {
		return new GsonMapper<>(this, InventoryOrder.class, true).convert();
	}

	@Override
	public DbInventoryOrder from(InventoryOrder dataModel) {
		return new GsonMapper<>(dataModel, this, DbInventoryOrder.class).convert();
	}

}
