package com.tgs.services.ims.dbmodel.air;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.hibernate.air.SegmentInfoType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "airinventory")
@Table(name = "airinventory")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
	@TypeDef(name = "SegmentInfoType", typeClass = SegmentInfoType.class),
})
public class DbAirInventory extends BaseModel<DbAirInventory, AirInventory>{
	
	@Column
	private boolean isEnabled;

	@Column
	private String fromAirport;
	
	@Column
	private String toAirport;
	
	@Column
	private String tripType;

	@Column
	private String name;

	@Column
	private boolean isReturn;
	
	@Column
	private boolean deleted;

	@Column
	private String supplierId;
	
	@Column
	private int disableBfrDept;
	
	@Column
	@Type(type = "SegmentInfoType")
	private List<SegmentInfo> segmentInfos;
	
	@Column
	private String carrier;
	
	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	@Override
    public AirInventory toDomain() {
        return new GsonMapper<>(this, AirInventory.class).convert();
    }

    @Override
    public DbAirInventory from(AirInventory dataModel) {
        return new GsonMapper<>(dataModel, this, DbAirInventory.class).convert();
    }

}
