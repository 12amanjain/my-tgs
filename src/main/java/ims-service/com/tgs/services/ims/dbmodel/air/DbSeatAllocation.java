package com.tgs.services.ims.dbmodel.air;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.common.collect.Lists;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.datamodel.air.AirSeatAllocationAdditionalInfo;
import com.tgs.services.ims.hibernate.air.SeatAllocationAdditionalInfoType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "seatallocation")
@Table(name = "seatallocation")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
	@TypeDef(name = "SeatAllocationAdditionalInfo", typeClass = SeatAllocationAdditionalInfoType.class),
})
public class DbSeatAllocation extends BaseModel<DbSeatAllocation, SeatAllocation>{

	@Column
	private String inventoryId;
	
	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private LocalDate validOn;

	@Column
	@SerializedName("rid")
	private String ratePlanId;
	
	@Column
	@SerializedName("rpn")
	private String ratePlanName;
	
	@Column
	@Type(type = "SeatAllocationAdditionalInfo")
	private DbSeatAllocationAdditionalInfo additionalInfo;

	@Column
	private String supplierId;
	
	@Column
	@SerializedName("ts")
	private int totalSeats;
	
	@Column
	@SerializedName("ss")
	private int seatsSold;
	
	@Column
	@SerializedName("fid")
	private String fareRuleId;
	
	@Column
	private boolean isDeleted;
	
	@Override
	public SeatAllocation toDomain() {
		AirSeatAllocationAdditionalInfo seatAllocationInfo =new GsonMapper<>(this, AirSeatAllocationAdditionalInfo.class).convert();
		if (additionalInfo != null) {
			seatAllocationInfo.setAirlinePnr(additionalInfo.getAirlinePnr());
		}
  		seatAllocationInfo.setStartDate(getValidOn());
		seatAllocationInfo.setEndDate(getValidOn());
		return SeatAllocation.builder().id(id).inventoryId(inventoryId).product(Product.AIR).allocationInfo(Lists.newArrayList(seatAllocationInfo)).build();
	}

}
