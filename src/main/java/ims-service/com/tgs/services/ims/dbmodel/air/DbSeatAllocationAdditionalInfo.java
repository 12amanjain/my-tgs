package com.tgs.services.ims.dbmodel.air;

import com.google.gson.annotations.SerializedName;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DbSeatAllocationAdditionalInfo {

	@SerializedName("aPnr")
	private String airlinePnr;
}
