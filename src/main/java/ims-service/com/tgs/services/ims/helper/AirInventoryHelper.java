package com.tgs.services.ims.helper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.jparepository.air.AirInventoryService;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;

@Service
public class AirInventoryHelper extends InMemoryInitializer {

	@Autowired
	AirInventoryService airInventoryService;

	@Autowired
	public static GeneralCachingCommunicator cachingCommunicator;

	public AirInventoryHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		AirInventoryHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		Runnable task = () -> {
			AirInventoryFilter airInventoryFilter = AirInventoryFilter.builder().isEnabled(true).build();
			airInventoryService.findAll(airInventoryFilter).forEach(dbAirInventory -> {
				UpdateAirInventory(dbAirInventory.toDomain());
			});
		};

		Thread dataThread = new Thread(task);
		dataThread.start();
	}

	// for async bulk write
	private void store(List<DbAirInventory> dbAirInventoryList) {
		Map<String, Map<String, String>> keyValueMap = new HashMap<>();
		dbAirInventoryList.forEach(airInventory -> {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.AIRINVENTORY.getName(), GsonUtils.getGson().toJson(airInventory.toDomain()));
			keyValueMap.put(airInventory.getId().toString(), binMap);
		});
		cachingCommunicator.asyncStore(CacheMetaInfo.builder().set(CacheSetName.AIR_INVENTORY.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).compress(false).plainData(true).expiration(NEVER_EXPIRE)
				.build(), keyValueMap);
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.AIR_INVENTORY.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).build());
	}

	public static void UpdateAirInventory(AirInventory airInventory) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.AIRINVENTORY.getName(), GsonUtils.getGson().toJson(airInventory));
		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.AIR_INVENTORY.getName())
						.namespace(CacheNameSpace.FLIGHT.getName()).key(airInventory.getId().toString()).build(),
				binMap, false, true, NEVER_EXPIRE);
	}

	public static Map<String, AirInventory> searchAirInventory(List<String> airInventoryIds) {
		Map<String, AirInventory> airInventory = new HashMap<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.AIR_INVENTORY.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).keys(airInventoryIds.toArray(new String[0])).build();
		Map<String, Map<String, String>> airInventoryMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(airInventoryMap)) {
			for (Entry<String, Map<String, String>> entrySet : airInventoryMap.entrySet()) {
				airInventory.put(entrySet.getKey(), GsonUtils.getGson()
						.fromJson(entrySet.getValue().get(BinName.AIRINVENTORY.getName()), AirInventory.class));
			}
		}
		return airInventory;
	}

}
