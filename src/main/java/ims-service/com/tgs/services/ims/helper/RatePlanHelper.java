package com.tgs.services.ims.helper;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Map.Entry;

import org.springframework.stereotype.Service;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.ims.datamodel.RatePlanFilter;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.ims.jparepository.RatePlanService;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;

@Service
public class RatePlanHelper extends InMemoryInitializer {

	@Autowired
	RatePlanService ratePlanService;

	@Autowired
	public static GeneralCachingCommunicator cachingCommunicator;

	public RatePlanHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		RatePlanHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		RatePlanFilter ratePlanFilter = RatePlanFilter.builder().build();
		ratePlanService.findAll(ratePlanFilter).forEach(dbRatePlan -> {
			updateRatePlan(dbRatePlan.toDomain());
		});
	}

	@Override
	public void deleteExistingInitializer() {

	}

	// for async bulk write
	private void store(List<DbRatePlan> dbRatePlanList) {
		Map<String, Map<String, String>> keyValueMap = new HashMap<>();
		dbRatePlanList.forEach(ratePlan -> {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.RATEPLAN.getName(), GsonUtils.getGson().toJson(ratePlan.toDomain()));
			keyValueMap.put(ratePlan.getId().toString(), binMap);
		});
		cachingCommunicator.asyncStore(
				CacheMetaInfo.builder().set(CacheSetName.RATE_PLAN.getName()).namespace(CacheNameSpace.FLIGHT.getName())
						.compress(false).plainData(true).expiration(NEVER_EXPIRE).build(),
				keyValueMap);
	}

	public static void updateRatePlan(AirRatePlan airRatePlan) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.RATEPLAN.getName(), GsonUtils.getGson().toJson(airRatePlan));
		cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.RATE_PLAN.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).key(airRatePlan.getId().toString()).build(), binMap, false,
				true, NEVER_EXPIRE);
	}

	public static Map<String, AirRatePlan> searchRatePlan(List<String> ratePlanIds) {
		Map<String, AirRatePlan> ratePlan = new HashMap<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().set(CacheSetName.RATE_PLAN.getName())
				.namespace(CacheNameSpace.FLIGHT.getName()).keys(ratePlanIds.toArray(new String[0])).build();
		Map<String, Map<String, String>> ratePlanMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(ratePlanMap)) {
			for (Entry<String, Map<String, String>> entrySet : ratePlanMap.entrySet()) {
				ratePlan.put(entrySet.getKey(), GsonUtils.getGson()
						.fromJson(entrySet.getValue().get(BinName.RATEPLAN.getName()), AirRatePlan.class));
			}
		}
		return ratePlan;
	}

}
