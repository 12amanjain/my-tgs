package com.tgs.services.ims.hibernate.air;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocationAdditionalInfo;

public class SeatAllocationAdditionalInfoType extends CustomUserType{

	@Override
	public Class returnedClass() {
		return DbSeatAllocationAdditionalInfo.class;
	}

}
