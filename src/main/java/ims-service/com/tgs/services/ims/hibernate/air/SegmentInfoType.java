package com.tgs.services.ims.hibernate.air;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.fms.datamodel.SegmentInfo;

public class SegmentInfoType extends CustomUserType{
	
	@Override
	public Class returnedClass() {
		List<SegmentInfo> segmentInfo = new ArrayList<>();
		return segmentInfo.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<SegmentInfo>>() {
		}.getType();
	}

}
