package com.tgs.services.ims.jparepository;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.ims.dbmodel.DbInventoryOrder;

@Service
public class InventoryOrderService extends SearchService<DbInventoryOrder>{

	@Autowired
	InventoryOrderRepository repo;
	
	public DbInventoryOrder save(DbInventoryOrder order) {
		order = repo.save(order);
		return order;
	}
	
	public DbInventoryOrder findById(Long id) {
		return repo.findOne(id);
	}
	
	public List<DbInventoryOrder> findAll(InventoryOrderFilter filter){
		SortByAttributes sortByAttr = new SortByAttributes();
		sortByAttr.setOrderBy("asc");
		sortByAttr.setParams(Arrays.asList("inventoryId", "validOn"));
		PageAttributes pageAttr = new PageAttributes();
		pageAttr.setSortByAttr(Arrays.asList(sortByAttr));
		pageAttr.setPageNumber(0);
		pageAttr.setSize(5000);
		filter.setPageAttr(pageAttr);
		List<DbInventoryOrder> orders =super.search(filter, repo);
		return orders;
	}
}
