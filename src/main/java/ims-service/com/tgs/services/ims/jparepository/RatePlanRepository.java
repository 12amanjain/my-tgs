package com.tgs.services.ims.jparepository;

import java.time.LocalDate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.ims.dbmodel.DbRatePlan;

@Repository
public interface RatePlanRepository extends JpaRepository<DbRatePlan, Long>, JpaSpecificationExecutor<DbRatePlan> {
	
	@Query(value = "select r.* from rateplan r join seatallocation s ON r.id = cast(s.rateplanid AS int) where s.inventoryid = :inventoryid"  +
            " and s.validOn = :validOn", nativeQuery = true)
    DbRatePlan fetchFareForInventory(@Param("inventoryid")String inventoryid, @Param("validOn")LocalDate validOn);
	
	
}
