package com.tgs.services.ims.jparepository;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.tgs.services.ims.datamodel.RatePlanFilter;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import com.tgs.utils.springframework.data.SpringDataUtils;


public enum RatePlanSearchPredicate {

	ID{
		@Override
		public void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				RatePlanFilter filter, List<Predicate> predicates) {
			if (filter.getId() != null) {
				predicates.add(criteriaBuilder.equal(root.get("id"), filter.getId()));
			}
		}
		
	},
	NAME {
		@Override
		public void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				RatePlanFilter filter, List<Predicate> predicates) {
			if (filter.getName() != null)
				predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
						"%" + filter.getName().toLowerCase() + "%"));
		}
	},
	ISDELETED {
		@Override
		public void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				RatePlanFilter filter, List<Predicate> predicates) {
			if (filter.getIsDeleted() != null) {
				predicates.add(criteriaBuilder.equal(root.get("isDeleted"), BooleanUtils.isTrue(filter.getIsDeleted())));
			}
		}
	} ,
	PRODUCT {
		@Override
		public void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				RatePlanFilter filter, List<Predicate> predicates) {
			if (filter.getProduct() != null) {
				predicates.add(criteriaBuilder.equal(root.get("product"), filter.getProduct().getCode()));
			}			
		}
	},
	SUPPLIERIDS {
		@Override
		public void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				RatePlanFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getSupplierIds())) {
				Expression<String> exp = root.get("supplierId");
				predicates.add(exp.in(filter.getSupplierIds()));
			}
		}
	},
	CREATED_ON {
		@Override
		public void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				RatePlanFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	};

	public abstract void addPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
			RatePlanFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnRatePlanFilter(Root<DbRatePlan> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, RatePlanFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (RatePlanSearchPredicate DbRatePlanCol : RatePlanSearchPredicate.values()) {
			DbRatePlanCol.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
