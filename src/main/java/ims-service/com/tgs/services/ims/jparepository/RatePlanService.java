package com.tgs.services.ims.jparepository;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.ims.datamodel.RatePlanFilter;
import com.tgs.services.ims.dbmodel.DbRatePlan;

@Service
public class RatePlanService {

	@Autowired
	private RatePlanRepository ratePlanRepo;
	
	public DbRatePlan save(DbRatePlan plan) {
		plan = ratePlanRepo.saveAndFlush(plan);
		return plan;
	}
	
	public DbRatePlan findById(Long id) {
		return ratePlanRepo.findOne(id);
	}

	public List<DbRatePlan> findAll(RatePlanFilter filter) {
		Specification<DbRatePlan> specification = new Specification<DbRatePlan>() {
			@Override
			public Predicate toPredicate(Root<DbRatePlan> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = RatePlanSearchPredicate.getPredicateListBasedOnRatePlanFilter(root, query,
						criteriaBuilder, filter);
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
		
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.DESC, Arrays.asList("createdOn"));
		}
		return ratePlanRepo.findAll(specification,sort);
	}
	
	public DbRatePlan fetchRatePlanForInventory(String inventoryId, LocalDate validOn) {
		return ratePlanRepo.fetchFareForInventory(inventoryId, validOn);
	}
}
