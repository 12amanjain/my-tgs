package com.tgs.services.ims.jparepository.air;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;

import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum AirInventorySearchPredicate {

	NAME {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (filter.getName() != null)
				predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
						"%" + filter.getName().toLowerCase() + "%"));
		}
	},
	INVENTORYID {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if(filter.getInventoryId() !=null)
				predicates.add(criteriaBuilder.equal(root.get("id"),filter.getInventoryId()));			
		}
	},
	SUPPLIERIDS {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getSupplierIds())) {
				Expression<String> exp = root.get("supplierId");
				predicates.add(exp.in(filter.getSupplierIds()));
			}
		}

	},
	AIRLINES {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getAirlines())) {
				Expression<String> exp = root.get("carrier");
				predicates.add(exp.in(filter.getAirlines()));
			}

		}
	},
	SOURCE {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getSource())) {
				Expression<String> exp = root.get("fromAirport");
				predicates.add(exp.in(filter.getSource()));
			}

		}
	},
	DESTINATION {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getDestination())) {
				Expression<String> exp = root.get("toAirport");
				predicates.add(exp.in(filter.getDestination()));
			}
		}
	},
	TRIPTYPE {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if(filter.getTripType()!=null) {
				predicates.add(criteriaBuilder.equal(root.get("tripType"),filter.getTripType().getCode()));			
			}
		}
	},
	ISENABLED {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (filter.getIsEnabled() != null) {
				predicates.add(criteriaBuilder.equal(root.get("isEnabled"), BooleanUtils.isTrue(filter.getIsEnabled())));
			}
		}
	},
	DELETED {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			if (filter.getDeleted() != null) {
				predicates.add(criteriaBuilder.equal(root.get("deleted"), filter.getDeleted()));
			}
		}
	},
	CREATED_ON {
		@Override
		public void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				AirInventoryFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	};

	public abstract void addPredicate(Root<DbAirInventory> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
			AirInventoryFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnAirInventoryFilter(Root<DbAirInventory> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, AirInventoryFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (AirInventorySearchPredicate inventory : AirInventorySearchPredicate.values()) {
			inventory.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
