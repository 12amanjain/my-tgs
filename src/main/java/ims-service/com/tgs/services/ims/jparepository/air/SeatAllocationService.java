package com.tgs.services.ims.jparepository.air;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.jparepository.SeatAllocationSearchPredicate;
import com.tgs.services.ims.restmodel.air.SeatAllocationFilter;

@Service
public class SeatAllocationService {

	@Autowired
	SeatAllocationRepository repo;
	
	public DbSeatAllocation save(DbSeatAllocation seatAllocation) {
		seatAllocation = repo.saveAndFlush(seatAllocation);
		return seatAllocation;
	}

	public DbSeatAllocation findById(Long id) {
		return repo.findOne(id);
	}
	
	public List<DbSeatAllocation> findByInventoryId( String inventoryId){
		return repo.findByInventoryIdOrderByValidOn(inventoryId);
	}
	
	public DbSeatAllocation findByInventoryIdAndValidOn(String inventoryId, LocalDate validOn) {
		return repo.findByInventoryIdAndValidOn(inventoryId, validOn);
	}
	
	public List<DbSeatAllocation> findAll(SeatAllocationFilter filter){
		Specification<DbSeatAllocation> specification = new Specification<DbSeatAllocation>() {
			@Override
			public Predicate toPredicate(Root<DbSeatAllocation> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				List<Predicate> predicates = SeatAllocationSearchPredicate.getPredicateListBasedOnSeatAllocationFilter(root,
						query,criteriaBuilder, filter);
				return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
			}
		};
		Sort sort = null;
		if (CollectionUtils.isNotEmpty(filter.getSortByAttr())) {
			Direction direction = Direction.fromString(filter.getSortByAttr().get(0).getOrderBy());
			sort = new Sort(direction, filter.getSortByAttr().get(0).getParams());
		} else {
			sort = new Sort(Direction.ASC, Arrays.asList("validOn"));
		}
		return repo.findAll(specification,sort);
	}
}
