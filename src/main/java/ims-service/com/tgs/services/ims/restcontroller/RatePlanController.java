package com.tgs.services.ims.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.ims.datamodel.RatePlanFilter;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import com.tgs.services.ims.restmodel.air.AirRatePlanResponse;
import com.tgs.services.ims.servicehandler.RatePlanFetchHandler;
import com.tgs.services.ims.servicehandler.RatePlanHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/ims/v1/rateplan")
@Scope(value = "session")
@CustomRequestProcessor(areaRole = AreaRole.IMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.SUPPLIER_ADMIN, UserRole.SUPPLIER, UserRole.CALLCENTER})
public class RatePlanController {

	@Autowired
	private RatePlanHandler ratePlanHandler;

	@Autowired
	private RatePlanFetchHandler ratePlanFetchHandler;

	@Autowired
	private AuditsHandler auditHandler;

	@RequestMapping(value = "/air/save", method = RequestMethod.POST)
	protected AirRatePlanResponse save(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirRatePlan ratePlanRequest) throws Exception {
		ratePlanHandler.initData(ratePlanRequest, new AirRatePlanResponse());
		return ratePlanHandler.getResponse();
	}

	@RequestMapping(value = "/air/update", method = RequestMethod.POST)
	protected AirRatePlanResponse update(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirRatePlan ratePlanRequest) throws Exception {
		ratePlanHandler.initData(ratePlanRequest, new AirRatePlanResponse());
		return ratePlanHandler.getResponse();
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse delete(HttpServletRequest request, HttpServletResponse response, @PathVariable("id") Long id)
			throws Exception {
		return ratePlanHandler.deleteRatePlan(id);
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	protected AirRatePlanResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid RatePlanFilter filter) throws Exception {
		ratePlanFetchHandler.initData(filter, new AirRatePlanResponse());
		return ratePlanFetchHandler.getResponse();
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbRatePlan.class, ""));
		return auditResponse;
	}

}
