package com.tgs.services.ims.restcontroller.air;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;
import com.tgs.services.ims.restmodel.air.AirInventoryListResponse;
import com.tgs.services.ims.restmodel.air.AirInventoryResponse;
import com.tgs.services.ims.servicehandler.air.AirInventoryHandler;
import com.tgs.services.ims.servicehandler.air.AirInventoryListingHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/ims/v1/airinventory")
@CustomRequestProcessor(areaRole = AreaRole.IMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.CALLCENTER, UserRole.SUPPLIER_ADMIN, UserRole.SUPPLIER})
@Scope(value = "session")
public class AirInventoryController {

	@Autowired
	private AirInventoryHandler inventoryHandler;

	@Autowired
	private AirInventoryListingHandler listHandler;

	@Autowired
	private AirInventoryValidator validator;

	@Autowired
	private AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected AirInventoryResponse createInventory(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirInventory airInventoryRequest) throws Exception {
		inventoryHandler.initData(airInventoryRequest, new AirInventoryResponse());
		return inventoryHandler.getResponse();
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse disableInventory(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return inventoryHandler.updateInventoryStatus(id, status);

	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	protected BaseResponse deleteInventory(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return inventoryHandler.deleteInventory(id);

	}

	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/search", method = RequestMethod.POST)
	protected AirInventoryListResponse listInventory(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirInventoryFilter airInventoryFilter) throws Exception {
		listHandler.initData(airInventoryFilter, new AirInventoryListResponse());
		return listHandler.getResponse();
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbAirInventory.class, ""));
		return auditResponse;
	}
}
