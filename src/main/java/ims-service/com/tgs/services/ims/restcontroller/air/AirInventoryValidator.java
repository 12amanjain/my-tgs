package com.tgs.services.ims.restcontroller.air;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.ims.datamodel.air.AirInventory;

@Component
public class AirInventoryValidator implements Validator {

	@Autowired
	private FMSCommunicator fmsImpl;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if(target instanceof AirInventory) {
			AirInventory inventoryRequest = (AirInventory) target;
			
			List<SegmentInfo> segmentInfos = inventoryRequest.getSegmentInfos();
			
			String destAirport = segmentInfos.get(inventoryRequest.getSegmentInfos().size()-1).getArrivalAirportInfo().getCode();
			//In case of return
			if(segmentInfos.size()==2 && segmentInfos.get(0).getDepartAirportInfo().getCode().equals(segmentInfos.get(1).getArrivalAirportInfo().getCode())
						&& segmentInfos.get(0).getArrivalAirportInfo().getCode().equals(segmentInfos.get(1).getDepartAirportInfo().getCode())) {
				destAirport=segmentInfos.get(0).getArrivalAirportInfo().getCode();
			}
			
			if(fmsImpl.getAirportInfo(segmentInfos.get(0).getDepartAirportInfo().getCode())==null) {
				rejectValue(errors, "fromAirport", SystemError.INVALID_AIRPORT_CODE,inventoryRequest.getFromAirport());
			}
			if(fmsImpl.getAirportInfo(destAirport)==null) {
				rejectValue(errors, "toAirport", SystemError.INVALID_AIRPORT_CODE,inventoryRequest.getToAirport());
			}
			if(CollectionUtils.isNotEmpty(checkDuplicateSegments(inventoryRequest.getSegmentInfos()))) {
				rejectValue(errors, "segmentInfos", SystemError.DUPLICATE_SEGMENTS,inventoryRequest.getSegmentInfos());
			}
			
			SegmentInfo prevSegmentInfo = null;
			for(SegmentInfo segmentInfo : segmentInfos) {
				LocalDate today = LocalDate.now();
				LocalDateTime prevDepartureTime = prevSegmentInfo!=null ? prevSegmentInfo.getDepartTime() : null;
				LocalDateTime departureTime = LocalDateTime.of(today, LocalTime.parse(segmentInfo.getDeparture()));
				LocalDateTime arrivalTime = LocalDateTime.of(BooleanUtils.isTrue(segmentInfo.isArrivalNextDay()) ? today.plusDays(1) : today , LocalTime.parse(segmentInfo.getArrival()));
				if(!arrivalTime.isAfter(departureTime))
					rejectValue(errors, "segmentInfos", SystemError.INVALID_TIMINGS,inventoryRequest.getSegmentInfos());
				if(arrivalTime.minusMinutes(30).isBefore(departureTime))
					rejectValue(errors, "segmentInfos", SystemError.INVALID_DURATION,inventoryRequest.getSegmentInfos());
				//if(prevDepartureTime!=null && prevDepartureTime.isBefore(arrivalTime) && BooleanUtils.isFalse(prevSegmentInfo.isArrivalNextDay()))
				//	rejectValue(errors, "segmentInfos", SystemError.INVALID_SEGMENT_TIMINGS,inventoryRequest.getSegmentInfos());
				prevSegmentInfo = segmentInfo;
			}
			
		}
		
	}

	public static List<SegmentInfo> checkDuplicateSegments(List<SegmentInfo> segmentList) {
		List<SegmentInfo> duplicates = segmentList.stream()
				.collect(Collectors.groupingBy(SegmentInfo::getUniqueAttributes)).entrySet().stream()
				.filter(e -> e.getValue().size() > 1).flatMap(e -> e.getValue().stream()).collect(Collectors.toList());
		return duplicates;
	}


	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
