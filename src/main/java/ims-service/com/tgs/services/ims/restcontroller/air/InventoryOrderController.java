package com.tgs.services.ims.restcontroller.air;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import com.tgs.services.ims.dbmodel.DbInventoryOrder;
import com.tgs.services.ims.jparepository.InventoryOrderService;
import com.tgs.services.ims.restmodel.air.AirInventoryOrder;
import com.tgs.services.ims.restmodel.air.InventoryOrderResponse;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("/ims/v1/inventoryorder")
@CustomRequestProcessor(areaRole = AreaRole.IMS_SERVICE,
		includedRoles = {UserRole.ADMIN, UserRole.CALLCENTER, UserRole.SUPPLIER_ADMIN, UserRole.SUPPLIER})
@Scope(value = "session")
public class InventoryOrderController {

	@Autowired
	InventoryOrderService orderService;

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	protected InventoryOrderResponse listInventoryOrders(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody InventoryOrderFilter inventoryOrderFilter) throws Exception {
		inventoryOrderFilter.setSupplierIdIn(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
				inventoryOrderFilter.getSupplierIdIn()));
		List<DbInventoryOrder> inventoryOrders = orderService.findAll(inventoryOrderFilter);
		List<InventoryOrder> orders = BaseModel.toDomainList(inventoryOrders);
		List<AirInventoryOrder> orderPerPax = new ArrayList<>();
		orders.forEach(order -> {
			AirInventoryOrderInfo additionalInfo = (AirInventoryOrderInfo) order.getAdditionalInfo();
			order.getTravellerInfo().forEach(traveller -> {
				AirInventoryOrder airInvOrder = new GsonMapper<>(order, null, AirInventoryOrder.class).convert();
				if (traveller.getStatus() == null && StringUtils.isBlank(additionalInfo.getPnr())
						&& StringUtils.isBlank(traveller.getPnr())) {
					traveller.setStatus(TravellerStatus.PENDING);
				}
				airInvOrder.setAirTravellerInfo(traveller);
				orderPerPax.add(airInvOrder);
			});
		});
		return new InventoryOrderResponse(orderPerPax);
	}
}
