package com.tgs.services.ims.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.ims.datamodel.ImsConstants;
import com.tgs.services.ims.datamodel.RatePlanFilter;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ims.dbmodel.DbRatePlan;
import com.tgs.services.ims.helper.RatePlanHelper;
import com.tgs.services.ims.jparepository.RatePlanService;
import com.tgs.services.ims.restmodel.air.AirRatePlanResponse;
import com.tgs.utils.exception.ResourceNotFoundException;

@Service
public class RatePlanHandler extends ServiceHandler<AirRatePlan, AirRatePlanResponse> {

	@Autowired
	private RatePlanService ratePlanService;

	DbRatePlan ratePlan;

	@Override
	public void beforeProcess() throws Exception {
		if (request.getId() != null) {
			ratePlan = ratePlanService.findById(Long.valueOf(request.getId()));
			if (ratePlan == null) {
				throw new ResourceNotFoundException(SystemError.INVALID_ID);
			}
		}
		checkDuplicacy();
	}

	@Override
	public void process() throws Exception {
		if (ratePlan == null) {
			ratePlan = new GsonMapper<>(request, ratePlan, DbRatePlan.class).convert();
			if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.SUPPLIER)) {
				ratePlan.setSupplierId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
			} else if (StringUtils.isEmpty(ratePlan.getSupplierId())) {
				ratePlan.setSupplierId(ImsConstants.DEFAULT_SUPPLIER);
			}
		} else {
			ratePlan = new GsonMapper<>(request, ratePlan, DbRatePlan.class).convert();
		}
		ratePlan.getRatePlanInfo().forEach(info -> {
			if (info.getBaseFare() <= 0)
				throw new CustomGeneralException(SystemError.INVALID_FARE);
			info.setCabinClass(request.getCabinClass().getCode());
			info.setBookingClass(request.getBookingClass());
		});
		ratePlanService.save(ratePlan);
		RatePlanHelper.updateRatePlan(ratePlan.toDomain());
		AirRatePlan plan = ratePlan.toDomain();
		plan.setCabinClass(request.getCabinClass());
		plan.setBookingClass(request.getBookingClass());
		response.getRatePlans().add(plan);
	}

	private void checkDuplicacy() {
		List<String> supplierIds = Lists.newArrayList(ObjectUtils.firstNonNull(request.getSupplierId(),
				SystemContextHolder.getContextData().getUser().getLoggedInUserId()));
		if (supplierIds == null)
			supplierIds = new ArrayList<>();
		supplierIds.add(ImsConstants.DEFAULT_SUPPLIER);
		RatePlanFilter rpFilter =
				RatePlanFilter.builder().name(request.getName()).supplierIds(supplierIds).isDeleted(false).build();
		List<DbRatePlan> ratePlans = ratePlanService.findAll(rpFilter);
		// This is done to ensure that if during update request same rateplan is being updated.
		if (request.getId() != null)
			ratePlans = ratePlans.stream().filter(p -> !p.getId().equals(request.getId())).collect(Collectors.toList());
		if (CollectionUtils.isNotEmpty(ratePlans)) {
			response.addError(SystemError.DUPLICATE_RATEPLAN
					.getErrorDetail(ratePlans.stream().map(obj -> obj.getName()).collect(Collectors.toList())));
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public BaseResponse deleteRatePlan(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbRatePlan dbRatePlan = ratePlanService.findById(id);
		if (Objects.nonNull(dbRatePlan)) {
			dbRatePlan.setDeleted(true);
			ratePlanService.save(dbRatePlan);
			RatePlanHelper.updateRatePlan(dbRatePlan.toDomain());
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
