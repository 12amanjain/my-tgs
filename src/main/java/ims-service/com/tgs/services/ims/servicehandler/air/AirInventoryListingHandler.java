package com.tgs.services.ims.servicehandler.air;

import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.ims.dbmodel.air.DbAirInventory;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.jparepository.air.AirInventoryService;
import com.tgs.services.ims.jparepository.air.SeatAllocationService;
import com.tgs.services.ims.restmodel.air.AirInventoryFilter;
import com.tgs.services.ims.restmodel.air.AirInventoryListResponse;
import com.tgs.services.ims.restmodel.air.AirInventoryTrip;

@Service
public class AirInventoryListingHandler extends ServiceHandler<AirInventoryFilter, AirInventoryListResponse> {

	@Autowired
	private AirInventoryService service;

	@Autowired
	private FMSCommunicator fmsImpl;

	@Autowired
	UserServiceCommunicator userServiceComm;

	@Autowired
	private SeatAllocationService seatAllocationSrv;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {
		request.setSupplierIds(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), request.getSupplierIds()));

		List<DbAirInventory> inventories = service.findAll(request);
		inventories.forEach(inventory -> {
			inventory.getSegmentInfos().forEach(segment -> {
				String departureTerminal = segment.getDepartAirportInfo().getTerminal();
				String arrivalTerminal = segment.getArrivalAirportInfo().getTerminal();
				segment.setDepartAirportInfo(fmsImpl.getAirportInfo(segment.getDepartAirportInfo().getCode()));
				segment.setArrivalAirportInfo(fmsImpl.getAirportInfo(segment.getArrivalAirportInfo().getCode()));
				segment.getDepartAirportInfo().setTerminal(departureTerminal);
				segment.getArrivalAirportInfo().setTerminal(arrivalTerminal);
			});
			TripInfo tripInfo = BookingUtils.createTripListFromSegmentList(inventory.getSegmentInfos()).get(0);
			AirInventoryTrip inventoryTrip = new GsonMapper<>(inventory, null, AirInventoryTrip.class).convert();
			inventoryTrip.setTripInfo(tripInfo);
			List<DbSeatAllocation> seatAllocations =
					seatAllocationSrv.findByInventoryId(String.valueOf(inventory.getId()));
			if (CollectionUtils.isNotEmpty(seatAllocations)) {
				Collections.sort(seatAllocations, (s1, s2) -> s1.getValidOn().compareTo(s2.getValidOn()));
				inventoryTrip.setStartDate(seatAllocations.get(0).getValidOn());
				inventoryTrip.setEndDate(seatAllocations.get(seatAllocations.size() - 1).getValidOn());
			}
			response.getInventoryTrip().add(inventoryTrip);
		});

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
