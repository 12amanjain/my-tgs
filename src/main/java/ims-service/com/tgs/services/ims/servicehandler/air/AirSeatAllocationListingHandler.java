package com.tgs.services.ims.servicehandler.air;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.ims.datamodel.air.AirSeatAllocationAdditionalInfo;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.jparepository.air.SeatAllocationService;
import com.tgs.services.ims.restmodel.air.AirSeatAllocationListResponse;
import com.tgs.services.ims.restmodel.air.SeatAllocationFilter;

@Service
public class AirSeatAllocationListingHandler
		extends ServiceHandler<SeatAllocationFilter, AirSeatAllocationListResponse> {

	@Autowired
	SeatAllocationService service;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void process() throws Exception {
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();

		// to fetch undeleted seat allocations
		request.setIsDeleted(false);

		List<DbSeatAllocation> seatAllocations = service.findAll(request);

		if (CollectionUtils.isNotEmpty(seatAllocations)) {
			for (DbSeatAllocation allocation : seatAllocations) {
				AirSeatAllocationAdditionalInfo seatAllocationInfo = gson.fromJson(gson.toJson(allocation),
						AirSeatAllocationAdditionalInfo.class);
				if (allocation.getAdditionalInfo() != null) {
					seatAllocationInfo.setAirlinePnr(allocation.getAdditionalInfo().getAirlinePnr());
				}
				seatAllocationInfo.setStartDate(allocation.getValidOn());
				seatAllocationInfo.setEndDate(allocation.getValidOn());
				if(StringUtils.isNotEmpty(allocation.getFareRuleId())) {
					FareRuleInfo fareRuleInfo = fmsCommunicator.getFareRule(Long.valueOf(allocation.getFareRuleId()));
					if (fareRuleInfo != null) {
						seatAllocationInfo.setFareRuleName(fareRuleInfo.getDescription());
					}
				}
				response.getAllocationInfo().add(seatAllocationInfo);
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.setInventoryId(request.getInventoryId());
		response.setProduct(Product.AIR);
	}

}
