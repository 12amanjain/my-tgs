package com.tgs.services.ims.servicehandler.air;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ims.datamodel.air.AirSeatAllocationAdditionalInfo;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocation;
import com.tgs.services.ims.dbmodel.air.DbSeatAllocationAdditionalInfo;
import com.tgs.services.ims.helper.AirInventoryHelper;
import com.tgs.services.ims.helper.RatePlanHelper;
import com.tgs.services.ims.helper.SeatAllocationHelper;
import com.tgs.services.ims.jparepository.air.SeatAllocationService;
import com.tgs.services.ims.restmodel.air.SeatAllocationResponse;


@Service
public class SeatAllocationHandler extends ServiceHandler<SeatAllocation, SeatAllocationResponse> {

	@Autowired
	private SeatAllocationService service;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void process() throws Exception {
		
		List<String> ratePlanIds = new ArrayList<>();
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		request.getAllocationInfo().forEach(allocationInfo -> {
			ratePlanIds.add(allocationInfo.getRatePlanId());
		});
		Map<String, AirRatePlan> ratePlanMap = RatePlanHelper.searchRatePlan(ratePlanIds);
		
		Map<String, AirInventory> airInventoryMap = AirInventoryHelper
				.searchAirInventory(Arrays.asList(request.getInventoryId()));
		
		request.getAllocationInfo().forEach(allocationInfo -> {
			LocalDate startDate = allocationInfo.getStartDate();
			LocalDate endDate = allocationInfo.getEndDate();
			if(startDate.isAfter(endDate))
				throw new CustomGeneralException(SystemError.INVALID_DATE_COMBINATION);	
			List<LocalDate> dates = Stream.iterate(startDate, date -> date.plusDays(1))
					.limit(ChronoUnit.DAYS.between(startDate, endDate)+1).collect(Collectors.toList());
			dates.forEach(date -> {
				AirInventory airInventory = airInventoryMap.get(request.getInventoryId());
				if(airInventory == null) {
					throw  new CustomGeneralException(SystemError.INVENTORY_NOT_FOUND);	
				}
					
				DbSeatAllocation seatAllocation = service.findByInventoryIdAndValidOn(request.getInventoryId(), date);
				seatAllocation = new GsonMapper<>(allocationInfo, seatAllocation, DbSeatAllocation.class).convert();
				seatAllocation.setInventoryId(request.getInventoryId());
				seatAllocation.setSupplierId(airInventory.getSupplierId());
				seatAllocation.setValidOn(date);
				AirRatePlan ratePlan = ratePlanMap.get(seatAllocation.getRatePlanId());
				if(ratePlan!=null)
					seatAllocation.setRatePlanName(ratePlan.getName());
				if (request.getProduct().equals(Product.AIR)) {
					String airlinePnr = ((AirSeatAllocationAdditionalInfo) allocationInfo).getAirlinePnr();
					if (airlinePnr != null) {
						DbSeatAllocationAdditionalInfo additionalInfo = DbSeatAllocationAdditionalInfo.builder()
								.airlinePnr(airlinePnr).build();
						seatAllocation.setAdditionalInfo(additionalInfo);
					}
					
				}
				service.save(seatAllocation);
				if (airInventory != null && ratePlan != null) {
					AirSeatAllocationAdditionalInfo seatAllocationInfo = gson.fromJson(gson.toJson(seatAllocation), AirSeatAllocationAdditionalInfo.class);
					seatAllocationInfo.setAirlinePnr(seatAllocation.getAdditionalInfo().getAirlinePnr());
					SeatAllocationHelper.updateSeatAllocation(seatAllocationInfo,airInventory);
				}
			});
		});
	}

	@Override
	public void afterProcess() throws Exception {
		response.getSeatAllocations().add(request);
	}

	public BaseResponse deleteSeatAllocation(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		DbSeatAllocation dbSeatAllocation = service.findById(id);
		if (Objects.nonNull(dbSeatAllocation)) {
			dbSeatAllocation.setDeleted(true);
			service.save(dbSeatAllocation);
			Map<String, AirInventory> airInventoryMap = AirInventoryHelper
					.searchAirInventory(Arrays.asList(dbSeatAllocation.getInventoryId()));
			SeatAllocationHelper.updateSeatAllocation(
					gson.fromJson(gson.toJson(dbSeatAllocation), AirSeatAllocationAdditionalInfo.class),
					airInventoryMap.get(dbSeatAllocation.getInventoryId()));
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
