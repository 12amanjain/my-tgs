import io.woo.htmltopdf.HtmlToPdf;
import io.woo.htmltopdf.HtmlToPdfObject;
import io.woo.htmltopdf.PdfOrientation;
import io.woo.htmltopdf.PdfPageSize;

public class HtmlToPdfExample {

	public static void main(String[] args) throws Exception {

		try {

			String url = "http://staging.technogramsolutions.com/booking-confirmation/5008182388" ;
			HtmlToPdfObject o1 =   HtmlToPdfObject.forUrl(url).defaultEncoding("utf-8").produceForms(true).
					usePrintMediaType(true).enableIntelligentShrinking(true).
					loadImages(true);
			boolean isSuccess= HtmlToPdf.create().pageSize(PdfPageSize.A4).orientation(PdfOrientation.PORTRAIT).compression(true).dpi(250)
					.object(o1).convert("/Users/nehaarora/Documents/file.pdf");
			System.out.println(isSuccess);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}  

}
