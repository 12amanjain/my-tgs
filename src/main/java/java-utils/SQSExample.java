import com.tgs.utils.encryption.EncryptionUtils;

public class SQSExample {

	public static void main(String[] args) {
		System.out.println(EncryptionUtils.decryptUsingKMS(
				"AQICAHh+oTNTPCwA7YYoO+KastkZV0+hn89X1BztsPjXuAoRFAFfji8IjMyVl4rYdZ8A/99rAAAAajBoBgkqhkiG9w0BBwagWzBZAgEAMFQGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMsYmhK6IF/0ZsHkzSAgEQgCeMyLfrNwdRvlBse0yhN5oUYTw1GDQMa6SkwNHrkK448QhV4e6UTcQ=",
				"arn:aws:kms:ap-south-1:766916178229:key/7d86ed5c-ac21-4411-b22b-a47a2defd372"));

		// AmazonSQSClient client = new AmazonSQSClient();
		//
		// SendMessageRequest send_msg_request = new SendMessageRequest()
		// .withQueueUrl("https://sqs.ap-south-1.amazonaws.com/766916178229/email-queue")
		// .withMessageBody("hello world1").withDelaySeconds(0);
		//
		// client.sendMessage(send_msg_request);
		//
		// List<Message> messages =
		// client.receiveMessage("https://sqs.ap-south-1.amazonaws.com/766916178229/email-queue")
		// .getMessages();
		// for (Message msg : messages) {
		// System.out.println(msg.getBody());
		// }
		//
		// for (Message m : messages) {
		// client.deleteMessage("https://sqs.ap-south-1.amazonaws.com/766916178229/email-queue",
		// m.getReceiptHandle());
		// }
	}
}
