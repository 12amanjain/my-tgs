import java.io.IOException;

import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

// build.gradle #  compile 'com.sendgrid:sendgrid-java:4.0.1'
public class SendGridExample {

	public static void main(String[] args) {
		Email from = new Email("ashu.gupta@technogramsolutions.com");
		String subject = "Interview Details";
		Email to = new Email("ashudreamcity@gmail.com");
		Content content = new Content("text/plain", "Sending you interview details for all the candidates");
		Mail mail = new Mail(from, subject, to, content);

		SendGrid sg = new SendGrid("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");
		Request request = new Request();
		try {
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(mail.build());
			Response response = sg.api(request);
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
		} catch (IOException ex) {

		}
	}
}
