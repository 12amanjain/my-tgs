import com.tgs.utils.string.StringTemplateUtils;

import lombok.Getter;
import lombok.Setter;

public class StringTemplateUtilsExample {

	@Getter
	@Setter
	public static class Attributes {
		public String value;

		public Attributes(String value, String emailId) {
			super();
			this.value = value;
			this.emailId = emailId;
		}

		public String emailId;

		public String fun() {
			return value;
		}

		public Attributes attr;
	}

	public static void main(String[] args) {
		String text = "Please send this value to $value$ to attributes $attr.fun$";
		Attributes attr = new Attributes("123", "ashu.gupta");
		Attributes innerAttr = new Attributes("inner", "inneremail");
		attr.setAttr(innerAttr);
		System.out.println(StringTemplateUtils.replaceAttributes(text, attr));
	}
}
