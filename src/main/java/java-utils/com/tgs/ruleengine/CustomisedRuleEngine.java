package com.tgs.ruleengine;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.lang3.BooleanUtils;

import com.amazonaws.util.CollectionUtils;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.ruleengine.IRuleCriteria;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.base.ruleengine.RuleEngine;
import com.tgs.services.base.utils.TgsObjectUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

@ToString
@Getter
@Setter
@Slf4j
public class CustomisedRuleEngine extends RuleEngine {

	Map<String, ? extends IRuleField> fieldResolverMap;

	public CustomisedRuleEngine(List<? extends IRule> rules, IFact fact,
			Map<String, ? extends IRuleField> fieldResolverMap) {
		super(rules, fact);
		this.fieldResolverMap = fieldResolverMap;
	}

	@Override
	public List<? extends IRule> fireAllRules() {
		if (CollectionUtils.isNullOrEmpty(rules)) {
			return rules;
		}
		List<IRule> matchingList = new ArrayList<>();

		// Consider Only Enabled filters
		rules = rules.stream().filter(IRule::getEnabled).collect(Collectors.toList());
		Collections.sort(getRules(), (Comparator<IRule>) (first, second) -> (first.getPriority() >= second.getPriority() ? -1 : 1));
		for (IRule rule : getRules()) {
			if (rule.getExclusionCriteria() == null || !isApplicableAgainstFact(rule.getExclusionCriteria(), false)) {
				if (rule.getInclusionCriteria() != null && isApplicableAgainstFact(rule.getInclusionCriteria(), true)) {
					matchingList.add(rule);
					if (BooleanUtils.isNotFalse(rule.exitOnMatch())) {
						break;
					}
				}
			}
		}
		return matchingList;
	}

	/**
	 * Break if validation doesn't evaluate to {@code defaultValue}, i.e.,
	 * <li>if {@code defaultValue} is {@code true}, it returns {@code true} if all
	 * fields in fact are valid - inclusion criteria,
	 * <li>if {@code defaultValue} is {@code false}, it returns {@code false} if all
	 * fields in fact are not valid - exclusion criteria.
	 * 
	 * @param ruleCriteria
	 * @param defaultValue
	 * @return
	 */
	private boolean isApplicableAgainstFact(IRuleCriteria ruleCriteria, boolean defaultValue) {
		List<String> fields = TgsObjectUtils.getNotNullFields(ruleCriteria, false,false);
		boolean breakValue = !defaultValue;
		for (String field : fields) {
			try {
				if (breakValue == fieldResolverMap.get(field.toUpperCase()).isValidAgainstFact(this.getFact(),
						ruleCriteria)) {
					return breakValue;
				}
			} catch (Exception e) {
				log.error("Unable to valid fact against rule for field {}, ", field, e);
			}
		}
		return defaultValue;
	}
}
