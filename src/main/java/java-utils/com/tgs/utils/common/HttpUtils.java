package com.tgs.utils.common;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.HttpHeader;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.PaymentHeader;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.gson.GsonUtils;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Setter
@Getter
public class HttpUtils {

	private Map<String, String> headerParams;
	public static final String REQ_METHOD_POST = "POST";
	public static final String REQ_METHOD_GET = "GET";
	public static final String REQ_METHOD_PUT = "PUT";
	public static final String API_HEADER_KEY = "apikey";
	private String urlString;
	private String postData;
	private String requestMethod;
	@Builder.Default
	private int timeout = 60000;
	private String responseString;
	private int status;
	private InputStream ip;
	private Boolean printResponseLog;
	private Map<String, String> queryParams;

	private Proxy proxy;

	public static final String HEADER_CHANNEL = "ChannelType";
	public static final String USER_AGENT = "User-Agent";
	public static final String HEADER_BROWSER = "browserName";
	public static final String HEADER_BROWSER_VERSION = "browserVersion";
	public static final String SYNC_HEADER = "sync";
	@Builder.Default
	private List<CheckPointData> checkPoints = new ArrayList<>();

	/**
	 * This will convert response into typeT only if response is json, In case typeT is null , It will response response
	 * as a String
	 *
	 * @param typeOfT
	 * @return Object of typeT if response is json
	 */
	@SuppressWarnings("unchecked")
	public <T> Optional<T> getResponse(Class<T> typeOfT) throws IOException {
		HttpURLConnection httpUrlCon = null;
		try {
			T response;
			httpUrlCon = getURLConnection();
			responseString = getContent(httpUrlCon);
			if (BooleanUtils.isNotFalse(printResponseLog)) {
				log.debug("Response String is " + responseString);
			}
			if (typeOfT == null) {
				response = (T) responseString;
				return Optional.ofNullable(response);
			}
			response = GsonUtils.getGson().fromJson(responseString, typeOfT);
			return Optional.ofNullable(response);
		} catch (IOException e) {
			responseString = e.getMessage();
			throw e;
		} catch (Exception e) {
			log.error("Unable to retrieve response for url {} , postData {}", urlString, postData, e);
		} finally {
			checkPoints.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_FINISHED.name())
					.time(System.currentTimeMillis()).build());
		}
		return Optional.empty();
	}

	public <T> HttpURLConnection getURLConnection() throws MalformedURLException, Exception, IOException {
		checkPoints.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_STARTED.name())
				.time(System.currentTimeMillis()).build());
		URL url = new URL(constructUrlWithQueryParams());
		HttpURLConnection httpUrlCon = getUrlConnection(url, proxy);
		setHeaderParams(httpUrlCon);
		setURLConnection(httpUrlCon);
		httpUrlCon.connect();

		boolean redirect = false;
		// normally, 3xx is redirect
		status = httpUrlCon.getResponseCode();
		if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP
				|| status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
			redirect = true;
		}
		if (redirect) {
			String newUrl = httpUrlCon.getHeaderField("Location");
			httpUrlCon = getUrlConnection(new URL(newUrl), proxy);
			setHeaderParams(httpUrlCon);
		}
		return httpUrlCon;
	}

	public HttpURLConnection getUrlConnection(URL url, Proxy proxy) throws Exception {
		if (proxy != null) {
			return (HttpURLConnection) url.openConnection(proxy);
		} else {
			return (HttpURLConnection) url.openConnection();
		}
	}

	private HttpURLConnection setURLConnection(HttpURLConnection httpUrlCon) throws Exception {
		if (timeout > 0) {
			httpUrlCon.setReadTimeout(timeout);
		}
		httpUrlCon.setDoInput(true);
		httpUrlCon.setInstanceFollowRedirects(true);
		if (StringUtils.isNotBlank(postData)) {
			if (requestMethod != null) {
				httpUrlCon.setRequestMethod(requestMethod);
			} else {
				httpUrlCon.setRequestMethod(REQ_METHOD_POST);
			}
			setPostDataForUrlCon(httpUrlCon, postData);
		} else if (requestMethod != null) {
			httpUrlCon.setRequestMethod(requestMethod);
		}
		return httpUrlCon;
	}

	public void setPostDataForUrlCon(URLConnection conn, String postData) throws Exception {
		conn.setDoOutput(true);
		OutputStream outStream = conn.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(outStream);
		osw.write(postData.toString(), 0, postData.toString().length());
		osw.flush();
		osw.close();
	}

	private void setHeaderParams(HttpURLConnection httpUrlCon) {
		if (headerParams == null || headerParams.get(USER_AGENT) == null) {
			httpUrlCon.setRequestProperty(USER_AGENT,
					"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.87 Safari/537.36");
		}
		if (null == headerParams || headerParams.isEmpty()) {
			return;
		}
		Set<String> keys = headerParams.keySet();
		for (String key : keys) {
			httpUrlCon.setRequestProperty(key, headerParams.get(key));
		}
	}

	public String constructUrlWithQueryParams() {
		if (null == queryParams || queryParams.isEmpty()) {
			return urlString;
		}

		Set<String> keys = queryParams.keySet();
		StringBuilder query = new StringBuilder();
		for (String key : keys) {
			try {
				if (queryParams.get(key) == null)
					continue;
				if (query.length() > 0)
					query.append("&");
				query.append(key).append("=").append(URLEncoder.encode(queryParams.get(key), "UTF-8"));
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to encode the query parameter with value {} ", queryParams.get(key));
				throw new RuntimeException("Unable to encode the query parameter with value : " + queryParams.get(key));
			}
		}
		log.debug("URL generated is {}", urlString + "?" + query);
		urlString = urlString + "?" + query;
		return urlString;
	}

	public String getContent(URLConnection urlCon) throws IOException {
		if (urlCon == null) {
			return null;
		}
		HttpURLConnection httpUrlCon = (HttpURLConnection) urlCon;
		String encoding = urlCon.getContentEncoding();
		InputStream ip = null;
		StringBuffer strBuf = new StringBuffer();
		try {
			if (httpUrlCon.getResponseCode() >= HttpURLConnection.HTTP_BAD_REQUEST
					&& httpUrlCon.getResponseCode() < HttpURLConnection.HTTP_INTERNAL_ERROR) {
				if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
					ip = new GZIPInputStream(new BufferedInputStream(httpUrlCon.getErrorStream()));
				} else {
					ip = new BufferedInputStream(httpUrlCon.getErrorStream());
				}
			} else if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
				ip = new GZIPInputStream(new BufferedInputStream(urlCon.getInputStream()));
			} else {
				ip = new BufferedInputStream(urlCon.getInputStream());
			}
			byte[] b = new byte[4096];
			for (int n = 0; (n = ip.read(b)) != -1;) {
				strBuf.append(new String(b, 0, n));
			}
		} finally {
			try {
				if (ip != null) {
					ip.close();
				}
			} catch (Exception e) {
				log.error("Not able to close bufferred stream {} " + e.getMessage());
			}
		}
		return strBuf.toString();
	}

	public byte[] getByteArrayFromInputStream() throws Exception {
		HttpURLConnection httpUrlCon = getURLConnection();
		String encoding = httpUrlCon.getContentEncoding();
		InputStream ip = null;
		ByteArrayOutputStream op = null;
		try {
			if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
				ip = new GZIPInputStream(new BufferedInputStream(httpUrlCon.getInputStream()));
			} else {
				ip = new BufferedInputStream(httpUrlCon.getInputStream());
			}
			op = new ByteArrayOutputStream();
			int bytesRead = 0;
			byte[] input = new byte[ip.available()];
			while (-1 != (bytesRead = ip.read(input))) {
				op.write(input, 0, bytesRead);
			}
			input = op.toByteArray();
			op.flush();
			// byte[] readData = new byte[ip.available()];
			// ip.read(readData);
			return input;
		} finally {
			try {
				if (ip != null) {
					ip.close();
				}
				if (op != null) {
					op.close();
				}
			} catch (Exception e) {
				log.error("Not able to close bufferred stream {} " + e.getMessage());
			}
		}
	}

	public static String getValueFromCookie(HttpServletRequest request, String cookieName) {
		String rawCookies = request.getHeader("cookie");
		if (!StringUtils.isBlank(rawCookies)) {
			String[] rawCookieParams = rawCookies.split(";");
			for (String rawCookieParam : rawCookieParams) {
				String[] cookieKeyValuePair = rawCookieParam.split("=");
				if (cookieKeyValuePair != null && cookieName.equalsIgnoreCase(cookieKeyValuePair[0].trim())) {
					try {
						return URLDecoder.decode(cookieKeyValuePair[1].trim(), "UTF-8");
					} catch (Exception e) {
						log.error("Note able to decode cookie {} due to", cookieKeyValuePair[1], e);
					}
				}
			}
		}

		String val = null;
		Cookie[] cookiesArray = request.getCookies();
		if (cookiesArray != null) {
			for (Cookie cookie : cookiesArray) {
				if (cookieName.equals(cookie.getName())) {
					val = cookie.getValue();
				}
			}
		}
		try {
			if (val != null) {
				val = URLDecoder.decode(val, "UTF-8");
			}
		} catch (Exception e) {
			log.error("Note able to decode cookie {} due to", val, e);
		}
		return val;
	}

	public static String getValueFromCookieOrHeader(HttpServletRequest request, String key) {
		String value = request.getHeader(key);
		if (StringUtils.isBlank(value)) {
			value = getValueFromCookie(request, key);
		}
		return value;
	}

	public static HttpHeader buildHttpHeader(HttpServletRequest request) {
		UserAgent uA = UserAgent.parseUserAgentString(request.getHeader(USER_AGENT));
		ChannelType channelType = (request.getHeader(HEADER_CHANNEL) != null)
				? ChannelType.getChannelType(request.getHeader(HEADER_CHANNEL))
				: null;
		String browserName = request.getHeader(HEADER_BROWSER) != null ? request.getHeader(HEADER_BROWSER)
				: uA.getBrowser().getName();
		String browserVersion =
				request.getHeader(HEADER_BROWSER_VERSION) != null ? request.getHeader(HEADER_BROWSER_VERSION)
						: (uA.getBrowserVersion() != null ? uA.getBrowserVersion().getVersion() : "Un-known");

		PaymentHeader payHeader = null;
		if (request.getHeader("payHeader") != null) {
			payHeader = GsonUtils.getGson().fromJson(request.getHeader("payHeader"), PaymentHeader.class);
		}

		boolean isSyncRequest = Boolean.parseBoolean(getValueFromCookieOrHeader(request, SYNC_HEADER));

		// This check will be removef after frontend code to send whitelabel id is merged
		String partnerId = request.getHeader("whiteLabel");
		if (StringUtils.isEmpty(partnerId))
			partnerId = "0";
		return HttpHeader.builder().browserVersion(browserVersion).browser(browserName)
				.ip(StringUtils.isBlank(request.getHeader("X-Forwarded-For")) ? request.getRemoteAddr()
						: request.getHeader("X-Forwarded-For"))
				.device(uA.getOperatingSystem().getDeviceType().getName()).channelType(channelType).payHeader(payHeader)
				.isSyncRequest(isSyncRequest).apikey(request.getHeader(API_HEADER_KEY))
				.deviceid(getValueFromCookieOrHeader(request, "deviceid"))
				.key1(getValueFromCookieOrHeader(request, "key1")).key2(getValueFromCookieOrHeader(request, "key2"))
				.key3(getValueFromCookieOrHeader(request, "key3")).currEnv(request.getHeader("currEnv"))
				.partnerId(partnerId).build();
	}

	public static Map<String, Object> getRequestParams(HttpServletRequest request) {
		Map<String, Object> requestMap = new HashMap<>();
		Enumeration<String> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = enumeration.nextElement();
			requestMap.put(parameterName, request.getParameter(parameterName));
		}
		return requestMap;

	}

}
