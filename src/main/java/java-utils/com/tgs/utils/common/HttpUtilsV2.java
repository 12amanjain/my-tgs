package com.tgs.utils.common;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.Proxy;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.MultiMap;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.HttpHeader;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.datamodel.PaymentHeader;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.gson.GsonUtils;
import eu.bitwalker.useragentutils.UserAgent;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Builder
@Setter
@Getter
public class HttpUtilsV2 {

	private Map<String, String> headerParams;
	@Builder.Default
	private Map<String, List<String>> responseHeaderParams = new HashMap<>();
	public static final String REQ_METHOD_POST = "POST";
	public static final String REQ_METHOD_GET = "GET";
	public static final String REQ_METHOD_PUT = "PUT";
	public static final String REQ_METHOD_DELETE = "DELETE";
	private String urlString;
	private String postData;
	private String putData;
	private String requestMethod;
	private int timeout;
	private String responseString;
	private Boolean printResponseLog;
	private MultiMap recurringQueryParams;
	private Boolean isGzippedResponse;

	private Proxy proxy;

	public static final String HEADER_CHANNEL = "ChannelType";
	public static final String HEADER_BROWSER = "browserName";
	public static final String HEADER_BROWSER_VERSION = "browserVersion";
	@Builder.Default
	private List<CheckPointData> checkPoints = new ArrayList<>();

	@SuppressWarnings("unchecked")
	public <T> Optional<?> getResponseFromType(Type typeOfT) throws IOException {
		try {
			checkPoints.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_STARTED.name())
					.time(System.currentTimeMillis()).build());
			URL url = new URL(constructUrlWithQueryParams());
			HttpURLConnection httpUrlCon = getUrlConnection(url, proxy);
			setHeaderParams(httpUrlCon);
			setURLConnection(httpUrlCon);
			httpUrlCon.connect();
			int a = HttpURLConnection.HTTP_BAD_REQUEST;
			T response = null;
			boolean redirect = false;
			// normally, 3xx is redirect
			int status = httpUrlCon.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP
					|| status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
				redirect = true;
			}
			if (redirect) {
				String newUrl = httpUrlCon.getHeaderField("Location");
				httpUrlCon = getUrlConnection(new URL(newUrl), proxy);
				setHeaderParams(httpUrlCon);
			}
			responseHeaderParams.put("status",
					new ArrayList<>(Arrays.asList(String.valueOf(httpUrlCon.getResponseCode()))));
			responseHeaderParams.putAll(httpUrlCon.getHeaderFields());
			responseString = getContent(httpUrlCon);

			if (BooleanUtils.isNotFalse(printResponseLog)) {
				log.debug("Response String is " + responseString);
			}
			if (typeOfT == null) {
				response = (T) responseString;
				return Optional.ofNullable(response);
			}

			response = GsonUtils.getGson().fromJson(responseString, typeOfT);
			return Optional.ofNullable(response);
		} catch (IOException e) {
			responseString = e.getMessage();
			throw e;
		} catch (Exception e) {
			log.error("Unable to retrieve response for url {}, postData {}, responseString {}", urlString, postData, responseString, e);
		} finally {
			checkPoints.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_FINISHED.name())
					.time(System.currentTimeMillis()).build());
		}
		return Optional.empty();
	}

	/**
	 * This will convert response into typeT only if response is json, In case typeT is null , It will response response
	 * as a String
	 *
	 * @param typeOfT
	 * @return Object of typeT if response is json
	 */
	@SuppressWarnings("unchecked")
	public <T> Optional<T> getResponse(Class<T> typeOfT) throws IOException {
		try {
			checkPoints.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_STARTED.name())
					.time(System.currentTimeMillis()).build());
			URL url = new URL(constructUrlWithQueryParams());
			HttpURLConnection httpUrlCon = getUrlConnection(url, proxy);
			setHeaderParams(httpUrlCon);
			setURLConnection(httpUrlCon);
			httpUrlCon.connect();

			T response = null;
			boolean redirect = false;
			// normally, 3xx is redirect
			int status = httpUrlCon.getResponseCode();
			if (status != HttpURLConnection.HTTP_OK && (status == HttpURLConnection.HTTP_MOVED_TEMP
					|| status == HttpURLConnection.HTTP_MOVED_PERM || status == HttpURLConnection.HTTP_SEE_OTHER)) {
				redirect = true;
			}
			if (redirect) {
				String newUrl = httpUrlCon.getHeaderField("Location");
				httpUrlCon = getUrlConnection(new URL(newUrl), proxy);
				setHeaderParams(httpUrlCon);
			}
			responseHeaderParams.put("status",
					new ArrayList<>(Arrays.asList(String.valueOf(httpUrlCon.getResponseCode()))));
			responseHeaderParams.putAll(httpUrlCon.getHeaderFields());
			responseString = getContent(httpUrlCon);
			if (BooleanUtils.isNotFalse(printResponseLog)) {
				log.debug("Response String is " + responseString);
			}
			if (typeOfT == null) {
				response = (T) responseString;
				return Optional.ofNullable(response);
			}
			response = GsonUtils.getGson().fromJson(responseString, typeOfT);
			return Optional.ofNullable(response);
		} catch (IOException e) {
			responseString = e.getMessage();
			throw e;
		} catch (Exception e) {
			log.error("Unable to retrieve response for url {}, postData {}, responseString {}", urlString, postData,
					responseString, e);
		} finally {
			checkPoints.add(CheckPointData.builder().type(SystemCheckPoint.EXTERNAL_API_FINISHED.name())
					.time(System.currentTimeMillis()).build());
		}
		return Optional.empty();
	}

	public HttpURLConnection getUrlConnection(URL url, Proxy proxy) throws Exception {
		if (proxy != null) {
			return (HttpURLConnection) url.openConnection(proxy);
		} else {
			return (HttpURLConnection) url.openConnection();
		}
	}

	private HttpURLConnection setURLConnection(HttpURLConnection httpUrlCon) throws Exception {
		if (timeout > 0) {
			httpUrlCon.setReadTimeout(timeout);
		}
		httpUrlCon.setDoInput(true);
		httpUrlCon.setInstanceFollowRedirects(true);
		if (StringUtils.isNotBlank(postData)) {
			httpUrlCon.setRequestMethod(REQ_METHOD_POST);
			setPostDataForUrlCon(httpUrlCon, postData);
		} else if (StringUtils.isNotBlank(putData)) {
			httpUrlCon.setRequestMethod(REQ_METHOD_PUT);
			setPostDataForUrlCon(httpUrlCon, putData);
		} else if (requestMethod != null) {
			httpUrlCon.setRequestMethod(requestMethod);
		}
		return httpUrlCon;
	}

	public void setPostDataForUrlCon(URLConnection conn, String postData) throws Exception {
		conn.setDoOutput(true);
		OutputStream outStream = conn.getOutputStream();
		OutputStreamWriter osw = new OutputStreamWriter(outStream);
		osw.write(postData.toString(), 0, postData.toString().length());
		osw.flush();
		osw.close();
	}

	private void setHeaderParams(HttpURLConnection httpUrlCon) {
		if (null == headerParams || headerParams.isEmpty()) {
			return;
		}
		Set<String> keys = headerParams.keySet();

		for (String key : keys) {
			httpUrlCon.setRequestProperty(key, headerParams.get(key));
		}
	}

	@SuppressWarnings("unchecked")
	public String constructUrlWithQueryParams() {
		if (null == recurringQueryParams || recurringQueryParams.isEmpty()) {
			return urlString;
		}

		Set<String> keys = recurringQueryParams.keySet();
		StringBuilder query = new StringBuilder();
		for (String key : keys) {
			try {
				if (recurringQueryParams.get(key) == null)
					continue;
				List<String> recurringQueryParamValues = (ArrayList<String>) recurringQueryParams.get(key);
				for (String queryParamValue : recurringQueryParamValues) {
					if (query.length() > 0)
						query.append("&");
					query.append(key).append("=").append(URLEncoder.encode(queryParamValue, "UTF-8"));
				}
			} catch (UnsupportedEncodingException e) {
				log.error("Unable to encode the query parameter with value {} ", recurringQueryParams.get(key));
				throw new RuntimeException(
						"Unable to encode the query parameter with value : " + recurringQueryParams.get(key));
			}
		}
		log.debug("URL generated is {}", urlString + "?" + query);
		urlString = urlString + "?" + query;
		return urlString;
	}

	public String getContent(URLConnection urlCon) throws IOException {
		if (urlCon == null) {
			return null;
		}
		HttpURLConnection httpUrlCon = (HttpURLConnection) urlCon;
		String encoding = urlCon.getContentEncoding();
		InputStream ip = null;
		StringBuffer strBuf = new StringBuffer();
		try {
			if (httpUrlCon.getResponseCode() >= HttpURLConnection.HTTP_BAD_REQUEST
					&& httpUrlCon.getResponseCode() <= HttpURLConnection.HTTP_INTERNAL_ERROR) {
				if (encoding != null && encoding.equalsIgnoreCase("gzip")) {
					ip = new GZIPInputStream(new BufferedInputStream(httpUrlCon.getErrorStream()));
				} else {
					ip = new BufferedInputStream(httpUrlCon.getErrorStream());
				}
			} else if ((encoding != null && encoding.equalsIgnoreCase("gzip"))
					|| BooleanUtils.isTrue(isGzippedResponse)) {
				ip = new GZIPInputStream(new BufferedInputStream(urlCon.getInputStream()));
			} else {
				ip = new BufferedInputStream(urlCon.getInputStream());
			}

			byte[] b = new byte[4096];
			for (int n = 0; (n = ip.read(b)) != -1;) {
				strBuf.append(new String(b, 0, n));
			}
		} finally {
			try {
				if (ip != null) {
					ip.close();
				}
			} catch (Exception e) {
				log.error("Not able to close buuferred stream" + e.getMessage());
			}
		}

		return strBuf.toString();
	}

	public static String getValueFromCookie(HttpServletRequest request, String cookieName) {
		String rawCookies = request.getHeader("cookie");
		if (!StringUtils.isBlank(rawCookies)) {
			String[] rawCookieParams = rawCookies.split(";");
			for (String rawCookieParam : rawCookieParams) {
				String[] cookieKeyValuePair = rawCookieParam.split("=");
				if (cookieName.equalsIgnoreCase(cookieKeyValuePair[0].trim())) {
					try {
						return URLDecoder.decode(cookieKeyValuePair[1].trim(), "UTF-8");
					} catch (Exception e) {
						log.error("Note able to decode cookie {} due to", cookieKeyValuePair[1], e);
					}
				}
			}
		}

		String val = null;
		Cookie[] cookiesArray = request.getCookies();
		if (cookiesArray != null) {
			for (Cookie cookie : cookiesArray) {
				if (cookieName.equals(cookie.getName())) {
					val = cookie.getValue();
				}
			}
		}
		try {
			if (val != null) {
				val = URLDecoder.decode(val, "UTF-8");
			}
		} catch (Exception e) {
			log.error("Note able to decode cookie {} due to", val, e);
		}
		return val;
	}

	public static HttpHeader buildHttpHeader(HttpServletRequest request) {
		UserAgent uA = UserAgent.parseUserAgentString(request.getHeader("User-Agent"));
		ChannelType channelType = (request.getHeader(HEADER_CHANNEL) != null)
				? ChannelType.getChannelType(request.getHeader(HEADER_CHANNEL))
				: null;
		String browserName = request.getHeader(HEADER_BROWSER) != null ? request.getHeader(HEADER_BROWSER)
				: uA.getBrowser().getName();
		String browserVersion =
				request.getHeader(HEADER_BROWSER_VERSION) != null ? request.getHeader(HEADER_BROWSER_VERSION)
						: (uA.getBrowserVersion() != null ? uA.getBrowserVersion().getVersion() : "Un-known");

		PaymentHeader payHeader = null;
		if (request.getHeader("payHeader") != null) {
			payHeader = GsonUtils.getGson().fromJson(request.getHeader("payHeader"), PaymentHeader.class);
		}

		String partnerId = request.getHeader("whiteLabel");
        if(StringUtils.isEmpty(partnerId))
        	partnerId = "0";
        
		return HttpHeader.builder().browserVersion(browserVersion).browser(browserName)
				.ip(StringUtils.isBlank(request.getHeader("X-Forwarded-For")) ? request.getRemoteAddr()
						: request.getHeader("X-Forwarded-For"))
				.device(uA.getOperatingSystem().getDeviceType().getName()).channelType(channelType).payHeader(payHeader)
				.currEnv(request.getHeader("currEnv")).partnerId(partnerId).build();
	}

	public static Map<String, Object> getRequestParams(HttpServletRequest request) {
		Map<String, Object> requestMap = new HashMap<>();
		Enumeration<String> enumeration = request.getParameterNames();
		while (enumeration.hasMoreElements()) {
			String parameterName = enumeration.nextElement();
			requestMap.put(parameterName, request.getParameter(parameterName));
		}
		return requestMap;

	}

}
