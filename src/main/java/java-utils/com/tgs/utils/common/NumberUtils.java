package com.tgs.utils.common;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

	/**
	 * While converting double to string, consider only 2 digits after the decimal
	 */
	public static String convertDoubleToString(double d) {
		BigDecimal bd = BigDecimal.valueOf(d);
		bd = bd.setScale(2, RoundingMode.HALF_UP);
		return bd.toString();
	}

	/**
	 * Often due to format issues, double (however same) values don't match. It's better to use {@code BigDecimal} for
	 * comparison.
	 * 
	 * @param d1 double value to be compared
	 * @param d2 double value to which {@code d1} is to be compared
	 * @return -1, 0, or 1 as {@code d1} is numerically less than, equal to, or greater than {@code d2}.
	 */
	public static int compareDoubleUpto2Decimal(double d1, double d2) {
		return compareDouble(d1, d2, 2);
	}

	/**
	 * Often due to format issues, double (however same) values don't match. It's better to use {@code BigDecimal} for
	 * comparison.
	 * 
	 * @param d1 double value to be compared
	 * @param d2 double value to which {@code d1} is to be compared
	 * @param scale
	 * @return -1, 0, or 1 as {@code d1} is numerically less than, equal to, or greater than {@code d2}.
	 */
	public static int compareDouble(double d1, double d2, int scale) {
		return BigDecimal.valueOf(d1).setScale(scale, BigDecimal.ROUND_HALF_EVEN)
				.compareTo(BigDecimal.valueOf(d2).setScale(scale, BigDecimal.ROUND_HALF_EVEN));
	}

	/**
	 * 
	 * @param number
	 * @param scale
	 * @return {@code number} rounded off to {@code scale} using {@code BigDecimal.ROUND_HALF_EVEN} mode. Returns
	 *         {@code null} if {@code number} is {@code null}.
	 */
	public static BigDecimal roundOff(BigDecimal number, int scale) {
		if (number == null) {
			return number;
		}
		return number.setScale(scale, BigDecimal.ROUND_HALF_EVEN);
	}
}
