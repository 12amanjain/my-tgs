package com.tgs.utils.common;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.tgs.services.base.helper.MaskedField;
import com.tgs.services.base.utils.ApplicationConstant;

public class UpdateMaskedField {

	public static void update(Object obj) throws IllegalArgumentException, IllegalAccessException {
		if (obj == null) {
			return;
		}
		Class<?> clazz = obj.getClass();

		if (clazz.isEnum()) {
			return;
		}

		if (clazz.getPackage().getName().startsWith(ApplicationConstant.ADAPTABLE_PACKAGE)) {
			for (Field f : clazz.getDeclaredFields()) {
				f.setAccessible(true);
				MaskedField maskedFieldAnnotation = f.getAnnotation(MaskedField.class);
				if (maskedFieldAnnotation != null && f.getType().isAssignableFrom(String.class)) {
					// check whether the field is masked or not
					// if it is masked , set it to null otherwise don't change
					char maskingChar = maskedFieldAnnotation.maskingChar();
					String value = (String) f.get(obj);

					// check this string
					if (Objects.nonNull(value)) {
						Boolean alreadyMasked = isAlreadyMasked(value, maskingChar);
						if (alreadyMasked) {
							f.set(obj, null);
						}
					}

				}
				Object fieldVal = f.get(obj);
				update(fieldVal);
			}

		}
		if (Collection.class.isAssignableFrom(clazz)) {
			for (Object item : (Collection) obj) {
				update(item);
			}
		}

		if (Map.class.isAssignableFrom(clazz)) {
			for (Object item : ((Map) obj).values()) {
				update(item);
			}
		}
		
	}

	/**
	 * This Method checks First 3 chars of the field, if they all matched with the
	 * masking characters then it returns true else returns false, Logic may be
	 * changed in future.
	 * 
	 * @param value
	 * @param maskingChar
	 * @return
	 */
	public static Boolean isAlreadyMasked(String value, char maskingChar) {
		if (StringUtils.isNotBlank(value)) {
			value = value.substring(0, 3);
			for (int i = 0; i < value.length(); i++) {
				if (value.charAt(i) != maskingChar) {
					return false;
				}
			}
			return true;
		}
		return false;
	}
}
