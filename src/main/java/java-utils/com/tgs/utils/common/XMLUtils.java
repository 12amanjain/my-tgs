package com.tgs.utils.common;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.xml.security.utils.XalanXPathAPI;
import org.apache.xml.serializer.OutputPropertiesFactory;
import org.apache.xml.serializer.SerializerFactory;
import org.apache.xml.serializer.Serializer;
import org.w3c.dom.*;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;


@Slf4j
public class XMLUtils {

    public static Node getNode(Node contextNode, String xpath) {
        try {
            XalanXPathAPI xalanXPathAPI = new XalanXPathAPI();
            NodeList nodeList = xalanXPathAPI.selectNodeList(contextNode, contextNode, xpath, contextNode);
            if (nodeList != null) {
                return nodeList.item(0);
            }
            //return XPathAPI.selectSingleNode(contextNode, xpath);
        } catch (Exception e) {
            return null;
        }
        return null;
    }


    public static String getAttributesFromElement(Node n) {
        String attrList = "";
        Element el = (Element) n;
        NamedNodeMap nm = el.getAttributes();
        if (nm != null && nm.getLength() > 0) {
            for (int i = 0; i < nm.getLength(); i++) {
                attrList += nm.item(i).getNodeName() + " = " + nm.item(i).getNodeValue() + "<br>";
            }
        }
        return attrList;
    }

    public static String getAttrributeValue(Node n, String name) {
        String attr = "";

        if (n == null)
            return attr;

        Element el = (Element) n;
        attr = el.getAttribute(name);

        if (attr == null)
            attr = "";

        return attr;
    }

	public static Node getNode(Document doc, String xpath) {
		if (doc != null) {
			return getNode(doc.getDocumentElement(), xpath);
		}
		return null;
	}

    public static NodeList getNodeList(Node contextNode, String xpath) {
        NodeList nodeList = null;
        try {
            XalanXPathAPI xalanXPathAPI = new XalanXPathAPI();
            nodeList = xalanXPathAPI.selectNodeList(contextNode, contextNode, xpath, contextNode);
        } catch (Exception e) {
            return nodeList;
        }
        return nodeList;
    }

    public static NodeList getNodeList(Document doc, String xpath) {
        return getNodeList(doc.getDocumentElement(), xpath);
    }

    public static String convertToString(Document doc) throws Exception {
        StringWriter sw = new StringWriter();
        try {
            org.apache.xml.serialize.OutputFormat of = new org.apache.xml.serialize.OutputFormat();
            of.setIndenting(true);
            org.apache.xml.serialize.XMLSerializer s = new org.apache.xml.serialize.XMLSerializer(sw, of);
            s.serialize(doc);
            return sw.toString();
        } catch (Exception e) {
            throw e;
        } finally {
            if (sw != null)
                sw.close();
        }
    }

    public static String convertToString(Node n) {
        try {
            StringWriter swout = new StringWriter();
            PrintWriter out = new PrintWriter(swout);
            Serializer serializer = SerializerFactory.getSerializer(OutputPropertiesFactory
                    .getDefaultMethodProperties("xml"));
            serializer.setWriter(out);
            serializer.asDOMSerializer().serialize(n);
            return swout.toString();
        } catch (Exception e) {
        }
        return null;
    }

    public static Document parseXml(String xml, boolean flag) {
        try {
            InputSource in = new InputSource(new java.io.StringReader(xml));
            DocumentBuilderFactory dfactory = DocumentBuilderFactory.newInstance();
            dfactory.setNamespaceAware(true);
            Document doc = dfactory.newDocumentBuilder().parse(in);
            return doc;
        } catch (Exception e) {
            log.error("Unable to parse XML ", e);
        }
        return null;
    }

    public static String getElementValue(Node n) {
        if (null == n)
            return "";
        else {
            if (n.getFirstChild() != null)
                return n.getFirstChild().getNodeValue();
            else
                return "";
        }
    }

    public static void setElementValue(Node n, String value) {
        if (n != null && n.getFirstChild() != null)
            n.getFirstChild().setNodeValue(value);
        else
            createTextNode(n, value);
    }

    public static boolean isNull(Object obj) {
        return (obj == null);
    }


    public static void transformXML(Document source, Document dest, String sourceXpath, String destXpath)
            throws Exception {
        Node destNode = null;
        String sourceValue = null;
        String attrName = null;
        boolean isAttr = false;
        // printDocument(source,null);
        Node sourceNode = getNode(source, sourceXpath);
        if (isNull(sourceNode))
            return;
        if (destXpath.indexOf("@") != -1) {
            // System.out.println(destXpath.substring(0,destXpath.indexOf("@")-1
            // ));
            attrName = destXpath.substring(destXpath.indexOf("@") + 1, destXpath.length());
            destXpath = destXpath.substring(0, destXpath.indexOf("@") - 1);
            destNode = getNode(dest, destXpath);
            isAttr = true;
        } else
            destNode = getNode(dest, destXpath);
        if (isNull(destNode))
            return;
        sourceValue = getElementValue(sourceNode);
        if (isNull(sourceValue))
            return;
        if (isAttr)
            ((Element) destNode).setAttribute(attrName, sourceValue);
        else
            setElementValue(destNode, sourceValue);
    }

    public static void createTextNode(Node refElement, String value) {
        Document doc = refElement.getOwnerDocument();
        Node valueNode = doc.createTextNode(value);
        refElement.appendChild(valueNode);
    }

    public static Element appendNewElement(Element refElement, String tagName, String value) {
        Document doc = refElement.getOwnerDocument();
        Element newElement = doc.createElement(tagName);
        doc.createTextNode(value);
        refElement.appendChild(newElement);
        return newElement;
    }

    public static Element getRootElement(Element refElement) {
        Element parent = refElement;
        while (parent.getParentNode() instanceof Element)
            parent = (Element) parent.getParentNode();
        return parent;
    }

    public static Document createDocumentFromString(String str) {
        DocumentBuilderFactory factory = null;
        Document d = null;
        try {
            if (StringUtils.isNotBlank(str)) {
                factory = DocumentBuilderFactory.newInstance();
                d = factory.newDocumentBuilder().parse(new InputSource(new java.io.StringReader(str)));
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.error("Unable to create document for {}", str);
        }
        return d;
    }

    /**
     * Finds the XPath for a node.
     */
    private static StringBuffer iterateBack(Node node, StringBuffer buffer) {
        int nthChild = 1;
        String nodeName = node.getNodeName();
        Node prevNode = node;
        while ((prevNode = prevNode.getPreviousSibling()) != null) {
            if (prevNode.getNodeName().equals(nodeName) && prevNode.getNodeType() == node.getNodeType()) {
                nthChild++;
            }
        }
        if (node.getNodeType() == Node.TEXT_NODE) {
            nodeName = "text()";
        } else {
            if (node.getNodeType() == Node.DOCUMENT_NODE) {
                nodeName = "";
            }
        }
        buffer.insert(0, "/" + nodeName + (nodeName.length() > 0 ? "[" + nthChild + "]" : ""));
        node = node.getParentNode();
        if (node != null && node.getNodeType() != Node.DOCUMENT_NODE) {
            buffer = iterateBack(node, buffer);
        }
        return buffer;
    }

    public static String xpathForNode(Node node, String attrName) {
        String finalXpath = "";
        try {
            StringBuffer xpath = new StringBuffer();
            xpath = iterateBack(node, xpath);
            if (attrName != null && !"".equals(attrName.trim()))
                finalXpath = xpath.toString() + "/@" + attrName;
            else
                finalXpath = xpath.toString();
            return finalXpath;
        } catch (Exception e) {
            return "error calculating xpath";
        }
    }

}
