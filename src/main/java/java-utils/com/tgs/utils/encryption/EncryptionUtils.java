package com.tgs.utils.encryption;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.lang3.StringUtils;
import com.amazonaws.services.kms.AWSKMS;
import com.amazonaws.services.kms.AWSKMSClientBuilder;
import com.amazonaws.services.kms.model.DecryptRequest;
import com.amazonaws.services.kms.model.EncryptRequest;
import com.amazonaws.util.Base64;
import com.tgs.services.base.datamodel.EncryptionData;

public class EncryptionUtils {

	private static int pswdIterations = 65536;
	private static int keySize = 256;
	private final static byte[] ivBytes = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

	public static String encryptUsingKMS(String plainText, String keyId) {
		if (StringUtils.isAllBlank(keyId)) {
			return plainText;
		}
		AWSKMS kmsClient = AWSKMSClientBuilder.defaultClient();
		ByteBuffer plainTextByteBuffer = ByteBuffer.wrap(plainText.getBytes());
		EncryptRequest req = new EncryptRequest().withKeyId(keyId).withPlaintext(plainTextByteBuffer);
		ByteBuffer ciphertext = kmsClient.encrypt(req).getCiphertextBlob();
		byte[] base64EncodedValue = Base64.encode(ciphertext.array());
		String encryptedValue = new String(base64EncodedValue, Charset.forName("UTF-8"));
		return encryptedValue;
	}

	public static String getString(ByteBuffer b) {
		byte[] byteArray = new byte[b.remaining()];
		b.get(byteArray);
		return new String(byteArray);
	}

	public static String decryptUsingKMS(String encyptedValue, String keyId) {
		if (StringUtils.isAllBlank(keyId)) {
			return encyptedValue;
		}
		AWSKMS kmsClient = AWSKMSClientBuilder.defaultClient();
		byte[] base64EncodedValue = Base64.decode(encyptedValue);
		ByteBuffer ciphertextBlob = ByteBuffer.wrap(base64EncodedValue);
		DecryptRequest req = new DecryptRequest().withCiphertextBlob(ciphertextBlob);
		String plainText = getString(kmsClient.decrypt(req).getPlaintext());
		return plainText;
	}

	public static void main(String[] args) {
		// System.out.println(encryptUsingKMS("ashu",
		// "arn:aws:kms:ap-south-1:766916178229:key/7d86ed5c-ac21-4411-b22b-a47a2defd372"));
		// System.out.println(encryptUsingKMS("ashu",
		// "arn:aws:kms:ap-south-1:766916178229:key/7d86ed5c-ac21-4411-b22b-a47a2defd372"));
		System.out.println(decryptUsingKMS(
				"AQICAHh+oTNTPCwA7YYoO+KastkZV0+hn89X1BztsPjXuAoRFAFfji8IjMyVl4rYdZ8A/99rAAAAajBoBgkqhkiG9w0BBwagWzBZAgEAMFQGCSqGSIb3DQEHATAeBglghkgBZQMEAS4wEQQMsYmhK6IF/0ZsHkzSAgEQgCeMyLfrNwdRvlBse0yhN5oUYTw1GDQMa6SkwNHrkK448QhV4e6UTcQ=",
				"arn:aws:kms:ap-south-1:766916178229:key/7d86ed5c-ac21-4411-b22b-a47a2defd372"));
	}

	/**
	 * @param type - type SHA-512 is used to encode the params
	 * @param str - field which required encoding
	 * @return string - encoded string based on key
	 * @throws NoSuchAlgorithmException
	 */
	public static String encryptPostData(String type, String str) throws NoSuchAlgorithmException {
		byte[] hashseq = str.getBytes();
		StringBuffer hexString = new StringBuffer();
		MessageDigest algorithm = MessageDigest.getInstance(type);
		algorithm.reset();
		algorithm.update(hashseq);
		byte messageDigest[] = algorithm.digest();
		for (int i = 0; i < messageDigest.length; i++) {
			String hex = Integer.toHexString(0xFF & messageDigest[i]);
			if (hex.length() == 1)
				hexString.append("0");
			hexString.append(hex);
		}
		return hexString.toString();

	}

	public static String encryptData(String data, EncryptionData encryptionData) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		String key = encryptionData.getAesKey();
		/**
		 * Temporarily fixing encryption/decryption for empty value received from UI.
		 */
		if (data == null || StringUtils.isEmpty(data) || StringUtils.isBlank(key)) {
			return data;
		}
		byte[] encryptedDataBytes = encryptUsingAES(key, data);
		return java.util.Base64.getEncoder().encodeToString(encryptedDataBytes);
	}

	public static String decryptData(String data, EncryptionData encryptionData) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {
		String key = encryptionData.getAesKey();
		if (data == null || StringUtils.isEmpty(data) || StringUtils.isBlank(key)) {
			return data;
		}
		byte[] encryptedDataBytes = java.util.Base64.getDecoder().decode(data);
		return decryptUsingAES(key, encryptedDataBytes);
	}

	/**
	 * 
	 * @param key
	 * @param encryptedBytes
	 * @return decrypted data on successful decryption. Otherwise, null.
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
	public static String decryptUsingAES(String key, byte[] encryptedBytes) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		if (StringUtils.isBlank(key)) {
			return new String(encryptedBytes);
		}
		byte[] bytes = key.getBytes();
		SecretKeySpec keySpec = new SecretKeySpec(bytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, keySpec);
		byte[] decryptedBytes = cipher.doFinal(encryptedBytes);
		return new String(decryptedBytes);
	}

	/**
	 * 
	 * @param key
	 * @param value
	 * @return encrypted data on successful encryption. Otherwise, null.
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */

	public static String encryptUsingAES(String plainText, String key, String salt) throws Exception {
		byte[] saltBytes = salt.getBytes("UTF-8");
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec spec = new PBEKeySpec(key.toCharArray(), saltBytes, pswdIterations, keySize);
		SecretKey secretKey = factory.generateSecret(spec);
		IvParameterSpec localIvParameterSpec = new IvParameterSpec(ivBytes);
		SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(1, secret, localIvParameterSpec);
		byte[] encryptedTextBytes = cipher.doFinal(plainText.getBytes("UTF-8"));
		return byteToHexString(encryptedTextBytes);
	}

	public static String decryptUsingAES(String encryptedText, String key, String salt) throws Exception {
		byte[] saltBytes = salt.getBytes("UTF-8");
		byte[] encryptedTextBytes = hex2ByteArray(encryptedText);
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
		PBEKeySpec spec = new PBEKeySpec(key.toCharArray(), saltBytes, pswdIterations, keySize);
		SecretKey secretKey = factory.generateSecret(spec);
		SecretKeySpec secret = new SecretKeySpec(secretKey.getEncoded(), "AES");
		IvParameterSpec localIvParameterSpec = new IvParameterSpec(ivBytes);
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(2, secret, localIvParameterSpec);
		byte[] decryptedTextBytes = null;
		decryptedTextBytes = cipher.doFinal(encryptedTextBytes);
		return new String(decryptedTextBytes);
	}

	private static byte[] hex2ByteArray(String sHexData) {
		byte[] rawData = new byte[sHexData.length() / 2];
		for (int i = 0; i < rawData.length; ++i) {
			int index = i * 2;
			int v = Integer.parseInt(sHexData.substring(index, index + 2), 16);
			rawData[i] = (byte) v;
		}
		return rawData;
	}

	public static byte[] encryptUsingAES(String key, String value) throws NoSuchAlgorithmException,
			NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		byte[] bytes = key.getBytes();
		SecretKeySpec keySpec = new SecretKeySpec(bytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec);
		byte[] encryptedBytes = cipher.doFinal(value.getBytes());
		return encryptedBytes;
	}

	public static String getEncodedValueUsingSHA512(String hashKey, String inputString)
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		if (StringUtils.isNotBlank(inputString)) {
			MessageDigest messageDigest = MessageDigest.getInstance("SHA-512");
			byte[] message = messageDigest.digest(inputString.getBytes());
			return byteToHexString(message);
		}
		return null;
	}

	public static String getValueEncodedWithSha2(String hashKey, String str)
			throws InvalidKeyException, NoSuchAlgorithmException, UnsupportedEncodingException {
		return byteToHexString(encryptUsingHMACSHA2(str, hashKey));
	}

	/**
	 * Hashing using key with HMACSHA512<br>
	 * Logging sensitive data is prohibited.
	 */
	public static byte[] encryptUsingHMACSHA2(String text, String keyString)
			throws java.security.NoSuchAlgorithmException, java.security.InvalidKeyException,
			java.io.UnsupportedEncodingException {

		java.security.Key sk = new javax.crypto.spec.SecretKeySpec(keyString.getBytes("UTF-8"), "HMACSHA512");
		javax.crypto.Mac mac = javax.crypto.Mac.getInstance(sk.getAlgorithm());
		mac.init(sk);

		byte[] hmac = mac.doFinal(text.getBytes("UTF-8"));

		return hmac;
	}

	public static byte[] encryptUsingHMACSHA256(String text, String keyString)
			throws java.security.NoSuchAlgorithmException, java.security.InvalidKeyException,
			java.io.UnsupportedEncodingException {
		java.security.Key sk = new javax.crypto.spec.SecretKeySpec(keyString.getBytes("UTF-8"), "HmacSHA256");
		javax.crypto.Mac mac = javax.crypto.Mac.getInstance(sk.getAlgorithm());
		mac.init(sk);

		byte[] hmac = mac.doFinal(text.getBytes("UTF-8"));

		return hmac;
	}

	/**
	 * Convert from byte array to HexString
	 * 
	 * @param byData
	 * @return
	 */
	public static String byteToHexString(byte byData[]) {
		StringBuilder sb = new StringBuilder(byData.length * 2);

		for (int i = 0; i < byData.length; i++) {
			int v = byData[i] & 0xff;
			if (v < 16)
				sb.append('0');
			sb.append(Integer.toHexString(v));
		}

		return sb.toString();
	}

	public static String encryptUsingCBC(String key, String value) throws Exception {
		IvParameterSpec iv = new IvParameterSpec(new byte[16]);
		byte[] bytes = key.getBytes();
		SecretKeySpec keySpec = new SecretKeySpec(bytes, "AES");
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, keySpec, iv);
		byte[] encryptedBytes = cipher.doFinal(value.getBytes());
		return java.util.Base64.getEncoder().encodeToString(encryptedBytes);
	}
}

