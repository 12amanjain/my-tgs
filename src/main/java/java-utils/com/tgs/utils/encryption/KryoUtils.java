package com.tgs.utils.encryption;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;

import org.objenesis.strategy.StdInstantiatorStrategy;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.esotericsoftware.kryo.pool.KryoFactory;
import com.esotericsoftware.kryo.pool.KryoPool;

public class KryoUtils {

	private KryoUtils() {
	}

	private static KryoFactory factory = () -> {
		Kryo kryo = new Kryo();
		kryo.setInstantiatorStrategy(new Kryo.DefaultInstantiatorStrategy(new StdInstantiatorStrategy()));
		kryo.register(Arrays.asList().getClass(), new ArraysAsListSerializer());
		return kryo;
	};

	private static KryoPool pool = new KryoPool.Builder(factory).softReferences().build();

	public static <T> Optional<byte[]> serialize(T obj) {
		return serialize(obj, false);
	}

	/**
	 * This will use kryoPool as creating a new instance of kryo is heavier
	 *
	 * @param obj
	 *            of any type
	 * @return it will return serialize byte array
	 */
	public static <T> Optional<byte[]> serialize(T obj, boolean compress) {
		if (Objects.isNull(obj)) {
			return Optional.empty();
		}

		return pool.run((kryo) -> {
			ByteArrayOutputStream bos = new ByteArrayOutputStream(16384);
			if (compress) {
				DeflaterOutputStream deflaterOutputStream = new DeflaterOutputStream(bos);
				Output output = new Output(deflaterOutputStream);
				kryo.writeObject(output, obj);
				output.close();
				return Optional.of(bos.toByteArray());
			} else {
				Output output = new Output(bos);
				kryo.writeObject(output, obj);
				output.close();
				return Optional.of(bos.toByteArray());
			}
		});

	}

	public static <T> Optional<T> deserialize(byte[] buff, Class<T> classType) {
		return deserialize(buff, classType, false);
	}

	/**
	 * This will use kryoPool as creating a new instance of kryo is heavier
	 *
	 * @param buff
	 *            byteArray of object to get
	 * @param classType
	 *            class type of object to get
	 * @return This will deserialize object and return object of classType
	 */

	public static <T> Optional<T> deserialize(byte[] buff, Class<T> classType, boolean compress) {
		if (Objects.isNull(buff)) {
			return Optional.empty();
		}
		return pool.run((kryo) -> {
			Input kInput = null;
			if (compress) {
				kInput = new Input(new InflaterInputStream(new ByteArrayInputStream(buff)));
			} else {
				kInput = new Input(new ByteArrayInputStream(buff));
			}
			T obj = kryo.readObject(kInput, classType);
			kInput.close();
			return Optional.of(obj);
		});

	}

	public static <T> T copy(T obj) {
		return pool.run((kryo) -> {
			return kryo.copy(obj);
		});
	}
}
