package com.tgs.utils.exception;

import lombok.Getter;

/**
 * This class represents errors that can occur in caching layer. It encapsulates error code and description for them.
 * These errors are identified by error codes.<br>
 * <br>
 * Implementation to avoid same error-code for two errors can be written.
 * 
 * @author Abhineet Kumar, Technogram Solutions
 *
 */
@Getter
public enum CachingLayerError {

	GENERAL_ERROR(0, "An error occured."),
	WRITE_ERROR(1, "Failed to write"),
	DUPLICATE_KEY(2, "Key already exists."),
	READ_ERROR(3, "Failed to read."),
	NULL_FILTER(4, "Filter can't be null"),
	BLANK_KEY(5, "Blank Keys are not allowed"),
	KEY_NOT_FOUND_ERROR(6, "Key Not found");

	private int errorCode;
	private String message;

	private CachingLayerError(int errorCode, String message) {
		this.errorCode = errorCode;
		this.message = message;
	}
}
