package com.tgs.utils.exception;

import lombok.Getter;

/**
 * Exception expected to have been thrown by caching layer. It is designed
 * around error descriptions which are maintained in
 * <code>enum CachingLayerError</code>. These exceptions/errors are identified
 * by error codes.<br>
 * <br>
 * It is designed strictly for errors enumerated in that enum.
 * 
 * @author Abhineet Kumar, Technogram Solutions
 *
 */
@Getter
public class CachingLayerException extends RuntimeException {

	private static final long serialVersionUID = 2L;

	private int errorCode;

	private CachingLayerException(int errorCode, String message) {
		super(message);
		this.errorCode = errorCode;
	}

	/**
	 * Creates a <code>CachingLayerException</code> instance using code and
	 * description contained in <code>CachingLayerError</code> parameter.
	 * 
	 * @param error Error description
	 */
	public CachingLayerException(CachingLayerError error) {
		this(error.getErrorCode(), error.getMessage());
	}

	/**
	 * Creates a <code>CachingLayerException</code> instance using code and
	 * description contained in <code>CachingLayerError.GENERAL_ERROR</code>.
	 * 
	 * @param error Error description
	 */
	public CachingLayerException() {
		this(CachingLayerError.GENERAL_ERROR);
	}

	/**
	 * Checks whether this exception was caused by the suspected error.
	 * 
	 * @param error Suspected error
	 * @return <code>true</code> if this exception was caused by the given error.
	 *         Otherwise, <code>false</code>.
	 */
	public boolean isCausedBy(CachingLayerError error) {
		if (error == null)
			return false;
		return error.getErrorCode() == errorCode;
	}
}
