package com.tgs.utils.exception;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;

public class CreditModuleException extends CustomGeneralException {

    private static final long serialVersionUID = 1L;

    public CreditModuleException(SystemError error) {
        super(error);
    }

    public CreditModuleException(String message) {
        super(message);
    }
}
