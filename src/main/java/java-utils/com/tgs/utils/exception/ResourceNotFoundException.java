package com.tgs.utils.exception;

import com.tgs.services.base.helper.SystemError;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ResourceNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private SystemError error;

	public ResourceNotFoundException(SystemError error) {
		this.error = error;
	}

	public ResourceNotFoundException(SystemError error, String message) {
		super(message);
		this.error = error;
	}

}
