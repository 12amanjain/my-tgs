package com.tgs.utils.exception;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.helper.SystemError;

public class TgsSecurityException extends CustomGeneralException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2304886716530068060L;

	public TgsSecurityException(Throwable cause, SystemError error, Object... args) {
		super(error, error.getMessage(args));
		initCause(cause);
	}

}
