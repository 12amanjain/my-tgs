package com.tgs.utils.exception.air;

@SuppressWarnings("serial")
public class AirSearchException extends RuntimeException {

    private static final long serialVersionUID = 2L;

    public AirSearchException(String message) {
        super(message);
    }

    public AirSearchException(Exception e) {
        super(e);
    }
}

