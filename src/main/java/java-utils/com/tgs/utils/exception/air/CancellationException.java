package com.tgs.utils.exception.air;

public class CancellationException extends RuntimeException {

	public static String PARTIAL_CANCELATTION = "Partial Cancellation Not Allowed ";
	/**
	 * This is to capture exceptions while creating binary or session token with the supplier
	 */
	private static final long serialVersionUID = 9L;

	public CancellationException(String message) {
		super(message);
	}
}
