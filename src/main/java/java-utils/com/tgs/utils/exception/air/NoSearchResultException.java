package com.tgs.utils.exception.air;

import org.apache.axis2.AxisFault;

public class NoSearchResultException extends RuntimeException {

	private static final long serialVersionUID = 5L;

	public NoSearchResultException(String message) {
		super(message);
	}

	public NoSearchResultException(AxisFault af) {
		super(af.getMessage());
	}

	public NoSearchResultException() {
		super();
	}

}
