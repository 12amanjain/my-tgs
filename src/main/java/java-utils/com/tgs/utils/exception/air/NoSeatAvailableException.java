package com.tgs.utils.exception.air;

public class NoSeatAvailableException extends RuntimeException {

    private static final long serialVersionUID = 3L;

    public NoSeatAvailableException(String message) {
        super(message);
    }

    public NoSeatAvailableException() {
        super();
    }

}
