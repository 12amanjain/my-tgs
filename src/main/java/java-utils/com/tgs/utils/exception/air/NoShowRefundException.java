package com.tgs.utils.exception.air;

import java.io.IOException;
import java.rmi.RemoteException;

public class NoShowRefundException extends RuntimeException {

	private static final long serialVersionUID = 20L;

	public NoShowRefundException(String message) {
		super(message);
	}

	public NoShowRefundException(RemoteException e) {
		super(e);
	}

	public NoShowRefundException(IOException e) {
		super(e);
	}
}
