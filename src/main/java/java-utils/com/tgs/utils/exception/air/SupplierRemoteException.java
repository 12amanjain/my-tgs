package com.tgs.utils.exception.air;

import java.io.IOException;
import java.rmi.RemoteException;

public class SupplierRemoteException extends RuntimeException {

    private static final long serialVersionUID = 7L;

    public SupplierRemoteException(String message) {
        super(message);
    }

    public SupplierRemoteException(RemoteException e) {
        super(e);
    }

    public SupplierRemoteException(IOException e) {
        super(e);
    }

}
