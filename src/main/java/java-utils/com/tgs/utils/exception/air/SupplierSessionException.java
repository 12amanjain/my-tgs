package com.tgs.utils.exception.air;

public class SupplierSessionException extends RuntimeException {

    /**
     * This is to capture exceptions while creating binary or session token with the
     * supplier
     */
    private static final long serialVersionUID = 9L;

    public SupplierSessionException(String message) {
        super(message);
    }
}
