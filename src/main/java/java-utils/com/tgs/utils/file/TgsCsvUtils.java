package com.tgs.utils.file;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;

import com.opencsv.CSVWriter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TgsCsvUtils {

	public static void writeToCSV(List<Object[]> object, File file, List<String> outputFields) {
		FileWriter outputfile;
		try {
			outputfile = new FileWriter(file);
			List<String[]> data = new ArrayList<String[]>();
			if(CollectionUtils.isNotEmpty(outputFields))
				data.add(outputFields.toArray(new String[0]));
			object.forEach(obj -> {
				try {
					if(obj instanceof Object[]){
						String[] strArr = convertObjArrToStrArr((Object[])obj);
						data.add(strArr);
					}
				}
				catch(Exception e) {
					log.error("Unable to write to csv file with filename {} and data {} ", file.getName(),Arrays.toString(obj),  e);
				}
			});
			CSVWriter writer = new CSVWriter(outputfile, ',', CSVWriter.NO_QUOTE_CHARACTER, 
					CSVWriter.DEFAULT_ESCAPE_CHARACTER, CSVWriter.DEFAULT_LINE_END); 
			writer.writeAll(data); 
			writer.close();
		} catch (IOException e) {
			log.error("Error occured while writing to csv file with filename {}", file.getName(), e);
		} 

	}

	private static String[] convertObjArrToStrArr(Object[] objArr) {
		String[] strArr = new String[objArr.length];
		for (int index = 0; index < objArr.length; index++) {
			strArr[index] = String.valueOf(objArr[index])!="null" ? String.valueOf(objArr[index]) : "";
		}
		return strArr;
	}

}
