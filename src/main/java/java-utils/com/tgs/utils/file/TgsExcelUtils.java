package com.tgs.utils.file;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TgsExcelUtils {

	public static void writeToExcel(List<Object[]> data, File file,List<String> outputFields) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet spreadsheet = workbook.createSheet( " Report ");
		int rowid = 0;
		XSSFRow row =  spreadsheet.createRow(rowid++) ;
		writeToRow(row, outputFields.toArray());
		try {
			for(Object[] rowData : data) {
				try {
					row = spreadsheet.createRow(rowid++);
					writeToRow(row, rowData);
				} catch(Exception e) {
					log.error("Unable to write row number {} and row data {} to excel file with filename {} ",rowid, Arrays.toString(rowData),file.getName(),  e);
				}
			}
			FileOutputStream out = new FileOutputStream(file);
			workbook.write(out);
			out.close();
			workbook.close();
		} catch (IOException e) {
			log.error("Unable to write to excel file with filename {} ",file.getName(),  e);
		}
	}

	private static void writeToRow(XSSFRow row, Object[] rowData) {
		int cellid = 0;
		for (Object obj : rowData){
			Cell cell = row.createCell(cellid++);
			cell.setCellValue(String.valueOf(obj));
		}
	}
}
