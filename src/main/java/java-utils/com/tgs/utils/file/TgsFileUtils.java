package com.tgs.utils.file;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.HttpURLConnection;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import com.tgs.services.rms.datamodel.ReportFormat;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class TgsFileUtils {

	private static FileNameMap fileNameMap;
	
	static {
		fileNameMap = HttpURLConnection.getFileNameMap();
	}
	
    public static byte[] convertToByteStream(File file) {
        try {
            return org.apache.commons.io.FileUtils.readFileToByteArray(file);
        } catch (IOException e) {
            log.error("Unable to convert to bytestream for filename {} ", file.getName());
        }
        return null;
    }

    /**
     * This method is used for determine the content-type to be used while sending the request.
     * If the file name map does not contain particular file extension then default value will be taken.
     */
    public static String resolveContentType(final String fileName) {
        String contentType = fileNameMap.getContentTypeFor(fileName);
        if (contentType == null) {
            contentType = "application/octet-stream";
        }
        return contentType;
    }

    public static void writeToResponseOutputStream(HttpServletResponse response, String fileName, byte[] data) {
        if (data != null) {
            try (OutputStream os = response.getOutputStream()) {
                response.setContentType(resolveContentType(fileName));
                response.setHeader("Content-Disposition", String.format("attachment; filename=\"".concat(fileName).concat("\"")));
                os.write(data);
            } catch (IOException e) {
                log.error("Unable to write file to response output stream with file name {} ", fileName, e);
            }
        }
    }

    public static void writeToFile(ReportFormat outputFormat, List<Object[]> object, List<String> outputFields, File file) {
        switch (outputFormat) {
            case EXCEL:
                TgsExcelUtils.writeToExcel(object, file, outputFields);
                break;
            case PDF:
                TgsPdfUtils.writeToPdf(object, file, outputFields);
                break;
            case CSV:
                TgsCsvUtils.writeToCSV(object, file, outputFields);
                break;
            default:
                break;
        }
    }
}

