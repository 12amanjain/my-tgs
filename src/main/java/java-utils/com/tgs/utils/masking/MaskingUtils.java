package com.tgs.utils.masking;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;

import com.tgs.services.base.MaskedFieldSettings;
import com.tgs.services.base.MaskedFields;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MaskingUtils {


	private static String getRegexForJson(String fieldName) {
		return "\"".concat(fieldName).concat("\":\"(.+?)\"");
	}

	private static String getRegexForXml(String fieldName) {
		return "<.*".concat(fieldName).concat(".*>(.+?)</.*").concat(fieldName).concat(">");
	}

	public static String getMaskedString(String text, MaskedFields maskedFields, boolean isXml) {
		List<MaskedFieldSettings> fields= maskedFields!=null ? maskedFields.getMaskedFields() : null;
		if(CollectionUtils.isNotEmpty(fields)) {
			for(MaskedFieldSettings field : fields){
				try {
					Pattern pattern = Pattern.compile(isXml ? getRegexForXml(field.getField()) : getRegexForJson(field.getField()),Pattern.CASE_INSENSITIVE);
					Matcher matcher = pattern.matcher(text);
					while (matcher.find()) { 				
						text = text.substring(0,matcher.start(1) +field.getUnmaskedBeg()) + "******" +text.substring(matcher.end(1)-field.getUnmaskedEnd(), text.length());
					}
				} catch (Exception e) {
					log.error("Unable to mask text {} for fieldname {} ", text, field.getField());
				}
			}
		}
		return text;
	}

}
