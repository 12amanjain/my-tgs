package com.tgs.utils.string;

import lombok.Builder;
import lombok.Getter;
import org.apache.commons.lang3.StringUtils;

import java.util.StringJoiner;


@Getter
@Builder
public class TGSToStringUtils {

    /**
     * The object being output, may be null.
     */
    private final Object object;


    /**
     * The Delimiter <code>'='</code>.
     */
    private final String delimiter = "=";


    /**
     * The content start <code>'['</code>.
     */
    private final String contentStart = "[";

    /**
     * The content end <code>']'</code>.
     */
    private final String contentEnd = "]";

    private final String fieldSeparator = ",";

    private final StringJoiner joiner = new StringJoiner(delimiter, contentStart, contentEnd);

    private final StringJoiner valueJoiner = new StringJoiner(fieldSeparator);


    /**
     * <p>Append to the <code>toString</code> an <code>Object</code>
     * value.</p>
     *
     * @param fieldName the field name
     * @param obj       the value to add to the <code>toString</code>
     * @return this
     */
    public TGSToStringUtils appendField(final String fieldName, final Object obj) {
        StringJoiner fieldJoiner = new StringJoiner(delimiter);
        if (obj != null) {
            fieldJoiner.add(fieldName).add(obj.toString());
        } else {
            fieldJoiner.add(fieldName).add(null);
        }
        valueJoiner.add(fieldJoiner.toString());
        return this;
    }

    /**
     * <p>Append to the <code>toString</code> an <code>Object</code>
     * value.</p>
     *
     * @param obj the value to add to the <code>toString</code>
     * @return this
     */
    public TGSToStringUtils appendField(final Object obj) {
        StringJoiner fieldJoiner = new StringJoiner(delimiter);
        fieldJoiner.add(obj.toString());
        valueJoiner.add(fieldJoiner.toString());
        return this;
    }

    /**
     * @param obj the value to add to the <code>toString</code>
     * @return this
     */
    public TGSToStringUtils merge(final Object obj) {
        joiner.add(obj.toString());
        return this;
    }

    public String string() {
        joiner.add(removeLastFieldSeparator());
        String obj = StringUtils.join(object.getClass().getSimpleName(), joiner.toString());
        return obj;
    }

    /**
     * <p>Remove the last field separator from the buffer.</p>
     */
    protected String removeLastFieldSeparator() {
        StringBuilder obj = new StringBuilder(valueJoiner.toString());
        final int len = valueJoiner.length();
        final int sepLen = fieldSeparator.length();
        if (len > 0 && sepLen > 0 && len >= sepLen) {
            boolean match = true;
            for (int i = 0; i < sepLen; i++) {
                if (valueJoiner.toString().charAt(len - 1 - i) != fieldSeparator.charAt(sepLen - 1 - i)) {
                    match = false;
                    break;
                }
            }
            if (match) {
                obj.setCharAt(valueJoiner.toString().length() - 1, ' ');
            }
        }
        return obj.toString();
    }


}
