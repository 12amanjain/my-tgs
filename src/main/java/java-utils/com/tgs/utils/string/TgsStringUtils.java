package com.tgs.utils.string;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import com.ibm.icu.text.NumberFormat;

public class TgsStringUtils {

    public static final String ALPHABET_CHARS = "abcdefghijklmnopqrstuvwxyz";
    public static final String ALPHABET_CHARS_WITH_SPACE = "abcdefghijklmnopqrstuvwxyz ";

    public static String generateOTP(int len) {
        Random random = new Random();
        StringBuffer temp = new StringBuffer();
        for (int i = 1; i <= len; i++) {
            temp.append(random.nextInt(10));
        }
        return temp.toString();
    }

    public static String generateRandomNumber(int len, String prefix) {
        int prefixLength = String.valueOf(prefix).length();
        StringBuffer temp = new StringBuffer();
        temp.append(prefix);
        Random random = new Random();
        for (int i = 0; i < (len - prefixLength); i++) {
            temp.append(random.nextInt(10));
        }
        return temp.toString();
    }

    public static boolean containsOnlyAlphabet(String input) {
        return StringUtils.containsOnly(input.toLowerCase(), ALPHABET_CHARS);
    }

    public static boolean containsOnlyAlphabetAndSpace(String input) {
        return StringUtils.containsOnly(input.toLowerCase(), ALPHABET_CHARS_WITH_SPACE);
    }
    
	public static String formatCurrency(Double amount) {
		
		NumberFormat formatter = NumberFormat.getCurrencyInstance(new Locale("en", "IN"));
		return formatter.format(amount);

	}
	
	public static List<String> parseStringByRejex(String data, String rejexPattern) {
		if (StringUtils.isNotEmpty(data) && StringUtils.isNotEmpty(rejexPattern)) {
			Pattern pattern = Pattern.compile(rejexPattern);
			Matcher matcher = pattern.matcher(data);
			List<String> output = new ArrayList<>();
			while (matcher.find()) {
				output.add(matcher.group());
			}
			return output;
		}
		return null;
	}
	
	public static boolean hasSQLKeywords(String... values) {
		if (ArrayUtils.isNotEmpty(values)) {
			for (String value : values) {
				if (value.equalsIgnoreCase("and") || value.equalsIgnoreCase("or") || value.equalsIgnoreCase("(")
						|| value.equalsIgnoreCase(")") || value.equalsIgnoreCase("select")
						|| value.equalsIgnoreCase("from") || value.equalsIgnoreCase("where")
						|| value.equalsIgnoreCase("'='") || value.equalsIgnoreCase(";")) {
					return true;
				}
			}
		}
		return false;
	}

}
