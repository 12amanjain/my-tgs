package springfox.documentation.spring.web.json;

import java.lang.reflect.Type;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.google.gson.JsonSerializationContext;

public class SpringfoxJsonToGsonAdapter implements com.google.gson.JsonSerializer<Json> {

	@Override
	public JsonElement serialize(Json json, Type type, JsonSerializationContext context) {
		final JsonParser parser = new JsonParser();
		return parser.parse(json.value());
	}

}