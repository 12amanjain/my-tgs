package com.tgs.services.loggingservice;

import com.tgs.services.base.LogData;
import com.tgs.services.base.LoggingClient;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.es.datamodel.ESMetaInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;


@Service
public class ElasticSearchLogging implements LoggingClient {


    @Autowired
    ElasticSearchCommunicator esCommunicator;


    @Override
    public void store(List<LogData> logDataList) {
        logDataList.forEach(logData -> {
            store(logData);
        });
    }

    @Override
    public void store(LogData logData) {
        esCommunicator.addBulkDocuments(Arrays.asList(logData), ESMetaInfo.LOG);
    }
}
