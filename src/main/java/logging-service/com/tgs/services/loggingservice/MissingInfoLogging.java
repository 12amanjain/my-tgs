package com.tgs.services.loggingservice;

import static net.logstash.logback.argument.StructuredArguments.entries;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.services.base.LogData;
import com.tgs.services.base.LoggingClient;
import com.tgs.services.base.utils.TgsObjectUtils;
import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Qualifier("MissingInfoLogging")
@Service
public class MissingInfoLogging implements LoggingClient {

	@Override
	public void store(List<LogData> logDataList) {
		if (!Collections.isEmpty(logDataList)) {
			for (LogData logData : logDataList) {
				store(logData);
			}
		}
	}

	@Override
	public void store(LogData logData) {
		try {
			if (!StringUtils.isEmpty(logData.getLogData())) {
				List<Object> objectList = new ArrayList<>();
				StringBuffer str = new StringBuffer();
				Map<String, Object> notNullFieldMap = TgsObjectUtils.getNotNullFieldValueMap(logData, false, false);
				notNullFieldMap.forEach((key, value) -> {
					str.append(key + " {} ,");
					objectList.add(value);
				});
				log.error(str.toString(), objectList.toArray(new Object[0]));
			} else if (!CollectionUtils.isEmpty(logData.getLogDataList())) {
				for (String logs : logData.getLogDataList()) {
					Map<String, Object> notNullFieldMap = new Gson().fromJson(logs, new TypeToken<Map<String, Object>>() {}.getType());
					log.error("Missing Info", entries(notNullFieldMap));
				}
			}
		} catch (Exception e) {
			log.error("Unable to store missing info of {} due to {}", logData, e);
		}
	}
}
