package com.tgs.services.loggingservice;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tgs.services.base.LogData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.tgs.services.base.LoggingClient;
import com.tgs.services.base.utils.TgsObjectUtils;

import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Qualifier("ServiceHealthLogging")
@Service
public class ServiceHealthLogging implements LoggingClient {

	@Autowired
	LogManager logManager;

	@Override
	public void store(List<LogData> logDataList) {
		if (!Collections.isEmpty(logDataList)) {
			for (LogData logData : logDataList) {
				store(logData);
			}
		}
	}

	@Override
	public void store(LogData logData) {

		List<Object> objectList = new ArrayList<>();
		StringBuffer str = new StringBuffer();
		Map<String, Object> notNullFieldMap = TgsObjectUtils.getNotNullFieldValueMap(logData,false,false);
		notNullFieldMap.forEach((key, value) -> {
			str.append(key + " {} ,");
			objectList.add(value);
		});
		log.error(str.toString(), objectList.toArray(new Object[0]));
	}
}
