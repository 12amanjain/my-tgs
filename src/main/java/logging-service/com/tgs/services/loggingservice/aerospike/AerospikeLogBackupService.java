package com.tgs.services.loggingservice.aerospike;

import static org.apache.commons.collections.CollectionUtils.isEmpty;
import static org.apache.commons.collections.CollectionUtils.isNotEmpty;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import java.util.stream.IntStream;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.aerospike.client.cdt.MapOperation;
import com.aerospike.client.cdt.MapReturnType;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.SystemContextFilter;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.loggingservice.utils.FileUtil;
import com.tgs.utils.aws.AwsUtils;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AerospikeLogBackupService {

	final private static String LOGGED_IDS_MAP_KEY = "loggedids", LOGGED_IDS_MAP_BIN_NAME = "idmaps",
			/**
			 * Maintain copy to make the service fault tolerant
			 */
			LOGGED_IDS_MAP_COPY_KEY = "loggedidscopy";

	final private static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	final private static int MAX_PARTITIONS = 10, MAX_COPY_PARTITIONS = 13;

	final private static int MAX_LOG_OBJECTS = 50;

	@Value("${log.aerospike.backup.enabled}")
	private String enabledProp;

	private Boolean enabled;

	@Value("${log.aerospike.backup.aws.bucket}")
	private String bucketName;

	@Value("${log.aerospike.backup.aws.objectPrefix}")
	private String objectPrefix;

	private int currentPartition = 0;

	final private AtomicBoolean inProgress = new AtomicBoolean(false);

	@Autowired
	private AerospikeLoggingService aerospikeLoggingService;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	/**
	 * Puts the {@code logData} key in map which maintains the "log ids" for which logs will be backed up when service
	 * starts.
	 * 
	 * @param logData
	 * @return {@code false} if it is rejected
	 */
	public boolean submitLogForBackup(LogData logData) {
		String logKey = logData.getKey();

		if (!isBackupEnabled()) {
			return false;
		}

		storeLogKey(logKey);
		log.debug("Added logKey {} for backup", logKey);
		return true;
	}

	public void startBackupService() {
		if (!isBackupEnabled()) {
			throw new CustomGeneralException(SystemError.AEROSPIKE_LOGS_BACKUP_SERVICE_DISABLED);
		}

		if (!inProgress.compareAndSet(false, true)) {
			throw new CustomGeneralException(SystemError.AEROSPIKE_LOGS_BACKUP_SERVICE_RUNNING);
		}

		Supplier<Set<String>> logKeysSupplier = () -> {
			Set<String> logKeysCopy = getAllLogKeysFromCopy();
			if (isEmpty(logKeysCopy)) {
				logKeysCopy = copyMapToCopy();
			}
			return logKeysCopy;
		};

		Set<String> logKeys = logKeysSupplier.get();
		log.info("Taking log backup for {} keys", logKeys.size());

		final ContextData contextData = SystemContextHolder.getContextData();
		new Thread(() -> {
			log.info("Started Aerospike log backup service...");
			try {
				SystemContextHolder.setContextData(contextData);
				SystemContextFilter.setMDCProperties();
				for (String logKey : logKeys) {
					backupLog(logKey);
				}
				deleteLogIdsMapCopy();
			} catch (Throwable e) {
				log.error("Aerospike log backup failed due to ", e);
			}
			log.info("Aerospike log backup service finished");

			inProgress.set(false);
		}).start();
	}

	/**
	 * Try all partitions for write. Returns if write fails for all partitions. This ensures that partition emptied by
	 * one application instance can be used by other instance.
	 * 
	 * @param logKey
	 */
	private void storeLogKey(final String logKey) {
		int numTry = 1;
		do {
			final int currentPartition = this.currentPartition;
			String logIdsMapKey = getKeyForLogIdsMapPartition(currentPartition);
			try {
				CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(logIdsMapKey)
						.namespace(CacheNameSpace.LOGS.getName()).set(CacheSetName.LOGS.getName()).build();
				HashMap<String, String> map = new HashMap<>();
				map.put(logKey, "1");
				cachingCommunicator.storeInMap(metaInfo, LOGGED_IDS_MAP_BIN_NAME, map);
				break;
			} catch (CachingLayerException e) {
				if (e.getErrorCode() == CachingLayerError.WRITE_ERROR.getErrorCode()) {
					/**
					 * Thread-safe: Two threads which will fail must be trying to write to same partition
					 */
					this.currentPartition = (currentPartition + 1) % MAX_PARTITIONS;
					log.debug("Record size too big for key {}, generated new key {} for fetch", logIdsMapKey,
							getKeyForLogIdsMapPartition(this.currentPartition));
				} else {
					throw e;
				}
			}
		} while (numTry++ < MAX_PARTITIONS);
	}

	/**
	 * Fetch log ids without removing them from map copy
	 * 
	 * @return
	 */
	@SuppressWarnings({"serial", "unchecked"})
	private Set<String> getAllLogKeysFromCopy() {
		Set<String> logKeys = new HashSet<>();
		for (int partition = 0; partition < MAX_COPY_PARTITIONS; partition++) {
			String logIdsMapKey = getKeyForLogIdsMapCopyPartition(partition);
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(logIdsMapKey).namespace(CacheNameSpace.LOGS.getName())
					.set(CacheSetName.LOGS.getName()).plainData(true)
					.typeOfT(new TypeToken<List<String>>() {}.getType()).build();
			try {
				List<String> fetchedLogKeys = cachingCommunicator.performOperationsAndFetchBinValue(metaInfo,
						LOGGED_IDS_MAP_BIN_NAME, List.class, -1,
						MapOperation.getByIndexRange(LOGGED_IDS_MAP_BIN_NAME, 0, MapReturnType.KEY));
				if (isNotEmpty(fetchedLogKeys)) {
					logKeys.addAll(fetchedLogKeys);
				}
			} catch (CachingLayerException e) {
				if (e.getErrorCode() != CachingLayerError.KEY_NOT_FOUND_ERROR.getErrorCode()) {
					log.error("Failed to fetch data for key {} from map copy due to ", logIdsMapKey, e);
					throw e;
				}
				log.debug("Key {} not found for loggedids map copy", logIdsMapKey);
			}
		}
		return logKeys;
	}

	/**
	 * Copy log ids to copy records partition-wise. Log ids are not removed from them during fetch
	 * 
	 * @return all keys copied
	 */
	private Set<String> copyMapToCopy() {
		Set<String> logKeysCopied = new HashSet<>();
		int currentCopyPartition = 0;
		final int chunkSize = 5000;
		final CacheMetaInfo metaInfo =
				CacheMetaInfo.builder().key(getKeyForLogIdsMapCopyPartition(currentCopyPartition))
						.namespace(CacheNameSpace.LOGS.getName()).set(CacheSetName.LOGS.getName()).build();
		final HashMap<String, String> map = new HashMap<>();
		for (int partition = 0; partition < MAX_PARTITIONS; partition++) {
			List<String> logKeys = getAllLogKeys(partition);
			if (isEmpty(logKeys)) {
				continue;
			}
			logKeysCopied.addAll(logKeys);
			for (int i = 0; i < logKeys.size() + chunkSize; i += chunkSize) {
				map.clear();
				IntStream.range(i, Math.min(i + chunkSize, logKeys.size())).forEach(j -> map.put(logKeys.get(j), "1"));
				if (map.isEmpty()) {
					continue;
				}
				try {
					cachingCommunicator.storeInMap(metaInfo, LOGGED_IDS_MAP_BIN_NAME, map);
				} catch (CachingLayerException e) {
					if (e.getErrorCode() == CachingLayerError.WRITE_ERROR.getErrorCode()
							&& currentCopyPartition < MAX_COPY_PARTITIONS) {
						metaInfo.setKey(getKeyForLogIdsMapCopyPartition(++currentCopyPartition));
						cachingCommunicator.storeInMap(metaInfo, LOGGED_IDS_MAP_BIN_NAME, map);
					} else {
						log.error("Failed to copy loggedids map to key {} due to ", metaInfo.getKey(), e);
						throw e;
					}
				}
			}
		}
		return logKeysCopied;
	}

	@SuppressWarnings({"serial"})
	private void deleteLogIdsMapCopy() {
		for (int partition = 0; partition < MAX_COPY_PARTITIONS; partition++) {
			String logIdsMapKey = getKeyForLogIdsMapCopyPartition(partition);
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(logIdsMapKey).namespace(CacheNameSpace.LOGS.getName())
					.set(CacheSetName.LOGS.getName()).plainData(true)
					.typeOfT(new TypeToken<List<String>>() {}.getType()).build();
			cachingCommunicator.delete(metaInfo);
		}
	}

	/**
	 * Empty the partition.
	 * 
	 * @param partition
	 * @return
	 */
	@SuppressWarnings({"serial", "unchecked"})
	private List<String> getAllLogKeys(int partition) {
		String logIdsMapKey = getKeyForLogIdsMapPartition(partition);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(logIdsMapKey).namespace(CacheNameSpace.LOGS.getName())
				.set(CacheSetName.LOGS.getName()).plainData(true).typeOfT(new TypeToken<List<String>>() {}.getType())
				.build();
		try {
			return cachingCommunicator.performOperationsAndFetchBinValue(metaInfo, LOGGED_IDS_MAP_BIN_NAME, List.class,
					-1, MapOperation.removeByIndexRange(LOGGED_IDS_MAP_BIN_NAME, 0, MapReturnType.KEY));
		} catch (CachingLayerException e) {
			if (e.getErrorCode() != CachingLayerError.KEY_NOT_FOUND_ERROR.getErrorCode()) {
				log.error("Failed to fetch data for key {} due to ", logIdsMapKey, e);
				throw e;
			}
			log.debug("Key {} not found for loggedids map", logIdsMapKey);
		}
		return null;
	}

	private String getKeyForLogIdsMapPartition(int partition) {
		if (partition == 0) {
			return LOGGED_IDS_MAP_KEY;
		}
		return new StringBuilder(LOGGED_IDS_MAP_KEY).append('_').append(partition).toString();
	}

	private String getKeyForLogIdsMapCopyPartition(int partition) {
		if (partition == 0) {
			return LOGGED_IDS_MAP_COPY_KEY;
		}
		return new StringBuilder(LOGGED_IDS_MAP_COPY_KEY).append('_').append(partition).toString();
	}

	private void backupLog(String key) throws IOException {
		log.debug("Taking log backup for key {}", key);
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
			Map<String, String> logs = aerospikeLoggingService.getFormattedLogs(key);
			FileUtil.writeZipFile(baos, logs);
			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			AwsUtils.uploadToAws(bais, bucketName, createNewObjectKey(key));
			bais.close();
		}

	}

	/**
	 * Get logs from backup as {@code Map<String, String>}.
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, String> getLogs(String key) {
		if (!isBackupEnabled()) {
			return new HashMap<>();
		}

		final Map<String, String> filesData = new LinkedHashMap<>();
		try {
			List<String> objectKeys = AwsUtils.listAwsObjects(bucketName, getObjectKeyPrefix(key).toString());
			/**
			 * Limit the number of objects fetched for very generalised key (prefix; e.g., "TJ")
			 */
			final int maxLogObjects = Math.min(MAX_LOG_OBJECTS, objectKeys.size());
			for (int i = 0; i < maxLogObjects; i++) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				AwsUtils.readFromAws(baos, bucketName, objectKeys.get(i));
				ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
				Map<String, String> objectFilesData = FileUtil.readZipFileAsString(bais);
				if (MapUtils.isNotEmpty(objectFilesData)) {
					filesData.putAll(objectFilesData);
				}
				bais.close();
				baos.close();
			}
			return filesData;
		} catch (IOException e) {
			log.error("Unable to fetch logs from bakcup for key {} due to", key, e);
		}
		return null;
	}

	/**
	 * Key for ZIP file written into s3 bucket.
	 * 
	 * @param key
	 * @return
	 */
	private String createNewObjectKey(String key) {
		LocalDate today = LocalDate.now();
		return getObjectKeyPrefix(key).append('_').append(today.format(FORMATTER)).append(".zip").toString();
	}

	private StringBuilder getObjectKeyPrefix(String key) {
		return new StringBuilder(objectPrefix).append(key);
	}

	private boolean isBackupEnabled() {
		if (enabled == null) {
			enabled = BooleanUtils.toBoolean(enabledProp);
		}
		return enabled;
	}

}
