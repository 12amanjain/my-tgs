package com.tgs.services.loggingservice.aerospike;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.LogData;
import com.tgs.services.base.LoggingClient;
import lombok.extern.slf4j.Slf4j;

@Service
@Qualifier("AerospikeLogging")
@Slf4j
public class AerospikeLogging implements LoggingClient {

	@Autowired
	private AerospikeLoggingService aerospikeLoggingService;

	@Autowired
	private AerospikeLogBackupService backupService;

	@Override
	public void store(List<LogData> logDataList) {
		logDataList.forEach(logData -> {
			store(logData);
		});

	}

	@Override
	public void store(LogData logData) {
		if (StringUtils.isBlank(logData.getKey()) || (logData.getTtl() != null && logData.getTtl() == 0)) {
			log.debug("TTl is zero for key {}", logData.getKey());
			return;
		}

		aerospikeLoggingService.storeLog(logData);
		backupService.submitLogForBackup(logData);
	}

}
