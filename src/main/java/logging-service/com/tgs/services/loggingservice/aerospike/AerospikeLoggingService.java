package com.tgs.services.loggingservice.aerospike;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AerospikeLoggingService {

	final private static DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss.SSS");

	final private static int MAX_RETRY = 4;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;


	void storeLog(LogData logData) {
		int ttl = 60 * 60;
		if (logData.getTtl() != null) {
			ttl = logData.getTtl().intValue();
		}
		int numTry = 1;
		String logKey = logData.getKey();
		do {
			try {
				CacheMetaInfo metaInfo = getCacheMetaInfo(logKey);
				cachingCommunicator.store(metaInfo, BinName.APILOGS.getName(), Arrays.asList(logData), true, false,
						ttl);
				break;
			} catch (CachingLayerException e) {
				if (e.getErrorCode() == CachingLayerError.WRITE_ERROR.getErrorCode()) {
					log.info("Record size too big for key {}, generated new key {}_{} for fetch", logKey,
							logData.getKey(), numTry);
					logKey = logData.getKey() + "_" + numTry;
				} else {
					throw e;
				}
			}
		} while (++numTry < MAX_RETRY);
	}

	/**
	 * 
	 * @param key
	 * @return non-null list of logs
	 */
	public List<LogData> getLogs(String key) {
		Map<String, List<LogData>> result = new HashMap<>();
		String copiedKey = key;
		int fetchCount = 1;
		do {
			CacheMetaInfo metaInfo = getCacheMetaInfo(copiedKey);
			Map<String, List<LogData>> temp =
					cachingCommunicator.getList(metaInfo, LogData.class, true, false, new String[0]);
			if (MapUtils.isNotEmpty(temp)) {
				for (String logKey : temp.keySet()) {
					List<LogData> logDataList = temp.get(logKey);
					if (CollectionUtils.isEmpty(logDataList)) {
						continue;
					}
					if (result.get(logKey) == null) {
						result.put(logKey, new ArrayList<>());
					}
					result.get(logKey).addAll(logDataList);
				}
			} else {
				break;
			}
			copiedKey = key + "_" + fetchCount;
		} while (++fetchCount < MAX_RETRY);

		List<LogData> logList = new ArrayList<>();
		for (List<LogData> logListFetched : result.values()) {
			if (logListFetched != null)
				logList.addAll(logListFetched);
		}
		return logList;
	}

	/**
	 * Get logs from aerospike as {@code Map<String, String>}, sorted and formatted.
	 * 
	 * @param key
	 * @return
	 */
	public Map<String, String> getFormattedLogs(String key) {
		return getFormattedLogs(key, l -> true);
	}

	/**
	 * Get filtered logs from aerospike as {@code Map<String, String>}, sorted and formatted.
	 * 
	 * @param key
	 * @param filter non-null filter
	 * @return
	 */
	public Map<String, String> getFormattedLogs(String key, Predicate<LogData> filter) {
		List<LogData> logList = getLogs(key);
		logList.sort(new Comparator<LogData>() {
			@Override
			public int compare(LogData o1, LogData o2) {
				return o1.getGenerationTime().compareTo(o2.getGenerationTime());
			}

		});

		/**
		 * Used LinkedHashMap to maintain order
		 */
		Map<String, String> formattedLogs = new LinkedHashMap<>();
		for (LogData logData : logList) {
			if (logData != null && StringUtils.isNotBlank(logData.getLogData()) && filter.test(logData)) {
				formattedLogs.put(
						String.join(":", logData.getType(), logData.getLoggedInUserId(),
								logData.getGenerationTime().format(FORMATTER)),
						logData.getLogData().replaceAll("'", "&#x27;"));
			}
		}
		return formattedLogs;
	}

	private CacheMetaInfo getCacheMetaInfo(String logKey) {
		return CacheMetaInfo.builder().key(logKey).namespace(CacheNameSpace.LOGS.getName()).kyroCompress(true)
				.set(CacheSetName.LOGS.getName()).build();
	}

}
