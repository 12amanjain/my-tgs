package com.tgs.services.loggingservice.communicator;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.tgs.services.base.LogData;
import com.tgs.services.base.LoggingClient;
import com.tgs.services.base.MaskedFields;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.LogServiceCommunicator;
import com.tgs.services.base.configurationmodel.LoggingClientInfo;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.loggingservice.LogManager;

import io.jsonwebtoken.lang.Collections;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LogServiceCommunicatorImpl implements LogServiceCommunicator {

	@Autowired
	LogManager logManager;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	@Override
	public void store(List<LogData> logDataList) {
		if (Collections.isEmpty(logDataList)) {
			return;
		}

		logDataList.forEach(logData -> {
			store(logData);
		});

	}

	@Override
	public void store(LogData logData) {
		ExecutorUtils.getLogThreadPool().submit(() -> {
			try {
				List<LoggingClientInfo> clients = logManager.getLogInfo(logData);
				clients.forEach(client -> {
					LoggingClient loggingClient = null;
					/**
					 * If time taken/diff is more than the threshold then to call appropriate
					 * loggingClient , else don't call
					 */
					if (client.getThreshold() != null) {
						if (logData.getDiff() > client.getThreshold()) {
							loggingClient = logManager.getLoggingClient(client.getClientName());
						}
					} else {
						loggingClient = logManager.getLoggingClient(client.getClientName());
					}

					/**
					 * If TTL is defined at client level then set ttl for every request
					 */
					if (client.getTtl() != null && logData.getTtl() == null)
						logData.setTtl((long) client.getTtl());
					if (loggingClient != null) {
						/*
						 * MaskedFields maskedFields = getFieldsToMask(); String logDataStr =
						 * MaskingUtils.getMaskedString(logData.getLogData(),maskedFields,true);
						 * logDataStr = MaskingUtils.getMaskedString(logDataStr,maskedFields,false);
						 * logData.setLogData(logDataStr);
						 */
						loggingClient.store(logData);
					}
				});
			} catch (Exception e) {
				log.error("Not able to store logs", e);
			}
		});
	}

	private MaskedFields getFieldsToMask() {
		List<Document> docList = gsCommunicator.fetchRoleSpecificDocument(CollectionServiceFilter.builder()
				.isConsiderHierarchy(true).types(Arrays.asList("masked_fields")).build());
		if (CollectionUtils.isNotEmpty(docList))
			return new Gson().fromJson(docList.get(0).getData(), MaskedFields.class);
		return null;
	}

}
