package com.tgs.services.loggingservice.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.loggingservice.aerospike.AerospikeLogBackupService;

@RestController
@RequestMapping("/logs/v1")
public class LogsController {

	@Autowired
	private AerospikeLogBackupService aerospikeLogBackupService;

	@RequestMapping(value = "/aerospike/job/backup-service/start", method = RequestMethod.GET)
	protected @ResponseBody BaseResponse backup() throws Exception {
		aerospikeLogBackupService.startBackupService();
		return new BaseResponse();
	}
}
