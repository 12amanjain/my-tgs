
package com.tgs.services.loggingservice.restcontroller;

import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLConnection;
import java.time.format.DateTimeFormatter;
import java.util.Map;
import java.util.function.Predicate;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.tgs.services.base.LogData;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.LogDataVisibilityGroup;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.loggingservice.aerospike.AerospikeLogBackupService;
import com.tgs.services.loggingservice.aerospike.AerospikeLoggingService;
import com.tgs.services.loggingservice.utils.FileUtil;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Controller
@RequestMapping("/jsp")
@Slf4j
@CustomRequestProcessor(areaRole = AreaRole.PROBE_VIEW, includedRoles = {UserRole.ADMIN},
		emulatedAllowedRoles = {UserRole.ADMIN})
public class ProbeController {

	private final static String TGS_EMAIL_SUFFIX = "@technogramsolutions.com";

	@Autowired
	private AerospikeLoggingService aerospikeLoggingService;

	@Autowired
	private AerospikeLogBackupService logBackupService;

	DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd_HH:mm:ss.SSS");

	@Value("${log.aerospike.visibility.disable}")
	private String visibilityDisabledProp;

	private static Boolean isVisibilityDisabled;

	@RequestMapping("/probe/home")
	public String request() {
		return "probe/probe_home";
	}

	@GetMapping("/probe/results")
	public String query(Model model, HttpServletRequest request, @RequestParam String key) {
		log.debug("key is {}", key);

		Map<String, String> logs = getLogs(key, getVisibilityFilter(SystemContextHolder.getContextData().getUser()));

		model.addAttribute("results", logs);
		model.addAttribute("keyValue", key);

		return "probe/probe_result";
	}

	/**
	 * Creates filter that returns {@code true} if
	 * <li>there is no visibility group for given logData
	 * <li>one of the visibility groups is PUBLIC,
	 * <li>User belongs to any visibility group, or
	 * <li>(for visibility groups other than PUBLIC) user has email with suffix {@link TGS_EMAIL_SUFFIX}.
	 * 
	 * @param user
	 * @return non-null predicate
	 */
	private Predicate<LogData> getVisibilityFilter(User user) {
		return logData -> {
			if (isVisibilityDisabled() || isEmpty(logData.getVisibilityGroups())) {
				return true;
			}
			for (String visibilityGroup : logData.getVisibilityGroups()) {
				if (LogDataVisibilityGroup.PUBLIC.name().equals(visibilityGroup)
						|| user.getEmail().contains(TGS_EMAIL_SUFFIX)) {
					return true;
				}
			}
			return false;
		};
	}

	private boolean isVisibilityDisabled() {
		if (isVisibilityDisabled == null) {
			isVisibilityDisabled = BooleanUtils.toBoolean(visibilityDisabledProp);
		}
		return isVisibilityDisabled;
	}

	@RequestMapping("/probe/downloadAll/{key}")
	public void downloadAll(@PathVariable String key, HttpServletResponse response) {
		Predicate<LogData> filter = getVisibilityFilter(SystemContextHolder.getContextData().getUser())
				.and(logData -> logData.getSupplier() == null);
		try (OutputStream os = response.getOutputStream()) {
			Map<String, String> logs = getLogs(key, filter);
			response.setContentType(URLConnection.guessContentTypeFromName(".zip"));
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"" + key + ".zip\""));
			FileUtil.writeZipFile(os, logs);
		} catch (IOException e) {
			log.error("Unable to download zip file for key {}", key, e);
		}
	}

	private Map<String, String> getLogs(String key, Predicate<LogData> filter) {
		Map<String, String> logs = aerospikeLoggingService.getFormattedLogs(key, filter);
		if (MapUtils.isEmpty(logs) || true) {
			Map<String, String> archivedlogs = logBackupService.getLogs(key);
			if (MapUtils.isNotEmpty(archivedlogs)) {
				logs.putAll(archivedlogs);
			}
		}
		return logs;
	}
}
