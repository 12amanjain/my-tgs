package com.tgs.services.messagingService.communicator.impl;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.configurationmodel.EmailVendorConfiguration;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.configurationmodel.WhatsAppConfiguration;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.messagingService.datamodel.EmailMetaData;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppAttributes;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppMetaData;
import com.tgs.services.messagingService.helper.MessageUtils;
import com.tgs.services.messagingService.runtime.EmailService;
import com.tgs.services.messagingService.runtime.SmsService;
import com.tgs.services.messagingService.runtime.WhatsAppService;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.string.StringTemplateUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MsgServiceCommunicatorImpl implements MsgServiceCommunicator {

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	@Lazy
	private UserServiceCommunicator userCommunicator;

	@Autowired
	private WhatsAppService whatsAppService;

	@Override
	public <T extends EmailAttributes> void sendMail(AbstractMessageSupplier<T> msgAttr) {
		sendMail(msgAttr.getAttributes());
	}

	@Override
	public void sendMail(EmailAttributes msgAttr) {
		if (msgAttr != null) {
			try {
				String key = msgAttr.getKey();
				if (msgAttr.getRole() != null && UserRole.CUSTOMER.equals(msgAttr.getRole()))
					key = key.concat("_").concat(msgAttr.getRole().getRoleDisplayName());
				if (msgAttr.getPartnerId() != null && !UserUtils.DEFAULT_PARTNERID.equals(msgAttr.getPartnerId()))
					key = key.concat("_").concat(msgAttr.getPartnerId());
				List<Document> docList = gmsCommunicator.fetchGeneralDocument(CollectionServiceFilter.builder()
						.fetchType(FetchType.CACHE).key(key).isConsiderHierarchy(true).build());
				log.info("DocList size is {} for key {} ", TgsCollectionUtils.size(docList), key);
				if (CollectionUtils.isNotEmpty(docList)) {
					EmailMetaData emailMetaData =
							GsonUtils.getGson().fromJson(docList.get(0).getData(), EmailMetaData.class);
					String subjectSuffix = "";
					if (msgAttr.isAddDateTimeInSubject()) {
						subjectSuffix = " - " + LocalDateTime.now().toString();
					}
					emailMetaData
							.setSubject(MessageUtils.setAttribute(emailMetaData.getSubject(), msgAttr) + subjectSuffix);
					emailMetaData.setBody(MessageUtils.setAttribute(emailMetaData.getBody(), msgAttr));
					emailMetaData.setAttachmentData(msgAttr.getAttachmentData());
					String toEMailInMssgAttr = (StringUtils.isNotEmpty(msgAttr.getToEmailUserId()))
							? getToEmailFromUser(msgAttr.getToEmailUserId())
							: msgAttr.getToEmailId();
					emailMetaData.setToEmail(ObjectUtils.firstNonNull(toEMailInMssgAttr, emailMetaData.getToEmail()));
					emailMetaData.setBccEmail(
							ObjectUtils.firstNonNull(msgAttr.getBccEmailId(), emailMetaData.getBccEmail()));
					emailMetaData
							.setCcEmail(ObjectUtils.firstNonNull(msgAttr.getCcEmailId(), emailMetaData.getCcEmail()));
					emailMetaData.setFromEmail(
							ObjectUtils.firstNonNull(msgAttr.getFromEmail(), emailMetaData.getFromEmail()));
					EmailVendorConfiguration vendorConfig = (EmailVendorConfiguration) gmsCommunicator
							.getConfiguratorInfo(ConfiguratorRuleType.EMAILCONFIG).getOutput();
					EmailService emailService =
							(EmailService) SpringContext.getApplicationContext().getBean(vendorConfig.getBeanName());
					ExecutorUtils.getCommunicationThreadPool().submit(() -> {
						emailService.sendEmail(emailMetaData, vendorConfig);
					});
				}
			} catch (Exception e) {
				log.error("Error occurred while sending email for the key {} exception {}", msgAttr.getKey(), e);
			}
		}
	}

	private String getToEmailFromUser(String toEmailUserId) {
		User toEmailUser = userCommunicator.getUserFromCache(toEmailUserId);
		return toEmailUser.getEmail();
	}

	@Override
	public void sendMessage(SmsAttributes msgAttr) {
		if (msgAttr != null) {
			try {
				if (CollectionUtils.isEmpty(msgAttr.getRecipientNumbers())) {
					return;
				}
				String key = msgAttr.getKey();
				if (msgAttr.getRole() != null && UserRole.CUSTOMER.equals(msgAttr.getRole()))
					key = key.concat("_").concat(msgAttr.getRole().getRoleDisplayName());
				if (msgAttr.getPartnerId() != null && !UserUtils.DEFAULT_PARTNERID.equals(msgAttr.getPartnerId()))
					key = key.concat("_").concat(msgAttr.getPartnerId());
				List<Document> docList = gmsCommunicator.fetchGeneralDocument(
						CollectionServiceFilter.builder().fetchType(FetchType.CACHE).key(key).build());
				if (CollectionUtils.isNotEmpty(docList)) {
					SmsMetaData smsMetaData = new SmsMetaData();
					String data = docList.get(0).getData();
					Type type = new TypeToken<Map<String, String>>() {}.getType();
					Map<String, String> map = new Gson().fromJson(data, type);
					smsMetaData
							.setBody(StringTemplateUtils.replaceAttributes(map.get("value"), msgAttr.getAttributes()));
					List<String> recpientNumbers = new ArrayList<>();
					msgAttr.getRecipientNumbers().forEach(number -> {
						// If mobile number contains the country code, consider the number part only for
						// now, which works for india
						if (number.contains("-")) {
							recpientNumbers.add(number.split("-")[1]);
						} else {
							recpientNumbers.add(number);
						}
					});
					smsMetaData.setRecipientNumbers(recpientNumbers);
					log.info("Sending sms to {} with message content {}", recpientNumbers.toString(),
							smsMetaData.getBody());
					if (MessageMedium.SMS.equals(msgAttr.getMedium())) {
						SmsVendorConfiguration conf = (SmsVendorConfiguration) gmsCommunicator
								.getConfigRule(ConfiguratorRuleType.SMSCONFIG, null);
						SmsService smsService =
								(SmsService) SpringContext.getApplicationContext().getBean(conf.getBeanName());
						ExecutorUtils.getCommunicationThreadPool().submit(() -> {
							smsService.sendSms(smsMetaData, conf);
						});
					}
				}
			} catch (Exception e) {
				log.error("Error occurred while sending sms for the key {} exception {}", msgAttr.getKey(), e);
			}
		}
	}

	@Override
	public void sendWhatsAppMessage(WhatsAppAttributes wappAttr) {
		if (wappAttr != null) {
			try {
				if (CollectionUtils.isEmpty(wappAttr.getRecipientNumbers())) {
					return;
				}
				List<Document> docList = gmsCommunicator.fetchDocument(
						CollectionServiceFilter.builder().fetchType(FetchType.CACHE).key("WHATSAPPCONFIG").build());
				if (CollectionUtils.isNotEmpty(docList)) {
					WhatsAppMetaData wappMetaData = new WhatsAppMetaData();
					String data = docList.get(0).getData();
					Type type = new TypeToken<Map<String, Object>>() {}.getType();
					Map<String, Object> map = new Gson().fromJson(data, type);
					Map<String, String> templateName = (Map<String, String>) map.get("templateName");
					List<String> recpientNumbers = new ArrayList<>();
					wappAttr.getRecipientNumbers().forEach(number -> {
						if (number.contains("-") || number.contains("+")) {
							recpientNumbers.add(number.replaceAll("[-+]", ""));
						} else {
							recpientNumbers.add(StringUtils.join("91", number));
						}
					});

					log.info("Sending whatsapp message to {} ", recpientNumbers.toString());
					if (MessageMedium.WHATSAPP.equals(wappAttr.getMedium())) {
						wappMetaData.setRecipientNumbers(recpientNumbers);
						wappMetaData.setAttributes(wappAttr.getAttributes());
						wappMetaData.setAttachmentData(wappAttr.getAttachmentData());
						wappMetaData.setTemplateName(templateName.get(wappAttr.getKey()));
						WhatsAppConfiguration conf = (WhatsAppConfiguration) gmsCommunicator
								.getConfigRule(ConfiguratorRuleType.WHATSAPPCONFIG, null);
						whatsAppService.sendWhatsAppMessage(wappMetaData, conf);
					}
				}
			} catch (Exception e) {
				log.error("Error occurred while sending whatsapp message for the key {} exception {}",
						wappAttr.getKey(), e.getMessage(), e);
			}
		}
	}
}
