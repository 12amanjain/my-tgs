package com.tgs.services.messagingService.helper;

import org.antlr.stringtemplate.StringTemplate;

import com.tgs.services.base.datamodel.EmailAttributes;

public class MessageUtils {

	public static String setAttribute(String text, EmailAttributes attr) {
		StringTemplate template = new StringTemplate(text);
		template.setAttribute("attr", attr);
		return template.toString();
	}

}
