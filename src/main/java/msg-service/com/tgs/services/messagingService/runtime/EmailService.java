package com.tgs.services.messagingService.runtime;

import com.tgs.services.base.configurationmodel.EmailVendorConfiguration;
import com.tgs.services.messagingService.datamodel.EmailMetaData;

public interface EmailService {

	public void sendEmail(EmailMetaData metaData, EmailVendorConfiguration vendorConfig);
}
