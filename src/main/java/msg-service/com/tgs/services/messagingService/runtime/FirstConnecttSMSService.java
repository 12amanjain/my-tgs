package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FirstConnecttSMSService implements SmsService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration conf) {
		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int retryCount = 3;
			String responseString = "";
			try {
				while (retryCount > 0) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl())
							.queryParams(getQueryParams(conf, number, metaData.getBody())).build();
					try {
						Map<String, String> responseObj = httpUtils.getResponse(Map.class).orElse(new HashMap<>());
						if (MapUtils.isNotEmpty(responseObj)) {
							if ("error".equals(responseObj.get("status"))) {
								log.info(
										"[FirstConnectt] Receipient Number {} , msg content {} and response received is {}",
										number, metaData.getBody(), responseString);
								break;
							}
						} else {
							log.error(
									"[FirstConnectt] Receipient Number {} , msg content {} and response received is {}",
									number, metaData.getBody(), responseString);
							break;
						}
						retryCount = 0;
					} catch (IOException e) {
						log.error("[FirstConnectt] Receipient Number {} , msg content {} and response received is {}",
								number, metaData.getBody(), responseString);
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("[FirstConnectt] Unable to send sms to : ", number,
							" with content : ", metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});
	}

	public Map<String, String> getQueryParams(SmsVendorConfiguration conf, String recepientMobile, String smsBody) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("username", conf.getUserName());
		queryParams.put("apikey", conf.getAuthkey());
		queryParams.put("route", conf.getRoute());
		queryParams.put("sender", conf.getSender());
		queryParams.put("mobile", recepientMobile);
		queryParams.put("message", smsBody);
		queryParams.put("apirequest", "Text");
		queryParams.put("format", "JSON");
		return queryParams;
	}

}
