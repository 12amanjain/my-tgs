package com.tgs.services.messagingService.runtime;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.gmail.Gmail;
import com.google.api.services.gmail.model.Message;
import com.tgs.services.base.configurationmodel.EmailVendorConfiguration;
import com.tgs.services.messagingService.datamodel.EmailMetaData;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class GoogleEmailService implements EmailService {

	private static final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();

	private static String userId = "me";

	@Override
	public void sendEmail(EmailMetaData metaData, EmailVendorConfiguration vendorConfig) {

		try {
			MimeMessage email = new MimeMessage(Session.getDefaultInstance(new Properties(), null));
			email.setFrom(new InternetAddress(metaData.getFromEmail()));
			String[] toMails = metaData.getToEmail().split(",");
			for (int i = 0; i < toMails.length; i++) {
				email.addRecipient(javax.mail.Message.RecipientType.TO, new InternetAddress(toMails[i].trim()));
			}

			if (StringUtils.isNotBlank(metaData.getCcEmail())) {
				String[] ccMails = metaData.getCcEmail().split(",");
				for (int i = 0; i < ccMails.length; i++) {
					email.addRecipient(javax.mail.Message.RecipientType.CC, new InternetAddress(ccMails[i].trim()));
				}
			}

			if (StringUtils.isNotBlank(metaData.getBccEmail())) {
				String[] bccMails = metaData.getBccEmail().split(",");
				for (int i = 0; i < bccMails.length; i++) {
					email.addRecipient(javax.mail.Message.RecipientType.BCC, new InternetAddress(bccMails[i].trim()));
				}
			}

			Multipart multipart = new MimeMultipart();
			MimeBodyPart mimeBodyPart = new MimeBodyPart();
			mimeBodyPart.setContent(metaData.getBody(), metaData.getType() + "; charset=utf-8");

			if (metaData.getAttachmentData() != null && metaData.getAttachmentData().getFileData() != null) {
				ByteArrayDataSource source =
						new ByteArrayDataSource(metaData.getAttachmentData().getFileData(), "application/octet-stream");
				MimeBodyPart mimeAttachmentPart = new MimeBodyPart();
				mimeAttachmentPart.setDataHandler(new DataHandler(source));
				mimeAttachmentPart.setFileName(metaData.getAttachmentData().getFileName());
				multipart.addBodyPart(mimeAttachmentPart);
			}

			multipart.addBodyPart(mimeBodyPart);
			email.setSubject(metaData.getSubject());
			email.setContent(multipart);

			ByteArrayOutputStream baoOut = new ByteArrayOutputStream();

			email.writeTo(baoOut);
			Message message = new Message().setRaw(Base64.encodeBase64URLSafeString(baoOut.toByteArray()));

			final NetHttpTransport HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();

			Gmail service = new Gmail.Builder(GoogleNetHttpTransport.newTrustedTransport(), JSON_FACTORY,
					getCredentials(HTTP_TRANSPORT, vendorConfig)).setApplicationName(vendorConfig.getApplicationName())
							.build();

			Message re = service.users().messages().send(userId, message).execute();
			log.info("Response status is {} , to {} , ccEMail {} , bccEmail {}, subject {}", re.getLabelIds(),
					metaData.getToEmail(), metaData.getCcEmail(), metaData.getBccEmail(), metaData.getSubject());


		} catch (MessagingException | IOException | GeneralSecurityException e) {
			log.error("Unable to send email to {} , ccEMail {} , bccEmail {}, subject {}", metaData.getToEmail(),
					metaData.getCcEmail(), metaData.getBccEmail(), metaData.getSubject(), e);
		}


	}

	/**
	 * Creates an authorized Credential object.
	 * 
	 * @param HTTP_TRANSPORT The network HTTP Transport.
	 * @return An authorized Credential object.
	 * @throws IOException If the credentials.json file cannot be found.
	 */
	private static Credential getCredentials(final NetHttpTransport HTTP_TRANSPORT,
			EmailVendorConfiguration vendorConfig) throws IOException {
		GoogleCredential cred = new GoogleCredential.Builder().setTransport(HTTP_TRANSPORT).setJsonFactory(JSON_FACTORY)
				.setClientSecrets(vendorConfig.getClientId(), vendorConfig.getClientSecret()).build()
				.setRefreshToken(vendorConfig.getRefreshToken());
		cred.refreshToken();
		return cred;
	}

}
