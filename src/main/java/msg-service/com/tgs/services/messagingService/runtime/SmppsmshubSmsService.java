package com.tgs.services.messagingService.runtime;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.SmsVendorConfiguration;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.messagingService.datamodel.sms.SmsMetaData;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SmppsmshubSmsService implements SmsService {

	@Autowired
	GeneralServiceCommunicator gnServiceComm;

	@Override
	public void sendSms(SmsMetaData metaData, SmsVendorConfiguration conf) {

		List<String> recepientNumbers = metaData.getRecipientNumbers();
		recepientNumbers.forEach(number -> {
			int retryCount = 3;
			String responseString = "";
			try {
				while (retryCount > 0) {
					HttpUtils httpUtils = HttpUtils.builder().urlString(conf.getUrl())
							.queryParams(getQueryParams(conf, number, metaData.getBody())).build();
					try {
						// STATUS:OK<br>CODE:100<br>INFO:SUBMITTED<br>UID:2beee1f1-38ae1e77-2b8c5070-13415f6
						responseString = (String) httpUtils.getResponse(null).orElse("");
						boolean validResponse = isValidResponse(responseString);
						if (validResponse) {
							log.info("[QuickSmart] Receipient Number {} , msg content {} and response received is {}",
									number, metaData.getBody(), responseString);
							break;
						} else {
							log.error("[QuickSmart] Receipient Number {} , msg content {} and response received is {}",
									number, metaData.getBody(), responseString);
							break;
						}
					} catch (IOException e) {
						log.error("[QuickSmart] Receipient Number {} , msg content {} and response received is {}",
								number, metaData.getBody(), responseString);
						retryCount--;
					}
				}
			} finally {
				if (retryCount == 1) {
					String noteMessage = StringUtils.join("[QuickSmart] Unable to send sms to : ", number,
							" with content : ", metaData.getBody(), "Response received is :", responseString);
					Note note = Note.builder().noteType(NoteType.SMSFAILURE).noteMessage(noteMessage).build();
					if (SystemContextHolder.getContextData().getUser() != null) {
						note.setUserId(SystemContextHolder.getContextData().getUser().getLoggedInUserId());
					}
					gnServiceComm.addNote(note);
				}
			}
		});
	}

	private Map<String, String> getQueryParams(SmsVendorConfiguration conf, String recepientMobile, String smsBody) {
		Map<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("user", conf.getUserName());
		queryParams.put("password", conf.getPassword());
		queryParams.put("senderid", conf.getSender());
		queryParams.put("route", conf.getRoute());
		queryParams.put("channel", conf.getAdditionalParameters().get(0));
		queryParams.put("DCS", conf.getAdditionalParameters().get(1));
		queryParams.put("flashsms", conf.getAdditionalParameters().get(2));
		queryParams.put("text", smsBody);
		queryParams.put("number", recepientMobile);
		return queryParams;
	}

	private static boolean isValidResponse(String responseString) {
		JSONObject response = new JSONObject(responseString);
		if (response.has("ErrorCode") && response.getString("ErrorCode").equals("000")) {
			return true;
		} else {
			return false;
		}
	}
}
