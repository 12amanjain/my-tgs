package com.tgs.services.messagingService.runtime;

import com.tgs.services.base.configurationmodel.WhatsAppConfiguration;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppMetaData;

public interface WhatsAppService {

    public void sendWhatsAppMessage(WhatsAppMetaData metaData, WhatsAppConfiguration config);
}
