package com.tgs.services.nss.analytics;

import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NoShowAnalyticsQuery {

	protected String pnr;
	protected String routeinfo;
	protected String firstname;
	protected String lastname;
	protected String title;
	protected String traveldate;
	protected String airline;
	protected String supplierid;
	protected String travellerstatus;
	protected String paxtype;
	protected String airtype;
	protected String bookingid;
	protected String userid;

	protected String emailid;
	protected String contactno;
	protected LocalDateTime createdon;

}
