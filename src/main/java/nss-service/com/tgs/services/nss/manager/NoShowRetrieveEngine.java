package com.tgs.services.nss.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.tgs.services.nss.datamodel.NoShowInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.gson.TypeAdapterFactory;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.datamodel.Conditions;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.RuntimeTypeAdapterFactory;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.nss.restmodel.NoShowBookingRetrieveRequest;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import com.tgs.services.oms.datamodel.air.AdditionalInfoFilter;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.air.ProcessedAirDetails;
import com.tgs.services.oms.restmodel.OrderResponse;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NoShowRetrieveEngine {

	@Value("${domain}")
	private String domain;

	@Value("${commProtocol}")
	private String protocol;

	private static String serverUrl;

	final protected String getServerUrl() {
		if (serverUrl == null) {
			serverUrl = new StringBuilder(protocol).append("://").append(domain).toString();
		}
		return serverUrl;
	}

	@Autowired
	OrderServiceCommunicator orderComm;

	@Autowired
	AirOrderItemCommunicator airOrderComm;

    public Map<String, NoShowInfo> findUniquePnrsMap(NoShowBookingRetrieveRequest postBookingRequest) {
        OrderResponse orderResponse = fetchOrders(postBookingRequest);
        Map<String, NoShowInfo> pnrMap = new HashMap<String, NoShowInfo>();
        if (orderResponse != null && CollectionUtils.isNotEmpty(orderResponse.getDetails())) {
            log.info("OrderItems  received for NoShowService: {}", orderResponse.getDetails().size());
            for (ProcessedBookingDetail processedDetail : orderResponse.getDetails()) {
                if (processedDetail.getOrder() != null && MapUtils.isNotEmpty(processedDetail.getItems())) {
                    String bookingId = processedDetail.getOrder().getBookingId();
                    List<AirOrderItem> airOrderItems = airOrderComm.findItems(bookingId);
                    for (AirOrderItem airOrderItem : airOrderItems) {
                        String pnr =
                                ObjectUtils.firstNonNull(airOrderItem.getTravellerInfo().get(0).getSupplierBookingId(),
                                        airOrderItem.getTravellerInfo().get(0).getPnr());
                        NoShowInfo noShowInfo = NoShowInfo.builder().supplierId(airOrderItem.getSupplierId()).build();
                        noShowInfo.setUserId(processedDetail.getOrder().getBookingUserId());
                        noShowInfo.setBookingId(processedDetail.getOrder().getBookingId());
                        noShowInfo.setCreatedOn(processedDetail.getOrder().getCreatedOn());
                        pnrMap.put(pnr, noShowInfo);
                    }
                }
            }
        }
        return pnrMap;
    }

	public OrderResponse fetchOrders(NoShowBookingRetrieveRequest postBookingRequest) {
		OrderResponse orderResponse = null;
		try {
			OrderFilter orderFilter = generateOrderFilter(postBookingRequest);
			String url = new StringBuilder(getServerUrl()).append("/oms/v1/orders").toString();
			log.info("Url for Order API is {}", url);
			orderResponse =
					processOrderResponse(doPostRequest(GsonUtils.getGson().toJson(orderFilter), "Orders", url, null));
		} catch (IOException e) {
			log.error("Error Occured while fetching orders due to {}", e.getMessage());
		}
		return orderResponse;
	}


    private OrderFilter generateOrderFilter(NoShowBookingRetrieveRequest postBookingRequest) {
        OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.SUCCESS))
                .bookingIds(postBookingRequest.getBookingIds())
                .itemFilter(AirOrderItemFilter.builder().departedOnAfterDate(postBookingRequest.getFromTravelDate())
                        .departedOnBeforeDate(postBookingRequest.getToTraveldate())
                        .supplierIds(postBookingRequest.getSupplierIds()).airlines(postBookingRequest.getAirlines())
                        .build())
                .build();

        if (CollectionUtils.isNotEmpty(postBookingRequest.getSourceIds())) {
            orderFilter.getItemFilter().setAdditionalInfoFilter(
                    AdditionalInfoFilter.builder().sourceIds(postBookingRequest.getSourceIds()).build());
        }
        return orderFilter;
    }

	@SuppressWarnings("unchecked")
	private OrderResponse processOrderResponse(String responseString) {
		List<TypeAdapterFactory> typeAdapters = new ArrayList<TypeAdapterFactory>();

		RuntimeTypeAdapterFactory<Conditions> processedItemAdapter =
				RuntimeTypeAdapterFactory.of(ProcessedItemDetails.class, "itemDetails");
		processedItemAdapter.registerSubtype(ProcessedAirDetails.class);
		typeAdapters.add(processedItemAdapter);
		GsonUtils gsonUtil = GsonUtils.builder().typeFactories(typeAdapters).build();
		OrderResponse response = gsonUtil.buildGson().fromJson(responseString, OrderResponse.class);

		return response;
	}


	private Map<String, String> getHeaderParams() {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "application/json");
		headerParams.put(SecurityConstants.HEADER_STRING,
				SecurityConstants.TOKEN_PREFIX + SystemContextHolder.getContextData().getHttpHeaders().getJwtToken());
		return headerParams;
	}

	protected <T> T doPostRequest(String request, String serviceName, String url, Class<T> type) throws IOException {
		T responseBody = null;
		HttpUtils httpUtils =
				HttpUtils.builder().urlString(url).postData(request).headerParams(getHeaderParams()).build();
		responseBody = httpUtils.getResponse(type).orElse(null);
		return responseBody;

	}

}
