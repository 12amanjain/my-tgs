package com.tgs.services.nss.servicehandler;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.nss.restcontroller.NoShowRefundEngine;
import com.tgs.services.nss.restmodel.NoShowBookingRetrieveResponse;
import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class NoShowRefundHandler extends ServiceHandler<NoShowRefundRequest, NoShowBookingRetrieveResponse> {


	@Autowired
	KafkaServiceCommunicator kafkaService;

	@Autowired
	NoShowRefundEngine refundEngine;


	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		refundEngine.processNoShowRefund(request);
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
