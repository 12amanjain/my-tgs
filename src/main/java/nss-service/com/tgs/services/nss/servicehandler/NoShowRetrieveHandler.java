package com.tgs.services.nss.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.nss.datamodel.NoShowInfo;
import com.tgs.services.nss.manager.NoShowRetrieveManager;
import com.tgs.services.nss.restmodel.NoShowRetrieveRequest;
import com.tgs.services.nss.restmodel.NoShowRetrieveResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NoShowRetrieveHandler extends ServiceHandler<NoShowRetrieveRequest, NoShowRetrieveResponse> {


	final static private int MAX_RETRIES = 10;

	@Autowired
	NoShowRetrieveManager noShowManager;


	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		List<Future<?>> futures = new ArrayList<>();
		for (Map.Entry<String, NoShowInfo> entry : request.getNoShowInfoMap().entrySet()) {
			Future<?> future = ExecutorUtils.getNoShowThreadPool().submit(() -> {
				noShowManager.checkNoShowTravellers(entry.getKey(), entry.getValue());
			});
			futures.add(future);
			if (futures.size() == 20) {
				for (Future<?> f : futures) {
					try {
						f.get(40, TimeUnit.SECONDS);
					} catch (Exception e) {
						log.info("Unable to get get response in 40 seconds due to {}", e.getMessage());
					}
				}
				futures.clear();
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {}


}
