package com.tgs.services.nss.sources.goair;

import com.tgs.services.nss.sources.AbstractNoShowRefundFactory;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;
import com.tgs.services.nss.restmodel.NoShowRefundRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class GoAirNoShowRefundFactory extends AbstractNoShowRefundFactory {

	public GoAirNoShowRefundFactory(NoShowRefundRequest noShowRefundRequest) {
		super(noShowRefundRequest);

	}

	@Override
	protected boolean refundNoShow() {
		GoAirNoShowRefundManager refundManager = GoAirNoShowRefundManager.builder().build();
		Document pnrResponse = refundManager.fetchNoShowPNRDetails(noShowRefundRequest);
		if (pnrResponse != null) {
			// refund no show fare
		}
		return false;
	}

}

