package com.tgs.services.nss.utils;

public class NoShowConstants {


	// header
	public static final String KEY_ACCEPT = "Accept";
	public static final String KEY_ACCEPT_ENCODING = "Accept-Encoding";
	public static final String KEY_ACCEPT_LANGUAGE = "Accept-Language";
	public static final String KEY_CACHE_CONTROL = "Cache-Control";
	public static final String KEY_CONNECTION = "Connection";
	public static final String KEY_USER_AGENT = "User-Agent";


	// value
	public static final String VALUE_KEEP_ALIVE = "keep-alive";
	public static final String VALUE_NO_CACHE = "no-cache";
	public static final String VALUE_ACCEPT_LANGUAGE = "en-GB,en-US;q=0.9,en;q=0.8";
	public static final String VALUE_ACCEPT_ENCODING = "gzip, deflate, br";
	public static final String VALUE_USER_AGENT =
			"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36";

	public static final String VALUE_ACCEPT =
			"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";


}
