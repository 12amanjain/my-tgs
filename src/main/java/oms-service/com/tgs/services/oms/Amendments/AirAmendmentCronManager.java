package com.tgs.services.oms.Amendments;

import com.tgs.filters.AmendmentFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.servicehandler.UpdateAirAmendmentHandler;
import com.tgs.services.oms.servicehandler.air.AirAmendmentCancellationHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class AirAmendmentCronManager {

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private GeneralCachingCommunicator cachingService;

	private AirOrderInfo orderInfo;

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	private AirAmendmentManager airAmendmentManager;

	private List<SegmentInfo> segmentInfoList;

	private List<SegmentInfo> previousSegments;

	@Autowired
	private UpdateAirAmendmentHandler updateAirAmendmentHandler;

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AirAmendmentCancellationHandler airAmendmentCancellationHandler;


	public void doCronCancellation() {
		ExecutorUtils.getGeneralPurposeThreadPool().submit(() -> {
			doAutoCancellation();
		});
	}

	private void doAutoCancellation() {
		// Fetch Last Created 15 minutes and Requested Status and CANCELLATION type
		List<Amendment> amendmentList = amendmentService.search(getAmendmentFilter());
		for (Amendment amendment : amendmentList) {
			// it should not assigned to anyone
			if (StringUtils.isBlank(amendment.getAssignedUserId())) {
				log.info("Doing AmendmentCron for amendmentid {} bookingid {}", amendment.getAmendmentId(),
						amendment.getBookingId());

				AirAmendmentPaxInfo request = fetchAmendmentRequestFromCache(amendment.getAmendmentId());
				if (request != null) {
					orderInfo = airAmendmentManager.getOrderInfoForAmendment(request.getBookingId(), request.getType());
					segmentInfoList = BookingUtils.getSegmentInfos(orderInfo.getTripInfoList());

					previousSegments = new ArrayList<>();
					for (SegmentInfo segmentInfo : segmentInfoList) {
						previousSegments.add(new GsonMapper<>(segmentInfo, SegmentInfo.class).convert());
					}

					if (amendment.getAmendmentType().equals(AmendmentType.CANCELLATION)) {
						AirCancellationRequest cancellationRequest = AirCancellationRequest.builder()
								.paxKeys(request.getPaxKeys()).segmentInfos(previousSegments).build();
						cancellationRequest.setBookingId(amendment.getBookingId());
						cancellationRequest.setAmendmentId(amendment.getAmendmentId());
						AmendmentResponse response = getCancellationAmendmentResponse(amendment);
						AirCancellationResponse cancellationResponse =
								getCancellationAmendmentReviewResponse(amendment);
						response.setAirCancellationReviewResponse(cancellationResponse);
						response.setAmendmentItems(amendmentList);
						if (BooleanUtils.isNotTrue(response.getIsCancelled())
								&& cancellationResponse.isAutoCancellationAllowed()) {
							log.info("Initalted AmendmentCron for amendmentid {} bookingid {} to supplier ",
									amendment.getAmendmentId(), amendment.getBookingId());
							airAmendmentCancellationHandler.setAmendment(amendment);
							airAmendmentCancellationHandler.doAutoCancellation(cancellationRequest, request,
									response, segmentInfoList);
						}
					}
				}
			}
		}
	}

	private AmendmentResponse getCancellationAmendmentResponse(Amendment amendment) {
		AmendmentResponse response =
				GsonUtils.getGson().fromJson(
						cachingService.get(
								CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONRESPONSE.name())
										.key(amendment.getAmendmentId()).build(),
								String.class, false, true, BinName.CONFIRMCANCEL.getName())
								.get(BinName.CONFIRMCANCEL.getName()),
						AmendmentResponse.class);
		if (response == null) {
			response = AmendmentResponse.builder().build();
			response.setIsCancelled(false);
		}
		return response;
	}

	private AirCancellationResponse getCancellationAmendmentReviewResponse(Amendment amendment) {
		return GsonUtils.getGson().fromJson(
				cachingService.get(
						CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREVIEWRESPONSE.name())
								.key(amendment.getAmendmentId()).build(),
						String.class, false, true, BinName.CANCELLATION.getName()).get(BinName.CANCELLATION.getName()),
				AirCancellationResponse.class);
	}

	private AmendmentFilter getAmendmentFilter() {
		return AmendmentFilter.builder().statusIn(Arrays.asList(AmendmentStatus.REQUESTED))
				.amendmentTypeIn(Arrays.asList(AmendmentType.CANCELLATION))
				.createdOnGreaterThan(LocalDateTime.now().minusMinutes(15)).build();
	}

	private AirAmendmentPaxInfo fetchAmendmentRequestFromCache(String amendmentId) {
		return GsonUtils.getGson()
				.fromJson(cachingService
						.get(CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREQUEST.name()).key(amendmentId)
								.build(), String.class, false, true, BinName.CANCELLATION.getName())
						.get(BinName.CANCELLATION.getName()), AirAmendmentPaxInfo.class);

	}


}
