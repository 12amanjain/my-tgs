package com.tgs.services.oms.Amendments;

import static com.tgs.services.base.enums.AmendmentType.*;
import static com.tgs.services.base.helper.SystemError.*;
import static com.tgs.services.oms.Amendments.AmendmentHelper.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.ListOutput;
import com.tgs.services.fms.datamodel.supplier.SupplierType;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.Amendments.Processors.AirAmendmentProcessor;
import com.tgs.services.oms.datamodel.InvoiceInfo;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.air.AirInvoiceDetail;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.AirAdditionalInfo;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentChecklist;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.ChecklistData;
import com.tgs.services.oms.datamodel.amendments.PWSReason;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.AmendmentInvoiceIdManager;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.restmodel.air.AirInvoiceRequest;
import com.tgs.services.oms.restmodel.air.AirInvoiceResponse;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentDetailResponse;
import com.tgs.services.oms.restmodel.air.AmendmentTravellerDetail;
import com.tgs.services.oms.restmodel.air.AmendmentTripCharges;
import com.tgs.services.oms.servicehandler.air.AirInvoiceHandler;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.GSTInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirAmendmentManager {

	@Autowired
	private AirOrderItemManager airOrderItemManager;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AirInvoiceHandler airInvoiceHandler;

	@Autowired
	private GeneralServiceCommunicator generalService;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	AmendmentInvoiceIdManager amendmentInvoiceIdManager;

	@Autowired
	private GstInfoService gstService;

	private boolean orderPending;

	private static final String RAISE_CHECKLIST_KEY = "RAISE_AMENDMENT_CHECKLIST";

	private static final String PROCESS_CHECKLIST_KEY = "PROCESS_AMENDMENT_CHECKLIST";

	private static final int NEGATIVE = -1;

	public AirOrderInfo getOrderInfoForAmendment(String bookingId, AmendmentType amdType) {

		DbOrder order = orderService.findByBookingId(bookingId);

		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

		List<TripInfo> tripInfoList =
				airOrderItemManager.findTripsByBookingIds(Collections.singletonList(bookingId)).get(bookingId);

		// checkPendingOrder(amdType, BookingUtils.getSegmentInfos(tripInfoList));

		if (CollectionUtils.isEmpty(tripInfoList))
			throw new CustomGeneralException(ORDER_NOT_FOUND);

		List<Document> documents = generalService.fetchDocument(
				CollectionServiceFilter.builder().fetchType(FetchType.CACHE).key(RAISE_CHECKLIST_KEY).build());

		List<AmendmentChecklist> agentChecklist = new ArrayList<>();
		if (!CollectionUtils.isEmpty(documents)) {
			ChecklistData data = GsonUtils.getGson().fromJson(documents.get(0).getData(), ChecklistData.class);
			agentChecklist =
					data.getData().stream().filter(ch -> ch.getType().equals(amdType)).collect(Collectors.toList());
		}

		AirSearchQuery searchQuery = BaseUtils.combineSearchQuery(BaseUtils.getSearchQueryFromTripInfos(tripInfoList));
		setSegmentGroupIds(tripInfoList, searchQuery);
		return AirOrderInfo.builder().tripInfoList(tripInfoList).searchQuery(searchQuery).bookingId(bookingId)
				.amendmentType(amdType).checklist(agentChecklist).build();
	}


	public AmendmentDetailResponse getAmendmentDetail(String amendmentId) {

		Amendment amendment = amendmentService.findByAmendmentId(amendmentId);

		if (amendment == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);

		List<DbAirOrderItem> orgOrderItems = airOrderItemService.findByBookingId(amendment.getBookingId());

		List<AirOrderItem> modItems =
				amendment.getModifiedInfo() == null || amendment.getModifiedInfo().getAirOrderItems() == null
						? new ArrayList<>()
						: amendment.getModifiedInfo().getAirOrderItems();
		User bookingUser = userService.getUserFromCache(amendment.getBookingUserId());
		AirAmendmentProcessor amendmentHandler =
				AmendmentHandlerFactory.initHandler(amendment, modItems, orgOrderItems, bookingUser);

		List<AirOrderItem> orderItems = amendmentHandler.patchAirOrderItems(false);

		amendmentHandler.getAmendmentDetails();

		orgOrderItems = new DbAirOrderItem().toDbList(orderItems);

		List<Note> notes = generalService.getNotes(NoteFilter.builder().amendmentId(amendmentId).build());

		List<SegmentInfo> segmentInfoList =
				AirBookingUtils.convertAirOrderItemListToSegmentInfoList(orgOrderItems, null);

		for (SegmentInfo segment : segmentInfoList) {
			for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
				if (traveller.getFareDetail() != null) {
					double discount = 0;
					if (UserRole.corporate(bookingUser.getRole())) {
						discount = AirBookingUtils.getGrossCommissionForPax(traveller, null, true);
						traveller.getFareDetail().getFareComponents().put(FareComponent.DS, discount);
					}
					traveller.getFareDetail().getFareComponents().put(FareComponent.NF,
							traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d)
									- discount);
				}
			}
		}

		List<Document> documents = generalService.fetchDocument(
				CollectionServiceFilter.builder().fetchType(FetchType.CACHE).key(RAISE_CHECKLIST_KEY).build());
		List<Document> processChecklistsDocs = generalService.fetchDocument(
				CollectionServiceFilter.builder().fetchType(FetchType.CACHE).key(PROCESS_CHECKLIST_KEY).build());

		List<AmendmentChecklist> checked = new ArrayList<>();
		List<AmendmentChecklist> processChecklist = new ArrayList<>();

		if (!CollectionUtils.isEmpty(documents)) {
			ChecklistData data = GsonUtils.getGson().fromJson(documents.get(0).getData(), ChecklistData.class);
			if (!CollectionUtils.isEmpty(amendment.getAdditionalInfo().getRaiseChecklistIds())) {
				checked = data.getData().stream()
						.filter(ch -> ch.getType().equals(amendment.getAmendmentType())
								&& amendment.getAdditionalInfo().getRaiseChecklistIds().contains(ch.getId()))
						.collect(Collectors.toList());
			}
		}

		if (!CollectionUtils.isEmpty(processChecklistsDocs)) {
			ChecklistData processChecklistData =
					GsonUtils.getGson().fromJson(processChecklistsDocs.get(0).getData(), ChecklistData.class);
			processChecklist = processChecklistData.getData().stream()
					.filter(ch -> ch.getType().equals(amendment.getAmendmentType())).collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(amendment.getAdditionalInfo().getProcessChecklistIds())) {
				processChecklist.forEach(ch -> ch
						.setChecked(amendment.getAdditionalInfo().getProcessChecklistIds().contains(ch.getId())));
			}
		}

		amendment.getActionList().removeIf(action -> !action.showOnProcess());

		SupplierType supplierType =
				fmsCommunicator.getAirlineInfo(segmentInfoList.get(0).getAirlineCode(false)).getIsLcc()
						? SupplierType.LCC
						: SupplierType.GDS;

		TripInfo tripInfo = new TripInfo();
		tripInfo.setSegmentInfos(segmentInfoList);

		AirSearchQuery searchQuery = BaseUtils.getSearchQueryFromTripInfo(tripInfo);

		FlightBasicFact fact = FlightBasicFact.createFact().generateFact(segmentInfoList.get(0));
		fact.setSupplierType(supplierType);
		fact.setAirType(searchQuery.getAirType());
		BaseUtils.createFactOnUser(fact, bookingUser);
		List<PWSReason> pwsReasons = null;
		AirConfiguratorInfo rule = fmsCommunicator.getAirConfigRuleInfo(AirConfiguratorRuleType.PWS_REASONS, fact);
		if (Objects.nonNull(rule) && Objects.nonNull(rule.getOutput())) {
			pwsReasons = ((ListOutput) rule.getOutput()).getValues();
		}
		return new AmendmentDetailResponse(segmentInfoList, amendment,
				CollectionUtils.isEmpty(notes) ? "" : notes.get(0).getNoteMessage(), checked, processChecklist,
				pwsReasons);
	}

	public Set<String> checkAndGetReIssueIds(String bookingId) {
		Set<String> reIssuedIds = new HashSet<>();
		List<AirOrderItem> orderItems = SuperBaseModel.toDomainList(airOrderItemService.findByBookingId(bookingId));
		if (!isParentBooking(orderItems))
			return reIssuedIds;
		orderItems.forEach(item -> item.getTravellerInfo().forEach(pax -> {
			if (!StringUtils.isBlank(pax.getNewBookingId())) {
				reIssuedIds.addAll(getReIssueIds(pax.getNewBookingId()));
			}
		}));
		return reIssuedIds;
	}

	private Set<String> getReIssueIds(String bookingId) {
		Set<String> reIssuedIds = new HashSet<>();
		reIssuedIds.add(bookingId);
		List<AirOrderItem> orderItems = SuperBaseModel.toDomainList(airOrderItemService.findByBookingId(bookingId));
		orderItems.forEach(item -> item.getTravellerInfo().forEach(pax -> {
			if (!StringUtils.isBlank(pax.getNewBookingId())) {
				reIssuedIds.addAll(getReIssueIds(pax.getNewBookingId()));
			}
		}));
		return reIssuedIds;
	}

	private boolean isParentBooking(List<AirOrderItem> orderItems) {
		for (AirOrderItem item : orderItems) {
			for (FlightTravellerInfo pax : item.getTravellerInfo()) {
				if (!StringUtils.isBlank(pax.getOldBookingId())) {
					return false;
				}
			}
		}
		return true;
	}

	private void setSegmentGroupIds(List<TripInfo> tripInfoList, AirSearchQuery searchQuery) {

		if (searchQuery.isIntl()) {
			tripInfoList
					.forEach(trip -> trip.getSegmentInfos().forEach(segment -> segment.setPricingGroupId((short) 1)));
		}

		tripInfoList.forEach(trip -> trip.getSegmentInfos().sort(Comparator.comparing(SegmentInfo::getSegmentNum)));

		short groupId = 0;
		for (TripInfo trip : tripInfoList) {
			for (SegmentInfo segment : trip.getSegmentInfos()) {
				Map<FareComponent, Double> map =
						segment.getBookingRelatedInfo().getTravellerInfo().get(0).getFareDetail().getFareComponents();
				if (!MapUtils.isEmpty(map) && map.containsKey(FareComponent.BF))
					groupId++;
				segment.setPricingGroupId(groupId);
			}
		}
	}

	public void checkPendingOrder(AmendmentType amendmentType, List<SegmentInfo> segmentInfoList) {
		orderPending = false;
		for (SegmentInfo segmentInfo : segmentInfoList) {
			if (!segmentInfo.getBookingRelatedInfo().getStatus().validAmdTypes().contains(amendmentType)) {
				orderPending = true;
				return;
			}
		}
	}

	public void checkPaxStatus(String bookingId, AirOrderInfo airOrderInfo, boolean validate, AmendmentType amdType) {

		Map<Long, Set<Long>> keyMap = new HashMap<>();

		AmendmentFilter filter = AmendmentFilter.builder().bookingIdIn(Collections.singletonList(bookingId)).build();

		List<Amendment> amendments = amendmentService.search(filter).stream().filter(
				a -> !a.getStatus().equals(AmendmentStatus.SUCCESS) && !a.getStatus().equals(AmendmentStatus.REJECTED))
				.collect(Collectors.toList());

		for (Amendment amd : amendments) {
			if (!CollectionUtils.isEmpty(amd.getAirAdditionalInfo().getPaxKeys()))
				keyMap = AirAdditionalInfo.mergeKeyMap(keyMap, amd.getAirAdditionalInfo().getKeyMap());
		}

		Map<Long, Set<Long>> finalKeyMap = keyMap;

		airOrderInfo.getTripInfoList().forEach(tripInfo -> {
			if (!CollectionUtils.isEmpty(tripInfo.getSegmentInfos())) {
				checkPendingOrder(amdType, BookingUtils.getSegmentInfos(Arrays.asList(tripInfo)));
				tripInfo.getSegmentInfos().forEach(s -> {
					if (!CollectionUtils.isEmpty(s.getTravellerInfo())) {
						s.getTravellerInfo().forEach(pax -> {
							if (AirAdditionalInfo.amdContainsPax(finalKeyMap, Long.valueOf(s.getId()), pax.getId())) {
								if (validate)
									throw new CustomGeneralException(AMENDMENT_EXISTS);
								else
									pax.setStatus(TravellerStatus.AMENDMENT_IN_PROGRESS);
							}
							if (orderPending) {
								if (validate)
									throw new CustomGeneralException(CANNOT_RAISE_AMENDMENT);
								// If pax status is Cancelled , and user can not raise further amendments then instead
								// of showing "Order_Pending", showing "Cancelled" is more understandable
								else if (!TravellerStatus.CANCELLED.equals(pax.getStatus()))
									pax.setStatus(TravellerStatus.ORDER_PENDING);
							}
							if (amdType.equals(CORRECTION)) {
								pax.setStatus(null);
							} else if (pax.getStatus() != null) {
								if (validate)
									throw new CustomGeneralException(PAX_NOT_CONFIRMED);
							}
						});
					}
				});
			}
		});
	}

	public AirInvoiceResponse getAmendmentInvoiceDetails(String amdId, String invoiceId) throws Exception {
		Amendment amd = amendmentService.findByAmendmentId(amdId);
		if (invoiceId == null && amd.getAdditionalInfo().getInvoiceId() == null) {
			invoiceId =
					amendmentInvoiceIdManager.getInvoiceId(amd, Product.getProductMetaInfoFromId(amd.getBookingId()));
			amd.getAdditionalInfo().setInvoiceId(invoiceId);
			amendmentService.saveWithoutProcessedOn(amd);
		}
		Amendment amendment = new GsonMapper<Amendment>(amd, null, Amendment.class).convert();
		List<AirOrderItem> amendmentSnapshot =
				getAmendmentSnapshot(amendment, !amendment.getAmendmentType().equals(CORRECTION));
		AirInvoiceRequest request =
				new AirInvoiceRequest(amendment.getBookingId(), InvoiceType.AGENCY, amendmentSnapshot);
		request.setInvoiceId(invoiceId != null ? invoiceId : amendment.getAdditionalInfo().getInvoiceId());
		AirInvoiceResponse invoiceResponse = new AirInvoiceResponse();
		airInvoiceHandler.initData(request, invoiceResponse);
		invoiceResponse = airInvoiceHandler.getResponse();
		invoiceResponse.setAmendmentId(amendment.getAmendmentId());
		invoiceResponse.setCreatedOn(amendment.getProcessedOn());
		invoiceResponse.setInvoiceId(invoiceId != null ? invoiceId : amendment.getAdditionalInfo().getInvoiceId());
		User bookingUser = userService.getUserFromCache(amendment.getBookingUserId());
		GeneralBasicFact fact = GeneralBasicFact.builder().build();
		fact.generateFact(bookingUser.getRole());
		FareBreakUpConfigOutput fbConfig = generalService.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);

		DbOrder order = orderService.findByBookingId(amendment.getBookingId());

		/**
		 * This is to override from Details based on amendment createdon date
		 */
		if (!UserUtils.DEFAULT_PARTNERID.equals(bookingUser.getPartnerId())) {
			invoiceResponse.setFrom(getPartnerInvoice(bookingUser));
		} else if (amendment.getAmount() < 0 && (AmendmentType.CANCELLATION.equals(amendment.getAmendmentType())
				|| AmendmentType.FULL_REFUND.equals(amendment.getAmendmentType())
				|| AmendmentType.NO_SHOW.equals(amendment.getAmendmentType())
				|| AmendmentType.VOIDED.equals(amendment.getAmendmentType())
				|| AmendmentType.CORRECTION.equals(amendment.getAmendmentType()))) {
			invoiceResponse.setFrom(AirInvoiceHandler
					.getClientInvoice((ClientGeneralInfo) generalService.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
							GeneralBasicFact.builder().applicableTime(order.getCreatedOn()).build())));
		} else {
			invoiceResponse.setFrom(AirInvoiceHandler
					.getClientInvoice((ClientGeneralInfo) generalService.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
							GeneralBasicFact.builder().applicableTime(amendment.getCreatedOn()).build())));
		}

		GstInfo gstInfo = null;
		DbOrderBilling dbGst = gstService.findByBookingId(amendment.getBookingId());
		if (dbGst != null) {
			gstInfo = dbGst.toDomain();
		}

		for (AirInvoiceDetail pax : invoiceResponse.getTravellerWiseDetail()) {
			double oldSSCharges = 0.0;
			pax.setAmendmentType(amendment.getAmendmentType());
			FareDetail fd = pax.getTravellerInfo().getFareDetail();
			fd.getFareComponents().put(FareComponent.CGST, 0d);
			fd.getFareComponents().put(FareComponent.IGST, 0d);
			fd.getFareComponents().put(FareComponent.SGST, 0d);
			OmsHelper.updateGstComponents(fd, userService.getUserFromCache(amendment.getBookingUserId()), gstInfo);
			if (!amendment.getAmendmentType().equals(CORRECTION)) {
				try {
					oldSSCharges = calcOldSSFares(fd);
					fd.getFareComponents().replaceAll((fc, val) -> Math.abs(val));
				} catch (Exception e) {
					log.error("FareComponent replaceAll for bookingId {} , FareComponent", amendment.getBookingId(),
							fd);
				}

			}
			if (amendment.getAmendmentType().equals(SSR)) {
				fd.getFareComponents().put(FareComponent.AAF,
						fd.getFareComponents().getOrDefault(FareComponent.AAF, 0d)
								+ fd.getFareComponents().getOrDefault(FareComponent.SP, 0d)
								+ fd.getFareComponents().getOrDefault(FareComponent.BP, 0d)
								+ fd.getFareComponents().getOrDefault(FareComponent.MP, 0d) - oldSSCharges);
			}
			fd.setFareComponents(fd.populateMappedFareComponent(fbConfig));
			fd.getFareComponents().computeIfPresent(FareComponent.NCM,
					(fc, val) -> val - fd.getFareComponents().get(FareComponent.TDS));
		}
		return invoiceResponse;
	}


	private double calcOldSSFares(FareDetail fd) {
		double oldSnapshotPrices = 0.0;
		for (Map.Entry<FareComponent, Double> entry : fd.getFareComponents().entrySet()) {
			if ((FareComponent.BP.equals(entry.getKey()) || FareComponent.SP.equals(entry.getKey())
					|| FareComponent.MP.equals(entry.getKey())) && entry.getValue() < 0.0) {
				oldSnapshotPrices += Math.abs(entry.getValue());
			}
		}
		return oldSnapshotPrices;
	}

	public List<AirOrderItem> getAmendmentSnapshot(Amendment originalAmendment, boolean keepOldTF) {
		// Creating a copy of amendments since we are modifying the original amendment snapshots here
		Amendment amendment = new GsonMapper<Amendment>(originalAmendment, null, Amendment.class).convert();
		// User in context data will be null while running amendment accounting job. Ip check validation for nulll user
		// in case of job has already been added
		if (SystemContextHolder.getContextData().getEmulateOrLoggedInUserId() != null)
			UserServiceHelper.checkAndReturnAllowedUserId(
					SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
					Arrays.asList(amendment.getBookingUserId()));
		if (amendment.getStatus().equals(AmendmentStatus.SUCCESS)) {
			List<AirOrderItem> old =
					filterItemsNPax(amendment, amendment.getAirAdditionalInfo().getOrderPreviousSnapshot());
			List<AirOrderItem> nuw =
					filterItemsNPax(amendment, amendment.getAirAdditionalInfo().getOrderNextSnapshot());
			nuw.forEach(item -> item.getTravellerInfo().forEach(pax -> {
				Map<FareComponent, Double> fareComponents = pax.getFareDetail().getFareComponents();
				Map<FareComponent, Double> oldFcs = !CollectionUtils.isEmpty(old)
						? old.stream().filter(a -> a.getId().equals(item.getId())).findFirst().get().getTravellerInfo()
								.stream().filter(p -> p.getId().equals(pax.getId())).findFirst().get().getFareDetail()
								.getFareComponents()
						: new HashMap<>();
				Stream.of(FareComponent.values()).forEach(fc -> {
					if (fc.equals(FareComponent.TF) && keepOldTF) {
						fareComponents.put(fc, oldFcs.get(FareComponent.TF));
						return;
					}
					fareComponents.put(fc, fareComponents.getOrDefault(fc, 0d) - oldFcs.getOrDefault(fc, 0d));
				});
			}));
			return nuw;
		} else
			throw new CustomGeneralException(SystemError.AMENDMENT_IN_PROGRESS);
	}

	public Set<String> setPSSH(List<String> amdIds, LocalDate date) {
		AmendmentFilter filter;
		Set<String> out = new HashSet<>();
		if (org.apache.commons.collections.CollectionUtils.isEmpty(amdIds)) {
			filter = AmendmentFilter.builder()
					.amendmentTypeIn(Arrays.asList(AmendmentType.CANCELLATION, VOIDED, NO_SHOW, SSR, CORRECTION,
							MISCELLANEOUS, REISSUE, FARE_CHANGE))
					.createdOnBeforeDateTime(LocalDateTime.of(date, LocalTime.MAX))
					.createdOnAfterDateTime(LocalDateTime.of(date, LocalTime.MIN)).build();
		} else {
			filter = AmendmentFilter.builder().amendmentIdIn(amdIds).build();
		}
		List<Amendment> amendments = amendmentService.search(filter);
		amendments = amendments.stream()
				.filter(x -> x.getStatus().equals(AmendmentStatus.SUCCESS)
						&& CollectionUtils.isEmpty(x.getAirAdditionalInfo().getOrderPreviousSnapshot()))
				.collect(Collectors.toList());
		for (Amendment amendment : amendments) {
			List<AirOrderItem> audits = airOrderItemManager.getAuditEntries(amendment.getBookingId());
			audits.sort(Comparator.comparing(x -> x.getProcessedOn()));
			Map<Long, List<AirOrderItem>> auditMap = audits.stream().collect(Collectors.groupingBy(x -> x.getId()));
			List<AirOrderItem> oldSSH = new ArrayList<>();
			for (Map.Entry<Long, List<AirOrderItem>> entry : auditMap.entrySet()) {
				List<AirOrderItem> items = entry.getValue();
				for (int i = 0; i < items.size() - 1; i++) {
					if (audits.get(i + 1).getProcessedOn().isAfter(amendment.getCreatedOn())
							&& audits.get(i).getProcessedOn().isBefore(amendment.getCreatedOn())) {
						oldSSH.add(audits.get(i));
						break;
					}
				}
			}
			if (!CollectionUtils.isEmpty(oldSSH)) {
				amendment.getAirAdditionalInfo().setOrderPreviousSnapshot(oldSSH);
				amendmentService.save(amendment);
				out.add(amendment.getAmendmentId());
			}
		}
		return out;
	}

	public void processAmendment(Amendment amendment, AirCancellationResponse cancellationReviewResponse,
			boolean isConfirm, boolean isNewFlow) {

		List<DbAirOrderItem> orgAirOrderItems = airOrderItemService.findByBookingId(amendment.getBookingId());
		AirAmendmentProcessor amendmentHandler =
				AmendmentHandlerFactory.initHandler(amendment, createModifiedItems(cancellationReviewResponse),
						orgAirOrderItems, userService.getUserFromCache(amendment.getBookingUserId()));
		amendmentHandler.setCancellationNewFlow(isNewFlow);
		amendmentHandler.setAmendment(amendment);
		amendmentHandler.patchAirOrderItems(false);
		amendmentHandler.process();
		cancellationReviewResponse.getCancellationDetail().setTotalAmountToRefund(amendment.getAmount());
		Double totalCancellationFee = getCancellationFee(cancellationReviewResponse);
		cancellationReviewResponse.getCancellationDetail().setTotalCancellationFare(totalCancellationFee);
		if (isConfirm)
			amendment.getModifiedInfo().setAirOrderItems(amendmentHandler.patchedItems);
	}

	private Double getCancellationFee(AirCancellationResponse cancellationReviewResponse) {
		if (cancellationReviewResponse != null && cancellationReviewResponse.getCancellationDetail() != null
				&& BooleanUtils
						.isTrue(cancellationReviewResponse.getCancellationDetail().getAutoCancellationAllowed())) {
			AtomicDouble cancellationFee = new AtomicDouble(0);
			cancellationReviewResponse.getCancellationDetail().getTravellers().forEach((segmentId, travellers) -> {
				travellers.forEach(traveller -> {
					Map<FareComponent, Double> fareComponents = traveller.getFareDetail().getFareComponents();
					for (FareComponent fareComponent : fareComponents.keySet()) {
						if (FareComponent.getCancellationComponents().contains(fareComponent)) {
							cancellationFee
									.getAndAdd(fareComponent.getAmount(fareComponents.getOrDefault(fareComponent, 0d)));
						}
					}
				});
			});
			return cancellationFee.doubleValue();
		}
		return null;
	}

	// this is to update cancellation fee and child pnr from FMS to OMS
	private List<AirOrderItem> createModifiedItems(AirCancellationResponse cancellationReviewResponse) {
		List<AirOrderItem> airOrderItems = new ArrayList<AirOrderItem>();
		for (Map.Entry<Long, List<FlightTravellerInfo>> entry : cancellationReviewResponse.getCancellationDetail()
				.getTravellers().entrySet()) {
			AirOrderItem airOrderItem = AirOrderItem.builder().build();
			airOrderItem.setId(entry.getKey());
			List<FlightTravellerInfo> flightTravellerList = new ArrayList<FlightTravellerInfo>();
			for (FlightTravellerInfo flightTravellerInfo : entry.getValue()) {
				FlightTravellerInfo flightTravellerInfoCopy =
						new GsonMapper<>(flightTravellerInfo, FlightTravellerInfo.class).convert();
				FareDetail fareDetail = new FareDetail();
				fareDetail.getFareComponents().put(FareComponent.ACF,
						flightTravellerInfo.getFareDetail().getFareComponents().get(FareComponent.ACF));
				fareDetail.getFareComponents().put(FareComponent.CCF,
						flightTravellerInfo.getFareDetail().getFareComponents().get(FareComponent.CCF));
				fareDetail.getFareComponents().put(FareComponent.CCFT,
						flightTravellerInfo.getFareDetail().getFareComponents().get(FareComponent.CCFT));
				flightTravellerInfoCopy.setFareDetail(fareDetail);
				flightTravellerList.add(flightTravellerInfoCopy);
			}
			airOrderItem.setTravellerInfo(flightTravellerList);
			airOrderItems.add(airOrderItem);
		}
		return airOrderItems;
	}

	private InvoiceInfo getPartnerInvoice(User user) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		User partner = user;
		if (user != null && !UserUtils.DEFAULT_PARTNERID.equals(user.getPartnerId())) {
			partner = userService.getUserFromCache(user.getPartnerId());
			invoiceInfo = buildInvoiceInfoFromUser(partner);
		}
		return invoiceInfo;
	}

	private InvoiceInfo buildInvoiceInfoFromUser(User user) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		AddressInfo addressInfo = AddressInfo.builder().build();
		CityInfo cityInfo = CityInfo.builder().build();
		if (user.getAddressInfo() != null) {
			addressInfo = user.getAddressInfo();
			if (addressInfo.getCityInfo() != null) {
				cityInfo = addressInfo.getCityInfo();
			}
		}
		GSTInfo gstInfo = GSTInfo.builder().build();
		if (user.getGstInfo() != null) {
			gstInfo = user.getGstInfo();
		}
		invoiceInfo = InvoiceInfo.builder().name(user.getName()).email(user.getEmail())
				.phone(ObjectUtils.firstNonNull(user.getPhone(), user.getMobile())).address(addressInfo.getAddress())
				.city(cityInfo.getName()).state(cityInfo.getState()).pincode(addressInfo.getPincode())
				.gstNumber(gstInfo.getGstNumber()).build();
		return invoiceInfo;
	}

	public List<AmendmentTripCharges> buildTripCharges(AirOrderInfo airOrderInfo, Amendment amendment) {
		List<AmendmentTripCharges> amendmentTripCharges = new ArrayList<>();
		List<DbAirOrderItem> airOrderItems = new ArrayList<>();
		if (amendment.getModifiedInfo() != null
				&& !CollectionUtils.isEmpty(amendment.getModifiedInfo().getAirOrderItems())) {
			for (AirOrderItem airOrderItem : amendment.getModifiedInfo().getAirOrderItems()) {
				airOrderItems.add(new DbAirOrderItem().from(airOrderItem));
			}
			List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(airOrderItems, new ArrayList<>()));
			for (TripInfo tripInfo : tripInfos) {
				AmendmentTripCharges tripCharges = AmendmentTripCharges.builder().build();
				tripCharges.setSource(tripInfo.getDepartureAirportCode());
				tripCharges.setDestination(tripInfo.getArrivalAirportCode());
				tripCharges.setDepartureDate(tripInfo.getDepartureTime());
				tripCharges.setFlightNumbers(tripInfo.getFlightNumberSet());
				tripCharges.setAirlines(tripInfo.getAirlineInfo(false));
				tripCharges.setTravellers(
						buildTravellers(tripInfo, amendment.getModifiedInfo().getPaxWiseAmount(), amendment));
				amendmentTripCharges.add(tripCharges);
			}
		}
		return amendmentTripCharges;
	}

	protected List<AmendmentTravellerDetail> buildTravellers(TripInfo tripInfo, Map<String, Double> paxWiseRefundAmount,
			Amendment amendment) {
		List<AmendmentTravellerDetail> travellersDetails = new ArrayList<AmendmentTravellerDetail>();
		for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
			String segmentId = segmentInfo.getId();
			for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
				String paxId = segmentId.concat("_").concat(travellerInfo.getId().toString());
				FareDetail fd = travellerInfo.getFareDetail();
				AmendmentTravellerDetail travellerDetail = getTravellerDetail(travellersDetails, travellerInfo);
				if (Objects.isNull(travellerDetail)) {
					travellerDetail = AmendmentTravellerDetail.builder().firstName(travellerInfo.getFirstName())
							.lastName(travellerInfo.getLastName()).build();
					setCharges(travellerDetail, paxWiseRefundAmount, fd, paxId, amendment);
					travellersDetails.add(travellerDetail);
				} else {
					setCharges(travellerDetail, paxWiseRefundAmount, fd, paxId, amendment);
				}
			}
		}
		return travellersDetails;
	}

	private void setCharges(AmendmentTravellerDetail travellerDetail, Map<String, Double> paxWiseRefundAmount,
			FareDetail fd, String paxId, Amendment amendment) {
		travellerDetail.setAmendmentCharges(
				travellerDetail.getAmendmentCharges() + fd.getComponentsSum(FareComponent.getCancellationComponents()));
		travellerDetail.setRefundableamount(
				(travellerDetail.getRefundableamount() + NEGATIVE * paxWiseRefundAmount.get(paxId)));
		travellerDetail.setTotalFare(travellerDetail.getRefundableamount() + travellerDetail.getAmendmentCharges());
	}

	private AmendmentTravellerDetail getTravellerDetail(List<AmendmentTravellerDetail> travellersDetails,
			FlightTravellerInfo travellerInfo) {
		AmendmentTravellerDetail travellerDetail = null;
		if (!CollectionUtils.isEmpty(travellersDetails)) {
			for (AmendmentTravellerDetail traveller : travellersDetails) {
				if (StringUtils.equalsIgnoreCase(traveller.getFirstName(), travellerInfo.getFirstName())
						&& StringUtils.equalsIgnoreCase(traveller.getLastName(), travellerInfo.getLastName())) {
					travellerDetail = traveller;
				}
			}
		}
		return travellerDetail;
	}

}
