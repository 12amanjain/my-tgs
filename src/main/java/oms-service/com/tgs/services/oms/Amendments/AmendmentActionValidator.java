package com.tgs.services.oms.Amendments;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Enums;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;

@Service
public class AmendmentActionValidator {

	@Autowired
	private UserServiceCommunicator userService;

	public Set<AmendmentAction> validActions(User loggedInUser, Amendment amendment) {

		if (loggedInUser == null)
			return new HashSet<>();

		List<AreaRoleMapping> areaRoleMappingList =
				userService.getAreaRoleMapByAreaRole(ApplicationArea.AMENDMENT.getAreaRoles());
		Map<UserRole, Set<AreaRole>> map = AreaRoleMapping.getMapByUserRole(areaRoleMappingList);
		UserRole userRole = loggedInUser.getRole();
		TreeSet<AmendmentAction> actions = new TreeSet<>(Comparator.comparing(Enum::name));
		List<AmendmentAction> notApplicableActions = getNotApplicableActions();
		boolean isAssignedUser = loggedInUser.getUserId().equals(amendment.getAssignedUserId());
		boolean isRaiseUser = loggedInUser.getUserId().equals(amendment.getBookingUserId())
				|| loggedInUser.getUserId().equals(amendment.getLoggedInUserId());
		boolean isProcessor = false;
		boolean isAdmin = false;

		if (map.containsKey(userRole)) {
			isProcessor = map.get(userRole).contains(AreaRole.AMENDMENT_PROCESSOR);
			isAdmin = map.get(userRole).contains(AreaRole.AMENDMENT_ADMIN);
		}

		switch (amendment.getStatus()) {

			case REQUESTED:
				if (isRaiseUser) {
					actions.add(AmendmentAction.ABORT);
					actions.add(AmendmentAction.VIEW);
				}
				if (isProcessor) {
					actions.add(AmendmentAction.ASSIGNME);
				}
				if (isAdmin) {
					actions.add(AmendmentAction.ASSIGN);
					actions.add(AmendmentAction.ASSIGNME);
				}
				break;


			case ASSIGNED:
				if (isAssignedUser) {
					actions.add(AmendmentAction.PROCESS);
					actions.add(AmendmentAction.PENDING_WITH_SUPPLIER);
					actions.add(AmendmentAction.ABORT);
				}
				if (isAdmin) {
					actions.add(AmendmentAction.ASSIGN);
					if (!isAssignedUser)
						actions.add(AmendmentAction.ASSIGNME);
				}
				if (isRaiseUser && !amendment.getLoggedInUserId().equals(amendment.getAssignedUserId())) {
					actions.add(AmendmentAction.VIEW);
				}
				break;


			case PROCESSING:
				if (isAssignedUser) {
					actions.add(AmendmentAction.PROCESS);
					actions.add(AmendmentAction.PENDING_WITH_SUPPLIER);
					actions.add(AmendmentAction.ABORT);
					actions.add(AmendmentAction.MERGE);
				}
				if (isAdmin) {
					actions.add(AmendmentAction.ASSIGN);
					if (!isAssignedUser)
						actions.add(AmendmentAction.ASSIGNME);
				}
				if (isRaiseUser && !amendment.getLoggedInUserId().equals(amendment.getAssignedUserId())) {
					actions.add(AmendmentAction.VIEW);
				}
				break;


			case PENDING_WITH_SUPPLIER:
				if (isRaiseUser) {
					actions.add(AmendmentAction.VIEW);
				}
				if (isProcessor) {
					actions.add(AmendmentAction.ASSIGNME);
					actions.add(AmendmentAction.VIEW);
				}
				if (isAdmin) {
					actions.add(AmendmentAction.VIEW);
					actions.add(AmendmentAction.ASSIGN);
					actions.add(AmendmentAction.ASSIGNME);
				}
				break;


			case PENDING_REASSIGN:
				if (isRaiseUser) {
					actions.add(AmendmentAction.VIEW);
				}
				if (isProcessor) {
					actions.add(AmendmentAction.ASSIGNME);
				}
				if (isAdmin) {
					actions.add(AmendmentAction.ASSIGN);
					actions.add(AmendmentAction.ASSIGNME);
				}
				break;


			case REJECTED:
				if (isRaiseUser || isProcessor || isAdmin) {
					actions.add(AmendmentAction.VIEW);
				}
				break;


			case PAYMENT_FAILED:
				if (isRaiseUser || isAssignedUser || isAdmin) {
					actions.add(AmendmentAction.VIEW);
				}
				if (isAssignedUser) {
					actions.add(AmendmentAction.ABORT);
					actions.add(AmendmentAction.MERGE);
				}
				if (isAdmin) {
					actions.add(AmendmentAction.ASSIGN);
					if (!isAssignedUser)
						actions.add(AmendmentAction.ASSIGNME);
				}
				break;

			case PAYMENT_SUCCESS:
				if (isRaiseUser || isAssignedUser || isAdmin) {
					actions.add(AmendmentAction.VIEW);
				}
				if (isAssignedUser) {
					actions.add(AmendmentAction.MERGE);
				}
				break;

			case SUCCESS:
				if (isRaiseUser || isProcessor || isAdmin) {
					actions.add(AmendmentAction.VIEW);
				}
				break;

			default:
				break;
		}
		actions.removeAll(notApplicableActions);
		return actions;
	}

	private List<AmendmentAction> getNotApplicableActions() {
		List<String> clientBasedNAAmendmentActionList = OmsHelper.getClientInfo().getNAAmendmentActions();
		List<AmendmentAction> notApplicableActions = new ArrayList<AmendmentAction>();
		if (CollectionUtils.isNotEmpty(clientBasedNAAmendmentActionList)) {
			for (String entry : clientBasedNAAmendmentActionList) {
				AmendmentAction action = Enums.getIfPresent(AmendmentAction.class, entry).orNull();
				if (action != null) {
					notApplicableActions.add(action);
				}
			}
		}
		return notApplicableActions;
	}
}
