package com.tgs.services.oms.Amendments;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.commons.compress.utils.Lists;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.datamodel.UserAssignment;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;

public final class AmendmentHelper {

	public static boolean isQuotation(Amendment amendment) {
		return amendment.getAmendmentType().equals(AmendmentType.CANCELLATION_QUOTATION)
				|| amendment.getAmendmentType().equals(AmendmentType.REISSUE_QUOTATION);
	}

	public static boolean isFareChange(Amendment amendment) {
		return amendment.getAmendmentType().equals(AmendmentType.FARE_CHANGE);

	}

	public static List<AmendmentStatus> assignedStatuses() {
		List<AmendmentStatus> set = new ArrayList<>();
		set.add(AmendmentStatus.ASSIGNED);
		set.add(AmendmentStatus.PROCESSING);
		set.add(AmendmentStatus.PAYMENT_FAILED);
		return set;
	}

	public static boolean returnTDS(LocalDateTime bookingDate) {
		return bookingDate.getMonth().equals(LocalDate.now().getMonth());
	}

	public static List<AirOrderItem> filterOrderItems(Amendment amendment, List<AirOrderItem> airOrderItems) {
		if (airOrderItems != null)
			return airOrderItems.stream().filter(a -> amendment.getAirAdditionalInfo().amdContainsSegment(a.getId()))
					.collect(Collectors.toList());
		return Lists.newArrayList();
	}

	public static void filterPax(Amendment amendment, List<AirOrderItem> airOrderItems) {
		airOrderItems.forEach(a -> {
			Long sId = a.getId();
			a.setTravellerInfo(a.getTravellerInfo().stream()
					.filter(t -> amendment.getAirAdditionalInfo().amdContainsPax(sId, t.getId()))
					.collect(Collectors.toList()));
		});
	}

	public static List<AirOrderItem> filterItemsNPax(Amendment amendment, List<AirOrderItem> airOrderItems) {
		airOrderItems = filterOrderItems(amendment, airOrderItems);
		filterPax(amendment, airOrderItems);
		return airOrderItems;
	}

	public static void updateAssignments(String newUserId, Amendment amendment) {
		LocalDateTime now = LocalDateTime.now();
		Optional<UserAssignment> releaseUser = amendment.getAdditionalInfo().getUserAssignments().stream()
				.filter(x -> x.getReleaseTime() == null).findFirst();
		releaseUser.ifPresent(userAssignment -> userAssignment.setReleaseTime(now));
		if (newUserId != null) {
			amendment.getAdditionalInfo().getUserAssignments().add(new UserAssignment(newUserId, now, null));
		}
	}
}
