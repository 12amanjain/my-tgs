package com.tgs.services.oms.Amendments;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.messagingService.datamodel.AmendmentMailAttribute;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.ums.datamodel.User;

@Service
public class AmendmentMessagingClient {

	@Autowired
	private MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private OrderManager orderManager;

	private Order order;

	private User user;

	@Value("${envname}")
	private String envName;

	@Value("${domain}")
	private String domain;

	public void sendMail(Amendment amendment, EmailTemplateKey emailTemplateKey, List<AirOrderItem> airOrderItemList) {
		if (emailTemplateKey == null)
			return;
		AbstractMessageSupplier<AmendmentMailAttribute> msgAttributes =
				new AbstractMessageSupplier<AmendmentMailAttribute>() {
					@Override
					public AmendmentMailAttribute get() {
						setDependencies(amendment);
						AmendmentMailAttribute mailAttributes = AmendmentMailAttribute.builder().build();
						mailAttributes.setToEmailId(user.getEmail());
						mailAttributes.setKey(emailTemplateKey.name());
						mailAttributes.setAmdId(amendment.getAmendmentId());
						mailAttributes.setBookingId(amendment.getBookingId());
						mailAttributes.setLink(link(amendment.getAmendmentId()));
						mailAttributes.setReason(amendment.getAdditionalInfo().getFinishingNotes());
						mailAttributes.setAgentId(user.getUserId());
						mailAttributes.setAgentName(user.getName());
						mailAttributes.setPartnerId(amendment.getPartnerId());
						mailAttributes.setAmendmentComment(amendment.getAdditionalInfo().getAgentRemarks());
						mailAttributes.setAmendmentStatus(amendment.getStatus().name());
						mailAttributes.setAmendmentType(amendment.getAmendmentType().name());
						mailAttributes.setBookingStatus(order.getStatus().name());
						if (amendment.getCreatedOn() != null) {
							mailAttributes.setGenerationTime(DateFormatterHelper
									.formatDateTime(amendment.getCreatedOn(), DateFormatType.AMENDMENT_EMAIL_FORMAT));
						}
						mailAttributes.setPassengerName(pax(airOrderItemList));
						mailAttributes.setSector(sector(airOrderItemList));
						mailAttributes.setRole(user.getRole());
						return mailAttributes;
					}
				};
		if (OrderUtils.isMessageSendingRequired(msgAttributes.get().getKey(), MessageMedium.EMAIL, user, order))
			msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}

	public void sendSMS(Amendment amendment, SmsTemplateKey smsTemplateKey) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				setDependencies(amendment);
				Map<String, String> attributeMap = new HashMap<>();
				attributeMap.put("amdId", amendment.getAmendmentId());
				attributeMap.put("bookingId", amendment.getBookingId());
				attributeMap.put("link", link(amendment.getAmendmentId()));
				List<String> mobile = new ArrayList<>();
				mobile.add(user.getMobile());
				SmsAttributes smsAttributes = SmsAttributes.builder().key(smsTemplateKey.name())
						.recipientNumbers(mobile).attributes(attributeMap).partnerId(amendment.getPartnerId())
						.role(user.getRole()).build();
				return smsAttributes;
			}
		};
		if (OrderUtils.isMessageSendingRequired(msgAttributes.get().getKey(), MessageMedium.SMS, user, order)) {
			msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());
		}
	}

	private void setDependencies(Amendment amendment) {
		order = orderManager.findByBookingId(amendment.getBookingId());
		user = userService.getUserFromCache(amendment.getBookingUserId());
	}

	private String link(String amendmentId) {
		StringBuilder link = new StringBuilder();
		domain = StringUtils.isEmpty(domain) ? "technogramsolutions.com" : domain;
		String prefix = StringUtils.isBlank(envName) ? "dashboard." : envName + "-dashboard.";
		link.append("https://").append(prefix).append(StringUtils.isEmpty(domain) ? "technogramsolutions.com" : domain)
				.append("/").append("process-amendment?").append(amendmentId);
		return link.toString();
	}

	private String sector(List<AirOrderItem> airOrderItemList) {
		StringBuilder sectorString = new StringBuilder();
		airOrderItemList
				.forEach(item -> sectorString.append(item.getSource()).append("-").append(item.getDest()).append(", "));
		return sectorString.substring(0, sectorString.length() - 2);
	}

	private String pax(List<AirOrderItem> airOrderItemList) {
		StringBuilder paxString = new StringBuilder();
		airOrderItemList.get(0).getTravellerInfo().forEach(
				pax -> paxString.append(pax.getFirstName()).append(" ").append(pax.getLastName()).append(", "));
		return paxString.substring(0, paxString.length() - 2);
	}
}
