package com.tgs.services.oms.Amendments;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.hotel.HotelAmendmentDetailResponse;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceRequest;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceResponse;
import com.tgs.services.oms.servicehandler.hotel.HotelInvoiceHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelAmendmentManager {

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private GeneralServiceCommunicator generalService;
	
	@Autowired
	private HotelInvoiceHandler hotelInvoiceHandler;


	public boolean checkIfAmendmentTypeValid(AmendmentType amendmentType, List<DbHotelOrderItem> orderList) {

		boolean isAmendmentTypeValidForOrderItem = true;
		for (DbHotelOrderItem hotelOrderitem : orderList) {
			if (!HotelItemStatus.getHotelItemStatus(hotelOrderitem.getStatus()).validAmdTypes()
					.contains(amendmentType)) {
				isAmendmentTypeValidForOrderItem = false;
				return isAmendmentTypeValidForOrderItem;
			}
		}
		return isAmendmentTypeValidForOrderItem;
	}

	public void checkAmendStatus(String BookingId, Set<String> keys) {

		Set<String> roomKeys = new HashSet<String>();
		AmendmentFilter filter = AmendmentFilter.builder().bookingIdIn(Collections.singletonList(BookingId)).build();

		List<Amendment> amendments = amendmentService.search(filter).stream().filter(
				a -> !a.getStatus().equals(AmendmentStatus.SUCCESS) && !a.getStatus().equals(AmendmentStatus.REJECTED))
				.collect(Collectors.toList());
		if (CollectionUtils.isEmpty(amendments)) {
			return;
		}

		for (Amendment amd : amendments) {
			if (amd.getHotelAdditionalInfo().getRoomKeys() != null)
				roomKeys.addAll(amd.getHotelAdditionalInfo().getRoomKeys());
		}
		if (CollectionUtils.isEmpty(keys) && CollectionUtils.isEmpty(roomKeys) && amendments.size() > 0) {
			throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
		}
		checkRoomAmendStatus(keys, roomKeys);
	}

	private void checkRoomAmendStatus(Set<String> keys, Set<String> roomKeys) {

		if (CollectionUtils.isEmpty(keys) && !CollectionUtils.isEmpty(roomKeys)) {
			throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
		}
		if (!CollectionUtils.isEmpty(keys) && CollectionUtils.isEmpty(roomKeys)) {
			throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
		}
		for (String key : keys) {
			if (roomKeys.contains(key)) {
				throw new CustomGeneralException(SystemError.AMENDMENT_EXISTS);
			}
		}
	}

	public HotelAmendmentDetailResponse getAmendmentDetail(@NotNull String amendmentId) {
		Amendment amendment = amendmentService.findByAmendmentId(amendmentId);

		if (amendment == null)
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);

		HotelAmendmentDetailResponse response = new HotelAmendmentDetailResponse();
		List<Note> notes = generalService.getNotes(NoteFilter.builder().amendmentId(amendmentId).build());
		response.setAmendment(amendment);
		response.setNote(CollectionUtils.isEmpty(notes) ? "" : notes.get(0).getNoteMessage());

		return response;

	}

	public double updateCommonAmounts(List<HotelOrderItem> modifiedItems, Amendment amendment) {

		double totalAmendFee = 0d;
		if(CollectionUtils.isEmpty(modifiedItems))
			return totalAmendFee;
		
		for (HotelOrderItem item : modifiedItems) {
			RoomInfo room = item.getRoomInfo();
			totalAmendFee += room.getTotalFareComponents().getOrDefault(HotelFareComponent.HAF, 0.0);
			totalAmendFee += room.getTotalFareComponents().getOrDefault(HotelFareComponent.HAFT, 0.0);
			totalAmendFee += room.getTotalFareComponents().getOrDefault(HotelFareComponent.CAF, 0.0);
			totalAmendFee += room.getTotalFareComponents().getOrDefault(HotelFareComponent.CAFT, 0.0);
		}
		amendment.getAdditionalInfo().setTotalAmendmentFee(totalAmendFee);
		return totalAmendFee;
	}

	public HotelInvoiceResponse getAmendmentInvoiceDetails(@NotNull String amendmentId, String invoiceId) throws Exception {
		
		Amendment amendment = amendmentService.findByAmendmentId(amendmentId);
		HotelInfo amendmentSnapshot = getAmendmentSnapshot(amendment);
		HotelInvoiceRequest request =
				new HotelInvoiceRequest(amendment.getBookingId(), InvoiceType.AGENCY, amendmentSnapshot);
		request.setInvoiceId(invoiceId);
		hotelInvoiceHandler.initData(request, new HotelInvoiceResponse());
		HotelInvoiceResponse invoiceResponse = hotelInvoiceHandler.getResponse();
		invoiceResponse.setAmendmentId(amendment.getAmendmentId());
		invoiceResponse.setCreatedOn(amendment.getProcessedOn());
		invoiceResponse.setAmendmentType(amendment.getAmendmentType());
		return invoiceResponse;
	}

	public HotelInfo getAmendmentSnapshot(Amendment amendment) {
		
		HotelInfo previousHotelInfo = getPreviousOrderSnapshotFromHotelAdditionalInfo(amendment);
		HotelInfo updatedHotelInfo = getCurrentOrderSnapshotFromHotelAdditionalInfo(amendment);
		updatedHotelInfo = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(updatedHotelInfo), HotelInfo.class);
		
		
		Map<String, RoomInfo> updatedMap = updatedHotelInfo.getOptions().get(0).getRoomInfos().stream()
				.collect(Collectors.toMap(RoomInfo::getId, Function.identity()));
		
		for(RoomInfo prevRoomInfo : previousHotelInfo.getOptions().get(0).getRoomInfos()) {
			
			if(BooleanUtils.isTrue(updatedMap.get(prevRoomInfo.getId()).getIsDeleted())) {
				RoomInfo ri = updatedMap.get(prevRoomInfo.getId());
				processForDeletedRoom(ri);
				//updatedHotelInfo.getOptions().get(0).getRoomInfos().add(processForDeletedRoom(ri));
			}else {
				RoomInfo updatedRoomInfo = updatedMap.get(prevRoomInfo.getId());
				BaseHotelUtils.flattenFareComponents(prevRoomInfo);
				BaseHotelUtils.flattenFareComponents(updatedRoomInfo);
				processForExistingRoom(prevRoomInfo , updatedRoomInfo);
			}
		}
		
		removeRoomsWithZeroAmount(updatedHotelInfo);
		BaseHotelUtils.updateTotalFareComponents(updatedHotelInfo);
		return updatedHotelInfo;
	}
	
	private void removeRoomsWithZeroAmount(HotelInfo updatedHotelInfo) {
		
		List<RoomInfo> ris = updatedHotelInfo.getOptions().get(0).getRoomInfos().stream()
		.filter(ri -> ri.getTotalPrice() > 0.0).collect(Collectors.toList());
		updatedHotelInfo.getOptions().get(0).setRoomInfos(ris);
		
	}

	private void processForExistingRoom(RoomInfo prevRoomInfo, RoomInfo updatedRoomInfo) {
		
		for(int i = 0 ; i <  updatedRoomInfo.getPerNightPriceInfos().size(); i++) {
			PriceInfo updatedPriceInfo = updatedRoomInfo.getPerNightPriceInfos().get(i);
			PriceInfo prevPriceInfo = prevRoomInfo.getPerNightPriceInfos().get(i);
			for(HotelFareComponent fc : updatedPriceInfo.getFareComponents().keySet()) {
				updatedPriceInfo.getFareComponents().put(fc, updatedPriceInfo.getFareComponents()
						.get(fc) - prevPriceInfo.getFareComponents().getOrDefault(fc, 0.0));
			}
		}
		
	}

	private RoomInfo processForDeletedRoom(RoomInfo room) {
		
		//RoomInfo room = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(deletedRoom), RoomInfo.class);
		
		BaseHotelUtils.flattenFareComponents(room);
		log.debug("RoomInfo is {}",GsonUtils.getGson().toJson(room));
		for(PriceInfo pi : room.getPerNightPriceInfos()) {
			Map<HotelFareComponent, Double> m = pi.getFareComponents();
			for(HotelFareComponent fc : m.keySet()) {
				/*
				 * As these components are not refunded
				 */
				if(fc.equals(HotelFareComponent.MF) || fc.equals(HotelFareComponent.MFT) || fc.equals(HotelFareComponent.CAF)
						|| fc.equals(HotelFareComponent.CAFT) || fc.equals(HotelFareComponent.HAF) || fc.equals(HotelFareComponent.HAFT))
					continue;
				m.put(fc, 0.0 - m.get(fc));
			}
		}
		return room;
		
	}

	public HotelInfo getBaseBookingFromAmendment(String bookingId) {
		
		Amendment amendment = getFirstAmendment(bookingId);
		if(Objects.isNull(amendment)) return null;
		HotelInfo previousSnapshot = getPreviousOrderSnapshotFromHotelAdditionalInfo(amendment);
		
		return previousSnapshot;
		
	}
	
	private Amendment getFirstAmendment(String bookingId) {

		AmendmentFilter filter = AmendmentFilter.builder().statusIn(Collections.singletonList(AmendmentStatus.SUCCESS))
				.bookingIdIn(Collections.singletonList(bookingId)).build();
		List<Amendment> amendmentList = amendmentService.search(filter);
		if (CollectionUtils.isNotEmpty(amendmentList)) {
			amendmentList.sort(Comparator.comparing(Amendment::getProcessedOn));
			return amendmentList.get(0);
		}
		return null;
	}
	
	/*
	 * These are used to do preprocessing of fareComponents
	 */
	public HotelInfo getPreviousOrderSnapshotFromHotelAdditionalInfo(Amendment amendment) {
		
		HotelInfo snapshot = amendment.getHotelAdditionalInfo().getOrderPreviousSnapshot();
		BaseHotelUtils.updateTotalFareComponents(snapshot);
		return snapshot;
		
	}
	
	public HotelInfo getCurrentOrderSnapshotFromHotelAdditionalInfo(Amendment amendment) {
		
		HotelInfo snapshot = amendment.getHotelAdditionalInfo().getOrderCurrentSnapshot();
		BaseHotelUtils.updateTotalFareComponents(snapshot);
		return snapshot;
		
	}
	
	public void setPreviousOrderSnapshotAfterProcessing(Amendment amendment, HotelInfo hInfo) {
		
		hInfo.getOptions().get(0).getRoomInfos().forEach((roomInfo) 
    			-> BaseHotelUtils.flattenFareComponents(roomInfo));
		amendment.getHotelAdditionalInfo().setOrderPreviousSnapshot(hInfo);
		
	}
	
	public void setCurrentOrderSnapshotAfterProcessing(Amendment amendment, HotelInfo hInfo) {
		
		hInfo.getOptions().get(0).getRoomInfos().forEach((roomInfo) 
    			-> BaseHotelUtils.flattenFareComponents(roomInfo));
		amendment.getHotelAdditionalInfo().setOrderCurrentSnapshot(hInfo);
	}

}



