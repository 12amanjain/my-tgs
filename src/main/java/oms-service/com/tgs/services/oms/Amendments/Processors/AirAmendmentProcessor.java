package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.*;
import static com.tgs.services.oms.Amendments.AmendmentHelper.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;

@Service
@Getter
@Setter
public abstract class AirAmendmentProcessor implements AmendmentProcessor {

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	protected FMSCommunicator fmsCommunicator;

	@Autowired
	protected UserServiceCommunicator usCommunicator;

	@Autowired
	protected AirOrderItemCommunicator airOrderCommunicator;

	@Autowired
	protected GstInfoService gstService;

	protected List<AirOrderItem> modifiedItems;

	protected List<DbAirOrderItem> orgItems;

	public List<AirOrderItem> patchedItems;

	protected DbOrder order;

	protected GstInfo gstInfo;

	protected Amendment amendment;

	protected boolean mergeAmendment;

	protected boolean isCancellationNewFlow;

	protected User bookingUser;

	private static List<AmendmentType> adjustTFForamendments =
			Arrays.asList(AmendmentType.CANCELLATION, AmendmentType.CANCELLATION_QUOTATION, AmendmentType.NO_SHOW,
					AmendmentType.VOIDED, AmendmentType.FARE_CHANGE);

	public void process() {
		processAmendment();
		updateCommonAmounts();
		updateAirOrderItemAmount();
		filterTravellers(false);
	}

	public void getAmendmentDetails() {
		filterTravellers(false);
		PrePopulate();
	}

	protected void filterTravellers(boolean reIssueProcessor) {
		if (amendment.getAmendmentType().equals(AmendmentType.REISSUE) && !reIssueProcessor)
			return;
		filterPax(amendment, patchedItems);
	}

	protected void filterSegments(boolean reIssueProcessor) {
		if (amendment.getAmendmentType().equals(AmendmentType.REISSUE) && !reIssueProcessor)
			return;
		patchedItems = filterOrderItems(amendment, patchedItems);
	}

	protected void updateCommonAmounts() {
		double totalAmendFee = 0d;
		double orderDiff = 0d;

		DbOrderBilling dbGst = gstService.findByBookingId(amendment.getBookingId());
		if (dbGst != null) {
			gstInfo = dbGst.toDomain();
		}

		List<Double> chargedBySupplierPaymentAmounts = new ArrayList<>();
		List<Double> notChargedBySupplierPaymentAmounts = new ArrayList<>();
		Map<String, Double> paxWiseCharges = new HashMap<>();
		for (AirOrderItem airOrderItem : patchedItems) {
			double itemDiff = 0d;
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				String key = airOrderItem.getId() + "_" + traveller.getId();
				double paxAmdFee = getAmendmentFee(traveller.getFareDetail());
				totalAmendFee += paxAmdFee;
				double tf = traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d);
				tf += paxAmdFee;
				traveller.getFareDetail().getFareComponents().put(TF, tf);
				double travellerDiff = tf - ObjectUtils.defaultIfNull(traveller.getFareDetail().getPaxPreviousTF(), 0d);

				// Applicable only in case of credit shell.
				double creditShellBalance = BaseUtils.calculateAvailablePNRCredit(traveller.getFareDetail());
				if (creditShellBalance > 0 && adjustTotalFareForAmendment()) {
					// Refund scenario
					if (travellerDiff < 0) {
						double chargedAmount =
								ObjectUtils.firstNonNull(traveller.getFareDetail().getMaxRefundableAmount(), 0d)
										- creditShellBalance;
						double cancellationFee =
								ObjectUtils.firstNonNull(traveller.getFareDetail().getPaxCancellationFee(), 0d);
						double refundableAmount = Math.abs(travellerDiff);
						/*
						 * If cancellation fee is 0 and refund is higher than the charged amount then only charged
						 * amount should be refunded.
						 */
						if (cancellationFee == 0 && refundableAmount > chargedAmount) {
							refundableAmount = -1 * (chargedAmount);
						} else if (cancellationFee > 0 && refundableAmount > chargedAmount) {

							/*
							 * If cancellation fee is > 0 and refund is higher than the charged amount and charged
							 * amount is higher than the cancellation fee then the difference of charged amount and
							 * cancellation fee should be refunded.
							 */
							if (chargedAmount > cancellationFee) {
								refundableAmount = -1 * (chargedAmount - cancellationFee);
							} else {
								/*
								 * If cancellation fee is > 0 and refund is higher than the charged amount and charged
								 * amount is lesser or equal to the cancellation fee then the difference should be
								 * charged.
								 */
								refundableAmount = -1
										* (cancellationFee - chargedAmount > 0 ? 0 : cancellationFee - chargedAmount);
							}
						} else if (cancellationFee > 0 && chargedAmount >= refundableAmount) {
							/*
							 * If cancellation fee is > 0 and charged amount is greater than refund then only the
							 * cancellation charge reduced from charged amount should be refunded.
							 */
							refundableAmount =
									-1 * (chargedAmount - cancellationFee < 0 ? 0 : chargedAmount - cancellationFee);
						}

						// To adjust CS in case of refund. So that next amendments for the same booking will have the
						// correct CS balance.
						if (AmendmentType.FARE_CHANGE.equals(amendment.getAmendmentType())) {
							traveller.getFareDetail().getFareComponents().put(CS,
									creditShellBalance - Math.min(creditShellBalance, Math.abs(travellerDiff)));
						}
						travellerDiff = refundableAmount;
					}
					if (!AmendmentType.FARE_CHANGE.equals(amendment.getAmendmentType())) {
						// This reset Credit shell balance.
						Set<FareComponent> unset = new HashSet<>();
						unset.add(CS);
						zeroOut(traveller.getFareDetail(), unset);
					}
				}
				paxWiseCharges.put(key, travellerDiff);
				itemDiff += travellerDiff;
				OmsHelper.updateGstComponents(traveller.getFareDetail(),
						userService.getUserFromCache(amendment.getBookingUserId()), gstInfo);

			}
			if (BooleanUtils.isTrue(airOrderItem.getAdditionalInfo().getIsUserCreditCard()) && itemDiff != 0
					&& amendment.getAmendmentType().isVirtualPaymentAllowed()) {
				Integer tripSource = airOrderItem.getAdditionalInfo().getSourceId();
				if (AirBookingUtils.isVpAllowed(tripSource, bookingUser))
					chargedBySupplierPaymentAmounts.add(itemDiff);
				else
					notChargedBySupplierPaymentAmounts.add(itemDiff);
			}
			orderDiff += itemDiff;
		}
		amendment.getAdditionalInfo().setTotalAmendmentFee(totalAmendFee);
		amendment.getAdditionalInfo().setOrderDiffAmount(orderDiff);
		amendment.getAdditionalInfo().setChargedBySuppplierRefundAmounts(chargedBySupplierPaymentAmounts);
		amendment.getAdditionalInfo().setNotChargedBySuppplierRefundAmounts(notChargedBySupplierPaymentAmounts);
		amendment.getModifiedInfo().getPaxWiseAmount().putAll(paxWiseCharges);
		amendment.setAmendmentAmount();
	}

	protected abstract void processAmendment();

	protected abstract void PrePopulate();

	/**
	 * It can also be calculated as: Sum(TF - MU) pax wise, the way it is calculated in SItoAOI mapper
	 */
	public void updateAirOrderItemAmount() {
		for (AirOrderItem patchedItem : patchedItems) {
			double itemDiff = 0d;
			double discount = 0;
			for (FlightTravellerInfo pax : patchedItem.getTravellerInfo()) {
				Double paxPreviousTF = pax.getFareDetail().getPaxPreviousTF();
				if (paxPreviousTF != null) {
					itemDiff += pax.getFareDetail().getFareComponents().getOrDefault(TF, 0d) - paxPreviousTF;
				}
				if (pax.getFareDetail().getPaxPreviousDS() != null) {
					discount += pax.getFareDetail().getPaxPreviousDS();
				}
				pax.getFareDetail().setPaxPreviousTF(null);
				pax.getFareDetail().setPaxPreviousDS(null);
			}
			double itemAmount = ObjectUtils.firstNonNull(patchedItem.getAmount(), 0d) - discount;
			patchedItem.setAmount(itemAmount + itemDiff);
		}
	}

	public List<AirOrderItem> patchAirOrderItems(boolean mergeAmendment) {
		this.mergeAmendment = mergeAmendment;
		if (!mergeAmendment)
			unsetFields(orgItems);
		if (modifiedItems != null) {
			modifiedItems.forEach(airOrderItem -> {
				Long sId = airOrderItem.getId();
				airOrderItem.getTravellerInfo().forEach(t -> {
					if (!amendment.getAirAdditionalInfo().amdContainsPax(sId, t.getId())) {
						throw new CustomGeneralException(SystemError.INVALID_PAX_ID);
					}
				});
			});
		}
		patchedItems = BaseModel.toDomainList(patchAirOrderItems());
		if (mergeAmendment)
			return patchedItems;
		filterSegments(false);
		filterTravellers(false);
		return patchedItems;
	}

	protected List<DbAirOrderItem> patchAirOrderItems() {
		List<DbAirOrderItem> modItems =
				new DbAirOrderItem().toDbList(ObjectUtils.firstNonNull(modifiedItems, new ArrayList<>()));
		for (DbAirOrderItem amdItem : modItems) {
			List<DbAirOrderItem> matchedItems =
					orgItems.stream().filter(a -> a.getId().equals(amdItem.getId())).collect(Collectors.toList());
			if (matchedItems.size() <= 0)
				throw new RuntimeException(String.format(
						"AirOrderItem patching failed for amendmentId %s. "
								+ "SegmentId - \"%s\" not found in original order items.",
						amendment.getAmendmentId(), amdItem.getId()));
			DbAirOrderItem patchedItem = matchedItems.get(0);
			AdditionalAirOrderItemInfo orgAdditionalInfo = patchedItem.getAdditionalInfo();
			AdditionalAirOrderItemInfo modAdditionalInfo = amdItem.getAdditionalInfo();
			List<FlightTravellerInfo> orgTravs = patchedItem.getTravellerInfo();
			String curStatus = patchedItem.getStatus();
			orgItems.remove(patchedItem);
			patchedItem = new GsonMapper<>(amdItem, patchedItem, DbAirOrderItem.class).convert();
			patchedItem.setTravellerInfo(patchTravellerInfoList(amdItem.getTravellerInfo(), orgTravs));
			if (modAdditionalInfo != null) {
				patchedItem.setAdditionalInfo(
						new GsonMapper<>(modAdditionalInfo, orgAdditionalInfo, AdditionalAirOrderItemInfo.class)
								.convert());
			}
			patchedItem.setStatus(curStatus);
			orgItems.add(patchedItem);
		}
		return orgItems;
	}

	private List<FlightTravellerInfo> patchTravellerInfoList(List<FlightTravellerInfo> modList,
			List<FlightTravellerInfo> orgList) {

		if (CollectionUtils.isEmpty(modList))
			return orgList;

		modList.forEach(amdPax -> {
			List<FlightTravellerInfo> matchList =
					orgList.stream().filter(ml -> ml.getId().equals(amdPax.getId())).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(matchList)) {
				throw new RuntimeException(String.format(
						"AirOrderItem patching failed for amendmentId %s. Pax Id - \"%s\" not found in original order items.",
						amendment.getAmendmentId(), amdPax.getId()));
			}
			FlightTravellerInfo patchedTrav = matchList.get(0);
			checkNullSsr(amdPax);
			FlightTravellerInfo oldPax = new GsonMapper<>(patchedTrav, FlightTravellerInfo.class).convert();
			double discount = 0;
			if (UserRole.corporate(bookingUser.getRole())) {
				discount = AirBookingUtils.getGrossCommission(oldPax.getFareDetail(), null, true);
				oldPax.getFareDetail().setPaxPreviousDS(discount);
			}
			// Voucher discount
			discount += oldPax.getFareDetail().getFareComponents().getOrDefault(FareComponent.VD, 0d);
			oldPax.getFareDetail().setPaxPreviousTF(
					oldPax.getFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d) - discount);
			FareDetail orgFd = oldPax.getFareDetail();
			String oldOst = patchedTrav.getOtherServicesText();
			orgList.remove(patchedTrav);
			patchedTrav = new GsonMapper<>(amdPax, patchedTrav, FlightTravellerInfo.class).convert();
			resetSSR(amdPax, patchedTrav);
			mergeSSRData(oldPax, patchedTrav);
			patchedTrav.setFareDetail(patchFareDetail(amdPax.getFareDetail(), orgFd));
			if (mergeAmendment) {
				patchedTrav.setOtherServicesText(patchOst(amdPax.getOtherServicesText(), oldOst));
				setSSRFromFCForCancelAmendment(patchedTrav);
			}
			orgList.add(patchedTrav);
		});
		return orgList;
	}

	private void setSSRFromFCForCancelAmendment(FlightTravellerInfo pax) {
		if (amendment.getAmendmentType().equals(AmendmentType.CANCELLATION)
				|| amendment.getAmendmentType().equals(AmendmentType.NO_SHOW)) {
			Map<FareComponent, Double> fc = pax.getFareDetail().getFareComponents();
			if (pax.getSsrBaggageInfo() != null) {
				pax.getSsrBaggageInfo().setAmount(fc.getOrDefault(BP, 0d));
			}
			if (pax.getSsrSeatInfo() != null) {
				pax.getSsrSeatInfo().setAmount(fc.getOrDefault(SP, 0d));
			}
			if (pax.getSsrMealInfo() != null) {
				pax.getSsrMealInfo().setAmount(fc.getOrDefault(MP, 0d));
			}
		}
	}

	protected FareDetail patchFareDetail(FareDetail modFareDetail, FareDetail fareDetail) {
		if (modFareDetail == null || modFareDetail.getFareComponents() == null)
			return fareDetail;
		Map<FareComponent, Double> orgFc = fareDetail.getFareComponents();
		fareDetail = new GsonMapper<>(modFareDetail, fareDetail, FareDetail.class).convert();
		fareDetail.setFareComponents(orgFc);
		fareDetail.setOrgManualRefundableFC(fareDetail.getSubSet(FareComponent.getManualRefundableFC()));
		if (mergeAmendment) {
			if (MapUtils.isEmpty(modFareDetail.getFareComponents()))
				return fareDetail;
			for (Map.Entry<FareComponent, Double> entry : modFareDetail.getFareComponents().entrySet()) {
				FareComponent key = entry.getKey();
				Double amendVal = entry.getValue();
				if (key.usedInAmendment()) {
					fareDetail.getFareComponents().compute(key, (k, v) -> ObjectUtils.firstNonNull(v, amendVal, 0d));
				} else {
					fareDetail.getFareComponents().put(key, amendVal);
				}
			}
			fareDetail.getFareComponents().put(TF, modFareDetail.getFareComponents().get(TF));
		} else {
			if (MapUtils.isEmpty(modFareDetail.getFareComponents()))
				return fareDetail;

			for (Map.Entry<FareComponent, Double> entry : modFareDetail.getFareComponents().entrySet()) {
				FareComponent fc = entry.getKey();
				Double amount = entry.getValue();
				fareDetail.getFareComponents().put(fc, amount);
			}
		}
		double discount = 0;
		if (UserRole.corporate(bookingUser.getRole())) {
			discount = AirBookingUtils.getGrossCommission(fareDetail, null, true);
		}
		// Voucher discount
		discount += fareDetail.getFareComponents().getOrDefault(FareComponent.VD, 0d);
		fareDetail.getFareComponents().put(TF, fareDetail.getFareComponents().getOrDefault(TF, 0.0) - discount);
		return fareDetail;
	}

	protected void unsetFields(List<DbAirOrderItem> airOrderItemList) {
		airOrderItemList.forEach(item -> item.getTravellerInfo().forEach(pax -> {
			pax.setOtherServicesText(null);
			pax.setSsrBaggageInfo(null);
			pax.setSsrMealInfo(null);
			pax.setSsrSeatInfo(null);
			pax.getFareDetail().setPreviousAmendmentFee(getAmendmentFee(pax.getFareDetail()));
			for (Map.Entry<FareComponent, Double> entry : pax.getFareDetail().getFareComponents().entrySet()) {
				FareComponent key = entry.getKey();
				if (key.usedInAmendment()) {
					entry.setValue(null);
				}
			}
		}));
	}

	private static void checkNullSsr(FlightTravellerInfo pax) {
		if (pax.getSsrBaggageInfo() != null && pax.getSsrBaggageInfo().getAmount() == null)
			pax.setSsrBaggageInfo(null);
		if (pax.getSsrMealInfo() != null && pax.getSsrMealInfo().getAmount() == null)
			pax.setSsrMealInfo(null);
		if (pax.getSsrSeatInfo() != null && pax.getSsrSeatInfo().getAmount() == null)
			pax.setSsrSeatInfo(null);
	}

	private static void resetSSR(FlightTravellerInfo nuw, FlightTravellerInfo patched) {
		patched.setSsrBaggageInfo(nuw.getSsrBaggageInfo());
		patched.setSsrMealInfo(nuw.getSsrMealInfo());
		patched.setSsrSeatInfo(nuw.getSsrSeatInfo());
	}

	private static void mergeSSRData(FlightTravellerInfo old, FlightTravellerInfo nuw) {
		if (old.getSsrBaggageInfo() != null) {
			if (nuw.getSsrBaggageInfo() == null)
				nuw.setSsrBaggageInfo(old.getSsrBaggageInfo());
			else
				nuw.getSsrBaggageInfo().setAmount(nuw.getSsrBaggageInfo().getAmount()
						+ ObjectUtils.firstNonNull(old.getSsrBaggageInfo().getAmount(), 0d));
		}
		if (old.getSsrMealInfo() != null) {
			if (nuw.getSsrMealInfo() == null)
				nuw.setSsrMealInfo(old.getSsrMealInfo());
			else
				nuw.getSsrMealInfo().setAmount(nuw.getSsrMealInfo().getAmount()
						+ ObjectUtils.firstNonNull(old.getSsrMealInfo().getAmount(), 0d));
		}
		if (old.getSsrSeatInfo() != null) {
			if (nuw.getSsrSeatInfo() == null)
				nuw.setSsrSeatInfo(old.getSsrSeatInfo());
			else
				nuw.getSsrSeatInfo().setAmount(nuw.getSsrSeatInfo().getAmount()
						+ ObjectUtils.firstNonNull(old.getSsrSeatInfo().getAmount(), 0d));
		}
	}

	protected static void setUpdatedFDForTravellers(SegmentInfo segmentInfo, AirOrderItem airOrderItem) {
		airOrderItem.setTravellerInfo(segmentInfo.getBookingRelatedInfo().getTravellerInfo());
	}

	protected static void zeroOut(FareDetail fareDetail, Set<FareComponent> toUnset) {
		for (Map.Entry<FareComponent, Double> entry : fareDetail.getFareComponents().entrySet()) {
			FareComponent fc = entry.getKey();
			if (toUnset.contains(fc)) {
				entry.setValue(0d);
			}
		}
	}

	private static String patchOst(String newOst, String oldOst) {
		if (StringUtils.isBlank(oldOst))
			return newOst;
		if (StringUtils.isBlank(newOst))
			return oldOst;
		return (oldOst + ", " + newOst).trim();
	}

	private boolean adjustTotalFareForAmendment() {
		boolean reduceCreditShellFromTotalFare = false;
		if (adjustTFForamendments.contains(amendment.getAmendmentType())) {
			reduceCreditShellFromTotalFare = true;
		}
		return reduceCreditShellFromTotalFare;
	}

	protected static double getAmendmentFee(FareDetail fareDetail) {
		double paxAmdFee = 0d;
		paxAmdFee += fareDetail.getFareComponents().getOrDefault(AAF, 0d);
		paxAmdFee += fareDetail.getFareComponents().getOrDefault(AAFT, 0d);
		paxAmdFee += fareDetail.getFareComponents().getOrDefault(CAF, 0d);
		paxAmdFee += fareDetail.getFareComponents().getOrDefault(CAFT, 0d);
		return paxAmdFee;
	}
}
