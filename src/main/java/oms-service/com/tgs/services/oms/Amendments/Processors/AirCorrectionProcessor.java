package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.AAF;
import static com.tgs.services.base.enums.FareComponent.AAFT;
import static com.tgs.services.base.enums.FareComponent.BP;
import static com.tgs.services.base.enums.FareComponent.CAF;
import static com.tgs.services.base.enums.FareComponent.CAFT;
import static com.tgs.services.base.enums.FareComponent.MP;
import static com.tgs.services.base.enums.FareComponent.SP;
import static com.tgs.services.base.enums.FareComponent.TF;
import static com.tgs.services.base.enums.FareComponent.getCommissionComponents;
import static com.tgs.services.base.enums.FareComponent.taxComponents;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.utils.air.AirBookingUtils;


@Service
public class AirCorrectionProcessor extends AirAmendmentProcessor {

	@Override
	protected void processAmendment() {
		for (AirOrderItem airOrderItem : patchedItems) {
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				handleSSRComponents(traveller);
			}
		}
	}

	@Override
	protected void PrePopulate() {}

	// expecting amount will never be null
	private static void handleSSRComponents(FlightTravellerInfo pax) {
		Map<FareComponent, Double> fc = pax.getFareDetail().getFareComponents();
		double totalFare = fc.getOrDefault(FareComponent.TF, 0.0);

		if (pax.getSsrBaggageInfo() != null) {
			double diff = ObjectUtils.defaultIfNull(pax.getSsrBaggageInfo().getAmount(), 0d)
					- ObjectUtils.defaultIfNull(fc.get(BP), 0d);
			fc.put(BP, ObjectUtils.defaultIfNull(pax.getSsrBaggageInfo().getAmount(), 0d));
			totalFare += diff;
			fc.put(FareComponent.TF, totalFare);
		}
		if (pax.getSsrMealInfo() != null) {
			double diff = ObjectUtils.defaultIfNull(pax.getSsrMealInfo().getAmount(), 0d)
					- ObjectUtils.defaultIfNull(fc.get(MP), 0d);
			fc.put(MP, ObjectUtils.defaultIfNull(pax.getSsrMealInfo().getAmount(), 0d));
			totalFare += diff;
			fc.put(FareComponent.TF, totalFare);
		}
		if (pax.getSsrSeatInfo() != null) {
			double diff = ObjectUtils.defaultIfNull(pax.getSsrSeatInfo().getAmount(), 0d)
					- ObjectUtils.defaultIfNull(fc.get(SP), 0d);
			fc.put(SP, ObjectUtils.defaultIfNull(pax.getSsrSeatInfo().getAmount(), 0d));
			totalFare += diff;
			fc.put(FareComponent.TF, totalFare);
		}
	}


	public List<AirOrderItem> patchAirOrderItems(boolean mergeAmendment) {
		if (modifiedItems != null) {
			modifiedItems.forEach(airOrderItem -> {
				Long sId = airOrderItem.getId();
				airOrderItem.getTravellerInfo().forEach(t -> {
					if (!amendment.getAirAdditionalInfo().amdContainsPax(sId, t.getId())) {
						throw new CustomGeneralException(SystemError.INVALID_PAX_ID);
					}
				});
			});
		}
		patchedItems = BaseModel.toDomainList(patchAirOrderItems());
		patchedItems.forEach(AirBookingUtils::setAirlineNAirportInfo);
		if (mergeAmendment) {
			return patchedItems;
		}
		filterSegments(false);
		filterTravellers(false);
		return patchedItems;
	}


	protected List<DbAirOrderItem> patchAirOrderItems() {
		List<DbAirOrderItem> modItems =
				new DbAirOrderItem().toDbList(ObjectUtils.firstNonNull(modifiedItems, new ArrayList<>()));
		for (DbAirOrderItem amdItem : modItems) {
			List<DbAirOrderItem> matchedItems =
					orgItems.stream().filter(a -> a.getId().equals(amdItem.getId())).collect(Collectors.toList());
			if (matchedItems.size() <= 0)
				throw new RuntimeException(String.format(
						"AirOrderItem patching failed for amendmentId %s. "
								+ "SegmentId - \"%s\" not found in original order items.",
						amendment.getAmendmentId(), amdItem.getId()));
			DbAirOrderItem patchedItem = matchedItems.get(0);
			AdditionalAirOrderItemInfo orgAdditionalInfo = patchedItem.getAdditionalInfo();
			AdditionalAirOrderItemInfo modAdditionalInfo = amdItem.getAdditionalInfo();
			List<FlightTravellerInfo> orgTravs = patchedItem.getTravellerInfo();
			orgItems.remove(patchedItem);
			patchedItem = new GsonMapper<>(amdItem, patchedItem, DbAirOrderItem.class).convert();
			patchedItem.setTravellerInfo(patchTravellerInfoList(amdItem.getTravellerInfo(), orgTravs));
			if (modAdditionalInfo != null) {
				patchedItem.setAdditionalInfo(
						new GsonMapper<>(modAdditionalInfo, orgAdditionalInfo, AdditionalAirOrderItemInfo.class)
								.convert());
			}
			orgItems.add(patchedItem);
		}
		return orgItems;
	}


	private List<FlightTravellerInfo> patchTravellerInfoList(List<FlightTravellerInfo> modList,
			List<FlightTravellerInfo> orgList) {
		if (CollectionUtils.isEmpty(modList))
			return orgList;
		modList.forEach(amdPax -> {
			List<FlightTravellerInfo> matchList =
					orgList.stream().filter(ml -> ml.getId().equals(amdPax.getId())).collect(Collectors.toList());
			if (CollectionUtils.isEmpty(matchList))
				throw new RuntimeException(String.format(
						"AirOrderItem patching failed for amendmentId %s. "
								+ "Pax Id - \"%s\" not found in original order items.",
						amendment.getAmendmentId(), amdPax.getId()));
			FlightTravellerInfo patchedTrav = matchList.get(0);
			FlightTravellerInfo oldPax = new GsonMapper<>(patchedTrav, FlightTravellerInfo.class).convert();
			oldPax.getFareDetail()
					.setPaxPreviousTF(AirBookingUtils.calculateTravellerTotalWithoutDiscount(oldPax, bookingUser));
			FareDetail orgFd = oldPax.getFareDetail();
			orgList.remove(patchedTrav);
			patchedTrav = new GsonMapper<>(amdPax, patchedTrav, FlightTravellerInfo.class).convert();
			patchedTrav.setFareDetail(patchFareDetail(amdPax.getFareDetail(), orgFd));
			orgList.add(patchedTrav);
		});
		return orgList;
	}


	protected FareDetail patchFareDetail(FareDetail modFareDetail, FareDetail orgFareDetail) {
		if (modFareDetail == null || modFareDetail.getFareComponents() == null)
			return orgFareDetail;
		Map<FareComponent, Double> orgFc = orgFareDetail.getFareComponents();
		orgFareDetail = new GsonMapper<>(modFareDetail, orgFareDetail, FareDetail.class).convert();
		orgFareDetail.setFareComponents(orgFc);
		if (MapUtils.isEmpty(modFareDetail.getFareComponents()))
			return orgFareDetail;
		double totalDiff = 0d;
		double totalFare = orgFareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0d);
		for (Map.Entry<FareComponent, Double> entry : modFareDetail.getFareComponents().entrySet()) {
			FareComponent fc = entry.getKey();
			Double amount = entry.getValue();
			if (!fc.equals(TF) && !FareComponent.getAllCommisionComponents().contains(fc)
					&& !taxComponents().contains(fc)) {
				totalDiff += amount - orgFareDetail.getFareComponents().getOrDefault(fc, 0d);
			}
			orgFareDetail.getFareComponents().put(fc, amount);
		}
		orgFareDetail.getFareComponents().put(FareComponent.TF, totalFare + totalDiff);
		return orgFareDetail;
	}


	@Override
	protected void updateCommonAmounts() {
		double totalAmendFee = 0d;
		double orderDiff = 0d;

		GstInfo gstInfo = null;
		DbOrderBilling dbGst = gstService.findByBookingId(amendment.getBookingId());
		if (dbGst != null) {
			gstInfo = dbGst.toDomain();
		}

		List<Double> chargedBySupplierPaymentAmounts = new ArrayList<>();
		List<Double> notChargedBySupplierPaymentAmounts = new ArrayList<>();
		Map<String, Double> paxWiseCharges = new HashMap<>();
		for (AirOrderItem airOrderItem : patchedItems) {
			double itemDiff = 0d;
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				String key = airOrderItem.getId() + "_" + traveller.getId();
				totalAmendFee += traveller.getFareDetail().getFareComponents().getOrDefault(AAF, 0d);
				totalAmendFee += traveller.getFareDetail().getFareComponents().getOrDefault(AAFT, 0d);
				totalAmendFee += traveller.getFareDetail().getFareComponents().getOrDefault(CAF, 0d);
				totalAmendFee += traveller.getFareDetail().getFareComponents().getOrDefault(CAFT, 0d);

				double tf = AirBookingUtils.calculateTravellerTotalWithoutDiscount(traveller, bookingUser);
				double paxCharges = tf - ObjectUtils.defaultIfNull(traveller.getFareDetail().getPaxPreviousTF(), 0d);
				itemDiff += paxCharges;
				paxWiseCharges.put(key, paxCharges);
				OmsHelper.updateGstComponents(traveller.getFareDetail(),
						userService.getUserFromCache(amendment.getBookingUserId()), gstInfo);
			}
			if (BooleanUtils.isTrue(airOrderItem.getAdditionalInfo().getIsUserCreditCard()) && itemDiff != 0
					&& amendment.getAmendmentType().isVirtualPaymentAllowed()) {
				Integer tripSource = airOrderItem.getAdditionalInfo().getSourceId();
				if (AirBookingUtils.isVpAllowed(tripSource, bookingUser))
					chargedBySupplierPaymentAmounts.add(itemDiff);
				else
					notChargedBySupplierPaymentAmounts.add(itemDiff);
			}
			orderDiff += itemDiff;
		}
		amendment.getAdditionalInfo().setTotalAmendmentFee(totalAmendFee);
		amendment.getAdditionalInfo().setOrderDiffAmount(orderDiff);
		amendment.getAdditionalInfo().setChargedBySuppplierRefundAmounts(chargedBySupplierPaymentAmounts);
		amendment.getAdditionalInfo().setNotChargedBySuppplierRefundAmounts(notChargedBySupplierPaymentAmounts);
		amendment.getModifiedInfo().getPaxWiseAmount().putAll(paxWiseCharges);
		amendment.setAmendmentAmount();
	}
}
