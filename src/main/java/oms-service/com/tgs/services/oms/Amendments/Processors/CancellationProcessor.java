package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.AAR;
import static com.tgs.services.base.enums.FareComponent.ACF;
import static com.tgs.services.base.enums.FareComponent.ACFT;
import static com.tgs.services.base.enums.FareComponent.BF;
import static com.tgs.services.base.enums.FareComponent.CAMU;
import static com.tgs.services.base.enums.FareComponent.CCF;
import static com.tgs.services.base.enums.FareComponent.CCFT;
import static com.tgs.services.base.enums.FareComponent.PCTDS;
import static com.tgs.services.base.enums.FareComponent.PMTDS;
import static com.tgs.services.base.enums.FareComponent.PMU;
import static com.tgs.services.base.enums.FareComponent.RP;
import static com.tgs.services.base.enums.FareComponent.TDS;
import static com.tgs.services.base.enums.FareComponent.TF;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CancellationProcessor extends AirAmendmentProcessor {

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Override
	public void processAmendment() {
		double totalAmtApplicableForRefund, totalCancelFee, commissionRecall, totalTds, partnerMarkUpRecalled,
				partnerCommissionRecalled, partnerMarkUpTds, partnerCommTds;
		totalAmtApplicableForRefund = totalCancelFee = commissionRecall =
				totalTds = partnerMarkUpRecalled = partnerCommissionRecalled = partnerMarkUpTds = partnerCommTds = 0d;
		amendment.getAdditionalInfo().setRecallCommission(BaseUtils.isRecalCommission(true, bookingUser.getRole()));
		amendment.getAdditionalInfo().setReturnTds(AmendmentHelper.returnTDS(patchedItems.get(0).getCreatedOn()));
		for (AirOrderItem airOrderItem : patchedItems) {
			boolean isSSRefundable = fmsCommunicator.isSSRRefundable(airOrderItem, bookingUser);
			boolean isPassThrough = BooleanUtils.isTrue(airOrderItem.getAdditionalInfo().getIsUserCreditCard());
			Set<FareComponent> refundableComponents = AirBookingUtils.amendmentRefundableComponents(airOrderItem,
					isCancellationNewFlow(), isSSRefundable);
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				traveller.setStatus(TravellerStatus.CANCELLED);
				double paxCancelFee = 0d;
				totalAmtApplicableForRefund +=
						handleRefundableComponents(traveller, refundableComponents, bookingUser, isSSRefundable);
				log.info("totalAmtApplicableForRefund {} ", totalAmtApplicableForRefund);
				double totalPaxFare = traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0d);
				paxCancelFee = traveller.getFareDetail()
						.getComponentsSum(AirBookingUtils.getPaxCancelComponents(isPassThrough));
				paxCancelFee -= traveller.getFareDetail().getFareComponents().getOrDefault(PMU, 0d);
				totalCancelFee += paxCancelFee;
				traveller.getFareDetail().setPaxCancellationFee(paxCancelFee);
				traveller.getFareDetail().getFareComponents().put(TF, totalPaxFare + paxCancelFee);
				commissionRecall += AirBookingUtils.getGrossCommissionForPax(traveller, bookingUser, false);
				// partnerMarkUpRecalled += AirBookingUtils.getPartnerMarkUpForPax(traveller);
				partnerCommissionRecalled += AirBookingUtils.getPartnerCommissionForPax(traveller);
				totalTds += traveller.getFareDetail().getFareComponents().getOrDefault(TDS, 0d);
				partnerMarkUpTds += traveller.getFareDetail().getFareComponents().getOrDefault(PMTDS, 0d);
				partnerCommTds += traveller.getFareDetail().getFareComponents().getOrDefault(PCTDS, 0d);
				zeroOut(traveller.getFareDetail(), FareComponent.getAllCommisionComponents());
				zeroOut(traveller.getFareDetail(), FareComponent.voucherComponents());
				// zeroOut(traveller.getFareDetail(), FareComponent.pointComponents());
				if (amendment.getAdditionalInfo().isReturnTds()) {
					traveller.getFareDetail().getFareComponents().put(TDS, 0d);
				}
			}
		}
		amendment.getAdditionalInfo().setAmountApplicableForRefund(totalAmtApplicableForRefund);
		amendment.getAirAdditionalInfo().setTotalCancellationFee(totalCancelFee);
		amendment.getAdditionalInfo().setTotalCommissionRecalled(commissionRecall);
		amendment.getAdditionalInfo().setTotalPartnerMarkupRecalled(partnerMarkUpRecalled);
		amendment.getAdditionalInfo().setTotalPartnerCommissionRecalled(partnerCommissionRecalled);
		amendment.getAdditionalInfo().setTdsToReturn(totalTds);
		amendment.getAdditionalInfo().setPartnerMarkUpTdsToReturn(partnerMarkUpTds);
		amendment.getAdditionalInfo().setPartnerCommissionTdsToReturn(partnerCommTds);
	}


	public static double handleRefundableComponents(FlightTravellerInfo traveller, Set<FareComponent> refComponents,
			User bookingUser, boolean isSSRefundable) {
		if (!MapUtils.isEmpty(traveller.getFareDetail().getFareComponents())) {
			Set<FareComponent> refundableComponents = refComponents;
			double refundableAmount = traveller.getFareDetail().getComponentsSum(refundableComponents);
			for (Map.Entry<FareComponent, Double> entry : traveller.getFareDetail().getOrgManualRefundableFC()
					.entrySet()) {
				FareComponent fc = entry.getKey();
				Double value = entry.getValue();
				refundableAmount += value - traveller.getFareDetail().getFareComponents().getOrDefault(fc, 0d);
			}
			log.info("refundableAmount b4 RP {} ", refundableAmount);
			refundableAmount -= traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.RP, 0d);
			log.info("refundableAmount after RP {} ", refundableAmount);
			double maxRefundableAmount =
					traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d)
							- traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.PF, 0d)
							- traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.MF, 0d)
							- traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.MFT, 0d)
							- traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.RP, 0d)
							- ObjectUtils.firstNonNull(traveller.getFareDetail().getPreviousAmendmentFee(), 0d);
			log.info("maxRefundableAmount after RP {} ", maxRefundableAmount);
			if (!isSSRefundable) {
				maxRefundableAmount -= traveller.getFareDetail().getComponentsSum(FareComponent.getSSRComponents());
			}
			traveller.getFareDetail().setMaxRefundableAmount(maxRefundableAmount);
			refundableAmount = (maxRefundableAmount < refundableAmount) ? maxRefundableAmount : refundableAmount;
			log.info("refundableAmount after comparison {} ", refundableAmount);
			if (UserRole.corporate(bookingUser.getRole())) {
				zeroOut(traveller.getFareDetail(), FareComponent.getAllCommisionComponents());
			}
			zeroOut(traveller.getFareDetail(), refundableComponents);
			traveller.getFareDetail().getFareComponents().put(AAR, refundableAmount);
			double finalRefundableAmount = refundableAmount;
			traveller.getFareDetail().getFareComponents().computeIfPresent(TF,
					(key, val) -> val - finalRefundableAmount);
			return traveller.getFareDetail().getFareComponents().get(AAR);
		}
		return 0d;
	}


	@Override
	public void PrePopulate() {
		if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) {
			patchedItems.forEach(airOrderItem -> {
				AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
						.fmsCommunicator(fmsCommunicator).item(new DbAirOrderItem().from(airOrderItem)).build();
				SegmentInfo segmentInfo = mapper.convert();
				fmsCommunicator.updateCancellationAndRescheduleFees(segmentInfo, bookingUser);
				setUpdatedFDForTravellers(segmentInfo, airOrderItem);
				airOrderItem.getTravellerInfo().forEach(pax -> {
					Map<FareComponent, Double> fc = pax.getFareDetail().getFareComponents();
					if (fc.getOrDefault(ACF, 0d) > fc.getOrDefault(BF, 0d)) {
						fc.put(ACF, fc.getOrDefault(BF, 0d));
					}
				});
			});
		}
	}
}
