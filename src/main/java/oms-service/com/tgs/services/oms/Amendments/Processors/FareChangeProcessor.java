package com.tgs.services.oms.Amendments.Processors;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FareDetail;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import java.util.Map;

@Service
public class FareChangeProcessor extends AirAmendmentProcessor {

	@Override
	public void processAmendment() {}


	@Override
	protected FareDetail patchFareDetail(FareDetail modFareDetail, FareDetail orgFareDetail) {

		if (modFareDetail == null || modFareDetail.getFareComponents() == null)
			return orgFareDetail;

		Map<FareComponent, Double> orgFc = orgFareDetail.getFareComponents();

		orgFareDetail = new GsonMapper<>(modFareDetail, orgFareDetail, FareDetail.class).convert();

		orgFareDetail.setFareComponents(orgFc);

		if (MapUtils.isEmpty(modFareDetail.getFareComponents()))
			return orgFareDetail;

		double totalDiff = 0d;

		double totalFare = orgFareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0d);

		for (Map.Entry<FareComponent, Double> entry : modFareDetail.getFareComponents().entrySet()) {

			FareComponent fc = entry.getKey();
			Double amount = entry.getValue();

			if (fc.airlineComponent() && !fc.equals(FareComponent.TF)) {
				totalDiff += amount - orgFareDetail.getFareComponents().getOrDefault(fc, 0d);
			}

			orgFareDetail.getFareComponents().put(fc, amount);
		}

		if (totalDiff <= 0) {
			orgFareDetail.setPaxCancellationFee(Math.abs(totalDiff));
		}
		orgFareDetail.getFareComponents().put(FareComponent.TF, totalFare + totalDiff);

		return orgFareDetail;
	}


	@Override
	public void PrePopulate() {}
}
