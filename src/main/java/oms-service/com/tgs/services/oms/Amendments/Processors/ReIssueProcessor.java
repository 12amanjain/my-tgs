package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.ARF;
import static com.tgs.services.base.enums.FareComponent.ARFT;
import static com.tgs.services.base.enums.FareComponent.CRF;
import static com.tgs.services.base.enums.FareComponent.CRFT;
import static com.tgs.services.base.enums.FareComponent.TF;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.restmodel.air.AirManualBookingRequest;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.Amendments.utils.AirAmendmentUtils;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.servicehandler.air.AirManualOrderHandler;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ReIssueProcessor extends AirAmendmentProcessor {

	@Autowired
	private AirManualOrderHandler manualOrderHandler;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private PaymentServiceCommunicator paymentService;

	@Autowired
	protected CommercialCommunicator cmsCommunicator;

	@Autowired
	protected OrderManager orderManager;

	@Override
	protected void processAmendment() {
		if (AmendmentHelper.isQuotation(amendment))
			return;

		GstInfo gstInfo = null;
		DbOrderBilling dbGst = gstService.findByBookingId(amendment.getBookingId());
		if (dbGst != null) {
			gstInfo = dbGst.toDomain();
		}

		double totalReIssueFee = 0d;
		double orderTotal = 0d;
		for (AirOrderItem airOrderItem : modifiedItems) {
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				traveller.getFareDetail().setPaxPreviousTF(0d);
				totalReIssueFee += traveller.getFareDetail().getFareComponents().getOrDefault(ARF, 0d);
				totalReIssueFee += traveller.getFareDetail().getFareComponents().getOrDefault(ARFT, 0d);
				totalReIssueFee += traveller.getFareDetail().getFareComponents().getOrDefault(CRF, 0d);
				totalReIssueFee += traveller.getFareDetail().getFareComponents().getOrDefault(CRFT, 0d);
				traveller.getFareDetail().getFareComponents().put(TF, BookingUtils.getPaxTotalFare(traveller));
				orderTotal += traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0.0);

				OmsHelper.updateGstComponents(traveller.getFareDetail(),
						userService.getUserFromCache(amendment.getBookingUserId()), gstInfo);
			}
			airOrderItem.setStatus(AirItemStatus.IN_PROGRESS.getCode());
			airOrderItem.setMarkup(0d);
		}
		amendment.getAirAdditionalInfo().setTotalReIssueFee(totalReIssueFee);
		amendment.getAdditionalInfo().setOrderDiffAmount(orderTotal);
		amendment.setAmendmentAmount();
		updateAirOrderItemAmount();
	}

	@Override
	protected void PrePopulate() {
		if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) {
			patchedItems.forEach(airOrderItem -> {
				AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
						.fmsCommunicator(fmsCommunicator).item(new DbAirOrderItem().from(airOrderItem)).build();
				SegmentInfo segmentInfo = mapper.convert();
				fmsCommunicator.updateCancellationAndRescheduleFees(segmentInfo, bookingUser);
				setUpdatedFDForTravellers(segmentInfo, airOrderItem);
			});
		}
	}

	@Override
	public List<AirOrderItem> patchAirOrderItems(boolean mergeAmendment) {
		this.mergeAmendment = mergeAmendment;
		patchedItems = BaseModel.toDomainList(orgItems);
		if (mergeAmendment && !AmendmentHelper.isQuotation(amendment)) {
			createNewOrderItems();
			return patchedItems;
		} else {
			filterSegments(true);
			filterTravellers(true);
			if (AmendmentHelper.isQuotation(amendment))
				return patchedItems;
		}

		// If no data in modified items, it means amendment is unprocessed and old data should be shown
		if (!CollectionUtils.isEmpty(modifiedItems)) {
			patchedItems = modifiedItems;
		} else {
			patchedItems.forEach(item -> item.getTravellerInfo().forEach(p -> {
				p.setOtherServicesText(null);
				p.setSsrBaggageInfo(null);
				p.setSsrMealInfo(null);
				p.setSsrSeatInfo(null);
				p.getFareDetail().getFareComponents().keySet().forEach(k -> {
					if (!k.usedInAmendment())
						p.getFareDetail().getFareComponents().put(k, null);
				});
			}));
		}
		return patchedItems;
	}

	private void createNewOrderItems() {
		modifiedItems.forEach(AirBookingUtils::setAirlineNAirportInfo);
		String newBookingId = ServiceUtils.generateId(Product.getProductMetaInfoFromId(amendment.getBookingId()));
		AirManualBookingRequest manualBookingRequest = new AirManualBookingRequest();
		manualBookingRequest.setAirOrderItems(modifiedItems);
		manualBookingRequest.setForReIssue(true);
		manualBookingRequest.setUserId(amendment.getBookingUserId());
		manualBookingRequest.setIsGroupBook(false);
		manualBookingRequest.setBookingId(newBookingId);
		manualBookingRequest.setDeliveryInfo(order.getDeliveryInfo());
		manualBookingRequest.setFlowType(OrderFlowType.REISSUED);
		manualBookingRequest.setGstInfo(gstInfo);
		manualBookingRequest.setType(OrderType.AIR);
		manualBookingRequest.setAutoPay(true);
		setOldNewBookingIds(newBookingId);
		modifiedItems.forEach(item -> {
			item.setBookingId(newBookingId);
			item.setStatus(AirItemStatus.IN_PROGRESS.getCode());
			item.getAdditionalInfo()
					.setDuration(Duration.between(item.getDepartureTime(), item.getArrivalTime()).toMinutes());
			AirAmendmentUtils.updateAdditionalInfoFromOrgItems(item, orgItems);
			item.setId(null);
		});

		updatePaymentRequests(manualBookingRequest, amendment.getBookingId());
		BookingResponse bookingResponse = new BookingResponse();
		manualOrderHandler.initData(manualBookingRequest, bookingResponse);
		try {
			manualOrderHandler.getResponse();
		} catch (Exception ex) {
			log.error("Unable to create Reissue order for amendment Id {} , bookingId {}", amendment.getAmendmentId(),
					amendment.getBookingId());
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	private void setOldNewBookingIds(String newBookingId) {
		patchedItems.forEach(item -> item.getTravellerInfo().forEach(pax -> {
			if (amendment.getAdditionalInfo().getAirAdditionalInfo().amdContainsPax(item.getId(), pax.getId())) {
				pax.setStatus(TravellerStatus.REISSUED);
				pax.setNewBookingId(newBookingId);
			}
		}));
		modifiedItems
				.forEach(item -> item.getTravellerInfo().forEach(pax -> pax.setOldBookingId(amendment.getBookingId())));
	}

	private List<AirOrderItem> updatePaymentRequests(AirManualBookingRequest manualBookingRequest,
			String oldBookingId) {
		List<DbAirOrderItem> dbList =
				DbAirOrderItem.builder().build().toDbList(manualBookingRequest.getAirOrderItems());
		List<SegmentInfo> segmentList = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(dbList, null);
		GstInfo oldOrderbillingEntity = orderManager.getGstInfo(oldBookingId);
		List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentList);
		if (isUserCCForPreviousPayment()
				&& cmsCommunicator.isVirtualCreditCardSupported(tripInfos, bookingUser, oldOrderbillingEntity)) {
			for (TripInfo trip : tripInfos) {
				CreditCardInfo cardInfo =
						cmsCommunicator.getVirtualCreditCard(trip, bookingUser, oldOrderbillingEntity);
				for (SegmentInfo segment : trip.getSegmentInfos()) {
					DbAirOrderItem item = AirBookingUtils.findOrderItemFromSegmentInfo(segment, dbList);
					AdditionalAirOrderItemInfo itemAdditionalInfo = ObjectUtils.firstNonNull(item.getAdditionalInfo(),
							AdditionalAirOrderItemInfo.builder().build());

					// Applicable for virtual payment.
					itemAdditionalInfo.setCcId(cardInfo.getId().intValue());
					segment.getPriceInfo(0).getMiscInfo().setCcInfoId(cardInfo.getId().intValue());
					itemAdditionalInfo.setIsUserCreditCard(true);
					item.setAdditionalInfo(itemAdditionalInfo);
				}
			}
			createPaymentRequests(tripInfos, manualBookingRequest, PaymentMedium.VIRTUAL_PAYMENT);
		} else {
			createPaymentRequests(tripInfos, manualBookingRequest, null);
		}
		modifiedItems = DbAirOrderItem.toDomainList(dbList);
		manualBookingRequest.setAirOrderItems(modifiedItems);
		return manualBookingRequest.getAirOrderItems();
	}

	private void createPaymentRequests(List<TripInfo> trips, AirManualBookingRequest manualBookingRequest,
			PaymentMedium paymentMedium) {
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
		PaymentRequest paymentRequest = PaymentRequest.builder().build().setAmount(amendment.getAmount())
				.setAmendmentId(amendment.getAmendmentId()).setOpType(PaymentOpType.DEBIT)
				.setPayUserId(amendment.getBookingUserId()).setTransactionType(PaymentTransactionType.PAID_FOR_ORDER)
				.setAdditionalInfo(additionalInfo).setAllowZeroAmount(true);
		paymentRequest.setRefId(manualBookingRequest.getBookingId());
		if (PaymentMedium.VIRTUAL_PAYMENT.equals(paymentMedium)) {
			Map<String, BookingSegments> bookingSegments =
					fmsCommunicator.getSupplierWiseBookingSegmentsUsingPNR(trips);
			paymentRequest.setPaymentMedium(PaymentMedium.VIRTUAL_PAYMENT);
			paymentRequest.setProduct(Product.getProductFromId(amendment.getBookingId()));
			if (MapUtils.isNotEmpty(bookingSegments)) {
				for (Entry<String, BookingSegments> entry : bookingSegments.entrySet()) {
					paymentRequestList.add(AirBookingUtils.createVirtualPaymentRequest(paymentRequest,
							entry.getValue().getSegmentInfos(), bookingUser));
				}
			}
		} else {
			WalletPaymentRequest walletPaymentRequest = new WalletPaymentRequest(paymentRequest);
			if (!paymentService.hasSufficientBalance(walletPaymentRequest)) {
				throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
			}
			paymentRequest.setProduct(Product.getProductFromId(amendment.getBookingId()));
			paymentRequestList.add(paymentRequest);
		}
		manualBookingRequest.setPaymentInfos(paymentRequestList);
	}

	private boolean isUserCCForPreviousPayment() {
		if (CollectionUtils.isNotEmpty(orgItems)) {
			for (DbAirOrderItem item : orgItems) {
				if (item.getAdditionalInfo() != null
						&& BooleanUtils.isTrue(item.getAdditionalInfo().getIsUserCreditCard())) {
					return true;
				}
			}
		}
		return false;
	}
}
