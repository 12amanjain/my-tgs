package com.tgs.services.oms.Amendments.Processors;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import static com.tgs.services.base.enums.FareComponent.BP;
import static com.tgs.services.base.enums.FareComponent.MP;
import static com.tgs.services.base.enums.FareComponent.SP;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SsrAmendmentProcessor extends AirAmendmentProcessor {

	@Override
	public void processAmendment() {

		for (AirOrderItem airOrderItem : patchedItems) {
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				updateFareComponents(traveller);
			}
		}
	}

	private static void updateFareComponents(FlightTravellerInfo traveller) {

		FareDetail fareDetail = traveller.getFareDetail();

		double totalFare = fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0);

		if (traveller.getSsrBaggageInfo() != null && traveller.getSsrBaggageInfo().getAmount() != null) {
			fareDetail.getFareComponents().put(BP, ObjectUtils.firstNonNull(fareDetail.getFareComponents().get(BP), 0d)
					+ traveller.getSsrBaggageInfo().getAmount());
			totalFare += traveller.getSsrBaggageInfo().getAmount();
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		}

		if (traveller.getSsrMealInfo() != null && traveller.getSsrMealInfo().getAmount() != null) {
			fareDetail.getFareComponents().put(MP, ObjectUtils.firstNonNull(fareDetail.getFareComponents().get(MP), 0d)
					+ traveller.getSsrMealInfo().getAmount());
			totalFare += ObjectUtils.firstNonNull(traveller.getSsrMealInfo().getAmount(), 0d);
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		}

		if (traveller.getSsrSeatInfo() != null && traveller.getSsrSeatInfo().getAmount() != null) {
			fareDetail.getFareComponents().put(SP, ObjectUtils.firstNonNull(fareDetail.getFareComponents().get(SP), 0d)
					+ traveller.getSsrSeatInfo().getAmount());
			totalFare += ObjectUtils.firstNonNull(traveller.getSsrSeatInfo().getAmount(), 0d);
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		}
	}

	@Override
	public void PrePopulate() {
		if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) {
			List<TripInfo> trips = airOrderCommunicator.findTripByBookingId(patchedItems.get(0).getBookingId());
			patchedItems.forEach(airOrderItem -> {
				AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
						.fmsCommunicator(fmsCommunicator).item(new DbAirOrderItem().from(airOrderItem)).build();
				SegmentInfo segmentInfo = mapper.convert();
				trips.forEach(trip -> {
					trip.getSegmentInfos().forEach(segmentInf -> {
						if (segmentInf.getId().equals(segmentInfo.getId())
								&& segmentInf.getSegmentNum().intValue() == 0) {
							fmsCommunicator.updateAmendmentClientFee(AmendmentType.SSR, segmentInfo,
									usCommunicator.getUserFromCache(amendment.getBookingUserId()), FareComponent.CAF);
							setUpdatedFDForTravellers(segmentInfo, airOrderItem);
						}
					});
				});
			});
		}
	}

}
