package com.tgs.services.oms.Amendments.Processors;

import static com.tgs.services.base.enums.FareComponent.*;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.MapUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.utils.air.AirBookingUtils;

@Service
public class VoidProcessor extends AirAmendmentProcessor {

	@Override
	public void processAmendment() {

		double totalAmtApplicableForRefund, totalCancelFee, commissionRecall, totalTds, partnerMarkUpRecalled,
				partnerCommissionRecalled, partnerMarkUpTds, partnerCommTds;

		totalAmtApplicableForRefund = totalCancelFee = commissionRecall =
				totalTds = partnerMarkUpRecalled = partnerCommissionRecalled = partnerMarkUpTds = partnerCommTds = 0d;

		amendment.getAdditionalInfo().setRecallCommission(BaseUtils.isRecalCommission(true, bookingUser.getRole()));
		amendment.getAdditionalInfo().setReturnTds(true);

		for (AirOrderItem airOrderItem : patchedItems) {
			for (FlightTravellerInfo traveller : airOrderItem.getTravellerInfo()) {
				traveller.setStatus(TravellerStatus.VOIDED);
				double paxCancelFee = 0d;
				totalAmtApplicableForRefund += handleRefundableComponents(traveller);
				double totalPaxFare = traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(ACF, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(ACFT, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(CCF, 0d);
				paxCancelFee += traveller.getFareDetail().getFareComponents().getOrDefault(CCFT, 0d);
				totalCancelFee += paxCancelFee;
				traveller.getFareDetail().setPaxCancellationFee(paxCancelFee);
				traveller.getFareDetail().getFareComponents().put(TF, totalPaxFare + paxCancelFee);
				commissionRecall += AirBookingUtils.getGrossCommissionForPax(traveller, bookingUser, false);
				// partnerMarkUpRecalled += AirBookingUtils.getPartnerMarkUpForPax(traveller);
				partnerCommissionRecalled += AirBookingUtils.getPartnerCommissionForPax(traveller);
				totalTds += traveller.getFareDetail().getFareComponents().getOrDefault(TDS, 0d);
				partnerMarkUpTds += traveller.getFareDetail().getFareComponents().getOrDefault(PMTDS, 0d);
				partnerCommTds += traveller.getFareDetail().getFareComponents().getOrDefault(PCTDS, 0d);
				zeroOut(traveller.getFareDetail(), FareComponent.getAllCommisionComponents());
				traveller.getFareDetail().getFareComponents().put(TDS, 0d);
			}
		}
		amendment.getAdditionalInfo().setAmountApplicableForRefund(totalAmtApplicableForRefund);
		amendment.getAirAdditionalInfo().setTotalCancellationFee(totalCancelFee);
		amendment.getAdditionalInfo().setTotalCommissionRecalled(commissionRecall);
		amendment.getAdditionalInfo().setTotalPartnerMarkupRecalled(partnerMarkUpRecalled);
		amendment.getAdditionalInfo().setTotalPartnerCommissionRecalled(partnerCommissionRecalled);
		amendment.getAdditionalInfo().setTdsToReturn(totalTds);
		amendment.getAdditionalInfo().setPartnerMarkUpTdsToReturn(partnerMarkUpTds);
		amendment.getAdditionalInfo().setPartnerCommissionTdsToReturn(partnerCommTds);
	}


	private double handleRefundableComponents(FlightTravellerInfo traveller) {
		double refundableAmount = traveller.getFareDetail().getFareComponents().getOrDefault(TF, 0d)
				- traveller.getFareDetail().getFareComponents().getOrDefault(PF, 0d)
				- traveller.getFareDetail().getFareComponents().getOrDefault(RP, 0d)
				- traveller.getFareDetail().getFareComponents().getOrDefault(PMU, 0d);
		traveller.getFareDetail().setMaxRefundableAmount(refundableAmount);
		traveller.getFareDetail().getFareComponents().put(AAR, refundableAmount);

		Set<FareComponent> keepComponents = new HashSet<>();
		keepComponents.add(TF);
		keepComponents.add(PF);
		keepComponents.add(TDS);
		keepComponents.addAll(getAllCommisionComponents());
		keepComponents.addAll(voidComponents());
		keepComponents.addAll(FareComponent.voucherComponents());
		zeroOut(traveller.getFareDetail(), Stream.of(FareComponent.values()).filter(fc -> !keepComponents.contains(fc))
				.collect(Collectors.toSet()));
		traveller.getFareDetail().getFareComponents().computeIfPresent(TF, (key, val) -> val - refundableAmount);
		return traveller.getFareDetail().getFareComponents().get(AAR);
	}


	@Override
	public void PrePopulate() {
		/*
		 * if (amendment.getStatus().equals(AmendmentStatus.ASSIGNED)) { List<TripInfo> trips =
		 * airOrderCommunicator.findTripByBookingId(patchedItems.get(0).getBookingId());
		 * patchedItems.forEach(airOrderItem -> { AirOrderItemToSegmentInfoMapper mapper =
		 * AirOrderItemToSegmentInfoMapper.builder() .fmsCommunicator(fmsCommunicator).item(new
		 * DbAirOrderItem().from(airOrderItem)).build(); SegmentInfo segmentInfo = mapper.convert(); trips.forEach(trip
		 * -> { trip.getSegmentInfos().forEach(segmentInf -> { if (segmentInf.getId().equals(segmentInfo.getId()) &&
		 * segmentInf.getSegmentNum().intValue() == 0) { fmsCommunicator.updateAmendmentClientFee(AmendmentType.VOIDED,
		 * segmentInfo, usCommunicator.getUserFromCache(amendment.getBookingUserId()), FareComponent.CAF);
		 * setUpdatedFDForTravellers(segmentInfo, airOrderItem); } }); }); }); }
		 */
	}


	protected FareDetail patchFareDetail(FareDetail modFareDetail, FareDetail fareDetail) {
		if (modFareDetail == null || modFareDetail.getFareComponents() == null)
			return fareDetail;
		Map<FareComponent, Double> orgFc = fareDetail.getFareComponents();
		fareDetail = new GsonMapper<>(modFareDetail, fareDetail, FareDetail.class).convert();
		fareDetail.setFareComponents(orgFc);
		fareDetail.setOrgManualRefundableFC(fareDetail.getSubSet(FareComponent.getManualRefundableFC()));
		if (mergeAmendment) {
			if (MapUtils.isEmpty(modFareDetail.getFareComponents()))
				return fareDetail;
			for (Map.Entry<FareComponent, Double> entry : modFareDetail.getFareComponents().entrySet()) {
				FareComponent key = entry.getKey();
				Double amendVal = entry.getValue();
				fareDetail.getFareComponents().put(key, amendVal);
			}
			fareDetail.getFareComponents().put(TF, modFareDetail.getFareComponents().get(TF));
		} else {
			if (MapUtils.isEmpty(modFareDetail.getFareComponents()))
				return fareDetail;
			for (Map.Entry<FareComponent, Double> entry : modFareDetail.getFareComponents().entrySet()) {
				FareComponent fc = entry.getKey();
				Double amount = entry.getValue();
				fareDetail.getFareComponents().put(fc, amount);
			}
		}
		return fareDetail;
	}

	protected void unsetFields(List<DbAirOrderItem> airOrderItemList) {
		airOrderItemList.forEach(item -> item.getTravellerInfo().forEach(pax -> {
			pax.setOtherServicesText(null);
			pax.setSsrBaggageInfo(null);
			pax.setSsrMealInfo(null);
			pax.setSsrSeatInfo(null);
			/*
			 * for (Map.Entry<FareComponent, Double> entry : pax.getFareDetail().getFareComponents().entrySet()) {
			 * FareComponent key = entry.getKey(); if (key.usedInAmendment()) { entry.setValue(null); } }
			 */
		}));
	}
}
