package com.tgs.services.oms.communicator.impl;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.AmendmentFilter;
import com.tgs.services.base.communicator.AmendmentCommunicator;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.jparepository.AmendmentService;

@Service
public class AmendmentCommunicatorImpl implements AmendmentCommunicator {

	@Autowired
	private AmendmentService service;

	@Override
	public List<Amendment> fetchAmendments(List<String> amendmentIds) {
		if (CollectionUtils.isEmpty(amendmentIds)) {
			return new ArrayList<>();
		}
		return service.search(AmendmentFilter.builder().amendmentIdIn(amendmentIds).build());
	}

	@Override
	public List<AirOrderItem> filterItemsNPax(Amendment amendment, List<AirOrderItem> airOrderItems) {
		return AmendmentHelper.filterItemsNPax(amendment, airOrderItems);
	}
}
