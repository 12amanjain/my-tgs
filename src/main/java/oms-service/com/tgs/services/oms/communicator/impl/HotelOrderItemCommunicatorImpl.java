package com.tgs.services.oms.communicator.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;

@Service
public class HotelOrderItemCommunicatorImpl implements HotelOrderItemCommunicator {

	@Autowired
	private HotelOrderItemManager itemManager;

	@Autowired
	private HotelOrderItemService service;

	@Override
	public void updateOrderItem(HotelInfo hInfo, Order order, HotelItemStatus itemStatus) {
		itemManager.updateOrderItem(hInfo, order, itemStatus);
	}

	@Override
	public void updateOrderItemStatus(Order order, HotelItemStatus itemStatus) {
		itemManager.updateOrderItemStatus(order, itemStatus);
	}

	@Override
	public List<HotelOrderItem> getHotelOrderItems(List<String> bookingIds) {
		if (CollectionUtils.isEmpty(bookingIds)) {
			return new ArrayList<>();
		}
		return service.findByBookingId(bookingIds);
	}

	@Override
	public HotelInfo getHotelInfo(String bookingId) {
		List<DbHotelOrderItem> items = itemManager.getItemList(bookingId);
		return DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
	}

}
