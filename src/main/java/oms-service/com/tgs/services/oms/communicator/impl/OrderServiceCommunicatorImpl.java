package com.tgs.services.oms.communicator.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PNRCreditInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.air.ProcessedAirDetails;
import com.tgs.services.oms.datamodel.air.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.ItemManager;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.ums.datamodel.User;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderServiceCommunicatorImpl implements OrderServiceCommunicator {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AirOrderItemManager airOrderItemManager;

	@Autowired
	private HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	@Lazy
	private PaymentServiceCommunicator pmsCommunicator;

	@Autowired
	@Lazy
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private AirOrderItemService orderItemService;

	@Override
	public GstInfo getGstInfo(String bookingId) {
		return orderManager.getGstInfo(bookingId);
	}

	@Override
	public void processBooking(String bookingId) {
		Order order = orderManager.findByBookingId(bookingId, null);
		if (order != null) {
			ItemManager itemManager = getItemManager(order.getOrderType());
			if (itemManager != null) {
				PaymentsData paymentsData = getPaymentsData(bookingId);
				boolean isFailedPaymentForHoldBooking = PaymentStatus.FAILURE.equals(paymentsData.paymentStatus)
						&& OrderStatus.ON_HOLD.equals(order.getStatus());
				if (!isFailedPaymentForHoldBooking) {
					itemManager.processBooking(bookingId, order, paymentsData.paymentStatus,
							paymentsData.totalPaymentFee);
				}
			}
		}
	}

	private ItemManager getItemManager(OrderType orderType) {
		if (orderType.equals(OrderType.AIR)) {
			return airOrderItemManager;
		} else if (orderType.equals(OrderType.HOTEL)) {
			return hotelOrderItemManager;
		}
		return null;
	}

	@Override
	public Order findByBookingId(String bookingId) {
		return StringUtils.isNotBlank(bookingId) ? orderManager.findByBookingId(bookingId, null) : null;
	}

	@Override
	public List<Order> getOrders(List<String> bookingIds) {
		return orderService.findByBookingIds(bookingIds);
	}

	@Override
	public void updateOrder(Order order) {
		orderService.save(new DbOrder().from(order));
	}

	private PaymentsData getPaymentsData(String bookingId) {
		PaymentStatus paymentStatus = PaymentStatus.FAILURE;
		double totalPaymentFee = 0;
		/**
		 * Won't work in case of partial payments which is not allowed for external payments.
		 */
		List<Payment> payments = pmsCommunicator.search(PaymentFilter.builder().refId(bookingId)
				.type(PaymentTransactionType.PAID_FOR_ORDER).status(PaymentStatus.SUCCESS).build());
		if (CollectionUtils.isNotEmpty(payments)) {
			totalPaymentFee = payments.get(0).getPaymentFee().doubleValue();
			paymentStatus = PaymentStatus.SUCCESS;
		}
		return new PaymentsData(paymentStatus, totalPaymentFee);
	}

	@AllArgsConstructor
	private class PaymentsData {
		PaymentStatus paymentStatus;
		double totalPaymentFee;
	}

	@Override
	public double getGrossCommission(Map<FareComponent, Double> fareComponents, User user,
			boolean includePartnerCommission) {
		return AirBookingUtils.getGrossCommission(fareComponents, user, includePartnerCommission);
	}

	@Override
	public void validateSearchQueryOnPNR(AirSearchQuery searchQuery) throws CustomGeneralException {
		PNRCreditInfo creditInfo = PNRCreditInfo.builder().build();
		String pnr = searchQuery.getSearchModifiers().getPnrCreditInfo().getPnr();

		TravellerInfoFilter travellerFilter = TravellerInfoFilter.builder().pnr(pnr).build();
		AirOrderItemFilter itemFilter = AirOrderItemFilter.builder().travellerInfoFilter(travellerFilter).build();
		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.SUCCESS))
				.bookingUserIds(Arrays.asList(SystemContextHolder.getContextData().getUser().getParentUserId()))
				.itemFilter(itemFilter).build();

		List<Object[]> results = orderItemService.findByJsonSearch(orderFilter);
		if (CollectionUtils.isEmpty(results)) {
			throw new CustomGeneralException(SystemError.NO_PNR_FOUND);
		}

		List<DbAirOrderItem> filteredItem = new ArrayList<DbAirOrderItem>();

		for (Object[] result : results) {
			filteredItem.add((DbAirOrderItem) result[0]);
		}

		if (CollectionUtils.isEmpty(filteredItem)) {
			throw new CustomGeneralException(SystemError.NO_PNR_FOUND);
		}

		Map<PaxType, Integer> orderPaxInfo = BaseUtils.getPaxInfo(filteredItem.get(0).getTravellerInfo());
		Map<PaxType, Integer> searchQueryPaxInfo = searchQuery.getPaxInfo();
		for (PaxType paxType : PaxType.values()) {
			if (searchQueryPaxInfo.getOrDefault(paxType, 0) != orderPaxInfo.getOrDefault(paxType, 0)) {
				throw new CustomGeneralException(SystemError.PNR_PASSENGER_MISMATCH);
			}
		}

		SupplierConfiguration supplierConfig =
				fmsCommunicator.getSupplierConfiguration(filteredItem.get(0).getSupplierId());


		AirCreditShellRequest creditShellRequest =
				AirCreditShellRequest.builder().travellerInfos(filteredItem.get(0).getTravellerInfo())
						.supplierConfig(supplierConfig).airline(filteredItem.get(0).getAirlinecode()).pnr(pnr).build();
		creditShellRequest.setBookingId(filteredItem.get(0).getBookingId());
		AirCreditShellResponse csResponse = null;
		try {
			csResponse = fmsCommunicator.fetchCreditBalance(creditShellRequest);
		} catch (Exception e) {
			if (e instanceof CustomGeneralException) {
				throw new CustomGeneralException(((CustomGeneralException) e).getError());
			}
			log.error("Error Occured for PNR {} ,due to ", pnr, e);
			throw new CustomGeneralException(SystemError.NO_PNR_FOUND);
		}

		if (csResponse == null || csResponse.getCreditBalance() == null) {
			throw new CustomGeneralException(SystemError.NO_PNR_FOUND);
		} else if (Double.compare(csResponse.getCreditBalance().doubleValue(), 0d) == 0) {
			throw new CustomGeneralException(SystemError.CREDIT_SHELL_BALANCE_NA);
		}

		creditInfo.setPnr(pnr);
		creditInfo.setCreditBalance(csResponse.getCreditBalance());
		creditInfo.setCSExpiryDate(csResponse.getExpiryDate());
		creditInfo.setBookingId(filteredItem.get(0).getBookingId());
		searchQuery.getSearchModifiers().setSourceId(filteredItem.get(0).getAdditionalInfo().getSourceId());
		searchQuery.setSupplierIds(Arrays.asList(filteredItem.get(0).getSupplierId()));
		searchQuery.getSearchModifiers().setPnrCreditInfo(creditInfo);
	}

	@Override
	public List<TravellerInfo> getFlightTravellers(String pnr) {
		List<TravellerInfo> travellers = new ArrayList<>();
		TravellerInfoFilter travellerInfoFilter = TravellerInfoFilter.builder().pnr(pnr).build();
		AirOrderItemFilter itemFilter = AirOrderItemFilter.builder().travellerInfoFilter(travellerInfoFilter).build();
		OrderFilter orderFilter = OrderFilter.builder().itemFilter(itemFilter)
				.bookingUserIds(Arrays.asList(SystemContextHolder.getContextData().getUser().getParentUserId()))
				.build();
		List<ProcessedBookingDetail> bookingDetails = airOrderItemManager.getProcessedItemDetails(orderFilter);
		if (CollectionUtils.isNotEmpty(bookingDetails)) {
			ProcessedBookingDetail bookingDetail = bookingDetails.get(0);
			ProcessedAirDetails processedAirDetail =
					(ProcessedAirDetails) bookingDetail.getItems().get(OrderType.AIR.name());
			for (ProcessedFlightTravellerInfo processedTraveller : processedAirDetail.getTravellerInfos()) {
				travellers
						.add(new GsonMapper<>(processedTraveller, new TravellerInfo(), TravellerInfo.class).convert());
			}
			travellers.forEach(traveller -> {
				traveller.setId(null);
				traveller.setInvoice(null);
				traveller.setUserId(null);
				traveller.setUserProfile(null);
			});
		}
		return travellers;
	}

}
