package com.tgs.services.oms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.hibernate.DeliveryInfoType;
import com.tgs.services.oms.hibernate.OrderAdditionalInfoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "orders")
@Getter
@Setter
@Audited
@TypeDefs({@TypeDef(name = "DeliveryInfoType", typeClass = DeliveryInfoType.class),
		@TypeDef(name = "OrderAdditionalInfoType", typeClass = OrderAdditionalInfoType.class)})
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class DbOrder extends SuperBaseModel<DbOrder, Order> {

	/**
	 * Whenever you are adding/removing any column from this class , make sure to add or remove it in corresponding data
	 * model class as well
	 */
	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "order_generator")
	@SequenceGenerator(name = "order_generator", sequenceName = "orders_id_seq", allocationSize = 1)
	private Long id;
	@Column
	private String bookingId;
	@Column
	private String bookingUserId;
	@Column
	private String partnerId;

	/**
	 * Total Amount should be exclusive of markup Amount
	 */
	@Column
	private double amount;

	@Column
	private double markup;

	@Column
	private String orderType;

	@Column
	private String status;

	@Column
	private String channelType;

	@CreationTimestamp
	@Column(updatable = false)
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private String loggedInUserId;

	@Column
	@Type(type = "DeliveryInfoType")
	private DeliveryInfo deliveryInfo;

	@Column
	@Type(type = "OrderAdditionalInfoType")
	private OrderAdditionalInfo additionalInfo;

	private String reason;

	@Access(value = AccessType.PROPERTY)
	@Column
	public String getStatus() {
		return status;
	}

	public static DbOrder create(Order order) {
		return new DbOrder().from(order);
	}

	@Override
	public Order toDomain() {
		return new GsonMapper<>(this, Order.class).convert();
	}

	@Override
	public DbOrder from(Order order) {
		return new GsonMapper<>(order, this, DbOrder.class).convert();
	}

	public OrderAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = OrderAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}
}
