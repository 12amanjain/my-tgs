package com.tgs.services.oms.dbmodel.air;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.UpdateTimestamp;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.hibernate.air.AdditionalAirOrderItemInfoType;
import com.tgs.services.oms.hibernate.air.FlightTravellerInfoType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "AirOrderItem")
@Table(name = "airorderitem")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({
		@TypeDef(name = "FlightTravellerInfoType", typeClass = FlightTravellerInfoType.class),
		@TypeDef(name = "AdditionalAirOrderItemInfoType", typeClass = AdditionalAirOrderItemInfoType.class)
})
public class DbAirOrderItem extends SuperBaseModel<DbAirOrderItem, AirOrderItem> {

	/**
	 * Whenever you are adding a column in this class, make sure to add it in
	 * corresponding data model class as well
	 */
	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "airorder_generator")
	@SequenceGenerator(name="airorder_generator", sequenceName = "airorderitem_id_seq",allocationSize=1)
	private Long id;
	
	@Column
	private String bookingId;

	@Column
	private String source;

	@Column
	private String dest;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@UpdateTimestamp
	private LocalDateTime processedOn;

	@Column
	private LocalDateTime departureTime;

	@Column
	private LocalDateTime arrivalTime;

	@Column
	private String supplierId;

	@Column
	@Type(type = "FlightTravellerInfoType")
	private List<FlightTravellerInfo> travellerInfo;

	/**
	 * Total Amount should be exclusive of markup Amount
	 */
	@Column
	private Double amount;

	@Column
	private Double markup;

	@Column
	private String status;

	@Column
	private String airlinecode;

	@Column
	private String flightNumber;

	@Column
	@Type(type = "AdditionalAirOrderItemInfoType")
	private AdditionalAirOrderItemInfo additionalInfo;

	public AirOrderItem toDomain() {
		return new GsonMapper<>(this, AirOrderItem.class).convert();
	}

	public DbAirOrderItem from(AirOrderItem dataModel) {
		return new GsonMapper<>(dataModel, new DbAirOrderItem(), DbAirOrderItem.class).convert();
	}
}
