package com.tgs.services.oms.dbmodel.hotel;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliation;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliationAdditionalInfo;
import com.tgs.services.oms.datamodel.hotel.ProcessedRoomInfo;
import com.tgs.services.oms.hibernate.hotel.HotelReconciliationAdditionalInfoType;
import com.tgs.services.oms.hibernate.hotel.ProcessedRoomInfoType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "hotelbookingreconciliation")
@Table(name = "hotelbookingreconciliation")
@Builder
@Audited
@NoArgsConstructor
@AllArgsConstructor
@TypeDefs({@TypeDef(name = "ProcessedRoomInfoType", typeClass = ProcessedRoomInfoType.class), @TypeDef(
		name = "HotelReconciliationAdditionalInfoType", typeClass = HotelReconciliationAdditionalInfoType.class)})
public class DbHotelBookingReconciliation extends BaseModel<DbHotelBookingReconciliation, HotelBookingReconciliation> {

	@Column
	private String bookingId;

	@Column
	private String bookingStatus;

	@Column
	private String hotelName;

	@Column
	private LocalDate checkInDate;

	@Column
	private LocalDate checkOutDate;

	@Column
	private LocalDate bookingDate;

	@Column
	private Double amount;

	@Column
	private String reconcilerUserId;

	@Column
	private LocalDateTime cancellationDeadline;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	@Type(type = "HotelReconciliationAdditionalInfoType")
	private HotelBookingReconciliationAdditionalInfo additionalInfo;

	@Column
	@Type(type = "ProcessedRoomInfoType")
	private List<ProcessedRoomInfo> roomInfos;

	@Override
	public HotelBookingReconciliation toDomain() {
		return new GsonMapper<>(this, HotelBookingReconciliation.class).convert();
	}

	public DbHotelBookingReconciliation from(HotelBookingReconciliation dataModel) {
		return new GsonMapper<>(dataModel, new DbHotelBookingReconciliation(), DbHotelBookingReconciliation.class)
				.convert();
	}
}
