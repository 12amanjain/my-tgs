package com.tgs.services.oms.helper;

import java.util.List;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OmsHelper {

	@Autowired
	private static GeneralServiceCommunicator generalServiceCommunicator;

	@Autowired
	public OmsHelper(GeneralServiceCommunicator generalServiceCommunicator) {
		OmsHelper.generalServiceCommunicator = generalServiceCommunicator;
	}

	public static ClientGeneralInfo getClientInfo() {

		ConfiguratorInfo info = generalServiceCommunicator.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO);
		return (info == null) ? null : (ClientGeneralInfo) info.getOutput();
	}

	private static boolean isIgstApplicable(User bookingUser, GstInfo gstInfo) {
		boolean igstApplicable = true;
		ClientGeneralInfo clientInfo = OmsHelper.getClientInfo();
		try {
			log.info("Client State : {} , bookingUser State : {}", clientInfo.getState(),
					bookingUser.getAddressInfo().getCityInfo().getState());
		} catch (Exception e) {
			log.error("Unable to fetch details for GST ", e);
		}
		// For B2C user IGST is always applicable.
		if (!UserUtils.isB2CUser(bookingUser) && clientInfo != null && StringUtils.isNotEmpty(clientInfo.getState())) {
			if (UserRole.corporate(bookingUser.getRole())) {
				igstApplicable = isIgstApplicableOnGSTInfo(clientInfo, bookingUser, gstInfo);
			} else {
				igstApplicable = isIgstApplicableOnAddressInfo(clientInfo, bookingUser);
			}
		}
		return igstApplicable;
	}

	// Older data of gstInfo in which city & state were stored in address.
	private static boolean isIgstApplicableOnGSTInfo(ClientGeneralInfo clientInfo, User bookingUser, GstInfo gstInfo) {
		boolean igstApplicable = true;
		if (gstInfo != null) {
			if ((StringUtils.isNotEmpty(gstInfo.getState())
					&& clientInfo.getState().equalsIgnoreCase(gstInfo.getState()))
					|| (StringUtils.isNotEmpty(gstInfo.getAddress())
							&& gstInfo.getAddress().toUpperCase().contains(clientInfo.getState().toUpperCase()))) {
				igstApplicable = false;
			}
		} else {
			igstApplicable = isIgstApplicableOnAddressInfo(clientInfo, bookingUser);
		}
		return igstApplicable;
	}

	private static boolean isIgstApplicableOnAddressInfo(ClientGeneralInfo clientInfo, User bookingUser) {
		boolean igstApplicable = true;
		if (bookingUser != null && bookingUser.getAddressInfo() != null
				&& bookingUser.getAddressInfo().getCityInfo() != null
				&& clientInfo.getState().equalsIgnoreCase(bookingUser.getAddressInfo().getCityInfo().getState())) {
			igstApplicable = false;
		}
		return igstApplicable;
	}

	private static boolean isIgstApplicable(User bookingUser) {
		return isIgstApplicable(bookingUser, null);
	}

	public static void updateGstComponent(RoomInfo roomInfo, User bookingUser) {
		OmsHelper.updateHotelGstComponent(roomInfo, bookingUser);
		BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
	}

	public static void updateGstComponents(FareDetail fareDetail, User bookingUser, GstInfo gstInfo) {
		boolean igstApplicable = isIgstApplicable(bookingUser, gstInfo);
		double totalGST = BaseUtils.totalFareComponentsAmount(fareDetail, FareComponent.getGstComponents());
		if (totalGST > 0) {
			if (igstApplicable) {
				fareDetail.getFareComponents().put(FareComponent.IGST, totalGST);
			} else {
				fareDetail.getFareComponents().put(FareComponent.SGST, totalGST / 2);
				fareDetail.getFareComponents().put(FareComponent.CGST, totalGST / 2);
			}
		}
	}

	public static void updateHotelGstComponent(RoomInfo roomInfo, User bookingUser) {
		boolean igstApplicable = isIgstApplicable(bookingUser);
		List<PriceInfo> priceInfoList = roomInfo.getPerNightPriceInfos();

		for (PriceInfo price : priceInfoList) {
			double totalGST = BaseHotelUtils.totalFareComponentsAmount(
					MapUtils.isEmpty(price.getAddlFareComponents().get(HotelFareComponent.TAF))
							? price.getFareComponents()
							: price.getAddlFareComponents().get(HotelFareComponent.TAF),
					HotelFareComponent.getGstDependendentComponents());
			if (totalGST > 0) {
				if (igstApplicable) {
					price.getFareComponents().put(HotelFareComponent.IGST, totalGST);
				} else {
					price.getFareComponents().put(HotelFareComponent.SGST, totalGST / 2);
					price.getFareComponents().put(HotelFareComponent.CGST, totalGST / 2);
				}
			}
		}
	}
}
