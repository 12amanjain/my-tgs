package com.tgs.services.oms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.oms.datamodel.amendments.ModifiedInfo;

public class ModifiedInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return ModifiedInfo.class;
    }
}
