package com.tgs.services.oms.hibernate.hotel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;

public class AdditionalHotelOrderItemInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return AdditionalHotelOrderItemInfo.class;
	}
}
