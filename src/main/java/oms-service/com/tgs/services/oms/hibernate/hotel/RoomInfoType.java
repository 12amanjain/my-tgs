package com.tgs.services.oms.hibernate.hotel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.hms.datamodel.RoomInfo;

public class RoomInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return RoomInfo.class;
	}
}

