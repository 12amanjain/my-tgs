package com.tgs.services.oms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import com.tgs.services.oms.dbmodel.DbAmendment;

public interface AmendmentRepository extends JpaRepository<DbAmendment, Long>, JpaSpecificationExecutor<DbAmendment> {

	DbAmendment findByAmendmentId(String amdId);

	@Query(value = "SELECT nextval('amendment_invoice_id_seq')", nativeQuery = true)
	Integer getNextInvoiceId();

	// Being Used right now for ATLAS only.
	@Query(value = "SELECT nextval('amendment_dom_invoice_id_seq')", nativeQuery = true)
	Integer getNextChargedDomInvoiceId();

	// Being Used right now for ATLAS only.
	@Query(value = "SELECT nextval('amendment_int_invoice_id_seq')", nativeQuery = true)
	Integer getNextChargedIntInvoiceId();


}
