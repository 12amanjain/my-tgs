package com.tgs.services.oms.jparepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.AmendmentFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.DbAmendment;

@Service
public class AmendmentService extends SearchService<DbAmendment> {

	@Autowired
	private AmendmentRepository repo;

	@Autowired
	private AmendmentActionValidator actionValidator;

	static final List<AmendmentType> INVOICE_NA_AMENDMENT_TYPES = Arrays.asList(AmendmentType.FARE_CHANGE,
			AmendmentType.CANCELLATION_QUOTATION, AmendmentType.REISSUE_QUOTATION);

	public Amendment findByAmendmentId(String amdId) {
		DbAmendment dbAmendment = repo.findByAmendmentId(amdId);
		if (dbAmendment == null)
			return null;
		Amendment amendment = dbAmendment.toDomain();
		amendment
				.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
		return amendment;
	}

	public Amendment save(Amendment amendment) {
		amendment.setProcessedOn(LocalDateTime.now());
		return repo.saveAndFlush(DbAmendment.create(amendment)).toDomain();
	}

	public Amendment saveWithoutProcessedOn(Amendment amendment) {
		return repo.saveAndFlush(DbAmendment.create(amendment)).toDomain();
	}

	public void save(List<Amendment> amendmentList) {
		amendmentList.forEach(amendment -> save(amendment));
	}

	public void saveWithoutProcessedOn(List<Amendment> amendmentList) {
		amendmentList.forEach(amendment -> saveWithoutProcessedOn(amendment));
	}

	public List<Amendment> search(AmendmentFilter filter) {
		List<Amendment> amendmentList = BaseModel.toDomainList(super.search(filter, repo));
		if (filter.intl != null) {
			amendmentList = amendmentList.parallelStream()
					.filter(a -> filter.intl.equals(a.getAdditionalInfo().getAirAdditionalInfo().isInternational()))
					.collect(Collectors.toList());
		}
		amendmentList.forEach(amendment -> amendment.setActionList(
				actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)));
		amendmentList.sort(Comparator.comparing(
				x -> ObjectUtils.defaultIfNull(x.getAirAdditionalInfo().getFirstDeparture(), LocalDateTime.MIN)));
		return amendmentList;
	}

	public Amendment getFirstAmendment(String bookingId) {
		Amendment firstAmendment = null;
		AmendmentFilter filter = AmendmentFilter.builder().statusIn(Collections.singletonList(AmendmentStatus.SUCCESS))
				.bookingIdIn(Collections.singletonList(bookingId)).build();
		List<Amendment> amendmentList = this.search(filter);
		if (CollectionUtils.isNotEmpty(amendmentList)) {
			amendmentList.sort(Comparator.comparing(Amendment::getProcessedOn));
			for (Amendment amendment : amendmentList) {
				if (!INVOICE_NA_AMENDMENT_TYPES.contains(amendment.getAmendmentType())) {
					firstAmendment = amendment;
					break;
				}
			}
		}
		return firstAmendment;
	}

	public Integer getNextInvoiceId() {
		return repo.getNextInvoiceId();
	}

	public Integer getNextChargedDomInvoiceId() {
		return repo.getNextChargedDomInvoiceId();
	}

	public Integer getNextChargedIntInvoiceId() {
		return repo.getNextChargedIntInvoiceId();
	}
}
