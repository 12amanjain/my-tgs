package com.tgs.services.oms.jparepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.Predicate;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.utils.springframework.data.SpringDataUtils;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepo;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
	public DbOrder save(DbOrder order) {
		order.setProcessedOn(LocalDateTime.now());
		order = orderRepo.save(order);
		return order;
	}

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
	public DbOrder saveWithoutProcessedOn(DbOrder order) {
		order = orderRepo.save(order);
		return order;
	}

	public DbOrder findByBookingId(String bookingId) {
		return orderRepo.findByBookingId(bookingId);
	}

	public List<Order> findByBookingIds(List<String> bookingIds) {
		return BaseModel.toDomainList(orderRepo.findByBookingIdIn(bookingIds));
	}

	public List<DbOrder> findAll(OrderFilter filter) {
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(filter);
		List<DbOrder> dbOrderList = new ArrayList<>();
		Page<DbOrder> pageList = orderRepo.findAll((root, query, criteriaBuilder) -> {
			List<Predicate> predicates =
					OrderSearchPredicate.getPredicateListBasedOnOrderFilter(root, query, criteriaBuilder, filter);
			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		}, request);

		pageList.forEach(Order -> dbOrderList.add(Order));
		return dbOrderList;
	}

	public void save(List<DbOrder> orders) {
		// TODO
		for (DbOrder order : orders) {
			save(order);
		}
	}

	public boolean deleteExistingFailedAndNewBooking(String bookingId) {
		return !CollectionUtils.isEmpty(orderRepo.deleteFailedAndNewOrderByBookingId(bookingId));
	}

}
