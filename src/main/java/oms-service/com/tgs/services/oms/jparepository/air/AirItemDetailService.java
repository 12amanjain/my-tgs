package com.tgs.services.oms.jparepository.air;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewRequest;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.air.DbAirItemDetail;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirItemDetailService extends SearchService<DbAirItemDetail> {

	@Autowired
	private AitItemDetailRepository repository;

	@Autowired
	private FMSCachingServiceCommunicator cachingService;

	@Autowired
	protected UserServiceCommunicator userService;

	public DbAirItemDetail save(DbAirItemDetail airItemDetail) {
		return repository.saveAndFlush(airItemDetail);
	}

	public AirItemDetail get(String bookingId) {
		DbAirItemDetail dbAirItemDetail = repository.findByBookingId(bookingId);
		return dbAirItemDetail == null ? null : dbAirItemDetail.toDomain();
	}

	public void saveProfileFields(List<FlightTravellerInfo> paxList, String bookingId) {
		if (CollectionUtils.isNotEmpty(paxList)) {
			AirItemDetail existing = get(bookingId);
			if (existing == null) {
				existing = new AirItemDetail();
			}
			Map<String, Map<String, Object>> map = new HashMap<>();
			paxList.forEach(pax -> {
				Map<String, Object> userProfileFields = new HashMap<>();
				if (StringUtils.isNotEmpty(pax.getUserId())) {
					User employee = userService.getUserFromCache(pax.getUserId());
					// Employee user exists in our system
					if (employee != null) {
						userProfileFields.putAll(employee.getUserProfile().getData());
					}
				}
				// Give more priority to custom fields passed in booking
				userProfileFields.putAll(pax.getUserProfile().getData());
				map.put(pax.getPaxKey(), userProfileFields);
			});
			existing.getInfo().setProfileData(map);
			existing.setBookingId(bookingId);
			repository.save(new DbAirItemDetail().from(existing));
		}
	}

	public void saveLFF(List<DbAirOrderItem> selectedItems, Order order) {
		User bookingUser = userService.getUserFromCache(order.getBookingUserId());
		if (!UserRole.corporate(bookingUser.getRole()))
			return;
		AirItemDetail existing = get(order.getBookingId());
		if (existing == null)
			existing = new AirItemDetail();
		AirReviewRequest reviewRequest = cachingService.fetchValue(order.getBookingId(), AirReviewRequest.class,
				CacheSetName.LOWEST_PRICE_IDS.getName(), BinName.LPIDS.getName());
		if (reviewRequest == null)
			return;
		List<String> lpids = reviewRequest.getLowestPriceIds();
		if (CollectionUtils.isEmpty(lpids))
			return;
		List<TripInfo> trips = new ArrayList<>();
		for (String lpid : lpids) {
			trips.add(cachingService.getTripInfo(lpid));
		}
		if (CollectionUtils.isEmpty(trips))
			return;
		Map<String, Pair<Long, List<SegmentInfo>>> sectorMap = new HashMap<>();
		selectedItems.forEach(x -> {
			String sector = getKey(x);
			Pair<Long, List<SegmentInfo>> pair = Pair.of(x.getId(), new ArrayList<>());
			sectorMap.put(sector, pair);
		});

		List<SegmentInfo> segmentInfoList = new ArrayList<>();
		List<SegmentInfo> remaining = new ArrayList<>();
		trips.forEach(t -> segmentInfoList.addAll(t.getSegmentInfos()));
		for (SegmentInfo segmentInfo : segmentInfoList) {
			String key = getKey(segmentInfo);
			if (sectorMap.containsKey(key)) {
				sectorMap.get(key).getRight().add(segmentInfo);
			} else {
				remaining.add(segmentInfo);
			}
		}
		existing.setBookingId(order.getBookingId());
		existing.getInfo().setReason(reviewRequest.getAdditionalInfo().getReason());
		Map<Long, List<SegmentInfo>> lffMap = new HashMap<>();
		sectorMap.forEach((k, v) -> lffMap.put(v.getLeft(), sort(v.getRight())));
		existing.getInfo().setLff(lffMap);
		repository.saveAndFlush(new DbAirItemDetail().from(existing));
	}

	private static List<SegmentInfo> sort(List<SegmentInfo> segmentInfos) {
		try {
			segmentInfos
					.sort(Comparator.comparing(
							s -> s.getPriceInfo(0).getFareDetail(PaxType.ADULT).getFareComponents()
									.getOrDefault(FareComponent.TF, 0.0),
							Comparator.nullsLast(Comparator.naturalOrder())));
			return segmentInfos;
		} catch (Exception ex) {
			log.error("AirItemDetailService Exception while Sorting segmentInfos {}", ex.getMessage(), ex);
			return segmentInfos;
		}
	}

	private static String getKey(DbAirOrderItem item) {
		return item.getSource() + "-" + item.getDest();
	}

	private static String getKey(SegmentInfo segment) {
		return segment.getDepartureAirportCode() + "-" + segment.getArrivalAirportCode();
	}

}
