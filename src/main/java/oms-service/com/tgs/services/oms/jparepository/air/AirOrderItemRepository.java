package com.tgs.services.oms.jparepository.air;

import java.util.List;

import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface AirOrderItemRepository
		extends JpaRepository<DbAirOrderItem, Long>, JpaSpecificationExecutor<DbAirOrderItem> {

	List<DbAirOrderItem> findByBookingIdOrderById(String bookingId);

	List<DbAirOrderItem> findByBookingIdInOrderByBookingIdAscIdAsc(List<String> bookingIds);

	DbAirOrderItem findById(Long id);
	
	@Query(value = "SELECT nextval('atlas_air_invoice_id_seq')", nativeQuery = true)
	Integer getNextInvoiceId();

	@Modifying(clearAutomatically = true)
	@Query(value = "delete from airorderitem where bookingid = :bookingId and (select count(*) from orders where bookingid = :bookingId) = 0", nativeQuery = true)
	void deleteItemsWithNoOrderByBookingId(@Param("bookingId") String bookingId);
}
