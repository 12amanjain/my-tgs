package com.tgs.services.oms.jparepository.air;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.base.Joiner;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AdditionalInfoFilter;
import com.tgs.services.oms.datamodel.air.AirOrderItemFilter;
import com.tgs.services.oms.datamodel.air.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.pms.datamodel.PaymentStatus;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirOrderItemService {

	@Autowired
	private AirOrderItemRepository airItemRepository;

	@PersistenceContext
	private EntityManager em;

	public DbAirOrderItem save(DbAirOrderItem orderItem) {
		setAndRemoveRedundantInfo(orderItem);
		sortPax(orderItem);
		if (orderItem.getCreatedOn() == null)
			orderItem.setCreatedOn(LocalDateTime.now());
		return airItemRepository.saveAndFlush(orderItem);
	}

	public List<DbAirOrderItem> save(List<DbAirOrderItem> orderItems) {
		sortItems(orderItems);
		for (DbAirOrderItem item : orderItems) {
			save(item);
		}
		return orderItems;
	}

	public List<DbAirOrderItem> findByBookingId(String bookingId) {
		List<DbAirOrderItem> airOrderItems = airItemRepository.findByBookingIdOrderById(bookingId);
		return airOrderItems;
	}

	public List<DbAirOrderItem> findAll() {
		return airItemRepository.findAll();
	}

	public List<DbAirOrderItem> findByBookingId(List<String> bookingIds) {
		List<DbAirOrderItem> airOrderItems = airItemRepository.findByBookingIdInOrderByBookingIdAscIdAsc(bookingIds);
		return airOrderItems;
	}

	public DbAirOrderItem findById(Long id) {
		return airItemRepository.findById(id);
	}

	@SuppressWarnings({"unchecked"})
	public List<Object[]> findByJsonSearch(OrderFilter orderFilter) {
		List<Object[]> results = new ArrayList<>();

		// Query Optimization : To stop request hitting all the partitions
		LocalDateTime createdOnAfterDateTime = getCreatedOnAfterDateForOrderFilter(orderFilter);
		LocalDateTime createdOnBeforeDateTime = getCreatedOnBeforeDateForOrderFilter(orderFilter);

		/**
		 * Max of 30,000 rows can be fetched.
		 */
		for (int i = 0; i < 20; i++) {

			StringBuilder query = new StringBuilder(
					"SELECT {a.*}, {b.*} FROM airorderitem a JOIN orders b " + "ON a.bookingid = b.bookingid ");
			List<String> queryParams = new ArrayList<>();

			if (orderFilter != null) {
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingUserIds())) {
					queryParams.add("b.bookinguserid IN ('"
							.concat(Joiner.on("','").join(orderFilter.getBookingUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getLoggedInUserIds())) {
					queryParams.add("b.loggedinuserid IN ('"
							.concat(Joiner.on("','").join(orderFilter.getLoggedInUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingIds())) {
					queryParams.add("b.bookingid IN ('".concat(Joiner.on("','").join(orderFilter.getBookingIds()))
							.concat("')"));
				}
				if (StringUtils.isNotEmpty(orderFilter.getBookingId())) {
					queryParams.add("b.bookingid = '".concat(orderFilter.getBookingId()).concat("'"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getStatuses())) {
					List<String> statusCodes = OrderStatus.getCodes((orderFilter.getStatuses()));
					queryParams.add("b.status IN ('".concat(Joiner.on("','").join(statusCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getChannels())) {
					List<String> channelCodes = ChannelType.getCodes(orderFilter.getChannels());
					queryParams.add("b.channeltype IN ('".concat(Joiner.on("','").join(channelCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getProducts())) {
					List<String> orderCodes = OrderType.getCodes(orderFilter.getProducts());
					queryParams.add("b.ordertype IN ('".concat(Joiner.on("','").join(orderCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getAssignedUserIdIn())) {
					queryParams.add("b.additionalInfo->>'auid' IN ('"
							.concat(Joiner.on("','").join(orderFilter.getAssignedUserIdIn())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getPaymentStatus())) {
					List<String> paymentStatus = PaymentStatus.getCodes(orderFilter.getPaymentStatus());
					queryParams.add(
							"b.additionalInfo->>'ps' IN ('".concat(Joiner.on("','").join(paymentStatus)).concat("')"));
				}

				if (createdOnAfterDateTime != null) {
					queryParams.add("b.createdOn >= '".concat(createdOnAfterDateTime.toString()).concat("'"));
					queryParams.add("a.createdOn >= '".concat(createdOnAfterDateTime.toString()).concat("'"));
				}

				queryParams.add("b.createdOn <= '".concat(createdOnBeforeDateTime.toString()).concat("'"));
				queryParams.add("a.createdOn <= '".concat(createdOnBeforeDateTime.toString()).concat("'"));

				if (orderFilter.getProcessedOnAfterDateTime() != null) {
					queryParams.add("b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDateTime() != null) {
					queryParams.add("b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDate() != null) {
					queryParams.add(
							"b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDate() != null) {
					queryParams.add(
							"b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDate().toString()).concat("'"));
				}
				if (orderFilter.getIsPushedToAccounting() != null) {
					queryParams.add("b.additionalinfo#>>'{ips}'='"
							.concat(orderFilter.getIsPushedToAccounting().toString()).concat("'"));
				}

				AirOrderItemFilter filter = orderFilter.getItemFilter();
				if (filter != null) {
					TravellerInfoFilter travellerInfoFilter = filter.getTravellerInfoFilter();
					AdditionalInfoFilter additionalInfoFilter = filter.getAdditionalInfoFilter();

					if (additionalInfoFilter != null) {
						queryParams.add(createQueryForJsonObject(additionalInfoFilter));
					}
					if (CollectionUtils.isNotEmpty(filter.getSupplierIds())) {
						queryParams.add("a.supplierId IN ('".concat(Joiner.on("','").join(filter.getSupplierIds()))
								.concat("')"));
					}
					if (CollectionUtils.isNotEmpty(filter.getAirlines())) {
						queryParams.add(
								"a.airlinecode IN ('".concat(Joiner.on("','").join(filter.getAirlines())).concat("')"));
					}
					if (filter.getDepartedOnAfterDate() != null) {
						LocalDateTime time = LocalDateTime.of(filter.getDepartedOnAfterDate(), LocalTime.MIN);
						queryParams.add("a.departuretime >= '".concat(time.toString()).concat("'"));
					}

					if (filter.getDepartedOnBeforeDate() != null) {
						LocalDateTime time = LocalDateTime.of(filter.getDepartedOnBeforeDate(), LocalTime.MAX);
						queryParams.add("a.departuretime <= '".concat(time.toString()).concat("'"));
					}

					if (filter.getCreatedOnAfterDate() != null) {
						LocalDateTime time = LocalDateTime.of(filter.getCreatedOnAfterDate(), LocalTime.MIN);
						queryParams.add("a.createdon >= '".concat(time.toString()).concat("'"));
					}

					if (filter.getCreatedOnBeforeDate() != null) {
						LocalDateTime time = LocalDateTime.of(filter.getCreatedOnBeforeDate(), LocalTime.MAX);
						queryParams.add("a.createdon <= '".concat(time.toString()).concat("'"));
					}

					if (travellerInfoFilter != null) {
						queryParams.add(createQueryForArrayOfJsonObjectsWithFunction(travellerInfoFilter));
						String str = createQueryForArrayOfJsonObjectsWithoutFunction(travellerInfoFilter);
						if (str != null)
							queryParams.add(createQueryForArrayOfJsonObjectsWithoutFunction(travellerInfoFilter));
					}
				}

			}

			if (CollectionUtils.isNotEmpty(queryParams)) {
				query.append(" WHERE ").append(Joiner.on(" AND ").join(queryParams));
			}
			if (orderFilter.getItemFilter() != null
					&& CollectionUtils.isNotEmpty(orderFilter.getItemFilter().getSortByAttr())) {
				Direction direction =
						Direction.fromString(orderFilter.getItemFilter().getSortByAttr().get(0).getOrderBy());
				List<String> properties = orderFilter.getItemFilter().getSortByAttr().get(0).getParams();
				query.append(" order by ").append(Joiner.on(",").join(properties)).append(" ,a.bookingid, a.id ")
						.append(direction.name());
			} else {
				query.append(" order by ").append("a.createdOn,a.bookingid, a.id");
			}
			query.append(" limit 1500 offset " + (i * 1500)).append(" ;");

			log.debug("[AirOrderSearch] Query formed is {} ", query.toString());
			EntityManager entityManager = null;
			entityManager = em.getEntityManagerFactory().createEntityManager();
			List<Object[]> pageResults = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
					.addEntity("a", DbAirOrderItem.class).addEntity("b", DbOrder.class).getResultList();
			entityManager.close();
			if (CollectionUtils.isNotEmpty(pageResults))
				results.addAll(pageResults);

			if (CollectionUtils.isEmpty(pageResults) || pageResults.size() < 1500) {
				break;
			}

		}
		return results;
	}

	// Example : additionalInfo @>'type'='D'
	private String createQueryForJsonObject(AdditionalInfoFilter additionalInfoFilter) {
		List<String> queryParams = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(additionalInfoFilter.getTripTypes())) {
			List<String> tripCodes = AirType.getCodes(additionalInfoFilter.getTripTypes());
			queryParams.add("a.additionalInfo->>'type' IN ('".concat(Joiner.on("','").join(tripCodes)).concat("')"));
		}
		if (CollectionUtils.isNotEmpty(additionalInfoFilter.getSourceIds())) {
			queryParams.add("a.additionalInfo ->> 'scid' IN ('"
					.concat(Joiner.on("','").join(additionalInfoFilter.getSourceIds())).concat("')"));
		}
		return Joiner.on(" AND ").join(queryParams);
	}

	private String createQueryForArrayOfJsonObjectsWithoutFunction(TravellerInfoFilter travellerInfoFilter) {
		List<String> queryParams = new ArrayList<>();
		if (travellerInfoFilter.getPnr() != null) {
			queryParams.add("a.travellerinfo @> '[{\"pnr\":\"".concat(travellerInfoFilter.getPnr()).concat("\"}]'"));
		}
		if (travellerInfoFilter.getTicketNumber() != null) {
			queryParams.add(
					"a.travellerinfo @> '[{\"tknum\":\"".concat(travellerInfoFilter.getTicketNumber()).concat("\"}]'"));
		}
		if (travellerInfoFilter.getGdsPnr() != null) {
			queryParams
					.add("a.travellerinfo @> '[{\"sBId\":\"".concat(travellerInfoFilter.getGdsPnr()).concat("\"}]'"));
		}
		if (CollectionUtils.isNotEmpty(queryParams))
			return Joiner.on(" AND ").join(queryParams);
		return null;
	}
	// Example : SELECT FROM jsonb_array_elements(t.travellerinfo) tinfo WHERE
	// tinfo->>'fN' LIKE 'Neha' AND tinfo->>'lN' like 'Arora'

	// Using jsonb_array_elements will do a functional scan and wont use the gin index created on travellerinfo
	private String createQueryForArrayOfJsonObjectsWithFunction(TravellerInfoFilter travellerInfoFilter) {
		StringBuilder query =
				new StringBuilder(" EXISTS ( SELECT FROM jsonb_array_elements(a.travellerinfo) tinfo WHERE ");
		List<String> queryParams = new ArrayList<>();

		if (travellerInfoFilter.getPassengerFirstName() != null) {
			queryParams.add("tinfo->>'fN' ILIKE '%".concat(travellerInfoFilter.getPassengerFirstName()).concat("%'"));
		}
		if (travellerInfoFilter.getPassengerLastName() != null) {
			queryParams.add("tinfo->>'lN' ILIKE '%".concat(travellerInfoFilter.getPassengerLastName()).concat("%'"));
		}

		if (!queryParams.isEmpty())
			query.append(Joiner.on(" AND ").join(queryParams)).append(")");
		else
			query.append("true)");
		return query.toString();
	}

	private LocalDateTime getCreatedOnAfterDateForOrderFilter(OrderFilter orderFilter) {
		LocalDateTime createdOnAfterDateTime =
				orderFilter.getCreatedOnAfterDateTime() != null ? orderFilter.getCreatedOnAfterDateTime()
						: orderFilter.getCreatedOnAfterDate() != null
								? LocalDateTime.of(orderFilter.getCreatedOnAfterDate(), LocalTime.MIN)
								: null;
		// If we are getting booking id in the filter, then there is no need to have createdon check. We already have
		// index on booking id but all partitions will be scanned in this case
		if (CollectionUtils.isNotEmpty(orderFilter.getBookingIds())
				|| StringUtils.isNotEmpty(orderFilter.getBookingId())) {
			return createdOnAfterDateTime;
		}
		if (createdOnAfterDateTime == null) {
			if (CollectionUtils.isNotEmpty(orderFilter.getBookingUserIds())) {
				createdOnAfterDateTime = LocalDateTime.now().minusMonths(12);
			} else if (orderFilter.getProcessedOnAfterDateTime() != null)
				createdOnAfterDateTime = orderFilter.getProcessedOnAfterDateTime().minusMonths(3);
			else if (orderFilter.getProcessedOnAfterDate() != null)
				createdOnAfterDateTime =
						LocalDateTime.of(orderFilter.getProcessedOnAfterDate().minusMonths(3), LocalTime.MIN);
			else if (orderFilter.getItemFilter() != null
					&& orderFilter.getItemFilter().getDepartedOnAfterDate() != null) {
				createdOnAfterDateTime = LocalDateTime
						.of(orderFilter.getItemFilter().getDepartedOnAfterDate().minusMonths(5), LocalTime.MIN);
			}
			// Right now we dont get created on dates in case of these fields from frontend . But by default we will
			// send only past 5 months data
			else if (CollectionUtils.isEmpty(orderFilter.getBookingIds())
					&& StringUtils.isEmpty(orderFilter.getBookingId())) {
				// This is done to ensure that if user searches through any PNR, first name, last name, ticket no - all
				// partitions are not scanned. Index size of jsonb columns is huge.
				createdOnAfterDateTime = LocalDateTime.now().minusMonths(5);
			}
		}
		return createdOnAfterDateTime;
	}

	private LocalDateTime getCreatedOnBeforeDateForOrderFilter(OrderFilter orderFilter) {
		LocalDateTime createdOnBeforeDateTime =
				orderFilter.getCreatedOnBeforeDateTime() != null ? orderFilter.getCreatedOnBeforeDateTime()
						: orderFilter.getCreatedOnBeforeDate() != null
								? LocalDateTime.of(orderFilter.getCreatedOnBeforeDate(), LocalTime.MAX)
								: null;
		if (createdOnBeforeDateTime == null) {
			if (orderFilter.getProcessedOnBeforeDateTime() != null)
				createdOnBeforeDateTime = orderFilter.getProcessedOnBeforeDateTime();
			else if (orderFilter.getProcessedOnBeforeDate() != null)
				createdOnBeforeDateTime = LocalDateTime.of(orderFilter.getProcessedOnBeforeDate(), LocalTime.MAX);
			else if (orderFilter.getItemFilter() != null
					&& orderFilter.getItemFilter().getDepartedOnBeforeDate() != null) {
				createdOnBeforeDateTime =
						LocalDateTime.of(orderFilter.getItemFilter().getDepartedOnBeforeDate(), LocalTime.MAX);
			} else {
				createdOnBeforeDateTime = LocalDateTime.now();
			}
		}
		return createdOnBeforeDateTime;
	}

	private void setAndRemoveRedundantInfo(DbAirOrderItem orderItem) {
		long counter = 1;
		for (FlightTravellerInfo travellerInfo : orderItem.getTravellerInfo()) {
			travellerInfo.getFareDetail().getFareComponents().entrySet()
					.removeIf(fc -> !fc.getKey().isPersistenceAllowed());
			if (travellerInfo.getId() != null)
				return;
			travellerInfo.setId(counter++);
		}
	}

	private void sortItems(List<DbAirOrderItem> orderItems) {
		if (!CollectionUtils.isEmpty(orderItems) && orderItems.get(0).getId() != null) {
			orderItems.sort(Comparator.comparing(DbAirOrderItem::getId));
		}
	}

	private void sortPax(DbAirOrderItem orderItem) {
		orderItem.getTravellerInfo().sort(Comparator.comparing(TravellerInfo::getId));
	}

	@Transactional
	public void deleteItemsWithNoOrderByBookingId(String bookingId) {
		airItemRepository.deleteItemsWithNoOrderByBookingId(bookingId);
	}

	public Integer getNextInvoiceId() {
		return airItemRepository.getNextInvoiceId();
	}
}
