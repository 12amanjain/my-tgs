package com.tgs.services.oms.jparepository.hotel;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.oms.dbmodel.hotel.DbHotelBookingReconciliation;

@Service
public class HotelBookingReconciliationService {

	@Autowired
	private HotelBookingReconciliationRepository reconciliationRepository;

	public DbHotelBookingReconciliation save(DbHotelBookingReconciliation bookingReconciliation) {
		return reconciliationRepository.saveAndFlush(bookingReconciliation);
	}

	public DbHotelBookingReconciliation findByBookingId(String bookingId) {
		return reconciliationRepository.findByBookingId(bookingId);
	}
}
