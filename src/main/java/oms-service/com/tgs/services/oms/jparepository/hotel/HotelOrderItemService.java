package com.tgs.services.oms.jparepository.hotel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.base.Joiner;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.AddressFilter;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItemFilter;
import com.tgs.services.oms.datamodel.hotel.TravellerInfoFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelOrderItemService {

	@Autowired
	private HotelOrderItemRepository hotelItemRepository;

	@PersistenceContext
	private EntityManager em;

	public DbHotelOrderItem save(DbHotelOrderItem orderItem) {
		return hotelItemRepository.saveAndFlush(orderItem);
	}

	public List<DbHotelOrderItem> findByBookingIdOrderByIdAsc(String bookingId) {
		List<DbHotelOrderItem> dbOrderItems = hotelItemRepository.findByBookingIdOrderByIdAsc(bookingId);
		dbOrderItems =
				dbOrderItems.stream().filter(orderItem -> !BooleanUtils.isTrue(orderItem.getRoomInfo().getIsDeleted()))
						.collect(Collectors.toList());
		return dbOrderItems;
	}

	public List<HotelOrderItem> findByBookingId(List<String> bookingIds) {
		return BaseModel.toDomainList(hotelItemRepository.findByBookingIdIn(bookingIds));
	}

	@SuppressWarnings("unchecked")
	public List<Object[]> findByJsonSearch(OrderFilter orderFilter) {

		List<Object[]> results = new ArrayList<>();
		
		for (int i = 0; i < 20; i++) {

			StringBuilder query = new StringBuilder(
					"SELECT {a.*} , {b.*} from hotelorderitem a JOIN orders b " + "ON a.bookingId = b.bookingId ");
			List<String> queryParams = new ArrayList<>();
			if (orderFilter != null) {
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingUserIds())) {
					queryParams.add("b.bookinguserid IN('"
							.concat(Joiner.on("','").join(orderFilter.getBookingUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getLoggedInUserIds())) {
					queryParams.add("b.loggedinuserid IN('"
							.concat(Joiner.on("','").join(orderFilter.getLoggedInUserIds())).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getBookingIds())) {
					queryParams.add(
							"b.bookingId IN('".concat(Joiner.on("','").join(orderFilter.getBookingIds())).concat("')"));
				}
				if (StringUtils.isNotEmpty(orderFilter.getBookingId())) {
					queryParams.add("b.bookingId = '".concat(orderFilter.getBookingId().concat("'")));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getStatuses())) {
					List<String> statusCodes = OrderStatus.getCodes(orderFilter.getStatuses());
					queryParams.add("b.status IN ('".concat(Joiner.on("','").join(statusCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getChannels())) {
					List<String> channelCodes = ChannelType.getCodes(orderFilter.getChannels());
					queryParams.add("b.channeltype IN ('".concat(Joiner.on("','").join(channelCodes)).concat("')"));
				}
				if (CollectionUtils.isNotEmpty(orderFilter.getProducts())) {
					List<String> orderCodes = OrderType.getCodes(orderFilter.getProducts());
					queryParams.add("b.ordertype IN ('".concat(Joiner.on("','").join(orderCodes)).concat("')"));
				}
				if (orderFilter.getCreatedOnAfterDateTime() != null) {
					queryParams.add(
							"b.createdOn >= '".concat(orderFilter.getCreatedOnAfterDateTime().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnBeforeDateTime() != null) {
					queryParams.add(
							"b.createdOn <= '".concat(orderFilter.getCreatedOnBeforeDateTime().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnAfterDate() != null) {
					queryParams
							.add("b.createdOn >= '".concat(orderFilter.getCreatedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getCreatedOnBeforeDate() != null) {
					queryParams.add(
							"b.createdOn <= '".concat(orderFilter.getCreatedOnBeforeDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDateTime() != null) {
					queryParams.add("b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDateTime() != null) {
					queryParams.add("b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDateTime().toString())
							.concat("'"));
				}
				if (orderFilter.getProcessedOnAfterDate() != null) {
					queryParams.add(
							"b.processedOn >= '".concat(orderFilter.getProcessedOnAfterDate().toString()).concat("'"));
				}
				if (orderFilter.getProcessedOnBeforeDate() != null) {
					queryParams.add(
							"b.processedOn <= '".concat(orderFilter.getProcessedOnBeforeDate().toString()).concat("'"));
				}
				HotelOrderItemFilter hotelFilter = orderFilter.getHotelItemFilter();
				if (hotelFilter != null) {
					TravellerInfoFilter travellerFilter = hotelFilter.getTravellerInfoFilter();
					AddressFilter addressFilter = hotelFilter.getAddressFilter();
					if (CollectionUtils.isNotEmpty(hotelFilter.getSupplierIds())) {
						queryParams.add("a.supplierId IN ('".concat(Joiner.on("','").join(hotelFilter.getSupplierIds()))
								.concat("')"));
					}
					if (hotelFilter.getHotelName() != null) {
						queryParams.add("a.hotel ILIKE '%".concat(hotelFilter.getHotelName().concat("%'")));
					}
					if (hotelFilter.getCheckInAfterDate() != null) {
						queryParams.add(
								"a.checkInDate >= '".concat(hotelFilter.getCheckInAfterDate().toString()).concat("'"));
					}
					if (hotelFilter.getCheckInBeforeDate() != null) {
						queryParams.add(
								"a.checkInDate <= '".concat(hotelFilter.getCheckInBeforeDate().toString()).concat("'"));
					}
					if (hotelFilter.getHotelBookingReference() != null) {
						queryParams.add(createQueryForHotelBookingReference(hotelFilter));
					}
					if (addressFilter != null) {
						queryParams.add(createQueryForListOfCities(addressFilter));
					}
					if (travellerFilter != null) {
						queryParams.add(createQueryForArrayOfJsonObjects(travellerFilter));
					}
					if (hotelFilter.getDeadlineDateAfterDateTime() != null
							|| hotelFilter.getDeadlineDateBeforeDateTime() != null) {
						queryParams.add(createQueryForDeadlineDate(hotelFilter));
					}
				}
			}
			if (CollectionUtils.isNotEmpty(queryParams)) {
				query.append(" WHERE ").append(Joiner.on(" AND ").join(queryParams));
			}
			if (orderFilter.getHotelItemFilter() != null
					&& CollectionUtils.isNotEmpty(orderFilter.getHotelItemFilter().getSortByAttr())) {
				Direction direction =
						Direction.fromString(orderFilter.getHotelItemFilter().getSortByAttr().get(0).getOrderBy());
				List<String> properties = orderFilter.getHotelItemFilter().getSortByAttr().get(0).getParams();
				query.append(" order by a.").append(Joiner.on(",a.").join(properties)).append(" " + direction.name());
			} else {
				query.append(" order by ").append("a.createdOn,a.bookingid, a.id");
			}
			query.append(" limit 1500 offset " + (i * 1500)).append(" ;");
			log.info("[HotelOrderSearch] Query formed is {} ", query.toString());

			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			List<Object[]> pageResults = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
					.addEntity("a", DbHotelOrderItem.class)
					.setFetchSize(orderFilter.getPageAttr() != null ? orderFilter.getPageAttr().getSize() : 5)
					.addEntity("b", DbOrder.class).getResultList();
			entityManager.close();
			if (CollectionUtils.isNotEmpty(pageResults))
				results.addAll(pageResults);

			if (CollectionUtils.isEmpty(pageResults) || pageResults.size() < 1500) {
				break;
			}
		}
		return results;

	}

	private String createQueryForHotelBookingReference(HotelOrderItemFilter orderFilter) {
		StringBuilder query = new StringBuilder(" EXISTS ( SELECT FROM jsonb(a.additionalinfo) ainfo WHERE ");
		List<String> queryParams = new ArrayList<>();
		queryParams.add("ainfo->>'hbr' = '".concat(orderFilter.getHotelBookingReference()).concat("'"));
		if (!queryParams.isEmpty())
			query.append(Joiner.on(" AND ").join(queryParams)).append(")");
		else
			query.append("true)");
		return query.toString();
	}

	private String createQueryForListOfCities(AddressFilter addressFilter) {
		StringBuilder query = new StringBuilder(" EXISTS ( SELECT FROM jsonb(a.additionalinfo) ainfo WHERE ");
		List<String> queryParams = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(addressFilter.getCityList())) {
			queryParams.add("ainfo->'phi'->'ad'->'city'->>'name' IN('"
					.concat(Joiner.on("','").join(addressFilter.getCityList()).concat("')")));
		}
		if (CollectionUtils.isNotEmpty(addressFilter.getCountryList())) {
			queryParams.add("ainfo->'phi'->'ad'->'country'->>'name' IN('"
					.concat(Joiner.on("','").join(addressFilter.getCountryList()).concat("'))")));
		}
		if (!queryParams.isEmpty())
			query.append(Joiner.on(" AND ").join(queryParams)).append(")");
		else
			query.append("true)");
		return query.toString();
	}

	private String createQueryForArrayOfJsonObjects(TravellerInfoFilter travellerInfoFilter) {
		StringBuilder query =
				new StringBuilder(" EXISTS ( SELECT FROM jsonb_array_elements(a.roominfo->'ti') tinfo WHERE ");
		List<String> queryParams = new ArrayList<>();
		if (travellerInfoFilter.getTravellerFirstName() != null) {
			queryParams.add("tinfo->>'fN' ILIKE '%".concat(travellerInfoFilter.getTravellerFirstName()).concat("%'"));
		}
		if (travellerInfoFilter.getTravellerLastName() != null) {
			queryParams.add("tinfo->>'lN' ILIKE '%".concat(travellerInfoFilter.getTravellerLastName()).concat("%'"));
		}
		if (!queryParams.isEmpty())
			query.append(Joiner.on(" AND ").join(queryParams)).append(")");
		else
			query.append("true)");
		return query.toString();

	}


	private String createQueryForDeadlineDate(HotelOrderItemFilter orderFilter) {

		StringBuilder query = new StringBuilder(" EXISTS ( SELECT FROM jsonb(a.roominfo) rinfo WHERE ");
		List<String> queryParams = new ArrayList<>();
		if (orderFilter.getDeadlineDateAfterDateTime() != null) {
			queryParams.add(
					"rinfo ->> 'ddt' >= '".concat(orderFilter.getDeadlineDateAfterDateTime().toString()).concat("'"));
		}
		if (orderFilter.getDeadlineDateBeforeDateTime() != null) {
			queryParams.add(
					"rinfo ->> 'ddt' <= '".concat(orderFilter.getDeadlineDateBeforeDateTime().toString()).concat("'"));
		}
		if (!queryParams.isEmpty())
			query.append(Joiner.on(" AND ").join(queryParams)).append(")");
		else
			query.append("true)");
		return query.toString();
	}

	public DbHotelOrderItem findById(Long id) {
		return hotelItemRepository.findOne(id);
	}

	@Transactional
	public void deleteItemsWithNoOrderByBookingId(String bookingId) {
		hotelItemRepository.deleteItemsWithNoOrderByBookingId(bookingId);
	}

	public Integer getNextInvoiceId() {
		return hotelItemRepository.getNextInvoiceId();
	}

}
