package com.tgs.services.oms.manager;

import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractAmendmentInvoiceIdManager {

	public String getInvoiceId(Amendment amendment, ProductMetaInfo productMetaInfo) {
		String prefix = getNonNullInvoiceIdPrefix(amendment, productMetaInfo);
		int numericInvoiceId = getNextNumericInvoiceId(amendment, prefix, productMetaInfo);
		String suffix = getNonNullInvoiceIdSuffix(amendment, productMetaInfo);
		StringBuffer sb = new StringBuffer(prefix).append(numericInvoiceId).append(suffix);
		return sb.toString();
	}

	private String getNonNullInvoiceIdPrefix(Amendment amendment, ProductMetaInfo productMetaInfo) {
		String prefix = getInvoiceIdPrefix(amendment, productMetaInfo);
		return prefix == null ? "" : prefix;
	}

	private String getNonNullInvoiceIdSuffix(Amendment amendment, ProductMetaInfo productMetaInfo) {
		String suffix = getInvoiceIdSuffix(amendment, productMetaInfo);
		return suffix == null ? "" : suffix;
	}

	public final int getNumericInvoiceId(String invoiceId, Amendment amendment, ProductMetaInfo productMetaInfo) {
		try {
			if (invoiceId == null || amendment == null || productMetaInfo == null) {
				throw new NullPointerException("Null argument passed");
			}

			int start, invoiceIdLength, end;

			start = getNonNullInvoiceIdPrefix(amendment, productMetaInfo).length();
			invoiceIdLength = invoiceId.length();
			end = invoiceIdLength - getNonNullInvoiceIdSuffix(amendment, productMetaInfo).length();

			if (start >= end) {
				throw new IllegalArgumentException("invoiceId is too short to be decoded");
			}

			try {
				return Integer.parseInt(invoiceId.substring(start, end));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("invoiceId is not in correct format");
			}
		} catch (Exception e) {
			String bookingId = null;
			if (amendment != null) {
				bookingId = amendment.getBookingId();
			}
			log.error("Unable to get numeric invoice id from invoiceId {} and bookingId {} due to ", invoiceId,
					bookingId, e);
			throw e;
		}
	}

	public abstract String getInvoiceIdPrefix(Amendment amendment, ProductMetaInfo productMetaInfo);

	protected abstract int getNextNumericInvoiceId(Amendment amendment, String prefix, ProductMetaInfo productMetaInfo);

	protected abstract String getInvoiceIdSuffix(Amendment amendment, ProductMetaInfo productMetaInfo);

}
