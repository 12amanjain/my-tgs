package com.tgs.services.oms.manager;

import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.gms.datamodel.InvoiceIdData;
import com.tgs.services.oms.datamodel.Order;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractInvoiceIdManager {

	/**
	 * Evaluates invoiceId and sets it in {@code order}.
	 * 
	 * @param order
	 * @param orderItems
	 */
	public final void setInvoiceIdFor(Order order, ProductMetaInfo productMetaInfo) {
		if (order == null) {
			return;
		}

		try {
			String invoiceId = order.getAdditionalInfo().getInvoiceId();
			if (StringUtils.isBlank(invoiceId)) {
				invoiceId = getInvoiceId(order, productMetaInfo);
				order.getAdditionalInfo().setInvoiceId(invoiceId);
			}
		} catch (Exception e) {
			log.error("Unable to set invoiceId for bookingId {} due to", order.getBookingId(), e);
		}
	}

	/**
	 * 
	 * @param order
	 * @param productMetaInfo
	 * @return invoiceId for given order. If (non-blank) invoiceId is already present in order, same is returned.
	 */
	public final String getInvoiceIdFor(Order order, ProductMetaInfo productMetaInfo) {
		if (order == null) {
			return null;
		}

		try {
			String invoiceId = order.getAdditionalInfo().getInvoiceId();
			if (StringUtils.isNotBlank(invoiceId)) {
				return invoiceId;
			}
			return getInvoiceId(order, productMetaInfo);
		} catch (Exception e) {
			log.error("Unable to evaluate invoiceId for bookingId {} due to", order.getBookingId(), e);
			return null;
		}
	}

	public final InvoiceIdData getInvoiceIdData(String invoiceId, Order order, ProductMetaInfo productMetaInfo) {
		try {
			if (invoiceId == null || order == null || productMetaInfo == null) {
				throw new NullPointerException("Null argument passed");
			}

			int start, invoiceIdLength, end;
			InvoiceIdData.InvoiceIdDataBuilder invoiceIdDataBuilder = InvoiceIdData.builder();

			final String prefix = getNonNullInvoiceIdPrefix(order, productMetaInfo);
			invoiceIdDataBuilder.prefix(prefix);

			final String suffix = getNonNullInvoiceIdSuffix(order, productMetaInfo);
			invoiceIdDataBuilder.suffix(suffix);

			start = prefix.length();
			invoiceIdLength = invoiceId.length();
			end = invoiceIdLength - suffix.length();
			if (start >= end) {
				throw new IllegalArgumentException("invoiceId is too short to be decoded");
			}
			try {
				invoiceIdDataBuilder.numericInvoiceId(Integer.parseInt(invoiceId.substring(start, end)));
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("invoiceId is not in correct format");
			}

			return invoiceIdDataBuilder.build();
		} catch (Exception e) {
			String bookingId = null;
			if (order != null) {
				bookingId = order.getBookingId();
			}
			log.error("Unable to get numeric invoice id from invoiceId {} and bookingId {} due to ", invoiceId,
					bookingId, e);
			throw e;
		}
	}

	private String getInvoiceId(Order order, ProductMetaInfo productMetaInfo) {
		int numericInvoiceId = getNextNumericInvoiceId(order, productMetaInfo);
		String prefix = getNonNullInvoiceIdPrefix(order, productMetaInfo);
		String suffix = getNonNullInvoiceIdSuffix(order, productMetaInfo);
		StringBuffer sb = new StringBuffer(prefix).append(numericInvoiceId).append(suffix);
		return sb.toString();
	}

	private String getNonNullInvoiceIdPrefix(Order order, ProductMetaInfo productMetaInfo) {
		String prefix = getInvoiceIdPrefix(order, productMetaInfo);
		return prefix == null ? "" : prefix;
	}

	private String getNonNullInvoiceIdSuffix(Order order, ProductMetaInfo productMetaInfo) {
		String suffix = getInvoiceIdSuffix(order, productMetaInfo);
		return suffix == null ? "" : suffix;
	}

	protected abstract String getInvoiceIdPrefix(Order order, ProductMetaInfo productMetaInfo);

	protected abstract String getInvoiceIdSuffix(Order order, ProductMetaInfo productMetaInfo);

	protected abstract int getNextNumericInvoiceId(Order order, ProductMetaInfo productMetaInfo);
}
