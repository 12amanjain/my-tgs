package com.tgs.services.oms.manager;

import java.time.LocalDateTime;
import javax.annotation.PostConstruct;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.InvoiceIdConfiguration;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.jparepository.AmendmentService;

@Service
public class AmendmentInvoiceIdManager extends AbstractAmendmentInvoiceIdManager {

	public static final String CHARGED_DOMESTIC_ID_PREFIX = "DW", CHARGED_INTERNATIONAL_ID_PREFIX = "IW";
	public static final String VOID_DOMESTIC_ID_PREFIX = "DY", VOID_INTERNATIONAL_ID_PREFIX = "IY";
	public static final String REFUND_DOMESTIC_ID_PREFIX = "DZ", REFUND_INTERNATIONAL_ID_PREFIX = "IZ";

	@Autowired
	private AmendmentService service;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	private InvoiceIdConfiguration invoiceIdConfiguration;

	@PostConstruct
	private void init() {
		GeneralBasicFact generalBasicFact = GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build();
		ClientGeneralInfo clientGeneralInfo =
				gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, generalBasicFact);
		if (clientGeneralInfo != null) {
			invoiceIdConfiguration = clientGeneralInfo.getInvoiceIdConfiguration();
		}
	}

	@Override
	public String getInvoiceIdPrefix(Amendment amendment, ProductMetaInfo productMetaInfo) {
		Product product = productMetaInfo.getProduct();
		String prefix;

		switch (product) {
			case AIR:
				String subProduct = productMetaInfo.getSubProduct();
				if (AirType.DOMESTIC.getCode().equals(subProduct)) {
					prefix = getPrefix(true, amendment.getAmendmentType(), amendment.getAmount());
				} else if (AirType.INTERNATIONAL.getCode().equals(subProduct)) {
					prefix = getPrefix(false, amendment.getAmendmentType(), amendment.getAmount());
				} else {
					throw new IllegalArgumentException("Invoice id not supported for sub-product: " + subProduct);
				}
				break;

			default:
				throw new IllegalArgumentException("Invoice id not supported for product: " + product);
		}

		return prefix + "-";
	}

	private String getPrefix(boolean isDomestic, AmendmentType type, Double amount) {
		boolean isCharged = amount > 0;
		switch (type) {
			case VOIDED:
				return isDomestic ? getVoidAirDomesticIdPrefix() : getVoidAirInternationalIdPrefix();
			case SSR:
			case FARE_CHANGE:
				return isDomestic ? getChargedAirDomesticIdPrefix() : getChargedAirInternationalIdPrefix();
			case CORRECTION:
				if (isCharged)
					return isDomestic ? getChargedAirDomesticIdPrefix() : getChargedAirInternationalIdPrefix();
				else
					return isDomestic ? getRefundAirDomesticIdPrefix() : getRefundAirInternationalIdPrefix();
			default:
				return isDomestic ? getRefundAirDomesticIdPrefix() : getRefundAirInternationalIdPrefix();
		}

	}

	private String getVoidAirDomesticIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getVoidAirDomesticIdPrefix(),
				VOID_DOMESTIC_ID_PREFIX);
	}

	private String getVoidAirInternationalIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getVoidAirDomesticIdPrefix(),
				VOID_INTERNATIONAL_ID_PREFIX);
	}

	private String getChargedAirDomesticIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getVoidAirDomesticIdPrefix(),
				CHARGED_DOMESTIC_ID_PREFIX);
	}

	private String getChargedAirInternationalIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getVoidAirDomesticIdPrefix(),
				CHARGED_INTERNATIONAL_ID_PREFIX);
	}

	private String getRefundAirDomesticIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getVoidAirDomesticIdPrefix(),
				REFUND_DOMESTIC_ID_PREFIX);
	}

	private String getRefundAirInternationalIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getVoidAirDomesticIdPrefix(),
				REFUND_INTERNATIONAL_ID_PREFIX);
	}

	@Override
	protected String getInvoiceIdSuffix(Amendment amendment, ProductMetaInfo productMetaInfo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected int getNextNumericInvoiceId(Amendment amendment, String prefix, ProductMetaInfo productMetaInfo) {
		Product product = productMetaInfo.getProduct();
		if (Product.AIR.equals(product)) {
			switch (prefix) {
				case CHARGED_DOMESTIC_ID_PREFIX + "-":
					return service.getNextChargedDomInvoiceId();
				case CHARGED_INTERNATIONAL_ID_PREFIX + "-":
					return service.getNextChargedIntInvoiceId();
				default:
					return service.getNextInvoiceId();
			}
		} else {
			throw new IllegalArgumentException("Invoice id not supported for product: " + product);

		}
	}

	private InvoiceIdConfiguration getInvoiceIdConfiguration() {
		if (invoiceIdConfiguration == null) {
			invoiceIdConfiguration = new InvoiceIdConfiguration();
		}
		return invoiceIdConfiguration;
	}

}
