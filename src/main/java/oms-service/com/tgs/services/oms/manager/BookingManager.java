package com.tgs.services.oms.manager;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import com.google.gson.Gson;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.gson.GsonUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.communicator.VoucherServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditLinePaymentMode;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.NRCreditPaymentMode;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentMode;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentMode;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.PaymentException;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Getter
@Setter
public abstract class BookingManager {

	protected BookingRequest bookingRequest;
	protected User bookingUser;

	protected User loggedInUser;
	protected double orderTotal;
	protected Order order;
	protected double totalMarkup;
	protected double partnerMarkup;
	protected double totalCommission;
	protected double totalTds;
	protected Double paymentTotal;

	protected double partnerCommission;
	protected double partnerMarkUpTds;
	protected double partnerCommissionTds;

	protected double pointsDiscount;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private PaymentServiceCommunicator paymentService;

	@Autowired
	protected VoucherServiceCommunicator voucherService;

	@Autowired
	private GeneralServiceCommunicator gmsComm;

	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected CommercialCommunicator cmsCommunicator;

	@Autowired
	protected FMSCommunicator fmsCommunicator;

	@Autowired
	protected GstInfoService gstService;

	protected void createOrder() {
		order = Order.builder().amount(orderTotal).bookingId(bookingRequest.getBookingId()).markup(totalMarkup)
				.bookingUserId(bookingUser.getParentUserId()).loggedInUserId(loggedInUser.getEmulateOrLoggedInUserId())
				.channelType(SystemContextHolder.getChannelType()).deliveryInfo(bookingRequest.getDeliveryInfo())
				.status(OrderStatus.IN_PROGRESS).createdOn(LocalDateTime.now()).orderType(bookingRequest.getType())
				.partnerId(bookingUser.getPartnerId()).additionalInfo(getOrderAdditionalInfo()).build();
		orderManager.saveWithoutFetch(order);
		log.info("Order successfully stored for bookingId {}", bookingRequest.getBookingId());
	}

	protected void storeGstInfo() {
		if (bookingRequest.getGstInfo() != null) {
			bookingRequest.getGstInfo().setBookingId(bookingRequest.getBookingId());
			bookingRequest.getGstInfo().setBookingUserId(loggedInUser.getParentUserId());
			orderManager.saveGstInfo(bookingRequest.getGstInfo());
		}
	}

	private OrderAdditionalInfo getOrderAdditionalInfo() {
		OrderAdditionalInfo additionalInfo = OrderAdditionalInfo.builder().build();
		if (bookingRequest.getFlowType() != null) {
			additionalInfo.setFlowType(bookingRequest.getFlowType());
		}
		PaymentRequest vRequest = getVoucherCodeFromRequest();
		if (vRequest != null) {
			additionalInfo.setVoucherCode(vRequest.getAdditionalInfo().getVoucherCode());
		}
		return additionalInfo;
	}

	protected void addOrderToAudit(AuditAction auditAction) {
		ContextData contextData = SystemContextHolder.getContextData();
		SystemAudit systemAudit = SystemAudit.builder().userId(contextData.getUser().getParentUserId())
				.bookingId(bookingRequest.getBookingId()).auditType(auditAction)
				.loggedInUserId(contextData.getUser().getEmulateOrLoggedInUserId())
				.ip(contextData.getHttpHeaders().getIp()).build();
		gmsComm.addToSystemAudit(systemAudit);
	}

	public abstract void updateItemStatus(OrderStatus orderStatus);

	private void updateStatus(OrderStatus orderStatus) {
		order.setStatus(orderStatus);
		updateItemStatus(orderStatus);
	}

	protected void doPayment() {
		boolean paymentStatus = false;
		boolean requireRedirection = false;
		List<Payment> payments = null;
		String reason = null;
		bookingRequest.setPaymentInfos(TgsCollectionUtils.getNonNullElements(bookingRequest.getPaymentInfos()));
		if (CollectionUtils.isEmpty(bookingRequest.getPaymentInfos())) {
			return;
		}
		try {
			BigDecimal totalPaymentAmount = BigDecimal.ZERO;
			PaymentRequest firstPayment = bookingRequest.getPaymentInfos().get(0);
			String payUserId = StringUtils.defaultIfBlank(firstPayment.getPayUserId(), order.getBookingUserId());
			User payUser = userService.getUserFromCache(payUserId);
			for (PaymentRequest paymentRequest : bookingRequest.getPaymentInfos()) {
				paymentRequest.setRefId(ObjectUtils.firstNonNull(paymentRequest.getRefId(), order.getBookingId()));
				paymentRequest.setOpType(PaymentOpType.DEBIT).setTransactionType(PaymentTransactionType.PAID_FOR_ORDER);
				paymentRequest.setAmount(
						ObjectUtils.firstNonNull(paymentRequest.getAmount(), BigDecimal.valueOf(order.getAmount())));
				paymentRequest.setPayUserId(payUserId);
				paymentRequest.setPartnerId(payUser.getPartnerId());
				// ignoring payment fee coming from UI(non-reliable), and we calculate it again using the ruleId later.
				paymentRequest.setPaymentFee(BigDecimal.ZERO);
				totalPaymentAmount = totalPaymentAmount.add(paymentRequest.getAmount());
			}


			if (AirBookingUtils.isVirtualPayment(bookingRequest)) {
				GstInfo orderBillingEntity = orderManager.getGstInfo(bookingRequest.getBookingId());
				List<PaymentRequest> paymentRequests =
						createVirualPaymentRequest(bookingRequest.getPaymentInfos(), orderBillingEntity);
				bookingRequest.setPaymentInfos(paymentRequests);
			}

			/*
			 * Only Air uses paymentTotal in case of hotel it will be null in this case orderTotal needs to be set in
			 * paymentTotal.
			 */
			paymentTotal = (paymentTotal == null) ? orderTotal : paymentTotal;

			double totalChargeablePaymentAmount = totalPaymentAmount.doubleValue() - totalMarkup;

			if (Math.abs(paymentTotal - totalChargeablePaymentAmount) >= 1) {
				log.error(
						"Amount mismatch error for bookingId {}, total chargeable amount (excluding markup) passed is {}, total amount to be charged is {}",
						bookingRequest.getBookingId(), totalChargeablePaymentAmount, paymentTotal);
				if (Math.abs(paymentTotal - totalChargeablePaymentAmount) > 10) {
					throw new CustomGeneralException(SystemError.AMOUNT_MISMATCH);
				}
			}
			BigDecimal orderMarkup = BigDecimal.valueOf(totalMarkup);
			firstPayment.setMarkup(orderMarkup);
			firstPayment.setAmount(firstPayment.getAmount().subtract(orderMarkup));
			bookingRequest.getPaymentInfos()
					.forEach(p -> p.getAdditionalInfo().setTotalDepositAmount(BigDecimal.valueOf(paymentTotal)));
			if (totalCommission > 0) {
				Optional<PaymentRequest> creditPaymentReq = bookingRequest.getPaymentInfos().stream()
						.filter(p -> (PaymentMedium.CREDIT_LINE.equals(p.getPaymentMedium())
								|| PaymentMedium.CREDIT.equals(p.getPaymentMedium())))
						.findFirst();
				String commPayUserId = StringUtils.isEmpty(payUserId) ? order.getBookingUserId() : payUserId;
				User commPayUser = userService.getUserFromCache(payUserId);
				WalletPaymentRequest comPayRequest = WalletPaymentRequest.builder()
						.amount(BigDecimal.valueOf(totalCommission - totalTds)).tds(BigDecimal.valueOf(totalTds))
						.product(bookingRequest.getPaymentInfos().get(0).getProduct()).refId(order.getBookingId())
						.opType(PaymentOpType.CREDIT).transactionType(PaymentTransactionType.COMMISSION)
						.payUserId(commPayUserId).partnerId(commPayUser.getPartnerId()).build();
				if (creditPaymentReq.isPresent()) {
					if (!directCommissionToWallet(comPayRequest, creditPaymentReq.get())) {
						comPayRequest.setPaymentMedium(creditPaymentReq.get().getPaymentMedium());
						comPayRequest
								.setWalletNumber(((WalletPaymentRequest) creditPaymentReq.get()).getWalletNumber());
						comPayRequest.setWalletId(((WalletPaymentRequest) creditPaymentReq.get()).getWalletId());
						comPayRequest.getAdditionalInfo()
								.setActualTransactionType(PaymentTransactionType.UTILISATION_ADJUSTMENT);
					}
				}
				bookingRequest.getPaymentInfos().add(comPayRequest);
			}
			if (partnerMarkup > 0) {
				WalletPaymentRequest creditToPartner = WalletPaymentRequest.builder()
						.amount(BigDecimal.valueOf(partnerMarkup - partnerMarkUpTds))
						.tds(BigDecimal.valueOf(partnerMarkUpTds))
						.product(bookingRequest.getPaymentInfos().get(0).getProduct()).refId(order.getBookingId())
						.opType(PaymentOpType.CREDIT).transactionType(PaymentTransactionType.PARTNER_MARKUP)
						.payUserId(payUser.getPartnerId()).tds(new BigDecimal(partnerMarkUpTds)).isInternalQuery(true)
						.build();
				bookingRequest.getPaymentInfos().add(creditToPartner);
			}
			if (partnerCommission > 0) {
				WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder()
						.refId(bookingRequest.getBookingId())
						.amount(BigDecimal.valueOf(partnerCommission - partnerCommissionTds))
						.tds(new BigDecimal(partnerCommissionTds)).payUserId(payUser.getPartnerId())
						.product(bookingRequest.getPaymentInfos().get(0).getProduct()).opType(PaymentOpType.CREDIT)
						.isInternalQuery(true).transactionType(PaymentTransactionType.PARTNER_COMMISSION).build();
				bookingRequest.getPaymentInfos().add(paymentRequest);
			}
			payments = paymentService.doPaymentsUsingPaymentRequests(bookingRequest.getPaymentInfos());
			if (CollectionUtils.isNotEmpty(payments)) {
				for (Payment payment : payments) {
					if (payment.getStatus().equals(PaymentStatus.SUCCESS)) {
						paymentStatus = true;
					}
					if (payment.getStatus().equals(PaymentStatus.INITIATE_REDIRECTION)) {
						requireRedirection = true;
						return;
					}
				}
				updateStatus(OrderStatus.PAYMENT_SUCCESS);
				if (paymentStatus) {
					log.info("Payment successfully charged for bookingId {}", bookingRequest.getBookingId());
				}
			}
		} catch (PaymentException pe) {
			log.error("Payment failed for bookingId {}", bookingRequest.getBookingId(), pe);
			reason = Optional.ofNullable(pe.getError()).orElse(SystemError.PAYMENT_FAILED).getMessage();
			throw pe;
		} finally {
			if (!paymentStatus && !requireRedirection) {
				if (!OrderStatus.ON_HOLD.equals(order.getStatus())) {
					order.setReason(reason);
					updateStatus(OrderStatus.FAILED);
					updateOrderAdditionalPaymentStatusInfo(PaymentStatus.FAILURE);
				}
			} else {
				updateOrderAdditionalPaymentStatusInfo(payments.get(0).getStatus());
			}
		}
	}

	private boolean directCommissionToWallet(PaymentRequest commissionReq, PaymentRequest creditPaymentReq) {
		if (commissionReq.getAmount().compareTo(creditPaymentReq.getAmount()) > 0) {
			commissionReq.setPaymentMedium(PaymentMedium.WALLET);
			return true;
		}
		return false;
	}

	private void updateOrderAdditionalPaymentStatusInfo(PaymentStatus paymentStatus) {
		OrderAdditionalInfo additionalInfo = order.getAdditionalInfo();
		additionalInfo.setPaymentStatus(paymentStatus);
		order.setAdditionalInfo(additionalInfo);
		order = orderManager.save(order);
	}

	public void buildPaymentInfos() {
		if (CollectionUtils.isNotEmpty(bookingRequest.getPaymentInfos()) && isApiChannel() && !hasPaymentMedium()) {
			PaymentModeRequest modeRequest = null;
			PaymentModeResponse modeResponse = null;
			try {
				List<PaymentRequest> paymentRequests = new ArrayList<>();
				modeRequest = buildPaymentRequest();
				BigDecimal payableAmount = modeRequest.getAmount();
				Product product = modeRequest.getProduct();
				modeResponse = paymentService.getPaymentModeResponse(modeRequest);
				boolean isRequestAdded = false;
				if (modeResponse != null && CollectionUtils.isNotEmpty(modeResponse.getPaymentModes())) {
					log.info("Internal PaymentInfo Build for {}", bookingRequest.getBookingId());
					WalletPaymentMode walletPaymentMode = getPaymentMode(modeResponse.getPaymentModes(),
							PaymentMedium.WALLET, WalletPaymentMode.class);
					CreditLinePaymentMode creditLineMode = getPaymentMode(modeResponse.getPaymentModes(),
							PaymentMedium.CREDIT_LINE, CreditLinePaymentMode.class);
					NRCreditPaymentMode nrCreditMode = getPaymentMode(modeResponse.getPaymentModes(),
							PaymentMedium.CREDIT, NRCreditPaymentMode.class);

					if (!isRequestAdded && walletPaymentMode != null
							&& walletPaymentMode.getWallet().getBalance().compareTo(payableAmount) >= 0) {
						PaymentRequest paymentRequest = PaymentRequest.builder().refId(bookingRequest.getBookingId())
								.paymentMedium(walletPaymentMode.getMode()).amount(payableAmount).build();
						paymentRequests.add(paymentRequest);
						isRequestAdded = true;
					}

					if (!isRequestAdded && creditLineMode != null
							&& CollectionUtils.isNotEmpty(creditLineMode.getCreditLines())) {
						for (CreditLine creditLine : creditLineMode.getCreditLines()) {
							if (creditLine.getBalance().compareTo(payableAmount) >= 0) {
								WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder()
										.refId(bookingRequest.getBookingId()).paymentMedium(PaymentMedium.CREDIT_LINE)
										.walletNumber(creditLine.getCreditNumber()).amount(payableAmount).build();
								paymentRequests.add(paymentRequest);
								isRequestAdded = true;
								break;
							}
						}
					}

					if (!isRequestAdded && nrCreditMode != null
							&& CollectionUtils.isNotEmpty(nrCreditMode.getNrCreditList())) {
						for (NRCredit nrCredit : nrCreditMode.getNrCreditList()) {
							if (nrCredit.getBalance().compareTo(payableAmount) >= 0) {
								WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder()
										.refId(bookingRequest.getBookingId()).paymentMedium(PaymentMedium.CREDIT)
										.walletNumber(nrCredit.getCreditId()).amount(payableAmount).build();
								paymentRequests.add(paymentRequest);
								isRequestAdded = true;
								break;
							}
						}
					}

					// finally if no payment assigned
					if (!isRequestAdded && CollectionUtils.isEmpty(paymentRequests)) {
						PaymentRequest paymentRequest = PaymentRequest.builder().refId(bookingRequest.getBookingId())
								.paymentMedium(PaymentMedium.WALLET).amount(payableAmount).build();
						paymentRequests.add(paymentRequest);
						isRequestAdded = true;
					}
					paymentRequests.forEach(pR -> {
						pR.setProduct(product);
						pR.setRefId(bookingRequest.getBookingId());
					});
					bookingRequest.setPaymentInfos(paymentRequests);
				}
			} finally {
				RestAPIListener.addInternalLog(LogData.builder().type("API Request Internal PaymentModeRq-")
						.key(bookingRequest.getBookingId()).logData(GsonUtils.getGson().toJson(modeRequest)).build());
				RestAPIListener.addInternalLog(LogData.builder().type("API Request Internal PaymentModeRs-")
						.key(bookingRequest.getBookingId()).logData(GsonUtils.getGson().toJson(modeResponse)).build());
			}
		}
	}

	protected <T> T getPaymentMode(List<PaymentMode> paymentModes, PaymentMedium medium, Class<T> paymentModeType) {
		for (PaymentMode paymentMode : paymentModes) {
			if (paymentMode.getMode().equals(medium)) {
				return (T) paymentMode;
			}
		}
		return null;
	}

	public PaymentModeRequest buildPaymentRequest() {
		PaymentModeRequest pModeRequest = PaymentModeRequest.builder().build();
		pModeRequest.setBookingId(bookingRequest.getBookingId());
		pModeRequest.setProduct(bookingRequest.getPaymentInfos().get(0).getProduct());
		pModeRequest.setAmount(bookingRequest.getPaymentInfos().get(0).getAmount());
		return pModeRequest;
	}

	public boolean isApiChannel() {
		return UserUtils.isApiUserRequest(SystemContextHolder.getContextData().getUser());
	}

	private boolean hasPaymentMedium() {
		return bookingRequest.getPaymentInfos().stream().filter(pInfo -> pInfo.getPaymentMedium() != null).findFirst()
				.isPresent();
	}

	protected abstract List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity);

	protected void calcRewardpoints() {

	}

	protected boolean isVoucherCodeInPayment() {
		if (CollectionUtils.isNotEmpty(bookingRequest.getPaymentInfos())) {
			for (PaymentRequest payment : bookingRequest.getPaymentInfos()) {
				if (StringUtils.isNotBlank(payment.getAdditionalInfo().getVoucherCode())) {
					return true;
				}
			}
		}
		return false;
	}

	protected PaymentRequest getVoucherCodeFromRequest() {
		if (CollectionUtils.isNotEmpty(bookingRequest.getPaymentInfos())) {
			for (PaymentRequest payment : bookingRequest.getPaymentInfos()) {
				if (StringUtils.isNotBlank(payment.getAdditionalInfo().getVoucherCode())) {
					return payment;
				}
			}
		}
		return null;
	}

	protected abstract void applyVoucher();

}
