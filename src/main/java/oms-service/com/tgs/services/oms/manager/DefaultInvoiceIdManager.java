package com.tgs.services.oms.manager;

import org.apache.commons.lang3.ObjectUtils;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.InvoiceIdConfiguration;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;

@Builder
public class DefaultInvoiceIdManager extends AbstractInvoiceIdManager {

	public static final String HOTEL_ID_PREFIX = "MW";
	public static final String AIR_DOMESTIC_ID_PREFIX = "TD";
	public static final String AIR_INTERNATIONAL_ID_PREFIX = "TI";
	public static final String CORPORATE_AIR_DOMESTIC_ID_PREFIX = "DD";
	public static final String CORPORATE_AIR_INTERNATIONAL_ID_PREFIX = "ID";

	private AirOrderItemService airOrderItemService;
	private HotelOrderItemService hotelOrderItemService;
	private UserServiceCommunicator userCommunicator;
	/**
	 * Must be non-null
	 */
	private InvoiceIdConfiguration invoiceIdConfiguration;

	@Override
	protected String getInvoiceIdPrefix(Order order, ProductMetaInfo productMetaInfo) {
		Product product = productMetaInfo.getProduct();
		String prefix;

		User bookingUser = userCommunicator.getUserFromCache(order.getBookingUserId());

		switch (product) {
			case AIR:
				String subProduct = productMetaInfo.getSubProduct();
				if (AirType.DOMESTIC.getCode().equals(subProduct)) {
					prefix = getPrefix(true, bookingUser);
				} else if (AirType.INTERNATIONAL.getCode().equals(subProduct)) {
					prefix = getPrefix(false, bookingUser);
				} else {
					throw new IllegalArgumentException("Invoice id not supported for sub-product: " + subProduct);
				}
				break;

			case HOTEL:
				prefix = getHotelIdPrefix();
				break;

			default:
				throw new IllegalArgumentException("Invoice id not supported for product: " + product);
		}

		return prefix + "-";
	}

	private String getPrefix(boolean isDomestic, User bookingUser) {
		String prefix = "";
		if (isDomestic) {
			prefix = getAirDomesticIdPrefix();
			if (UserUtils.isCorporate(bookingUser))
				prefix = getCorporateAirDomesticIdPrefix();
		} else {
			prefix = getAirInternationalIdPrefix();
			if (UserUtils.isCorporate(bookingUser))
				prefix = getCorporateAirInternationalIdPrefix();
		}
		return prefix;
	}

	private String getAirDomesticIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getAirDomesticIdPrefix(), AIR_DOMESTIC_ID_PREFIX);
	}

	private String getCorporateAirDomesticIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getCorporateAirDomesticIdPrefix(),
				CORPORATE_AIR_DOMESTIC_ID_PREFIX);
	}

	private String getAirInternationalIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getAirInternationalIdPrefix(),
				AIR_INTERNATIONAL_ID_PREFIX);
	}

	private String getCorporateAirInternationalIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getCorporateAirInternationalIdPrefix(),
				CORPORATE_AIR_INTERNATIONAL_ID_PREFIX);
	}

	private String getHotelIdPrefix() {
		return ObjectUtils.firstNonNull(getInvoiceIdConfiguration().getHotelIdPrefix(), HOTEL_ID_PREFIX);
	}

	private InvoiceIdConfiguration getInvoiceIdConfiguration() {
		if (invoiceIdConfiguration == null) {
			invoiceIdConfiguration = new InvoiceIdConfiguration();
		}
		return invoiceIdConfiguration;
	}

	@Override
	protected String getInvoiceIdSuffix(Order order, ProductMetaInfo productMetaInfo) {
		return null;
	}

	@Override
	protected int getNextNumericInvoiceId(Order order, ProductMetaInfo productMetaInfo) {
		Product product = productMetaInfo.getProduct();
		switch (product) {
			case AIR:
				return airOrderItemService.getNextInvoiceId();

			case HOTEL:
				return hotelOrderItemService.getNextInvoiceId();

			default:
				throw new IllegalArgumentException("Invoice id not supported for product: " + product);

		}
	}
}
