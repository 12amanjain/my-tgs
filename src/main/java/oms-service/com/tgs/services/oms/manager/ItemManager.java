package com.tgs.services.oms.manager;

import org.springframework.stereotype.Service;

import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.pms.datamodel.PaymentStatus;

@Service
public interface ItemManager {

	public void processBooking(String bookingId, Order order, PaymentStatus paymentStatus, double paymentFee);

}
