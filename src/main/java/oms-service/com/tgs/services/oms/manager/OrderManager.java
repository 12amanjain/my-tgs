package com.tgs.services.oms.manager;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class OrderManager {

	@Autowired
	private OrderService orderService;

	@Autowired
	private GstInfoService gstService;
	
	@Autowired
	private UserServiceCommunicator userSrvComm;
	
	@Autowired 
	private GeneralServiceCommunicator gmsComm;

	public Order save(Order order) {
		DbOrder dbOrder = orderService.findByBookingId(order.getBookingId());
		dbOrder = Optional.ofNullable(dbOrder).orElse(new DbOrder()).from(order);
		dbOrder = orderService.save(dbOrder);
		return dbOrder.toDomain();
	}

	public Order saveWithoutFetch(Order order) {
		DbOrder dbOrder = new DbOrder().from(order);
		dbOrder = orderService.save(dbOrder);
		return dbOrder.toDomain();
	}
	
	public Order saveWithoutProcessedOn(Order order) {
		DbOrder dbOrder = new DbOrder().from(order);
		dbOrder = orderService.saveWithoutProcessedOn(dbOrder);
		return dbOrder.toDomain();
	}

	public Order findByBookingId(String bookingId) {
		return findByBookingId(bookingId, null);
	}
	
	public Order findByBookingId(String bookingId, Order order) {
		DbOrder dbOrder = orderService.findByBookingId(bookingId);
		return dbOrder != null ? dbOrder.from(order).toDomain() : null;

	}
	
	public boolean deleteExistingFailedAndNewBooking(String bookingId) {
		return orderService.deleteExistingFailedAndNewBooking(bookingId);
	}

	public TravellerInfo saveTravellerInfo(TravellerInfo travellerInfo, User bookingUser, DeliveryInfo deliveryInfo) {
		if(BooleanUtils.isTrue(travellerInfo.getIsSave())) {
			gmsComm.saveTravellerInfo(travellerInfo, deliveryInfo,bookingUser);
		}
		return travellerInfo;
	}

	public GstInfo saveGstInfo(GstInfo gstInfo) {
		if (gstInfo != null && StringUtils.isNotEmpty(gstInfo.getGstNumber())) {
			DbOrderBilling dbGst = gstService.findByBookingId(gstInfo.getBookingId());
			gstService.save(Optional.ofNullable(dbGst).orElseGet(() -> new DbOrderBilling()).from(gstInfo));
			if (BooleanUtils.isTrue(gstInfo.getIsSave())) {
				/**
				 * Due to duplicate GST info error, duplicate booking issue (OrderUtils.checkDuplicateBooking(..)) is happening 
				 */
				try {
					gmsComm.saveBillingEntity(gstInfo);
				} catch (Exception e) {
					log.error("Failed to save billing entity for bookingId {} due to ", gstInfo.getBookingId(), e);
				}
			}
		}
		return gstInfo;
	}

	public GstInfo getGstInfo(String bookingId) {
		return Optional.ofNullable(gstService.findByBookingId(bookingId)).orElseGet(() -> new DbOrderBilling()).toDomain();
	}
	
	public void updateLastTransactionTime(Product product,String userId) {
		User userFromCache = userSrvComm.getUserFromCache(userId);
		Map<Product, LocalDateTime> lastTxnTime = userFromCache.getAdditionalInfo().getLastTxnTime() != null ? userFromCache.getAdditionalInfo().getLastTxnTime() : new HashMap<>();
		lastTxnTime.put(product, LocalDateTime.now());
		User user = User.builder().userId(userId).additionalInfo(UserAdditionalInfo.builder().lastTxnTime(lastTxnTime).build()).build();
		userSrvComm.updateUser(user);
	}
}
