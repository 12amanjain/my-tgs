package com.tgs.services.oms.manager.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.PointsServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteAdditionalInfo;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.air.AirItemDetailService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.mapper.SegmentInfoToAirOrderItemMapper;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.points.datamodel.AirRedeemPointsData;
import com.tgs.services.points.restmodel.AirRedeemPointResponse;
import com.tgs.services.vms.restmodel.FlightVoucherValidateRequest;
import com.tgs.services.vms.restmodel.FlightVoucherValidateResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
@Qualifier("AirBookingManager")
public class AirBookingManager extends BookingManager {
	private AirBookingRequest airBookingRequest;
	protected List<TripInfo> tripInfos;
	protected List<DbAirOrderItem> items;
	protected AirSearchQuery searchQuery;

	@Autowired
	protected AirOrderItemManager itemManager;

	@Autowired
	protected AirOrderItemService itemService;

	@Autowired
	protected AirItemDetailService detailService;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private PointsServiceCommunicator pointsService;

	// @Transactional
	public Order processBooking() {
		airBookingRequest = (AirBookingRequest) bookingRequest;
		bookingRequest.setFlowType(OrderFlowType.ONLINE);
		this.storeTravelInfo();
		this.storeGstInfo();
		this.calcRewardpoints();
		this.applyVoucher();
		this.storeOrderItem();
		this.createOrder();
		this.addOrderToAudit(AuditAction.ORDER_CREATE);
		log.info("Order items successfully stored for bookingId {}", airBookingRequest.getBookingId());
		this.doPayment();
		this.postPaymentProcessing();
		return order;
	}

	private void postPaymentProcessing() {
		// if (CollectionUtils.isEmpty(airBookingRequest.getPaymentInfos())) {
		// itemComm.save(items, order, AirItemStatus.HOLD_PENDING);
		// }
	}

	public void storeTravelInfo() {
		log.info("Storing Traveller Info for bookingId {}", airBookingRequest.getBookingId());
		for (FlightTravellerInfo travellerInfo : airBookingRequest.getTravellerInfo()) {
			orderManager.saveTravellerInfo(travellerInfo, bookingUser, airBookingRequest.getDeliveryInfo());
		}
		if (UserRole.corporate(bookingUser.getRole())) {
			detailService.saveProfileFields(airBookingRequest.getTravellerInfo(), airBookingRequest.getBookingId());
		}
	}

	protected void storeOrderItem() {

		this.updateUserCCinfo();

		List<DbAirOrderItem> existsItems = itemManager.findByBooking(airBookingRequest.getBookingId());
		if (CollectionUtils.isNotEmpty(existsItems)) {
			log.error("Duplicate airOrderItems exists {}", airBookingRequest.getBookingId());
			throw new CustomGeneralException(SystemError.DUPLICATE_BOOKING_ID);
		}

		items = new ArrayList<>();
		double creditShellBalance = 0;
		paymentTotal = 0d;
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				DbAirOrderItem orderItem = SegmentInfoToAirOrderItemMapper.builder().segmentInfo(segmentInfo)
						.bookingUser(bookingUser).bookingId(airBookingRequest.getBookingId()).searchQuery(searchQuery)
						.fmsCommunicator(fmsCommunicator).gstInfo(airBookingRequest.getGstInfo()).build().convert();
				items.add(orderItem);
				orderTotal += orderItem.getAmount();
				totalMarkup += orderItem.getMarkup();
				partnerMarkup += AirBookingUtils.getPartnerMarkUp(Arrays.asList(orderItem), bookingUser);
				totalCommission += AirBookingUtils.getGrossCommission(Arrays.asList(orderItem), bookingUser, false);
				totalTds += AirBookingUtils.getTDS(Arrays.asList(orderItem));
				paymentTotal += AirBookingUtils.getPaymentTotal(Arrays.asList(orderItem), bookingUser);
				creditShellBalance += AirBookingUtils.calculateAvailablePNRCredit(Arrays.asList(orderItem));
				partnerCommission += AirBookingUtils.getPartnerCommission(Arrays.asList(orderItem));
				partnerCommissionTds += AirBookingUtils.getPartnerTds(Arrays.asList(orderItem), FareComponent.PCTDS);
				partnerMarkUpTds += AirBookingUtils.getPartnerTds(Arrays.asList(orderItem), FareComponent.PMTDS);
				// pointsDiscount += AirBookingUtils.getRedeemedPointAmount(Arrays.asList(orderItem), FareComponent.RP);
			}
		}
		updateCreditShellUtilizationAmount(creditShellBalance, items);
		// If booking has PNR credits (EX: credit shell) then it shouldn't be charged.
		paymentTotal -= AirBookingUtils.calculateAvailablePNRCredit(items);
		itemManager.save(items, null, null);
	}

	/**
	 * In credit shell till review CS component will have available credit shell balance, <br>
	 * while in booking CS will be updated with amount to be utilized from credit shell.
	 * 
	 * Reset CS to 0 for all airOrderItem and then updates CS amount in first airOrderItem.
	 */
	private void updateCreditShellUtilizationAmount(double creditShellBalance, List<DbAirOrderItem> items) {
		double totalCreditShellBalance = creditShellBalance;
		if (creditShellBalance > 0) {
			double totalCreditUtilizationAmount = 0;
			for (DbAirOrderItem item : items) {
				for (FlightTravellerInfo traveller : item.getTravellerInfo()) {
					double airlineFare = 0;
					airlineFare += BookingUtils.getAmountChargedByAirline(traveller);
					// resets CS to 0 for all travellers.
					traveller.getFareDetail().getFareComponents().put(FareComponent.CS, 0d);
					double creditUtilizationAmount = Math.min(airlineFare, creditShellBalance);
					creditShellBalance -= creditUtilizationAmount;
					traveller.getFareDetail().getFareComponents().put(FareComponent.CS, creditUtilizationAmount);
					totalCreditUtilizationAmount += creditUtilizationAmount;
				}
			}
			Note creditShellNote = Note.builder().bookingId(airBookingRequest.getBookingId()).noteType(NoteType.GENERAL)
					.noteMessage(StringUtils.join("Credit shell Balance ", totalCreditUtilizationAmount, " of ",
							totalCreditShellBalance, " applied for the booking. PNR - ",
							items.get(0).getAdditionalInfo().getCreditShellPNR(), ". Old Cart Id - ",
							searchQuery.getSearchModifiers().getPnrCreditInfo().getBookingId()))
					.additionalInfo(NoteAdditionalInfo.builder().visibleToAgent(true).build()).build();
			super.getGmsComm().addNote(creditShellNote);
		}
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		AirItemStatus itemStatus = orderStatus.equals(OrderStatus.PAYMENT_SUCCESS) ? AirItemStatus.PAYMENT_SUCCESS
				: AirItemStatus.PAYMENT_FAILED;
		itemManager.save(items, order, itemStatus);
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		List<PaymentRequest> virtualPaymentRequests = new ArrayList<>();
		Map<String, BookingSegments> bookingSegments = fmsCommunicator
				.getSupplierWiseBookingSegmentsUsingSession(tripInfos, bookingRequest.getBookingId(), bookingUser);
		if (MapUtils.isNotEmpty(bookingSegments)) {
			for (Entry<String, BookingSegments> entry : bookingSegments.entrySet()) {
				virtualPaymentRequests.add(AirBookingUtils.createVirtualPaymentRequest(paymentInfos.get(0),
						entry.getValue().getSegmentInfos(), bookingUser));
			}
		}
		return virtualPaymentRequests;
	}

	// Only for virtual payment.
	protected void updateUserCCinfo() {
		if (AirBookingUtils.isVirtualPayment(bookingRequest) && CollectionUtils.isNotEmpty(tripInfos)) {
			for (TripInfo trip : tripInfos) {
				// cardInfo can not be null as we verified it in request itself.
				CreditCardInfo cardInfo =
						cmsCommunicator.getVirtualCreditCard(trip, bookingUser, bookingRequest.getGstInfo());
				for (SegmentInfo segment : trip.getSegmentInfos()) {
					segment.getPriceInfo(0).getMiscInfo().setIsUserCreditCard(true);
					// This overrides the previously applied credit card based on billing entity.
					// Applicable for virtual payment.
					segment.getPriceInfo(0).getMiscInfo().setCcInfoId(cardInfo.getId().intValue());
				}
			}
		}
	}

	@Override
	protected void calcRewardpoints() {
		// Debit
		Optional<PaymentRequest> paymentRq = getAirBookingRequest().getPaymentRequestOnMedium(PaymentMedium.POINTS);
		if (paymentRq.isPresent()) {
			PaymentRequest debitPaymentRq = paymentRq.get();
			log.info("Payment request nop {}, additional info nop {}", debitPaymentRq.getNoOfPoints(),
					debitPaymentRq.getAdditionalInfo().getNoOfPoints());
			Double totalPoints = ObjectUtils.firstNonNull(debitPaymentRq.getNoOfPoints(),
					debitPaymentRq.getAdditionalInfo().getNoOfPoints());
			AirRedeemPointsData debitPointRequest = new AirRedeemPointsData();
			debitPointRequest.setTripInfos(tripInfos);
			debitPointRequest.setBookingId(bookingRequest.getBookingId());
			debitPointRequest.setProduct(Product.AIR);
			debitPointRequest.setOpType(PaymentOpType.DEBIT);
			debitPointRequest.setPoints(totalPoints);
			debitPaymentRq.getAdditionalInfo().setPointsType(PointsType.COIN);
			debitPointRequest.setType(PointsType.COIN);
			AirRedeemPointResponse pointResponse = pointsService.validateAirRewardPoints(debitPointRequest, true);
			if (CollectionUtils.isNotEmpty(pointResponse.getTripInfos())
					&& CollectionUtils.isEmpty(pointResponse.getErrors())) {
				tripInfos = pointResponse.getTripInfos();
				debitPaymentRq.setAmount(pointResponse.getAmount());
				debitPaymentRq.setNoOfPoints(totalPoints);
				log.info("Point Converted to Amount for booking {} and amount {}", bookingRequest.getBookingId(),
						debitPaymentRq.getAmount());
			}
		}

		// Credit
		/*
		 * FlightsRedeemPointsRequest creditPaymentRq = new FlightsRedeemPointsRequest();
		 * creditPaymentRq.setTripInfos(tripInfos); creditPaymentRq.setBookingId(bookingRequest.getBookingId());
		 * creditPaymentRq.setProduct(Product.AIR); creditPaymentRq.setOpType(PaymentOpType.CREDIT);
		 * creditPaymentRq.setType(PointsType.COIN); FlightsRedeemPointResponse creditPointRsp =
		 * pointsService.creditRewardPoints(creditPaymentRq, true); if (creditPointRsp.getTotalRedeemablePoints() > 0 &&
		 * CollectionUtils.isEmpty(pointResponse.getErrors())) { PaymentAdditionalInfo additionalInfo =
		 * PaymentAdditionalInfo.builder().pointsType(PointsType.COIN)
		 * .noOfPoints(creditPointRsp.getTotalRedeemablePoints()) .pointsExpiry(creditPointRsp.getExpiryTime() != null ?
		 * LocalDateTime.now().plus(creditPointRsp.getExpiryTime().getHour(), ChronoUnit.HOURS) : null) .build();
		 * PaymentRequest paymentRequest =
		 * PaymentRequest.builder().refId(bookingRequest.getBookingId()).paymentMedium(PaymentMedium.POINTS)
		 * .additionalInfo(additionalInfo).transactionType(PaymentTransactionType.TOPUP).build();
		 * getAirBookingRequest().getPaymentInfos().add(paymentRequest);
		 * log.info("Credit New Points for Booking {} points {}", getAirBookingRequest().getBookingId(),
		 * creditPointRsp.getTotalRedeemablePoints()); }
		 */
	}

	protected void applyVoucher() {
		if (isVoucherCodeInPayment()) {
			try {
				PaymentRequest payment = super.getVoucherCodeFromRequest();
				List<PaymentMedium> mediumList = bookingRequest.getPaymentInfos().stream()
						.map(pm -> pm.getPaymentMedium()).collect(Collectors.toList());
				FlightVoucherValidateRequest voucherRq = new FlightVoucherValidateRequest(bookingRequest.getBookingId(),
						payment.getAdditionalInfo().getVoucherCode(), Product.AIR, mediumList, null);
				voucherRq.setTripInfos(tripInfos);
				FlightVoucherValidateResponse response = voucherService.applyAirVoucher(voucherRq, true);
				if (response.getDiscountedAmount() > 0 && CollectionUtils.isNotEmpty(response.getTripInfos())) {
					// voucher applied tripinfo with modified fare detail
					tripInfos = response.getTripInfos();
				}
			} catch (CustomGeneralException e) {
				throw e;
			} catch (Exception e) {
				throw new CustomGeneralException(SystemError.VOUCHER_CODE_NOT_AVAILABLE);
			}
		}
	}
}
