package com.tgs.services.oms.manager.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.restmodel.air.AirConfirmBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
@Qualifier("AirConfirmBookingManager")
public class AirConfirmBookingManager extends BookingManager {
	private AirConfirmBookingRequest confirmBookingRequest;
	private List<TripInfo> tripInfos;
	private List<DbAirOrderItem> items;
	Map<String, BookingSegments> bookingSegments;

	@Autowired
	private AirOrderItemManager itemManager;

	@Autowired
	private UserServiceCommunicator userService;

	public Order processBooking() {
		paymentTotal = 0d;
		confirmBookingRequest = (AirConfirmBookingRequest) bookingRequest;
		order = orderManager.findByBookingId(confirmBookingRequest.getBookingId(), null);
		if (OrderFlowType.ONLINE.equals(order.getAdditionalInfo().getFlowType()))
			order.getAdditionalInfo().setFlowType(OrderFlowType.HOLD_CONFIRM);
		items = itemManager.findByBookingId(confirmBookingRequest.getBookingId());

		applyVoucher();

		List<SegmentInfo> segmentList = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(getItems(), null);
		tripInfos = BookingUtils.createTripListFromSegmentList(segmentList);
		bookingSegments = fmsCommunicator.getSupplierWiseBookingSegmentsUsingPNR(tripInfos);
		this.updateUserCCinfo();
		totalCommission = AirBookingUtils.getGrossCommission(items, bookingUser, false);
		totalTds += AirBookingUtils.getTDS(items);
		totalMarkup = order.getMarkup();
		orderTotal = order.getAmount();
		paymentTotal += AirBookingUtils.getPaymentTotal(items, bookingUser);
		partnerMarkup += AirBookingUtils.getPartnerMarkUp(items, bookingUser);
		partnerCommission += AirBookingUtils.getPartnerCommission(items);
		partnerCommissionTds += AirBookingUtils.getPartnerTds(items, FareComponent.PCTDS);
		partnerMarkUpTds += AirBookingUtils.getPartnerTds(items, FareComponent.PMTDS);
		this.doPayment();
		this.addOrderToAudit(AuditAction.ORDER_CONFIRM);
		return order;
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		AirItemStatus itemStatus = orderStatus.equals(OrderStatus.PAYMENT_SUCCESS) ? AirItemStatus.PAYMENT_SUCCESS
				: AirItemStatus.PAYMENT_FAILED;
		itemManager.save(items, order, itemStatus);

	}

	public void updateItemStatus() {
		for (DbAirOrderItem item : items) {
			itemManager.save(Arrays.asList(item), order, itemManager.getAirItemStatus(item.toDomain(), order));
		}
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		List<PaymentRequest> virtualPaymentRequests = new ArrayList<>();
		if (MapUtils.isNotEmpty(bookingSegments)) {
			for (Entry<String, BookingSegments> entry : bookingSegments.entrySet()) {
				virtualPaymentRequests.add(AirBookingUtils.createVirtualPaymentRequest(paymentInfos.get(0),
						entry.getValue().getSegmentInfos(), bookingUser));
			}
		}
		return virtualPaymentRequests;
	}

	// Only for virtual payment.
	private void updateUserCCinfo() {
		if (AirBookingUtils.isVirtualPayment(bookingRequest)) {
			GstInfo billingEntity = orderManager.getGstInfo(bookingRequest.getBookingId());
			for (TripInfo trip : tripInfos) {

				// cardInfo can not be null as we verified it in request itself.
				CreditCardInfo cardInfo = cmsCommunicator.getVirtualCreditCard(trip, bookingUser, billingEntity);
				for (SegmentInfo segment : trip.getSegmentInfos()) {
					DbAirOrderItem item = AirBookingUtils.findOrderItemFromSegmentInfo(segment, items);
					AdditionalAirOrderItemInfo itemAdditionalInfo = ObjectUtils.firstNonNull(item.getAdditionalInfo(),
							AdditionalAirOrderItemInfo.builder().build());

					// This overrides the previously applied credit card based on billing entity.
					// Applicable for virtual payment.
					itemAdditionalInfo.setCcId(cardInfo.getId().intValue());
					itemAdditionalInfo.setIsUserCreditCard(true);
					item.setAdditionalInfo(itemAdditionalInfo);
					itemManager.save(item, order, AirItemStatus.getAirItemStatus(item.getStatus()));

					// This updates the global tripInfos.
					segment.getPriceInfo(0).getMiscInfo().setIsUserCreditCard(true);
					segment.getPriceInfo(0).getMiscInfo().setCcInfoId(cardInfo.getId().intValue());
				}
			}
		}
	}


	@Override
	protected void applyVoucher() {
		// Post booking you can't apply voucher
		/*
		 * if (isVoucherCodeInPayment()) { PaymentRequest payment = this.getVoucherCodeFromRequest();
		 * List<PaymentMedium> mediumList = bookingRequest.getPaymentInfos().stream().map(pm -> pm.getPaymentMedium())
		 * .collect(Collectors.toList()); FlightVoucherValidateRequest voucherRq = new
		 * FlightVoucherValidateRequest(bookingRequest.getBookingId(), payment.getAdditionalInfo().getVoucherCode(),
		 * Product.AIR, mediumList, null); FlightVoucherValidateResponse response =
		 * voucherService.applyAirVoucher(voucherRq, true); if (response != null && response.getDiscountedAmount() > 0)
		 * { // itemManager.save(items, order, itemManager.getAirItemStatus(item.toDomain(), order)); } }
		 */
	}
}
