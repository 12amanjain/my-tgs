package com.tgs.services.oms.manager.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.tgs.services.oms.restmodel.air.AirManualBookingRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Service
@Qualifier("AirManualBookingManager")
public class AirManualBookingManager extends AirBookingManager {

	@Autowired
	AirOrderItemManager itemManager;

	@Autowired
	FMSCachingServiceCommunicator cacheService;

	protected List<FlightTravellerInfo> paxProfiles;

	@Override
	// @Transactional
	public Order processBooking() {
		this.storeGstInfo();
		this.storeUserProfiles();
		this.storeOrderItem();
		checkExistingItemsAndSave();
		this.createOrder();
		this.addOrderToAudit(getAuditAction());
		log.info("Orderitems successfully stored for bookingId {}", bookingRequest.getBookingId());
		this.doPayment();
		this.postPaymentProcessing();
		return order;
	}

	private AuditAction getAuditAction() {
		if (bookingRequest.getFlowType().equals(OrderFlowType.MANUAL_ORDER))
			return AuditAction.MANUAL_ORDER_CREATE;
		return AuditAction.REISSUED;
	}

	@Override
	protected void storeOrderItem() {
		paymentTotal = 0d;
		updateUserCCinfo();
		for (DbAirOrderItem orderItem : items) {
			orderTotal += orderItem.getAmount();
			totalMarkup += orderItem.getMarkup();
			totalCommission += AirBookingUtils.getGrossCommission(Arrays.asList(orderItem), bookingUser, true);
			totalTds += AirBookingUtils.getTDS(Arrays.asList(orderItem));
			paymentTotal += AirBookingUtils.getPaymentTotal(Arrays.asList(orderItem), bookingUser);
		}
	}

	public void storeUserProfiles() {
		if (UserRole.corporate(bookingUser.getRole())) {
			detailService.saveProfileFields(paxProfiles, bookingRequest.getBookingId());
		}
	}

	protected void checkExistingItemsAndSave() {
		List<DbAirOrderItem> existingItems = itemService.findByBookingId(bookingRequest.getBookingId());
		if (CollectionUtils.isNotEmpty(existingItems)) {
			for (int i = 0; i < existingItems.size(); i++) {
				items.get(i).setId(existingItems.get(i).getId());
			}
		}
		itemManager.save(items, null, null);
	}

	public List<DbAirOrderItem> getAirOrderItems(List<com.tgs.services.oms.datamodel.air.AirOrderItem> airOrderItems) {
		List<DbAirOrderItem> dbAirOrderItems1 = new ArrayList<>();
		dbAirOrderItems1.addAll(new DbAirOrderItem().toDbList(airOrderItems));
		return dbAirOrderItems1;
	}

	public AirManualBookingRequest getBookingRequest(AirManualBookingRequest request) {
		return cacheService.fetchValue(request.getBookingId(), AirManualBookingRequest.class,
				CacheSetName.AIR_REVIEW.getName(), BinName.AIRREVIEW.getName());
	}

	public boolean storeBookingRequest(AirManualBookingRequest request) {
		return cacheService.store(request.getBookingId(), BinName.AIRREVIEW.getName(), request, CacheMetaInfo.builder()
				.set(CacheSetName.AIR_REVIEW.name()).compress(true).plainData(false).expiration(7200).build());
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		AirItemStatus itemStatus = orderStatus.equals(OrderStatus.PAYMENT_SUCCESS) ? AirItemStatus.PAYMENT_SUCCESS
				: AirItemStatus.PAYMENT_FAILED;
		itemManager.save(items, order, itemStatus);
	}

	private void postPaymentProcessing() {
		for (DbAirOrderItem item : items) {
			item.setStatus(itemManager.getAirItemStatus(item.toDomain(), order).getStatus());
		}
		itemManager.save(items, order, null);
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		List<PaymentRequest> virtualPaymentRequests = new ArrayList<>();
		Map<String, BookingSegments> bookingSegments = new HashMap<String, BookingSegments>();
		List<SegmentInfo> segmentList = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(items, null);
		List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentList);
		bookingSegments = fmsCommunicator.getSupplierWiseBookingSegmentsUsingPNR(tripInfos);
		if (MapUtils.isNotEmpty(bookingSegments)) {
			for (Entry<String, BookingSegments> entry : bookingSegments.entrySet()) {
				virtualPaymentRequests.add(AirBookingUtils.createVirtualPaymentRequest(paymentInfos.get(0),
						entry.getValue().getSegmentInfos(), bookingUser));
			}
		}
		return virtualPaymentRequests;
	}
}
