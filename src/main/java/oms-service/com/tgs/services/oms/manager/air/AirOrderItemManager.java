package com.tgs.services.oms.manager.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.AuditResult;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirMessageRequest;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import com.tgs.services.oms.datamodel.ProcessedOrder;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.air.ProcessedAirDetails;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.air.AirItemDetailService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.manager.ItemManager;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.mapper.SegmentInfoToAirOrderItemMapper;
import com.tgs.services.oms.restcontroller.air.OrderActionValidator;
import com.tgs.services.oms.servicehandler.air.AirOrderMessageHandler;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirOrderItemManager implements ItemManager {

	@Autowired
	private AirOrderItemService itemService;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private AirItemDetailService airDetailService;

	private static FMSCommunicator fmsCommunicator;

	private static AirOrderMessageHandler msgHandler;

	private static GeneralServiceCommunicator gmsComm;

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	private AuditsHandler auditsHandler;

	@Autowired
	private GstInfoService gstSevice;

	private static List<OrderStatus> ANALYTICS_ORDER_STATUS =
			Arrays.asList(OrderStatus.FAILED, OrderStatus.ON_HOLD, OrderStatus.SUCCESS);

	@Autowired
	private OrderActionValidator actionValidator;

	@Value("${domain}")
	private String domain;

	@Value("${commProtocol}")
	private String protocol;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	public static void init(FMSCommunicator fmsCommunicator, AirOrderMessageHandler msgHandler,
			GeneralServiceCommunicator gmsComm) {
		AirOrderItemManager.fmsCommunicator = fmsCommunicator;
		AirOrderItemManager.msgHandler = msgHandler;
		AirOrderItemManager.gmsComm = gmsComm;
	}

	private OrderStatus getOrderStatus(String bookingId, List<DbAirOrderItem> items) {
		if (Objects.isNull(items)) {
			items = findByBookingId(bookingId);
		}
		AirItemStatus itemStatus = AirItemStatus.getAirItemStatus(items.get(0).getStatus());
		boolean hasAllItemSameStatus = true;
		OrderStatus status = OrderStatus.SUCCESS;
		boolean isAnySegmentCancelled = false;
		for (DbAirOrderItem item : items) {
			if (!item.getStatus().equals(itemStatus.getCode())) {
				hasAllItemSameStatus = false;
			}
			if (item.getStatus().equals(AirItemStatus.CANCELLED.getCode()))
				isAnySegmentCancelled = true;
			if (item.getCreatedOn().plusMinutes(3).isBefore(LocalDateTime.now())
					&& (item.getStatus().equals(AirItemStatus.PNR_PENDING.getCode())
							|| item.getStatus().equals(AirItemStatus.TICKET_PENDING.getCode())
							|| item.getStatus().equals(AirItemStatus.HOLD_PENDING.getCode())
							|| item.getStatus().equals(AirItemStatus.CONFIRM_PENDING.getCode())
							|| itemStatus.getCode().equals(AirItemStatus.PAYMENT_SUCCESS.getCode()))) {
				status = OrderStatus.PENDING;
				break;
			} else {
				if (item.getStatus().equals(AirItemStatus.UNCONFIRMED.getCode())) {
					status = OrderStatus.UNCONFIRMED;
					break;
				}
				if (isAnySegmentCancelled)
					status = OrderStatus.SUCCESS;
				else if (status != OrderStatus.PENDING)
					status = OrderStatus.IN_PROGRESS;
			}
		}
		if (hasAllItemSameStatus) {
			if (itemStatus.getCode().equals(AirItemStatus.SUCCESS.getCode())) {
				status = OrderStatus.SUCCESS;
			}
			if (itemStatus.getCode().equals(AirItemStatus.ON_HOLD.getCode())) {
				status = OrderStatus.ON_HOLD;
			}
			if (itemStatus.getCode().equals(AirItemStatus.IN_PROGRESS.getCode())) {
				status = OrderStatus.IN_PROGRESS;
			}
			if (itemStatus.getCode().equals(AirItemStatus.PAYMENT_FAILED.getCode())) {
				status = OrderStatus.FAILED;
			}
			if (itemStatus.getCode().equals(AirItemStatus.ABORTED.getCode())) {
				status = OrderStatus.ABORTED;
			}
			if (itemStatus.getCode().equals(AirItemStatus.CANCELLED.getCode())) {
				status = OrderStatus.CANCELLED;
			}
		}
		log.info("New Order status for bookingId {}  is status {}", bookingId, status);
		return status;
	}

	private void updateOrderStatus(Order order, List<DbAirOrderItem> items) {
		OrderStatus oldOrderStatus = order.getStatus();
		OrderStatus status = getOrderStatus(order.getBookingId(), items);
		log.debug("Order Status {} for BookingId {}", status, order.getBookingId());
		order.setStatus(status);
		order.getAdditionalInfo().setFirstUpdateTime(statusfirstUpdateTime(order));
		order = orderManager.save(order);
		if (!oldOrderStatus.equals(status)) {
			if (status.equals(OrderStatus.SUCCESS)) {
				orderManager.updateLastTransactionTime(Product.AIR, order.getBookingUserId());
				User bookingUser = userService.getUserFromCache(order.getBookingUserId());
				if (UserRole.corporate(bookingUser.getRole())) {
					Set<String> pnrSet = new HashSet<>();
					for (DbAirOrderItem item : items) {
						for (FlightTravellerInfo pax : item.getTravellerInfo()) {
							pnrSet.add(pax.pnrKey());
						}
					}
					log.debug("UpdateOrderStatus: BookingId = {}, PNRKeySet = {}", order.getBookingId(),
							new Gson().toJson(pnrSet));
					Map<String, String> invoiceMap = new HashMap<>();
					for (String key : pnrSet) {
						String inv = invoiceIdManager.getInvoiceIdFor(order,
								Product.getProductMetaInfoFromId(order.getBookingId()));
						invoiceMap.put(key, inv);
						log.debug("UpdateOrderStatus: BookingId = {}, Generated invoice = {}, for key = {}",
								order.getBookingId(), inv, key);
						for (DbAirOrderItem x : items) {
							for (FlightTravellerInfo pax : x.getTravellerInfo()) {
								String key1 = pax.pnrKey();
								if (key.equals(key1)) {
									pax.setInvoice(inv);
								}
							}
						}
					}
					order.getAdditionalInfo().setInvoiceMap(invoiceMap);
				}
				invoiceIdManager.setInvoiceIdFor(order, Product.getProductMetaInfoFromId(order.getBookingId()));
				orderManager.save(order);
				itemService.save(items);
				httpReqToSendEmail(order);
				msgHandler.sendBookingSms(order, SmsTemplateKey.BOOKING_CONFIRMATION_SMS);
			} else if (status.equals(OrderStatus.FAILED)) {
				httpReqToSendEmail(order);
				msgHandler.sendBookingSms(order, SmsTemplateKey.BOOKING_FAILURE_SMS);
			} else if (status.equals(OrderStatus.PENDING)) {
				httpReqToSendEmail(order);
				msgHandler.sendBookingSms(order, SmsTemplateKey.BOOKING_PENDING_SMS);
			} else if (status.equals(OrderStatus.ON_HOLD)) {
				// msgHandler.sendBookingEmail(order, EmailTemplateKey.AIR_BOOKING_HOLD_EMAIL);
				httpReqToSendEmail(order);
				msgHandler.sendBookingSms(order, SmsTemplateKey.BOOKING_HOLD_SMS);
			} else if (status.equals(OrderStatus.ABORTED)) {
				msgHandler.sendBookingSms(order, SmsTemplateKey.BOOKING_ABORTED_SMS);
			}
		}
	}

	private void httpReqToSendEmail(Order order) {
		try {
			Map<String, String> headerParams = new HashMap<>();
			headerParams.put("Content-Type", "application/json");
			headerParams.put("deviceid", "System");
			if (SystemContextHolder.getContextData().getHttpHeaders().getJwtToken() != null) {
				headerParams.put("Authorization", SecurityConstants.TOKEN_PREFIX
						+ SystemContextHolder.getContextData().getHttpHeaders().getJwtToken());
			} else if (SystemContextHolder.getContextData().getHttpHeaders().getApikey() != null) {
				headerParams.put(SecurityConstants.API_HEADER_KEY,
						SystemContextHolder.getContextData().getHttpHeaders().getApikey());
			}
			User bookingUser = userService.getUserFromCache(order.getBookingUserId());
			String toEmails = OrderUtils.getToEmailId(order, bookingUser, null);
			AirMessageRequest request = AirMessageRequest.builder().mode(MessageMedium.EMAIL)
					.bookingId(order.getBookingId()).toIds(toEmails).build();
			String strPostData = GsonUtils.getGson().toJson(request);
			HttpUtils httpUtils = HttpUtils.builder().urlString(protocol + "://" + domain + "/oms/v1/air/msg-ticket")
					.headerParams(headerParams).postData(strPostData).build();
			httpUtils.getResponse(BaseResponse.class).get();
		} catch (Exception e) {
			log.error("Unable to send booking confirmation email for bookingid {}", order.getBookingId(), e);
		}
	}

	public void save(List<DbAirOrderItem> items, Order order, AirItemStatus itemStatus) {
		List<DbAirOrderItem> savedItems = new ArrayList<>();
		for (DbAirOrderItem item : items) {
			savedItems.add(save(item, order, itemStatus));
		}
		if (order != null) {
			order = orderManager.findByBookingId(order.getBookingId(), order);
			updateOrderStatus(order, savedItems);
			addOrderToAnalytics(order);
			airDetailService.saveLFF(savedItems, order);
		}
	}

	public DbAirOrderItem save(DbAirOrderItem item, Order order, AirItemStatus itemStatus) {
		AirItemStatus currentStatus = AirItemStatus.getAirItemStatus(item.getStatus());
		if (itemStatus != null && currentStatus.nextStatusSet().contains(itemStatus)) {
			item.setStatus(itemStatus.getCode());
		}
		return itemService.save(item);
	}

	public List<SegmentInfo> updateOrderAndItem(List<SegmentInfo> segmentInfos, Order order, AirItemStatus itemStatus) {
		if (order == null) {
			throw new RuntimeException("Order can't be null");
		}
		List<DbAirOrderItem> items = findByBookingId(order.getBookingId());
		log.info("Updating item status for bookingId {}  is status {} ", order.getBookingId(), itemStatus);

		GstInfo gstInfo = null;
		DbOrderBilling dbGst = gstSevice.findByBookingId(order.getBookingId());
		if (dbGst != null) {
			gstInfo = dbGst.toDomain();
		}

		for (SegmentInfo info : segmentInfos) {
			DbAirOrderItem dbItem = AirBookingUtils.findOrderItemFromSegmentInfo(info, items);
			dbItem = SegmentInfoToAirOrderItemMapper.builder().segmentInfo(info).fmsCommunicator(fmsCommunicator)
					.gstInfo(gstInfo).build().setOutput(dbItem).convert();

			if (itemStatus != null) {
				if (isValidItemStatus(dbItem, itemStatus)) {
					dbItem.setStatus(itemStatus.getCode());
				} else {
					log.error("Invalid status to update air order item bookingId {}", order.getBookingId());
				}
			}
			itemService.save(dbItem);
		}

		double markup = 0.0;
		for (DbAirOrderItem orderItem : items) {
			markup += orderItem.getMarkup();
		}
		order.setMarkup(markup);

		updateOrderStatus(order, items);
		addOrderToAnalytics(order);
		return segmentInfos;
	}

	private boolean isValidItemStatus(DbAirOrderItem item, AirItemStatus itemStatus) {
		if (AirItemStatus.getAirItemStatus(item.getStatus()).nextStatusSet().contains(itemStatus)) {
			return true;
		}
		return false;
	}

	public List<DbAirOrderItem> findByBookingId(String bookingId) {
		List<DbAirOrderItem> itemList;
		itemList = itemService.findByBookingId(bookingId);
		if (CollectionUtils.isEmpty(itemList)) {
			log.error("No airOrderItems found for bookingId {}", bookingId);
			throw new RuntimeException();
		}
		return itemList;
	}

	public List<DbAirOrderItem> findByBooking(String bookingId) {
		List<DbAirOrderItem> itemList = itemService.findByBookingId(bookingId);
		return itemList;
	}

	public Map<String, List<TripInfo>> findTripsByBookingIds(List<String> bookingIds) {
		List<DbAirOrderItem> items = itemService.findByBookingId(bookingIds);
		return fetchTripInfoFromAirOrderItems(items);
	}

	public List<Object[]> findTripsByOrderFilter(OrderFilter filter) {
		return itemService.findByJsonSearch(filter);
	}

	public Map<String, List<TripInfo>> fetchTripInfoFromAirOrderItems(List<DbAirOrderItem> items) {
		Map<String, List<TripInfo>> bookingTripMap = new LinkedHashMap<>();
		if (CollectionUtils.isNotEmpty(items)) {
			Map<String, List<DbAirOrderItem>> itemMap =
					items.stream().collect(Collectors.groupingBy(DbAirOrderItem::getBookingId, LinkedHashMap::new,
							Collectors.toCollection(ArrayList::new)));

			for (Map.Entry<String, List<DbAirOrderItem>> entry : itemMap.entrySet()) {
				List<SegmentInfo> segmentList = new ArrayList<>();
				for (DbAirOrderItem item : entry.getValue()) {
					SegmentInfo sInfo = AirOrderItemToSegmentInfoMapper.builder().item(item)
							.fmsCommunicator(fmsCommunicator).build().convert();
					segmentList.add(sInfo);
				}
				List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);
				bookingTripMap.put(entry.getKey(), tripList);
			}
		}
		return bookingTripMap;
	}

	public Map<String, ProcessedItemDetails> getProcessDetails(List<DbAirOrderItem> items,
			Map<String, User> bookingUsers) {
		Map<String, ProcessedItemDetails> processDetailsMap = new LinkedHashMap<>();
		Map<String, List<DbAirOrderItem>> itemMap = items.stream().collect(Collectors
				.groupingBy(DbAirOrderItem::getBookingId, LinkedHashMap::new, Collectors.toCollection(ArrayList::new)));

		for (Map.Entry<String, List<DbAirOrderItem>> entry : itemMap.entrySet()) {
			ProcessedAirDetails ad = null;
			List<SegmentInfo> segmentList = new ArrayList<>();
			List<DbAirOrderItem> sortedList = entry.getValue().stream()
					.sorted((aO1, aO2) -> aO1.getId().compareTo(aO2.getId())).collect(Collectors.toList());

			SearchType type = SearchType.ONEWAY;
			for (DbAirOrderItem item : sortedList) {
				if ((!segmentList.isEmpty() && item.getAdditionalInfo().getSegmentNo() > 0)
						|| (item.getAdditionalInfo().getSegmentNo() == 0)) {
					SegmentInfo sInfo = AirOrderItemToSegmentInfoMapper.builder().item(item)
							.fmsCommunicator(fmsCommunicator).build().convert();
					if (BooleanUtils.isTrue(item.getAdditionalInfo().getIsReturnSegment()))
						type = SearchType.RETURN;
					segmentList.add(sInfo);
					ad = new ProcessedAirDetails();
					AirType airType = AirType.getEnumFromCode(item.getAdditionalInfo().getType());
					if (airType == null) {
						ProductMetaInfo metaInfo = Product.getProductMetaInfoFromId(item.getBookingId());
						airType = AirType.getEnumFromCode(metaInfo.getSubProduct());
					}
					ad.setAirType(airType);
					List<ProcessedFlightTravellerInfo> travellerInfo =
							AirBookingUtils.generateProcessedTravellerInfo(segmentList, 0);
					ad.getTravellerInfos().addAll(travellerInfo);
				}
			}
			List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);
			if (!tripList.isEmpty()) {
				ad.setProcessedTripInfos(new ArrayList<>());
				for (TripInfo tripInfo : tripList) {
					ad.getProcessedTripInfos().add(fmsCommunicator.getProcessTripInfo(tripInfo));
				}
				if (tripList.size() > 1 && type != SearchType.RETURN)
					type = SearchType.MULTICITY;
				ad.setSearchType(type);
				ad.setDiscount(AirBookingUtils.calculateDiscount(sortedList, bookingUsers.get(entry.getKey())));
				processDetailsMap.put(entry.getKey(), ad);
			}
		}
		return processDetailsMap;
	}

	@Override
	public void processBooking(String bookingId, Order order, PaymentStatus paymentStatus, double paymentFee) {
		List<DbAirOrderItem> airItems = itemService.findByBookingId(bookingId);
		AirItemStatus itemStatus;
		if (PaymentStatus.SUCCESS.equals(paymentStatus)) {
			itemStatus = AirItemStatus.PAYMENT_SUCCESS;
			AirBookingUtils.updatePaymentFee(airItems, order, paymentFee);
		} else {
			itemStatus = AirItemStatus.PAYMENT_FAILED;
			order.setReason(SystemError.PAYMENT_FAILED.getMessage());
		}
		order.getAdditionalInfo().setPaymentStatus(paymentStatus);
		this.save(airItems, order, itemStatus);
		List<TripInfo> tripInfos = findTripsByBookingIds(Arrays.asList(bookingId)).get(bookingId);
		if (isConfirmBooking(tripInfos)) {
			fmsCommunicator.doConfirmBooking(tripInfos, order);
		} else {
			AirReviewResponse airReviewResponse = fmsCommunicator.getAirReviewResponse(null, bookingId);
			order.getAdditionalInfo().setTripId(airReviewResponse.getTripId());
			log.info("Trip id {}  for booking Id {}", airReviewResponse.getTripId(), airReviewResponse.getBookingId());
			List<SegmentInfo> segmentList = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(airItems,
					BookingUtils.getSegmentInfos(airReviewResponse.getTripInfos()));
			List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);
			fmsCommunicator.doBooking(tripList, order);
		}

	}

	private boolean isConfirmBooking(List<TripInfo> tripInfos) {
		AtomicBoolean isConfirmBook = new AtomicBoolean(false);
		tripInfos.forEach(tripInfo -> {
			if (tripInfo.hasPNR()) {
				isConfirmBook.set(Boolean.TRUE);
			}
		});
		return isConfirmBook.get();
	}

	public List<ProcessedBookingDetail> getProcessedItemDetails(OrderFilter orderFilter) {
		Set<String> userIds = new HashSet<>();
		Set<String> bookingIds = new HashSet<>();
		List<ProcessedBookingDetail> pbds = new ArrayList<>();
		LogUtils.log("getProcessedItemDetails#findTripsByOrderFilter#start",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), "system#filter");
		List<Object[]> orderObjects = findTripsByOrderFilter(orderFilter);
		LogUtils.log("getProcessedItemDetails#findTripsByOrderFilter#end",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"getProcessedItemDetails#findTripsByOrderFilter#start");

		List<DbAirOrderItem> dbAirOrderItems = new ArrayList<>();

		LogUtils.log("getProcessedItemDetails#processing#start",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		Map<String, String> bookingUserIds = new HashMap<>();
		for (Object[] obj : orderObjects) {
			dbAirOrderItems.add((DbAirOrderItem) obj[0]);
			ProcessedOrder pOrder = new GsonMapper<>(obj[1], ProcessedOrder.class).convert();
			if (bookingIds.add(pOrder.getBookingId())) {
				bookingUserIds.put(pOrder.getBookingId(), pOrder.getBookingUserId());
				pbds.add(ProcessedBookingDetail.builder().order(pOrder).build());
				userIds.add(pOrder.getBookingUserId());
				userIds.add(pOrder.getLoggedInUserId());
			}
		}

		Map<String, User> bookingUsers = getBookingUsers(bookingUserIds);
		Map<String, ProcessedItemDetails> processItemDetails = getProcessDetails(dbAirOrderItems, bookingUsers);

		pbds.forEach(pbd -> {
			try {
				if (processItemDetails.get(pbd.getOrder().getBookingId()) != null) {
					pbd.getOrder()
							.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(),
									Order.builder().status(pbd.getOrder().getStatus())
											.additionalInfo(pbd.getOrder().getAdditionalInfo()).build()));
					pbd.getOrder()
							.setDiscount(((ProcessedAirDetails) processItemDetails.get(pbd.getOrder().getBookingId()))
									.getDiscount());
					pbd.getOrder().setNetFare(
							pbd.getOrder().getAmount() - ObjectUtils.firstNonNull(pbd.getOrder().getDiscount(), 0d));
					pbd.getItems().put(OrderType.AIR.name(), processItemDetails.get(pbd.getOrder().getBookingId()));
				} else {
					log.debug("[OrderListing] Order booking id is :", pbd.getOrder().getBookingId());
					pbd.setOrder(null);
				}
			} catch (Exception e) {
				log.error("Unable to parse booking information for bookingId {}", pbd.getOrder().getBookingId(), e);
			}

		});

		LogUtils.log("getProcessedItemDetails#processing#end",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"getProcessedItemDetails#processing#start");

		return pbds;
	}

	private Map<String, User> getBookingUsers(Map<String, String> bookingUserIds) {
		Map<String, User> bookingUsers = new HashMap<>();
		for (Entry<String, String> entry : bookingUserIds.entrySet()) {
			bookingUsers.put(entry.getKey(), userService.getUserFromCache(entry.getValue()));
		}
		return bookingUsers;
	}

	public DbAirOrderItem findByAirOrderItemId(Long id) {
		DbAirOrderItem item = itemService.findById(id);
		if (item == null) {
			log.error("No airOrderItems found for id {}", id);
			throw new RuntimeException();
		}
		return item;
	}

	public AirItemStatus getAirItemStatus(AirOrderItem airOrderItem, Order order) {
		AirItemStatus currentStatus = AirItemStatus.getAirItemStatus(airOrderItem.getStatus());
		PaymentStatus paymentStatus = getOrderPaymentStatus(airOrderItem, order);
		AirItemStatus nextStatus = currentStatus;
		boolean isAllPaxHasPNR = airOrderItem.hasPNRForAllPNR();
		boolean isAllPaxHasTicketNumber = airOrderItem.hasTicketNumberForAllPax();
		if (PaymentStatus.SUCCESS.equals(paymentStatus)) {
			AirlineInfo airlineInfo = fmsCommunicator.getAirlineInfo(airOrderItem.getAirlinecode());
			if (airlineInfo.getIsTkRequired()) {
				if (isAllPaxHasPNR) {
					if (isAllPaxHasTicketNumber) {
						nextStatus = AirItemStatus.SUCCESS;
					} else {
						nextStatus = AirItemStatus.CONFIRM_PENDING;
					}
				} else {
					nextStatus = AirItemStatus.PNR_PENDING;
				}
			} else {
				if (isAllPaxHasPNR && !airlineInfo.getIsTkRequired()) {
					// Case to handle some LCC Airlines not required ticket number
					nextStatus = AirItemStatus.SUCCESS;
				}
			}
		} else {
			if (isAllPaxHasPNR) {
				nextStatus = AirItemStatus.ON_HOLD;
			} else {
				nextStatus = AirItemStatus.HOLD_PENDING;
			}
		}
		return nextStatus;
	}

	/**
	 * @param airOrderItem
	 * @return PaymentStatus
	 * @implSpec : This method return order payment status Success or Failure)
	 */
	public PaymentStatus getOrderPaymentStatus(AirOrderItem airOrderItem, Order order) {
		if (order == null) {
			order = orderManager.findByBookingId(airOrderItem.getBookingId(), null);
		}
		OrderAdditionalInfo additionalInfo = order.getAdditionalInfo();
		if (additionalInfo != null && additionalInfo.getPaymentStatus() != null) {
			return additionalInfo.getPaymentStatus();
		}
		return null;
	}

	public AirlineInfo getAirlineInfo(String airlineCode) {
		return fmsCommunicator.getAirlineInfo(airlineCode);
	}

	/**
	 * @implSpec : As After completing the order, yet booking is not success, make order to PENDING more scenarious are
	 *           required for making airorder status as well.
	 */
	public List<SegmentInfo> checkAndUpdateOrderStatus(List<SegmentInfo> segmentInfos, Order order) {
		if (order.getStatus().equals(OrderStatus.IN_PROGRESS)) {
			order.setStatus(OrderStatus.PENDING);
			orderManager.save(order);
			httpReqToSendEmail(order);
			msgHandler.sendBookingSms(order, SmsTemplateKey.BOOKING_PENDING_SMS);
		}
		return segmentInfos;
	}

	private void addOrderToAnalytics(Order order) {
		try {
			if (ANALYTICS_ORDER_STATUS.contains(order.getStatus())) {
				List<DbAirOrderItem> items = itemService.findByBookingId(order.getBookingId());
				List<SegmentInfo> segmentList = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(items, null);
				List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);
				List<Note> notes = gmsComm.getNotes(NoteFilter.builder().bookingId(order.getBookingId()).build());
				fmsCommunicator.addBookToAnalytics(order, tripList, null, AirBookingUtils.getNoteMessage(notes),
						AirAnalyticsType.BOOK_TRIP, null);
			}
		} catch (Exception e) {
			log.error("Failed in Air Analytics {} cause ", order.getBookingId(), e);
		}
	}

	public Map<String, LocalDateTime> statusfirstUpdateTime(Order order) {
		OrderStatus status = order.getStatus();
		Map<String, LocalDateTime> firstTxnTime = order.getAdditionalInfo().getFirstUpdateTime();
		if (OrderStatus.SUCCESS.equals(status) || OrderStatus.PENDING.equals(status)
				|| OrderStatus.ABORTED.equals(status)) {
			if (firstTxnTime != null && firstTxnTime.get(status.getCode()) == null)
				firstTxnTime.put(status.getCode(), LocalDateTime.now());
		}
		return firstTxnTime;
	}

	public Order getOrderByBookingId(String bookingId) {
		return orderManager.findByBookingId(bookingId, null);
	}


	public List<AirOrderItem> getAuditEntries(String bookingId) {
		AuditsRequest request = new AuditsRequest();
		request.setId(bookingId);
		List<AuditResult> auditResults = auditsHandler.fetchAudits(request, DbAirOrderItem.class, "bookingId", true);
		List<AirOrderItem> airOrderItems = new ArrayList<>();
		auditResults.forEach(x -> {
			AirOrderItem airOrderItem = ((DbAirOrderItem) x.getActualValue()).toDomain();
			airOrderItems.add(airOrderItem);
		});
		airOrderItems.forEach(x -> log.info("AirOrderItem SSH: " + new Gson().toJson(x)));
		return airOrderItems;
	}
}
