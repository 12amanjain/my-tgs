package com.tgs.services.oms.manager.hotel;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderDisputeInfo;
import com.tgs.services.oms.datamodel.OrderDisputeStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restmodel.hotel.HotelBookingDisputeRequest;
import com.tgs.services.oms.servicehandler.hotel.HotelOrderMessageHandler;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
public class HotelBookingDisputeManager {

	private HotelBookingDisputeRequest disputeRequest;
	private Order order;
	private List<DbHotelOrderItem> orderItems;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private HotelOrderMessageHandler messageHandler;

	public void raiseDispute() {
		try {
			List<OrderDisputeInfo> disputeInfos = order.getAdditionalInfo().getDisputeInfo();
			if (CollectionUtils.isEmpty(disputeInfos)) {
				disputeInfos = new ArrayList<>();
				disputeRequest.getDisputeInfo().setDisputeRaiseTime(LocalDateTime.now());
				disputeInfos.add(disputeRequest.getDisputeInfo());
				order.getAdditionalInfo().setDisputeInfo(disputeInfos);
			} else {
				disputeInfos.add(disputeRequest.getDisputeInfo());
			}
			orderManager.saveWithoutProcessedOn(order);
			messageHandler.sendBookingEmail(order, orderItems, EmailTemplateKey.HOTEL_DISPUTE_RAISE_EMAIL,
					Arrays.asList(disputeRequest.getEmail()));
		} catch (Exception e) {

			log.error("Unable to raise dispute {}", order.getBookingId(), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	public void resolveDispute() {
		try {
			List<OrderDisputeInfo> disputeInfos = order.getAdditionalInfo().getDisputeInfo();
			if (CollectionUtils.isEmpty(disputeInfos)) {
				throw new CustomGeneralException(SystemError.DISPUTE_CANNOT_BE_RESOLVED);
			}
			OrderDisputeInfo disputeInfo = disputeInfos.get(disputeInfos.size() - 1);
			disputeInfo.setDisputeResolveTime(LocalDateTime.now());
			disputeInfo.setDisputeStatus(OrderDisputeStatus.RESOLVED);
			orderManager.saveWithoutProcessedOn(order);
		} catch (Exception e) {

			log.error("Unable to resolve dispute {}", order.getBookingId(), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}
}
