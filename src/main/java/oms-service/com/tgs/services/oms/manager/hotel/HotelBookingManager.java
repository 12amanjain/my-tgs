package com.tgs.services.oms.manager.hotel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.PointsServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSSR;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.mapper.RoomInfoToDbHotelOrderItemMapper;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.hotel.HotelBookingUtils;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.points.datamodel.HotelRedeemPointData;
import com.tgs.services.points.restmodel.HotelRedeemPointResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.vms.restmodel.HotelVoucherValidateRequest;
import com.tgs.services.vms.restmodel.HotelVoucherValidateResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Getter
@Setter
public class HotelBookingManager extends BookingManager {

	private HotelBookingRequest hotelBookingRequest;
	private HotelInfo hInfo;
	protected List<DbHotelOrderItem> items;

	@Autowired
	protected HotelOrderItemManager itemManager;

	@Autowired
	HMSCommunicator hmsComm;

	@Autowired
	protected HotelOrderItemService itemService;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	private UserServiceCommunicator userService;
	
	@Autowired
	private PointsServiceCommunicator pointsService;

	// @Transactional
	public Order processBooking() {
		hotelBookingRequest = (HotelBookingRequest) bookingRequest;
		this.storeGstInfo();
		this.calcRewardpoints();
		this.applyVoucher();
		this.storeOrderItem();
		this.createOrder();
		this.addSSRToNotes();
		this.addOrderToAudit(AuditAction.ORDER_CREATE);
		log.info("Orderitems successfully stored for bookingId {}", hotelBookingRequest.getBookingId());
		this.doPayment();
		return order;
	}

	protected void storeOrderItem() {
		items = new ArrayList<>();
		int i = 0;
		paymentTotal = 0d;
		List<RoomInfo> roomInfoList = hInfo.getOptions().get(0).getRoomInfos();
		for (RoomInfo roomInfo : roomInfoList) {
			DbHotelOrderItem hotelItem = RoomInfoToDbHotelOrderItemMapper.builder().roomInfo(roomInfo)
					.roomTravellerInfo(hotelBookingRequest.getRoomTravellerInfo().get(i).getTravellerInfo()).index(i)
					.bookingId(hotelBookingRequest.getBookingId()).hInfo(hInfo)
					.roomSSR(hotelBookingRequest.getRoomSSR()).bookingUser(bookingUser).build().convert();
			this.storeOrderItemMarkupInfo(roomInfo, hotelItem);
			items.add(hotelItem);
			paymentTotal += HotelBookingUtils.getPaymentTotal(Arrays.asList(hotelItem), bookingUser);
			orderTotal += hotelItem.getAmount();
			i++;
		}
		itemManager.save(items, null, HotelItemStatus.IN_PROGRESS);
	}

	protected void storeOrderItemMarkupInfo(RoomInfo roomInfo, DbHotelOrderItem hotelItem) {
		if (CollectionUtils.isNotEmpty(roomInfo.getPerNightPriceInfos())) {
			Double orderItemMarkup = roomInfo.getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
				Map<HotelFareComponent, Double> fareComponent = priceInfo.getFareComponents();
				if (fareComponent.get(HotelFareComponent.MU) != null)
					return fareComponent.get(HotelFareComponent.MU);
				else
					return 0;
			}).sum();
			hotelItem.setMarkup(orderItemMarkup);
			this.totalMarkup += orderItemMarkup;
		}
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		HotelItemStatus itemStatus = orderStatus.equals(OrderStatus.PAYMENT_SUCCESS) ? HotelItemStatus.PAYMENT_SUCCESS
				: HotelItemStatus.PAYMENT_FAILED;
		itemManager.save(items, order, itemStatus);
	}

	private void addSSRToNotes() {
		List<RoomSSR> roomSSR = hotelBookingRequest.getRoomSSR();
		String stringSSR = "";
		if (CollectionUtils.isNotEmpty(roomSSR)) {
			for (RoomSSR ssr : roomSSR) {
				stringSSR = String.join(",", GsonUtils.getGson().toJson(ssr));
			}
			if (StringUtils.isNotBlank(stringSSR)) {
				Note note = Note.builder().bookingId(hotelBookingRequest.getBookingId())
						.noteType(NoteType.SPECIAL_SERVICE_REQUEST).noteMessage(stringSSR).build();
				gmsCommunicator.addNote(note);
			}
		}
	}

	public void checkDuplicateBooking(HotelInfo hInfo, List<OrderStatus> failureStatus, String bookingId,
			ContextData contextData, DeliveryInfo deliveryInfo, List<RoomTravellerInfo> roomTravellerInfo) {
		User bookingUser = userService.getUserFromCache(this.bookingUser.getParentUserId());
		Map<String, Integer> keysWithTtl = BaseHotelUtils.generateTravellerKeyWithTTL(hInfo, contextData, deliveryInfo,
				roomTravellerInfo, bookingUser);
		OrderUtils.checkDuplicateBooking(keysWithTtl, bookingId, failureStatus, orderManager,
				hmsComm.getHotelConfigRule(HotelConfiguratorRuleType.GNPURPOSE), null);
	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		return paymentInfos;
	}

	@Override
	protected void calcRewardpoints() {
		// Debit
		Optional<PaymentRequest> paymentRq = getHotelBookingRequest().getPaymentRequestOnMedium(PaymentMedium.POINTS);
		if (paymentRq.isPresent()) {
			PaymentRequest debitPaymentRq = paymentRq.get();
			log.info("Payment request nop {}, additional info nop {}", debitPaymentRq.getNoOfPoints(),
					debitPaymentRq.getAdditionalInfo().getNoOfPoints());
			Double totalPoints = ObjectUtils.firstNonNull(debitPaymentRq.getNoOfPoints(),
					debitPaymentRq.getAdditionalInfo().getNoOfPoints());
			HotelRedeemPointData debitPointRequest = new HotelRedeemPointData();
			debitPointRequest.setHotelInfo(hInfo);
			debitPointRequest.setBookingId(bookingRequest.getBookingId());
			debitPointRequest.setProduct(Product.HOTEL);
			debitPointRequest.setOpType(PaymentOpType.DEBIT);
			debitPointRequest.setPoints(totalPoints);
			debitPaymentRq.getAdditionalInfo().setPointsType(PointsType.COIN);
			debitPointRequest.setType(PointsType.COIN);
			HotelRedeemPointResponse pointResponse = pointsService.validateHotelRewardPoints(debitPointRequest, true);
			if (Objects.nonNull(pointResponse.getHotelInfo())
					&& CollectionUtils.isEmpty(pointResponse.getErrors())) {
				hInfo = pointResponse.getHotelInfo();
				debitPaymentRq.setAmount(pointResponse.getAmount());
				debitPaymentRq.setNoOfPoints(totalPoints);
				log.info("Point Converted to Amount for booking {} and amount {}", bookingRequest.getBookingId(),
						debitPaymentRq.getAmount());
			}
		}
	}

	@Override
	protected void applyVoucher() {
		if (isVoucherCodeInPayment()) {
			try {
				PaymentRequest payment = super.getVoucherCodeFromRequest();
				List<PaymentMedium> mediumList = bookingRequest.getPaymentInfos().stream()
						.map(pm -> pm.getPaymentMedium()).collect(Collectors.toList());
				HotelVoucherValidateRequest voucherRq = new HotelVoucherValidateRequest(bookingRequest.getBookingId(),
						payment.getAdditionalInfo().getVoucherCode(), Product.HOTEL, mediumList, null);
				voucherRq.setHotelInfo(hInfo);
				HotelVoucherValidateResponse response = voucherService.applyHotelVoucher(voucherRq, true);
				if (response.getDiscountedAmount() > 0 && Objects.nonNull(response.getHotelInfo())) {
					hInfo = response.getHotelInfo();
				}
			} catch (CustomGeneralException e) {
				log.error("Unable to apply voucher code for booking id {}", bookingRequest.getBookingId(), e);
				throw e;
			} catch (Exception e) {
				log.error("Failed to apply voucher code for booking id {}", bookingRequest.getBookingId(), e);
				throw new CustomGeneralException(SystemError.VOUCHER_CODE_NOT_AVAILABLE);
			}
		}
	}
}
