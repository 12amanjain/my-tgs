package com.tgs.services.oms.manager.hotel;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.BookingManager;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Getter;

@Qualifier("HotelConfirmBookingManager")
@Getter
@Service
public class HotelConfirmBookingManager extends BookingManager {

	private BookingRequest confirmBookingRequest;
	private List<DbHotelOrderItem> items;

	@Autowired
	private HotelOrderItemManager itemManager;

	public Order processBooking() {
		confirmBookingRequest = bookingRequest;
		order = orderManager.findByBookingId(confirmBookingRequest.getBookingId(), null);
		if (!order.getStatus().equals(OrderStatus.ON_HOLD)) {
			throw new CustomGeneralException(SystemError.ORDER_CONFIRMATION_FAILED,
					"Order Can't Be Confirmed as it is not in ON_HOLD status");
		}
		items = itemManager.getItemList(confirmBookingRequest.getBookingId());
		totalMarkup = order.getMarkup();
		orderTotal = order.getAmount();
		this.doPayment();
		this.addOrderToAudit(AuditAction.ORDER_CONFIRM);
		return order;
	}

	@Override
	public void updateItemStatus(OrderStatus orderStatus) {
		HotelItemStatus itemStatus = orderStatus.equals(OrderStatus.PAYMENT_SUCCESS) ? HotelItemStatus.PAYMENT_SUCCESS
				: HotelItemStatus.PAYMENT_FAILED;
		itemManager.save(items, order, itemStatus);

	}

	@Override
	protected List<PaymentRequest> createVirualPaymentRequest(List<PaymentRequest> paymentInfos,
			GstInfo orderBillingEntity) {
		return paymentInfos;
	}

	@Override
	protected void applyVoucher() {

	}

}
