package com.tgs.services.oms.manager.hotel;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.datamodel.ProcessedItemDetails;
import com.tgs.services.oms.datamodel.ProcessedOrder;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.ProcessedHotelDetails;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.AbstractInvoiceIdManager;
import com.tgs.services.oms.manager.ItemManager;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.mapper.RoomInfoToDbHotelOrderItemMapper;
import com.tgs.services.oms.servicehandler.hotel.HotelOrderActionValidator;
import com.tgs.services.oms.servicehandler.hotel.HotelOrderMessageHandler;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelOrderItemManager implements ItemManager {

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private UserServiceCommunicator userSrvComm;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	HotelOrderMessageHandler messageHandler;

	@Autowired
	private HotelOrderActionValidator actionValidator;

	@Autowired
	private AbstractInvoiceIdManager invoiceIdManager;

	private static HMSCommunicator hmsCommunicator;

	public static void init(HMSCommunicator hmsCommunicator) {
		HotelOrderItemManager.hmsCommunicator = hmsCommunicator;
	}

	private OrderStatus getOrderStatus(String bookingId, List<DbHotelOrderItem> items) {
		HotelItemStatus itemStatus = HotelItemStatus.getHotelItemStatus(items.get(0).getStatus());
		boolean hasAllItemSameStatus = true;
		OrderStatus status = OrderStatus.PENDING;
		for (DbHotelOrderItem item : items) {
			if (!item.getStatus().equals(itemStatus.getCode())) {
				hasAllItemSameStatus = false;
			}
			if (item.getStatus().equals(HotelItemStatus.PENDING.getCode())) {
				status = OrderStatus.PENDING;
				break;
			}
		}

		if (hasAllItemSameStatus) {
			status = itemStatus.getOrderStatus();
		}

		log.info("New Order status for bookingId {}  is status {}", bookingId, status);
		return status;
	}

	public void save(List<DbHotelOrderItem> items, Order order, HotelItemStatus itemStatus) {
		for (DbHotelOrderItem item : items) {
			save(item, order, itemStatus);
		}
		if (order != null) {
			order = orderManager.findByBookingId(order.getBookingId(), order);
			updateOrderStatus(order, items);
		}
	}

	private DbHotelOrderItem save(DbHotelOrderItem item, Order order, HotelItemStatus newStatus) {
		HotelItemStatus currentStatus = HotelItemStatus.getHotelItemStatus(item.getStatus());
		if (newStatus != null && currentStatus.nextStatusSet().contains(newStatus)) {
			item.setStatus(newStatus.getCode());
		}
		itemService.save(item);
		return item;
	}
	
	public String getCcEmails(Order order, List<DbHotelOrderItem> items) {

		HotelConfiguratorInfo configOutput = hmsCommunicator.getHotelConfigRule(HotelConfiguratorRuleType.GNPURPOSE, null);
		HotelGeneralPurposeOutput configMap = (HotelGeneralPurposeOutput) configOutput.getOutput();
		Map<String, String> supplierEmailMap = configMap.getCcEmails();
		StringJoiner ccEmails = new StringJoiner(",");
		if (MapUtils.isNotEmpty(supplierEmailMap)) {
			String supplierName = items.get(0).getSupplierId();
			if (StringUtils.isNotBlank(supplierEmailMap.get(supplierName)))
				ccEmails.add(supplierEmailMap.get(supplierName));
		}

		// Get the User Sales relation Email Id
		List<String> bookingUserId = Arrays.asList(order.getBookingUserId());
		Map<String, Set<User>> salesHierarchy = userSrvComm.getUserRelations(bookingUserId); 
		if (salesHierarchy != null) {
			Set<User> userRelations = salesHierarchy.get(order.getBookingUserId());
			if (userRelations != null) {
				User u = userSrvComm.getUserFromCache(userRelations.stream().findFirst().get().getUserId());
				ccEmails.add(u.getEmail());
			}
		}

		User user = userSrvComm.getUserFromCache(order.getBookingUserId());
		ccEmails.add(OrderUtils.getToEmailId(order, user, null));
		return ccEmails.toString();
	}
	
	

	public void updateOrderStatus(Order order, List<DbHotelOrderItem> items) {
		OrderStatus oldOrderStatus = order.getStatus();
		OrderStatus status = getOrderStatus(order.getBookingId(), items);
		order.setStatus(status);
		String ccEmails = getCcEmails(order, items);
		order.getAdditionalInfo().setIsReconciledWithSupplier(false);
		order = orderManager.save(order);
		if (oldOrderStatus != status) {

			if (OrderStatus.SUCCESS.equals(status)) {
				orderManager.updateLastTransactionTime(Product.HOTEL, order.getBookingUserId());
				invoiceIdManager.setInvoiceIdFor(order, Product.getProductMetaInfoFromId(order.getBookingId()));
				/**
				 * to avoid any other impact
				 */
				order = orderManager.save(order);
			}
			try {
				if (order.getStatus().equals(OrderStatus.SUCCESS)) {
					notifyForSpecialRequestIfPresent(order, items);
					messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_SUCCESS_SMS, null);
				} else if (order.getStatus().equals(OrderStatus.FAILED)) {
					messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_FAIL_EMAIL,
							Arrays.asList(ccEmails));
					messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_FAILURE_SMS, null);
				} else if (order.getStatus().equals(OrderStatus.ON_HOLD)) {
					messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_HOLD_EMAIL,
							Arrays.asList(ccEmails));
					messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_HOLD_SMS, null);
					notifyForSpecialRequestIfPresent(order, items);
				} else if (order.getStatus().equals(OrderStatus.ABORTED)) {
					messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_ABORT_EMAIL,
							Arrays.asList(ccEmails));
				} else if (order.getStatus().equals(OrderStatus.PENDING)) {
					messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_PENDING_EMAIL,
							Arrays.asList(ccEmails));
					messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_PENDING_SMS, null);
				} else if (order.getStatus().equals(OrderStatus.CANCELLED)) {
					messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_CANCELLATION_EMAIL,
							Arrays.asList(ccEmails));
					messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_CANCELLATION_SMS, null);
				} else if (order.getStatus().equals(OrderStatus.UNCONFIRMED)) {
					messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_BOOKING_ABORT_EMAIL,
							Arrays.asList(ccEmails));
					messageHandler.sendBookingSms(order, items, SmsTemplateKey.HOTEL_BOOKING_CANCELLATION_SMS, null);
				}
			} catch (Exception e) {
				log.error("Unable to send email for booking id {}", order.getBookingId(), e);
			}
		}
	}

	public void notifyForSpecialRequestIfPresent(Order order, List<DbHotelOrderItem> items) {

		if (CollectionUtils.isNotEmpty(items.get(0).getRoomInfo().getRoomSSR())) {
			messageHandler.sendBookingEmail(order, items, EmailTemplateKey.HOTEL_SPECIAL_COMMENT_EMAIL, null);
		}
	}

	public void updateOrderItem(HotelInfo hInfo, Order order, HotelItemStatus itemStatus) {
		List<DbHotelOrderItem> items = null;
		int i = 0;
		items = itemService.findByBookingIdOrderByIdAsc(order.getBookingId());
		User bookingUser = userSrvComm.getUserFromCache(order.getBookingUserId());
		for (DbHotelOrderItem dbItem : items) {
			RoomInfo roomInfo = hInfo.getOptions().get(0).getRoomInfos().get(i);
			dbItem = RoomInfoToDbHotelOrderItemMapper.builder().roomInfo(roomInfo).bookingUser(bookingUser).hInfo(hInfo)
					.index(i).build().setOutput(dbItem).convert();
			if (itemStatus != null)
				dbItem.setStatus(itemStatus.getCode());
			itemService.save(dbItem);
			i++;
		}
		updateOrderStatus(order, items);
	}

	public void updateOrderItemStatus(Order order, HotelItemStatus itemStatus) {
		List<DbHotelOrderItem> items = null;
		items = itemService.findByBookingIdOrderByIdAsc(order.getBookingId());
		for (DbHotelOrderItem dbItem : items) {
			if (itemStatus != null)
				dbItem.setStatus(itemStatus.getCode());
			itemService.save(dbItem);
		}
		updateOrderStatus(order, items);
	}

	public List<DbHotelOrderItem> getItemList(String bookingId) {
		return itemService.findByBookingIdOrderByIdAsc(bookingId);
	}

	public List<ProcessedBookingDetail> getProcessedItemDetails(OrderFilter orderFilter) {

		List<DbHotelOrder> hotelOrderList = getHotelOrderList(orderFilter);
		if (hotelOrderList == null) {
			return new ArrayList<>();
		}
		List<ProcessedBookingDetail> pbds = new ArrayList<>();
		for (DbHotelOrder hotelOrder : hotelOrderList) {
			ProcessedOrder pOrder = new GsonMapper<>(hotelOrder.getOrder(), ProcessedOrder.class).convert();
			List<DbHotelOrderItem> itemList = hotelOrder.getOrderItems();
			pbds.add(ProcessedBookingDetail.builder().order(pOrder).items(getProcessedItemMap(itemList)).build());
		}

		updateActionList(pbds);
		return pbds;
	}

	public void updateActionList(List<ProcessedBookingDetail> pbds) {

		pbds.forEach(pbd -> {
			try {
				if (pbd.getOrder() != null) {
					pbd.getOrder()
							.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(),
									Order.builder().status(pbd.getOrder().getStatus())
											.additionalInfo(pbd.getOrder().getAdditionalInfo()).build()));
				}
			} catch (Exception e) {
				log.error("Unable to parse booking information for bookingId {}", pbd.getOrder().getBookingId(), e);
			}
		});

	}

	public List<DbHotelOrder> getHotelOrderList(OrderFilter orderFilter) {

		List<Object[]> orderList = findInfoByOrderFilter(orderFilter);
		if (orderList == null || orderList.size() == 0) {
			return null;
		}

		Map<String, DbOrder> orderMap = new HashMap<>();
		List<DbHotelOrderItem> orderItemList = new ArrayList<DbHotelOrderItem>();
		List<DbOrder> orders = new ArrayList<>();
		for (Object[] object : orderList) {
			DbOrder order = (DbOrder) object[1];
			if (orderMap.isEmpty() || (!orderMap.isEmpty() && !orderMap.containsKey(order.getBookingId()))) {
				orders.add(order);
			}
			orderMap.put(order.getBookingId(), order);
			orderItemList.add((DbHotelOrderItem) object[0]);
		}
		Map<String, List<DbHotelOrderItem>> itemsMap =
				orderItemList.stream().collect(Collectors.groupingBy(DbHotelOrderItem::getBookingId));
		List<DbHotelOrder> hotelOrderList = new ArrayList<DbHotelOrder>(orderMap.size());

		for (DbOrder order : orders) {
			List<DbHotelOrderItem> dbOrderItems = itemsMap.get(order.getBookingId());
			for (int i = 1; i < dbOrderItems.size(); i++) {
				dbOrderItems.get(i).setAdditionalInfo(dbOrderItems.get(0).getAdditionalInfo());
			}
			DbHotelOrder hotelOrder = DbHotelOrder.builder().order(order).orderItems(dbOrderItems).build();
			hotelOrderList.add(hotelOrder);
		}

		return hotelOrderList;
	}

	public Map<String, ProcessedItemDetails> getProcessedItemMap(List<DbHotelOrderItem> itemList) {
		ProcessedItemDetails processedItemDetails = getProcessedItem(itemList);
		Map<String, ProcessedItemDetails> map = new HashMap<>();
		map.put(OrderType.HOTEL.getName(), processedItemDetails);
		return map;

	}

	public ProcessedItemDetails getProcessedItem(List<DbHotelOrderItem> dbOrderItems) {

		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(dbOrderItems).build().convert();
		HotelSearchQuery searchQuery = HotelSearchQuery.builder().checkinDate(dbOrderItems.get(0).getCheckInDate())
				.checkoutDate(dbOrderItems.get(0).getCheckOutDate()).build();
		ProcessedHotelDetails processedHotelDetails =
				ProcessedHotelDetails.builder().hInfo(hInfo).searchQuery(searchQuery).build();
		return processedHotelDetails;

	}

	public List<Object[]> findInfoByOrderFilter(OrderFilter filter) {
		return itemService.findByJsonSearch(filter);
	}

	public DbHotelOrderItem findByOrderItemId(Long id) {
		return itemService.findById(id);
	}


	@Override
	public void processBooking(String bookingId, Order order, PaymentStatus paymentStatus, double paymentFee) {

		List<DbHotelOrderItem> items = itemService.findByBookingIdOrderByIdAsc(bookingId);
		HotelInfo storedHInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
		boolean isConfirmBooking = order.getStatus().equals(OrderStatus.ON_HOLD) ? true : false;
		HotelItemStatus itemStatus;
		if (PaymentStatus.SUCCESS.equals(paymentStatus)) {

			itemStatus = HotelItemStatus.IN_PROGRESS;
			updatePaymentFee(storedHInfo, order, paymentFee);
			updateOrderItem(storedHInfo, order, itemStatus);
			if (isConfirmBooking)
				hmsCommunicator.callSupplierConfirmBook(storedHInfo, order.getBookingId());
			else {
				HotelInfo hInfo = hmsCommunicator.getHotelReviewResponse(bookingId).getHInfo();
				updateStoredHotelWithReviewHotel(storedHInfo, hInfo);
				hmsCommunicator.callSupplierBook(storedHInfo, bookingId);
			}

		} else if (!isConfirmBooking) {

			order.setReason(SystemError.PAYMENT_FAILED.getMessage());
			order.getAdditionalInfo().setPaymentStatus(paymentStatus);
			save(items, order, HotelItemStatus.PAYMENT_FAILED);

		}
	}


	private void updateStoredHotelWithReviewHotel(HotelInfo storedHInfo, HotelInfo reviewHInfo) {

		OptionMiscInfo miscInfo = reviewHInfo.getOptions().get(0).getMiscInfo();
		storedHInfo.getOptions().get(0).setMiscInfo(miscInfo);
		storedHInfo.getOptions().get(0).setId(reviewHInfo.getOptions().get(0).getId());
	}


	public void updatePaymentFee(HotelInfo hInfo, Order order, double paymentFee) {

		Map<HotelFareComponent, Double> fcs = hInfo.getOptions().get(0).getRoomInfos().get(0).getPerNightPriceInfos()
				.get(0).getAddlFareComponents().get(HotelFareComponent.TAF);
		fcs.put(HotelFareComponent.PF, paymentFee);
		order.setAmount(order.getAmount() + paymentFee);
		order.getAdditionalInfo().setPaymentStatus(PaymentStatus.SUCCESS);
	}
}
