package com.tgs.services.oms.manager.hotel;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.services.ums.datamodel.fee.AmountType;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;

@Service
public class HotelPostBookingManager {
	
	@Autowired
	HMSCommunicator hmsComm;
	
	@Autowired
	HMSCachingServiceCommunicator cachingService;
	
	public void updateNewHotelInfoInRequestForManualOrder(HotelPostBookingBaseRequest request) {
		updateNewHotelInfo(request);
		
		
		HotelInfo hInfo = request.getHInfo();
		HotelSearchQuery searchQuery = request.getSearchQuery();
		Option option = hInfo.getOptions().get(0);
		
		/*
		 * This method makes new cancellation policy . So can't be used during modify booking
		 */
		BaseHotelUtils.updateCancellationPolicyFromDeadlineDatetimeInOption(option,searchQuery);
		
	}
	
	public void updateNewHotelInfoInRequestForModifyOrder(HotelPostBookingBaseRequest request) {
		
		updateNewHotelInfo(request);
		HotelInfo hInfo = request.getHInfo();
		HotelSearchQuery searchQuery = request.getSearchQuery();
		Option option = hInfo.getOptions().get(0);
		
		LocalDateTime oldCurrentTime = null;
		if(option.getCancellationPolicy() != null 
				&& CollectionUtils.isNotEmpty(option.getCancellationPolicy().getPenalyDetails())) {
			oldCurrentTime = option.getCancellationPolicy().getPenalyDetails().get(0).getFromDate();
		}else oldCurrentTime = LocalDateTime.now();
		
		BaseHotelUtils.updateCancellationPolicyFromDeadlineDatetimeInOption(option,searchQuery);
		option.getCancellationPolicy().getPenalyDetails().get(0).setFromDate(oldCurrentTime);
		
	}
	

	private void updateNewHotelInfo(HotelPostBookingBaseRequest request) {
		
		HotelInfo hInfo = request.getHInfo();
		HotelSearchQuery searchQuery = request.getSearchQuery();
		Option option = hInfo.getOptions().get(0);
		
		HotelMiscInfo hMiscInfo = HotelMiscInfo.builder().hotelBookingReference(request.getHotelBookingReference())
				.supplierBookingReference(request.getSupplierBookingId()).build();
		hInfo.setMiscInfo(hMiscInfo);
		option.setMiscInfo(OptionMiscInfo.builder().supplierId(request.getSupplier()).build());
		for(RoomInfo roomInfo : option.getRoomInfos()) {
			if(StringUtils.isBlank(roomInfo.getId())) 
				roomInfo.setId(RandomStringUtils.random(10, false, true));
			if(Objects.isNull(roomInfo.getCheckInDate()))
				roomInfo.setCheckInDate(searchQuery.getCheckinDate());
			if(Objects.isNull(roomInfo.getCheckOutDate()))
				roomInfo.setCheckOutDate(searchQuery.getCheckoutDate());
			roomInfo.setRoomCategory(roomInfo.getRoomType());
			boolean isPerNightSupplierPricePresent = BaseHotelUtils.isPerNightSupplierPricePresent(roomInfo);
			if(!isPerNightSupplierPricePresent) updatePerNightSupplierPriceInfo(roomInfo, request);
			if(!BaseHotelUtils.isPerNightManagementFeePresent(roomInfo)) BaseHotelUtils.updateManagementFee(roomInfo);
			else BaseHotelUtils.updateManagementFeeTax(roomInfo);
		}
		
		if(request.getTotalMarkup() != null && request.getTotalMarkup() >= 0.0)
			updateMarkUp(request);
		
		
	}

	private void updateMarkUp(HotelPostBookingBaseRequest request) {
		
		UserFeeAmount feeAmount = new UserFeeAmount();
		feeAmount.setAmountType(AmountType.FIXED.name());
		feeAmount.setValue(request.getTotalMarkup());
		hmsComm.updateUserFee(request.getHInfo(), feeAmount);
		
	}

	private void updatePerNightSupplierPriceInfo(RoomInfo roomInfo , HotelPostBookingBaseRequest request) {

		HotelFareComponent fc = roomInfo.getTotalFareComponents().containsKey(HotelFareComponent.SP) 
				? HotelFareComponent.SP : HotelFareComponent.BF;
		Double roomPrice = roomInfo.getTotalFareComponents().get(fc);
		
		
		long numberOfDays = Duration.between(roomInfo.getCheckInDate().atTime(LocalTime.NOON)
				, roomInfo.getCheckOutDate().atTime(LocalTime.NOON)).toDays();
		
		Double perNightPrice = roomPrice/numberOfDays;
		List<PriceInfo> priceInfoList = roomInfo.getPerNightPriceInfos();
		
		if(CollectionUtils.isNotEmpty(priceInfoList)) {
			for(PriceInfo priceInfo : priceInfoList) {
				priceInfo.getFareComponents().put(fc, perNightPrice);
			}
		}else updatePerNightPriceInfo(roomInfo, perNightPrice, numberOfDays, fc);
			
	}
	
	public  void updatePerNightPriceInfo(RoomInfo rInfo, Double perNightRoomSupplierPrice, long numDays, HotelFareComponent fc) {

		List<PriceInfo> priceInfoList = new ArrayList<>();
		for (int i = 1; i <= numDays; i++) {
			PriceInfo pi = new PriceInfo();
			pi.getFareComponents().put(fc, perNightRoomSupplierPrice);
			pi.setDay(i);
			priceInfoList.add(pi);
		}
		rInfo.setPerNightPriceInfos(priceInfoList);
	}
	
}
