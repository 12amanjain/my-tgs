package com.tgs.services.oms.mapper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareType;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightDesignator;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.PriceMiscInfo;
import com.tgs.services.fms.datamodel.SegmentBookingRelatedInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierBasicInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Builder
@Getter
@Setter
@Slf4j
public class AirOrderItemToSegmentInfoMapper extends Mapper<SegmentInfo> {

	private FMSCommunicator fmsCommunicator;

	private DbAirOrderItem item;
	private SegmentInfo segmentInfo;

	@Override
	protected void execute() throws CustomGeneralException {

		output = new SegmentInfo();
		output.setArrivalAirportInfo(fmsCommunicator.getAirportInfo(item.getDest())
				.setTerminal(item.getAdditionalInfo().getArrivalTerminal()));
		output.setDepartAirportInfo(fmsCommunicator.getAirportInfo(item.getSource())
				.setTerminal(item.getAdditionalInfo().getDepartureTerminal()));
		output.setFlightDesignator(FlightDesignator.builder()
				.airlineInfo(fmsCommunicator.getAirlineInfo(item.getAirlinecode())).flightNumber(item.getFlightNumber())
				.equipType(item.getAdditionalInfo().getEquipType()).build());
		output.setDepartTime(item.getDepartureTime());

		SupplierInfo supplierInfo = fmsCommunicator.getSupplierInfo(item.getSupplierId());
		if (Objects.isNull(supplierInfo)) {
			throw new CustomGeneralException(SystemError.INVALID_FBRC_SUPPLIERID,
					SystemError.INVALID_FBRC_SUPPLIERID.getMessage(item.getSupplierId()));
		}
		AdditionalAirOrderItemInfo addItemInfo = item.getAdditionalInfo();

		log.debug("Supplier rule id is {}", item.getAdditionalInfo().getSupplierRuleId());
		log.debug("Supplier id from supplier info {} ", supplierInfo.getSupplierId());
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setSupplierBasicInfo(SupplierBasicInfo.builder().ruleId(item.getAdditionalInfo().getSupplierRuleId())
				.supplierId(supplierInfo.getSupplierId()).supplierName(supplierInfo.getName())
				.sourceId(supplierInfo.getSourceId()).build());

		priceInfo.setFareDetails(getFareDetails());

		priceInfo.setMiscInfo(PriceMiscInfo.builder().accountCode(addItemInfo.getAccountCode())
				.tourCode(addItemInfo.getTourCode()).ccInfoId(addItemInfo.getCcId())
				.commericialRuleId(addItemInfo.getCommercialRuleId()).accountCode(addItemInfo.getAccountCode())
				.timeLimit(addItemInfo.getTimeLimit()).iata(addItemInfo.getIata())
				.inventoryId(addItemInfo.getInventoryId()).legNum(item.getAdditionalInfo().getLegNum()).build());

		if (StringUtils.isNotBlank(addItemInfo.getSegmentReference())) {
			priceInfo.getMiscInfo().setSegmentKey(addItemInfo.getSegmentReference());
		}
		if (StringUtils.isNotEmpty(addItemInfo.getPlatingCarrier())) {
			priceInfo.getMiscInfo().setPlatingCarrier(fmsCommunicator.getAirlineInfo(addItemInfo.getPlatingCarrier()));
		}
		if (CollectionUtils.isNotEmpty(addItemInfo.getMessages())) {
			priceInfo.setMessages(addItemInfo.getMessages());

		}
		priceInfo.getMiscInfo().setTimeLimit(item.getAdditionalInfo().getTimeLimit());
		output.getPriceInfoList().add(priceInfo);
		output.setArrivalTime(item.getArrivalTime());
		output.setDuration(item.getAdditionalInfo().getDuration());
		output.setSegmentNum(item.getAdditionalInfo().getSegmentNo());

		if (StringUtils.isNotBlank(item.getAdditionalInfo().getOperatingAirline())) {
			output.setOperatedByAirlineInfo(
					fmsCommunicator.getAirlineInfo(item.getAdditionalInfo().getOperatingAirline()));
		}
		if (item.getAdditionalInfo().getIsEticket() != null) {
			output.getFlightDesignator().getAirlineInfo()
					.setIsTkRequired(BooleanUtils.isTrue(item.getAdditionalInfo().getIsEticket()));
			output.getPriceInfo(0).getMiscInfo()
					.setIsEticket(BooleanUtils.isTrue(item.getAdditionalInfo().getIsEticket()));
		}
		output.setBookingRelatedInfo(SegmentBookingRelatedInfo.builder().travellerInfo(item.getTravellerInfo())
				.status(AirItemStatus.getAirItemStatus(item.getStatus())).build());

		if (CollectionUtils.isNotEmpty(item.getAdditionalInfo().getStopOverAirport())) {
			output.setStopOverAirports(new ArrayList<>());
			for (String code : item.getAdditionalInfo().getStopOverAirport()) {
				output.getStopOverAirports().add(fmsCommunicator.getAirportInfo(code));
			}
		}

		if (item.getAdditionalInfo().getProviderCode() != null) {
			output.getPriceInfo(0).getMiscInfo().setProviderCode(item.getAdditionalInfo().getProviderCode());
		}
		if (BooleanUtils.isTrue(item.getAdditionalInfo().getIsPrivateFare())) {
			output.getPriceInfo(0).getMiscInfo().setIsPrivateFare(item.getAdditionalInfo().getIsPrivateFare());
		}
		if (item.getAdditionalInfo().getCreditShellPNR() != null) {
			output.getPriceInfo(0).getMiscInfo().setCreditShellPNR(item.getAdditionalInfo().getCreditShellPNR());
		}
		if (BooleanUtils.isTrue(item.getAdditionalInfo().getIsUserCreditCard())) {
			output.getPriceInfo(0).getMiscInfo().setIsUserCreditCard(item.getAdditionalInfo().getIsUserCreditCard());
		}
		output.setId(String.valueOf(item.getId()));
		output.setStops(item.getAdditionalInfo().getStops());
		output.setSegmentNum(item.getAdditionalInfo().getSegmentNo());
		output.getPriceInfo(0)
				.setFareIdentifier(FareType.getEnumFromName(item.getAdditionalInfo().getFareIdentifier()));
		output.getMiscInfo().setSearchType(item.getAdditionalInfo().getSearchType());
		output.getMiscInfo().setAirType(AirType.getEnumFromCode(item.getAdditionalInfo().getType()));
		if (segmentInfo != null) {
			output.setSsrInfo(segmentInfo.getSsrInfo());
			output.setIsReturnSegment(segmentInfo.getIsReturnSegment());
			output.setMiscInfo(segmentInfo.getMiscInfo());
			if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())) {
				Long logicalFlightId = segmentInfo.getPriceInfoList().get(0).getMiscInfo().getLogicalFlightId();
				PriceMiscInfo segmentPriceMisc = segmentInfo.getPriceInfoList().get(0).getMiscInfo();
				output.getPriceInfo(0).getMiscInfo().setLogicalFlightId(logicalFlightId);
				output.getPriceInfo(0).getMiscInfo().setMealWayType(segmentPriceMisc.getMealWayType());
				output.getPriceInfo(0).getMiscInfo().setWeight(segmentPriceMisc.getWeight());
				output.getPriceInfo(0).getMiscInfo().setBaggageWayType(segmentPriceMisc.getBaggageWayType());
				output.getPriceInfo(0).getMiscInfo().setTraceId(segmentPriceMisc.getTraceId());
				output.getPriceInfo(0).getMiscInfo().setJourneyKey(segmentPriceMisc.getJourneyKey());
				output.getPriceInfo(0).getMiscInfo().setMarriageGrp(segmentPriceMisc.getMarriageGrp());
				output.getPriceInfo(0).getMiscInfo().setTokenId(segmentPriceMisc.getTokenId());
				output.getPriceInfo(0).getMiscInfo().setInventoryId(segmentPriceMisc.getInventoryId());
				output.getPriceInfo(0).getMiscInfo().setFareTypeId(segmentPriceMisc.getFareTypeId());
				output.getPriceInfo(0).getMiscInfo().setLegNum(segmentPriceMisc.getLegNum());
				output.getPriceInfo(0).getMiscInfo().setProviderCode(segmentPriceMisc.getProviderCode());
				output.getPriceInfo(0).getMiscInfo().setEffectiveDate(segmentPriceMisc.getEffectiveDate());
				output.getPriceInfo(0).getMiscInfo().setPaxPricingInfo(segmentPriceMisc.getPaxPricingInfo());
				output.getPriceInfo(0).getMiscInfo().setFareKey(segmentPriceMisc.getFareKey());
				output.getPriceInfo(0).getMiscInfo().setFareLevel(segmentPriceMisc.getFareLevel());
				output.getPriceInfo(0).getMiscInfo().setCreditShellPNR(segmentPriceMisc.getCreditShellPNR());
			}
		}
	}

	private Map<PaxType, FareDetail> getFareDetails() {
		Map<PaxType, FareDetail> map = new HashMap<>();
		item.getTravellerInfo().forEach(t -> {
			if (!map.keySet().contains(t.getPaxType())) {
				map.put(t.getPaxType(), t.getFareDetail());
			}
		});

		return map;
	}
}
