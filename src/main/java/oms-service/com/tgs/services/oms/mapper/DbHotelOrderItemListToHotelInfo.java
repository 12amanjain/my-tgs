package com.tgs.services.oms.mapper;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMiscInfo;
import com.tgs.services.hms.datamodel.Instruction;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.OptionMiscInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import lombok.Builder;

@Builder
public class DbHotelOrderItemListToHotelInfo extends Mapper<HotelInfo> {

	private List<DbHotelOrderItem> itemList;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = HotelInfo.builder().build();
		}
		HotelCancellationPolicy cancellationPolicy = null;
		List<String> roomAmenities = null;
		LocalDateTime deadlineDateTime = null;
		boolean isOptionOnRequest = false;
		Boolean isPackageRate = null;
		List<Instruction> instructions = null;
		int index = 0, i = 0;
		Double totalOptionPrice = 0.0;
		for (DbHotelOrderItem dbItem : itemList) {
			if (dbItem.getAdditionalInfo().getHInfo() != null) {
				cancellationPolicy = getCancellationPolicyFromDbItemToSaveInOption(dbItem);
				roomAmenities = dbItem.getRoomInfo().getRoomAmenities();
				deadlineDateTime = dbItem.getRoomInfo().getDeadlineDateTime();
				break;
			}
			index++;
		}
		Option option = Option.builder().build();
		List<RoomInfo> roomInfoList = new ArrayList<>();

		String supplierId = null;
		for (DbHotelOrderItem dbItem : itemList) {
			totalOptionPrice += dbItem.getAmount();
			RoomInfo room =
					GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(dbItem.getRoomInfo()), RoomInfo.class);
			BaseHotelUtils.updateRoomTotalFareComponents(room);
			isOptionOnRequest = room.getIsOptionOnRequest();
			room.setCheckInDate(dbItem.getCheckInDate());
			room.setCheckOutDate(dbItem.getCheckOutDate());
			if (cancellationPolicy != null)
				room.setCancellationPolicy(null);
			room.setRoomAmenities(roomAmenities);
			if (i == index && dbItem.getAdditionalInfo() != null && dbItem.getAdditionalInfo().getHInfo() != null) {
				output = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(dbItem.getAdditionalInfo().getHInfo()),
						HotelInfo.class);
				HotelMiscInfo miscInfo = HotelMiscInfo.builder()
						.supplierBookingId(dbItem.getAdditionalInfo().getSupplierBookingId())
						.supplierBookingReference(dbItem.getAdditionalInfo().getSupplierBookingReference())
						.hotelBookingReference(dbItem.getAdditionalInfo().getHotelBookingReference())
						.hotelBookingCancellationReference(dbItem.getAdditionalInfo().getBookingCancellationReference())
						.supplierBookingUrl(dbItem.getAdditionalInfo().getSupplierBookingUrl())
						.creditCardAppliedId(dbItem.getAdditionalInfo().getCreditCardAppliedId())
						.tempSupplierBookingId(dbItem.getAdditionalInfo().getTempSupplierBookingId())
						.isFailedFromSupplier(dbItem.getAdditionalInfo().getIsFailedFromSupplier()).build();
				if (!ObjectUtils.isEmpty(output.getOptions())) {
					instructions = output.getOptions().get(0).getInstructions();
					isPackageRate = output.getOptions().get(0).getIsPackageRate();
				}
				supplierId = dbItem.getAdditionalInfo().getSupplierId();
				if (output.getMiscInfo() == null) {
					output.setMiscInfo(miscInfo);
				} else {
					output.getMiscInfo().setSupplierBookingId(dbItem.getAdditionalInfo().getSupplierBookingId());
				}
			}
			i++;
			roomInfoList.add(room);
		}
		option.setInstructions(instructions);
		option.setIsPackageRate(isPackageRate);
		option.setRoomInfos(roomInfoList);
		option.setCancellationPolicy(cancellationPolicy == null ? option.getCancellationPolicy() : cancellationPolicy);
		option.setDeadlineDateTime(deadlineDateTime);
		option.setTotalPrice(totalOptionPrice);
		option.setRoomSSRs(itemList.get(0).getRoomInfo().getRoomSSR());
		option.setMiscInfo(OptionMiscInfo.builder().supplierId(supplierId).build());
		option.setIsOptionOnRequest(isOptionOnRequest);
		output.setOptions(new ArrayList<>(Arrays.asList(option)));
	}

	private HotelCancellationPolicy getCancellationPolicyFromDbItemToSaveInOption(DbHotelOrderItem dbItem) {
		HotelCancellationPolicy cp = dbItem.getRoomInfo().getCancellationPolicy();
		if (cp != null && cp.getMiscInfo() != null && !cp.getMiscInfo().cancellationPolicyBelongToRoom())
			return cp;
		return null;
	}

}
