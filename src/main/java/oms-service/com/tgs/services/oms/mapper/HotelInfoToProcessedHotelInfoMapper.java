package com.tgs.services.oms.mapper;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.ProcessedHotelInfo;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class HotelInfoToProcessedHotelInfoMapper extends Mapper<ProcessedHotelInfo> {

	private HotelInfo hInfo;

	@Override
	protected void execute() throws CustomGeneralException {
		output = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(hInfo), ProcessedHotelInfo.class);
	}

}
