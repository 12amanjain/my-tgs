package com.tgs.services.oms.mapper;

import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.MessageType;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;

@Builder
public class SegmentInfoToAirOrderItemMapper extends Mapper<DbAirOrderItem> {
	private SegmentInfo segmentInfo;
	private String bookingId;
	private double totalAmount;
	private AirSearchQuery searchQuery;
	private User bookingUser;
	private GstInfo gstInfo;
	private FMSCommunicator fmsCommunicator;


	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = DbAirOrderItem.builder().build();
		}
		output.setBookingId(ObjectUtils.firstNonNull(bookingId, output.getBookingId()));
		output.setAirlinecode(ObjectUtils.firstNonNull(segmentInfo.getFlightDesignator().getAirlineInfo().getCode(),
				output.getAirlinecode()));
		output.setSource(ObjectUtils.firstNonNull(segmentInfo.getDepartAirportInfo().getCode(), output.getSource()));
		output.setDest(ObjectUtils.firstNonNull(segmentInfo.getArrivalAirportInfo().getCode(), output.getDest()));
		output.setArrivalTime(ObjectUtils.firstNonNull(segmentInfo.getArrivalTime(), output.getArrivalTime()));
		output.setDepartureTime(ObjectUtils.firstNonNull(segmentInfo.getDepartTime(), output.getDepartureTime()));
		output.setFlightNumber(ObjectUtils.firstNonNull(segmentInfo.getFlightDesignator().getFlightNumber(),
				output.getFlightNumber()));
		output.setSupplierId(ObjectUtils.firstNonNull(
				segmentInfo.getPriceInfo(0).getSupplierBasicInfo().getSupplierId(), output.getSupplierId()));
		output.setStatus(ObjectUtils.firstNonNull(output.getStatus(), AirItemStatus.IN_PROGRESS.getStatus()));
		if (output.getTravellerInfo() == null) {
			updateSSRPricing();
			output.setAmount(totalAmount);
		}
		output.setMarkup(getMarkup(segmentInfo.getBookingRelatedInfo().getTravellerInfo()));
		output.setTravellerInfo(ObjectUtils.firstNonNull(segmentInfo.getBookingRelatedInfo().getTravellerInfo(),
				output.getTravellerInfo()));
		LocalDateTime timeLimit = ObjectUtils.firstNonNull(segmentInfo.getPriceInfo(0).getMiscInfo().getTimeLimit(),
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getTimeLimit() : null);

		String type = ObjectUtils.firstNonNull(
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getType() : null,
				searchQuery != null && searchQuery.isIntl() ? AirType.INTERNATIONAL.getCode()
						: AirType.DOMESTIC.getCode());

		SearchType searchType = ObjectUtils.firstNonNull(
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getSearchType() : null,
				searchQuery != null ? searchQuery.getSearchType() : null);

		long supplierRuleId = ObjectUtils.firstNonNull(segmentInfo.getPriceInfo(0).getSupplierBasicInfo().getRuleId(),
				(output.getAdditionalInfo() != null ? output.getAdditionalInfo().getSupplierRuleId() : null), 0L);
		Long commericialRuleId = ObjectUtils.firstNonNull(
				(segmentInfo.getPriceInfo(0).getMiscInfo() != null
						? segmentInfo.getPriceInfo(0).getMiscInfo().getCommericialRuleId()
						: null),
				(output.getAdditionalInfo() != null ? output.getAdditionalInfo().getCommercialRuleId() : null));
		String tourCode = ObjectUtils.firstNonNull(
				(segmentInfo.getPriceInfo(0).getMiscInfo() != null
						? segmentInfo.getPriceInfo(0).getMiscInfo().getTourCode()
						: null),
				(output.getAdditionalInfo() != null ? output.getAdditionalInfo().getTourCode() : null));

		String accountCode = ObjectUtils.firstNonNull(
				(segmentInfo.getPriceInfo(0).getMiscInfo() != null
						? segmentInfo.getPriceInfo(0).getMiscInfo().getAccountCode()
						: null),
				(output.getAdditionalInfo() != null ? output.getAdditionalInfo().getAccountCode() : null));

		Integer ccId =
				ObjectUtils.firstNonNull(
						(segmentInfo.getPriceInfo(0).getMiscInfo() != null
								? segmentInfo.getPriceInfo(0).getMiscInfo().getCcInfoId()
								: null),
						(output.getAdditionalInfo() != null ? output.getAdditionalInfo().getCcId() : null));

		Double iata = ObjectUtils.firstNonNull(
				(segmentInfo.getPriceInfo(0).getMiscInfo() != null ? segmentInfo.getPriceInfo(0).getMiscInfo().getIata()
						: null),
				(output.getAdditionalInfo() != null ? output.getAdditionalInfo().getIata() : null));

		String plattingCarrier = ObjectUtils.firstNonNull(segmentInfo.getPlatingCarrier(null),
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getPlatingCarrier() : null);

		String equipmentType = ObjectUtils.firstNonNull(segmentInfo.getFlightDesignator().getEquipType(),
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getEquipType() : null);

		Integer legNum = ObjectUtils.firstNonNull(segmentInfo.getPriceInfo(0).getMiscInfo().getLegNum(),
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getLegNum() : null);

		String creditShellPNR = ObjectUtils.firstNonNull(segmentInfo.getPriceInfo(0).getMiscInfo().getCreditShellPNR(),
				output.getAdditionalInfo() != null ? output.getAdditionalInfo().getCreditShellPNR() : null);

		AdditionalAirOrderItemInfo additonalItemInfo = AdditionalAirOrderItemInfo.builder()
				.stops(segmentInfo.getStops()).arrivalTerminal(segmentInfo.getArrivalAirportInfo().getTerminal())
				.type(type).duration(segmentInfo.getDuration())
				.departureTerminal(segmentInfo.getDepartAirportInfo().getTerminal()).timeLimit(timeLimit)
				.segmentNo(segmentInfo.getSegmentNum()).supplierRuleId(supplierRuleId).iata(iata)
				.commercialRuleId(commericialRuleId).tourCode(tourCode)
				.isReturnSegment(segmentInfo.getIsReturnSegment()).ccId(ccId).accountCode(accountCode)
				.platingCarrier(plattingCarrier).equipType(equipmentType)
				.fareIdentifier(segmentInfo.getPriceInfo(0).getFareType())
				.sourceId(fmsCommunicator.getSupplierInfo(output.getSupplierId()).getSourceId())
				.inventoryId(segmentInfo.getPriceInfo(0).getMiscInfo().getInventoryId()).searchType(searchType)
				.legNum(legNum).creditShellPNR(creditShellPNR).build();

		additonalItemInfo.setEquipType(segmentInfo.getFlightDesignator().getEquipType());
		if (CollectionUtils.isNotEmpty(segmentInfo.getStopOverAirports())) {
			additonalItemInfo.setStopOverAirport(
					segmentInfo.getStopOverAirports().stream().map(AirportInfo::getCode).collect(Collectors.toList()));
		}
		List<MessageInfo> messageInfo = segmentInfo.getPriceInfo(0).getMessages();
		if (CollectionUtils.isEmpty(messageInfo)) {
			messageInfo = output.getAdditionalInfo() != null ? output.getAdditionalInfo().getMessages() : null;
		}

		if (CollectionUtils.isNotEmpty(messageInfo)) {
			Iterator<MessageInfo> iterator = messageInfo.iterator();
			while (iterator.hasNext()) {
				MessageInfo message = iterator.next();
				if (MessageType.GENERAL.equals(message.getType())) {
				} else {
					iterator.remove();
				}
			}
			additonalItemInfo.setMessages(messageInfo);
		}


		if (segmentInfo.getOperatedByAirlineInfo() != null) {
			additonalItemInfo.setOperatingAirline(segmentInfo.getOperatedByAirlineInfo().getCode());
		}

		if (StringUtils.isNotEmpty(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey())) {
			additonalItemInfo.setSegmentReference(segmentInfo.getPriceInfo(0).getMiscInfo().getSegmentKey());
		}
		if (segmentInfo.getPriceInfo(0).getMiscInfo().getIsEticket() != null) {
			additonalItemInfo
					.setIsEticket(BooleanUtils.isTrue(segmentInfo.getPriceInfo(0).getMiscInfo().getIsEticket()));
		}
		if (segmentInfo.getPriceInfo(0).getMiscInfo().getProviderCode() != null) {
			additonalItemInfo.setProviderCode(segmentInfo.getPriceInfo(0).getMiscInfo().getProviderCode());
		}
		if (BooleanUtils.isTrue(segmentInfo.getPriceInfo(0).getMiscInfo().getIsPrivateFare())) {
			additonalItemInfo.setIsPrivateFare(segmentInfo.getPriceInfo(0).getMiscInfo().getIsPrivateFare());
		}
		if (BooleanUtils.isTrue(segmentInfo.getPriceInfo(0).getMiscInfo().getIsUserCreditCard())) {
			additonalItemInfo.setIsUserCreditCard(segmentInfo.getPriceInfo(0).getMiscInfo().getIsUserCreditCard());
		}
		output.setAdditionalInfo(additonalItemInfo);
	}

	private double getMarkup(List<FlightTravellerInfo> travellerInfo) {
		double totalMarkup = 0;
		for (FlightTravellerInfo traveller : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
			if (traveller.getFareDetail() != null && traveller.getFareDetail().getFareComponents() != null) {
				totalMarkup += traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.MU, 0.0);
			}
		}
		return totalMarkup;
	}

	private void updateSSRPricing() {
		for (FlightTravellerInfo traveller : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
			FareDetail fareDetail =
					new GsonMapper<>(segmentInfo.getPriceInfo(0, traveller).getFareDetail(traveller.getPaxType()),
							FareDetail.class).convert();
			traveller.setFareDetail(fareDetail);
			if (traveller.getFareDetail() != null) {
				addSSRToTotalFare(traveller);
				OmsHelper.updateGstComponents(traveller.getFareDetail(), bookingUser, gstInfo);
				totalAmount += fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0);
			}
		}
	}

	public static void addSSRToTotalFare(FlightTravellerInfo traveller) {

		FareDetail fareDetail = traveller.getFareDetail();

		double totalFare = fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0.0);
		if (traveller.getSsrBaggageInfo() != null && traveller.getSsrBaggageInfo().getAmount() != null) {
			if (fareDetail.getFareComponents().get(FareComponent.BP) != null) {
				fareDetail.getFareComponents().put(FareComponent.TF,
						totalFare - fareDetail.getFareComponents().get(FareComponent.BP));
			}
			fareDetail.getFareComponents().put(FareComponent.BP, traveller.getSsrBaggageInfo().getAmount());
			totalFare += ObjectUtils.firstNonNull(traveller.getSsrBaggageInfo().getAmount(), 0d);
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		}

		if (traveller.getSsrMealInfo() != null && traveller.getSsrMealInfo().getAmount() != null) {
			if (fareDetail.getFareComponents().get(FareComponent.MP) != null) {
				fareDetail.getFareComponents().put(FareComponent.TF,
						totalFare - fareDetail.getFareComponents().get(FareComponent.MP));
			}
			fareDetail.getFareComponents().put(FareComponent.MP, traveller.getSsrMealInfo().getAmount());
			totalFare += ObjectUtils.firstNonNull(traveller.getSsrMealInfo().getAmount(), 0d);
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		}

		if (traveller.getSsrSeatInfo() != null && traveller.getSsrSeatInfo().getAmount() != null) {
			if (fareDetail.getFareComponents().get(FareComponent.SP) != null) {
				fareDetail.getFareComponents().put(FareComponent.TF,
						totalFare - fareDetail.getFareComponents().get(FareComponent.SP));
			}
			fareDetail.getFareComponents().put(FareComponent.SP, traveller.getSsrSeatInfo().getAmount());
			totalFare += ObjectUtils.firstNonNull(traveller.getSsrSeatInfo().getAmount(), 0d);
			fareDetail.getFareComponents().put(FareComponent.TF, totalFare);
		}
	}
}
