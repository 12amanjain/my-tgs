package com.tgs.services.oms.mapper.hotel;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

import lombok.Builder;

@Builder
public class HotelOrderItemToHotelSearchQueryMapper extends Mapper<HotelSearchQuery> {

	private List<DbHotelOrderItem> items;

	@Override
	protected void execute() throws CustomGeneralException {
		output = HotelSearchQuery.builder().checkinDate(items.get(0).getCheckInDate())
				.checkoutDate(items.get(0).getCheckOutDate()).build();
		output.setRoomInfo(new ArrayList<>());
		for (DbHotelOrderItem item : items) {
			int adult = 0, child = 0;
			List<TravellerInfo> roomTravellerInfo = item.getRoomInfo().getTravellerInfo();
			if (CollectionUtils.isNotEmpty(roomTravellerInfo)) {
				for (TravellerInfo travellerInfo : roomTravellerInfo) {
					if (travellerInfo.getPaxType() != null) {
						if (travellerInfo.getPaxType().equals(PaxType.ADULT))
							adult++;
						else
							child++;
					}
				}
			} else {
				adult = item.getRoomInfo().getNumberOfAdults();
				child = item.getRoomInfo().getNumberOfChild();
			}
			output.getRoomInfo().add(RoomSearchInfo.builder().numberOfAdults(adult).numberOfChild(child).build());
		}
	}
}
