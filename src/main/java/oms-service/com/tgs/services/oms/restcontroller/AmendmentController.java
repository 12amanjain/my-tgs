package com.tgs.services.oms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.tgs.filters.AmendmentFilter;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.spring.AirlineResponseProcessor;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.oms.Amendments.AmendmentCronManager;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.jparepository.AmendmentService;
import lombok.extern.slf4j.Slf4j;

@RequestMapping("/oms/v1/amendments")
@RestController
@Slf4j
public class AmendmentController {

    @Autowired
    private AmendmentService amendmentService;

    @Autowired
    private AmendmentCronManager amendmentCronManager;
    
    @Autowired 
	private FiltersValidator validator;

	@InitBinder 
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator); 
	}
    
    @CustomRequestMapping(responseProcessors = { AirlineResponseProcessor.class, UserIdResponseProcessor.class})
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    protected AmendmentResponse search(@RequestBody @Valid AmendmentFilter filter, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			AmendmentResponse response = AmendmentResponse.builder().build();
			response.setErrors(validator.getErrorDetailFromBindingResult(result));
			return response;
		} else {
	        return AmendmentResponse.builder().amendmentItems(amendmentService.search(filter)).build();
		}
    }

    @RequestMapping(value = "/job/pws", method = RequestMethod.GET)
    protected AmendmentResponse pwsCron(HttpServletRequest request, @QueryParam("scanHours") Short scanHours) throws Exception {
        return amendmentCronManager.checkPWSStatus(scanHours);
    }
    
    @RequestMapping(value = "/reset-pax-ids", method = RequestMethod.POST)
	protected BaseResponse updateAmendment(@Valid @RequestBody AmendmentFilter amendmentFilter) throws Exception {
		log.info("update-pax-ids: Amendment filter {}", new Gson().toJson(amendmentFilter));
		List<Amendment> items = amendmentService.search(amendmentFilter);
		log.info("reset-pax-ids: Count returned {}", items.size());
		for (Amendment item : items) {
			log.info("reset-pax-ids: amendmentId {}", item.getAmendmentId());
			try {
				for(AirOrderItem airOrder : item.getAirAdditionalInfo().getOrderPreviousSnapshot()) {
					long counter = 1;
					for (TravellerInfo info : airOrder.getTravellerInfo()) {
						info.setId(counter++);
					}
				}
				for(AirOrderItem airOrder : item.getAirAdditionalInfo().getOrderNextSnapshot()) {
					long counter = 1;
					for (TravellerInfo info : airOrder.getTravellerInfo()) {
						info.setId(counter++);
					}
				}
				
			} catch (Exception e) {
				log.error("Unable to update pax for amendmentId {}, id {}", item.getAmendmentId(), item.getId());
			}
			amendmentService.save(item);
		}
		return new BaseResponse();
	}
	
}
