package com.tgs.services.oms.restcontroller;

import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.helper.SystemError;

@Component
public class DeliveryInfoValidator {
	
	public void validate(Errors errors, String fieldName, DeliveryInfo deliveryInfo) {
		for (String email : deliveryInfo.getEmails()) {
			if (!EmailValidator.getInstance().isValid(email)) {
				rejectValue(errors, fieldName + ".emails", SystemError.INVALID_EMAIL);
			}
		}

		for (String mobile : deliveryInfo.getContacts()) {
			PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
			try {
				if (!phoneUtil.isValidNumber(phoneUtil.parse(mobile, "IN"))) {
					rejectValue(errors, fieldName + ".contacts", SystemError.INVALID_MOBILE);
				}
			} catch (NumberParseException ex) {
				rejectValue(errors, fieldName + ".contacts", SystemError.INVALID_MOBILE);
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
