package com.tgs.services.oms.restcontroller;

import com.google.gson.Gson;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAction;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.ItemDetailResponse;
import com.tgs.services.oms.restmodel.OrderResponse;
import com.tgs.services.oms.servicehandler.*;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;

@RequestMapping("/oms/v1")
@RestController
@Slf4j
public class OrderController {

	@Autowired
	private BookingDetailHandler bookingDetailHandler;

	@Autowired
	private OrderListingHandler orderListingHandler;

	@Autowired
	private UpdateOrderHandler updateOrderHandler;

	@Autowired
	private ItemDetailHandler itemHandler;

	@Autowired
	private AirOrderItemService itemService;

	@Autowired
	private AuditsHandler auditHandler;
	
	@Autowired
	private UpdateOrderStatusHandler updateOrderStatusHandler;

	@CustomRequestMapping(responseProcessors = { UserIdResponseProcessor.class })
	@RequestMapping(value = "/orders", method = RequestMethod.POST)
	protected OrderResponse getOrderList(HttpServletRequest request, @Valid @RequestBody OrderFilter orderFilter, BindingResult result)
			throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.AIR));
		orderFilter.setBookingUserIds(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), orderFilter.getBookingUserIds()));
		FiltersValidator validator = (FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(orderFilter, result);
		if (result.hasErrors()) {
			OrderResponse response = new OrderResponse();
			response.setErrors(validator.getErrorDetailFromBindingResult(result));
			return response;
		} else {
			orderListingHandler.initData(orderFilter, new OrderResponse());
			return orderListingHandler.getResponse();
		}
	}

	@Deprecated
	@RequestMapping(value = "/booking-details/{bookingId}", method = RequestMethod.GET)
	protected BookingDetailResponse getBookingDetails(HttpServletRequest request, @PathVariable String bookingId)
			throws Exception {
		bookingDetailHandler.initData(BookingDetailRequest.builder().bookingId(bookingId).build(),
				new BookingDetailResponse());
		return bookingDetailHandler.getResponse();
	}

	@RequestMapping(value = "/updateorder", method = RequestMethod.GET)
	protected BookingDetailResponse updateOrder(HttpServletRequest request) throws Exception {
		List<DbAirOrderItem> items = itemService.findAll();
		for (DbAirOrderItem item : items) {
			try {
				int i = 1;
				for (TravellerInfo info : item.getTravellerInfo()) {
					info.setId((long) i++);
				}
				itemService.save(item);
			} catch (Exception e) {
				log.error("Unable to save item for id {}", item.getId());
			}
		}

		return bookingDetailHandler.getResponse();
	}

	@RequestMapping(value = "/reset-pax-ids", method = RequestMethod.POST)
	protected BaseResponse updateOrder(@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		log.info("update-pax-ids: Order filter {}", new Gson().toJson(orderFilter));
		List<Object[]> items = itemService.findByJsonSearch(orderFilter);
		log.info("reset-pax-ids: Count returned {}", items.size());
		for (Object[] object : items) {
			DbAirOrderItem item = (DbAirOrderItem) object[0];
			log.info("reset-pax-ids: bookingId {}", item.getBookingId());
			try {
				for (TravellerInfo info : item.getTravellerInfo()) {
					info.setId(null);
				}
				itemService.save(item);
			} catch (Exception e) {
				log.error("Unable to update pax for bookingId {}, id {}", item.getBookingId(), item.getId());
			}
		}
		return new BaseResponse();
	}
	
	@RequestMapping(value="/updateorder/{action}",method=RequestMethod.POST)
	protected BookingDetailResponse updateOrderAction(HttpServletRequest request
			,@PathVariable @NotNull OrderAction action
			,@RequestBody Order detailRequest) throws Exception{
		updateOrderHandler.initData(detailRequest, BookingDetailResponse.builder().build());
		updateOrderHandler.setAction(action);
		return updateOrderHandler.getResponse();
	}

	@CustomRequestMapping(responseProcessors = { UserIdResponseProcessor.class })
	@RequestMapping(value = "/booking-details", method = RequestMethod.POST)
	protected BookingDetailResponse getBookingDetails(HttpServletRequest request,
			@RequestBody BookingDetailRequest detailRequest) throws Exception {
		bookingDetailHandler.initData(detailRequest, new BookingDetailResponse());
		return bookingDetailHandler.getResponse();
	}

	@Deprecated
	@RequestMapping(value = "/item-details/{bookingId}", method = RequestMethod.GET)
	protected ItemDetailResponse getItemDetails(HttpServletRequest request, @PathVariable String bookingId)
			throws Exception {
		itemHandler.initData(bookingId, new ItemDetailResponse());
		return itemHandler.getResponse();
	}

	@RequestMapping(value = "/orders-audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = { UserIdResponseProcessor.class })
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbOrder.class, "bookingId"));
		return auditResponse;
	}

	@RequestMapping(value = "/job/update-order-status", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse updateOrderStatus(HttpServletRequest request,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		updateOrderStatusHandler.initData(orderFilter, new BaseResponse());
		return updateOrderStatusHandler.getResponse();
	}
}
