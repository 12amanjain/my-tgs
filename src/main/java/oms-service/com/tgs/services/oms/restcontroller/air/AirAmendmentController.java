package com.tgs.services.oms.restcontroller.air;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.Amendments.AirAmendmentCronManager;
import com.tgs.services.oms.datamodel.amendments.AmendmentIdRequest;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.ProcessAirAmendmentRequest;
import com.tgs.services.oms.restmodel.air.AirInvoiceResponse;
import com.tgs.services.oms.restmodel.air.AirInvoiceRequest;
import com.tgs.services.oms.restmodel.air.AmendmentDetailResponse;
import com.tgs.services.oms.restmodel.air.AmendmentChargesResponse;
import com.tgs.services.oms.restmodel.air.PartnerAmendmentResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.gson.RestExcludeStrategy;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.AirlineResponseProcessor;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.BookingIdResponse;
import com.tgs.services.oms.servicehandler.UpdateAirAmendmentHandler;
import com.tgs.services.oms.servicehandler.air.AirAmendmentCancellationHandler;
import com.tgs.services.oms.servicehandler.air.AirAmendmentStatusHandler;
import com.tgs.services.oms.servicehandler.air.AmendmentChargesHandler;
import com.tgs.services.oms.servicehandler.air.AmendmentSubmitHandler;
import com.tgs.services.oms.servicehandler.air.ProcessAirAmendmentHandler;
import com.tgs.services.oms.servicehandler.air.RaiseAirAmendmentHandler;

@RequestMapping("/oms/v1/air/amendment")
@RestController
public class AirAmendmentController {

	@Autowired
	private RaiseAirAmendmentHandler raiseAirAmendmentHandler;

	@Autowired
	private ProcessAirAmendmentHandler processAirAmendmentHandler;

	@Autowired
	private UpdateAirAmendmentHandler updateAirAmendmentHandler;

	@Autowired
	private AirAmendmentManager airAmendmentManager;

	@Autowired
	private AirAmendmentCancellationHandler airAmendmentCancellationHandler;

	@Autowired
	private AirAmendmentCronManager cronManager;

	@Autowired
	private AmendmentChargesHandler chargesHandler;

	@Autowired
	private AmendmentSubmitHandler submitHandler;

	@Autowired
	private AirAmendmentStatusHandler airAmendmentStatusHandler;

	@Autowired
	private AmendmentRequestValidator requestValidator;

	@InitBinder("amendmentRequest")
	public void initBinderForAmendmentRequest(WebDataBinder binder) {
		binder.setValidator(requestValidator);
	}


	@RequestMapping(value = "/getOrderInfo/{bookingId}/{amdType}", method = RequestMethod.GET)
	protected AirOrderInfo getAirOrder(HttpServletRequest request, @PathVariable @NotNull String bookingId,
			@PathVariable @NotNull AmendmentType amdType) {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		SystemContextHolder.getContextData().getReqIds().add(bookingId);
		AirOrderInfo airOrderInfo = airAmendmentManager.getOrderInfoForAmendment(bookingId, amdType);
		airAmendmentManager.checkPaxStatus(bookingId, airOrderInfo, false, amdType);
		return airOrderInfo;
	}

	@RequestMapping(value = "/raise", method = RequestMethod.POST)
	protected AmendmentResponse raise(@RequestBody @Valid AirAmendmentPaxInfo request) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
		raiseAirAmendmentHandler.initData(request,
				AmendmentResponse.builder().amendmentItems(new ArrayList<>()).build());
		return raiseAirAmendmentHandler.getResponse();
	}

	@CustomRequestMapping(responseProcessors = {AirlineResponseProcessor.class, UserIdResponseProcessor.class})
	@RequestMapping(value = "/getDetail/{amendmentId}", method = RequestMethod.GET)
	protected AmendmentDetailResponse getDetail(HttpServletRequest request, @PathVariable @NotNull String amendmentId) {
		return airAmendmentManager.getAmendmentDetail(amendmentId);
	}

	@RequestMapping(value = "/confirm-cancellation", method = RequestMethod.POST)
	protected AmendmentResponse confirmCancellation(@RequestBody @Valid AirAmendmentPaxInfo request) throws Exception {
		SystemContextHolder.getContextData().getExclusionStrategys().add(new RestExcludeStrategy());
		SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
		airAmendmentCancellationHandler.initData(request,
				AmendmentResponse.builder().amendmentItems(new ArrayList<>()).build());
		return airAmendmentCancellationHandler.getResponse();
	}

	@RequestMapping(value = "/cron-confirm-cancellation", method = RequestMethod.POST)
	protected BaseResponse confirmCronCancellation() throws Exception {
		cronManager.doCronCancellation();
		return new BaseResponse();
	}


	@RequestMapping(value = "/process", method = RequestMethod.POST)
	protected AmendmentResponse process(HttpServletRequest request,
			@RequestBody @Valid ProcessAirAmendmentRequest amendment) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendment.getAmendmentId());
		processAirAmendmentHandler.initData(amendment, AmendmentResponse.builder().build());
		AmendmentResponse response = processAirAmendmentHandler.getResponse();
		if (response != null && CollectionUtils.isNotEmpty(response.getAmendmentItems()))
			SystemContextHolder.getContextData().getReqIds().add(response.getAmendmentItems().get(0).getBookingId());
		return response;
	}

	@RequestMapping(value = "/update/{action}", method = RequestMethod.POST)
	protected AmendmentResponse update(HttpServletRequest request, @PathVariable @NotNull AmendmentAction action,
			@RequestBody @Valid UpdateAmendmentRequest amendmentRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendmentRequest.getAmendmentId());
		updateAirAmendmentHandler.setAction(action);
		AmendmentResponse response = AmendmentResponse.builder()
				.amendmentItems(
						Stream.of(updateAirAmendmentHandler.process(amendmentRequest)).collect(Collectors.toList()))
				.build();
		if (response != null && CollectionUtils.isNotEmpty(response.getAmendmentItems()))
			SystemContextHolder.getContextData().getReqIds().add(response.getAmendmentItems().get(0).getBookingId());
		return response;
	}

	@RequestMapping(value = "/invoice/{amendmentId}", method = RequestMethod.GET)
	protected AirInvoiceResponse invoice(@PathVariable @NotNull String amendmentId) throws Exception {
		return airAmendmentManager.getAmendmentInvoiceDetails(amendmentId, null);
	}

	@RequestMapping(value = "/invoice2", method = RequestMethod.POST)
	protected AirInvoiceResponse invoice(@RequestBody @Valid AirInvoiceRequest invoiceRequest) throws Exception {
		return airAmendmentManager.getAmendmentInvoiceDetails(invoiceRequest.getId(), invoiceRequest.getInvoiceId());
	}

	@RequestMapping(value = "/getreissueids/{bookingId}", method = RequestMethod.GET)
	protected BookingIdResponse getReissueIds(@PathVariable String bookingId) {
		return new BookingIdResponse(airAmendmentManager.checkAndGetReIssueIds(bookingId));
	}

	@RequestMapping(value = "/internal/setmissingssh", method = RequestMethod.POST)
	protected BookingIdResponse setMissingSSH(@RequestBody AmendmentIdRequest request) {
		return new BookingIdResponse(airAmendmentManager.setPSSH(request.getAmdIds(), request.getDate()));
	}

	@RequestMapping(value = "/amendment-charges", method = RequestMethod.POST)
	protected AmendmentChargesResponse amendmentCharges(HttpServletRequest request,
			@RequestBody @Valid AmendmentRequest amendmentRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendmentRequest.getBookingId());
		chargesHandler.initData(amendmentRequest, AmendmentChargesResponse.builder().build());
		return chargesHandler.getResponse();
	}

	@RequestMapping(value = "/submit-amendment", method = RequestMethod.POST)
	protected PartnerAmendmentResponse submitAmendment(HttpServletRequest request,
			@RequestBody @Valid AmendmentRequest amendmentRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendmentRequest.getBookingId());
		submitHandler.initData(amendmentRequest, PartnerAmendmentResponse.builder().build());
		return submitHandler.getResponse();
	}

	@RequestMapping(value = "/amendment-details", method = RequestMethod.POST)
	protected PartnerAmendmentResponse amendmentStatus(HttpServletRequest request,
			@RequestBody @Valid AmendmentRequest amendment) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(amendment.getAmendmentId());
		airAmendmentStatusHandler.initData(amendment, PartnerAmendmentResponse.builder().build());
		return airAmendmentStatusHandler.getResponse();
	}
}
