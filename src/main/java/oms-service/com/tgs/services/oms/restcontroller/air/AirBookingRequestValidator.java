package com.tgs.services.oms.restcontroller.air;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.datamodel.TravellerInfoValidatingData;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfoValidatingData;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.DobOutput;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.oms.restcontroller.BookingRequestValidator;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AirBookingRequestValidator extends BookingRequestValidator {

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	protected CommercialCommunicator cmsCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	public static final List<String> ADULT_TITLES = Arrays.asList("MR", "MRS", "MS");
	public static final List<String> CHILD_TITLES = Arrays.asList("MASTER", "MS");

	@Override
	public boolean supports(Class<?> clazz) {
		return AirBookingRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		AirBookingRequest airBookingRequest = (AirBookingRequest) target;

		if (StringUtils.isBlank(airBookingRequest.getBookingId())) {
			throw new CustomGeneralException(SystemError.ORDER_OR_BOOKINGID_MANDATORY);
		}

		super.validate(target, errors);

		Boolean isBlockRequest = CollectionUtils.isEmpty(airBookingRequest.getPaymentInfos());

		AirBookingConditions conditions = getBookingConditions(airBookingRequest);

		AirReviewResponse reviewResponse =
				fmsComm.getAirReviewResponse(airBookingRequest.getBookingId(), airBookingRequest.getOldBookingId());

		if (conditions.getSessionCreationTime() != null) {
			if (conditions.getSessionCreationTime().plusSeconds(conditions.getSessionTimeInSecond())
					.isBefore(LocalDateTime.now()))
				throw new CustomGeneralException(SystemError.SESSION_EXPIRED);
		}

		DobOutput dobOutput = null;
		if (conditions != null) {
			dobOutput = conditions.getDobOutput();
		}

		boolean isSpaceAllowedInLastName = true;

		List<FlightBasicFact> facts = new ArrayList<>();
		List<TripInfo> tripInfos = fmsComm.getTripInfos(airBookingRequest);

		User loggedInUser = SystemContextHolder.getContextData().getUser();
		User bookingUser = userService.getUserFromCache(loggedInUser.getParentUserId());

		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segInfo : tripInfo.getSegmentInfos()) {
				FlightBasicFact fact = FlightBasicFact.createFact().generateFact(segInfo);
				BaseUtils.createFactOnUser(fact, bookingUser);
				facts.add(fact);
				String airline = segInfo.getAirlineCode(false);
				if (airline.equals("ZO") || airline.equals("2T")) {
					isSpaceAllowedInLastName = false;
				}
			}
		}

		double paymentFee = 0d;
		if (CollectionUtils.isNotEmpty(airBookingRequest.getPaymentInfos())) {
			for (PaymentRequest request : airBookingRequest.getPaymentInfos()) {
				if (SystemContextHolder.getContextData().getUser() == null
						&& (request.getPaymentMedium().equals(PaymentMedium.WALLET)
								|| request.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE))) {
					throw new CustomGeneralException(SystemError.DISALLOWED_PAYMENT_MODE);
				}
				paymentFee += request.getPaymentFee().doubleValue();
			}

			// Virtual payment
			if (AirBookingUtils.isVirtualPayment(airBookingRequest)) {
				this.validateVirtualPayment(tripInfos, airBookingRequest, bookingUser, paymentFee);
			}
		}

		// most strict limit
		NameLengthLimit nameLengthLimit = getMostStrictNameLengthLimit(facts);
		int tripCount = reviewResponse.getTripInfos().size();
		TripInfo lastTrip = reviewResponse.getTripInfos().get(tripCount - 1);
		SegmentInfo lastSegment = lastTrip.getSegmentInfos().get(lastTrip.getNumOfSegments() - 1);
		LocalDate lastTravelDate = lastSegment.getDepartTime().toLocalDate();
		LocalDate firstTravelDate = reviewResponse.getTripInfos().get(0).getDepartureTime().toLocalDate();
		FlightTravellerInfoValidatingData validatingData = FlightTravellerInfoValidatingData.builder()
				.nameLengthLimit(nameLengthLimit).lastTravelDate(lastTravelDate).firstTravelDate(firstTravelDate)
				.isSpaceAllowedInLastName(isSpaceAllowedInLastName).isSpaceAllowedInFirstName(true).dobOutput(dobOutput)
				.bookingConditions(conditions).build();
		validateTravellerInfos(errors, airBookingRequest.getTravellerInfo(), validatingData, reviewResponse);

		if (errors.hasErrors()) {
			log.info("Validation errors for bookingId {},errors are {}", airBookingRequest.getBookingId(),
					errors.getFieldErrors());
		}

		Boolean isBlockingAllowed = BooleanUtils.isTrue(conditions.getIsBlockingAllowed());

		if (!isBlockingAllowed && isBlockRequest) {
			// in case of api request, review response blocking is not supported , but customer tries to block ticket
			throw new CustomGeneralException(SystemError.HOLD_NOT_ALLOWED);
		}

	}

	@Override
	public AirBookingConditions getBookingConditions(BookingRequest bookingRequest) {
		AirReviewResponse reviewResponse =
				fmsComm.getAirReviewResponse(bookingRequest.getBookingId(), bookingRequest.getOldBookingId());
		AirBookingConditions conditions = (AirBookingConditions) reviewResponse.getConditions();
		return conditions;
	}

	private void validateTravellerInfos(Errors errors, List<FlightTravellerInfo> travellerInfos,
			TravellerInfoValidatingData validatingData, AirReviewResponse reviewResponse) {
		int travellerIndex = 0;
		Map<String, Boolean> nameMap = new HashMap<>();
		Map<String, Boolean> panMap = new HashMap<>();
		for (TravellerInfo travellerInfo : travellerInfos) {
			String travellerField = "travellerInfo[" + travellerIndex++ + "]";
			registerErrors(errors, travellerField, travellerInfo, validatingData);
			if (!isValidTitle(travellerInfo.getPaxType(), travellerInfo.getTitle())) {
				rejectValue(errors, StringUtils.join(travellerField, ".title"), SystemError.INVALID_PAX_TITLE);
			}
			if (nameMap.get(travellerInfo.getFirstName() + travellerInfo.getLastName()) != null) {
				rejectValue(errors, StringUtils.join(travellerField, ".firstName"), SystemError.SAME_NAME_ERROR);
			}
			nameMap.put(travellerInfo.getFirstName() + travellerInfo.getLastName(), true);

			if (StringUtils.isNotBlank(travellerInfo.getPanNumber())
					&& PaxType.ADULT.equals(travellerInfo.getPaxType())) {
				if (panMap.get(travellerInfo.getPanNumber()) != null) {
					rejectValue(errors, StringUtils.join(travellerField, ".panNumber"), SystemError.SAME_PAN_NUMBER);
				}
				panMap.put(travellerInfo.getPanNumber(), true);
			}

			if (CollectionUtils.isNotEmpty(reviewResponse.getTravellers())) {
				boolean isReviewedTraveller = false;
				for (TravellerInfo reviewedTraveller : reviewResponse.getTravellers()) {
					if ((reviewedTraveller.getFullName()).equalsIgnoreCase(travellerInfo.getFullName())) {
						isReviewedTraveller = true;
					}
				}
				if (!isReviewedTraveller) {
					rejectValue(errors, StringUtils.join(travellerField, ".firstName"),
							SystemError.PNR_PASSENGER_MISMATCH);
				}
			}
		}
	}

	private void rejectValue(Errors errors, String field, SystemError systemError) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage());
	}

	private NameLengthLimit getMostStrictNameLengthLimit(List<FlightBasicFact> facts) {
		NameLengthLimit nameLengthLimit = new NameLengthLimit();
		for (FlightBasicFact flightBasicFact : facts) {
			NameLengthLimit airNameLengthLimitFromRule = fmsComm.getAirNameLength(flightBasicFact);
			if (airNameLengthLimitFromRule != null) {
				nameLengthLimit.merge(airNameLengthLimitFromRule);

			}
		}
		return nameLengthLimit;
	}

	private boolean isValidTitle(PaxType paxType, String title) {
		if (PaxType.ADULT.equals(paxType)) {
			return validateTitle(title, ADULT_TITLES);
		}
		if ((PaxType.CHILD.equals(paxType) || PaxType.INFANT.equals(paxType))) {
			return validateTitle(title, CHILD_TITLES);
		}
		return false;
	}

	private boolean validateTitle(String title, List<String> titleList) {
		if (titleList.contains(title.toUpperCase()))
			return true;
		return false;
	}

	private void validateVirtualPayment(List<TripInfo> tripInfos, AirBookingRequest airBookingRequest, User bookingUser,
			double paymentFee) {
		double commission = 0d;
		double managementFee = 0d;
		double clientMarkup = 0d;
		for (TripInfo trip : tripInfos) {
			for (SegmentInfo segment : trip.getSegmentInfos()) {
				for (Entry<PaxType, FareDetail> entry : segment.getPriceInfo(0).getFareDetails().entrySet()) {
					managementFee += entry.getValue().getFareComponents().getOrDefault(FareComponent.MF, 0d);
					managementFee += entry.getValue().getFareComponents().getOrDefault(FareComponent.MFT, 0d);
					clientMarkup += entry.getValue().getFareComponents().getOrDefault(FareComponent.CMU, 0d);
					commission += AirBookingUtils.getGrossCommission(entry.getValue().getFareComponents(), null, true);
				}
			}
		}
		// Virtual payment not allowed for booking which has commission/discount, management fee, payment fee.
		if (Math.abs(clientMarkup) > 0 || Math.abs(commission) > 0 || Math.abs(managementFee) > 0
				|| Math.abs(paymentFee) > 0
				|| !cmsCommunicator.isVirtualCreditCardSupported(airBookingRequest.getBookingId(), bookingUser,
						airBookingRequest.getGstInfo())) {
			throw new CustomGeneralException(SystemError.DISALLOWED_PAYMENT_MODE);
		}
	}
}
