package com.tgs.services.oms.restcontroller.air;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.restcontroller.BookingRequestValidator;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.air.AirConfirmBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.User;

@Service
public class AirConfirmBookingRequestValidator extends BookingRequestValidator {

	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected CommercialCommunicator cmsCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private AirOrderItemManager itemManager;

	@Override
	public boolean supports(Class<?> clazz) {
		return AirConfirmBookingRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AirConfirmBookingRequest bookingRequest = (AirConfirmBookingRequest) target;

		if (StringUtils.isBlank(bookingRequest.getBookingId())) {
			throw new CustomGeneralException(SystemError.ORDER_OR_BOOKINGID_MANDATORY);
		}

		List<DbAirOrderItem> items = itemManager.findByBooking(bookingRequest.getBookingId());
		if (CollectionUtils.isEmpty(items)) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}

		User loggedInUser = SystemContextHolder.getContextData().getUser();
		User bookingUser = userService.getUserFromCache(loggedInUser.getParentUserId());

		double paymentFee = 0d;

		if (CollectionUtils.isEmpty(bookingRequest.getPaymentInfos())) {
			throw new CustomGeneralException(SystemError.DISALLOWED_PAYMENT_MODE);
		}

		for (PaymentRequest request : bookingRequest.getPaymentInfos()) {
			if (SystemContextHolder.getContextData().getUser() == null
					&& (request.getPaymentMedium().equals(PaymentMedium.WALLET)
							|| request.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE))) {
				throw new CustomGeneralException(SystemError.DISALLOWED_PAYMENT_MODE);
			}
			paymentFee += request.getPaymentFee().doubleValue();
		}

		// Virtual payment
		if (AirBookingUtils.isVirtualPayment(bookingRequest)) {
			this.validateVirtualPayment(items, bookingRequest, bookingUser, paymentFee);
		}
	}

	@Override
	public AirBookingConditions getBookingConditions(BookingRequest bookingRequest) {
		return null;
	}

	private void validateVirtualPayment(List<DbAirOrderItem> items, AirConfirmBookingRequest bookingRequest,
			User bookingUser, double paymentFee) {
		double commission = 0d;
		double managementFee = 0d;
		double clientMarkup = 0d;
		for (DbAirOrderItem item : items) {
			managementFee += AirBookingUtils.getTotalFareComponentAmount(item, FareComponent.MF);
			paymentFee += AirBookingUtils.getTotalFareComponentAmount(item, FareComponent.PF);
			clientMarkup += AirBookingUtils.getTotalFareComponentAmount(item, FareComponent.CMU);
		}

		commission = AirBookingUtils.getGrossCommission(items, null, true);
		GstInfo billingEntity = orderManager.getGstInfo(bookingRequest.getBookingId());

		// Virtual payment not allowed for booking which has commission/discount, management fee, payment fee.
		if (Math.abs(clientMarkup) > 0 || Math.abs(commission) > 0 || Math.abs(managementFee) > 0
				|| Math.abs(paymentFee) > 0 || !cmsCommunicator
						.isVirtualCreditCardSupported(bookingRequest.getBookingId(), bookingUser, billingEntity)) {
			throw new CustomGeneralException(SystemError.DISALLOWED_PAYMENT_MODE);
		}
	}

}
