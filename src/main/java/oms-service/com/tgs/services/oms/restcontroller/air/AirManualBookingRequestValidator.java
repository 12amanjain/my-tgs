package com.tgs.services.oms.restcontroller.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.oms.restmodel.air.AirManualBookingRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.TravellerInfoValidatingData;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirBookingConditions;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfoValidatingData;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.restcontroller.BookingRequestValidator;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.ums.datamodel.User;

@Component
public class AirManualBookingRequestValidator extends BookingRequestValidator {

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	private AirOrderItemValidator orderItemValidator;

	@Autowired
	private UserServiceCommunicator userServiceComm;

	@Override
	public boolean supports(Class<?> clazz) {
		return AirManualBookingRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AirManualBookingRequest manualBookingRequest = (AirManualBookingRequest) target;

		if (manualBookingRequest.getBookingId() != null) {
			return;
		}

		super.validate(target, errors);

		User bookingUser = userServiceComm.getUserFromCache(manualBookingRequest.getUserId());
		if (bookingUser == null) {
			rejectValue(errors, "userId", SystemError.INVALID_USERID);
		}

		boolean isSpaceAllowedInLastName = true;

		if (CollectionUtils.isEmpty(manualBookingRequest.getAirOrderItems())) {
			rejectValue(errors, "airOrderItems", SystemError.EMPTY_AIR_ORDER_ITEM_LIST);
		} else {
			List<FlightBasicFact> facts = new ArrayList<>();
			LocalDateTime lastTravelDateTime = null;
			LocalDateTime firstTravelDateTime = null;
			int orderItemIndex = 0;
			SegmentInfo prevSegmentInfo = null;
			for (AirOrderItem orderItem : manualBookingRequest.getAirOrderItems()) {
				if (!orderItem.getArrivalTime().isAfter(orderItem.getDepartureTime()))
					rejectValue(errors, "airOrderItems[" + orderItemIndex + "]", SystemError.INVALID_TIMINGS);

				if (lastTravelDateTime == null) {
					lastTravelDateTime = orderItem.getDepartureTime();
				} else if (orderItem.getDepartureTime() != null
						&& orderItem.getDepartureTime().isAfter(lastTravelDateTime)) {
					lastTravelDateTime = orderItem.getDepartureTime();
				}

				if (firstTravelDateTime == null) {
					firstTravelDateTime = orderItem.getDepartureTime();
				} else if (orderItem.getDepartureTime() != null
						&& orderItem.getDepartureTime().isBefore(firstTravelDateTime)) {
					firstTravelDateTime = orderItem.getDepartureTime();
				}

				SegmentInfo segmentInfo = AirOrderItemToSegmentInfoMapper.builder().fmsCommunicator(fmsComm)
						.item(new DbAirOrderItem().from(orderItem)).build().convert();

				String airline = segmentInfo.getAirlineCode(false);
				if (airline.equals("ZO") || airline.equals("2T")) {
					isSpaceAllowedInLastName = false;
				}

				validateSegmentNumbers(errors, "airOrderItems[" + orderItemIndex + "]", segmentInfo, prevSegmentInfo);
				prevSegmentInfo = segmentInfo;
				FlightBasicFact fact = FlightBasicFact.createFact().generateFact(segmentInfo);
				BaseUtils.createFactOnUser(fact, bookingUser);
				facts.add(fact);
				orderItemValidator.validate(errors, "airOrderItems[" + orderItemIndex + "]", orderItem);
				orderItemIndex++;
			}

			List<FlightTravellerInfo> travellerInfos =
					manualBookingRequest.getAirOrderItems().get(0).getTravellerInfo();

			// most strict limit
			NameLengthLimit nameLengthLimit = getMostStrictNameLengthLimit(facts);
			FlightTravellerInfoValidatingData validatingData = FlightTravellerInfoValidatingData.builder()
					.nameLengthLimit(nameLengthLimit).lastTravelDate(lastTravelDateTime.toLocalDate())
					.firstTravelDate(firstTravelDateTime.toLocalDate())
					.isSpaceAllowedInLastName(isSpaceAllowedInLastName).isSpaceAllowedInFirstName(true).build();
			validateTravellerInfos(errors, "airOrderItems[0].travellerInfo", travellerInfos, validatingData);

			Map<PaxType, Integer> paxInfo = BaseUtils.getPaxInfo(travellerInfos);
			int adults = paxInfo.getOrDefault(PaxType.ADULT, 0), children = paxInfo.getOrDefault(PaxType.CHILD, 0),
					infants = paxInfo.getOrDefault(PaxType.INFANT, 0);
			/**
			 * Intentionally we have disabled this check because in case of manual order , only infant/child order can
			 * be created.
			 */
			// validatePaxCount(errors, "airOrderItems[0].travellerInfo", adults, children,
			// infants);
		}
	}

	private void rejectValue(Errors errors, String field, SystemError systemError, Object... args) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage(args));
	}

	@Override
	public AirBookingConditions getBookingConditions(BookingRequest bookingRequest) {
		return null;
	}

	private void validatePaxCount(Errors errors, String fieldName, int adults, int children, int infants) {
		if (adults + children > 9) {
			rejectValue(errors, fieldName, SystemError.INVALID_PAX_COUNT,
					"adults and children combined can't be more than 9");
		}

		if (children > adults) {
			rejectValue(errors, fieldName, SystemError.INVALID_PAX_COUNT, "child can't be more than adults");
		}

		if (infants > adults) {
			rejectValue(errors, fieldName, SystemError.INVALID_PAX_COUNT, "infants can't be more than adults");
		}
	}

	private void validateTravellerInfos(Errors errors, String fieldName, List<FlightTravellerInfo> travellerInfos,
			TravellerInfoValidatingData validatingData) {
		int travellerIndex = 0;
		Map<String, Boolean> nameMap = new HashMap<>();
		for (FlightTravellerInfo travellerInfo : travellerInfos) {
			String travellerField = fieldName + "[" + travellerIndex++ + "]";
			registerErrors(errors, travellerField, travellerInfo, validatingData);

			if (nameMap.get(travellerInfo.getFirstName() + travellerInfo.getLastName()) != null) {
				rejectValue(errors, StringUtils.join(travellerField, ".firstName"), SystemError.SAME_NAME_ERROR);
			}
			nameMap.put(travellerInfo.getFirstName() + travellerInfo.getLastName(), true);

			validateSsrInformations(errors, travellerField + ".ssrMealInfos", "Meal", travellerInfo.getSsrMealInfos());
			validateSsrInformations(errors, travellerField + ".ssrSeatInfos", "Seat", travellerInfo.getSsrSeatInfos());
			validateSsrInformations(errors, travellerField + ".ssrBaggageInfos", "Baggage",
					travellerInfo.getSsrBaggageInfos());
		}
	}

	private void validateSsrInformations(Errors errors, String fieldName, String ssrType,
			List<SSRInformation> ssrInformations) {
		if (ssrInformations != null) {
			int i = 0;
			for (SSRInformation ssrInformation : ssrInformations) {
				if (ssrInformation.getAmount() == null) {
					rejectValue(errors, fieldName + "[" + i++ + "]", SystemError.INVALID_SSR_INFORMATION, ssrType);
				}
			}
		}
	}

	private void validateSegmentNumbers(Errors errors, String fieldName, SegmentInfo segmentInfo,
			SegmentInfo prevSegmentInfo) {
		if (prevSegmentInfo != null) {
			int segNum = segmentInfo.getSegmentNum();
			if (segNum > 0) {
				if (segNum != prevSegmentInfo.getSegmentNum() + 1) {
					rejectValue(errors, fieldName + ".additionalInfo.segmentNo", SystemError.INVALID_SEGMENT_NUMBER);
				}
				if (prevSegmentInfo.getArrivalTime() != null && segmentInfo.getDepartTime() != null
						&& !segmentInfo.getDepartTime().isAfter(prevSegmentInfo.getArrivalTime())) {
					rejectValue(errors, fieldName + ".departureTime", SystemError.INVALID_SEGMENT_TIMINGS);
					rejectValue(errors, fieldName + ".arrivalTime", SystemError.INVALID_SEGMENT_TIMINGS);
				}
			}
		} else if (segmentInfo.getSegmentNum() > 0) {
			rejectValue(errors, fieldName + ".additionalInfo.segmentNo", SystemError.INVALID_SEGMENT_NUMBER);
		}
	}

	private NameLengthLimit getMostStrictNameLengthLimit(List<FlightBasicFact> facts) {
		NameLengthLimit nameLengthLimit = new NameLengthLimit();
		for (FlightBasicFact flightBasicFact : facts) {
			NameLengthLimit airNameLengthLimitFromRule = fmsComm.getAirNameLength(flightBasicFact);
			if (airNameLengthLimitFromRule != null) {
				nameLengthLimit.merge(airNameLengthLimitFromRule);
			}
		}
		return nameLengthLimit;
	}
}
