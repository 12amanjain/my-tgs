package com.tgs.services.oms.restcontroller.air;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.fms.restmodel.AirMessageRequest;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.restmodel.air.AirConfirmBookingRequest;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.oms.restmodel.air.AirImportPnrReviewResponse;
import com.tgs.services.oms.restmodel.air.AirInvoiceRequest;
import com.tgs.services.oms.restmodel.air.AirInvoiceResponse;
import com.tgs.services.oms.restmodel.air.AirItemDetailRequest;
import com.tgs.services.oms.restmodel.air.AirManualBookingRequest;
import com.tgs.services.oms.restmodel.air.AirReleasePNRRequest;
import com.tgs.services.oms.servicehandler.air.AbortOrderHandler;
import com.tgs.services.oms.servicehandler.air.AirBookingFareConfirmationHandler;
import com.tgs.services.oms.servicehandler.air.AirBookingHandler;
import com.tgs.services.oms.servicehandler.air.AirConfirmBookingHandler;
import com.tgs.services.oms.servicehandler.air.AirImportPnrBookingHandler;
import com.tgs.services.oms.servicehandler.air.AirImportPnrReviewHandler;
import com.tgs.services.oms.servicehandler.air.AirInvoiceHandler;
import com.tgs.services.oms.servicehandler.air.AirItemDetailUpdateHandler;
import com.tgs.services.oms.servicehandler.air.AirManualOrderHandler;
import com.tgs.services.oms.servicehandler.air.AirManualReviewHandler;
import com.tgs.services.oms.servicehandler.air.AirOrderItemHandler;
import com.tgs.services.oms.servicehandler.air.AirOrderMessageHandler;
import com.tgs.services.oms.servicehandler.air.AirReleasePNRHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RequestMapping("/oms/v1/air")
@RestController
public class AirOrderController {

	@Autowired
	private AirBookingHandler bookingHandler;

	@Autowired
	private AirBookingFareConfirmationHandler fareConfirmationHandler;

	@Autowired
	private AirConfirmBookingHandler confirmBookingHandler;

	@Autowired
	private AirImportPnrReviewHandler airImportPnrHandler;

	@Autowired
	private AirImportPnrBookingHandler airPostHandler;

	@Autowired
	private AirManualOrderHandler manualOrderHandler;

	@Autowired
	private AirManualReviewHandler manualReviewHandler;

	@Autowired
	private AirOrderItemHandler itemHandler;

	@Autowired
	private AirBookingRequestValidator airBookingRequestValidator;

	@Autowired
	private AirManualBookingRequestValidator airManualBookingRequestValidator;

	@Autowired
	private AirConfirmBookingRequestValidator airConfirmBookingRequestValidator;

	@Autowired
	private AirOrderMessageHandler msgHandler;

	@Autowired
	private AbortOrderHandler abortOrderHandler;

	@Autowired
	private AuditsHandler auditsHandler;

	@Autowired
	private AirInvoiceHandler invoiceHandler;

	@Autowired
	private AirReleasePNRHandler releasePNRHandler;

	@Autowired
	private AirItemDetailUpdateHandler airItemDetailUpdateHandler;

	@InitBinder("airBookingRequest")
	public void initBinderForAirBookingRequest(WebDataBinder binder) {
		binder.setValidator(airBookingRequestValidator);
	}

	@InitBinder("airConfirmBookingRequest")
	public void initBinderForAirConfirmBookingRequest(WebDataBinder binder) {
		binder.setValidator(airConfirmBookingRequestValidator);
	}

	@InitBinder("airManualBookingRequest")
	public void initBinderForAirManualBookingRequest(WebDataBinder binder) {
		binder.setValidator(airManualBookingRequestValidator);
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.BOOKING_REQUEST,
			includedRoles = {UserRole.AGENT, UserRole.AGENT_STAFF, UserRole.CORPORATE, UserRole.CORPORATE_STAFF,
					UserRole.CUSTOMER, UserRole.GUEST},
			emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.CORPORATE, UserRole.ADMIN, UserRole.AGENT,
					UserRole.SUPERVISOR})
	protected BookingResponse airBook(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirBookingRequest bookingRequest) throws Exception {
		bookingHandler.initData(bookingRequest, new BookingResponse());
		return bookingHandler.getResponse();
	}

	@RequestMapping(value = "/confirm-book", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.BOOKING_REQUEST,
			includedRoles = {UserRole.AGENT, UserRole.AGENT_STAFF, UserRole.CORPORATE, UserRole.CORPORATE_STAFF,
					UserRole.CUSTOMER, UserRole.GUEST},
			emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.CORPORATE, UserRole.ADMIN, UserRole.AGENT,
					UserRole.SUPERVISOR})
	protected BookingResponse confirmAirBook(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirConfirmBookingRequest bookingRequest) throws Exception {
		confirmBookingHandler.initData(bookingRequest, new BookingResponse());
		return confirmBookingHandler.getResponse();
	}

	@RequestMapping(value = "/import-pnr-book", method = RequestMethod.POST)
	protected BookingResponse importPnrBook(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirImportPnrBookingRequest importPnrBookRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(importPnrBookRequest.getBookingId());
		airPostHandler.initData(importPnrBookRequest, new BookingResponse());
		return airPostHandler.getResponse();
	}

	@RequestMapping(value = "/import-pnr-book/review", method = RequestMethod.POST)
	protected AirImportPnrReviewResponse reviewImportPnrBooking(HttpServletRequest request,
			HttpServletResponse response, @RequestBody @Valid AirImportPnrBookingRequest importPnrBookRequest)
			throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(importPnrBookRequest.getBookingId());
		airImportPnrHandler.initData(importPnrBookRequest, new AirImportPnrReviewResponse());
		return airImportPnrHandler.getResponse();
	}

	@RequestMapping(value = "/fare-validate", method = RequestMethod.POST)
	protected BookingResponse validateTripFare(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid BookingRequest bookingRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(bookingRequest.getBookingId());
		fareConfirmationHandler.initData(bookingRequest, new BookingResponse());
		return fareConfirmationHandler.getResponse();
	}

	@RequestMapping(value = "/msg-ticket", method = RequestMethod.POST)
	protected BaseResponse sendETicket(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirMessageRequest msgRequest) throws Exception {
		msgHandler.initData(msgRequest, new BaseResponse());
		return msgHandler.getResponse();
	}

	@RequestMapping(value = "/review/manual-order", method = RequestMethod.POST)
	protected AirReviewResponse reviewManualOrder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirManualBookingRequest bookingRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(bookingRequest.getBookingId());
		manualReviewHandler.initData(bookingRequest, new AirReviewResponse());
		return manualReviewHandler.getResponse();
	}

	@RequestMapping(value = "/manual-order", method = RequestMethod.POST)
	protected BookingResponse createManualOrder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid AirManualBookingRequest bookingRequest) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(bookingRequest.getBookingId());
		manualOrderHandler.initData(bookingRequest, new BookingResponse());
		return manualOrderHandler.getResponse();
	}

	@RequestMapping(value = "/update-item", method = RequestMethod.POST)
	protected BaseResponse updateAirOrderItem(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid List<AirOrderItem> airOrderItems) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(airOrderItems.get(0).getBookingId());
		itemHandler.initData(airOrderItems, new BaseResponse());
		return itemHandler.getResponse();
	}

	@RequestMapping(value = "/abort/{bookingId}", method = RequestMethod.GET)
	protected BaseResponse airBook(HttpServletRequest request, HttpServletResponse response,
			@PathVariable String bookingId) throws Exception {
		SystemContextHolder.getContextData().getReqIds().add(bookingId);
		abortOrderHandler.abort(bookingId);
		return new BaseResponse();
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditsHandler.fetchAudits(auditsRequestData, DbAirOrderItem.class, "bookingId"));
		return auditResponse;
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.POST)
	protected AirInvoiceResponse getInvoiceDetail(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AirInvoiceRequest airInvoiceRequest) throws Exception {
		invoiceHandler.initData(airInvoiceRequest, new AirInvoiceResponse());
		return invoiceHandler.getResponse();
	}

	@RequestMapping(value = "/unhold", method = RequestMethod.POST)
	protected BaseResponse releaseSeat(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirReleasePNRRequest cancellationRequest) throws Exception {
		releasePNRHandler.initData(cancellationRequest, new BaseResponse());
		SystemContextHolder.getContextData().getReqIds().add(cancellationRequest.getBookingId());
		return releasePNRHandler.getResponse();
	}

	@RequestMapping(value = "/airItemDetail/update", method = RequestMethod.POST)
	protected BaseResponse updateAirItemDetail(HttpServletRequest request, HttpServletResponse response,
			@RequestBody AirItemDetailRequest airItemDetailRequest) throws Exception {
		airItemDetailUpdateHandler.initData(airItemDetailRequest, new BaseResponse());
		SystemContextHolder.getContextData().getReqIds().add(airItemDetailRequest.getBookingId());
		return airItemDetailUpdateHandler.getResponse();
	}
}
