package com.tgs.services.oms.restcontroller.air;

import com.tgs.services.base.TgsValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.air.AirOrderItem;

@Component
public class AirOrderItemValidator extends TgsValidator {

	@Autowired
	private FMSCommunicator fmsComm;

	public void validate(Errors errors, String fieldName, AirOrderItem orderItem) {
		if (orderItem == null) {
			return;
		}

		registerErrors(errors, fieldName, orderItem);

		if (fmsComm.getAirportInfo(orderItem.getSource()) == null) {
			rejectValue(errors, fieldName + ".source", SystemError.INVALID_SOURCEID);
		}

		if (fmsComm.getAirportInfo(orderItem.getDest()) == null) {
			rejectValue(errors, fieldName + ".dest", SystemError.INVALID_SOURCEID);
		}
		
		if (fmsComm.getSupplierInfo(orderItem.getSupplierId()) == null) {
			rejectValue(errors, fieldName + ".supplierId", SystemError.INVALID_SUPPLIERID);
		}
		
		if (fmsComm.getAirlineInfo(orderItem.getAirlinecode()) == null) {
			rejectValue(errors, fieldName + ".airlineCode", SystemError.INVALID_AIRLINE_CODE);
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
