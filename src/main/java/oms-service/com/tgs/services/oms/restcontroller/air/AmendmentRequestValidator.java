package com.tgs.services.oms.restcontroller.air;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.amendments.AmendmentTrip;
import com.tgs.services.oms.datamodel.amendments.AmendmentTripValidatingData;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Slf4j
@Component
public class AmendmentRequestValidator extends TgsValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return AmendmentRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		AmendmentRequest amendmentRequest = (AmendmentRequest) target;

		if (StringUtils.isNotBlank(amendmentRequest.getAmendmentId())) {
			// used for polling when amendmentId present
			return;
		}
		if (StringUtils.isBlank(amendmentRequest.getBookingId())) {
			rejectValue(errors, "bookingId", SystemError.ORDER_OR_BOOKINGID_MANDATORY);
		}

		if (amendmentRequest.getType() == null) {
			rejectValue(errors, "type", SystemError.AMENDMENT_TYPE_REQ);
		}
		AmendmentTripValidatingData validatingData =
				AmendmentTripValidatingData.builder().amendmentType(amendmentRequest.getType()).build();

		registerErrors(errors, "", amendmentRequest, validatingData);

		if (errors.hasErrors()) {
			log.info("Validation errors for bookingId {},errors are {}", amendmentRequest.getBookingId(),
					errors.getFieldErrors());
		}

	}

	private void rejectValue(Errors errors, String field, SystemError systemError, Object... args) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage(args));
	}
}

