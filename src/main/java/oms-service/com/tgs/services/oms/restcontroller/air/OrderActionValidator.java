package com.tgs.services.oms.restcontroller.air;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAction;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;

@Service
public class OrderActionValidator {

	@Autowired
	private UserServiceCommunicator userService;

	public Set<OrderAction> validActions(User loggedInUser, Order order) {

		userService = SpringContext.getApplicationContext().getBean(UserServiceCommunicator.class);
		List<AreaRoleMapping> areaRoleMappingList = userService
				.getAreaRoleMapByAreaRole(ApplicationArea.CART.getAreaRoles());

		Map<UserRole, Set<AreaRole>> map = AreaRoleMapping.getMapByUserRole(areaRoleMappingList);

		UserRole userRole = loggedInUser.getRole();
		Set<OrderAction> actions = new HashSet<>();

		boolean assignedUser = loggedInUser.getUserId().equals(order.getAdditionalInfo().getAssignedUserId());
		boolean processor = false;
		boolean admin = false;
		if (map.containsKey(userRole)) {
			processor = map.get(userRole).contains(AreaRole.CART_PROCESSOR);
			admin = map.get(userRole).contains(AreaRole.CART_ADMIN);
		}

		switch (order.getStatus()) {

		case IN_PROGRESS:
		case PAYMENT_SUCCESS:
		case PAYMENT_PENDING:
		case ON_HOLD:
		case PENDING:
			if (assignedUser) {
				actions.add(OrderAction.ABORT);
			}
			if (admin) {
				actions.add(OrderAction.ASSIGN);
				if (!assignedUser) {
					actions.add(OrderAction.ASSIGNME);
				}
			} else if (processor) {
				actions.add(OrderAction.ASSIGNME);
			}

			break;

		case SUCCESS:
		case FAILED:
		case REJECTED:
		case ABORTED:

		default:
			break;

		}
		return actions;
	}
}
