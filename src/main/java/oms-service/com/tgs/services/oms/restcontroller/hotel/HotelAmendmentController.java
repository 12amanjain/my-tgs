package com.tgs.services.oms.restcontroller.hotel;

import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.hotel.HotelAmendmentDetailResponse;
import com.tgs.services.oms.restmodel.hotel.HotelAmendmentRequest;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceResponse;
import com.tgs.services.oms.restmodel.hotel.ProcessHotelAmendmentRequest;
import com.tgs.services.oms.servicehandler.hotel.HotelAmendmentInvoiceHandler;
import com.tgs.services.oms.servicehandler.hotel.ProcessHotelAmendmentHandler;
import com.tgs.services.oms.servicehandler.hotel.RaiseHotelAmendmentHandler;
import com.tgs.services.oms.servicehandler.hotel.UpdateHotelAmendmentHandler;

@RequestMapping("/oms/v1/hotel/amendment")
@RestController
public class HotelAmendmentController {

	@Autowired
	private RaiseHotelAmendmentHandler raiseHotelAmendmentHandler;

	@Autowired
	private ProcessHotelAmendmentHandler processHotelAmendmentHandler;

	@Autowired
	private UpdateHotelAmendmentHandler updateHotelAmendmentHandler;

	@Autowired
	private HotelAmendmentManager hotelAmendmentManager;
	
	@Autowired
	private HotelAmendmentInvoiceHandler amendmentInvoiceHandler;
	
	@Autowired
	private HotelProcessAmendmentRequestValidator processAmendmentValidator;

	@InitBinder("processHotelAmendmentRequest")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(processAmendmentValidator);
	}
	

	@RequestMapping(value = "/raise", method = RequestMethod.POST)
	protected AmendmentResponse raise(@RequestBody @Valid HotelAmendmentRequest request) throws Exception {

		SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
		raiseHotelAmendmentHandler.initData(request,
				AmendmentResponse.builder().amendmentItems(new ArrayList<>()).build());
		return raiseHotelAmendmentHandler.getResponse();
	}

	@RequestMapping(value = "/process", method = RequestMethod.POST)
	protected AmendmentResponse process(HttpServletRequest request,
			@RequestBody @Valid ProcessHotelAmendmentRequest amendment) throws Exception {

		SystemContextHolder.getContextData().getReqIds().add(amendment.getAmendmentId());
		processHotelAmendmentHandler.initData(amendment, AmendmentResponse.builder().build());
		AmendmentResponse response = processHotelAmendmentHandler.getResponse();
		if (response != null && CollectionUtils.isNotEmpty(response.getAmendmentItems()))
			SystemContextHolder.getContextData().getReqIds().add(response.getAmendmentItems().get(0).getBookingId());
		return response;

	}

	@RequestMapping(value = "/update/{action}", method = RequestMethod.POST)
	protected AmendmentResponse update(HttpServletRequest request, @PathVariable @NotNull AmendmentAction action,
			@RequestBody @Valid UpdateAmendmentRequest amendmentRequest) throws Exception {

		SystemContextHolder.getContextData().getReqIds().add(amendmentRequest.getAmendmentId());
		updateHotelAmendmentHandler.setAction(action);
		AmendmentResponse response = AmendmentResponse.builder()
				.amendmentItems(
						Stream.of(updateHotelAmendmentHandler.process(amendmentRequest)).collect(Collectors.toList()))
				.build();
		if (response != null && CollectionUtils.isNotEmpty(response.getAmendmentItems()))
			SystemContextHolder.getContextData().getReqIds().add(response.getAmendmentItems().get(0).getBookingId());
		return response;
	}

	@RequestMapping(value = "/detail/{amendmentId}", method = RequestMethod.GET)
	protected HotelAmendmentDetailResponse getDetail(HttpServletRequest request,
			@PathVariable @NotNull String amendmentId) {

		return hotelAmendmentManager.getAmendmentDetail(amendmentId);
	}

	@RequestMapping(value = "/invoice/{amendmentId}", method = RequestMethod.GET)
	protected HotelInvoiceResponse invoice(@PathVariable @NotNull String amendmentId) throws Exception {
		amendmentInvoiceHandler.initData(amendmentId, new HotelInvoiceResponse());
		return amendmentInvoiceHandler.getResponse();
	}


}
