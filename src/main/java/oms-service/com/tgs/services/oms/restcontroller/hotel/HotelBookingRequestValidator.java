package com.tgs.services.oms.restcontroller.hotel;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.datamodel.TravellerInfoValidatingData;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.datamodel.HotelBookingConditions;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.restcontroller.validator.HotelSupplierBookingValidator;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;

@Component
public class HotelBookingRequestValidator extends TgsValidator implements Validator {

	@Autowired
	HMSCachingServiceCommunicator cacheService;
	
	@Autowired 
	HotelSupplierBookingValidator supplierValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {

		HotelReviewResponse reviewResponse  = null;
		if (target instanceof HotelBookingRequest) {
			HotelBookingRequest bookingRequest = (HotelBookingRequest) target;
			if (bookingRequest.getBookingId() == null) {
				rejectValue(errors, "bookingId", SystemError.INVALID_BOOKING_ID);
			}
			DeliveryInfo deliveryInfo = bookingRequest.getDeliveryInfo();
			if (deliveryInfo != null) {
				registerErrors(errors, "deliveryInfo", deliveryInfo);
			}else {
				rejectValue(errors, "deliveryInfo", SystemError.NULL_DELIVERY_INFO);
			}
			List<RoomTravellerInfo> roomTravellerInfoList = bookingRequest.getRoomTravellerInfo();

			if (roomTravellerInfoList != null) {
				CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
						.set(CacheSetName.HOTEL_BOOKING.getName()).key(bookingRequest.getBookingId())
						.bins(new String[] { BinName.HOTELINFO.getName() }).kyroCompress(false).build();
				reviewResponse = cacheService.fetchValue(HotelReviewResponse.class, metaInfo);
				if (reviewResponse != null) {
					List<RoomSearchInfo> roomInfoList = reviewResponse.getQuery().getRoomInfo();
					if (roomInfoList.size() != roomTravellerInfoList.size()) {
						rejectValue(errors, "roomTravellerInfo", SystemError.INVALID_HOTEL_TRAVELLERS_LIST);
					}
					int travellerIndex = 0;
					for (RoomTravellerInfo roomTravellerInfo : roomTravellerInfoList) {
						String travellerField = "roomTravellerInfo[" + travellerIndex++ + "].travellerInfo[";
						validateTravellerInfo(TravellerInfoValidatingData.builder().build(),
								roomTravellerInfo, travellerField, errors);
					}
				}else {
					rejectValue(errors, "bookingId", SystemError.EXPIRED_BOOKING_ID);
				}
			} else {
				rejectValue(errors, "roomTravellerInfo", SystemError.INVALID_HOTEL_TRAVELLERS);
			}
		}
		if(!errors.hasErrors())
			supplierValidator.validateForSupplier(target, reviewResponse.getQuery(), errors);
	}

	public void validateTravellerInfo(TravellerInfoValidatingData validatingData, RoomTravellerInfo roomTravellerInfo,
			String roomTravellerField, Errors errors) {

		List<TravellerInfo> travellers = roomTravellerInfo.getTravellerInfo();

		if (!CollectionUtils.isEmpty(travellers)) {
			for (TravellerInfo traveller : travellers) {
				int i = 0;
				if (traveller != null) {
					if (traveller.getTitle() == null) {
						rejectValue(errors, roomTravellerField + i++ + "].title", SystemError.INVALID_TITLE);
					}
					if (traveller.getFirstName() == null
							|| !TgsStringUtils.containsOnlyAlphabetAndSpace(traveller.getFirstName())) {
						rejectValue(errors, roomTravellerField + i++ + "].firstName", SystemError.FIRSTNAME_VALIDATION);
					}

					if (traveller.getLastName() == null
							|| !TgsStringUtils.containsOnlyAlphabetAndSpace(traveller.getLastName())) {
						rejectValue(errors, roomTravellerField + i++ + "].lastName", SystemError.LASTNAME_VALIDATION);
					}
				}
			}
		}
	}

	public HotelBookingConditions getBookingConditions(BookingRequest bookingRequest) {
		return null;
	}

	protected void rejectValue(Errors errors, String field, SystemError systemError) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage());
	}

}
