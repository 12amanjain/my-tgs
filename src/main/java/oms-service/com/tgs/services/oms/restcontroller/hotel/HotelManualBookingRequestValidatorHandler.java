package com.tgs.services.oms.restcontroller.hotel;


import java.time.Duration;
import java.time.LocalTime;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.restmodel.hotel.HotelManualOrderRequest;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;

@Component
public class HotelManualBookingRequestValidatorHandler extends HotelPostBookingBaseRequestValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return HotelManualOrderRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		super.validate(target, errors);
		
		HotelPostBookingBaseRequest request = (HotelPostBookingBaseRequest) target;
		HotelInfo hInfo = request.getHInfo();
		HotelSearchQuery searchQuery = request.getSearchQuery();
		long days = Duration.between(searchQuery.getCheckinDate().atTime(LocalTime.NOON)
				, searchQuery.getCheckoutDate().atTime(LocalTime.NOON)).toDays();
		int index = 0;
		Option option = hInfo.getOptions().get(0);
		
		for(RoomInfo ri : option.getRoomInfos()) {
			validateRoomNightCount(ri,days, index, errors);
			index++;
		}
	}

	private void validateRoomNightCount(RoomInfo ri, long days, int index, Errors errors) {
		
		if(CollectionUtils.isNotEmpty(ri.getPerNightPriceInfos()) 
				&& days != ri.getPerNightPriceInfos().size()) {
			rejectValue(errors,"hInfo.options[0].roomInfos["+index+"]", SystemError.INVALID_ROOM_NIGHT);
		}
		
	}


}
