package com.tgs.services.oms.restcontroller.hotel;

import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.BookingResyncResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAction;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItemFilter;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.restmodel.OrderResponse;
import com.tgs.services.oms.restmodel.hotel.HotelAmendOrderRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingDisputeRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingInfoUpdateRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingReconciliationRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingReconciliationResponse;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelForceCancellationRequest;
import com.tgs.services.oms.restmodel.hotel.HotelImportBookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceRequest;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceResponse;
import com.tgs.services.oms.restmodel.hotel.HotelManualOrderRequest;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingAbortHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingCancellationHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingConfirmationHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingDetailHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingDisputeHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingInfoUpdateHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingReconciliationHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelBookingResyncHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelImportBookingHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelInvoiceHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelManualOrderHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelManualOrderReviewHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelModifyBookingHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelModifyBookingReviewHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelOrderItemHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelOrderListingHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelUpdateOrderHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/oms/v1/hotel")
public class HotelOrderController {

	@Autowired
	HotelBookingRequestValidator bookingRequestvalidator;

	@Autowired
	HotelBookingHandler bookingHandler;

	@Autowired
	HotelBookingDetailHandler bookingDetailHandler;

	@Autowired
	HotelOrderListingHandler orderListingHandler;

	@Autowired
	HotelOrderItemHandler itemHandler;

	@Autowired
	HotelBookingAbortHandler bookingAbortHandler;

	@Autowired
	HotelBookingCancellationHandler bookingCancellationHandler;

	@Autowired
	HotelImportBookingHandler hotelImportBookingHandler;

	@Autowired
	private HotelBookingConfirmationHandler confirmBookingHandler;

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	HotelUpdateOrderHandler updateOrderHandler;

	@Autowired
	HotelBookingResyncHandler bookingResyncHandler;

	@Autowired
	HotelBookingReconciliationHandler reconciliationHandler;

	@Autowired
	HotelBookingDisputeHandler disputeHandler;

	@Autowired
	private HotelInvoiceHandler invoiceHandler;

	@Autowired
	private HotelBookingInfoUpdateHandler bookingInfoUpdateHandler;

	@Autowired
	HotelManualOrderHandler manualOrderHandler;

	@Autowired
	HotelManualOrderReviewHandler manualOrderReviewHandler;

	@Autowired
	HotelModifyBookingReviewHandler modifyOrderReviewHandler;

	@Autowired
	HotelModifyBookingHandler modifyBookingHandler;

	@Autowired
	HotelManualBookingRequestValidatorHandler manualOrderRequestValidatorHandler;

	@Autowired
	HotelModifyBookingRequestValidator modifyBookingRequestValidatorHandler;

	@Autowired
	private AuditsHandler auditsHandler;


	@InitBinder("hotelBookingRequest")
	public void initBinder(WebDataBinder binder) {
		binder.setValidator(bookingRequestvalidator);
	}

	@InitBinder("hotelManualOrderRequest")
	public void initBinderForHotelManualBookingRequest(WebDataBinder binder) {
		binder.setValidator(manualOrderRequestValidatorHandler);
	}

	@InitBinder("hotelAmendOrderRequest")
	public void initBinderForHotelAmendBookingRequest(WebDataBinder binder) {
		binder.setValidator(modifyBookingRequestValidatorHandler);
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.BOOKING_REQUEST,
			includedRoles = {UserRole.AGENT, UserRole.AGENT_STAFF, UserRole.CORPORATE, UserRole.CORPORATE_STAFF,
					UserRole.CUSTOMER, UserRole.GUEST},
			emulatedAllowedRoles = {UserRole.CALLCENTER, UserRole.CORPORATE, UserRole.ADMIN, UserRole.AGENT,
					UserRole.SUPERVISOR})
	protected BookingResponse airBook(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody HotelBookingRequest bookingRequest) throws Exception {
		bookingHandler.initData(bookingRequest, new BookingResponse());
		return bookingHandler.getResponse();
	}

	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/booking-details", method = RequestMethod.POST)
	protected BookingDetailResponse getBookingDetails(HttpServletRequest request,
			@RequestBody BookingDetailRequest detailRequest) throws Exception {
		bookingDetailHandler.initData(detailRequest, new BookingDetailResponse());

		return bookingDetailHandler.getResponse();
	}

	@RequestMapping(value = "/update-item", method = RequestMethod.POST)
	protected BaseResponse updateHotelOrderItem(HttpServletRequest request,
			@Valid @RequestBody List<HotelOrderItem> items) throws Exception {

		itemHandler.initData(items, new BaseResponse());
		return itemHandler.getResponse();
	}


	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/orders", method = RequestMethod.POST)
	protected OrderResponse getOrderList(HttpServletRequest request, @Valid @RequestBody OrderFilter orderFilter,
			BindingResult result) throws Exception {
		orderFilter.setProducts(Arrays.asList(OrderType.HOTEL));
		orderFilter.setBookingUserIds(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), orderFilter.getBookingUserIds()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(orderFilter, result);
		if (result.hasErrors()) {
			OrderResponse response = new OrderResponse();
			response.setErrors(validator.getErrorDetailFromBindingResult(result));
			return response;
		} else {
			orderListingHandler.initData(orderFilter, new OrderResponse());
			return orderListingHandler.getResponse();
		}
	}

	@RequestMapping(value = "/abort-booking/{bookingId}", method = RequestMethod.POST)
	protected BaseResponse abortBooking(HttpServletRequest request, @PathVariable String bookingId) throws Exception {

		bookingAbortHandler.abortBooking(bookingId);
		return new BaseResponse();
	}

	@RequestMapping(value = "/cancel-booking/{bookingId}", method = RequestMethod.POST)
	protected BaseResponse cancelBooking(HttpServletRequest request, @PathVariable String bookingId) throws Exception {

		bookingCancellationHandler.cancelBooking(bookingId, false);
		return new BaseResponse();
	}

	@RequestMapping(value = "/force-cancel-booking", method = RequestMethod.POST)
	protected BaseResponse forcefullyCancelBooking(HttpServletRequest request,
			@RequestBody HotelForceCancellationRequest cancellationRequest) throws Exception {
		bookingCancellationHandler.forcefullyCancelBooking(cancellationRequest);
		return new BaseResponse();
	}

	@RequestMapping(value = "/confirm-book", method = RequestMethod.POST)
	protected BookingResponse confirmAirBook(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody BookingRequest bookingRequest) throws Exception {
		confirmBookingHandler.initData(bookingRequest, new BookingResponse());
		return confirmBookingHandler.getResponse();
	}

	@RequestMapping(value = "/fare-validate", method = RequestMethod.POST)
	protected BookingResponse validateTripFare(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid BookingRequest bookingRequest) throws Exception {
		BookingResponse res = new BookingResponse();
		res.setBookingId(bookingRequest.getBookingId());
		return res;
	}

	@RequestMapping(value = "/updateorder/{action}", method = RequestMethod.POST)
	protected BookingDetailResponse updateOrderAction(HttpServletRequest request,
			@PathVariable @NotNull OrderAction action, @RequestBody Order detailRequest) throws Exception {
		updateOrderHandler.initData(detailRequest, BookingDetailResponse.builder().build());
		updateOrderHandler.setAction(action);
		return updateOrderHandler.getResponse();
	}

	@RequestMapping(value = "/import-booking", method = RequestMethod.POST)
	protected BookingDetailResponse reviewOrder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid HotelImportBookingRequest importBookingRequest) throws Exception {
		hotelImportBookingHandler.initData(importBookingRequest, new BookingDetailResponse());
		return hotelImportBookingHandler.getResponse();
	}

	@RequestMapping(value = "/booking-resync", method = RequestMethod.POST)
	protected BookingResyncResponse resyncBooking(HttpServletRequest request, HttpServletResponse response,
			@RequestBody BookingDetailRequest detailRequest) throws Exception {
		bookingResyncHandler.initData(detailRequest, new BookingResyncResponse());
		return bookingResyncHandler.getResponse();
	}

	@RequestMapping(value = "/booking-reconcile", method = RequestMethod.POST)
	protected BaseResponse reconcileBooking(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelBookingReconciliationRequest reconciliationRequest) throws Exception {
		reconciliationHandler.initData(reconciliationRequest, new BaseResponse());
		return reconciliationHandler.getResponse();
	}

	@RequestMapping(value = "/update-reconcilation-status", method = RequestMethod.POST)
	protected BaseResponse updateReconciledBooking(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelBookingReconciliationRequest updateRequest) throws Exception {
		reconciliationHandler.updateReconciledBooking(updateRequest);
		return new BaseResponse();
	}

	@RequestMapping(value = "/update-dispute-info", method = RequestMethod.POST)
	protected BaseResponse updateDisputeInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelBookingDisputeRequest updateRequest) throws Exception {
		disputeHandler.initData(updateRequest, new BaseResponse());
		return disputeHandler.getResponse();
	}

	@RequestMapping(value = "/search-reconciled-booking", method = RequestMethod.POST)
	protected HotelBookingReconciliationResponse searchReconciledBooking(HttpServletRequest request,
			HttpServletResponse response, @RequestBody HotelBookingReconciliationRequest searchRequest)
			throws Exception {
		return reconciliationHandler.getReconciliationInfo(searchRequest);
	}

	@RequestMapping(value = "/invoice", method = RequestMethod.POST)
	protected HotelInvoiceResponse getInvoiceDetail(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody HotelInvoiceRequest hotelInvoiceRequest) throws Exception {
		invoiceHandler.initData(hotelInvoiceRequest, new HotelInvoiceResponse());
		return invoiceHandler.getResponse();
	}

	@RequestMapping(value = "/update-hotel-booking-info", method = RequestMethod.POST)
	protected BaseResponse updateHotelBookingInfo(HttpServletRequest request,
			@RequestBody HotelBookingInfoUpdateRequest updateRequest) throws Exception {
		bookingInfoUpdateHandler.initData(updateRequest, new BaseResponse());
		return bookingInfoUpdateHandler.getResponse();
	}

	@RequestMapping(value = "/review/manual-order", method = RequestMethod.POST)
	protected HotelReviewResponse reviewManualOrder(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody HotelManualOrderRequest manualOrderRequest) throws Exception {
		manualOrderReviewHandler.initData(manualOrderRequest, new HotelReviewResponse());
		return manualOrderReviewHandler.getResponse();
	}

	@RequestMapping(value = "/manual-order", method = RequestMethod.POST)
	protected BookingResponse manualOrder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelManualOrderRequest manualOrderRequest) throws Exception {
		manualOrderHandler.initData(manualOrderRequest, new BookingResponse());
		return manualOrderHandler.getResponse();
	}

	@RequestMapping(value = "/review/modify-order", method = RequestMethod.POST)
	protected HotelReviewResponse reviewModifyOrder(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody HotelAmendOrderRequest reviewAmendmentRequest) throws Exception {
		modifyOrderReviewHandler.initData(reviewAmendmentRequest, new HotelReviewResponse());
		return modifyOrderReviewHandler.getResponse();
	}

	@RequestMapping(value = "/modify-order", method = RequestMethod.POST)
	protected BookingResponse amendOrder(HttpServletRequest request, HttpServletResponse response,
			@RequestBody HotelAmendOrderRequest amendmentRequest) throws Exception {
		modifyBookingHandler.initData(amendmentRequest, new BookingResponse());
		return modifyBookingHandler.getResponse();
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditsHandler.fetchAudits(auditsRequestData, DbHotelOrderItem.class, "bookingId"));
		return auditResponse;
	}
}
