package com.tgs.services.oms.restcontroller.hotel;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.restmodel.hotel.UpdateOrderRequest;
import com.tgs.services.oms.servicehandler.hotel.UpdateSupplierBookingStatusHandler;
import com.tgs.services.oms.servicehandler.hotel.UpdateSupplierHotelConfirmationUpdateHandler;
import com.tgs.services.oms.servicehandler.hotel.HotelUpdateOrderStatusHandler;
import com.tgs.services.oms.servicehandler.hotel.PayLaterManager;


@RestController
@RequestMapping("/oms/v1/hotel/job")
public class HotelOrderJobController {
	
	@Autowired
	PayLaterManager payLaterManager;
	
	@Autowired
	private HotelUpdateOrderStatusHandler orderStatusHandler;	
	
	@Autowired
	private UpdateSupplierBookingStatusHandler agodaBookingStatusService;
	
	@Autowired
	private UpdateSupplierHotelConfirmationUpdateHandler agodaHotelConfirmationUpdateHandler;

	
	@RequestMapping(value = "/cancel-unconfirmed-booking", method = RequestMethod.POST)
	protected BaseResponse payLater(HttpServletRequest request) {
		payLaterManager.processCancellationJob();
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/notify-unconfirmed-booking/{hoursFrom}/{hoursTo}" , method = RequestMethod.POST)
	protected BaseResponse notifyFirst(HttpServletRequest request , @PathVariable String hoursFrom , @PathVariable String hoursTo ) {
		payLaterManager.processNotificationJob(hoursFrom , hoursTo);
		return new BaseResponse();
	}
	
	@RequestMapping(value = "/update-order-status" , method = RequestMethod.POST)
	protected BaseResponse updateOrderStatus(HttpServletRequest request,
			@Valid @RequestBody OrderFilter orderFilter) throws Exception {
		orderStatusHandler.initData(orderFilter, new BaseResponse());
		return orderStatusHandler.getResponse();
	}
	@RequestMapping(value = "/update-status", method = RequestMethod.POST)
	protected void updateOrderStatus(@RequestBody UpdateOrderRequest retrieveBookingDetailRequest,
			HttpServletRequest request, HttpServletResponse response) throws Exception {

		agodaBookingStatusService.initData(retrieveBookingDetailRequest, new BaseResponse());
		agodaBookingStatusService.getResponse();
	}

	@RequestMapping(value = "/update-supplierreference", method = RequestMethod.POST)
	protected void updateSupplierReference(@RequestBody UpdateOrderRequest retrieveBookingDetailRequest,
			HttpServletRequest request, HttpServletResponse response) throws Exception {
		agodaHotelConfirmationUpdateHandler.initData(retrieveBookingDetailRequest, new BaseResponse());		
		agodaHotelConfirmationUpdateHandler.getResponse();
	}
}
