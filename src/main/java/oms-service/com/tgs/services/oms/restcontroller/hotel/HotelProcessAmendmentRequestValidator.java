package com.tgs.services.oms.restcontroller.hotel;

import java.util.Objects;

import org.apache.commons.lang3.BooleanUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.restmodel.hotel.ProcessHotelAmendmentRequest;

@Component
public class HotelProcessAmendmentRequestValidator extends TgsValidator implements Validator{

	@Override
	public boolean supports(Class<?> clazz) {
		return ProcessHotelAmendmentRequest.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		
		ProcessHotelAmendmentRequest request = (ProcessHotelAmendmentRequest)target;
		HotelInfo modifiedHotelInfo = request.getModifiedInfo().getHInfo();
		if(Objects.isNull(modifiedHotelInfo)) return;
		
		checkIfAllRoomsDeleted(modifiedHotelInfo, errors);
		
	}

	private void checkIfAllRoomsDeleted(HotelInfo modifiedHotelInfo, Errors errors) {
		
		long nonDeletedRooms = modifiedHotelInfo.getOptions().get(0).getRoomInfos().stream().filter(ri -> !BooleanUtils.isTrue(ri.getIsDeleted())).count();
		if(nonDeletedRooms == 0) {
			rejectValue(errors, "hInfo.options[0]", SystemError.NOT_ALLOWED_TO_DELETE_ALL_ROOMS);
		}
	}
	
	
	
	protected void rejectValue(Errors errors, String field, SystemError systemError) {
		errors.rejectValue(field, systemError.errorCode(), systemError.getMessage());
	}

}
