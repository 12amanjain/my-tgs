package com.tgs.services.oms.restcontroller.validator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;

@Service
public class DotwBookingValidator implements HotelSupplierBookingValidator {

	@Override
	public <T> void validateForSupplier(T t, HotelSearchQuery searchQuery, Errors errors) {
		
		Set<String> firstNameLastNameSet = new HashSet<>();
		HotelBookingRequest bookingRequest = (HotelBookingRequest) t;
		List<RoomTravellerInfo> roomTravellerInfo = bookingRequest.getRoomTravellerInfo();
		roomTravellerInfo.forEach(roomTraveller -> {
			roomTraveller.getTravellerInfo().forEach(traveller -> {
				String travellerName = traveller.getFirstName().replaceAll("\\s", "") 
						+ traveller.getLastName().replaceAll("\\s", "");
				if(firstNameLastNameSet.contains(travellerName)) {
					SystemError systemError = SystemError.DUPLICATE_FIRSTNAME_LASTNAME;
					errors.rejectValue("roomTravellerInfo[0].travellerInfo[0]", systemError.errorCode(), systemError.getMessage());
				}else firstNameLastNameSet.add(travellerName);
				if(traveller.getFirstName().length() <= 1 || traveller.getLastName().length() <=1) {
					SystemError systemError = SystemError.MIN_PASSENGER_NAME;
					errors.rejectValue("roomTravellerInfo[0].travellerInfo[0]", systemError.errorCode(), systemError.getMessage());
				}
			});
		});
		
	}

}
