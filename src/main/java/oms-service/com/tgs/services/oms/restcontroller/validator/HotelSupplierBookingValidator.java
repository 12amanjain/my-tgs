package com.tgs.services.oms.restcontroller.validator;

import org.springframework.validation.Errors;

import com.tgs.services.hms.datamodel.HotelSearchQuery;

public interface HotelSupplierBookingValidator {
	
	public <T> void validateForSupplier(T t,HotelSearchQuery searchQuery, Errors errors);

}
