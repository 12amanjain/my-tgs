package com.tgs.services.oms.servicehandler;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.oms.datamodel.InvoiceInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.ums.datamodel.GSTInfo;
import com.tgs.services.ums.datamodel.User;

@Service
public abstract class InvoiceHandler<T, RT extends BaseResponse> extends ServiceHandler<T, RT> {

	@Autowired
	private UserServiceCommunicator userServiceCommunicator;

	public InvoiceInfo getAgentInvoiceInfo(User user) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		if (user != null) {
			User parentUser = user;
			if (StringUtils.isNotEmpty(user.getParentUserId())) {
				parentUser = userServiceCommunicator.getUserFromCache(user.getParentUserId());
			}
			AddressInfo addressInfo = AddressInfo.builder().build();
			CityInfo cityInfo = CityInfo.builder().build();
			if (parentUser.getAddressInfo() != null) {
				addressInfo = parentUser.getAddressInfo();
				if (addressInfo.getCityInfo() != null) {
					cityInfo = addressInfo.getCityInfo();
				}
			}
			GSTInfo gstInfo = GSTInfo.builder().build();
			if (parentUser.getGstInfo() != null) {
				gstInfo = parentUser.getGstInfo();
			}
			invoiceInfo = InvoiceInfo.builder().name(parentUser.getName()).email(parentUser.getEmail())
					.phone(ObjectUtils.firstNonNull(parentUser.getPhone(), parentUser.getMobile()))
					.address(addressInfo.getAddress()).city(cityInfo.getName()).state(cityInfo.getState())
					.pincode(addressInfo.getPincode()).gstNumber(gstInfo.getGstNumber()).build();
			return invoiceInfo;
		}
		return invoiceInfo;
	}

	public InvoiceInfo getClientInvoiceInfo(ClientGeneralInfo clientInfo) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		GstInfo gstInfo = new GstInfo();
		if (clientInfo.getGstInfo() != null) {
			gstInfo = clientInfo.getGstInfo();
		}
		if (clientInfo != null) {
			invoiceInfo = InvoiceInfo.builder().name(clientInfo.getName()).email(clientInfo.getEmail())
					.phone(clientInfo.getWorkPhone())
					.address(StringUtils.join(clientInfo.getAddress1(), clientInfo.getAddress2()))
					.city(clientInfo.getCity()).state(clientInfo.getState()).pincode(clientInfo.getPostalCode())
					.gstNumber(gstInfo.getGstNumber()).build();
		}
		return invoiceInfo;
	}

	public InvoiceInfo getCustomerInvoiceInfo(Order order) {
		if (order != null && order.getDeliveryInfo() != null) {
			String email = null, phone = null;
			if (CollectionUtils.isNotEmpty(order.getDeliveryInfo().getEmails())) {
				email = order.getDeliveryInfo().getEmails().get(0);
			}
			if (CollectionUtils.isNotEmpty(order.getDeliveryInfo().getContacts())) {
				phone = order.getDeliveryInfo().getContacts().get(0);
			}
			return InvoiceInfo.builder().email(email).phone(phone).build();
		}
		return null;
	}
}
