package com.tgs.services.oms.servicehandler;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAction;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.restcontroller.air.OrderActionValidator;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.OrderResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class OrderListingHandler extends ServiceHandler<OrderFilter, OrderResponse> {

	@Autowired
	OrderService orderService;

	@Autowired
	UserServiceCommunicator userComm;

	@Autowired
	AirOrderItemManager itemManager;

	@Autowired
	UpdateOrderHandler updateHandler;

	@Autowired
	private GeneralServiceCommunicator gSCommunicator;

	@Autowired
	private OrderActionValidator actionValidator;

	@Override
	public void beforeProcess() throws Exception {
		// if (CollectionUtils.isEmpty(request.getProducts())) {
		// throw new CustomGeneralException(SystemError.PRODUCT_NON_NULL);
		// }
		if (request.getPageAttr() == null) {
			request.setPageAttr(new PageAttributes());
			request.getPageAttr().setSize(10000);
		}

		// if (request.getProducts().get(0).equals(OrderType.AIR)) {
		List<ProcessedBookingDetail> pbds = itemManager.getProcessedItemDetails(request);
		doAutoAssignment(pbds);
		addOrderOpenAudit();
		// }

	}

	private void doAutoAssignment(List<ProcessedBookingDetail> pbds) throws Exception {
		if (CollectionUtils.isNotEmpty(pbds)) {
			response.setDetails(pbds);
			ProcessedBookingDetail detail = pbds.get(0);
			if (false && detail != null && BooleanUtils.isTrue(request.getCheckCartAssignment())
					&& response.getDetails().size() == 1 && detail.getOrder().getStatus().equals(OrderStatus.PENDING)
					&& StringUtils.isBlank(detail.getOrder().getAdditionalInfo().getAssignedUserId())
					&& UserUtils.isMidOfficeRole(SystemContextHolder.getContextData().getUser().getRole().getCode())) {
				log.info("Auto assignment for bookingId {}", detail.getOrder().getBookingId());
				updateHandler.initData(new GsonMapper<>(detail.getOrder(), Order.class).convert(),
						BookingDetailResponse.builder().build());
				updateHandler.setAction(OrderAction.ASSIGNME);
				updateHandler.getResponse();
				response.setDetails(itemManager.getProcessedItemDetails(request));
				detail = response.getDetails().get(0);
				detail.getOrder()
						.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(),
								Order.builder().status(detail.getOrder().getStatus())
										.additionalInfo(detail.getOrder().getAdditionalInfo()).build()));
			}
		}
	}

	private void addOrderOpenAudit() {
		if (StringUtils.isNotBlank(request.getBookingId())) {
			ContextData contextData = SystemContextHolder.getContextData();
			SystemAudit systemAudit = SystemAudit.builder().userId(contextData.getUser().getParentUserId())
					.bookingId(request.getBookingId()).auditType(AuditAction.ORDER_OPEN)
					.loggedInUserId(contextData.getUser().getEmulateOrLoggedInUserId())
					.ip(contextData.getHttpHeaders().getIp()).build();
			gSCommunicator.addToSystemAudit(systemAudit);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {

	}

}
