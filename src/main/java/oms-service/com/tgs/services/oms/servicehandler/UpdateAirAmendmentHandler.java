package com.tgs.services.oms.servicehandler;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.AmendmentHandlerFactory;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.Amendments.AmendmentMessagingClient;
import com.tgs.services.oms.Amendments.Processors.AirAmendmentProcessor;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UpdateAirAmendmentHandler {

	@Autowired
	private OrderService orderService;

	// @Autowired
	// private AirOrderItemService airOrderItemService;

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Autowired
	private AmendmentService service;

	@Autowired
	private AmendmentMessagingClient messageHandler;

	@Autowired
	private PaymentServiceCommunicator paymentServiceCommunicator;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private GstInfoService gstService;

	@Autowired
	private DealInventoryCommunicator inventorySrvComm;

	@Autowired
	private AirOrderItemManager airOrderItemManager;

	@Setter
	private Amendment amendment;

	private DbOrder dbOrder;

	@Setter
	private boolean paymentRequired;

	private List<AirOrderItem> orgAirOrderItems;

	@Setter
	private AmendmentAction action;

	private static final short AMD_ASSIGNMENT_CAP = 100;

	private void validateAction() {
		if (amendment.getStatus().equals(AmendmentStatus.SUCCESS)
				|| amendment.getStatus().equals(AmendmentStatus.REJECTED))
			throw new CustomGeneralException(SystemError.AMENDMENT_NOT_UPDATABLE);

		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment).contains(action))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
	}

	public Amendment process(UpdateAmendmentRequest request) throws Exception {

		amendment = service.findByAmendmentId(request.getAmendmentId());
		validateAction();
		paymentRequired =
				!AmendmentHelper.isQuotation(amendment) && !amendment.getAmendmentType().equals(AmendmentType.REISSUE)
						&& !amendment.getStatus().equals(AmendmentStatus.PAYMENT_SUCCESS);

		if (!StringUtils.isEmpty(request.getFinishingNotes())) {
			amendment.getAdditionalInfo().setFinishingNotes(request.getFinishingNotes());
		}

		if (action.equals(AmendmentAction.ASSIGN)) {
			assign(request.getAssignedUserId());
		}

		if (action.equals(AmendmentAction.ASSIGNME)) {
			assign(SystemContextHolder.getContextData().getUser().getUserId());
		}

		if (action.equals(AmendmentAction.ABORT)) {
			amendment.setStatus(AmendmentStatus.REJECTED);
			AmendmentHelper.updateAssignments(null, amendment);
		}

		if (action.equals(AmendmentAction.PENDING_WITH_SUPPLIER)) {
			amendment.setStatus(AmendmentStatus.PENDING_WITH_SUPPLIER);
			amendment.getAirAdditionalInfo().setPwsUserId(amendment.getAssignedUserId());
			amendment.setAssignedUserId(null);
			amendment.getAirAdditionalInfo().setPwsReasons(request.getPwsReasons());
			amendment.getAirAdditionalInfo().setPwsEnterTime(LocalDateTime.now());
			AmendmentHelper.updateAssignments(null, amendment);
		}

		if (action.equals(AmendmentAction.MERGE)) {
			processPayment();
			merge();
		}

		service.save(amendment);
		sendMail();
		amendment
				.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
		return amendment;
	}

	private void sendMail() {

		if (orgAirOrderItems == null)
			orgAirOrderItems = BaseModel.toDomainList(airOrderItemManager.findByBookingId(amendment.getBookingId()));

		orgAirOrderItems = AmendmentHelper.filterItemsNPax(amendment, orgAirOrderItems);

		if (action.equals(AmendmentAction.ABORT)) {
			messageHandler.sendMail(amendment, amendment.getAmendmentType().abortMailKey(), orgAirOrderItems);
			messageHandler.sendSMS(amendment, SmsTemplateKey.AMENDMENT_ABORT_SMS);
		}
		if (action.equals(AmendmentAction.PENDING_WITH_SUPPLIER)) {
			messageHandler.sendMail(amendment, EmailTemplateKey.AMENDMENT_PWS_EMAIL, orgAirOrderItems);
			messageHandler.sendSMS(amendment, SmsTemplateKey.AMENDMENT_PWS_SMS);
		}
		if (action.equals(AmendmentAction.MERGE)) {
			messageHandler.sendMail(amendment, amendment.getAmendmentType().mergeMailKey(), orgAirOrderItems);
			messageHandler.sendSMS(amendment, SmsTemplateKey.AMENDMENT_MERGE_SMS);
		}
		if (action.equals(AmendmentAction.PROCESS)) {
			messageHandler.sendSMS(amendment, SmsTemplateKey.AMENDMENT_PROCESS_SMS);
		}
	}

	@Transactional
	public void merge() {
		log.info("Merging Amendment for order {} ", amendment.getAmendmentId());
		dbOrder = orderService.findByBookingId(amendment.getBookingId());
		applyAmendment();
		amendment.setStatus(AmendmentStatus.SUCCESS);
		AmendmentHelper.updateAssignments(null, amendment);
		service.save(amendment);
	}

	private void assign(String userId) {
		List<Amendment> amendments = service.search(AmendmentFilter.builder()
				.assignedUserIdIn(Collections.singletonList(userId)).statusIn(AmendmentHelper.assignedStatuses())
				.orderTypeIn(Collections.singletonList(OrderType.AIR)).build());

		if (amendments.size() >= getAmendmentCapacity()) {
			throw new CustomGeneralException(SystemError.AMENDMENT_ASSIGNMENT_LIMIT);
		}
		amendment.setAssignedUserId(userId);
		amendment.setAssignedOn(LocalDateTime.now());
		if (amendment.getStatus().equals(AmendmentStatus.REQUESTED))
			amendment.setStatus(AmendmentStatus.ASSIGNED);
		else
			amendment.setStatus(AmendmentStatus.PROCESSING);
		AmendmentHelper.updateAssignments(userId, amendment);
	}

	private int getAmendmentCapacity() {
		ClientGeneralInfo clientInfo = OmsHelper.getClientInfo();

		if (clientInfo.getAmendmentAssignCap() != null && clientInfo.getAmendmentAssignCap() > 0) {
			return clientInfo.getAmendmentAssignCap();
		}
		return AMD_ASSIGNMENT_CAP;
	}

	@Transactional
	public void processPayment() {
		if (!paymentRequired || amendment.getAmount() == 0)
			return;
		boolean isRefund = amendment.getAmount() < 0;
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		try {

			Double remaining = amendment.getAmount();
			for (Double amount : amendment.getAdditionalInfo().getChargedBySuppplierRefundAmounts()) {
				remaining -= amount;
				isRefund = amount < 0;
				PaymentRequest paymentRequest = constructRefundRequest(amount, isRefund, PaymentMedium.VIRTUAL_PAYMENT);
				paymentRequest.getAdditionalInfo().setIsChargedBySupplier(true);
				paymentRequestList.add(paymentRequest);
			}
			for (Double amount : amendment.getAdditionalInfo().getNotChargedBySuppplierRefundAmounts()) {
				remaining -= amount;
				isRefund = amount < 0;
				PaymentRequest paymentRequest = constructRefundRequest(amount, isRefund, PaymentMedium.VIRTUAL_PAYMENT);
				paymentRequestList.add(paymentRequest);
			}
			isRefund = remaining < 0;
			if (Math.abs(remaining) > 0.5) {
				PaymentRequest paymentRequest = constructRefundRequest(remaining, isRefund, null);
				paymentRequestList.add(paymentRequest);
			}
			if (amendment.getAdditionalInfo().isRecallCommission()
					&& ObjectUtils.firstNonNull(amendment.getAdditionalInfo().getTotalCommissionRecalled(), 0d) > 0) {
				paymentRequestList.add(createCommissionRecallRequest());
			}
			if (ObjectUtils.firstNonNull(amendment.getAdditionalInfo().getTotalPartnerMarkupRecalled(), 0d) > 0) {
				paymentRequestList.add(createPartnerMarkUpRecallRequest());
			}
			if (ObjectUtils.firstNonNull(amendment.getAdditionalInfo().getTotalPartnerCommissionRecalled(), 0d) > 0) {
				paymentRequestList.add(createPartnerCommissionRecallRequest());
			}
			paymentServiceCommunicator.doPaymentsUsingPaymentRequests(paymentRequestList);
			amendment.setStatus(AmendmentStatus.PAYMENT_SUCCESS);
		} catch (Exception ex) {
			amendment.setStatus(AmendmentStatus.PAYMENT_FAILED);
			throw ex;
		} finally {
			service.save(amendment);
		}
	}

	private PaymentRequest constructRefundRequest(Double amount, boolean isRefund, PaymentMedium medium) {
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
		PaymentRequest paymentRequest = WalletPaymentRequest.builder().build().setAmount(Math.abs(amount))
				.setPaymentMedium(medium).setProduct(Product.getProductFromId(amendment.getBookingId()))
				.setAmendmentId(amendment.getAmendmentId())
				.setOpType(isRefund ? PaymentOpType.CREDIT : PaymentOpType.DEBIT)
				.setPayUserId(amendment.getBookingUserId()).setRefId(amendment.getBookingId())
				.setTransactionType(
						isRefund ? PaymentTransactionType.REFUND : PaymentTransactionType.PAID_FOR_AMENDMENT)
				.setAdditionalInfo(additionalInfo);
		if (isRefund) {
			evalRefundMedium(paymentRequest);
		}
		return paymentRequest;
	}

	private PaymentRequest createCommissionRecallRequest() {
		Double amount = amendment.getAdditionalInfo().getTotalCommissionRecalled();
		Double tds = null;
		if (amendment.getAdditionalInfo().isReturnTds()) {
			tds = amendment.getAdditionalInfo().getTdsToReturn();
			amount -= tds;
			tds = -1 * tds;
		}
		return constructRecallPaymentRequest(amount, tds, PaymentTransactionType.COMMISSION,
				amendment.getBookingUserId());
	}

	private PaymentRequest createPartnerMarkUpRecallRequest() {
		Double amount = amendment.getAdditionalInfo().getTotalPartnerMarkupRecalled();
		Double tds = null;
		if (ObjectUtils.firstNonNull(amendment.getAdditionalInfo().getPartnerMarkUpTdsToReturn(), 0d) > 0) {
			tds = amendment.getAdditionalInfo().getPartnerMarkUpTdsToReturn();
			amount -= tds;
			tds = -1 * tds;
		}
		PaymentRequest request = constructRecallPaymentRequest(amount, tds, PaymentTransactionType.PARTNER_MARKUP,
				amendment.getPartnerId());
		request.setInternalQuery(true);
		return request;
	}

	private PaymentRequest createPartnerCommissionRecallRequest() {
		Double amount = amendment.getAdditionalInfo().getTotalPartnerCommissionRecalled();
		Double tds = null;
		if (ObjectUtils.firstNonNull(amendment.getAdditionalInfo().getPartnerCommissionTdsToReturn(), 0d) > 0) {
			tds = amendment.getAdditionalInfo().getPartnerCommissionTdsToReturn();
			amount -= tds;
			tds = -1 * tds;
		}
		PaymentRequest request = constructRecallPaymentRequest(amount, tds, PaymentTransactionType.PARTNER_COMMISSION,
				amendment.getPartnerId());
		request.setInternalQuery(true);
		return request;
	}

	private PaymentRequest constructRecallPaymentRequest(Double amount, Double tds, PaymentTransactionType txnType,
			String payUserId) {
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
		User payUser = userService.getUserFromCache(payUserId);
		return WalletPaymentRequest.builder().build().setAmount(amount)
				.setTds(tds != null ? BigDecimal.valueOf(tds) : null)
				.setProduct(Product.getProductFromId(amendment.getBookingId()))
				.setAmendmentId(amendment.getAmendmentId()).setOpType(PaymentOpType.DEBIT).setPayUserId(payUserId)
				.setPartnerId(payUser.getPartnerId()).setRefId(amendment.getBookingId())
				.setAdditionalInfo(additionalInfo).setTransactionType(txnType);
	}

	private void evalRefundMedium(PaymentRequest paymentRequest) {
		PaymentMedium medium = PaymentMedium.FUND_HANDLER;
		List<Payment> paymentList =
				paymentServiceCommunicator.search(PaymentFilter.builder().refId(amendment.getBookingId())
						.type(PaymentTransactionType.PAID_FOR_ORDER).status(PaymentStatus.SUCCESS).build());

		paymentList.sort(Comparator.comparing(Payment::getCreatedOn));
		PaymentMedium bookingMedium = paymentList.get(0).getPaymentMedium();
		BigDecimal extMediumAmount = Payment.sum(paymentList.stream()
				.filter(p -> p.getPaymentMedium().isExternalPaymentMedium()).collect(Collectors.toList()));
		PaymentFact fact = PaymentFact.builder().medium(bookingMedium).build();
		List<PaymentConfigurationRule> rules =
				paymentServiceCommunicator.getApplicableRulesOutput(fact, PaymentRuleType.REFUND, bookingMedium);
		if (!CollectionUtils.isEmpty(rules)) {
			medium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
		}
		if (medium.isExternalPaymentMedium() && extMediumAmount.compareTo(paymentRequest.getAmount()) < 0) {
			medium = PaymentMedium.FUND_HANDLER;
		}
		if (!medium.equals(PaymentMedium.FUND_HANDLER)) {
			paymentRequest.setPaymentMedium(medium);
		}
	}

	private void applyAmendment() {
		List<DbAirOrderItem> orgDbAirOrderItems = airOrderItemManager.findByBookingId(amendment.getBookingId());
		orgAirOrderItems = BaseModel.toDomainList(orgDbAirOrderItems);
		amendment.getAirAdditionalInfo().setOrderPreviousSnapshot(orgAirOrderItems);
		if (AmendmentHelper.isQuotation(amendment)) {
			amendment.getAirAdditionalInfo().setOrderNextSnapshot(orgAirOrderItems);
			return;
		}
		AirAmendmentProcessor amendmentHandler = AmendmentHandlerFactory.initHandler(amendment,
				amendment.getModifiedInfo() == null ? new ArrayList<>()
						: amendment.getModifiedInfo().getAirOrderItems(),
				orgDbAirOrderItems, userService.getUserFromCache(amendment.getBookingUserId()));

		if (dbOrder == null) {
			dbOrder = orderService.findByBookingId(amendment.getBookingId());
		}

		GstInfo gst = Optional.ofNullable(gstService.findByBookingId(amendment.getBookingId()))
				.orElseGet(DbOrderBilling::new).toDomain();

		amendmentHandler.setOrder(dbOrder);

		amendmentHandler.setGstInfo(gst);
		ArrayList<String> airOrderIds = getAirOrderIds(amendment.getModifiedInfo().getAirOrderItems());
		List<AirOrderItem> patchedAirOrderItems = amendmentHandler.patchAirOrderItems(true);
		takeActionOnInventoryAmendments(patchedAirOrderItems);
		updateTravellerStatus(patchedAirOrderItems, airOrderIds);
		updateOrderTotal(dbOrder, patchedAirOrderItems);
		updateItemStatus(patchedAirOrderItems);
		save(dbOrder, new DbAirOrderItem().toDbList(patchedAirOrderItems));
		amendment.getAirAdditionalInfo().setOrderNextSnapshot(patchedAirOrderItems);
	}

	private void takeActionOnInventoryAmendments(List<AirOrderItem> patchedAirOrderItems) {
		ClientGeneralInfo clientGeneralInfo =
				(ClientGeneralInfo) gsCommunicator.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
		boolean isAmendmentApplicable = amendment.getAmendmentType().equals(AmendmentType.REISSUE)
				|| amendment.getAmendmentType().equals(AmendmentType.CANCELLATION)
				|| amendment.getAmendmentType().equals(AmendmentType.VOIDED);
		if (BooleanUtils.isTrue(clientGeneralInfo.getDecreaseInventorySeatsOnAmendment()) && isAmendmentApplicable) {
			patchedAirOrderItems.forEach(airOrderItem -> {
				// We have only 1 inventory for connecting flight
				if (airOrderItem.getAdditionalInfo().getSegmentNo() == 0
						&& airOrderItem.getAdditionalInfo().getInventoryId() != null) {
					int seats = airOrderItem.getTravellerInfo().stream()
							.filter(t -> !t.getPaxType().equals(PaxType.INFANT)).collect(Collectors.toList()).size();
					inventorySrvComm.revertSeatsSold(airOrderItem.getAdditionalInfo().getInventoryId(),
							airOrderItem.getDepartureTime().toLocalDate(), seats);
				}
			});
		}
	}

	private void updateOrderTotal(DbOrder dbOrder, List<AirOrderItem> airOrderItems) {
		double total = airOrderItems.stream().mapToDouble(AirOrderItem::getAmount).sum();
		dbOrder.setAmount(total);
	}

	private void updateItemStatus(List<AirOrderItem> patchedAirOrderItems) {
		if (amendment.getAmendmentType().equals(AmendmentType.CANCELLATION)) {
			for (AirOrderItem airOrderItem : patchedAirOrderItems) {
				if (airOrderItem.isAllTravellerCancelled() && AirItemStatus.getAirItemStatus(airOrderItem.getStatus())
						.nextStatusSet().contains(AirItemStatus.CANCELLED)) {
					log.info("Update cancellation status for item {}", airOrderItem.getBookingId());
					airOrderItem.setStatus(AirItemStatus.CANCELLED.getCode());
				}
			}
		}
	}


	@Transactional
	private void save(DbOrder dbOrder, List<DbAirOrderItem> airOrderItems) {
		airOrderItemManager.save(airOrderItems, dbOrder.toDomain(), null);
	}

	private void updateTravellerStatus(List<AirOrderItem> patchedAirOrderItems, ArrayList<String> airOrderIds) {
		airOrderIds.forEach(id -> {
			patchedAirOrderItems.forEach(item -> {
				if (id.equals(Long.toString(item.getId()))) {
					InventoryOrderFilter filter =
							InventoryOrderFilter.builder().referenceIdIn(Arrays.asList(item.getBookingId()))
									.inventoryIdIn(Arrays.asList(item.getAdditionalInfo().getInventoryId())).build();
					List<InventoryOrder> inventories = inventorySrvComm.fetchInventoryOrders(filter);
					if (!CollectionUtils.isEmpty(inventories)) {
						InventoryOrder invOrder = inventories.get(0);
						invOrder.getTravellerInfo().forEach(traveller -> {
							item.getTravellerInfo().forEach(patchedTraveller -> {
								if (traveller.getId().equals(patchedTraveller.getId())) {
									traveller.setStatus(patchedTraveller.getStatus());
								}
							});
						});
						inventorySrvComm.saveInventoryOrder(invOrder);
					}
				}
			});
		});
	}

	private ArrayList<String> getAirOrderIds(List<AirOrderItem> airOrderItems) {
		ArrayList<String> airOrderIds = new ArrayList<String>();
		airOrderItems.forEach(item -> {
			// In case of Amendment reissue there is a provision to add new segments which will not have id.
			if (item.getId() != null) {
				airOrderIds.add(item.getId().toString());
			}
		});
		return airOrderIds;
	}
}
