package com.tgs.services.oms.servicehandler;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.pms.datamodel.PaymentStatus;

import lombok.extern.slf4j.Slf4j;

/**
 * Updates order's status to {@code OrderStatus.PENDING} (only), if its current status is one of the statuses mentioned
 * in {@code UpdateOrderStatusHandler.STATUSES}.
 * <p>
 * If empty filter is passed, status of orders processed before 5 minutes ago and within last 30 minutes and having status
 * contained in {@code UpdateOrderStatusHandler.STATUSES} is updated.
 * <p>
 * Upper/lower time limit of filter is set, if it's absent.
 * <p>
 * Status filter is always set to {@code UpdateOrderStatusHandler.STATUSES} for safety purpose.
 * 
 * @author Abhineet Kumar
 *
 */

@Service
@Slf4j
public class UpdateOrderStatusHandler extends ServiceHandler<OrderFilter, BaseResponse> {

	final private static List<OrderStatus> STATUSES =
			Arrays.asList(OrderStatus.IN_PROGRESS, OrderStatus.PAYMENT_SUCCESS, OrderStatus.PAYMENT_PENDING);

	@Autowired
	private OrderService orderService;

	@Override
	public void beforeProcess() throws Exception {
		final LocalDateTime now = LocalDateTime.now();
		if (request.getProcessedOnAfterDateTime() == null && request.getProcessedOnAfterDate() == null) {
			request.setProcessedOnAfterDateTime(now.minusMinutes(30));
		}
		if (request.getProcessedOnBeforeDateTime() == null && request.getProcessedOnBeforeDate() == null) {
			request.setProcessedOnBeforeDateTime(now.minusMinutes(7));
		}
		request.setStatuses(STATUSES);
	}

	@Override
	public void process() throws Exception {
		List<DbOrder> orders = orderService.findAll(request);
		log.info("Found {} eligible orders to change status", orders.size());
		orders.forEach(order -> {
			String status = OrderStatus.PENDING.getCode();
			if (order.getStatus().equals(OrderStatus.IN_PROGRESS.getCode())
					&& PaymentStatus.INITIATE_REDIRECTION.equals(order.getAdditionalInfo().getPaymentStatus())) {
				status = OrderStatus.FAILED.getCode();
				order.setReason("Cancelled By User");
			}
			log.info("Changing order status for booking Id {} to {} ", order.getBookingId(), status);
			order.setStatus(status);
		});
		orderService.save(orders);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
