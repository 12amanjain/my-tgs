package com.tgs.services.oms.servicehandler.air;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderMailAttributes;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restmodel.air.AirReleasePNRRequest;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AbortOrderHandler {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private PaymentServiceCommunicator paymentServiceCommunicator;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private MsgServiceCommunicator msgServiceCommunicator;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private AirReleasePNRHandler cancellationHandler;

	@Autowired
	private DealInventoryCommunicator invCommunicator;

	private Order order;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public void abort(String bookingId) {
		order = orderManager.findByBookingId(bookingId, null);
		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		order.setPayments(paymentServiceCommunicator.fetchPaymentByBookingId(order.getBookingId()).stream()
				.filter(x -> x.getStatus().equals(PaymentStatus.SUCCESS)).collect(Collectors.toList()));
		canAbort();
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		for (Payment p : order.getPayments()) {
			if (p.getAmount().compareTo(BigDecimal.ZERO) != 0) {
				if (p.getOpType().equals(PaymentOpType.CREDIT)) {
					paymentRequestList.add(BasePaymentUtils.createDebitPaymentRequest(p));
					continue;
				}
				PaymentFact fact = PaymentFact.builder().medium(p.getPaymentMedium()).build();
				List<PaymentConfigurationRule> rules = paymentServiceCommunicator.getApplicableRulesOutput(fact,
						PaymentRuleType.REFUND, p.getPaymentMedium());
				PaymentMedium reverseMedium = PaymentMedium.FUND_HANDLER;
				if (!CollectionUtils.isEmpty(rules)) {
					reverseMedium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
				}
				if (reverseMedium.equals(PaymentMedium.FUND_HANDLER)) {
					paymentRequestList.add(createRefundPayments(p, null));
				} else {
					paymentRequestList.add(createRefundPayments(p, reverseMedium));
				}
			}
		}

		List<PaymentRequest> clubbedPayments = BasePaymentUtils.club(paymentRequestList);
		List<DbAirOrderItem> orderItems = airOrderItemService.findByBookingId(bookingId);
		List<Amendment> amendments = amendmentService
				.search(AmendmentFilter.builder().bookingIdIn(Collections.singletonList(bookingId)).build()).stream()
				.filter(a -> !a.getStatus().equals(AmendmentStatus.SUCCESS)
						&& !a.getStatus().equals(AmendmentStatus.PAYMENT_SUCCESS))
				.collect(Collectors.toList());

		List<Payment> outPayments = paymentServiceCommunicator.doPaymentsUsingPaymentRequests(clubbedPayments);
		order.getPayments().addAll(outPayments);
		amendments.forEach(a -> a.setStatus(AmendmentStatus.REJECTED));
		amendmentService.save(amendments);
		orderManager.save(order);

		processCancellation(orderItems);

		if (!isCancellationSuccess()) {
			setTravellerStatus(orderItems);
			orderItems.forEach(item -> item.setStatus(AirItemStatus.ABORTED.getCode()));
			airOrderItemService.save(orderItems);
			order.setStatus(OrderStatus.ABORTED);
			orderManager.save(order);
		}

		sendAbortMail();
		sendAbortSms();
	}

	private void setTravellerStatus(List<DbAirOrderItem> orderItems) {
		AirBookingUtils.setTravellerStatus(orderItems, TravellerStatus.ABORTED);
		orderItems.forEach(item -> {
			if (StringUtils.isNotBlank(item.getAdditionalInfo().getInventoryId())) {
				InventoryOrderFilter filter =
						InventoryOrderFilter.builder().referenceIdIn(Arrays.asList(item.getBookingId()))
								.inventoryIdIn(Arrays.asList(item.getAdditionalInfo().getInventoryId())).build();
				List<InventoryOrder> inventories = invCommunicator.fetchInventoryOrders(filter);
				if (!CollectionUtils.isEmpty(inventories)) {
					InventoryOrder invOrder = inventories.get(0);
					invOrder.getTravellerInfo().forEach(traveller -> traveller.setStatus(TravellerStatus.ABORTED));
					invCommunicator.saveInventoryOrder(invOrder);
				}
			}
		});
	}

	/**
	 * This will try cancellation of PNR from airline side
	 * 
	 * @param orderItems
	 * @return
	 */
	private void processCancellation(List<DbAirOrderItem> orderItems) {
		try {
			initCancellationHandler(orderItems);
			cancellationHandler.getResponse();
		} catch (Exception e) {
			log.error("Cancellation cannot be processed for booking Id {} reason {}", order.getBookingId(),
					e.getMessage());
		}
	}

	private void initCancellationHandler(List<DbAirOrderItem> orderItems) {
		Set<String> pnrs = new HashSet<>();
		orderItems.forEach(item -> {
			pnrs.add(item.getTravellerInfo().get(0).getPnr());
		});
		AirReleasePNRRequest cancellationRequest = new AirReleasePNRRequest();
		cancellationRequest.setBookingId(order.getBookingId());
		cancellationRequest.setPnrs(new ArrayList<String>(pnrs));
		cancellationHandler.setRequest(cancellationRequest);
		cancellationHandler.setResponse(new BaseResponse());
	}

	private void sendAbortMail() {
		AbstractMessageSupplier<OrderMailAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<OrderMailAttributes>() {
					@Override
					public OrderMailAttributes get() {
						User user = userService.getUserFromCache(order.getBookingUserId());
						OrderMailAttributes mailAttr =
								OrderMailAttributes.builder().key(EmailTemplateKey.AIR_BOOKING_ABORTED_EMAIL.name())
										.bookingId(order.getBookingId()).toEmailId(user.getEmail())
										.partnerId(order.getPartnerId()).role(user.getRole()).build();
						return mailAttr;
					}
				};
		msgServiceCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}

	private void sendAbortSms() {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("bookingId", order.getBookingId());
				User bookingUser = userService.getUserFromCache(order.getBookingUserId());
				attributes.put("contact", bookingUser.getMobile());
				SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.BOOKING_ABORTED_SMS.name())
						.recipientNumbers(OrderUtils.getToReceipientNumbers(order, bookingUser, null))
						.attributes(attributes).partnerId(order.getPartnerId()).role(bookingUser.getRole()).build();
				return smsAttr;
			}
		};
		msgServiceCommunicator.sendMessage(msgAttributes.getAttributes());
	}

	private void canAbort() {
		if (order.getStatus().equals(OrderStatus.FAILED) || order.getStatus().equals(OrderStatus.SUCCESS))
			throw new CustomGeneralException(SystemError.CANNOT_BE_ABORTED);
	}

	private PaymentRequest createRefundPayments(Payment p, PaymentMedium medium) {
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().build();
		Double noOfPoints = null;
		if (p.getAdditionalInfo() != null && p.getAdditionalInfo().getNoOfPoints() != null) {
			// Earlier points were debited now we will credit these points
			noOfPoints = p.getAdditionalInfo().getNoOfPoints() * -1;
			additionalInfo.setPointsType(p.getAdditionalInfo().getPointsType());
		}
		return PaymentRequest.builder().build().setPayUserId(p.getPayUserId()).setPartnerId(p.getPartnerId())
				.setProduct(p.getProduct()).setOpType(PaymentOpType.CREDIT).setRefId(p.getRefId())
				.setOriginalPaymentRefId(p.getPaymentRefId()).setAmount(p.getAmount().abs()).setPaymentMedium(medium)
				.setTds(p.getTds()).setTransactionType(PaymentTransactionType.REVERSE).setNoOfPoints(noOfPoints)
				.setAdditionalInfo(additionalInfo);
	}

	private boolean isCancellationSuccess() {
		Order order = orderManager.findByBookingId(this.order.getBookingId(), null);
		if (order != null && OrderStatus.UNCONFIRMED.equals(order.getStatus())) {
			return true;
		}
		return false;
	}

}
