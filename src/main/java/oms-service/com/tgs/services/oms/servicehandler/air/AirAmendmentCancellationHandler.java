package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.servicehandler.UpdateAirAmendmentHandler;
import com.tgs.services.ums.datamodel.User;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;
import lombok.Setter;

@Setter
@Slf4j
@Service
public class AirAmendmentCancellationHandler extends RaiseAirAmendmentHandler {

	@Autowired
	private UpdateAirAmendmentHandler updateAirAmendmentHandler;

	@Autowired
	private AmendmentActionValidator actionValidator;

	private AmendmentResponse amendmentResponse;

	private boolean isCancellationAlreadyDone;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;

	@Override
	public void beforeProcess() throws Exception {

		if (isCancellationRequestNotInCache()
				|| getAirCancellationResponseFromCache(request.getAmendmentId()) == null) {
			List<ErrorDetail> errorMessage = new ArrayList<>();
			errorMessage.add(ErrorDetail.builder().message("Raise Amendment Before Confirming").build());
			response.setErrors(errorMessage);
			return;
		}
		amendmentInCache();
		if (amendmentResponse == null) {
			originalOrder = orderService.findByBookingId(request.getBookingId()).toDomain();
			orderInfo = airAmendmentManager.getOrderInfoForAmendment(request.getBookingId(), request.getType());
			segmentInfoList = BookingUtils.getSegmentInfos(orderInfo.getTripInfoList());
			previousSegments = new ArrayList<SegmentInfo>();
			for (SegmentInfo segmentInfo : segmentInfoList) {
				previousSegments.add(new GsonMapper<>(segmentInfo, SegmentInfo.class).convert());
			}
			createAmendment(request.getAmendmentId());
			filterPaxs(orderInfo);
		}
	}

	// prefill pax keys , in case of api partner , not require to send paxkeys in mulitiple request many times
	private boolean isCancellationRequestNotInCache() {
		AirAmendmentPaxInfo amendmentRequest =
				GsonUtils.getGson().fromJson(
						cachingService.get(
								CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREQUEST.name())
										.key(request.getAmendmentId()).build(),
								String.class, false, true, BinName.CANCELLATION.getName())
								.get(BinName.CANCELLATION.getName()),
						AirAmendmentPaxInfo.class);
		if (amendmentRequest != null) {
			request.setPaxKeys(amendmentRequest.getPaxKeys());
			return false;
		}
		return true;
	}

	private void amendmentInCache() {
		amendmentResponse =
				GsonUtils.getGson().fromJson(
						cachingService.get(
								CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONRESPONSE.name())
										.key(request.getAmendmentId()).build(),
								String.class, false, true, BinName.CONFIRMCANCEL.getName())
								.get(BinName.CONFIRMCANCEL.getName()),
						AmendmentResponse.class);
	}

	@Override
	public void process() throws Exception {
		if (!isCancellationAlreadyDone && amendmentResponse == null) {
			AirCancellationRequest cancellationRequest = AirCancellationRequest.builder().paxKeys(request.getPaxKeys())
					.segmentInfos(previousSegments).build();
			cancellationRequest.setBookingId(amendment.getBookingId());
			cancellationRequest.setAmendmentId(amendment.getAmendmentId());
			service.save(amendment);

			AirGeneralPurposeOutput gnOutPut = null;
			User bookingUser = userService.getUserFromCache(originalOrder.getBookingUserId());
			AirConfiguratorInfo configRule = fmsComm.getAirConfigRule(AirConfiguratorRuleType.GNPUPROSE, bookingUser);

			boolean isThroughCron =
					(configRule != null && Objects.nonNull(gnOutPut = (AirGeneralPurposeOutput) configRule.getOutput()))
							? BooleanUtils.isTrue(gnOutPut.getIsAutoCancelOnCron())
							: false;

			if (!isThroughCron) {
				ExecutorUtils.getGeneralPurposeThreadPool().submit(() -> {
					amendment = service.findByAmendmentId(request.getAmendmentId());
					doAutoCancellation(cancellationRequest, request, response, segmentInfoList);
				});
			}


			if (response.getAmendmentItems().size() == 0) {
				response.setAmendmentItems(new ArrayList<Amendment>());
				response.getAmendmentItems().add(amendment);
			} else {
				response.getAmendmentItems().set(0, amendment);
			}
			storeAmendmentResponseInCache(response, amendment.getAmendmentId());
		} else {
			response = amendmentResponse;
		}
	}

	public void doAutoCancellation(AirCancellationRequest cancellationRequest, AirAmendmentPaxInfo request,
			AmendmentResponse amendmentResponse, List<SegmentInfo> segmentInfoList) {
		originalOrder = orderService.findByBookingId(request.getBookingId()).toDomain();
		AirCancellationResponse cancellationResponse = null;
		try {
			cancellationResponse = getCancellationResponse(cancellationRequest, amendmentResponse);
			if (!cancellationResponse.getCancellationDetail().getAutoCancellationAllowed()) {
				amendment.setStatus(AmendmentStatus.REQUESTED);
				amendment.setAssignedUserId(null);
				amendment.setAssignedOn(null);
				return;
			}
			amendment.setStatus(AmendmentStatus.PROCESSING);
			airAmendmentManager.processAmendment(amendment, cancellationResponse, true, true);
			boolean isPaymentRequired = true;
			updateAirAmendmentHandler.setPaymentRequired(isPaymentRequired);
			updateAirAmendmentHandler.setAmendment(amendment);
			updateAirAmendmentHandler.processPayment();
			updateAirAmendmentHandler.merge();
			amendment.setActionList(
					actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
			List<Amendment> amendmentItems = new ArrayList<>();
			amendmentItems.add(amendment);
			amendmentResponse.setAmendmentItems(amendmentItems);
		} catch (Exception e) {
			if (e instanceof PaymentException && cancellationResponse != null
					&& cancellationResponse.getCancellationDetail() != null
					&& BooleanUtils.isTrue(cancellationResponse.getCancellationDetail().getAutoCancellationAllowed())) {
				Note note = Note.builder().bookingId(request.getBookingId()).noteType(NoteType.SUPPLIER_MESSAGE)
						.noteMessage(e.getMessage() + " Please contact customer care for refund. ").build();
				List<ErrorDetail> errorMessage = new ArrayList<>();
				errorMessage.add(ErrorDetail.builder()
						.message(e.getMessage() + " Please contact customer care for refund. ").build());
				cancellationResponse.setErrors(errorMessage);
				generalServiceCommunicator.addNote(note);
			}
			log.error("Amendment Auto Cancelled Failed for BookingId {} ", request.getBookingId(), e);
			amendment.setStatus(AmendmentStatus.REQUESTED);
			amendment.setAssignedUserId(null);
			amendment.setAssignedOn(null);
		} finally {
			service.save(amendment);
			storeAmendmentResponseInCache(amendmentResponse, amendment.getAmendmentId());
			cancellationRequest.setSegmentInfos(segmentInfoList);
			fmsComm.sendCancellationDataToAnalytics(cancellationRequest, originalOrder,
					amendmentResponse.getAirCancellationReviewResponse(), amendment.getAmount(), false);
		}
	}


	private void storeAmendmentResponseInCache(AmendmentResponse amendmentResponse, String amendmentId) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.CONFIRMCANCEL.getName(), GsonUtils.getGson().toJson(response));
		cachingService.store(
				CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONRESPONSE.name()).key(amendmentId).build(), binMap,
				false, true, 3600);
	}


	public AirCancellationResponse getCancellationResponse(AirCancellationRequest cancellationRequest,
			AmendmentResponse amendmentResponse) {
		this.amendmentResponse = amendmentResponse;
		AirCancellationResponse cancellationResponse = getAirCancellationResponseFromCache(amendment.getAmendmentId());
		boolean isCancellationAllowed = false;
		try {
			if (cancellationResponse != null
					&& cancellationResponse.getCancellationDetail().getAutoCancellationAllowed()) {
				amendment.setAssignedUserId(getUserIdFromConfigurator());
				amendment.setStatus(AmendmentStatus.ASSIGNED);
				amendment.setAssignedOn(LocalDateTime.now());
				cancellationResponse =
						fmsComm.confirmCancellation(cancellationRequest, cancellationResponse, originalOrder);
				isCancellationAllowed = cancellationResponse.getCancellationDetail().getAutoCancellationAllowed();
				// new cancellation response where actually tried to cancel
				addSystemRemarks(cancellationResponse);
			} else {
				// cached Review cancellation response
				addSystemRemarks(cancellationResponse);
			}
		} finally {
			cancellationResponse.getCancellationDetail().setAutoCancellationAllowed(isCancellationAllowed);
			amendmentResponse.setAirCancellationReviewResponse(cancellationResponse);
			log.info("Amendment Auto Cancelled {} for bookingId {} ",
					cancellationResponse.getCancellationDetail().getAutoCancellationAllowed(),
					amendment.getBookingId());
		}
		return cancellationResponse;
	}

	private String getUserIdFromConfigurator() {
		ClientGeneralInfo clientGeneralInfo = (ClientGeneralInfo) generalServiceCommunicator
				.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
		return clientGeneralInfo.getCancellationAutomatedUserId();
	}

	public AirCancellationResponse getAirCancellationResponseFromCache(String amendmentId) {
		return GsonUtils.getGson()
				.fromJson(
						cachingService.get(
								CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREVIEWRESPONSE.name())
										.key(amendmentId).build(),
								String.class, false, true, BinName.CANCELLATION.getName())
								.get(BinName.CANCELLATION.getName()),
						AirCancellationResponse.class);
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
