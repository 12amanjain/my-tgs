package com.tgs.services.oms.servicehandler.air;

import java.math.BigDecimal;
import java.util.Objects;
import com.tgs.services.base.runtime.SystemContextHolder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import com.tgs.services.oms.restmodel.air.PartnerAmendmentResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirAmendmentStatusHandler extends ServiceHandler<AmendmentRequest, PartnerAmendmentResponse> {

	@Autowired
	private AmendmentService amendmentService;

	@Autowired
	private AirAmendmentManager airAmendmentManager;

	private Amendment amendment;

	private static final int NEGATIVE = -1;

	@Override
	public void beforeProcess() throws Exception {
		if (StringUtils.isBlank(request.getAmendmentId())) {
			throw new CustomGeneralException(SystemError.AMENDMENT_ID_BLANK);
		}

	}

	@Override
	public void process() throws Exception {
		amendment = amendmentService.findByAmendmentId(request.getAmendmentId());
		if (Objects.isNull(amendment)) {
			throw new CustomGeneralException(SystemError.AMENDMENT_ID_BLANK);
		}
		SystemContextHolder.getContextData().getReqIds().add(amendment.getBookingId());
		AirOrderInfo airOrderInfo =
				airAmendmentManager.getOrderInfoForAmendment(amendment.getBookingId(), amendment.getAmendmentType());
		response.setAmendmentStatus(amendment.getStatus());
		response.setBookingId(amendment.getBookingId());
		response.setAmendmentId(amendment.getAmendmentId());
		if (Objects.nonNull(amendment.getAdditionalInfo().getTotalAmendmentFee()))
			response.setAmendmentCharges(amendment.getAdditionalInfo().getTotalAmendmentFee());
		if (Objects.nonNull(amendment.getAmount()))
			response.setRefundableAmount(NEGATIVE * (amendment.getAmount()));
		response.setTrips(airAmendmentManager.buildTripCharges(airOrderInfo, amendment));
	}

	@Override
	public void afterProcess() throws Exception {
		BigDecimal actualRefundedAmount = new BigDecimal(response.getRefundableAmount());
		BigDecimal totalPaxRefundableAmount = new BigDecimal(response.getPaxRefundableAmount(amendment));
		log.info("Amendment {} Actual Refundable Fare {} Total Pax Refundable fare {} ", response.getAmendmentId(),
				actualRefundedAmount, totalPaxRefundableAmount);
		if (actualRefundedAmount.compareTo(totalPaxRefundableAmount) != 0) {
			throw new CustomGeneralException(SystemError.AMENDMENT_REFUNDABLE_CHARGES_DIFF);
		}
	}

}
