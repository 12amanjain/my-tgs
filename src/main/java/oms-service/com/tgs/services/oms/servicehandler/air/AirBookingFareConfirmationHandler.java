package com.tgs.services.oms.servicehandler.air;


import com.tgs.services.oms.BookingResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.utils.exception.air.HoldLimitException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirBookingFareConfirmationHandler extends ServiceHandler<BookingRequest, BookingResponse> {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	FMSCommunicator fmsBookingCommunicator;

	@Autowired
	private AbortOrderHandler abortHandler;

	@Autowired
	protected GeneralServiceCommunicator gmsCommunicator;

	private Order order;


	@Override
	public void beforeProcess() throws Exception {
		boolean isPendingRequest = OrderUtils.isPendingRequest(request.getBookingId(), "farevalidate");
		if (isPendingRequest) {
			throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST);
		}

	}

	@Override
	public void process() throws Exception {
		try {
			if (StringUtils.isNotBlank(request.getBookingId())) {
				this.order = orderManager.findByBookingId(request.getBookingId(), null);
				if (this.order == null) {
					throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
				}
				response.setBookingId(request.getBookingId());
				if (isValidOrderStatus(order) && isFareValidateAllowed(order)
						&& fmsBookingCommunicator.doConfirmFare(order)) {
					// abortBooking(SystemError.FARE_NO_LONGER_AVAILABLE.getMessage());
					addNoteMessage(SystemError.FARE_NO_LONGER_AVAILABLE.getMessage());
					throw new CustomGeneralException(SystemError.FARE_NO_LONGER_AVAILABLE);
				}
			}
		} catch (HoldLimitException e) {
			// abortBooking(SystemError.HOLD_TIME_LIMIT_EXCEED.getMessage());
			addNoteMessage(SystemError.HOLD_TIME_LIMIT_EXCEED.getMessage());
			throw new CustomGeneralException(SystemError.HOLD_TIME_LIMIT_EXCEED);
		} finally {
			OrderUtils.removePendingRequest(request.getBookingId(), "farevalidate");
		}
	}

	private void addNoteMessage(String message) {
		String noteMsg = StringUtils.join(message, " : SYSTEM GENERATED");
		Note orderNote = Note.builder().bookingId(order.getBookingId()).noteType(NoteType.MANUAL_CONFIRMATION)
				.noteMessage(noteMsg).build();
		gmsCommunicator.addNote(orderNote);


	}

	private boolean isValidOrderStatus(Order order) {
		return order.getStatus().equals(OrderStatus.ON_HOLD);
	}


	@Override
	public void afterProcess() throws Exception {

	}

	public boolean isFareValidateAllowed(Order order) {
		boolean isFareValidateAllowed = true;
		if (order.getAdditionalInfo() != null && order.getAdditionalInfo().getFlowType() != null) {
			OrderFlowType flowType = order.getAdditionalInfo().getFlowType();
			if (flowType.equals(OrderFlowType.IMPORT_PNR) || flowType.equals(OrderFlowType.MANUAL_ORDER)) {
				isFareValidateAllowed = false;
			}
		}
		return isFareValidateAllowed;
	}

	private void abortBooking(String abortReason) {
		abortHandler.abort(this.order.getBookingId());
		this.order = orderManager.findByBookingId(request.getBookingId(), null);
		if (OrderStatus.ABORTED.equals(order.getStatus()) || OrderStatus.UNCONFIRMED.equals(order.getStatus())) {
			order.setReason(abortReason);
			orderManager.save(order);
			String noteMsg = StringUtils.join(abortReason, "- Booking has been Aborted :SYSTEM GENERATED");
			Note orderAbortNote = Note.builder().bookingId(order.getBookingId()).noteType(NoteType.AUTO_ABORT)
					.noteMessage(noteMsg).build();
			gmsCommunicator.addNote(orderAbortNote);
		}
	}
}
