package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirBookingManager;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.restmodel.RegisterRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirBookingHandler extends ServiceHandler<AirBookingRequest, BookingResponse> {

	@Autowired
	@Qualifier("AirBookingManager")
	private AirBookingManager bookingManager;

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private AirOrderItemService orderItemService;

	@Autowired
	private PaymentServiceCommunicator paymentServiceComm;

	@Autowired
	FMSCachingServiceCommunicator cachingService;

	@Autowired
	private UserServiceCommunicator userService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		Order order = null;
		List<TripInfo> tripInfos = null;
		AirSearchQuery searchQuery = null;
		StringJoiner exceptions = new StringJoiner("-");
		try {
			registerGuestUser();
			LocalDateTime startTime = LocalDateTime.now();
			SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			response.setBookingId(request.getBookingId());
			tripInfos = fmsComm.getTripInfos(request);
			AirReviewResponse reviewResponse =
					fmsComm.getAirReviewResponse(request.getBookingId(), request.getOldBookingId());
			searchQuery = reviewResponse.getSearchQuery();
			User loggedInUser = SystemContextHolder.getContextData().getUser();
			User bookingUser = userService.getUserFromCache(loggedInUser.getParentUserId());

			Map<String, Integer> keysWithTtl =
					BaseUtils.generatePaxTravelKeysWithTTL(tripInfos, request.getTravellerInfo(), bookingUser);
			if (BooleanUtils.isNotFalse(request.getCheckDuplicateBooking())) {
				List<OrderStatus> failureStatusList =
						Arrays.asList(OrderStatus.FAILED, OrderStatus.ABORTED, OrderStatus.UNCONFIRMED);
				OrderUtils.checkDuplicateBooking(keysWithTtl, request.getBookingId(), failureStatusList, orderManager,
						fmsComm.getAirConfigRule(AirConfiguratorRuleType.GNPUPROSE, bookingUser),
						getCancelledKeysSupplier());
			}

			removeExistingData();

			bookingManager.setBookingRequest(request);
			bookingManager.setTripInfos(tripInfos);
			bookingManager.setSearchQuery(searchQuery);
			bookingManager.setBookingUser(loggedInUser);
			bookingManager.setLoggedInUser(loggedInUser);
			if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
				request.getPaymentInfos().forEach(p -> p.setProduct(Product.AIR));
			}
			bookingManager.buildPaymentInfos();

			order = bookingManager.processBooking();

			order = orderManager.findByBookingId(order.getBookingId(), order);
			log.info("Trip id {}  for booking Id {}", reviewResponse.getTripId(), request.getBookingId());

			order.getAdditionalInfo().setTripId(reviewResponse.getTripId());

			if (!LocalDateTime.now().minusMinutes(1).isAfter(startTime)) {
				if (CollectionUtils.isEmpty(request.getPaymentInfos()) || (order.getAdditionalInfo() != null
						&& order.getAdditionalInfo().getPaymentStatus() == PaymentStatus.SUCCESS)) {
					List<SegmentInfo> segmentList = AirBookingUtils.convertAirOrderItemListToSegmentInfoList(
							bookingManager.getItems(), BookingUtils.getSegmentInfos(tripInfos));
					List<TripInfo> tripList = BookingUtils.createTripListFromSegmentList(segmentList);
					fmsComm.doBooking(tripList, order);
				}
			} else {
				log.info("Booking completion took more than 1 min , hence it can't be automated {}",
						order.getBookingId());
			}
		} catch (DataIntegrityViolationException e) {
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			exceptions.add(e.getMessage());
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER);
		} catch (CustomGeneralException e) {
			exceptions.add(e.getMessage());
			throw e;
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		} finally {
			List<Alert> alerts = SystemContextHolder.getContextData().getAlerts();
			if (CollectionUtils.isNotEmpty(alerts)) {
				alerts.forEach(alert -> response.addAlert(alert));
			}
			pushToAnalytics(order, tripInfos, request.getPaymentInfos(), exceptions.toString(), AirAnalyticsType.BOOK,
					searchQuery, exceptions.toString());
		}
	}

	/**
	 * If UserRole is Guest, and he/she is trying to do booking, we will register the user <br>
	 * in background with email and phone provided in the deliveryinfo. New UserRole will be Customer now
	 */

	private void registerGuestUser() throws Exception {
		if (UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
			try {
				String email = request.getDeliveryInfo().getEmails().get(0);
				String mobile = request.getDeliveryInfo().getContacts().get(0);
				User user = ObjectUtils.firstNonNull(
						userService.getUser(
								UserFilter.builder().email(email).roles(Arrays.asList(UserRole.CUSTOMER)).build()),
						userService.getUser(
								UserFilter.builder().mobile(mobile).roles(Arrays.asList(UserRole.CUSTOMER)).build()));
				if (user == null) {
					String password = RandomStringUtils.randomAlphanumeric(8);
					user = userService.registerUser(RegisterRequest.builder().email(email).mobile(mobile)
							.name(request.getTravellerInfo().get(0).getFirstName() + " "
									+ request.getTravellerInfo().get(0).getLastName())
							.role(UserRole.CUSTOMER).password(password).build());
				}

				if (!user.getEmail().equals(email)) {
					throw new CustomGeneralException(SystemError.USER_EXISTS_WITH_DIFFERENT_EMAIL);
				}
				if (!user.getMobile().equals(mobile)) {
					throw new CustomGeneralException(SystemError.USER_EXISTS_WITH_DIFFERENT_MOBILE);
				}

				SystemContextHolder.getContextData().setUser(user);
				try {
					HttpServletResponse res = SystemContextHolder.getContextData().getHttpResponse();
					if (res != null) {
						log.debug("JWT Token in http headers {} ",
								SystemContextHolder.getContextData().getHttpHeaders().getJwtToken());
						log.debug("Auth header is response 1  {}", res.getHeader(SecurityConstants.HEADER_STRING));
						res.setHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX
								+ SystemContextHolder.getContextData().getHttpHeaders().getJwtToken());
						log.debug("Auth header is response 2  {}", res.getHeader(SecurityConstants.HEADER_STRING));
						log.debug("Auth header is request  {}", SystemContextHolder.getContextData().getHttpRequest()
								.getHeader(SecurityConstants.HEADER_STRING));

					} else {
						log.debug("Response is null");
					}
				} catch (Exception e) {
					log.error("Failed to generate jwt for {} due to {} ", user.getUserId(), e.getMessage(), e);
				}
			} catch (Exception e) {
				log.error("Error occured during user registration with email {} and mobile {} due to {}",
						request.getDeliveryInfo().getEmails().get(0), request.getDeliveryInfo().getContacts().get(0),
						e.getMessage(), e);
				throw new CustomGeneralException(SystemError.USER_EXISTS);
			}
		}
	}

	private Function<String, Collection<String>> getCancelledKeysSupplier() {
		return bookingId -> {
			Collection<String> cancelledPaxKeys = new ArrayList<>();
			Order dupOrder = orderManager.findByBookingId(bookingId, null);
			if (dupOrder != null) {
				User bookingUser = userService.getUserFromCache(dupOrder.getBookingUserId());
				List<DbAirOrderItem> airOrderitems = orderItemService.findByBookingId(bookingId);
				airOrderitems.forEach(airOrderItem -> {
					AirOrderItemToSegmentInfoMapper mapper = AirOrderItemToSegmentInfoMapper.builder()
							.fmsCommunicator(fmsComm).item(airOrderItem).build();
					SegmentInfo segmentInfo = mapper.convert();
					List<FlightTravellerInfo> tInfo = airOrderItem.getTravellerInfo();
					List<FlightTravellerInfo> cancelledFlightTravellers =
							tInfo.stream().filter(t -> t.getStatus() != null && t.getStatus().isCancelled())
									.collect(Collectors.toList());
					cancelledFlightTravellers.forEach(tinfo -> cancelledPaxKeys
							.add(BaseUtils.generatePaxTravelKey(segmentInfo, tinfo, bookingUser)));
				});
			}
			return cancelledPaxKeys;
		};
	}

	private void pushToAnalytics(Order order, List<TripInfo> tripInfos, List<PaymentRequest> paymentRequests,
			String errorMsg, AirAnalyticsType analyticsType, AirSearchQuery searchQuery, String exceptions) {
		try {
			if (order == null)
				order = orderManager.findByBookingId(request.getBookingId(), null);
			fmsComm.addBookToAnalytics(order, tripInfos, request.getPaymentInfos(), exceptions, AirAnalyticsType.BOOK,
					searchQuery);
		} catch (Exception e) {
			log.error("Unable to log analytics for bookingId {}", request.getBookingId(), e);
		}
	}

	/**
	 * Remove any existing data related to previous failed booking.
	 */
	private void removeExistingData() {
		orderManager.deleteExistingFailedAndNewBooking(request.getBookingId());
		orderItemService.deleteItemsWithNoOrderByBookingId(request.getBookingId());
		paymentServiceComm.deleteFailedPaymentsByRefId(request.getBookingId());
	}

	@Override
	public void afterProcess() {

	}

}
