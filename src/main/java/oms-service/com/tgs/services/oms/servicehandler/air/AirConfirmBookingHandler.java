package com.tgs.services.oms.servicehandler.air;

import java.util.List;
import java.util.StringJoiner;

import com.tgs.services.oms.BookingResponse;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirConfirmBookingManager;
import com.tgs.services.oms.restmodel.air.AirConfirmBookingRequest;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.pms.datamodel.PaymentStatus;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirConfirmBookingHandler extends ServiceHandler<AirConfirmBookingRequest, BookingResponse> {

	@Autowired
	@Qualifier("AirConfirmBookingManager")
	private AirConfirmBookingManager bookingManager;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private OrderManager orderManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		Order order = null;
		List<TripInfo> tripList = null;
		StringJoiner exceptions = new StringJoiner("-");
		try {
			bookingManager.setBookingRequest(request);
			SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			bookingManager.setBookingUser(SystemContextHolder.getContextData().getUser());
			bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
			order = orderManager.findByBookingId(request.getBookingId(), order);

			if (order.getStatus() != OrderStatus.ON_HOLD) {
				throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);
			}

			order = orderManager.findByBookingId(order.getBookingId(), order);

			if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
				request.getPaymentInfos().forEach(p -> p.setProduct(Product.AIR));
			}
			
			bookingManager.buildPaymentInfos();
			
			order = bookingManager.processBooking();

			List<SegmentInfo> segmentList =
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(bookingManager.getItems(), null);
			tripList = BookingUtils.createTripListFromSegmentList(segmentList);

			if (isConfirmBookingAllowed(order)
					&& order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
				fmsCommunicator.doConfirmBooking(tripList, order);
			} else {
				// case when emulate or manual order
				bookingManager.updateItemStatus();
			}
		} catch (DataIntegrityViolationException e) {
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			exceptions.add(e.getMessage());
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER);
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.INVALID_REQUEST);
		} finally {
			if (CollectionUtils.isEmpty(tripList)) {
				List<SegmentInfo> segmentList =
						AirBookingUtils.convertAirOrderItemListToSegmentInfoList(bookingManager.getItems(), null);
				tripList = BookingUtils.createTripListFromSegmentList(segmentList);
			}
			fmsCommunicator.addBookToAnalytics(order, tripList, request.getPaymentInfos(), exceptions.toString(),
					AirAnalyticsType.CONFIRM_BOOK, null);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public boolean isConfirmBookingAllowed(Order order) {
		boolean isAllowed = true;
		if (order.getAdditionalInfo() != null) {
			OrderFlowType flowType = order.getAdditionalInfo().getFlowType();
			if (flowType != null
					&& (flowType.equals(OrderFlowType.IMPORT_PNR) || flowType.equals(OrderFlowType.MANUAL_ORDER))) {
				isAllowed = false;
			}
		}
		return isAllowed;
	}
}
