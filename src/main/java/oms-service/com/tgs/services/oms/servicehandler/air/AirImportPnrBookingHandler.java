package com.tgs.services.oms.servicehandler.air;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;

import com.tgs.services.oms.BookingResponse;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirImportPnrBookingManager;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.utils.exception.air.ImportPNRNotAllowedException;
import com.tgs.utils.exception.air.NoPNRFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirImportPnrBookingHandler extends ServiceHandler<AirImportPnrBookingRequest, BookingResponse> {

	@Autowired
	AirImportPnrBookingManager bookingManager;

	@Autowired
	UserServiceCommunicator userService;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	private OrderManager orderManager;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		AirImportPnrBooking pnrBooking = AirImportPnrBooking.builder().build();
		Order order = null;
		StringJoiner exceptions = new StringJoiner("-");
		try {
			pnrBooking = fmsCommunicator.retrieveBooking(request);
			if (pnrBooking == null
					|| CollectionUtils.isEmpty(pnrBooking.getTripInfos()) && !isValidStatus(pnrBooking)) {
				log.error("No Booking Found for PNR {}", request.getPnr());
				throw new CustomGeneralException(SystemError.NO_BOOKING_FOUND);
			}
			String bookingId = ServiceUtils
					.generateId(ProductMetaInfo.builder().product(Product.AIR)
							.subProduct((BaseUtils.isDomesticTrip(pnrBooking.getTripInfos().get(0)))
									? AirType.DOMESTIC.getName()
									: AirType.INTERNATIONAL.getName())
							.build());
			request.setBookingId(bookingId);
			request.setType(OrderType.AIR);
			request.setDeliveryInfo(pnrBooking.getDeliveryInfo());
			request.setGstInfo(pnrBooking.getGstInfo());
			request.setFlowType(OrderFlowType.IMPORT_PNR);
			bookingManager.setBookingRequest(request);
			bookingManager.setItemStatus(pnrBooking.getItemStatus());
			bookingManager.setTripInfos(pnrBooking.getTripInfos());
			bookingManager.setBookingUser(userService.getUserFromCache(request.getBookingUserId()));
			bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
			order = bookingManager.processBooking();
		} catch (NoPNRFoundException pnre) {
			exceptions.add(pnre.getMessage());
			throw new CustomGeneralException(SystemError.NO_PNR_FOUND, pnre.getMessage());
		} catch (ImportPNRNotAllowedException iE) {
			exceptions.add(iE.getMessage());
			throw new CustomGeneralException(SystemError.IMPORT_PNR_NOT_ALLWOED_RETREIEVE, iE.getMessage());
		} catch (CustomGeneralException e) {
			throw new CustomGeneralException(SystemError.NO_PNR_FOUND, e.getMessage());
		} catch (Exception e) {
			exceptions.add(e.getMessage());
			log.error("Unable to fetch booking for pnr {}  and cause {}", request.getPnr(), e);
		} finally {
			fmsCommunicator.addBookToAnalytics(order, pnrBooking.getTripInfos(), null, exceptions.toString(),
					AirAnalyticsType.IMPORT_PNR, null);
		}
	}

	@Override
	public void afterProcess() throws Exception {
		response.setBookingId(request.getBookingId());
	}

	/**
	 * Booking Should be on HOLD or it can be NEW
	 */
	public boolean isValidStatus(AirImportPnrBooking pnrBooking) {
		if (pnrBooking != null && pnrBooking.getItemStatus() == null) {
			return false;
		}
		List<AirItemStatus> itemStatusList = Arrays.asList(AirItemStatus.IN_PROGRESS, AirItemStatus.SUCCESS,
				AirItemStatus.ON_HOLD);
		return itemStatusList.contains(pnrBooking.getItemStatus());
	}

//	public void isDuplicateBooking(AirImportPnrBooking pnrBooking) {
//		if (CollectionUtils.isNotEmpty(pnrBooking.getTripInfos())) {
//			Map<String, Integer> keysWithTtl = BaseUtils.generatePaxTravelKeysWithTTL(pnrBooking.getTripInfos(),
//					pnrBooking.getTripInfos().get(0).getTravllerInfos());
//			List<OrderStatus> failureStatusList = Arrays.asList(OrderStatus.FAILED);
//			OrderUtils.checkDuplicateBooking(keysWithTtl, request.getBookingId(), failureStatusList, orderManager,
//					fmsCommunicator.getAirConfigRule(AirConfiguratorRuleType.GNPUPROSE), null);
//		}
//	}

}
