package com.tgs.services.oms.servicehandler.air;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.InvoiceInfo;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirInvoiceDetail;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.dbmodel.DbOrderBilling;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.oms.jparepository.air.AirItemDetailService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.restmodel.air.AirInvoiceRequest;
import com.tgs.services.oms.restmodel.air.AirInvoiceResponse;
import com.tgs.services.ums.datamodel.FieldMetaInfo;
import com.tgs.services.ums.datamodel.GSTInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserProfileFields;

@Service
public class AirInvoiceHandler extends ServiceHandler<AirInvoiceRequest, AirInvoiceResponse> {

	@Autowired
	private AirOrderItemService orderItemService;

	@Autowired
	private static AirItemDetailService detailService;

	@Autowired
	private static GeneralServiceCommunicator generalServiceCommunicator;

	@Autowired
	private static UserServiceCommunicator userServiceCommunicator;

	@Autowired
	private static AmendmentService amendmentService;

	@Autowired
	private static OrderManager orderManager;

	@Autowired
	private GstInfoService gstInfoService;

	private List<AirOrderItem> airOrderItems;

	private boolean paxInvoice;

	private boolean corporateInvoice;

	@Autowired
	public AirInvoiceHandler(GeneralServiceCommunicator generalServiceCommunicator,
			UserServiceCommunicator userServiceCommunicator, AmendmentService amendmentService,
			OrderManager orderManager, AirItemDetailService detailService) {
		AirInvoiceHandler.generalServiceCommunicator = generalServiceCommunicator;
		AirInvoiceHandler.userServiceCommunicator = userServiceCommunicator;
		AirInvoiceHandler.amendmentService = amendmentService;
		AirInvoiceHandler.orderManager = orderManager;
		AirInvoiceHandler.detailService = detailService;
	}

	@Override
	public void beforeProcess() throws Exception {
		if (CollectionUtils.isNotEmpty(request.getAirOrderItemList())) {
			airOrderItems = request.getAirOrderItemList();
			return;
		}
		Amendment amendment = amendmentService.getFirstAmendment(request.getId());

		/**
		 * There was a bug from 2nd Aug 2019 to 7th Aug 2019, previous snapshot was not being stored in database.
		 */
		airOrderItems = Objects.nonNull(amendment)
				&& amendment.getAdditionalInfo().getAirAdditionalInfo().getOrderPreviousSnapshot() != null
						? amendment.getAdditionalInfo().getAirAdditionalInfo().getOrderPreviousSnapshot()
						: DbAirOrderItem.toDomainList(orderItemService.findByBookingId(Arrays.asList(request.getId())));
		if (CollectionUtils.isEmpty(airOrderItems)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}
	}

	@Override
	public void process() throws Exception {
		String bookingUserId = null;
		User user = null;
		Order order = orderManager.findByBookingId(request.getId(), new Order());
		if (order != null) {
			bookingUserId = order.getBookingUserId();
			UserServiceHelper.checkAndReturnAllowedUserId(
					SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(), Arrays.asList(bookingUserId));
			user = userServiceCommunicator.getUserFromCache(bookingUserId);
			paxInvoice = UserRole.corporate(user.getRole()) && request.getInvoiceId() != null;
			corporateInvoice = UserRole.corporate(user.getRole());
			response.setInvoiceId(order.getAdditionalInfo().getInvoiceId());
			response.setTripId(order.getAdditionalInfo().getTripId());
			response.setCreatedOn(order.getCreatedOn());
		}
		InvoiceInfo resellerInfo = corporateInvoice ? getOrderInvoice(request.getId(), user)
				: InvoiceType.PARTNER.equals(request.getType()) ? getPartnerInvoice(user) : getResellerInvoice(user);
		if (request.getType().equals(InvoiceType.AGENCY) || corporateInvoice) {
			if (!UserUtils.DEFAULT_PARTNERID.equals(user.getPartnerId())) {
				response.setFrom(getPartnerInvoice(user));
			} else {
				response.setFrom(getClientInvoice(
						(ClientGeneralInfo) generalServiceCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
								GeneralBasicFact.builder().applicableTime(order.getCreatedOn()).build())));
			}
			response.setTo(resellerInfo);
		} else if (InvoiceType.PARTNER.equals(request.getType())) {
			response.setFrom(getClientInvoice(
					(ClientGeneralInfo) generalServiceCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
							GeneralBasicFact.builder().applicableTime(order.getCreatedOn()).build())));
			response.setTo(resellerInfo);
		} else {
			response.setFrom(resellerInfo);
		}

		List<AirInvoiceDetail> airInvoiceDetails = new ArrayList<>();
		if (paxInvoice) {
			airOrderItems
					.forEach(x -> x.getTravellerInfo().removeIf(t -> !request.getInvoiceId().equals(t.getInvoice())));
			response.setInvoiceId(request.getInvoiceId());
		}
		if (corporateInvoice) {
			setProfileFields(request.getId(), bookingUserId, airOrderItems);
		}

		GeneralBasicFact fact = GeneralBasicFact.builder().build();
		fact.generateFact(user.getRole());
		FareBreakUpConfigOutput fbConfig =
				generalServiceCommunicator.getConfigRule(ConfiguratorRuleType.FAREBREAKUP, fact);

		for (AirOrderItem airOrderItem : airOrderItems) {
			for (FlightTravellerInfo flightTraveller : airOrderItem.getTravellerInfo()) {
				AirInvoiceDetail invoiceDetail = findTravellerWithSameId(airInvoiceDetails, flightTraveller.getId());
				if (invoiceDetail == null) {
					invoiceDetail = new AirInvoiceDetail();
					invoiceDetail.setTravellerInfo(flightTraveller);
					invoiceDetail.getSectors().add(airOrderItem.getSource());
					airInvoiceDetails.add(invoiceDetail);
				} else {
					setTotalFare(invoiceDetail.getTravellerInfo().getFareDetail(), flightTraveller.getFareDetail());
				}
				invoiceDetail.getPnr().add(flightTraveller.getPnr());
				invoiceDetail.getTicketNumber().add(flightTraveller.getTicketNumber());
				invoiceDetail.getSectors().add(airOrderItem.getDest());
				invoiceDetail.getFlightNumber().add(airOrderItem.getFlightInfo());
				invoiceDetail.getClassOfBooking().add(flightTraveller.getFareDetail().getClassOfBooking());
			}
			response.getDepartureDates().add(airOrderItem.getDepartureTime());
			response.getArrivalDates().add(airOrderItem.getArrivalTime());
		}

		for (AirInvoiceDetail invoiceDetail : airInvoiceDetails) {
			FlightTravellerInfo traveller = invoiceDetail.getTravellerInfo();
			invoiceDetail.getTravellerInfo().getFareDetail()
					.setFareComponents(traveller.getFareDetail().populateMappedFareComponent(fbConfig));
		}
		response.setGstInfo(orderManager.getGstInfo(request.getId()));
		response.setBookingId(request.getId());
		response.setTravellerWiseDetail(airInvoiceDetails);
		Set<String> voucherCodes = new HashSet<>();
		voucherCodes.add(order.getAdditionalInfo().getVoucherCode());
		response.setVoucherCodes(voucherCodes);
	}

	private InvoiceInfo getPartnerInvoice(User user) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		User partner = user;
		if (user != null && !UserUtils.DEFAULT_PARTNERID.equals(user.getPartnerId())) {
			partner = userServiceCommunicator.getUserFromCache(user.getPartnerId());
			invoiceInfo = buildInvoiceInfoFromUser(partner);
		}
		return invoiceInfo;
	}

	private InvoiceInfo getResellerInvoice(User user) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		if (user != null) {
			User parentUser = user;
			if (StringUtils.isNotEmpty(user.getParentUserId())) {
				parentUser = userServiceCommunicator.getUserFromCache(user.getParentUserId());
			}
			invoiceInfo = buildInvoiceInfoFromUser(parentUser);
		}
		return invoiceInfo;
	}

	private InvoiceInfo buildInvoiceInfoFromUser(User user) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		AddressInfo addressInfo = AddressInfo.builder().build();
		CityInfo cityInfo = CityInfo.builder().build();
		if (user.getAddressInfo() != null) {
			addressInfo = user.getAddressInfo();
			if (addressInfo.getCityInfo() != null) {
				cityInfo = addressInfo.getCityInfo();
			}
		}
		GSTInfo gstInfo = GSTInfo.builder().build();
		if (!UserUtils.isB2CUser(user) && user.getGstInfo() != null) {
			gstInfo = user.getGstInfo();
		}
		invoiceInfo = InvoiceInfo.builder().name(user.getName()).email(user.getEmail())
				.phone(ObjectUtils.firstNonNull(user.getPhone(), user.getMobile())).address(addressInfo.getAddress())
				.city(cityInfo.getName()).state(cityInfo.getState()).pincode(addressInfo.getPincode())
				.gstNumber(gstInfo.getGstNumber()).build();
		return invoiceInfo;
	}

	private InvoiceInfo getOrderInvoice(String bookingId, User user) {
		InvoiceInfo invoiceInfo = null;
		DbOrderBilling db = gstInfoService.findByBookingId(bookingId);
		if (db != null) {
			GstInfo gstInfo = db.toDomain();
			invoiceInfo = InvoiceInfo.builder().name(gstInfo.getRegisteredName()).email(gstInfo.getEmail())
					.phone(gstInfo.getMobile()).address(gstInfo.getAddress()).city(gstInfo.getCityName())
					.state(gstInfo.getState()).pincode(gstInfo.getPincode()).gstNumber(gstInfo.getGstNumber()).build();
		} else {
			invoiceInfo = getResellerInvoice(user);
		}

		return invoiceInfo;
	}

	public static InvoiceInfo getClientInvoice(ClientGeneralInfo clientInfo) {
		InvoiceInfo invoiceInfo = InvoiceInfo.builder().build();
		GstInfo gstInfo = new GstInfo();
		if (clientInfo.getGstInfo() != null) {
			gstInfo = clientInfo.getGstInfo();
		}
		if (clientInfo != null) {
			invoiceInfo = InvoiceInfo.builder().name(clientInfo.getCompanyName()).email(clientInfo.getEmail())
					.phone(ObjectUtils.firstNonNull(gstInfo.getPhone(), clientInfo.getWorkPhone()))
					.address(StringUtils.join(clientInfo.getAddress1(), clientInfo.getAddress2()))
					.city(clientInfo.getCity()).state(clientInfo.getState()).pincode(clientInfo.getPostalCode())
					.gstNumber(gstInfo.getGstNumber()).build();
		}
		return invoiceInfo;
	}

	private void setTotalFare(FareDetail invoiceFareDetail, FareDetail flightTravellerFareDetail) {
		for (Entry<FareComponent, Double> fareComponent : flightTravellerFareDetail.getFareComponents().entrySet()) {
			Double oldValue =
					ObjectUtils.defaultIfNull(invoiceFareDetail.getFareComponents().get(fareComponent.getKey()), 0.0);
			invoiceFareDetail.getFareComponents().put(fareComponent.getKey(),
					ObjectUtils.defaultIfNull(fareComponent.getValue(), 0d) + oldValue);
		}
	}

	private AirInvoiceDetail findTravellerWithSameId(List<AirInvoiceDetail> airInvoiceDetails, Long id) {
		for (AirInvoiceDetail invoiceDetail : airInvoiceDetails) {
			if (invoiceDetail.getTravellerInfo().getId() == id) {
				return invoiceDetail;
			}
		}
		return null;
	}

	public static void setProfileFieldsDb(String bookingId, String bookingUserId, List<DbAirOrderItem> airOrderItems) {
		CollectionServiceFilter filter = CollectionServiceFilter.builder().key(bookingUserId).build();
		List<Document> documentList = generalServiceCommunicator.fetchGeneralDocument(filter);
		if (CollectionUtils.isNotEmpty(documentList)) {
			UserProfileFields profileMeta =
					GsonUtils.getGson().fromJson(documentList.get(0).getData(), UserProfileFields.class);
			AirItemDetail itemDetail = detailService.get(bookingId);
			for (DbAirOrderItem x : airOrderItems) {
				for (FlightTravellerInfo t : x.getTravellerInfo()) {
					setPaxProfileFields(t, itemDetail, profileMeta);
				}
			}
		}
	}

	public static void setProfileFields(String bookingId, String bookingUserId, List<AirOrderItem> airOrderItems) {
		CollectionServiceFilter filter = CollectionServiceFilter.builder().key(bookingUserId).build();
		List<Document> documentList = generalServiceCommunicator.fetchGeneralDocument(filter);
		if (CollectionUtils.isNotEmpty(documentList)) {
			UserProfileFields profileMeta =
					GsonUtils.getGson().fromJson(documentList.get(0).getData(), UserProfileFields.class);
			AirItemDetail itemDetail = detailService.get(bookingId);
			for (AirOrderItem x : airOrderItems) {
				for (FlightTravellerInfo t : x.getTravellerInfo()) {
					setPaxProfileFields(t, itemDetail, profileMeta);
				}
			}
		}
	}

	private static void setPaxProfileFields(TravellerInfo pax, AirItemDetail itemDetail,
			UserProfileFields profileMeta) {
		profileMeta.InitMap();
		UserProfile visibleProfile = UserProfile.builder().build();
		visibleProfile.setData(new HashMap<>());
		String paxKey = pax.getPaxKey();
		if (itemDetail != null && itemDetail.getInfo().getProfileData().get(paxKey) != null) {
			itemDetail.getInfo().getProfileData().get(paxKey).forEach((key, value) -> {
				if (profileMeta.getFieldMap().containsKey(key) && (profileMeta.getFieldMap().get(key).isVInvoice()
						|| profileMeta.getFieldMap().get(key).isVReports())) {
					FieldMetaInfo meta = profileMeta.getFieldMap().get(key);
					String label =
							StringUtils.isBlank(meta.getDisplayLabel()) ? meta.getName() : meta.getDisplayLabel();
					visibleProfile.getData().put(label, value);
				}
			});
		}
		pax.setUserProfile(visibleProfile);
	}

	@Override
	public void afterProcess() throws Exception {}

}
