package com.tgs.services.oms.servicehandler.air;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;
import com.tgs.services.oms.restmodel.air.AirManualBookingRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.CommissionExceededAlert;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.air.AirManualBookingManager;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.NumberUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirManualReviewHandler extends ServiceHandler<AirManualBookingRequest, AirReviewResponse> {

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private AirManualBookingManager manualBookingManager;

	@Autowired
	private CommercialCommunicator commericialCommunicator;

	private List<DbAirOrderItem> dbAirOrderItems;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		try {
			User user = userService.getUserFromCache(request.getUserId());
			dbAirOrderItems = getAirOrderItems();
			List<TripInfo> tripInfos = getTripsFromAirOrderItems(dbAirOrderItems);
			AirSearchQuery searchQuery = fmsComm.getSearchQueryFromTripInfos(tripInfos);
			setAdditionalParams(dbAirOrderItems, searchQuery);
			String bookingId = request.getBookingId();
			if (StringUtils.isBlank(bookingId)) {
				bookingId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AIR)
						.subProduct((BaseUtils.isDomesticTrip(tripInfos.get(0))) ? AirType.DOMESTIC.getName()
								: AirType.INTERNATIONAL.getName())
						.build());
				request.setBookingId(bookingId);
			}
			if (BooleanUtils.isTrue(request.getIsCommissionEdited())) {
				calculateCommissionDependentComponents(tripInfos, user);
				validateCommission(tripInfos, user, bookingId);
			} else {
				calculateCommission(tripInfos, user, bookingId);
			}
			updateTravellerInfoAndCommissionIdOnItems(dbAirOrderItems, tripInfos);
			request.setAirOrderItems(getAirOrderItemList(dbAirOrderItems));
			SystemContextHolder.getContextData().getReqIds().add(bookingId);
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Error Occured for manual booking requested userid {} bookingid {}", request.getUserId(),
					request.getBookingId(), e);
			ErrorDetail errorDetail = new ErrorDetail();
			errorDetail.setErrCode(SystemError.SERVICE_EXCEPTION.errorCode());
			errorDetail.setMessage(SystemError.SERVICE_EXCEPTION.getMessage());
			response.addError(errorDetail);
		}
	}

	private void setAdditionalParams(List<DbAirOrderItem> dbAirOrderItems, AirSearchQuery searchQuery) {
		if (CollectionUtils.isNotEmpty(dbAirOrderItems) && searchQuery != null) {
			dbAirOrderItems.forEach(item -> {
				if (item.getAdditionalInfo() == null) {
					item.setAdditionalInfo(AdditionalAirOrderItemInfo.builder().build());
				}
				item.getAdditionalInfo().setType(searchQuery.getAirType().getCode());
				item.getAdditionalInfo().setSearchType(searchQuery.getSearchType());
			});
		}
	}

	@Override
	public void afterProcess() throws Exception {
		try {
			response.setTotalPriceInfo(getTotalPriceInfo(request.getAirOrderItems()));
			temporaryFixForNCM();
			response.setBookingId(request.getBookingId());
		} finally {
			if (request.getFlowType() == null) {
				request.setFlowType(OrderFlowType.MANUAL_ORDER);
			}
			manualBookingManager.storeBookingRequest(request);
		}
	}

	@Deprecated
	private void temporaryFixForNCM() {
		Map<FareComponent, Double> fareComponents =
				response.getTotalPriceInfo().getTotalFareDetail().getFareComponents();
		Map<FareComponent, Double> commissionFareComponents =
				response.getTotalPriceInfo().getTotalFareDetail().getAddlFareComponents(FareComponent.NCM);
		double totalCommission = FareComponent.getAllCommisionComponents().stream()
				.mapToDouble(commissionComponent -> commissionFareComponents.getOrDefault(commissionComponent, 0.0))
				.sum();
		fareComponents.put(FareComponent.NCM,
				totalCommission - commissionFareComponents.getOrDefault(FareComponent.TDS, 0.0));
	}

	/**
	 * As Request on Data Model Convert to DB Model for trip processing (convert request.getAirOrderItems (Data Model)
	 * to DB Model)
	 */
	private List<DbAirOrderItem> getAirOrderItems() {
		List<DbAirOrderItem> dbAirOrderItems = new ArrayList<>();
		request.getAirOrderItems().forEach(airOrderItem -> {
			DbAirOrderItem dbAirOrderItem1 = new GsonMapper<>(airOrderItem, DbAirOrderItem.class).convert();
			dbAirOrderItem1.setStatus(AirItemStatus.IN_PROGRESS.getCode());
			dbAirOrderItem1.setAirlinecode(fmsComm.getAirlineInfo(airOrderItem.getAirlinecode()).getCode());
			dbAirOrderItem1.setBookingId(request.getBookingId());
			AdditionalAirOrderItemInfo oldInfo = airOrderItem.getAdditionalInfo();
			AdditionalAirOrderItemInfo newInfo = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(oldInfo),
					new TypeToken<AdditionalAirOrderItemInfo>() {}.getType());
			if (newInfo == null) {
				newInfo = AdditionalAirOrderItemInfo.builder().build();
			}
			long supplierRuleId = -1L;
			if (StringUtils.isBlank(dbAirOrderItem1.getSupplierId())) {
				SupplierInfo supplierInfo = fmsComm.getSupplierInfo(dbAirOrderItem1.getSupplierId());
				if (supplierInfo != null) {
					SupplierRule supplierRule = fmsComm.getSupplierRule(supplierInfo);
					supplierRuleId = supplierRule.getId();
				}
			}
			newInfo.setSupplierRuleId(supplierRuleId);
			newInfo.setIsReturnSegment(BooleanUtils.isTrue(oldInfo.getIsReturnSegment()));
			if (StringUtils.isBlank(newInfo.getDepartureTerminal())) {
				newInfo.setDepartureTerminal(fmsComm.getAirportInfo(airOrderItem.getSource()).getTerminal());
			}
			if (StringUtils.isBlank(newInfo.getArrivalTerminal())) {
				newInfo.setArrivalTerminal(fmsComm.getAirportInfo(airOrderItem.getDest()).getTerminal());
			}
			newInfo.setDuration(
					Duration.between(airOrderItem.getDepartureTime(), airOrderItem.getArrivalTime()).toMinutes());
			if (StringUtils.isNotBlank(oldInfo.getOperatingAirline()))
				newInfo.setOperatingAirline(fmsComm.getAirlineInfo(oldInfo.getOperatingAirline()).getCode());
			dbAirOrderItem1.setAdditionalInfo(newInfo);
			dbAirOrderItems.add(dbAirOrderItem1);
		});
		return dbAirOrderItems;
	}

	public PriceInfo getTotalPriceInfo(List<AirOrderItem> airOrderItems) {
		PriceInfo priceInfo = PriceInfo.builder().build();
		priceInfo.setTotalFareDetail(new FareDetail());
		airOrderItems.forEach(airOrderItem -> {
			airOrderItem.getTravellerInfo().forEach(travellerInfo -> {
				AirBookingUtils.generateFareComponents(priceInfo, travellerInfo, null, false);
			});
		});
		return priceInfo;
	}

	public double getNetCommissionForOrderItems(List<DbAirOrderItem> airOrderItems,
			ToDoubleFunction<Map<FareComponent, Double>> amountFunction) {
		return airOrderItems.stream().map(DbAirOrderItem::getTravellerInfo).reduce(ListUtils::union).get().stream()
				.mapToDouble(travellerInfo -> amountFunction
						.applyAsDouble(travellerInfo.getFareDetail().getFareComponents()))
				.sum();
	}

	public double getNetCommissionForTrips(List<TripInfo> tripInfos,
			ToDoubleFunction<Map<FareComponent, Double>> amountFunction) {
		return tripInfos.stream().map(TripInfo::getSegmentInfos).reduce(ListUtils::union).get().stream()
				.map(SegmentInfo::getTravellerInfo).reduce(ListUtils::union).get().stream()
				.mapToDouble(travellerInfo -> amountFunction
						.applyAsDouble(travellerInfo.getFareDetail().getFareComponents()))
				.sum();
	}

	private void validateCommission(List<TripInfo> tripInfos, User user, String bookingId) {
		double netSubmittedCommission, netCalculatedCommission;
		ToDoubleFunction<Map<FareComponent, Double>> netCommissionFunction = fareComponents -> {
			double totalCommission = FareComponent.getAllCommisionComponents().stream()
					.mapToDouble(commissionComponent -> fareComponents.getOrDefault(commissionComponent, 0.0)).sum();
			return totalCommission - fareComponents.getOrDefault(FareComponent.TDS, 0.0)
					- fareComponents.getOrDefault(FareComponent.PMTDS, 0.0);
		};
		List<TripInfo> dupTripInfos = tripInfos.stream()
				.map(trip -> new GsonMapper<TripInfo>(trip, TripInfo.class).convert()).collect(Collectors.toList());
		calculateCommission(dupTripInfos, user, bookingId);

		netSubmittedCommission = getNetCommissionForOrderItems(dbAirOrderItems, netCommissionFunction);
		netCalculatedCommission = getNetCommissionForTrips(dupTripInfos, netCommissionFunction);
		if (NumberUtils.compareDoubleUpto2Decimal(netSubmittedCommission, netCalculatedCommission) > 0
				&& netSubmittedCommission > netCalculatedCommission) {
			log.info("Net Commission submitted for bookingId {} exceeds the limit {}", bookingId,
					netCalculatedCommission);
			response.addAlert(CommissionExceededAlert.builder()
					.message("Net Commission submitted exceeds the limit "
							+ TgsStringUtils.formatCurrency(netCalculatedCommission))
					.type(AlertType.COMMISSION_DIFF.name()).build());
		}
	}

	/**
	 * 1.Initially Reset the Commercial Type fareComponents to zero 2.Apply User Commission on trips 3.Other Commissions
	 * are not applied [Due to manual order]
	 *
	 * @param tripInfos
	 */
	public void calculateCommission(List<TripInfo> tripInfos, User user, String bookingId) {
		try {
			AirSearchQuery searchQuery = BaseUtils.combineSearchQuery(BaseUtils.getSearchQueryFromTripInfos(tripInfos));
			commericialCommunicator.resetCommission(tripInfos, user);
			commericialCommunicator.processUserCommission(tripInfos, user, searchQuery);
		} catch (Exception e) {
			log.error("Commission Not applied for booking id {}", bookingId, e);
		}
	}

	/**
	 * In case commission components are sent in request, calculate dependent components.
	 * 
	 * @param tripInfos
	 * @param user
	 * @param bookingId
	 */
	public void calculateCommissionDependentComponents(List<TripInfo> tripInfos, User user) {
		tripInfos.forEach(trip -> {
			trip.getSegmentInfos().forEach(segment -> {
				segment.getBookingRelatedInfo().getTravellerInfo().forEach(priceInfo -> {
					FareDetail fareDetail = priceInfo.getFareDetail();
					Map<FareComponent, Double> fareComponents = fareDetail.getFareComponents();
					FareComponent.getCommercialComponents().forEach(commissionComponent -> {
						if (fareComponents.containsKey(commissionComponent)) {
							Double amount = fareComponents.getOrDefault(commissionComponent, 0.0);
							BaseUtils.updateDependentFareComponents(fareComponents, commissionComponent, amount, user);
						}
					});
				});
			});
		});
	}

	public List<com.tgs.services.oms.datamodel.air.AirOrderItem> getAirOrderItemList(
			List<DbAirOrderItem> dbAirOrderItems) {
		List<com.tgs.services.oms.datamodel.air.AirOrderItem> airOrderItems1 = new ArrayList<>();
		dbAirOrderItems.forEach(airOrderItem -> {
			airOrderItems1.add(airOrderItem.toDomain());
		});
		return airOrderItems1;
	}

	/**
	 * 1.Flight Travaller Info will be copied to AirOrderItem 2.Add the commission Plan Id & rule id in AirOrder Item
	 *
	 * @param dbAirOrderItems
	 * @param tripInfos
	 */
	public void updateTravellerInfoAndCommissionIdOnItems(List<DbAirOrderItem> dbAirOrderItems,
			List<TripInfo> tripInfos) {
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				DbAirOrderItem dbAirOrderItem =
						AirBookingUtils.findOrderItemFromSegmentInfo(segmentInfo, dbAirOrderItems);
				if (dbAirOrderItem != null) {
					dbAirOrderItem.setTravellerInfo(segmentInfo.getBookingRelatedInfo().getTravellerInfo());
					AdditionalAirOrderItemInfo additionalItemInfo = dbAirOrderItem.getAdditionalInfo();
					if (additionalItemInfo == null) {
						additionalItemInfo = AdditionalAirOrderItemInfo.builder().build();
					}


					/**
					 * Setting total amount field in FareComponent && SSR
					 */
					Double totalAmount = 0d;
					for (FlightTravellerInfo tInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
						updateTotalFare(tInfo);
						totalAmount += tInfo.getFareDetail().getFareComponents().get(FareComponent.TF);
						cleanSSRData(tInfo);
						updateSSRAmount(tInfo);
					}

					dbAirOrderItem.setAmount(totalAmount);

					dbAirOrderItem
							.setMarkup(AirBookingUtils.getTotalFareComponentAmount(dbAirOrderItem, FareComponent.MU));
					if (segmentInfo.getPriceInfo(0) != null) {
						additionalItemInfo
								.setCommercialRuleId(segmentInfo.getPriceInfo(0).getMiscInfo().getCommericialRuleId());
					}
					dbAirOrderItem.setAdditionalInfo(additionalItemInfo);
				}
			});
		});
	}

	private void updateTotalFare(FlightTravellerInfo tInfo) {
		Map<FareComponent, Double> fareComponents = tInfo.getFareDetail().getFareComponents();
		double totalFare =
				fareComponents.getOrDefault(FareComponent.TF, 0d) + BookingUtils.getAmountChargedByAirline(tInfo);
		fareComponents.put(FareComponent.TF, totalFare);
		/**
		 * Add mentioned components if commission edited.
		 */
		if (BooleanUtils.isTrue(request.getIsCommissionEdited())) {
			/**
			 * MFT is already added in TF.
			 */
			addFareComponentsToTotalFare(fareComponents, FareComponent.MF, FareComponent.CMU);
		}
	}

	private static void addFareComponentsToTotalFare(Map<FareComponent, Double> fareComponentsMap,
			FareComponent... components) {
		double totalFare = fareComponentsMap.getOrDefault(FareComponent.TF, 0d);
		for (FareComponent component : components) {
			totalFare += fareComponentsMap.getOrDefault(component, 0d);
		}
		fareComponentsMap.put(FareComponent.TF, totalFare);
	}

	private static void cleanSSRData(FlightTravellerInfo tInfo) {
		if (tInfo.getSsrBaggageInfo() != null && StringUtils.isBlank(tInfo.getSsrBaggageInfo().getDesc())) {
			tInfo.setSsrBaggageInfo(null);
		}
		if (tInfo.getSsrMealInfo() != null && StringUtils.isBlank(tInfo.getSsrMealInfo().getDesc())) {
			tInfo.setSsrMealInfo(null);
		}
		if (tInfo.getSsrSeatInfo() != null && StringUtils.isBlank(tInfo.getSsrSeatInfo().getDesc())) {
			tInfo.setSsrSeatInfo(null);
		}
	}

	private static void updateSSRAmount(FlightTravellerInfo tInfo) {
		if (tInfo.getFareDetail() != null && MapUtils.isNotEmpty(tInfo.getFareDetail().getFareComponents())) {
			if (tInfo.getSsrBaggageInfo() != null) {
				tInfo.getSsrBaggageInfo()
						.setAmount(tInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.BP, 0d));
			}
			if (tInfo.getSsrMealInfo() != null) {
				tInfo.getSsrMealInfo()
						.setAmount(tInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.MP, 0d));
			}
			if (tInfo.getSsrSeatInfo() != null) {
				tInfo.getSsrSeatInfo()
						.setAmount(tInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.SP, 0d));
			}
		}
	}

	/**
	 * Create Trip List for AirOrder Items(request)
	 */
	public List<TripInfo> getTripsFromAirOrderItems(List<DbAirOrderItem> dbAirOrderItems) {
		return BookingUtils.createTripListFromSegmentList(
				AirBookingUtils.convertAirOrderItemListToSegmentInfoList(dbAirOrderItems, null));
	}

}
