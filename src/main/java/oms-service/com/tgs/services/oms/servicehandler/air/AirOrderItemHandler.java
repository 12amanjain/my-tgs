package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.CorporateTripServiceCommunicator;
import com.tgs.services.base.communicator.DealInventoryCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.ims.datamodel.InventoryOrder;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirOrderItemManager;

import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirOrderItemHandler extends ServiceHandler<List<AirOrderItem>, BaseResponse> {

	@Autowired
	AirOrderItemManager itemManager;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private DealInventoryCommunicator invCommunicator;

	@Autowired
	private CorporateTripServiceCommunicator corpTripCommunicator;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		User user = SystemContextHolder.getContextData().getUser();
		Order order = null;
		for (AirOrderItem airOrderItem : request) {
			if (airOrderItem.getId() != null) {
				DbAirOrderItem dbAirOrderItem = itemManager.findByAirOrderItemId(airOrderItem.getId());
				log.info("Updating order and item details for bookingId {}", dbAirOrderItem.getBookingId());
				order = orderManager.findByBookingId(dbAirOrderItem.getBookingId(), null);

				/**
				 * Post successful order if staff want to change any detail without any assignment , they will be allow
				 * to do that. For example : SupplierId change post success status
				 */
				if (!user.getUserId().equals(order.getAdditionalInfo().getAssignedUserId())
						&& !(order.getStatus().equals(OrderStatus.SUCCESS)
								&& order.getCreatedOn().isBefore(LocalDateTime.now().minusMinutes(60)))) {
					throw new CustomGeneralException(SystemError.DIFFERENT_ASSIGNED_USER);
				}
				if (dbAirOrderItem.getAdditionalInfo().getInventoryId() != null)
					updatePNRInInventoryOrder(dbAirOrderItem.getBookingId(), airOrderItem.getTravellerInfo(),
							dbAirOrderItem.getAdditionalInfo().getInventoryId());
				if (StringUtils.isNotBlank(order.getAdditionalInfo().getTripId()))
					updateStatusInCorporateTrip(order);

				updateTravellerData(dbAirOrderItem, airOrderItem);
				updateSupplierId(dbAirOrderItem, airOrderItem);
				itemManager.save(dbAirOrderItem, order, itemManager.getAirItemStatus(dbAirOrderItem.toDomain(), order));
				log.info("Updation for order and item details completed for bookingId {}", order.getBookingId());
			}
		}
		itemManager.save(itemManager.findByBookingId(order.getBookingId()), order, null);
	}

	private void updateStatusInCorporateTrip(Order order) {
		String status = OrderStatus.SUCCESS.equals(order.getStatus()) ? "Booked" : "In_Progress";
		corpTripCommunicator.linkBookingIdWithTrip(order.getAdditionalInfo().getTripId(), order.getBookingId(), status);

	}

	private void updatePNRInInventoryOrder(String bookingId, List<FlightTravellerInfo> travellersList,
			String inventoryId) {
		InventoryOrderFilter filter = InventoryOrderFilter.builder().referenceIdIn(Arrays.asList(bookingId))
				.inventoryIdIn(Arrays.asList(inventoryId)).build();
		List<InventoryOrder> inventories = invCommunicator.fetchInventoryOrders(filter);
		if (CollectionUtils.isNotEmpty(inventories)) {
			InventoryOrder invOrder = inventories.get(0);
			updateTravellerPNR(invOrder, travellersList);
			((AirInventoryOrderInfo) invOrder.getAdditionalInfo()).setPnr(travellersList.get(0).getPnr());
			invCommunicator.saveInventoryOrder(invOrder);
		}
	}

	private void updateSupplierId(DbAirOrderItem dbAirOrderItem, AirOrderItem airOrderItem) {
		if (StringUtils.isNotBlank(airOrderItem.getSupplierId())) {
			if (!airOrderItem.getSupplierId().equals(dbAirOrderItem.getSupplierId())) {
				if (fmsCommunicator.getSupplierInfo(airOrderItem.getSupplierId()) == null)
					throw new CustomGeneralException(SystemError.INVALID_FBRC_SUPPLIERID, airOrderItem.getSupplierId());
				if (dbAirOrderItem.getAdditionalInfo().getOrgSupplierName() == null)
					dbAirOrderItem.getAdditionalInfo().setOrgSupplierName(dbAirOrderItem.getSupplierId());
				dbAirOrderItem.setSupplierId(airOrderItem.getSupplierId());

			}
		}
	}

	private DbAirOrderItem updateTravellerData(DbAirOrderItem dbAirOrderItem, AirOrderItem airOrderItem) {
		List<FlightTravellerInfo> dbTravellers = dbAirOrderItem.getTravellerInfo();
		airOrderItem.getTravellerInfo().forEach(dmTravellerInfo -> {
			FlightTravellerInfo travellerInfo1 = dbTravellers.stream()
					.filter(travellerInfo2 -> dmTravellerInfo.getId().equals(travellerInfo2.getId())).findFirst().get();
			new GsonMapper<>(dmTravellerInfo, travellerInfo1, FlightTravellerInfo.class).convert();
		});
		return dbAirOrderItem;
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private void updateTravellerPNR(InventoryOrder invOrder, List<FlightTravellerInfo> travellersList) {
		invOrder.getTravellerInfo().forEach(traveller -> {
			travellersList.forEach(newTravellerList -> {
				if (traveller.getId().equals(newTravellerList.getId())) {
					traveller.setPnr(newTravellerList.getPnr());
				}
			});
		});
	}
}
