package com.tgs.services.oms.servicehandler.air;

import java.text.DecimalFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AttachmentMetadata;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.fms.datamodel.AdditionalAirOrderItemInfo;
import com.tgs.services.fms.datamodel.AirMessageAttributes;
import com.tgs.services.fms.datamodel.AirMessagePriceInfo;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirMessageRequest;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppAttributes;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AirOrderMessageHandler extends ServiceHandler<AirMessageRequest, BaseResponse> {

	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	private AirOrderItemManager itemManager;

	@Autowired
	protected OrderManager orderManager;

	@Autowired
	protected GeneralServiceCommunicator gsCommunicator;

	@Autowired
	protected FMSCommunicator fmsCommunicator;

	DecimalFormat df = new DecimalFormat("0.00");

	@Value("${attachmentPath}")
	private String attachmentPath;

	@Value("${bookingUrl}")
	private String bookingUrl;

	@Value("${S3Domain}")
	private String S3Domain;

	@Override
	public void beforeProcess() {
		if (request.getMode().equals(MessageMedium.EMAIL)) {
			Order order = new GsonMapper<>(orderManager.findByBookingId(request.getBookingId()), Order.class).convert();
			if (OrderStatus.SUCCESS.equals(order.getStatus()))
				sendBookingEmail(order, EmailTemplateKey.AIR_BOOK_EMAIL);
			else if (OrderStatus.ON_HOLD.equals(order.getStatus()))
				sendBookingEmail(order, EmailTemplateKey.AIR_BOOKING_HOLD_EMAIL);
			else if (OrderStatus.PENDING.equals(order.getStatus()))
				sendBookingEmail(order, EmailTemplateKey.AIR_BOOKING_PENDING_EMAIL);
			else if (OrderStatus.FAILED.equals(order.getStatus()))
				sendBookingEmail(order, EmailTemplateKey.AIR_BOOKING_FAILED_EMAIL);
		} else if (request.getMode().equals(MessageMedium.SMS)) {
			Order order = new GsonMapper<>(orderManager.findByBookingId(request.getBookingId()), Order.class).convert();
			if (OrderStatus.SUCCESS.equals(order.getStatus()))
				sendBookingSms(order, SmsTemplateKey.BOOKING_CONFIRMATION_SMS);
			else if (OrderStatus.ON_HOLD.equals(order.getStatus()))
				sendBookingSms(order, SmsTemplateKey.BOOKING_HOLD_SMS);
			else if (OrderStatus.PENDING.equals(order.getStatus()))
				sendBookingSms(order, SmsTemplateKey.BOOKING_PENDING_SMS);
			else if (OrderStatus.FAILED.equals(order.getStatus()))
				sendBookingSms(order, SmsTemplateKey.BOOKING_FAILURE_SMS);
		} else if (request.getMode().equals(MessageMedium.WHATSAPP)) {
			Order order = new GsonMapper<>(orderManager.findByBookingId(request.getBookingId()), Order.class).convert();
			if (OrderStatus.SUCCESS.equals(order.getStatus()))
				sendBookingWhatsAppMessage(order, WhatsAppTemplateKey.FLIGHT_TICKET);
		}
	}

	@Override
	public void process() {}

	@Override
	public void afterProcess() {}

	public void sendBookingEmail(Order order, EmailTemplateKey templateKey) {
		try {
			ExecutorUtils.getCommunicationThreadPool().submit(() -> {
				if (request != null
						|| (order != null && CollectionUtils.isNotEmpty(order.getDeliveryInfo().getEmails()))) {
					User bookingUser = userService.getUserFromCache(order.getBookingUserId());
					String toEmailId = OrderUtils.getToEmailId(order, bookingUser, request);
					log.info("Sending Email to emailIds {}", toEmailId);
					List<TripInfo> tripInfos = itemManager.findTripsByBookingIds(Arrays.asList(order.getBookingId()))
							.get(order.getBookingId());
					log.info("[SendGridES] Fetched order of bookingid {} for sending email", order.getBookingId());
					List<FlightTravellerInfo> travellerInfos = tripInfos.get(0).getTravllerInfos();
					AirBookingUtils.setPriceInfoList(BookingUtils.getSegmentInfos(tripInfos));
					PriceInfo priceInfo =
							AirBookingUtils.getTotalPriceInfo(BookingUtils.getSegmentInfos(tripInfos), 0, null, false);
					AirMessageAttributes messageAttributes = getMessageAttributes(templateKey, toEmailId, bookingUser);

					messageAttributes = populateEmailAttributes(messageAttributes, order, tripInfos, priceInfo,
							travellerInfos, bookingUser);
					if (request != null && request.isWithoutPrice()) {
						messageAttributes.setWithPrice(false);
					}
					if (!request.isWithoutGst()) {
						messageAttributes.setGstInfo(orderManager.getGstInfo(order.getBookingId()));
					}
					messageAttributes.setAgentName(bookingUser.getName());
					messageAttributes.setAgentContact(bookingUser.getMobile());
					if (bookingUser.getAddressInfo() != null)
						messageAttributes.setAgentAddress(bookingUser.getAddressInfo().getAddress());
					messageAttributes.setAgentLogo(getAgentLogoUrl(bookingUser));
					messageAttributes.setFromEmail(getFromEmail(order, bookingUser));
					messageAttributes.setPartnerId(order.getPartnerId());
					messageAttributes.setRole(bookingUser.getRole());
					messageAttributes.setCcEmailId(bookingUser.getAdditionalInfo().getCcEmailIds());
					// Sending empty string because it will be then handled in the email template. If empty string found
					// we won't show trip id in email
					messageAttributes.setTripId(showTripId(bookingUser) && order.getAdditionalInfo().getTripId() != null
							? order.getAdditionalInfo().getTripId()
							: "");
					sendEmail(order, messageAttributes);
				}
			});
		} catch (Exception ex) {
			log.error("Error while sending message for bookingId {}. Ex: {}", order.getBookingId(), ex.getMessage());
		}
	}

	private boolean showTripId(User bookingUser) {
		return UserRole.getMidOfficeRoles().contains(bookingUser.getRole())
				|| UserRole.corporate(bookingUser.getRole());
	}

	private String getAgentLogoUrl(User bookingUser) {
		if (StringUtils.isNotBlank(bookingUser.getAdditionalInfo().getLogoURL())) {
			return StringUtils.join("http:", bookingUser.getAdditionalInfo().getLogoURL());
		}
		return null;
	}

	private String getFromEmail(Order order, User user) {
		if (order.getStatus().equals(OrderStatus.PENDING)) {
			return null;
		}
		return user.getEmail();
	}

	public AirMessageAttributes populateEmailAttributes(AirMessageAttributes messageAttributes, Order order,
			List<TripInfo> tripInfos, PriceInfo tripPriceInfo, List<FlightTravellerInfo> travellerInfos,
			User bookingUser) {
		messageAttributes.setStatus(order.getStatus().name());
		messageAttributes.setTripInfos(tripInfos);
		messageAttributes.setPaxCount(Integer.toString(travellerInfos.size()));
		messageAttributes.setTravellerList(getProcessedTravellerInfo(tripInfos));
		messageAttributes.setBookingId(order.getBookingId());
		LocalDateTime bookingTime = order.getCreatedOn();
		if (bookingTime != null) {
			messageAttributes.setBookingTime(
					DateFormatterHelper.formatDateTime(bookingTime, DateFormatType.BOOKING_EMAIL_FORMAT));
		}
		populateDateTimeInfo(messageAttributes, tripInfos);
		messageAttributes.setCabinClass(WordUtils.capitalizeFully(tripInfos.get(0).getCabinClass().name()));
		AirMessagePriceInfo messagePriceInfo = AirMessagePriceInfo.builder().build();
		double discount = 0d;
		if (tripPriceInfo != null && MapUtils.isNotEmpty(tripPriceInfo.getTotalFareDetail().getFareComponents())) {
			messagePriceInfo.setBaseFare(df.format(
					tripPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.BF, 0.0)));
			messagePriceInfo.setTotalTax(df.format(tripPriceInfo.getTotalFareDetail().getFareComponents()
					.getOrDefault(FareComponent.TAF, 0.0)
					+ tripPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.AFS, 0.0)));
			messagePriceInfo.setSsrFare(df.format(
					tripPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.SSRP, 0.0)));
			Double totalAmount = getTotalAmountWithmarkUp(tripPriceInfo);
			messagePriceInfo.setTotalAmount(df.format(totalAmount));
			messagePriceInfo.setFareType(tripPriceInfo.getTotalFareDetail().getFareType());
			messagePriceInfo.setNetFare(messagePriceInfo.getTotalAmount());
			for (TripInfo trip : tripInfos) {
				for (SegmentInfo segment : trip.getSegmentInfos()) {
					for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
						// Voucher Discount
						discount += traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.VD, 0d);
						if (UserRole.corporate(bookingUser.getRole())) {
							discount += AirBookingUtils.getGrossCommissionForPax(traveller, null, true);
						}
					}
				}
			}
			if (discount > 0) {
				messagePriceInfo.setNetFare(df.format(totalAmount - discount));
				messagePriceInfo.setDiscount(df.format(discount));
			}
		} else {
			log.error("TripPrice Info is empty Unable to send email {}", order.getBookingId());
		}
		messagePriceInfo.setAmountPaid(df.format(order.getAmount() + order.getMarkup() - discount));
		messageAttributes.setPriceInfo(messagePriceInfo);
		log.debug("[SendGridES] Message attributes constructed for sending email");

		ExecutorService executor = Executors.newSingleThreadExecutor();
		ContextData contextData = SystemContextHolder.getContextData();
		Future<?> future = executor.submit(() -> generatePdf(messageAttributes, order, contextData));
		try {
			future.get(30, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			log.error("[HtmlToPdf] Unable to generate PDF for url for bookingid in 30 seconds {}", order.getBookingId(),
					e);
			future.cancel(true);
		}
		executor.shutdownNow();
		return messageAttributes;
	}

	private AttachmentMetadata getAttachmentData(Order order) {
		if (StringUtils.isEmpty(S3Domain) || S3Domain == "${S3Domain}") {
			return null;
		}
		String url =
				"https://s3.ap-south-1.amazonaws.com/".concat(S3Domain).concat("/wa/wa-").concat(order.getBookingId());
		int retryCount = 6;
		while (retryCount > 0) {
			Map<String, String> headerParams = new HashMap<>();
			headerParams.put("Content-Type", "application/json");
			try {
				Thread.sleep(5 * 1000);
				HttpUtils httpUtils = HttpUtils.builder().urlString(url).requestMethod(HttpUtils.REQ_METHOD_GET)
						.headerParams(headerParams).build();

				Object responseObj = httpUtils.getResponse(null).orElse(null);
				int status = httpUtils.getStatus();
				if (status == 200) {
					retryCount = 0;
					String fileName = "wa-" + order.getBookingId() + ".pdf";
					AttachmentMetadata attachmentData =
							AttachmentMetadata.builder().url(url).fileName(fileName).build();
					return attachmentData;
				} else
					retryCount--;
			} catch (Exception e) {
				retryCount--;
			}
		}
		return null;
	}

	private void generatePdf(AirMessageAttributes messageAttributes, Order order, ContextData contextData) {
		User bookingUser = userService.getUserFromCache(order.getBookingUserId());
		String url = bookingUrl.concat("?bookingId=").concat(order.getBookingId());
		if (StringUtils.isNotBlank(bookingUser.getAdditionalInfo().getLogoURL()))
			url = url.concat("&agentLogo=").concat(bookingUser.getAdditionalInfo().getLogoURL());
		if (request != null && request.isWithoutPrice()) {
			url = StringUtils.join(url, "&showPrice=0");
		}
		if (request != null && request.isWithoutGst()) {
			url = StringUtils.join(url, "&gst=false");
		}
		if (request != null && request.isOldPrintCopy()) {
			url = StringUtils.join(url, "&isOldPrintCopy=true");
		}
		if (request != null && request.isNewTicketPrint()) {
			url = StringUtils.join(url, "&newTicketPrint=true");
		}
		log.debug("URL to generate booking confirmation pdf is {} ", url);
		String fileName = "Booking_Details_" + order.getBookingId() + ".pdf";
		Map<String, String> headerParams = getHeaderParams(contextData);
		HttpUtils httpUtils = HttpUtils.builder().urlString(url).headerParams(headerParams).timeout(30000).build();
		try {
			byte[] readData = httpUtils.getByteArrayFromInputStream();
			AttachmentMetadata attachmentData =
					AttachmentMetadata.builder().fileData(readData).fileName(fileName.toString()).build();
			messageAttributes.setAttachmentData(attachmentData);
		} catch (Exception e) {
			log.error("Unable to generate PDF for url {} , filename {} ", url, fileName, e);
		}
		log.debug("[SendGridES] HtmltoPdf generated");
	}

	private Map<String, String> getHeaderParams(ContextData contextData) {
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Accept-Encoding", "gzip");
		headerParams.put("Content-Encoding", "gzip");
		headerParams.put("deviceid", "System");
		headerParams.put("Accept", "*/*");
		headerParams.put("Authorization", contextData.getHttpHeaders().getJwtToken());
		headerParams.put("allowexternal", "true");
		return headerParams;
	}

	private void populateDateTimeInfo(AirMessageAttributes messageAttributes, List<TripInfo> tripInfos) {
		Map<TripInfo, String> tripDepartureTime = new HashMap<>();
		Map<SegmentInfo, String> segmentDepartureTime = new HashMap<>();
		Map<SegmentInfo, String> segmentArrivalTime = new HashMap<>();
		tripInfos.forEach(tripInfo -> {
			tripDepartureTime.put(tripInfo,
					DateFormatterHelper.formatDate(tripInfo.getDepartureTime(), DateFormatType.BOOKING_EMAIL_FORMAT));
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				segmentDepartureTime.put(segmentInfo, DateFormatterHelper.formatDateTime(segmentInfo.getDepartTime(),
						DateFormatType.BOOKING_EMAIL_FORMAT));
				segmentArrivalTime.put(segmentInfo, DateFormatterHelper.formatDateTime(segmentInfo.getArrivalTime(),
						DateFormatType.BOOKING_EMAIL_FORMAT));
			});
		});
		messageAttributes.setTripDepartureTime(tripDepartureTime);
		messageAttributes.setSegmentDepartureTime(segmentDepartureTime);
		messageAttributes.setSegmentArrivalTime(segmentArrivalTime);
	}

	private Double getTotalAmountWithmarkUp(PriceInfo tripPriceInfo) {
		return tripPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0.0)
				+ tripPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.MU, 0.0);
	}

	private List<ProcessedFlightTravellerInfo> getProcessedTravellerInfo(List<TripInfo> tripInfos) {
		List<ProcessedFlightTravellerInfo> pFTravellers = new ArrayList<>();
		tripInfos.forEach(tripInfo -> {
			List<ProcessedFlightTravellerInfo> travellerInfos =
					AirBookingUtils.generateProcessedTravellerInfo(tripInfo.getSegmentInfos(), 0);
			travellerInfos.forEach(travellerInfo -> {
				ProcessedFlightTravellerInfo existTraveller = isTravellerExists(travellerInfo, pFTravellers);
				if (existTraveller != null) {
					existTraveller.getPnrDetails().putAll(travellerInfo.getPnrDetails());
					existTraveller.getTicketNumberDetails().putAll(travellerInfo.getTicketNumberDetails());
				} else {
					pFTravellers.add(travellerInfo);
				}
			});
		});
		return pFTravellers;
	}

	private ProcessedFlightTravellerInfo isTravellerExists(ProcessedFlightTravellerInfo travellerInfo,
			List<ProcessedFlightTravellerInfo> pFTravellers) {
		ProcessedFlightTravellerInfo existTravller = null;
		if (CollectionUtils.isNotEmpty(pFTravellers)) {
			Optional<ProcessedFlightTravellerInfo> tt = Optional.ofNullable(pFTravellers.stream()
					.filter(traveller -> Objects.equals(traveller.getFirstName(), travellerInfo.getFirstName())
							&& Objects.equals(traveller.getLastName(), travellerInfo.getLastName()))
					.findFirst()).orElse(null);
			if (tt.isPresent()) {
				existTravller = tt.get();
			}
		}
		return existTravller;
	}

	public void sendEmail(Order order, AirMessageAttributes messageAttributes) {
		if (OrderUtils.isMessageSendingRequired(messageAttributes.getKey(), MessageMedium.EMAIL,
				userService.getUserFromCache(order.getBookingUserId()), order)) {
			msgSrvCommunicator.sendMail(messageAttributes);
		}
	}

	public AirMessageAttributes getMessageAttributes(EmailTemplateKey templateKey, String toEmail, User user) {
		AbstractMessageSupplier<AirMessageAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<AirMessageAttributes>() {
					@Override
					public AirMessageAttributes get() {
						AirMessageAttributes messageAttributes = AirMessageAttributes.builder().build();
						messageAttributes.setToEmailId(toEmail);
						messageAttributes.setKey(templateKey.name());
						return messageAttributes;
					}
				};
		return emailAttributeSupplier.getAttributes();
	}

	public WhatsAppAttributes getWhatsAppAttributes(WhatsAppTemplateKey templateKey, List<String> attributes,
			User bookingUser) {
		AbstractMessageSupplier<WhatsAppAttributes> wappAttr = new AbstractMessageSupplier<WhatsAppAttributes>() {
			@Override
			public WhatsAppAttributes get() {
				WhatsAppAttributes wappAttributes = WhatsAppAttributes.builder().build();
				wappAttributes.setAttributes(attributes);
				wappAttributes.setKey(templateKey.name());
				return wappAttributes;
			}
		};
		return wappAttr.getAttributes();
	}

	public void sendBookingWhatsAppMessage(Order order, WhatsAppTemplateKey templateKey) {
		List<String> attributes = new ArrayList<>();
		User bookingUser = userService.getUserFromCache(order.getBookingUserId());
		if (templateKey.equals(WhatsAppTemplateKey.FLIGHT_TICKET)) {
			List<TripInfo> tripInfos =
					itemManager.findTripsByBookingIds(Arrays.asList(order.getBookingId())).get(order.getBookingId());
			List<FlightTravellerInfo> travellerInfos = tripInfos.get(0).getTravllerInfos();
			Set<String> passengers = travellerInfos.stream().map(t -> t.getFullName()).collect(Collectors.toSet());
			List<String> pnrs = travellerInfos.stream().map(t -> t.getPnr()).collect(Collectors.toList());
			List<SegmentInfo> segmentInfos = BookingUtils.getSegmentInfos(tripInfos);
			attributes.add(Integer.toString(travellerInfos.size()));
			attributes.add(Integer.toString(pnrs.size()));
			attributes.add(Integer.toString(segmentInfos.size()));
			attributes.add(Joiner.on(",").join(passengers));
			attributes.add(Integer.toString(travellerInfos.size()));
			attributes.add(order.getBookingId());
			attributes.add(pnrs.get(0));
			attributes.add(Integer.toString(pnrs.size() - 1));
			attributes.add(segmentInfos.get(0).getDepartureAndArrivalAirport());
			attributes.add(segmentInfos.get(0).getDepartTime().format(DateTimeFormatter.ofPattern("HH:mm a")));
			attributes.add(segmentInfos.get(0).getDepartTime().format(DateTimeFormatter.ofPattern("MM/dd/yyyy")));
			attributes.add(Integer.toString(segmentInfos.size()));
		}
		WhatsAppAttributes wappAttr = getWhatsAppAttributes(templateKey, attributes, bookingUser);
		wappAttr.setRecipientNumbers(OrderUtils.getToReceipientNumbers(order, bookingUser, request));
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<?> future = executor.submit(() -> wappAttr.setAttachmentData(getAttachmentData(order)));
		try {
			future.get(30, TimeUnit.SECONDS);
		} catch (InterruptedException | ExecutionException | TimeoutException e) {
			log.error("[WHATSAPP] Unable to get attachment pdf for 30 sec for bookingId {} and error is {}",
					order.getBookingId(), e.getMessage());
			future.cancel(true);
		}
		executor.shutdownNow();
		if (wappAttr.getAttachmentData() != null) {
			msgSrvCommunicator.sendWhatsAppMessage(wappAttr);
		}
	}

	public void sendBookingSms(Order order, SmsTemplateKey templateKey) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("bookingId", order.getBookingId());
				User bookingUser = userService.getUserFromCache(order.getBookingUserId());
				attributes.put("contact", bookingUser.getMobile());
				if (templateKey.equals(SmsTemplateKey.BOOKING_CONFIRMATION_SMS)) {
					List<TripInfo> tripInfos = itemManager.findTripsByBookingIds(Arrays.asList(order.getBookingId()))
							.get(order.getBookingId());
					List<FlightTravellerInfo> travellerInfos = BookingUtils.getTravellerInfos(tripInfos);
					Set<String> passengers =
							travellerInfos.stream().map(t -> t.getFullName()).collect(Collectors.toSet());
					Set<String> pnr = travellerInfos.stream().map(t -> t.getPnr()).collect(Collectors.toSet());
					List<SegmentInfo> segmentInfos = BookingUtils.getSegmentInfos(tripInfos);
					StringBuffer sb = new StringBuffer();
					int index = 1;
					for (SegmentInfo info : segmentInfos) {
						// 06:45 27-May
						sb.append("\n").append(index++).append(". AIR : ").append(info.getAirlineCode(false))
								.append("-").append(info.getFlightNumber()).append(" ")
								.append(info.getDepartureAndArrivalAirport()).append(" at ")
								.append(info.getDepartTime().format(DateTimeFormatter.ofPattern("HH:mm,EE dd-MMM")))
								.append(" arriving ")
								.append(info.getArrivalTime().format(DateTimeFormatter.ofPattern("HH:mm,EE dd-MMM")))
								.append(" ");
					}
					attributes.put("bookingUser", bookingUser.getName());
					attributes.put("passengers", Joiner.on(",").join(passengers));
					attributes.put("pnr", Joiner.on(",").join(pnr));
					attributes.put("msg", sb.toString());
				} else if (templateKey.equals(SmsTemplateKey.BOOKING_HOLD_SMS)) {
					List<AirOrderItem> airOrderItems =
							BaseModel.toDomainList(itemManager.findByBookingId(order.getBookingId()));
					AirlineInfo airlineInfo = fmsCommunicator.getAirlineInfo(airOrderItems.get(0).getAirlinecode());
					String timeLimit = "TBA";
					if (BooleanUtils.isTrue(airlineInfo.getIsLcc())) {
						AdditionalAirOrderItemInfo additionalInfo = airOrderItems.get(0).getAdditionalInfo();
						LocalDateTime tL = additionalInfo.getTimeLimit();
						// Some airlines do not provide time limit
						if (tL != null)
							timeLimit = "till ".concat(tL.format(DateTimeFormatter.ofPattern("HH:mm,EE dd-MMM")))
									.concat(" IST");
					}
					attributes.put("timeLimit", timeLimit);
				}
				return SmsAttributes.builder().key(templateKey.name())
						.recipientNumbers(OrderUtils.getToReceipientNumbers(order, bookingUser, request))
						.attributes(attributes).partnerId(order.getPartnerId()).role(bookingUser.getRole()).build();

			}
		};
		if (OrderUtils.isMessageSendingRequired(templateKey.name(), MessageMedium.SMS,
				userService.getUserFromCache(order.getBookingUserId()), order))
			msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());
	}

	public List<String> getPnrs(List<TripInfo> tripInfos) {
		Set<String> pnrs = new HashSet<>();
		tripInfos.forEach(tripInfo -> {
			tripInfo.getSegmentInfos().forEach(segmentInfo -> {
				List<FlightTravellerInfo> travellerInfos = segmentInfo.getBookingRelatedInfo().getTravellerInfo();
				pnrs.addAll(travellerInfos.stream().map(t -> t.getPnr()).collect(Collectors.toList()));
			});
		});
		return pnrs.stream().collect(Collectors.toList());
	}

}
