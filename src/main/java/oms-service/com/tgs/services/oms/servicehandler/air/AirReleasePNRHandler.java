package com.tgs.services.oms.servicehandler.air;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.restmodel.air.AirReleasePNRRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BookingUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.math3.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;

@Slf4j
@Service
public class AirReleasePNRHandler extends ServiceHandler<AirReleasePNRRequest, BaseResponse> {

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private UserServiceCommunicator umsCommunicator;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		Order order = orderManager.findByBookingId(request.getBookingId(), null);

		if (CollectionUtils.isEmpty(request.getPnrs())) {
			throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PNR);
		}
		if (order == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		if (OrderStatus.UNCONFIRMED.equals(order.getStatus())) {
			throw new CustomGeneralException(SystemError.PNR_ALREADY_CANCELLED);
		}
		User bookingUser = umsCommunicator.getUserFromCache(order.getBookingUserId());
		if (!OrderType.AIR.equals(order.getOrderType())
				|| !(OrderStatus.ON_HOLD.equals(order.getStatus()) || OrderStatus.UNCONFIRMED.equals(order.getStatus()))
				|| !AirBookingUtils.isAllowedFlowTypeForUnHold(order, bookingUser)) {
			throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);
		}

		try {
			List<DbAirOrderItem> dbAirorderItems = airOrderItemService.findByBookingId(request.getBookingId());
			List<SegmentInfo> segmentInfos =
					AirBookingUtils.convertAirOrderItemListToSegmentInfoList(dbAirorderItems, null);
			List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentInfos);
			Map<String, BookingSegments> cancellationSegmentMap =
					filterCancellationSegments(fmsCommunicator.getSupplierWiseBookingSegmentsUsingPNR(tripInfos),
							request.getPnrs(), order, bookingUser);
			log.info("Cancelling bookingid {} for segments  {}", order.getBookingId(), cancellationSegmentMap);
			fmsCommunicator.releasePnr(cancellationSegmentMap, order);
		} finally {
			order.getAdditionalInfo().setFlowType(OrderFlowType.PNR_RELEASED);
			orderManager.save(order);

		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private Map<String, BookingSegments> filterCancellationSegments(
			Map<String, BookingSegments> supplierWiseBookingSegments, List<String> cancellationPNR, Order order,
			User bookingUser) {

		Map<String, BookingSegments> cancellationSegments = new HashMap<>();
		for (String cancelPNR : cancellationPNR) {
			if (StringUtils.isEmpty(cancelPNR)) {
				throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PNR, cancelPNR);
			}
			Pair<String, BookingSegments> bookingSegmentsMap =
					AirReleasePNRHandler.getBookingSegmentFromAirlinePNR(cancelPNR, supplierWiseBookingSegments);
			if (bookingSegmentsMap == null
					|| !AirBookingUtils.isUnHoldAllowed(bookingSegmentsMap.getValue().getSegmentInfos(),
							order.getAdditionalInfo().getFlowType(), bookingUser)) {
				throw new CustomGeneralException(SystemError.INVALID_CANCELLATION_PNR, cancelPNR);
			}

			if (cancellationSegments.get(bookingSegmentsMap.getKey()) == null) {
				cancellationSegments.put(bookingSegmentsMap.getKey(), bookingSegmentsMap.getValue());
			}
		}
		return cancellationSegments;
	}

	private static Pair<String, BookingSegments> getBookingSegmentFromAirlinePNR(String airlinePNR,
			Map<String, BookingSegments> supplierWiseBookingSegments) {
		Pair<String, BookingSegments> keyValue = null;
		for (Entry<String, BookingSegments> entry : supplierWiseBookingSegments.entrySet()) {
			if (entry.getValue().getBookingSegmentsPNR().contains(airlinePNR)) {
				keyValue = new Pair<String, BookingSegments>(entry.getKey(), entry.getValue());
				break;
			}
		}
		return keyValue;
	}

}
