package com.tgs.services.oms.servicehandler.air;

import java.util.ArrayList;
import java.util.Objects;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.oms.Amendments.AmendmentHandlerFactory;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentChargesResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.servicehandler.air.amendment.AbstractAmendmentHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.Amendments.AmendmentChargesManager;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Setter
@Getter
public class AmendmentChargesHandler extends ServiceHandler<AmendmentRequest, AmendmentChargesResponse> {

	@Autowired
	private RaiseAirAmendmentHandler raiseAirAmendmentHandler;

	@Autowired
	private AirAmendmentManager airAmendmentManager;

	@Autowired
	private AmendmentChargesManager amendmentChargesManager;

	protected AmendmentResponse amendmentResponse;

	private String amendmentId;

	private AirOrderInfo airOrderInfo;

	@Override
	public void beforeProcess() throws Exception {
		airOrderInfo = airAmendmentManager.getOrderInfoForAmendment(request.getBookingId(), request.getType());
		if (airOrderInfo == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			AbstractAmendmentHandler amendmentHandler = AmendmentHandlerFactory.initChargeHandler(request);
			amendmentHandler.setAirOrderInfo(airOrderInfo);
			amendmentHandler.setAmendmentRequest(request);
			amendmentHandler.preSetDetails();
			amendmentHandler.initHandler();
			AirAmendmentPaxInfo amendmentRequest = amendmentHandler.buildInternalAmendmentRequest(airOrderInfo);
			AmendmentResponse amendmentResponse = AmendmentResponse.builder().build();
			amendmentResponse.setAmendmentItems(new ArrayList<>());
			raiseAirAmendmentHandler.initData(amendmentRequest, amendmentResponse);
			raiseAirAmendmentHandler.getResponse();
			amendmentHandler.setAmendmentResponse(amendmentResponse);
			amendmentHandler.validateRaiseAmendmentResponse();
			response = amendmentHandler.buildChargesResponse();
		} catch (CustomGeneralException ce) {
			log.error("Error occured while calculating amendment charges for booking id {} {}", request.getBookingId(),
					ce);
			throw ce;
		} catch (RuntimeException re) {
			log.error("Error occured while calculating amendment charges for booking id {} {}", request.getBookingId(),
					re);
			throw new CustomGeneralException(SystemError.CONTACT_SUPPORT);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
