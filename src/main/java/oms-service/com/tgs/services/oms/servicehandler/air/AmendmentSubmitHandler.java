package com.tgs.services.oms.servicehandler.air;

import java.util.ArrayList;
import java.util.Objects;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.oms.Amendments.AmendmentHandlerFactory;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.servicehandler.air.amendment.AbstractAmendmentHandler;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.Amendments.AmendmentChargesManager;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.PartnerAmendmentResponse;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Setter
@Getter
public class AmendmentSubmitHandler extends ServiceHandler<AmendmentRequest, PartnerAmendmentResponse> {

	@Autowired
	private RaiseAirAmendmentHandler raiseAirAmendmentHandler;

	@Autowired
	private AirAmendmentManager airAmendmentManager;

	@Autowired
	private AirAmendmentStatusHandler statusHandler;

	@Autowired
	private AmendmentChargesManager amendmentChargesManager;

	private String amendmentId;

	protected AirOrderInfo airOrderInfo;


	@Override
	public void beforeProcess() throws Exception {
		airOrderInfo = airAmendmentManager.getOrderInfoForAmendment(request.getBookingId(), request.getType());
		if (airOrderInfo == null) {
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
		if (StringUtils.isBlank(request.getRemarks()) && AmendmentType.CANCELLATION.equals(request.getType())) {
			throw new CustomGeneralException(SystemError.REMARKS);
		}
	}

	@Override
	public void process() throws Exception {
		try {
			AbstractAmendmentHandler amendmentHandler = AmendmentHandlerFactory.initPartnerSubmitHandler(request);
			amendmentHandler.setAirOrderInfo(airOrderInfo);
			amendmentHandler.setAmendmentRequest(request);
			amendmentHandler.preSetDetails();
			amendmentHandler.initHandler();
			AirAmendmentPaxInfo amendmentRequest = amendmentHandler.buildInternalAmendmentRequest(airOrderInfo);
			AmendmentResponse amendmentResponse = AmendmentResponse.builder().build();
			amendmentResponse.setAmendmentItems(new ArrayList<>());
			raiseAirAmendmentHandler.initData(amendmentRequest, amendmentResponse);
			amendmentResponse = raiseAirAmendmentHandler.getResponse();
			amendmentHandler.setAmendmentResponse(amendmentResponse);
			amendmentHandler.validateRaiseAmendmentResponse();
			amendmentHandler.additionalSubmitAmendment();
			Amendment amendment = amendmentResponse.getAmendmentItems().get(0);
			response.setAmendmentId(amendment.getAmendmentId());
			response.setBookingId(amendment.getBookingId());
			// response.setRemarks(amendment.getAdditionalInfo().getAgentRemarks());
		} catch (CustomGeneralException ce) {
			log.error("Error occured while submitting amendment for booking id {} {}", request.getBookingId(), ce);
			throw ce;
		} catch (RuntimeException re) {
			log.error("Error occured while submitting amendment for booking id {} {}", request.getBookingId(), re);
			throw new CustomGeneralException(SystemError.CONTACT_SUPPORT);
		}
	}


	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}


}
