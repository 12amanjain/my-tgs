package com.tgs.services.oms.servicehandler.air;

import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.AmendmentHandlerFactory;
import com.tgs.services.oms.Amendments.Processors.AirAmendmentProcessor;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.air.AirOrderItemService;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.ProcessAirAmendmentRequest;
import com.tgs.services.ums.datamodel.User;

@Service
public class ProcessAirAmendmentHandler extends ServiceHandler<ProcessAirAmendmentRequest, AmendmentResponse> {

	@Autowired
	private AmendmentService service;

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	private AirOrderItemService airOrderItemService;

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Autowired
	private UserServiceCommunicator userService;

	private Amendment amendment;

	@Override
	public void beforeProcess() {
		amendment = service.findByAmendmentId(request.getAmendmentId());
		validate();
	}

	private void validate() {
		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)
				.contains(AmendmentAction.PROCESS))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
	}

	@Transactional
	@Override
	public void process() {

		Double differentialPayment = null;

		User bookingUser = userService.getUserFromCache(amendment.getBookingUserId());

		amendment.getAdditionalInfo()
				.setRecallCommission(BaseUtils.isRecalCommission(request.isRecallCommission(), bookingUser.getRole()));

		if (!CollectionUtils.isEmpty(request.getModItems())) {
			List<DbAirOrderItem> orgAirOrderItems = airOrderItemService.findByBookingId(amendment.getBookingId());
			AirAmendmentProcessor amendmentHandler = AmendmentHandlerFactory.initHandler(amendment,
					request.getModItems(), orgAirOrderItems, bookingUser);
			amendmentHandler.patchAirOrderItems(false);
			amendmentHandler.process();
			differentialPayment = amendment.getAmount();
			amendment.getModifiedInfo().setAirOrderItems(amendmentHandler.patchedItems);
		}
		if (StringUtils.isNotBlank(request.getNote())) {
			Note note = Note.builder().bookingId(amendment.getBookingId()).amendmentId(amendment.getAmendmentId())
					.noteType(NoteType.AMENDMENT).noteMessage(request.getNote()).userId(amendment.getAssignedUserId())
					.build();
			gmsCommunicator.addNote(note);
		}

		amendment.getAdditionalInfo().setProcessChecklistIds(request.getChecked());
		amendment.setStatus(AmendmentStatus.PROCESSING);
		service.save(amendment);
		amendment
				.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
		response.setAmendmentItems(Arrays.asList(amendment));
		response.setDifferentialPayment(differentialPayment);
	}

	@Override
	public void afterProcess() {}

}
