package com.tgs.services.oms.servicehandler.air;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.Amendments.AirAmendmentManager;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.Amendments.AmendmentMessagingClient;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.PaxKey;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAdditionalInfo;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;


@Slf4j
@Service
public class RaiseAirAmendmentHandler extends ServiceHandler<AirAmendmentPaxInfo, AmendmentResponse> {

	@Autowired
	protected OrderService orderService;

	@Autowired
	protected AmendmentService service;

	@Autowired
	protected AirAmendmentManager airAmendmentManager;

	@Autowired
	protected FMSCommunicator fmsComm;

	@Setter
	protected Amendment amendment;

	protected AirOrderInfo orderInfo;

	protected Order originalOrder;

	protected List<SegmentInfo> segmentInfoList;

	protected List<SegmentInfo> previousSegments;

	protected List<AirOrderItem> orgAirOrderItems;

	@Autowired
	protected GeneralCachingCommunicator cachingService;

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired


	private AmendmentMessagingClient amendmentMsgClient;

	@Autowired
	private AirOrderItemManager airOrderItemManager;

	@Override
	public void beforeProcess() throws Exception {

		originalOrder = orderService.findByBookingId(request.getBookingId()).toDomain();

		UserServiceHelper.checkAndReturnAllowedUserId(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
				Arrays.asList(originalOrder.getBookingUserId()));

		if (request.getType().equals(AmendmentType.VOIDED))
			canRaiseVoid();

		orderInfo = airAmendmentManager.getOrderInfoForAmendment(request.getBookingId(), request.getType());
		segmentInfoList = BookingUtils.getSegmentInfos(orderInfo.getTripInfoList());

		previousSegments = new ArrayList<SegmentInfo>();
		for (SegmentInfo segmentInfo : segmentInfoList) {
			previousSegments.add(new GsonMapper<>(segmentInfo, SegmentInfo.class).convert());
		}

		validateAmend();

		/**
		 * Commenting @validateSegmentGroups because there are cases when one segment is already flown but user has to
		 * request amendment for unflown segment 2. In case of GDS, onward and return pricing is combined from GDS
		 * response , However cancellation and rescheduling is possible journey wise. After understanding all cases we
		 * may enable this code again.
		 */
		validateSegmentGroups();
		createAmendment(null);

		filterPaxs(orderInfo);
		airAmendmentManager.checkPaxStatus(request.getBookingId(), orderInfo, true, amendment.getAmendmentType());
	}

	private void storeCancellationRequestInCache(Amendment amendment) {
		request.setAmendmentId(amendment.getAmendmentId());
		if (request.getType().equals(AmendmentType.CANCELLATION)) {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.CANCELLATION.getName(), GsonUtils.getGson().toJson(request));
			cachingService.store(CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREQUEST.name())
					.key(amendment.getAmendmentId()).build(), binMap, false, true, 3600);
		}
	}

	private void canRaiseVoid() {
		LocalDateTime orderSuccessTime = originalOrder.getCreatedOn();
		Map<String, LocalDateTime> firstTxnTime = originalOrder.getAdditionalInfo().getFirstUpdateTime();
		if (firstTxnTime != null && firstTxnTime.containsKey(OrderStatus.SUCCESS.getCode())) {
			orderSuccessTime = firstTxnTime.get(OrderStatus.SUCCESS.getCode());
		}
		if (orderSuccessTime.toLocalDate().isBefore(LocalDate.now()))
			throw new CustomGeneralException(SystemError.CANNOT_RAISE_VOID);
	}

	private void validateSegmentGroups() {
		Map<Short, Set<String>> groupMap = new HashMap<>();
		segmentInfoList.forEach(s -> {
			if (!groupMap.containsKey(s.getPricingGroupId()))
				groupMap.put(s.getPricingGroupId(), new HashSet<>());
			groupMap.get(s.getPricingGroupId()).add(s.getId());
		});

		Map<String, Set<PaxKey>> paxKeys = request.getPaxKeys();
		Set<String> validated = new HashSet<>();
		segmentInfoList.forEach(s -> {
			if (paxKeys.containsKey(s.getId()) && !validated.contains(s.getId())) {
				Set<PaxKey> orgPxns = paxKeys.get(s.getId());
				groupMap.get(s.getPricingGroupId()).forEach(segId -> {
					if (!paxKeys.containsKey(segId))
						throw new CustomGeneralException(SystemError.COMBINED_PRICING);
					Set<PaxKey> pxns = paxKeys.get(segId);
					if (!PaxKey.equal(pxns, orgPxns))
						throw new CustomGeneralException(SystemError.COMBINED_PRICING);
					validated.add(segId);
				});
				validated.add(s.getId());
			}
		});
	}

	@Override
	public void process() throws Exception {
		AirCancellationResponse cancellationReviewResponse = null;
		AirCancellationRequest cancellationRequest = null;
		try {
			cancellationRequest = AirCancellationRequest.builder().paxKeys(request.getPaxKeys())
					.segmentInfos(previousSegments).build();
			cancellationRequest.setBookingId(amendment.getBookingId());
			cancellationRequest.setAmendmentId(amendment.getAmendmentId());
			cancellationRequest.setAmendment(amendment);
			if (!request.getType().equals(AmendmentType.CANCELLATION)
					&& !request.getType().equals(AmendmentType.CANCELLATION_QUOTATION)) {
				service.save(amendment);
			} else {
				amendment.setStatus(AmendmentStatus.REQUESTED);
				cancellationReviewResponse = fmsComm.getCancellationFare(cancellationRequest, originalOrder);
				// Adding remarks only in case of cancellation quotation.
				if (AmendmentType.CANCELLATION_QUOTATION.equals(request.getType())) {
					addSystemRemarks(cancellationReviewResponse);
				}
				if (cancellationReviewResponse.isAutoCancellationAllowed()) {
					airAmendmentManager.processAmendment(amendment, cancellationReviewResponse, false, true);
				} else if (request.getType().equals(AmendmentType.CANCELLATION_QUOTATION)
						&& !cancellationReviewResponse.isAutoCancellationAllowed()) {
					service.save(amendment);
				}
			}
		} catch (Exception e) {
			log.error("AutoCancellation Failed for booking Id {} and exception is ", request.getBookingId(), e);
		} finally {
			storeAmendmentIdInCache(cancellationReviewResponse);
			storeCancellationRequestInCache(amendment);
			response.setAirCancellationReviewResponse(cancellationReviewResponse);
			response.getAmendmentItems().add(amendment);
			if (request.getType().equals(AmendmentType.CANCELLATION)
					|| request.getType().equals(AmendmentType.CANCELLATION_QUOTATION)) {
				cancellationRequest.setSegmentInfos(segmentInfoList);
				fmsComm.sendCancellationDataToAnalytics(cancellationRequest, originalOrder, cancellationReviewResponse,
						amendment.getAmount(), true);
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {
		amendmentMsgClient.sendSMS(amendment, SmsTemplateKey.AMENDMENT_REQUESTED_SMS);
		sendAmendmentRaisedMail();
	}

	private void sendAmendmentRaisedMail() {
		if (orgAirOrderItems == null)
			orgAirOrderItems = BaseModel.toDomainList(airOrderItemManager.findByBookingId(amendment.getBookingId()));
		orgAirOrderItems = AmendmentHelper.filterItemsNPax(amendment, orgAirOrderItems);
		amendmentMsgClient.sendMail(amendment, EmailTemplateKey.AMENDMENT_RAISED_EMAIL, orgAirOrderItems);
	}

	public void storeAmendmentIdInCache(AirCancellationResponse cancellationReviewResponse) {
		if (AmendmentType.CANCELLATION.equals(request.getType())) {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.CANCELLATION.getName(), GsonUtils.getGson().toJson(cancellationReviewResponse));
			cachingService.store(CacheMetaInfo.builder().set(CacheSetName.CANCELLATIONREVIEWRESPONSE.name())
					.key(amendment.getAmendmentId()).build(), binMap, false, true, 3600);
		}
	}

	private void validateAmend() {
		if (CollectionUtils.isEmpty(request.getPaxKeys()))
			throw new CustomGeneralException(SystemError.NO_PAX_SELECTED);

		if (originalOrder == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);

		airAmendmentManager.checkPendingOrder(request.getType(),
				BookingUtils.getSegmentInfos(orderInfo.getTripInfoList()));
	}

	protected void filterPaxs(AirOrderInfo orderInfo) {

		if (amendment.getAirAdditionalInfo().getKeyMap().isEmpty())
			throw new CustomGeneralException(SystemError.NO_PAX_SELECTED);
		amendment.getAirAdditionalInfo().setAirlineCodes(new HashSet<>());
		orderInfo.getTripInfoList().forEach(t -> t.getSegmentInfos().forEach(s -> {
			if (!CollectionUtils.isEmpty(s.getPriceInfoList())) {
				amendment.getAirAdditionalInfo().getSourceIds().add(s.getPriceInfo(0).getSourceId());
				amendment.getAirAdditionalInfo().getSuppliers().add(s.getPriceInfo(0).getSupplierId());
			}

			amendment.getAirAdditionalInfo().getAirlineCodes().add(s.getAirlineCode(false));
			s.getBookingRelatedInfo().getTravellerInfo().removeIf(
					p -> !amendment.getAirAdditionalInfo().amdContainsPax(Long.valueOf(s.getId()), p.getId()));
		}));
	}

	protected void createAmendment(String amendmentId) {
		User bookingUser = userService.getUserFromCache(originalOrder.getBookingUserId());
		String amedRefId = StringUtils.isBlank(amendmentId)
				? ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AMENDMENT).build())
				: amendmentId;
		amendment = Amendment.builder().amendmentId(amedRefId)
				.loggedInUserId(SystemContextHolder.getContextData().getUser().getUserId())
				.bookingId(originalOrder.getBookingId()).bookingUserId(originalOrder.getBookingUserId())
				.partnerId(bookingUser.getPartnerId()).status(AmendmentStatus.REQUESTED)
				.amendmentType(request.getType()).orderType(OrderType.AIR)
				.channelType(SystemContextHolder.getChannelType()).build();
		setAdditionalInfo();
	}

	private void setAdditionalInfo() {
		LocalDateTime firstDeparture = getFirstDepartureTime();
		AmendmentAdditionalInfo additionalInfo = new AmendmentAdditionalInfo();
		additionalInfo.setAgentRemarks(request.getRemarks());
		additionalInfo.getAirAdditionalInfo().setPaxKeys(request.getPaxKeys());
		additionalInfo.setRaiseChecklistIds(request.getChecklistIds());
		additionalInfo.getAirAdditionalInfo().setFirstDeparture(firstDeparture);
		if (request.getNextTravelDate() != null)
			additionalInfo.getAirAdditionalInfo().setNextTravelDate(request.getNextTravelDate().toLocalDate());
		additionalInfo.getAirAdditionalInfo()
				.setClosestActionDate(getClosestActionDate(firstDeparture, request.getNextTravelDate()));
		additionalInfo.getAirAdditionalInfo().setInternational(orderInfo.getSearchQuery().isIntl());
		for (Map.Entry<String, Set<PaxKey>> entry : additionalInfo.getAirAdditionalInfo().getPaxKeys().entrySet()) {
			for (SegmentInfo segmentInfo : segmentInfoList) {
				if (entry.getKey().equals(segmentInfo.getId())) {
					for (PaxKey paxKey : entry.getValue()) {
						for (FlightTravellerInfo travellerInfo : segmentInfo.getTravellerInfo()) {
							if (paxKey.getId().equals(travellerInfo.getId())) {
								paxKey.setAirlinePnr(travellerInfo.getPnr());
								paxKey.setPassengerName(travellerInfo.getFullName());
								paxKey.setTicketNumber(travellerInfo.getTicketNumber());
							}
						}
					}
				}
			}
		}
		amendment.setAdditionalInfo(additionalInfo);
	}

	private LocalDateTime getClosestActionDate(LocalDateTime departureDate, LocalDateTime nextTravelDate) {
		if (nextTravelDate != null && nextTravelDate.isBefore(departureDate)) {
			return nextTravelDate;
		}
		return departureDate;
	}

	private LocalDateTime getFirstDepartureTime() {
		List<SegmentInfo> modSegmentInfos = segmentInfoList.stream()
				.filter(s -> request.getPaxKeys().containsKey(s.getId()))
				.sorted((s1, s2) -> s1.getDepartTime().compareTo(s2.getDepartTime())).collect(Collectors.toList());
		return modSegmentInfos.get(0).getDepartTime();
	}

	protected void addSystemRemarks(AirCancellationResponse cancellationResponse) {
		if (cancellationResponse != null
				&& MapUtils.isNotEmpty(cancellationResponse.getCancellationDetail().getNotAllowedSegments())) {
			if (amendment.getAdditionalInfo() == null) {
				amendment.setAdditionalInfo(new AmendmentAdditionalInfo());
			}
			String remarks = amendment.getAdditionalInfo().getSystemRemarks();
			Map<String, MessageInfo> errorMsgs =
					cancellationResponse.getCancellationDetail().getCancellationNotAllowedSegments();
			amendment.getAdditionalInfo().setSystemRemarks(StringUtils.join(remarks,
					errorMsgs.values().stream().map(MessageInfo::getMessage).collect(Collectors.joining(","))));
		}
	}
}
