package com.tgs.services.oms.servicehandler.air.amendment;

import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.Amendments.AmendmentChargesManager;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentChargesResponse;
import com.tgs.services.oms.restmodel.air.AmendmentRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Slf4j
public abstract class AbstractAmendmentHandler {

	private AirOrderInfo airOrderInfo;

	@Autowired
	private AmendmentChargesManager chargesManager;

	@Autowired
	protected FMSCommunicator fmsCommunicator;

	@Autowired
	private UserServiceCommunicator umsCommunicator;

	@Autowired
	private AirOrderItemManager itemManager;

	private AmendmentRequest amendmentRequest;

	private AmendmentResponse amendmentResponse;

	protected User bookingUser;

	protected List<DbAirOrderItem> orgAirOrderItems;

	public void initHandler() {
		Order order = itemManager.getOrderByBookingId(airOrderInfo.getBookingId());
		bookingUser = umsCommunicator.getUserFromCache(order.getBookingUserId());
		orgAirOrderItems = itemManager.findByBooking(getAirOrderInfo().getBookingId());
	}

	public abstract void preSetDetails();

	public AirAmendmentPaxInfo buildInternalAmendmentRequest(AirOrderInfo airOrderInfo) {
		AirAmendmentPaxInfo paxInfo = new AirAmendmentPaxInfo();
		paxInfo.setType(amendmentRequest.getType());
		paxInfo.setBookingId(amendmentRequest.getBookingId());
		paxInfo.setPaxKeys(chargesManager.getKeys(amendmentRequest.getTrips(), airOrderInfo));
		paxInfo.setRemarks(amendmentRequest.getRemarks());
		return paxInfo;
	}

	public AmendmentChargesResponse buildChargesResponse() {
		AmendmentChargesResponse chargesResponse = AmendmentChargesResponse.builder().build();
		chargesResponse.setBookingId(airOrderInfo.getBookingId());
		return chargesResponse;
	}

	public void additionalSubmitAmendment() {

	}

	public void validateRaiseAmendmentResponse() {
		if (StringUtils.isNotBlank(amendmentResponse.getErrorMessage())) {
			throw new CustomGeneralException(SystemError.AMENDMENT_NA_SUPPORTED, amendmentResponse.getErrorMessage());
		}
	}
}
