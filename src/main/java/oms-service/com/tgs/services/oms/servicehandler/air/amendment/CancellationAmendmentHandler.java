package com.tgs.services.oms.servicehandler.air.amendment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.cancel.JourneyCancellationDetail;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.oms.datamodel.amendments.AmendmentTrip;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.air.AirAmendmentPaxInfo;
import com.tgs.services.oms.restmodel.air.AirOrderInfo;
import com.tgs.services.oms.restmodel.air.AmendmentChargesResponse;
import com.tgs.services.oms.restmodel.air.AmendmentTripCharges;
import com.tgs.services.oms.restmodel.air.PaxAmendmentCharges;
import com.tgs.services.oms.servicehandler.air.AirAmendmentCancellationHandler;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.air.AirBookingUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CancellationAmendmentHandler extends AbstractAmendmentHandler {

	@Override
	public void preSetDetails() {
		if (CollectionUtils.isEmpty(getAmendmentRequest().getTrips())) {
			for (TripInfo tripInfo : getAirOrderInfo().getTripInfoList()) {
				AmendmentTrip amendmentTrip = AmendmentTrip.builder().source(tripInfo.getDepartureAirportCode())
						.destination(tripInfo.getArrivalAirportCode())
						.departureDate(tripInfo.getDepartureTime().toLocalDate()).build();
				getAmendmentRequest().getTrips().add(amendmentTrip);
			}
		}
	}

	@Override
	public AmendmentChargesResponse buildChargesResponse() {
		AmendmentChargesResponse chargesResponse = super.buildChargesResponse();
		List<AmendmentTripCharges> amendmentTripCharges = new ArrayList<>();
		if (getAmendmentResponse().getAirCancellationReviewResponse() != null
				&& getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail() != null
				&& getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail()
						.getAutoCancellationAllowed()
				&& getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail()
						.getJourneyCancellationDetails() != null) {
			Map<String, JourneyCancellationDetail> cancellationDetailMap = getAmendmentResponse()
					.getAirCancellationReviewResponse().getCancellationDetail().getJourneyCancellationDetails();
			for (String tripKey : cancellationDetailMap.keySet()) {
				JourneyCancellationDetail cancellationDetail = cancellationDetailMap.get(tripKey);
				AmendmentTripCharges tripCharges = AmendmentTripCharges.builder().build();
				TripInfo tripInfo = getTripInfo(cancellationDetail, getAirOrderInfo());
				if (tripInfo != null) {
					buildAmedmentTrip(tripCharges, tripInfo);
					tripCharges.setAmendmentInfo(buildFareDetail(cancellationDetail, tripInfo));
					amendmentTripCharges.add(tripCharges);
				}
			}
			chargesResponse.setTrips(amendmentTripCharges);
		} else if (getFmsCommunicator().isGeneralAndSourceApplicable(getAirOrderInfo().getTripInfoList(),
				getBookingUser())) {
			for (TripInfo trip : getAirOrderInfo().getTripInfoList()) {
				getFmsCommunicator().updateCancellationAndRescheduleFees(trip, getBookingUser());
				AmendmentTripCharges tripCharges = AmendmentTripCharges.builder().build();
				buildAmedmentTrip(tripCharges, trip);
				tripCharges.setAmendmentInfo(buildChargesFromFareRule(tripCharges, trip));
				amendmentTripCharges.add(tripCharges);
				chargesResponse.setTrips(amendmentTripCharges);
			}
		} else if (MapUtils.isNotEmpty(getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail()
				.getCancellationNotAllowedSegments())) {
			String message = getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail()
					.getCancellationNotAllowedSegments().values().stream().map(MessageInfo::getMessage)
					.collect(Collectors.joining(","));
			throw new CustomGeneralException(SystemError.AMENDMENT_NA_SUPPORTED,
					SystemError.AMENDMENT_NA_SUPPORTED.getMessage(message));
		} else {
			throw new CustomGeneralException(SystemError.CONTACT_SUPPORT);
		}
		return chargesResponse;
	}

	private void buildAmedmentTrip(AmendmentTripCharges tripCharges, TripInfo tripInfo) {
		tripCharges.setSource(tripInfo.getDepartureAirportCode());
		tripCharges.setDestination(tripInfo.getArrivalAirportCode());
		tripCharges.setDepartureDate(tripInfo.getDepartureTime());
		tripCharges.setFlightNumbers(tripInfo.getFlightNumberSet());
		tripCharges.setAirlines(tripInfo.getAirlineInfo(false));
	}

	@Override
	public void additionalSubmitAmendment() {
		// this will submit cancellation
		AirAmendmentCancellationHandler cancellationHandler =
				SpringContext.getApplicationContext().getBean(AirAmendmentCancellationHandler.class);
		AirAmendmentPaxInfo amendmentPaxInfo = new AirAmendmentPaxInfo();
		amendmentPaxInfo.setAmendmentId(getAmendmentResponse().getAmendmentItems().get(0).getAmendmentId());
		amendmentPaxInfo.setBookingId(getAmendmentResponse().getAmendmentItems().get(0).getBookingId());
		amendmentPaxInfo.setType(getAmendmentResponse().getAmendmentItems().get(0).getAmendmentType());
		amendmentPaxInfo.setRemarks(getAmendmentRequest().getRemarks());
		AmendmentResponse amendmentResponse = AmendmentResponse.builder().build();
		amendmentResponse.setAmendmentItems(new ArrayList<>());
		cancellationHandler.initData(amendmentPaxInfo, amendmentResponse);
		try {
			amendmentResponse = cancellationHandler.getResponse();
		} catch (Exception e) {
			log.error("Unable to process cancellation for {}", getAmendmentRequest().getBookingId(), e);
		}
	}

	private Map<PaxType, PaxAmendmentCharges> buildFareDetail(JourneyCancellationDetail cancellationDetail,
			TripInfo tripInfo) {
		Map<PaxType, PaxAmendmentCharges> paxAmendmentCharges = new HashMap<>();
		for (PaxType paxType : cancellationDetail.getFareDetails().keySet()) {
			boolean isSSRefundable = fmsCommunicator.isSSRRefundable(tripInfo, bookingUser);
			FareDetail fareDetail = cancellationDetail.getFareDetails().get(paxType);
			Set<FareComponent> refundableComponents = FareComponent.getRefundableComponents(isSSRefundable);
			boolean isPassThru = BooleanUtils.isTrue(orgAirOrderItems.get(0).getAdditionalInfo().getIsUserCreditCard());
			Set<FareComponent> paxCancelComponents = AirBookingUtils.getPaxCancelComponents(isPassThru);
			PaxAmendmentCharges charges = PaxAmendmentCharges.builder().build();
			charges.setTotalFare(fareDetail.getFareComponents().getOrDefault(FareComponent.TF, 0d));
			charges.setAmendmentCharges(fareDetail.getComponentsSum(FareComponent.getCancellationComponents()));
			charges.setRefundAmount(fareDetail.getComponentsSum(refundableComponents)
					- fareDetail.getComponentsSum(paxCancelComponents));

			paxAmendmentCharges.put(paxType, charges);
		}
		return paxAmendmentCharges;
	}

	public static TripInfo getTripInfo(JourneyCancellationDetail cancellationDetail, AirOrderInfo airOrderInfo) {
		List<TripInfo> tripInfos = airOrderInfo.getTripInfoList();
		String tripKey = StringUtils.join(cancellationDetail.getDepartureAirportCode(),
				cancellationDetail.getArrivalAirportCode(), cancellationDetail.getDepartureDate());
		for (TripInfo tripInfo : tripInfos) {
			String key = StringUtils.join(tripInfo.getDepartureAirportCode(), tripInfo.getArrivalAirportCode(),
					tripInfo.getDepartureTime());
			if (StringUtils.equalsIgnoreCase(key, tripKey)) {
				return tripInfo;
			}
		}
		return null;
	}

	@Override
	public void validateRaiseAmendmentResponse() {
		super.validateRaiseAmendmentResponse();
		/*
		 * if (MapUtils.isNotEmpty(getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail()
		 * .getCancellationNotAllowedSegments())) { String message =
		 * getAmendmentResponse().getAirCancellationReviewResponse().getCancellationDetail()
		 * .getCancellationNotAllowedSegments().values().stream().map(MessageInfo::getMessage)
		 * .collect(Collectors.joining(",")); throw new CustomGeneralException(SystemError.AMENDMENT_NA_SUPPORTED,
		 * SystemError.AMENDMENT_NA_SUPPORTED.getMessage(message)); }
		 */
	}

	private Map<PaxType, PaxAmendmentCharges> buildChargesFromFareRule(AmendmentTripCharges tripCharges,
			TripInfo tripInfo) {
		Map<PaxType, PaxAmendmentCharges> paxAmendmentCharges = new HashMap<>();
		SegmentInfo segmentInfo = tripInfo.getSegmentInfos().get(0);
		for (PriceInfo price : segmentInfo.getPriceInfoList()) {
			for (PaxType paxType : price.getFareDetails().keySet()) {
				PaxAmendmentCharges charges = PaxAmendmentCharges.builder().build();
				FareDetail fd = price.getFareDetail(paxType);
				charges.setTotalFare(fd.getFareComponents().getOrDefault(FareComponent.TF, 0d));
				charges.setAmendmentCharges(fd.getComponentsSum(FareComponent.getCancellationComponents()));
				charges.setRefundAmount(charges.getTotalFare() - charges.getAmendmentCharges());
				paxAmendmentCharges.put(paxType, charges);
			}
		}
		return paxAmendmentCharges;
	}
}

