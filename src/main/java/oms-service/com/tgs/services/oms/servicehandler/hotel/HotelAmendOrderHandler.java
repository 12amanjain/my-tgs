package com.tgs.services.oms.servicehandler.hotel;

import java.util.List;

import com.tgs.services.oms.BookingResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.hotel.HotelPostBookingManager;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j

public class HotelAmendOrderHandler extends ServiceHandler<HotelPostBookingBaseRequest, BookingResponse> {

	@Autowired
	HotelPostBookingManager manualBookingManager;
	
	@Autowired
	HotelModifyBookingManager modifyBookingManager;
	
	@Autowired
	private HotelOrderItemManager itemManager;
	
	
	@Override
	public void beforeProcess() throws Exception {
		
		if(StringUtils.isEmpty(request.getBookingId()))
			throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
		manualBookingManager.updateNewHotelInfoInRequestForModifyOrder(request);
		
	}

	@Override
	public void process() throws Exception {
		
		HotelOrder hotelOrder = modifyBookingManager.getHotelOrderFromUpdatedHotelInfo(request);
		List<HotelOrderItem> items = hotelOrder.getItems();
		List<DbHotelOrderItem> dbItems = new DbHotelOrderItem().toDbList(items);
		itemManager.save(dbItems, hotelOrder.getOrder(), null);
		
	}
	
	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

}
