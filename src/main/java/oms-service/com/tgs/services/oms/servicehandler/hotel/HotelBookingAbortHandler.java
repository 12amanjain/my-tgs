package com.tgs.services.oms.servicehandler.hotel;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.hotel.HotelBookingUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import com.tgs.services.ums.datamodel.AreaRole;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBookingAbortHandler {

	@Autowired
	HotelOrderItemService itemService;

	@Autowired
	OrderManager orderManager;

	@Autowired
	private PaymentServiceCommunicator paymentServiceCommunicator;

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	HMSCommunicator hmsComm;

	@Autowired
	UserServiceCommunicator userService;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public void abortBooking(String bookingId) {

		log.info("Aborting booking for bookingId {}", bookingId);
		Order order = orderManager.findByBookingId(bookingId, null);
		if (order == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		OrderUtils.isAllowed(AreaRole.CART_HOTEL_BOOKING_ABORT);
		List<DbHotelOrderItem> orderItems = itemService.findByBookingIdOrderByIdAsc(bookingId);
		HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(orderItems).build().convert();
		HotelBookingUtils.canAbort(hInfo, order);
		/*
		 * As discussed , we won't hit supplier to cancel booking in case of abort .
		 */
		order.setPayments(paymentServiceCommunicator.fetchPaymentByBookingId(order.getBookingId()).stream()
				.filter(x -> x.getStatus().equals(PaymentStatus.SUCCESS)).collect(Collectors.toList()));
		if (CollectionUtils.isNotEmpty(order.getPayments())) {
			boolean isPaymentReversed = reversePayments(order);
			if (isPaymentReversed) {
				if (order.getAdditionalInfo() == null) {
					order.setAdditionalInfo(OrderAdditionalInfo.builder().build());
				}
				order.getAdditionalInfo().setPaymentStatus(PaymentStatus.REFUND_SUCCESS);
			}
		}
		itemManager.updateOrderItem(hInfo, order, HotelItemStatus.ABORTED);
	}

	private boolean reversePayments(Order order) {
		Double amountToReverse = 0.0;
		boolean isPaymentReversed = false;
		for (Payment payment : order.getPayments()) {
			if (payment.getType().equals(PaymentTransactionType.PAID_FOR_ORDER)) {
				amountToReverse += payment.getAmount().doubleValue();
			}
		}
		if (OrderUtils.isPaymentAlreadyDone(order, PaymentTransactionType.REVERSE, new BigDecimal(amountToReverse)))
			return true;
		else {
			List<PaymentRequest> paymentRequests = createReversePayments(order);
			List<PaymentRequest> clubbedPayments = BasePaymentUtils.club(paymentRequests);
			List<Payment> reversePayments = paymentServiceCommunicator.doPaymentsUsingPaymentRequests(clubbedPayments);
			order.getPayments().addAll(reversePayments);
			if (CollectionUtils.isNotEmpty(reversePayments)) {
				isPaymentReversed = true;
			}
		}
		return isPaymentReversed;
	}

	private List<PaymentRequest> createReversePayments(Order order) {
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		for (Payment p : order.getPayments()) {
			if (p.getAmount().compareTo(BigDecimal.ZERO) != 0) {
				if (p.getOpType().equals(PaymentOpType.CREDIT)) {
					PaymentTransactionType txnType =
							BasePaymentUtils.getReverseTransactionType(PaymentOpType.DEBIT, p.getType());
					paymentRequestList.add(HotelBookingUtils.createAPHPayments(p, txnType, p.getAmount()));
					continue;
				}
				PaymentFact fact = PaymentFact.builder().medium(p.getPaymentMedium()).build();
				List<PaymentConfigurationRule> rules = paymentServiceCommunicator.getApplicableRulesOutput(fact,
						PaymentRuleType.REFUND, p.getPaymentMedium());
				PaymentMedium reverseMedium = PaymentMedium.FUND_HANDLER;
				if (!CollectionUtils.isEmpty(rules)) {
					reverseMedium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
				}
				if (reverseMedium.equals(PaymentMedium.FUND_HANDLER)) {
					paymentRequestList.add(HotelBookingUtils.createFundHandlerRequests(p,
							PaymentTransactionType.REVERSE, p.getAmount()));
				} else {
					p.setPaymentMedium(reverseMedium);
					paymentRequestList.add(
							HotelBookingUtils.createRefundPayments(p, PaymentTransactionType.REVERSE, p.getAmount()));
				}
			}
		}
		return paymentRequestList;
	}

}
