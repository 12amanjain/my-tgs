package com.tgs.services.oms.servicehandler.hotel;

import com.tgs.services.oms.BookingResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelConfirmBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HotelBookingConfirmationHandler extends ServiceHandler<BookingRequest, BookingResponse> {

	@Autowired
	@Qualifier("HotelConfirmBookingManager")
	private HotelConfirmBookingManager bookingManager;

	@Autowired
	HMSCommunicator hmsComm;
	
	@Autowired
	private OrderManager orderManager;

	@Override
	public void beforeProcess() throws Exception {
		try {
			bookingManager.setBookingRequest(request);
			bookingManager.setBookingUser(SystemContextHolder.getContextData().getUser());
			bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
			Order order = bookingManager.processBooking();
			order = orderManager.findByBookingId(order.getBookingId(), order);
			
			if(order.getAdditionalInfo().getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
				
				HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(bookingManager.getItems()).build()
						.convert();
				hmsComm.callSupplierConfirmBook(hInfo, order.getBookingId());
			}
		} catch (DataIntegrityViolationException e) {
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER);
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to process order for booking Id {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.INVALID_REQUEST);
		}
	}

	@Override
	public void process() throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
