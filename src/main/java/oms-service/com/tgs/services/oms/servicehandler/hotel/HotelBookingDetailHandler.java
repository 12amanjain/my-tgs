package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderDetails;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.mapper.hotel.HotelOrderItemToHotelSearchQueryMapper;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.ums.datamodel.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBookingDetailHandler extends ServiceHandler<BookingDetailRequest, BookingDetailResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private HotelOrderActionValidator actionValidator;

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private OrderManager orderManger;

	private static final int MAX_RETRIES = 5;
	private boolean isApiPartnerFlow;
	private int currentRetry = 0;

	@Override
	public void beforeProcess() throws Exception {
		isApiPartnerFlow = checkIfApiPartnerFlow();
	}

	@Override
	public void process() throws Exception {
		try {
			OrderFilter filter = OrderFilter.builder().bookingIds(Arrays.asList(request.getBookingId())).build();

			if (SystemContextHolder.getContextData().getUser() != null
					&& UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
				User user = userComm.getUser(
						UserFilter.builder().email(request.getEmail()).roles(Arrays.asList(UserRole.CUSTOMER)).build());
				log.info("User found with id {} ", user.getUserId());
				filter.setBookingUserIds(Arrays.asList(user.getUserId()));
			}

			Order order = new GsonMapper<>(orderService.findAll(filter).get(0), Order.class).convert();
			if (SystemContextHolder.getContextData().getUser() != null
					&& !UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
				UserServiceHelper.checkAndReturnAllowedUserId(
						SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
						Arrays.asList(order.getBookingUserId()));
				order.setActionList(
						actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order));

			}
			User bookingUser = userComm.getUserFromCache(order.getBookingUserId());
			response.setBookingUser(User.builder().userId(bookingUser.getUserId()).build());
			response.setOrder(order);
			List<DbHotelOrderItem> dbOrderItems = itemService.findByBookingIdOrderByIdAsc(request.getBookingId());
			HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(dbOrderItems).build().convert();
			HotelSearchQuery query =
					HotelOrderItemToHotelSearchQueryMapper.builder().items(dbOrderItems).build().convert();
			HotelOrderDetails orderDetails = HotelOrderDetails.builder().hInfo(hInfo).query(query)

					.build();
			Map<String, ItemDetails> map = new HashMap<>();
			map.put(OrderType.HOTEL.getName(), orderDetails);
			response.setItemInfos(map);
			response.setGstInfo(orderManger.getGstInfo(request.getBookingId()));
			checkForOrderStatus();

		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to find any order corresponding to bookingId {}", request.getBookingId(), e);
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		}
	}

	private void checkForOrderStatus() throws InterruptedException {

		Order order = new GsonMapper<>(orderService.findByBookingId(request.getBookingId()), Order.class).convert();

		if (!(order.getStatus().equals(OrderStatus.SUCCESS) || order.getStatus().equals(OrderStatus.ON_HOLD)
				|| order.getStatus().equals(OrderStatus.FAILED) || order.getStatus().equals(OrderStatus.PENDING)
				|| OrderStatus.ABORTED.equals(order.getStatus()))
				&& LocalDateTime.now().minusMinutes(5).isBefore(order.getCreatedOn())) {
			response.setRetryInSecond(10);
		}

		if (isApiPartnerFlow && Objects.nonNull(response.getRetryInSecond()) && (currentRetry < MAX_RETRIES)) {
			Thread.sleep(response.getRetryInSecond() * 1000);
			currentRetry++;
			response.setRetryInSecond(null);
			checkForOrderStatus();
		}

	}

	private boolean checkIfApiPartnerFlow() {
		User user = SystemContextHolder.getContextData().getUser();
		return UserUtils.isApiUserRequest(user);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

}
