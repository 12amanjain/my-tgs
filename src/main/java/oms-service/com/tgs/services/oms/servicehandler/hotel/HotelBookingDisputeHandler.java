package com.tgs.services.oms.servicehandler.hotel;

import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderDisputeInfo;
import com.tgs.services.oms.datamodel.OrderDisputeStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.hotel.HotelBookingDisputeManager;
import com.tgs.services.oms.restmodel.hotel.HotelBookingDisputeRequest;

@Service
public class HotelBookingDisputeHandler extends ServiceHandler<HotelBookingDisputeRequest, BaseResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private HotelBookingDisputeManager disputeManager;

	@Override
	public void beforeProcess() throws Exception {
		Order order = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (Objects.isNull(order)) {

			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}

		List<DbHotelOrderItem> dbOrderItems = itemService.findByBookingIdOrderByIdAsc(request.getBookingId());
		isDisputeActionValid(order);
		disputeManager.setOrder(order);
		disputeManager.setOrderItems(dbOrderItems);
		disputeManager.setDisputeRequest(request);
	}

	@Override
	public void process() throws Exception {

		switch (request.getDisputeInfo().getDisputeStatus()) {

			case IN_DISPUTE:
				disputeManager.raiseDispute();
				break;

			case RESOLVED:
				disputeManager.resolveDispute();
				break;

			default:
				throw new CustomGeneralException(SystemError.DISPUTE_INVALID_ACTION);
		}
	}

	private void isDisputeActionValid(Order order) {

		switch (order.getStatus()) {
			case SUCCESS:
				OrderDisputeStatus disputeStatus = request.getDisputeInfo().getDisputeStatus();
				List<OrderDisputeInfo> disputeInfos = order.getAdditionalInfo().getDisputeInfo();
				if (!CollectionUtils.isEmpty(disputeInfos)) {

					OrderDisputeInfo disputeInfo = disputeInfos.get(disputeInfos.size() - 1);
					if (disputeInfo.getDisputeStatus().equals(disputeStatus)) {
						throw new CustomGeneralException(SystemError.DISPUTE_NOT_ALLOWED);
					}
				}
				break;
			default:
				throw new CustomGeneralException(SystemError.DISPUTE_NOT_ALLOWED);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
