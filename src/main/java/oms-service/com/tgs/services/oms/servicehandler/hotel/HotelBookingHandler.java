package com.tgs.services.oms.servicehandler.hotel;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.hms.analytics.HotelBookingAnalyticsInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.BookingResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAdditionalInfo;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.hotel.HotelOrderItemToHotelSearchQueryMapper;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.restmodel.RegisterRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBookingHandler extends ServiceHandler<HotelBookingRequest, BookingResponse> {

	@Autowired
	HotelBookingManager bookingManager;

	@Autowired
	HMSCommunicator hmsComm;

	@Autowired
	OrderManager orderManager;

	@Autowired
	HotelOrderItemService itemService;

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	private PaymentServiceCommunicator paymentServiceComm;


	@Autowired
	private UserServiceCommunicator userService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		Order order = null;
		HotelInfo hInfo = null;
		HotelSearchQuery searchQuery = null;
		try {
			registerGuestUser();
			SystemContextHolder.getContextData().getReqIds().add(request.getBookingId());
			response.setBookingId(request.getBookingId());
			HotelReviewResponse reviewResponse = hmsComm.getHotelReviewResponse(request.getBookingId());
			hInfo = reviewResponse.getHInfo();
			searchQuery = reviewResponse.getQuery();
			updateChildAgeInRoom(searchQuery, request.getRoomTravellerInfo());

			bookingManager.setBookingUser(SystemContextHolder.getContextData().getUser());
			if (BooleanUtils.isNotFalse(request.getCheckDuplicateBooking())) {
				List<OrderStatus> failureStatusList = Arrays.asList(OrderStatus.ABORTED, OrderStatus.FAILED,
						OrderStatus.CANCELLED, OrderStatus.UNCONFIRMED);
				bookingManager.checkDuplicateBooking(hInfo, failureStatusList, request.getBookingId(), contextData,
						null, request.getRoomTravellerInfo());
			}
			removeExistingFailedBooking();
			bookingManager.setBookingRequest(request);
			bookingManager.setHInfo(hInfo);
			request.setFlowType(OrderFlowType.ONLINE);
			bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
			if (CollectionUtils.isNotEmpty(request.getPaymentInfos())) {
				request.getPaymentInfos().forEach(p -> p.setProduct(Product.HOTEL));
			}
			order = bookingManager.processBooking();
			order = orderManager.findByBookingId(order.getBookingId(), order);
			if (CollectionUtils.isEmpty(request.getPaymentInfos()) || (order.getAdditionalInfo() != null
					&& order.getAdditionalInfo().getPaymentStatus() == PaymentStatus.SUCCESS)) {
				hmsComm.callSupplierBook(hInfo, order.getBookingId());
			}
		} catch (DataIntegrityViolationException e) {
			String errorMsg = StringUtils.join("Unable to process order for booking Id ", request.getBookingId(),
					" due to", SystemError.DUPLICATE_ORDER);
			contextData.getErrorMessages().add("Unable to process order due to duplicate booking");
			log.error(errorMsg, e);
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER);
		} catch (CustomGeneralException e) {
			String errorMsg = StringUtils.join("Unable to process order for booking Id ", request.getBookingId(),
					" due to", e.getMessage());
			contextData.getErrorMessages().add("Unable to process booking");
			log.info(errorMsg);
			throw e;
		} catch (Exception e) {
			String errorMsg = StringUtils.join("Unable to process order for booking Id ", request.getBookingId(),
					" due to", SystemError.SERVICE_EXCEPTION);
			contextData.getErrorMessages().add("Unable to process booking due to service exception");
			log.error(errorMsg, e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		} finally {
			sendBookingDataToAnalytics(hInfo, order, searchQuery);
		}
	}

	private void sendBookingDataToAnalytics(HotelInfo hInfo, Order order, HotelSearchQuery searchQuery) {
		try {
			contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
					Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
							.time(System.currentTimeMillis()).build()));
			String orderStatus = null;
			HotelBookingAnalyticsInfo bookingAnalyticsInfo =
					HotelBookingAnalyticsInfo.builder().hInfo(hInfo).bookingId(request.getBookingId()).build();
			if (ObjectUtils.isEmpty(order)) {
				order = orderManager.findByBookingId(request.getBookingId(), order);
			}

			if (order != null) {
				searchQuery = searchQuery != null ? searchQuery
						: HotelOrderItemToHotelSearchQueryMapper.builder().items(bookingManager.getItems()).build()
								.convert();
				bookingAnalyticsInfo.setSearchQuery(searchQuery);
				orderStatus = order.getStatus().getCode();
				OrderAdditionalInfo additionalInfo = order.getAdditionalInfo();
				if (!ObjectUtils.isEmpty(request.getPaymentInfos())) {
					bookingAnalyticsInfo.setPaymentMedium(BaseUtils.getPaymentMediums(request.getPaymentInfos()));
				}
				bookingAnalyticsInfo.setPaymentStatus(
						additionalInfo.getPaymentStatus() != null ? additionalInfo.getPaymentStatus().name() : null);
				bookingAnalyticsInfo.setFlowType(additionalInfo.getFlowType().getName());
				bookingAnalyticsInfo.setOrderStatus(orderStatus);
				hmsComm.addBookingToAnalytics(bookingAnalyticsInfo);
			}
		} catch (Exception e) {
			log.error("Unable to push booking analytics info for booking id {}", request.getBookingId(), e);
		}
	}

	private void removeExistingFailedBooking() {
		orderManager.deleteExistingFailedAndNewBooking(request.getBookingId());
		itemService.deleteItemsWithNoOrderByBookingId(request.getBookingId());
		paymentServiceComm.deleteFailedPaymentsByRefId(request.getBookingId());
	}

	private void updateChildAgeInRoom(HotelSearchQuery searchQuery, List<RoomTravellerInfo> roomTravellerInfo) {
		int i = 0;
		for (RoomTravellerInfo roomTravellers : roomTravellerInfo) {
			RoomSearchInfo roomSearchInfo = searchQuery.getRoomInfo().get(i);
			if (!CollectionUtils.isEmpty(roomSearchInfo.getChildAge())) {
				int childIndex = 0;
				for (TravellerInfo traveller : roomTravellers.getTravellerInfo()) {
					if (traveller.getPaxType().equals(PaxType.CHILD)) {
						traveller.setAge(roomSearchInfo.getChildAge().get(childIndex));
						childIndex++;
					}
				}
			}
			i++;
		}
	}

	/**
	 * If UserRole is Guest, and he/she is trying to do booking, we will register the user <br>
	 * in background with email and phone provided in the deliveryinfo. New UserRole will be Customer now
	 */

	private void registerGuestUser() throws Exception {
		if (UserRole.GUEST.equals(SystemContextHolder.getContextData().getUser().getRole())) {
			try {
				String email = request.getDeliveryInfo().getEmails().get(0);
				String phone = request.getDeliveryInfo().getContacts().get(0);
				User user = org.apache.commons.lang3.ObjectUtils.firstNonNull(
						userService.getUser(
								UserFilter.builder().email(email).roles(Arrays.asList(UserRole.CUSTOMER)).build()),
						userService.getUser(
								UserFilter.builder().mobile(phone).roles(Arrays.asList(UserRole.CUSTOMER)).build()));
				if (user == null) {
					String password = RandomStringUtils.randomAlphanumeric(8);
					user = userService.registerUser(RegisterRequest.builder().email(email).mobile(phone).phone(phone)
							.name(request.getRoomTravellerInfo().get(0).getTravellerInfo().get(0).getFirstName() + " "
									+ request.getRoomTravellerInfo().get(0).getTravellerInfo().get(0).getLastName())
							.role(UserRole.CUSTOMER).password(password).build());
				}
				SystemContextHolder.getContextData().setUser(user);
			} catch (Exception e) {
				log.error("Error occured during user registration with email {} and mobile {} due to {}",
						request.getDeliveryInfo().getEmails().get(0), request.getDeliveryInfo().getContacts().get(0),
						e.getMessage(), e);
				throw new CustomGeneralException(SystemError.USER_EXISTS);
			}
		}
	}

	@Override
	public void afterProcess() throws Exception {}
}
