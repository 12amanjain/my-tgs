package com.tgs.services.oms.servicehandler.hotel;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliation;
import com.tgs.services.oms.datamodel.hotel.HotelBookingReconciliationAdditionalInfo;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.datamodel.hotel.HotelReconciliationResult;
import com.tgs.services.oms.dbmodel.hotel.DbHotelBookingReconciliation;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.hotel.HotelBookingReconciliationService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.mapper.HotelInfoToHotelReconciliationInfo;
import com.tgs.services.oms.restmodel.hotel.HotelBookingReconciliationRequest;
import com.tgs.services.oms.restmodel.hotel.HotelBookingReconciliationResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBookingReconciliationHandler extends ServiceHandler<HotelBookingReconciliationRequest, BaseResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private HotelBookingReconciliationService reconciliationService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {

		Order order = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (Objects.isNull(order)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}

		isReconciliationAllowed(order);
		List<DbHotelOrderItem> dbOrderItems = itemService.findByBookingIdOrderByIdAsc(request.getBookingId());
		if (StringUtils.isBlank(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingReference())) {
			throw new CustomGeneralException(SystemError.SUPPLIER_REFERENCE_MISSING);
		}
		log.info("Reconciling booking id {}", request.getBookingId());

		HotelBookingReconciliation newReconciliationInfo = getUpdatedBookingInfoToReconcile(order, dbOrderItems);
		saveOrUpdateReconciledInfo(newReconciliationInfo, order);
	}

	private HotelBookingReconciliation getUpdatedBookingInfoToReconcile(Order order,
			List<DbHotelOrderItem> dbOrderItems) {
		try {
			HotelImportBookingParams importBookingParams = HotelImportBookingParams.builder()
					.supplierBookingId(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingReference())
					.supplierId(dbOrderItems.get(0).getSupplierId()).bookingId(order.getBookingId())
					.deliveryInfo(order.getDeliveryInfo()).marketingFees(getMarketingFees(dbOrderItems)).build();
			User bookingUser = userComm.getUserFromCache(order.getBookingUserId());
			HotelImportedBookingInfo bookingInfo = hmsCommunicator.retrieveBooking(importBookingParams, bookingUser);
			return HotelInfoToHotelReconciliationInfo.builder().hotelInfo(bookingInfo.getHInfo())
					.deliveryInfo(bookingInfo.getDeliveryInfo()).bookingStatus(bookingInfo.getOrderStatus())
					.bookingDate(Objects.isNull(bookingInfo.getBookingDate()) ? order.getCreatedOn().toLocalDate()
							: bookingInfo.getBookingDate())
					.bookingId(order.getBookingId()).userId(SystemContextHolder.getContextData().getUser().getUserId())
					.build().convert();
		} catch (Exception e) {
			log.error("Unable to convert supplier info to reconciled info for booking id {}", order.getBookingId(), e);
			throw new CustomGeneralException(SystemError.SUPPLIER_INFO_PROCESS_ERROR);
		}
	}

	private HotelBookingReconciliation createHotelReconciliationInfoFromDB(Order order,
			List<DbHotelOrderItem> dbOrderItems) {

		HotelInfo oldHotelInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(dbOrderItems).build().convert();
		return HotelInfoToHotelReconciliationInfo.builder().hotelInfo(oldHotelInfo)
				.deliveryInfo(order.getDeliveryInfo()).bookingStatus(order.getStatus().name())
				.bookingDate(order.getCreatedOn().toLocalDate()).build().convert();
	}

	public HotelBookingReconciliationResponse getReconciliationInfo(
			HotelBookingReconciliationRequest reconciliationRequest) {
		HotelBookingReconciliationResponse response = new HotelBookingReconciliationResponse();
		Order order = orderService.findByBookingId(reconciliationRequest.getBookingId()).toDomain();
		if (Objects.isNull(order)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}

		List<DbHotelOrderItem> dbOrderItems =
				itemService.findByBookingIdOrderByIdAsc(reconciliationRequest.getBookingId());
		HotelBookingReconciliation oldReconciliationInfo = createHotelReconciliationInfoFromDB(order, dbOrderItems);
		DbHotelBookingReconciliation dbNewReconciliationInfo =
				reconciliationService.findByBookingId(reconciliationRequest.getBookingId());
		if (Objects.nonNull(dbNewReconciliationInfo)) {
			HotelBookingReconciliation newReconciliationInfo = dbNewReconciliationInfo.toDomain();
			response.setHotelInfos(reconcileHotelInfo(oldReconciliationInfo, newReconciliationInfo));
			response.setRoomInfos(
					reconcileRoomsInfo(oldReconciliationInfo.getRoomInfos(), newReconciliationInfo.getRoomInfos()));
		} else {
			HotelBookingReconciliation newReconciliationInfo = getUpdatedBookingInfoToReconcile(order, dbOrderItems);
			saveOrUpdateReconciledInfo(newReconciliationInfo, order);
			response.setHotelInfos(reconcileHotelInfo(oldReconciliationInfo, newReconciliationInfo));
			response.setRoomInfos(
					reconcileRoomsInfo(oldReconciliationInfo.getRoomInfos(), newReconciliationInfo.getRoomInfos()));
		}
		DbHotelOrderItem orderItem = dbOrderItems.get(0);
		AdditionalHotelOrderItemInfo hotelInfo = orderItem.getAdditionalInfo();
		response.setBookingId(orderItem.getBookingId());
		response.setBookingRefNo(hotelInfo.getSupplierBookingReference());
		response.setSupplier(orderItem.getSupplierId());
		response.setHotelName(hotelInfo.getHInfo().getName());
		response.setDescription(hotelInfo.getHInfo().getName());
		return response;
	}

	private List<HotelReconciliationResult> reconcileHotelInfo(HotelBookingReconciliation oldReconciliationInfo,
			HotelBookingReconciliation newReconciliationInfo) {

		List<HotelReconciliationResult> hotelInfos = new ArrayList<>();
		HotelBookingReconciliationAdditionalInfo oldReconciliationAddlInfo = oldReconciliationInfo.getAdditionalInfo();
		HotelBookingReconciliationAdditionalInfo newReconciliationAddlInfo = newReconciliationInfo.getAdditionalInfo();

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("bookingStatus")
				.oldValue(oldReconciliationInfo.getBookingStatus()).newValue(newReconciliationInfo.getBookingStatus())
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("bookingDate")
				.oldValue(oldReconciliationInfo.getBookingDate()).newValue(newReconciliationInfo.getBookingDate())
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("hotelName")
				.oldValue(oldReconciliationInfo.getHotelName()).newValue(newReconciliationInfo.getHotelName()).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("city")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getCityInfo().getName())
				.newValue(newReconciliationAddlInfo.getAddressInfo().getCityInfo().getName()).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("country")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getCityInfo().getCountry())
				.newValue(newReconciliationAddlInfo.getAddressInfo().getCityInfo().getCountry()).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("checkIn")
				.oldValue(oldReconciliationInfo.getCheckInDate()).newValue(newReconciliationInfo.getCheckInDate())
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("checkOut")
				.oldValue(oldReconciliationInfo.getCheckOutDate()).newValue(newReconciliationInfo.getCheckOutDate())
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("currency")
				.oldValue(oldReconciliationAddlInfo.getCurrency()).newValue(newReconciliationAddlInfo.getCurrency())
				.build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("cancellationDeadline")
				.oldValue(oldReconciliationInfo.getCancellationDeadline())
				.newValue(newReconciliationInfo.getCancellationDeadline()).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("pinCode")
				.oldValue(oldReconciliationAddlInfo.getAddressInfo().getPincode())
				.newValue(newReconciliationAddlInfo.getAddressInfo().getPincode()).build());

		hotelInfos.add(HotelReconciliationResult.builder().fieldName("totalPrice")
				.oldValue(oldReconciliationInfo.getAmount()).newValue(newReconciliationInfo.getAmount()).build());

		return hotelInfos;
	}

	private List<List<HotelReconciliationResult>> reconcileRoomsInfo(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		if (isIdPresent(newRoomInfos.get(0))) {
			return reconcileRoomsInfoById(oldRoomInfos, newRoomInfos);
		} else {
			return reconcileRoomsInfoByType(oldRoomInfos, newRoomInfos);
		}
	}

	private List<List<HotelReconciliationResult>> reconcileRoomsInfoById(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		Map<String, RoomInfo> oldRoomInfoMap =
				oldRoomInfos.stream().collect(Collectors.toMap(RoomInfo::getId, roomInfo -> roomInfo));

		Map<String, RoomInfo> newRoomInfoMap =
				newRoomInfos.stream().collect(Collectors.toMap(RoomInfo::getId, roomInfo -> roomInfo));

		List<List<HotelReconciliationResult>> roomReconciliationInfos = new ArrayList<>();
		Set<String> traversedIds = new HashSet<>();
		oldRoomInfoMap.forEach((id, oldRoomInfo) -> {
			RoomInfo newRoomInfo = newRoomInfoMap.get(id);
			List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
			perRoomReconciliationInfos
					.add(HotelReconciliationResult.builder().fieldName("mealBasis").oldValue(oldRoomInfo.getMealBasis())
							.newValue(Objects.isNull(newRoomInfo) ? null : newRoomInfo.getMealBasis()).build());

			perRoomReconciliationInfos
					.add(HotelReconciliationResult.builder().fieldName("roomType").oldValue(oldRoomInfo.getRoomType())
							.newValue(Objects.isNull(newRoomInfo) ? null : oldRoomInfo.getRoomType()).build());

			roomReconciliationInfos.add(perRoomReconciliationInfos);

			traversedIds.add(id);
		});

		newRoomInfoMap.forEach((id, newRoomInfo) -> {
			if (!traversedIds.contains(id)) {
				List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
				perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
						.newValue(newRoomInfo.getMealBasis()).build());

				perRoomReconciliationInfos
						.add(HotelReconciliationResult.builder().fieldName("roomType").newValue("Unknown").build());
				roomReconciliationInfos.add(perRoomReconciliationInfos);
			}
		});

		return roomReconciliationInfos;
	}

	private List<List<HotelReconciliationResult>> reconcileRoomsInfoByType(List<RoomInfo> oldRoomInfos,
			List<RoomInfo> newRoomInfos) {

		Map<String, List<RoomInfo>> oldRoomInfoMap =
				oldRoomInfos.stream().collect(Collectors.groupingBy(roomInfo -> roomInfo.getRoomType()));

		Map<String, List<RoomInfo>> newRoomInfoMap =
				newRoomInfos.stream().collect(Collectors.groupingBy(roomInfo -> roomInfo.getRoomType()));

		List<List<HotelReconciliationResult>> roomReconciliationInfos = new ArrayList<>();
		Set<RoomInfo> traversedIds = new HashSet<>();
		oldRoomInfoMap.forEach((roomType, groupedOldRoomInfos) -> {
			List<RoomInfo> groupedNewRoomInfos = newRoomInfoMap.get(roomType);
			if (CollectionUtils.isNotEmpty(groupedNewRoomInfos)) {
				for (RoomInfo oldRoomInfo : groupedOldRoomInfos) {

					for (Iterator<RoomInfo> groupedNewRoomInfoIterator =
							groupedNewRoomInfos.iterator(); groupedNewRoomInfoIterator.hasNext();) {

						RoomInfo newRoomInfo = groupedNewRoomInfoIterator.next();
						List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
						perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
								.oldValue(oldRoomInfo.getMealBasis())
								.newValue(ObjectUtils.firstNonNull(newRoomInfo.getMealBasis(), "Unknown")).build());

						perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
								.oldValue(oldRoomInfo.getRoomType()).newValue(newRoomInfo.getRoomType()).build());
						roomReconciliationInfos.add(perRoomReconciliationInfos);
						groupedNewRoomInfoIterator.remove();
						traversedIds.add(newRoomInfo);
						break;
					}
				}
			} else {
				for (RoomInfo oldRoomInfo : groupedOldRoomInfos) {
					List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
							.oldValue(oldRoomInfo.getMealBasis()).newValue("Unknown").build());

					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
							.oldValue(oldRoomInfo.getRoomType()).newValue("Unknown").build());
					roomReconciliationInfos.add(perRoomReconciliationInfos);
				}
			}

		});

		newRoomInfoMap.forEach((roomType, groupedNewRoomInfos) -> {

			for (RoomInfo newRoomInfo : groupedNewRoomInfos) {
				if (!traversedIds.contains(newRoomInfo)) {
					List<HotelReconciliationResult> perRoomReconciliationInfos = new ArrayList<>();
					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("mealBasis")
							.newValue(ObjectUtils.firstNonNull(newRoomInfo.getMealBasis(), "Unknown"))
							.oldValue("Unknown").build());

					perRoomReconciliationInfos.add(HotelReconciliationResult.builder().fieldName("roomType")
							.oldValue("Unknown").newValue(newRoomInfo.getRoomType()).build());
					roomReconciliationInfos.add(perRoomReconciliationInfos);
				}
			}
		});

		return roomReconciliationInfos;
	}

	private void saveOrUpdateReconciledInfo(HotelBookingReconciliation newReconciliationInfo, Order order) {
		try {
			DbHotelBookingReconciliation oldDBReconciliationInfo =
					reconciliationService.findByBookingId(newReconciliationInfo.getBookingId());
			if (Objects.isNull(oldDBReconciliationInfo)) {
				oldDBReconciliationInfo = new DbHotelBookingReconciliation().from(newReconciliationInfo);
				order.getAdditionalInfo().setIsReconciledWithSupplier(true);
				orderManager.saveWithoutFetch(order);
			} else {
				DbHotelBookingReconciliation newDBReconciliationInfo =
						new DbHotelBookingReconciliation().from(newReconciliationInfo);
				oldDBReconciliationInfo = new GsonMapper<>(newDBReconciliationInfo, oldDBReconciliationInfo,
						DbHotelBookingReconciliation.class).convert();
			}
			reconciliationService.save(oldDBReconciliationInfo);
		} catch (Exception e) {
			log.error("Unable to save hotel reconciliation info {} ", GsonUtils.getGson().toJson(newReconciliationInfo),
					e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		}
	}

	public void updateReconciledBooking(HotelBookingReconciliationRequest reconciliationRequest) {
		Order order = orderService.findByBookingId(reconciliationRequest.getBookingId()).toDomain();
		if (Objects.isNull(order)) {
			throw new CustomGeneralException(SystemError.INVALID_BOOKING_ID);
		}

		order.getAdditionalInfo().setReconciliationStatus(reconciliationRequest.getReconciliationStatus());
		orderManager.saveWithoutProcessedOn(order);
	}

	private void isReconciliationAllowed(Order order) {

		switch (order.getStatus()) {
			case SUCCESS:
				break;
			default:
				throw new CustomGeneralException(SystemError.RECONCILIATION_NOT_ALLOWED);
		}
	}

	private double getMarketingFees(List<DbHotelOrderItem> orderItems) {

		DbHotelOrderItem firstOrderItem = orderItems.get(0);
		return firstOrderItem.getRoomInfo().getPerNightPriceInfos().get(0).getFareComponents()
				.getOrDefault(HotelFareComponent.TMF, 0.0d);
	}

	private boolean isIdPresent(RoomInfo roomInfo) {

		return StringUtils.isNotBlank(roomInfo.getId());
	}

	@Override
	public void afterProcess() throws Exception {

	}
}
