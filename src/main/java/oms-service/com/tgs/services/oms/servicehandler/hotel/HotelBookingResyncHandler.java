package com.tgs.services.oms.servicehandler.hotel;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.hms.datamodel.AdditionalHotelOrderItemInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.ProcessedHotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.BookingResyncResponse;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderDetails;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.helper.OmsHelper;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.jparepository.hotel.HotelOrderItemService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelBookingResyncHandler extends ServiceHandler<BookingDetailRequest, BookingResyncResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private HotelOrderItemService itemService;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	HotelOrderItemManager orderItemManager;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private MoneyExchangeCommunicator moneyExchangeComm;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		Order order = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (!ObjectUtils.isEmpty(SystemContextHolder.getContextData().getUser())) {
			UserServiceHelper.checkAndReturnAllowedUserId(
					SystemContextHolder.getContextData().getUser().getLoggedInUserId(),
					Arrays.asList(order.getBookingUserId()));
		}
		List<DbHotelOrderItem> dbOrderItems = itemService.findByBookingIdOrderByIdAsc(request.getBookingId());
		if (StringUtils.isBlank(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingReference())) {
			throw new CustomGeneralException(SystemError.SUPPLIER_REFERENCE_MISSING);
		}

		HotelImportBookingParams importBookingParams = HotelImportBookingParams.builder()
				.supplierBookingId(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingReference())
				.supplierId(dbOrderItems.get(0).getSupplierId()).bookingId(order.getBookingId()).build();
		User bookingUser = userComm.getUserFromCache(order.getBookingUserId());
		HotelImportedBookingInfo bookingInfo = hmsCommunicator.retrieveBooking(importBookingParams, bookingUser);
		bookingInfo.getHInfo().getMiscInfo()
				.setSupplierBookingId(dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingId());
		BookingDetailResponse bookingResponse = getBookingDetailResponse(bookingInfo, bookingUser);
		resyncBooking(bookingResponse, order, dbOrderItems, bookingResponse.getBookingUser(),
				bookingInfo.getOrderStatus(), bookingInfo.getBookingCurrencyCode());
	}

	private BookingDetailResponse getBookingDetailResponse(HotelImportedBookingInfo bookingInfo, User user) {
		Order order = Order.builder().deliveryInfo(bookingInfo.getDeliveryInfo())
				.channelType(SystemContextHolder.getChannelType()).orderType(OrderType.HOTEL)
				.loggedInUserId(user.getUserId()).build();
		Map<String, ItemDetails> hotelOrderDetailsMap = new HashMap<>();
		hotelOrderDetailsMap.put(OrderType.HOTEL.getName(),
				HotelOrderDetails.builder().hInfo(bookingInfo.getHInfo()).query(bookingInfo.getSearchQuery()).build());
		return BookingDetailResponse.builder().bookingUser(user).order(order).itemInfos(hotelOrderDetailsMap).build();
	}

	private void resyncBooking(BookingDetailResponse bookingResponse, Order order, List<DbHotelOrderItem> dbOrderItems,
			User user, String supplierOrderStatus, String currencyCode) {
		HotelOrderDetails apiOrderDetails =
				(HotelOrderDetails) bookingResponse.getItemInfos().get(OrderType.HOTEL.getName());
		List<DbHotelOrderItem> resyncedHotelOrderItem =
				resyncOption(apiOrderDetails, dbOrderItems, bookingResponse.getOrder(), user, currencyCode);
		resyncOrderWithOrderItems(resyncedHotelOrderItem, order);
		if (order.getStatus().equals(OrderStatus.PENDING) && (StringUtils.isNotBlank(supplierOrderStatus)
				&& OrderStatus.valueOf(supplierOrderStatus).equals(OrderStatus.SUCCESS))) {
			if (ObjectUtils.isEmpty(order.getAdditionalInfo().getPaymentStatus())) {
				orderItemManager.save(resyncedHotelOrderItem, order, HotelItemStatus.ON_HOLD);
			}
		} else {
			orderItemManager.save(resyncedHotelOrderItem, order, null);
		}
	}

	private void resyncOrderWithOrderItems(List<DbHotelOrderItem> resyncedHotelOrderItem, Order order) {
		Double totalOrderMarkup = 0.0;
		Double totalOrderAmount = 0.0;
		for (DbHotelOrderItem newHotelOrderItem : resyncedHotelOrderItem) {
			if (!BooleanUtils.isTrue(newHotelOrderItem.getRoomInfo().getIsDeleted())) {
				Double orderItemMarkup =
						newHotelOrderItem.getRoomInfo().getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
							if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF))) {
								Map<HotelFareComponent, Double> fareComponent =
										priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
								return fareComponent.getOrDefault(HotelFareComponent.MU, 0.0);
							}
							return 0.0;
						}).sum();
				totalOrderAmount += newHotelOrderItem.getAmount();
				totalOrderMarkup += orderItemMarkup;
				newHotelOrderItem.setMarkup(orderItemMarkup);
			}
		}
		order.setAmount(totalOrderAmount);
		order.setMarkup(totalOrderMarkup);
	}


	private List<DbHotelOrderItem> resyncOption(HotelOrderDetails apiOrderDetails, List<DbHotelOrderItem> dbOrderItems,
			Order order, User user, String currencyCode) {
		Double perNightMarkup = dbOrderItems.get(0).getRoomInfo().getPerNightPriceInfos().get(0).getFareComponents()
				.get(HotelFareComponent.MU);
		HotelInfo newHotelInfo = apiOrderDetails.getHInfo();
		String supplierBookingId = dbOrderItems.get(0).getAdditionalInfo().getSupplierBookingId();
		if (StringUtils.isNotBlank(supplierBookingId)) {
			newHotelInfo.getMiscInfo().setSupplierBookingId(supplierBookingId);
		}
		double totalPriceDiff = 0;
		String bookingId = dbOrderItems.get(0).getBookingId();
		List<RoomInfo> notMatchedRooms = new ArrayList<>();
		List<Long> matchedRoomIds = new ArrayList<>();
		Option apiOption = newHotelInfo.getOptions().get(0);
		for (RoomInfo roomInfo : apiOption.getRoomInfos()) {
			String roomType = StringEscapeUtils.unescapeHtml4(roomInfo.getRoomType());
			boolean isRoomFound = false;

			for (DbHotelOrderItem orderItem : dbOrderItems) {
				if (!matchedRoomIds.contains(orderItem.getId())) {
					RoomInfo dbRoomInfo = orderItem.getRoomInfo();
					if (roomType.equals(StringEscapeUtils.unescapeHtml4(dbRoomInfo.getRoomType()))) {
						isRoomFound = true;
						totalPriceDiff += resyncRoomInfo(roomInfo, dbRoomInfo);
						createOrUpdateOrderItem(roomInfo, apiOrderDetails, orderItem, bookingId, order, user,
								perNightMarkup);
						matchedRoomIds.add(orderItem.getId());
						break;
					}
				}
			}
			if (!isRoomFound)
				notMatchedRooms.add(roomInfo);
		}

		for (RoomInfo roomInfo : notMatchedRooms) {
			totalPriceDiff += roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
					+ roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SP, 0.0);
			DbHotelOrderItem dbOrderItem =
					createOrUpdateOrderItem(roomInfo, apiOrderDetails, null, bookingId, order, user, perNightMarkup);
			dbOrderItems.add(dbOrderItem);
		}

		if (matchedRoomIds.size() != dbOrderItems.size()) {
			for (DbHotelOrderItem orderItem : dbOrderItems) {
				if (orderItem.getId() != null && !matchedRoomIds.contains(orderItem.getId())) {
					totalPriceDiff -= orderItem.getAmount();
					log.info("OrderItemId {} , bookingId {} should be deleted from database", orderItem.getId(),
							bookingId);
					orderItem.getRoomInfo().setIsDeleted(true);
				}
			}
		}

		if (totalPriceDiff != 0) {
			// Call Payment Module;
			MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().type("General")
					.fromCurrency(currencyCode).toCurrency("INR").build();
			if (!filter.fromCurrency.equals(filter.toCurrency)) {
				totalPriceDiff = moneyExchangeComm.getExchangeValue(totalPriceDiff, filter, true);
			}
			DecimalFormat df = new DecimalFormat("#.##");
			if (totalPriceDiff < 0) {
				response.addNotes(df.format(Math.abs(totalPriceDiff)) + " INR are need to be refunded");
			} else {
				response.addNotes(df.format(totalPriceDiff) + " INR are need to be charged");
			}
			log.error("Total amount difference for booking id {} is {}", bookingId, totalPriceDiff);
		}
		return dbOrderItems;
	}

	private DbHotelOrderItem createOrUpdateOrderItem(RoomInfo roomInfo, HotelOrderDetails apiOrderDetails,
			DbHotelOrderItem orderItem, String bookingId, Order order, User user, Double perNightMarkup) {
		if (orderItem == null) {
			orderItem = new DbHotelOrderItem();
		}
		HotelInfo hInfo = apiOrderDetails.getHInfo();
		HotelSearchQuery searchQuery = apiOrderDetails.getQuery();
		orderItem.setBookingId(bookingId);
		orderItem.setCheckInDate(searchQuery.getCheckinDate());
		orderItem.setCheckOutDate(searchQuery.getCheckoutDate());
		OmsHelper.updateGstComponent(roomInfo, user);
		AdditionalHotelOrderItemInfo additionalItemInfo = AdditionalHotelOrderItemInfo.builder().build();
		additionalItemInfo =
				new GsonMapper<>(orderItem.getAdditionalInfo(), additionalItemInfo, AdditionalHotelOrderItemInfo.class)
						.convert();
		if (!ObjectUtils.isEmpty(orderItem.getAdditionalInfo())
				&& !ObjectUtils.isEmpty(orderItem.getAdditionalInfo().getHInfo())) {
			ProcessedHotelInfo processedHotelInfo = ProcessedHotelInfo.builder().build();
			processedHotelInfo = new GsonMapper<>(orderItem.getAdditionalInfo().getHInfo(), processedHotelInfo,
					ProcessedHotelInfo.class).convert();
			processedHotelInfo.setAddress(hInfo.getAddress());
			processedHotelInfo.setName(hInfo.getName());
			additionalItemInfo.setHInfo(processedHotelInfo);
		}
		additionalItemInfo.setSupplierId(hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
		additionalItemInfo.setSupplierBookingReference(hInfo.getMiscInfo().getSupplierBookingReference());
		additionalItemInfo.setSupplierBookingId(hInfo.getMiscInfo().getSupplierBookingId());
		orderItem.setAdditionalInfo(additionalItemInfo);
		orderItem.setAmount(roomInfo.getTotalFareComponents().get(HotelFareComponent.TF));
		orderItem.setHotel(hInfo.getName());
		if (orderItem.getStatus() != null) {
			orderItem.setStatus(orderItem.getStatus());
		} else {
			orderItem.setStatus(HotelItemStatus.IN_PROGRESS.getCode());
		}
		orderItem.setRoomName(roomInfo.getRoomType());
		orderItem.setSupplierId(apiOrderDetails.getHInfo().getOptions().get(0).getMiscInfo().getSupplierId());
		OmsHelper.updateGstComponent(roomInfo, user);
		if (roomInfo.getId() == null) {
			if (perNightMarkup != null) {
				for (int i = 0; i < roomInfo.getPerNightPriceInfos().size(); i++) {
					roomInfo.getPerNightPriceInfos().get(i).getFareComponents().put(HotelFareComponent.MU,
							perNightMarkup);
				}
				BaseHotelUtils.updatePerNightTotalFareComponents(roomInfo);
			}
			roomInfo.setId(TgsStringUtils.generateRandomNumber(12, "id_"));
		}
		BaseHotelUtils.flattenFareComponents(roomInfo);
		orderItem.setRoomInfo(roomInfo);
		return orderItem;
	}

	private double resyncRoomInfo(RoomInfo newRoomInfo, RoomInfo oldRoomInfo) {
		double totalPriceDiff = 0;
		oldRoomInfo.setRoomType(newRoomInfo.getRoomType());
		newRoomInfo.setMealBasis(oldRoomInfo.getMealBasis());
		if (newRoomInfo.getTotalPrice() != oldRoomInfo.getTotalPrice()) {
			totalPriceDiff = newRoomInfo.getTotalFareComponents().get(HotelFareComponent.BF)
					- (oldRoomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
					+ oldRoomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SP, 0.0));
			oldRoomInfo.getTotalFareComponents().put(HotelFareComponent.BF,
					newRoomInfo.getTotalFareComponents().get(HotelFareComponent.BF));

			List<PriceInfo> newPriceInfos = new ArrayList<>();
			if (newRoomInfo.getPerNightPriceInfos().size() != oldRoomInfo.getPerNightPriceInfos().size()) {
				for (int i = 0; i < newRoomInfo.getPerNightPriceInfos().size(); i++) {
					newPriceInfos.add(newRoomInfo.getPerNightPriceInfos().get(i));
					newPriceInfos.get(i).getFareComponents().put(HotelFareComponent.BF,
							newRoomInfo.getPerNightPriceInfos().get(i).getFareComponents().get(HotelFareComponent.BF));
				}
				oldRoomInfo.setPerNightPriceInfos(newPriceInfos);
			}
		}
		if (newRoomInfo.getTravellerInfo() != null && oldRoomInfo.getTravellerInfo() != null) {
			if (newRoomInfo.getTravellerInfo().size() != oldRoomInfo.getTravellerInfo().size()) {
				oldRoomInfo.setTravellerInfo(newRoomInfo.getTravellerInfo());
			}
		}
		setPanNumber(newRoomInfo, oldRoomInfo);
		return totalPriceDiff;
	}

	private void setPanNumber(RoomInfo newRoomInfo, RoomInfo oldRoomInfo) {

		List<TravellerInfo> travellerInfo = oldRoomInfo.getTravellerInfo();
		newRoomInfo.getTravellerInfo().get(0).setPanNumber(travellerInfo.get(0).getPanNumber());
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

}
