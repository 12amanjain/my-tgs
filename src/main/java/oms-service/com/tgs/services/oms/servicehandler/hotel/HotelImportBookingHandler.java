package com.tgs.services.oms.servicehandler.hotel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.analytics.HotelBookingAnalyticsInfo;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderDetails;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.manager.hotel.HotelBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.hotel.HotelOrderItemToHotelSearchQueryMapper;
import com.tgs.services.oms.restmodel.BookingDetailResponse;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelImportBookingRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelImportBookingHandler extends ServiceHandler<HotelImportBookingRequest, BookingDetailResponse> {

	@Autowired
	HotelBookingManager bookingManager;

	@Autowired
	HotelOrderItemManager itemManager;
	
	@Autowired
	HMSCachingServiceCommunicator cachingService;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	private String bookingId;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void process() throws Exception {
		HotelImportedBookingInfo bookingInfo = null;
		try {
			bookingId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.HOTEL).build());

			HotelImportBookingParams importBookingParams = HotelImportBookingParams.builder().bookingId(bookingId)
					.supplierBookingId(request.getSupplierBookingId()).supplierId(request.getSupplierId())
					.deliveryInfo(request.getDeliveryInfo()).build();
			User bookingUser = userComm.getUserFromCache(request.getBookingUserId());
			bookingInfo = hmsCommunicator.retrieveBooking(importBookingParams, bookingUser);
			storeInCache(bookingInfo.getHInfo(), bookingInfo.getSearchQuery(), bookingId);
			response.setBookingUser(bookingUser);
			processAndSaveBooking(bookingInfo);
		} catch (Exception e) {
			contextData.getErrorMessages().add("Unable to process booking due to service exception");
			log.error("Unable to process imported booking for booking Id {}", bookingId, e);
			throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
		} finally {
			if (bookingInfo != null && response.getOrder() != null)
				sendBookingDataToAnalytics(bookingInfo.getHInfo(), response.getOrder());
		}
	}

	private void sendBookingDataToAnalytics(HotelInfo hInfo, Order order) {
		try {
			contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
					Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
							.time(System.currentTimeMillis()).build()));
			HotelSearchQuery searchQuery = HotelSearchQuery.builder().build();
			String orderStatus = null;
			HotelBookingAnalyticsInfo bookingAnalyticsInfo =
					HotelBookingAnalyticsInfo.builder().hInfo(hInfo).bookingId(order.getBookingId()).build();

			if (order != null) {
				searchQuery = HotelOrderItemToHotelSearchQueryMapper.builder().items(bookingManager.getItems()).build()
						.convert();
				bookingAnalyticsInfo.setSearchQuery(searchQuery);
				orderStatus = order.getStatus().getCode();
				bookingAnalyticsInfo.setFlowType(OrderFlowType.IMPORT_PNR.getName());
				bookingAnalyticsInfo.setOrderStatus(orderStatus);
			}
			hmsCommunicator.addBookingToAnalytics(bookingAnalyticsInfo);
		} catch (Exception e) {
			log.error("Unable to push booking analytics info for booking id {}", request.getBookingId(), e);
		}
	}

	public void storeInCache(HotelInfo hInfo, HotelSearchQuery searchQuery, String bookingId) {
		HotelReviewResponse reviewResponse = new HotelReviewResponse();
		reviewResponse.setBookingId(bookingId);
		reviewResponse.setHInfo(hInfo);
		reviewResponse.setQuery(searchQuery);
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(bookingId)
				.binValues(ImmutableMap.of(BinName.HOTELINFO.getName(), reviewResponse)).compress(false).build();
		cachingService.store(metaInfo);
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	private void processAndSaveBooking(HotelImportedBookingInfo bookingInfo) {
		HotelBookingRequest bookingRequest = new HotelBookingRequest();
		List<RoomTravellerInfo> roomTravellerInfoList = new ArrayList<>();
		HotelInfo hInfo = bookingInfo.getHInfo();
		hInfo.getMiscInfo().setSupplierBookingId(request.getSupplierBookingId());
		hInfo.getOptions().get(0).getRoomInfos().stream().forEach(roomInfo -> {
			RoomTravellerInfo roomTravellerInfo = new RoomTravellerInfo();
			roomTravellerInfo.setTravellerInfo(roomInfo.getTravellerInfo());
			roomTravellerInfoList.add(roomTravellerInfo);
		});
		bookingRequest.setBookingId(bookingId);
		bookingRequest.setDeliveryInfo(bookingInfo.getDeliveryInfo());
		bookingRequest.setType(OrderType.HOTEL);
		bookingRequest.setRoomTravellerInfo(roomTravellerInfoList);
		bookingRequest.setFlowType(OrderFlowType.IMPORT_PNR);
		bookingManager.setBookingRequest(bookingRequest);
		bookingManager.setHInfo(hInfo);
		bookingManager.setBookingUser(response.getBookingUser());
		bookingManager.setLoggedInUser(contextData.getUser());
		Order dbOrder = bookingManager.processBooking();
		itemManager.updateOrderItemStatus(dbOrder, HotelItemStatus.ON_HOLD);
		createBookingDetailResponse(bookingInfo, dbOrder);
	}

	private void createBookingDetailResponse(HotelImportedBookingInfo bookingInfo, Order order) {
		Map<String, ItemDetails> hotelOrderDetailsMap = new HashMap<>();
		BaseHotelUtils.updateTotalFareComponents(bookingInfo.getHInfo());
		hotelOrderDetailsMap.put(OrderType.HOTEL.getName(),
				HotelOrderDetails.builder().hInfo(bookingInfo.getHInfo()).query(bookingInfo.getSearchQuery()).build());
		response.setItemInfos(hotelOrderDetailsMap);
		response.setOrder(order);
	}


}
