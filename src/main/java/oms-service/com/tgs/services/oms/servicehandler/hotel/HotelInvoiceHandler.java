package com.tgs.services.oms.servicehandler.hotel;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.InvoiceInfo;
import com.tgs.services.oms.datamodel.InvoiceType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceRequest;
import com.tgs.services.oms.restmodel.hotel.HotelInvoiceResponse;
import com.tgs.services.oms.servicehandler.InvoiceHandler;
import com.tgs.services.ums.datamodel.User;

@Service
public class HotelInvoiceHandler extends InvoiceHandler<HotelInvoiceRequest, HotelInvoiceResponse> {

	@Autowired
	private HotelOrderItemManager itemManager;

	@Autowired
	private  OrderManager orderManager;

	@Autowired
	private  UserServiceCommunicator userServiceCommunicator;

	@Autowired
	private  GeneralServiceCommunicator generalServiceCommunicator;
	
	@Autowired
	private HotelAmendmentManager amendmentManager;

	private HotelInfo hInfo;


	@Override
	public void beforeProcess() throws Exception {

		if (Objects.nonNull(request.getHInfo())) {
			/*
			 * Case when we are fetching invoice for an amendment . In this case 
			 * hotelInfo will be provided by the amendmentManager itself.
			 */
			hInfo = request.getHInfo();
			return;
		}
		
		/*
		 * Case when we are trying to get invoice for a booking & it has been amended.
		 */
		hInfo = amendmentManager.getBaseBookingFromAmendment(request.getId());
		if(Objects.isNull(hInfo)) {
			/*
			 * Case when we are trying to get invoice for a booking & it hasn't yet been amended.
			 */
			List<DbHotelOrderItem> orderItems = itemManager.getItemList(request.getId());
			hInfo = DbHotelOrderItemListToHotelInfo.builder()
					.itemList((orderItems)).build().convert();
		}
	}

	/**/

	@Override
	public void process() throws Exception {

		Order order = orderManager.findByBookingId(request.getId(), new Order());
		if (order == null) {
			response.addError(new ErrorDetail(SystemError.ORDER_NOT_FOUND));
			return;
		}
		User user = userServiceCommunicator.getUserFromCache(order.getBookingUserId());
		response.setInvoiceId(order.getAdditionalInfo().getInvoiceId());
		response.setConfirmationNumber(hInfo.getMiscInfo().getHotelBookingReference());
		response.setCreatedOn(order.getCreatedOn());
		InvoiceInfo agentInfo = getAgentInvoiceInfo(user);
		if (request.getType() == InvoiceType.AGENCY) {
			response.setFrom(getClientInvoiceInfo(
					(ClientGeneralInfo) generalServiceCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
							GeneralBasicFact.builder().applicableTime(order.getCreatedOn()).build())));
			response.setTo(agentInfo);
		} else {
			response.setFrom(agentInfo);
			response.setTo(getCustomerInvoiceInfo(order));
		}

		HotelInfo newHotelInfo = getUpdatedHotelInfo();
		response.setGstInfo(orderManager.getGstInfo(request.getId()));
		response.setHotel(newHotelInfo);
		response.setBookingId(order.getBookingId());
	}

	private HotelInfo getUpdatedHotelInfo() {

		Option oldOption = hInfo.getOptions().get(0);
		List<RoomInfo> oldRoomInfos = oldOption.getRoomInfos();
		List<RoomInfo> newRoomInfos = new ArrayList<>();
		for (RoomInfo oldRoom : oldRoomInfos) {
			RoomInfo newRoom = new RoomInfo();
			newRoom.setCheckInDate(oldRoom.getCheckInDate());
			newRoom.setCheckOutDate(oldRoom.getCheckOutDate());
			newRoom.setRoomType(oldRoom.getRoomType());
			newRoom.setRoomCategory(oldRoom.getRoomCategory());
			newRoom.setTravellerInfo(oldRoom.getTravellerInfo());
			newRoom.setTotalFareComponents(oldRoom.getTotalFareComponents());
			newRoom.setTotalAddlFareComponents(oldRoom.getTotalAddlFareComponents());
			newRoom.setTotalPrice(oldRoom.getTotalPrice());
			newRoomInfos.add(newRoom);
		}
		Option newOption = Option.builder().roomInfos(newRoomInfos).totalPrice(oldOption.getTotalPrice()).build();
		HotelInfo newHotelInfo = HotelInfo.builder().name(hInfo.getName()).address(hInfo.getAddress())
				.options(Arrays.asList(newOption)).build();
		return newHotelInfo;
	}
	
	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}


}
