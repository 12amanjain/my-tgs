
package com.tgs.services.oms.servicehandler.hotel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.oms.BookingResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.hotel.HotelBookingRequest;
import com.tgs.services.oms.restmodel.hotel.HotelManualOrderRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelManualOrderHandler extends ServiceHandler<HotelManualOrderRequest, BookingResponse> {

	@Autowired
	HotelBookingManager bookingManager;
	
	@Autowired
	HMSCommunicator hmsComm;
	
	@Autowired
	OrderManager orderManager;
	
	@Autowired
	HotelOrderItemManager itemManager;
	
	@Autowired
	HMSCachingServiceCommunicator cachingService;
	
	@Autowired
	GeneralServiceCommunicator gmsCommunicator;
	
	@Autowired
	private UserServiceCommunicator userServiceComm;
	
	@Override
	public void beforeProcess() throws Exception {
		
		if(StringUtils.isEmpty(request.getBookingId())) {
			throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
		}
	}

	
	@Override
	public void process() throws Exception {
		
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().key(request.getBookingId()).namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).compress(false).bins(new String[] {BinName.HOTELMANUAL.getName()})
				.build();
		HotelManualOrderRequest manualOrderRequest = cachingService.fetchValue(HotelManualOrderRequest.class, metaInfo);
		
		updateRemarkInNotes(manualOrderRequest);
		bookingManager.setBookingRequest(getHotelBookingRequest(manualOrderRequest));
		bookingManager.setHInfo(manualOrderRequest.getHInfo());
		bookingManager.setLoggedInUser(SystemContextHolder.getContextData().getUser());
		bookingManager.setBookingUser(userServiceComm.getUserFromCache(manualOrderRequest.getUserId()));
		bookingManager.processBooking();
		
	}
	
	private void updateRemarkInNotes(HotelManualOrderRequest manualOrderRequest) {
		
		if(StringUtils.isNotBlank(manualOrderRequest.getRemark())) {
			String note = manualOrderRequest.getRemark();
			gmsCommunicator.addNote(Note.builder().noteType(NoteType.GENERAL)
			        .noteMessage(note).bookingId(request.getBookingId()).build());
		}
	}


	private HotelBookingRequest getHotelBookingRequest(HotelManualOrderRequest manualOrderRequest) {
		
		HotelBookingRequest bookingRequest = new HotelBookingRequest();
		bookingRequest.setBookingId(request.getBookingId());
		bookingRequest.setDeliveryInfo(manualOrderRequest.getDeliveryInfo());
		bookingRequest.setType(OrderType.HOTEL);
		bookingRequest.setRoomTravellerInfo(getRoomTravellerInfoFromRoom(manualOrderRequest));
		bookingRequest.setFlowType(OrderFlowType.MANUAL_ORDER);
		bookingRequest.setGstInfo(manualOrderRequest.getGstInfo());
		return bookingRequest;
	}

	private List<RoomTravellerInfo> getRoomTravellerInfoFromRoom(HotelManualOrderRequest manualOrderRequest) {
		
		HotelInfo hInfo = manualOrderRequest.getHInfo();
		List<RoomTravellerInfo> roomTravellerInfo = new ArrayList<>();
		for(RoomInfo ri : hInfo.getOptions().get(0).getRoomInfos()) {
			RoomTravellerInfo rti = new RoomTravellerInfo();
			rti.setTravellerInfo(ri.getTravellerInfo());
			roomTravellerInfo.add(rti);
		}
		return roomTravellerInfo;
	}

	@Override
	public void afterProcess() throws Exception {
		Order order = orderManager.findByBookingId(request.getBookingId(), null);
		itemManager.updateOrderItemStatus(order, HotelItemStatus.ON_HOLD);
		response.setBookingId(request.getBookingId());
	}

}
