package com.tgs.services.oms.servicehandler.hotel;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.manager.hotel.HotelPostBookingManager;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelManualOrderReviewHandler extends ServiceHandler<HotelPostBookingBaseRequest, HotelReviewResponse>{
	
	@Autowired
	HotelPostBookingManager postBookingManager;
	
	@Autowired
	HMSCachingServiceCommunicator cachingService;
	
	@Override
	public void beforeProcess() throws Exception {

		
		log.debug("Request is {}", GsonUtils.getGson().toJson(request));
		request.setFlowType(OrderFlowType.MANUAL_ORDER);
		if(StringUtils.isBlank(request.getBookingId())) updateBookingIdInRequest();
		postBookingManager.updateNewHotelInfoInRequestForManualOrder(request);
	}

	private void updateBookingIdInRequest() {
		String bookingId = ServiceUtils.generateId(ProductMetaInfo.builder()
				.product(Product.HOTEL).build());
		request.setBookingId(bookingId);
	}

	@Override
	public void process() throws Exception {
		
		String bookingId = request.getBookingId();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(bookingId)
				.binValues(ImmutableMap.of(BinName.HOTELMANUAL.getName(), request)).compress(false).build();
		cachingService.store(metaInfo);
	}
	
	

	@Override
	public void afterProcess() throws Exception {
		response.setHInfo(request.getHInfo());
		response.setQuery(request.getSearchQuery());
		response.setBookingId(request.getBookingId());
		
		log.debug("Request is {}", GsonUtils.getGson().toJson(response));
	}

}
