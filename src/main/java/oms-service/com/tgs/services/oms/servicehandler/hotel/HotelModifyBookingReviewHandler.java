package com.tgs.services.oms.servicehandler.hotel;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCachingServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.manager.hotel.HotelPostBookingManager;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.services.ums.datamodel.User;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelModifyBookingReviewHandler extends ServiceHandler<HotelPostBookingBaseRequest, HotelReviewResponse>{
	
	
	@Autowired
	HotelPostBookingManager postBookingManager;
	
	@Autowired
	HotelModifyBookingManager modifyBookingManager;
	
	@Autowired
	OrderManager orderManager;
	
	@Autowired
	HMSCachingServiceCommunicator cachingService;
	
	private Order order;
	
	@Override
	public void beforeProcess() throws Exception {
		
		log.info("Request is {}", GsonUtils.getGson().toJson(request));
		if(StringUtils.isBlank(request.getBookingId()))
			throw new CustomGeneralException(SystemError.KEYS_EXPIRED);
		
		User user = SystemContextHolder.getContextData().getUser();
		order = orderManager.findByBookingId(request.getBookingId(), null);
		
		if(!user.getUserId().equals(order.getAdditionalInfo().getAssignedUserId())) {
			throw new CustomGeneralException(SystemError.DIFFERENT_ASSIGNED_USER);
		}
		
		postBookingManager.updateNewHotelInfoInRequestForModifyOrder(request);
	}

	@Override
	public void process() throws Exception {
		
		HotelOrder modifiedHotelOrder = modifyBookingManager.getHotelOrderFromUpdatedHotelInfo(request);
		Order updatedOrder = modifiedHotelOrder.getOrder();
		Double differentialAmount = updatedOrder.getAmount() - order.getAmount();
		response.setDifferentialPayment(differentialAmount);
		
		String bookingId = request.getBookingId();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.HOTEL.name())
				.set(CacheSetName.HOTEL_BOOKING.getName()).key(bookingId)
				.binValues(ImmutableMap.of(BinName.HOTELMODIFY.getName(), modifiedHotelOrder))
				.compress(false).build();
		cachingService.store(metaInfo);
	}

	@Override
	public void afterProcess() throws Exception {
		response.setHInfo(request.getHInfo());
		response.setQuery(request.getSearchQuery());
		response.setBookingId(request.getBookingId());
		
		log.info("Response is {}", GsonUtils.getGson().toJson(response));
		
	}

}
