package com.tgs.services.oms.servicehandler.hotel;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.RoomSSR;
import com.tgs.services.messagingService.datamodel.AmendmentMailAttribute;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderDisputeInfo;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.hotel.messagingservice.HotelMessageAttributes;
import com.tgs.services.oms.hotel.messagingservice.HotelMessagePriceInfo;
import com.tgs.services.oms.hotel.messagingservice.HotelMessageRoomInfo;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.utils.OrderUtils;
import com.tgs.services.oms.utils.hotel.HotelBookingUtils;
import com.tgs.services.ums.datamodel.User;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class HotelOrderMessageHandler {

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	protected MsgServiceCommunicator msgSrvCommunicator;

	public void sendBookingEmail(Order order, List<DbHotelOrderItem> items, EmailTemplateKey templateKey,
			List<String> recipients) {

		try {
			if (!(order != null && CollectionUtils.isNotEmpty(order.getDeliveryInfo().getEmails()))) {
				log.info("Booking Confirmation Email Can not be send as there are no recepient emails present");
				return;
			}

			HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
			String ssr = getSSR(items);
			LocalDate checkInDate = null, checkOutDate = null;
			long nights = 0L;
			long deadlineDateHours = 0;
			Option option = null;
			if (hInfo != null && CollectionUtils.isNotEmpty(hInfo.getOptions())) {
				option = hInfo.getOptions().get(0);
				RoomInfo room = option.getRoomInfos().get(0);
				checkInDate = room.getCheckInDate();
				checkOutDate = room.getCheckOutDate();
				deadlineDateHours = Duration.between(LocalDateTime.now(), room.getDeadlineDateTime()).toHours();
				nights = ChronoUnit.DAYS.between(checkInDate, checkOutDate);
			}
			User user = userService.getUserFromCache(order.getBookingUserId());
			String toEmailId = CollectionUtils.isEmpty(recipients) ? OrderUtils.getToEmailId(order, user, null)
					: Joiner.on(",").join(recipients);
			List<HotelMessageRoomInfo> roomInfoList = getRoomInfoList(option);
			List<String> cancellationPolicyList = getCancellationPolicyList(option);
			HotelMessagePriceInfo priceInfo = getHotelMessagePriceInfo(option);

			HotelMessageAttributes msgAttr = HotelMessageAttributes.builder().hInfo(hInfo)
					.bookingId(order.getBookingId()).checkInDate(checkInDate).userName(user.getName())
					.confirmationDate(LocalDate.now()).checkOutDate(checkOutDate).toEmailId(toEmailId)
					.key(templateKey.name()).nights(nights).roomInfoList(roomInfoList)
					.cancellationPolicyList(cancellationPolicyList).priceInfo(priceInfo).partnerId(order.getPartnerId())
					.cancellationDeadlinesHours(deadlineDateHours).ssr(ssr).bookingStatus(order.getStatus().name())
					.role(user.getRole()).build();

			if (SystemContextHolder.getContextData().getUser() != null) {
				msgAttr.setConfirmedBy(SystemContextHolder.getContextData().getUser().getName());
			}

			setDisputeInfo(msgAttr, order, items);
			if (SystemContextHolder.getContextData().getUser() != null) {
				msgAttr.setConfirmedBy(SystemContextHolder.getContextData().getUser().getName());
			}

			log.info("Sending Email for order with booking Id {} , key {}", order.getBookingId(), templateKey);
			sendEmail(msgAttr);
		} catch (Exception e) {
			log.error("Error while sending email for bookingId {}", order.getBookingId(), e);
		}
	}

	public void sendAmendmentEmail(Order order, List<DbHotelOrderItem> items, EmailTemplateKey templateKey,
			List<String> recipients, Amendment amendment) {

		User user = userService.getUserFromCache(order.getBookingUserId());
		String toEmailId = CollectionUtils.isEmpty(recipients) ? OrderUtils.getToEmailId(order, user, null)
				: Joiner.on(",").join(recipients);
		AmendmentMailAttribute amendmentAttributes = AmendmentMailAttribute.builder().bookingId(order.getBookingId())
				.amdId(amendment.getAmendmentId()).amendmentType(amendment.getAmendmentType().name())
				.generationTime(DateFormatterHelper.formatDateTime(amendment.getCreatedOn(),
						DateFormatType.AMENDMENT_EMAIL_FORMAT))
				.bookingStatus(order.getStatus().name()).amendmentStatus(amendment.getStatus().name())
				.amendmentComment(amendment.getAdditionalInfo().getAgentRemarks()).agentId(user.getUserId())
				.agentName(user.getName()).toEmailId(toEmailId).key(templateKey.name()).build();
		msgSrvCommunicator.sendMail(amendmentAttributes);

	}

	public void sendBookingSms(Order order, List<DbHotelOrderItem> items, SmsTemplateKey templateKey,
			List<String> recipients) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("bookingId", order.getBookingId());
				User bookingUser = userService.getUserFromCache(order.getBookingUserId());
				attributes.put("contact", bookingUser.getMobile());

				HotelInfo hInfo = DbHotelOrderItemListToHotelInfo.builder().itemList(items).build().convert();
				LocalDate checkInDate = null, checkOutDate = null;
				long nights = 0L;
				long deadlineDateHours = 0L;
				Option option = null;
				if (hInfo != null && CollectionUtils.isNotEmpty(hInfo.getOptions())) {
					option = hInfo.getOptions().get(0);
					RoomInfo room = option.getRoomInfos().get(0);
					checkInDate = room.getCheckInDate();
					checkOutDate = room.getCheckOutDate();
					deadlineDateHours = Duration.between(LocalDateTime.now(), room.getDeadlineDateTime()).toHours();
					nights = ChronoUnit.DAYS.between(checkInDate, checkOutDate);
				}
				Set<String> passengers = new HashSet<>();
				hInfo.getOptions().get(0).getRoomInfos().forEach(roomInfo -> {
					passengers.addAll(
							roomInfo.getTravellerInfo().stream().map(t -> t.getFullName()).collect(Collectors.toSet()));
				});


				String leadPax = HotelBookingUtils.getLeadPax(hInfo.getOptions().get(0));
				if (passengers.size() - 1 > 0) {
					String paxCount = String.valueOf(passengers.size() - 1);
					attributes.put("paxcount", "& " + paxCount);
				}
				String hotelAddress = HotelBookingUtils.getHotelAddress(hInfo);
				attributes.put("bookingUser", bookingUser.getName());
				attributes.put("hotelBookingRef", hInfo.getMiscInfo().getHotelBookingReference());
				attributes.put("deadlineDateHours", String.valueOf(deadlineDateHours));
				attributes.put("passengers", Joiner.on(",").join(passengers));
				attributes.put("hname", hInfo.getName());
				attributes.put("checkindate",
						DateFormatterHelper.formatDate(checkInDate, DateFormatType.DEFAULT_FORMAT));
				attributes.put("checkoutdate",
						DateFormatterHelper.formatDate(checkOutDate, DateFormatType.DEFAULT_FORMAT));
				attributes.put("totalnights", String.valueOf(nights));
				attributes.put("leadpax", leadPax);
				attributes.put("hoteladdress", hotelAddress);
				SmsAttributes smsAttributes = SmsAttributes.builder().key(templateKey.name()).attributes(attributes)
						.partnerId(order.getPartnerId()).role(bookingUser.getRole()).build();
				smsAttributes.setRecipientNumbers(CollectionUtils.isEmpty(recipients)
						? OrderUtils.getToReceipientNumbers(order, bookingUser, null)
						: recipients);
				return smsAttributes;
			}
		};
		msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());
	}

	public List<HotelMessageRoomInfo> getRoomInfoList(Option option) {
		if (option != null) {
			int i = 1;
			List<HotelMessageRoomInfo> roomInfoList = new ArrayList<>();
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				roomInfoList.add(HotelMessageRoomInfo.builder().roomNumber(i).roomInfo(roomInfo).build());
				i++;
			}
			return roomInfoList;
		}
		return null;
	}

	private void setDisputeInfo(HotelMessageAttributes msgAttr, Order order, List<DbHotelOrderItem> items) {

		DbHotelOrderItem firstHotelOrderItem = items.get(0);
		List<OrderDisputeInfo> disputeInfos = order.getAdditionalInfo().getDisputeInfo();

		if (CollectionUtils.isNotEmpty(disputeInfos)) {

			OrderDisputeInfo disputeInfo = disputeInfos.get(disputeInfos.size() - 1);
			msgAttr.setSupplierName(firstHotelOrderItem.getSupplierId());
			msgAttr.setDisputeReason(disputeInfo.getReason());
			msgAttr.setBookingRefNo(firstHotelOrderItem.getAdditionalInfo().getSupplierBookingReference());
		}
	}

	public List<String> getCancellationPolicyList(Option option) {

		if (option != null) {
			List<String> cancellationPolicyList = new ArrayList<>();
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm");
			HotelCancellationPolicy cp = option.getCancellationPolicy();
			if (cp.getPenalyDetails() != null) {
				for (PenaltyDetails pd : cp.getPenalyDetails()) {
					String str = StringUtils.join("Charges - INR ", pd.getPenaltyAmount(),
							"/- applicable if cancelled between ", formatter.format(pd.getFromDate()), " , ",
							formatter.format(pd.getToDate()));
					cancellationPolicyList.add(str);
				}
				return cancellationPolicyList;
			} 
		}
		return null;
	}

	public HotelMessagePriceInfo getHotelMessagePriceInfo(Option option) {
		if (option != null) {
			double optionBasePrice = getOptionBasePrice(option);
			double taxAndFees = getTaxesAndFees(option);
			double totalPrice = optionBasePrice + taxAndFees;
			double discount = getDiscount(option);
			HotelMessagePriceInfo priceInfo = HotelMessagePriceInfo.builder()
					.basePrice(String.format("%.2f", optionBasePrice)).totalTax(String.format("%.2f", taxAndFees))
					.totalAmount(String.format("%.2f", totalPrice - discount)).build();
			if (discount > 0) {
				priceInfo.setDiscount(String.format("%.2f", discount));
			}
			return priceInfo;
		}
		return null;
	}

	public double getDiscount(Option option) {
		double discount = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			Map<HotelFareComponent, Double> totalFareComponents =
					roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF);
			discount += totalFareComponents.getOrDefault(HotelFareComponent.DS, 0.0);
		}
		return discount;
	}

	public double getTaxesAndFees(Option option) {

		double taxesAndFees = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			Map<HotelFareComponent, Double> totalFareComponents =
					roomInfo.getTotalAddlFareComponents().get(HotelFareComponent.TAF);
			taxesAndFees += totalFareComponents.getOrDefault(HotelFareComponent.MF, 0.0)
					+ totalFareComponents.getOrDefault(HotelFareComponent.MFT, 0.0)
					+ totalFareComponents.getOrDefault(HotelFareComponent.MU, 0.0);


		}
		return taxesAndFees;
	}

	public double getOptionBasePrice(Option option) {

		double optionBasePrice = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			optionBasePrice += (roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
					+ roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SP, 0.0));
		}
		return optionBasePrice;

	}

	public void sendEmail(HotelMessageAttributes msgAttr) {
		msgSrvCommunicator.sendMail(msgAttr);
	}


	public String getSSR(List<DbHotelOrderItem> items) {

		StringBuilder ssr = new StringBuilder();
		RoomInfo roomInfo = items.get(0).getRoomInfo();
		List<RoomSSR> roomSSRList = roomInfo.getRoomSSR();
		if (!CollectionUtils.isEmpty(roomSSRList)) {
			for (RoomSSR roomSSR : roomSSRList) {
				ssr.append(roomSSR.getRequestMsg());
			}
		}
		return ssr.toString();
	}
}
