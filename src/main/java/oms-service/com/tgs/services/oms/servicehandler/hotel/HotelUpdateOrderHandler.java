package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;

import com.tgs.services.oms.BookingResyncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderAction;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.restmodel.BookingDetailRequest;
import com.tgs.services.oms.restmodel.BookingDetailResponse;


import lombok.Setter;

@Service
public class HotelUpdateOrderHandler extends ServiceHandler<Order, BookingDetailResponse> {

	@Autowired
	private OrderService orderService;

	@Autowired
	private HotelOrderActionValidator actionValidator;

	@Autowired
	private HotelBookingAbortHandler bookingAbortHandler;
	
	@Autowired
	HotelBookingResyncHandler bookingResyncHandler;

	@Setter
	private OrderAction action;

	private Order order;

	@Override
	public void beforeProcess() throws Exception {

		order = orderService.findByBookingId(request.getBookingId()).toDomain();
		order.setReason(request.getReason());
		order.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order));
		validateAction();

	}

	private void validateAction() {
		// validate actions here
		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order).contains(action))
			throw new CustomGeneralException(SystemError.ORDER_INVALID_ACTION);

	}

	@Override
	public void process() throws Exception {

		if (action.equals(OrderAction.ASSIGN)) {

			order.getAdditionalInfo().setAssignedUserId(request.getAdditionalInfo().getAssignedUserId());
			order.getAdditionalInfo().setAssignedTime(LocalDateTime.now());
			orderService.save(DbOrder.create(order));
		}
		if (action.equals(OrderAction.ASSIGNME)) {
			/**
			 * Admin can assign the cart to himself, even if it was assigned to someone else
			 */
			if (StringUtils.isEmpty(order.getAdditionalInfo().getAssignedUserId())
					|| SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.ADMIN)) {
				order.getAdditionalInfo().setAssignedUserId(SystemContextHolder.getContextData().getUser().getUserId());
				order.getAdditionalInfo().setAssignedTime(LocalDateTime.now());
				orderService.save(DbOrder.create(order));
			} else {
				throw new CustomGeneralException(SystemError.ORDER_ALREADY_ASSIGNED);
			}
		}

		if (action.equals(OrderAction.ABORT)) {
			bookingAbortHandler.abortBooking(order.getBookingId());
			order.setStatus(OrderStatus.ABORTED);
		}

		if (action.equals(OrderAction.RESYNC)) {
			bookingResyncHandler.initData(BookingDetailRequest.builder().bookingId(order.getBookingId()).build()
					, new BookingResyncResponse());
			bookingResyncHandler.getResponse();
		}

		
		order.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), order));
		response.setOrder(order);

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
