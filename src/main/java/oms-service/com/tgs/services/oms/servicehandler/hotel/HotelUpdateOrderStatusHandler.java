package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.BookingResyncResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.restmodel.BookingDetailRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelUpdateOrderStatusHandler extends ServiceHandler<OrderFilter, BaseResponse> {
	final private static List<OrderStatus> STATUSES = Arrays.asList(OrderStatus.PENDING);

	@Autowired
	private OrderService orderService;

	@Autowired
	HotelBookingResyncHandler bookingResyncHandler;

	@Override
	public void beforeProcess() throws Exception {
		final LocalDateTime now = LocalDateTime.now();
		if (request.getCreatedOnAfterDateTime() == null && request.getCreatedOnAfterDate() == null) {
			request.setCreatedOnAfterDateTime(now.minusDays(30));
		}
		if (request.getCreatedOnBeforeDateTime() == null && request.getCreatedOnBeforeDate() == null) {
			request.setCreatedOnBeforeDateTime(now.minusMinutes(5));
		}
		request.setStatuses(STATUSES);
		request.setProducts(Arrays.asList(OrderType.HOTEL));
	}

	@Override
	public void process() throws Exception {
		List<DbOrder> orders = orderService.findAll(request);
		log.info("Found {} eligible orders to change status", orders.size());
		for (DbOrder order : orders) {
			log.info("Current order status for booking Id {} is {} ", order.getBookingId(), order.getStatus());
			if (order.getStatus().equals(OrderStatus.PENDING.getCode())) {
				try {
					bookingResyncHandler.initData(
							BookingDetailRequest.builder().bookingId(order.getBookingId()).build(),
							new BookingResyncResponse());
					bookingResyncHandler.getResponse();
				} catch (CustomGeneralException e) {
					log.error("Unable to update order status for booking Id {} to {} ", order.getBookingId(),
							order.getStatus(), e);
				}
			}
			DbOrder updatedOrder = orderService.findByBookingId(order.getBookingId());
			log.info("Changing order status for booking Id {} to {} ", updatedOrder.getBookingId(),
					updatedOrder.getStatus());
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}
}
