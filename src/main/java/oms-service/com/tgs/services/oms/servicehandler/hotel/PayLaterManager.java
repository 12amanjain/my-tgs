package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItemFilter;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PayLaterManager {

	@Autowired
	HotelOrderItemManager itemManager;

	@Autowired
	HotelBookingCancellationHandler bookingCancellationHandler;

	@Autowired
	OrderService orderService;

	@Autowired
	HotelOrderMessageHandler orderMessageHandler;

	public void processCancellationJob() {

		log.info("Cancelling unconfirmed boookings");
		LocalDateTime currentTime = LocalDateTime.now();
		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.ON_HOLD))
				.hotelItemFilter(HotelOrderItemFilter.builder()
						.deadlineDateBeforeDateTime(currentTime)
						.deadlineDateAfterDateTime(currentTime.minusHours(7 * 24)).build())
				.build();
		List<DbHotelOrder> hotelOrderList = itemManager.getHotelOrderList(orderFilter);
		cancelBookingIfAllowed(hotelOrderList);
	}

	private void cancelBookingIfAllowed(List<DbHotelOrder> hotelOrderList) {
		if (CollectionUtils.isEmpty(hotelOrderList)) {
			log.info("There are no orders found which is pending for cancellation");
			return;
		}
		for (DbHotelOrder hotelOrder : hotelOrderList) {
			log.info("Cancelling hotelorder for bookingId {}", hotelOrder.getOrder().getBookingId());
			cancelSupplierBooking(hotelOrder);
		}
	}

	private void cancelSupplierBooking(DbHotelOrder hotelOrder) {
		try {
			bookingCancellationHandler.cancelBooking(hotelOrder.getOrder().getBookingId(), true);
		} catch (Exception e) {
			log.error("Unable to cancel booking from supplier for bookingId {}", hotelOrder.getOrder().getBookingId(),
					e);
		}
	}

	public void processNotificationJob(String hoursFrom, String hoursTo) {
		log.info("Processing notificationjob hoursFrom {}, hoursTo {}", hoursFrom, hoursTo);
		LocalDateTime currentTime = LocalDateTime.now();
		OrderFilter orderFilter = OrderFilter.builder().statuses(Arrays.asList(OrderStatus.ON_HOLD))
				.hotelItemFilter(HotelOrderItemFilter.builder()
						.deadlineDateBeforeDateTime(currentTime.plusHours(Long.valueOf(hoursTo)))
						.deadlineDateAfterDateTime(currentTime.plusHours(Long.valueOf(hoursFrom))).build())
				.build();
		List<DbHotelOrder> hotelOrderList = itemManager.getHotelOrderList(orderFilter);
		if (CollectionUtils.isEmpty(hotelOrderList)) {
			log.info("There is no order for which notification is pending");
			return;
		}
		for (DbHotelOrder hotelOrder : hotelOrderList) {
			Order order = hotelOrder.getOrder().toDomain();
			log.info("Sending reminder notification for bookingId {}", order.getBookingId());
			orderMessageHandler.sendBookingEmail(order, hotelOrder.getOrderItems(),
					EmailTemplateKey.HOTEL_BOOKING_CANCELLATION_DEADLINE_EMAIL, null);
			orderMessageHandler.sendBookingSms(order, hotelOrder.getOrderItems(),
					SmsTemplateKey.HOTEL_BOOKING_CANCELLATION_DEADLINE_SMS, null);

		}
	}


}
