package com.tgs.services.oms.servicehandler.hotel;


import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.filters.NoteFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.NoteType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.manager.hotel.HotelPostBookingManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.hotel.ProcessHotelAmendmentRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ProcessHotelAmendmentHandler extends ServiceHandler<ProcessHotelAmendmentRequest, AmendmentResponse> {

	@Autowired
	private AmendmentService service;

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Autowired
	HotelModifyBookingManager modifyBookingManager;

	@Autowired
	OrderManager orderManager;

	@Autowired
	HotelAmendmentManager amendmentManager;
	
	@Autowired
	HotelPostBookingManager manualOrderManager;
	
	@Autowired
	HotelOrderItemManager itemManager;

	@Override
	public void beforeProcess() {}

	private void validate(Amendment amendment) {

		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment)
				.contains(AmendmentAction.PROCESS))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
		manualOrderManager.updateNewHotelInfoInRequestForModifyOrder(request.getModifiedInfo());
		
	}

	@Transactional
	@Override
	public void process() {

		Amendment amendment = service.findByAmendmentId(request.getAmendmentId());
		validate(amendment);
		HotelOrder modifiedHotelOrder = getModifiedHotelOrder();
		
		HotelInfo modifiedHotelInfoWithDeleted = DbHotelOrderItemListToHotelInfo.builder()
				.itemList(new DbHotelOrderItem().toDbList(modifiedHotelOrder.getItems())).build().convert();
		
		amendmentManager.updateCommonAmounts(modifiedHotelOrder.getItems(), amendment);
		Double differentialPayment = getDifferentialPayment(amendment, modifiedHotelInfoWithDeleted);
		amendment.getAdditionalInfo().setOrderDiffAmount(differentialPayment);
		amendment.setAmendmentAmount();
		updateModifiedHotelInfoInAmendment(amendment,modifiedHotelInfoWithDeleted);
		if (StringUtils.isNotBlank(request.getNote()))
			updateNote(amendment);
		amendment.setStatus(AmendmentStatus.PROCESSING);
		service.save(amendment);
		amendment.setActionList(actionValidator.validActions(
				SystemContextHolder.getContextData().getUser(), amendment));
		response.setAmendmentItems(Arrays.asList(amendment));
		response.setDifferentialPayment(differentialPayment);
	}
	

	private void updateModifiedHotelInfoInAmendment(Amendment amendment, HotelInfo modifiedHotelInfo) {
		
		modifiedHotelInfo.getOptions().get(0).getRoomInfos().forEach((roomInfo) 
    			-> BaseHotelUtils.flattenFareComponents(roomInfo));
		log.debug("Modified Hotel is {}", GsonUtils.getGson().toJson(modifiedHotelInfo));
		amendment.getModifiedInfo().setHotelInfo(modifiedHotelInfo);
		
	}

	private Double getDifferentialPayment(Amendment amendment, HotelInfo modifiedHotelInfo) {
		
		Amendment dummyAmendment = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(amendment), Amendment.class);
		amendmentManager.setCurrentOrderSnapshotAfterProcessing(dummyAmendment, modifiedHotelInfo);
		amendmentManager.setPreviousOrderSnapshotAfterProcessing(dummyAmendment, DbHotelOrderItemListToHotelInfo.builder()
				.itemList(itemManager.getItemList(amendment.getBookingId())).build().convert());
		
		return amendmentManager.getAmendmentSnapshot(dummyAmendment)
				.getOptions().get(0).getRoomInfos().stream()
				.mapToDouble((roomInfo) -> roomInfo.getTotalPrice()).sum();
		
	}

	private HotelOrder getModifiedHotelOrder() {
		
		BaseHotelUtils.updateTotalFareComponents(request.getModifiedInfo().getHInfo());
		log.debug("Modified Hotel After manualOrderMAnager is {}", GsonUtils.getGson().toJson(request.getModifiedInfo().getHInfo()));
		HotelOrder modifiedHotelOrder = modifyBookingManager
				.getHotelOrderFromUpdatedHotelInfo(request.getModifiedInfo());
		
		return modifiedHotelOrder;
		
	}

	private void updateNote(Amendment amendment) {
		
		Note note = Note.builder().bookingId(amendment.getBookingId()).amendmentId(amendment.getAmendmentId())
				.noteType(NoteType.AMENDMENT).noteMessage(request.getNote()).userId(amendment.getAssignedUserId())
				.build();

		List<Note> noteList =
				gmsCommunicator.getNotes(NoteFilter.builder().amendmentId(amendment.getAmendmentId()).build());
		if (!noteList.isEmpty())
			note.setId(noteList.get(0).getId());
		gmsCommunicator.addNote(note);
		
	}

	@Override
	public void afterProcess() {}
}
