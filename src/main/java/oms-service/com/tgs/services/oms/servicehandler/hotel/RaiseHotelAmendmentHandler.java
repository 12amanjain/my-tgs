package com.tgs.services.oms.servicehandler.hotel;


import static com.tgs.services.base.helper.SystemError.ORDER_NOT_FOUND;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAdditionalInfo;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.jparepository.OrderService;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.AmendmentResponse;
import com.tgs.services.oms.restmodel.hotel.HotelAmendmentRequest;

@Service
public class RaiseHotelAmendmentHandler extends ServiceHandler<HotelAmendmentRequest, AmendmentResponse> {

	@Autowired
	private HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	private OrderService orderService;

	@Autowired
	private AmendmentService amendmendService;

	@Autowired
	private HotelAmendmentManager hotelAmendmentManager;

	@Autowired
	private HotelOrderMessageHandler messageHandler;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {

		Order originalOrder = orderService.findByBookingId(request.getBookingId()).toDomain();
		if (originalOrder == null)
			throw new CustomGeneralException(SystemError.ORDER_NOT_FOUND);
		UserServiceHelper.checkAndReturnAllowedUserId(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId(),
				Arrays.asList(originalOrder.getBookingUserId()));
		List<DbHotelOrderItem> orderItemList = hotelOrderItemManager.getItemList(request.getBookingId());
		if (CollectionUtils.isEmpty(orderItemList))
			throw new CustomGeneralException(ORDER_NOT_FOUND);

		boolean isAmendmentTypeValidForOrderItem =
				hotelAmendmentManager.checkIfAmendmentTypeValid(request.getType(), orderItemList);
		if (!isAmendmentTypeValidForOrderItem)
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);

		Set<String> roomKeys = request.getRoomKeys() != null ? request.getRoomKeys() : new HashSet<>();

		hotelAmendmentManager.checkAmendStatus(request.getBookingId(), roomKeys);

		Amendment amendment = createAmendment(originalOrder);
		amendment = amendmendService.save(amendment);
		messageHandler.sendAmendmentEmail(originalOrder, orderItemList, EmailTemplateKey.HOTEL_AMENDMENT_EMAIL, null,
				amendment);
		response.getAmendmentItems().add(amendment);
	}

	@Override
	public void afterProcess() throws Exception {}


	private Amendment createAmendment(Order order) {

		String amendmentType = request.getType().getCode();
		Amendment amendment = Amendment.builder()
				.amendmentId(ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.AMENDMENT).build()))
				.loggedInUserId(SystemContextHolder.getContextData().getUser().getUserId())
				.bookingId(order.getBookingId()).bookingUserId(order.getBookingUserId())
				.status(AmendmentStatus.REQUESTED).amendmentType(AmendmentType.getAmendmentType(amendmentType))
				.orderType(OrderType.HOTEL).build();
		setAdditionalInfo(amendment);
		return amendment;
	}

	private void setAdditionalInfo(Amendment amendment) {

		AmendmentAdditionalInfo additionalInfo = new AmendmentAdditionalInfo();
		additionalInfo.setAgentRemarks(request.getRemarks());
		additionalInfo.getHotelAdditionalInfo().setRoomKeys(request.getRoomKeys());
		amendment.setAdditionalInfo(additionalInfo);
	}

}
