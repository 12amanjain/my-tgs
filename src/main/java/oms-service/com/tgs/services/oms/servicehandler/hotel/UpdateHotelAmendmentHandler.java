package com.tgs.services.oms.servicehandler.hotel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.AmendmentFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.Amendments.AmendmentActionValidator;
import com.tgs.services.oms.Amendments.AmendmentHelper;
import com.tgs.services.oms.Amendments.HotelAmendmentManager;
import com.tgs.services.oms.datamodel.HotelOrder;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentAction;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.UpdateAmendmentRequest;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.oms.jparepository.AmendmentService;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.oms.manager.hotel.HotelModifyBookingManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.mapper.DbHotelOrderItemListToHotelInfo;
import com.tgs.services.oms.restmodel.hotel.HotelPostBookingBaseRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.ruleengine.RefundOutput;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;

@Service

public class UpdateHotelAmendmentHandler {

	@Autowired
	private AmendmentActionValidator actionValidator;

	@Autowired
	private AmendmentService service;

	@Autowired
	private PaymentServiceCommunicator paymentServiceCommunicator;

	private Amendment amendment;

	private boolean paymentRequired;

	@Setter
	private AmendmentAction action;

	@Autowired
	private HotelModifyBookingManager modifyBookingManager;

	@Autowired
	private OrderManager orderManager;

	@Autowired
	private HotelOrderItemManager itemManager;

	@Autowired
	HotelAmendmentManager amendmentManager;

	@Autowired
	private UserServiceCommunicator userService;


	private static final short AMD_ASSIGNMENT_CAP = 3;

	private void validateAction() {

		if (amendment.getStatus().equals(AmendmentStatus.SUCCESS)
				|| amendment.getStatus().equals(AmendmentStatus.REJECTED))
			throw new CustomGeneralException(SystemError.AMENDMENT_NOT_UPDATABLE);

		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment).contains(action))
			throw new CustomGeneralException(SystemError.AMENDMENT_INVALID_ACTION);
	}

	public Amendment process(UpdateAmendmentRequest request) throws Exception {

		amendment = service.findByAmendmentId(request.getAmendmentId());
		validateAction();
		paymentRequired = !AmendmentHelper.isQuotation(amendment)
				&& !amendment.getStatus().equals(AmendmentStatus.PAYMENT_SUCCESS);

		if (!StringUtils.isEmpty(request.getFinishingNotes())) {
			amendment.getAdditionalInfo().setFinishingNotes(request.getFinishingNotes());
		}

		if (action.equals(AmendmentAction.ASSIGN)) {
			assign(request.getAssignedUserId());
		}

		if (action.equals(AmendmentAction.ASSIGNME)) {
			assign(SystemContextHolder.getContextData().getUser().getUserId());
		}

		if (action.equals(AmendmentAction.ABORT)) {
			amendment.setStatus(AmendmentStatus.REJECTED);
			AmendmentHelper.updateAssignments(null, amendment);
		}

		if (action.equals(AmendmentAction.PENDING_WITH_SUPPLIER)) {
			amendment.setStatus(AmendmentStatus.PENDING_WITH_SUPPLIER);
			amendment.setAssignedUserId(null);
			AmendmentHelper.updateAssignments(null, amendment);
		}
		if (action.equals(AmendmentAction.MERGE)) {
			processPayment();
			merge();
		}
		service.save(amendment);
		amendment
				.setActionList(actionValidator.validActions(SystemContextHolder.getContextData().getUser(), amendment));
		return amendment;
	}

	@Transactional
	private void merge() {

		UpdateDbOrders();
		amendment.setStatus(AmendmentStatus.SUCCESS);
		AmendmentHelper.updateAssignments(null, amendment);
		service.save(amendment);
	}

	private void assign(String userId) {

		List<Amendment> amendments = service.search(AmendmentFilter.builder()
				.assignedUserIdIn(Collections.singletonList(userId)).statusIn(AmendmentHelper.assignedStatuses())
				.orderTypeIn(Collections.singletonList(OrderType.HOTEL)).build());

		if (amendments.size() >= AMD_ASSIGNMENT_CAP) {
			throw new CustomGeneralException(SystemError.AMENDMENT_ASSIGNMENT_LIMIT);
		}

		amendment.setAssignedUserId(userId);
		amendment.setAssignedOn(LocalDateTime.now());
		if (amendment.getStatus().equals(AmendmentStatus.REQUESTED))
			amendment.setStatus(AmendmentStatus.ASSIGNED);
		else
			amendment.setStatus(AmendmentStatus.PROCESSING);
		AmendmentHelper.updateAssignments(userId, amendment);
	}

	@Transactional
	private void processPayment() {

		if (!paymentRequired || amendment.getAmount() == 0)
			return;
		boolean refund = amendment.getAmount() < 0;
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		try {
			PaymentAdditionalInfo additionalInfo =
					PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
			PaymentRequest paymentRequest =
					WalletPaymentRequest.builder().build().setAmount(Math.abs(amendment.getAmount()))
							.setProduct(Product.getProductFromId(amendment.getBookingId()))
							.setAmendmentId(amendment.getAmendmentId())
							.setOpType(refund ? PaymentOpType.CREDIT : PaymentOpType.DEBIT)
							.setPayUserId(amendment.getBookingUserId()).setRefId(amendment.getBookingId())
							.setTransactionType(
									refund ? PaymentTransactionType.REFUND : PaymentTransactionType.PAID_FOR_AMENDMENT)
							.setAdditionalInfo(additionalInfo);
			if (refund) {
				evalRefundMedium(paymentRequest);
			}
			paymentRequestList.add(paymentRequest);
			if (amendment.getAdditionalInfo().isRecallCommission()
					&& ObjectUtils.firstNonNull(amendment.getAdditionalInfo().getTotalCommissionRecalled(), 0d) > 0) {
				paymentRequestList.add(createCommissionRecallRequest());
			}
			paymentServiceCommunicator.doPaymentsUsingPaymentRequests(paymentRequestList);
			amendment.setStatus(AmendmentStatus.PAYMENT_SUCCESS);
		} catch (Exception ex) {
			amendment.setStatus(AmendmentStatus.PAYMENT_FAILED);
			throw ex;
		} finally {
			service.save(amendment);
		}
	}

	private PaymentRequest createCommissionRecallRequest() {

		Double amount = amendment.getAdditionalInfo().getTotalCommissionRecalled();
		Double tds = null;
		if (amendment.getAdditionalInfo().isReturnTds()) {
			tds = amendment.getAdditionalInfo().getTdsToReturn();
			amount -= tds;
			tds = -1 * tds;
		}
		User payUser = userService.getUserFromCache(amendment.getBookingUserId());
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build();
		return WalletPaymentRequest.builder().build().setAmount(amount)
				.setTds(tds != null ? BigDecimal.valueOf(tds) : null)
				.setProduct(Product.getProductFromId(amendment.getBookingId()))
				.setAmendmentId(amendment.getAmendmentId()).setOpType(PaymentOpType.DEBIT)
				.setPayUserId(amendment.getBookingUserId()).setRefId(amendment.getBookingId())
				.setPartnerId(payUser.getPartnerId()).setAdditionalInfo(additionalInfo)
				.setTransactionType(PaymentTransactionType.COMMISSION);
	}

	private void evalRefundMedium(PaymentRequest paymentRequest) {

		PaymentMedium medium = PaymentMedium.FUND_HANDLER;
		List<Payment> paymentList = paymentServiceCommunicator.search(PaymentFilter.builder()
				.refId(amendment.getBookingId()).type(PaymentTransactionType.PAID_FOR_ORDER).build());

		paymentList.sort(Comparator.comparing(Payment::getCreatedOn));
		PaymentMedium bookingMedium = paymentList.get(0).getPaymentMedium();
		BigDecimal extMediumAmount = Payment.sum(paymentList.stream()
				.filter(p -> p.getPaymentMedium().isExternalPaymentMedium()).collect(Collectors.toList()));
		PaymentFact fact = PaymentFact.builder().medium(bookingMedium).build();
		List<PaymentConfigurationRule> rules =
				paymentServiceCommunicator.getApplicableRulesOutput(fact, PaymentRuleType.REFUND, bookingMedium);
		if (!CollectionUtils.isEmpty(rules)) {
			medium = ((RefundOutput) rules.get(0).getOutput()).getMedium();
		}
		if (medium.isExternalPaymentMedium() && extMediumAmount.compareTo(paymentRequest.getAmount()) < 0) {
			medium = PaymentMedium.FUND_HANDLER;
		}
		if (!medium.equals(PaymentMedium.FUND_HANDLER)) {
			paymentRequest.setPaymentMedium(medium);
		}
	}

	private void UpdateDbOrders() {

		Order oldOrder = orderManager.findByBookingId(amendment.getBookingId(), null);
		HotelPostBookingBaseRequest request = new HotelPostBookingBaseRequest();
		request.setBookingId(amendment.getBookingId());

		HotelInfo modifiedHotelInfo = amendment.getModifiedInfo().getHotelInfo();
		BaseHotelUtils.updateTotalFareComponents(modifiedHotelInfo);
		request.setHInfo(modifiedHotelInfo);
		request.setDeliveryInfo(oldOrder.getDeliveryInfo());
		HotelOrder modifiedHotelOrder = modifyBookingManager.getHotelOrderFromUpdatedHotelInfo(request);

		List<HotelOrderItem> modifiedItemsFiltered =
				modifiedHotelOrder.getItems().stream().collect(Collectors.toList());

		List<DbHotelOrderItem> oldDbOrderItems = itemManager.getItemList(amendment.getBookingId());
		HotelInfo oldHotelSnapshot =
				DbHotelOrderItemListToHotelInfo.builder().itemList(oldDbOrderItems).build().convert();
		amendmentManager.setPreviousOrderSnapshotAfterProcessing(amendment, oldHotelSnapshot);
		HotelInfo updatedHotelSnapshot = DbHotelOrderItemListToHotelInfo.builder()
				.itemList(new DbHotelOrderItem().toDbList(modifiedItemsFiltered)).build().convert();
		amendmentManager.setCurrentOrderSnapshotAfterProcessing(amendment, updatedHotelSnapshot);
		Order order = modifiedHotelOrder.getOrder();
		List<DbHotelOrderItem> updatedDbOrderItems = new DbHotelOrderItem().toDbList(modifiedHotelOrder.getItems());

		itemManager.save(updatedDbOrderItems, order, null);
	}

}
