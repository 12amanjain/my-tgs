package com.tgs.services.oms.servicehandler.hotel;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelInfo;

import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFilter;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItemFilter;
import com.tgs.services.oms.dbmodel.DbHotelOrder;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.restmodel.hotel.UpdateOrderRequest;

@Service
public class UpdateSupplierHotelConfirmationUpdateHandler extends ServiceHandler<UpdateOrderRequest, BaseResponse> {

	@Autowired
	HotelOrderItemManager hotelOrderItemManager;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	OrderServiceCommunicator orderCommunicator;

	@Autowired
	HotelOrderItemCommunicator orderItemCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		if (request != null && request.getCreatedAfterDate() == null) {
			request.setCreatedAfterDate(LocalDate.now().minusDays(90));
		}
	}

	@Override
	public void process() throws Exception {
		if (request != null && !CollectionUtils.sizeIsEmpty(request.getBookingUpdateRequest())) {
			request.getBookingUpdateRequest().forEach(req -> {
				Order order = orderCommunicator.findByBookingId(req.getBookingId());
				HotelInfo hotelInfo = orderItemCommunicator.getHotelInfo(req.getBookingId());
				hmsCommunicator.updateHotelConfirmationNumber(hotelInfo, order);
			});
			return;
		}
		List<DbHotelOrder> hotelOrders=new ArrayList<>();
		OrderFilter orderFilter = OrderFilter.builder().build();
		orderFilter.setProducts(Arrays.asList(OrderType.HOTEL));
		orderFilter.setStatuses(Arrays.asList(OrderStatus.SUCCESS));
		orderFilter.setCreatedOnAfterDate(request.getCreatedAfterDate());
		orderFilter.setHotelItemFilter(HotelOrderItemFilter.builder().supplierIds(Arrays.asList("AGODA")).build());
		hotelOrders = hotelOrderItemManager.getHotelOrderList(orderFilter).stream()
				.filter(order -> ObjectUtils
						.isEmpty(order.getOrderItems().get(0).getAdditionalInfo().getHotelBookingReference()))
				.collect(Collectors.toList());

		hotelOrders.forEach(hotelOrder -> {
			Order order = orderCommunicator.findByBookingId(hotelOrder.getOrder().getBookingId());
			HotelInfo hotelInfo = orderItemCommunicator.getHotelInfo(hotelOrder.getOrder().getBookingId());
			hmsCommunicator.updateHotelConfirmationNumber(hotelInfo, order);
		});
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

}
