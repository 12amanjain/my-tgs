package com.tgs.services.oms.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import com.google.common.base.Joiner;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.APIPartnerMessageConfig;
import com.tgs.services.base.enums.AlertType;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.MessageMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.msg.restmodel.MessageRequest;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.ruleengine.IRule;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.datamodel.DuplicateBookingAlert;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelGeneralPurposeOutput;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.manager.OrderManager;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.CachingLayerError;
import com.tgs.utils.exception.CachingLayerException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class OrderUtils {

	private static GeneralCachingCommunicator cachingService;

	private static GeneralServiceCommunicator gsCommunicator;

	private static UserServiceCommunicator userService;

	private static PaymentServiceCommunicator psCommunicator;

	public static void init(GeneralServiceCommunicator gsCommunicator, GeneralCachingCommunicator cachingService,
			PaymentServiceCommunicator psCommunicator, UserServiceCommunicator userService) {
		OrderUtils.gsCommunicator = gsCommunicator;
		OrderUtils.cachingService = cachingService;
		OrderUtils.userService = userService;
		OrderUtils.psCommunicator = psCommunicator;
	}

	/**
	 * It fetches boolean from general-purpose rule to checks whether duplicate booking is disabled.
	 *
	 * @return <code>true</code> if duplicate booking disabled
	 */
	public static boolean isDuplicateBookingCheckEnabled(IRule rule) {
		if (rule instanceof AirConfiguratorInfo) {
			AirGeneralPurposeOutput output;
			if (rule != null && (output = (AirGeneralPurposeOutput) rule.getOutput()) != null) {
				return BooleanUtils.isTrue(output.getDuplicateBookingEnabled());
			}
		} else if (rule instanceof HotelConfiguratorInfo) {
			HotelGeneralPurposeOutput output;
			if (rule != null && (output = (HotelGeneralPurposeOutput) rule.getOutput()) != null) {
				return BooleanUtils.isTrue(output.getDuplicateBookingEnabled());
			}
		}
		return true;
	}

	/**
	 * It attempts to store/write bookingId for all the supplied keys, using <i>Write-Only</i> update policy.<br>
	 * <br>
	 * If the operation fails for any key (which means key already exists), it will fetch the bookingId for that key and
	 * find an order/booking corresponding to it. Then it will check whether the booking was successful using supplied
	 * list of failure statuses.<br>
	 * <br>
	 * If the booking was a successful one, exception is thrown and keys stored previously in this loop will be deleted
	 * because they were new keys and must fail along with failed key. Otherwise, it will update the bookingId for the
	 * current key and continue.<br>
	 * <br>
	 * Returns without exception if all keys are successfully stored, i.e., no duplicates found.
	 *
	 * @param newKeysWithTtl
	 * @param bookingId
	 * @param failureStatusList
	 * @param orderManager
	 * @param cancelledKeysSupplier for supplying cancelled keys for given bookingId
	 */
	public static void checkDuplicateBooking(Map<String, Integer> newKeysWithTtl, String bookingId,
			Collection<OrderStatus> failureStatusList, OrderManager orderManager, IRule rule,
			Function<String, Collection<String>> cancelledKeysSupplier) {
		log.info("Checking duplicacy for {} keys for bookingId {}", newKeysWithTtl.size(), bookingId);
		if (!isDuplicateBookingCheckEnabled(rule)) {
			log.info("No duplicacy check because duplicate booking is allowed for bookingId {}", bookingId);
		}

		String existingDuplicateBookingId = null;
		Set<String> duplicateKeys = new HashSet<>();
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.BOOKINGID.getName(), bookingId);

		for (Map.Entry<String, Integer> entry : newKeysWithTtl.entrySet()) {
			Integer ttl = entry.getValue();
			String key = entry.getKey();
			ttl = Objects.isNull(ttl) ? 0 : ttl;
			try {
				cachingService.store(
						CacheMetaInfo.builder().createOnly(true).namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
								.set(CacheSetName.BOOKING_DUP.getName()).key(key).build(),
						binMap, false, true, ttl);
			} catch (CachingLayerException e) {
				if (e.isCausedBy(CachingLayerError.DUPLICATE_KEY)) {
					String storedBookingId = getStoredBookingId(key);
					Order storedOrder = orderManager.findByBookingId(storedBookingId, null);
					boolean isDuplicateKey = true;
					if (storedOrder == null) {
						/**
						 * If {@code true}, either a duplicate request in parallel or the booking failed with
						 * storedBookingId. Wait for at least 10 minutes.
						 */
						saveInCache(storedBookingId, key, 10 * 60);
					} else {
						Collection<String> cancelledKeys =
								(cancelledKeysSupplier == null) ? null : cancelledKeysSupplier.apply(storedBookingId);
						boolean isCancelledKey =
								CollectionUtils.isNotEmpty(cancelledKeys) && cancelledKeys.contains(key);
						if (isCancelledKey || failureStatusList.contains(storedOrder.getStatus())) {
							isDuplicateKey = false;
							saveInCache(bookingId, key, ttl);
						}
					}
					if (isDuplicateKey) {
						existingDuplicateBookingId = storedBookingId;
						duplicateKeys.add(key);
						log.info(
								"Duplicate key {} encountered for bookingId {}. BookingId already stored with the key is {}",
								key, bookingId, storedBookingId);
					}
				} else {
					deleteKeysFromCache(newKeysWithTtl.keySet(), duplicateKeys);
					throw e;
				}
			}
		}

		if (CollectionUtils.isNotEmpty(duplicateKeys)) {
			deleteKeysFromCache(newKeysWithTtl.keySet(), duplicateKeys);
			SystemContextHolder.getContextData().setAlreadyExistingBookingId(existingDuplicateBookingId);
			String errorMessage = String.join("", "Duplicate Booking. You have an existing booking under the cart ID ",
					existingDuplicateBookingId, ". Please, check your cart.");
			SystemContextHolder.getContextData()
					.addAlert(DuplicateBookingAlert.builder().type(AlertType.DUPLICATE_ORDER.name())
							.message(errorMessage).alreadyExistingBookingId(existingDuplicateBookingId).build());
			throw new CustomGeneralException(SystemError.DUPLICATE_ORDER, errorMessage);
		}
	}

	public static void saveInCache(String bookingId, String key, Integer ttl) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.BOOKINGID.getName(), bookingId);
		cachingService.store(CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.BOOKING_DUP.getName()).key(key).build(), binMap, false, true, ttl);
	}

	/**
	 * Delete keys in {@code keysToBeDeleted} that are not present in {@code keysExcluded}.
	 * 
	 * @param keysExcluded
	 * @param keysToBeDeleted
	 */
	public static void deleteKeysFromCache(Collection<String> keysToBeDeleted, Collection<String> keysExcluded) {
		keysToBeDeleted.forEach(k -> {
			// delete new successful keys from cache since booking is not allowed
			if (!keysExcluded.contains(k)) {
				OrderUtils.deleteKeyFromCache(k);
			}
		});
	}

	/**
	 * Deletes record corresponding to the given key.
	 *
	 * @param key Key for which the record has to be deleted
	 */
	public static void deleteKeyFromCache(String key) {
		cachingService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.BOOKING_DUP.getName()).key(key).build());
	}

	/**
	 * Fetches already existing value from cache corresponding to provided key.<br>
	 * <br>
	 * Note: Not meant to be used at any other place because it can throw serious exception.
	 *
	 * @param key
	 * @return
	 */
	private static String getStoredBookingId(String key) {
		Map<String, String> readBinMap = cachingService.get(
				CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
						.set(CacheSetName.BOOKING_DUP.getName()).key(key).build(),
				String.class, false, true, BinName.BOOKINGID.getName());
		return readBinMap.get(BinName.BOOKINGID.getName());
	}

	/*
	 * Receiver mail Varies as per the order status. if order status is success, hold then delivery mail should be
	 * picked. if order status is pending then booking user email should be picked.
	 */
	public static String getToEmailId(Order order, User bookingUser, MessageRequest msgRequest) {
		if (msgRequest != null && StringUtils.isNotEmpty(msgRequest.getToIds()))
			return msgRequest.getToIds();
		/**
		 * In case of import pnr , email info can be null/empty.
		 */

		if (order.getChannelType().equals(ChannelType.API)) {
			return bookingUser.getEmail();
		}
		if (order.getStatus().equals(OrderStatus.SUCCESS) && order.getDeliveryInfo() != null
				&& CollectionUtils.isNotEmpty(order.getDeliveryInfo().getEmails())) {
			return bookingUser.getEmail().equals((order.getDeliveryInfo().getEmails().get(0)))
					? order.getDeliveryInfo().getEmails().get(0)
					: Joiner.on(",").join(order.getDeliveryInfo().getEmails().get(0), bookingUser.getEmail());
		} else {
			return bookingUser.getEmail();
		}
	}

	public static List<String> getToReceipientNumbers(Order order, User bookingUser, MessageRequest msgRequest) {
		if (msgRequest != null && StringUtils.isNotEmpty(msgRequest.getToIds()))
			return Arrays.asList(msgRequest.getToIds());

		if (order.getChannelType().equals(ChannelType.API)) {
			return Arrays.asList(bookingUser.getMobile());
		}
		if (order.getStatus().equals(OrderStatus.SUCCESS) && order.getDeliveryInfo() != null
				&& CollectionUtils.isNotEmpty(order.getDeliveryInfo().getContacts())) {
			return (bookingUser.getMobile().equals((order.getDeliveryInfo().getContacts().get(0)))
					? Arrays.asList(order.getDeliveryInfo().getContacts().get(0))
					: Arrays.asList(order.getDeliveryInfo().getContacts().get(0), bookingUser.getMobile()));
		} else {
			return Arrays.asList(bookingUser.getMobile());
		}
	}


	/**
	 * This function checks that if a particular API partner like HEG, does not want to send every SMS/ EMail to its
	 * Agents because the no of such SMS/Email is huge we can restrict those specific SMS/Email. If small API Partners
	 * are okay , then we don't need to restrict them
	 */
	public static boolean isMessageSendingRequired(String key, MessageMedium messageMedium, User bookingUser,
			Order order) {
		if (order == null || bookingUser == null) {
			return false;
		}
		if (order.getChannelType().equals(ChannelType.API)) {
			GeneralBasicFact generalBasicFact = GeneralBasicFact.builder().userId(bookingUser.getUserId()).build();
			APIPartnerMessageConfig msgConfig = (APIPartnerMessageConfig) gsCommunicator
					.getConfigRule(ConfiguratorRuleType.MESSAGECONTROL, generalBasicFact);
			if (msgConfig != null) {
				if (MessageMedium.SMS.equals(messageMedium)) {
					if (BooleanUtils.isFalse(msgConfig.getIsSmsAllowed())) {
						return false;
					}
					if (msgConfig.getExcludedSmsTemplateKeys().contains(key)
							|| (!msgConfig.getAllowedSmsTemplateKeys().isEmpty()
									&& !msgConfig.getAllowedSmsTemplateKeys().contains(key)))
						return false;
					return true;
				} else if (MessageMedium.EMAIL.equals(messageMedium)) {
					if (BooleanUtils.isFalse(msgConfig.getIsEmailAllowed())) {
						return false;
					}
					if (msgConfig.getExcludedEmailTemplateKeys().contains(key)
							|| (!msgConfig.getAllowedEmailTemplateKeys().isEmpty()
									&& !msgConfig.getAllowedEmailTemplateKeys().contains(key)))
						return false;
					return true;
				}
			}
			return true;
		}
		return true;
	}


	public static void isAllowed(AreaRole areaRole) {

		List<AreaRoleMapping> areaRoleMappingList =
				userService.getAreaRoleMapByAreaRole(ApplicationArea.CART.getAreaRoles());
		Map<UserRole, Set<AreaRole>> map = AreaRoleMapping.getMapByUserRole(areaRoleMappingList);
		User user = SystemContextHolder.getContextData().getUser();

		if (user == null || map.get(user.getRole()) == null || !map.get(user.getRole()).contains(areaRole)) {
			throw new CustomGeneralException(SystemError.NOT_ALLOWED);
		}
	}

	public static boolean isPaymentAlreadyDone(Order order, PaymentTransactionType transactionType,
			BigDecimal amountToPay) {
		PaymentFilter filter = PaymentFilter.builder().refId(order.getBookingId()).type(transactionType).build();
		List<Payment> payments = psCommunicator.search(filter);
		if (CollectionUtils.isEmpty(payments)) {
			return false;
		}
		boolean isPaymentAlreadyDone = computeAndCheckIfAlreadyDone(payments, amountToPay);
		if (isPaymentAlreadyDone) {
			log.info("Duplicate entry found while checking for is payment already done for order {}", order.getId());
		}
		return isPaymentAlreadyDone;
	}

	private static boolean computeAndCheckIfAlreadyDone(List<Payment> payments, BigDecimal amountToPay) {
		Double totalAmountPaid = 0.0;
		for (Payment payment : payments) {
			totalAmountPaid += Math.abs(payment.getAmount().doubleValue());
			if (new BigDecimal(totalAmountPaid).compareTo(amountToPay) == 0) {
				return true;
			}
		}
		return false;
	}

	public static boolean isPendingRequest(String id, String type) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.ID.getName(), id);
		String key = StringUtils.join(id, "_", type);
		try {
			cachingService
					.store(CacheMetaInfo.builder().createOnly(true).namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
							.set(CacheSetName.BOOKING_DUP.getName()).key(key).build(), binMap, false, true, 180);
		} catch (CachingLayerException e) {
			if (e.isCausedBy(CachingLayerError.DUPLICATE_KEY)) {
				return true;
			}
		}
		return false;
	}

	public static void removePendingRequest(String id, String type) {
		String key = StringUtils.join(id, "_", type);
		cachingService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.BOOKING_DUP.getName()).key(key).build());
	}

}
