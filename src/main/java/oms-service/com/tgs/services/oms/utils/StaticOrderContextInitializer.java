package com.tgs.services.oms.utils;


import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.oms.manager.air.AirOrderItemManager;
import com.tgs.services.oms.manager.hotel.HotelOrderItemManager;
import com.tgs.services.oms.servicehandler.air.AirOrderMessageHandler;
import com.tgs.services.oms.utils.air.AirBookingUtils;

@Service
public class StaticOrderContextInitializer {

    @Autowired
    FMSCommunicator fmsCommunicator;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;
    
	@Autowired
	GeneralCachingCommunicator cachingService;
	
	@Autowired
	private HMSCommunicator hmsCommunicator;
	
	@Autowired
	AirOrderMessageHandler msgHandler;
	
	@Autowired
	PaymentServiceCommunicator psCommunicator;
	
	@Autowired
	UserServiceCommunicator userService;
	
    @PostConstruct
    public void init() {
        AirOrderItemManager.init(fmsCommunicator, msgHandler,gsCommunicator);
        AirBookingUtils.init(fmsCommunicator);
        OrderUtils.init(gsCommunicator, cachingService, psCommunicator, userService);
        HotelOrderItemManager.init(hmsCommunicator);
    }
}
