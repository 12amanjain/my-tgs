package com.tgs.services.oms.utils.air;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.StringJoiner;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.configurationmodel.FareBreakUpConfigOutput;
import com.tgs.services.base.datamodel.AirItemStatus;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BookingUtils;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PNRInfo;
import com.tgs.services.fms.datamodel.PNRStatus;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.ProcessedFlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TravellerStatus;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.AirGeneralPurposeOutput;
import com.tgs.services.fms.datamodel.ssr.SSRInformation;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.datamodel.air.AirOrderDetails;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.mapper.AirOrderItemToSegmentInfoMapper;
import com.tgs.services.oms.restmodel.BookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.User;

public class AirBookingUtils {

	private static FMSCommunicator fmsCommunicator;

	public static void init(FMSCommunicator fmsCommunicator) {
		AirBookingUtils.fmsCommunicator = fmsCommunicator;
	}

	public static List<SegmentInfo> convertAirOrderItemListToSegmentInfoList(List<DbAirOrderItem> itemList,
			List<SegmentInfo> segmentInfos) {
		List<SegmentInfo> segmentList = new ArrayList<>();
		SegmentInfo segmentInfo = null;
		for (DbAirOrderItem item : itemList) {
			if (CollectionUtils.isNotEmpty(segmentInfos)) {
				segmentInfo = findSegmentInfoFromOrderItem(segmentInfos, item);
			}
			segmentList.add(AirOrderItemToSegmentInfoMapper.builder().segmentInfo(segmentInfo).item(item)
					.fmsCommunicator(fmsCommunicator).build().convert());
		}
		return segmentList;
	}

	public static DbAirOrderItem findOrderItemFromSegmentInfo(SegmentInfo segmentInfo, List<DbAirOrderItem> items) {
		for (DbAirOrderItem item : items) {
			if (AirBookingUtils.isSame(item, segmentInfo)) {
				return item;
			}
		}
		return null;
	}

	public static SegmentInfo findSegmentInfoFromOrderItem(List<SegmentInfo> segmentInfos, DbAirOrderItem item) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (AirBookingUtils.isSame(item, segmentInfo)) {
				return segmentInfo;
			}
		}
		return null;
	}

	public static boolean isSame(DbAirOrderItem item, SegmentInfo segmentInfo) {
		if (item.getDepartureTime().isEqual(segmentInfo.getDepartTime())
				&& item.getArrivalTime().isEqual(segmentInfo.getArrivalTime())) {
			return true;
		}
		return false;
	}

	// This is total gross commission (without subtracting TDS)
	public static double getGrossCommission(List<DbAirOrderItem> items, User user, boolean includePartnerCommission) {
		double totalCommission = 0;
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
				totalCommission += getGrossCommissionForPax(travellerInfo, user, includePartnerCommission);
			}
		}
		return totalCommission;
	}

	public static double getGrossCommissionForPax(FlightTravellerInfo travellerInfo, User user,
			boolean includePartnerCommission) {
		double totalCommission = 0;
		if (travellerInfo != null) {
			totalCommission = getGrossCommission(travellerInfo.getFareDetail(), user, includePartnerCommission);
		}
		return totalCommission;
	}

	public static double getGrossCommission(FareDetail fareDetail, User user, boolean includePartnerCommission) {
		double totalCommission = 0;
		if (fareDetail != null) {
			totalCommission = getGrossCommission(fareDetail.getFareComponents(), user, includePartnerCommission);
		}
		return totalCommission;
	}

	public static double getGrossCommission(Map<FareComponent, Double> fareComponents, User user,
			boolean includePartnerCommission) {
		double totalCommission = 0;
		if (MapUtils.isNotEmpty(fareComponents) && (user == null || !UserRole.corporate(user.getRole()))) {
			for (Entry<FareComponent, Double> entry : fareComponents.entrySet()) {
				if (FareComponent.getCommissionComponents().contains(entry.getKey())) {
					totalCommission += entry.getValue();
				}
				if (includePartnerCommission
						&& FareComponent.getPartnerCommissionComponents().contains(entry.getKey())) {
					totalCommission += entry.getValue();
				}
			}
		}
		return totalCommission;
	}

	public static double getPartnerMarkUp(List<DbAirOrderItem> items, User user) {
		double partnerMarkUp = 0;
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
				partnerMarkUp += getPartnerMarkUpForPax(travellerInfo);
			}
		}
		return partnerMarkUp;
	}

	public static double getPartnerMarkUpForPax(FlightTravellerInfo travellerInfo) {
		double partnerMarkUp = 0;
		if (travellerInfo != null) {
			FareDetail fareDetail = travellerInfo.getFareDetail();
			if (fareDetail != null && MapUtils.isNotEmpty(fareDetail.getFareComponents())) {
				partnerMarkUp = fareDetail.getFareComponents().getOrDefault(FareComponent.PMU, 0.0);
			}
		}
		return partnerMarkUp;
	}

	public static Double getPartnerCommission(List<DbAirOrderItem> items) {
		Double partnerCommission = new Double(0);
		for (DbAirOrderItem item : items) {
			if (CollectionUtils.isNotEmpty(item.getTravellerInfo())) {
				for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
					partnerCommission += getPartnerCommissionForPax(travellerInfo);
				}
			}
		}
		return partnerCommission;
	}

	public static Double getPartnerCommissionForPax(FlightTravellerInfo travellerInfo) {
		Double partnerCommission = new Double(0);
		FareDetail fDetail = travellerInfo.getFareDetail();
		if (fDetail != null && MapUtils.isNotEmpty(fDetail.getFareComponents())) {
			for (FareComponent component : fDetail.getFareComponents().keySet()) {
				if (FareComponent.getPartnerCommissionComponents().contains(component)) {
					partnerCommission += fDetail.getFareComponents().getOrDefault(component, 0.0);
				}
			}
		}
		return partnerCommission;
	}

	public static double getTDS(List<DbAirOrderItem> items) {
		double totalTds = 0;
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
				totalTds += travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.TDS, 0.0);
			}
		}
		return totalTds;
	}

	public static double getTotalFareComponentAmount(DbAirOrderItem item, FareComponent fareComponent) {
		double totalAmount = 0;
		for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
			totalAmount += travellerInfo.getFareDetail().getFareComponents().getOrDefault(fareComponent, 0.0);
		}
		return totalAmount;
	}

	public static PriceInfo getTotalPriceInfo(List<SegmentInfo> segmentInfos, int paxNumber,
			FareBreakUpConfigOutput fbConfig, boolean isApiPartnerFlow) {
		PriceInfo newPriceInfo = null;
		for (SegmentInfo segmentInfo : segmentInfos) {
			if (paxNumber != 0) {
				FlightTravellerInfo travellerInfo =
						segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(paxNumber - 1);
				newPriceInfo = generateFareComponents(newPriceInfo, travellerInfo, fbConfig, isApiPartnerFlow);
			} else {
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					newPriceInfo = generateFareComponents(newPriceInfo, travellerInfo, fbConfig, isApiPartnerFlow);
				}
			}
		}
		return newPriceInfo;
	}

	public static PriceInfo generateFareComponents(PriceInfo newPriceInfo, FlightTravellerInfo travellerInfo,
			FareBreakUpConfigOutput fbConfig, boolean isApiPartnerFlow) {
		if (newPriceInfo == null) {
			newPriceInfo = PriceInfo.builder().build();
			newPriceInfo.setTotalFareDetail(new FareDetail());
		}
		double markup = 0;
		double voucherDiscount = 0;
		for (Entry<FareComponent, Double> fareComponent : travellerInfo.getFareDetail().getFareComponents()
				.entrySet()) {
			FareComponent key = ObjectUtils.firstNonNull(
					fareComponent.getKey().mapComponent(fbConfig,
							SystemContextHolder.getContextData().getRequestContextHolder().getEndPoint()),
					fareComponent.getKey());
			double existingValue = newPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(key, 0.0);
			double newValue = fareComponent.getKey().getAmount(fareComponent.getValue());
			if (FareComponent.MU.equals(fareComponent.getKey())) {
				markup = fareComponent.getValue();
			} else if (FareComponent.VD.equals(fareComponent.getKey())) {
				voucherDiscount = fareComponent.getValue();
			}
			newPriceInfo.getTotalFareDetail().getFareComponents().put(key, (existingValue + newValue));
			if (fareComponent.getKey().mapComponent(fbConfig, null) != null) {
				FareComponent mapComponent = fareComponent.getKey().mapComponent(fbConfig, null);
				if (isApiPartnerFlow) {
					if (fareComponent.getKey().keepCurrentComponent()) {
						double value = fareComponent.getKey().getAmount(fareComponent.getValue());
						if (MapUtils
								.isNotEmpty(newPriceInfo.getTotalFareDetail().getAddlFareComponents(mapComponent))) {
							Double initialAmount = newPriceInfo.getTotalFareDetail().getAddlFareComponents(mapComponent)
									.getOrDefault(fareComponent.getKey(), 0.0);
							value += initialAmount;
						}
						newPriceInfo.getTotalFareDetail().getAddlFareComponents(mapComponent)
								.put(fareComponent.getKey(), value);
					} else {
						double amount = fareComponent.getKey().getAmount(fareComponent.getValue());
						if (MapUtils.isNotEmpty(newPriceInfo.getTotalFareDetail().getAddlFareComponents())) {
							Map<FareComponent, Double> mapComponentValue = newPriceInfo.getTotalFareDetail()
									.getAddlFareComponents().getOrDefault(mapComponent, new HashMap<>());
							amount += mapComponentValue.getOrDefault(FareComponent.OT, 0.0);
						}
						newPriceInfo.getTotalFareDetail().getAddlFareComponents(mapComponent).put(FareComponent.OT,
								amount);
					}
				} else {
					Double oldValue = newPriceInfo.getTotalFareDetail().getAddlFareComponents(mapComponent)
							.getOrDefault(fareComponent.getKey(), 0.0);
					newPriceInfo.getTotalFareDetail().getAddlFareComponents(mapComponent).put(fareComponent.getKey(),
							oldValue + fareComponent.getValue());
				}
			}
		}
		// TF = TF + Markup - voucher discount
		newPriceInfo.getTotalFareDetail().getFareComponents().put(FareComponent.TF,
				newPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d) + markup);

		// NF = TF - NCM - Markup - discount
		newPriceInfo.getTotalFareDetail().getFareComponents().put(FareComponent.NF,
				newPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d)
						- newPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.NCM, 0d)
						- newPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.VD, 0d)
						- newPriceInfo.getTotalFareDetail().getFareComponents().getOrDefault(FareComponent.DS, 0.0)
						- getTotalMarkUp(newPriceInfo));
		return newPriceInfo;
	}

	private static Double getTotalMarkUp(PriceInfo newPriceInfo) {
		Double markup = new Double(0);
		if (newPriceInfo.getTotalFareDetail() != null
				&& MapUtils.isNotEmpty(newPriceInfo.getTotalFareDetail().getAddlFareComponents())) {
			markup = newPriceInfo.getTotalFareDetail().getAddlFareComponents()
					.getOrDefault(FareComponent.TAF, new HashMap<>()).getOrDefault(FareComponent.MU, 0.0);
		}
		return markup;
	}

	public static List<ProcessedFlightTravellerInfo> generateProcessedTravellerInfo(List<SegmentInfo> segmentList,
			int paxNumber) {
		int listSize = paxNumber != 0 ? 1 : segmentList.get(0).getBookingRelatedInfo().getTravellerInfo().size();
		List<ProcessedFlightTravellerInfo> travellerList = Arrays.asList(new ProcessedFlightTravellerInfo[listSize]);
		for (SegmentInfo segmentInfo : segmentList) {
			ProcessedFlightTravellerInfo newTravellerInfo = null;
			String segmentKey = segmentInfo.getDepartureAndArrivalAirport();
			if (paxNumber != 0) {
				int index = paxNumber - 1;
				FlightTravellerInfo travellerInfo = segmentInfo.getBookingRelatedInfo().getTravellerInfo().get(index);
				newTravellerInfo = generateProcessedFlightTravellerInfo(segmentKey, travellerList, 0, travellerInfo);
				checkAndUpdateTravellerStatus(segmentInfo, newTravellerInfo, segmentKey);
				travellerList.set(0, newTravellerInfo);
			} else {
				int travellerIndex = 0;
				for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					newTravellerInfo = generateProcessedFlightTravellerInfo(segmentKey, travellerList, travellerIndex,
							travellerInfo);
					checkAndUpdateTravellerStatus(segmentInfo, newTravellerInfo, segmentKey);
					travellerList.set(travellerIndex, newTravellerInfo);
					travellerIndex++;
				}
			}
		}
		return travellerList;
	}

	public static void checkAndUpdateTravellerStatus(SegmentInfo segmentInfo,
			ProcessedFlightTravellerInfo newTravellerInfo, String segmentKey) {
		if (segmentInfo.getBookingRelatedInfo().getStatus() != AirItemStatus.SUCCESS
				&& segmentInfo.getBookingRelatedInfo().getStatus() != AirItemStatus.ON_HOLD
				&& segmentInfo.getBookingRelatedInfo().getStatus() != AirItemStatus.CANCELLED
				&& segmentInfo.getBookingRelatedInfo().getStatus() != AirItemStatus.ABORTED) {
			newTravellerInfo.getStatusMap().put(segmentKey, TravellerStatus.PENDING);
		}
	}

	private static ProcessedFlightTravellerInfo generateProcessedFlightTravellerInfo(String segmentKey,
			List<ProcessedFlightTravellerInfo> travellerList, int travellerIndex, FlightTravellerInfo travellerInfo) {
		ProcessedFlightTravellerInfo newProcessedTravellerInfo = ObjectUtils.firstNonNull(
				travellerIndex < travellerList.size() ? travellerList.get(travellerIndex) : null, GsonUtils.getGson()
						.fromJson(GsonUtils.getGson().toJson(travellerInfo), ProcessedFlightTravellerInfo.class));

		if (StringUtils.isNoneEmpty(travellerInfo.getPnr()))
			newProcessedTravellerInfo.getPnrDetails().put(segmentKey, travellerInfo.getPnr());

		if (StringUtils.isNoneEmpty(travellerInfo.getTicketNumber()))
			newProcessedTravellerInfo.getTicketNumberDetails().put(segmentKey, travellerInfo.getTicketNumber());

		if (travellerInfo.getSsrBaggageInfo() != null)
			newProcessedTravellerInfo.getSsrBaggageInfos().put(segmentKey, travellerInfo.getSsrBaggageInfo());

		if (travellerInfo.getSsrMealInfo() != null)
			newProcessedTravellerInfo.getSsrMealInfos().put(segmentKey, travellerInfo.getSsrMealInfo());

		if (travellerInfo.getSsrSeatInfo() != null)
			newProcessedTravellerInfo.getSsrSeatInfos().put(segmentKey, travellerInfo.getSsrSeatInfo());

		if (CollectionUtils.isNotEmpty(travellerInfo.getExtraServices()))
			newProcessedTravellerInfo.getSsrExtraServiceInfos().put(segmentKey, travellerInfo.getExtraServices());

		if (travellerInfo.getOtherServicesText() != null)
			newProcessedTravellerInfo.getMiscSsr().put(segmentKey, travellerInfo.getOtherServicesText());

		if (travellerInfo.getStatus() != null)
			newProcessedTravellerInfo.getStatusMap().put(segmentKey, travellerInfo.getStatus());

		if (travellerInfo.getNewBookingId() != null)
			newProcessedTravellerInfo.getNbIdMap().put(segmentKey, travellerInfo.getNewBookingId());

		if (travellerInfo.getOldBookingId() != null)
			newProcessedTravellerInfo.getObIdMap().put(segmentKey, travellerInfo.getOldBookingId());

		if (StringUtils.isNotEmpty(travellerInfo.getSupplierBookingId()))
			newProcessedTravellerInfo.getSupplierBookingIds().put(segmentKey, travellerInfo.getSupplierBookingId());

		if (travellerInfo.getInvoice() != null)
			newProcessedTravellerInfo.getInvoiceIds().put(segmentKey, travellerInfo.getInvoice());

		newProcessedTravellerInfo.setUserProfile(travellerInfo.getUserProfile());
		return newProcessedTravellerInfo;
	}

	public static void setPriceInfoList(List<SegmentInfo> segmentList) {
		for (SegmentInfo segmentInfo : segmentList) {
			PriceInfo priceInfo = PriceInfo.builder().build();
			priceInfo.setSupplierBasicInfo(segmentInfo.getPriceInfoList().get(0).getSupplierBasicInfo());
			priceInfo.setFareIdentifier(segmentInfo.getPriceInfoList().get(0).getFareIdentifier());
			for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
				priceInfo.getFareDetails().put(travellerInfo.getPaxType(), travellerInfo.getFareDetail());
			}
			if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfo(0).getMessages())) {
				priceInfo.setMessages(segmentInfo.getPriceInfo(0).getMessages());
			}
			segmentInfo.getPriceInfoList().set(0, priceInfo);
		}
	}

	public static void removeTravellerInfoFromSegmentList(List<SegmentInfo> segmentList) {
		for (SegmentInfo segmentInfo : segmentList) {
			segmentInfo.setBookingRelatedInfo(null);
		}
	}

	public static void setTimeLimit(List<SegmentInfo> segmentList, AirOrderDetails airDetails) {
		for (SegmentInfo segmentInfo : segmentList) {
			/**
			 * GDS timelimit is not set correctly , hence can't display this to customer : Ashu
			 */
			if (!segmentInfo.getFlightDesignator().getAirlineInfo().getIsLcc()) {
				airDetails.setTimeLimit(null);
				return;
			}

			if (segmentInfo.getPriceInfo(0).getMiscInfo() != null) {
				LocalDateTime segmentTimeLimit = segmentInfo.getPriceInfo(0).getMiscInfo().getTimeLimit();
				if (segmentTimeLimit != null && (airDetails.getTimeLimit() == null
						|| segmentTimeLimit.isBefore(airDetails.getTimeLimit()))) {
					airDetails.setTimeLimit(segmentInfo.getPriceInfo(0).getMiscInfo().getTimeLimit());
				}
			}
		}
	}


	public static boolean isDomesticOrder(List<AirOrderItem> orderItems) {
		for (AirOrderItem orderItem : orderItems) {
			if (!AirType.DOMESTIC.equals(AirType.getEnumFromCode(orderItem.getAdditionalInfo().getType())))
				return false;
		}
		return true;
	}

	public static boolean isDomesticDbOrder(List<DbAirOrderItem> orderItems) {
		for (DbAirOrderItem orderItem : orderItems) {
			if (!AirType.DOMESTIC.equals(AirType.getEnumFromCode(orderItem.getAdditionalInfo().getType())))
				return false;
		}
		return true;
	}

	public static List<SSRInformation> clubSSRInfo(FlightTravellerInfo travellerInfo) {
		List<SSRInformation> ssrInformationList = new ArrayList<>();
		if (!org.springframework.util.CollectionUtils.isEmpty(travellerInfo.getSsrBaggageInfos())) {
			ssrInformationList.addAll(travellerInfo.getSsrBaggageInfos());
		}
		if (!org.springframework.util.CollectionUtils.isEmpty(travellerInfo.getSsrMealInfos())) {
			ssrInformationList.addAll(travellerInfo.getSsrMealInfos());
		}
		if (!org.springframework.util.CollectionUtils.isEmpty(travellerInfo.getSsrSeatInfos())) {
			ssrInformationList.addAll(travellerInfo.getSsrSeatInfos());
		}
		return ssrInformationList;
	}

	public static void setAirlineNAirportInfo(AirOrderItem item) {
		item.setAirlineInfo(fmsCommunicator.getAirlineInfo(item.getAirlinecode()));
		item.setDepartAirportInfo(fmsCommunicator.getAirportInfo(item.getSource()));
		item.setArrivalAirportInfo(fmsCommunicator.getAirportInfo(item.getDest()));
		item.setDuration();
	}

	public static Map<FareComponent, Double> getMergedComponents(List<AirOrderItem> airOrderItems, int paxIndex) {
		List<FareDetail> fareDetails = new ArrayList<>();
		for (AirOrderItem airOrderItem : airOrderItems) {
			fareDetails.add(airOrderItem.getTravellerInfo().get(paxIndex).getFareDetail());
		}
		return getMergedComponents(fareDetails);
	}

	public static Map<FareComponent, Double> getMergedComponents(List<FareDetail> fareDetails) {
		Map<FareComponent, Double> mergedFares = new HashMap<>();
		for (FareDetail fareDetail : fareDetails) {
			Map<FareComponent, Double> fares = fareDetail.getFareComponents();
			for (FareComponent fareComponent : fares.keySet()) {
				Double mergedAmount = mergedFares.getOrDefault(fareComponent, 0d),
						amount = fares.getOrDefault(fareComponent, 0d);
				mergedFares.put(fareComponent, mergedAmount + amount);
			}
		}
		return mergedFares;
	}

	public static void updatePaymentFee(List<DbAirOrderItem> items, Order order, double paymentFee) {
		Map<FareComponent, Double> fcs = items.get(0).getTravellerInfo().get(0).getFareDetail().getFareComponents();
		fcs.put(FareComponent.PF, paymentFee);
		fcs.put(FareComponent.TF, fcs.get(FareComponent.TF) + paymentFee);
		items.get(0).setAmount(items.get(0).getAmount() + paymentFee);
		order.setAmount(order.getAmount() + paymentFee);
	}

	public static String getNoteMessage(List<Note> notes) {
		if (CollectionUtils.isNotEmpty(notes)) {
			StringJoiner noteMessage = new StringJoiner(",");
			notes.stream().forEach(note -> noteMessage.add(note.getNoteMessage()));
			return noteMessage.toString();
		}
		return null;
	}

	public static PNRStatus getPNRStatus(Order order, List<SegmentInfo> segmentList, User bookingUser) {

		List<PNRInfo> pnrInfos = new ArrayList<>();
		if ((OrderStatus.ON_HOLD.equals(order.getStatus()) || OrderStatus.UNCONFIRMED.equals(order.getStatus()))
				&& AirBookingUtils.isAllowedFlowTypeForUnHold(order, bookingUser)) {


			List<TripInfo> tripInfos = BookingUtils.createTripListFromSegmentList(segmentList);
			Map<String, BookingSegments> bookingSegmentMap =
					fmsCommunicator.getSupplierWiseBookingSegmentsUsingPNR(tripInfos);
			for (Entry<String, BookingSegments> entry : bookingSegmentMap.entrySet()) {
				BookingSegments bookingSegments = entry.getValue();
				SegmentInfo firstSegment = bookingSegments.getSegmentInfos().get(0);

				AirItemStatus itemStatus = firstSegment.getBookingRelatedInfo().getStatus();
				if (AirBookingUtils.isUnHoldAllowed(bookingSegments.getSegmentInfos(),
						order.getAdditionalInfo().getFlowType(), bookingUser)) {
					PNRInfo pnrInfo = new PNRInfo();
					List<String> pnr = new ArrayList<>(bookingSegments.getBookingSegmentsPNR());
					pnrInfo.setPnr(pnr);
					pnrInfo.setUnHoldAllowed(true);
					pnrInfo.setStatus(itemStatus.getStatus());
					String tripKey = StringUtils.join(firstSegment.getDepartureAirportCode(), "-",
							bookingSegments.getSegmentInfos().get(bookingSegments.getSegmentInfos().size() - 1)
									.getArrivalAirportCode());
					pnrInfo.setTripKey(tripKey);
					pnrInfos.add(pnrInfo);
				}

			}
		}

		if (CollectionUtils.isNotEmpty(pnrInfos)) {
			PNRStatus pnrStatus = new PNRStatus();
			pnrStatus.setPnrInfo(pnrInfos);
			return pnrStatus;
		}
		return null;
	}

	public static boolean isUnHoldAllowed(List<SegmentInfo> segmentInfos, OrderFlowType flowType, User bookingUser) {
		boolean isAllowed = false;
		if (CollectionUtils.isNotEmpty(segmentInfos) && flowType != null) {
			AirConfiguratorInfo gnPuprose =
					fmsCommunicator.getAirConfigRule(AirConfiguratorRuleType.GNPUPROSE, bookingUser);
			AirGeneralPurposeOutput gnOutput = null;
			if (gnPuprose != null && ((gnOutput = (AirGeneralPurposeOutput) gnPuprose.getOutput()) != null)
					&& MapUtils.isNotEmpty(gnOutput.getUnHoldAllowedSource())) {
				Integer sourceId = segmentInfos.get(0).getSupplierInfo().getSourceId();
				AirItemStatus itemStatus = segmentInfos.get(0).getBookingRelatedInfo().getStatus();
				if (gnOutput.getUnHoldAllowedSource().get(flowType.getName()) != null
						&& gnOutput.getUnHoldAllowedSource().get(flowType.getName()).contains(sourceId)
						&& !AirItemStatus.UNCONFIRMED.equals(itemStatus)) {
					isAllowed = true;
				}
			}
		}
		return isAllowed;
	}

	public static boolean isAllowedFlowTypeForUnHold(Order order, User bookingUser) {
		boolean isAllowedFlowType = false;
		AirConfiguratorInfo gnPuprose =
				fmsCommunicator.getAirConfigRule(AirConfiguratorRuleType.GNPUPROSE, bookingUser);
		AirGeneralPurposeOutput gnPurposeOutput = null;
		if (gnPuprose != null && ((gnPurposeOutput = (AirGeneralPurposeOutput) gnPuprose.getOutput()) != null)
				&& MapUtils.isNotEmpty(gnPurposeOutput.getUnHoldAllowedSource()) && gnPurposeOutput
						.getUnHoldAllowedSource().containsKey(order.getAdditionalInfo().getFlowType().getName())) {
			isAllowedFlowType = true;
		}
		return isAllowedFlowType;
	}

	public static double getPaymentTotal(List<DbAirOrderItem> items, User user) {
		double paymentTotal = 0;
		double voucherDiscount = 0;

		double totalCommission = getGrossCommission(items, null, true);
		for (DbAirOrderItem item : items) {
			paymentTotal += getTotalFareComponentAmount(item, FareComponent.TF);
			voucherDiscount += getTotalFareComponentAmount(item, FareComponent.VD);
		}

		// Corporate user doesn't have commission logic. Commission will be discount for Corporate.
		if (UserRole.corporate(user.getRole())) {
			paymentTotal = paymentTotal - totalCommission;
		}

		// Voucher discount should not be charged.
		paymentTotal = paymentTotal - voucherDiscount;

		return paymentTotal;
	}

	public static double getPaymentTotalForSegments(List<SegmentInfo> segments, User user) {
		double paymentTotal = 0;
		if (CollectionUtils.isNotEmpty(segments)) {
			for (SegmentInfo segment : segments) {
				if (segment.getBookingRelatedInfo() != null
						&& CollectionUtils.isNotEmpty(segment.getBookingRelatedInfo().getTravellerInfo())) {
					for (FlightTravellerInfo traveller : segment.getBookingRelatedInfo().getTravellerInfo()) {
						paymentTotal += calculateTravellerTotalWithoutDiscount(traveller, user);
					}
				}
			}
		}
		return paymentTotal;
	}

	public static double calculateTravellerTotalWithoutDiscount(FlightTravellerInfo travellerInfo, User user) {
		double totalAmount = 0d;

		if (travellerInfo.getFareDetail() != null) {
			totalAmount = travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.TF, 0d);
			if (UserRole.corporate(user.getRole())) {
				totalAmount -= AirBookingUtils.getGrossCommissionForPax(travellerInfo, null, true);
			}
			// Voucher discount
			totalAmount -= travellerInfo.getFareDetail().getFareComponents().getOrDefault(FareComponent.VD, 0d);
		}

		return totalAmount;
	}

	public static double calculateAvailablePNRCredit(List<DbAirOrderItem> orderItems) {
		double totalCreditBalance = 0;
		if (CollectionUtils.isNotEmpty(orderItems)) {
			for (DbAirOrderItem item : orderItems) {
				totalCreditBalance += BaseUtils.calculateAvailablePNRCredit(item.getTravellerInfo());
			}
		}
		return totalCreditBalance;
	}

	public static void setTravellerStatus(List<DbAirOrderItem> airOrderItems, TravellerStatus status) {
		if (CollectionUtils.isNotEmpty(airOrderItems)) {
			airOrderItems.forEach(item -> {
				item.getTravellerInfo().forEach(traveller -> traveller.setStatus(status));
			});
		}
	}

	public static Double getPartnerTds(List<DbAirOrderItem> items, FareComponent targetComponent) {
		Double totalTds = new Double(0);
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
				totalTds += travellerInfo.getFareDetail().getFareComponents().getOrDefault(targetComponent, 0.0);
			}
		}
		return totalTds;
	}

	public static Double getRedeemedPointAmount(List<DbAirOrderItem> items, FareComponent targetComponent) {
		Double redeemAmount = new Double(0);
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo travellerInfo : item.getTravellerInfo()) {
				redeemAmount += travellerInfo.getFareDetail().getFareComponents().getOrDefault(targetComponent, 0.0);
			}
		}
		return redeemAmount;
	}

	public static boolean isVirtualPayment(BookingRequest bookingRequest) {
		boolean isVirtualPayment = false;
		if (CollectionUtils.isNotEmpty(bookingRequest.getPaymentInfos())) {
			List<PaymentRequest> virtualPayments = bookingRequest.getPaymentInfos().stream()
					.filter(p -> (PaymentMedium.VIRTUAL_PAYMENT.equals(p.getPaymentMedium())))
					.collect(Collectors.toList());
			if (CollectionUtils.isNotEmpty(virtualPayments)) {
				isVirtualPayment = true;
			}
		}
		return isVirtualPayment;
	}

	public static PaymentRequest createVirtualPaymentRequest(PaymentRequest paymentInfo, List<SegmentInfo> segmentInfos,
			User bookingUser) {
		PaymentRequest vpRequest = new GsonMapper<>(paymentInfo, PaymentRequest.class).convert();
		double totalFare = AirBookingUtils.getPaymentTotalForSegments(segmentInfos, bookingUser);
		vpRequest.getAdditionalInfo()
				.setCreditCardId(segmentInfos.get(0).getPriceInfo(0).getMiscInfo().getCcInfoId().longValue());
		if (StringUtils.isNotBlank(paymentInfo.getAdditionalInfo().getVoucherCode())) {
			vpRequest.getAdditionalInfo().setVoucherCode(paymentInfo.getAdditionalInfo().getVoucherCode());
		}
		Integer tripSource = segmentInfos.get(0).getSupplierInfo().getSourceId();
		if (isVpAllowed(tripSource, bookingUser)) {
			vpRequest.getAdditionalInfo().setIsChargedBySupplier(true);
		}
		vpRequest.setAmount(totalFare);
		return vpRequest;
	}

	public static boolean isVpAllowed(Integer tripSource, User bookingUser) {
		AirConfiguratorInfo gnPuprose =
				fmsCommunicator.getAirConfigRule(AirConfiguratorRuleType.GNPUPROSE, bookingUser);
		AirGeneralPurposeOutput output = null;
		if (gnPuprose != null && (output = (AirGeneralPurposeOutput) gnPuprose.getOutput()) != null
				&& CollectionUtils.isNotEmpty(output.getUserCreditCardSupportedSources())
				&& output.getUserCreditCardSupportedSources().contains(tripSource)) {
			return true;
		}
		return false;
	}

	public static double calculateDiscount(List<DbAirOrderItem> items, User user) {
		double discount = 0d;

		// Voucher discount
		for (DbAirOrderItem item : items) {
			for (FlightTravellerInfo traveller : item.getTravellerInfo()) {
				discount += traveller.getFareDetail().getFareComponents().getOrDefault(FareComponent.VD, 0d);
			}
		}

		// Corporate discount
		if (UserRole.corporate(user.getRole())) {
			discount += getGrossCommission(items, null, true);
		}
		return discount;
	}

	public static Set<FareComponent> amendmentRefundableComponents(AirOrderItem airOrderItem,
			boolean cancellationNewFlow, boolean isSSRRefundable) {
		Set<FareComponent> refundableComponents = FareComponent.getRefundableComponents(false);
		if (cancellationNewFlow) {
			refundableComponents = FareComponent.getRefundableComponents(isSSRRefundable);
		}
		return refundableComponents;
	}

	public static Set<FareComponent> getPaxCancelComponents(boolean isPassThrough) {
		Set<FareComponent> paxCancelComponents = FareComponent.paxCancelComponents();
		if (isPassThrough) {
			paxCancelComponents.remove(FareComponent.CCF);
			paxCancelComponents.remove(FareComponent.CCFT);
		}
		return paxCancelComponents;
	}
}
