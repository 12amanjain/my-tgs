package com.tgs.services.oms.utils.hotel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.OrderStatus;
import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.ums.datamodel.User;

public class HotelBookingUtils {

	public static void canAbortOrCancel(HotelInfo hInfo, Order order, SystemError sysError) {

		OrderStatus orderStatus = order.getStatus();
		if (orderStatus.equals(OrderStatus.FAILED) || orderStatus.equals(OrderStatus.ABORTED)
				|| orderStatus.equals(OrderStatus.CANCELLED) || orderStatus.equals(OrderStatus.REJECTED)
				|| orderStatus.equals(OrderStatus.IN_PROGRESS) || orderStatus.equals(OrderStatus.UNCONFIRMED)) {
			throw new CustomGeneralException(sysError, "As Order is in" + orderStatus.toString() + "Status");
		}
	}

	public static void canAbort(HotelInfo hInfo, Order order) {

		canAbortOrCancel(hInfo, order, SystemError.CANNOT_BE_ABORTED);
		if (order.getStatus().equals(OrderStatus.SUCCESS)) {
			throw new CustomGeneralException(SystemError.CANNOT_BE_ABORTED, "Vouchered Booking can't be aborted");
		} else if (order.getStatus().equals(OrderStatus.ON_HOLD)) {
			throw new CustomGeneralException(SystemError.CANNOT_BE_ABORTED, "On_Hold Booking can't be aborted");
		}
	}


	public static void canCancel(HotelInfo hInfo, Order order, boolean allowAfterCheckin) {

		if (!allowAfterCheckin && isCheckInDatePassed(hInfo)) {
			throw new CustomGeneralException(SystemError.CANNOT_BE_CANCELLED,
					"Booking Cannot be Cancelled as CheckIn date has passed");
		}

		canAbortOrCancel(hInfo, order, SystemError.CANNOT_BE_CANCELLED);
		if (order.getStatus().equals(OrderStatus.PENDING)) {
			throw new CustomGeneralException(SystemError.CANNOT_BE_CANCELLED,
					"Booking Cannot be cancelled as it is in Pending status "
							+ "Please go for abort instead of cancellation");
		} else if (order.getStatus().equals(OrderStatus.PAYMENT_SUCCESS)) {
			throw new CustomGeneralException(SystemError.CANNOT_BE_CANCELLED,
					"Booking Cannot be cancelled as it is in Payment_Success status"
							+ "Please go for abort instead of cancellation");
		}

		Option option = hInfo.getOptions().get(0);
		if (option.getCancellationRestrictedDateTime() != null
				&& LocalDateTime.now().isAfter(option.getCancellationRestrictedDateTime()))
			throw new CustomGeneralException(SystemError.CANNOT_BE_CANCELLED,
					"Booking Cannot be cancelled as Cancellation Is Restricted");

	}


	public static boolean isFullRefundAvailable(Option option) {

		LocalDateTime deadlineDateTime = option.getDeadlineDateTime();
		boolean isFullRefundAllowed = option.getCancellationPolicy().getIsFullRefundAllowed();
		if (isFullRefundAllowed && deadlineDateTime.isAfter(LocalDateTime.now())) {
			return true;
		}
		return false;
	}

	public static boolean isFullRefundAvailableForVoucheredBooking(Order order, Option option) {

		boolean isFullRefund = false;
		if (order.getStatus().equals(OrderStatus.SUCCESS)) {
			if (isFullRefundAvailable(option))
				isFullRefund = true;
		}
		return isFullRefund;
	}

	public static PaymentRequest createRefundPayments(Payment p, PaymentTransactionType transactionType,
			BigDecimal amount) {
		return PaymentRequest.builder().build().setPayUserId(p.getPayUserId()).setProduct(p.getProduct())
				.setOpType(PaymentOpType.CREDIT).setRefId(p.getRefId()).setOriginalPaymentRefId(p.getPaymentRefId())
				.setAmount(amount.abs()).setPaymentMedium(p.getPaymentMedium()).setTransactionType(transactionType);
	}

	public static PaymentRequest createAPHPayments(Payment payment, PaymentTransactionType transactionType,
			BigDecimal amount) {
		return PaymentRequest.builder().build().setPayUserId(payment.getPayUserId()).setProduct(payment.getProduct())
				.setOpType(PaymentOpType.DEBIT).setRefId(payment.getRefId())
				.setOriginalPaymentRefId(payment.getPaymentRefId()).setAmount(amount.abs()).setTds(payment.getTds())
				.setTransactionType(transactionType);
	}

	public static PaymentRequest createFundHandlerRequests(Payment payment, PaymentTransactionType transactionType,
			BigDecimal amount) {
		return PaymentRequest.builder().build().setPayUserId(payment.getPayUserId()).setProduct(payment.getProduct())
				.setOpType(PaymentOpType.CREDIT).setRefId(payment.getRefId())
				.setOriginalPaymentRefId(payment.getPaymentRefId()).setAmount(amount.abs())
				.setTransactionType(transactionType);
	}

	public static boolean isCheckInDatePassed(HotelInfo hInfo) {

		boolean isCheckInDatePassed = false;
		LocalDateTime currentTime = LocalDateTime.now();
		RoomInfo roomInfo = hInfo.getOptions().get(0).getRoomInfos().get(0);
		/*
		 * Concerned About this case -> As we are taking default time of 12 noon , What if traveller checkin earlier
		 * than 12 noon ?
		 */
		if (currentTime.isAfter(roomInfo.getCheckInDate().atTime(12, 00))) {
			isCheckInDatePassed = true;
		}
		return isCheckInDatePassed;
	}

	public static String getLeadPax(Option option) {

		TravellerInfo leadTraveller = option.getRoomInfos().get(0).getTravellerInfo().get(0);
		String leadPax = StringUtils.join(leadTraveller.getFirstName(), " ", leadTraveller.getLastName());
		return leadPax;

	}

	public static String getHotelAddress(HotelInfo hInfo) {

		Address address = hInfo.getAddress();
		String hotelAddress = address.getAddressLine1();
		if (address.isCityPresent()) {
			hotelAddress = StringUtils.join(hotelAddress, ",", address.getCity().getName());
		}
		if (address.isCountryPresent()) {
			hotelAddress = StringUtils.join(hotelAddress, ",", address.getCountry().getName());
		}
		return hotelAddress;
	}

	public static double getPaymentTotal(List<DbHotelOrderItem> items, User user) {
		double paymentTotal = 0;
		double voucherDiscount = 0;

		for (DbHotelOrderItem item : items) {
			paymentTotal += getTotalFareComponentAmount(item, HotelFareComponent.NF);
			voucherDiscount += getTotalFareComponentAmount(item, HotelFareComponent.VD);
		}

		// Voucher discount should not be charged.
		paymentTotal = paymentTotal - voucherDiscount;
		return paymentTotal;
	}

	public static double getTotalFareComponentAmount(DbHotelOrderItem item, HotelFareComponent fareComponent) {

		return item.getRoomInfo().getPerNightPriceInfos().stream().mapToDouble(priceInfo -> {
			return priceInfo.getFareComponents().getOrDefault(fareComponent, 0.0d);
		}).sum();

	}
}
