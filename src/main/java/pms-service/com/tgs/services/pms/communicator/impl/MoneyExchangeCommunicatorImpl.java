package com.tgs.services.pms.communicator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import com.tgs.services.pms.helper.MoneyExchangeHelper;
import com.tgs.services.pms.jparepository.MoneyExchangeService;
import com.tgs.utils.common.SpelExpressionUtils;

@Service
public class MoneyExchangeCommunicatorImpl implements MoneyExchangeCommunicator {

	@Autowired
	MoneyExchangeService moneyExchangeService;

	@Autowired
	MoneyExchangeHelper moneyExchangeHelper;

	/**
	 * This method will return conversion value of one currency to another currency and adds up the mark up if
	 * applicable.
	 */
	@Override
	public double getExchangeValue(Double amount, MoneyExchangeInfoFilter moneyExchangeInfoFilter,
			boolean isFallBackReq) {
		Double exchangeValue = 0.0;
		if (amount != null) {
			MoneyExchangeInfo exchangeInfo = searchMoneyExchangeInfo(moneyExchangeInfoFilter, isFallBackReq);
			if (exchangeInfo != null) {
				exchangeValue =
						amount * exchangeInfo.getExchangeRate() + getMarkupValue(amount, exchangeInfo.getMarkup());
			}
		}
		return exchangeValue;
	}

	private Double getMarkupValue(Double totalAmount, String markup) {
		if (StringUtils.isNotEmpty(markup)) {
			Map<String, Double> attributes = new HashMap<>();
			attributes.put("amt", totalAmount);
			return SpelExpressionUtils.evaluateExpression(markup, attributes);
		}
		return 0.0;
	}

	private MoneyExchangeInfo searchMoneyExchangeInfo(MoneyExchangeInfoFilter exchangeFilter, boolean isFallBackReq) {

		// 1. check based on type
		MoneyExchangeInfo exchangeInfo = MoneyExchangeHelper.getExchangeRate(exchangeFilter);

		if (exchangeInfo == null && isFallBackReq) {
			// 2. check based on supplier source name
			exchangeFilter.setType(exchangeFilter.getType().split("_")[0]);
			exchangeInfo = MoneyExchangeHelper.getExchangeRate(exchangeFilter);
		}

		if (exchangeInfo == null && isFallBackReq) {
			// 3. check based on Default
			exchangeFilter.setType("Default");
			exchangeInfo = MoneyExchangeHelper.getExchangeRate(exchangeFilter);
		}
		return exchangeInfo;
	}
}
