package com.tgs.services.pms.communicator.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.UserPoint;
import com.tgs.services.pms.datamodel.UserPointsCacheFilter;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.dbmodel.DbUserPoint;
import com.tgs.services.pms.dbmodel.DbUserWallet;
import com.tgs.services.pms.helper.CreditLineHelper;
import com.tgs.services.pms.helper.NRCreditHelper;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.helper.UserPointsHelper;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.jparepository.UserPointsService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.services.pms.manager.UserBalanceManager;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.servicehandler.DebitHandler;
import com.tgs.services.pms.servicehandler.PaymentModeHandler;
import com.tgs.services.ums.datamodel.UserDuesSummary;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentServiceCommunicatorImpl implements PaymentServiceCommunicator {

	@Autowired
	@Lazy
	private PaymentProcessor paymentProcessor;

	@Autowired
	private CreditPolicyService policyService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private UserWalletService walletService;

	@Autowired
	private UserPointsService userPointsService;

	@Autowired
	@Lazy
	private DebitHandler debitHandler;

	@Autowired
	private PaymentModeHandler paymentModeHandler;

	@Autowired
	private UserBalanceManager userBalanceManager;

	@Override
	public CreditPolicy getPolicy(String policyId) {
		return policyService.findByPolicyId(policyId);
	}

	@Override
	public boolean hasSufficientBalance(WalletPaymentRequest request) {
		try {
			debitHandler.evaluatePaymentMediums(request);
		} catch (PaymentException ex) {
			if (ex.getError().equals(SystemError.INSUFFICIENT_BALANCE)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public List<Payment> doPaymentsUsingPaymentRequests(List<PaymentRequest> paymentRequests) throws PaymentException {
		if (CollectionUtils.isEmpty(paymentRequests)) {
			return new ArrayList<>();
		}
		return paymentProcessor.process(paymentRequests);
	}

	@Override
	public List<PaymentConfigurationRule> getApplicableRulesOutput(PaymentFact fact, PaymentRuleType type,
			PaymentMedium medium) {
		return PaymentConfigurationHelper.getApplicableRules(fact, type, medium);
	}

	@Override
	public List<Payment> search(PaymentFilter paymentFilter) {
		return paymentService.search(paymentFilter);
	}

	@Override

	public Map<String, List<CreditLine>> fetchCreditLineFromCache(List<String> userIds) {
		return CreditLineHelper.getCreditLineMap(userIds);
	}

	@Override
	public List<CreditLine> fetchCreditLineFromCache(String userId) {
		return CreditLineHelper.getCreditLines(userId);
	}

	@Override
	public Map<String, List<NRCredit>> fetchNRCreditsFromCache(List<String> userIds) {
		return NRCreditHelper.getCreditsByUser(userIds);
	}

	@Override
	public List<NRCredit> fetchNRCreditsFromCache(String userId) {
		return NRCreditHelper.getCreditsByUser(userId);
	}

	@Override
	public List<Payment> fetchPaymentByBookingId(String bookingId) {
		return DbPayment.toDomainList(paymentService.findByRefId(bookingId));
	}

	@Override
	public UserWallet updateUserWallet(UserWallet wallet) {
		DbUserWallet dbWallet = Optional.ofNullable(walletService.findByUserId(wallet.getUserId()))
				.orElseGet(() -> new DbUserWallet().from(wallet));
		if (dbWallet.getBalance() == null)
			dbWallet.setBalance(0L);
		dbWallet = walletService.save(dbWallet);
		return dbWallet.toDomain();
	}

	@Override
	public UserPoint updateUserPoints(UserPoint userPoints) {
		DbUserPoint dbUserPoints = userPointsService.getDbUserPoints(userPoints.getUserId(), userPoints.getType());
		dbUserPoints = userPointsService.save(dbUserPoints);
		return dbUserPoints.toDomain();
	}

	@Override
	public Map<String, BigDecimal> getUserBalanceFromCache(PaymentMetaInfo paymentMetaInfo) {
		return userBalanceManager.getUserBalanceFromCache(paymentMetaInfo);
	}

	@Override
	public UserBalanceSummary getUserBalanceSummary(String userId) {
		return userBalanceManager.getUserBalanceSummary(userId);
	}

	@Override
	public UserDuesSummary getUserDueSummary(String userId) {
		return userBalanceManager.getUserDueSummary(userId);
	}

	@Override
	public void deleteFailedPaymentsByRefId(String refId) {
		paymentService.deleteFailedPaymentsByRefId(refId);
	}

	@Override
	public PaymentModeResponse getPaymentModeResponse(PaymentModeRequest paymentModeRequest) {
		return paymentModeHandler.getResponse(paymentModeRequest);
	}

	@Override
	public Map<String, Map<PointsType, BigDecimal>> getUserPoints(List<String> userIds) {
		Map<String, Map<PointsType, BigDecimal>> userPointsBlc = new HashMap<>();
		Map<String, Map<PointsType, UserPoint>> userPoints =
				UserPointsHelper.getUserPoints(UserPointsCacheFilter.builder().userIds(userIds).build());
		userPoints.forEach((userId, pts) -> {
			Map<PointsType, BigDecimal> balanceMap = new HashMap<>();
			pts.forEach((k, v) -> balanceMap.put(k, v.getBalance()));
			userPointsBlc.put(userId, balanceMap);
		});
		return userPointsBlc;
	}

}
