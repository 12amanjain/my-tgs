package com.tgs.services.pms.dbmodel;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.tgs.services.base.gson.BigDecimalUnitToCentAdaptorFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.DoubleToLongAdapterFactory;
import com.tgs.services.pms.datamodel.BillAdditionalInfo;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.hibernate.BillAdditionalInfoType;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "creditBill")
@Table(name = "creditBill")
@Getter
@Setter
@Audited
@TypeDefs({ @TypeDef(name = "BillAdditionalInfoType", typeClass = BillAdditionalInfoType.class) })
public class DbCreditBill extends BaseModel<DbCreditBill, CreditBill> {

	@Column(updatable = false)
	private String billNumber;

	@Column(updatable = false)
	private String userId;

	@Column(updatable = false)
	private Long creditId;

	@Column
	@Type(type = "BillAdditionalInfoType")
	private BillAdditionalInfo additionalInfo;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private LocalDateTime processedOn;

	@Column
	private LocalDateTime paymentDueDate;

	@Column
	private LocalDateTime lockDate;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long billAmount;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long settledAmount;

	@Column
	private Boolean isSettled = false;

	@Column
	private Integer extensionCount;

	public DbCreditBill() { }

	@Override
	public CreditBill toDomain() {
		CreditBill bill = new GsonMapper<>(this, CreditBill.class).convert();
		if (bill.getIsSettled().equals(false)) {
			bill.setDaysSinceDueDate(Duration.between(bill.getPaymentDueDate(), LocalDateTime.now()).toDays());
		}
		if (bill.getAdditionalInfo().getSettledDate() == null) {
			bill.getAdditionalInfo().setSettledDate(bill.getProcessedOn());
		}
		return bill;
	}

	@Override
	public DbCreditBill from(CreditBill dataModel) {
		return new GsonMapper<>(dataModel, this, DbCreditBill.class).convert();
	}
}