package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.BigDecimalUnitToCentAdaptorFactory;
import com.tgs.services.pms.datamodel.CreditAdditionalInfo;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.hibernate.CreditLineAdditionalInfoType;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "creditLine")
@Table(name = "creditLine")
@TypeDefs({ @TypeDef(name = "CreditLineAdditionalInfoType", typeClass = CreditLineAdditionalInfoType.class) })
@Getter
@Setter
@Audited
public class DbCreditLine extends BaseModel<DbCreditLine, CreditLine> {

	@Column(updatable = false)
	private String userId;

	@Column(updatable = false)
	private String creditNumber;

	@Column
	private String policyId;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long creditLimit;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long outstandingBalance;

	@Column
	@Type(type = "StringArrayUserType")
	private String[] products = {};

	@Column
	private String billCycleType;

	@Column
	private LocalDateTime billCycleStart;

	@Column
	private LocalDateTime billCycleEnd;

	@Column
	private String status;

	@Column
	private String issuedByUserId;

	@Column
	@Type(type = "CreditLineAdditionalInfoType")
	private CreditAdditionalInfo additionalInfo;

	@CreationTimestamp
	private LocalDateTime createdOn;

	public static DbCreditLine create(CreditLine dataModel) {
		return new DbCreditLine().from(dataModel);
	}

	@Override
	public CreditLine toDomain() {
		CreditLine creditLine = new GsonMapper<>(this, CreditLine.class).convert();
		creditLine.setBalance(creditLine.getBalance());
		creditLine.setExtendedLimit(creditLine.getExtendedLimit());
		return creditLine;
	}

	@Override
	public DbCreditLine from(CreditLine dataModel) {
		return new GsonMapper<>(dataModel, this, DbCreditLine.class).convert();
	}
}