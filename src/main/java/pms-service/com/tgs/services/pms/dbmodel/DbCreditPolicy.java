package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.gson.BigDecimalUnitToCentAdaptorFactory;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.CreditBillCycleType;
import com.tgs.services.pms.datamodel.CreditAdditionalInfo;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.hibernate.CreditLineAdditionalInfoType;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.envers.Audited;

@Entity(name = "creditPolicy")
@Table(name = "creditPolicy")
@TypeDefs({ @TypeDef(name = "CreditLineAdditionalInfoType", typeClass = CreditLineAdditionalInfoType.class) })
@Getter
@Setter
@Audited
public class DbCreditPolicy extends BaseModel<DbCreditPolicy, CreditPolicy> {

	@Column
	private String policyId;

	@Column
	private long creditLimit;

	@Column
	@Type(type = "StringArrayUserType")
	private String[] products = {};

	@Column
	private CreditBillCycleType billCycleType;

	@Column
	@Type(type = "CreditLineAdditionalInfoType")
	private CreditAdditionalInfo additionalInfo;

	@Column
	@Type(type = "StringArrayUserType")
	private String[] documents = {};

	@CreationTimestamp
	private LocalDateTime createdOn;

	@Override
	public CreditPolicy toDomain() {
		return new GsonMapper<>(this, CreditPolicy.class).convert();
	}

	@Override
	public DbCreditPolicy from(CreditPolicy creditPolicy) {
		return new GsonMapper<>(creditPolicy, this, DbCreditPolicy.class).convert();
	}
}
