package com.tgs.services.pms.dbmodel;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.DepositAdditionalInfo;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.hibernate.DepositAdditionalInfoType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "depositrequest")
@Getter
@Setter
@Audited
@TypeDef(name = "DepositAdditionalInfoType", typeClass = DepositAdditionalInfoType.class)
public class DbDepositRequest extends BaseModel<DbDepositRequest, DepositRequest> {

	@CreationTimestamp
	private LocalDateTime createdOn;

	private LocalDateTime processedOn;
	@Column
	private String userId;
	@Column
	private String partnerId;
	@Column
	private String updateUserId;
	@Column
	private String status;
	@Column
	private String type;
	@Column
	private String mobile;
	@Column
	private Double requestedAmount;
	@Column
	private Double paymentFee;
	@Column
	private String transactionId;
	@Column
	private String depositBranch;

	@Column
	private String depositBank;

	@Column
	@Type(type = "DepositAdditionalInfoType")
	private DepositAdditionalInfo additionalInfo;
	@Column
	private String bank;
	@Column
	private String accountNumber;
	@Column
	private String chequeDrawOnBank;
	@Column
	private LocalDate chequeIssueDate;
	@Column
	private String chequeNumber;
	@Column
	private String comments;

	@Column
	private String reqId;

	@Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}

		if (obj == null || !(obj instanceof DbDepositRequest)) {
			return false;
		}

		DbDepositRequest otherRequest = (DbDepositRequest) obj;

		return getKey().equals(otherRequest.getKey());
	}

	private String getKey() {
		return StringUtils.join(userId, updateUserId, status, type, mobile, String.valueOf(requestedAmount),
				transactionId, depositBranch, bank, accountNumber, chequeDrawOnBank, chequeNumber, comments);
	}
	
	public DepositAdditionalInfo getAdditionalInfo() {
		if (additionalInfo == null) {
			additionalInfo = DepositAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	@Override
	public DepositRequest toDomain() {
		return new GsonMapper<>(this, DepositRequest.class).convert();
	}

	@Override
	public DbDepositRequest from(DepositRequest dataModel) {
		return new GsonMapper<>(dataModel, this, DbDepositRequest.class).convert();
	}
}
