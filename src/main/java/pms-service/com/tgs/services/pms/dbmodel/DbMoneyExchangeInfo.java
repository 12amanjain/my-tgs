package com.tgs.services.pms.dbmodel;

import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;
import javax.persistence.Table;
import javax.persistence.Column;
import javax.persistence.Entity;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.JsonDoubleDefaultSerializer;
import org.hibernate.annotations.CreationTimestamp;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;

@Table(name = "moneyexchange")
@Entity(name = "moneyexchange")
@Getter
@Setter
@Audited
public class DbMoneyExchangeInfo extends BaseModel<DbMoneyExchangeInfo, MoneyExchangeInfo> {

	@Column
	private String type;

	@Column
	private String fromCurrency;

	@Column
	private String toCurrency;

	@Column
	private String markup;
	
	@Column
	private Boolean isEnabled;

	@Column
	@JsonAdapter(JsonDoubleDefaultSerializer.class)
	private Double exchangeRate;

	@Column
	private LocalDateTime processedOn;
	
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Override
	public MoneyExchangeInfo toDomain() {
		return new GsonMapper<>(this, MoneyExchangeInfo.class).convert();
	}

	@Override
	public DbMoneyExchangeInfo from(MoneyExchangeInfo dataModel) {
		return new GsonMapper<>(dataModel, DbMoneyExchangeInfo.class).convert();
	}

}
