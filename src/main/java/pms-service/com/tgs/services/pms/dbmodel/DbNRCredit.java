package com.tgs.services.pms.dbmodel;

import java.time.Duration;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.BigDecimalUnitToCentAdaptorFactory;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.NRCreditAdditionalInfo;
import com.tgs.services.pms.hibernate.NRCreditAdditionalInfoType;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "nonrevolvingcredit")
@Table(name = "nonrevolvingcredit")
@TypeDefs({ @TypeDef(name = "NRCreditAdditionalInfoType", typeClass = NRCreditAdditionalInfoType.class) })
@Getter
@Setter
@Audited
public class DbNRCredit extends BaseModel<DbNRCredit, NRCredit> {

    @Column
    private String userId;

    @Column
    private String partnerId;

    @Column
    private String creditId;

    @Column
    @JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
    private Long creditAmount;

    @Column
    @JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
    private Long utilized;

    @Column
    private LocalDateTime creditExpiry;

    @Column
    private String status;

    @Column
    @Type(type = "StringArrayUserType")
    private String[] products = {};

    @Column
    @Type(type = "NRCreditAdditionalInfoType")
    private NRCreditAdditionalInfo additionalInfo;

    @Column
    private String issuedByUserId;

    @Column
    private Short expiryExtCount;

    @Column
    @CreationTimestamp
    private LocalDateTime createdOn;

    @Column
    private LocalDateTime processedOn;

    @Override
    public NRCredit toDomain() {
        NRCredit credit = new GsonMapper<>(this, NRCredit.class).convert();
        credit.setBalance(credit.getBalance());
        credit.getAdditionalInfo().setCreditExtension(credit.getAdditionalInfo().getOriginalCreditLimit().subtract(credit.getCreditAmount()));
        LocalDateTime now = LocalDateTime.now();
        if (now.isAfter(creditExpiry)) {
            credit.setDueSince(Duration.between(creditExpiry, now));
        }
        return credit;
    }

    @Override
    public DbNRCredit from(NRCredit credit) {
        return new GsonMapper<>(credit, DbNRCredit.class).convert();
    }
}
