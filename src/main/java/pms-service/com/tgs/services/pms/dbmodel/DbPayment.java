package com.tgs.services.pms.dbmodel;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.tgs.services.base.gson.BigDecimalUnitToCentAdaptorFactory;
import com.tgs.services.base.datamodel.Product;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.google.gson.annotations.JsonAdapter;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.hibernate.PaymentAdditionalInfoType;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity(name = "Payment")
@Table(name = "Payment")
@TypeDefs({ @TypeDef(name = "PaymentAdditionalInfoType", typeClass = PaymentAdditionalInfoType.class) })
@Getter
@Setter
@Audited
@ToString
public class DbPayment extends SuperBaseModel<DbPayment, Payment> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "payment_generator")
	@SequenceGenerator(name = "payment_generator", sequenceName = "payment_id_seq", allocationSize = 1)
	private Long id;
	@Column
	private String paymentRefId;
	@Column
	private String payUserId;
	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long amount;
	@SerializedName(value = "refId", alternate = { "bookingId" })
	@Column
	private String refId;
	@Column
	private String amendmentId;
	@Column
	private String status;
	@Column
	private String paymentMedium;
	/**
	 * Transaction Type
	 */
	@Column
	private String type;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long markup;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long tds;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long paymentFee;

	@Column
	private String loggedInUserId;
	
	@Column
	private String partnerId;

	@Column
	@Type(type = "PaymentAdditionalInfoType")
	private PaymentAdditionalInfo additionalInfo;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private String reason;

	private String merchantTxnId;

	@Column
	@JsonAdapter(BigDecimalUnitToCentAdaptorFactory.class)
	private long currentBalance;

	@Access(value = AccessType.PROPERTY)
	@Column
	public String getStatus() {
		return status;
	}

	@Column
	private Long walletId;

	public String getReason() {
		return reason == null ? "" : reason;
	}

	@Override
	public Payment toDomain() {
		Payment payment = new GsonMapper<>(this, Payment.class).convert();
		if (payment.getAmount().compareTo(BigDecimal.ZERO) > 0) {
			payment.setOpType(PaymentOpType.DEBIT);
		} else {
			payment.setOpType(PaymentOpType.CREDIT);
		}
		payment.setProduct(StringUtils.isNotEmpty(refId) ? Product.getProductFromId(refId) : Product.NA);
		payment.setAmendmentType(AmendmentType.getAmendmentTypeFromId(payment.getAmendmentId()));
		return payment;
	}

	@Override
	public DbPayment from(Payment payment) {
		return new GsonMapper<>(payment, this, DbPayment.class).convert();
	}

}
