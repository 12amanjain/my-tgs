package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.hibernate.PaymentBasicRuleExclusionCriteriaType;
import com.tgs.services.pms.hibernate.PaymentBasicRuleInclusionCriteriaType;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleExclusionCriteria;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleInclusionCriteria;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "paymentconfigurationrule")
@TypeDefs({ @TypeDef(name = "PaymentBasicRuleInclusionCriteriaType", typeClass = PaymentBasicRuleInclusionCriteriaType.class),
	@TypeDef(name = "PaymentBasicRuleExclusionCriteriaType", typeClass = PaymentBasicRuleExclusionCriteriaType.class) })
@Audited
public class DbPaymentConfigurationRule extends BaseModel<DbPaymentConfigurationRule, PaymentConfigurationRule> {

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private String medium;

	@Column
	private boolean enabled;

	@Column
	private String ruleType;

	@Column
	@Type(type = "PaymentBasicRuleInclusionCriteriaType")
	private PaymentBasicRuleInclusionCriteria inclusionCriteria;

	@Column
	@Type(type = "PaymentBasicRuleExclusionCriteriaType")
	private PaymentBasicRuleExclusionCriteria exclusionCriteria;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String output;

	@Column
	private double priority;
	
	@Column
	private boolean isDeleted;

	@Column
	private String description;
	

	public static DbPaymentConfigurationRule create(PaymentConfigurationRule rule) {
		return new DbPaymentConfigurationRule().from(rule);
	}

	@Override
	public PaymentConfigurationRule toDomain() {
		return new GsonMapper<>(this, PaymentConfigurationRule.class, true).convert();
	}

	public PaymentConfigurationRule toDomain(PaymentConfigurationRule rule) {
		return new GsonMapper<>(this, rule, PaymentConfigurationRule.class).convert();
	}

	@Override
	public DbPaymentConfigurationRule from(PaymentConfigurationRule rule) {
		return new GsonMapper<>(rule, this, DbPaymentConfigurationRule.class).convert();
	}

}
