package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.gson.DoubleToLongAdapterFactory;
import com.tgs.services.pms.datamodel.UserWallet;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "userwallet")
@Table(name = "userwallet")
@Getter
@Setter
public class DbUserWallet extends BaseModel<DbUserWallet, UserWallet> {
	
	@Column
	private String userId;
	
	@Column
	@JsonAdapter(DoubleToLongAdapterFactory.class)
	private Long balance;
	
	@CreationTimestamp
	private LocalDateTime processedOn;
	
	@Override
	public UserWallet toDomain() {
		return new GsonMapper<>(this, UserWallet.class).convert();
	}

	@Override
	public DbUserWallet from(UserWallet dataModel) {
		return new GsonMapper<>(dataModel, this, DbUserWallet.class).convert();
	}
}
