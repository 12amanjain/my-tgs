package com.tgs.services.pms.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;

import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.PaymentLedgerAdditionalInfo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@Audited
@AllArgsConstructor
@NoArgsConstructor
@Builder
@TypeDefs({ @TypeDef(name = "PaymentLedgerAdditionalInfoType", typeClass = PaymentLedgerAdditionalInfoType.class) })
public class PaymentLedger extends BaseModel {

	@Column
	private long paymentId;
	@Column
	private double amount;
	@Column
	private String status;

	@Column
	private String payUserId;
	/**
	 * Payment debit/credit against which type of operation
	 */
	@Column
	private String type;

	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private double currentBalance;

	@Column
	private String bookingId;

	@Column
	@Type(type = "PaymentLedgerAdditionalInfoType")
	private PaymentLedgerAdditionalInfo additionalInfo;

}
