package com.tgs.services.pms.dbmodel;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;

public class PaymentLedgerAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return PaymentLedgerAdditionalInfoType.class;
	}
}
