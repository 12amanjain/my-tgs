package com.tgs.services.pms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.filters.CreditBillFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.jparepository.CreditBillingService;

@Service
public class CreditBillHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private CreditBillingService creditBillService;

	@Autowired
	public CreditBillHelper(CustomInMemoryHashMap supplierHashMap, GeneralCachingCommunicator cachingCommunicator) {
		super(configurationHashMap);
		CreditBillHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		List<CreditBill> creditBills = creditBillService.search(CreditBillFilter.builder().isSettled(false).build());
		updateBillsInCache(creditBills);
	}

	public void updateBillsInCache(String userId) {
		List<CreditBill> creditBills = creditBillService.search(CreditBillFilter.builder()
				.userIdIn(Arrays.asList(userId)).isInternalQuery(true).isSettled(false).build());
		if (CollectionUtils.isEmpty(creditBills)) {
			cachingCommunicator.delete(CacheMetaInfo.builder().key(userId).set(CacheSetName.CREDIT_BILL.getName())
					.namespace(CacheNameSpace.USERS.getName()).build());
		} else {
			updateBillsInCache(creditBills);
		}
	}

	private void updateBillsInCache(List<CreditBill> creditBills) {
		Map<String, List<CreditBill>> userWiseCreditBill =
				creditBills.stream().collect(Collectors.groupingBy(CreditBill::getUserId));
		userWiseCreditBill.forEach((key, value) -> {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.USERID.getName(), key);
			binMap.put(BinName.CREDITBILL.getName(), GsonUtils.getGson().toJson(value));
			cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.CREDIT_BILL.getName())
					.namespace(CacheNameSpace.USERS.getName()).key(key).build(), binMap, false, true, -1);
		});
	}

	public static Map<String, List<CreditBill>> getCreditBillMap(List<String> userIds) {
		Map<String, List<CreditBill>> userCreditBills = new HashMap<>();
		Map<String, Map<String, String>> userCreditBillMap;
		CacheMetaInfo metaInfo;
		metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.CREDIT_BILL.getName()).keys(userIds.toArray(new String[0])).build();
		userCreditBillMap = cachingCommunicator.get(metaInfo, String.class);

		if (MapUtils.isNotEmpty(userCreditBillMap)) {
			for (Map.Entry<String, Map<String, String>> entrySet : userCreditBillMap.entrySet()) {
				userCreditBills.put(entrySet.getKey(),
						GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.CREDITBILL.getName()),
								new TypeToken<List<CreditBill>>() {}.getType()));
			}
		}
		return userCreditBills;
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.CREDIT_BILL.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	public static List<CreditBill> getCreditBills(String userId) {
		return Optional.ofNullable(getCreditBillMap(Arrays.asList(userId)).get(userId))
				.orElseGet(() -> new ArrayList<>());
	}
}
