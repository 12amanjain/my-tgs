package com.tgs.services.pms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.filters.CreditLineFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.jparepository.CreditLineService;

@Service
public class CreditLineHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private CreditLineService credlineService;

	@Autowired
	public CreditLineHelper(CustomInMemoryHashMap supplierHashMap, GeneralCachingCommunicator cachingCommunicator) {
		super(configurationHashMap);
		CreditLineHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		List<CreditLine> creditLines = credlineService.search(CreditLineFilter.builder()
				.statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED))
				.build());
		updateCreditLineInCache(creditLines);
	}

	public void updateCreditInCache(String userId) {
		// If code has reached here , it means there were no visibility restrictions and creditline has already been updated in the database
		List<CreditLine> creditLines = credlineService.search(CreditLineFilter.builder()
				.userIdIn(Arrays.asList(userId))
				.isInternalQuery(true)
				.statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED))
				.build());
		if (CollectionUtils.isEmpty(creditLines)) {
			cachingCommunicator.delete(CacheMetaInfo.builder().key(userId).set(CacheSetName.CREDIT_LINE.getName())
					.namespace(CacheNameSpace.USERS.getName()).build());
		} else {
			updateCreditLineInCache(creditLines);
		}
	}

	private void updateCreditLineInCache(List<CreditLine> creditLines) {
		Map<String, List<CreditLine>> userWiseCreditLine = creditLines.stream()
				.collect(Collectors.groupingBy(CreditLine::getUserId));
		userWiseCreditLine.forEach((key, value) -> {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.USERID.getName(), key);
			binMap.put(BinName.CREDITLINE.getName(), GsonUtils.getGson().toJson(value));
			cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.CREDIT_LINE.getName())
					.namespace(CacheNameSpace.USERS.getName()).key(key).build(), binMap, false, true, -1);
		});
	}

	@SuppressWarnings({ "serial" })
	public static Map<String, List<CreditLine>> getCreditLineMap(List<String> userIds) {
		Map<String, List<CreditLine>> userCreditLine = new HashMap<>();
		Map<String, Map<String, String>> userCreditLineMap;
		CacheMetaInfo metaInfo;
		metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.CREDIT_LINE.getName()).keys(userIds.toArray(new String[0])).build();
		userCreditLineMap = cachingCommunicator.get(metaInfo, String.class);

		if (MapUtils.isNotEmpty(userCreditLineMap)) {
			for (Entry<String, Map<String, String>> entrySet : userCreditLineMap.entrySet()) {
				userCreditLine.put(entrySet.getKey(), GsonUtils.getGson().fromJson(
						entrySet.getValue().get(BinName.CREDITLINE.getName()), new TypeToken<List<CreditLine>>() {
						}.getType()));
			}
		}
		return userCreditLine;
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.CREDIT_LINE.getName())
                .namespace(CacheNameSpace.USERS.getName()).build());
	}

	public static List<CreditLine> getCreditLines(String userId) {
		return Optional.ofNullable(getCreditLineMap(Arrays.asList(userId)).get(userId))
				.orElseGet(() -> new ArrayList<>());
	}
}
