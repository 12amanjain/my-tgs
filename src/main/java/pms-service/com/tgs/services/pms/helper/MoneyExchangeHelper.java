package com.tgs.services.pms.helper;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.reflect.TypeToken;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import com.tgs.services.pms.jparepository.MoneyExchangeService;

@Service
public class MoneyExchangeHelper extends InMemoryInitializer {

	@Autowired
	private static CustomInMemoryHashMap customMemoryMap;

	@Autowired
	private MoneyExchangeService exchangeService;

	@Autowired
	public MoneyExchangeHelper(CustomInMemoryHashMap supplierHashMap, CustomInMemoryHashMap cachingCommunicator) {
		super(configurationHashMap);
		MoneyExchangeHelper.customMemoryMap = cachingCommunicator;
	}

	@Override
	public void process() {
		List<MoneyExchangeInfo> moneyExchangeList = exchangeService.findAll(MoneyExchangeInfoFilter.builder().build());
		updateExchangeInfoInCache(moneyExchangeList);
	}

	public void updateInCache(String type) {
		List<MoneyExchangeInfo> moneyExchangeList =
				exchangeService.findAll(MoneyExchangeInfoFilter.builder().type(type).build());
		updateExchangeInfoInCache(moneyExchangeList);
	}

	private void updateExchangeInfoInCache(List<MoneyExchangeInfo> exchangeList) {
		Map<String, List<MoneyExchangeInfo>> typeWiseExchangeList = exchangeList.stream()
				.collect(Collectors.groupingBy(MoneyExchangeInfo::getType));
		typeWiseExchangeList.forEach((key, value) -> {
			customMemoryMap.put(key, BinName.MONEYEXCHANGE.getName(), value,
					CacheMetaInfo.builder().set(CacheSetName.MONEY_EXCHANGE.getName())
							.namespace(CacheNameSpace.STATIC_MAP.getName()).compress(false).build());
		});
	}

	@SuppressWarnings({"serial"})
	public static List<MoneyExchangeInfo> getMoneyExchangeList(String type) {
		CacheMetaInfo metaInfo;
		metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.STATIC_MAP.getName()).compress(false)
				.set(CacheSetName.MONEY_EXCHANGE.getName()).keys(new String[] {type})
				.typeOfT(new TypeToken<List<MoneyExchangeInfo>>() {}.getType()).build();
		List<MoneyExchangeInfo> exchangeList =
				customMemoryMap.get(type, BinName.MONEYEXCHANGE.getName(), null, metaInfo);
		if (CollectionUtils.isNotEmpty(exchangeList)) {
			exchangeList = exchangeList.stream().filter(info -> BooleanUtils.isTrue(info.getIsEnabled()))
					.collect(Collectors.toList());
		}
		return exchangeList;
	}
	
	public static MoneyExchangeInfo getExchangeRate(MoneyExchangeInfoFilter exchangeFilter) {
		List<MoneyExchangeInfo> exchangeInfos = getMoneyExchangeList(exchangeFilter.type);
		return filterOnCurrencyCode(exchangeInfos, exchangeFilter);
	}

	private static MoneyExchangeInfo filterOnCurrencyCode(List<MoneyExchangeInfo> moneyExchangeInfos,
			MoneyExchangeInfoFilter exchangeInfoFilter) {
		if (CollectionUtils.isNotEmpty(moneyExchangeInfos)) {
			for (MoneyExchangeInfo exchangeInfo : moneyExchangeInfos) {
				if (exchangeInfo.getFromCurrency().equals(exchangeInfoFilter.fromCurrency)
						&& exchangeInfo.getToCurrency().equals(exchangeInfoFilter.toCurrency)) {
					return exchangeInfo;
				}
			}
		}
		return null;
	}

	@Override
	public void deleteExistingInitializer() {
	}

}
