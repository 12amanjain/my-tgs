package com.tgs.services.pms.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.filters.NRCreditFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.jparepository.NRCreditService;

@Service
public class NRCreditHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	public static final String FIELD = "non_revolving_credit";

	@Autowired
	private NRCreditService service;

	@Autowired
	public NRCreditHelper(CustomInMemoryHashMap supplierHashMap, GeneralCachingCommunicator cachingCommunicator) {
		super(configurationHashMap);
		NRCreditHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		List<NRCredit> credits = service.search(
				NRCreditFilter.builder().statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED)).build());
		updateCreditsInCache(credits);
	}

	public void updateCreditInCache(String userId) {
		List<NRCredit> creditsToCache = service.search(NRCreditFilter.builder()
				.userIdIn(Arrays.asList(userId))
				.isInternalQuery(true)
				.statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED))
				.build());
		if (CollectionUtils.isEmpty(creditsToCache)) {
			cachingCommunicator.delete(CacheMetaInfo.builder().key(userId).set(CacheSetName.NR_CREDIT.getName())
					.namespace(CacheNameSpace.USERS.getName()).build());
		} else {
			updateCreditsInCache(creditsToCache);
		}
	}

	private static void updateCreditsInCache(List<NRCredit> credits) {
		Map<String, List<NRCredit>> creditMap = credits.stream().collect(Collectors.groupingBy(NRCredit::getUserId));
		creditMap.forEach((key, value) -> {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.USERID.getName(), key);
			binMap.put(BinName.NRCREDIT.getName(), GsonUtils.getGson().toJson(value));
			cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.NR_CREDIT.getName())
					.namespace(CacheNameSpace.USERS.getName()).key(key).build(), binMap, false, true, -1);
		});
	}

	@SuppressWarnings({ "serial" })
	public static Map<String, List<NRCredit>> getCreditsByUser(List<String> userIds) {
		Map<String, List<NRCredit>> userCreditMap = new HashMap<>();
		Map<String, Map<String, String>> userCreditCacheResult;
		CacheMetaInfo metaInfo;
		metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.NR_CREDIT.getName()).keys(userIds.toArray(new String[0])).build();
		userCreditCacheResult = cachingCommunicator.get(metaInfo, String.class);

		if (MapUtils.isNotEmpty(userCreditCacheResult)) {
			for (Map.Entry<String, Map<String, String>> entrySet : userCreditCacheResult.entrySet()) {
				userCreditMap.put(entrySet.getKey(), GsonUtils.getGson()
						.fromJson(entrySet.getValue().get(BinName.NRCREDIT.getName()), new TypeToken<List<NRCredit>>() {
						}.getType()));
			}
		}
		return userCreditMap;
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.delete(CacheMetaInfo.builder().set(CacheSetName.NR_CREDIT.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	public static List<NRCredit> getCreditsByUser(String userId) {
		return Optional.ofNullable(getCreditsByUser(Arrays.asList(userId)).get(userId))
				.orElseGet(() -> new ArrayList<>());
	}
}
