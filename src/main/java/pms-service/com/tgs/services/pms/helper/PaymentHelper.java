package com.tgs.services.pms.helper;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.ums.datamodel.DailyUsageInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentHelper {

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private CreditLineHelper creditLineHelper;

	@Autowired
	private NRCreditHelper nrCreditHelper;

	@Autowired
	private CreditBillHelper billHelper;

	@Autowired
	private UserWalletService walletService;

	public void refreshUserBalanceInfoInCache(String payUserId) {
		log.info("Refresh balance in cache for user: " + payUserId);
		creditLineHelper.updateCreditInCache(payUserId);
		nrCreditHelper.updateCreditInCache(payUserId);
		billHelper.updateBillsInCache(payUserId);
		UserWalletHelper.saveInCache(walletService.findByUserId(payUserId).toDomain());
		String loggedInUserId = SystemContextHolder.getContextData().getEmulateOrLoggedInUserId();
		if (!payUserId.equals(loggedInUserId) && loggedInUserId != null) {
			User user = userService.getUserFromCache(loggedInUserId);
			Double limit = user.getAdditionalInfo().getDailyUsageLimit();
			if (limit != null) {
				BigDecimal usage = getTodaysUsage(loggedInUserId);
				userService.storeDailyUsageInfo(DailyUsageInfo.builder().balance(limit - usage.doubleValue())
						.limit(limit).userId(loggedInUserId).build());
			}
		}
	}

	private BigDecimal getTodaysUsage(String userId) {
		PaymentFilter filter = PaymentFilter.builder().loggedInUserIdIn(Collections.singletonList(userId))
				.status(PaymentStatus.SUCCESS).isInternalQuery(true).build();
		filter.setCreatedOnBeforeDate(LocalDate.now().plusDays(1));
		filter.setCreatedOnAfterDate(LocalDate.now());
		List<Payment> dbPayments = paymentService.search(filter);
		return Payment.sum(dbPayments.stream().filter(p -> p.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE)
				|| p.getPaymentMedium().equals(PaymentMedium.WALLET)).collect(Collectors.toList()));
	}

	public void fixClosingBalance(User user, PaymentFilter paymentFilter) {
		paymentFilter.setPayUserIdIn(java.util.Arrays.asList(user.getUserId()));
		paymentFilter.setStatus(PaymentStatus.SUCCESS);
		paymentFilter.setCreatedOnAfterDateTime(LocalDate.now().minusDays(30).atStartOfDay());
		List<Payment> payments = paymentService.search(paymentFilter);
		int index = 0;
		for (int i = 0; i < payments.size(); i++) {
			if (payments.get(i).getPaymentMedium() == PaymentMedium.NETBANKING
					|| payments.get(i).getPaymentMedium() == PaymentMedium.CREDITCARD
					|| payments.get(i).getType() == PaymentTransactionType.CREDIT_EXPIRED
					|| payments.get(i).getType() == PaymentTransactionType.CREDIT_ISSUED) {
				index++;
			} else {
				break;
			}
		}
		if (index == payments.size()
				|| (index != payments.size() && payments.get(index).getAdditionalInfo().getClosingBalance() == null
						&& payments.get(index).getCreatedOn().isAfter(LocalDateTime.now().minusHours(2)))) {
			log.error(
					"Not found any payment entry in last 30 days which has closing balance for userid {}, Therefore searching in last 365 days",
					user.getUserId());
			paymentFilter.setCreatedOnAfterDateTime(LocalDate.now().minusDays(365).atStartOfDay());
		}
		fixClosingBalance(java.util.Arrays.asList(user), paymentFilter);
	}

	public void fixClosingBalance(List<User> users, PaymentFilter paymentFilter) {
		for (User user : users) {
			paymentFilter.setPayUserIdIn(java.util.Arrays.asList(user.getUserId()));
			paymentFilter.setStatus(PaymentStatus.SUCCESS);
			List<Payment> payments = paymentService.search(paymentFilter);

			if (CollectionUtils.isNotEmpty(payments)) {
				int index = 0;
				for (int i = 0; i < payments.size(); i++) {
					if (payments.get(i).getPaymentMedium() == PaymentMedium.NETBANKING
							|| payments.get(i).getPaymentMedium() == PaymentMedium.CREDITCARD
							|| payments.get(i).getType() == PaymentTransactionType.CREDIT_EXPIRED
							|| payments.get(i).getType() == PaymentTransactionType.CREDIT_ISSUED) {
						index++;
					} else {
						break;
					}
				}

				if (index == payments.size()) {
					continue;
				}
				log.info("Payment Filter is {} , Index is {}", GsonUtils.getGson().toJson(paymentFilter), index);
				Payment payment = payments.get(index);
				BigDecimal closingBalance = payment.getAdditionalInfo().getTotalClosingBalance();
				log.error("Fixing closing balance for userId {}, closingbalance is {}", user.getUserId(),
						closingBalance);
				if (closingBalance == null || closingBalance.compareTo(BigDecimal.ZERO) == 0) {
					closingBalance = BigDecimal.ZERO;
					if (payment.getPaymentMedium() == PaymentMedium.WALLET) {
						closingBalance = payment.getCurrentBalance();
					} else if (payment.getPaymentMedium() == PaymentMedium.CREDIT_LINE
							|| payment.getPaymentMedium() == PaymentMedium.CREDIT) {
						closingBalance = payment.getAdditionalInfo().getUtilizedBalance().multiply(new BigDecimal(-1));
					}
				}
				log.error("Closing Balance for userid {} , refId {} is {}", user.getUserId(), payment.getRefId(),
						closingBalance);
				payment.getAdditionalInfo().setTotalClosingBalance(closingBalance);
				DbPayment dbPayment = new DbPayment().from(payment);
				paymentService.save(dbPayment);

				for (int i = index + 1; i < payments.size(); i++) {
					payment = payments.get(i);
					if (payment.getType() == PaymentTransactionType.CREDIT_EXPIRED
							|| payment.getType() == PaymentTransactionType.CREDIT_ISSUED
							|| payment.getPaymentMedium() == PaymentMedium.NETBANKING) {
						continue;
					}

					if (payment.getPaymentMedium() == PaymentMedium.WALLET) {
						closingBalance = closingBalance.subtract(payment.getAmount());
					}
					if (payment.getPaymentMedium() == PaymentMedium.CREDIT_LINE
							|| payment.getPaymentMedium() == PaymentMedium.CREDIT) {
						closingBalance = closingBalance.subtract(payment.getAmount());
					}
					payment.getAdditionalInfo().setTotalClosingBalance(closingBalance);
					log.error("Closing Balance for userid {} , refId {} is {}", user.getUserId(), payment.getRefId(),
							closingBalance);
					dbPayment = new DbPayment().from(payment);
					paymentService.save(dbPayment);
				}
			}

		}
	}
}
