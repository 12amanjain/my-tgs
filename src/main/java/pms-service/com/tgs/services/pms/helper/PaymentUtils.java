package com.tgs.services.pms.helper;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.filters.CreditBillFilter;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.ExternalPayment;
import com.tgs.services.pms.datamodel.ExternalPaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.FundTransferOperation;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.dbmodel.DbExternalPayment;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.manager.RazorpayBasicGateWayManager.RazorpayBasicGateWayManager;
import com.tgs.services.pms.manager.hdfcCcavenue.HdfcCcavenueGatewayManager;
import com.tgs.services.pms.manager.atomAES.AtomAESGatewayManager;
import com.tgs.services.pms.manager.credimax.CredimaxGateWayManager;
import com.tgs.services.pms.manager.easebuzz.EasebuzzGatewayManager;
import com.tgs.services.pms.manager.easypay.EasyPayPaymentManager;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.pms.manager.gateway.HdfcGatewayManager;
import com.tgs.services.pms.manager.payu.PayUGatewayManager;
import com.tgs.services.pms.manager.razorpay.RazorpayGatewayManager;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.SpelExpressionUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentUtils {

	public static BigDecimal totalAmountPR(List<PaymentRequest> payments) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (PaymentRequest payment : payments) {
			try {
				Product pdt =
						ObjectUtils.firstNonNull(payment.getProduct(), Product.getProductFromId(payment.getRefId()));
				if (!pdt.equals(Product.WALLET_TOPUP)
						&& !payment.getTransactionType().equals(PaymentTransactionType.TEMPORARY_LIMIT_CHANGE)) {
					if (getPaymentOpType(payment).equals(PaymentOpType.DEBIT)) {
						totalAmount = totalAmount.add(payment.getAmount().abs());
					} else {
						totalAmount = totalAmount.subtract(payment.getAmount().abs());
					}
				}
			} catch (Exception e) {
				log.error("Unable to fetch amount for payment {}", payment);
			}
		}
		return totalAmount;
	}

	public static BigDecimal totalAmount(List<Payment> payments) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (Payment payment : payments) {

			try {
				Product pdt =
						ObjectUtils.firstNonNull(payment.getProduct(), Product.getProductFromId(payment.getRefId()));
				if (!pdt.equals(Product.WALLET_TOPUP)
						&& !payment.getType().equals(PaymentTransactionType.TEMPORARY_LIMIT_CHANGE)) {
					if (getPaymentOpType(payment).equals(PaymentOpType.DEBIT)) {
						totalAmount = totalAmount.add(payment.getAmount().abs());
					} else {
						totalAmount = totalAmount.subtract(payment.getAmount().abs());
					}
				}
			} catch (Exception e) {
				log.error("Unable to fetch amount for payment {}", payment);
			}
		}
		return totalAmount;
	}

	public static GateWayManager getGatewayManager(GateWayType gatewayType) {
		switch (gatewayType) {
			case ATOM_PAY:
				return (GateWayManager) SpringContext.getApplicationContext().getBean("atomGatewayManager");
			case EASYPAY_ICICI:
				return SpringContext.getApplicationContext().getBean(EasyPayPaymentManager.class);
			case HDFC_FSSNET:
				return SpringContext.getApplicationContext().getBean(HdfcGatewayManager.class);
			case RAZOR_PAY:
				return SpringContext.getApplicationContext().getBean(RazorpayGatewayManager.class);
			case RAZOR_PAY_BASIC:
				return SpringContext.getApplicationContext().getBean(RazorpayBasicGateWayManager.class);
			case PAYU:
				return SpringContext.getApplicationContext().getBean(PayUGatewayManager.class);
			case EASEBUZZ:
				return SpringContext.getApplicationContext().getBean(EasebuzzGatewayManager.class);
			case ATOM_AES:
				return SpringContext.getApplicationContext().getBean(AtomAESGatewayManager.class);
			case CREDIMAX:
				return SpringContext.getApplicationContext().getBean(CredimaxGateWayManager.class);
			case HDFC_CCAVENUE:
				return SpringContext.getApplicationContext().getBean(HdfcCcavenueGatewayManager.class);
			default:
				return null;
		}
	}

	@Deprecated
	public static BigDecimal getPaymentFee(User user, BigDecimal amount, Integer ruleId,
			Map<String, Double> attributes) {
		if (ruleId == null) {
			return BigDecimal.ZERO;
		}
		PaymentConfigurationRule rule =
				PaymentConfigurationHelper.getPaymentRule(ruleId, PaymentRuleType.PAYMENT_MEDIUM);
		PaymentConfigOutput output = (PaymentConfigOutput) rule.getOutput();
		String paymentFeeExp = output.getPaymentFeeExpression();
		if (user != null && user.getAdditionalInfo().getPaymentFees() != null) {
			paymentFeeExp = user.getAdditionalInfo().getPaymentFees().getOrDefault(rule.getMedium(), paymentFeeExp);
		}
		return getPaymentFee(amount, paymentFeeExp, attributes);
	}

	public static BigDecimal getPaymentFee(User user, BigDecimal amount, PaymentConfigurationRule rule,
			Map<String, Double> attributes) {
		PaymentConfigOutput output = (PaymentConfigOutput) rule.getOutput();
		String paymentFeeExp = output.getPaymentFeeExpression();
		if (user != null && user.getAdditionalInfo().getPaymentFees() != null) {
			paymentFeeExp = user.getAdditionalInfo().getPaymentFees().getOrDefault(rule.getMedium(), paymentFeeExp);
		}
		return getPaymentFee(amount, paymentFeeExp, attributes);
	}

	public static BigDecimal getPaymentFee(BigDecimal amount, String paymentFeeExp, Map<String, Double> attributes) {
		if (StringUtils.isBlank(paymentFeeExp)) {
			return BigDecimal.ZERO;
		}
		if (attributes == null)
			attributes = new HashMap<>();
		attributes.put("amt", amount.doubleValue());
		return BigDecimal.valueOf(SpelExpressionUtils.evaluateExpression(paymentFeeExp, attributes)).setScale(2,
				BigDecimal.ROUND_HALF_UP);
	}

	public static PaymentOpType getPaymentOpType(PaymentRequest paymentRequest) {
		return ObjectUtils.firstNonNull(paymentRequest.getOpType(), paymentRequest.getTransactionType().getOpType());
	}

	private static PaymentOpType getPaymentOpType(Payment payment) {
		return ObjectUtils.firstNonNull(payment.getOpType(), payment.getType().getOpType());
	}

	public static boolean isDepositRequestRejected(DepositRequestStatus status) {
		return DepositRequestStatus.ABORTED.equals(status) || DepositRequestStatus.REJECTED.equals(status);
	}

	public static BigDecimal getTotalDueBillAmount(CreditBillingService billingService, Long creditId, String userId,
			boolean isInternalQuery) {
		BigDecimal totalDueAmount = BigDecimal.ZERO;
		List<CreditBill> bills = billingService.search(CreditBillFilter.builder().isSettled(false).creditId(creditId)
				.userIdIn(Arrays.asList(userId)).isInternalQuery(isInternalQuery).build());
		for (CreditBill bill : bills) {
			totalDueAmount = totalDueAmount.add(bill.getPendingAmount());
		}
		return totalDueAmount;
	}

	public static boolean forBillSettlement(Payment payment) {
		return (payment.getType().equals(PaymentTransactionType.BILL_SETTLE)
				|| PaymentTransactionType.BILL_SETTLE.equals(payment.getAdditionalInfo().getActualTransactionType()))
				&& StringUtils.isNotEmpty(payment.getAdditionalInfo().getBillNumber());
	}

	public static AreaRole getAreaRoleConfig(FundTransferOperation transferOperation) {
		switch (transferOperation) {
			case FUND_ALLOCATE:
				return AreaRole.FUND_ALLOCATE;
			case FUND_RECALL:
				return AreaRole.FUND_RECALL;
			case PAYMENT:
				return AreaRole.FUND_PAYMENT;
		}
		return null;
	}

	public static ExternalPayment toExternalPayment(PaymentRequest extPaymentRequest) {
		PaymentAdditionalInfo additionalInfo = extPaymentRequest.getAdditionalInfo();
		return ExternalPayment.builder().refId(extPaymentRequest.getRefId()).payUserId(extPaymentRequest.getPayUserId())
				.additionalInfo(ExternalPaymentAdditionalInfo.builder().amount(extPaymentRequest.getAmount())
						.gatewayType(additionalInfo.getGateway()).ruleId(additionalInfo.getRuleId())
						.merchantTxnId(extPaymentRequest.getMerchantTxnId()).build())
				.build();
	}

	public static Payment toPayment(DbExternalPayment externalPayment) {
		ExternalPaymentAdditionalInfo additionalInfo = externalPayment.getAdditionalInfo();
		return Payment.builder().refId(externalPayment.getRefId()).payUserId(externalPayment.getPayUserId())
				.amount(additionalInfo.getAmount()).merchantTxnId(additionalInfo.getMerchantTxnId())
				.additionalInfo(PaymentAdditionalInfo.builder().gateway(additionalInfo.getGatewayType())
						.ruleId(additionalInfo.getRuleId()).build())
				.build();
	}


	public static List<Payment> mergePayment(List<Payment> paymentList) {
		if (CollectionUtils.isEmpty(paymentList))
			return paymentList;

		Map<String, Payment> tempMap = new HashMap<>();

		for (Iterator<Payment> iter = paymentList.iterator(); iter.hasNext();) {
			Payment payment = iter.next();
			if (payment.getStatus() == PaymentStatus.SUCCESS) {
				String key = payment.getPayUserId() + "-" + payment.getRefId() + "-" + payment.getType() + "-"
						+ payment.getOpType();

				if (tempMap.get(key) == null)
					tempMap.put(key, payment);
				else {
					Payment oldPayment = tempMap.get(key);
					/**
					 * List is sorted based on createdon , then oldPayment and newPayment createdon diff is less than 1
					 * min then it will be considered as part of 1 single transaction . Therefore it will be merge
					 */
					if (oldPayment.getCreatedOn().isAfter(payment.getCreatedOn().minusMinutes(1))) {
						oldPayment.setMarkup(oldPayment.getMarkup().add(payment.getMarkup()));
						oldPayment.getAdditionalInfo()
								.setTotalClosingBalance(payment.getAdditionalInfo().getTotalClosingBalance());
						oldPayment.setAmount(oldPayment.getAmount().add(payment.getAmount()));
						oldPayment.setPaymentFee(oldPayment.getPaymentFee().add(payment.getPaymentFee()));
						oldPayment.setTds(oldPayment.getTds().add(payment.getTds()));
						iter.remove();
					} else {
						tempMap.put(key, payment);
					}
				}
			}
		}

		return paymentList;
	}

	public static <T> Predicate<T> distinctByKey(Function<? super T, ?> keyExtractor) {
		Set<Object> seen = ConcurrentHashMap.newKeySet();
		return t -> seen.add(keyExtractor.apply(t));
	}
}
