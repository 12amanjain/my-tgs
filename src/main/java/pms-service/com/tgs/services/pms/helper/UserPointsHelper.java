package com.tgs.services.pms.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.tgs.filters.UserPointFilter;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.UserPoint;
import com.tgs.services.pms.datamodel.UserPointsCacheFilter;
import com.tgs.services.pms.jparepository.UserPointsService;

@Service
public class UserPointsHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private UserPointsService userPointsService;;

	public UserPointsHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		UserPointsHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		Runnable task = () -> {
			for (int i = 0; i < 500; i++) {
				UserPointFilter filter = UserPointFilter.builder().build();
				PageAttributes pageAttr = new PageAttributes();
				pageAttr.setPageNumber(i);
				pageAttr.setSize(5000);
				SortByAttributes srtBy = new SortByAttributes();
				srtBy.setParams(Arrays.asList("id"));
				srtBy.setOrderBy("asc");
				pageAttr.setSortByAttr(Arrays.asList(srtBy));
				filter.setPageAttr(pageAttr);
				List<UserPoint> userPointsList = BaseModel.toDomainList(userPointsService.search(filter));
				userPointsList.forEach(userPoints -> saveInCache(userPoints));
				if (CollectionUtils.isEmpty(userPointsList))
					break;
			}
		};
		Thread dataThread = new Thread(task);
		dataThread.start();

	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_POINTS.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	public static void saveInCache(UserPoint userPoints) {
		if (userPoints == null) {
			return;
		}
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.USERPOINTS.getName(), GsonUtils.getGson().toJson(userPoints));
		binMap.put(BinName.USERPOINTSTYPE.getName(), userPoints.getType().getCode());
		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.USER_POINTS.getName())
						.namespace(CacheNameSpace.USERS.getName()).key(userPoints.getUserId()).build(),
				binMap, false, true, -1);
	}

	public static Map<String, Map<PointsType, UserPoint>> getUserPoints(UserPointsCacheFilter userPointsFilter) {
		Map<String, Map<PointsType, UserPoint>> userPointsFromCache = new HashMap<>();
		Map<String, Map<String, String>> userPoints = null;
		CacheMetaInfo metaInfo = null;
		if (CollectionUtils.isNotEmpty(userPointsFilter.getUserIds())) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
					.set(CacheSetName.USER_POINTS.getName()).keys(userPointsFilter.getUserIds().toArray(new String[0]))
					.build();
			userPoints = cachingCommunicator.get(metaInfo, String.class);
		} else if (userPointsFilter.getType() != null) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
					.set(CacheSetName.USER_POINTS.getName()).build();
			userPoints = cachingCommunicator.getResultSet(metaInfo, String.class,
					Filter.equal(BinName.USERPOINTSTYPE.getName(), userPointsFilter.getType()));
		}
		if (MapUtils.isNotEmpty(userPoints)) {
			for (Entry<String, Map<String, String>> entrySet : userPoints.entrySet()) {
				if (entrySet.getValue().get(BinName.USERPOINTS.getName()) != null) {
					UserPoint pointsBlc = GsonUtils.getGson()
							.fromJson(entrySet.getValue().get(BinName.USERPOINTS.getName()), UserPoint.class);
					Map<PointsType, UserPoint> userPointsMap =
							userPointsFromCache.getOrDefault(entrySet.getKey(), new HashMap<>());
					userPointsMap.put(pointsBlc.getType(), pointsBlc);
					userPointsFromCache.put(entrySet.getKey(), userPointsMap);
				}
			}
		}
		return userPointsFromCache;
	}

	public static Map<PointsType, UserPoint> fetchUserPoints(String userId) {
		return getUserPoints(UserPointsCacheFilter.builder().userIds(Arrays.asList(userId)).build()).get(userId);
	}

	public static UserPoint fetchUserPoints(String userId, PointsType type) {
		Map<PointsType, UserPoint> userPointsMap =
				getUserPoints(UserPointsCacheFilter.builder().userIds(Arrays.asList(userId)).build()).get(userId);
		return MapUtils.isNotEmpty(userPointsMap) ? userPointsMap.get(type) : null;
	}

}
