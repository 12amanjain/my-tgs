package com.tgs.services.pms.helper;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserWalletFilter;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.jparepository.UserWalletService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserWalletHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private UserWalletService walletService;;

	public UserWalletHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		UserWalletHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		Runnable task = () -> {
			for (int i = 0; i < 500; i++) {
				UserWalletFilter filter = UserWalletFilter.builder().build();
				PageAttributes pageAttr = new PageAttributes();
				pageAttr.setPageNumber(i);
				pageAttr.setSize(5000);
				SortByAttributes srtBy = new SortByAttributes();
				srtBy.setParams(Arrays.asList("id"));
				srtBy.setOrderBy("asc");
				pageAttr.setSortByAttr(Arrays.asList(srtBy));
				filter.setPageAttr(pageAttr);
				List<UserWallet> userList = walletService.search(filter);
				log.info("[UserWalletHelper Thread] Fetched user wallet info from database, info list size is {} ", userList.size());
				userList.forEach(userWallet -> saveInCache(userWallet));
				if (CollectionUtils.isEmpty(userList))
					break;
			}
		};
		Thread dataThread = new Thread(task);
		dataThread.start();
		
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_BALANCE.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	public static void saveInCache(UserWallet userWallet) {
		if (userWallet == null) {
			return;
		}
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.USERBALANCE.getName(), GsonUtils.getGson().toJson(userWallet));
		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.USER_BALANCE.getName())
						.namespace(CacheNameSpace.USERS.getName()).key(userWallet.getUserId()).build(),
				binMap, false, true, -1);
	}

	public static Map<String, UserWallet> getUserWallet(List<String> userIds) {
		Map<String, UserWallet> userWalletsFromCache = new HashMap<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.USER_BALANCE.getName()).keys(userIds.toArray(new String[0])).build();
		Map<String, Map<String, String>> userMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(userMap)) {
			for (Entry<String, Map<String, String>> entrySet : userMap.entrySet()) {
				if (entrySet.getValue().get(BinName.USERBALANCE.getName()) != null) {
					UserWallet walletBalance = GsonUtils.getGson()
							.fromJson(entrySet.getValue().get(BinName.USERBALANCE.getName()), UserWallet.class);
					userWalletsFromCache.put(entrySet.getKey(), walletBalance);
				}
			}
		}
		return userWalletsFromCache;
	}

	public static UserWallet fetchUserWallet(String userId) {
		return getUserWallet(Arrays.asList(userId)).get(userId);
	}
}
