package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.datamodel.BillAdditionalInfo;

public class BillAdditionalInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return BillAdditionalInfo.class;
    }
}
