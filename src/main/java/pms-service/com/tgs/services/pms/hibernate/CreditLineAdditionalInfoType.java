package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.datamodel.CreditAdditionalInfo;

public class CreditLineAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return CreditAdditionalInfo.class;
	}
}
