package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.datamodel.ExternalPaymentAdditionalInfo;

public class ExternalPaymentAdditionalInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return ExternalPaymentAdditionalInfo.class;
	}
}
