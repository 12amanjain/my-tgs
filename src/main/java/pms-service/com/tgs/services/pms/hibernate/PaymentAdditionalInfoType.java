package com.tgs.services.pms.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;

public class PaymentAdditionalInfoType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return PaymentAdditionalInfo.class;
    }
}
