package com.tgs.services.pms.jparepository;

import com.tgs.services.pms.dbmodel.DbCreditBill;
import com.tgs.services.pms.dbmodel.DbCreditLine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public interface CreditBillRepository
        extends JpaRepository<DbCreditBill, Long>, JpaSpecificationExecutor<DbCreditBill> {

    DbCreditBill findByBillNumber(String billNumber);

    List<DbCreditBill> findByUserIdAndIsSettledOrderByCreatedOnAsc(String userId, boolean isSettled);

    List<DbCreditBill> findByIsSettledAndPaymentDueDateBeforeAndPaymentDueDateAfter(boolean isSettled, LocalDateTime dateTime, LocalDateTime afterDateTime);

    List<DbCreditBill> findByCreditId(long id);

    @Query(value = "select * from creditbill where createdon > (CURRENT_TIMESTAMP - INTERVAL '30 DAYS') " +
            "AND TO_TIMESTAMP(additionalinfo->>'extensionRecallTime', 'YYYY/MM/DD HH24:MI:SS') < CURRENT_TIMESTAMP",
            nativeQuery = true)
    List<DbCreditBill> findBoundedExtensionsToRecall();

}
