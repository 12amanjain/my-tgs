package com.tgs.services.pms.jparepository;

import com.google.gson.Gson;
import com.tgs.filters.CreditBillFilter;
import com.tgs.filters.CreditLineFilter;
import com.tgs.services.base.AuditResult;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditBillDetail;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.dbmodel.DbCreditBill;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.helper.CreditBillHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Slf4j
public class CreditBillingService extends SearchService<DbCreditBill> {

	@Autowired
	private CreditLineService creditLineService;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private CreditBillRepository repository;

	@Autowired
	private CreditBillHelper creditBillHelper;

	@Autowired
	private AuditsHandler auditsHandler;

	// used for dependant search
	public List<DbCreditBill> search(QueryFilter filter) {
		return search(filter, repository);
	}

	public List<CreditBill> search(CreditBillFilter filter) {
		return BaseModel.toDomainList(search(filter, repository));
	}

	public List<CreditBill> findUnsettledBillsByUserId(String userId) {
		return BaseModel.toDomainList(repository.findByUserIdAndIsSettledOrderByCreatedOnAsc(userId, false));
	}

	public List<CreditBill> findBillsToLock(Short scanHours) {
		LocalDateTime now = LocalDateTime.now();
		CreditBillFilter billFilter = CreditBillFilter.builder()
				.isSettled(false)
				.lockDateGreaterThan(now.minusHours(scanHours))
				.lockDateLessThan(now)
				.build();
		return search(billFilter).stream().filter(b -> !b.getAdditionalInfo().isThresholdSettled()).collect(Collectors.toList());
	}

	public CreditBill findBillByBillNumber(String billNumber) {
		return repository.findByBillNumber(billNumber).toDomain();
	}

	public List<CreditBill> findBoundedExtensionsToRecall() {
		List<DbCreditBill> dbCreditBills = repository.findBoundedExtensionsToRecall();
		return BaseModel.toDomainList(dbCreditBills);
	}

	public CreditBill save(CreditBill bill) {
		if (bill.getId() == null) {  // new bill
			if (duplicate(bill)) return bill;
		}
		bill.setProcessedOn(LocalDateTime.now());
		bill = repository.saveAndFlush(new DbCreditBill().from(bill)).toDomain();
		creditBillHelper.updateBillsInCache(bill.getUserId());
		return bill;
	}

	private boolean duplicate(CreditBill bill) {
		CreditBillFilter filter = CreditBillFilter.builder()
				.userIdIn(Collections.singletonList(bill.getUserId()))
				.createdOnAfterDate(LocalDate.now().minusDays(30))
				.build();
		List<CreditBill> oldBills = search(filter);
		for (CreditBill oldBill : oldBills) {
			if (oldBill.getAdditionalInfo().getCycleStart().equals(bill.getAdditionalInfo().getCycleStart()) && oldBill.getCreditId().equals(bill.getCreditId())) {
				log.error("DUPLICATE BILL => " + new Gson().toJson(bill));
				return true;
			}
		}
		return false;
	}

	public void settledDateMigration(CreditBillFilter filter) {
		filter.setIsSettled(true);
		log.info("SettledDateMigration: filter received: {}", new Gson().toJson(filter));
		List<DbCreditBill> bills = super.search(filter, repository);
		log.info("SettledDateMigration: count = {}, startDate = {}", bills.size(), filter.getCreatedOnAfterDate());
		for (DbCreditBill bill : bills) {
			AuditsRequest auditsRequest = new AuditsRequest();
			auditsRequest.setId(bill.getBillNumber());
			List<AuditResult> auditList = auditsHandler.fetchAudits(auditsRequest, DbCreditBill.class, "billNumber");
			log.info("auditList Size: {}", auditList.size());
			List<DbCreditBill> billAudits = new ArrayList<>();
			auditList.forEach(x -> billAudits.add((DbCreditBill) x.getActualValue()));
			//log.info("bill audits: {}", new Gson().toJson(billAudits));
			Optional<DbCreditBill> firstSettled = billAudits.stream().filter(b -> b.getIsSettled()).min(Comparator.comparing(x -> x.getProcessedOn()));
			if (firstSettled.isPresent()) {
				bill.getAdditionalInfo().setSettledDate(firstSettled.get().getProcessedOn());
				save(bill.toDomain());
			} else {
				log.info("SettledDateMigration: settled date not found for billNumber = {}, startDate = {}", bill.getBillNumber(), filter.getCreatedOnAfterDate());
			}
		}
	}


	// region Detailed-Bill operations and wrappers
	public List<CreditBillDetail> getPendingBillDetailsByUserId(String userId) {
		List<CreditBill> creditBills = findUnsettledBillsByUserId(userId);
		return fillBillDetails(toBillDetails(creditBills));
	}

	private List<CreditBillDetail> fillBillDetails(List<CreditBillDetail> creditBills) {
		fillPaymentDetails(creditBills);
		fillCreditLineDetails(creditBills);
		return creditBills;
	}

	private void fillPaymentDetails(List<CreditBillDetail> creditBills) {
		List<Long> paymentIds = new ArrayList<>();
		creditBills.forEach(b -> {
			paymentIds.addAll(b.getAdditionalInfo().getChargedPaymentIds());
			paymentIds.addAll(b.getAdditionalInfo().getSettlementPaymentIds());
		});

		List<DbPayment> dbPayments = paymentService.findByIds(paymentIds);
		List<com.tgs.services.pms.datamodel.Payment> payments = new ArrayList<>();
		GsonMapper paymentMapper = new GsonMapper<>(com.tgs.services.pms.datamodel.Payment.class);
		dbPayments.forEach(p -> {
			paymentMapper.setInput(p);
			payments.add((com.tgs.services.pms.datamodel.Payment) paymentMapper.convert());
		});
	}

	private void fillCreditLineDetails(List<CreditBillDetail> creditBills) {
		List<Long> creditIds = new ArrayList<>();
		creditBills.forEach(b -> creditIds.add(b.getCreditId()));

		List<CreditLine> creditLines = creditLineService.search(CreditLineFilter.builder().idIn(creditIds).build());
		creditBills.forEach(b -> {
			Optional<CreditLine> optional = creditLines.stream().filter(cl -> cl.getId().equals(b.getCreditId()))
					.findFirst();
			if (!optional.isPresent())
				throw new RuntimeException("Credit Line not found for walletId " + b.getCreditId());
			b.setCreditLine(optional.get());
		});
	}

	private static List<CreditBillDetail> toBillDetails(List<CreditBill> bills) {
		List<CreditBillDetail> billDetails = new ArrayList<>();
		GsonMapper mapper = new GsonMapper<>(CreditBillDetail.class);
		bills.forEach(b -> {
			mapper.setInput(b);
			billDetails.add((CreditBillDetail) mapper.convert());
		});
		return billDetails;
	}

	// endregion
}
