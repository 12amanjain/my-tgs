package com.tgs.services.pms.jparepository;

import com.tgs.filters.DepositRequestFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.dbmodel.DbDepositRequest;
import com.tgs.utils.exception.PaymentException;
import org.postgresql.util.PSQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class DepositRequestService extends SearchService<DbDepositRequest> {

	@Autowired
	private DepositRequestRepository depositRepo;

	public DbDepositRequest save(DbDepositRequest dbRequest) {
		try {
			dbRequest.setProcessedOn(LocalDateTime.now());
			return depositRepo.saveAndFlush(dbRequest);
		} catch (Exception ex) {
			if (duplicateRCNo(ex)) throw new CustomGeneralException(SystemError.DUPLICATE_RC_NUMBER);
			else throw ex;
		}
	}

	public DbDepositRequest save(DepositRequest dbRequest) {
		try {
			dbRequest.setProcessedOn(LocalDateTime.now());
			return depositRepo.saveAndFlush(new DbDepositRequest().from(dbRequest));
		} catch (Exception ex) {
			if (duplicateRCNo(ex)) throw new CustomGeneralException(SystemError.DUPLICATE_RC_NUMBER);
			else throw ex;
		}
	}

	public List<DepositRequest> findAll(@Valid DepositRequestFilter filter) {
		return BaseModel.toDomainList(super.search(filter, depositRepo));
	}

	public DepositRequest fetchById(String reqId) {
		DbDepositRequest depositRequest = depositRepo.findByReqId(reqId);
		if (depositRequest == null)
			throw new PaymentException(SystemError.RESOURCE_NOT_FOUND);
		return depositRequest.toDomain();
	}

	public DbDepositRequest fetchByReqId(String reqId) {
		return depositRepo.findByReqId(reqId);
	}

	public List<DbDepositRequest> fetchByTransactionIdOrderByCreatedOn(String transactionId) {
		return depositRepo.findByTransactionIdOrderByCreatedOn(transactionId);
	}

	public List<DbDepositRequest> getRequestsReceivedInLastMinutes(String userId, int minutes) {
		return depositRepo.getRequestsReceivedAfterTimeStamp(userId,
				Timestamp.valueOf(LocalDateTime.now().minusMinutes(minutes)));
	}

	private boolean duplicateRCNo(Throwable throwable) {
		int c = 0;
		while (throwable.getCause() != null && c < 10) {
			Throwable innerEx = throwable.getCause();
			if (innerEx instanceof PSQLException && innerEx.getMessage().contains("unq_idx_rcno")) {
				return true;
			}
			throwable = innerEx;
			c++;
		}
		return false;
	}
}
