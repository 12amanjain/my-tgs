package com.tgs.services.pms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.tgs.services.pms.dbmodel.DbExternalPayment;

@Repository
public interface ExernalPaymentRepository
		extends JpaRepository<DbExternalPayment, Long>, JpaSpecificationExecutor<DbExternalPayment> {

	@Modifying
	@Query(value = "UPDATE externalpayment SET additionalinfo = jsonb_set(additionalinfo, '{gs}', to_jsonb(cast(:status AS text))) WHERE refid = :refId",
			nativeQuery = true)
	public void updateGatewayStatus(@Param("refId") String refId, @Param("status") String status);

	public DbExternalPayment findByRefId(String refId);

}
