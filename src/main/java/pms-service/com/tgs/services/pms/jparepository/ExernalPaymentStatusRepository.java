package com.tgs.services.pms.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import com.tgs.services.pms.dbmodel.DbExternalPaymentStatusInfo;

@Repository
public interface ExernalPaymentStatusRepository
		extends JpaRepository<DbExternalPaymentStatusInfo, Long>, JpaSpecificationExecutor<DbExternalPaymentStatusInfo> {

}
