package com.tgs.services.pms.jparepository;

import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.ExternalPaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.ExternalPayment;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.dbmodel.DbExternalPayment;
import com.tgs.services.pms.dbmodel.DbExternalPaymentStatusInfo;

@Service
public class ExternalPaymentService extends SearchService<DbExternalPayment> {

	@Autowired
	private ExernalPaymentRepository repository;

	@Autowired
	private ExernalPaymentStatusRepository statusRepository;

	public List<DbExternalPayment> search(ExternalPaymentFilter filter) {
		return super.search(filter, repository);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void storePaymentInitiationInfo(ExternalPayment externalPayment) {
		DbExternalPayment existingExternalPayment =
				Optional.ofNullable(repository.findByRefId(externalPayment.getRefId())).orElse(new DbExternalPayment());
		if (!PaymentStatus.SUCCESS.getCode().equals(existingExternalPayment.getStatus())) {
			externalPayment.setStatus(PaymentStatus.INITIATE_REDIRECTION);
			DbExternalPayment dbExternalPayment = existingExternalPayment.from(externalPayment);
			repository.save(dbExternalPayment);
		}
		DbExternalPaymentStatusInfo dbExternalPaymentStatus = DbExternalPaymentStatusInfo.builder()
				.refId(externalPayment.getRefId()).status(PaymentStatus.INITIATE_REDIRECTION.getCode()).build();
		statusRepository.save(dbExternalPaymentStatus);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateExternalPayment(ExternalPayment externalPaymentUpdate) {
		DbExternalPayment dbExternalPayment = repository.findByRefId(externalPaymentUpdate.getRefId());
		if (dbExternalPayment == null) {
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		}
		removeNonUpdatableFields(externalPaymentUpdate);
		dbExternalPayment = dbExternalPayment.from(externalPaymentUpdate);
		repository.save(dbExternalPayment);
	}

	private void removeNonUpdatableFields(ExternalPayment externalPaymentUpdate) {
		externalPaymentUpdate.setCreatedOn(null);
		externalPaymentUpdate.setPayUserId(null);
	}

	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void savePaymentStatus(ExternalPaymentStatusInfo paymentStatus) {
		repository.updateGatewayStatus(paymentStatus.getRefId(), paymentStatus.getStatus().getCode());
		DbExternalPaymentStatusInfo dbExternalPaymentStatus = new DbExternalPaymentStatusInfo().from(paymentStatus);
		statusRepository.save(dbExternalPaymentStatus);
	}

}
