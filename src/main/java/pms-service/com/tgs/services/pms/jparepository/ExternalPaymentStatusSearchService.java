package com.tgs.services.pms.jparepository;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.ExternalPaymentStatusInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.pms.dbmodel.DbExternalPaymentStatusInfo;

@Service
public class ExternalPaymentStatusSearchService extends SearchService<DbExternalPaymentStatusInfo> {

	@Autowired
	private ExernalPaymentStatusRepository statusRepository;

	public List<DbExternalPaymentStatusInfo> search(ExternalPaymentStatusInfoFilter filter) {
		return super.search(filter, statusRepository);
	}
}
