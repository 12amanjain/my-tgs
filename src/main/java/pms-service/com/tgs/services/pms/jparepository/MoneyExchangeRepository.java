package com.tgs.services.pms.jparepository;

import org.springframework.stereotype.Repository;
import com.tgs.services.pms.dbmodel.DbMoneyExchangeInfo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

@Repository
public interface MoneyExchangeRepository
		extends JpaRepository<DbMoneyExchangeInfo, Long>, JpaSpecificationExecutor<DbMoneyExchangeInfo> {

}
