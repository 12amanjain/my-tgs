package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SearchService;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import com.tgs.services.pms.dbmodel.DbMoneyExchangeInfo;
import com.tgs.services.pms.helper.MoneyExchangeHelper;

@Service
public class MoneyExchangeService extends SearchService<DbMoneyExchangeInfo> {

	@Autowired
	MoneyExchangeRepository moneyExchangeRepo;

	@Autowired
	MoneyExchangeHelper exchangeHelper;

	public MoneyExchangeInfo save(MoneyExchangeInfo moneyExchangeInfo) {
		try {
			DbMoneyExchangeInfo dbMoneyExchangeInfo = null;
			if (Objects.nonNull(moneyExchangeInfo.getId())) {
				dbMoneyExchangeInfo = moneyExchangeRepo.findOne(moneyExchangeInfo.getId());
			} else {
				moneyExchangeInfo.setIsEnabled(true);
			}
			dbMoneyExchangeInfo = new GsonMapper<>(moneyExchangeInfo, dbMoneyExchangeInfo, DbMoneyExchangeInfo.class)
					.convert();
			dbMoneyExchangeInfo.setProcessedOn(LocalDateTime.now());
			return moneyExchangeRepo.saveAndFlush(dbMoneyExchangeInfo).toDomain();
		} finally {
			exchangeHelper.updateInCache(moneyExchangeInfo.getType());
		}
	}

	public List<MoneyExchangeInfo> findAll(MoneyExchangeInfoFilter filter) {
		return DbMoneyExchangeInfo.toDomainList(super.search(filter, moneyExchangeRepo));
	}

	public DbMoneyExchangeInfo findById(Long id) {
		return moneyExchangeRepo.findOne(id);
	}

	public MoneyExchangeInfo updateStatus(Long id, Boolean isEnabled) {
		DbMoneyExchangeInfo dbMoneyExchangeInfo = findById(id);
		if (dbMoneyExchangeInfo != null) {
			MoneyExchangeInfo moneyExchangeInfo = dbMoneyExchangeInfo.toDomain();
			moneyExchangeInfo.setIsEnabled(isEnabled);
			exchangeHelper.updateInCache(moneyExchangeInfo.getType());
			return save(moneyExchangeInfo);
		}
		return null;
	}
	
	public void saveAll(List<MoneyExchangeInfo> exchangeInfoList) {
		List<DbMoneyExchangeInfo> dbList = exchangeInfoList.stream()
				.map(info -> new DbMoneyExchangeInfo().from(info)).collect(Collectors.toList());
		moneyExchangeRepo.save(dbList);
	}
}
