package com.tgs.services.pms.jparepository;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.NRCreditFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.dbmodel.DbNRCredit;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.helper.NRCreditHelper;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class NRCreditService extends SearchService<DbNRCredit> {

	@Autowired
	protected NRCreditRepository repository;

	@Autowired
	private NRCreditHelper creditHelper;

	@Autowired
	@Lazy
	private PaymentServiceCommunicator paymentCommunicator;

	public NRCredit getByCreditId(String creditId) {
		DbNRCredit credit = repository.getByCreditId(creditId);
		if (credit == null) {
			log.error("NR Credit not found for creditId {}", creditId);
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		}
		return credit.toDomain();
	}

	public NRCredit getByIdOrNumber(Long id, String creditNumber) {
		if (StringUtils.isNotBlank(creditNumber)) {
			return getByCreditId(creditNumber);
		}
		if (id != null) {
			return get(id);
		}
		log.error("NR Credit not found for creditId {}", creditNumber);
		throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
	}

	public NRCredit get(Long id) {
		DbNRCredit credit = repository.findOne(id);
		if (credit == null) {
			log.error("NR Credit not found for Id {}", id);
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		}
		return credit.toDomain();
	}

	public NRCredit save(NRCredit credit) {
		credit.setProcessedOn(LocalDateTime.now());
		NRCredit savedCredit = repository.save(new DbNRCredit().from(credit)).toDomain();
		savedCredit.setBalance(savedCredit.getBalance());
		creditHelper.updateCreditInCache(credit.getUserId());
		return savedCredit;
	}

	public List<NRCredit> search(NRCreditFilter filter) {
		return BaseModel.toDomainList(search(filter, repository));
	}

	public List<NRCredit> getCurrentCredits(String userId) {
		List<DbNRCredit> creditList = repository.getByUserIdAndStatusIn(userId, Arrays.asList(CreditStatus.ACTIVE.getCode(),
				CreditStatus.BLOCKED.getCode()));
		if (CollectionUtils.isEmpty(creditList))
			return new ArrayList<>();
		return BaseModel.toDomainList(creditList);
	}

	public List<NRCredit> getCurrentUnSettledCredits(String userId) {
		NRCreditFilter filter = NRCreditFilter.builder()
				.utilizedGreaterThan(BigDecimal.ZERO)
				.userIdIn(Collections.singletonList(userId))
				.statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED))
				.build();
		List<NRCredit> creditList = search(filter);
		if (CollectionUtils.isEmpty(creditList))
			return new ArrayList<>();
		return creditList;
	}

	public static String generateCreditId(String userId) {
		return new StringBuilder().append("NRC").append(userId).append("-V")
				.append(RandomStringUtils.random(3, false, true)).toString();
	}

	public List<NRCredit> getUsableCredits(String userId, Product product, BigDecimal amount) {
		Stream<NRCredit> nrCreditList = search(NRCreditFilter.builder()
				.userIdIn(Collections.singletonList(userId))
				.statusIn(Collections.singletonList(CreditStatus.ACTIVE)).build())
				.stream().filter(c -> (c.getUtilized().add(amount)).compareTo(c.getCreditAmount()) < 0
						&& c.getCreditExpiry().isAfter(LocalDateTime.now()));
		if (product != null) {
			nrCreditList = nrCreditList.filter(c -> c.getProducts().contains(product));
		}
		return nrCreditList.collect(Collectors.toList());
	}

	@Transactional
	public NRCredit transact(BigDecimal transactionAmount, NRCredit credit, boolean reduceLimit) {
		if (transactionAmount.compareTo(BigDecimal.ZERO) > 0 && transactionAmount.compareTo(credit.getBalance()) > 0) {
			throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
		} else {
			credit.setUtilized(credit.getUtilized().add(transactionAmount));
			credit.setBalance(credit.getBalance());
			if (reduceLimit) {
				credit.setCreditAmount(credit.getCreditAmount().add(transactionAmount));
			}
			save(credit);
		}
		return credit;
	}

	public Map<DbNRCredit, List<Payment>> getNRCreditsToBill() {
		Map<DbNRCredit, List<Payment>> map = new HashMap<>();
		List<Object[]> NRCreditsWithPayments = repository.findCreditsToBeBilled();
		NRCreditsWithPayments.forEach(entry -> {
			DbNRCredit nrCredit = (DbNRCredit) entry[0];
			DbPayment dbPayment = (DbPayment) entry[1];
			if (map.containsKey(nrCredit) && dbPayment != null) {
				map.get(nrCredit).add(dbPayment.toDomain());
			} else {
				List<Payment> payments = new ArrayList<>();
				if (dbPayment != null) payments.add(dbPayment.toDomain());
				map.put(nrCredit, payments);
			}
		});
		return map;
	}

	public void delete(Long id) {
		repository.delete(id);
	}

	@Transactional
	public void expireCredit(NRCredit credit) {
		if (credit.getStatus().equals(CreditStatus.EXPIRED)) return;
		BigDecimal diff = credit.getCreditAmount();
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder()
				.utilizedBalance(credit.getUtilized())
				.allowForDisabledMediums(true)
				.closingBalance(credit.getBalance().subtract(diff))
				.build();
		WalletPaymentRequest request = WalletPaymentRequest.builder()
				.amount(diff)
				.paymentMedium(PaymentMedium.CREDIT)
				.additionalInfo(additionalInfo)
				.transactionType(PaymentTransactionType.CREDIT_EXPIRED)
				.payUserId(credit.getUserId())
				.opType(PaymentOpType.DEBIT)
				.walletId(credit.getId())
				.walletNumber(credit.getCreditId())
				.reason(credit.getAdditionalInfo().getComments())
				.build();
		paymentCommunicator.doPaymentsUsingPaymentRequests(Stream.of(request).collect(Collectors.toList()));
		credit.setCreditAmount(BigDecimal.ZERO);
		credit.setCreditExpiry(BaseUtils.min(LocalDateTime.now(), credit.getCreditExpiry()));
		credit.setStatus(CreditStatus.EXPIRED);
		save(credit);
	}
}
