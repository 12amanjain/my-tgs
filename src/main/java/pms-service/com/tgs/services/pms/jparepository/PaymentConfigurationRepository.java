package com.tgs.services.pms.jparepository;


import com.tgs.services.pms.dbmodel.DbPaymentConfigurationRule;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PaymentConfigurationRepository extends JpaRepository<DbPaymentConfigurationRule, Long>, JpaSpecificationExecutor<DbPaymentConfigurationRule> {

    public List<DbPaymentConfigurationRule> findByEnabledAndMedium(Boolean enabled, String medium);

	public List<DbPaymentConfigurationRule> findByEnabledOrderByProcessedOnDesc(Boolean enabled);

}
