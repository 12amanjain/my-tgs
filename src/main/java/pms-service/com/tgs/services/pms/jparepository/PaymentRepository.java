package com.tgs.services.pms.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.pms.dbmodel.DbPayment;

@Repository
public interface PaymentRepository extends JpaRepository<DbPayment, Long>, JpaSpecificationExecutor<DbPayment> {

	List<DbPayment> findByRefIdOrderByProcessedOnDesc(String refId);

	List<DbPayment> findByIdIn(List<Long> ids);

	List<DbPayment> findByPaymentRefIdIn(List<String> paymentRefIds);
	
	@Modifying(clearAutomatically = true)
	@Query(value = "delete from payment where refid=:refid and status in ('F', 'IR')", nativeQuery = true)
	void deleteFailedPaymentsByRefId(@Param("refid") String refid);

}