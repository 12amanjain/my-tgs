package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserPointFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.pms.dbmodel.DbUserPoint;
import com.tgs.services.pms.helper.UserPointsHelper;

@Service
public class UserPointsService extends SearchService<DbUserPoint> {

	@Autowired
	private UserPointsRepository pointsRepo;

	public DbUserPoint save(DbUserPoint userPoints) {
		userPoints.setProcessedOn(LocalDateTime.now());
		userPoints = pointsRepo.saveAndFlush(userPoints);
		UserPointsHelper.saveInCache(userPoints.toDomain());
		return userPoints;
	}

	public List<DbUserPoint> findByUserId(String userId) {
		return pointsRepo.findByUserId(userId);
	}

	public List<DbUserPoint> search(UserPointFilter filter) {
		return super.search(filter, pointsRepo);
	}

	public DbUserPoint getDbUserPoints(String userId, PointsType type) {
		List<DbUserPoint> userPoints =
				search(UserPointFilter.builder().userIdIn(Arrays.asList(userId)).typeIn(Arrays.asList(type)).build());
		DbUserPoint dbUserPoints = CollectionUtils.isNotEmpty(userPoints) ? userPoints.get(0)
				: DbUserPoint.builder().userId(userId).type(type.getCode()).balance(0d).build();
		return dbUserPoints;
	}


}
