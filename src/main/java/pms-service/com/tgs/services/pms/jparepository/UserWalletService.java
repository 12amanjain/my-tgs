package com.tgs.services.pms.jparepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.UserWalletFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.dbmodel.DbUserWallet;
import com.tgs.services.pms.helper.UserWalletHelper;

@Service
public class UserWalletService extends SearchService<DbUserWallet> {

	@Autowired
	private UserWalletRepository walletRepo;

	public DbUserWallet save(DbUserWallet userBalance) {
		userBalance.setProcessedOn(LocalDateTime.now());
		walletRepo.saveAndFlush(userBalance);
		UserWalletHelper.saveInCache(userBalance.toDomain());
		return userBalance;
	}
	
	public DbUserWallet findByUserId(String userId) {
		return walletRepo.findByUserId(userId);
	}
	
	public List<UserWallet> search(UserWalletFilter filter) {
		return BaseModel.toDomainList(super.search(filter, walletRepo));
	}

}
