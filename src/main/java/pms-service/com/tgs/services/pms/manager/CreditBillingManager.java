package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoField;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.tgs.filters.CreditBillFilter;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.AirOrderItemCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HotelOrderItemCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.Incidence.AutoUnlockCredit;
import com.tgs.services.gms.datamodel.Incidence.BillDueNotification;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import com.tgs.services.gms.datamodel.Incidence.IncidenceType;
import com.tgs.services.hms.datamodel.Address;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.pms.datamodel.BillAdditionalInfo;
import com.tgs.services.pms.datamodel.CalendarCycles;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditBillCycleType;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.CreditType;
import com.tgs.services.pms.datamodel.Cycle;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentDetail;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.TemporaryExtensionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.dbmodel.DbCreditLine;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.restmodel.UpdateBillRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@Qualifier("CreditBillingManager")
public class CreditBillingManager {

	@Autowired
	protected CreditPolicyService policyService;

	@Autowired
	private CreditBillingService billingService;

	@Autowired
	private CreditLineService creditLineService;

	@Autowired
	private CreditSystemMessagingClient messagingClient;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	@Lazy
	private PaymentServiceCommunicator paymentCommunicator;

	@Autowired
	private AirOrderItemCommunicator airOrderItemService;

	@Autowired
	private HotelOrderItemCommunicator hotelOrderItemService;

	@Autowired
	private OrderServiceCommunicator orderService;

	@Autowired
	private GeneralServiceCommunicator generalService;

	private boolean generateBills;

	public List<CreditBill> generateBillsRecur() {
		generateBills = true;
		short counter = 0;
		List<CreditBill> bills = new ArrayList<>();
		while (generateBills && counter < 5) {
			bills.addAll(checkAndGenerateLineBills());
			counter++;
		}
		return bills;
	}

	private List<CreditBill> checkAndGenerateLineBills() {
		generateBills = false;
		Map<DbCreditLine, List<Payment>> creditPaymentMap = creditLineService.getCreditLinesToBill();
		List<CreditBill> bills = new ArrayList<>();
		creditPaymentMap.forEach((dbCreditLine, paymentList) -> {
			// Need to maintain separate copies of old and new creditLines, do not change
			// this logic
			CreditLine oldCreditLine = dbCreditLine.toDomain();
			CreditLine nuwCreditLine = dbCreditLine.toDomain();
			CreditBill creditBill = generateLineBill(nuwCreditLine, paymentList, false);
			if (creditBill != null) {
				bills.add(creditBill);
				messagingClient.sendCreditBillMail(creditBill, oldCreditLine.toCredit(),
						EmailTemplateKey.CREDITBILL_STATEMENT_EMAIL, Duration.ZERO);
			}
			generateBills = generateMore(nuwCreditLine);
		});
		return bills;
	}

	private boolean generateMore(CreditLine nuw) {
		return nuw.getBillCycleEnd() != null && nuw.getBillCycleEnd().isBefore(LocalDateTime.now());
	}

	public static List<Payment> filterPaymentsToBill(List<Payment> paymentList, PaymentMedium medium) {
		return paymentList.stream().filter(p -> p.includeInBill() && p.getPaymentMedium().equals(medium))
				.collect(Collectors.toList());
	}

	@Transactional
	public CreditBill generateLineBill(CreditLine creditLine, List<Payment> paymentList, boolean expire) {
		log.info("Bill generate request. CreditLine: " + new Gson().toJson(creditLine));
		CreditBill creditBill = null;
		paymentList = filterPaymentsToBill(paymentList, PaymentMedium.CREDIT_LINE);
		BigDecimal billAmount = BigDecimal.ZERO;
		for (Payment p : paymentList) {
			billAmount = billAmount.add(p.getAmount());
		}
		billAmount = billAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
		crossCheckOutstandingAndBillAmount(creditLine, billAmount);
		if (billAmount.compareTo(BigDecimal.ZERO) > 0) {
			short lockAfterHours = creditLine.getAdditionalInfo().getLockAfterHours();
			LocalDateTime paymentDate = creditLine.getBillCycleType().equals(CreditBillCycleType.CALENDAR)
					? creditLine.getAdditionalInfo().getDueDate() != null ? creditLine.getAdditionalInfo().getDueDate()
							: creditLine.getAdditionalInfo().getLockDate()
					: LocalDateTime.now().plusHours(creditLine.getAdditionalInfo().getPaymentDueHours());
			// For calendar
			LocalDateTime lockDate = lockAfterHours < 0 ? LocalDateTime.MAX
					: creditLine.getBillCycleType().equals(CreditBillCycleType.CALENDAR)
							? creditLine.getAdditionalInfo().getLockDate().plusHours(lockAfterHours)
							: paymentDate.plusHours(lockAfterHours);
			BillAdditionalInfo additionalInfo = BillAdditionalInfo.builder().creditType(CreditType.REVOLVING)
					.chargedPaymentIds(paymentList.stream().map(Payment::getId).collect(Collectors.toSet()))
					.originalDueDate(paymentDate).originalLockDate(lockDate).cycleStart(creditLine.getBillCycleStart())
					.cycleEnd(expire ? LocalDateTime.now() : creditLine.getBillCycleEnd())
					.policy(creditLine.getPolicyId())
					.isThresholdSettled(billAmount.compareTo(BigDecimal.valueOf(500)) < 0).build();
			if (creditLine.getAdditionalInfo().getTemporaryExtensionType().equals(TemporaryExtensionType.BOUNDED)
					&& creditLine.getAdditionalInfo().getCurTemporaryExt().compareTo(BigDecimal.ZERO) > 0 && !expire) {
				LocalDateTime extensionRecallTime = paymentDate.plusHours(lockAfterHours);
				additionalInfo.setExtensionRecallTime(extensionRecallTime);
				additionalInfo.setExtensionAmount(creditLine.getAdditionalInfo().getCurTemporaryExt());
			}
			creditBill = CreditBill.builder().billNumber(RandomStringUtils.random(10, false, true))
					.userId(creditLine.getUserId()).creditId(creditLine.getId()).paymentDueDate(paymentDate)
					.additionalInfo(additionalInfo).lockDate(lockDate).billAmount(billAmount).isSettled(false).build();
			billingService.save(creditBill);
		} else if (billAmount.compareTo(BigDecimal.ZERO) < 0) {
			log.error("ERROR: Bill amount is negative for creditline {} => {}", creditLine.getCreditNumber(),
					billAmount);
		}
		resetCycle(creditLine, expire, creditBill == null);
		creditLineService.save(creditLine);
		return creditBill;
	}

	private void crossCheckOutstandingAndBillAmount(CreditLine creditLine, BigDecimal billAmount) {
		List<CreditBill> unsettledBills = billingService.findUnsettledBillsByUserId(creditLine.getUserId());
		unsettledBills = unsettledBills.stream().filter(b -> b.getCreditId().equals(creditLine.getId()))
				.collect(Collectors.toList());
		BigDecimal dues = BigDecimal.ZERO;
		for (CreditBill b : unsettledBills) {
			dues = dues.add(b.getPendingAmount());
		}
		if (!dues.add(billAmount).equals(creditLine.getOutstandingBalance())) {
			log.error(
					"Outstanding and Bill Sum mismatch for creditline number {}. Outstanding Sum: {}, Pending Bill Sum: {}",
					creditLine.getCreditNumber(), creditLine.getOutstandingBalance(), dues.add(billAmount));
		}
	}

	public List<CreditLine> recallBoundedExtension() {
		List<CreditBill> creditBills = billingService.findBoundedExtensionsToRecall();
		List<CreditLine> creditLineList = new ArrayList<>();
		creditBills.forEach(b -> creditLineList.add(recallBoundedExtension(b)));
		return creditLineList;
	}

	@Transactional
	private CreditLine recallBoundedExtension(CreditBill creditBill) {
		CreditLine creditLine = creditLineService.findByCreditNumberOrId(null, creditBill.getCreditId());
		// need the check because during migration, while converting BOUNDED TLE to
		// UNBOUNDED it may already have a bill with recallTime set
		if (creditLine.getAdditionalInfo().getTemporaryExtensionType().equals(TemporaryExtensionType.UNBOUNDED))
			return creditLine;
		recallTemporaryExtension(creditLine, creditBill.getAdditionalInfo().getExtensionAmount());
		creditBill.getAdditionalInfo().setExtensionAmount(null);
		creditBill.getAdditionalInfo().setExtensionRecallTime(null);
		billingService.save(creditBill);
		creditLineService.save(creditLine);
		return creditLine;
	}

	public void unlock(CreditBill bill) {
		if (bill.getAdditionalInfo().getCreditType().equals(CreditType.NON_REVOLVING))
			return;
		CreditLine creditLine = creditLineService.findByCreditNumberOrId(null, bill.getCreditId());
		if (creditLine.getStatus().equals(CreditStatus.EXPIRED))
			return;
		short autoUnlockMaxDays;
		List<Incidence> incidenceCollection = getIncidences(creditLine, IncidenceType.UNLOCK_CREDIT);
		if (!CollectionUtils.isEmpty(incidenceCollection)) {
			autoUnlockMaxDays = ((AutoUnlockCredit) incidenceCollection.get(0).getIncidenceData())
					.getAutoUnlockMaxDays();
			if (bill.getPaymentDueDate().plusDays(autoUnlockMaxDays).isBefore(LocalDateTime.now()))
				return;
		}
		creditLine.setStatus(CreditStatus.ACTIVE);
		creditLineService.save(creditLine);
	}

	public void bdn(Short scanHoursAhead) {
		LocalDateTime scanBillsUntil = LocalDateTime.now().plusHours(scanHoursAhead);
		List<CreditBill> bills = billingService
				.search(CreditBillFilter.builder().paymentDueDateGreaterThan(LocalDateTime.now())
						.paymentDueDateLessThan(scanBillsUntil).isSettled(false).build());
		bills = bills.stream().filter(b -> !b.getAdditionalInfo().isBpnSent()).collect(Collectors.toList());
		Map<Long, CreditBill> billMap = bills.stream().collect(Collectors.toMap(CreditBill::getCreditId, b -> b));
		List<CreditLine> creditLines = creditLineService.search(CreditLineFilter.builder()
				.idIn(bills.stream().map(CreditBill::getCreditId).collect(Collectors.toList())).build());
		LocalDateTime now = LocalDateTime.now();
		creditLines.forEach(creditLine -> {
			List<Incidence> incidenceList = getIncidences(creditLine, IncidenceType.BILL_DUE_NOTIFICATION);
			if (!CollectionUtils.isEmpty(incidenceList)) {
				Short notifyBeforeHours = ((BillDueNotification) incidenceList.get(0).getIncidenceData())
						.getNotifyBeforeHours();
				CreditBill bill = billMap.get(creditLine.getId());
				LocalDateTime notifyTime = bill.getPaymentDueDate().minusHours(notifyBeforeHours);
				if (notifyTime.isBefore(now) || Math.abs(Duration.between(notifyTime, now).toMinutes()) < 60) {
					// TODO: Send Bill Due mail
					bill.getAdditionalInfo().setBpnSent(true);
					billingService.save(bill);
				}
			}
		});
	}

	/*
	 * private boolean partialSettlement(CreditBill bill, CreditLine creditLine) {
	 * List<Incidence> incidenceCollection = getIncidences(creditLine,
	 * IncidenceType.PARTIAL_BILL_PAY); if
	 * (!CollectionUtils.isEmpty(incidenceCollection)) { Short minPercent =
	 * ((PartialBillPayment)
	 * incidenceCollection.get(0).getIncidenceData()).getBillPayPercent(); Double
	 * minAmount = bill.getBillAmount() * minPercent / 100; if
	 * (bill.getSettledAmount() >= minAmount) {
	 * bill.getAdditionalInfo().setThresholdSettled(true);
	 * billingService.save(bill); return true; } } return false; }
	 */

	private List<Incidence> getIncidences(CreditLine creditLine, IncidenceType incidenceType) {
		return new ArrayList<>();
	}

	@Transactional(isolation = Isolation.REPEATABLE_READ)
	public CreditBill updateBill(UpdateBillRequest updateBillRequest) {
		CreditBill bill = billingService.findBillByBillNumber(updateBillRequest.getBillNumber());
		if (updateBillRequest.getNewLockDate() != null) {
			if (updateBillRequest.getNewLockDate().isBefore(LocalDateTime.now())
					|| updateBillRequest.getNewLockDate().isBefore(bill.getLockDate()))
				throw new CustomGeneralException(SystemError.CB_INVALID_LOCK_DATE);
			Duration extension = Duration.between(bill.getLockDate(), updateBillRequest.getNewLockDate());
			bill.setLockDate(updateBillRequest.getNewLockDate());
			bill.setExtensionCount(bill.getExtensionCount() + 1);
			if (bill.getAdditionalInfo().getCreditType().equals(CreditType.REVOLVING)) {
				CreditLine creditLine = creditLineService.findByCreditNumberOrId(null, bill.getCreditId());
				if (updateBillRequest.getStatus() != null && !creditLine.getStatus().equals(CreditStatus.EXPIRED)) {
					creditLine.setStatus(updateBillRequest.getStatus());
				} else if (creditLine.getStatus().equals(CreditStatus.BLOCKED)) {
					creditLine.setStatus(CreditStatus.ACTIVE);
				}
				creditLineService.save(creditLine);
			}
			billingService.save(bill);
			messagingClient.sendCreditBillMail(bill, null, EmailTemplateKey.CREDITBILL_PAYMENT_EXTENSION_EMAIL,
					extension);
		}
		return bill;
	}

	public List<CreditBill> getPendingBills(String userId) {
		return billingService.findUnsettledBillsByUserId(userId);
	}

	public List<PaymentDetail> getBillPayments(String billNumber) {
		List<PaymentDetail> paymentDetailList = new ArrayList<>();
		PaymentDetail paymentDetail = new PaymentDetail();
		try {
			CreditBill bill = billingService.findBillByBillNumber(billNumber);
			List<Long> paymentIds = new ArrayList<>();
			paymentIds.addAll(bill.getAdditionalInfo().getChargedPaymentIds());
			paymentIds.addAll(bill.getAdditionalInfo().getSettlementPaymentIds());
			List<Payment> payments = paymentService
					.search(PaymentFilter.builder().idIn(paymentIds).status(PaymentStatus.SUCCESS).build());
			List<String> amendmentIds = new ArrayList<>();
			List<String> airBookingIds = new ArrayList<>();
			List<String> hotelBookingIds = new ArrayList<>();
			for (Payment payment : payments) {
				PaymentDetail pd = new PaymentDetail();
				pd.setPaymentDetail(payment);
				paymentDetailList.add(pd);
				if (StringUtils.isNotEmpty(payment.getAmendmentId())) {
					amendmentIds.add(payment.getAmendmentId());
				}
				if (StringUtils.isNotEmpty(payment.getRefId())
						&& Product.getProductFromId(payment.getRefId()).equals(Product.AIR)) {
					airBookingIds.add(payment.getRefId());
				}
				if (StringUtils.isNotEmpty(payment.getRefId())
						&& Product.getProductFromId(payment.getRefId()).equals(Product.HOTEL)) {
					hotelBookingIds.add(payment.getRefId());
				}
			}
			List<String> allBookingIds = Stream.concat(airBookingIds.stream(), hotelBookingIds.stream())
					.collect(Collectors.toList());
			Map<String, List<HotelOrderItem>> hotelOrderItemMap = hotelOrderItemService
					.getHotelOrderItems(hotelBookingIds).stream()
					.collect(Collectors.groupingBy(HotelOrderItem::getBookingId));
			Map<String, List<AirOrderItem>> airOrderItemMap = airOrderItemService.getAirOrderItems(airBookingIds)
					.stream().collect(Collectors.groupingBy(AirOrderItem::getBookingId));
			Map<String, Order> orderMap = orderService.getOrders(allBookingIds).stream()
					.collect(Collectors.toMap(Order::getBookingId, x -> x));
			for (int i = 0; i < paymentDetailList.size(); i++) {
				paymentDetail = paymentDetailList.get(i);
				if (StringUtils.isNotEmpty(paymentDetail.getRefId())
						&& Product.getProductFromId(paymentDetail.getRefId()).equals(Product.AIR)) {
					setPaymentAirDetails(airOrderItemMap.get(paymentDetail.getRefId()),
							orderMap.get(paymentDetail.getRefId()), paymentDetail);
				}
				if (StringUtils.isNotEmpty(paymentDetail.getRefId())
						&& Product.getProductFromId(paymentDetail.getRefId()).equals(Product.HOTEL)) {
					setPaymentHotelDetails(hotelOrderItemMap.get(paymentDetail.getRefId()),
							orderMap.get(paymentDetail.getRefId()), paymentDetail);
				}
			}
		} catch (Exception ex) {
			log.error("ERROR while fetching payments for billNumber: {} & OrderId: {}. Exception: {}", billNumber,
					paymentDetail.getRefId(), ex);
			throw ex;
		}
		return paymentDetailList;
	}

	public void resetCycle(CreditLine creditLine, boolean expire, boolean noBill) {
		if (!expire) {
			resetCycleDates(creditLine);
			resetDynamicCycle(creditLine);
		}
		if ((creditLine.getAdditionalInfo().getTemporaryExtensionType().equals(TemporaryExtensionType.BOUNDED)
				&& noBill) || expire) {
			recallTemporaryExtension(creditLine, null);
		}
	}

	private void resetDynamicCycle(CreditLine creditLine) {
		if (creditLine.getBillCycleType().equals(CreditBillCycleType.DYNAMIC)) {
			List<Payment> paymentList = paymentService.search(PaymentFilter.builder()
					.createdOnAfterDateTime(creditLine.getBillCycleEnd()).paymentMedium(PaymentMedium.CREDIT_LINE)
					.payUserIdIn(Collections.singletonList(creditLine.getUserId())).status(PaymentStatus.SUCCESS)
					.build());
			paymentList = paymentList.stream()
					.filter(p -> creditLine.getId() != null && creditLine.getId().equals(p.getWalletId()))
					.collect(Collectors.toList());
			// Check if any payment is made after previous cycle ended, if yes we start the
			// new cycle from that time.
			if (CollectionUtils.isNotEmpty(paymentList)) {
				paymentList.sort(Comparator.comparing(Payment::getCreatedOn));
				creditLine.setBillCycleStart(
						LocalDateTime.of(paymentList.get(0).getCreatedOn().toLocalDate(), LocalTime.MIN));
				creditLine.setBillCycleEnd(LocalDateTime.of(paymentList.get(0).getCreatedOn().toLocalDate()
						.plusDays(creditLine.getAdditionalInfo().getBillCycleDays() - 1), LocalTime.MAX));
			} else {
				creditLine.setBillCycleEnd(null);
				creditLine.setBillCycleStart(null);
			}
		}
	}

	private void resetCycleDates(CreditLine creditLine) {
		if (creditLine.getBillCycleEnd() != null && creditLine.getBillCycleEnd().isAfter(LocalDateTime.now()))
			return;
		LocalDate nextCycleStartDate = creditLine.getBillCycleEnd() != null
				? creditLine.getBillCycleEnd().toLocalDate().plusDays(1)
				: LocalDate.now();
		if (creditLine.getBillCycleType().equals(CreditBillCycleType.NON_REVOLVING)) {
			if (creditLine.getBillCycleEnd() == null) { // CL create
				creditLine.setBillCycleStart(LocalDateTime.of(nextCycleStartDate, LocalTime.MIN));
				creditLine.setBillCycleEnd(LocalDateTime.of(
						nextCycleStartDate.plusDays(creditLine.getAdditionalInfo().getBillCycleDays() - 1),
						LocalTime.MAX));
			} else { // CL cycle ended
				creditLine.setBillCycleEnd(null);
				creditLine.setBillCycleStart(null);
			}
		}
		creditLine.setBillCycleStart(LocalDateTime.of(nextCycleStartDate, LocalTime.MIN));
		if (creditLine.getBillCycleType().equals(CreditBillCycleType.FIXED_NUM_OF_DAYS)) {
			creditLine.setBillCycleEnd(LocalDateTime.of(
					nextCycleStartDate.plusDays(creditLine.getAdditionalInfo().getBillCycleDays() - 1), LocalTime.MAX));
		}

		if (creditLine.getBillCycleType().equals(CreditBillCycleType.FIXED_DAY_OF_WEEK)) {
			int days = (7 - nextCycleStartDate.getDayOfWeek().getValue()
					+ creditLine.getAdditionalInfo().getBillingDOW().getValue()) % 7;
			creditLine.setBillCycleEnd(LocalDateTime.of(nextCycleStartDate.plusDays(days), LocalTime.MAX));
		}

		if (creditLine.getBillCycleType().equals(CreditBillCycleType.FIXED_DAY_OF_MONTH)) {
			LocalDate cycleEnd = nextCycleStartDate;
			if (nextCycleStartDate.getDayOfMonth() >= creditLine.getAdditionalInfo().getBillingDOM()) {
				cycleEnd = nextCycleStartDate.plusMonths(1);
			}
			cycleEnd = cycleEnd.with(ChronoField.DAY_OF_MONTH, creditLine.getAdditionalInfo().getBillingDOM());
			creditLine.setBillCycleEnd(LocalDateTime.of(cycleEnd, LocalTime.MAX));
		}
		if (creditLine.getBillCycleType().equals(CreditBillCycleType.CALENDAR)) {
			creditLine.setBillCycleStart(null);
			creditLine.setBillCycleEnd(null);
			String bspCalendarKey = StringUtils.isNotBlank(creditLine.getAdditionalInfo().getAssociatedCalendar())
					? creditLine.getAdditionalInfo().getAssociatedCalendar() + "_" + LocalDate.now().getYear()
					: "";
			List<Cycle> cycles = getCalendar(bspCalendarKey);
			LocalDateTime now = LocalDateTime.now();
			for (Cycle cycle : cycles) {
				LocalDateTime cycleStartTime = LocalDateTime.of(cycle.getStart(), LocalTime.MIN);
				LocalDateTime cycleEndTime = LocalDateTime.of(cycle.getEnd(), LocalTime.MAX);
				if (now.isAfter(cycleStartTime) && now.isBefore(cycleEndTime)) {
					creditLine.setBillCycleStart(cycleStartTime);
					creditLine.setBillCycleEnd(cycleEndTime);
					creditLine.getAdditionalInfo().setLockDate(LocalDateTime.of(cycle.getLock(), LocalTime.MAX));
					if (cycle.getDue() != null) {
						creditLine.getAdditionalInfo().setDueDate(LocalDateTime.of(cycle.getDue(), LocalTime.MAX));
					}
					break;
				}
			}
			if (creditLine.getBillCycleStart() == null) {
				log.error("Unable to set next Calendar cycle for creditLine: {}", creditLine.getCreditNumber());
				throw new CustomGeneralException(SystemError.CANNOT_SET_CYCLE);
			}
		}
	}

	private List<Cycle> getCalendar(String calendarKey) {
		List<Document> docs = generalService.fetchDocument(CollectionServiceFilter.builder().key(calendarKey).build());
		if (CollectionUtils.isEmpty(docs))
			throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
		CalendarCycles collection = GsonUtils.getGson().fromJson(docs.get(0).getData(), CalendarCycles.class);
		List<Cycle> cycles = collection.getCycles();
		cycles.sort(Comparator.comparing(Cycle::getStart));
		return cycles;
	}

	private void recallTemporaryExtension(CreditLine creditLine, BigDecimal recallAmount) {
		recallAmount = recallAmount == null ? creditLine.getAdditionalInfo().getCurTemporaryExt()
				: recallAmount.min(creditLine.getAdditionalInfo().getCurTemporaryExt());
		creditLine.getAdditionalInfo()
				.setCurTemporaryExt(creditLine.getAdditionalInfo().getCurTemporaryExt().subtract(recallAmount));
		if (recallAmount.compareTo(BigDecimal.ZERO) > 0) {
			PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder()
					.utilizedBalance(creditLine.getOutstandingBalance()).closingBalance(creditLine.getBalance())
					.allowForDisabledMediums(true).build();
			WalletPaymentRequest request = WalletPaymentRequest.builder().amount(recallAmount)
					.paymentMedium(PaymentMedium.CREDIT_LINE).additionalInfo(additionalInfo)
					.transactionType(PaymentTransactionType.TEMPORARY_LIMIT_CHANGE).payUserId(creditLine.getUserId())
					.opType(PaymentOpType.DEBIT).walletNumber(creditLine.getCreditNumber()).walletId(creditLine.getId())
					.build();
			paymentCommunicator.doPaymentsUsingPaymentRequests(Stream.of(request).collect(Collectors.toList()));
		}
	}

	private void setPaymentAirDetails(List<AirOrderItem> airOrderItemList, Order order, PaymentDetail paymentDetail) {
		if (CollectionUtils.isEmpty(airOrderItemList) || order == null)
			return;
		StringBuilder summaryBuilder = new StringBuilder();
		airOrderItemList.forEach(a -> summaryBuilder.append(a.getAirlinecode()).append("-").append(a.getFlightNumber())
				.append(", ").append(a.getSource()).append("-").append(a.getDest()).append(", ")
				.append(DateFormatterHelper.formatDate(a.getDepartureTime(), DateFormatType.DEFAULT_FORMAT))
				.append(", x"));
		int paxCount = airOrderItemList.get(0).getTravellerInfo().size();
		paymentDetail.setSummary(summaryBuilder.append(paxCount).toString());
		paymentDetail.setLeadPax(airOrderItemList.get(0).getTravellerInfo().get(0).getFullName());
		paymentDetail.setInvoiceId(order.getAdditionalInfo().getInvoiceId());
	}

	private void setPaymentHotelDetails(List<HotelOrderItem> hotelOrderItemList, Order order,
			PaymentDetail paymentDetail) {
		if (CollectionUtils.isEmpty(hotelOrderItemList) || order == null)
			return;
		hotelOrderItemList.sort(Comparator.comparing(HotelOrderItem::getId));
		HotelOrderItem hotelOrderItem = hotelOrderItemList.get(0);
		Address address = hotelOrderItem.getAdditionalInfo().getHInfo().getAddress();
		List<TravellerInfo> travellerInfo = hotelOrderItem.getRoomInfo().getTravellerInfo();
		StringBuilder summaryBuilder = new StringBuilder(hotelOrderItem.getHotel());
		try {
			summaryBuilder.append(", ").append(address.getCity().getName()).append("(")
					.append(address.getCountry().getName()).append("), ").append(DateFormatterHelper
							.formatDate(hotelOrderItem.getCheckInDate(), DateFormatType.DEFAULT_FORMAT));
		} catch (Exception e) {
			log.info("Unable to parse hotelinfo for bookingId {}", order.getBookingId());
		}
		paymentDetail.setSummary(summaryBuilder.toString());
		paymentDetail.setLeadPax(travellerInfo.get(0).getFullName());
		paymentDetail.setInvoiceId(order.getAdditionalInfo().getInvoiceId());
	}
}
