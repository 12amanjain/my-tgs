package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.tgs.services.base.restmodel.BulkUploadResponse;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.CreditLineFilter;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.restmodel.CreditLineMigrationRequest;
import com.tgs.services.pms.restmodel.MigrateCreditLineRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CreditLineBulkMigrateManager {

	@Autowired
	CreditLineService creditLineService;

	@Autowired
	CreditLineManager creditLineManager;

	public BulkUploadResponse bulkMigrateCreditLines(MigrateCreditLineRequest creditLineRequest) throws Exception {
		if (creditLineRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(creditLineRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable uploadCreditLineTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (CreditLineMigrationRequest creditLineUpdate : creditLineRequest.getMigrateQuery()) {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(creditLineUpdate.getRowId());
					try {
						migrateCreditLine(creditLineUpdate);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to migrate creditline with userid {} , creditlimit {} , maxTempExt {} ",
								creditLineUpdate.getUserId(), creditLineUpdate.getCreditLimit(),
								creditLineUpdate.getMaxTemporaryExt(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, "creditLineHelper");
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting creditline for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(uploadCreditLineTask, creditLineRequest.getUploadType(), jobId);
	}

	public void migrateCreditLine(CreditLineMigrationRequest clRequest) throws Exception {

		List<CreditLine> creditLines = creditLineService
				.search(CreditLineFilter.builder().userIdIn(new ArrayList<>(Arrays.asList(clRequest.getUserId())))
						.statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED)).build());
		if (CollectionUtils.isNotEmpty(creditLines) && creditLines.size() == 1) {
			BigDecimal diff = clRequest.getCreditLimit().subtract(creditLines.get(0).getCreditLimit());
			creditLineManager.updateLimitPPBEntry(creditLines.get(0), diff);
			creditLines.get(0).setCreditLimit(clRequest.getCreditLimit());
			creditLines.get(0).getAdditionalInfo().setMaxTemporaryExt(clRequest.getMaxTemporaryExt());
			creditLineService.save(creditLines.get(0));
		} else {
			log.info("CreditLine is empty or more than one active credit line for userId {}", clRequest.getUserId());
		}
	}
}
