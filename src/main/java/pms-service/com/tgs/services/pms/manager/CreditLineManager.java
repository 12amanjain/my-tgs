package com.tgs.services.pms.manager;

import static com.tgs.services.pms.jparepository.CreditLineService.generateCreditNumber;
import static java.math.BigDecimal.ZERO;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.PolicyServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.DocumentInfo;
import com.tgs.services.base.datamodel.DocumentType;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.CreditAdditionalInfo;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.CreditType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.restmodel.SaveCreditLineRequest;
import com.tgs.services.ps.datamodel.configurator.DoubleOutput;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.CreditModuleException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreditLineManager {

	@Autowired
	private CreditLineService service;

	@Autowired
	private CreditPolicyService polService;

	@Autowired
	@Qualifier("CreditBillingManager")
	private CreditBillingManager billingManager;

	@Autowired
	private CreditBillingService billingService;

	@Autowired
	private UserServiceCommunicator userServiceCommunicator;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private CreditSystemMessagingClient messagingClient;

	@Autowired
	private PolicyServiceCommunicator policyComm;

	private static final short DEFAULT_SCAN_HOURS_LOCK_JOB = 120;

	@Transactional
	public CreditLine issueCreditLine(CreditPolicy policy, String userId) {
		CreditPolicy policyTemplate = polService.findByPolicyId(policy.getPolicyId());
		checkPermission(userId);
		checkValidDocuments(policy, userId);
		CreditLine creditLine = CreditLine.builder().creditNumber(generateCreditNumber(userId))
				.issuedByUserId(SystemContextHolder.getContextData().getUser().getUserId()).outstandingBalance(ZERO)
				.userId(userId).policyId(policy.getPolicyId()).build();
		setPolicyFields(creditLine, policy, policyTemplate);
		checkDuplicate(creditLine);
		billingManager.resetCycle(creditLine, false, false);
		setExemptedDocs(creditLine, policy, policyTemplate);
		creditLine = service.save(creditLine);
		WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder()
				.amount(BigDecimal.valueOf(policy.getCreditLimit())).walletNumber(creditLine.getCreditNumber())
				.walletId(creditLine.getId()).payUserId(creditLine.getUserId()).paymentMedium(PaymentMedium.CREDIT_LINE)
				.opType(PaymentOpType.CREDIT).transactionType(PaymentTransactionType.CREDIT_ISSUED).build();
		paymentRequest.getAdditionalInfo().setClosingBalance(creditLine.getCreditLimit());
		paymentRequest.getAdditionalInfo().setUtilizedBalance(creditLine.getOutstandingBalance());
		paymentProcessor.process(Stream.of(paymentRequest).collect(Collectors.toList()));
		return creditLine;
	}

	private void checkDuplicate(CreditLine creditLine) {
		for (CreditLine existingCreditLine : service
				.search(CreditLineFilter.builder().userIdIn(Arrays.asList(creditLine.getUserId())).build())) {
			if (existingCreditLine.equals(creditLine)) {
				throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST);
			}
		}
	}

	private void setExemptedDocs(CreditLine creditLine, CreditPolicy policy, CreditPolicy policyTemplate) {
		if (CollectionUtils.isNotEmpty(policyTemplate.getDocuments())) {
			for (DocumentType docType : policyTemplate.getDocuments()) {
				if (policy.getDocuments() != null && !policy.getDocuments().contains(docType)) {
					creditLine.getAdditionalInfo().getExemptedDocs().add(docType);
				}
			}
			if (CollectionUtils.isNotEmpty(creditLine.getAdditionalInfo().getExemptedDocs())) {
				messagingClient.sendExemptedDocsMail(creditLine);
			}
		}
	}

	private void checkValidDocuments(CreditPolicy policy, String userId) {
		if (CollectionUtils.isNotEmpty(policy.getDocuments())) {
			for (DocumentType docType : policy.getDocuments()) {
				User user = userServiceCommunicator.getUserFromCache(userId);
				if (CollectionUtils.isNotEmpty(user.getAdditionalInfo().getDocuments())) {
					boolean isDocumentPresent = false;
					for (DocumentInfo doc : user.getAdditionalInfo().getDocuments()) {
						if (doc.getType().equals(docType) && BooleanUtils.isTrue(doc.getIsAvailable())) {
							isDocumentPresent = true;
							break;
						}
					}
					if (!isDocumentPresent) {
						throw new CustomGeneralException(SystemError.CL_DOCUMENT_MISSING);
					}
				} else {
					throw new CustomGeneralException(SystemError.CL_DOCUMENT_MISSING);
				}
			}
		}
	}

	public CreditLine save(SaveCreditLineRequest request) {
		CreditLine creditLine = request.getCreditLine();
		checkPermission(creditLine.getUserId());
		if (StringUtils.isEmpty(creditLine.getCreditNumber()))
			return create(creditLine);
		return update(creditLine);
	}

	private void checkPermission(String userId) {
		UserRole role = SystemContextHolder.getContextData().getUser().getRole();
		User user = userServiceCommunicator.getUserFromCache(userId);
		if (user == null || !user.getRole().isCreditLineAllowed()) {
			throw new CreditModuleException(SystemError.CL_NOT_ALLOWED);
		}

		List<AreaRoleMapping> mappingList =
				userServiceCommunicator.getAreaRoleMapByAreaRole(ApplicationArea.CREDIT.getAreaRoles());
		Map<UserRole, Set<AreaRole>> userRoleMap = AreaRoleMapping.getMapByUserRole(mappingList);
		if (userRoleMap.get(role) == null || !userRoleMap.get(role).contains(AreaRole.CL_EDIT)) {
			throw new CreditModuleException(SystemError.CL_NO_PERMISSION);
		}
	}

	private CreditLine create(CreditLine creditLine) {
		validateCreate(creditLine);
		creditLine.setCreditNumber(generateCreditNumber(creditLine.getUserId()));
		billingManager.resetCycle(creditLine, false, false);
		return service.save(creditLine);
	}

	@Transactional(isolation = Isolation.REPEATABLE_READ)
	private CreditLine update(CreditLine nuw) {
		CreditLine old = service.findByCreditNumber(nuw.getCreditNumber());
		validateUpdate(old, nuw);
		old.getAdditionalInfo().setComments(nuw.getAdditionalInfo().getComments());
		updateExtensionPPBEntry(old, TLEChange(old, nuw));
		old.getAdditionalInfo().setCurTemporaryExt(nuw.getAdditionalInfo().getCurTemporaryExt());
		if (!old.getStatus().equals(nuw.getStatus()) && nuw.getStatus() != null) {
			old = updateStatus(nuw.getStatus(), old);
		}
		old = service.save(old);
		return old;
	}

	@Transactional
	private CreditLine updateStatus(CreditStatus status, CreditLine creditLine) {
		if (status.equals(CreditStatus.EXPIRED)) {
			return expireCreditLine(creditLine).getLeft();
		} else {
			creditLine.setStatus(status);
			return service.save(creditLine);
		}
	}

	public CreditLine updateCycle(CreditLine nuw) {
		CreditLine old = service.findByCreditNumber(nuw.getCreditNumber());
		if (nuw.getBillCycleEnd() != null) {
			old.setBillCycleEnd(nuw.getBillCycleEnd());
		}
		if (nuw.getAdditionalInfo().getPaymentDueHours() != null) {
			old.getAdditionalInfo().setPaymentDueHours(nuw.getAdditionalInfo().getPaymentDueHours());
		}
		if (nuw.getAdditionalInfo().getLockAfterHours() != null) {
			old.getAdditionalInfo().setLockAfterHours(nuw.getAdditionalInfo().getLockAfterHours());
		}
		return service.save(old);
	}

	public CreditBill expireCreditLine(String userId, Product product) {
		CreditLine creditLine = service
				.search(CreditLineFilter.builder().userIdIn(Collections.singletonList(userId))
						.statusIn(Arrays.asList(CreditStatus.ACTIVE, CreditStatus.BLOCKED)).build())
				.stream().filter(c -> c.getProducts().contains(product)).findFirst().orElse(null);
		if (creditLine == null)
			return null;
		return expireCreditLine(creditLine).getRight();
	}

	public List<CreditLine> takeLockActions(Short scanHours) {
		scanHours = scanHours == null ? DEFAULT_SCAN_HOURS_LOCK_JOB : scanHours;
		List<CreditBill> creditBills = billingService.findBillsToLock(scanHours);
		List<CreditLine> lockedCreditLines = new ArrayList<>();
		for (CreditBill bill : creditBills) {
			if (bill.getAdditionalInfo().getCreditType().equals(CreditType.REVOLVING)) {
				CreditLine creditLine = service.findByCreditNumberOrId(null, bill.getCreditId());
				if (creditLine.getStatus().equals(CreditStatus.ACTIVE)) {
					checkAndLock(bill, creditLine);
					lockedCreditLines.add(creditLine);
				}
			}
		}
		return lockedCreditLines;
	}

	@Transactional
	private void checkAndLock(CreditBill bill, CreditLine credit) {
		if (lockCredit(bill)) {
			updateStatus(CreditStatus.BLOCKED, credit);
			messagingClient.sendCreditBillMail(bill, null, EmailTemplateKey.CREDIT_LOCK_EMAIL, null);
		}
		/*
		 * List<Incidence> incidenceCollection = getIncidences(credit, IncidenceType.LOCK_CREDIT); if
		 * (!CollectionUtils.isEmpty(incidenceCollection)) { CreditLockAction lockAction = ((LockCredit)
		 * incidenceCollection.get(0).getIncidenceData()).getLockAction(); switch (lockAction) { case LOCK_ACCOUNT:
		 * userServiceCommunicator.lockAccount(credit.userId());
		 * messagingClient.sendUserDeactivationEmail(credit.userId()); break; case LOCK_CREDITLINE:
		 * credit.status(CreditStatus.BLOCKED); creditLineService.save(credit); break; default:
		 * credit.status(CreditStatus.BLOCKED); creditLineService.save(credit); break; } }
		 */
	}

	private static boolean lockCredit(CreditBill bill) {
		if (bill.getLockDate() == null)
			return false;
		return bill.getLockDate().isBefore(LocalDateTime.now());
	}

	@Transactional
	private Pair<CreditLine, CreditBill> expireCreditLine(CreditLine creditLine) {
		CreditBill bill = null;
		List<Payment> paymentList = new ArrayList<>();
		if (creditLine.getBillCycleEnd() != null) {
			paymentList = paymentService
					.search(PaymentFilter.builder().payUserIdIn(Collections.singletonList(creditLine.getUserId()))
							.createdOnAfterDateTime(creditLine.getBillCycleStart())
							.createdOnBeforeDateTime(creditLine.getBillCycleEnd()).status(PaymentStatus.SUCCESS)
							.pageAttr(PageAttributes.builder().pageNumber(0).size(100000).build()).build())
					.stream().filter(p -> p.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE)
							&& creditLine.getId().equals(p.getWalletId()))
					.collect(Collectors.toList());
		}
		bill = billingManager.generateLineBill(creditLine, paymentList, true);
		if (bill != null) {
			messagingClient.sendCreditBillMail(bill, creditLine.toCredit(), EmailTemplateKey.CREDITBILL_STATEMENT_EMAIL,
					Duration.ZERO);
		}

		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder()
				.utilizedBalance(creditLine.getOutstandingBalance()).allowForDisabledMediums(true)
				.closingBalance(creditLine.getBalance().subtract(creditLine.getCreditLimit())).build();
		WalletPaymentRequest request = WalletPaymentRequest.builder().amount(creditLine.getCreditLimit())
				.paymentMedium(PaymentMedium.CREDIT_LINE).additionalInfo(additionalInfo)
				.transactionType(PaymentTransactionType.CREDIT_EXPIRED).payUserId(creditLine.getUserId())
				.opType(PaymentOpType.DEBIT).walletId(creditLine.getId()).walletNumber(creditLine.getCreditNumber())
				.reason(creditLine.getAdditionalInfo().getComments()).build();
		paymentProcessor.process(Stream.of(request).collect(Collectors.toList()));
		creditLine.setCreditLimit(ZERO);
		creditLine.setStatus(CreditStatus.EXPIRED);
		CreditLine expiredCl = service.save(creditLine);
		return Pair.of(expiredCl, bill);
	}


	public void updateLimitPPBEntry(CreditLine old, BigDecimal diff) {
		PaymentAdditionalInfo additionalInfo =
				PaymentAdditionalInfo.builder().utilizedBalance(old.getOutstandingBalance())
						.allowForDisabledMediums(true).closingBalance(old.getBalance().add(diff)).build();
		WalletPaymentRequest request = WalletPaymentRequest.builder().amount(diff.abs())
				.paymentMedium(PaymentMedium.CREDIT_LINE).additionalInfo(additionalInfo)
				.transactionType(diff.compareTo(ZERO) > 0 ? PaymentTransactionType.CREDIT_ISSUED
						: PaymentTransactionType.CREDIT_RECALLED)
				.payUserId(old.getUserId())
				.opType(diff.compareTo(ZERO) > 0 ? PaymentOpType.CREDIT : PaymentOpType.DEBIT)
				.walletNumber(old.getCreditNumber()).walletId(old.getId()).build();
		paymentProcessor.process(Stream.of(request).collect(Collectors.toList()));
	}

	private BigDecimal TLEChange(CreditLine old, CreditLine nuw) {
		return nuw.getAdditionalInfo().getCurTemporaryExt().subtract(old.getAdditionalInfo().getCurTemporaryExt());
	}

	private void updateExtensionPPBEntry(CreditLine old, BigDecimal tleDiff) {
		if (tleDiff.compareTo(BigDecimal.ZERO) == 0)
			return;
		PaymentAdditionalInfo additionalInfo =
				PaymentAdditionalInfo.builder().utilizedBalance(old.getOutstandingBalance())
						.allowForDisabledMediums(true).closingBalance(old.getBalance().add(tleDiff)).build();
		WalletPaymentRequest request = WalletPaymentRequest.builder().amount(tleDiff.abs())
				.paymentMedium(PaymentMedium.CREDIT_LINE).additionalInfo(additionalInfo)
				.transactionType(PaymentTransactionType.TEMPORARY_LIMIT_CHANGE).payUserId(old.getUserId())
				.opType(tleDiff.compareTo(ZERO) > 0 ? PaymentOpType.CREDIT : PaymentOpType.DEBIT)
				.walletNumber(old.getCreditNumber()).walletId(old.getId()).reason(old.getAdditionalInfo().getComments())
				.build();
		paymentProcessor.process(Stream.of(request).collect(Collectors.toList()));
	}

	private void validateUpdate(CreditLine old, CreditLine nuw) {
		if (old.getStatus().equals(CreditStatus.EXPIRED)) {
			throw new CreditModuleException(SystemError.EXPIRED_CREDIT);
		}
		if (nuw.getUserId() != null && !nuw.getUserId().equals(old.getUserId()))
			throw new CreditModuleException(SystemError.CL_USER_NOT_UPDATABLE);

		if (!StringUtils.equals(nuw.getPolicyId(), old.getPolicyId()))
			throw new CreditModuleException(SystemError.CL_POLICY_CHANGE_IS_NOT_ALLOWED);

		BigDecimal nuwOd = nuw.getAdditionalInfo().getCurTemporaryExt();
		if (nuwOd.compareTo(ZERO) < 0)
			throw new CreditModuleException(SystemError.CANNOT_BE_NEGATIVE);
		if (nuwOd.compareTo(old.getAdditionalInfo().getMaxTemporaryExt()) > 0) {
			User user = SystemContextHolder.getContextData().getEmulateOrLoggedInUser();
			DoubleOutput policyOutput = policyComm.fetchPolicyOutput(user.getUserId(), "CREDITLINE_OVERRULE_TEMP_EXT");
			// If user can assign new max temp extension allow him, else throw error.
			if (policyOutput != null && policyOutput.getOutput().compareTo(nuwOd) >= 0) {
				log.debug("[Policy Overruled]Policy output is " + policyOutput.getOutput()
						+ "and new temp extension is " + nuwOd + "for user id " + nuw.getUserId());
			} else
				throw new CreditModuleException(SystemError.CL_TEMPORARY_LIMIT_EXCEEDED);
		}
	}

	private void validateCreate(CreditLine creditLine) {
		if (StringUtils.isEmpty(creditLine.getUserId()))
			throw new CreditModuleException(SystemError.INVALID_USERID);
		try {
			if (userServiceCommunicator.getUserFromCache(creditLine.getUserId()) == null)
				throw new CreditModuleException(SystemError.INVALID_USERID);
		} catch (Exception ex) {
			throw new CreditModuleException(SystemError.INVALID_USERID);
		}
	}

	private void setPolicyFields(CreditLine creditLine, CreditPolicy currentValues, CreditPolicy policyTemplate) {
		if (BigDecimal.valueOf(currentValues.getCreditLimit())
				.add(currentValues.getAdditionalInfo().getMaxTemporaryExt())
				.compareTo(policyTemplate.getAdditionalInfo().getMaxLimit()) > 0) {
			throw new CustomGeneralException(SystemError.MAX_CREDIT_LIMIT_EXCEEDED);
		}
		creditLine.setPolicyId(currentValues.getPolicyId());
		creditLine.setBillCycleType(currentValues.getBillCycleType());
		creditLine.setCreditLimit(BigDecimal.valueOf(currentValues.getCreditLimit()));
		creditLine.setProducts(currentValues.getProducts());
		creditLine.setStatus(CreditStatus.ACTIVE);
		creditLine.setAdditionalInfo(
				CreditAdditionalInfo.builder().billCycleDays(currentValues.getAdditionalInfo().getBillCycleDays())
						.billingDOM(currentValues.getAdditionalInfo().getBillingDOM())
						.billingDOW(currentValues.getAdditionalInfo().getBillingDOW())
						.maxTemporaryExt(currentValues.getAdditionalInfo().getMaxTemporaryExt())
						.curTemporaryExt(currentValues.getAdditionalInfo().getPreApprovedOd())
						.preApprovedOd(currentValues.getAdditionalInfo().getPreApprovedOd())
						.lockAfterHours(currentValues.getAdditionalInfo().getLockAfterHours())
						.dynamicReset(currentValues.getAdditionalInfo().isDynamicReset())
						.temporaryExtensionType(currentValues.getAdditionalInfo().getTemporaryExtensionType())
						.paymentDueHours(currentValues.getAdditionalInfo().getPaymentDueHours())
						.associatedCalendar(currentValues.getAdditionalInfo().getAssociatedCalendar()).build());
	}
}
