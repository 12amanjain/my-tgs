package com.tgs.services.pms.manager;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.CreditFilter;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.NRCreditFilter;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.Credit;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.NRCreditService;

@Service
public class CreditManager {

    private static final short DEFAULT_UNUSED_CREDIT_LOCK_DAYS = 15;

    private static final LocalDateTime LOCK_JOB_ORIGIN = LocalDateTime.of(2019, 9, 11, 0,0 );

    @Autowired
    private CreditLineService creditLineService;

    @Autowired
    private NRCreditService nrCreditService;

    public List<Credit> search(CreditFilter creditFilter) {
        String loggedInUserId = SystemContextHolder.getContextData().getEmulateOrLoggedInUserId();
        creditFilter.setUserIdIn(UserServiceHelper.checkAndReturnAllowedUserId(loggedInUserId, creditFilter.getUserIdIn()));
        creditFilter.setPartnerIdIn(UserServiceHelper.returnPartnerIds(loggedInUserId, false, creditFilter.getPartnerIdIn()));
        NRCreditFilter nrCreditFilter = Credit.creditFilterToNRCreditFilter(creditFilter);
        CreditLineFilter creditLineFilter = Credit.creditFilterToCreditLineFilter(creditFilter);
        List<CreditLine> creditLines = creditLineService.search(creditLineFilter);
        List<NRCredit> nrCreditList = nrCreditService.search(nrCreditFilter);
        List<Credit> credits = new ArrayList<>();
        creditLines.forEach(c -> credits.add(c.toCredit()));
        nrCreditList.forEach(c -> credits.add(c.toCredit()));
        return credits;
    }

    public List<String> lockUnusedCreditLines(Short lockDays) {
        lockDays = lockDays == null ? DEFAULT_UNUSED_CREDIT_LOCK_DAYS : lockDays;
        List<String> lockedCredits = new ArrayList<>();
        boolean lockUnsetLOT = LocalDateTime.now().isAfter(LOCK_JOB_ORIGIN.plusDays(10));
        CreditLineFilter filter = CreditLineFilter.builder().status(CreditStatus.ACTIVE).build();
        List<CreditLine> creditLines = creditLineService.search(filter);
        for (CreditLine creditLine : creditLines) {
            LocalDateTime lot = creditLine.getAdditionalInfo().getLastOrderTransaction();
            if (lot == null && lockUnsetLOT) {
                creditLine.setStatus(CreditStatus.BLOCKED);
                creditLineService.save(creditLine);
                lockedCredits.add(creditLine.getCreditNumber());
            }
            if (lot != null && lot.plusDays(lockDays).isBefore(LocalDateTime.now())) {
                creditLine.setStatus(CreditStatus.BLOCKED);
                creditLineService.save(creditLine);
                lockedCredits.add(creditLine.getCreditNumber());
            }
        }
        return lockedCredits;
    }

}
