package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.datamodel.DIRequest;
import com.tgs.services.pms.datamodel.DepositAdditionalInfo;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.DepositType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.jparepository.DepositRequestService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.servicehandler.DepositRequestHandler;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DIManager {

	@Autowired
	private CreditPolicyService policyService;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	private PaymentService payService;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private DepositRequestService depositRequestService;

	// DI daily job
	/*
	public List<Payment> diJob(Short scanDays, boolean includeToday) {
		List<Payment> DIPayments = new ArrayList<>();
		scanDays = ObjectUtils.defaultIfNull(scanDays, DEFAULT_SCAN_DAYS);
		LocalDate date = LocalDate.now();
		int count = 0;
		if (!includeToday)
			date = date.minusDays(1);
		while (count > scanDays) {
			List<Payment> depositPayments = paymentService
					.search(PaymentFilter.builder()
							.type(PaymentTransactionType.TOPUP)
							.paymentMediumIn(Arrays.asList(PaymentMedium.WALLET, PaymentMedium.CREDIT))
							.createdOnAfterDate(date)
							.createdOnBeforeDate(date)
							.status(PaymentStatus.SUCCESS)
							.build())
					.stream().filter(p -> p.getAdditionalInfo().getDepositIncentive() == null)
					.collect(Collectors.toList());
			Map<String, List<Payment>> map = depositPayments.stream()
					.collect(Collectors.groupingBy(Payment::getPayUserId));
			for (Map.Entry<String, List<Payment>> entry : map.entrySet()) {
				String userId = entry.getKey();
				List<Payment> payments = entry.getValue();
				List<NRCredit> unsettledCredits = creditService.search(NRCreditFilter.builder()
						.userIdIn(Collections.singletonList(userId))
						.utilizedGreaterThan(BigDecimal.ZERO)
						.build());
				if (CollectionUtils.isNotEmpty(unsettledCredits)) {
					log.info(
							"UserId: {} did not receive DI for date: {} because of unsettled NRCredit with creditNumber: {}",
							userId, date.toString(), unsettledCredits.get(0).getCreditId());
					continue;
				}
				User user = userService.getUserFromCache(userId);
				PaymentFact fact = PaymentFact.builder().userCity(user.getAddressInfo().getCityInfo().getName())
						.userState(user.getAddressInfo().getCityInfo().getState()).build();
				List<PaymentConfigurationRule> configurationRules = PaymentConfigurationHelper
						.getPaymentRuleBasedOnRuleType(fact, PaymentRuleType.DEPOSIT_INCENTIVE);
				DepositIncentiveConfig incentiveConfig;
				try {
					incentiveConfig = (DepositIncentiveConfig) configurationRules.get(0).getOutput();
					BigDecimal depositSum = Payment.sum(payments);
					double incentive = incentiveConfig.getIncentive(depositSum.doubleValue());
					if (incentive == 0d) {
						log.info(
								"UserId: {} did not receive DI for date: {} because Deposit Sum did not match any slab",
								userId, date.toString());
						continue;
					}
					WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder()
							.amount(depositSum.multiply(BigDecimal.valueOf(incentive)))
							.payUserId(userId)
							.paymentMedium(PaymentMedium.WALLET)
							.opType(PaymentOpType.CREDIT)
							.transactionType(PaymentTransactionType.DEPOSIT_INCENTIVE)
							.build();
					DIPayments.addAll(
							paymentService.doPaymentsUsingPaymentRequests(Collections.singletonList(paymentRequest)));
				} catch (Exception ex) {
					log.error("Exception occurred while evaluating DI for userId: {}", userId);
					log.error("Exception: ", ex);
				}
			}
			date = date.minusDays(1);
			count++;
		}
		return DIPayments;
	}
	*/

	public List<Payment> evaluateDI(Map<String, DIRequest> diRequestMap) {
		List<Payment> DIPayments = new ArrayList<>();
		List<Payment> paymentList = payService.search(PaymentFilter.builder()
				.paymentRefIdIn(diRequestMap.keySet().stream().collect(Collectors.toList()))
				.status(PaymentStatus.SUCCESS)
				.build());
		List<String> userIds = paymentList.stream().map(Payment::getPayUserId).distinct().collect(Collectors.toList());
		Map<String, User> userMap = userService.getUsersFromCache(UserCacheFilter.builder().userIds(userIds).build());
		Map<String, List<Payment>> mapByPaymentRefId = paymentList.stream().collect(Collectors.groupingBy(Payment::getPaymentRefId));
		log.info("DI Manager: userId {}", new Gson().toJson(userIds));
		log.info("DI Manager: mapByRefId {}", new Gson().toJson(mapByPaymentRefId));
		for (Map.Entry<String, List<Payment>> entry : mapByPaymentRefId.entrySet()) {
			BigDecimal diSum = BigDecimal.ZERO;
			List<Payment> refPayments = entry.getValue();
			if (refPayments.size() <= 0) continue;
			List<Payment> diYesPayments = new ArrayList<>();
			List<Payment> diNoPayments = new ArrayList<>();
			CreditPolicy creditPolicy = null;
			User user = userMap.get(refPayments.get(0).getPayUserId());
			String airPolicy = user.getUserConf().getCreditPolicyMap().get(Product.AIR);
			if (airPolicy != null) creditPolicy = policyService.findByPolicyId(airPolicy);
			String refId = null;
			DIRequest diRequest = diRequestMap.get(entry.getKey());
			for (Payment payment : refPayments) {
				refId = payment.getRefId();
				if (BooleanUtils.isTrue(diRequest.getDiProcessed()) && diRequest.getDi().compareTo(BigDecimal.ZERO) > 0) {
					if (creditPolicy == null || !creditPolicy.getAdditionalInfo().isDiAllowed()) {
							throw new PaymentException(SystemError.DI_NOT_APPLICABLE);
					}
					if (payment.getAdditionalInfo().getDepositIncentive().compareTo(BigDecimal.ZERO) > 0) {
						throw new PaymentException(SystemError.DI_ALREADY_PROCESSED);
					}
					payment.getAdditionalInfo().setDepositIncentive(diRequest.getDi().multiply(BigDecimal.valueOf(100)));
					diSum = diSum.add(payment.getAmount().abs().multiply(diRequest.getDi()));
					diYesPayments.add(payment);
				} else {
					payment.getAdditionalInfo().setDepositIncentive(BigDecimal.ZERO);
					diNoPayments.add(payment);
				}
			}
			if (!diYesPayments.isEmpty()) {
				//create DI entry
				DepositAdditionalInfo DRAdditionalInfo = DepositAdditionalInfo.builder()
						.approvedByUserId(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId())
						.processedAmount(diSum.doubleValue())
						.rcNo(diRequest.getRc())
						.dhNo(diRequest.getDh())
						.build();
				DepositRequest depositRequest = DepositRequest.builder()
						.type(DepositType.DEPOSIT_INCENTIVE)
						.comments(diRequest.getComment())
						.requestedAmount(diSum.doubleValue())
						.status(DepositRequestStatus.ACCEPTED)
						.transactionId(refId)
						.userId(user.getUserId())
						.partnerId(user.getPartnerId())
						.additionalInfo(DRAdditionalInfo)
						.build();
				DepositRequestHandler.generateDRId(depositRequest);
				depositRequestService.save(depositRequest);

				// process DI payments
				PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder()
						.paymentRefId(diYesPayments.stream().map(Payment::getPaymentRefId).collect(Collectors.toList()))
						.totalDepositAmount(diSum)
						.comments(diRequest.getComment())
						.build();
				PaymentRequest paymentRequest = PaymentRequest.builder()
						.amount(diSum)
						.payUserId(user.getUserId())
						.partnerId(user.getPartnerId())
						.opType(PaymentOpType.CREDIT)
						.transactionType(PaymentTransactionType.DEPOSIT_INCENTIVE)
						.additionalInfo(additionalInfo)
						.product(Product.NA)
						.build();
				List<Payment> outPayments = paymentProcessor.process(Arrays.asList(paymentRequest));

				// update original TopUp payments with newly created DI payment's refId
				diYesPayments.forEach(p -> {
					p.getAdditionalInfo().getDiRefId().add(outPayments.get(0).getRefId());
					payService.save(new DbPayment().from(p));
				});
				DIPayments.addAll(outPayments);
			}
			diNoPayments.forEach(p -> payService.save(new DbPayment().from(p)));
		}
		return DIPayments;
	}

	/*
    private void sendDIMail(Payment payment) {
        AbstractMessageSupplier<FundEmailAttributes> attributeSupplier = new AbstractMessageSupplier<FundEmailAttributes>() {
            @Override
            public FundEmailAttributes get() {
                FundEmailAttributes mailAttr = FundEmailAttributes.builder().build();
                mailAttr.setKey(EmailTemplateKey.DEPOSIT_INCENTIVE_EMAIL.name());
                mailAttr.setToEmailUserId(payment.getPayUserId());
                Pair<Double, Double> incentivePair = incentiveMap.get(payment.getRefId());
                mailAttr.setIncentivePercentage(TgsStringUtils.formatCurrency(incentivePair.getValue() * 100));
                mailAttr.setIncentiveAmount(
                        TgsStringUtils.formatCurrency(incentivePair.getKey() * incentivePair.getValue()));
                mailAttr.setProcessedAmount(TgsStringUtils.formatCurrency(incentivePair.getKey()));
                return mailAttr;
            }
        };
        //msgCommunicator.sendMail(attributeSupplier.getAttributes());
    }*/
}
