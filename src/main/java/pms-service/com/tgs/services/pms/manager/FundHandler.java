package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditType;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FundHandler {

	@Autowired
	@Qualifier("CreditBillingManager")
	private CreditBillingManager billingManager;

	@Autowired
	private CreditBillingService billingService;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private CreditLineService creditLineService;

	@Autowired
	private NRCreditService nrCreditService;

	@Autowired
	private UserWalletService walletService;

	@Autowired
	private PaymentService paymentService;

	private List<CreditBill> pendingBills;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public List<PaymentRequest> dividePaymentRequest(PaymentRequest request) {
		if (request.getTransactionType() == null) {
			throw new PaymentException(SystemError.INVALID_TRANSACTION_TYPE);
		}
		BigDecimal remainingAmount = request.getAmount();
		List<PaymentRequest> payments = new ArrayList<>();
		if (request.getRefId() == null) {
			request.setRefId(ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build()));
		} else if (StringUtils.isNotBlank(request.getMerchantTxnId())
				&& Product.WALLET_TOPUP.equals(Product.getProductFromId(request.getRefId()))) {
			List<Payment> topUpPayments = paymentService.search(PaymentFilter.builder()
					.createdOnAfterDateTime(LocalDateTime.now().minusDays(2)).merchantTxnId(request.getMerchantTxnId())
					.refId(request.getRefId()).status(PaymentStatus.SUCCESS).build());
			if (topUpPayments.stream().anyMatch(p -> p.getOpType().equals(PaymentOpType.CREDIT))) {
				throw new CustomGeneralException(SystemError.DUPLICATE_DEPOSIT_REQUEST, request.getMerchantTxnId());
			}
		}
		Pair<List<PaymentRequest>, BigDecimal> pair;
		pendingBills = billingManager.getPendingBills(request.getPayUserId());

		if (shouldSettleNegativeWallet(request)) {
			pair = settleNegativeWallet(request, remainingAmount);
			payments.addAll(pair.getLeft());
			remainingAmount = pair.getRight();
		}
		if (shouldSettleBill(request)) {
			pair = settleBills(request, remainingAmount);
			payments.addAll(pair.getLeft());
			remainingAmount = pair.getRight();
		}
		if (remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
			pair = creditLineOutStandingPayments(request, remainingAmount);
			payments.addAll(pair.getLeft());
			remainingAmount = pair.getRight();
		}
		if (remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
			pair = adjustNRCreditOutstanding(request, remainingAmount);
			payments.addAll(pair.getLeft());
			remainingAmount = pair.getRight();
		}
		if (remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
			payments.addAll(walletPayments(remainingAmount, request));
		}
		return payments;
	}

	private boolean shouldSettleNegativeWallet(PaymentRequest request) {
		if (request.getTransactionType().equals(PaymentTransactionType.TOPUP))
			return true;
		return false;
	}

	private boolean shouldSettleBill(PaymentRequest request) {
		if (request.getTransactionType().equals(PaymentTransactionType.TOPUP))
			return true;
		if (request.getTransactionType().equals(PaymentTransactionType.REFUND)
				|| request.getTransactionType().equals(PaymentTransactionType.REVERSE)) {
			Optional<Payment> optionalPayment =
					paymentService.search(PaymentFilter.builder().refId(request.getRefId()).build()).stream()
							.filter(p -> p.getType().equals(PaymentTransactionType.PAID_FOR_ORDER))
							.min(Comparator.comparing(Payment::getCreatedOn));
			if (!optionalPayment.isPresent())
				return true;
			Payment payment = optionalPayment.get();
			if (CollectionUtils.isNotEmpty(pendingBills)) {
				CreditBill latestBill = pendingBills.stream().max(Comparator.comparing(CreditBill::getCreatedOn)).get();
				return payment.getCreatedOn().isBefore(latestBill.getCreatedOn());
			}
		}
		return false;
	}

	public void setReversePayment(PaymentRequest request) {
		request.setPaymentMedium(null);
		request.setOpType(PaymentOpType.DEBIT);
	}

	public void setDepositIncentivePayment(PaymentRequest request) {
		request.setPaymentMedium(null);
		request.setOpType(PaymentOpType.CREDIT);
	}


	private List<PaymentRequest> walletPayments(BigDecimal amount, PaymentRequest request) {
		List<PaymentRequest> paymentList = createWalletPaymentDataList(amount, request);
		return paymentList;
	}

	private Pair<List<PaymentRequest>, BigDecimal> settleNegativeWallet(PaymentRequest request,
			BigDecimal remainingAmount) {
		List<PaymentRequest> paymentList = new ArrayList<>();
		UserWallet walletBalance = walletService.findByUserId(request.getPayUserId()).toDomain();
		BigDecimal availBal = walletBalance.getBalance();
		if (availBal.compareTo(BigDecimal.ZERO) > 0) {
			return Pair.of(paymentList, remainingAmount);
		}
		BigDecimal amount = availBal.abs();
		if (remainingAmount.compareTo(amount) < 0) {
			amount = remainingAmount;
			//remainingAmount = BigDecimal.ZERO;
		}
		remainingAmount = remainingAmount.subtract(amount);
		PaymentAdditionalInfo additionalInfo =
				new GsonMapper<>(request.getAdditionalInfo(), PaymentAdditionalInfo.class).convert();
		additionalInfo.setTotalDepositAmount(request.getAmount());
		PaymentRequest payment = PaymentRequest.builder().payUserId(request.getPayUserId()).amount(amount)
				.opType(PaymentOpType.CREDIT).paymentMedium(PaymentMedium.WALLET)
				.merchantTxnId(request.getMerchantTxnId()).refId(request.getRefId()).product(request.getProduct())
				.amendmentId(request.getAmendmentId())
				.transactionType(ObjectUtils.firstNonNull(request.getTransactionType(), PaymentTransactionType.TOPUP))
				.additionalInfo(additionalInfo).tds(getTds(amount, request)).reason(request.getReason()).build();
		payment.setSubType(PaymentTransactionType.TOPUP);
		paymentList.add(payment);
		return Pair.of(paymentList, remainingAmount);

	}

	private Pair<List<PaymentRequest>, BigDecimal> creditLineOutStandingPayments(PaymentRequest request,
			BigDecimal remainingAmount) {
		List<PaymentRequest> paymentList = new ArrayList<>();
		List<CreditLine> creditLines = creditLineService
				.search(CreditLineFilter.builder().userIdIn(Arrays.asList(request.getPayUserId())).isInternalQuery(request.isInternalQuery()).build());
		if (CollectionUtils.isNotEmpty(creditLines)) {
			for (CreditLine creditLine : creditLines) {
				BigDecimal totalDueAmount =
						PaymentUtils.getTotalDueBillAmount(billingService, creditLine.getId(), creditLine.getUserId(), request.isInternalQuery()),
						excessOutstanding = creditLine.getOutstandingBalance().subtract(totalDueAmount);
				if (excessOutstanding.compareTo(BigDecimal.ZERO) > 0
						&& remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal transferAmount = excessOutstanding.min(remainingAmount);
					paymentList.add(createPaymentForCreditLineOutstanding(transferAmount, creditLine, request));
					remainingAmount = remainingAmount.subtract(transferAmount);
				}
			}
		}
		return Pair.of(paymentList, remainingAmount);
	}

	private Pair<List<PaymentRequest>, BigDecimal> adjustNRCreditOutstanding(PaymentRequest request,
			BigDecimal remainingAmount) {
		List<PaymentRequest> paymentList = new ArrayList<>();
		BigDecimal totalOA = BigDecimal.ZERO;
		List<NRCredit> nrCreditList = nrCreditService.getCurrentCredits(request.getPayUserId());
		if (CollectionUtils.isNotEmpty(nrCreditList)) {
			for (NRCredit nrCredit : nrCreditList) {
				if (nrCredit.getUtilized().compareTo(BigDecimal.ZERO) > 0
						&& remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal transferAmount = nrCredit.getUtilized().min(remainingAmount);
					totalOA = totalOA.add(transferAmount);
					paymentList.add(createPaymentForNRCreditOutstanding(transferAmount, nrCredit, request));
					remainingAmount = remainingAmount.subtract(transferAmount);
				}
			}
		}
		User user = userService.getUserFromCache(request.getPayUserId());
		if (StringUtils.isNotEmpty(user.getUserConf().getDistributorId())) {
			User parentUser = userService.getUserFromCache(user.getParentUserId());
			if (parentUser.getRole().equals(UserRole.DISTRIBUTOR)) {
				PaymentRequest distributorPayment =
						PaymentRequest.builder().paymentMedium(PaymentMedium.WALLET).refId(request.getRefId())
								.payUserId(user.getUserConf().getDistributorId()).loggedInUserId(user.getUserId())
								.opType(PaymentOpType.CREDIT).transactionType(PaymentTransactionType.TOPUP)
								.additionalInfo(request.getAdditionalInfo()).build();
				distributorPayment.setAmount(totalOA);
				paymentList.add(distributorPayment);

			}
		}
		return Pair.of(paymentList, remainingAmount);
	}

	private Pair<List<PaymentRequest>, BigDecimal> settleBills(PaymentRequest request, BigDecimal remainingAmount) {
		List<PaymentRequest> paymentList = new ArrayList<>();
		List<CreditBill> bills = billingManager.getPendingBills(request.getPayUserId());
		for (CreditBill bill : bills) {
			if (remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
				BigDecimal transAmount = remainingAmount;
				if (bill.getPendingAmount().compareTo(remainingAmount) <= 0) {
					transAmount = bill.getPendingAmount();
				}
				paymentList.add(settleBill(bill, transAmount, request));
				remainingAmount = remainingAmount.subtract(transAmount);
			} else {
				return Pair.of(paymentList, remainingAmount);
			}
		}
		return Pair.of(paymentList, remainingAmount);
	}

	/*
	 * private PaymentRequest getWalletDIPayment(Double amount, PaymentRequest request) { User payUser =
	 * userService.getUserFromCache(request.getPayUserId()); PaymentRequest payment = null; if
	 * (request.getAdditionalInfo() != null && BooleanUtils.isTrue(payUser.getUserConf().getIsDIAllowed())) { payment =
	 * createDIPayment(amount, request, PaymentMedium.WALLET, null); } return payment; }
	 */


	/*
	 * private PaymentRequest createDIPayment(Double amount, PaymentRequest request, PaymentMedium medium, Long
	 * walletId) { WalletPaymentRequest payment = null; List<PaymentConfigurationRule> rules =
	 * PaymentConfigurationHelper.getPaymentRuleBasedOnRuleType(
	 * PaymentFact.builder().userId(request.getPayUserId()).build(), PaymentRuleType.DEPOSIT_INCENTIVE); if
	 * (CollectionUtils.isNotEmpty(rules)) { double incentive = 0; for (PaymentConfigurationRule rule : rules) {
	 * DepositIncentiveConfig config = (DepositIncentiveConfig) rule.getOutput(); for (IncentiveSlab slab :
	 * config.getSlabList()) { if (slab.getMaxAmount() > amount && slab.getMinAmount() < amount) { incentive =
	 * slab.getIncentive(); break; } } } if (incentive > 0d) { if (incentiveMap == null) { incentiveMap = new
	 * HashMap<>(); } payment = WalletPaymentRequest.builder().amount(amount * incentive)
	 * .payUserId(request.getPayUserId()) .opType(PaymentOpType.CREDIT) .refId(request.getRefId()) .walletId(walletId)
	 * .paymentMedium(medium) .product(request.getProduct())
	 * .transactionType(ObjectUtils.firstNonNull(request.getTransactionType(),
	 * PaymentTransactionType.DEPOSIT_INCENTIVE)) .build();
	 * payment.setSubType(PaymentTransactionType.DEPOSIT_INCENTIVE);
	 * payment.getAdditionalInfo().setComments(request.getAdditionalInfo().getComments());
	 * incentiveMap.put(request.getRefId(), new Pair<>(amount, incentive)); } } return payment; }
	 */

	private PaymentRequest settleBill(CreditBill bill, BigDecimal transAmount, PaymentRequest request) {
		PaymentAdditionalInfo additionalInfo =
				new GsonMapper<>(request.getAdditionalInfo(), PaymentAdditionalInfo.class).convert();
		additionalInfo.setTotalDepositAmount(request.getAmount());
		additionalInfo.setBillNumber(bill.getBillNumber());
		PaymentMedium medium = request.getPaymentMedium();
		if (medium == null) {
			medium = bill.getAdditionalInfo().getCreditType().equals(CreditType.REVOLVING) ? PaymentMedium.CREDIT_LINE
					: PaymentMedium.CREDIT;
		}
		WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder().payUserId(request.getPayUserId())
				.amount(transAmount).opType(PaymentOpType.CREDIT).paymentMedium(medium)
				.transactionType(
						ObjectUtils.firstNonNull(request.getTransactionType(), PaymentTransactionType.BILL_SETTLE))
				.merchantTxnId(request.getMerchantTxnId()).walletId(bill.getCreditId()).product(request.getProduct())
				.refId(request.getRefId()).amendmentId(request.getAmendmentId()).additionalInfo(additionalInfo)
				.tds(getTds(transAmount, request)).reason(request.getReason()).build();
		paymentRequest.setSubType(PaymentTransactionType.BILL_SETTLE);
		return paymentRequest;
	}

	private List<PaymentRequest> createWalletPaymentDataList(BigDecimal amount, PaymentRequest request) {
		PaymentAdditionalInfo additionalInfo =
				new GsonMapper<>(request.getAdditionalInfo(), PaymentAdditionalInfo.class).convert();
		additionalInfo.setTotalDepositAmount(request.getAmount());
		List<PaymentRequest> paymentList = new ArrayList<>();
		PaymentRequest payment = PaymentRequest.builder().payUserId(request.getPayUserId()).amount(amount)
				.opType(PaymentOpType.CREDIT).paymentMedium(PaymentMedium.WALLET)
				.merchantTxnId(request.getMerchantTxnId()).refId(request.getRefId()).product(request.getProduct())
				.amendmentId(request.getAmendmentId())
				.transactionType(ObjectUtils.firstNonNull(request.getTransactionType(), PaymentTransactionType.TOPUP))
				.additionalInfo(additionalInfo).tds(getTds(amount, request)).reason(request.getReason())
				.paymentFee(request.getPaymentFee()).build();
		payment.setSubType(PaymentTransactionType.TOPUP);
		paymentList.add(payment);
		return paymentList;
	}

	private PaymentRequest createPaymentForCreditLineOutstanding(BigDecimal amount, CreditLine cLine,
			PaymentRequest request) {
		PaymentAdditionalInfo additionalInfo =
				new GsonMapper<>(request.getAdditionalInfo(), PaymentAdditionalInfo.class).convert();
		additionalInfo.setTotalDepositAmount(request.getAmount());
		WalletPaymentRequest payment = WalletPaymentRequest.builder().payUserId(request.getPayUserId()).amount(amount)
				.opType(PaymentOpType.CREDIT).walletId(cLine.getId()).paymentMedium(PaymentMedium.CREDIT_LINE)
				.merchantTxnId(request.getMerchantTxnId()).refId(request.getRefId()).product(request.getProduct())
				.amendmentId(request.getAmendmentId())
				.transactionType(ObjectUtils.firstNonNull(request.getTransactionType(),
						PaymentTransactionType.UTILISATION_ADJUSTMENT))
				.additionalInfo(additionalInfo).tds(getTds(amount, request)).reason(request.getReason()).build();
		payment.setSubType(PaymentTransactionType.UTILISATION_ADJUSTMENT);
		return payment;
	}

	private PaymentRequest createPaymentForNRCreditOutstanding(BigDecimal amount, NRCredit nrCredit,
			PaymentRequest request) {
		PaymentAdditionalInfo additionalInfo =
				new GsonMapper<>(request.getAdditionalInfo(), PaymentAdditionalInfo.class).convert();
		additionalInfo.setTotalDepositAmount(request.getAmount());
		WalletPaymentRequest payment = WalletPaymentRequest.builder().amount(amount).payUserId(request.getPayUserId())
				.opType(PaymentOpType.CREDIT).walletNumber(nrCredit.getCreditId()).walletId(nrCredit.getId())
				.paymentMedium(PaymentMedium.CREDIT).merchantTxnId(request.getMerchantTxnId()).refId(request.getRefId())
				.amendmentId(request.getAmendmentId()).product(request.getProduct())
				.transactionType(ObjectUtils.firstNonNull(request.getTransactionType(),
						PaymentTransactionType.UTILISATION_ADJUSTMENT))
				.additionalInfo(additionalInfo).tds(getTds(amount, request)).reason(request.getReason()).build();
		payment.setSubType(PaymentTransactionType.UTILISATION_ADJUSTMENT);
		return payment;
	}


	private BigDecimal getTds(BigDecimal amount, PaymentRequest paymentRequest) {
		if (paymentRequest.getTds() == null || amount.equals(paymentRequest.getAmount())) {
			return paymentRequest.getTds();
		}
		BigDecimal ratio = paymentRequest.getTds().divide(paymentRequest.getAmount(), 8, BigDecimal.ROUND_HALF_EVEN);
		return ratio.multiply(amount).setScale(2, BigDecimal.ROUND_HALF_UP);
	}
}
