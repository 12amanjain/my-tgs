package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.services.base.Constants;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.FundTransferOperation;
import com.tgs.services.pms.datamodel.FundTransferRequest;
import com.tgs.services.pms.datamodel.MediumTransferRequest;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FundsTransferHandler {

	@Autowired
	private NRCreditManager nrCreditManager;

	@Autowired
	private NRCreditService nrCreditService;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Transactional
	public List<Payment> transfer(FundTransferRequest request) {
		if (request.getAmount().compareTo(BigDecimal.ZERO) <= 0) {
			throw new CustomGeneralException(SystemError.ZERO_AMOUNT_PAYMENT);
		}
		List<Payment> paymentList = new ArrayList<>();
		String refId = Product.FUND_TRANSFER.getPrefix() + RandomStringUtils.random(8, false, true);
		request.setRefId(refId);
		List<PaymentRequest> paymentRequests = new ArrayList<>();
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		List<NRCredit> nrCreditList = nrCreditService.getCurrentCredits(request.getPayUserId());
		if (request.getTransferOperation().equals(FundTransferOperation.PAYMENT)) {
			paymentRequests.addAll(createAddFundPayments(request));
		} else if (request.getTransferOperation().equals(FundTransferOperation.FUND_ALLOCATE)) {
			PaymentRequest sourcePayment =
					PaymentRequest.builder().amount(request.getAmount()).payUserId(loggedInUser.getUserId())
							.opType(PaymentOpType.DEBIT).transactionType(PaymentTransactionType.FUND_TRANSFER)
							.additionalInfo(request.getAdditionalInfo()).build();
			sourcePayment.getAdditionalInfo().setAgentId(request.getPayUserId());
			paymentRequests.add(sourcePayment);
			if (CollectionUtils.isEmpty(nrCreditList)) { // fund allocate will always be in NR_CREDIT
				NRCredit credit =
						NRCredit.builder().creditAmount(request.getAmount()).creditExpiry(Constants.maxDateTime)
								.userId(request.getPayUserId()).products(request.getProducts()).build();
				credit.getAdditionalInfo().setComments(request.getAdditionalInfo().getComments());
				credit.getAdditionalInfo().setRefId(refId);
				nrCreditManager.save(credit);
			} else {
				NRCredit nrCredit = nrCreditList.get(0);
				nrCredit.getAdditionalInfo().setComments(request.getAdditionalInfo().getComments());
				paymentList.add(
						nrCreditManager.updateLimitByAmount(null, nrCreditList.get(0), request.getAmount(), refId));
			}
		} else {
			if (request.getTargetPaymentMedium().equals(PaymentMedium.CREDIT)) {
				if (CollectionUtils.isEmpty(nrCreditList)) {
					throw new CustomGeneralException(SystemError.RESOURCE_NOT_FOUND);
				}
				NRCredit nrCredit = nrCreditList.get(0);
				nrCredit.getAdditionalInfo().setComments(request.getAdditionalInfo().getComments());
				paymentList.add(nrCreditManager.updateLimitByAmount(null, nrCredit,
						request.getAmount().multiply(BigDecimal.valueOf(-1)), refId));
			}
			if (request.getTargetPaymentMedium().equals(PaymentMedium.WALLET)) {
				PaymentRequest targetPayment = PaymentRequest.builder().amount(request.getAmount())
						.paymentMedium(PaymentMedium.WALLET).payUserId(request.getPayUserId())
						.opType(PaymentOpType.DEBIT).transactionType(PaymentTransactionType.REVERSE).build();
				paymentRequests.add(targetPayment);
			}
			PaymentRequest sourcePayment =
					PaymentRequest.builder().amount(request.getAmount()).payUserId(loggedInUser.getUserId())
							.opType(PaymentOpType.CREDIT).transactionType(PaymentTransactionType.FUND_TRANSFER).build();
			sourcePayment.getAdditionalInfo().setAgentId(request.getPayUserId());
			paymentRequests.add(sourcePayment);
		}
		paymentRequests.forEach(p -> {
			p.getAdditionalInfo().setComments(request.getAdditionalInfo().getComments());
			p.setRefId(refId);
		});
		paymentList.addAll(paymentProcessor.process(paymentRequests));
		return paymentList;
	}

	private List<PaymentRequest> createAddFundPayments(FundTransferRequest request) {
		BigDecimal remaining = request.getAmount();
		request.setTotalAmountSum(request.getAmount());
		Pair<List<PaymentRequest>, BigDecimal> pair =
				NRCreditOutstandingPayments(request, remaining, request.getRefId());
		List<PaymentRequest> paymentRequests = pair.getLeft();
		remaining = pair.getRight();
		log.info("remaining = {}", remaining.doubleValue());
		if (remaining.compareTo(BigDecimal.ZERO) > 0) {
			// wallet topup
			WalletPaymentRequest targetPayment = WalletPaymentRequest.builder().amount(remaining)
					.refId(request.getRefId()).payUserId(request.getPayUserId()).opType(PaymentOpType.CREDIT)
					.transactionType(PaymentTransactionType.TOPUP).build();
			targetPayment.getAdditionalInfo().setTotalDepositAmount(request.getTotalAmountSum());
			paymentRequests.add(targetPayment);

			// subtract the wallet top-up amount from distributor.
			PaymentRequest sourcePayment = PaymentRequest.builder().amount(remaining)
					.payUserId(SystemContextHolder.getContextData().getUser().getUserId()).opType(PaymentOpType.DEBIT)
					.refId(request.getRefId()).transactionType(PaymentTransactionType.FUND_TRANSFER).build();
			sourcePayment.getAdditionalInfo().setAgentId(request.getPayUserId());
			paymentRequests.add(sourcePayment);
			sourcePayment.getAdditionalInfo().setAgentId(request.getPayUserId());
		}
		return paymentRequests;
	}

	private Pair<List<PaymentRequest>, BigDecimal> NRCreditOutstandingPayments(FundTransferRequest request,
			BigDecimal remainingAmount, String refId) {
		List<PaymentRequest> paymentRequests = new ArrayList<>();
		List<NRCredit> nrCredits = nrCreditService.getCurrentUnSettledCredits(request.getPayUserId());
		if (CollectionUtils.isNotEmpty(nrCredits)) {
			for (NRCredit nrCredit : nrCredits) {
				if (nrCredit.getUtilized().compareTo(BigDecimal.ZERO) > 0
						&& remainingAmount.compareTo(BigDecimal.ZERO) > 0) {
					BigDecimal transferAmount = nrCredit.getUtilized().min(remainingAmount);
					paymentRequests.add(createPaymentForNRCreditOutstanding(transferAmount, nrCredit, request, refId));
					remainingAmount = remainingAmount.subtract(transferAmount);
				}
			}
		}
		return Pair.of(paymentRequests, remainingAmount);
	}

	private PaymentRequest createPaymentForNRCreditOutstanding(BigDecimal amount, NRCredit nrCredit,
			FundTransferRequest request, String refId) {
		WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder().amount(amount)
				.payUserId(request.getPayUserId()).opType(PaymentOpType.CREDIT).walletNumber(nrCredit.getCreditId())
				.walletId(nrCredit.getId()).paymentMedium(PaymentMedium.CREDIT).refId(refId)
				.transactionType(PaymentTransactionType.TOPUP).additionalInfo(request.getAdditionalInfo()).build();
		paymentRequest.getAdditionalInfo().setActualTransactionType(PaymentTransactionType.UTILISATION_ADJUSTMENT);
		paymentRequest.getAdditionalInfo().setTotalDepositAmount(request.getTotalAmountSum());
		return paymentRequest;
	}

	public List<Payment> internalFundTransfer(MediumTransferRequest request) {
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		WalletPaymentRequest sourcePaymentRequest =
				WalletPaymentRequest.builder().amount(request.getAmount()).paymentMedium(request.getSource())
						.walletNumber(request.getSourceWalletNumber()).payUserId(request.getUserId())
						.opType(PaymentOpType.DEBIT).transactionType(PaymentTransactionType.INTERNAL_PAYMENT)
						.additionalInfo(PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build())
						.reason(request.getComments()).build();
		paymentRequestList.add(sourcePaymentRequest);

		WalletPaymentRequest targetPaymentRequest =
				WalletPaymentRequest.builder().amount(request.getAmount()).paymentMedium(request.getTarget())
						.walletNumber(request.getTargetWalletNumber()).payUserId(request.getUserId())
						.opType(PaymentOpType.CREDIT).transactionType(PaymentTransactionType.INTERNAL_PAYMENT)
						.additionalInfo(PaymentAdditionalInfo.builder().allowForDisabledMediums(true).build())
						.reason(request.getComments()).build();
		paymentRequestList.add(targetPaymentRequest);
		return paymentProcessor.process(paymentRequestList);
	}
}
