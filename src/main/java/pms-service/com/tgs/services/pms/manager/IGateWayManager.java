package com.tgs.services.pms.manager;

import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.utils.exception.PaymentException;

public interface IGateWayManager {

	public PaymentRequest initializeGatewayData(PaymentRequest payment, PaymentConfigOutput mediumConfig);

	public Payment refund(Payment payment, PaymentConfigOutput mediumConfig) throws PaymentException;

}
