package com.tgs.services.pms.manager;

import com.tgs.services.pms.datamodel.Payment;
import com.tgs.utils.exception.PaymentException;

public interface IPaymentManager {

	public Payment debit(Payment payment) throws PaymentException;

	public Payment credit(Payment payment) throws PaymentException;
}