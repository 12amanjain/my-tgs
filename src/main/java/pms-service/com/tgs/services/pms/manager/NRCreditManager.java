package com.tgs.services.pms.manager;

import static java.math.BigDecimal.ZERO;
import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.NRCreditFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.paymentManagers.NRBillingManager;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public class NRCreditManager {


	@Autowired
	private NRCreditService service;

	@Autowired
	private GeneralServiceCommunicator generalServiceCommunicator;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	private CreditPolicyService policyService;

	@Autowired
	@Qualifier("NRBillingManager")
	private NRBillingManager nrBillingManager;

	@Autowired
	private UserServiceCommunicator userServiceCommunicator;

	@Autowired
	private CreditSystemMessagingClient messagingClient;

	public NRCredit save(NRCredit credit) {
		if (StringUtils.isEmpty(credit.getCreditId()))
			return issueCredit(credit);
		return updateCredit(credit);
	}

	@Transactional
	private NRCredit issueCredit(NRCredit credit) {
		User user = userServiceCommunicator.getUserFromCache(credit.getUserId());
		String policyId = user.getUserConf().getCreditPolicyMap().get(Product.AIR);
		if (policyId == null && user.getUserConf().getDistributorId() == null) {
			throw new CustomGeneralException(SystemError.NO_POLICY_MAPPED);
		}
		CreditPolicy policy = null;
		if (policyId != null) {
			policy = policyService.findByPolicyId(policyId);
			credit.getAdditionalInfo().setPolicy(policyId);
			credit.setProducts(policy.getProducts());
		} else {
			credit.setProducts(Product.getBookingProducts());
		}
		List<NRCredit> previousCredits =
				service.search(NRCreditFilter.builder().userIdIn(Arrays.asList(credit.getUserId())).build());
		BigDecimal dues = ZERO;
		for (NRCredit c : previousCredits) {
			dues = dues.add(c.getUtilized());
		}
		validateLimit(credit, policy, dues);
		List<NRCredit> unExpiredCredits = previousCredits.stream()
				.filter(c -> c.getStatus().equals(CreditStatus.ACTIVE) || c.getStatus().equals(CreditStatus.BLOCKED))
				.collect(Collectors.toList());

		if (CollectionUtils.isNotEmpty(unExpiredCredits))
			throw new CustomGeneralException(SystemError.NRC_CANNOT_ISSUE);

		credit.setCreditId(NRCreditService.generateCreditId(credit.getUserId()));
		credit.setUtilized(ZERO);
		credit.setStatus(CreditStatus.ACTIVE);
		credit.setIssuedByUserId(SystemContextHolder.getContextData().getUser().getUserId());
		credit.getAdditionalInfo().setOriginalCreditExpiry(credit.getCreditExpiry());
		credit.getAdditionalInfo().setOriginalCreditLimit(credit.getCreditAmount());
		credit.getAdditionalInfo().setDIExpiry(credit.getCreditExpiry());
		credit.setPartnerId(user.getPartnerId());
		credit = service.save(credit);
		WalletPaymentRequest paymentRequest = WalletPaymentRequest.builder().amount(credit.getCreditAmount())
				.walletNumber(credit.getCreditId()).walletId(credit.getId()).payUserId(credit.getUserId())
				.paymentMedium(PaymentMedium.CREDIT).opType(PaymentOpType.CREDIT)
				.transactionType(PaymentTransactionType.CREDIT_ISSUED).reason(credit.getAdditionalInfo().getComments())
				.refId(credit.getAdditionalInfo().getRefId()).build();
		paymentRequest.getAdditionalInfo().setUtilizedBalance(credit.getUtilized());
		paymentRequest.getAdditionalInfo().setClosingBalance(credit.getCreditAmount());
		paymentProcessor.process(Stream.of(paymentRequest).collect(Collectors.toList()));
		return credit;
	}

	@Transactional
	private NRCredit updateCredit(NRCredit nuw) {
		NRCredit old = service.getByCreditId(nuw.getCreditId());
		if (old.getStatus().equals(CreditStatus.EXPIRED))
			throw new CustomGeneralException(SystemError.NRC_CANNOT_UPDATE);
		old.getAdditionalInfo().setComments(nuw.getAdditionalInfo().getComments());
		if (!nuw.getCreditAmount().equals(old.getCreditAmount())) {
			BigDecimal ext = nuw.getCreditAmount().subtract(old.getCreditAmount());
			updateLimitByAmount(old.getCreditId(), old, ext, null);
		}
		if (!nuw.getCreditExpiry().equals(old.getCreditExpiry())) {
			if (nuw.getCreditExpiry().isBefore(LocalDateTime.now()))
				throw new CustomGeneralException(SystemError.NRC_INVALID_EXPIRY);
			old.setCreditExpiry(nuw.getCreditExpiry());
			old.setStatus(CreditStatus.ACTIVE);
			old.incrementExtCount();
		}
		if (nuw.getStatus() != null && !nuw.getStatus().equals(old.getStatus())) {
			updateStatus(old, nuw.getStatus());
		}
		service.save(old);
		return old;
	}

	private Payment creditLimitChangePPB(NRCredit old, BigDecimal ext, String refId) {
		WalletPaymentRequest paymentRequest =
				WalletPaymentRequest.builder().amount(ext.abs()).walletNumber(old.getCreditId()).walletId(old.getId())
						.payUserId(old.getUserId()).paymentMedium(PaymentMedium.CREDIT)
						.opType(ext.compareTo(ZERO) < 0 ? PaymentOpType.DEBIT : PaymentOpType.CREDIT)
						.transactionType(ext.compareTo(ZERO) < 0 ? PaymentTransactionType.CREDIT_RECALLED
								: PaymentTransactionType.CREDIT_ISSUED)
						.reason(old.getAdditionalInfo().getComments()).refId(refId).build();
		paymentRequest.getAdditionalInfo().setUtilizedBalance(old.getUtilized());
		paymentRequest.getAdditionalInfo().setClosingBalance(old.getBalance());
		return paymentProcessor.process(Stream.of(paymentRequest).collect(Collectors.toList())).get(0);
	}

	@Transactional
	public Payment updateLimitByAmount(String creditId, NRCredit credit, BigDecimal amount, String refId) {
		if (credit == null) {
			credit = service.getByCreditId(creditId);
		}
		credit.setCreditAmount(credit.getCreditAmount().add(amount));
		CreditPolicy policy = null;
		if (credit.getAdditionalInfo().getPolicy() != null) {
			policy = policyService.findByPolicyId(credit.getAdditionalInfo().getPolicy());
		}
		List<NRCredit> previousCredits =
				service.search(NRCreditFilter.builder().userIdIn(Arrays.asList(credit.getUserId())).build());
		BigDecimal dues = ZERO;
		for (NRCredit c : previousCredits) {
			dues = dues.add(c.getUtilized());
		}
		validateLimit(credit, policy, dues);
		service.save(credit);
		return creditLimitChangePPB(credit, amount, refId);
	}

	private void validateLimit(NRCredit credit, CreditPolicy creditPolicy, BigDecimal dues) {
		if (creditPolicy != null
				&& credit.getCreditAmount().add(dues).compareTo(creditPolicy.getAdditionalInfo().getMaxLimit()) > 0) {
			throw new CustomGeneralException(SystemError.MAX_CREDIT_LIMIT_EXCEEDED);
		}
		if (credit.getUtilized().compareTo(credit.getCreditAmount()) > 0) {
			throw new CustomGeneralException(SystemError.INSUFFICIENT_BALANCE);
		}
	}

	private void updateStatus(NRCredit credit, CreditStatus status) {
		if (status == null)
			throw new CustomGeneralException(SystemError.INVALID_VALUE);
		if (status.equals(CreditStatus.EXPIRED)) {
			generateBillAndExpire(credit);
		} else
			credit.setStatus(status);
	}

	private void generateBillAndExpire(NRCredit credit) {
		List<Payment> paymentList = paymentService
				.search(PaymentFilter.builder().payUserIdIn(Collections.singletonList(credit.getUserId()))
						.createdOnAfterDateTime(credit.getCreatedOn()).createdOnBeforeDateTime(LocalDateTime.now())
						.status(PaymentStatus.SUCCESS)
						.pageAttr(PageAttributes.builder().pageNumber(0).size(100000).build()).build())
				.stream().filter(p -> p.getPaymentMedium().equals(PaymentMedium.CREDIT)
						&& credit.getId().equals(p.getWalletId()))
				.collect(Collectors.toList());
		CreditBill bill = nrBillingManager.generateBillAndExpire(credit, paymentList);
		if (bill != null) {
			messagingClient.sendCreditBillMail(bill, credit.toCredit(), EmailTemplateKey.CREDITBILL_STATEMENT_EMAIL,
					Duration.ZERO);
		}
	}

	public NRCredit getExpiryTime(NRCredit credit) {
		/*
		 * List<PaymentConfigurationRule> paymentConfigurationRules = PaymentConfigurationHelper
		 * .getPaymentRuleBasedOnRuleType(PaymentFact.builder().build(), NR_CREDIT_EXPIRY); if
		 * (!paymentConfigurationRules.isEmpty()) { NRCreditExpiryOutput output = (NRCreditExpiryOutput)
		 * paymentConfigurationRules.get(0).getOutput(); LocalDateTime creditExpiry =
		 * CustomisedDateTime.getAbsoluteDateTime(output.getCreditExpiry());
		 * credit.setCreditExpiry(generalServiceCommunicator.nextWorkingDay(creditExpiry));
		 * credit.getAdditionalInfo().setDIExpiry(credit.getCreditExpiry()); }
		 */
		credit.setCreditExpiry(LocalDateTime.now().plusDays(1));
		return credit;
	}

}
