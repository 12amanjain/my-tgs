package com.tgs.services.pms.manager;

import com.tgs.services.base.SpringContext;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.PaymentConfigurationService;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PaymentManagerFactory {

	public static IPaymentManager getPaymentManager(Payment payment) {
		if (payment.getPaymentMedium().equals(PaymentMedium.WALLET)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("walletPaymentManager");
		}
		if (payment.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("creditLinePaymentManager");
		}
		if (payment.getPaymentMedium().equals(PaymentMedium.CREDIT)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("NRCreditPaymentManager");
		}
		if (payment.getPaymentMedium().equals(PaymentMedium.CREDITCARD)
				|| payment.getPaymentMedium().equals(PaymentMedium.DEBITCARD)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("vendorPaymentManager");
		}
		if (payment.getPaymentMedium().equals(PaymentMedium.NETBANKING)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("vendorPaymentManager");
		}
		if (payment.getPaymentMedium().equals(PaymentMedium.VIRTUAL_PAYMENT)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("virtualPaymentManager");
		}
		if (payment.getPaymentMedium().equals(PaymentMedium.POINTS)) {
			return (IPaymentManager) SpringContext.getApplicationContext().getBean("pointsPaymentManager");
		}
		throw new PaymentException(SystemError.PAYMENT_MEDIUM_NOT_SUPPORTED);
	}

	/**
	 * @param payment
	 *        <p>
	 *        The Payment Object Must Specify which Medium & Product to find the GateWay for Specified GateWay
	 *        {@link GateWayType } - for Debit, Credit or Refund Process
	 *        </p>
	 * @return Payment with pgDetail
	 */
	public static PaymentRequest initializeGatewayRedirectionData(PaymentRequest payment,
			PaymentConfigurationRule rule) {
		PaymentConfigOutput paymentConfig = (PaymentConfigOutput) rule.getOutput();
		GateWayManager gateWayManager = getGateWayManager(paymentConfig);
		if (gateWayManager != null) {
			log.info("GateWayManager found for bookingId {}, ruleId {}, GateWayType {}", payment.getRefId(),
					payment.getAdditionalInfo().getRuleId(), paymentConfig.getGatewayType());
			return gateWayManager.initializeGatewayData(payment, paymentConfig);
		}
		log.error("Couldn't find GateWayManager for bookingId {}, ruleId {}, GateWayType {}", payment.getRefId(),
				payment.getAdditionalInfo().getRuleId(), paymentConfig.getGatewayType());
		return null;
	}

	public static Payment processGatewayRefund(Payment payment) {
		try {
			PaymentConfigurationService configurationService =
					SpringContext.getApplicationContext().getBean(PaymentConfigurationService.class);
			PaymentConfigurationRule paymentConfigurationRule =
					configurationService.fetchRuleById(payment.getAdditionalInfo().getRuleId()).toDomain();
			PaymentConfigOutput paymentConfig = (PaymentConfigOutput) paymentConfigurationRule.getOutput();
			GateWayManager gateWayManager =
					PaymentUtils.getGatewayManager(GateWayType.valueOf(payment.getAdditionalInfo().getGateway()));
			if (gateWayManager != null) {
				return gateWayManager.refund(payment, paymentConfig);
			} else {
				log.error("Unable to process refund for refId {}. GateWayManager is null", payment.getRefId());
				throw new PaymentException(SystemError.REFUND_FAILED);
			}
		} catch (PaymentException e) {
			throw e;
		} catch (Exception e) {
			log.error("Unable to process refund for refId {} ", payment.getRefId(), e);
			throw new PaymentException(SystemError.REFUND_FAILED);
		}
	}

	private static GateWayManager getGateWayManager(PaymentConfigOutput paymentConfig) {
		GateWayManager gateWayManager = null;
		if (paymentConfig != null && paymentConfig.getGatewayType() != null) {
			gateWayManager = PaymentUtils.getGatewayManager(paymentConfig.getGatewayType());
		}
		return gateWayManager;
	}

}
