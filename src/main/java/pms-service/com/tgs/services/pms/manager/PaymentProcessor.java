package com.tgs.services.pms.manager;

import static com.tgs.services.pms.helper.PaymentUtils.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.hibernate.exception.LockAcquisitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.pms.datamodel.PGDetail;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.helper.PaymentHelper;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.ExternalPaymentService;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.paymentManagers.PaymentMessenger;
import com.tgs.services.pms.mapper.PaymentRequestToPaymentMapper;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.pms.servicehandler.DebitHandler;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentProcessor {

	@Autowired
	private TransactionHandler transactionHandler;

	@Autowired
	private PaymentService paymentService;
	@Autowired
	private ExternalPaymentService externalPaymentService;

	@Autowired
	private FundHandler fundHandler;

	@Autowired
	private DebitHandler debitHandler;

	@Autowired
	private PaymentHelper paymentHelper;

	@Autowired
	private PaymentRequestToPaymentMapper paymentMapper;

	@Autowired
	private OrderServiceCommunicator orderComm;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private PaymentMessenger paymentMessenger;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	private static final int MAX_RETRY = 5;

	private static final int MAX_DEPTH = 5;

	// Don't make it transactional, otherwise refresh balance in cache will not work for failed payment.
	public List<Payment> process(List<PaymentRequest> paymentRequests) throws PaymentException {
		return tryProcess(paymentRequests, 0);
	}

	// Don't make it transactional, otherwise refresh balance in cache will not work for failed payment.
	private List<Payment> tryProcess(List<PaymentRequest> paymentRequests, int tryCount) throws PaymentException {
		for (PaymentRequest p : paymentRequests) {
			log.info("Payment Requests info log " + new Gson().toJson(p));
		}
		validatePaymentRequests(paymentRequests);
		if (holdPaymentsForPGResponse(paymentRequests)) {
			List<Payment> outPayments = new ArrayList<>();
			paymentRequests.forEach(p -> {
				Payment payment = paymentMapper.toPayment(p);
				payment.setStatus(PaymentStatus.INITIATE_REDIRECTION);
				outPayments.add(payment);
			});
			return outPayments;
		}
		List<Payment> payments = preparePayments(paymentRequests);
		for (Payment p : payments) {
			log.info("Payments info " + new Gson().toJson(p));
		}
		try {
			payments = transactionHandler.doPayments(payments);
		} catch (PaymentException e) {
			payments.forEach(p -> paymentService.storeFailedPayment(p, e.getError()));
			paymentRequests.forEach(p -> log.info("TGS PaymentException occurred: {} \n Payment Request: {}", e, p));
			throw e;
		} catch (Exception e) {
			if (tryCount < MAX_RETRY && retry(e)) {
				log.info(
						"PaymentProcessor.tryProcess: LockAcquisitionException encountered. Retrying payment in 500ms!");
				tryCount++;
				try {
					Thread.sleep(500);
				} catch (InterruptedException iex) {
					log.error("PaymentProcessor.tryProcess: Encountered InterruptedException while retrying payment!");
					throw new CustomGeneralException("Encountered InterruptedException while retrying payment!");
				}
				return tryProcess(paymentRequests, tryCount);
			}
			log.error("PMS Runtime Exception " + e.getMessage(), e);
			paymentRequests.forEach(p -> log.error("Payment Request: " + new Gson().toJson(p)));
			throw new PaymentException(SystemError.UNABLE_TO_PROCESS);
		} finally {
			log.info("Transaction finished !! for referenceId {}", payments.get(0).getRefId());
			paymentHelper.refreshUserBalanceInfoInCache(payments.get(0).getPayUserId());
		}
		paymentMessenger.sendPaymentEmail(payments);
		return payments;
	}

	private boolean retry(Throwable ex) {
		int depth = MAX_DEPTH;
		while (depth > 0) {
			if (ex instanceof LockAcquisitionException) {
				return true;
			}
			if (ex.getCause() == null)
				return false;
			ex = ex.getCause();
			depth--;
		}
		return false;
	}

	private List<Payment> preparePayments(List<PaymentRequest> paymentRequests) {
		List<Payment> paymentList = new ArrayList<>();
		for (PaymentRequest paymentRequest : paymentRequests) {
			if (paymentRequest.getOpType() == null) {
				paymentRequest.setOpType(paymentRequest.getTransactionType().getOpType());
			}
			handleTds(paymentRequest);
			if (paymentRequest.getPaymentMedium() == null) {
				if (paymentRequest.getOpType().equals(PaymentOpType.CREDIT)) {
					List<PaymentRequest> fundHandlerPRs = fundHandler.dividePaymentRequest(paymentRequest);
					for (PaymentRequest p : fundHandlerPRs) {
						log.info("Fund Handler: Divided PRs: " + new Gson().toJson(p));
					}
					handleDuplicateMerchantTxnIdConstraint(fundHandlerPRs);
					paymentList.addAll(paymentMapper.toPayments(fundHandlerPRs));
				} else {
					WalletPaymentRequest walletPaymentRequest = new WalletPaymentRequest(paymentRequest);
					List<PaymentRequest> DebitHandlerPRs = debitHandler.evaluatePaymentMediums(walletPaymentRequest);
					for (PaymentRequest p : DebitHandlerPRs) {
						log.info("Debit Handler: Divided PRs: " + new Gson().toJson(p));
					}
					paymentList.addAll(paymentMapper.toPayments(DebitHandlerPRs));
				}
			} else {
				paymentList.add(paymentMapper.toPayment(paymentRequest));
			}
		}
		paymentList.removeIf(Objects::isNull);
		return paymentList;
	}

	// increase payment amount by 1 paisa if it's equal to another CL payment amount of this transaction
	private void handleDuplicateMerchantTxnIdConstraint(List<PaymentRequest> paymentRequests) {
		List<PaymentRequest> billPayments =
				paymentRequests.stream().filter(p -> p.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE)
						|| p.getPaymentMedium().equals(PaymentMedium.CREDIT)).collect(Collectors.toList());
		BigDecimal onePaisa = BigDecimal.valueOf(0.01);
		if (billPayments.size() > 1) {
			for (int i = 0; i < billPayments.size(); i++) {
				for (int j = i + 1; j < billPayments.size(); j++) {
					PaymentRequest pr1 = billPayments.get(i);
					PaymentRequest pr2 = billPayments.get(j);
					if (pr1.getAmount().compareTo(pr2.getAmount()) == 0) {
						pr2.setAmount(pr2.getAmount().add(onePaisa));
					}
				}
			}
		}
	}

	private void handleTds(PaymentRequest paymentRequest) {
		if ((paymentRequest.getTransactionType().equals(PaymentTransactionType.COMMISSION)
				|| paymentRequest.getTransactionType().equals(PaymentTransactionType.DEPOSIT_INCENTIVE))
				&& paymentRequest.getTds() == null && paymentRequest.getOpType().equals(PaymentOpType.CREDIT)) {
			log.info("Setting tds in pms for refId {}", paymentRequest.getRefId());
			User payUser = userService.getUserFromCache(paymentRequest.getPayUserId());
			if (payUser == null) {
				log.error("Invalid userId passed for paymentRequest {}", paymentRequest);
				throw new CustomGeneralException(SystemError.INVALID_PAYUSER_ID);
			}
			double tdsRateDouble = payUser.getAdditionalInfo() != null
					? ObjectUtils.defaultIfNull(payUser.getAdditionalInfo().getTdsRate(), 0.0)
					: 0d;
			BigDecimal tdsRate = BigDecimal.valueOf(tdsRateDouble / 100).setScale(4, BigDecimal.ROUND_HALF_UP);
			if (tdsRate.compareTo(BigDecimal.ZERO) > 0) {
				paymentRequest.setTds(tdsRate.multiply(paymentRequest.getAmount()));
				paymentRequest.setAmount(paymentRequest.getAmount().subtract(paymentRequest.getTds()));
			}
		} else {
			paymentRequest.setTds(ObjectUtils.firstNonNull(paymentRequest.getTds(), BigDecimal.ZERO));
		}
	}

	private boolean holdPaymentsForPGResponse(List<PaymentRequest> paymentRequestList) {
		boolean hasExternalDebitPayment = false;
		PaymentRequest extPaymentRequest = null;
		for (PaymentRequest p : paymentRequestList) {
			if (p.getPaymentMedium() != null && p.getPaymentMedium().isExternalPaymentMedium()
					&& p.getOpType().equals(PaymentOpType.DEBIT)) {
				hasExternalDebitPayment = true;
				extPaymentRequest = p;
				break;
			}
		}
		ContextData contextData = SystemContextHolder.getContextData();
		boolean containsPGResponse = contextData.getMetaInfo() != null;
		if (hasExternalDebitPayment) {
			// Need to consider negative points balance if external payment medium is involved
			for (PaymentRequest p : paymentRequestList) {
				if (p.getPaymentMedium() != null && p.getPaymentMedium().equals(PaymentMedium.POINTS)
						&& p.getOpType().equals(PaymentOpType.DEBIT)) {
					p.setConsiderNegativeBalance(true);
					break;
				}
			}
			// need to init PG and hold payments for PG response
			if (!containsPGResponse) {
				PaymentConfigurationRule rule = evaluateApplicablePGRule(extPaymentRequest);
				extPaymentRequest.getAdditionalInfo().setRuleId(rule.getId());
				extPaymentRequest.setRuleId(rule.getId());
				evaluatePGPaymentFee(rule, extPaymentRequest);
				PaymentManagerFactory.initializeGatewayRedirectionData(extPaymentRequest, rule);
				cacheAndHoldPayments(paymentRequestList, extPaymentRequest.getPgDetail());
				externalPaymentService.storePaymentInitiationInfo(toExternalPayment(extPaymentRequest));
				return true;
			}
		}
		return false;
	}
	private PaymentConfigurationRule evaluateApplicablePGRule(PaymentRequest paymentRequest) {
		Integer ruleId =
				ObjectUtils.firstNonNull(paymentRequest.getRuleId(), paymentRequest.getAdditionalInfo().getRuleId());
		if (ruleId != null) {
			PaymentConfigurationRule rule =
					PaymentConfigurationHelper.getPaymentRule(ruleId, PaymentRuleType.PAYMENT_MEDIUM);
			if (isPaymentProcessSupported(rule)) {
				return rule;
			}
		}
		PaymentFact paymentFact = PaymentFact.builder().isPaymentProcess(true).subMedium(paymentRequest.getSubMedium())
				.build().generateFact(paymentRequest);
		return PaymentConfigurationHelper.getPaymentRule(paymentFact, PaymentRuleType.PAYMENT_MEDIUM);
	}
	private boolean isPaymentProcessSupported(PaymentConfigurationRule rule) {
		return BooleanUtils.isNotFalse(rule.getInclusionCriteria().getIsPaymentProcess())
				&& (rule.getExclusionCriteria() == null
						|| BooleanUtils.isNotTrue(rule.getExclusionCriteria().getIsPaymentProcess()));
	}
	private void evaluatePGPaymentFee(PaymentConfigurationRule rule, PaymentRequest extPaymentRequest) {
		if (rule != null && SystemContextHolder.getContextData().getEmulateOrLoggedInUserId() != null
				&& extPaymentRequest.getPaymentFee().equals(BigDecimal.ZERO)) {
			BigDecimal paymentFee = PaymentUtils.getPaymentFee(SystemContextHolder.getContextData().getUser(),
					extPaymentRequest.getAmount(), rule, null);
			extPaymentRequest.setAmount(extPaymentRequest.getAmount().add(paymentFee));
			extPaymentRequest.setPaymentFee(paymentFee);
		}
	}

	private void cacheAndHoldPayments(List<PaymentRequest> payments, PGDetail pgDetail) {
		if (pgDetail != null) {
			HttpUtils httpUtils =
					HttpUtils.builder().urlString("/pg/redirect/").queryParams(pgDetail.getPaymentFields()).build();
			String response = httpUtils.constructUrlWithQueryParams();
			log.info("Payment Link Redirecting for BookingId {}, redirectionURL {} ", payments.get(0).getRefId(),
					response);
			/**
			 * Logging for future reference
			 */
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(response)
					.key(payments.get(0).getRefId()).type("Payment Redirection URL").logType("AirSupplierAPILogs")
					.build()));
			// header name needs to change because UI team has written some rule over it
			String headerName = "redirectionUrl";
			String gatewayType = pgDetail.getPaymentFields().get("gatewayType");
			if (gatewayType != null && (gatewayType.equals(GateWayType.RAZOR_PAY.name())
					|| gatewayType.equals(GateWayType.CREDIMAX.name()))) {
				headerName = "pgData";
			}
			SystemContextHolder.getContextData().getHttpResponse().setHeader(headerName, response);

			/**
			 * Storing in cache for later re-use with ttl of 1hr.
			 */
			CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.EXTERNAL_PAYMENTS.getName()).key(payments.get(0).getRefId()).build();
			Map<String, List<PaymentRequest>> bins = new HashMap<>();
			bins.put(BinName.PAYMENTS.name(), payments);
			/**
			 * user can sit on payment page for 96 hrs. Temporarily for PayU certification
			 */
			cachingCommunicator.store(metaInfo, bins, false, false, 96 * 3600);

			/**
			 * Storing header Info which will be used post redirection from payment gateway
			 */
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.EXTERNAL_PAYMENTS.getName()).key("header" + payments.get(0).getRefId()).build();
			cachingCommunicator.store(metaInfo, BinName.PAYMENTS.name(),
					Arrays.asList(SystemContextHolder.getContextData().getHttpHeaders().getPayHeader()), false, false,
					96 * 3600);
		}
	}
	private void validatePaymentRequests(List<PaymentRequest> paymentRequestList) {
		paymentRequestList.forEach(request -> {
			if (StringUtils.isEmpty(request.getPayUserId())
					&& Product.getProductFromId(request.getRefId()).hasOrder()) {
				Order order = orderComm.findByBookingId(request.getRefId());
				request.setPayUserId(order.getBookingUserId());
			}
			if (StringUtils.isEmpty(request.getPartnerId())) {
				User payUser = userService.getUserFromCache(request.getPayUserId());
				request.setPartnerId(payUser.getPartnerId());
			}

		});
		paymentRequestList.forEach(p -> {
			if (p.getTransactionType() == null || p.getPayUserId() == null)
				throw new PaymentException(SystemError.INVALID_VALUE);
			if (p.getAmount().compareTo(BigDecimal.ZERO) == 0 && !p.getTransactionType().forLimitChange()
					&& !p.isAllowZeroAmount()) {
				throw new PaymentException(SystemError.INVALID_PAYMENT_REQUEST);
			}
			if (p.getAmount().compareTo(BigDecimal.ZERO) < 0) {
				throw new PaymentException(SystemError.INVALID_PAYMENT_REQUEST);
			}
		});
		validateRefundAmount(paymentRequestList);
		checkStaffDailyUsage(paymentRequestList);
	}

	private void validateRefundAmount(List<PaymentRequest> newPayments) {
		BigDecimal newPaymentAmount = PaymentUtils.totalAmountPR(newPayments);
		BigDecimal oldPaymentAmount;
		if (newPayments.get(0).getRefId() != null
				&& Product.getProductFromId(newPayments.get(0).getRefId()).hasOrder()) {
			List<Payment> oldPayments = paymentService.search(PaymentFilter.builder()
					.refId(newPayments.get(0).getRefId()).status(PaymentStatus.SUCCESS).isInternalQuery(true).build());
			oldPaymentAmount = PaymentUtils.totalAmount(oldPayments);
			/**
			 * At any time, total amount refund to customer can't exceed charge amount. Note: To avoid issues related to
			 * double handling, compare the sum with -1.
			 */
			if (newPaymentAmount.add(oldPaymentAmount).compareTo(BigDecimal.valueOf(-1)) < 0) {
				throw new PaymentException(SystemError.CREDIT_ERROR);
			}
		}
	}

	private void checkStaffDailyUsage(List<PaymentRequest> paymentRequestList) {
		PaymentRequest payment = paymentRequestList.get(0);
		User loggedInUser =
				userService.getUserFromCache(SystemContextHolder.getContextData().getEmulateOrLoggedInUserId());
		if (loggedInUser == null || !loggedInUser.getRole().equals(UserRole.AGENT_STAFF))
			return;
		BigDecimal dayUsage;
		BigDecimal totalPaymentAmount = PaymentUtils.totalAmountPR(paymentRequestList);
		if (loggedInUser.getAdditionalInfo() != null) {
			double dailyLimit = ObjectUtils.defaultIfNull(loggedInUser.getAdditionalInfo().getDailyUsageLimit(), 0d);
			double walletLimit = 0d;
			if (dailyLimit > 0) {
				PaymentFilter filter =
						PaymentFilter.builder().loggedInUserIdIn(Collections.singletonList(payment.getLoggedInUserId()))
								.status(PaymentStatus.SUCCESS).isInternalQuery(true).build();
				filter.setCreatedOnBeforeDate(LocalDate.now());
				filter.setCreatedOnAfterDate(LocalDate.now());
				List<Payment> payments = paymentService.search(filter);
				if (dailyLimit > 0) {
					payments = payments.stream()
							.filter(p -> p.getPaymentMedium().equals(PaymentMedium.CREDIT_LINE)
									|| p.getPaymentMedium().equals(PaymentMedium.WALLET)
									|| p.getPaymentMedium().equals(PaymentMedium.CREDIT))
							.collect(Collectors.toList());
					dayUsage = Payment.sum(payments);
					if (dayUsage.doubleValue() + totalPaymentAmount.doubleValue() > dailyLimit)
						throw new PaymentException(SystemError.DAILY_USAGE_LIMIT_EXCEEDED);
				}
				if (walletLimit > 0) {
					dayUsage = Payment
							.sum(payments.stream().filter(p -> p.getPaymentMedium().equals(payment.getPaymentMedium()))
									.collect(Collectors.toList()));
					if (dayUsage.doubleValue() + totalPaymentAmount.doubleValue() > walletLimit)
						throw new PaymentException(SystemError.WALLET_USAGE_LIMIT_EXCEEDED);
				}
			}
		}
	}
}
