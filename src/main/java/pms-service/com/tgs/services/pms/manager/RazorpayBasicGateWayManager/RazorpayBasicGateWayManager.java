package com.tgs.services.pms.manager.RazorpayBasicGateWayManager;

import java.math.BigDecimal;
import java.util.Map;

import javax.xml.bind.DatatypeConverter;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.reflect.TypeToken;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.CurrencyConverter;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorPayBasicRefundInfo;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorPayBasicRefundResponse;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicMerchantInfo;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicPgResponse;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicTransactionDetails;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RazorpayBasicGateWayManager extends GateWayManager {

	private RazorpayBasicPgResponse razorpayPgResponse;

	@Autowired
	private UserServiceCommunicator userService;

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		RazorpayBasicMerchantInfo razorPayBasicGatewayInfo = (RazorpayBasicMerchantInfo) gatewayInfo;
		User payUser = userService.getUserFromCache(payment.getPayUserId());
		String razorPayBasicOrderId = createRazorOrderId(razorPayBasicGatewayInfo, payment);
		razorPayBasicGatewayInfo.setOrder_id(razorPayBasicOrderId);
		razorPayBasicGatewayInfo.setAmount(CurrencyConverter.toSubUnit(payment.getAmount()).toString());
		razorPayBasicGatewayInfo.setEmail(payUser.getEmail());
		razorPayBasicGatewayInfo.setContact(payUser.getMobile());
		razorPayBasicGatewayInfo.setNotes(payment.getRefId());
		razorPayBasicGatewayInfo.appendRefIdToCallBack(payment.getRefId());
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		return super.convertToMap(gatewayInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		razorpayPgResponse = (RazorpayBasicPgResponse) pgResponse;
		RazorpayBasicMerchantInfo merchantInfo = (RazorpayBasicMerchantInfo) gatewayMerchantInfo;
		PaymentProcessingResult verificationResult = new PaymentProcessingResult();
		if (verifyPaymentSignature(merchantInfo, verificationResult)) {
			verificationResult.setStatus(PaymentStatus.SUCCESS);
			verificationResult.getExternalPaymentInfo().setMerchantTxnId(getRazorpayPaymentId(razorpayPgResponse));
		} else {
			verificationResult.setStatus(PaymentStatus.FAILURE);
		}
		return verificationResult;
	}

	public Boolean verifyPaymentSignature(RazorpayBasicMerchantInfo merchantInfo,
			PaymentProcessingResult verificationResult) {
		String razorpayPaymentId = razorpayPgResponse.getRazorpay_payment_id();
		if (razorpayPaymentId != null) {
			String expectedSignature = razorpayPgResponse.getRazorpay_signature();
			String orderId = razorpayPgResponse.getRazorpay_order_id();
			String payload = orderId + '|' + razorpayPaymentId;
			if (verifySignature(payload, expectedSignature, merchantInfo.getSecret_key())) {
				return true;
			}
			verificationResult.getExternalPaymentInfo().setGatewayComment("Signature mismatch");
		} else {
			verificationResult.getExternalPaymentInfo().setGatewayComment(razorpayPgResponse.getErrorDesc());
		}
		return false;
	}

	public boolean verifySignature(String data, String expectedSignature, String secret) {
		try {
			String actualSignature = DatatypeConverter
					.printHexBinary(EncryptionUtils.encryptUsingHMACSHA256(data, secret)).toLowerCase();
			return actualSignature.equals(expectedSignature);
		} catch (Exception e) {
			throw new RuntimeException("Failed to generate HMAC : " + e.getMessage(), e);
		}
	}

	@SuppressWarnings("serial")
	private String getRazorpayPaymentId(RazorpayBasicPgResponse razorpayPgResponse) {
		String razorpayPaymentId = razorpayPgResponse.getRazorpay_payment_id();
		if (razorpayPgResponse.getRazorpay_payment_id() == null) {
			Map<String, String> map = GsonUtils.getGson().fromJson(razorpayPgResponse.getErrorMetaData(),
					new TypeToken<Map<String, String>>() {
					}.getType());
			razorpayPaymentId = map.get("payment_id");
		}
		return razorpayPaymentId;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		RazorpayBasicMerchantInfo merchantInfo = (RazorpayBasicMerchantInfo) gatewayMerchantInfo;
		com.razorpay.Payment razorPayBasicPayment;
		try {
			RazorpayClient razorpayClient = getrazorPayBasicClient(merchantInfo.getKey_id(),
					merchantInfo.getSecret_key());
			String paymentId = payment.getMerchantTxnId();
			razorPayBasicPayment = razorpayClient.Payments.fetch(paymentId);
			JSONObject notesObj = razorPayBasicPayment.get("notes");
			if (payment.getRefId().trim().compareTo(notesObj.get("transaction_id").toString()) != 0) {
				log.error("Mismatched refId {}. Stored RefId: {},", payment.getRefId(), notesObj.get("transaction_id"));
				return null;
			}
			RazorpayBasicTransactionDetails transactionDetails = GsonUtils.getGson()
					.fromJson(razorPayBasicPayment.toJson().toString(), RazorpayBasicTransactionDetails.class);
			log.info("Verication response for Razor pay basic payment with refId {} is {}", payment.getRefId(),
					razorPayBasicPayment.toString());
			return prepareTrackingResult(transactionDetails, payment);
		} catch (Exception e) {
			log.error("Unable to verify payment for refId {} due to ", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "RazorPayBasicVerification", e);
		}
		return null;
	}

	private PaymentProcessingResult prepareTrackingResult(RazorpayBasicTransactionDetails transactionDetails,
			Payment payment) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		if (razorpayPgResponse != null) {
			trackingResult.getExternalPaymentInfo().setGatewayComment(razorpayPgResponse.getErrorDesc());
		}
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(transactionDetails.getStatus());
		if (transactionDetails.getStatus().equalsIgnoreCase("captured")) {
			if (payment.getAmount().compareTo(CurrencyConverter.toUnit(transactionDetails.getAmount())) == 0) {
				trackingResult.setStatus(PaymentStatus.SUCCESS);
			} else {
				log.error("Mismatched Amount for refId {}. Amount stored: {}, amount received: {}", payment.getRefId(),
						payment.getAmount(), transactionDetails.getAmount());
				trackingResult.setStatus(PaymentStatus.FAILURE);
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
		}
		return trackingResult;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		return RazorPayBasicRefundInfo.builder().refundAmt(String.valueOf(payment.getAmount()))
				.razorPayBasictxnId(payment.getMerchantTxnId()).refId(payment.getPaymentRefId()).speedType("normal")
				.build();
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo) {
		RazorPayBasicRefundInfo razorPayBasicRefundInfo = (RazorPayBasicRefundInfo) refundInfo;
		RazorpayBasicMerchantInfo merchantInfo = (RazorpayBasicMerchantInfo) gatewayInfo;
		com.razorpay.Refund razorPayBasicRefund;
		boolean isRefundSuccessful = false;
		String merchantRefundId = null;
		try {
			RazorpayClient razorpayClient = getrazorPayBasicClient(merchantInfo.getKey_id(),
					merchantInfo.getSecret_key());
			JSONObject refundRequest = new JSONObject();
			refundRequest.put("amount",
					CurrencyConverter.toSubUnit(new BigDecimal(razorPayBasicRefundInfo.getRefundAmt())));
			refundRequest.put("speed", razorPayBasicRefundInfo.getSpeedType());
			refundRequest.put("receipt", razorPayBasicRefundInfo.getRefId());
			razorPayBasicRefund = razorpayClient.Payments.refund(razorPayBasicRefundInfo.getRazorPayBasictxnId(),
					refundRequest);
			RazorPayBasicRefundResponse refundResponse = GsonUtils.getGson()
					.fromJson(razorPayBasicRefund.toJson().toString(), RazorPayBasicRefundResponse.class);
			log.info("Refund response for Razor Pay Basic payment with refId {} is {}", payment.getRefId(),
					razorPayBasicRefund.toString());
			isRefundSuccessful = "processed".equals(refundResponse.getStatus());
			merchantRefundId = refundResponse.getRefundId();
		} catch (Exception e) {
			log.error("Unable to initiate refund for refId {} due to ", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "RazorPayBasicRefund", e);
		}
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(merchantRefundId).build();
	}

	private String createRazorOrderId(RazorpayBasicMerchantInfo razorPayBasicInfo, PaymentRequest payment) {
		com.razorpay.Order razorPayBasicOrder;
		try {
			RazorpayClient razorpayClient = getrazorPayBasicClient(razorPayBasicInfo.getKey_id(),
					razorPayBasicInfo.getSecret_key());
			JSONObject orderRequest = new JSONObject();
			orderRequest.put("amount", CurrencyConverter.toSubUnit(payment.getAmount()));
			orderRequest.put("currency", razorPayBasicInfo.getCurrency());
			orderRequest.put("receipt", payment.getRefId());
			orderRequest.put("payment_capture", true);
			razorPayBasicOrder = razorpayClient.Orders.create(orderRequest);
		} catch (RazorpayException ex) {
			log.error("Exception while creating Razorpay Basic order for refId {}. Error: {}. stackTrace {}",
					payment.getRefId(), ex.getMessage(), ex.getStackTrace());
			throw new CustomGeneralException(SystemError.RAZORPAY_ORDER);
		}
		if (payment.getTransactionType().equals(PaymentTransactionType.TOPUP)) {
			payment.setMerchantTxnId(razorPayBasicOrder.get("id"));
		}
		return razorPayBasicOrder.get("id");
	}

	public RazorpayClient getrazorPayBasicClient(String pk, String sk) throws RazorpayException {
		return new RazorpayClient(pk, sk);
	}

	@Override
	protected String getGatewayName() {
		return "Razorpay PG";
	}
}
