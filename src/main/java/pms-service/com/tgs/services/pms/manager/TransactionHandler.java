package com.tgs.services.pms.manager;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.utils.exception.PaymentException;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TransactionHandler {

	@Autowired
	private PaymentPostProcessor postProcessor;

	@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.REPEATABLE_READ)
	public List<Payment> doPayments(List<Payment> payments) {
		List<Payment> outPayments = new ArrayList<>();
		for (Payment p : payments) {
			Payment processedPayment = doPayment(p);
			outPayments.add(processedPayment);
		}
		postProcessor.doPaymentPostProcessing(outPayments);
		return outPayments;
	}


	@Transactional(propagation = Propagation.REQUIRED)
	private Payment doPayment(Payment payment) {
		IPaymentManager paymentManager;
		paymentManager = PaymentManagerFactory.getPaymentManager(payment);
		if (payment.getOpType().equals(PaymentOpType.CREDIT)) {
			return paymentManager.credit(payment);
		}
		if (payment.getOpType().equals(PaymentOpType.DEBIT)) {
			return paymentManager.debit(payment);

		}
		throw new PaymentException(SystemError.UNSUPPORTED_PAYMENT_OP);
	}
}