package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditBillCycleType;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.pms.datamodel.UserPoint;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.helper.CreditBillHelper;
import com.tgs.services.pms.helper.CreditLineHelper;
import com.tgs.services.pms.helper.NRCreditHelper;
import com.tgs.services.pms.helper.UserPointsHelper;
import com.tgs.services.pms.helper.UserWalletHelper;
import com.tgs.services.ums.datamodel.DueInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserDuesSummary;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserBalanceManager {

	@Autowired
	private UserServiceCommunicator userServiceCommunicator;

	public Map<String, BigDecimal> getUserBalanceFromCache(PaymentMetaInfo paymentMetaInfo) {
		Map<String, BigDecimal> userBalance = new HashMap<>();
		if (BooleanUtils.isNotFalse(paymentMetaInfo.getFetchWalletBalance())) {
			Map<String, UserWallet> walletBalanceFromCache =
					UserWalletHelper.getUserWallet(paymentMetaInfo.getUserIds());
			walletBalanceFromCache.forEach((userId, userWallet) -> {
				BigDecimal balance = BigDecimal.ZERO;
				if (walletBalanceFromCache.get(userId).getBalance().equals(BigDecimal.ZERO)) {
					User user = userServiceCommunicator.getUserFromCache(userId);
					if (StringUtils.isNotEmpty(user.getParentUserId())) {
						Map<String, UserWallet> parentWalletBalance =
								UserWalletHelper.getUserWallet(Arrays.asList(user.getParentUserId()));
						balance = parentWalletBalance.getOrDefault(user.getParentUserId(), UserWallet.builder().build())
								.getBalance();
					}
				} else {
					try {
						balance = walletBalanceFromCache.get(userId).getBalance();
					} catch (Exception e) {
						log.error("Unable to fetch userwallet balance for userId {}", userId, e);
					}
				}
				userBalance.put(userId, balance);
			});
		}

		if (BooleanUtils.isNotFalse(paymentMetaInfo.getFetchCreditLineBalance())) {
			Map<String, List<CreditLine>> creditLineMap =
					CreditLineHelper.getCreditLineMap(paymentMetaInfo.getUserIds());
			creditLineMap.forEach((user, creditLines) -> {
				if (CollectionUtils.isNotEmpty(creditLines)) {
					for (CreditLine cline : creditLines) {
						userBalance.put(user, userBalance.getOrDefault(user, BigDecimal.ZERO).add(cline.getBalance()));
					}
				}
			});
		}

		if (BooleanUtils.isNotFalse(paymentMetaInfo.getFetchNRCredit())) {
			Map<String, List<NRCredit>> nrCreditsMp = NRCreditHelper.getCreditsByUser(paymentMetaInfo.getUserIds());
			nrCreditsMp.forEach((user, nrCredits) -> {

				if (CollectionUtils.isNotEmpty(nrCredits)) {
					for (NRCredit cline : nrCredits) {
						userBalance.put(user, userBalance.getOrDefault(user, BigDecimal.ZERO).add(cline.getBalance()));
					}
				}

			});
		}
		return userBalance;
	}

	public UserBalanceSummary getUserBalanceSummary(String userId) {
		UserBalanceSummary summary = new UserBalanceSummary();
		UserWallet userWallet = UserWalletHelper.fetchUserWallet(userId);
		if (userWallet != null) {
			summary.setWalletBalance(userWallet.getBalance());
		}
		List<CreditLine> creditLines = CreditLineHelper.getCreditLines(userId);
		if (CollectionUtils.isNotEmpty(creditLines)) {
			for (CreditLine cline : creditLines) {
				summary.setCreditBalance(summary.getCreditBalance().add(cline.getBalance()));
				summary.setStatus(cline.getStatus().name());
			}
		}

		Map<String, List<NRCredit>> nrCredits = NRCreditHelper.getCreditsByUser(Arrays.asList(userId));
		if (nrCredits != null && CollectionUtils.isNotEmpty(nrCredits.get(userId))) {
			for (NRCredit nrCredit : nrCredits.get(userId)) {
				summary.setCreditBalance(summary.getCreditBalance().add(nrCredit.getBalance()));
				summary.setStatus(nrCredit.getStatus().name());
			}
		}
		summary.setTotalBalance(summary.getCreditBalance().add(summary.getWalletBalance()));
		Map<PointsType, UserPoint> userPointsMap = UserPointsHelper.fetchUserPoints(userId);
		Map<PointsType, BigDecimal> balanceMap = new HashMap<>();
		if (MapUtils.isNotEmpty(userPointsMap))
			userPointsMap.forEach((k, v) -> balanceMap.put(k, v.getBalance()));
		summary.setPointsBalance(balanceMap);
		return summary;
	}

	public UserDuesSummary getUserDueSummary(String userId) {
		UserDuesSummary dueSummary = UserDuesSummary.builder().build();

		Map<String, List<NRCredit>> nrCredits = NRCreditHelper.getCreditsByUser(Arrays.asList(userId));
		if (nrCredits != null && CollectionUtils.isNotEmpty(nrCredits.get(userId))) {
			NRCredit nrCredit = nrCredits.get(userId).get(0);
			Integer dueDays = null;
			if (nrCredit.getAdditionalInfo().getOriginalCreditExpiry() != null) {
				dueDays = (int) ChronoUnit.DAYS.between(nrCredit.getAdditionalInfo().getOriginalCreditExpiry(),
						LocalDateTime.now());
			}
			dueSummary.setLockDate(nrCredit.getCreditExpiry());
			dueSummary.setCurrentDueInfo(DueInfo.builder().amount(nrCredit.getUtilized())
					.dueDate(nrCredit.getCreditExpiry()).dueDays(dueDays).build());
		}

		List<CreditBill> creditBills = CreditBillHelper.getCreditBills(userId);
		Map<Long, BigDecimal> creditOutstanding = new HashMap<>();
		if (CollectionUtils.isNotEmpty(creditBills)) {
			dueSummary.setOldDueInfo(new ArrayList<>());
			for (CreditBill bill : creditBills) {
				creditOutstanding.put(bill.getCreditId(), bill.getPendingAmount()
						.add(creditOutstanding.getOrDefault(bill.getCreditId(), BigDecimal.ZERO)));
				if (!bill.getAdditionalInfo().isThresholdSettled())
					dueSummary.setLockDate(bill.getLockDate());
				dueSummary.getOldDueInfo().add(
						DueInfo.builder().amount(bill.getPendingAmount()).dueDate(bill.getPaymentDueDate()).build());
			}
		}

		List<CreditLine> creditLines = CreditLineHelper.getCreditLines(userId);
		if (CollectionUtils.isNotEmpty(creditLines)) {
			CreditLine creditLine = creditLines.get(0);
			/**
			 * BillCycleEnd is null in case of Dynamic CreditLines
			 */
			LocalDateTime currentCycleDueDate = null;
			LocalDateTime currentCycleLockDate = null;
			if (creditLine.getBillCycleEnd() != null) {
				if (creditLine.getBillCycleType().equals(CreditBillCycleType.CALENDAR)) {
					currentCycleDueDate = creditLine.getAdditionalInfo().getDueDate() != null
							? creditLine.getAdditionalInfo().getDueDate()
							: creditLine.getAdditionalInfo().getLockDate();
					currentCycleLockDate = creditLine.getAdditionalInfo().getLockDate();
				} else {
					currentCycleDueDate =
							creditLine.getBillCycleEnd().plusHours(creditLine.getAdditionalInfo().getPaymentDueHours())
									.plusHours(creditLine.getAdditionalInfo().getLockAfterHours());
					currentCycleLockDate = currentCycleDueDate;
				}
				dueSummary.setLockDate(currentCycleLockDate);
			}
			dueSummary.setCurrentDueInfo(DueInfo.builder()
					.amount(creditLine.getOutstandingBalance()
							.subtract(creditOutstanding.getOrDefault(creditLine.getId(), BigDecimal.ZERO)))
					.dueDate(currentCycleDueDate).build());
		}

		dueSummary.setTotalDues(dueSummary.getTotalDues());
		// This is to send null object in case there is no due for this user.
		if (dueSummary.getTotalDues().compareTo(BigDecimal.valueOf(1)) < 0) {
			return null;
		}
		return dueSummary;
	}

}
