package com.tgs.services.pms.manager;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.PointsConfiguration;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.restmodel.UploadUserPointsRequest;
import com.tgs.services.pms.restmodel.UserPointsUploadRequest;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserPointsBulkUpdateManager {


	@Autowired
	PaymentProcessor paymentProcessor;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	public BulkUploadResponse bulkUpdateUserPoints(UploadUserPointsRequest userPointsUploadRequest) throws Exception {
		if (userPointsUploadRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(userPointsUploadRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable uploadUserPoints = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (UserPointsUploadRequest userPointsRequest : userPointsUploadRequest.getPointsRequest()) {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(userPointsRequest.getRowId());
					try {
						uploadUserPoints(userPointsRequest);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to upload user points with userid {} , type {} , balance {} ",
								userPointsRequest.getUserId(), userPointsRequest.getType(),
								userPointsRequest.getValue(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting creditline for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(uploadUserPoints, userPointsUploadRequest.getUploadType(),
				jobId);
	}

	public void uploadUserPoints(UserPointsUploadRequest userPtsRequest) throws Exception {
		ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
				GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build());
		Duration duration = null;
		if (clientInfo != null) {
			Map<PointsType, PointsConfiguration> clientPointsConfiguration = clientInfo.getPointsConfigurations();
			PointsConfiguration pointConfig = clientPointsConfiguration.get(userPtsRequest.getType());
			if (pointConfig != null) {
				String expiry = pointConfig.getDefaultExpiry();
				duration = StringUtils.isNotEmpty(expiry) ? Duration.parse(expiry) : null;
			}
		}
		BigDecimal amount =
				BigDecimal.valueOf(Math.abs(userPtsRequest.getValue()) * getAmountOfOnePoint(userPtsRequest.getType()));
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder().pointsType(userPtsRequest.getType())
				.pointsExpiry(duration != null
						? LocalDateTime.now().plus(duration.get(ChronoUnit.SECONDS), ChronoUnit.SECONDS)
						: null)
				.build();
		PaymentRequest paymentRequest = PaymentRequest.builder().amount(amount).payUserId(userPtsRequest.getUserId())
				.noOfPoints(Math.abs(userPtsRequest.getValue())).additionalInfo(additionalInfo)
				.paymentMedium(PaymentMedium.POINTS).build();
		if (userPtsRequest.getValue() < 0) {
			paymentRequest.setOpType(PaymentOpType.DEBIT);
			paymentRequest.setTransactionType(PaymentTransactionType.POINTS_RECALL);
		} else {
			paymentRequest.setOpType(PaymentOpType.CREDIT);
			paymentRequest.setTransactionType(PaymentTransactionType.TOPUP);
		}
		paymentProcessor.process(Arrays.asList(paymentRequest));
	}

	public Double getAmountOfOnePoint(PointsType type) {
		ClientGeneralInfo clientInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO,
				GeneralBasicFact.builder().applicableTime(LocalDateTime.now()).build());
		if (clientInfo != null) {
			Map<PointsType, PointsConfiguration> clientPointsConfiguration = clientInfo.getPointsConfigurations();
			PointsConfiguration pointConfig = clientPointsConfiguration.get(type);
			if (pointConfig != null)
				return pointConfig.getAmountOfOnePoint();
			throw new CustomGeneralException("Configuration is missing for points type {}" + type.name());
		}
		throw new CustomGeneralException("Configuration is missing for points type {}" + type.name());
	}

}
