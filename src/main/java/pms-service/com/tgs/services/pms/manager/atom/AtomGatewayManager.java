package com.tgs.services.pms.manager.atom;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.AtomMerchantInfo;
import com.tgs.services.pms.datamodel.pg.AtomPgResponse;
import com.tgs.services.pms.datamodel.pg.AtomRefundInfo;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.NumberUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AtomGatewayManager extends GateWayManager {

	private PaymentProcessingResult pgResponseVerificationResult;

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		AtomMerchantInfo atomGatewayInfo = (AtomMerchantInfo) gatewayInfo;
		setAtomMerchantInfo(atomGatewayInfo, payment);
	}

	protected void setAtomMerchantInfo(AtomMerchantInfo atomGatewayInfo, PaymentRequest payment) {
		atomGatewayInfo.setRefundURL(null);
		atomGatewayInfo.setTxnid(payment.getRefId());
		atomGatewayInfo.setAmt(String.valueOf(payment.getAmount()));
		atomGatewayInfo.setDate(getFormattedDate(LocalDate.now()));
		atomGatewayInfo.setTxnscamt("0");
		atomGatewayInfo.setUdf1(GateWayType.ATOM_PAY.name());
		try {
			atomGatewayInfo.setClientcode(payment.getPayUserId());
			atomGatewayInfo.encodeClientCode();
			atomGatewayInfo.setSignature(EncryptionUtils.getValueEncodedWithSha2(atomGatewayInfo.getReqHashKey(),
					atomGatewayInfo.getPlainSignature()));
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			log.error("Unable to encode signature for refId {} due to {}", payment.getRefId(), e);
		}
	}

	protected String getFormattedDate(LocalDate date) {
		if (date == null) {
			return null;
		}
		return date.format(DateTimeFormatter.ofPattern("dd/MM/yyyy"));
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		pgResponseVerificationResult = new PaymentProcessingResult();

		// String signature = EncryptionUtils.getValueEncodedWithSha2(atomGatewayInfo.getReqHashKey(),
		// atomPgReponse.getPlainSignature());
		//
		// if (!signature.equals(atomPgReponse.getSignature())) {
		// log.error("Atom Signature mismatch for refId {}. Signature received is {}. Signature generated is {}",
		// payment.getRefId(), atomPgReponse.getSignature(), signature);
		// }

		AtomPgResponse atomPgReponse = (AtomPgResponse) pgResponse;
		verifyAmount(payment, atomPgReponse);
		pgResponseVerificationResult.getExternalPaymentInfo().setGatewayComment(atomPgReponse.getDesc());
		pgResponseVerificationResult.setStatus(PaymentStatus.SUCCESS);
		return pgResponseVerificationResult;
	}

	public void verifyAmount(Payment payment, AtomPgResponse atomPgReponse) {
		if (atomPgReponse != null) {
			double pgResponseAmt = org.apache.commons.lang3.math.NumberUtils.isCreatable(atomPgReponse.getAmt())
					? Double.valueOf(atomPgReponse.getAmt())
					: 0.0;
			if (NumberUtils.compareDoubleUpto2Decimal(pgResponseAmt, payment.getAmount().doubleValue()) != 0) {
				log.error(
						"Mismatched Amount for refId {}. Amount received in atomPgReponse is {}. Amount stored in payment is {}",
						payment.getRefId(), atomPgReponse.getAmt(), payment.getAmount());
			}
		}
	}

	/**
	 * @param pgResponse not required here
	 * @param payment
	 */
	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PaymentProcessingResult trackingResult = null;
		try {
			AtomMerchantInfo atomGatewayInfo = (AtomMerchantInfo) gatewayMerchantInfo;

			Map<String, String> queryParams = getTrackPaymentQueryParams(payment, atomGatewayInfo);
			HttpUtils httpUtils =
					HttpUtils.builder().queryParams(queryParams).urlString(atomGatewayInfo.getTrackingUrl()).build();
			Optional<String> response = httpUtils.getResponse(null);
			String url = httpUtils.getUrlString();
			String xml = response.orElse(null);
			log.info("Request URL {}, Response received from atom payment gateway for ref Id {} is {}", url,
					payment.getRefId(), xml);
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(url)
					.key(payment.getRefId()).type("Atom Tracking Request").logType("AirSupplierAPILogs").build()));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(xml)
					.key(payment.getRefId()).type("Atom Tracking Response").logType("AirSupplierAPILogs").build()));

			Document doc = getDocument(xml);
			Node verifiedOutput = null;
			NodeList nodeList = doc.getChildNodes();
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (node.getAttributes().getNamedItem("VERIFIED") != null) {
					verifiedOutput = node;
					break;
				}
			}

			trackingResult = prepareTrackingResult(payment, verifiedOutput);
		} catch (Exception e) {
			log.error("Fetching payment status failed for refId {} due to", payment.getRefId(), e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}

	public Map<String, String> getTrackPaymentQueryParams(Payment payment, AtomMerchantInfo atomGatewayInfo) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("merchantid", atomGatewayInfo.getLogin());
		queryParams.put("merchanttxnid", payment.getRefId());
		queryParams.put("amt", String.valueOf(payment.getAmount()));
		queryParams.put("tdate", getFormattedDateForTracking(LocalDate.now()));
		return queryParams;
	}

	public String getFormattedDateForTracking(LocalDate date) {
		if (date == null) {
			return null;
		}
		return date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	private PaymentProcessingResult prepareTrackingResult(Payment payment, Node verifiedOutput) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		String verified = verifiedOutput.getAttributes().getNamedItem("VERIFIED").getNodeValue();
		String amtVal = verifiedOutput.getAttributes().getNamedItem("AMT").getNodeValue();
		String refId = verifiedOutput.getAttributes().getNamedItem("MerchantTxnID").getNodeValue();
		Double amt = org.apache.commons.lang3.math.NumberUtils.isCreatable(amtVal) ? Double.valueOf(amtVal) : 0.0;

		if (pgResponseVerificationResult != null
				&& StringUtils.isNotBlank(pgResponseVerificationResult.getExternalPaymentInfo().getGatewayComment())) {
			trackingResult.getExternalPaymentInfo()
					.setGatewayComment(pgResponseVerificationResult.getExternalPaymentInfo().getGatewayComment());
		}
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(verified);
		if ("SUCCESS".equals(verified)
				&& NumberUtils.compareDoubleUpto2Decimal(amt, payment.getAmount().doubleValue()) == 0
				&& payment.getRefId().equals(refId)) {
			trackingResult.setStatus(PaymentStatus.SUCCESS);
		} else if (isPending(verified)) {
			trackingResult.setStatus(PaymentStatus.PENDING);
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
			if ("SUCCESS".equals(verified)) {
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
		}

		String atomTxnId = verifiedOutput.getAttributes().getNamedItem("atomtxnId").getNodeValue();
		if (!("null".equals(atomTxnId) || "0".equals(atomTxnId))) {
			trackingResult.getExternalPaymentInfo().setMerchantTxnId(atomTxnId);
		}

		String surchargeVal = verifiedOutput.getAttributes().getNamedItem("surcharge").getNodeValue();
		Double surcharge =
				org.apache.commons.lang3.math.NumberUtils.isCreatable(surchargeVal) ? Double.valueOf(surchargeVal)
						: null;
		if (surcharge != null) {
			BigDecimal atomPaymentFee = BigDecimal.valueOf(surcharge);
			trackingResult.getExternalPaymentInfo().setGatewayPaymentFee(atomPaymentFee);
			trackingResult.getExternalPaymentInfo().setTotalAmount(payment.getAmount().add(atomPaymentFee));
		}

		return trackingResult;
	}

	private boolean isPending(String verifiedField) {
		// TODO
		return false;
	}

	@Override
	protected AtomRefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		AtomRefundInfo atomRefundInfo = new AtomRefundInfo();
		AtomMerchantInfo atomMerchantInfo = (AtomMerchantInfo) gatewayInfo;

		try {
			atomRefundInfo.setPwd(atomMerchantInfo.getEncodedPassword());
		} catch (UnsupportedEncodingException e) {
			log.error("Unable to encode password for refId {} due to {}", payment.getRefId(), e);
		}
		atomRefundInfo.setMerchantid(atomMerchantInfo.getLogin());
		atomRefundInfo.setRefundamt(String.valueOf(payment.getAmount()));
		atomRefundInfo.setMerefundref(payment.getPaymentRefId());
		atomRefundInfo.setTxndate(getFormattedDateForTracking(payment.getAdditionalInfo().getPaymentDate()));
		atomRefundInfo.setAtomtxnid(payment.getMerchantTxnId());
		return atomRefundInfo;
	}


	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		boolean isRefundSuccessful = false;

		AtomMerchantInfo atomMerchantInfo = (AtomMerchantInfo) gatewayInfo;
		AtomRefundInfo atomRefundInfo = (AtomRefundInfo) refundInfo;

		Map<String, String> fields = super.convertToMap(atomRefundInfo);
		HttpUtils httpUtils = HttpUtils.builder().requestMethod(HttpUtils.REQ_METHOD_POST).queryParams(fields)
				.urlString(atomMerchantInfo.getRefundURL()).build();
		Optional<String> response = httpUtils.getResponse(null);
		String url = httpUtils.getUrlString();
		String xml = response.orElse(null);
		log.info("Request URL {}, Response received from atom payment gateway for ref Id {} is {}", url,
				payment.getRefId(), xml);

		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(url)
				.key(payment.getRefId()).type("Atom Refund Request").logType("AirSupplierAPILogs").build()));

		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(xml)
				.key(payment.getRefId()).type("Atom Refund Response").logType("AirSupplierAPILogs").build()));

		Document doc = getDocument(xml);

		String statusCode = StringUtils.trim(getTextValue(doc, "STATUSCODE"));
		if ("00".equals(statusCode) || "01".equals(statusCode)) {
			isRefundSuccessful = true;
		}

		String atomRefundId = null;
		if (isRefundSuccessful) {
			atomRefundId = StringUtils.trim(getTextValue(doc, "ATOMREFUNDID"));
			payment.setMerchantTxnId(atomRefundId);
		}

		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(atomRefundId).build();
	}

	/**
	 * Return value in Text Node of Tag Node.
	 */
	private String getTextValue(Document doc, String tagName) {
		NodeList nodeList = doc.getElementsByTagName(tagName);
		if (nodeList.getLength() > 0) {
			String value = null;
			Node statusCodeNode = nodeList.item(0);
			NodeList statusCodeTextNodes = statusCodeNode.getChildNodes();
			if (statusCodeTextNodes.getLength() > 0) {
				Node textNode = statusCodeTextNodes.item(0);
				value = textNode.getNodeValue();
				return value;
			}
		}
		return null;
	}

	private Document getDocument(String xml) throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		InputSource inputSource = new InputSource(new StringReader(xml));
		return db.parse(inputSource);
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		return super.convertToMap(gatewayInfo);
	}

	@Override
	protected String getGatewayName() {
		return "Atom Gateway";
	}
}
