package com.tgs.services.pms.manager.atomAES;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.AtomMerchantInfo;
import com.tgs.services.pms.datamodel.pg.AtomPgResponse;
import com.tgs.services.pms.datamodel.pg.AtomRefundInfo;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.manager.atom.AtomGatewayManager;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.NumberUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AtomAESGatewayManager extends AtomGatewayManager {

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		PaymentProcessingResult pgResponseVerificationResult = new PaymentProcessingResult();
		AtomMerchantInfo atomGatewayInfo = (AtomMerchantInfo) gatewayMerchantInfo;
		AtomPgResponse atomPgReponse = (AtomPgResponse) pgResponse;
		String refId = atomPgReponse.getRefId();
		try {
			String decrptData = getdecodedData(atomPgReponse.getEncdata(), atomGatewayInfo);
			if (decrptData != null) {
				Map<String, String> data = getDecodedData(decrptData);
				atomPgReponse = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(data), AtomPgResponse.class);
				if (atomPgReponse != null) {
					if (refId.equals(atomPgReponse.getRefId())) {
						payment.setMerchantTxnId(atomPgReponse.getAtomTxnId());
						super.verifyAmount(payment, atomPgReponse);
						pgResponseVerificationResult.getExternalPaymentInfo()
								.setGatewayComment(atomPgReponse.getDesc());
						pgResponseVerificationResult.setStatus(PaymentStatus.SUCCESS);
						if (!verifySignature(atomPgReponse, atomGatewayInfo)) {
							pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
						}
					} else {
						log.error(
								"Mismatched refId. RefId received in atomPgReponse is {}. RefId stored in payment is {}",
								refId, atomPgReponse.getRefId());
						pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
					}
				}
			} else {
				pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
			}
		} catch (Exception e) {
			log.error("Unable to verify response for refId {} due to {}", payment.getRefId(), e);
			pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
		}
		return pgResponseVerificationResult;
	}

	private Map<String, String> getDecodedData(String decrptData) throws IOException {
		String propertiesFormat = decrptData.replaceAll("&", "\n");
		Properties properties = new Properties();
		properties.load(new StringReader(propertiesFormat));
		Map<String, String> data = new HashMap(properties);
		return data;
	}

	private boolean verifySignature(AtomPgResponse atomPgReponse, AtomMerchantInfo atomGatewayInfo) {
		String signature;
		try {
			signature = EncryptionUtils.getValueEncodedWithSha2(atomGatewayInfo.getRespHashKey(),
					atomPgReponse.getPlainSignature());
			if (!signature.equals(atomPgReponse.getSignature())) {
				log.error("Atom Signature mismatch for refId {}. Signature received is {}. Signature generated is {}",
						atomPgReponse.getRefId(), atomPgReponse.getSignature(), signature);
				return false;
			}
			return true;
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			log.error("error occured while verifying signature for refId {}", atomPgReponse.getRefId());
			return false;
		}
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PaymentProcessingResult trackingResult = null;
		try {
			AtomMerchantInfo atomGatewayInfo = (AtomMerchantInfo) gatewayMerchantInfo;

			Map<String, String> queryParams = super.getTrackPaymentQueryParams(payment, atomGatewayInfo);
			String params = getRequestString(queryParams);
			Map<String, String> queryParam = new HashMap<>();
			queryParam.put("encdata", getEncodeParams(params, atomGatewayInfo));
			queryParam.put("login", atomGatewayInfo.getLogin());
			HttpUtils httpUtils = HttpUtils.builder().queryParams(queryParam)
					.urlString(atomGatewayInfo.getTrackingUrl()).requestMethod(HttpUtils.REQ_METHOD_POST).build();
			String response = httpUtils.getResponse(String.class).orElse(new String());
			String decrptData = getdecodedData(response, atomGatewayInfo);
			List<Map<String, String>> data = GsonUtils.getGson().fromJson(decrptData, List.class);
			log.info("Request URL {}, Response received from atom payment gateway for ref Id {} is {}",
					httpUtils.getUrlString(), payment.getRefId(), decrptData);
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(httpUtils.getUrlString()).key(payment.getRefId()).type("Atom AES Tracking Request")
					.logType("AirSupplierAPILogs").build()));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(decrptData)
					.key(payment.getRefId()).type("Atom AES Tracking Response").logType("AirSupplierAPILogs").build()));
			trackingResult = prepareTrackingResult(payment, data.get(0));
		} catch (Exception e) {
			log.error("Fetching payment status failed for refId {} due to", payment.getRefId(), e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}

	protected PaymentProcessingResult prepareTrackingResult(Payment payment, Map<String, String> data) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(data.get("statusCode"));
		if ("SUCCESS".equals(data.get("verified"))
				&& NumberUtils.compareDoubleUpto2Decimal(Double.valueOf(data.get("amt")),
						payment.getAmount().doubleValue()) == 0
				&& payment.getRefId().equals(data.get("merchantTxnID"))) {
			trackingResult.setStatus(PaymentStatus.SUCCESS);
		} else if ("Initiated".equals(data.get("verified")) || "Auto Reversal".equals(data.get("verified"))
				|| "Force Success".equals(data.get("verified"))) {
			trackingResult.setStatus(PaymentStatus.PENDING);
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
			if ("SUCCESS".equals(data.get("verified"))) {
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
		}

		String atomTxnId = data.get("atomTxnId");
		if (!("null".equals(atomTxnId) || "0".equals(atomTxnId))) {
			trackingResult.getExternalPaymentInfo().setMerchantTxnId(atomTxnId);
		}

		String surchargeVal = data.get("surcharge");
		Double surcharge =
				org.apache.commons.lang3.math.NumberUtils.isCreatable(surchargeVal) ? Double.valueOf(surchargeVal)
						: null;
		if (surcharge != null) {
			BigDecimal atomPaymentFee = BigDecimal.valueOf(surcharge);
			trackingResult.getExternalPaymentInfo().setGatewayPaymentFee(atomPaymentFee);
			trackingResult.getExternalPaymentInfo().setTotalAmount(payment.getAmount().add(atomPaymentFee));
		}
		return trackingResult;
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		boolean isRefundSuccessful = false;
		String atomRefundId = null;
		AtomMerchantInfo atomMerchantInfo = (AtomMerchantInfo) gatewayInfo;
		AtomRefundInfo atomRefundInfo = (AtomRefundInfo) refundInfo;
		atomRefundInfo.setProdid(atomMerchantInfo.getProdid());
		Map<String, String> queryParam = new HashMap<>();
		String data = GsonUtils.getGson().toJson(atomRefundInfo);
		queryParam.put("encdata", getEncodeParams(data, atomMerchantInfo));
		queryParam.put("login", atomMerchantInfo.getLogin());
		HttpUtils httpUtils = HttpUtils.builder().queryParams(queryParam).urlString(atomMerchantInfo.getRefundURL())
				.requestMethod(HttpUtils.REQ_METHOD_POST).build();
		String response = httpUtils.getResponse(String.class).orElse(new String());
		String url = httpUtils.getUrlString();
		String decrptData = getdecodedData(response, atomMerchantInfo);
		Map<String, String> responseMap = GsonUtils.getGson().fromJson(decrptData, Map.class);
		log.info("Request URL {}, Response received from atom payment gateway for ref Id {} is {}", url,
				payment.getRefId(), decrptData);

		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(url)
				.key(payment.getRefId()).type("Atom AES Refund Request").logType("AirSupplierAPILogs").build()));

		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(decrptData)
				.key(payment.getRefId()).type("Atom AES Refund Response").logType("AirSupplierAPILogs").build()));

		if ("00".equals(responseMap.get("statusCode")) || "01".equals(responseMap.get("statusCode"))) {
			isRefundSuccessful = true;
		}
		if (isRefundSuccessful) {
			atomRefundId = responseMap.get("atomrefundid");
			payment.setMerchantTxnId(atomRefundId);
		}
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(atomRefundId).build();
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		AtomMerchantInfo atomGatewayInfo = (AtomMerchantInfo) gatewayInfo;
		AtomMerchantInfo atomAESGatewayInfo = new AtomMerchantInfo();
		atomAESGatewayInfo.setEncdata(atomGatewayInfo.getEncdata());
		atomAESGatewayInfo.setLogin(atomGatewayInfo.getLogin());
		atomAESGatewayInfo.setGatewayType(atomGatewayInfo.getGatewayType());
		atomAESGatewayInfo.setGatewayURL(atomGatewayInfo.getGatewayURL());
		return super.convertToMap(atomAESGatewayInfo);
	}

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		AtomMerchantInfo atomGatewayInfo = (AtomMerchantInfo) gatewayInfo;
		super.setAtomMerchantInfo(atomGatewayInfo, payment);
		atomGatewayInfo.setUdf1(GateWayType.ATOM_AES.name());
		atomGatewayInfo.appendRefIdToCallBack(payment.getRefId());
		try {
			Map<String, String> queryParams = new Gson().fromJson(GsonUtils.getGson().toJson(atomGatewayInfo),
					new TypeToken<HashMap<String, String>>() {}.getType());
			String params = getRequestString(queryParams);
			atomGatewayInfo.setEncdata(getEncodeParams(params, atomGatewayInfo));
		} catch (Exception e) {
			log.error("Unable to encode signature for refId {} due to {}", payment.getRefId(), e);
		}
	}

	public String getRequestString(Map<String, String> queryParams) {
		Set<String> keys = queryParams.keySet();
		StringBuilder query = new StringBuilder();
		for (String key : keys) {
			if (excludeFields(queryParams, key))
				continue;
			if (query.length() > 0)
				query.append("&");
			query.append(key).append("=").append(queryParams.get(key));
		}
		return query.toString();
	}

	private boolean excludeFields(Map<String, String> queryParams, String key) {
		return queryParams.get(key) == null || key.equals("gatewayURL") || key.equals("reqHashKey")
				|| key.equals("trackingUrl") || key.equals("isClientCodeEncoded") || key.equals("reqEncrypKey")
				|| key.equals("requestType") || key.equals("reqIVKey") || key.equals("gatewayType")
				|| key.equals("respHashKey") || key.equals("respIVKey") || key.equals("respDecryptKey");
	}

	public String getEncodeParams(String params, AtomMerchantInfo atomGatewayInfo) throws Exception {
		return EncryptionUtils.encryptUsingAES(params, atomGatewayInfo.getReqEncrypKey(),
				atomGatewayInfo.getReqIVKey());
	}

	public String getdecodedData(String encrptedString, AtomMerchantInfo atomGatewayInfo) throws Exception {
		return EncryptionUtils.decryptUsingAES(encrptedString, atomGatewayInfo.getRespDecryptKey(),
				atomGatewayInfo.getRespIVKey());
	}

	@Override
	protected String getGatewayName() {
		return "atom_aes";
	}


}
