package com.tgs.services.pms.manager.credimax;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxMerchantInfo;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxPgResponse;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxTransactionDetails;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CredimaxGateWayManager extends GateWayManager {

	private CredimaxPgResponse credimaxPgResponse;

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		return super.convertToMap(gatewayInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		PaymentProcessingResult verificationResult = new PaymentProcessingResult();
		credimaxPgResponse = (CredimaxPgResponse) pgResponse;
		if (payment.getRefId().equals(credimaxPgResponse.getRefId())) {
			verificationResult.setStatus(PaymentStatus.SUCCESS);
		} else {
			verificationResult.setStatus(PaymentStatus.FAILURE);
		}
		return verificationResult;
	}

	private String getTransactionIds(JsonArray transaction) {
		if (transaction != null && getTransactionObj(transaction) != null) {
			Map<String, Object> trans = (Map<String, Object>) getTransactionObj(transaction).get("transaction");
			return ((Map<String, Object>) trans.get("acquirer")).get("transactionId").toString();
		}
		return null;
	}

	private Map<String, Object> getTransactionObj(JsonArray transaction) {
		List<Map<String, Object>> list = GsonUtils.getGson().fromJson(transaction, ArrayList.class);
		if (CollectionUtils.isNotEmpty(list)) {
			for (Map<String, Object> trans : list) {
				if (trans.get("result").equals("SUCCESS")) {
					return trans;
				}
			}
		}
		return null;
	}

	private CredimaxTransactionDetails getTransactionDetails(JsonArray transaction) {
		if (transaction != null && getTransactionObj(transaction) != null) {
			JsonObject trans = (JsonObject) GsonUtils.getGson()
					.fromJson(GsonUtils.getGson().toJson(getTransactionObj(transaction)), JsonObject.class)
					.get("order");
			CredimaxTransactionDetails details = CredimaxTransactionDetails.builder()
					.amount(trans.get("amount").getAsString()).currency(trans.get("currency").getAsString())
					.status(trans.get("status").getAsString()).build();
			return details;
		}
		return null;
	}

	private JsonObject getOrderInfo(String orderId, CredimaxMerchantInfo merchantInfo) throws IOException {
		JsonObject responseObj = new JsonObject();
		Map<String, String> headerParams = getHeaderParams(merchantInfo);
		String baseUrl = merchantInfo.getBaseUrl().concat("/" + merchantInfo.getMerchantId() + "/order/" + orderId);
		HttpUtils httpUtils =
				HttpUtils.builder().urlString(baseUrl).headerParams(getHeaderParams(merchantInfo)).build();
		try {
			responseObj = httpUtils.getResponse(JsonObject.class).orElse(new JsonObject());
		} catch (IOException e) {
			throw e;
		}
		return responseObj;
	}

	private Map<String, String> getHeaderParams(CredimaxMerchantInfo merchantInfo) {
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Content-Type", "application/json");
		byte[] bytes =
				(merchantInfo.getApiKey() + ":" + merchantInfo.getApiPassword()).getBytes(Charset.forName("UTF-8"));
		String encoding = Base64.getEncoder().encodeToString(bytes);
		headerParams.put("Authorization", "Basic " + encoding);
		return headerParams;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		try {
			CredimaxMerchantInfo merchantInfo = (CredimaxMerchantInfo) gatewayMerchantInfo;
			JsonObject orderDetails = getOrderInfo(credimaxPgResponse.getRefId(), merchantInfo);
			JsonArray transaction =
					orderDetails.get("transaction") != null ? orderDetails.get("transaction").getAsJsonArray() : null;
			String transactionId = getTransactionIds(transaction);
			credimaxPgResponse
					.setError(orderDetails.get("error") != null ? orderDetails.get("error").getAsJsonObject() : null);
			credimaxPgResponse
					.setResult(orderDetails.get("result") != null ? orderDetails.get("result").getAsString() : null);
			credimaxPgResponse
					.setStatus(orderDetails.get("status") != null ? orderDetails.get("status").getAsString() : null);
			credimaxPgResponse.setTransactionId(transactionId);
			payment.setMerchantTxnId(transactionId);
			payment.setCurrency(merchantInfo.getCurrency());
			capturePayment(payment, merchantInfo);
			CredimaxTransactionDetails transactionDetails = getTransactionDetails(transaction);
			return prepareTrackingResult(transactionDetails, payment);
		} catch (Exception e) {
			log.error("Unable to verify payment for refId {} due to ", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "CredimaxVerification", e);
		}
		return null;
	}

	private void capturePayment(Payment payment, CredimaxMerchantInfo merchantInfo) {
		if (shouldCapturePayment(merchantInfo)) {
			Map<String, Object> putData = getPutData(payment, merchantInfo);
			String baseUrl = merchantInfo.getBaseUrl().concat("/" + merchantInfo.getMerchantId() + "/order/"
					+ credimaxPgResponse.getRefId() + "/transaction/" + credimaxPgResponse.getTransactionId());
			HttpUtils httpUtils = HttpUtils.builder().urlString(baseUrl).headerParams(getHeaderParams(merchantInfo))
					.requestMethod(HttpUtils.REQ_METHOD_PUT).postData(GsonUtils.getGson().toJson(putData)).build();
			try {
				JsonObject responseObj = httpUtils.getResponse(JsonObject.class).orElse(new JsonObject());
				log.info("Capture payment response for Credimax payment with refId {} is {}", payment.getRefId(),
						responseObj.toString());
				if ("SUCCESS".equals(responseObj.get("result").getAsString())) {
					JsonObject responseData = (JsonObject) responseObj.get("response");
					String gatewayCode = responseData.get("gatewayCode").getAsString();
					if ("APPROVED".equals(gatewayCode) || "APPROVED_AUTO".equals(gatewayCode))
						credimaxPgResponse.setStatus("CAPTURED");
				} else if ("error".equals(responseObj.get("result").getAsString())) {
					credimaxPgResponse.setError(responseObj.get("error").getAsJsonObject());
				}
				credimaxPgResponse.setResult(responseObj.get("result").getAsString());
			} catch (IOException e) {
				log.error("Unable to capture payment for refId {} due to ", payment.getRefId(), e);
				LogUtils.log(payment.getRefId(), "CredimaxCaptureResponse", e);
			}
		}
	}

	private boolean shouldCapturePayment(CredimaxMerchantInfo merchantInfo) {
		return merchantInfo.getShouldCapture() && credimaxPgResponse.getResult().equals("SUCCESS")
				&& credimaxPgResponse.getStatus().equals("AUTHORIZED");
	}

	private Map<String, Object> getPutData(Payment payment, CredimaxMerchantInfo merchantInfo) {
		Map<String, Object> params = new HashMap<>();
		Map<String, String> transParams = new HashMap<>();
		params.put("apiOperation", "CAPTURE");
		transParams.put("amount", String.valueOf(payment.getAdditionalInfo().getExchangedAmount()));
		transParams.put("currency", payment.getAdditionalInfo().getExchangedCurrency());
		params.put("transaction", transParams);
		return params;
	}

	private PaymentProcessingResult prepareTrackingResult(CredimaxTransactionDetails transactionDetails,
			Payment payment) {
		PaymentProcessingResult verificationResult = new PaymentProcessingResult();
		if (credimaxPgResponse.getError() != null) {
			verificationResult.getExternalPaymentInfo()
					.setGatewayComment(credimaxPgResponse.getError().get("explanation").getAsString());
		}
		verificationResult.getExternalPaymentInfo().setMerchantTxnId(credimaxPgResponse.getTransactionId());
		if (credimaxPgResponse.getResult().equals("SUCCESS") && credimaxPgResponse.getStatus().equals("CAPTURED")
				&& transactionDetails != null) {
			verificationResult.setStatus(PaymentStatus.SUCCESS);
			verificationResult.getExternalPaymentInfo().setGatewayStatusCode(transactionDetails.getStatus());
		} else if (credimaxPgResponse.getStatus().equals("AUTHORIZED")) {
			verificationResult.setStatus(PaymentStatus.PENDING);
		} else {
			verificationResult.setStatus(PaymentStatus.FAILURE);
		}
		return verificationResult;
	}

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		CredimaxMerchantInfo credimaxmerchantInfo = (CredimaxMerchantInfo) gatewayInfo;
		credimaxmerchantInfo.setOrder_id(payment.getRefId());
		ClientGeneralInfo clientInfo = super.getClientInfo();
		BigDecimal exchangedAmount = super.getAmountBasedOnCurrency(payment.getAmount(), clientInfo.getCurrencyCode(),
				credimaxmerchantInfo.getCurrency());
		payment.getAdditionalInfo().setExchangedAmount(exchangedAmount);
		payment.getAdditionalInfo().setExchangedCurrency(credimaxmerchantInfo.getCurrency());
		credimaxmerchantInfo.setAmount(exchangedAmount.toString());
		credimaxmerchantInfo.appendRefIdToCallBack(payment.getRefId());
		String credimaxSessionID = createSessionId(credimaxmerchantInfo, payment);
		credimaxmerchantInfo.setSession_id(credimaxSessionID);
	}

	private String createSessionId(CredimaxMerchantInfo credimaxmerchantInfo, PaymentRequest payment) {
		Map<String, Object> postData = getPostData(credimaxmerchantInfo);
		String baseUrl =
				credimaxmerchantInfo.getBaseUrl().concat("/" + credimaxmerchantInfo.getMerchantId() + "/session");
		HttpUtils httpUtils = HttpUtils.builder().urlString(baseUrl).headerParams(getHeaderParams(credimaxmerchantInfo))
				.postData(GsonUtils.getGson().toJson(postData)).build();
		try {
			Map<String, Object> responseObj = httpUtils.getResponse(Map.class).orElse(new HashMap<>());
			if ("SUCCESS".equals(responseObj.get("result"))) {
				String responseData = GsonUtils.getGson().toJson(responseObj.get("session"));
				Map<String, String> responseMap =
						GsonUtils.getGson().fromJson(responseData, new TypeToken<Map<String, String>>() {}.getType());
				return responseMap.get("id");
			} else {
				throw new CustomGeneralException(SystemError.FAILED_TO_INITIALIZE_PAYMENT);
			}
		} catch (IOException e) {
			log.info("Failed to initialize payment for credimax {}", e);
			throw new CustomGeneralException(SystemError.FAILED_TO_INITIALIZE_PAYMENT);
		}
	}

	private Map<String, Object> getPostData(CredimaxMerchantInfo credimaxmerchantInfo) {
		Map<String, Object> postData = new HashMap<>();
		Map<String, Object> orderParams = new HashMap<>();
		Map<String, Object> interactionParams = new HashMap<>();
		Map<String, String> displayControl = new HashMap<>();
		orderParams.put("id", credimaxmerchantInfo.getOrder_id());
		orderParams.put("amount", credimaxmerchantInfo.getAmount());
		orderParams.put("currency", credimaxmerchantInfo.getCurrency());
		interactionParams.put("operation", credimaxmerchantInfo.getInteractionOperation());
		interactionParams.put("returnUrl", credimaxmerchantInfo.getCallback_url());
		interactionParams.put("cancelUrl", credimaxmerchantInfo.getCallback_url());
		displayControl.put("billingAddress", "HIDE");
		interactionParams.put("displayControl", displayControl);
		postData.put("apiOperation", credimaxmerchantInfo.getApiOperation());
		postData.put("order", orderParams);
		postData.put("interaction", interactionParams);
		return postData;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		return null;
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		boolean isRefundSuccessful = true;
		String merchantRefundId = payment.getMerchantTxnId();
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(merchantRefundId).build();
	}

	@Override
	protected String getGatewayName() {
		return "Credimax";
	}

}
