package com.tgs.services.pms.manager.dbs;

import org.bouncycastle.bcpg.HashAlgorithmTags;
import org.bouncycastle.bcpg.SymmetricKeyAlgorithmTags;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;

public class AlgorithmUtils {

	public static String getHashAlgorithm(int alg) {
		switch (alg) {
		case HashAlgorithmTags.MD5:
			return "MD5";
		case HashAlgorithmTags.SHA1:
			return "SHA1";
		case HashAlgorithmTags.RIPEMD160:
			return "RIPEMD160";
		case HashAlgorithmTags.DOUBLE_SHA:
			return "DOUBLE_SHA";
		case HashAlgorithmTags.MD2:
			return "MD2";
		case HashAlgorithmTags.TIGER_192:
			return "TIGER_192";
		case HashAlgorithmTags.HAVAL_5_160:
			return "HAVAL_5_160";
		case HashAlgorithmTags.SHA256:
			return "SHA256";
		case HashAlgorithmTags.SHA384:
			return "SHA384";
		case HashAlgorithmTags.SHA512:
			return "SHA512";
		case HashAlgorithmTags.SHA224:
			return "SHA224";
		default:
			return "Unknown Hash Algorithm";
		}
	}

	public static String getSymmetricKeyAlgorithm(int alg) {
		switch (alg) {
		case SymmetricKeyAlgorithmTags.IDEA:
			return "IDEA";
		case SymmetricKeyAlgorithmTags.TRIPLE_DES:
			return "TRIPLE_DES";
		case SymmetricKeyAlgorithmTags.CAST5:
			return "CAST5";
		case SymmetricKeyAlgorithmTags.BLOWFISH:
			return "BLOWFISH";
		case SymmetricKeyAlgorithmTags.SAFER:
			return "SAFER";
		case SymmetricKeyAlgorithmTags.DES:
			return "DES";
		case SymmetricKeyAlgorithmTags.AES_128:
			return "AES_128";
		case SymmetricKeyAlgorithmTags.AES_192:
			return "AES_192";
		case SymmetricKeyAlgorithmTags.AES_256:
			return "AES_256";
		case SymmetricKeyAlgorithmTags.TWOFISH:
			return "TWOFISH";
		case SymmetricKeyAlgorithmTags.CAMELLIA_128:
			return "CAMELLIA_128";
		case SymmetricKeyAlgorithmTags.CAMELLIA_192:
			return "CAMELLIA_192";
		case SymmetricKeyAlgorithmTags.CAMELLIA_256:
			return "CAMELLIA_256";
		default:
			return "Unknown Symmetric Key Algorithm";
		}
	}

	public static String getCompressionAlgorithm(int alg) {
		switch (alg) {
		case PGPCompressedDataGenerator.BZIP2:
			return "BZIP2";
		case PGPCompressedDataGenerator.UNCOMPRESSED:
			return "UNCOMPRESSED";
		case PGPCompressedDataGenerator.ZIP:
			return "ZIP";
		case PGPCompressedDataGenerator.ZLIB:
			return "ZLIB";
		default:
			return "Unknown Compression Algorithm";
		}
	}

}