package com.tgs.services.pms.manager.dbs;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.security.Provider;
import java.security.Security;
import java.util.Iterator;

import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignature;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPSignature;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.operator.bc.BcKeyFingerprintCalculator;
import org.bouncycastle.openpgp.operator.bc.BcPGPContentVerifierBuilderProvider;
import org.bouncycastle.openpgp.operator.bc.BcPublicKeyDataDecryptorFactory;
import org.bouncycastle.util.io.Streams;
import org.springframework.stereotype.Service;

@Service
public class BCPGPDecryptor {

//	private int compressionAlgorithm;
//	private int hashAlgorithm;
//	private int symmetricKeyAlgorithm;

	private static void addProviderIfAbsent() {
		Provider provider = Security.getProvider(BouncyCastleProvider.PROVIDER_NAME);
		if (provider == null) {
			provider = new BouncyCastleProvider();
			Security.addProvider(provider);
		}
	}

	public String decryptandVerify(String encryptedMessage, String privateKey, String publicKeyStr, String passPhrase)
			throws IOException, PGPException {
		addProviderIfAbsent();
		InputStream input = null;
		InputStream verifyKeyInput = null;
		InputStream decryptKeyInput = null;
		char[] passwd = null;
		try {
			passwd = passPhrase.toCharArray();
			verifyKeyInput = new ByteArrayInputStream(privateKey.getBytes(Charset.forName("UTF-8")));
			decryptKeyInput = new ByteArrayInputStream(publicKeyStr.getBytes(Charset.forName("UTF-8")));
			input = PGPUtil
					.getDecoderStream(new ByteArrayInputStream(encryptedMessage.getBytes(Charset.forName("UTF-8"))));

			PGPObjectFactory pgpF = new PGPObjectFactory(input, new BcKeyFingerprintCalculator());
			PGPEncryptedDataList enc;

			Object o = pgpF.nextObject();

			if (o instanceof PGPEncryptedDataList) {
				enc = (PGPEncryptedDataList) o;
			} else {
				enc = (PGPEncryptedDataList) pgpF.nextObject();
			}

			Iterator<?> it = enc.getEncryptedDataObjects();
			PGPPrivateKey sKey = null;
			PGPPublicKeyEncryptedData pbe = null;
			PGPSecretKeyRingCollection pgpSec = new PGPSecretKeyRingCollection(PGPUtil.getDecoderStream(verifyKeyInput),
					new BcKeyFingerprintCalculator());

			while (sKey == null && it.hasNext()) {
				pbe = (PGPPublicKeyEncryptedData) it.next();
				sKey = BCPGPUtils.findSecretKey(pgpSec, pbe.getKeyID(), passwd);
			}

			if (sKey == null) {
				throw new IllegalArgumentException("secret key for message not found.");
			}

			InputStream clear = pbe.getDataStream(new BcPublicKeyDataDecryptorFactory(sKey));
//			this.symmetricKeyAlgorithm = 
					pbe.getSymmetricAlgorithm(new BcPublicKeyDataDecryptorFactory(sKey));

			PGPObjectFactory plainFact = new PGPObjectFactory(clear, new BcKeyFingerprintCalculator());

			Object message = null;

			PGPOnePassSignatureList onePassSignatureList = null;
			PGPSignatureList signatureList = null;
			PGPCompressedData compressedData = null;

			message = plainFact.nextObject();
			ByteArrayOutputStream actualOutput = new ByteArrayOutputStream();

			while (message != null) {
				if (message instanceof PGPCompressedData) {
					compressedData = (PGPCompressedData) message;
					plainFact = new PGPObjectFactory(compressedData.getDataStream(), new BcKeyFingerprintCalculator());
					message = plainFact.nextObject();
//					this.compressionAlgorithm = compressedData.getAlgorithm();
				}

				if (message instanceof PGPLiteralData) {
					Streams.pipeAll(((PGPLiteralData) message).getInputStream(), actualOutput);
				} else if (message instanceof PGPOnePassSignatureList) {
					onePassSignatureList = (PGPOnePassSignatureList) message;
				} else if (message instanceof PGPSignatureList) {
					signatureList = (PGPSignatureList) message;
				}
				message = plainFact.nextObject();
			}
			actualOutput.close();
			PGPPublicKey publicKey = null;
			byte[] outputBytes = actualOutput.toByteArray();
			if (onePassSignatureList == null || signatureList == null) {
				throw new PGPException("Poor PGP. Signatures not found.");
			} else {
				for (int i = 0; i < onePassSignatureList.size(); i++) {
					PGPOnePassSignature ops = onePassSignatureList.get(0);
					PGPPublicKeyRingCollection pgpRing = new PGPPublicKeyRingCollection(
							PGPUtil.getDecoderStream(decryptKeyInput), new BcKeyFingerprintCalculator());
					publicKey = pgpRing.getPublicKey(ops.getKeyID());
					if (publicKey != null) {
						ops.init(new BcPGPContentVerifierBuilderProvider(), publicKey);
						ops.update(outputBytes);
						PGPSignature signature = signatureList.get(i);
						if (ops.verify(signature)) {
//							this.hashAlgorithm = ops != null ? ops.getHashAlgorithm() : 0;
							publicKey.getUserIDs();
						}
					}
				}

			}

			if (pbe.isIntegrityProtected()) {
				pbe.verify();
			}

			return actualOutput.toString();
		} finally {
			try {
				input.close();
			} catch (Exception e) {
			}
			try {
				verifyKeyInput.close();
			} catch (Exception e) {
			}
			try {
				decryptKeyInput.close();
			} catch (Exception e) {
			}
		}
	}
}
