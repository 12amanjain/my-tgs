package com.tgs.services.pms.manager.easebuzz;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.URLEncoder;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzMerchantInfo;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzPgResponse;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzRefundInfo;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzRefundResponse;
import com.tgs.services.pms.datamodel.pg.easebuzz.EasebuzzVerificationResponse;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EasebuzzGatewayManager extends GateWayManager {

	@Autowired
	private UserServiceCommunicator usComm;

	@Autowired
	private PaymentService pmsService;

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		try {
			User payUser = usComm.getUserFromCache(payment.getPayUserId());
			Product product = payment.getProduct();
			if (product == null) {
				product = Product.NA;
			}
			EasebuzzMerchantInfo easebuzzMerchantInfo = (EasebuzzMerchantInfo) gatewayInfo;
			String key = easebuzzMerchantInfo.getKey();
			String txnid = payment.getRefId();
			String amount = payment.getAmount().toString();
			// underscore not supported by Easebuzz API
			String productInfo = product.name().replace('_', ' ');
			String firstname = payUser.getName();
			String email = payUser.getEmail();
			String salt = easebuzzMerchantInfo.getSalt();
			String plainSignature = Joiner.on('|').useForNull("").join(key, txnid, amount, productInfo, firstname,
					email, "|||||||||", salt);

			StringBuilder sb = new StringBuilder();
			sb.append("amount=").append(urlEncode(amount));
			sb.append("&firstname=").append(urlEncode(firstname));
			sb.append("&phone=").append(urlEncode(payUser.getMobile()));
			sb.append("&email=").append(urlEncode(email));
			sb.append("&key=").append(urlEncode(easebuzzMerchantInfo.getKey()));
			sb.append("&surl=").append(urlEncode(easebuzzMerchantInfo.getSurl()));
			sb.append("&furl=").append(urlEncode(easebuzzMerchantInfo.getFurl()));
			sb.append("&txnid=").append(urlEncode(txnid));
			sb.append("&productinfo=").append(urlEncode(productInfo));
			sb.append("&hash=").append(urlEncode(EncryptionUtils.encryptPostData("SHA-512", plainSignature)));

			String formData = sb.toString();

			log.debug("Form data for initiate redirection to Easebuzz for refId is {}", formData);

			HttpUtils httpUtils = HttpUtils.builder().postData(formData).requestMethod(HttpUtils.REQ_METHOD_POST)
					.urlString(easebuzzMerchantInfo.getInitiateRedirectionURL()).build();
			Optional<String> responseOptional = httpUtils.getResponse(null);
			LogUtils.log(payment.getRefId(), "EasebuzzRedirection", httpUtils);
			if (responseOptional.isPresent()) {
				JsonParser jsonParser = new JsonParser();
				JsonObject responseJson = jsonParser.parse(responseOptional.get()).getAsJsonObject();
				String status = responseJson.get("status").getAsString();
				if (!"1".equals(status)) {
					JsonElement errorJson = responseJson.get("data");
					String error = errorJson == null ? "Failed to initiate redirection" : errorJson.toString();
					throw new Exception(error);
				}

				String data = responseJson.get("data").getAsString();
				easebuzzMerchantInfo.setGatewayURL(easebuzzMerchantInfo.getGatewayURL() + "/" + data);
			}
		} catch (Exception e) {
			log.error("Failed to get redirection response from Eazebuzz for refId {} due to {}", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "EasebuzzRedirection", e);
			if (e.getMessage() != null && e.getMessage().toLowerCase().contains("duplicate transaction id")) {
				throw new PaymentException(SystemError.EASEBUZZ_DUPLICATE_TXNID);
			}
			throw new PaymentException(SystemError.PAYMENT_FAILED);
		}
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		GatewayMerchantInfo updatedGatewayInfo = new GatewayMerchantInfo();
		updatedGatewayInfo.setGatewayType(gatewayInfo.getGatewayType());
		updatedGatewayInfo.setGatewayURL(gatewayInfo.getGatewayURL());
		updatedGatewayInfo.setRequestType(gatewayInfo.getRequestType());
		return super.convertToMap(updatedGatewayInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		PaymentProcessingResult processingResult = new PaymentProcessingResult();
		EasebuzzPgResponse easebuzzPgResponse = (EasebuzzPgResponse) pgResponse;
		EasebuzzMerchantInfo easebuzzGatewayInfo = (EasebuzzMerchantInfo) gatewayMerchantInfo;
		if ((verifyHash(easebuzzPgResponse, easebuzzGatewayInfo, payment.getRefId(), processingResult)
				&& verifyPgResponseData(easebuzzPgResponse, payment, processingResult))) {
			processingResult.setStatus(PaymentStatus.SUCCESS);
		} else {
			processingResult.setStatus(PaymentStatus.FAILURE);
		}
		return processingResult;
	}

	private boolean verifyHash(EasebuzzPgResponse easebuzzPgResponse, EasebuzzMerchantInfo easebuzzGatewayInfo,
			String refId, PaymentProcessingResult processingResult) {
		try {
			String plainSignature =
					easebuzzPgResponse.getPlainSignature(easebuzzGatewayInfo.getSalt(), easebuzzGatewayInfo.getKey());
			String calculatedHash = EncryptionUtils.encryptPostData("SHA-512", plainSignature);
			if (!calculatedHash.equals(easebuzzPgResponse.getHash())) {
				log.error("Mismatched hash for refId {}. Calculated hash: {}, recieved hash: {}", refId, calculatedHash,
						easebuzzPgResponse.getHash());
				processingResult.getExternalPaymentInfo().setGatewayComment("Hash mismatch");
				return false;
			}
			return true;
		} catch (NoSuchAlgorithmException e) {
			log.error("Unable to evaluate signature for refId {} due to {}", refId, e);
			LogUtils.log(refId, "EasebuzzRedirection", e);
		}
		return true;
	}

	private boolean verifyPgResponseData(EasebuzzPgResponse easebuzzPgResponse, Payment payment,
			PaymentProcessingResult processingResult) {
		if (payment.getAmount().compareTo(new BigDecimal(easebuzzPgResponse.getAmount())) != 0) {
			log.error("Mismatched Amount for refId {}. Amount stored: {}, amount received: {}", payment.getRefId(),
					payment.getAmount(), easebuzzPgResponse.getAmount());
			processingResult.setStatus(PaymentStatus.FAILURE);
			processingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch in redirection request");
			return false;
		}
		return true;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PaymentProcessingResult trackingResult = null;
		try {
			EasebuzzMerchantInfo easebuzzGatewayInfo = (EasebuzzMerchantInfo) gatewayMerchantInfo;
			User payUser = usComm.getUserFromCache(payment.getPayUserId());
			double amount = payment.getAmount().doubleValue();
			String plainSignature = Joiner.on('|').useForNull("").join(easebuzzGatewayInfo.getKey(), payment.getRefId(),
					amount, payUser.getEmail(), payUser.getMobile(), easebuzzGatewayInfo.getSalt());
			String hash = EncryptionUtils.encryptPostData("SHA-512", plainSignature);

			StringBuilder sb = new StringBuilder();
			sb.append("amount=").append(urlEncode(String.valueOf(amount)));
			sb.append("&phone=").append(urlEncode(payUser.getMobile()));
			sb.append("&email=").append(urlEncode(payUser.getEmail()));
			sb.append("&txnid=").append(urlEncode(payment.getRefId()));
			sb.append("&key=").append(urlEncode(easebuzzGatewayInfo.getKey()));
			sb.append("&hash=").append(urlEncode(hash));
			String postData = sb.toString();

			EasebuzzVerificationResponse verificationResponse;
			final ExternalPaymentStatusInfo externalPaymentStatusInfo = new ExternalPaymentStatusInfo();
			externalPaymentStatusInfo.setRefId(payment.getRefId());
			int retries = 0;
			do {
				if (retries > 0) {
					externalPaymentStatusInfo.setStatus(trackingResult.getStatus());
					externalPaymentStatusInfo
							.setComment(getExternalPaymentStatusComment(trackingResult.getExternalPaymentInfo()));
					getExernalPaymentService().savePaymentStatus(externalPaymentStatusInfo);
					log.info("Payment for refId {} is in Pending status. Retry {}th time after 20 seconds.",
							payment.getRefId(), retries);
					Thread.sleep(20000);
				}
				HttpUtils httpUtils =
						HttpUtils.builder().urlString(easebuzzGatewayInfo.getTrackingUrl()).postData(postData).build();
				verificationResponse = httpUtils.getResponse(EasebuzzVerificationResponse.class).orElse(null);
				LogUtils.log(payment.getRefId(), "EasebuzzVerification", httpUtils);
				trackingResult = prepareTrackingResult(verificationResponse, payment);
			} while (PaymentStatus.PENDING.equals(trackingResult.getStatus()) && ++retries <= 10);
		} catch (Exception e) {
			log.error("Failed to verify payment from Easebuzz for refId {} due to", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "EasebuzzVerification", e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}

	private PaymentProcessingResult prepareTrackingResult(EasebuzzVerificationResponse verificationResponse,
			Payment payment) {
		PaymentProcessingResult processingResult = new PaymentProcessingResult();
		String status = verificationResponse.getTransactionDetails().getStatus();
		String amountDebitedStr = verificationResponse.getTransactionDetails().getTotalAmountDebited();
		BigDecimal amountDebited = new BigDecimal(amountDebitedStr);
		processingResult.getExternalPaymentInfo()
				.setGatewayComment(verificationResponse.getTransactionDetails().getError());
		processingResult.getExternalPaymentInfo().setGatewayStatusCode(status);
		if (status == null || "pending".equals(status = status.toLowerCase())) {
			processingResult.setStatus(PaymentStatus.PENDING);
		} else if ("success".equals(status) && amountDebited.compareTo(payment.getAmount()) >= 0) {
			processingResult.setStatus(PaymentStatus.SUCCESS);
		} else if ("pending".equals(status)) {
			processingResult.setStatus(PaymentStatus.PENDING);
		} else {
			processingResult.setStatus(PaymentStatus.FAILURE);
			if ("success".equals(status)) {
				processingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
		}
		BigDecimal easebuzzPaymentFee = amountDebited.subtract(payment.getAmount());
		processingResult.getExternalPaymentInfo().setGatewayPaymentFee(easebuzzPaymentFee);
		processingResult.getExternalPaymentInfo().setTotalAmount(payment.getAmount().add(easebuzzPaymentFee));
		processingResult.getExternalPaymentInfo()
				.setMerchantTxnId(verificationResponse.getTransactionDetails().getEasebuzzTxnId());
		return processingResult;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		try {
			User payUser = usComm.getUserFromCache(payment.getPayUserId());
			EasebuzzMerchantInfo easebuzzMerchantInfo = (EasebuzzMerchantInfo) gatewayInfo;
			List<Payment> originalPayments = getOriginalPayments(payment.getRefId());

			BigDecimal originalAmount = originalPayments.get(0).getAmount()
					.subtract(originalPayments.get(0).getAdditionalInfo().getMerchantPaymentFee());
			String amount = String.valueOf(originalAmount.doubleValue()); // original amount
			String key = easebuzzMerchantInfo.getKey();
			String txnid = payment.getMerchantTxnId();
			String refund_amount = String.valueOf(payment.getAmount().doubleValue());
			String email = payUser.getEmail();
			String phone = payUser.getMobile();
			String plainSignature = Joiner.on('|').useForNull("").join(key, txnid, amount, refund_amount, email, phone,
					easebuzzMerchantInfo.getSalt());
			String hash = EncryptionUtils.encryptPostData("SHA-512", plainSignature);

			return EasebuzzRefundInfo.builder().amount(amount).key(key).email(email).phone(phone)
					.refund_amount(refund_amount).txnid(txnid).hash(hash).build();
		} catch (Exception e) {
			LogUtils.log(payment.getRefId(), "EasebuzzRefund", e);
			throw new RuntimeException("Could not create EasebuzzRefundInfo", e);
		}
	}

	private List<Payment> getOriginalPayments(String refId) {
		return pmsService.search(PaymentFilter.builder().refId(refId).status(PaymentStatus.SUCCESS)
				.type(PaymentTransactionType.PAID_FOR_ORDER).build());
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo) {
		EasebuzzRefundInfo easebuzzRefundInfo = (EasebuzzRefundInfo) refundInfo;
		EasebuzzMerchantInfo easebuzzGatewayInfo = (EasebuzzMerchantInfo) gatewayInfo;

		EasebuzzRefundResponse response = null;
		try {
			StringBuilder sb = new StringBuilder();
			sb.append("amount=").append(urlEncode(easebuzzRefundInfo.getAmount()));
			sb.append("&refund_amount=").append(urlEncode(easebuzzRefundInfo.getRefund_amount()));
			sb.append("&phone=").append(urlEncode(easebuzzRefundInfo.getPhone()));
			sb.append("&email=").append(urlEncode(easebuzzRefundInfo.getEmail()));
			sb.append("&txnid=").append(urlEncode(easebuzzRefundInfo.getTxnid()));
			sb.append("&key=").append(urlEncode(easebuzzRefundInfo.getKey()));
			sb.append("&hash=").append(urlEncode(easebuzzRefundInfo.getHash()));
			String postData = sb.toString();

			HttpUtils httpUtils =
					HttpUtils.builder().urlString(easebuzzGatewayInfo.getRefundURL()).postData(postData).build();
			response = httpUtils.getResponse(EasebuzzRefundResponse.class).orElse(null);
			LogUtils.log(payment.getRefId(), "EasebuzzRefund", httpUtils);
		} catch (IOException e) {
			log.error("Unable to initiate refund for refId {} due to ", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "EasebuzzRefund", e);
		}

		boolean success = (response != null && BooleanUtils.isTrue(response.getStatus()));
		String merchantRefundId = response == null ? null : response.getRefundId();
		return RefundResult.builder().isRefundSuccessful(success).merchantRefundId(merchantRefundId).build();
	}

	private String urlEncode(String str) throws UnsupportedEncodingException {
		return URLEncoder.encode(str, "UTF-8");
	}

	@Override
	protected String getGatewayName() {
		return "Easebuzz Gateway";
	}

}
