package com.tgs.services.pms.manager.easypay;

import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.StringJoiner;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.EasyPayMerchantInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.EasyPayRefundInfo;
import com.tgs.services.pms.datamodel.pg.EasyPayRefundInputData;
import com.tgs.services.pms.datamodel.pg.EasyPayRefundResponse;
import com.tgs.services.pms.datamodel.pg.EasyPayResponse;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EasyPayPaymentManager extends GateWayManager {

	@Autowired
	UserServiceCommunicator userComm;

	protected static final String SUCCESSCODE = "E000";
	/**
	 * For real time transaction, We can consider RIP as success transaction and can process order, We will receive
	 * success when amount is credited to merchant's account, It will take 1-2 business day.
	 * 
	 */
	protected static final String SUCCESSSTATUS = "RIP";

	private EasyPayResponse eazyPayResponse;

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {

		User payUser = userComm.getUserFromCache(payment.getPayUserId());
		EasyPayMerchantInfo easyMerchantInfo = (EasyPayMerchantInfo) gatewayInfo;
		easyMerchantInfo.setPgamount(String.valueOf(payment.getAmount()));
		easyMerchantInfo.setEmail(payUser.getEmail());
		easyMerchantInfo.setMobileno(payUser.getMobile());
		easyMerchantInfo.setReference_No(payment.getRefId());
		easyMerchantInfo.setPaymode(easyMerchantInfo.getPaymode().substring(0, 1));
		easyMerchantInfo.setOptional_fields("");
		easyMerchantInfo.setMandatory_fields(easyMerchantInfo.getMandatoryFields());
		try {
			encryptFields(easyMerchantInfo);
		} catch (Exception e) {
			log.error("Unable to encrypt fields for refId {} due to {}", payment.getRefId(), e);
		}
	}

	public void encryptFields(EasyPayMerchantInfo easyMerchantInfo) throws Exception {
		String signature = easyMerchantInfo.getSignature();
		easyMerchantInfo.setMandatory_fields(encrypt(signature, easyMerchantInfo.getMandatory_fields()));
		easyMerchantInfo.setReturnurl(encrypt(signature, easyMerchantInfo.getReturnurl()));
		easyMerchantInfo.setReference_No(encrypt(signature, easyMerchantInfo.getReference_No()));
		easyMerchantInfo.setSubmerchantid(encrypt(signature, easyMerchantInfo.getSubmerchantid()));
		easyMerchantInfo.setTransaction_amount(encrypt(signature, easyMerchantInfo.getPgamount()));
		easyMerchantInfo.setPaymode(encrypt(signature, easyMerchantInfo.getPaymode()));
	}

	public String encrypt(String key, String data) throws Exception {
		byte[] encryptedBytes = EncryptionUtils.encryptUsingAES(key, data);
		return java.util.Base64.getEncoder().encodeToString(encryptedBytes);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		eazyPayResponse = (EasyPayResponse) pgResponse;
		return PaymentProcessingResult.builder().status(PaymentStatus.SUCCESS).build();
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		try {
			EasyPayMerchantInfo easyPayMerchantInfo = (EasyPayMerchantInfo) gatewayMerchantInfo;
			if (eazyPayResponse.getResponseCode().equals(SUCCESSCODE)) {
				Map<String, String> queryParams = new HashMap<>();
				queryParams.put("ezpaytranid", eazyPayResponse.getUniqueRefId());
				queryParams.put("amount", eazyPayResponse.getTransactionAmount());
				queryParams.put("merchantid", eazyPayResponse.getMerchantId());
				queryParams.put("trandate", getFormattedDateForTracking(LocalDate.now()));
				queryParams.put("pgreferenceno", payment.getRefId());

				HttpUtils httpUtils = HttpUtils.builder().queryParams(queryParams)
						.urlString(easyPayMerchantInfo.getTrackingUrl()).build();

				Optional<String> trackingResponse = httpUtils.getResponse(null);
				String url = httpUtils.getUrlString();
				String responseString = trackingResponse.orElse(null);
				log.info("Request URL {}, Response received from EasyPay payment gateway for ref Id {} is {}", url,
						payment.getRefId(), responseString);

				LogUtils.store(Arrays.asList(
						LogData.builder().generationTime(LocalDateTime.now()).logData(url).key(payment.getRefId())
								.type("Eazy Pay Tracking Request").logType("AirSupplierAPILogs").build()));

				LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
						.logData(responseString).key(payment.getRefId()).type("Eazy Pay Tracking Response")
						.logType("AirSupplierAPILogs").build()));


				Map<String, String> query_pairs = getResponseQuerypairs(responseString);

				String mechantTxnId = query_pairs.get("ezpaytranid");
				trackingResult.getExternalPaymentInfo().setMerchantTxnId(mechantTxnId);

				Double transAmt = NumberUtils.isCreatable(eazyPayResponse.getTransactionAmount())
						? Double.valueOf(eazyPayResponse.getTransactionAmount())
						: 0.0;

				trackingResult.getExternalPaymentInfo().setGatewayStatusCode(query_pairs.get("status"));
				if (eazyPayResponse.getUniqueRefId().equals(mechantTxnId)
						&& eazyPayResponse.getReferenceNo().equals(payment.getRefId())
						&& com.tgs.utils.common.NumberUtils.compareDouble(transAmt.doubleValue(),
								payment.getAmount().doubleValue(), 2) == 0
						&& SUCCESSSTATUS.equalsIgnoreCase((query_pairs.get("status")))) {
					trackingResult.setStatus(PaymentStatus.SUCCESS);
				} else {
					trackingResult.setStatus(PaymentStatus.FAILURE);
					if (SUCCESSSTATUS.equalsIgnoreCase(query_pairs.get("status"))) {
						trackingResult.getExternalPaymentInfo().setGatewayComment("Params Mismatch");
					}
				}
			}
		} catch (Exception e) {
			log.error("Fetching payment status failed for refId {} due to", payment.getRefId(), e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}


	private Map<String, String> getResponseQuerypairs(String responseString) {
		Map<String, String> query_pairs = new LinkedHashMap<String, String>();
		String[] pairs = responseString.split("&");
		for (String pair : pairs) {
			int idx = pair.indexOf("=");
			query_pairs.put(pair.substring(0, idx), pair.substring(idx + 1));
		}
		return query_pairs;
	}

	@Override
	protected EasyPayRefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		EasyPayRefundInfo easyPayRefundInfo = new EasyPayRefundInfo();
		EasyPayMerchantInfo merchantInfo = (EasyPayMerchantInfo) gatewayInfo;
		easyPayRefundInfo.setMerchantid(merchantInfo.getMerchantid());
		String inputdata = GsonUtils.getGson().toJson(getRefundInputData(merchantInfo, payment));
		log.info("Refund Input Data for EasyPay for refId {} is : {}", payment.getRefId(), inputdata);
		try {
			easyPayRefundInfo.setInputData(encrypt(merchantInfo.getSignature(), inputdata));
		} catch (Exception e) {
			log.error("Unable to encrypt signature for refund with refId {} due to {}", payment.getRefId(), e);
			throw new PaymentException(SystemError.REFUND_FAILED);
		}
		return easyPayRefundInfo;
	}

	private EasyPayRefundInputData getRefundInputData(EasyPayMerchantInfo merchantInfo, Payment payment) {

		EasyPayRefundInputData inputData = new EasyPayRefundInputData();
		try {
			inputData.setMerchantId(merchantInfo.getMerchantid());
			inputData.setPaymode(merchantInfo.getPaymode().substring(1, merchantInfo.getPaymode().length()));
			inputData.setRefundAmount(payment.getAmount().toString());
			inputData.setSignature(EncryptionUtils.getEncodedValueUsingSHA512(merchantInfo.getSignature(),
					getSignature(merchantInfo, payment)));
			inputData.setTransactionDate(payment.getAdditionalInfo().getPaymentDate().toString());
			inputData.setTransactionId(payment.getMerchantTxnId());
			inputData.setUserId(merchantInfo.getUserId());
		} catch (InvalidKeyException | NoSuchAlgorithmException | UnsupportedEncodingException e) {
			log.error("Unable to encode signature for refId {} due to {}", payment.getRefId(), e);
			throw new PaymentException(SystemError.REFUND_FAILED);
		}

		return inputData;
	}

	private String getSignature(EasyPayMerchantInfo merchantInfo, Payment payment) {
		String signature = new String();
		StringJoiner joiner = new StringJoiner("|");
		joiner.add(merchantInfo.getMerchantid());
		joiner.add(payment.getMerchantTxnId());
		joiner.add(payment.getAmount().toString());
		signature = joiner.toString();
		log.info("EasyPay Refund Signature for refId {} is  {} ", payment.getRefId(), signature);
		return signature;
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		boolean isRefundSuccessful = false;
		EasyPayRefundInfo easyPayRefundInfo = (EasyPayRefundInfo) refundInfo;
		String refundRequest = GsonUtils.getGson().toJson(easyPayRefundInfo);
		log.info("EasyPay Refund Request for refId {} is : {}", payment.getRefId(), refundRequest);
		EasyPayMerchantInfo easyPayMerchantInfo = (EasyPayMerchantInfo) gatewayInfo;
		HttpUtils httpUtils = HttpUtils.builder().requestMethod(HttpUtils.REQ_METHOD_POST).postData(refundRequest)
				.urlString(easyPayMerchantInfo.getRefundURL()).headerParams(getHeaderParams(easyPayMerchantInfo))
				.build();
		EasyPayRefundResponse refundResponse = httpUtils.getResponse(EasyPayRefundResponse.class).orElse(null);
		log.info("EasyPay Refund Response for refId {} is : {}", payment.getRefId(),
				GsonUtils.getGson().toJson(refundResponse));
		if ("Y".equals(refundResponse.getStatus())) {
			isRefundSuccessful = true;
			log.info("Total refunded amount for refId {} is : {}", payment.getRefId(),
					refundResponse.getRefundedAmount());
		} else {
			log.info("Error while initiating refund for refId {}, Reason : {}", payment.getRefId(),
					StringUtils.join(refundResponse.getMessage(), ",", refundResponse.getErrorMessage()));
		}
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful)
				.merchantRefundId(refundResponse.getTransactionId()).build();
	}

	private Map<String, String> getHeaderParams(EasyPayMerchantInfo easyPayMerchantInfo) {
		Map<String, String> headerParams = new HashMap<String, String>();
		headerParams.put("content-type", "application/json");
		headerParams.put("private_key", easyPayMerchantInfo.getHeaderPrivateKey());
		log.info("Headers Param : {}", headerParams);
		return headerParams;
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		Map<String, String> gatewayFields = new HashMap<>();
		gatewayFields = super.convertToMap(gatewayInfo);
		Map<String, String> orderedGatewayFields = arrangeGateWayFields(gatewayFields);
		return orderedGatewayFields;
	}

	private Map<String, String> arrangeGateWayFields(Map<String, String> gatewayFields) {
		Map<String, String> newFieldMap = new LinkedHashMap<>();
		newFieldMap.put("merchantid", gatewayFields.get("merchantid"));
		newFieldMap.put("mandatory fields", gatewayFields.get("mandatory_fields"));
		newFieldMap.put("optional fields", gatewayFields.get("optional_fields"));
		newFieldMap.put("returnurl", gatewayFields.get("returnurl"));
		newFieldMap.put("Reference No", gatewayFields.get("Reference_No"));
		newFieldMap.put("submerchantid", gatewayFields.get("submerchantid"));
		newFieldMap.put("transaction amount", gatewayFields.get("transaction_amount"));
		newFieldMap.put("paymode", gatewayFields.get("paymode"));
		newFieldMap.put("gatewayType", gatewayFields.get("gatewayType"));
		newFieldMap.put("requestType", "GET");
		return newFieldMap;
	}

	private String getFormattedDateForTracking(LocalDate date) {
		if (date == null) {
			return null;
		}
		return date.format(DateTimeFormatter.ofPattern("yyyyMMdd"));
	}

	@Override
	protected String getGatewayName() {
		return "Easypay Gateway";
	}

}
