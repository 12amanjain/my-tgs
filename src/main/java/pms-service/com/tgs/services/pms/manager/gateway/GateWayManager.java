package com.tgs.services.pms.manager.gateway;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MoneyExchangeCommunicator;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.ExternalPayment;
import com.tgs.services.pms.datamodel.ExternalPaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.PGDetail;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentProcessingResult.ExternalPaymentInfo;
import com.tgs.services.pms.datamodel.PaymentReason;
import com.tgs.services.pms.datamodel.PaymentReason.PaymentReasonContainer;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.jparepository.ExternalPaymentService;
import com.tgs.services.pms.manager.IGateWayManager;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.utils.exception.PaymentException;
import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Getter
@Setter
@Service
@Slf4j
public abstract class GateWayManager implements IGateWayManager {

	@Getter(AccessLevel.PROTECTED)
	@Autowired
	private ExternalPaymentService exernalPaymentService;

	@Autowired
	private MoneyExchangeCommunicator moneyExchnageComm;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	/**
	 * Set Payment PGDetail Object as Payment Gateway -Redirection URL & post data
	 */

	private void setPGDetail(PaymentConfigOutput mediumConfig, PaymentRequest payment) {
		mediumConfig.getGateWayInfo().setGatewayType(mediumConfig.getGatewayType().toString());
		populateGatewayMerchantInfo(mediumConfig.getGateWayInfo(), payment);
		Map<String, String> gateWayFields = getGatewayFields(mediumConfig.getGateWayInfo());
		PGDetail pgDetail = PGDetail.builder().paymentFields(gateWayFields).build();
		payment.setPgDetail(pgDetail);
	}

	/**
	 * 
	 * Implementation decides the order of Query-Params or any other transformation not supported by convertToMap(..),
	 * if needed.
	 * 
	 * @param gatewayInfo
	 * @return
	 */
	protected abstract Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo);


	@Override
	public final PaymentRequest initializeGatewayData(PaymentRequest payment, PaymentConfigOutput mediumConfig)
			throws PaymentException {
		payment.getAdditionalInfo().setGateway(mediumConfig.getGatewayType().name());
		setPGDetail(mediumConfig, payment);
		return payment;
	}

	protected Map<String, String> convertToMap(Object obj) {
		if (obj == null)
			return new HashMap<>();
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

	@Override
	public final Payment refund(Payment payment, PaymentConfigOutput mediumConfig) throws PaymentException {
		boolean isRefundSuccessful = false;
		try {
			RefundInfo refundInfo = getRefundInfo(mediumConfig.getGateWayInfo(), payment);
			RefundResult refundResult = refund(payment, refundInfo, mediumConfig.getGateWayInfo());
			if (refundResult != null) {
				isRefundSuccessful = refundResult.isRefundSuccessful;
				if (isRefundSuccessful) {
					payment.setMerchantTxnId(refundResult.merchantRefundId);
				}
			}
		} catch (Exception e) {
			log.error("Unable to process refund for refId {} ", payment.getRefId(), e);
		}

		if (!isRefundSuccessful) {
			log.error("Refund failed for refId {} ", payment.getRefId());
			payment.setMerchantTxnId(null);
			throw new PaymentException(SystemError.REFUND_FAILED);
		}
		return payment;
	}

	public BigDecimal getAmountBasedOnCurrency(BigDecimal amount, String fromCurrency, String toCurrency) {
		if (fromCurrency.equalsIgnoreCase(toCurrency)) {
			return amount;
		}
		MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().fromCurrency(fromCurrency)
				.toCurrency(toCurrency).type("paymentGateway").build();
		Double exchangedAmount = moneyExchnageComm.getExchangeValue(amount.doubleValue(), filter, true);
		return BigDecimal.valueOf(exchangedAmount).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	final public PaymentProcessingResult verifyPayment(Payment payment, PgResponse pgResponse) throws PaymentException {
		PaymentProcessingResult verificationResult;
		final ExternalPaymentStatusInfo externalPaymentStatusInfo = new ExternalPaymentStatusInfo();
		externalPaymentStatusInfo.setRefId(payment.getRefId());
		try {
			GatewayMerchantInfo gatewayMerchantInfo = getGatewayMerchantInfo(payment);
			verificationResult = verifyPgResponse(payment, pgResponse, gatewayMerchantInfo);
			mustNotBeNull(verificationResult);
			updatePayment(verificationResult, payment);
			if (PaymentStatus.SUCCESS.equals(verificationResult.getStatus())) {
				verificationResult = trackPayment(payment, gatewayMerchantInfo);
				mustNotBeNull(verificationResult);
				updatePayment(verificationResult, payment);
			}
			externalPaymentStatusInfo.setStatus(verificationResult.getStatus());
			externalPaymentStatusInfo
					.setComment(getExternalPaymentStatusComment(verificationResult.getExternalPaymentInfo()));
		} catch (Exception e) {
			externalPaymentStatusInfo.setStatus(PaymentStatus.PENDING);
			externalPaymentStatusInfo.setComment("Redirected; Failed to track payment");
			throw e;
		} finally {
			exernalPaymentService.updateExternalPayment(getExternalPaymentUpdate(payment, externalPaymentStatusInfo));
			exernalPaymentService.savePaymentStatus(externalPaymentStatusInfo);
		}
		return verificationResult;
	}

	private void mustNotBeNull(PaymentProcessingResult verificationResult) {
		if (verificationResult == null || verificationResult.getStatus() == null) {
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
	}

	private GatewayMerchantInfo getGatewayMerchantInfo(Payment payment) {
		return (GatewayMerchantInfo) ((PaymentConfigOutput) PaymentConfigurationHelper
				.getPaymentRule(payment.getAdditionalInfo().getRuleId(), PaymentRuleType.PAYMENT_MEDIUM).getOutput())
						.getGateWayInfo();
	}

	private void updatePayment(PaymentProcessingResult verificationResult, Payment payment) {
		ExternalPaymentInfo externalPaymentInfo = verificationResult.getExternalPaymentInfo();
		BigDecimal gatewayPaymentFee = externalPaymentInfo.getGatewayPaymentFee();
		if (gatewayPaymentFee != null) {
			payment.getAdditionalInfo().setMerchantPaymentFee(gatewayPaymentFee);
			payment.setPaymentFee(payment.getPaymentFee().add(gatewayPaymentFee));
		}
		// if amount actually debited was different from requested amount, update payment amount
		BigDecimal totalAmount = externalPaymentInfo.getTotalAmount();
		if (totalAmount != null) {
			payment.setAmount(totalAmount);
		}
		if (externalPaymentInfo.getMerchantTxnId() != null) {
			payment.setMerchantTxnId(externalPaymentInfo.getMerchantTxnId());
		}
		payment.setReason(getPaymentReasons(verificationResult, payment).getCombinedReasons());
	}

	private PaymentReasonContainer getPaymentReasons(PaymentProcessingResult processingResult, Payment payment) {
		PaymentReasonContainer reasonContainer = new PaymentReasonContainer();
		reasonContainer.addReason(PaymentReason.PAID_USING, getGatewayName());
		reasonContainer.addReason(PaymentReason.PAYMENT_FEE, payment.getPaymentFee());
		if (PaymentStatus.SUCCESS.equals(processingResult.getStatus())) {
			reasonContainer.addReason(PaymentReason.MERCHANT_TXN_ID, payment.getMerchantTxnId());
		} else {
			reasonContainer.addReason(PaymentReason.FAILURE_REASON,
					processingResult.getExternalPaymentInfo().getGatewayComment());
		}
		return reasonContainer;
	}

	protected String getExternalPaymentStatusComment(ExternalPaymentInfo externalPaymentInfo) {
		return Joiner.on("; ").useForNull("null")
				.join("Redirected", externalPaymentInfo.getGatewayStatusCode(), externalPaymentInfo.getGatewayComment())
				.toString();
	}

	protected ExternalPayment getExternalPaymentUpdate(Payment payment,
			ExternalPaymentStatusInfo externalPaymentStatusInfo) {
		ExternalPaymentAdditionalInfo additionalInfo =
				ExternalPaymentAdditionalInfo.builder().merchantTxnId(payment.getMerchantTxnId()).build();
		return ExternalPayment.builder().refId(payment.getRefId()).status(externalPaymentStatusInfo.getStatus())
				.additionalInfo(additionalInfo).build();
	}

	public PaymentProcessingResult trackPayment(Payment payment) throws PaymentException {
		GatewayMerchantInfo gatewayMerchantInfo = getGatewayMerchantInfo(payment);
		return trackPayment(payment, gatewayMerchantInfo);
	}

	protected abstract PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException;

	protected abstract PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException;

	protected abstract void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment);

	protected abstract RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment);

	/**
	 *
	 * @param payment
	 * @param refundInfo
	 * @param gatewayInfo
	 * @return
	 * @throws Exception
	 */


	protected abstract RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception;

	protected abstract String getGatewayName();


	@Setter
	@Builder
	protected static class RefundResult {
		private boolean isRefundSuccessful;
		private String merchantRefundId;
	}


	public ClientGeneralInfo getClientInfo() {
		return gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
	}

}
