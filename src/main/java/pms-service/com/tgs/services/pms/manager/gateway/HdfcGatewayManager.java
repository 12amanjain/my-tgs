package com.tgs.services.pms.manager.gateway;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.HdfcMerchantInfo;
import com.tgs.services.pms.datamodel.pg.HdfcPgResponse;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class HdfcGatewayManager extends GateWayManager {


	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		HdfcPgResponse hdfcResponse = (HdfcPgResponse) pgResponse;

		/**
		 * PaymentID stored in MerchantTxnId is required for payment-verification
		 */
		boolean isCorrectPayment = hdfcResponse.verifyResponse(payment);

		// payment.setMerchantTxnId(hdfcResponse.getTranid());

		return null;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		return null;
	}

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		HdfcMerchantInfo hdfcMerchantInfo = (HdfcMerchantInfo) gatewayInfo;
		hdfcMerchantInfo.setAmt(String.valueOf(payment.getAmount()));
		hdfcMerchantInfo.setUdf1(GateWayType.HDFC_FSSNET.name());
		hdfcMerchantInfo.setTrackid(payment.getRefId());
		try {
			setRedirectionInfo(hdfcMerchantInfo);
		} catch (Exception e) {
			log.error("Unable to get redirection data from HDFC merchant for refId {} due to", payment.getRefId(), e);
			throw new PaymentException(SystemError.PAYMENT_FAILED);
		}
		/**
		 * For future reference while checking status.
		 */
		payment.setMerchantTxnId(hdfcMerchantInfo.getPaymentID());
	}

	private void setRedirectionInfo(HdfcMerchantInfo hdfcMerchantInfo) throws IOException {
		Map<String, String> params = convertToMap(hdfcMerchantInfo);
		String url = params.remove("gatewayURL");
		String response = getResponse(params, url), paymentId, redirectionUrl;

		int partitionIndex = response.indexOf(':');
		paymentId = response.substring(0, partitionIndex);
		redirectionUrl = response.substring(partitionIndex + 1);

		hdfcMerchantInfo.clear();
		hdfcMerchantInfo.setPaymentID(paymentId);
		hdfcMerchantInfo.setGatewayURL(redirectionUrl);
	}

	private String getResponse(Map<String, String> params, String urlString) throws IOException {
		HttpUtils httpUtils = HttpUtils.builder().queryParams(params).urlString(urlString)
				.requestMethod(HttpUtils.REQ_METHOD_POST).build();
		URL url = new URL(httpUtils.constructUrlWithQueryParams());
		HttpURLConnection httpUrlCon = (HttpURLConnection) url.openConnection();
		httpUrlCon.setDoInput(true);
		httpUrlCon.setDoOutput(true);
		byte[] data;
		data = httpUrlCon.getURL().toString().getBytes("UTF-8");
		httpUrlCon.setRequestProperty("Content-Length", String.valueOf(data.length));
		httpUrlCon.setRequestMethod("POST");
		OutputStream os = httpUrlCon.getOutputStream();
		BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
		writer.write("");

		writer.flush();
		writer.close();

		return httpUtils.getContent(httpUrlCon);
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		return super.convertToMap(gatewayInfo);
	}

	@Override
	protected String getGatewayName() {
		return "HDFC FSSNET Gateway";
	}
}
