package com.tgs.services.pms.manager.hdfcCcavenue;

import java.io.IOException;
import java.io.StringReader;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.services.base.LogData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueMerchantInfo;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenuePgResponse;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueRefundInfo;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueRefundResponse;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenueTransactionalDetails;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.common.NumberUtils;
import com.tgs.utils.encryption.AesCryptUtil;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HdfcCcavenueGatewayManager extends GateWayManager {

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		HdfcCcavenueMerchantInfo merchantInfo = (HdfcCcavenueMerchantInfo) gatewayInfo;
		merchantInfo.setAmount(payment.getAmount());
		merchantInfo.setOrder_id(payment.getRefId());
		merchantInfo.appendRefIdToCallBack(payment.getRefId());
		merchantInfo.setCancel_url(merchantInfo.getRedirect_url());
		merchantInfo.setCommand("initiateTransaction");
		Map<String, String> queryParams = new Gson().fromJson(GsonUtils.getGson().toJson(merchantInfo),
				new TypeToken<HashMap<String, String>>() {}.getType());
		String params = getRequestString(queryParams);
		try {
			merchantInfo.setEncRequest(getEncRequest(params, merchantInfo));
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| InvalidAlgorithmParameterException | IllegalBlockSizeException | BadPaddingException
				| UnsupportedEncodingException e) {
			log.error("Unable to encode request for refId {} due to {}", payment.getRefId(), e);
		}
	}

	private String getEncRequest(String params, HdfcCcavenueMerchantInfo merchantInfo) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException,
			UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		AesCryptUtil aesUtil = new AesCryptUtil(merchantInfo.getKey());
		String encRequest = aesUtil.encrypt(params);
		return encRequest;
	}

	public String getRequestString(Map<String, String> queryParams) {
		Set<String> keys = queryParams.keySet();
		StringBuilder query = new StringBuilder();
		for (String key : keys) {
			if (excludeFields(queryParams, key))
				continue;
			if (query.length() > 0)
				query.append("&");
			query.append(key).append("=").append(queryParams.get(key));
		}
		return query.toString();
	}

	private boolean excludeFields(Map<String, String> queryParams, String key) {
		return queryParams.get(key) == null || key.equals("gatewayURL") || key.equals("trackingUrl")
				|| key.equals("refundURL") || key.equals("requestType") || key.equals("gatewayType")
				|| key.equals("access_code") || key.equals("version") || key.equals("command") || key.equals("key")
				|| key.equals("encRequest");
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		HdfcCcavenueMerchantInfo merchantInfo = (HdfcCcavenueMerchantInfo) gatewayInfo;
		HdfcCcavenueMerchantInfo hdfcCcavenueMerchantInfo = new HdfcCcavenueMerchantInfo();
		hdfcCcavenueMerchantInfo.setGatewayType(merchantInfo.getGatewayType());
		hdfcCcavenueMerchantInfo.setGatewayURL(merchantInfo.getGatewayURL());
		hdfcCcavenueMerchantInfo.setEncRequest(merchantInfo.getEncRequest());
		hdfcCcavenueMerchantInfo.setCommand(merchantInfo.getCommand());
		hdfcCcavenueMerchantInfo.setAccess_code(merchantInfo.getAccess_code());
		return super.convertToMap(hdfcCcavenueMerchantInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		PaymentProcessingResult pgResponseVerificationResult = new PaymentProcessingResult();
		HdfcCcavenueMerchantInfo hdfcCcavenueGatewayInfo = (HdfcCcavenueMerchantInfo) gatewayMerchantInfo;
		HdfcCcavenuePgResponse hdfcCcavenuePgReponse = (HdfcCcavenuePgResponse) pgResponse;
		String refId = hdfcCcavenuePgReponse.getRefId();
		try {
			String decrptData = getdecodedData(hdfcCcavenuePgReponse.getEncResp(), hdfcCcavenueGatewayInfo);
			if (decrptData != null) {
				Map<String, String> data = getDecodedData(decrptData);
				hdfcCcavenuePgReponse =
						GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(data), HdfcCcavenuePgResponse.class);
				if (hdfcCcavenuePgReponse != null) {
					if (refId.equals(hdfcCcavenuePgReponse.getOrder_id())) {
						payment.setMerchantTxnId(hdfcCcavenuePgReponse.getTracking_id());
						pgResponseVerificationResult.getExternalPaymentInfo()
								.setGatewayComment(hdfcCcavenuePgReponse.getStatus_message());
						pgResponseVerificationResult.setStatus(PaymentStatus.SUCCESS);
					} else {
						log.error(
								"Mismatched refId. RefId received in  hdfcCcavenuePgReponse is {}. RefId stored in payment is {}",
								refId, hdfcCcavenuePgReponse.getRefId());
						pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
					}
				}
			} else {
				pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
			}
		} catch (Exception e) {
			log.error("Unable to verify response for refId {} due to {}", payment.getRefId(), e);
			pgResponseVerificationResult.setStatus(PaymentStatus.FAILURE);
		}
		return pgResponseVerificationResult;
	}

	private Map<String, String> getDecodedData(String decrptData) throws IOException {
		String propertiesFormat = decrptData.replaceAll("&", "\n");
		Properties properties = new Properties();
		properties.load(new StringReader(propertiesFormat));
		Map<String, String> data = new HashMap(properties);
		return data;
	}

	private String getdecodedData(String encResp, HdfcCcavenueMerchantInfo hdfcCcavenueGatewayInfo)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			InvalidAlgorithmParameterException, UnsupportedEncodingException, IllegalBlockSizeException,
			BadPaddingException {
		AesCryptUtil aesUtil = new AesCryptUtil(hdfcCcavenueGatewayInfo.getKey());
		return aesUtil.decrypt(encResp);
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PaymentProcessingResult trackingResult = null;
		try {
			HdfcCcavenueMerchantInfo hdfcCcavenueGatewayInfo = (HdfcCcavenueMerchantInfo) gatewayMerchantInfo;
			Map<String, String> queryParams = getTrackPaymentQueryParams(payment);
			String data = GsonUtils.getGson().toJson(queryParams);
			Map<String, String> queryParam = new HashMap<>();
			getRequestParams(hdfcCcavenueGatewayInfo, data, queryParam, "1.2", "orderStatusTracker");
			HttpUtils httpUtils =
					HttpUtils.builder().queryParams(queryParam).urlString(hdfcCcavenueGatewayInfo.getTrackingUrl())
							.requestMethod(HttpUtils.REQ_METHOD_POST).build();
			String response = (String) httpUtils.getResponse(null).get();
			HdfcCcavenueTransactionalDetails responseMap = null;
			Map<String, String> decResp = getDecodedData(response);
			if (decResp.get("status").equals("0")) {
				String decrptData = getdecodedData(decResp.get("enc_response"), hdfcCcavenueGatewayInfo);
				responseMap = GsonUtils.getGson().fromJson(decrptData, HdfcCcavenueTransactionalDetails.class);
			} else {
				responseMap = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(response),
						HdfcCcavenueTransactionalDetails.class);
			}

			log.info("Request URL {}, Response received from hdfc ccavenue payment gateway for ref Id {} is {}",
					httpUtils.getUrlString(), payment.getRefId(), GsonUtils.getGson().toJson(responseMap));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(httpUtils.getUrlString()).key(payment.getRefId()).type("Hdfc ccavenue Tracking Request")
					.logType("AirSupplierAPILogs").build()));
			LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
					.logData(GsonUtils.getGson().toJson(responseMap)).key(payment.getRefId())
					.type("Hdfc ccavenue Tracking Response").logType("AirSupplierAPILogs").build()));
			trackingResult = prepareTrackingResult(payment, responseMap);
		} catch (Exception e) {
			log.error("Fetching payment status failed for refId {} due to", payment.getRefId(), e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}

	private void getRequestParams(HdfcCcavenueMerchantInfo hdfcCcavenueGatewayInfo, String data,
			Map<String, String> queryParam, String version, String command) throws InvalidKeyException,
			NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException,
			UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
		queryParam.put("enc_request", getEncRequest(data, hdfcCcavenueGatewayInfo));
		queryParam.put("access_code", hdfcCcavenueGatewayInfo.getAccess_code());
		queryParam.put("command", command);
		queryParam.put("request_type", "JSON");
		queryParam.put("response_type", "JSON");
		queryParam.put("version", version);
	}

	public Map<String, String> getTrackPaymentQueryParams(Payment payment) {
		Map<String, String> queryParams = new HashMap<>();
		queryParams.put("order_no", payment.getRefId());
		return queryParams;
	}

	protected PaymentProcessingResult prepareTrackingResult(Payment payment,
			HdfcCcavenueTransactionalDetails transactionDetails) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(transactionDetails.getEnc_error_code());
		if (transactionDetails.getOrder_status() != null && transactionDetails.getStatus().equals("1")) {
			trackingResult.setStatus(PaymentStatus.FAILURE);
			trackingResult.getExternalPaymentInfo().setGatewayComment(transactionDetails.getEnc_response());
		} else if ("Shipped".equals(transactionDetails.getOrder_status())
				|| "Successful".equals(transactionDetails.getOrder_status())) {
			trackingResult.getExternalPaymentInfo().setMerchantTxnId(transactionDetails.getReference_no());
			if (NumberUtils.compareDoubleUpto2Decimal(Double.valueOf(transactionDetails.getOrder_amt()),
					payment.getAmount().doubleValue()) == 0) {
				trackingResult.setStatus(PaymentStatus.SUCCESS);
			} else {
				trackingResult.setStatus(PaymentStatus.FAILURE);
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch");
			}
		} else if ("Awaited".equals(transactionDetails.getOrder_status())
				|| "Initiated".equals(transactionDetails.getOrder_status())) {
			trackingResult.setStatus(PaymentStatus.PENDING);
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
			if (transactionDetails.getError_code() != null) {
				trackingResult.getExternalPaymentInfo().setGatewayComment(transactionDetails.getError_desc());
			}
		}
		return trackingResult;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		return HdfcCcavenueRefundInfo.builder().refund_amount(payment.getAmount())
				.reference_no(payment.getMerchantTxnId()).refund_ref_no(RandomStringUtils.random(7, true, true))
				.build();
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		boolean isRefundSuccessful = false;
		String atomRefundId = null;
		HdfcCcavenueMerchantInfo hdfcCcavenueGatewayInfo = (HdfcCcavenueMerchantInfo) gatewayInfo;
		HdfcCcavenueRefundInfo hdfcCcavenueRefundInfo = (HdfcCcavenueRefundInfo) refundInfo;
		String data = GsonUtils.getGson().toJson(hdfcCcavenueRefundInfo);
		Map<String, String> queryParam = new HashMap<>();
		getRequestParams(hdfcCcavenueGatewayInfo, data, queryParam, "1.1", "refundOrder");
		HttpUtils httpUtils = HttpUtils.builder().queryParams(queryParam)
				.urlString(hdfcCcavenueGatewayInfo.getTrackingUrl()).requestMethod(HttpUtils.REQ_METHOD_POST).build();
		String response = (String) httpUtils.getResponse(null).get();
		HdfcCcavenueRefundResponse responseMap = null;
		Map<String, String> decResp = getDecodedData(response);
		if (decResp.get("status").equals("0")) {
			String decrptData = getdecodedData(decResp.get("enc_response"), hdfcCcavenueGatewayInfo);
			responseMap = GsonUtils.getGson().fromJson(decrptData, HdfcCcavenueRefundResponse.class);
			if (responseMap.getRefund_status().equals("0")) {
				isRefundSuccessful = true;
				atomRefundId = hdfcCcavenueRefundInfo.getRefund_ref_no();
			}
		} else {
			responseMap = GsonUtils.getGson().fromJson(GsonUtils.getGson().toJson(response),
					HdfcCcavenueRefundResponse.class);
			isRefundSuccessful = false;
		}

		log.info("Request URL {}, Refund Response received from hdfc ccavenue payment gateway for ref Id {} is {}",
				httpUtils.getUrlString(), payment.getRefId(), GsonUtils.getGson().toJson(responseMap));
		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
				.logData(httpUtils.getUrlString()).key(payment.getRefId()).type("Hdfc ccavenue Refund Request")
				.logType("AirSupplierAPILogs").build()));
		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now())
				.logData(GsonUtils.getGson().toJson(responseMap)).key(payment.getRefId())
				.type("Hdfc ccavenue Refund Response").logType("AirSupplierAPILogs").build()));
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(atomRefundId).build();
	}

	@Override
	protected String getGatewayName() {
		return "hdfc_ccavenue";
	}

}
