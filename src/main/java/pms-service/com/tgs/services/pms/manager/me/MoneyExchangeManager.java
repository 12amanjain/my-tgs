package com.tgs.services.pms.manager.me;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.MoneyExchangeInfoFilter;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.MoneyExchangeConfiguration;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import com.tgs.services.pms.jparepository.MoneyExchangeService;

@Service
public class MoneyExchangeManager {
	
	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;
	
	@Autowired
	private MoneyExchangeService moneyExchangeService;
	
	
	public List<MoneyExchangeInfo> updateExchangeRate(MoneyExchangeInfo moneyExchangeInfo) throws IOException {
		
		List<MoneyExchangeInfo> result = new ArrayList<>();
		Set<String> types = new HashSet<>(moneyExchangeInfo.getTypes());
		List<ConfiguratorInfo> configRules = gmsCommunicator
				.getConfiguratorInfos(ConfiguratorRuleType.MONEYEXCHANGE);
		
		
		for(ConfiguratorInfo configRule : configRules) {
			
			MoneyExchangeConfiguration configOutput = (MoneyExchangeConfiguration)configRule.getOutput();
			if(CollectionUtils.isEmpty(configOutput.getType())) continue;
			List<String> selectedTypeList = new ArrayList<>();
			for(String typeInConfigInfo : configOutput.getType()) {
				if(types.contains(typeInConfigInfo))
					selectedTypeList.add(typeInConfigInfo);
			}
			
			if(CollectionUtils.isEmpty(selectedTypeList)) continue;
			
			MoneyExchangeInfoFilter filter = MoneyExchangeInfoFilter.builder().types(selectedTypeList).build();
			List<MoneyExchangeInfo> filteredMoneyExchangeInfo = moneyExchangeService.findAll(filter);
			if(CollectionUtils.isEmpty(filteredMoneyExchangeInfo))
				continue;
			IMoneyExchangeManager moneyExchangeManager = (IMoneyExchangeManager) SpringContext.getApplicationContext()
					.getBean(configOutput.getBean(), configOutput);
			List<MoneyExchangeInfo> updatedMoneyExchangeInfoList = moneyExchangeManager.getExchangeData(filteredMoneyExchangeInfo);
			moneyExchangeService.saveAll(updatedMoneyExchangeInfoList);
			result.addAll(updatedMoneyExchangeInfoList);
		}
		return result;
	}

}
