package com.tgs.services.pms.manager.me.xe;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class XERequest {

	private String from;
	private String to;
	private BigDecimal amount;

}
