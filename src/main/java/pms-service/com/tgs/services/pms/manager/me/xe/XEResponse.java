package com.tgs.services.pms.manager.me.xe;

import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class XEResponse {
	
	private String from;
	private List<ToData> to;
	
	
	@Getter
	@Setter
	class ToData {
		private String quotecurrency;
		private BigDecimal mid;
	}
	
}
