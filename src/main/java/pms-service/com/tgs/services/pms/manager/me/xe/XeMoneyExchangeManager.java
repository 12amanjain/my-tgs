package com.tgs.services.pms.manager.me.xe;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.configurationmodel.MoneyExchangeConfiguration;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import com.tgs.services.pms.manager.me.IMoneyExchangeManager;
import com.tgs.utils.common.HttpUtils;

@Service
public class XeMoneyExchangeManager implements IMoneyExchangeManager {
	
	@Autowired
	GeneralServiceCommunicator gmsComm;
	
	private MoneyExchangeConfiguration configInfo;
	
	public XeMoneyExchangeManager(MoneyExchangeConfiguration configInfo) {
		this.configInfo = configInfo;
	}

	@Override
	public List<MoneyExchangeInfo> getExchangeData(List<MoneyExchangeInfo> infoList) throws IOException {
		
		Map<String, List<MoneyExchangeInfo>> fromCurrencyKeyInfoValue = infoList.stream()
				.collect(Collectors.groupingBy(MoneyExchangeInfo :: getFromCurrency));
		
		for(String fromCurrency : fromCurrencyKeyInfoValue.keySet()) {
			
			List<MoneyExchangeInfo> moneyExchangeListwithForCurrency = fromCurrencyKeyInfoValue.get(fromCurrency);
			Map<String, MoneyExchangeInfo> toCurrencyKeyInfoValue = moneyExchangeListwithForCurrency
					.stream().collect(Collectors.toMap(MoneyExchangeInfo :: getToCurrency, Function.identity()));
			String to = moneyExchangeListwithForCurrency.stream().map(info -> info.getToCurrency())
					.collect(Collectors.joining(","));
			XERequest request = XERequest.builder().from(fromCurrency).to(to).amount(BigDecimal.ONE).build();
			HttpUtils httpUtils = getHttpUtils(request);
			XEResponse response = httpUtils.getResponse(XEResponse.class).orElse(null);
			updateMoneyExchangeInfoResult(response, toCurrencyKeyInfoValue);
		}
		
		return infoList;
		
	}
	
	private void updateMoneyExchangeInfoResult(XEResponse response, Map<String, MoneyExchangeInfo> toCurrencyKeyInfoValue) {
		
		List<XEResponse.ToData> toList = response.getTo();
		toList.forEach(toInfo -> {
			MoneyExchangeInfo moneyExchangeInfo = toCurrencyKeyInfoValue.get(toInfo.getQuotecurrency());
			moneyExchangeInfo.setExchangeRate(toInfo.getMid().doubleValue());
		});
	}

	private HttpUtils getHttpUtils(XERequest request) {
		
		String baseUrl = configInfo.getBaseUrl();
		Map<String, String> headerParams = getHeaderParams();
		Map<String, String> queryParams = convertToMap(request);
		HttpUtils httpUtils = HttpUtils.builder().headerParams(headerParams)
				.queryParams(queryParams).urlString(baseUrl).build();
		return httpUtils;
		
	}
	
	private Map<String, String> getHeaderParams() {
		
		String userName = configInfo.getUserName();
		String password = configInfo.getPassword();
		String authString = userName + ":" + password;
		String authStringEnc = new String(Base64.getEncoder().encode(authString.getBytes()));
		authStringEnc = "Basic "+authStringEnc;
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put("Authorization", authStringEnc);
		return headerParams;
	}

	private static <T> Map<String, String> convertToMap(T obj) {
		return new Gson().fromJson(
				GsonUtils.getGson(Arrays.asList(new AnnotationExclusionStrategy()), null).toJson(obj),
				new TypeToken<HashMap<String, String>>() {}.getType());
	}

}
