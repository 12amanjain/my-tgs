package com.tgs.services.pms.manager.paymentManagers;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.IPaymentManager;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public abstract class AbstractPaymentManager implements IPaymentManager {

	@Autowired
	protected PaymentService paymentService;

	@Autowired
	protected UserServiceCommunicator userServiceCommunicator;
	
	protected DbPayment doPayment(Payment payment) {
		try {
			DbPayment dbPayment = new DbPayment().from(payment);
			dbPayment.setStatus(PaymentStatus.SUCCESS.getPaymentStatus());
			if (StringUtils.isBlank(dbPayment.getPaymentRefId())) {
				dbPayment.setPaymentRefId(RandomStringUtils.random(10, false, true));
			}
			if(StringUtils.isEmpty(dbPayment.getPartnerId())) {
				User payUser = userServiceCommunicator.getUserFromCache(dbPayment.getPayUserId());
				dbPayment.setPartnerId(payUser.getPartnerId());
			}
			paymentService.save(dbPayment);
			return dbPayment;
		} catch (PaymentException e) {
			log.error("Unable to do payment for refId {} due to reason {}", payment.getRefId(), e.getMessage(), e);
			throw e;
		} catch (ConstraintViolationException | DataIntegrityViolationException e) {
			log.error("Unable to do payment for refId {} due to reason {}", payment.getRefId(), e.getMessage(), e);
			throw new PaymentException(SystemError.DUPLICATE_REQUEST);
		} catch (Exception e) {
			log.error("Unable to do payment for refId {} due to reason {}", payment.getRefId(), e.getMessage(), e);
			throw new PaymentException(SystemError.DATABASE_FAILURE);
		}
	}

}
