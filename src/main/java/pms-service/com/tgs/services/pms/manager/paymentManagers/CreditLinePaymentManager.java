package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.UserWalletFilter;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.pms.datamodel.CreditBillCycleType;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.TemporaryExtensionType;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.mapper.PaymentRequestToPaymentMapper;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class CreditLinePaymentManager extends AbstractPaymentManager {

	@Autowired
	private CreditLineService service;

	@Autowired
	private PaymentRequestToPaymentMapper paymentMapper;

	private CreditLine creditLine;

	@Autowired
	private UserWalletService walletService;

	@Override
	public Payment debit(Payment payment) throws PaymentException {
		return processPayment(payment);
	}

	@Override
	public Payment credit(Payment payment) throws PaymentException {
		payment.setAmount(payment.getAmount().multiply(BigDecimal.valueOf(-1)));
		return processPayment(payment);
	}

	private Payment processPayment(Payment payment) {
		if (payment.getType().equals(PaymentTransactionType.CREDIT_ISSUED)
				|| payment.getType().equals(PaymentTransactionType.CREDIT_RECALLED)
				|| payment.getType().equals(PaymentTransactionType.CREDIT_EXPIRED)
				|| payment.getType().equals(PaymentTransactionType.TEMPORARY_LIMIT_CHANGE)) {
			payment.setCurrentBalance(payment.getAdditionalInfo().getClosingBalance());
			return doPayment(payment).toDomain();
		}
		validateAndSetCreditLine(payment);
		creditLine = service.transact(payment.getAmount(), creditLine);
		sanitizeNegativeOutstanding();
		resetDynamicCycle(payment);
		payment.setCurrentBalance(creditLine.getBalance());
		payment.getAdditionalInfo().setUtilizedBalance(creditLine.getOutstandingBalance());
		setTotalClosingBalance(payment);
		Payment out = doPayment(payment).toDomain();
		checkAndReduceUnboundedTLE(payment);
		return out;
	}

	protected void setTotalClosingBalance(Payment payment) {
		if (BasePaymentUtils.isStoreClosingBalance()) {
			List<UserWallet> userWallet = walletService
					.search(UserWalletFilter.builder().userIdIn(Arrays.asList(payment.getPayUserId())).build());
			List<CreditLine> creditLines =
					service.search(CreditLineFilter.builder().userIdIn(Arrays.asList(payment.getPayUserId()))
							.statusIn(Arrays.asList(CreditStatus.BLOCKED, CreditStatus.EXPIRED)).build());
			BigDecimal totalDues = BigDecimal.ZERO;
			if (CollectionUtils.isNotEmpty(creditLines)) {
				for (CreditLine creditline : creditLines) {
					totalDues = totalDues.add(creditline.getOutstandingBalance());
					log.error("Total Due is {} for userid {}", totalDues, creditLine.getUserId());
				}
			}
			log.error("Befoe Total Dues {}, Utilized Balance {} , Wallet Balance {} for userId {} ", totalDues,
					creditLine.getOutstandingBalance(), userWallet.get(0).getBalance(), creditLine.getUserId());
			totalDues = totalDues.add(creditLine.getOutstandingBalance());
			log.error("Total Dues {}, Utilized Balance {} , Wallet Balance {} for userId {} ", totalDues,
					creditLine.getOutstandingBalance(), userWallet.get(0).getBalance(), creditLine.getUserId());
			payment.getAdditionalInfo().setTotalClosingBalance(userWallet.get(0).getBalance().subtract(totalDues));
		}
	}

	private void resetDynamicCycle(Payment payment) {
		if (creditLine.getBillCycleType().equals(CreditBillCycleType.DYNAMIC)) {
			if (creditLine.getBillCycleStart() == null && payment.includeInBill()) {
				creditLine.setBillCycleStart(LocalDateTime.now());
				creditLine.setBillCycleEnd(LocalDateTime.of(
						LocalDate.now().plusDays(creditLine.getAdditionalInfo().getBillCycleDays() - 1),
						LocalTime.MAX));
				service.save(creditLine);
			}
			if (creditLine.getAdditionalInfo().isDynamicReset()
					&& payment.getType().equals(PaymentTransactionType.TOPUP)
					&& creditLine.getOutstandingBalance().compareTo(BigDecimal.ZERO) <= 0) {
				creditLine.setBillCycleStart(null);
				creditLine.setBillCycleEnd(null);
				service.save(creditLine);
			}
		}
	}

	private void validateAndSetCreditLine(Payment payment) {
		if (StringUtils.isEmpty(payment.getWalletNumber()) && payment.getWalletId() == null) {
			List<CreditLine> creditLines = service.findCreditLinesByUserIdAndProduct(payment.getPayUserId(),
					payment.getProduct().getCode(), payment.getAmount());
			if (creditLines.size() == 0)
				throw new PaymentException(SystemError.NO_USABLE_CREDITLINE);
			creditLine = creditLines.get(0);
		} else {
			validate(payment);
		}
		payment.setWalletId(creditLine.getId());
		payment.setWalletNumber(creditLine.getCreditNumber());
	}

	private void validate(Payment payment) {

		creditLine = service.findByCreditNumberOrId(payment.getWalletNumber(),
				ObjectUtils.firstNonNull(payment.getWalletId(), 0L));
		if (!payment.getPayUserId().equals(creditLine.getUserId())) {
			throw new PaymentException(SystemError.CREDITLINE_USERID_NOTMATCH);
		}

		if (payment.getOpType().equals(PaymentOpType.CREDIT))
			return;

		if (!creditLine.getStatus().equals(CreditStatus.ACTIVE)
				&& !payment.getAdditionalInfo().isAllowForDisabledMediums())
			throw new PaymentException(SystemError.CREDITLINE_DISABLED);

		if (payment.getProduct() != null && payment.getProduct().hasOrder()
				&& creditLine.getProducts().stream().noneMatch(product -> product.equals(payment.getProduct())))
			throw new PaymentException(SystemError.CREDITLINE_INVALID_PRODUCT);

		if (creditLine.getBalance().compareTo(payment.getAmount()) < 0) {
			throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
		}
	}

	private void checkAndReduceUnboundedTLE(Payment payment) {
		log.info("CheckAndReduceUnboundedTLE called!");
		BigDecimal recallAmount = BigDecimal.ZERO;
		if (creditLine.getAdditionalInfo().getTemporaryExtensionType().equals(TemporaryExtensionType.UNBOUNDED)
				&& payment.getType().equals(PaymentTransactionType.TOPUP)
				&& creditLine.getAdditionalInfo().getCurTemporaryExt().compareTo(BigDecimal.ZERO) > 0) {
			recallAmount = creditLine.getAdditionalInfo().getCurTemporaryExt().min(payment.getAmount().abs());
		}
		log.info("recallAmount = {}", recallAmount.doubleValue());
		if (recallAmount.compareTo(BigDecimal.ZERO) > 0) {
			creditLine.getAdditionalInfo()
					.setCurTemporaryExt(creditLine.getAdditionalInfo().getCurTemporaryExt().subtract(recallAmount));
			service.save(creditLine);
			TLERecallPPB(recallAmount);
		}
	}

	private void TLERecallPPB(BigDecimal recallAmount) {
		PaymentAdditionalInfo additionalInfo = PaymentAdditionalInfo.builder()
				.utilizedBalance(creditLine.getOutstandingBalance()).allowForDisabledMediums(true).build();
		WalletPaymentRequest request =
				WalletPaymentRequest.builder().amount(recallAmount).paymentMedium(PaymentMedium.CREDIT_LINE)
						.additionalInfo(additionalInfo).transactionType(PaymentTransactionType.TEMPORARY_LIMIT_CHANGE)
						.payUserId(creditLine.getUserId()).opType(PaymentOpType.DEBIT)
						.walletNumber(creditLine.getCreditNumber()).walletId(creditLine.getId()).build();
		Payment payment = paymentMapper.toPayment(request);
		payment.setCurrentBalance(creditLine.getBalance());
		doPayment(payment);
	}

	private void sanitizeNegativeOutstanding() {
		if (creditLine.getOutstandingBalance().compareTo(BigDecimal.ZERO) < 0) {
			if (creditLine.getOutstandingBalance().compareTo(BigDecimal.valueOf(-0.05)) >= 0) {
				log.error("SANITIZING negative outstanding for creditLine: {}, outstanding = {}",
						creditLine.getCreditNumber(), creditLine.getOutstandingBalance());
				creditLine.setOutstandingBalance(BigDecimal.ZERO);
				service.save(creditLine);
			}
			log.error("Outstanding went negative for credit number: {}, outstanding = {}", creditLine.getCreditNumber(),
					creditLine.getOutstandingBalance());
		}
	}
}
