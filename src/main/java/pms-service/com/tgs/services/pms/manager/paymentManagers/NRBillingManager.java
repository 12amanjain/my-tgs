package com.tgs.services.pms.manager.paymentManagers;

import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.pms.datamodel.*;
import com.tgs.services.pms.dbmodel.DbNRCredit;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.manager.CreditBillingManager;
import com.tgs.services.pms.manager.CreditSystemMessagingClient;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@Slf4j
@Service
@Qualifier("NRBillingManager")
public class NRBillingManager extends CreditBillingManager {

    @Autowired
    private CreditBillingService billingService;

    @Autowired
    private NRCreditService nrCreditService;

    @Autowired
    private CreditSystemMessagingClient messagingClient;

    public List<CreditBill> checkAndGenerateNRBills() {
        Map<DbNRCredit, List<Payment>> creditPaymentMap = nrCreditService.getNRCreditsToBill();
        List<CreditBill> bills = new ArrayList<>();
        creditPaymentMap.forEach((dbNRCredit, paymentList) -> {
            // Need to maintain separate copies of old and new credits, do not change this logic
            NRCredit old = dbNRCredit.toDomain();
            NRCredit nuw = dbNRCredit.toDomain();
            CreditBill creditBill = generateBillAndExpire(nuw, paymentList);
            if (creditBill != null) {
                bills.add(creditBill);
                messagingClient.sendCreditBillMail(creditBill, old.toCredit(),
                        EmailTemplateKey.CREDITBILL_STATEMENT_EMAIL, Duration.ZERO);
            }
        });
        return bills;
    }

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public CreditBill generateBillAndExpire(NRCredit nrCredit, List<Payment> paymentList) {
        paymentList = CreditBillingManager.filterPaymentsToBill(paymentList, PaymentMedium.CREDIT);
        CreditBill creditBill = null;
        BigDecimal billAmount = BigDecimal.ZERO;
        for (Payment p : paymentList) {
            billAmount = billAmount.add(p.getAmount());
        }
        billAmount = billAmount.setScale(2, BigDecimal.ROUND_HALF_UP);
        if (billAmount.compareTo(BigDecimal.ZERO) > 0) {
            CreditPolicy policy = policyService.findByPolicyId(nrCredit.getAdditionalInfo().getPolicy());
            LocalDateTime paymentDate = LocalDateTime.now()
                    .plusHours(policy.getAdditionalInfo().getPaymentDueHours());
            short lockAfterHours = policy.getAdditionalInfo().getLockAfterHours();
            LocalDateTime lockDate = lockAfterHours < 0 ? LocalDateTime.MAX : paymentDate.plusHours(lockAfterHours);
            BillAdditionalInfo additionalInfo = BillAdditionalInfo.builder()
                    .creditType(CreditType.NON_REVOLVING)
                    .chargedPaymentIds(paymentList.stream().map(Payment::getId).collect(Collectors.toSet()))
                    .originalDueDate(paymentDate)
                    .originalLockDate(lockDate)
                    .cycleStart(nrCredit.getCreatedOn())
                    .cycleEnd(BaseUtils.min(LocalDateTime.now(), nrCredit.getCreditExpiry()))
                    .extensionAmount(nrCredit.creditExtensionAmount())
                    .policy(nrCredit.getAdditionalInfo().getPolicy())
                    .build();
            creditBill = CreditBill.builder()
                    .billNumber(RandomStringUtils.random(10, false, true))
                    .userId(nrCredit.getUserId())
                    .creditId(nrCredit.getId())
                    .paymentDueDate(paymentDate)
                    .additionalInfo(additionalInfo)
                    .lockDate(lockDate)
                    .billAmount(billAmount)
                    .isSettled(false)
                    .build();
            billingService.save(creditBill);
        } else if (billAmount.compareTo(BigDecimal.ZERO) < 0) {
            log.error("ERROR: Bill amount is negative for NRCredit {} => {}", nrCredit.getCreditId(), billAmount);
        }
        if (!nrCredit.getUtilized().equals(billAmount)) {
            log.error("Bill Amount & Utilized Amount mismatch for NRCredit number {}. Utilized = {}, BillAmount = {}",
                    nrCredit.getCreditId(), nrCredit.getUtilized(), billAmount);
        }
        nrCreditService.expireCredit(nrCredit);
        return creditBill;
    }
}
