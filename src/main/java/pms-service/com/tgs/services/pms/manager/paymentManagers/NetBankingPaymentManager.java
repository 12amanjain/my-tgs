package com.tgs.services.pms.manager.paymentManagers;

import org.springframework.stereotype.Service;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.utils.exception.PaymentException;

@Service
public class NetBankingPaymentManager extends AbstractPaymentManager {

	/**
	 * Return Payment Object that can be debited based on Gateway type.
	 *
	 * @param payment
	 *            <p>
	 *            EX: EASYPAY - [Redirection GateWay -Payment Entity Will be created
	 *            with IR status ]
	 *            </p>
	 * @return Specified Payment Object {@link Payment}
	 */
	@Override
	public Payment debit(Payment payment) throws PaymentException {
		boolean success = ((PgResponse) SystemContextHolder.getContextData().getMetaInfo()).getSuccess();
		if (!success) {
			throw new PaymentException(SystemError.PAYMENT_FAILED);
		}
		DbPayment dbPayment = doPayment(payment);
		return dbPayment.toDomain();
	}

	/**
	 * Return Payment Object that can be credited based on Gateway type. The Payment
	 * Object Must Specify which Medium & Product {@link GateWayType } - to get
	 * GateWay Type to Debit
	 *
	 * @param payment
	 * @return Specified Payment Object {@link Payment}
	 */
	@Override
	public Payment credit(Payment payment) throws PaymentException {
//		IGateWayManager gateWayManager = PaymentManagerFactory.initializeGatewayRedirectionData(payment);
//		gateWayManager.initializeGatewayParams(payment);
//		return gateWayManager.refund();
		return debit(payment);
	}
}
