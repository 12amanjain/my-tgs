package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.pms.datamodel.FundEmailAttributes;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.manager.UserBalanceManager;
import com.tgs.services.ums.datamodel.User;

@Service
public class PaymentMessenger {

	@Autowired
	private MsgServiceCommunicator msgCommunicator;

	@Autowired
	private UserBalanceManager userBalanceManager;

	@Autowired
	private UserServiceCommunicator userSrvCommunicator;

	public void sendPaymentEmail(List<Payment> paymentList) {
		Map<String, List<Payment>> userPayments =
				paymentList.stream().collect(Collectors.groupingBy(Payment::getPayUserId));
		for (Map.Entry<String, List<Payment>> entry : userPayments.entrySet()) {
			List<Payment> list = entry.getValue();
			if (CollectionUtils.isNotEmpty(list) && (list.get(0).getType().equals(PaymentTransactionType.REFUND)
					|| list.get(0).getType().equals(PaymentTransactionType.REVERSE)
					|| (list.get(0).getType().equals(PaymentTransactionType.TOPUP)
							&& list.get(0).getOpType().equals(PaymentOpType.CREDIT)))) {
				sendTopupOrRefundEmail(list);
			}
		}
	}

	private void sendTopupOrRefundEmail(List<Payment> paymentList) {
		BigDecimal processedAmount = BigDecimal.ZERO;
		for (Payment payment : paymentList) {
			processedAmount = processedAmount.add(payment.getAmount());
		}
		Payment firstPayment = paymentList.get(0);
		Map<String, BigDecimal> userBalance = userBalanceManager.getUserBalanceFromCache(
				PaymentMetaInfo.builder().userIds(Arrays.asList(firstPayment.getPayUserId())).build());
		BigDecimal newBalance = userBalance.get(firstPayment.getPayUserId());
		BigDecimal finalProcessedAmount = processedAmount.multiply(new BigDecimal(-1));
		AbstractMessageSupplier<FundEmailAttributes> msgAttributes =
				new AbstractMessageSupplier<FundEmailAttributes>() {
					@Override
					public FundEmailAttributes get() {
						User user = userSrvCommunicator.getUserFromCache(firstPayment.getPayUserId());
						FundEmailAttributes mailAttr = FundEmailAttributes.builder().build();
						mailAttr.setProcessedAmount(TgsStringUtils.formatCurrency(finalProcessedAmount));
						mailAttr.setPreviousBalance(
								TgsStringUtils.formatCurrency(newBalance.subtract(finalProcessedAmount)));
						mailAttr.setUsableBalance(TgsStringUtils.formatCurrency(newBalance));
						mailAttr.setRefId(firstPayment.getRefId());
						mailAttr.setKey(firstPayment.getType().equals(PaymentTransactionType.TOPUP)
								? EmailTemplateKey.DEPOSIT_UPDATE_EMAIL.name()
								: EmailTemplateKey.REFUND_EMAIL.name());
						mailAttr.setToEmailUserId(firstPayment.getPayUserId());
						mailAttr.setPartnerId(firstPayment.getPartnerId());
						mailAttr.setRole(user.getRole());
						return mailAttr;
					}
				};
		msgCommunicator.sendMail(msgAttributes.getAttributes());
	}

}
