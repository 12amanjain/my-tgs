package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.dbmodel.DbUserPoint;
import com.tgs.services.pms.jparepository.UserPointsService;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PointsPaymentManager extends AbstractPaymentManager {

	@Autowired
	private UserPointsService pointsService;

	@Override
	public Payment debit(Payment payment) throws PaymentException {
		payment.getAdditionalInfo().setNoOfPoints(payment.getAdditionalInfo().getNoOfPoints() * -1);
		payment.setCurrentBalance(updatePointsBalance(payment, false));
		DbPayment dbPayment = doPayment(payment);
		return dbPayment.toDomain();
	}

	@Override
	public Payment credit(Payment payment) throws PaymentException {
		payment.setAmount(payment.getAmount().multiply(BigDecimal.valueOf(-1)));
		payment.setCurrentBalance(updatePointsBalance(payment, true));
		DbPayment dbPayment = doPayment(payment);
		return dbPayment.toDomain();
	}

	public BigDecimal updatePointsBalance(Payment payment, boolean isCredit) {
		log.info("payment request {} ", GsonUtils.getGson().toJson(payment));
		Double quantity = payment.getAdditionalInfo().getNoOfPoints();
		PointsType type = payment.getAdditionalInfo().getPointsType();
		String payUserId = payment.getPayUserId();
		DbUserPoint dbUserPoints = pointsService.getDbUserPoints(payUserId, type);
		Double balance = quantity + dbUserPoints.getBalance();
		log.info("updated blc {}", balance);
		if (isCredit) {
			return updatePointsBalance(dbUserPoints, balance);
		} else {
			// In case of debit when other external payment medium is involved, points balance can go negative only till
			// minBalance
			long minBalance = BasePaymentUtils
					.getMinPointsBalanceAllowed(BooleanUtils.isTrue(payment.getConsiderNegativeBalance()));
			if (balance >= minBalance) {
				return updatePointsBalance(dbUserPoints, balance);
			} else {
				log.error("[Insufficient points balance] Points balance {}, Min Balance {}, requested balance {} ",
						dbUserPoints.getBalance(), BigDecimal.valueOf(minBalance), balance);
				throw new PaymentException(SystemError.INSUFFICIENT_BALANCE);
			}
		}

	}

	private BigDecimal updatePointsBalance(DbUserPoint pointsBalance, Double balance) {
		pointsBalance.setBalance(balance);
		pointsBalance = pointsService.save(pointsBalance);
		return BigDecimal.valueOf(pointsBalance.getBalance());
	}

}
