package com.tgs.services.pms.manager.paymentManagers;

import java.math.BigDecimal;

import org.springframework.stereotype.Service;

import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.utils.exception.PaymentException;

@Service
public class VirtualPaymentManager extends AbstractPaymentManager {

	@Override
	public Payment debit(Payment payment) throws PaymentException {
		DbPayment dbPayment = doPayment(payment);
		return dbPayment.toDomain();
	}

	@Override
	public Payment credit(Payment payment) throws PaymentException {
		payment.setAmount(payment.getAmount().multiply(BigDecimal.valueOf(-1)));
		DbPayment dbPayment = doPayment(payment);
		return dbPayment.toDomain();
	}

}
