package com.tgs.services.pms.manager.payu;

import java.math.BigDecimal;
import java.security.NoSuchAlgorithmException;
import java.util.HashMap;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.datamodel.pg.payu.PayUMerchantInfo;
import com.tgs.services.pms.datamodel.pg.payu.PayUPgResponse;
import com.tgs.services.pms.datamodel.pg.payu.PayURefundInfo;
import com.tgs.services.pms.datamodel.pg.payu.PayURefundResponse;
import com.tgs.services.pms.datamodel.pg.payu.PayUTransactionDetails;
import com.tgs.services.pms.datamodel.pg.payu.PayUVerificationResponse;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.encryption.EncryptionUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PayUGatewayManager extends GateWayManager {

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		PayUMerchantInfo payUGatewayInfo = (PayUMerchantInfo) gatewayInfo;
		payUGatewayInfo.setRefundURL(null);
		payUGatewayInfo.setTxnid(payment.getRefId());
		payUGatewayInfo.setProductinfo(payment.getProduct().name());
		payUGatewayInfo.setAmount(String.valueOf(payment.getAmount()));
		payUGatewayInfo.setFirstname(loggedInUser.getName());
		payUGatewayInfo.setEmail(loggedInUser.getEmail());
		payUGatewayInfo.setPhone(loggedInUser.getPhone());
		try {
			String plainSignature = payUGatewayInfo.getPlainSignature();
			payUGatewayInfo.setHash(EncryptionUtils.encryptPostData("SHA-512", plainSignature));
		} catch (NoSuchAlgorithmException e) {
			log.error("Unable to evaluate signature for refId {} due to {}", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "PayURedirection", e);
		}
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		return super.convertToMap(gatewayInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayInfo) throws PaymentException {
		PaymentProcessingResult processingResult = PaymentProcessingResult.builder().build();
		PayUPgResponse payUPgResponse = (PayUPgResponse) pgResponse;
		PayUMerchantInfo payUGatewayInfo = (PayUMerchantInfo) gatewayInfo;
		if (verifyHash(payUPgResponse, payUGatewayInfo, payment.getRefId(), processingResult)
				&& verifyPgResponseData(payUPgResponse, payment, processingResult)) {
			processingResult.setStatus(PaymentStatus.SUCCESS);
		} else {
			processingResult.setStatus(PaymentStatus.FAILURE);
		}
		return processingResult;
	}

	private boolean verifyHash(PayUPgResponse payUPgResponse, PayUMerchantInfo payUGatewayInfo, String refId,
			PaymentProcessingResult processingResult) {
		try {
			String plainSignature = payUPgResponse.getPlainSignature(payUGatewayInfo.getSalt());
			String calculatedHash = EncryptionUtils.encryptPostData("SHA-512", plainSignature);
			if (!calculatedHash.equals(payUPgResponse.getHash())) {
				log.error("Mismatched hash for refId {}. Calculated hash: {}, recieved hash: {}", refId, calculatedHash,
						payUPgResponse.getHash());
				processingResult.getExternalPaymentInfo().setGatewayComment("Hash mismatch");
				return false;
			}
			return true;
		} catch (NoSuchAlgorithmException e) {
			log.error("Unable to evaluate signature for refId {} due to {}", refId, e);
			LogUtils.log(refId, "PayUVerification", e);
		}
		return false;
	}

	private boolean verifyPgResponseData(PayUPgResponse payUPgResponse, Payment payment,
			PaymentProcessingResult processingResult) {
		if (payment.getAmount().compareTo(new BigDecimal(payUPgResponse.getAmount())) != 0) {
			log.error("Mismatched Amount for refId {}. Amount stored: {}, amount received: {}", payment.getRefId(),
					payment.getAmount(), payUPgResponse.getAmount());
			processingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch in redirection request");
			return false;
		}
		return true;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		PayUMerchantInfo payUGatewayInfo = (PayUMerchantInfo) gatewayMerchantInfo;
		PaymentProcessingResult trackingResult = null;
		final String key = payUGatewayInfo.getKey();
		final String var1 = payment.getRefId();
		final String checkPaymentCommand = "verify_payment";
		HashMap<String, String> queryParams = new HashMap<>();
		queryParams.put("form", "2");
		try {
			HttpUtils httpUtils = HttpUtils.builder().urlString(payUGatewayInfo.getTrackingUrl())
					.queryParams(queryParams).requestMethod(HttpUtils.REQ_METHOD_POST)
					.postData(getPayUWebServicePostData(key, checkPaymentCommand, payUGatewayInfo.getSalt(), var1))
					.build();
			PayUVerificationResponse verificationResponse =
					httpUtils.getResponse(PayUVerificationResponse.class).orElse(null);
			log.info("Verication response for PayU payment with refId {} is {}", payment.getRefId(),
					httpUtils.getResponseString());
			LogUtils.log(payment.getRefId(), "PayUVerification", httpUtils);

			trackingResult = prepareTrackingResult(verificationResponse, payment);
		} catch (Exception e) {
			log.error("Unable to verify payment for refId {} due to ", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "PayUVerification", e);
			throw new PaymentException(SystemError.PAYMENT_TRACKING_FAILED);
		}
		return trackingResult;
	}

	private String getPayUWebServicePostData(String key, String command, String salt, String... vars)
			throws NoSuchAlgorithmException {
		String hash = null;
		String plainSignature = Joiner.on('|').join(key, command, vars[0], salt);
		hash = EncryptionUtils.encryptPostData("SHA-512", plainSignature);
		StringBuilder sb = new StringBuilder();
		sb.append("key=").append(key).append("&command=").append(command).append("&hash=").append(hash);
		for (int i = 0; i < vars.length; i++) {
			sb.append("&var").append(i + 1).append("=").append(vars[i]);
		}
		return sb.toString();
	}

	private PaymentProcessingResult prepareTrackingResult(PayUVerificationResponse verificationResponse,
			Payment payment) {
		PaymentProcessingResult trackingResult = new PaymentProcessingResult();
		PayUTransactionDetails transactionDetails =
				verificationResponse.getTransactionDetails().get(payment.getRefId());
		String status = transactionDetails.getStatus().toLowerCase();
		trackingResult.getExternalPaymentInfo().setGatewayStatusCode(transactionDetails.getStatus());
		trackingResult.getExternalPaymentInfo().setGatewayComment(transactionDetails.getErrorMessage());
		if (status.equals("success")
				&& payment.getAmount().compareTo(new BigDecimal(transactionDetails.getAmount())) == 0) {
			trackingResult.setStatus(PaymentStatus.SUCCESS);
		} else if (status.equals("pending")) {
			trackingResult.setStatus(PaymentStatus.PENDING);
		} else {
			trackingResult.setStatus(PaymentStatus.FAILURE);
			if (status.equals("success")) {
				log.error("Mismatched Amount for refId {}. Amount stored: {}, amount received: {}", payment.getRefId(),
						payment.getAmount(), transactionDetails.getAmount());
				trackingResult.getExternalPaymentInfo().setGatewayComment("Amount mismatch in redirection request");
			}
		}
		trackingResult.getExternalPaymentInfo().setMerchantTxnId(transactionDetails.getPayUTxnId());
		BigDecimal payUPaymentFee = new BigDecimal(transactionDetails.getAdditionalCharges());
		trackingResult.getExternalPaymentInfo().setGatewayPaymentFee(payUPaymentFee);
		trackingResult.getExternalPaymentInfo().setTotalAmount(payment.getAmount().add(payUPaymentFee));
		return trackingResult;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		return PayURefundInfo.builder().refundAmt(String.valueOf(payment.getAmount()))
				.payUTxnId(payment.getMerchantTxnId()).tokenId(payment.getPaymentRefId()).build();
	}

	@Override
	protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo)
			throws Exception {
		PayURefundInfo payURefundInfo = (PayURefundInfo) refundInfo;
		PayUMerchantInfo payUGatewayInfo = (PayUMerchantInfo) gatewayInfo;
		final String key = payUGatewayInfo.getKey();
		final String var1 = payURefundInfo.getPayUTxnId();
		final String var2 = payURefundInfo.getTokenId();
		final String var3 = payURefundInfo.getRefundAmt();
		final String checkPaymentCommand = "cancel_refund_transaction";
		HashMap<String, String> queryParams = new HashMap<>();
		queryParams.put("form", "2");
		boolean isRefundSuccessful = false;
		String merchantRefundId = null;
		try {
			HttpUtils httpUtils = HttpUtils.builder().urlString(payUGatewayInfo.getRefundURL()).queryParams(queryParams)
					.requestMethod(HttpUtils.REQ_METHOD_POST).postData(getPayUWebServicePostData(key,
							checkPaymentCommand, payUGatewayInfo.getSalt(), var1, var2, var3))
					.build();
			PayURefundResponse refundResponse = httpUtils.getResponse(PayURefundResponse.class).orElse(null);
			log.info("Refund response for PayU payment with refId {} is {}", payment.getRefId(),
					httpUtils.getResponseString());
			LogUtils.log(payment.getRefId(), "PayURefund", httpUtils);
			isRefundSuccessful = "1".equals(refundResponse.getStatus());
			merchantRefundId = refundResponse.getRequestId();
		} catch (Exception e) {
			log.error("Unable to initiate refund for refId {} due to ", payment.getRefId(), e);
			LogUtils.log(payment.getRefId(), "PayURefund", e);
		}
		return RefundResult.builder().isRefundSuccessful(isRefundSuccessful).merchantRefundId(merchantRefundId).build();
	}

	@Override
	protected String getGatewayName() {
		return "PayU Gateway";
	}

}
