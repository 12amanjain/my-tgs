package com.tgs.services.pms.manager.razorpay;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Map;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.razorpay.RazorpayClient;
import com.razorpay.RazorpayException;
import com.razorpay.Refund;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.CurrencyConverter;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.pms.datamodel.GatewayMerchantInfo;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentProcessingResult;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.PgResponse;
import com.tgs.services.pms.datamodel.pg.RazorPayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.RazorPayRefundInfo;
import com.tgs.services.pms.datamodel.pg.RazorpayPgResponse;
import com.tgs.services.pms.datamodel.pg.RefundInfo;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.gateway.GateWayManager;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RazorpayGatewayManager extends GateWayManager {

	@Autowired
	private PaymentService paymentService;

	private PaymentProcessingResult verificationResult;

	@Override
	protected void populateGatewayMerchantInfo(GatewayMerchantInfo gatewayInfo, PaymentRequest payment) {
		RazorPayMerchantInfo razorpayGatewayInfo = (RazorPayMerchantInfo) gatewayInfo;
		String razorOrderId = createRazorPayOrderId(razorpayGatewayInfo, payment);
		razorpayGatewayInfo.setOrder_id(razorOrderId);
		razorpayGatewayInfo.setAmount(CurrencyConverter.toSubUnit(payment.getAmount()).toString());
		razorpayGatewayInfo.appendOrderIdToCallBack(razorOrderId);
		razorpayGatewayInfo.setGatewayURL(null);
	}

	@Override
	protected Map<String, String> getGatewayFields(GatewayMerchantInfo gatewayInfo) {
		return super.convertToMap(gatewayInfo);
	}

	@Override
	protected PaymentProcessingResult verifyPgResponse(Payment payment, PgResponse pgResponse,
			GatewayMerchantInfo gatewayMerchantInfo) throws PaymentException {
		verificationResult = new PaymentProcessingResult();
		RazorpayPgResponse response = (RazorpayPgResponse) pgResponse;
		verificationResult.getExternalPaymentInfo().setMerchantTxnId(response.getRazorpay_order_id());
		verificationResult.getExternalPaymentInfo().setGatewayComment(response.getErrorDesc());
		verificationResult.getExternalPaymentInfo().setGatewayComment(response.getErrorDesc());
		if (pgResponse.getSuccess()) {
			verificationResult.setStatus(PaymentStatus.SUCCESS);
		} else {
			verificationResult.setStatus(PaymentStatus.FAILURE);
		}
		return verificationResult;
	}

	@Override
	protected PaymentProcessingResult trackPayment(Payment payment, GatewayMerchantInfo gatewayMerchantInfo)
			throws PaymentException {
		return verificationResult;
	}

	@Override
	protected RefundInfo getRefundInfo(GatewayMerchantInfo gatewayInfo, Payment payment) {
		return new RazorPayRefundInfo();
	}

    @Override
    protected RefundResult refund(Payment payment, RefundInfo refundInfo, GatewayMerchantInfo gatewayInfo) {
        Payment orgPayment = DbPayment.toDomainList(paymentService.findByRefId(payment.getRefId())).stream()
                .filter(p -> p.getType().equals(PaymentTransactionType.PAID_FOR_ORDER) && p.getPaymentMedium().isExternalPaymentMedium()
                        && p.getAdditionalInfo().getGateway().equals(GateWayType.RAZOR_PAY.name())).findFirst().get();
        RazorPayMerchantInfo razorPayInfo = (RazorPayMerchantInfo) gatewayInfo;
        Refund refund;
        try {
            RazorpayClient razorpayClient = getRazorPayClient(razorPayInfo.getKey(), razorPayInfo.getSecret_key());
            JSONObject refundRequest = new JSONObject();
            refundRequest.put("amount", CurrencyConverter.toSubUnit(payment.getAmount()).toString());
            refund = razorpayClient.Payments.refund(orgPayment.getAdditionalInfo().getExternalPayId(), refundRequest);
        } catch (RazorpayException ex) {
            log.error("Exception while refunding order for refId {}. Error: {}", payment.getRefId(), ex.getMessage());
            throw new CustomGeneralException(SystemError.RAZORPAY_REFUND);
        }
        log.info("RazorPay:: Response received from gateway for ref Id {} is {}", payment.getRefId(), refund.toString());
        LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(refund.toString())
                .key(payment.getRefId()).type("RazorPay Refund Response").logType("AirSupplierAPILogs").build()));
        return RefundResult.builder().isRefundSuccessful(true).merchantRefundId(refund.get("id")).build();
    }

	private String createRazorPayOrderId(RazorPayMerchantInfo razorPayInfo, PaymentRequest payment) {
		com.razorpay.Order razorOrder;
		try {
			RazorpayClient razorpayClient = getRazorPayClient(razorPayInfo.getKey(), razorPayInfo.getSecret_key());
			JSONObject orderRequest = new JSONObject();
			orderRequest.put("amount", CurrencyConverter.toSubUnit(payment.getAmount()));
			orderRequest.put("currency", razorPayInfo.getCurrency());
			orderRequest.put("receipt", payment.getRefId());
			orderRequest.put("payment_capture", true);
			razorOrder = razorpayClient.Orders.create(orderRequest);
		} catch (RazorpayException ex) {
			log.error("Exception while creating razorpay order for refId {}. Error: {}", payment.getRefId(),
					ex.getMessage());
			throw new CustomGeneralException(SystemError.RAZORPAY_ORDER);
		}
		// if (payment.getTransactionType().equals(PaymentTransactionType.TOPUP)) {
		// payment.setMerchantTxnId(razorOrder.get("id"));
		// }
		payment.setMerchantTxnId(razorOrder.get("id"));
		return razorOrder.get("id");
	}

	public RazorpayClient getRazorPayClient(String pk, String sk) throws RazorpayException {
		return new RazorpayClient(pk, sk);
	}

	@Override
	protected String getGatewayName() {
		return "RazorPay Gateway";
	}
}
