package com.tgs.services.pms.mapper;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentRequestToPaymentMapper {

	@Autowired
	private PaymentService paymentService;

	public List<Payment> toPayments(List<PaymentRequest> paymentRequestList) {
		List<Payment> payments = new ArrayList<>();
		for (PaymentRequest paymentRequest : paymentRequestList) {
			payments.add(toPayment(paymentRequest));
		}
		return payments;
	}

	public Payment toPayment(PaymentRequest paymentRequest) {
		Payment payment = null;
		ContextData contextData = SystemContextHolder.getContextData();
		if (paymentRequest.getAmount().compareTo(BigDecimal.ZERO) > 0
				|| paymentRequest.getTransactionType().forLimitChange() || paymentRequest.isAllowZeroAmount()) {
			String loggedInUserId = contextData.getEmulateOrLoggedInUserId();
			if (paymentRequest.getAdditionalInfo().getRuleId() == null) {
				paymentRequest.getAdditionalInfo().setRuleId(paymentRequest.getRuleId());
			}
			setPaymentFee(paymentRequest, loggedInUserId);
			payment = Payment.builder().amount(paymentRequest.getAmount())
					.markup(ObjectUtils.defaultIfNull(paymentRequest.getMarkup(), BigDecimal.ZERO))
					.paymentMedium(paymentRequest.getPaymentMedium()).payUserId(paymentRequest.getPayUserId())
					.partnerId(paymentRequest.getPartnerId())
					.loggedInUserId(ObjectUtils.defaultIfNull(paymentRequest.getLoggedInUserId(), loggedInUserId))
					.opType(PaymentUtils.getPaymentOpType(paymentRequest)).type(paymentRequest.getTransactionType())
					.refId(paymentRequest.getRefId()).amendmentId(paymentRequest.getAmendmentId())
					.product(paymentRequest.getProduct()).reason(paymentRequest.getReason())
					.merchantTxnId(paymentRequest.getMerchantTxnId()).tds(paymentRequest.getTds())
					.paymentFee(paymentRequest.getPaymentFee()).additionalInfo(paymentRequest.getAdditionalInfo())
					.considerNegativeBalance(paymentRequest.getConsiderNegativeBalance()).build();

			payment.getAdditionalInfo().setOriginalPaymentRefId(paymentRequest.getOriginalPaymentRefId());
			payment.getAdditionalInfo().setPointsType(paymentRequest.getAdditionalInfo().getPointsType());
			payment.getAdditionalInfo().setNoOfPoints(paymentRequest.getNoOfPoints());
			populateRefundMissingParameters(paymentRequest, payment);
			setWalletParams(paymentRequest, payment);
			payment.setProduct();
			payment.setRefId();
		}
		return payment;
	}

	private void setWalletParams(PaymentRequest paymentRequest, Payment payment) {
		if (paymentRequest instanceof WalletPaymentRequest) {
			WalletPaymentRequest walletPaymentRequest = (WalletPaymentRequest) paymentRequest;
			payment.setWalletNumber(walletPaymentRequest.getWalletNumber());
			payment.setWalletId(walletPaymentRequest.getWalletId());
		}
	}

	// Note: PaymentConfigurationRule should be passed
	public static void setPaymentFee(PaymentRequest paymentRequest, String loggedInUserId) {
		BigDecimal paymentFee;
		if (paymentRequest.getRuleId() != null && loggedInUserId != null
				&& paymentRequest.getPaymentFee().equals(BigDecimal.ZERO)) {
			paymentFee = PaymentUtils.getPaymentFee(SystemContextHolder.getContextData().getUser(),
					paymentRequest.getAmount(), paymentRequest.getRuleId(), null);
			paymentRequest.setAmount(paymentRequest.getAmount().add(paymentFee));
			paymentRequest.setPaymentFee(paymentFee);
		}
	}

	private void populateRefundMissingParameters(PaymentRequest paymentRequest, Payment payment) {
		PaymentTransactionType transactionType = paymentRequest.getTransactionType();
		if (PaymentTransactionType.REFUND.equals(transactionType)
				|| PaymentTransactionType.REVERSE.equals(transactionType)) {
			if (paymentRequest.getPaymentMedium().isExternalPaymentMedium()
					&& StringUtils.isNotBlank(paymentRequest.getRefId())) {
				Payment successfulPayment = null;
				List<Payment> payments = paymentService.search(PaymentFilter.builder().refId(paymentRequest.getRefId())
						.paymentMedium(paymentRequest.getPaymentMedium()).status(PaymentStatus.SUCCESS).build());
				if (CollectionUtils.isEmpty(payments)) {
					throw new PaymentException(SystemError.PAYMENT_NOT_FOUND);
				}
				successfulPayment = payments.get(0);

				/**
				 * Adjust refund amount in case of full refund. E.g. Abort Order.
				 */
				if (successfulPayment.getAmount().setScale(2, BigDecimal.ROUND_HALF_UP).equals(payment.getAmount())) {
					payment.setAmount(successfulPayment.getMaximumRefundableAmount());
				}
				payment.setMerchantTxnId(successfulPayment.getMerchantTxnId());
				payment.getAdditionalInfo().setRuleId(successfulPayment.getAdditionalInfo().getRuleId());
				payment.getAdditionalInfo().setGateway(successfulPayment.getAdditionalInfo().getGateway());
				payment.getAdditionalInfo().setPaymentDate(successfulPayment.getCreatedOn().toLocalDate());
			}
		}
	}
}
