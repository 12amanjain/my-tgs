package com.tgs.services.pms.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.CreditBillFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.spring.AirlineResponseProcessor;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.jparepository.CreditBillingService;
import com.tgs.services.pms.manager.CreditBillingManager;
import com.tgs.services.pms.manager.CreditLineManager;
import com.tgs.services.pms.restmodel.CreditBillResponse;
import com.tgs.services.pms.restmodel.CreditLineResponse;
import com.tgs.services.pms.restmodel.PaymentDetailResponse;
import com.tgs.services.pms.restmodel.UpdateBillRequest;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pms/v1/creditbills")
public class CreditBillController {

	@Autowired
	private CreditBillingService creditBillingService;

	@Autowired
	@Qualifier("CreditBillingManager")
	private CreditBillingManager billingManager;

	@Autowired
	private AuditsHandler auditHandler;

	@Autowired
	private FiltersValidator validator;

	@Autowired
	private CreditLineManager creditLineManager;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(validator);
	}

	@RequestMapping(value = "/get/{billNumber}", method = RequestMethod.GET)
	protected CreditBillResponse getByBillNumber(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("billNumber") String billNumber) throws Exception {
		CreditBill bill = creditBillingService.findBillByBillNumber(billNumber);
		return new CreditBillResponse(bill);
	}

	@RequestMapping(value = "/job/generate", method = RequestMethod.GET)
	protected CreditBillResponse generateBills(HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		List<CreditBill> creditBill = billingManager.generateBillsRecur();
		return new CreditBillResponse(creditBill);
	}

	@RequestMapping(value = "/job/lock", method = RequestMethod.GET)
	protected CreditLineResponse lock(@QueryParam("scanHours") Short scanHours) throws Exception {
		return new CreditLineResponse(creditLineManager.takeLockActions(scanHours));
	}

	@RequestMapping(value = "/job/bpn", method = RequestMethod.GET)
	protected BaseResponse billPendingNotification() {
		// billingManager.takeLockActions();
		return new BaseResponse();
	}

	@RequestMapping(value = "/job/recallextension", method = RequestMethod.GET)
	protected CreditLineResponse recallExtensions() {
		return new CreditLineResponse(billingManager.recallBoundedExtension());
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected CreditBillResponse search(@RequestBody @Valid CreditBillFilter filter, BindingResult result) {
		if (result.hasErrors()) {
			CreditBillResponse billResponse = new CreditBillResponse();
			billResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return billResponse;
		} else {
			List<CreditBill> bills = creditBillingService.search(filter);
			return new CreditBillResponse(bills);
		}
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	protected CreditBillResponse update(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid UpdateBillRequest updateBillRequest) throws Exception {
		CreditBill bill = billingManager.updateBill(updateBillRequest);
		return new CreditBillResponse(bill);
	}

	@CustomRequestMapping(responseProcessors = {AirlineResponseProcessor.class, UserIdResponseProcessor.class})
	@RequestMapping(value = "/getPayments/{billNumber}", method = RequestMethod.GET)
	protected PaymentDetailResponse getPayments(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("billNumber") String billNumber) {
		return new PaymentDetailResponse(billingManager.getBillPayments(billNumber));
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData,
				com.tgs.services.pms.dbmodel.DbCreditBill.class, "billNumber"));
		return auditResponse;
	}

	@RequestMapping(value = "/settledDateMigration", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse settledDateMigration(@RequestBody CreditBillFilter filter) {
		creditBillingService.settledDateMigration(filter);
		return new BaseResponse();
	}
}
