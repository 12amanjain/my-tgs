package com.tgs.services.pms.restcontroller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.validation.Valid;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.DepositType;
import com.tgs.services.pms.datamodel.FundTransferRequest;
import com.tgs.services.pms.datamodel.Method;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.DepositRequestService;
import com.tgs.services.pms.manager.FundHandler;
import com.tgs.services.pms.manager.FundsTransferHandler;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.services.pms.restmodel.PaymentResponse;
import com.tgs.services.pms.ruleengine.DepositRequestConfig;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
@RequestMapping("/pms/v1/funds")
public class FundsController {

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	private FundHandler fundsHandler;

	@Autowired
	private FundsTransferHandler fundsTransferHandler;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	private DepositRequestService depositRequestService;

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.PMS_SERVICE, includedRoles = {UserRole.ADMIN, UserRole.ACCOUNTS})
	protected PaymentResponse addFunds(@RequestBody @Validated(Method.Add.class) List<PaymentRequest> paymentRequests) {
		User user = SystemContextHolder.getContextData().getUser();
		if (paymentRequests.get(0).getPayUserId().equals(user.getUserId())) {
			throw new CustomGeneralException(SystemError.PERMISSION_DENIED);
		}
		User payUser = userService.getUserFromCache(paymentRequests.get(0).getPayUserId());
		if (payUser == null || !payUser.getRole().isDepositWalletAllowed()) {
			throw new CustomGeneralException(SystemError.FUNDTRANSFER_NOT_ALLOWED);
		}
		PaymentResponse paymentResponse = new PaymentResponse(new ArrayList<>());
		for (PaymentRequest paymentRequest : paymentRequests) {
			if (paymentRequest.getTransactionType() != null
					&& paymentRequest.getTransactionType().equals(PaymentTransactionType.REVERSE)) {
				fundsHandler.setReversePayment(paymentRequest);
				paymentResponse.getPaymentList()
						.addAll(paymentProcessor.process(Collections.singletonList(paymentRequest)));
			} else if (paymentRequest.getTransactionType() != null
					&& paymentRequest.getTransactionType().equals(PaymentTransactionType.DEPOSIT_INCENTIVE)) {
				fundsHandler.setDepositIncentivePayment(paymentRequest);
				paymentResponse.getPaymentList()
						.addAll(paymentProcessor.process(Collections.singletonList(paymentRequest)));
			} else {
				paymentRequest.setTransactionType(PaymentTransactionType.TOPUP);
				paymentRequest.setPaymentMedium(null);
				DepositRequest depositRequest = depositRequestService.fetchById(paymentRequest.getRefId());
				if (DepositRequestStatus.REJECTED.equals(depositRequest.getStatus())) {
					throw new CustomGeneralException(SystemError.DEPOSITREQUEST_NOT_UPDATABLE);
				}
				depositRequest.getAdditionalInfo().setRcNo(paymentRequest.getDepositAdditionalInfo().getRcNo());
				depositRequest.getAdditionalInfo().setDhNo(paymentRequest.getDepositAdditionalInfo().getDhNo());
				depositRequest.getAdditionalInfo()
						.setPaymentReceivedDate(paymentRequest.getDepositAdditionalInfo().getPaymentReceivedDate());
				evaluateFeeAndUpdateDRAndPR(depositRequest, paymentRequest);
				depositRequestService.save(depositRequest);
				paymentResponse.getPaymentList().addAll(paymentProcessor.process(Arrays.asList(paymentRequest)));
			}
		}
		return paymentResponse;
	}

	/**
	 * Deduct fees from amount being requested to process.
	 * 
	 * @param depositRequest
	 * @param paymentRequest
	 */
	private void evaluateFeeAndUpdateDRAndPR(DepositRequest depositRequest, PaymentRequest paymentRequest) {
		PaymentFact paymentFact = PaymentFact.createFact();
		paymentFact.generateFact(depositRequest);
		PaymentConfigurationRule dRConfigurationRule =
				PaymentConfigurationHelper.getPaymentRule(paymentFact, PaymentRuleType.DEPOSIT_REQUEST);
		BigDecimal paymentFee =
				(depositRequest.getPaymentFee() == null || depositRequest.getPaymentFee() == 0) ? BigDecimal.ZERO
						: new BigDecimal(depositRequest.getPaymentFee());
		if (dRConfigurationRule != null && paymentFee == BigDecimal.ZERO) {
			DepositRequestConfig dRConfig = (DepositRequestConfig) dRConfigurationRule.getOutput();
			paymentFee =
					PaymentUtils.getPaymentFee(paymentRequest.getAmount(), dRConfig.getPaymentFeeExpression(), null);
		}
		if (paymentRequest.getAmount().compareTo(paymentFee) <= 0) {
			log.error("Amount {} exceeds fee {} for reqId {}", paymentRequest.getAmount(), paymentFee,
					depositRequest.getReqId());
			throw new CustomGeneralException(SystemError.INVALID_DEPOSIT_REQUEST_AMOUNT);
		}
		depositRequest.setPaymentFee(paymentFee.doubleValue());
		String comments = paymentRequest.getAdditionalInfo().getComments();
		if (depositRequest.getType().equals(DepositType.CASH_DEPOSITED)
				|| depositRequest.getType().equals(DepositType.CASH_IN_BANK)) {
			comments = StringUtils.appendIfMissing("Cash Deposited Charges: " + String.valueOf(paymentFee),
					(StringUtils.isNotEmpty(comments) ? "; " + comments : null));

		}
		if (depositRequest.getType().equals(DepositType.PAYMENT_GATEWAY)) {
			paymentRequest.setPaymentFee(paymentFee);
			comments = StringUtils.appendIfMissing("Manual Payment Gateway Topup",
					(StringUtils.isNotEmpty(comments) ? "; " + comments : null));
		}
		paymentRequest.getAdditionalInfo().setComments(comments);
		paymentRequest.setAmount(paymentRequest.getAmount().subtract(paymentFee));
	}

	@RequestMapping(value = "/transfer", method = RequestMethod.POST)
	protected PaymentResponse transfer(@RequestBody @Valid FundTransferRequest transferRequest) {
		User user = SystemContextHolder.getContextData().getUser();
		if (transferRequest.getPayUserId().equals(user.getUserId())) {
			throw new CustomGeneralException(SystemError.PERMISSION_DENIED);
		}
		AreaRole expectedAreaRole = PaymentUtils.getAreaRoleConfig(transferRequest.getTransferOperation());
		List<AreaRole> areaRoles = Arrays.asList(expectedAreaRole);
		List<AreaRoleMapping> mappingList = userService.getAreaRoleMapByAreaRole(areaRoles);
		Map<UserRole, Set<AreaRole>> userRoleMap = AreaRoleMapping.getMapByUserRole(mappingList);
		if (MapUtils.isEmpty(userRoleMap)) {
			if (!UserRole.DISTRIBUTOR.equals(user.getRole()) && !UserRole.WHITELABEL_PARTNER.equals(user.getRole()))
				throw new CustomGeneralException(SystemError.FUNDTRANSFER_NOT_ALLOWED);
		} else if (userRoleMap.get(user.getRole()) == null
				|| !userRoleMap.get(user.getRole()).contains(expectedAreaRole)) {
			throw new CustomGeneralException(SystemError.FUNDTRANSFER_NOT_ALLOWED);
		}
		return new PaymentResponse(fundsTransferHandler.transfer(transferRequest));
	}

}
