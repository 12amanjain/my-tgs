package com.tgs.services.pms.restcontroller;

import com.tgs.filters.CreditBillFilter;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.MediumTransferRequest;
import com.tgs.services.pms.helper.PaymentHelper;
import com.tgs.services.pms.manager.CreditLineManager;
import com.tgs.services.pms.manager.FundsTransferHandler;
import com.tgs.services.pms.manager.InternalHandler;
import com.tgs.services.pms.restmodel.CreditBillResponse;
import com.tgs.services.pms.restmodel.CreditLineResponse;
import com.tgs.services.pms.restmodel.InternalResponse;
import com.tgs.services.pms.restmodel.PaymentResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.ws.rs.QueryParam;

@Slf4j
@RestController
@RequestMapping("/pms/v1/internal")
public class InternalController {

    @Autowired
    private FundsTransferHandler fundsTransferHandler;

    @Autowired
    private PaymentHelper paymentHelper;

    @Autowired
    private CreditLineManager lineManager;

    @Autowired
    private InternalHandler internalHandler;

    @RequestMapping(value = "/funds/mediumtransfer", method = RequestMethod.POST)
    protected PaymentResponse mediumtransfer(@RequestBody MediumTransferRequest mediumTransferRequest) {
        return new PaymentResponse(fundsTransferHandler.internalFundTransfer(mediumTransferRequest));
    }

    @RequestMapping(value = "/refreshcache/{userId}", method = RequestMethod.GET)
    protected BaseResponse refreshCache(@PathVariable("userId") String userId) {
        paymentHelper.refreshUserBalanceInfoInCache(userId);
        return new BaseResponse();
    }

    @RequestMapping(value = "/creditline/update", method = RequestMethod.POST)
    protected CreditLineResponse refreshCache(@RequestBody CreditLine creditLine) {
        return new CreditLineResponse(lineManager.updateCycle(creditLine));
    }

    @RequestMapping(value = "/credit/check", method = RequestMethod.GET)
    protected InternalResponse check() {
        return internalHandler.inconsistentOutstandingAndBillAmount();
    }

    @RequestMapping(value = "/bills/useridfix", method = RequestMethod.POST)
    protected CreditBillResponse refreshCache(@RequestBody CreditBillFilter billFilter) {
        return new CreditBillResponse(internalHandler.fixBills(billFilter));
    }

    @RequestMapping(value = "/bills/updatefix", method = RequestMethod.GET)
    protected PaymentResponse updateFix(@QueryParam("payref") String payref) {
        return new PaymentResponse(internalHandler.billUpdateFix(payref));
    }

}
