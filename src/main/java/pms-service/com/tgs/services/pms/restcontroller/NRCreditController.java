package com.tgs.services.pms.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.QueryParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.NRCreditFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.dbmodel.DbNRCredit;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.manager.NRCreditManager;
import com.tgs.services.pms.manager.paymentManagers.NRBillingManager;
import com.tgs.services.pms.restmodel.CreditBillResponse;
import com.tgs.services.pms.restmodel.NRCreditResponse;

@RestController
@RequestMapping("/pms/v1/nrcredit")
public class NRCreditController {

	@Autowired
	private NRCreditManager creditManager;

	@Autowired
	private NRCreditService creditService;

	@Autowired
	private AuditsHandler auditHandler;


	@Autowired
	@Qualifier("NRBillingManager")
	private NRBillingManager nrBillingManager;

	@RequestMapping(value = "/get/{creditId}", method = RequestMethod.GET)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected NRCreditResponse get(@PathVariable("creditId") String creditId) {
		return new NRCreditResponse(creditService.getByCreditId(creditId));
	}

	@RequestMapping(value = "/search", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected NRCreditResponse search(@RequestBody @Valid NRCreditFilter filter) throws Exception {
		return new NRCreditResponse(creditService.search(filter));
	}

	@RequestMapping(value = "/getexpiry", method = RequestMethod.GET)
	protected NRCreditResponse getExpiry() throws Exception {
		return new NRCreditResponse(creditManager.getExpiryTime(NRCredit.builder().build()));
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected NRCreditResponse save(@RequestBody @Valid NRCredit creditRequest) {
		return new NRCreditResponse(creditManager.save(creditRequest));
	}

	@RequestMapping(value = "/job/generatebills", method = RequestMethod.GET)
	protected CreditBillResponse generatebills(@QueryParam("scanHours") Short scanHours) throws Exception {
		return new CreditBillResponse(nrBillingManager.checkAndGenerateNRBills());
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbNRCredit.class, "creditId"));
		return auditResponse;
	}
}
