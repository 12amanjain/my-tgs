package com.tgs.services.pms.restcontroller;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import com.tgs.services.vms.restmodel.VoucherDiscountRequest;
import com.tgs.services.vms.restmodel.VoucherDiscountResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.ExternalPaymentFilter;
import com.tgs.filters.PaymentFilter;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.AirlineResponseProcessor;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.pms.datamodel.DIRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.helper.PaymentHelper;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.PaymentService;
import com.tgs.services.pms.manager.DIManager;
import com.tgs.services.pms.manager.ExternalPaymentManager;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.services.pms.manager.UserBalanceManager;
import com.tgs.services.pms.manager.UserPointsBulkUpdateManager;
import com.tgs.services.pms.restmodel.ExternalPaymentDetailResponse;
import com.tgs.services.pms.restmodel.ExternalPaymentResponse;
import com.tgs.services.pms.restmodel.ExternalPaymentStatusResponse;
import com.tgs.services.pms.restmodel.PaymentConfigurationRuleResponse;
import com.tgs.services.pms.restmodel.PaymentIdsResponse;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import com.tgs.services.pms.restmodel.PaymentResponse;
import com.tgs.services.pms.restmodel.RedeemPointsRequest;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import com.tgs.services.pms.restmodel.UploadUserPointsRequest;
import com.tgs.services.pms.restmodel.UserPaymentInfo;
import com.tgs.services.pms.restmodel.UserPaymentInfoRequest;
import com.tgs.services.pms.servicehandler.ExternalPaymentDetailHandler;
import com.tgs.services.pms.servicehandler.ExternalPaymentListHandler;
import com.tgs.services.pms.servicehandler.PaymentConfigurationRuleHandler;
import com.tgs.services.pms.servicehandler.PaymentModeHandler;
import com.tgs.services.pms.servicehandler.RedeemPointsHandler;
import com.tgs.services.pms.servicehandler.VoucherDiscountHandler;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/pms/v1")
@Slf4j
public class PaymentController {

	@Autowired
	private PaymentModeHandler paymentModeHandler;

	@Autowired
	private PaymentService paymentService;

	@Autowired
	private PaymentConfigurationRuleHandler ruleHandler;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@Autowired
	private DIManager diManager;

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private ExternalPaymentListHandler externalPaymentListHandler;

	@Autowired
	private ExternalPaymentDetailHandler externalPaymentDetailHandler;

	@Autowired
	private ExternalPaymentManager extPaymentManager;

	@Autowired
	private UserBalanceManager userBalanceManager;

	@Autowired
	private VoucherDiscountHandler voucherHandler;

	@Autowired
	private PaymentHelper paymentHelper;

	@Autowired
	private UserServiceCommunicator userComm;

	@Autowired
	private UserPointsBulkUpdateManager userPtsManager;

	@Autowired
	private RedeemPointsHandler redeemPointsHandler;

	@RequestMapping(value = "/payment-modes", method = RequestMethod.POST)
	protected PaymentModeResponse search(HttpServletRequest request, HttpServletResponse response,
			@RequestBody PaymentModeRequest modeRequest) throws Exception {
		return paymentModeHandler.getResponse(modeRequest);
	}

	@RequestMapping(value = "/pay", method = RequestMethod.POST)
	protected BaseResponse paymentGateway(HttpServletRequest request, HttpServletResponse response,
			@RequestBody PaymentRequest paymentRequest) {
		paymentRequest.setProduct(Product.WALLET_TOPUP);
		paymentRequest.setTransactionType(PaymentTransactionType.TOPUP);
		paymentRequest.setPaymentFee(BigDecimal.ZERO); // ignoring payment fee coming from UI as we will calculate it
														// using the ruleId later.
		paymentRequest.setOpType(PaymentOpType.DEBIT);
		if (paymentRequest.getPayUserId() == null) {
			paymentRequest.setPayUserId(SystemContextHolder.getContextData().getUser().getParentUserId());
		} else {
			User payUser = userService.getUserFromCache(paymentRequest.getPayUserId());
			paymentRequest.setPayUserId(payUser.getParentUserId());
		}
		List<Payment> payments = paymentProcessor.process(Arrays.asList(paymentRequest));
		if (CollectionUtils.isEmpty(payments)) {
			return null;
		}
		return new BaseResponse();
	}

	@RequestMapping(value = "/payments/process", method = RequestMethod.POST)
	protected PaymentIdsResponse processPayment(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Validated List<PaymentRequest> paymentRequests) {
		if (!CollectionUtils.isEmpty(paymentRequests)) {
			String refid = paymentRequests.get(0).getRefId();
			if (!StringUtils.isBlank(refid)) {
				Product pdt = Product.getProductFromId(refid);
				paymentRequests.forEach(paymentRequest -> paymentRequest.setProduct(pdt));
			}
		}
		return new PaymentIdsResponse(paymentProcessor.process(paymentRequests).stream().map(Payment::getPaymentRefId)
				.collect(Collectors.toList()));
	}

	// Used when payment medium is not defined, and is evaluated by system.
	@RequestMapping(value = "/payments/auto-process", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.MISC_PAYMENT)
	protected PaymentResponse miscellaneousPayments(@RequestBody @Validated PaymentRequest paymentRequest) {
		return new PaymentResponse(paymentProcessor.process(Arrays.asList(paymentRequest)));
	}

	@CustomRequestMapping(responseProcessors = {AirlineResponseProcessor.class, UserIdResponseProcessor.class})
	@RequestMapping(value = "/payments/fetch", method = RequestMethod.POST)
	protected PaymentResponse getPaymentDetails(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid PaymentFilter filter, BindingResult result) {
		filter.setPayUserIdIn(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), filter.getPayUserIdIn()));
		FiltersValidator validator =
				(FiltersValidator) SpringContext.getApplicationContext().getBean("filtersValidator");
		validator.validate(filter, result);
		if (result.hasErrors()) {
			PaymentResponse paymentResponse = new PaymentResponse();
			paymentResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return paymentResponse;
		} else {
			if (UserRole.DISTRIBUTOR.equals(SystemContextHolder.getContextData().getUser().getRole())) {
				List<String> payUserIds = filter.getPayUserIdIn();
				if (!CollectionUtils.isEmpty(payUserIds)) {
					// In all requests, there is only 1 payuserid
					payUserIds.forEach(payUserId -> {
						User user = userService.getUserFromCache(payUserId);
						if (user.getUserConf().getDistributorId() != null
								&& user.getUserConf().getMigrationDate() != null) {
							if (filter.getCreatedOnAfterDate() == null || filter.getCreatedOnAfterDate()
									.isBefore(user.getUserConf().getMigrationDate().toLocalDate())) {
								filter.setCreatedOnAfterDate(user.getUserConf().getMigrationDate().toLocalDate());
							}
						}
					});
				}
			}
			List<Payment> payments = paymentService.search(filter);
			if (BooleanUtils.isTrue(filter.getMerge())) {
				payments = PaymentUtils.mergePayment(payments);
			}
			return new PaymentResponse(payments);
		}
	}

	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/external-payment/fetch", method = RequestMethod.POST)
	protected ExternalPaymentResponse getExternalPaymentsList(@RequestBody ExternalPaymentFilter filter)
			throws Exception {
		externalPaymentListHandler.initData(filter, new ExternalPaymentResponse());
		return externalPaymentListHandler.getResponse();
	}

	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	@RequestMapping(value = "/external-payment/detail/{refId}", method = RequestMethod.POST)
	protected ExternalPaymentDetailResponse getExternalPaymentDetails(@PathVariable String refId) throws Exception {
		externalPaymentDetailHandler.initData(refId, new ExternalPaymentDetailResponse());
		return externalPaymentDetailHandler.getResponse();
	}

	@RequestMapping(value = "/job/external-payment/track", method = RequestMethod.POST)
	protected BaseResponse trackExternalPayments(@RequestBody ExternalPaymentFilter filter) throws Exception {
		extPaymentManager.trackExternalPayments(filter);
		return new BaseResponse();
	}

	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	@RequestMapping(value = "/external-payment/track", method = RequestMethod.GET)
	protected ExternalPaymentStatusResponse trackExternalPayment(@RequestParam("refId") String refId) throws Exception {
		ExternalPaymentStatusResponse response = new ExternalPaymentStatusResponse();
		response.setPaymentStatusInfo(extPaymentManager.trackExternalPayment(refId));
		return response;
	}

	@RequestMapping(value = "/save-rule", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected PaymentConfigurationRuleResponse savePaymentRules(HttpServletRequest request,
			HttpServletResponse response, @RequestBody @Valid PaymentConfigurationRule confRule) throws Exception {
		ruleHandler.initData(confRule, new PaymentConfigurationRuleResponse());
		return ruleHandler.getResponse();
	}

	@RequestMapping(value = "/get-payment-medium/{product}/{enabled}", method = RequestMethod.GET)
	protected PaymentConfigurationRuleResponse listPaymentMediums(HttpServletRequest request,
			HttpServletResponse response, @PathVariable("product") Product product,
			@PathVariable("enabled") Boolean enabled) throws Exception {
		return ruleHandler.getPaymentMediums(product, enabled);
	}

	@RequestMapping(value = "/delete-rule/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT)
	protected BaseResponse deletePaymentConfiguratorRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") String id) throws Exception {
		return ruleHandler.deletePaymentRule(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	protected BaseResponse disablePaymentRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") String id, @PathVariable("status") Boolean status) throws Exception {
		return ruleHandler.updatePaymentRuleStatus(id, status);
	}

	@RequestMapping(value = "/user-payment-info", method = RequestMethod.POST)
	protected UserPaymentInfo fetchUserPaymentInfo(HttpServletRequest request, HttpServletResponse response,
			@RequestBody UserPaymentInfoRequest paymentRequest) throws Exception {
		UserPaymentInfo res = UserPaymentInfo.builder().build();
		res.setBalanceSummary(userBalanceManager.getUserBalanceSummary(paymentRequest.getUserId()));
		return res;
	}

	@RequestMapping(value = "/processdi", method = RequestMethod.POST)
	protected PaymentResponse processDI(@RequestBody Map<String, DIRequest> diRequestMap) {
		return new PaymentResponse(diManager.evaluateDI(diRequestMap));
	}

	@CustomRequestMapping(responseProcessors = {AirlineResponseProcessor.class, UserIdResponseProcessor.class})
	@RequestMapping(value = "payments/fundtransfer/{toUserId}", method = RequestMethod.GET)
	protected PaymentResponse getp2pPayments(@PathVariable("toUserId") String toUserId) {
		PaymentFilter filter = PaymentFilter.builder()
				.loggedInUserIdIn(Collections.singletonList(SystemContextHolder.getContextData().getUser().getUserId()))
				.payUserIdIn(Collections.singletonList(toUserId)).build();
		List<Payment> agentPayments = paymentService.search(filter);
		List<Payment> distPayments = null;
		if (!CollectionUtils.isEmpty(agentPayments)) {
			distPayments = paymentService.search(PaymentFilter.builder()
					.refIdIn(agentPayments.stream().map(Payment::getRefId).collect(Collectors.toList())).build());
		}
		return new PaymentResponse(distPayments);
	}

	@RequestMapping(value = "/voucher-discount", method = RequestMethod.POST)
	protected VoucherDiscountResponse getVoucherDiscount(HttpServletRequest request, HttpServletResponse response,
			@RequestBody VoucherDiscountRequest voucherDiscountRequest) throws Exception {
		voucherHandler.initData(voucherDiscountRequest, new VoucherDiscountResponse());
		return voucherHandler.getResponse();
	}


	@RequestMapping(value = "job/fix-closingbalance", method = RequestMethod.POST)
	protected BaseResponse fixClosingBalance(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody PaymentFilter paymentFilter) throws Exception {
		List<Payment> payments = paymentService.search(paymentFilter);
		if (CollectionUtils.isNotEmpty(payments)) {
			List<String> payuserIds = payments.stream().filter(PaymentUtils.distinctByKey(Payment::getPayUserId))
					.map(Payment::getPayUserId).collect(Collectors.toList());
			List<User> users = userComm.getUsers(UserFilter.builder().userIds(payuserIds).build());
			if (CollectionUtils.isNotEmpty(users)) {
				log.error("Fixing closing balance for userids {}", payuserIds);
				for (User user : users) {
					paymentHelper.fixClosingBalance(user, paymentFilter);
				}
			}
		}
		return new BaseResponse();
	}

	@RequestMapping(value = "/upload-userpoints", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadUserPoints(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody UploadUserPointsRequest userPointsUploadRequest)
			throws Exception {
		return userPtsManager.bulkUpdateUserPoints(userPointsUploadRequest);
	}

	@RequestMapping(value = "/redeem-points", method = RequestMethod.POST)
	protected RedeemPointsResponse getPointsDiscount(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RedeemPointsRequest redeemPointsRequest) throws Exception {
		redeemPointsHandler.initData(redeemPointsRequest, new RedeemPointsResponse());
		return redeemPointsHandler.getResponse();
	}

	@RequestMapping(value = "job/expire-points", method = RequestMethod.POST)
	protected BaseResponse expireUserPoints(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody PaymentFilter paymentFilter) throws Exception {
		// Through jenkins jobs. add expiry-time and transaction type
		List<Payment> payments = paymentService.search(paymentFilter);
		List<PaymentRequest> paymentRequests = new ArrayList<>();
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(payments)) {
			payments.forEach(payment -> {
				PaymentRequest paymentRequest = BasePaymentUtils.createDebitPaymentRequest(payment);
				paymentRequests.add(paymentRequest);
			});
		}
		if (org.apache.commons.collections.CollectionUtils.isNotEmpty(paymentRequests))
			paymentProcessor.process(paymentRequests);
		return new BaseResponse();

	}
}
