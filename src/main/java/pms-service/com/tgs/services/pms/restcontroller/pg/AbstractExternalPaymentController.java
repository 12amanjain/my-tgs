package com.tgs.services.pms.restcontroller.pg;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.PaymentHeader;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.manager.ExternalPaymentFundGatewayHandler;
import com.tgs.services.pms.manager.PaymentMessagingClient;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractExternalPaymentController {

	@Autowired
	protected UserServiceCommunicator userService;

	@Autowired
	private OrderServiceCommunicator orderService;

	@Autowired
	private ExternalPaymentFundGatewayHandler epFundHandler;

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Getter
	@Autowired
	private PaymentMessagingClient paymentMessagingClient;

	@Transactional
	protected final void processPayments(List<Payment> payments, String refId, DepositRequest oldDepositRequest) {
		Product product = Product.getProductFromId(refId);
		if (!product.equals(Product.WALLET_TOPUP)) {
			orderService.processBooking(refId);
		} else if (CollectionUtils.isNotEmpty(payments)) {
			epFundHandler.processPayment(payments, oldDepositRequest);
		}
		/**
		 * else it is a case of failed payment
		 */
	}

	protected final void setLoggedInUserIfAbsent(String userId) {
		User user = SystemContextHolder.getContextData().getUser();
		if (user == null) {
			if (StringUtils.isBlank(userId)
					|| (user = userService.getUser(UserFilter.builder().userId(userId).build())) == null) {
				/**
				 * If userId is blank, it will pick any random user.
				 */
				throw new CustomGeneralException(SystemError.INVALID_USERID);
			}
			SystemContextHolder.getContextData().setUser(user);
		}
	}

	protected List<PaymentRequest> getPaymentEntries(String refId, String logData) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
				.set(CacheSetName.EXTERNAL_PAYMENTS.getName()).typeOfT(new TypeToken<List<PaymentRequest>>() {
				}.getType()).key(refId).build();
		List<PaymentRequest> result = cachingCommunicator.getBinValue(metaInfo, List.class, false, false, BinName.PAYMENTS.name());
		LogUtils.store(Arrays.asList(LogData.builder().generationTime(LocalDateTime.now()).logData(new Gson().toJson(logData))
				.key(result.get(0).getRefId()).logType("AirSupplierAPILogs").type("Payment Gateway Response")
				.build()));
		return result;
	}

	/**
	 * Redirects to success-url received in headers when initiating payment. Always sends success status in response
	 * even if redirection fails because an automated s2s call might retry until it receives success status.
	 * 
	 * @param refId
	 * @param response
	 * @throws IOException
	 */
	protected void redirect(String refId, HttpServletResponse response) throws IOException {
		try {
			CacheMetaInfo headerMetaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.GENERAL_PURPOSE.getName())
					.set(CacheSetName.EXTERNAL_PAYMENTS.getName()).key("header" + refId)
					.typeOfT(new TypeToken<List<PaymentHeader>>() {}.getType()).build();
			List<PaymentHeader> paymentHeaders =
					cachingCommunicator.get(headerMetaInfo, List.class, false, false, null, BinName.PAYMENTS.name())
							.get(BinName.PAYMENTS.name());
			response.sendRedirect(paymentHeaders.get(0).getSuccessurl());
		} catch (Exception e) {
			log.error("Failed to redirect for bookingId {} due to ", refId, e);
		}
		response.getWriter().println("OK");
	}
}
