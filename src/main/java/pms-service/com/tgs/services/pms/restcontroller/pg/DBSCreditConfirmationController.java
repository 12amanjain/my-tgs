package com.tgs.services.pms.restcontroller.pg;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.util.MimeType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.google.gson.Gson;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.datamodel.DBSConfiguration;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentReason;
import com.tgs.services.pms.datamodel.PaymentReason.PaymentReasonContainer;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.dbs.DBSBankMessage;
import com.tgs.services.pms.datamodel.pg.dbs.DBSBankMessageTxnInfo;
import com.tgs.services.pms.datamodel.pg.dbs.DBSVirtualAccountNoData;
import com.tgs.services.pms.manager.DBSVirtualAccountNoManager;
import com.tgs.services.pms.manager.dbs.BCPGPDecryptor;
import com.tgs.services.ums.datamodel.User;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class DBSCreditConfirmationController extends AbstractExternalPaymentController {

	private static final String ENDPOINT = "/pg/v1/dbs_call";

	@Autowired
	private BCPGPDecryptor decryptor;

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	@RequestMapping(value = ENDPOINT, method = RequestMethod.POST)
	public void processConfirmedPayment(HttpServletRequest request, HttpServletResponse response) {
		String json = null;
		Payment payment = null;
		try {
			String msg = IOUtils.toString(request.getInputStream(), getCharsetName(request));
			log.info("Message from DBS:\n{}", msg);

			DBSConfiguration dbsConfiguration = null;
			ClientGeneralInfo clientGeneralInfo =
					(ClientGeneralInfo) gsCommunicator.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
			for (DBSConfiguration config : clientGeneralInfo.getExternalPaymentNotificationConfiguration()
					.getDbsConfigurations()) {
				if (ENDPOINT.equals(config.getEndPoint())) {
					dbsConfiguration = config;
				}
			}

			setLoggedInUserIfAbsent(dbsConfiguration.getUserId());

			json = decryptor.decryptandVerify(msg, dbsConfiguration.getPrivateKey(), dbsConfiguration.getPublicKey(),
					StringUtils.defaultString(dbsConfiguration.getPassPhrase()));
			log.info("Message decrypted from DBS:\n{}", json);

			DBSBankMessage dbsBankMessage = new Gson().fromJson(json, DBSBankMessage.class);

			String virtualAccountNo = dbsBankMessage.getTxnInfo().getReceivingParty().getVirtualAccountNo();
			DBSVirtualAccountNoData accountNoData = DBSVirtualAccountNoManager.getData(virtualAccountNo);
			User payUser = userService.getUserFromCache(accountNoData.getUserId());
			payment = buildPayment(dbsBankMessage, payUser);
			validateAccountNumber(virtualAccountNo, payUser);
			processPayments(Arrays.asList(payment), payment.getRefId(), DepositRequest.builder().bank("DBS").build());
		} catch (Exception e) {
			/**
			 * If json is null, either configuration was wrong or message couldn't be read properly.
			 */
			log.error("Error occured while processing DBS call {} :", json, e);

			/**
			 * If payment was not null, payment processing failed.
			 */
			if (payment != null) {
				PaymentReasonContainer addnlReasonsContainer = new PaymentReasonContainer();
				addnlReasonsContainer.addReason(PaymentReason.PROCESSED, "No");
				addnlReasonsContainer.addReason(PaymentReason.FAILURE_REASON, e.getMessage());
				payment.setReason(addnlReasonsContainer.appendReasonsTo(payment.getReason()));
			}
			getPaymentMessagingClient().sendPaymentEmail(payment);
		}
	}

	private PaymentReasonContainer getReasons(Payment payment) {
		PaymentReasonContainer reasonContainer = new PaymentReasonContainer();
		reasonContainer.addReason(PaymentReason.MERCHANT_TXN_ID, payment.getMerchantTxnId());
		reasonContainer.addReason(PaymentReason.PAID_USING, "DBS");
		return reasonContainer;
	}

	private String getCharsetName(HttpServletRequest request) {
		String contentType = request.getHeader(HttpHeaders.CONTENT_TYPE);
		if (StringUtils.isNotBlank(contentType)) {
			MimeType mimetype = MimeType.valueOf(contentType);
			if (mimetype != null) {
				Charset charset = mimetype.getCharset();
				if (charset != null) {
					return charset.name();
				}
			}
		}
		return null;
	}

	private Payment buildPayment(DBSBankMessage dbsBankMessage, User payUser) {
		DBSBankMessageTxnInfo txnInfo = dbsBankMessage.getTxnInfo();
		double amount = txnInfo.getAmtDtls().getTxnAmt();
		String merchantTxnId = txnInfo.getTxnRefId();
		String refId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build());
		Payment payment = Payment.builder().payUserId(payUser.getUserId()).refId(refId)
				.amount(BigDecimal.valueOf(amount)).merchantTxnId(merchantTxnId).status(PaymentStatus.SUCCESS)
				.partnerId(payUser != null ? payUser.getPartnerId() : UserUtils.DEFAULT_PARTNERID).type(PaymentTransactionType.TOPUP).build();
		payment.setReason(getReasons(payment).toString());
		return payment;
	}

	private void validateAccountNumber(String virtualAccountNo, User payUser) {
		boolean isValid = false;
		if (payUser != null) {
			log.info("Received DBS payment for accountNumber {} , userId {}", virtualAccountNo, payUser.getUserId());
			if (payUser.getRole().isDepositWalletAllowed()) {
				for (BankAccountInfo accountInfo : payUser.getAdditionalInfo().getDepositBankAccounts()) {
					if (accountInfo.getAccountNumber().toUpperCase().equals(virtualAccountNo)) {
						isValid = true;
					}
				}
			}
		}

		if (!isValid) {
			throw new RuntimeException("Mentioned accountnumber " + virtualAccountNo + " is not available in database");
		}
	}
}
