package com.tgs.services.pms.restcontroller.pg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.credimax.CredimaxPgResponse;
import com.tgs.services.pms.datamodel.pg.hdfcCcavenue.HdfcCcavenuePgResponse;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.PaymentException;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class HdfcCcavenueGatewayController extends AbstractExternalPaymentController {

	@Autowired
	private PaymentProcessor paymentProcessor;

	@RequestMapping("/pg/v1/response_callback/hdfc_ccavenue/{refId}")
	public void responseCallBack(Model model, HttpServletRequest request, HttpServletResponse response,
			@PathVariable("refId") String refId) throws IOException {
		try {
			HdfcCcavenuePgResponse pgResponse = GsonUtils.getGson()
					.fromJson(GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request)), HdfcCcavenuePgResponse.class);
			SystemContextHolder.getContextData().setMetaInfo(pgResponse);
			String responseStr = GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request));
			log.info("Response received from payment-gateway is {}", responseStr);
			if (refId == null)
				return;
			pgResponse.setRefId(refId);
			List<PaymentRequest> result = getPaymentEntries(refId, responseStr);
			setLoggedInUserIfAbsent(result.get(0).getPayUserId());
			List<Payment> outPayments = new ArrayList<>();
			try {
				outPayments.addAll(paymentProcessor.process(result));
			} catch (PaymentException pe) {
				log.info("Payment failed for {}", pe.getMessage());
			}
			processPayments(outPayments, pgResponse.getRefId(), DepositRequest.builder().bank("Hdfc CCAvenue").build());
		} catch (Exception e) {
			log.error("Unable to process payment for bookingId {}", refId, e);
		}
		redirect(refId, response);
	}

}
