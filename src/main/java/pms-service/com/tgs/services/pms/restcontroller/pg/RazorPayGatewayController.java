package com.tgs.services.pms.restcontroller.pg;

import com.google.gson.Gson;
import com.razorpay.Order;
import com.razorpay.RazorpayClient;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.*;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.RazorPayMerchantInfo;
import com.tgs.services.pms.datamodel.pg.RazorpayPgResponse;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.services.pms.manager.razorpay.RazorpayGatewayManager;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
public class RazorPayGatewayController extends AbstractExternalPaymentController {

    private static final String HMAC_SHA256_ALGORITHM = "HmacSHA256";

    @Autowired
    private RazorpayGatewayManager razorpayGatewayManager;

    @Autowired
    private PaymentProcessor paymentProcessor;

    @SuppressWarnings({"unchecked", "serial"})
    @RequestMapping("/pg/v1/response_callback/razorpay/{orderId2}")
    public void responseCallBack(Model model, HttpServletRequest request, HttpServletResponse response
            , @PathVariable("orderId2") String orderId) throws IOException {
        String reqParams = GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request));
        List<Payment> outPayments = new ArrayList<>();
        log.info("Razorpay:: Request Params {}, OrderId {}", reqParams, orderId);
        RazorpayPgResponse pgResponse = RazorpayPgResponse.builder()
                .errorCode(request.getParameter("error[code]"))
                .errorDesc(request.getParameter("error[description]"))
                .razorpay_order_id(request.getParameter("razorpay_order_id"))
                .razorpay_payment_id(request.getParameter("razorpay_payment_id"))
                .razorpay_signature(request.getParameter("razorpay_signature"))
                .build();
        log.info("Razorpay:: Call back received. Response=> {}", new Gson().toJson(pgResponse));
        try {
            pgResponse.setRazorpay_order_id(orderId);
            RazorPayMerchantInfo merchantInfo = (RazorPayMerchantInfo) getMerchantInfo();
            SystemContextHolder.getContextData().setMetaInfo(pgResponse);
            RazorpayClient rp = razorpayGatewayManager.getRazorPayClient(merchantInfo.getKey(), merchantInfo.getSecret_key());
            Order order = rp.Orders.fetch(pgResponse.getRazorpay_order_id());
            pgResponse.setRefId(order.get("receipt"));
            List<PaymentRequest> result = getPaymentEntries(pgResponse.getRefId(), GsonUtils.getGson().toJson(pgResponse));
            pgResponse.setGateWayType(GateWayType.RAZOR_PAY);
            if (pgResponse.getRazorpay_payment_id() != null) {          // razorpay success
                persistPgData(pgResponse, result.get(0));
                String calculatedHMAC = calculateRFC2104HMAC(pgResponse.getRazorpay_order_id() + "|"
                        + pgResponse.getRazorpay_payment_id(), merchantInfo.getSecret_key());
                if (calculatedHMAC.equals(pgResponse.getRazorpay_signature())) {
                    log.info("Razorpay:: Signature Verified");
                    pgResponse.setSuccess(true);
                } else {
                    pgResponse.setSuccess(false);
                    pgResponse.setErrorDesc("Signature not verified!");
                }
            } else pgResponse.setSuccess(false);
            setLoggedInUserIfAbsent(result.get(0).getPayUserId());
            outPayments.addAll(paymentProcessor.process(result));
        } catch (Exception e) {
            log.error("Exception while processing RazorPay PGResponse. RZP_OrderId: {}, TGS_RefId: {}",
                    pgResponse.getRazorpay_order_id(), pgResponse.getRefId(), e);
        }
        processPayments(outPayments, pgResponse.getRefId(), DepositRequest.builder().bank(pgResponse.getGateWayType().name()).build());
        redirect(pgResponse.getRefId(), response);
    }

    private void persistPgData(RazorpayPgResponse pgResponse, PaymentRequest paymentRequest) {
        paymentRequest.getAdditionalInfo().setExternalPayId(pgResponse.getRazorpay_payment_id());
        paymentRequest.getAdditionalInfo().setExternalSignature(pgResponse.getRazorpay_signature());
    }

    private static GatewayMerchantInfo getMerchantInfo() {
        List<PaymentConfigurationRule> rules = PaymentConfigurationHelper.getRuleOnRuleType(PaymentRuleType.PAYMENT_MEDIUM);
        for (PaymentConfigurationRule rule : rules) {
            if (rule.getMedium().isExternalPaymentMedium() && ((PaymentConfigOutput) rule.getOutput()).getGatewayType().equals(GateWayType.RAZOR_PAY))
                return ((PaymentConfigOutput) rule.getOutput()).getGateWayInfo();
        }
        return null;
    }

    /**
     * Computes RFC 2104-compliant HMAC signature.
     * @param data
     * The data to be signed.
     * @param key
     * The signing key.
     * @return
     * The Base64-encoded RFC 2104-compliant HMAC signature.
     * @throws
     * java.security.SignatureException when signature generation fails
     */
    private static String calculateRFC2104HMAC(String data, String key) throws java.security.SignatureException {
        String result;
        try {
            // get an hmac_sha256 key from the raw secret bytes
            SecretKeySpec signingKey = new SecretKeySpec(key.getBytes(), HMAC_SHA256_ALGORITHM);
            // get an hmac_sha256 Mac instance and initialize with the signing key
            Mac mac = Mac.getInstance(HMAC_SHA256_ALGORITHM);
            mac.init(signingKey);
            // compute the hmac on input data bytes
            byte[] rawHmac = mac.doFinal(data.getBytes());
            // base64-encode the hmac
            result = DatatypeConverter.printHexBinary(rawHmac).toLowerCase();
        } catch (Exception e) {
            throw new SignatureException("Failed to generate HMAC : " + e.getMessage());
        }
        return result;
    }
}