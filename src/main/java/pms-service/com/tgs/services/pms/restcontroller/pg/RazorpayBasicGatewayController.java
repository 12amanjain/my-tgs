package com.tgs.services.pms.restcontroller.pg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.reflect.TypeToken;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.pg.GateWayType;
import com.tgs.services.pms.datamodel.pg.razorpaybasic.RazorpayBasicPgResponse;
import com.tgs.services.pms.manager.PaymentProcessor;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.PaymentException;

import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class RazorpayBasicGatewayController extends AbstractExternalPaymentController {

	@Autowired
	UserServiceCommunicator userService;

	@Autowired
	private PaymentProcessor paymentProcessor;

	@RequestMapping("/pg/v1/response_callback/razorpaybasic/{refId}")
	public void responseCallBack(Model model, HttpServletRequest request, HttpServletResponse response,
			@PathVariable("refId") String refId) throws IOException {
		try {
			String responseStr = GsonUtils.getGson().toJson(HttpUtils.getRequestParams(request));
			Map<String, String> map = GsonUtils.getGson().fromJson(responseStr, new TypeToken<Map<String, String>>() {
			}.getType());
			RazorpayBasicPgResponse pgResponse = RazorpayBasicPgResponse.builder().errorCode(map.get("error[code]"))
					.errorDesc(map.get("error[description]")).razorpay_order_id(map.get("razorpay_order_id"))
					.razorpay_payment_id(map.get("razorpay_payment_id"))
					.razorpay_signature(map.get("razorpay_signature")).errorMetaData(map.get("error[metadata]"))
					.errorReason(map.get("error[reason]")).build();
			pgResponse.setGateWayType(GateWayType.RAZOR_PAY_BASIC);
			log.info("Response received from payment-gateway is {}", responseStr);
			pgResponse.setRefId(refId);
			SystemContextHolder.getContextData().setMetaInfo(pgResponse);
			List<PaymentRequest> result = getPaymentEntries(pgResponse.getRefId(), responseStr);
			setLoggedInUserIfAbsent(result.get(0).getPayUserId());
			List<Payment> outPayments = new ArrayList<>();
			try {
				outPayments.addAll(paymentProcessor.process(result));
			} catch (PaymentException pe) {
				log.info("Payment failed for {}", pe.getMessage());
			}
			processPayments(outPayments, pgResponse.getRefId(), DepositRequest.builder().bank("Razorpay").build());
		} catch (Exception e) {
			log.error("Unable to process payment for bookingId {}", refId, e);
		}
		redirect(refId, response);
	}
}
