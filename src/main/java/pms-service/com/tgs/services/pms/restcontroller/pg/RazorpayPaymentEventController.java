package com.tgs.services.pms.restcontroller.pg;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.google.gson.Gson;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.datamodel.RazorpayConfiguration;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentReason;
import com.tgs.services.pms.datamodel.PaymentReason.PaymentReasonContainer;
import com.tgs.services.pms.datamodel.PaymentStatus;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import com.tgs.services.pms.datamodel.pg.razorpay.RazorpayMessage;
import com.tgs.services.pms.datamodel.pg.razorpay.RazorpayPaymentEntity;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class RazorpayPaymentEventController extends AbstractExternalPaymentController {

	@Autowired
	private GeneralServiceCommunicator gsCommunicator;

	@Autowired
	private UserServiceCommunicator userServiceComm;

	@RequestMapping(value = "/pg/v1/razorpay_call/credit", method = RequestMethod.POST)
	public void processConfirmedPayment(HttpServletRequest request, HttpServletResponse response,
			@RequestBody RazorpayMessage message) {
		try {
			log.info("Message from Razorpay:\n{}", new Gson().toJson(message));

			RazorpayConfiguration razorpayConfiguration = null;
			ClientGeneralInfo clientGeneralInfo =
					(ClientGeneralInfo) gsCommunicator.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
			List<RazorpayConfiguration> configurations =
					clientGeneralInfo.getExternalPaymentNotificationConfiguration().getRazorpayConfigurations();
			for (RazorpayConfiguration config : configurations) {
				if (message.getAccount_id().equals(config.getRazorpayId())) {
					razorpayConfiguration = config;
					break;
				}
			}

			setLoggedInUserIfAbsent(razorpayConfiguration.getUserId());

			Payment payment = buildPayment(message, razorpayConfiguration);
			payment.setReason(getReasons(payment).getCombinedReasons());
			processPayments(Arrays.asList(payment), payment.getRefId(), DepositRequest.builder().bank("Razorpay").build());
		} catch (Exception e) {
			log.error("Error occured while processing Razorpay call {} :", message, e);
		}
	}

	private PaymentReasonContainer getReasons(Payment payment) {
		PaymentReasonContainer reasonContainer = new PaymentReasonContainer();
		reasonContainer.addReason(PaymentReason.MERCHANT_TXN_ID, payment.getMerchantTxnId());
		reasonContainer.addReason(PaymentReason.PAYMENT_FEE, payment.getPaymentFee());
		reasonContainer.addReason(PaymentReason.PAID_USING, "Razorpay");
		return reasonContainer;
	}

	private Payment buildPayment(RazorpayMessage message, RazorpayConfiguration razorpayConfiguration) {
		RazorpayPaymentEntity paymentEntity = message.getPayload().getPayment().getEntity();
		String mobile = StringUtils.right(paymentEntity.getContact(), 10);
		List<User> users =
				userServiceComm.getUsers(UserFilter.builder().mobile(mobile).email(paymentEntity.getEmail()).build());
		// How many receivers?
		User user = findUserWithAccountNo(users,
				message.getPayload().getVirtual_account().getEntity().getReceivers().get(0).getAccount_number());

		/**
		 * All amounts are in paise.
		 */
		double amount = paymentEntity.getAmount() / 100;
		double paymentFee = 0;
		if (BooleanUtils.isTrue(razorpayConfiguration.getNeedToDeductPaymentFee())) {
			paymentFee = (paymentEntity.getTax() + paymentEntity.getFee()) / 100;
		}
		String merchantTxnId = message.getPayload().getPayment().getEntity().getId();
		String refId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build());

		return Payment.builder().payUserId(user.getUserId()).partnerId(user.getPartnerId())
				.paymentFee(BigDecimal.valueOf(paymentFee)).refId(refId).amount(BigDecimal.valueOf(amount))
				.merchantTxnId(merchantTxnId).status(PaymentStatus.SUCCESS).type(PaymentTransactionType.TOPUP).build();
	}

	private User findUserWithAccountNo(List<User> users, String accountNo) {
		if (CollectionUtils.isEmpty(users)) {
			return null;
		}

		for (User user : users) {
			if (CollectionUtils.isNotEmpty(user.getAdditionalInfo().getDepositBankAccounts())) {
				for (BankAccountInfo config : user.getAdditionalInfo().getDepositBankAccounts()) {
					if (accountNo.equals(config.getAccountNumber())) {
						return user;
					}
				}
			}
		}

		return null;
	}
}
