package com.tgs.services.pms.restcontroller.requestValidator;

import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.restmodel.SaveCreditLineRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

@Component
public class CreditLineValidator implements Validator {

    private boolean isForEdit;

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    @Override
    public void validate(Object target, Errors errors) {

        if (target instanceof SaveCreditLineRequest) {



            SaveCreditLineRequest request = (SaveCreditLineRequest) target;

            isForEdit = StringUtils.isNotEmpty(request.getCreditLine().getCreditNumber());
        }
    }

    private void registerError(Errors errors, String field, SystemError systemError) {
        errors.rejectValue(field, systemError.errorCode(), systemError.getMessage());
    }
}
