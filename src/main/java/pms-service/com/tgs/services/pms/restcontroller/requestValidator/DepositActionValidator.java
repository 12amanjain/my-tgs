package com.tgs.services.pms.restcontroller.requestValidator;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.pms.datamodel.DepositAction;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;

@Service
public class DepositActionValidator {

	@Autowired
	private UserServiceCommunicator userService;

	public Set<DepositAction> validActions(User loggedInUser, DepositRequest depositRequest) {

		List<AreaRoleMapping> areaRoleMappingList =
				userService.getAreaRoleMapByAreaRole(ApplicationArea.DEPOSIT_REQUEST.getAreaRoles());

		if (CollectionUtils.isEmpty(areaRoleMappingList)) {
			return new HashSet<>();
		}

		Map<UserRole, Set<AreaRole>> map = AreaRoleMapping.getMapByUserRole(areaRoleMappingList);

		UserRole userRole = loggedInUser.getRole();
		Set<DepositAction> actions = new HashSet<>();

		boolean depositingUser = loggedInUser.getUserId().equals(depositRequest.getUserId());
		boolean assignedUser = loggedInUser.getUserId().equals(depositRequest.getAdditionalInfo().getAssignedUserId());
		boolean admin = false;
		boolean processor = false;

		if (map.containsKey(userRole)) {
			admin = map.get(userRole).contains(AreaRole.DEPOSIT_ADMIN);
			processor = map.get(userRole).contains(AreaRole.DEPOSIT_PROCESSOR);
		}

		switch (depositRequest.getStatus()) {

			case SUBMITTED:
				if (depositingUser) {
					actions.add(DepositAction.ABORT);
				}

			case PROCESSING:
			case PAYMENT_NOT_RECEIVED:
			case PAYMENT_RECEIVED:
				if (assignedUser) {
					actions.add(DepositAction.ABORT);
					actions.add(DepositAction.PROCESS);
					actions.add(DepositAction.UPDATE);
				}
				if (admin) {
					actions.add(DepositAction.ASSIGN);
					if (!assignedUser) {
						actions.add(DepositAction.ASSIGNME);
					}
				} else if (processor) {
					actions.add(DepositAction.ASSIGN);
					actions.add(DepositAction.ASSIGNME);
				}

			case ABORTED:
				break;

			case ACCEPTED:
				break;

			case REJECTED:
				break;

			default:
				break;

		}
		return actions;
	}

}
