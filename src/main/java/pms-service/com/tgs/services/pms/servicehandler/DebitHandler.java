package com.tgs.services.pms.servicehandler;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.user.BasePaymentUtils;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditLinePaymentMode;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.NRCreditPaymentMode;
import com.tgs.services.pms.datamodel.PaymentMode;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class DebitHandler {

	@Autowired
	private PaymentModeHandler paymentModeHandler;

	@Autowired
	private UserWalletService walletService;

	public List<PaymentRequest> evaluatePaymentMediums(WalletPaymentRequest request) {
		List<PaymentRequest> paymentRequestList = new ArrayList<>();
		if (request.getAmount().compareTo(BigDecimal.ZERO) == 0) {
			log.info("Debit Handler: Zero amount received!");
			request.setPaymentMedium(PaymentMedium.WALLET);
			paymentRequestList.add(request);
			return paymentRequestList;
		}
		PaymentModeResponse modeResponse = paymentModeHandler
				.getResponse(PaymentModeRequest.builder().amount(request.getAmount()).payUserId(request.getPayUserId())
						.product(request.getProduct()).bookingId(request.getRefId())
						.onlyEnabledModes(!request.getAdditionalInfo().isAllowForDisabledMediums()).build());
		modeResponse.getPaymentModes().sort(Comparator.comparing(PaymentMode::getPriority));
		BigDecimal remaining = new BigDecimal(request.getAmount().doubleValue());
		Pair<List<PaymentRequest>, BigDecimal> pair;
		//If commission is being changed (lets say 300) and wallet blc is 100. so after the loop exits, while checking for negative blc,
		//again we will fetch available blc , and instead of considering availBlc 0, we will consider this 100!!
		BigDecimal amtAlreadyDeducted = BigDecimal.ZERO;
		for (PaymentMode mode : modeResponse.getPaymentModes()) {
			if (remaining.compareTo(BigDecimal.ZERO) <= 0)
				break;
			if (mode.getMode().equals(PaymentMedium.WALLET)) {
				pair = payWithWallet(request, remaining,false, amtAlreadyDeducted);
				amtAlreadyDeducted = remaining.subtract(pair.getRight());
				remaining = pair.getRight();
				paymentRequestList.addAll(pair.getLeft());
				continue;
			}
			if (mode.getMode().equals(PaymentMedium.CREDIT_LINE)) {
				pair = payWithCreditLine((CreditLinePaymentMode) mode, request, remaining);
				remaining = pair.getRight();
				paymentRequestList.addAll(pair.getLeft());
				continue;
			}
			if (mode.getMode().equals(PaymentMedium.CREDIT)) {
				pair = payWithNRCredit((NRCreditPaymentMode) mode, request, remaining);
				remaining = pair.getRight();
				paymentRequestList.addAll(pair.getLeft());
			}
		}

		/**
		 * In case remaining amount is less than minBalanceAmount then remaining balance
		 * can't be deduced upto minBalance amount from wallet
		 */
		if (remaining.compareTo(BigDecimal.ZERO) > 0) {
			// If some amount has already been deducted from any of the payment mediums,
			// then no need to deduct tds again
			if (remaining.compareTo(request.getAmount()) < 0)
				request.setTds(BigDecimal.ZERO);
			//If some amount was already deducted in the same transaction (in the loop above), then we need to consider that for this calculation
			pair = payWithWallet(request, remaining, true,amtAlreadyDeducted);
			remaining = pair.getRight();
			paymentRequestList.addAll(pair.getLeft());

		}
		if (remaining.compareTo(BigDecimal.ZERO) > 0) {
			log.debug("Remaing balance is {}" , remaining);
			throw new CustomGeneralException(SystemError.INSUFFICIENT_BALANCE);
		}
		return paymentRequestList;
	}

	private Pair<List<PaymentRequest>, BigDecimal> payWithWallet(PaymentRequest request, BigDecimal remaining,
			boolean minBalanceToBeConsider,BigDecimal amtDeductedInSameTransaction) {
		List<PaymentRequest> list = new ArrayList<>();
		UserWallet walletBalance = walletService.findByUserId(request.getPayUserId()).toDomain();
		BigDecimal availBal = walletBalance.getBalance();		
		BigDecimal balToBeConsidered = availBal.subtract(amtDeductedInSameTransaction);
		BigDecimal minBalanceAllowed = BigDecimal.ZERO;
		if (minBalanceToBeConsider) {
			minBalanceAllowed = BigDecimal
					.valueOf(BasePaymentUtils.getMinBalanceAllowed(request.getOpType(), request.getTransactionType()));
			if (balToBeConsidered.compareTo(remaining) <= 0)
				balToBeConsidered = balToBeConsidered.subtract(minBalanceAllowed);
		}
		BigDecimal transactionAmount = balToBeConsidered.min(remaining);
		if (transactionAmount.compareTo(minBalanceAllowed) <= 0) {
			return Pair.of(list, remaining);
		}
		remaining = remaining.subtract(transactionAmount);
		WalletPaymentRequest walletPR = new WalletPaymentRequest(request);
		walletPR.setAmount(transactionAmount);
		walletPR.setPaymentMedium(PaymentMedium.WALLET);
		list.add(walletPR);
		log.debug("Remaining blc {} , balToBeConsidered {} and wallet request amount {} ", remaining, balToBeConsidered,
				transactionAmount);
		return Pair.of(list, remaining);
	}

	private Pair<List<PaymentRequest>, BigDecimal> payWithCreditLine(CreditLinePaymentMode creditLinePaymentMode,
			WalletPaymentRequest request, BigDecimal remaining) {
		List<PaymentRequest> list = new ArrayList<>();
		for (CreditLine cl : creditLinePaymentMode.getCreditLines()) {
			BigDecimal transactionAmount = cl.getBalance().min(remaining);
			if (transactionAmount.compareTo(BigDecimal.ZERO) <= 0) {
				return Pair.of(list, remaining);
			}
			remaining = remaining.subtract(transactionAmount);
			WalletPaymentRequest creditlinePR = new WalletPaymentRequest(request);
			creditlinePR.setAmount(transactionAmount);
			creditlinePR.setPaymentMedium(PaymentMedium.CREDIT_LINE);
			creditlinePR.setWalletNumber(cl.getCreditNumber());
			creditlinePR.setWalletId(cl.getId());
			list.add(creditlinePR);
		}
		return Pair.of(list, remaining);
	}

	private Pair<List<PaymentRequest>, BigDecimal> payWithNRCredit(NRCreditPaymentMode nrCreditPaymentMode,
			WalletPaymentRequest request, BigDecimal remaining) {
		List<PaymentRequest> list = new ArrayList<>();
		for (NRCredit credit : nrCreditPaymentMode.getNrCreditList()) {
			BigDecimal transactionAmount = credit.getBalance().min(remaining);
			if (transactionAmount.compareTo(BigDecimal.ZERO) <= 0) {
				return Pair.of(list, remaining);
			}
			remaining = remaining.subtract(transactionAmount);
			WalletPaymentRequest NRCreditPR = new WalletPaymentRequest(request);
			NRCreditPR.setAmount(transactionAmount);
			NRCreditPR.setPaymentMedium(PaymentMedium.CREDIT);
			NRCreditPR.setWalletNumber(credit.getCreditId());
			NRCreditPR.setWalletId(credit.getId());
			list.add(NRCreditPR);
		}
		return Pair.of(list, remaining);
	}
}
