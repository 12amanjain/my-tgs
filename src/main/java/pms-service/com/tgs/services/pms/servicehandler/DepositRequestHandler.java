package com.tgs.services.pms.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.DepositRequestFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.pms.datamodel.DIPayment;
import com.tgs.services.pms.datamodel.DepositAction;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.DepositType;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.jparepository.DepositRequestService;
import com.tgs.services.pms.manager.DepositManager;
import com.tgs.services.pms.restcontroller.requestValidator.DepositActionValidator;
import com.tgs.services.pms.restmodel.DepositResponse;
import com.tgs.services.ums.datamodel.User;
import lombok.Setter;

@Service
public class DepositRequestHandler extends ServiceHandler<DepositRequest, DepositResponse> {

	@Autowired
	private DepositManager depositManager;

	@Autowired
	private DepositRequestService depositService;

	@Autowired
	private DepositActionValidator actionValidator;

	@Autowired
	private UserServiceCommunicator userComm;

	@Setter
	private DepositAction action;

	private DepositRequest depositRequest;

	@Override
	public void beforeProcess() throws Exception {
		if (request.getReqId() != null)
			depositRequest = depositService.fetchByReqId(request.getReqId()).toDomain();

		request.setActionList(
				actionValidator.validActions(SystemContextHolder.getContextData().getUser(), depositRequest));

		validateAction();
	}

	private void validateAction() {

		if (depositRequest.getStatus().equals(DepositRequestStatus.ACCEPTED)
				|| depositRequest.getStatus().equals(DepositRequestStatus.REJECTED))
			throw new CustomGeneralException(SystemError.DEPOSITREQUEST_NOT_UPDATABLE);

		if (!actionValidator.validActions(SystemContextHolder.getContextData().getUser(), depositRequest)
				.contains(action))
			throw new CustomGeneralException(SystemError.DEPOSITREQUEST_INVALID_ACTION);

	}

	@Override
	public void process() throws Exception {

		if (action.equals(DepositAction.ASSIGN)) {

			request.getAdditionalInfo().setAssignedTime(LocalDateTime.now());
			request.setStatus(DepositRequestStatus.PROCESSING);
		}

		if (action.equals(DepositAction.ASSIGNME)) {
			if (StringUtils.isEmpty(depositRequest.getAdditionalInfo().getAssignedUserId())
					|| SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.ADMIN)) {
				request.getAdditionalInfo()
						.setAssignedUserId(SystemContextHolder.getContextData().getUser().getUserId());
				request.getAdditionalInfo().setAssignedTime(LocalDateTime.now());
				request.setStatus(DepositRequestStatus.PROCESSING);
			} else {
				throw new CustomGeneralException(SystemError.DEPOSITREQUEST_ALREADY_ASSIGNED);
			}
		}

		if (action.equals(DepositAction.ABORT)) {
			if (SystemContextHolder.getContextData().getUser().getUserId().equals(depositRequest.getUserId())) {
				request.setStatus(DepositRequestStatus.ABORTED);
			} else {
				request.setStatus(DepositRequestStatus.REJECTED);
			}
		}

		response.setDepositRequests(new ArrayList<>());
		generateDRId(request);
		depositRequest = depositManager.submit(request);
		depositRequest.setActionList(
				actionValidator.validActions(SystemContextHolder.getContextData().getUser(), depositRequest));
		response.getDepositRequests().add(depositRequest);

	}

	@Override
	public void afterProcess() throws Exception {

	}

	/**
	 * This function will be called in case of new deposit request and while abort
	 * 
	 * @return
	 */
	public DepositResponse saveRequest() {
		response.setDepositRequests(new ArrayList<>());

		/**
		 * In case if new deposit request is raised from user
		 */
		if (StringUtils.isBlank(request.getReqId())) {
			if (StringUtils.isBlank(request.getUserId()))
				request.setUserId(UserUtils.getUserId(SystemContextHolder.getContextData().getUser()));

			User depositorUser = userComm.getUserFromCache(request.getUserId());

			if (!depositorUser.getRole().isDepositWalletAllowed()) {
				// Check if wallet is allowed for parent user. Because staff can raise on behalf of parent.
				if (depositorUser.getParentUserId() != null) {
					request.setUserId(depositorUser.getParentUserId());
					User parentUser = userComm.getUserFromCache(depositorUser.getParentUserId());
					if (!parentUser.getRole().isDepositWalletAllowed()) {
						throw new CustomGeneralException(SystemError.DEPOSIT_NOT_ALLOWED);
					}
				} else {
					throw new CustomGeneralException(SystemError.DEPOSIT_NOT_ALLOWED);
				}
			}

			if (request.getType().equals(DepositType.PAYMENT_GATEWAY)) {
				request.setPaymentFee(request.getPaymentFee() + request.getAdditionalInfo().getGst());
				request.setRequestedAmount(request.getRequestedAmount() + request.getPaymentFee());
			}

			request.setPartnerId(depositorUser.getPartnerId());
			request.setStatus(DepositRequestStatus.SUBMITTED);
		} else {
			DepositRequest depositRequest = depositService.fetchByReqId(request.getReqId()).toDomain();
			// We allow to update RcNo and DhNo even if DR is in accepted (or rejected) state
			boolean updatingRcOrDhNo = request.getAdditionalInfo() != null
					&& (StringUtils.isNotEmpty(request.getAdditionalInfo().getRcNo())
							|| StringUtils.isNotEmpty(request.getAdditionalInfo().getDhNo()));
			if (!updatingRcOrDhNo && (depositRequest.getStatus().equals(DepositRequestStatus.ACCEPTED)
					|| depositRequest.getStatus().equals(DepositRequestStatus.REJECTED))) {
				throw new CustomGeneralException(SystemError.DEPOSITREQUEST_NOT_UPDATABLE);
			}
		}
		generateDRId(request);
		response.getDepositRequests().add(depositManager.submit(request));
		return response;
	}

	public static void generateDRId(DepositRequest depositRequest) {
		if (StringUtils.isBlank(depositRequest.getReqId())) {
			String reqId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build());
			depositRequest.setReqId(reqId);
		}
	}

	public List<DIPayment> getDIPayments(List<Payment> payments) {
		List<DIPayment> diPaymentList = new ArrayList<>();
		List<String> reqIds = payments.stream().map(Payment::getRefId).collect(Collectors.toList());
		DepositRequestFilter filter = DepositRequestFilter.builder().transactionIdIn(reqIds).build();
		List<DepositRequest> DIList = depositService.findAll(filter);
		Map<String, DepositRequest> map =
				DIList.stream().collect(Collectors.toMap(DepositRequest::getTransactionId, x -> x));
		payments.forEach(p -> {
			DIPayment diPayment = new GsonMapper<>(p, DIPayment.class).convert();
			diPayment.setUserId(p.getPayUserId());
			diPaymentList.add(diPayment);
			DepositRequest depositIncentive = map.get(p.getRefId());
			if (depositIncentive != null) {
				diPayment.setDiRcNumber(depositIncentive.getAdditionalInfo().getRcNo());
				diPayment.setDiDhNumber(depositIncentive.getAdditionalInfo().getDhNo());
				diPayment.getAdditionalInfo().setComments(depositIncentive.getComments());
			}
		});
		return diPaymentList;
	}
}
