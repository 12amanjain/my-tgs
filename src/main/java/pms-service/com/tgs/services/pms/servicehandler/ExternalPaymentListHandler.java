package com.tgs.services.pms.servicehandler;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.ExternalPaymentFilter;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.pms.datamodel.ExternalPayment;
import com.tgs.services.pms.dbmodel.DbExternalPayment;
import com.tgs.services.pms.jparepository.ExternalPaymentService;
import com.tgs.services.pms.restmodel.ExternalPaymentResponse;

@Service
public class ExternalPaymentListHandler extends ServiceHandler<ExternalPaymentFilter, ExternalPaymentResponse> {

	@Autowired
	private ExternalPaymentService externalPaymentService;

	private List<ExternalPayment> paymentList;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		paymentList = DbExternalPayment.toDomainList(externalPaymentService.search(request));
	}

	@Override
	public void afterProcess() throws Exception {
		response.setPaymentList(paymentList);
	}

}
