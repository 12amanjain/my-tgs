package com.tgs.services.pms.servicehandler;

import java.util.Objects;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.dbmodel.DbPaymentConfigurationRule;
import com.tgs.services.pms.jparepository.PaymentConfigurationService;
import com.tgs.services.pms.restmodel.PaymentConfigurationRuleResponse;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PaymentConfigurationRuleHandler
		extends ServiceHandler<PaymentConfigurationRule, PaymentConfigurationRuleResponse> {

	@Autowired
	private PaymentConfigurationService confService;

	@Override
	public void beforeProcess() throws Exception {
		DbPaymentConfigurationRule confRule = null;
		try {
			if (request != null && Objects.nonNull(request.getId())) {
				confRule = confService.fetchRuleById(request.getId());
			}
			// confRule = new GsonMapper<>(request, confRule, DbPaymentConfigurationRule.class).convert();
			/**
			 * patch update will not work
			 */
			confRule = new DbPaymentConfigurationRule().from(request);
			confRule = confService.saveorUpdate(confRule);
			request.setId(confRule.getId().intValue());
		} catch (Exception e) {
			log.error("Unable to save rule ", e);
			throw new CustomGeneralException("Save Or Update Failed " + e);
		}
	}

	@Override
	public void process() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		response.getRules().add(request);
	}

	/**
	 * This will list the Medium which added in Configuration Based on Product
	 *
	 * @param product
	 * @param enabled
	 * @return Payment Medium Available for Product payment medium />
	 */
	public PaymentConfigurationRuleResponse getPaymentMediums(Product product, Boolean enabled) {
		PaymentConfigurationRuleResponse mediumResponse = new PaymentConfigurationRuleResponse();
		confService.findAllPaymentRules().forEach(medium -> {
			PaymentConfigurationRule mediumConfiguration = new GsonMapper<>(medium, PaymentConfigurationRule.class, true)
					.convert();
			if(enabled!=null && mediumConfiguration.getEnabled()==enabled)
				mediumResponse.getRules().add(mediumConfiguration);
		});
		return mediumResponse;
	}
	
	/**
	 * 
	 * @param id -> paymentConfigurator rule id
	 * @return
	 */
	public BaseResponse deletePaymentRule(String id) {
		BaseResponse baseResponse= new BaseResponse();
		if (StringUtils.isNotBlank(id)) {	
			DbPaymentConfigurationRule dbPaymentRule= confService.fetchRuleById(NumberUtils.toInt(id));
			dbPaymentRule.setDeleted(true);
			dbPaymentRule.setEnabled(false);
			confService.saveorUpdate(dbPaymentRule);
		}else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updatePaymentRuleStatus(String id, Boolean status) {
		BaseResponse baseResponse= new BaseResponse();
		if (StringUtils.isNotBlank(id)) {
			DbPaymentConfigurationRule dbPaymentRule= confService.fetchRuleById(NumberUtils.toInt(id));
			if(dbPaymentRule!=null && BooleanUtils.isNotTrue(dbPaymentRule.isDeleted())) {
				dbPaymentRule.setEnabled(status);
				confService.saveorUpdate(dbPaymentRule);
			}else {
				baseResponse.addError(new ErrorDetail(SystemError.RESOURCE_NOT_FOUND));
			}
		}
		else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
