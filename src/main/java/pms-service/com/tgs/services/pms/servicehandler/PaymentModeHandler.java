package com.tgs.services.pms.servicehandler;

import static java.math.BigDecimal.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.GstInfoFilter;
import com.tgs.filters.NRCreditFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.CommercialCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditLinePaymentMode;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.NRCreditPaymentMode;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentMode;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentMode;
import com.tgs.services.pms.helper.PaymentConfigurationHelper;
import com.tgs.services.pms.helper.PaymentUtils;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.jparepository.UserWalletService;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import com.tgs.services.pms.ruleengine.PaymentConfigOutput;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PaymentModeHandler {

	@Autowired
	private CreditLineService creditLineService;

	@Autowired
	private NRCreditService nrCreditService;

	@Autowired
	private UserWalletService walletService;

	@Autowired
	@Lazy
	private OrderServiceCommunicator orderServiceComm;

	@Autowired
	private FMSCommunicator fmsComm;

	@Autowired
	private HMSCommunicator hmsComm;

	@Autowired
	private UserServiceCommunicator userServiceComm;

	@Autowired
	GeneralServiceCommunicator gmsCommunicator;

	@Autowired
	CommercialCommunicator commComm;

	private PaymentModeRequest request;

	private PaymentModeResponse response;

	/**
	 * Pre-Populated available mediums converted to Payment Mode with Corresponding Payment Fee
	 */
	public PaymentModeResponse getResponse(PaymentModeRequest request) {
		log.info("Payment modes request received: " + new Gson().toJson(request));
		PaymentModeResponse response = new PaymentModeResponse();
		this.request = request;
		this.response = response;
		ContextData contextData = SystemContextHolder.getContextData();
		/*
		 * This is majorly to prevent any mid office user doing booking after emulating into agent staff login.
		 */
		if (contextData.getUser() != null && UserUtils.isMidOfficeRole(UserUtils.getEmulateRole(contextData.getUser()))
				&& !contextData.getUser().getRole().isCreditLineAllowed()) {
			throw new CustomGeneralException(SystemError.NOT_ALLOWED_PAYMENT_MODES);
		}

		String bookingId = request.getBookingId();
		String payUserId = ObjectUtils.firstNonNull(request.getPayUserId(),
				UserUtils.getPayUserId(SystemContextHolder.getContextData().getUser()));
		SystemContextHolder.getContextData().getReqIds().add(bookingId);
		Long billingEntityId = request.getBillingEntityId();
		Order order = orderServiceComm.findByBookingId(bookingId);
		if (order != null) {
			payUserId = order.getBookingUserId();
		}
		User payUser = userServiceComm.getUserFromCache(payUserId);
		if (request.getProduct() == null && !StringUtils.isBlank(bookingId)) {
			request.setProduct(Product.getProductMetaInfoFromId(bookingId).getProduct());
		}

		PaymentFact paymentFact = PaymentFact.builder().build();
		if (request.getProduct() != null) {
			bookingId = setRefIdIfWalletTopup(request);
			setMarkUp(request.getProduct(), bookingId, order);
			paymentFact.setProduct(request.getProduct());
		}
		if (request.getAmount() != null) {
			paymentFact.setAmount(request.getAmount());
		}
		paymentFact.setSubMedium("all");
		paymentFact.setAccountName(request.getAccountName());
		paymentFact.setUserId(payUserId);
		paymentFact.setRole(payUser.getRole());
		paymentFact.setIsPartner(UserUtils.isPartnerUser(payUser));
		response.setBookingId(bookingId);

		if (Product.WALLET_TOPUP.equals(request.getProduct()) && !payUser.getRole().isDepositWalletAllowed()) {
			throw new CustomGeneralException(SystemError.NOT_ALLOWED_PAYMENT_MODES);
		}

		GstInfoFilter gstFilter =
				GstInfoFilter.builder().idIn(Arrays.asList(billingEntityId)).userIdIn(Arrays.asList(payUserId)).build();

		if (Product.AIR.equals(request.getProduct())) {
			listVirtualPaymentMedium(paymentFact, payUser, gstFilter, bookingId);
		}
		// In case of Virtual Payment, we dont need to show other payment mediums
		if (CollectionUtils.isEmpty(response.getPaymentModes())) {
			listWalletPaymentMedium(paymentFact, payUserId);
			listCreditLines(paymentFact, payUserId);
			listNRCredits(paymentFact, payUserId);
			listExternalPaymentMediums(paymentFact, payUserId);
		}
		return response;
	}

	private String setRefIdIfWalletTopup(PaymentModeRequest request) {
		String referenceId = request.getBookingId();
		/**
		 * Product must be non-null.
		 */
		if (request.getProduct().equals(Product.WALLET_TOPUP) && StringUtils.isBlank(referenceId)) {
			referenceId = ServiceUtils.generateId(ProductMetaInfo.builder().product(Product.WALLET_TOPUP).build());
			request.setBookingId(referenceId);
		}
		return referenceId;
	}

	/**
	 * Will return true/false based on wallet balance is sufficient for doing transaction
	 * 
	 * @return
	 */
	private boolean isWalletBalanceSufficient() {
		WalletPaymentMode paymentMode = (WalletPaymentMode) response.getPaymentModes().stream()
				.filter(mode -> mode.getMode().equals(PaymentMedium.WALLET)).findFirst().get();
		return paymentMode.getWallet().getBalance().compareTo(request.getAmount()) > 0;
	}

	private void listWalletPaymentMedium(PaymentFact paymentFact, String payUserId) {
		PaymentConfigurationRule walletConf = getPaymentMediumRuleForMedium(PaymentMedium.WALLET, paymentFact);
		if (walletConf != null) {
			UserWallet userWallet = walletService.findByUserId(payUserId).toDomain();
			WalletPaymentMode paymentMode = WalletPaymentMode.builder().wallet(userWallet).ruleId(walletConf.getId())
					.mode(PaymentMedium.WALLET).priority(walletConf.getPriority()).build();
			response.getPaymentModes().add(paymentMode);
		}
	}

	private void listVirtualPaymentMedium(PaymentFact paymentFact, User payuser, GstInfoFilter gstFilter,
			String bookingId) {
		GstInfo gstInfo = null;
		GstInfo orderBillingEntity = orderServiceComm.getGstInfo(bookingId);
		// In case of hold confirm, manual order, import PNR billing entity should be fetched from orderbillingentity.
		if (orderBillingEntity != null && StringUtils.isNotEmpty(orderBillingEntity.getGstNumber())) {
			gstInfo = orderBillingEntity;
		} else {
			// In case of direct booking billingEntityId will be from request.
			List<GstInfo> gstInfos = gmsCommunicator.searchBillingEntity(gstFilter);
			if (CollectionUtils.isNotEmpty(gstInfos)) {
				log.debug("GSt info found with id {} ", gstInfos.get(0).getId());
				gstInfo = gstInfos.get(0);
			}
		}
		boolean fetchFromCMS = commComm.isVirtualCreditCardSupported(bookingId, payuser, gstInfo);
		if (fetchFromCMS) {
			PaymentConfigurationRule vpMedium =
					getPaymentMediumRuleForMedium(PaymentMedium.VIRTUAL_PAYMENT, paymentFact);
			if (vpMedium != null) {
				PaymentMode paymentMode = PaymentMode.builder().mode(PaymentMedium.VIRTUAL_PAYMENT)
						.ruleId(vpMedium.getId()).priority(vpMedium.getPriority()).paymentFee(ZERO).build();
				response.getPaymentModes().add(paymentMode);
			}
		}

	}

	private void listExternalPaymentMediums(PaymentFact paymentFact, String payUserId) {
		List<PaymentConfigurationRule> availableMediums = new ArrayList<>();
		availableMediums.add(getPaymentMediumRuleForMedium(PaymentMedium.CREDITCARD, paymentFact));
		availableMediums.add(getPaymentMediumRuleForMedium(PaymentMedium.DEBITCARD, paymentFact));
		availableMediums.add(getPaymentMediumRuleForMedium(PaymentMedium.NETBANKING, paymentFact));
		availableMediums = TgsCollectionUtils.getNonNullElements(availableMediums);
		if (availableMediums != null) {
			for (PaymentConfigurationRule medium : availableMediums) {
				BigDecimal paymentFee = PaymentUtils.getPaymentFee(SystemContextHolder.getContextData().getUser(),
						request.getAmount(), medium.getId(), null);

				BigDecimal partialPaymentFee = ZERO;
				PaymentConfigOutput config =
						PaymentConfigurationHelper
								.getPaymentRuleOutput(
										PaymentFact.createFact()
												.setUserId(UserUtils.getParentUserId(
														SystemContextHolder.getContextData().getUser())),
										PaymentRuleType.PARTIALPAYMENTMEDIUMS);

				if (config != null && config.getPaymentMediums().contains(medium.getMedium())
						&& CollectionUtils.isNotEmpty(response.getPaymentModes())
						&& response.getPaymentModes().get(0).getMode() == PaymentMedium.WALLET) {
					UserWallet userWallet = ((WalletPaymentMode) response.getPaymentModes().get(0)).getWallet();
					BigDecimal amountToBeCharged = ZERO;
					if (userWallet.getBalance().compareTo(request.getAmount()) < 0) {
						amountToBeCharged = request.getAmount().subtract(userWallet.getBalance());
					}
					partialPaymentFee = PaymentUtils.getPaymentFee(SystemContextHolder.getContextData().getUser(),
							amountToBeCharged, medium.getId(), null);
				}
				PaymentConfigOutput mediumConfigOutput = (PaymentConfigOutput) medium.getOutput();
				PaymentMode paymentMode = PaymentMode.builder().mode(medium.getMedium()).paymentFee(paymentFee)
						.priority(medium.getPriority()).ruleId(medium.getId()).partialpaymentFee(partialPaymentFee)
						.displayName(mediumConfigOutput.getDisplayName()).subMediums(getSubMediums(medium)).build();
				response.getPaymentModes().add(paymentMode);
			}
		}
	}

	private List<String> getSubMediums(PaymentConfigurationRule medium) {
		final List<String> subMediums = new ArrayList<>();
		List<String> subMediumsInIncl = medium.getInclusionCriteria().getSubMediums();
		if (subMediumsInIncl != null) {
			subMediums.addAll(subMediumsInIncl);
		}
		if (medium.getExclusionCriteria() == null) {
			return subMediums;
		}
		List<String> subMediumsInExcl = medium.getExclusionCriteria().getSubMediums();
		if (CollectionUtils.isNotEmpty(subMediumsInExcl)) {
			subMediums.addAll(subMediumsInExcl);
		}
		return subMediums.isEmpty() ? null : subMediums;
	}

	/**
	 * In case of sufficient Wallet Balance we shouldn't show creditlines.
	 * 
	 * @param availableMediums
	 * @param payUserId
	 */
	private void listCreditLines(PaymentFact paymentFact, String payUserId) {
		PaymentConfigurationRule creditLineConf = getPaymentMediumRuleForMedium(PaymentMedium.CREDIT_LINE, paymentFact);
		if (creditLineConf != null) {
			if (BooleanUtils.isTrue(request.getConsiderWalletBalance()) && isWalletBalanceSufficient()) {
				return;
			}

			List<CreditLine> creditLines = creditLineService
					.search(CreditLineFilter.builder().userIdIn(Arrays.asList(payUserId))
							.statusIn(Arrays.asList(CreditStatus.BLOCKED, CreditStatus.ACTIVE)).build())
					.stream()
					.filter(c -> paymentFact.getProduct() == null || paymentFact.getProduct().equals(Product.NA)
							|| c.getProducts().contains(paymentFact.getProduct()))
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(creditLines)) {
				if (BooleanUtils.isTrue(request.getOnlyEnabledModes())) {
					creditLines = creditLines.stream().filter(c -> c.getStatus().equals(CreditStatus.ACTIVE))
							.collect(Collectors.toList());
				}

				CreditLinePaymentMode paymentMode = new CreditLinePaymentMode(PaymentMedium.CREDIT_LINE, ZERO,
						creditLines, creditLineConf.getPriority(), null);
				response.getPaymentModes().add(paymentMode);
			}
		}
	}

	/**
	 * In case of sufficient Wallet Balance we shouldn't show nr credits.
	 * 
	 * @param availableMediums
	 * @param product
	 * @param payUserId
	 */

	private void listNRCredits(PaymentFact paymentFact, String payUserId) {
		PaymentConfigurationRule nrCreditMedium = getPaymentMediumRuleForMedium(PaymentMedium.CREDIT, paymentFact);
		if (nrCreditMedium != null) {
			if (BooleanUtils.isTrue(request.getConsiderWalletBalance()) && isWalletBalanceSufficient()) {
				return;
			}
			List<NRCredit> nrCreditList = nrCreditService
					.search(NRCreditFilter.builder().userIdIn(Collections.singletonList(payUserId))
							.statusIn(Arrays.asList(CreditStatus.BLOCKED, CreditStatus.ACTIVE)).build())
					.stream()
					.filter(c -> paymentFact.getProduct() == null || paymentFact.getProduct().equals(Product.NA)
							|| c.getProducts().contains(paymentFact.getProduct()))
					.collect(Collectors.toList());

			if (CollectionUtils.isNotEmpty(nrCreditList)) {
				if (BooleanUtils.isTrue(request.getOnlyEnabledModes())) {
					nrCreditList = nrCreditList.stream().filter(c -> c.getStatus().equals(CreditStatus.ACTIVE))
							.collect(Collectors.toList());
				}
				NRCreditPaymentMode paymentMode = new NRCreditPaymentMode(PaymentMedium.CREDIT, ZERO, nrCreditList,
						nrCreditMedium.getPriority(), null);
				response.getPaymentModes().add(paymentMode);
			}

		}
	}

	private void setMarkUp(Product pdt, String bookingId, Order order) {
		double markup = 0.0;
		if (Product.AIR.equals(pdt)) {
			if (order != null) {
				// hold booking
				markup = order.getMarkup();
			} else {
				markup = fmsComm.getTotalFareComponentAmount(bookingId, null, FareComponent.MU);
			}
		} else if (Product.HOTEL.equals(pdt)) {
			if (order != null) {
				// hold booking
				markup = order.getMarkup();
			} else {
				markup = hmsComm.getMarkup(bookingId);
			}
		}
		response.setAmount(request.getAmount());
		request.setAmount(request.getAmount().subtract(BigDecimal.valueOf(markup)).setScale(2, ROUND_HALF_UP));
		response.setMarkup(markup);
	}

	private PaymentConfigurationRule getPaymentMediumRuleForMedium(PaymentMedium paymentMedium,
			PaymentFact paymentFact) {
		paymentFact.setMedium(paymentMedium);
		return PaymentConfigurationHelper.getPaymentRule(paymentFact, PaymentRuleType.PAYMENT_MEDIUM);
	}
}
