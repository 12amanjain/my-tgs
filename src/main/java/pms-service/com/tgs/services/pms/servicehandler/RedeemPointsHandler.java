package com.tgs.services.pms.servicehandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.PointsServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.dbmodel.DbUserPoint;
import com.tgs.services.pms.jparepository.UserPointsService;
import com.tgs.services.pms.restmodel.RedeemPointsRequest;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import com.tgs.services.points.datamodel.ReedemPointsData;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RedeemPointsHandler extends ServiceHandler<RedeemPointsRequest, RedeemPointsResponse> {

	@Autowired
	protected UserPointsService userPointsService;

	@Autowired
	private PointsServiceCommunicator pointsService;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {
		// Respective services will give applicable points (after checking max redeemable points in the rule.
		// Hard coded for testing
		try {
			User user = SystemContextHolder.getContextData().getUser();
			if (user == null) {
				throw new CustomGeneralException(SystemError.UNAUTHORIZED_ACCESS);
			}
			String payUserId = UserUtils.getParentUserId(user);

			// If needed by services, move to Communicator
			DbUserPoint userPoints = userPointsService.getDbUserPoints(payUserId, request.getType());

			Double userPointsBalance = userPoints.getBalance();

			if (request.getPoints() > userPointsBalance) {
				throw new CustomGeneralException(SystemError.INSUFFICIENT_POINTS);
			}

			ProductMetaInfo metaInfo = Product.getProductMetaInfoFromId(request.getBookingId());
			ReedemPointsData pointsData = new ReedemPointsData();
			pointsData.setUserId(payUserId);
			pointsData.setPoints(request.getPoints());
			pointsData.setProduct(metaInfo.getProduct());
			pointsData.setType(request.getType());
			pointsData.setOpType(PaymentOpType.DEBIT);
			pointsData.setBookingId(request.getBookingId());
			response = pointsService.validatePoints(pointsData, false);

		} catch (CustomGeneralException e) {
			log.info("Unable to apply points for bookingId {}. Requested points {}. Reason {} ", request.getBookingId(),
					request.getPoints(), e.getMessage(), e);
			throw e;
		} catch (Exception e) {
			log.info("Unable to apply points for bookingId {}. Requested points {}. Reason {} ", request.getBookingId(),
					request.getPoints(), e.getMessage(), e);
			throw new CustomGeneralException(SystemError.INSUFFICIENT_POINTS);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
