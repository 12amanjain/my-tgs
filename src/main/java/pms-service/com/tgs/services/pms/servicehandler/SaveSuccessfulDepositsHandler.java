package com.tgs.services.pms.servicehandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.pms.datamodel.DepositRequest;
import com.tgs.services.pms.datamodel.DepositRequestStatus;
import com.tgs.services.pms.datamodel.SuccessfulDepositsRequest;
import com.tgs.services.pms.dbmodel.DbDepositRequest;
import com.tgs.services.pms.jparepository.DepositRequestService;


import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SaveSuccessfulDepositsHandler extends ServiceHandler<SuccessfulDepositsRequest, BaseResponse> {

	private static final int TTL = (int) TimeUnit.DAYS.toSeconds(1);

	@Autowired
	private GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private DepositRequestService depositService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		List<DepositRequest> successfulDeposits = request.getDepositRequests();
		List<String> transactionIds = new ArrayList<>();

		if (CollectionUtils.isEmpty(successfulDeposits)) {
			return;
		}

		for (DepositRequest depositRequest : successfulDeposits) {
			try {
				if (StringUtils.isBlank(depositRequest.getTransactionId())) {
					continue;
				}

				transactionIds.add(depositRequest.getTransactionId());

				DbDepositRequest dbDepositRequest = null;

				List<DbDepositRequest> dbDepositRequests = depositService
						.fetchByTransactionIdOrderByCreatedOn(depositRequest.getTransactionId());
				if (CollectionUtils.isNotEmpty(dbDepositRequests)) {
					for (DbDepositRequest dr : dbDepositRequests) {
						DepositRequestStatus status = DepositRequestStatus.getEnumFromCode(dr.getStatus());
						switch (status) {
						case PAYMENT_RECEIVED:
						case ACCEPTED:
						case PROCESSING:
							/**
							 * If there is a DepositRequest with given transactionId with status
							 * <ul>
							 * <li>ACCEPTED, DepositRequest for that txn id has been served
							 * <li>PAYMENT_RECEIVED or PROCESSING, DepositRequest for that txn id will be
							 * served
							 * </ul>
							 * and no other DepositRequest needs to be served, hence, quit.
							 */
							return;

						case SUBMITTED:
						case PAYMENT_NOT_RECEIVED:
							/**
							 * Most recently submitted or payment-not-received DepositRequest
							 */
							dbDepositRequest = dr;
							break;

						default:
							break;
						}
					}
				}

				if (dbDepositRequest == null) {
					CacheMetaInfo cacheMetaInfo = CacheMetaInfo.builder().set(CacheSetName.DEPOSIT_REQUEST.name())
							.key(depositRequest.getTransactionId()).build();
					cachingCommunicator.delete(cacheMetaInfo);
					cachingCommunicator.store(cacheMetaInfo, BinName.SUCCESSDEPOSIT.name(),
							Arrays.asList(depositRequest), false, false, TTL);
				} else {
					dbDepositRequest.setStatus(DepositRequestStatus.PAYMENT_RECEIVED.getCode());
					depositService.save(dbDepositRequest);
				}
			} catch (Exception e) {
				log.error("Processing deposit request failed for transactionId {}", depositRequest.getTransactionId(),
						e);
			}
		}
		log.info("Received transactionIds for processing: {}", transactionIds);
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
