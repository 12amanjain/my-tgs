package com.tgs.services.pms.servicehandler;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tgs.filters.CreditLineFilter;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.CreditBill;
import com.tgs.services.pms.datamodel.CreditBillCycleType;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.datamodel.CreditStatus;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.jparepository.CreditLineService;
import com.tgs.services.pms.jparepository.CreditPolicyService;
import com.tgs.services.pms.jparepository.NRCreditService;
import com.tgs.services.pms.manager.CreditLineManager;
import com.tgs.services.ums.datamodel.User;

@Service
public class UserPolicyHandler {

	@Autowired
	private UserServiceCommunicator service;

	@Autowired
	private CreditPolicyService policyService;

	@Autowired
	private CreditLineManager creditLineManager;

	@Autowired
	private CreditLineService creditLineService;

	@Autowired
	private NRCreditService nrCreditService;

	@Transactional
	public CreditBill updatePolicy(String userId, Product product, CreditPolicy creditPolicy) {
		User user = service.getUserFromUsrService(UserFilter.builder().userId(userId).build());
		if (user.getUserConf().getCreditPolicyMap().get(product) != null) {
			return changePolicy(user, product, creditPolicy);
		} else {
			assignPolicy(user, product, creditPolicy);
			return null;
		}
	}

	private CreditBill changePolicy(User user, Product product, CreditPolicy creditPolicy) {
		CreditBill bill = null;
		CreditPolicy old = policyService.findByPolicyId(user.getUserConf().getCreditPolicyMap().get(product));
		if (creditPolicy.getPolicyId().equals(user.getUserConf().getCreditPolicyMap().get(product))) {
			List<NRCredit> currentCredits = nrCreditService.getCurrentCredits(user.getUserId());
			List<CreditLine> creditLines = creditLineService
					.search(CreditLineFilter.builder().userIdIn(Collections.singletonList(user.getUserId()))
							.statusIn(Arrays.asList(CreditStatus.BLOCKED, CreditStatus.ACTIVE)).build());
			if (CollectionUtils.isNotEmpty(creditLines) || CollectionUtils.isNotEmpty(currentCredits)) {
				throw new CustomGeneralException(SystemError.SAME_POLICY);
			}
		}

		if (old.getBillCycleType().equals(CreditBillCycleType.NON_REVOLVING)) {
			List<NRCredit> currentCredits = nrCreditService.getCurrentCredits(user.getUserId());
			if (CollectionUtils.isNotEmpty(currentCredits))
				throw new CustomGeneralException(SystemError.NRC_CANNOT_ISSUE);
		} else {
			bill = creditLineManager.expireCreditLine(user.getUserId(), product);
		}
		if (!creditPolicy.getBillCycleType().equals(CreditBillCycleType.NON_REVOLVING)) {
			creditLineManager.issueCreditLine(creditPolicy, user.getUserId());
		}
		user.getUserConf().getCreditPolicyMap().put(product, creditPolicy.getPolicyId());
		service.updateUser(user);
		return bill;
	}

	private void assignPolicy(User user, Product product, CreditPolicy creditPolicy) {
		if (!creditPolicy.getBillCycleType().equals(CreditBillCycleType.NON_REVOLVING)) {
			creditLineManager.issueCreditLine(creditPolicy, user.getUserId());
		}
		user.getUserConf().getCreditPolicyMap().put(product, creditPolicy.getPolicyId());
		service.updateUser(user);
	}
}
