package com.tgs.services.pms.servicehandler;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.VoucherServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.vms.restmodel.VoucherDiscountRequest;
import com.tgs.services.vms.restmodel.VoucherDiscountResponse;
import com.tgs.services.vms.restmodel.VoucherValidateRequest;
import com.tgs.services.vms.restmodel.VoucherValidateResponse;

@Service
public class VoucherDiscountHandler extends ServiceHandler<VoucherDiscountRequest, VoucherDiscountResponse> {

	@Autowired
	private VoucherServiceCommunicator voucherService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		VoucherValidateResponse voucherResponse = getVoucherRules();
		if (voucherResponse != null && voucherResponse.getDiscountedAmount() != null) {
			response.setTotalDiscount(voucherResponse.getDiscountedAmount());
		} else {
			SystemError systemError = SystemError.INVALID_VOUCHER_CODE;
			ErrorDetail errorDetail = ErrorDetail.builder().errCode(systemError.errorCode())
					.message(systemError.getMessage()).details(systemError.getMessage()).build();
			response.setErrors(Arrays.asList(errorDetail));
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public VoucherValidateResponse getVoucherRules() {
		Product product = Product.getProductMetaInfoFromId(request.getBookingId()).getProduct();
		VoucherValidateRequest request = new VoucherValidateRequest(getRequest().getBookingId(),
				getRequest().getVoucherCode(), product, getRequest().getMediums());
		return voucherService.applyVoucher(request, false);
	}

}
