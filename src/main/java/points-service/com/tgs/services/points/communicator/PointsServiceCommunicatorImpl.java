package com.tgs.services.points.communicator;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PointsConfigurationRuleFilter;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.PointsServiceCommunicator;
import com.tgs.services.base.datamodel.PointsConfiguration;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import com.tgs.services.points.datamodel.AirRedeemPointsData;
import com.tgs.services.points.datamodel.HotelRedeemPointData;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.datamodel.ReedemPointsData;
import com.tgs.services.points.helper.PointsConfigurationHelper;
import com.tgs.services.points.restmodel.AirRedeemPointResponse;
import com.tgs.services.points.restmodel.HotelRedeemPointResponse;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PointsServiceCommunicatorImpl implements PointsServiceCommunicator {

	@Autowired
	private FMSCommunicator fmsCommunicator;

	@Autowired
	private HMSCommunicator hmsCommunicator;

	@Autowired
	private GeneralServiceCommunicator gmsService;

	@Override
	public RedeemPointsResponse validatePoints(ReedemPointsData request, boolean modifyfareDetail) {

		log.info("Redeem Points Validate for {}", request);
		preProcessPointRules(request);
		if (Product.AIR.equals(request.getProduct())) {
			AirRedeemPointsData pointRequest = new AirRedeemPointsData();
			pointRequest.setBookingId(request.getBookingId());
			pointRequest.setOpType(request.getOpType());
			pointRequest.setPointRules(request.getPointRules());
			pointRequest.setPointsConfiguration(request.getPointsConfiguration());
			pointRequest.setPoints(request.getPoints());
			pointRequest.setProduct(Product.AIR);
			pointRequest.setType(request.getType());
			return validateAirRewardPoints(pointRequest, modifyfareDetail);
		} else if (Product.HOTEL.equals(request.getProduct())) {
			HotelRedeemPointData pointRequest = new HotelRedeemPointData();
			pointRequest.setBookingId(request.getBookingId());
			pointRequest.setOpType(request.getOpType());
			pointRequest.setPointRules(request.getPointRules());
			pointRequest.setPointsConfiguration(request.getPointsConfiguration());
			pointRequest.setPoints(request.getPoints());
			pointRequest.setProduct(Product.HOTEL);
			pointRequest.setType(request.getType());
			return validateHotelRewardPoints(pointRequest, modifyfareDetail);
		}
		return null;
	}

	private void preProcessPointRules(ReedemPointsData pointsRequest) {
		List<PointsConfigurationRule> pointRules = null;
		PointsConfigurationRuleFilter pointsConfigFilter =
				PointsConfigurationRuleFilter.builder().enabled(true).build();
		pointsConfigFilter.setOperationTypeIn(Arrays.asList(pointsRequest.getOpType()));
		pointRules = PointsConfigurationHelper.getPointsRuleBasedOnType(pointsRequest.getType());
		pointsRequest.setPointRules(pointRules);

		ClientGeneralInfo clientGeneralInfo =
				(ClientGeneralInfo) gmsService.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO).getOutput();
		Map<PointsType, PointsConfiguration> pointsConfigurationMap = clientGeneralInfo.getPointsConfigurations();
		PointsConfiguration pointsConfig = pointsConfigurationMap.get(pointsRequest.getType());
		pointsRequest.setPointsConfiguration(pointsConfig);
	}

	@Override
	public AirRedeemPointResponse validateAirRewardPoints(AirRedeemPointsData request, boolean modifyfareDetail) {
		log.info("Air Redeem Points Validate for {}", request);
		request.setOpType(PaymentOpType.DEBIT);
		if (CollectionUtils.isEmpty(request.getPointRules())) {
			preProcessPointRules(request);
		}
		return fmsCommunicator.calcOrApplyPoints(request, modifyfareDetail);
	}

	public HotelRedeemPointResponse validateHotelRewardPoints(HotelRedeemPointData request,
			boolean modifyfareDetail) {
		log.info("Hotel Redeem Points Validate for {}", request);
		request.setOpType(PaymentOpType.DEBIT);
		if (CollectionUtils.isEmpty(request.getPointRules())) {
			preProcessPointRules(request);
		}
		return hmsCommunicator.calcOrApplyPoints((HotelRedeemPointData) request, modifyfareDetail);
	}
}
