package com.tgs.services.points.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;
import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Getter
@Setter
@Audited
@Table(name = "pointsconfigrule")
public class DbPointsConfigurationRule extends BaseModel<DbPointsConfigurationRule, PointsConfigurationRule> {

	@Column
	private String type;

	@Column
	private String operationType;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String inclusionCriteria;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String exclusionCriteria;

	@Column
	@JsonAdapter(JsonStringSerializer.class)
	private String outputCriteria;

	@Column
	private boolean enabled;

	@Column
	private boolean isDeleted;

	@Column
	private Double priority;

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Override
	public PointsConfigurationRule toDomain() {
		return new GsonMapper<>(this, PointsConfigurationRule.class, true).convert();
	}

	@Override
	public DbPointsConfigurationRule from(PointsConfigurationRule dataModel) {
		return new GsonMapper<>(dataModel, this, DbPointsConfigurationRule.class).convert();
	}

}
