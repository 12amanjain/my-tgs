package com.tgs.services.points.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.EnumUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.filters.PointsConfigurationRuleFilter;
import com.tgs.ruleengine.CustomisedRuleEngine;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.ruleengine.IRuleField;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.fms.ruleengine.FlightBasicRuleField;
import com.tgs.services.hms.HotelBasicRuleField;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.ruleengine.PaymentBasicRuleField;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.jparepository.PointsConfigurationService;

@Service
public class PointsConfigurationHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap pointConfigRules;

	private static final String FIELD = "point_rules";

	@Autowired
	private PointsConfigurationService pointsConfigService;


	public PointsConfigurationHelper(CustomInMemoryHashMap configurationHashMap,
			CustomInMemoryHashMap voucherConfigRules) {
		super(configurationHashMap);
		PointsConfigurationHelper.pointConfigRules = voucherConfigRules;
	}

	@Override
	public void process() {
		List<PointsConfigurationRule> pointsConfigurations =
				pointsConfigService.findAll(PointsConfigurationRuleFilter.builder().build());
		Map<PointsType, List<PointsConfigurationRule>> ruleMap =
				pointsConfigurations.stream().collect(Collectors.groupingBy(PointsConfigurationRule::getType));
		ruleMap.forEach((key, value) -> pointConfigRules.put(key.name(), FIELD, value,
				CacheMetaInfo.builder().compress(true).expiration(InMemoryInitializer.NEVER_EXPIRE)
						.set(CacheSetName.POINTS_CONFIG.getName()).build()));
	}

	@Override
	public void deleteExistingInitializer() {
		pointConfigRules.truncate(CacheSetName.POINTS_CONFIG.getName());
	}

	@SuppressWarnings("unchecked")
	public static List<PointsConfigurationRule> getPointsRuleBasedOnType(PointsType type) {
		if (type == null) {
			return null;
		}
		List<PointsConfigurationRule> ruleList = new ArrayList<>();
		return pointConfigRules.get(type.name(), FIELD, ruleList.getClass(),
				CacheMetaInfo.builder().set(CacheSetName.POINTS_CONFIG.getName()).compress(true)
						.typeOfT(new TypeToken<List<PointsConfigurationRule>>() {}.getType()).build());

	}

	@SuppressWarnings("unchecked")
	public static List<PointsConfigurationRule> getPointsRuleBasedOnType(IFact fact, PointsType type) {

		List<PointsConfigurationRule> matchingRules = getPointsRuleBasedOnType(type);

		Map<String, ? extends IRuleField> fieldResolverMap = new HashMap<String, IRuleField>();
		if (fact instanceof FlightBasicFact) {
			fieldResolverMap = EnumUtils.getEnumMap(FlightBasicRuleField.class);
		} else {
			fieldResolverMap = EnumUtils.getEnumMap(HotelBasicRuleField.class);
		}
		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(matchingRules, fact, fieldResolverMap);

		return (List<PointsConfigurationRule>) ruleEngine.fireAllRules();
	}

	public <T> T getPointsRuleOutput(IFact fact, PointsType type) {
		List<PointsConfigurationRule> rules = getPointsRuleBasedOnType(fact, type);
		if (CollectionUtils.isNotEmpty(rules)) {
			return (T) rules.get(0).getOutput();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static List<PointsConfigurationRule> getApplicableRules(IFact fact, PointsType type, PaymentOpType opType) {

		List<PointsConfigurationRule> matchingRules = getPointsRuleBasedOnType(type);
		matchingRules =
				matchingRules.stream().filter(r -> r.getOperationType().equals(opType)).collect(Collectors.toList());

		Map<String, ? extends IRuleField> fieldResolverMap = EnumUtils.getEnumMap(PaymentBasicRuleField.class);

		CustomisedRuleEngine ruleEngine = new CustomisedRuleEngine(matchingRules, fact, fieldResolverMap);

		return (List<PointsConfigurationRule>) ruleEngine.fireAllRules();
	}


}
