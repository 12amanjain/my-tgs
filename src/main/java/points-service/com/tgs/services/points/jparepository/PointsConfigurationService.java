package com.tgs.services.points.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PointsConfigurationRuleFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.dbmodel.DbPointsConfigurationRule;

@Service
public class PointsConfigurationService extends SearchService<DbPointsConfigurationRule> {

	@Autowired
	PointsConfigurationRepository pointsConfigRepo;

	public DbPointsConfigurationRule saveOrUpdate(DbPointsConfigurationRule pointsConfig) {
		pointsConfig.setProcessedOn(LocalDateTime.now());
		pointsConfig = pointsConfigRepo.save(pointsConfig);
		return pointsConfig;
	}

	public DbPointsConfigurationRule fetchById(Long id) {
		return pointsConfigRepo.findOne(id);
	}

	public List<PointsConfigurationRule> findAll(PointsConfigurationRuleFilter pointsConfigFilter) {
		return BaseModel.toDomainList(super.search(pointsConfigFilter, pointsConfigRepo));
	}


}
