package com.tgs.services.points.restcontroller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.PointsConfigurationRuleFilter;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.restmodel.PointsRuleResponse;
import com.tgs.services.points.servicehandler.PointsConfigurationHandler;
import com.tgs.services.ums.datamodel.AreaRole;

@RestController
@RequestMapping("/points/v1/config")
public class PointsConfigController {

	@Autowired
	private PointsConfigurationHandler pointsConfigurationHandler;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT, isMidOfficeAllowed = true)
	protected PointsRuleResponse savePointsConfigRule(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid PointsConfigurationRule confRule) throws Exception {
		pointsConfigurationHandler.initData(confRule, new PointsRuleResponse());
		return pointsConfigurationHandler.getResponse();
	}


	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected PointsRuleResponse listPointsConfigRules(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid PointsConfigurationRuleFilter pointsConfigFilter) throws Exception {
		List<PointsConfigurationRule> rules = pointsConfigurationHandler.findAll(pointsConfigFilter);
		PointsRuleResponse ruleResponse = new PointsRuleResponse();
		ruleResponse.getPointsConfigurations().addAll(rules);
		return ruleResponse;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT, isMidOfficeAllowed = true)
	protected BaseResponse deletePointsConfigRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return pointsConfigurationHandler.deletePointsConfigurationRule(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT, isMidOfficeAllowed = true)
	protected BaseResponse changePointsRuleStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return pointsConfigurationHandler.updatePointsConfigRuleStatus(id, status);

	}
}
