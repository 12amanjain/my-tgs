package com.tgs.services.points.servicehandler;

import java.util.List;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PointsConfigurationRuleFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import com.tgs.services.points.dbmodel.DbPointsConfigurationRule;
import com.tgs.services.points.jparepository.PointsConfigurationService;
import com.tgs.services.points.restmodel.PointsRuleResponse;
import com.tgs.utils.exception.ResourceNotFoundException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PointsConfigurationHandler extends ServiceHandler<PointsConfigurationRule, PointsRuleResponse> {

	@Autowired
	protected PointsConfigurationService pointsConfigService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		DbPointsConfigurationRule pointsConfig = null;
		try {
			if (request != null && Objects.nonNull(request.getId())) {
				pointsConfig = pointsConfigService.fetchById(Long.valueOf(request.getId()));
				if (pointsConfig == null) {
					throw new ResourceNotFoundException(SystemError.INVALID_ID);
				}
				pointsConfig = new DbPointsConfigurationRule().from(request);
			} else {
				pointsConfig = new GsonMapper<>(request, DbPointsConfigurationRule.class, true).convert();
				pointsConfig.setEnabled(Boolean.TRUE);
			}
			pointsConfig = pointsConfigService.saveOrUpdate(pointsConfig);
			response.getPointsConfigurations().add(pointsConfig.toDomain());
		} catch (Exception e) {
			log.error("Unable to save points configuration with type {} and request {} because {}", request.getType(),
					GsonUtils.getGson().toJson(request), e.getMessage(), e);
			throw new CustomGeneralException(
					"Save Or Update of Points configuration Failed because {}" + e.getMessage());
		}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	public List<PointsConfigurationRule> findAll(PointsConfigurationRuleFilter pointsConfigFilter) {
		return pointsConfigService.findAll(pointsConfigFilter);
	}

	public BaseResponse deletePointsConfigurationRule(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbPointsConfigurationRule pointsConfigRule = pointsConfigService.fetchById(id);
		if (Objects.nonNull(pointsConfigRule)) {
			pointsConfigRule.setDeleted(true);
			pointsConfigService.saveOrUpdate(pointsConfigRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updatePointsConfigRuleStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbPointsConfigurationRule pointsConfigRule = pointsConfigService.fetchById(id);
		if (Objects.nonNull(pointsConfigRule)) {
			pointsConfigRule.setEnabled(status);
			pointsConfigService.saveOrUpdate(pointsConfigRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
