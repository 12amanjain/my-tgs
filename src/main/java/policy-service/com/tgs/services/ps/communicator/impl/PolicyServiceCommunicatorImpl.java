package com.tgs.services.ps.communicator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.PolicyServiceCommunicator;
import com.tgs.services.base.ruleengine.IPolicyOutput;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.helper.UserPolicyHelper;
import com.tgs.services.ps.manager.UserPolicyManager;

@Service
public class PolicyServiceCommunicatorImpl implements PolicyServiceCommunicator {

	@Autowired
	UserPolicyManager policyManager;
	
	@Override
	public UserPolicy fetchUserPolicies(String userId) {
		return UserPolicyHelper.getUserPolicy(userId);
	}

	@Override
	public <T extends IPolicyOutput> T fetchPolicyOutput(String userId, String policyId) {
		Policy policy = policyManager.fetchPolicyById(userId, policyId);
		return policy !=null ? policy.getOutput() : null;
	}

	@Override
	public Map<String, List<Policy>> fetchUserPolicies(List<String> userIds, List<String> labels) {
		Map<String,  List<Policy>> policyMap = new HashMap<>();
		userIds.forEach(userid -> {
			policyMap.put(userid, policyManager.getUserPolicies(userid,labels,true));
		});
		return policyMap;
	}

}
