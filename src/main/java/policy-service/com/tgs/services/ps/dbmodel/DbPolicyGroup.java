package com.tgs.services.ps.dbmodel;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.runtime.database.CustomTypes.UserPolicyListType;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ps.datamodel.PolicyGroup;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "policygroup")
@TypeDefs(@TypeDef(name = "UserPolicyListType", typeClass = UserPolicyListType.class))
@Setter
@Getter
public class DbPolicyGroup extends BaseModel<DbPolicyGroup, PolicyGroup> {

	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	private boolean enabled;

	@Column
	private boolean isDeleted;

	@Column
	private String name;

	@Column
	private String description;

	@Type(type = "UserPolicyListType")
	@Column
	private List<Policy> policies;

	public PolicyGroup toDomain() {
		return new GsonMapper<>(this, PolicyGroup.class).convert();
	}

	public DbPolicyGroup from(PolicyGroup dataModel) {
		return new GsonMapper<>(dataModel, this, DbPolicyGroup.class).convert();
	}

}
