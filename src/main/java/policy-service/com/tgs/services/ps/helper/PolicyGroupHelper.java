package com.tgs.services.ps.helper;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.PolicyGroupFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ps.datamodel.PolicyGroup;
import com.tgs.services.ps.dbmodel.DbPolicyGroup;
import com.tgs.services.ps.jparepository.PolicyGroupService;

@Service
public class PolicyGroupHelper extends InMemoryInitializer {

	private static final String FIELD = "policy_grp";

	private static CustomInMemoryHashMap policyGroupHashMap;

	@Autowired
	private PolicyGroupService service;

	public PolicyGroupHelper(CustomInMemoryHashMap configurationHashMap,
			CustomInMemoryHashMap permissionsGroupHashMap) {
		super(configurationHashMap);
		PolicyGroupHelper.policyGroupHashMap = permissionsGroupHashMap;
	}

	@Override
	public void process() {
		List<PolicyGroup> policyGroups = DbPolicyGroup.toDomainList(service.findAll(PolicyGroupFilter.builder().build()));
		if (!CollectionUtils.isEmpty(policyGroups)) {
			policyGroups.forEach(group -> update(group));
		}
	}

	@Override
	public void deleteExistingInitializer() {
		policyGroupHashMap.truncate(CacheSetName.POLICY_GROUP.getName());
	}

	public void update(PolicyGroup policyGroup) {
		if (policyGroup.isDeleted()) {
			policyGroupHashMap.delete(String.valueOf(policyGroup.getId()),CacheMetaInfo.builder().
					namespace(CacheNameSpace.USERS.getName()).set(CacheSetName.POLICY_GROUP.getName()).build());
		} else {
			policyGroupHashMap.put(String.valueOf(policyGroup.getId()), FIELD, policyGroup,
					CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
							.set(CacheSetName.POLICY_GROUP.getName()).compress(false)
							.expiration(InMemoryInitializer.NEVER_EXPIRE).build());
		}
	}

	public PolicyGroup getPolicyGroup(String groupId) {
		return policyGroupHashMap.get(groupId, FIELD, PolicyGroup.class, CacheMetaInfo.builder()
				.namespace(CacheNameSpace.USERS.getName()).set(CacheSetName.POLICY_GROUP.getName()).build());
	}

}
