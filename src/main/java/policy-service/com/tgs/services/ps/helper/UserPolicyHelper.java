package com.tgs.services.ps.helper;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import com.tgs.filters.UserPolicyFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.jparepository.UserPolicyService;

@Service
public class UserPolicyHelper extends InMemoryInitializer {

	private static final String FIELD = "user_policy";

	private static CustomInMemoryHashMap policyHashMap;

	@Autowired
	private UserPolicyService policyService;

	public UserPolicyHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap userPolicyHashMap) {
		super(configurationHashMap);
		UserPolicyHelper.policyHashMap = userPolicyHashMap;
	}

	@Override
	public void process() {
		List<UserPolicy> userPolicies = policyService.search(UserPolicyFilter.builder().build());
		if (!CollectionUtils.isEmpty(userPolicies)) {
			userPolicies.forEach(policy -> updateInCache(policy));
		}

	}

	@Override
	public void deleteExistingInitializer() {
		policyHashMap.truncate(CacheSetName.USER_POLICY.getName());
	}

	public void updateInCache(UserPolicy policy) {
		policyHashMap.put(policy.getUserId(), FIELD, policy,
				CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
						.set(CacheSetName.USER_POLICY.getName()).compress(false)
						.expiration(InMemoryInitializer.NEVER_EXPIRE).build());
	}

	public static UserPolicy getUserPolicy(String userId) {
		return policyHashMap.get(userId, FIELD, UserPolicy.class, CacheMetaInfo.builder()
				.namespace(CacheNameSpace.USERS.getName()).set(CacheSetName.USER_POLICY.getName()).build());
	}

}
