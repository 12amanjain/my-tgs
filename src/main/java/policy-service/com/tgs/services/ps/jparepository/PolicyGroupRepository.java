package com.tgs.services.ps.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import com.tgs.services.ps.dbmodel.DbPolicyGroup;

public interface PolicyGroupRepository
		extends JpaRepository<DbPolicyGroup, Long>, JpaSpecificationExecutor<DbPolicyGroup> {

	DbPolicyGroup findById(Long id);

	List<DbPolicyGroup> findByEnabledAndIsDeleted(Boolean enabled, Boolean isDeleted);
	
	List<DbPolicyGroup> findByIsDeleted(Boolean isDeleted);
}
