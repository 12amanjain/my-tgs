package com.tgs.services.ps.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserPolicyFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.dbmodel.DbUserPolicy;
import com.tgs.services.ps.helper.UserPolicyHelper;

@Service
public class UserPolicyService extends SearchService<DbUserPolicy> {

	@Autowired
	private UserPolicyRepository repository;
	
	@Autowired
	private UserPolicyHelper policyHelper;

	public DbUserPolicy save(DbUserPolicy userPolicy) {
		userPolicy.setProcessedOn(LocalDateTime.now());
		repository.save(userPolicy);
		policyHelper.updateInCache(userPolicy.toDomain());
		return userPolicy;
	}
	
	public DbUserPolicy findByUserId(String userId) {
		return repository.findByUserId(userId);
	}
	
	public List<UserPolicy> search(UserPolicyFilter filter) {
		return BaseModel.toDomainList(super.search(filter, repository));
	}
}
