package com.tgs.services.ps.restcontroller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.PolicyGroupFilter;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ps.datamodel.AssignPolicyGroup;
import com.tgs.services.ps.datamodel.PolicyGroup;
import com.tgs.services.ps.restmodel.PolicyGroupResponse;
import com.tgs.services.ps.servicehandler.PolicyGroupHandler;
import com.tgs.services.ums.datamodel.AreaRole;


@RestController
@RequestMapping("/ps/v1/group")
public class PolicyGroupController {

	@Autowired
	PolicyGroupHandler policyGroupHandler;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.POLICY_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody PolicyGroupResponse savePermission(@RequestBody PolicyGroup permission) throws Exception {
		policyGroupHandler.initData(permission, new PolicyGroupResponse());
		return policyGroupHandler.getResponse();
	}

	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected @ResponseBody PolicyGroupResponse listPermission(@RequestBody PolicyGroupFilter filter) {
		return policyGroupHandler.list(filter);
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.POLICY_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody BaseResponse deletePolicyGroup(@PathVariable("id") Long id) throws Exception {
		return policyGroupHandler.deletePolicyGroup(id);
	}

	@RequestMapping(value = "/status/{id}/{enabled}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.POLICY_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody PolicyGroupResponse changeStatus(@PathVariable("id") Long id,
			@PathVariable("enabled") Boolean enabled) throws Exception {
		policyGroupHandler.initData(PolicyGroup.builder().id(id).enabled(enabled).build(), new PolicyGroupResponse());
		return policyGroupHandler.getResponse();
	}

	@RequestMapping(value = "/assign", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.POLICY_SERVICE, isMidOfficeAllowed = true)
	protected @ResponseBody BaseResponse assignToUser(@RequestBody AssignPolicyGroup policyGroup) throws Exception {
		policyGroupHandler.assignPoliciesToUsers(policyGroup);
		return new BaseResponse();
	}

}
