package com.tgs.services.ps.servicehandler;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ps.restmodel.PolicyGroupResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.compress.utils.Lists;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.PolicyGroupFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.ps.datamodel.AssignPolicyGroup;
import com.tgs.services.ps.datamodel.PolicyGroup;
import com.tgs.services.ps.datamodel.UserPolicyInfo;
import com.tgs.services.ps.dbmodel.DbPolicyGroup;
import com.tgs.services.ps.dbmodel.DbUserPolicy;
import com.tgs.services.ps.jparepository.PolicyGroupService;
import com.tgs.services.ps.jparepository.UserPolicyService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class PolicyGroupHandler extends ServiceHandler<PolicyGroup, PolicyGroupResponse> {

	@Autowired
	PolicyGroupService policyGroupService;
	
	@Autowired
	UserPolicyService userPolicyService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void process() throws Exception {
		DbPolicyGroup dbPolicyGroup = null;
		if (checkDuplicacy()) {
			throw new CustomGeneralException(SystemError.DUPLICATE_GROUP);
		}
		if (request.getId() != null) {
			dbPolicyGroup = policyGroupService.findById(request.getId());
			if (dbPolicyGroup == null || dbPolicyGroup.isDeleted()) {
				throw new CustomGeneralException(SystemError.POLICY_GROUP_NOT_FOUND);
			}
		} else {
			dbPolicyGroup = new DbPolicyGroup();
		}
		try {
			dbPolicyGroup = dbPolicyGroup.from(request);
			if(CollectionUtils.isEmpty(dbPolicyGroup.getPolicies()))
				throw new CustomGeneralException(SystemError.EMPTY_GROUP);
			PolicyGroup savedPolicyGroup = policyGroupService.save(dbPolicyGroup).toDomain();				
			response.getPolicyGroups().add(savedPolicyGroup);
		} catch (CustomGeneralException e) {
			throw e;
		} catch (Exception exception) {
			log.error("Saving/Updating Policy Group {} failed due to {} ", request, exception);
			throw new CustomGeneralException(SystemError.POLICY_GROUP_NOT_SAVED);
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public PolicyGroupResponse list(PolicyGroupFilter filter) {
		if (response == null) {
			response = new PolicyGroupResponse();
		}
		response.getPolicyGroups().addAll(DbPolicyGroup.toDomainList(policyGroupService.findAll(filter)));
		response.getPolicyGroups().removeIf(group -> group.isDeleted());
		return response;
	}

	public void assignPoliciesToUsers(AssignPolicyGroup policyGroup) {
		policyGroup.getUserIds().forEach(userId -> {
			DbUserPolicy userPolicy = userPolicyService.findByUserId(userId);
			if(userPolicy == null) {
				userPolicy = new DbUserPolicy();
				userPolicy.setUserId(userId);
			}
			UserPolicyInfo policyInfo = ObjectUtils.defaultIfNull(userPolicy.getPolicyInfo() , UserPolicyInfo.builder().build());
			if(policyInfo.getGroupIds() ==null)
				policyInfo.setGroupIds(Lists.newArrayList());
			policyInfo.getGroupIds().add(policyGroup.getGroupId());
			userPolicyService.save(userPolicy);
		});
	}

	//Group name should be unique amongst non deleted groups
	private boolean checkDuplicacy() {
		if(request.getName() !=null) {
			PolicyGroupFilter groupFilter = PolicyGroupFilter.builder().name(request.getName()).
					isDeleted(false).build();
			List<DbPolicyGroup> policyGroups = policyGroupService.findAll(groupFilter);
			//This is done to ensure that if during update request same group is being updated. 
			if(request.getId()!=null)
				policyGroups= policyGroups.stream().filter(p->p.getId()!=request.getId()).collect(Collectors.toList());
			return CollectionUtils.isNotEmpty(policyGroups);
		}
		return false;
	}

	public BaseResponse deletePolicyGroup (Long groupId) {
		BaseResponse baseResponse = new BaseResponse();
		DbPolicyGroup dbPolicyGroup = policyGroupService.findById(groupId);
		if (Objects.nonNull(dbPolicyGroup)) {
			dbPolicyGroup.setDeleted(true);
			policyGroupService.save(dbPolicyGroup);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}
}
