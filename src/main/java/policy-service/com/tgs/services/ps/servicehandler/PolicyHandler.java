package com.tgs.services.ps.servicehandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import com.tgs.services.ps.restmodel.UserPolicyResponse;
import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ps.datamodel.PolicyGroup;
import com.tgs.services.ps.datamodel.UserPolicy;
import com.tgs.services.ps.datamodel.UserPolicyFilter;
import com.tgs.services.ps.datamodel.UserPolicyInfo;
import com.tgs.services.ps.dbmodel.DbUserPolicy;
import com.tgs.services.ps.helper.PolicyGroupHelper;
import com.tgs.services.ps.helper.UserPolicyHelper;
import com.tgs.services.ps.jparepository.UserPolicyService;
import com.tgs.services.ps.manager.UserPolicyManager;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class PolicyHandler extends ServiceHandler<UserPolicy, UserPolicyResponse> {

	@Autowired
	private UserPolicyService policyService;
	
	@Autowired
	private UserPolicyManager policyManager;
	
	@Autowired
	private PolicyGroupHelper groupHelper;
	
	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void process() throws Exception {
		DbUserPolicy dbUserPolicy = ObjectUtils.firstNonNull(policyService.findByUserId(request.getUserId()), new DbUserPolicy());
		dbUserPolicy= dbUserPolicy.from(request);
		//Check only if new group is being added
		if(CollectionUtils.isNotEmpty(dbUserPolicy.getPolicyInfo().getGroupIds()))
			detectConflictingPolicies(dbUserPolicy);
		policyService.save(dbUserPolicy);
		response.getUserPolicies().add(dbUserPolicy.toDomain());		
	}

	private void detectConflictingPolicies(DbUserPolicy dbUserPolicy) {
		List<String> groupPolicyIds = new ArrayList<>();
		Set<String> conflictingPolicies = new HashSet<>();
		if(CollectionUtils.isNotEmpty(dbUserPolicy.getPolicyInfo().getGroupIds())) {
			dbUserPolicy.getPolicyInfo().getGroupIds().forEach(groupId-> {
				PolicyGroup pg = groupHelper.getPolicyGroup(groupId);
				pg.getPolicies().stream().forEach(p-> {
					if(groupPolicyIds.contains(p.getPolicyId())) {
						conflictingPolicies.add(p.getPolicyId());
					}else {
						groupPolicyIds.add(p.getPolicyId());
					}
				});
			});
		}
		if(CollectionUtils.isNotEmpty(dbUserPolicy.getPolicyInfo().getPolicies())) {
			dbUserPolicy.getPolicyInfo().getPolicies().forEach(p-> {
				conflictingPolicies.remove(p.getPolicyId());
			});
		}
		if(!conflictingPolicies.isEmpty()) {
			log.error("Conflicting policies {} found for userId {} and groups {} ", conflictingPolicies, dbUserPolicy.getUserId(), dbUserPolicy.getPolicyInfo().getGroupIds());
			throw new CustomGeneralException(SystemError.CONFLICTING_POLICIES,"Please assign these policies at user-level if you want to assign user to mentioned groups : "+conflictingPolicies.toString());
	}
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
		
	}

	public List<UserPolicy> list(UserPolicyFilter filter) {
		List<UserPolicy> policies = new ArrayList<>();
		filter.getUserIds().forEach(userId -> {
			List<Policy> userPolicies = policyManager.getUserPolicies(userId, filter.getLabels(), filter.getEnabled());
			List<Policy> filteredPolicies = CollectionUtils.isNotEmpty(filter.getPolicyIds()) ?  userPolicies.stream().filter(p-> filter.getPolicyIds().contains(p.getPolicyId())).collect(Collectors.toList())
					: userPolicies;
			UserPolicy userpolicy = UserPolicyHelper.getUserPolicy(userId);
			List<String> groupIds  = new ArrayList<>();
			if(userpolicy!=null && userpolicy.getPolicyInfo()!=null)
				groupIds= ObjectUtils.firstNonNull(userpolicy.getPolicyInfo().getGroupIds(), Collections.emptyList());
			UserPolicy policy = UserPolicy.builder().policyInfo(UserPolicyInfo.builder().groupIds(groupIds).policies(filteredPolicies).build()).userId(userId).build();
			policies.add(policy);
		});
		return policies;
	}
	
	public BaseResponse updatePolicyStatus (String userId, String policyId,Boolean enabled) {
		BaseResponse baseResponse = new BaseResponse();
		try {
			DbUserPolicy userPolicy = policyService.findByUserId(userId);
			List<Policy> policies = userPolicy.getPolicyInfo().getPolicies();
			for(Policy policy : policies) {
				if(policy.getGroupId() !=null && policy.getPolicyId().equals(policyId)) {
					policy.setEnabled(enabled);
					break;
				}
			}
			policyService.save(userPolicy);
		}
		catch (Exception e) {
			log.error("Unable to change status of policy {} for user {} to {}",policyId,userId, enabled, e);
			baseResponse.addError(new ErrorDetail(SystemError.STATUS_CHANGE));
		}
		return baseResponse;
	}
}
