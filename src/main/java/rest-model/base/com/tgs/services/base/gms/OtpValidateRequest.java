package com.tgs.services.base.gms;

import javax.validation.constraints.NotNull;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OtpValidateRequest {

	@NotNull
	@ApiModelProperty(required = true)
	private String requestId;
	@NotNull
	@ApiModelProperty(required = true)
	private String otp;

	@SerializedName("maxattempt")
	private Integer maxAttemptCount;

	@SerializedName("et")
	private Integer expiryTimeInMinutes;

}
