package com.tgs.services.base.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.AuditResult;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class AuditResponse extends BaseResponse {
	List<AuditResult> results;

	public List<AuditResult> getResults() {
		if (results == null) {
			results = new ArrayList<>();
		}
		return results;
	}
}
