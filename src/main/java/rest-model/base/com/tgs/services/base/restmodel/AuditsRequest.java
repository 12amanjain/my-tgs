package com.tgs.services.base.restmodel;

import java.time.LocalDateTime;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuditsRequest extends BaseRequest {

	private String id;

	private LocalDateTime beforeDate;

	private LocalDateTime afterDate;

	private String userName;

	private String userId;

	private String userIp;
	
	private LocalDateTime createdOnBeforeDateTime;
	
	private LocalDateTime createdOnAfterDateTime;

}
