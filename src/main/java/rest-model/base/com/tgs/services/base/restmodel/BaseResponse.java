package com.tgs.services.base.restmodel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.datamodel.Conditions;

import lombok.Getter;
import lombok.Setter;
import org.springframework.util.CollectionUtils;

@Getter
@Setter
public class BaseResponse {
	private Status status;
	private List<ErrorDetail> errors;
	private Conditions conditions;
	private Integer retryInSecond;
	private Boolean refresh;

	private Map<String, Object> metaInfo;

	public void addError(ErrorDetail errorDetail) {
		if (errors == null) {
			errors = new ArrayList<>();
		}
		errors.add(errorDetail);
	}

	public Map<String, Object> getMetaInfo() {
		if (metaInfo == null)
			metaInfo = new HashMap<>();
		return metaInfo;
	}

	public String getErrorMessage() {
		if (!CollectionUtils.isEmpty(errors)) {
			return errors.stream().map(error -> error.getMessage()).collect(Collectors.joining(","));
		}
		return null;
	}

}
