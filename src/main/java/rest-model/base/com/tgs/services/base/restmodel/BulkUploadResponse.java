package com.tgs.services.base.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulkUploadResponse extends BaseResponse {

	private String uploadId;
	private List<BulkUploadInfo> uploadInfo;
	
}
