package com.tgs.services.base.restmodel;

import lombok.Getter;

@Getter
public class ReloadRequest {
	String type;
}
