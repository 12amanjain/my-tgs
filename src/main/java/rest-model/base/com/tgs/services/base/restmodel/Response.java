package com.tgs.services.base.restmodel;

import com.tgs.services.base.ErrorDetail;
import lombok.Data;

@Data
public class Response {
	private Status status;
	private ErrorDetail errors;
	private BaseResponse data;
}
