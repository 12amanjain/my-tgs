package com.tgs.services.cms.restmodel.commission;


import com.tgs.services.cms.datamodel.commission.CommissionPlanMapper;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommissionPlanMapperRequest {

    private CommissionPlanMapper planMapper;
}