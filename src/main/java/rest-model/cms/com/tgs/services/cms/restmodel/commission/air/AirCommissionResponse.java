package com.tgs.services.cms.restmodel.commission.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.datamodel.commission.air.AirCommissionRule;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirCommissionResponse extends BaseResponse {

	private List<AirCommissionRule> airCommissionRules;

	public List<AirCommissionRule> getAirCommissionRules() {
		if (airCommissionRules == null) {
			airCommissionRules = new ArrayList<>();
		}
		return airCommissionRules;
	}
}
