package com.tgs.services.cms.restmodel.commission.rule;

import com.tgs.services.cms.datamodel.commission.CommissionRule;
import lombok.Getter;

@Getter
public class CommissionRuleRequest {

    private CommissionRule commissionRule;
}