package com.tgs.services.cms.restmodel.commission.rule;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.datamodel.commission.CommissionRule;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CommissionRuleResponse extends BaseResponse {

	private List<CommissionRule> commissionRule;

	public List<CommissionRule> getCommissionRule() {
		if (commissionRule == null) {
			commissionRule = new ArrayList<>();
		}
		return commissionRule;
	}
}