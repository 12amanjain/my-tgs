package com.tgs.services.cms.restmodel.creditcard;

import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class CreditCardRequest {

    CreditCardInfo cardInfo;
}