package com.tgs.services.cms.restmodel.tourcode;


import com.tgs.services.cms.datamodel.tourcode.TourCodeFilter;
import lombok.Getter;

@Getter
public class TourCodeListRequest {

    private TourCodeFilter tourCodeFilter;
}
