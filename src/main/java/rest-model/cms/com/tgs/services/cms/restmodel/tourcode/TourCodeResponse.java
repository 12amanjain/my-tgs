package com.tgs.services.cms.restmodel.tourcode;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.cms.datamodel.tourcode.TourCode;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Setter
public class TourCodeResponse extends BaseResponse {

    List<TourCode> tourCodeList;

    public List<TourCode> getTourCodeList() {
        if (tourCodeList == null) {
            tourCodeList = new ArrayList<>();
        }
        return tourCodeList;
    }
}