package com.tgs.services.es.restmodel;

import lombok.Getter;
import lombok.Setter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.tgs.services.base.restmodel.BaseResponse;

@Getter
@Setter
public class ESAutoSuggestionResponse extends BaseResponse {

	private List<Map<String, ? extends Object>> suggestions;

	public List<Map<String, ? extends Object>> getSuggestions() {
		if (suggestions == null) {
			suggestions = new ArrayList<>();
		}
		return suggestions;
	}
}
