package com.tgs.services.ffts.restmodel;

import java.util.ArrayList;
import java.util.List;
import javax.validation.Valid;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.ffts.restmodel.AirFareTrackerRequest;

public class AirFareTrackerRequestList extends BulkUploadRequest {

	@Valid
	@SerializedName("aftrs")
	private List<AirFareTrackerRequest> airFareTrackerRequests;
	
	public List<AirFareTrackerRequest> getAirFareTrackerRequests() {
		if (airFareTrackerRequests == null) {
			airFareTrackerRequests = new ArrayList<>();
		}
		return airFareTrackerRequests;
	}
}
