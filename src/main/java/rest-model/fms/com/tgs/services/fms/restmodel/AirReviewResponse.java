package com.tgs.services.fms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.MessageInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirReviewResponse extends BaseResponse {

	private List<TripInfo> tripInfos;
	private List<Alert> alerts;
	private AirSearchQuery searchQuery;
	private String bookingId;
	@APIUserExclude
	private String tripId;
	private List<MessageInfo> messages;
	private PriceInfo totalPriceInfo;
	@APIUserExclude
	private Boolean priceValidation;
	private List<TravellerInfo> travellers;


	public void addAlert(Alert alert) {
		if (alert == null) {
			return;
		}
		if (alerts == null) {
			alerts = new ArrayList<>();
		}
		alerts.add(alert);
	}
}
