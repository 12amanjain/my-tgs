package com.tgs.services.fms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.AirSearchQuery;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel(value = "This will return list of search query based on sources which can be triggered parallely to get source wise response")
public class AirSearchQueryListResponse extends BaseResponse {
	private List<AirSearchQuery> searchQueryList;

	private AirSearchQuery searchQuery;
	private List<String> searchIds;

	public void addSearchId(String searchId) {
		if (searchIds == null) {
			searchIds = new ArrayList<>();
		}
		searchIds.add(searchId);
	}
}
