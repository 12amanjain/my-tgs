package com.tgs.services.fms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.AirSearchResult;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirSearchResponse extends BaseResponse {

	private AirSearchResult searchResult;

	private String searchId;
}
