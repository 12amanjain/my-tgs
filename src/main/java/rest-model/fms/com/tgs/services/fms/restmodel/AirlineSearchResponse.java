package com.tgs.services.fms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.AirlineInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirlineSearchResponse extends BaseResponse {
	private List<AirlineInfo> airlineList;
}
