package com.tgs.services.fms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.restmodel.BaseResponse;

public class AirportInfoResponse extends BaseResponse {
	
	private List<AirportInfo> airportInfos;
	
	public List<AirportInfo> getAirportInfos() {
		if (airportInfos == null) {
			airportInfos = new ArrayList<>();
		}
		return airportInfos;
	}

}
