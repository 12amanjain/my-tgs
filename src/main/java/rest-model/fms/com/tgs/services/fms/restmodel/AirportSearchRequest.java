package com.tgs.services.fms.restmodel;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirportSearchRequest {

	List<String> airportCodes;
}
