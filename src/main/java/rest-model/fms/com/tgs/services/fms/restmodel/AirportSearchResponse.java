package com.tgs.services.fms.restmodel;

import java.util.List;

import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.restmodel.BaseResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirportSearchResponse extends BaseResponse {

	private List<AirportInfo> airportList;
}
