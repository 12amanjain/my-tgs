package com.tgs.services.fms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AirFlowType;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class FareRuleRequest {

    private String id;
    private AirFlowType flowType;
    
    //Cat 16 category
    @SerializedName("dfr")
    private Boolean isDetailed;

}