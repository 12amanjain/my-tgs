package com.tgs.services.fms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;

import java.util.ArrayList;
import java.util.List;

public class FareRuleResponse extends BaseResponse {

    private List<FareRuleInfo> fareRule;

    public List<FareRuleInfo> getFareRuleInfos() {
        if (fareRule == null) {
            fareRule = new ArrayList<>();
        } return fareRule;
    }
}