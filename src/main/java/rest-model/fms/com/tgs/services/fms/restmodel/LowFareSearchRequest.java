package com.tgs.services.fms.restmodel;

import java.time.LocalDate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LowFareSearchRequest {
	LocalDate fromDate;
	LocalDate toDate;
	String source;
	String destination;
}
