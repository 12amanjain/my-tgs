package com.tgs.services.fms.restmodel;

import java.util.Map;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.LowFareInfo;

import lombok.Setter;

@Setter
public class LowFareSearchResponse extends BaseResponse {
	Map<String, LowFareInfo> fareResult;
}
