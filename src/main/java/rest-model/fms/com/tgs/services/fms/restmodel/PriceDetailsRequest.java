package com.tgs.services.fms.restmodel;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
import com.tgs.services.base.enums.AirFlowType;

@Getter
@Setter
public class PriceDetailsRequest {

	private List<String> priceIds;
	private AirFlowType flowType;

}
