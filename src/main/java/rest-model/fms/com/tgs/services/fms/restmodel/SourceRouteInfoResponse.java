package com.tgs.services.fms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.SourceRouteInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceRouteInfoResponse extends BaseResponse {
	private List<SourceRouteInfo> sourceRouteInfos;


	public List<SourceRouteInfo> getSourceRouteInfos() {
		if (sourceRouteInfos == null) {
			sourceRouteInfos = new ArrayList<>();
		}
		return sourceRouteInfos;
	}
}
