package com.tgs.services.fms.restmodel;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SourceRouteRequest {
	private List<Integer> sourceIds;

}
