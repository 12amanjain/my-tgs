package com.tgs.services.fms.restmodel;

import com.tgs.services.base.enums.AirFlowType;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class UserFeeUpdateRequest {

	private String id;

	private AirFlowType flowType;

	private String feeType;

	private Double value;
}
