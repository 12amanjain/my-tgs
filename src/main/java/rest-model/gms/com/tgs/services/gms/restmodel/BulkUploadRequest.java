package com.tgs.services.gms.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BulkUploadRequest {

	private String uploadId;
}
