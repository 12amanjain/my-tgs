package com.tgs.services.gms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.Document;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CollectionServiceResponse extends BaseResponse {

	private List<Document> documents = new ArrayList<>();
}
