package com.tgs.services.gms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.ruleengine.IRuleOutPut;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ConfigResponse extends BaseResponse {

	IRuleOutPut config;

}
