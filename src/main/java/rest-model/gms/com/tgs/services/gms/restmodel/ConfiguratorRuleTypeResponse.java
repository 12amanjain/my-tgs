package com.tgs.services.gms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
public class ConfiguratorRuleTypeResponse extends BaseResponse {

    private List<ConfiguratorInfo> configuratorInfos;

    public List<ConfiguratorInfo> getConfiguratorInfos() {
        if (configuratorInfos == null) {
            configuratorInfos = new ArrayList<>();
        }
        return configuratorInfos;
    }

}
