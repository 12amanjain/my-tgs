package com.tgs.services.gms.restmodel;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.CountryInfo;

@Getter
@Setter
public class CountryResponse extends BaseResponse {
	
	private List<CountryInfo> countryInfos;
	
}
