package com.tgs.services.gms.restmodel;

import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.ArrayList;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.Note;

@Getter
@Setter
public class NoteResponse extends BaseResponse {

	private List<Note> notes;

	public List<Note> getOrderNotes() {
		if (notes == null) {
			notes = new ArrayList<>();
		}
		return notes;
	}

}
