package com.tgs.services.gms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.PolicyInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PolicyInfoResponse extends BaseResponse {
	List<PolicyInfo> policyInfos;
}
