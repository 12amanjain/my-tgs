package com.tgs.services.gms.restmodel.systemaudit;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;

import java.util.ArrayList;
import java.util.List;

public class SystemAuditResponse extends BaseResponse {

    List<SystemAudit> auditList;

    public List<SystemAudit> getAuditList() {
        if (auditList == null) {
            auditList = new ArrayList<>();
        }
        return auditList;
    }
}