package com.tgs.services.hms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelStaticCityInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FetchHotelCityInfoResponse extends BaseResponse {
	
	private HotelStaticCityInfo response;

}
