package com.tgs.services.hms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelCancellationPolicyResponse extends BaseResponse {
	private String id;
	private List<HotelCancellationPolicy> cancellationPolicyList;
	private HotelCancellationPolicy cancellationPolicy;

}
