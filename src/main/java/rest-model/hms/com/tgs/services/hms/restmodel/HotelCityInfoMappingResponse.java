package com.tgs.services.hms.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.CityInfoMapping;
import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class HotelCityInfoMappingResponse extends BaseResponse {

	private List<CityInfoMapping> cityInfos;
}
