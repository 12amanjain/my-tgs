package com.tgs.services.hms.restmodel;

import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelCityInfoQuery extends BulkUploadQuery {

	private String cityname;
	private String suppliercityid;
	private String suppliercountryid;
	private String countryname;
	private String suppliername;
}
