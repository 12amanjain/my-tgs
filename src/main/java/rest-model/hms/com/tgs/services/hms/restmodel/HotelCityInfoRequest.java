package com.tgs.services.hms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelCityInfoRequest extends BulkUploadRequest {
	
	private List<HotelCityInfoQuery> cityInfoQuery;
}
