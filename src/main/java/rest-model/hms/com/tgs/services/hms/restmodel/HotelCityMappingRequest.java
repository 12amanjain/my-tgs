package com.tgs.services.hms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.hms.datamodel.HotelCityMappingQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelCityMappingRequest extends BulkUploadRequest {

	private List<HotelCityMappingQuery> cityMappingQuery;
}