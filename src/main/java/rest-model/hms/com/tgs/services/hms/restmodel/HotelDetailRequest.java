package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelSearchQuery;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelDetailRequest {
	private String id;
	private HotelSearchQuery searchQuery;
}
