package com.tgs.services.hms.restmodel;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.IataCodeCityMappingQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelIataCodeMappingRequest {

	@SerializedName("iccms")
	private List<IataCodeCityMappingQuery> iataCodeCityMappings;	
}
