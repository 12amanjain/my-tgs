package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelStaticDataMappingRequest;
import com.tgs.services.hms.datamodel.HotelStaticDataRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelInfoStaticDataRequest {
	private HotelStaticDataRequest staticDataRequest;
	private HotelStaticDataMappingRequest mappingRequest;

}

