package com.tgs.services.hms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelList {
	
    private String HotelId;
    private String HotelName;
    private String LocalHotelId;
    private Double PropertyRating;
    private Integer Available;
    private String ThumbNailUrl;
    private String RateCurrencyCode;
    private Double TotalCharges;
    private List<HotelProperty> HotelProperty; 

}