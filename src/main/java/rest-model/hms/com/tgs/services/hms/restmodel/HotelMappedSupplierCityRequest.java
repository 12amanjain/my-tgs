package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.CityInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelMappedSupplierCityRequest {

	private CityInfo supplierCityQuery;
}
