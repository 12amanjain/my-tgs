package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.supplier.HotelMealBasis;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelMealInfoResponse extends BaseResponse {

	private List<HotelMealBasis> mealInfos;
	
	public List<HotelMealBasis> getMealInfos() {
		if (mealInfos == null) {
			mealInfos = new ArrayList<>();
		}
		return mealInfos;
	}
}
