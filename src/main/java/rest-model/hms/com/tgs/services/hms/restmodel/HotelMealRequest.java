package com.tgs.services.hms.restmodel;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.hms.datamodel.HotelMealQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelMealRequest extends BulkUploadRequest {

	private List<HotelMealQuery> mealQuery;
}
