package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelNationalitySupplierMapping;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelNationalityStaticDataRequest {

	private HotelNationalitySupplierMapping nationalityMapping;
	
	
}
