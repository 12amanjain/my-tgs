package com.tgs.services.hms.restmodel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelPriceValidationRequest {

	private String bookingId;

}
