package com.tgs.services.hms.restmodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelReservationRequest extends HotelDetailRequest {

	private String expected_price;
	private String agent_ref_no;
	private String roomDetails;
	private String gzip;
	
}
