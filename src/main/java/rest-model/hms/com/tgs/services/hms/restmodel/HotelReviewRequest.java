package com.tgs.services.hms.restmodel;

import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.helper.SystemError;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class HotelReviewRequest implements Validatable {

	private String hotelId;
	private String optionId;

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();

		if (hotelId == null) {
			errors.put("hotelId", SystemError.INVALID_HOTEL_ID.getErrorDetail());
		}
		if (optionId == null) {
			errors.put("hotelId", SystemError.INVALID_OPTION_ID.getErrorDetail());
		}
		return errors;
	}
}
