package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelReviewResponse extends BaseResponse {
	private HotelInfo hInfo;
	private String bookingId;
	private List<Alert> alerts;
	private HotelSearchQuery query;
	private boolean isPriceChanged;
	private Double differentialPayment;

	public void addAlert(Alert alert) {
		if (alert == null) {
			return;
		}
		if (alerts == null) {
			alerts = new ArrayList<>();
		}
		alerts.add(alert);
	}

}
