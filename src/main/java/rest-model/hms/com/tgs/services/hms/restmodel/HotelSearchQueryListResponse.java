package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelSearchQuery;

import com.tgs.services.hms.datamodel.HotelSearchResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSearchQueryListResponse extends BaseResponse {
	private HotelSearchQuery searchQuery;
	private List<String> searchIds;
	private HotelSearchResult searchResult;

	public void addSearchId(String searchId) {
		if (searchIds == null) {
			searchIds = new ArrayList<>();
		}
		searchIds.add(searchId);
	}
}
