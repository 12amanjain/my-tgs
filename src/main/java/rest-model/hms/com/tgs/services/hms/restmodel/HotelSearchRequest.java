package com.tgs.services.hms.restmodel;

import javax.validation.Valid;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class HotelSearchRequest {

	@Valid
	@NonNull
	private HotelSearchQuery searchQuery;
	private String bookingId;
	private String searchId;
	private Boolean sync;
	private Boolean crossSell;
}
