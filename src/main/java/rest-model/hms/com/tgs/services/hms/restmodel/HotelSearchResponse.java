package com.tgs.services.hms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.HotelSearchResult;

import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelSearchResponse extends BaseResponse{

    private HotelSearchResult searchResult;
    private HotelSearchQuery searchQuery;
}
