package com.tgs.services.hms.restmodel;

import com.tgs.services.hms.datamodel.HotelSupplierCityQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSupplierCityRequest {

	private HotelSupplierCityQuery supplierCityQuery;
}
