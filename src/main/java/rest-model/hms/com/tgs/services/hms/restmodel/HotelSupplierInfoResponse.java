package com.tgs.services.hms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;

public class HotelSupplierInfoResponse extends BaseResponse {

	private List<HotelSupplierInfo> supplierInfos;

	public List<HotelSupplierInfo> getSupplierInfos() {
		if (supplierInfos == null) {
			supplierInfos = new ArrayList<>();
		}
		return supplierInfos;
	}

}
