package com.tgs.services.hms.restmodel;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.inventory.MappedHotelInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelSupplierMappedListResponse extends BaseResponse {
	
	private List<MappedHotelInfo> hInfoList;
	private String supplierId;
	private String sourceId;

}
