package com.tgs.services.hms.restmodel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Passengers {

	
	private String Saluation;
	private String FirstName;
	private String LastName;
	private String PassengerType;
	private String Age;
}
