package com.tgs.services.hms.restmodel.inventory;

import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRoomTypeInfoRequest {
	
	private HotelRoomTypeInfo roomTypeInfo;

}
