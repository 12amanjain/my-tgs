package com.tgs.services.hms.restmodel.qtech;

import javax.validation.Valid;

import com.tgs.services.hms.datamodel.qtech.QTechSaveQuery;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class QTechSaveRequest {

	@Valid
	@NonNull
	private QTechSaveQuery searchQuery;
}
