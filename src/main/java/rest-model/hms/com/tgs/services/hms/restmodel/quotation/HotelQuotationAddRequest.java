package com.tgs.services.hms.restmodel.quotation;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQuotationAddRequest {

	@SerializedName("id")
	private Long quotationId;
	@SerializedName("name")
	private String quotationName; 
	private String hotelId;
	private String optionId; 
	
}
