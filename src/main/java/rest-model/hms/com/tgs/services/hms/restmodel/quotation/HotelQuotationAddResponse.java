package com.tgs.services.hms.restmodel.quotation;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.quotation.HotelQuotationAddResult;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQuotationAddResponse extends BaseResponse {

	private HotelQuotationAddResult hotelQuotationAddResult;
}
