package com.tgs.services.hms.restmodel.quotation;

import com.google.gson.annotations.SerializedName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQuotationSearchRequest {

	@SerializedName("id")
	private Long quotationId;
	private Boolean showAll;
}
