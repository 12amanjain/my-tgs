package com.tgs.services.hms.restmodel.quotation;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.hms.datamodel.quotation.HotelQuotationSearchResult;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelQuotationSearchResponse extends BaseResponse {

	private List<HotelQuotationSearchResult> hotelQuotationSearchResult;
}
