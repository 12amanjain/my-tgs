package com.tgs.services.hms.restmodel.tripjack;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.Data;

@Data
public class HotelItemDetails {

	private HotelInfo hInfo;
	private HotelSearchQuery query;
}
