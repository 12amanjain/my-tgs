package com.tgs.services.hms.restmodel.tripjack;

import java.time.LocalDateTime;
import com.tgs.services.base.datamodel.DeliveryInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TjOrder {

	private String bookingId;
	private String partnerId;
	private Double amount;
	private Integer markup;
	private String orderType;
	private DeliveryInfo deliveryInfo;
	private String status;
	private LocalDateTime createdOn;
}
