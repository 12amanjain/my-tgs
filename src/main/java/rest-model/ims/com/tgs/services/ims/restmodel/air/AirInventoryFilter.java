package com.tgs.services.ims.restmodel.air;

import java.util.List;
import java.util.Set;

import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.enums.AirType;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class AirInventoryFilter extends QueryFilter {
	
	@ApiModelProperty(value = "To fetch inventory based on specific airline, you can set airline code here")
	private Set<String> airlines;
	@ApiModelProperty(value = "To fetch inventory based on specific departure airline, you can set departure airport code")
	private Set<String> source;
	@ApiModelProperty(value = "To fetch inventory based on specific arrival airline, you can set arrival airport code")
    private Set<String> destination;
	@ApiModelProperty(value = "To fetch enabled/disbaled inventories", example = "true")
	private Boolean isEnabled;
	@ApiModelProperty(value = "To fetch inventories by name")
	private String name;
	@ApiModelProperty(value = "To fetch inventories for a particular supplier")
	private List<String> supplierIds;
	@ApiModelProperty(value = "To fetch inventories based on trip type", example = "DOMESTIC")
	private AirType tripType;
	@ApiModelProperty(value = "To fetch inventories by id")
	private String inventoryId;
	
}
