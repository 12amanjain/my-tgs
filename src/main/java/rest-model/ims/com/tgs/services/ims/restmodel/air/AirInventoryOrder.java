package com.tgs.services.ims.restmodel.air;

import java.time.LocalDate;
import java.time.LocalDateTime;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.ims.datamodel.air.AirInventoryOrderInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AirInventoryOrder {

	private String inventoryId;

	private LocalDateTime createdOn;

	private String referenceId;

	private LocalDate validOn;

	private TravellerInfo airTravellerInfo;

	private AirInventoryOrderInfo additionalInfo;
	
	private String bookingUserId;
	
	private String costPrice;

}
