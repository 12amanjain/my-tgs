package com.tgs.services.ims.restmodel.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ims.datamodel.air.AirInventory;

public class AirInventoryResponse extends BaseResponse{

	List<AirInventory> airInventories;

	public List<AirInventory> getAirInventories(){
		if (airInventories == null) {
			airInventories = new ArrayList<>();
		}
		return airInventories;
	}
}
