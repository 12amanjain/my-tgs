package com.tgs.services.ims.restmodel.air;

import java.time.LocalDate;
import java.time.LocalDateTime;

import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.fms.datamodel.TripInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirInventoryTrip {

	private Long id;

	private String name;

	private TripInfo tripInfo;

	private boolean isEnabled;
	
	private boolean deleted;

	private Double disableBfrDept;

	private boolean isReturn;

	private LocalDateTime createdOn;

	private AirType tripType;

	@UserId
	private String supplierId;

	private LocalDate startDate;

	private LocalDate endDate;

}
