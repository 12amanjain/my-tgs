package com.tgs.services.ims.restmodel.air;

import java.time.LocalDate;
import java.util.Map;

import com.tgs.services.base.enums.CabinClass;
import com.tgs.services.base.enums.PaxType;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@SuperBuilder
@Getter
@Setter
public class AirSeatInventoryFilter extends AirInventoryFilter {

	private LocalDate travelDate;

	private CabinClass cabinClass;

	private Integer totalSeats;

	private Map<PaxType, Integer> paxInfo;

}
