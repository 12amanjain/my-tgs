package com.tgs.services.ims.restmodel.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;

public class InventoryOrderResponse extends BaseResponse {

	List<AirInventoryOrder> orders;

	public InventoryOrderResponse(List<AirInventoryOrder> orders) {
		this.orders = orders;
	}

	public List<AirInventoryOrder> getOrders() {
		if (orders == null) {
			orders = new ArrayList<>();
		}
		return orders;
	}
}
