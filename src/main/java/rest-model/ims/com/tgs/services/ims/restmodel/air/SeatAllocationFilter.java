package com.tgs.services.ims.restmodel.air;

import java.time.LocalDate;

import com.tgs.services.base.datamodel.QueryFilter;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@Getter
@Setter
@SuperBuilder
public class SeatAllocationFilter extends QueryFilter{

	@ApiModelProperty(notes = "To fetch seat allocations based on inventory id", example = "1001")
	private String inventoryId;
	
	@ApiModelProperty(notes = "To fetch deleted/ undeleted seat allocations", example = "true")
	private Boolean isDeleted;
	
	@ApiModelProperty(notes = "To fetch seat allocations after a certain date.", example = "2018-05-25")
	private LocalDate validOnAfterDate;
	
	@ApiModelProperty(notes = "To fetch seat allocations before a certain date.", example = "2018-05-27")
	private LocalDate validOnBeforeDate;
		
}
