package com.tgs.services.ims.restmodel.air;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ims.datamodel.SeatAllocation;

public class SeatAllocationResponse extends BaseResponse{

	List<SeatAllocation> seatAllocations;
	
	public List<SeatAllocation> getSeatAllocations(){
		if (seatAllocations == null) {
			seatAllocations = new ArrayList<>();
		}
		return seatAllocations;
	}
}
