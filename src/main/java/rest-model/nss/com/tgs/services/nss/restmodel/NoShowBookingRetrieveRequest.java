package com.tgs.services.nss.restmodel;

import java.time.LocalDate;
import java.util.List;
import javax.validation.constraints.NotNull;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NoShowBookingRetrieveRequest {


	private LocalDate fromTravelDate;

	private LocalDate toTraveldate;

	private List<String> bookingIds;

	private List<String> airlines;

	@NotNull
	private List<String> sourceIds;

	private List<String> supplierIds;


}
