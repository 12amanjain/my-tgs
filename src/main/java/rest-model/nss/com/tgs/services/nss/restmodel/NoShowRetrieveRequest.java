package com.tgs.services.nss.restmodel;

import java.util.Map;
import com.tgs.services.nss.datamodel.NoShowInfo;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class NoShowRetrieveRequest {

	/*
	 * Map of pnr vs SupplierId
	 */
	private Map<String, String> pnrMap;


	private Map<String, NoShowInfo> noShowInfoMap;


}
