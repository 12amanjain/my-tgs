package com.tgs.services.oms;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.restmodel.BaseResponse;

import com.tgs.services.pms.datamodel.PGDetail;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingResponse extends BaseResponse {

    private String bookingId;
    private PGDetail pgDetail;
    private List<Alert> alerts;

	public void addAlert(Alert alert) {
		if (alert == null) {
			return;
		}
		if (alerts == null) {
			alerts = new ArrayList<>();
		}
		alerts.add(alert);
	}
}
