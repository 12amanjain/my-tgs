package com.tgs.services.oms;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingResyncResponse extends BaseResponse {

	private List<String> notes;

	public void addNotes(String note) {
		if (notes == null) {
			notes = new ArrayList<>();
		}
		notes.add(note);
	}
}
