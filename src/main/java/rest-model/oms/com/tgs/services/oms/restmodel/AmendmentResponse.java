package com.tgs.services.oms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
@Builder
public class AmendmentResponse extends BaseResponse {

	private List<Amendment> amendmentItems;

	private Double differentialPayment;

	private AirCancellationResponse airCancellationReviewResponse;

	private Boolean isCancelled;


}
