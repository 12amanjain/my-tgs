package com.tgs.services.oms.restmodel;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.gson.UpperCaseTypeAdaptor;
import com.tgs.services.base.helper.ToUpper;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class BookingDetailRequest {

	@JsonAdapter(UpperCaseTypeAdaptor.class)
	private String bookingId;
	private int paxNo;
	private Boolean requirePaxPricing;
	private String email;
}
