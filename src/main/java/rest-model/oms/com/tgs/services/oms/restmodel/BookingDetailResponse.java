package com.tgs.services.oms.restmodel;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.oms.datamodel.ItemDetails;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.ums.datamodel.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookingDetailResponse extends BaseResponse {

	private Order order;
	@APIUserExclude
	private User bookingUser;

	private Map<String, ItemDetails> itemInfos;

	private List<Payment> paymentInfos;

	private GstInfo gstInfo;

	public Map<String, ItemDetails> getItemInfos() {
		if (itemInfos == null) {
			itemInfos = new HashMap<>();
		}
		return itemInfos;
	}

}
