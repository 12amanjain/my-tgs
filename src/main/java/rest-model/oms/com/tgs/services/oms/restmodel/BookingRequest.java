package com.tgs.services.oms.restmodel;


import java.util.List;
import java.util.Optional;
import org.apache.commons.collections4.CollectionUtils;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.OrderType;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.oms.datamodel.OrderFlowType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingRequest {

	private String bookingId;
	private GstInfo gstInfo;
	private DeliveryInfo deliveryInfo;
	private String oldBookingId;
	private List<PaymentRequest> paymentInfos;
	private OrderType type;
	private OrderFlowType flowType;
	private boolean autoPay;

	@SerializedName("cdb")
	private Boolean checkDuplicateBooking;

	public Optional<PaymentRequest> getPaymentRequestOnMedium(PaymentMedium medium) {
		if (CollectionUtils.isNotEmpty(paymentInfos) && hasPaymentMedium()) {
			return paymentInfos.stream().filter(pReq -> pReq.getPaymentMedium().equals(medium)).findFirst();
		}
		return Optional.empty();
	}

	private boolean hasPaymentMedium() {
		return getPaymentInfos().stream().filter(pInfo -> pInfo.getPaymentMedium() != null).findFirst().isPresent();
	}
}
