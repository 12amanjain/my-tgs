package com.tgs.services.oms.restmodel;

import java.time.LocalDateTime;

import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.InvoiceInfo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class InvoiceResponse extends BaseResponse {

	private InvoiceInfo from;

	private InvoiceInfo to;

	private LocalDateTime createdOn;

	private GstInfo gstInfo;
	
	private String bookingId;

	private String amendmentId;
}
