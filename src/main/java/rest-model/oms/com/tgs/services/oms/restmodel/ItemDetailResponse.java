package com.tgs.services.oms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.air.AirOrderItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemDetailResponse extends BaseResponse {

	List<AirOrderItem> items;

	public List<AirOrderItem> getItems() {
		if (items == null)
			items = new ArrayList<>();
		return items;
	}
}
