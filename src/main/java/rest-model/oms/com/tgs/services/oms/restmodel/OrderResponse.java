package com.tgs.services.oms.restmodel;

import java.util.ArrayList;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.ProcessedBookingDetail;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.Singular;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderResponse extends BaseResponse {
	@Singular
	private List<ProcessedBookingDetail> details;

	public List<ProcessedBookingDetail> getDetails() {
		if (details == null) {
			details = new ArrayList<>();
		}
		return details;
	}
}
