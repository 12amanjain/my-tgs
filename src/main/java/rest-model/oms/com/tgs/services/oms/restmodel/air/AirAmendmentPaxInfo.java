package com.tgs.services.oms.restmodel.air;

import java.time.LocalDateTime;
import java.util.Map;
import java.util.Set;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.oms.datamodel.PaxKey;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirAmendmentPaxInfo {
	
	private String amendmentId;

	private String bookingId;

	private AmendmentType type;

	private String remarks;

	private Map<String, Set<PaxKey>> paxKeys;

	private Set<Short> checklistIds;
	
	//applicable in case of Re-issue amemdment
	private LocalDateTime nextTravelDate;
	
	
}
