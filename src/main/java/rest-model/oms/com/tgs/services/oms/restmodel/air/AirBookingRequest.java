package com.tgs.services.oms.restmodel.air;

import java.util.List;

import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.oms.restmodel.BookingRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AirBookingRequest extends BookingRequest {
	private List<FlightTravellerInfo> travellerInfo;
	private Double markup;
}
