package com.tgs.services.oms.restmodel.air;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.restmodel.BaseRequest;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.oms.datamodel.PaxKey;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Getter
@Setter
@Builder
public class AirCancellationRequest extends BaseRequest {

	private Amendment amendment;

	private String bookingId;

	private String amendmentId;

	private AmendmentType type;

	private Map<String, Set<PaxKey>> paxKeys;

	private List<SegmentInfo> segmentInfos;

}
