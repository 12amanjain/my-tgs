package com.tgs.services.oms.restmodel.air;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.cancel.AirCancellationDetail;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.apache.commons.lang3.BooleanUtils;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AirCancellationResponse extends BaseResponse {

	@SerializedName("pi")
	@APIUserExclude
	private PriceInfo priceInfo;

	@SerializedName("cds")
	private AirCancellationDetail cancellationDetail;

	public AirCancellationDetail getCancellationDetail() {
		if (cancellationDetail == null) {
			cancellationDetail = AirCancellationDetail.builder().build();
		}
		return cancellationDetail;
	}
	
	public boolean isAutoCancellationAllowed() {
		return getCancellationDetail() != null
				&& BooleanUtils.isTrue(getCancellationDetail().getAutoCancellationAllowed());
	}
}
