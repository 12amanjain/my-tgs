package com.tgs.services.oms.restmodel.air;

import com.tgs.services.base.enums.AirFlowType;
import com.tgs.services.oms.restmodel.BookingRequest;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@ToString
@Builder
public class AirImportPnrBookingRequest extends BookingRequest {

	@NotNull
	private String pnr;

	@NotNull
	private String supplierId;

	@NotNull
	private String bookingUserId;

	private String accountCode;

	private AirFlowType airFlowType;

}
