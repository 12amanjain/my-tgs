package com.tgs.services.oms.restmodel.air;

import java.util.List;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.SegmentInfo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AirImportPnrReviewResponse extends BaseResponse {

	private List<SegmentInfo> segmentInfos;
	private String bookingId;
	private String userId;
	private GstInfo gstInfo;
	private DeliveryInfo deliveryInfo;

}
