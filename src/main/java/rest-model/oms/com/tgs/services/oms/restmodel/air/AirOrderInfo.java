package com.tgs.services.oms.restmodel.air;

import java.util.List;

import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.APIUserExclude;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.oms.datamodel.amendments.AmendmentChecklist;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AirOrderInfo extends BaseResponse {

	private List<TripInfo> tripInfoList;

	@APIUserExclude
	private AirSearchQuery searchQuery;

	private String bookingId;

	private AmendmentType amendmentType;

	@APIUserExclude
	private List<AmendmentChecklist> checklist;

}
