package com.tgs.services.oms.restmodel.air;

import java.util.List;
import javax.validation.Valid;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class AmendmentChargesResponse extends BaseResponse {

	@Valid
	private String bookingId;

	private List<AmendmentTripCharges> trips;

}
