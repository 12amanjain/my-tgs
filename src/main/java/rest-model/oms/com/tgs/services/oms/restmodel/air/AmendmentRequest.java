package com.tgs.services.oms.restmodel.air;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.validation.constraints.NotNull;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.amendments.AmendmentTrip;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AmendmentRequest implements Validatable {

	@NotNull
	private String bookingId;

	private String amendmentId;

	@NotNull
	private AmendmentType type;

	@NotNull
	private String remarks;

	private List<AmendmentTrip> trips;

	public List<AmendmentTrip> getTrips() {
		if (Objects.isNull(trips)) {
			trips = new ArrayList<>();
		}
		return trips;
	}

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap errors = new FieldErrorMap();
		if (AmendmentType.REISSUE.equals(getType())) {
			if (CollectionUtils.isEmpty(getTrips())) {
				errors.put("trips", SystemError.TRIP_SELECTION_REQ.getErrorDetail(getType()));
			}
		}
		int index = 0;
		for (AmendmentTrip trip : getTrips()) {
			errors.putAll(trip.validate(validatingData).withPrefixToFieldNames("trips[" + index++ + "]."));
		}
		return errors;
	}
}
