package com.tgs.services.oms.restmodel.air;

import java.util.List;
import java.util.Objects;

import com.tgs.services.base.helper.AirAPIExcludeV1;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.amendments.Amendment;
import com.tgs.services.oms.datamodel.amendments.AmendmentStatus;
import com.tgs.services.oms.datamodel.amendments.AmendmentTrip;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;

@Setter
@Builder
@Getter
public class PartnerAmendmentResponse extends BaseResponse {

	private String bookingId;

	private String amendmentId;

	private AmendmentStatus amendmentStatus;

	@AirAPIExcludeV1
	private Double amendmentCharges;

	private Double refundableAmount;

	private Double totalFare;

	private String remarks;

	private List<AmendmentTripCharges> trips;

	public double getAmendmentCharges() {
		if (Objects.isNull(amendmentCharges)) {
			amendmentCharges = new Double(0.0);
		}
		return amendmentCharges;
	}

	public double getRefundableAmount() {
		if (Objects.isNull(refundableAmount)) {
			refundableAmount = new Double(0.0);
		}
		return refundableAmount;
	}

	public double getTotalFare() {
		if (Objects.isNull(totalFare)) {
			totalFare = new Double(0.0);
		}
		return totalFare;
	}

	public Double getPaxRefundableAmount(Amendment amendment) {
		Double refundableAmount = Double.valueOf(0);
		if (CollectionUtils.isNotEmpty(trips)) {
			for (AmendmentTripCharges amendmentTrip : trips) {
				refundableAmount += amendmentTrip.getTravellersRefundableAmount(amendment);
			}
		}
		return refundableAmount;
	}
}
