package com.tgs.services.oms.restmodel.air;

import com.tgs.services.oms.datamodel.air.AirOrderItem;
import lombok.Getter;
import java.util.List;
import java.util.Set;

@Getter
public class ProcessAirAmendmentRequest {

    private String amendmentId;

    private List<AirOrderItem> modItems;

    private String note;

    private Set<Short> checked;

    private boolean recallCommission;
}
