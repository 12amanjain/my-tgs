package com.tgs.services.oms.restmodel.air;

import lombok.Getter;

import java.time.LocalDate;
import java.util.List;

@Getter
public class SSHRequest {

    private List<String> amdIds;

    private LocalDate date;
}
