package com.tgs.services.oms.restmodel.hotel;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class HotelAmendOrderRequest extends HotelPostBookingBaseRequest {

}
