package com.tgs.services.oms.restmodel.hotel;

import java.util.Set;
import com.tgs.services.base.enums.AmendmentType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelAmendmentRequest {

	private String bookingId;

	private AmendmentType type;

	private String remarks;
	
	private Set<String> roomKeys;

}
