package com.tgs.services.oms.restmodel.hotel;

import com.tgs.services.oms.datamodel.OrderDisputeInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingDisputeRequest {

	private String bookingId;
	private String email;
	private OrderDisputeInfo disputeInfo;
}
