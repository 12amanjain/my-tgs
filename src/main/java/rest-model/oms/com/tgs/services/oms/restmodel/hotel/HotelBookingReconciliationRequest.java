package com.tgs.services.oms.restmodel.hotel;

import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.oms.datamodel.ReconciliationStatus;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingReconciliationRequest {

	private String bookingId; 
	private FetchType fetchType;
	private ReconciliationStatus reconciliationStatus;
	private Boolean processedOnUpdate;
}
