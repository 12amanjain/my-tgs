package com.tgs.services.oms.restmodel.hotel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.oms.datamodel.hotel.HotelReconciliationResult;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingReconciliationResponse extends BaseResponse {

	private String bookingId;
	private String supplier;
	private String bookingRefNo;
	private String hotelName;
	private String description;
	private List<HotelReconciliationResult> hotelInfos;
	private List<List<HotelReconciliationResult>> roomInfos;

}
