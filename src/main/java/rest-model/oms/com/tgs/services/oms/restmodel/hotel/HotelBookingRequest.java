package com.tgs.services.oms.restmodel.hotel;

import java.util.List;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.hms.datamodel.RoomSSR;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.oms.restmodel.BookingRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelBookingRequest extends BookingRequest {
	private List<RoomTravellerInfo> roomTravellerInfo;
	
	@SerializedName("ssr")
	private List<RoomSSR> roomSSR;
	
}
