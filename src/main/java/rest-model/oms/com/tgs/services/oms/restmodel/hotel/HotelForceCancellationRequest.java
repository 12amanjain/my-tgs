package com.tgs.services.oms.restmodel.hotel;

import com.google.gson.annotations.SerializedName;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelForceCancellationRequest {
	private String bookingid;
	private double amount;
	private String reason;
	@SerializedName("irws")
	private boolean isReconciledWithSupplier;
}
