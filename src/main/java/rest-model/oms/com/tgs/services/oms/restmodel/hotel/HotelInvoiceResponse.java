package com.tgs.services.oms.restmodel.hotel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.restmodel.InvoiceResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelInvoiceResponse extends InvoiceResponse {

    private HotelInfo hotel;

    @SerializedName("iid")
	private String invoiceId;
    
    @SerializedName("cnf")
    private String confirmationNumber;

    @SerializedName("at")
    private AmendmentType amendmentType; 

	
}
