package com.tgs.services.oms.restmodel.hotel;

import java.util.List;
import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import com.tgs.services.oms.restmodel.BookingRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelManualBookingRequest extends BookingRequest {

	private List<HotelOrderItem> hotelOrderItems;

	private String userId;

}
