package com.tgs.services.oms.restmodel.hotel;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelModifyBookingRequest extends HotelPostBookingBaseRequest {

}
