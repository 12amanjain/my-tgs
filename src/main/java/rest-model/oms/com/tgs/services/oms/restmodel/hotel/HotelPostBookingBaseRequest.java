package com.tgs.services.oms.restmodel.hotel;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.oms.restmodel.BookingRequest;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelPostBookingBaseRequest  extends BookingRequest{


	private HotelInfo hInfo;
	private HotelSearchQuery searchQuery;
	private String supplier;
	private String supplierBookingId;
	private String hotelBookingReference;
	private Double totalMarkup;
	private String userId;
	private String remark;
	
}
