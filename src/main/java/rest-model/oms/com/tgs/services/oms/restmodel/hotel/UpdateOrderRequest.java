package com.tgs.services.oms.restmodel.hotel;

import java.time.LocalDate;
import java.util.List;
import com.tgs.services.oms.restmodel.hotel.HotelImportBookingRequest;
import lombok.Data;

@Data
public class UpdateOrderRequest {

	private List<HotelImportBookingRequest> bookingUpdateRequest;
	
	private LocalDate createdAfterDate;
}
