package com.tgs.services.pms.restmodel;

import java.math.BigDecimal;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class CreditLineMigrationRequest extends BulkUploadQuery{

	private String userId;

	@SerializedName("cl")
	private BigDecimal creditLimit;

	@SerializedName("mte")
	private BigDecimal maxTemporaryExt;
	
	@SerializedName("mrs")
	List<CreditLineMigrationRequest> migrationRequests;
}
