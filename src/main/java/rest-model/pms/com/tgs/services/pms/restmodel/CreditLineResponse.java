package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.CreditLine;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
public class CreditLineResponse extends BaseResponse {

    private List<CreditLine> creditLines;

    public CreditLineResponse(List<CreditLine> creditLines) {
        this.creditLines = creditLines;
    }

    public CreditLineResponse(CreditLine creditLine) {
        this.creditLines = new ArrayList<>();
        creditLines.add(creditLine);
    }

}
