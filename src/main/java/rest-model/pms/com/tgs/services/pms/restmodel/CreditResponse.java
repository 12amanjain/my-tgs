package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.Credit;

import lombok.Setter;

import java.util.List;

@Setter
public class CreditResponse extends BaseResponse {

    private List<Credit> credits;

    public CreditResponse(List<Credit> credits) {
        this.credits = credits;
    }
}
