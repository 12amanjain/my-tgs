package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.DIPayment;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class DIPaymentResponse extends BaseResponse {

   private List<DIPayment> paymentList;

    public DIPaymentResponse(DIPayment payment) {
        this.paymentList = new ArrayList<>();
        this.paymentList.add(payment);
    }

    public DIPaymentResponse(List<DIPayment> diPayments) {
        this.paymentList = diPayments;
    }
    public DIPaymentResponse() {
        this.paymentList = new ArrayList<>();
    }
}
