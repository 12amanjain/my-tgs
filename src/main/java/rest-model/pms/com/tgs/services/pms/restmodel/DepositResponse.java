package com.tgs.services.pms.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.DepositRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepositResponse extends BaseResponse {

	private List<DepositRequest> depositRequests;

}

