package com.tgs.services.pms.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.ExternalPayment;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExternalPaymentResponse extends BaseResponse {

	private List<ExternalPayment> paymentList;

}
