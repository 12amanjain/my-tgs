package com.tgs.services.pms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.ExternalPaymentStatusInfo;
import lombok.Setter;

@Setter
public class ExternalPaymentStatusResponse extends BaseResponse {

	@SerializedName("psi")
	private ExternalPaymentStatusInfo paymentStatusInfo;
	
}
