package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
public class InternalResponse extends BaseResponse {

    private List<UtilizationSum> summary;

    // userIds
    private List<String> unPolicedCreditUsers;

    public InternalResponse(List<UtilizationSum> summary) {
        this.summary = summary;
    }

    public InternalResponse() {
        this.summary = new ArrayList<>();
        this.unPolicedCreditUsers = new ArrayList<>();
    }

}
