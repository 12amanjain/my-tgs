package com.tgs.services.pms.restmodel;

import lombok.Getter;
import lombok.Setter;
import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;

@Getter
@Setter
public class MoneyExchangeListResponse extends BaseResponse{

	List<MoneyExchangeInfo> moneyExchangeList;

}
