package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.MoneyExchangeInfo;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneyExchangeRequest extends BaseResponse {
	
	private MoneyExchangeInfo moneyExchangeInfo;

}
