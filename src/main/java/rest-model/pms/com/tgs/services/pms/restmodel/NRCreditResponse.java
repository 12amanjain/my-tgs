package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.NRCredit;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class NRCreditResponse extends BaseResponse {

    private List<NRCredit> credits;

    public NRCreditResponse(List<NRCredit> credits) {
        this.credits = credits;
    }

    public NRCreditResponse(NRCredit credit) {
        this.credits = new ArrayList<>();
        credits.add(credit);
    }
}
