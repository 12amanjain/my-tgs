package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.pms.datamodel.Payment;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class PaymentResponse extends BaseResponse {

    List<Payment> paymentList;

    public PaymentResponse(List<Payment> paymentList) {
        this.paymentList = paymentList;
    }

    public PaymentResponse(Payment payment) {
        this.paymentList = new ArrayList<>();
        this.paymentList.add(payment);
    }
}
