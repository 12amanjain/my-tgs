package com.tgs.services.pms.restmodel;

import com.tgs.services.base.restmodel.BaseResponse;
import lombok.Getter;
import lombok.Setter;
import java.time.LocalDateTime;

@Getter
@Setter
public class RedeemPointsResponse extends BaseResponse {

	private LocalDateTime expiryTime;

	private Double amount;

	public Double getAmount() {
		if (amount == null) {
			amount = Double.valueOf(0);
		}
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = getAmount() + amount;
	}

	public RedeemPointsResponse() {}

}
