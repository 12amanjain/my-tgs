package com.tgs.services.pms.restmodel;

import java.time.LocalDateTime;
import lombok.Getter;
import javax.validation.constraints.NotNull;

import com.tgs.services.pms.datamodel.CreditStatus;

@Getter
public class UpdateBillRequest {

	@NotNull
	private String billNumber;

	private LocalDateTime newLockDate;
	
	private CreditStatus status;
}
