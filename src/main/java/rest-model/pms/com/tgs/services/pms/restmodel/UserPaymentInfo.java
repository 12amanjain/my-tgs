package com.tgs.services.pms.restmodel;

import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class UserPaymentInfo extends BaseResponse {
	@SerializedName("bs")
	private UserBalanceSummary balanceSummary;
}
