package com.tgs.services.pms.restmodel;

import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.base.enums.PointsType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserPointsUploadRequest extends BulkUploadQuery {

	private String userId;

	private PointsType type;

	private Double value;

}
