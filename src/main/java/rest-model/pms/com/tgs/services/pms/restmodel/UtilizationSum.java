package com.tgs.services.pms.restmodel;

import com.tgs.services.pms.datamodel.CreditType;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
public class UtilizationSum {

    private String userId;

    private BigDecimal utilizationSum;

    private BigDecimal pendingBillSum;

    private CreditType creditType;

    public UtilizationSum(String userId, BigDecimal utilizationSum, BigDecimal pendingBillSum, CreditType creditType) {
        this.userId = userId;
        this.utilizationSum = utilizationSum;
        this.pendingBillSum = pendingBillSum;
        this.creditType = creditType;
    }
}
