package com.tgs.services.points.restmodel;


import java.util.List;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AirRedeemPointResponse extends RedeemPointsResponse {

	List<TripInfo> tripInfos;

	public AirRedeemPointResponse() {}

}
