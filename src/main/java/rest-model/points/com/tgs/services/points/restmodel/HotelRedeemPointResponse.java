package com.tgs.services.points.restmodel;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelRedeemPointResponse extends RedeemPointsResponse {

	HotelInfo hotelInfo;

	public HotelRedeemPointResponse() {

	}
}