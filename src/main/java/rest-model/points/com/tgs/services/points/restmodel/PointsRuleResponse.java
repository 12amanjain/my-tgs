package com.tgs.services.points.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.points.datamodel.PointsConfigurationRule;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PointsRuleResponse extends BaseResponse {

	@SerializedName("pc")
	private List<PointsConfigurationRule> pointsConfigurations;

	public List<PointsConfigurationRule> getPointsConfigurations() {
		if (pointsConfigurations == null) {
			pointsConfigurations = new ArrayList<>();
		}
		return pointsConfigurations;
	}
}
