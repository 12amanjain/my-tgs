package com.tgs.services.ps.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ps.datamodel.PolicyGroup;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PolicyGroupResponse extends BaseResponse {

	@SerializedName("pgs")
	private List<PolicyGroup> policyGroups;

	public List<PolicyGroup> getPolicyGroups() {
		if (policyGroups == null) {
			policyGroups = new ArrayList<>();
		}
		return policyGroups;
	}
}
