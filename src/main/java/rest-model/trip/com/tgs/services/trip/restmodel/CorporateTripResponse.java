package com.tgs.services.trip.restmodel;

import java.util.ArrayList;
import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.trip.datamodel.CorporateTrip;

public class CorporateTripResponse extends BaseResponse {

	@SerializedName("trs")
	private List<CorporateTrip> tripRequests;

	public List<CorporateTrip> getTripRequests() {
		if (tripRequests == null) {
			tripRequests = new ArrayList<>();
		}
		return tripRequests;
	}
}
