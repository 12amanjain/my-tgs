package com.tgs.services.ums.restmodel;

import com.tgs.services.base.BulkUploadQuery;
import lombok.Getter;
import lombok.Setter;
import java.util.Map;

@Getter
@Setter
public class Employee extends BulkUploadQuery {

    private Map<String, Object> data;
}
