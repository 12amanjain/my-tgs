package com.tgs.services.ums.restmodel;

import java.time.LocalDateTime;
import java.util.StringJoiner;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.BulkUploadQuery;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.FieldErrorMap;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.datamodel.Validatable;
import com.tgs.services.base.datamodel.ValidatingData;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.ums.datamodel.ContactPersonInfo;
import com.tgs.services.ums.datamodel.GSTInfo;
import com.tgs.services.ums.datamodel.PanInfo;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RegisterRequest extends BulkUploadQuery implements Validatable<ValidatingData> {

	private String userId;
	@NotNull
	@ApiModelProperty(required = true)
	private String name;
	@NotNull
	@ApiModelProperty(required = true, notes = "Make sure to convert email into lowercase",
			example = "ashu.gupta@technogramsolutions.com")
	private String email;
	@ApiModelProperty(required = true,
			notes = "Minimum length should be 5 and max can be 20, should consist of alphanumberic characters")
	private String password;
	@NotNull
	@ApiModelProperty(required = true,
			notes = "It shouldn't have country code. Correct way is 9379433592, Incorrect way is +91-9379433592",
			example = "9379433592")
	private String mobile;
	@ApiModelProperty(
			notes = "Please add local STD code as well. In case of phone number is not available then pass mobile number without country code",
			required = true, example = "01126479985")
	private String phone;
	@NotNull
	@ApiModelProperty(value = "List of Allowed roles are AGENT, DISTRIBUTOR, ADMIN, ACCOUNTS, SALES", required = true,
			example = "AGENT")
	private UserRole role;

	@Valid
	private GSTInfo gstInfo;
	@Valid
	private PanInfo panInfo;
	@Valid
	private AddressInfo addressInfo;
	@Valid
	private ContactPersonInfo contactPersonInfo;
	@Valid
	private UserAdditionalInfo additionalInfo;

	private String parentUserId;

	private LocalDateTime createdOn;

	private boolean enabled;

	private Boolean sendEmailSms;

	@Valid
	private UserProfile userProfile;

	private String employeeId;

	public UserProfile getUserProfile() {
		if (userProfile == null)
			userProfile = UserProfile.builder().build();
		return userProfile;
	}

	@Override
	public FieldErrorMap validate(ValidatingData validatingData) {
		FieldErrorMap fieldErrorMap = new FieldErrorMap();
		String field = null;
		StringJoiner sj = new StringJoiner(", ");
		if (role == null) {
			sj.add(field = "role");
		}
		if (StringUtils.isBlank(mobile)) {
			sj.add(field = "mobile");
		}
		if (StringUtils.isBlank(email)) {
			sj.add(field = "email");
		}
		if (StringUtils.isBlank(name)) {
			sj.add(field = "name");
		}
		if (StringUtils.isBlank(password)) {
			sj.add(field = "password");
		}
		if (field != null) {
			fieldErrorMap.put(field, SystemError.INVALID_REGISRATION_REQUEST.getErrorDetail(sj.toString()));
		}
		return fieldErrorMap;
	}

}
