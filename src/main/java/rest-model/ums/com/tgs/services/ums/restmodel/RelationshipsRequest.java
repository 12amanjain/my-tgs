package com.tgs.services.ums.restmodel;

import java.util.List;
import com.tgs.services.base.BulkUploadQuery;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class RelationshipsRequest extends BulkUploadQuery{

	private String userId;
	
	private List<String> relatedUserIds;
	
}
