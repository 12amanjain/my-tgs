package com.tgs.services.ums.restmodel;

import javax.validation.constraints.NotNull;

import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gms.OtpValidateRequest;
import com.tgs.services.base.restmodel.BaseRequest;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SignInRequest extends BaseRequest {
	@NotNull
	@ApiModelProperty(required = true)
	private String password;
	@NotNull
	@ApiModelProperty(value = "In case of Mobile it should be without country code. In case of email all characters should be in lower case", required = true, example = "9379433592")
	private String username;
	@NotNull
	@ApiModelProperty(value = "List of Allowed roles are AGENT, DISTRIBUTOR, ADMIN, ACCOUNTS, SALES", required = true, example = "AGENT")
	private UserRole role;
		
	private OtpValidateRequest otpValidateRequest;
}
