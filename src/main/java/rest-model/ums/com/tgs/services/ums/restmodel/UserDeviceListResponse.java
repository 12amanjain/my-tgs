package com.tgs.services.ums.restmodel;

import java.util.List;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.UserDeviceInfo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class UserDeviceListResponse extends BaseResponse {
	private List<UserDeviceInfo> userDeviceInfo;
}
