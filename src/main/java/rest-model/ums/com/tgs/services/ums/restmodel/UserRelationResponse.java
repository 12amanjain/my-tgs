package com.tgs.services.ums.restmodel;


import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.UserRelation;
import java.util.List;

public class UserRelationResponse extends BaseResponse {

	public UserRelationResponse(List<UserRelation> userRelations) {
		this.userRelations = userRelations;
	}

	private List<UserRelation> userRelations;
}
