package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.ums.datamodel.bulkUpload.DistributorIdUpdate;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DistributorIdUpdateRequest extends UserUpdateRequest {

	private List<DistributorIdUpdate> updateQuery;

	@Override
	public List<DistributorIdUpdate> getUpdateQuery() {
		return updateQuery;
	}
}
