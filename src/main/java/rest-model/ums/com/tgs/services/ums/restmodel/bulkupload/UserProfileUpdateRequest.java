package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.ums.datamodel.bulkUpload.UserProfileUpdateQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserProfileUpdateRequest extends UserUpdateRequest {

	private List<UserProfileUpdateQuery> updateQuery;

	@Override
	public List<UserProfileUpdateQuery> getUpdateQuery() {
		return updateQuery;
	}
}
