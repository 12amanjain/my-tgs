package com.tgs.services.ums.restmodel.bulkupload;

import java.util.ArrayList;
import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.ums.restmodel.RegisterRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRegistrationRequest extends BulkUploadRequest {

	private List<RegisterRequest> registerQuery;

	public List<RegisterRequest> getRegisterQuery() {
		if (registerQuery == null)
			registerQuery = new ArrayList<>();
		return registerQuery;
	}

}
