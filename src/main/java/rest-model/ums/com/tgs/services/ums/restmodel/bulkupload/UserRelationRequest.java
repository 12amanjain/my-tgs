package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.ums.restmodel.RelationshipsRequest;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRelationRequest extends BulkUploadRequest {

	private List<RelationshipsRequest> relations;
	
}
