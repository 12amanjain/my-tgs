package com.tgs.services.ums.restmodel.bulkupload;

import java.util.List;
import com.tgs.services.base.datamodel.BulkUploadRequest;
import com.tgs.services.base.gson.ClassType;
import com.tgs.services.base.gson.GsonPolymorphismMapping;
import com.tgs.services.base.gson.GsonRunTimeAdaptorRequired;
import com.tgs.services.ums.datamodel.bulkUpload.UserUpdateQuery;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@GsonRunTimeAdaptorRequired(dependOn = "updateField")
@GsonPolymorphismMapping({@ClassType(keys = {"distributorId"}, value = DistributorIdUpdateRequest.class),
		@ClassType(keys = {"grade"}, value = GradeUpdateRequest.class),
		@ClassType(keys = {"commPlanMap"}, value = CommissionPlanUpdateRequest.class),
		@ClassType(keys = {"userProfile"}, value = UserProfileUpdateRequest.class)})
public abstract class UserUpdateRequest extends BulkUploadRequest {

	private String updateField;

	public abstract List<? extends UserUpdateQuery> getUpdateQuery();

}
