package com.tgs.services.vms.restmodel;

import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import java.util.List;


@Getter
@Setter
public class FlightVoucherValidateRequest extends VoucherValidateRequest {

	List<TripInfo> tripInfos;

	public FlightVoucherValidateRequest(String bookingId, String voucherCode, Product product,
			List<PaymentMedium> mediums, List<VoucherConfiguration> configurations) {
		this.setBookingId(bookingId);
		this.setVoucherCode(voucherCode);
		this.setMediums(mediums);
		this.setProduct(product);
		this.setConfigurations(configurations);
	}

	public FlightVoucherValidateRequest() {}
}
