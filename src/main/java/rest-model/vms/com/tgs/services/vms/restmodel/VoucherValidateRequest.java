package com.tgs.services.vms.restmodel;

import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.restmodel.BaseRequest;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

@Getter
@Setter
public class VoucherValidateRequest extends BaseRequest {

	private String bookingId;

	private String voucherCode;

	private Product product;

	private List<PaymentMedium> mediums;

	private transient List<VoucherConfiguration> configurations;

	public VoucherValidateRequest() {}

	public VoucherValidateRequest(String bookingId, String voucherCode, Product product, List<PaymentMedium> mediums) {
		this.setBookingId(bookingId);
		this.setMediums(mediums);
		this.setProduct(product);
		this.setVoucherCode(voucherCode);
	}

}
