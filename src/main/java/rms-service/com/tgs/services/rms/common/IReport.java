package com.tgs.services.rms.common;

import java.util.List;

public interface IReport {

	public List getResultsFromDb();

	public String getValuePartForEmail();

	default public List getProcessedResults(List results, List<String> outputFields) {
		return results;
		/**
		 List<String> rowdata = new ArrayList<>();
		 Map<String, Object> map = TgsObjectUtils.getNotNullFieldValueMap(obj, true, true);
			outputFields.forEach(field-> {
				rowdata.add((String) map.get(field));
		});
		data.add(rowdata.toArray(new String[0])); 
		 */
	}

}
