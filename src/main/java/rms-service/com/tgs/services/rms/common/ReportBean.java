package com.tgs.services.rms.common;

import com.tgs.services.rms.servicehandler.*;

import lombok.Getter;


@Getter
public enum ReportBean {

	DATABASEQUERY(DynamicReportsHandler.class),
	AIRLINE_TRANSACTION(AirLineTransactionReportHandler.class),
	BOOKING_TRANSACTION(BookingTransactionReportHandler.class),
	HOTEL_TRANSACTION(HotelTransactionReportHandler.class),
	DSR(DSRHandler.class),
	DIGI_TOOL(DigiToolReportHandler.class),
	AMEX_REPORT(AmexReportHandler.class);

	private Class classType;

	private ReportBean(Class classType) {
		this.classType = classType;
	}

	
}
