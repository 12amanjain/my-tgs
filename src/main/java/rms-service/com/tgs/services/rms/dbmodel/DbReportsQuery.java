package com.tgs.services.rms.dbmodel;

import java.time.LocalDateTime;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.rms.datamodel.ReportsQuery;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity(name = "reportsquery")
@Table(name = "reportsquery")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DbReportsQuery extends BaseModel<DbReportsQuery, ReportsQuery>{

	@Column
	private String query;
	
	@Column
	@CreationTimestamp
	private LocalDateTime createdOn;

	@Column
	private String columns;
	
	@Column
	private String description;
	
	@Override
    public ReportsQuery toDomain() {
        return new GsonMapper<>(this, ReportsQuery.class).convert();
    }

    @Override
    public DbReportsQuery from(ReportsQuery dataModel) {
        return new GsonMapper<>(dataModel, this, DbReportsQuery.class).convert();
    }
    
    public Map<String,String> getColumnMap(){
		return new Gson().fromJson(
				columns, new TypeToken<Map<String,String>>() {}.getType()
			);
    }
}
