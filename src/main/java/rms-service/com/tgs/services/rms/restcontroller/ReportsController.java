package com.tgs.services.rms.restcontroller;

import java.time.LocalDateTime;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.runtime.FiltersValidator;
import com.tgs.services.rms.common.ReportBean;
import com.tgs.services.rms.datamodel.ReportFilter;
import com.tgs.services.rms.datamodel.ReportFilterValidator;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.rms.servicehandler.DynamicReportsHandler;
import com.tgs.services.rms.servicehandler.ReportHandler;
import com.tgs.utils.file.TgsFileUtils;

@RestController
@RequestMapping("/rms/v1")
@Scope(value = "session")
public class ReportsController {

	@Autowired
	ReportFilterValidator filterValidator;

	@Autowired
	private FiltersValidator validator;

	@InitBinder
	public void initBinderForAirBookingRequest(WebDataBinder binder) {
		binder.addValidators(filterValidator, validator);
	}

	@RequestMapping(value = "/report", method = RequestMethod.POST)
	protected ReportResponse reports(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid ReportFilter filter, BindingResult result) throws Exception {
		ReportResponse reportResponse = getAndSendReport(filter, result);
		if (CollectionUtils.isNotEmpty(reportResponse.getErrors())) {
			return reportResponse;
		}
		String fileName = "";
		if (filter.getReportType() != null) {
			fileName = filter.getReportType().name();
		}
		String outputfile = fileName.toLowerCase().concat("_Report_")
				.concat(DateFormatterHelper.formatDateTime(LocalDateTime.now(), DateFormatType.REPORT_FORMAT))
				.concat(".").concat(filter.getOutputFormat().getCode());
		TgsFileUtils.writeToResponseOutputStream(response, outputfile, reportResponse.getByteStream());
		return reportResponse;
	}

	@RequestMapping(value = "/job/report", method = RequestMethod.POST)
	protected @ResponseBody ReportResponse reportsJob(@RequestBody @Valid ReportFilter filter, BindingResult result)
			throws Exception {
		ReportResponse reportResponse = getAndSendReport(filter, result);
		if (CollectionUtils.isNotEmpty(reportResponse.getErrors())) {
			return reportResponse;
		}
		return ReportResponse.builder().build();
	}

	private ReportResponse getAndSendReport(ReportFilter filter, BindingResult result) throws Exception {
		ReportResponse reportResponse = ReportResponse.builder().build();
		if (result.hasErrors()) {
			reportResponse.setErrors(validator.getErrorDetailFromBindingResult(result));
			return reportResponse;
		}
		ReportBean bean = null;
		if (filter.getReportType() != null) {
			bean = ReportBean.valueOf(filter.getReportType().name());
		}
		ReportHandler handler = (ReportHandler) SpringContext.getApplicationContext()
				.getBean(bean != null ? bean.getClassType() : DynamicReportsHandler.class);
		handler.initData(filter, reportResponse);
		reportResponse = (ReportResponse) handler.getResponse();
		return reportResponse;
	}

}
