package com.tgs.services.rms.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.rms.datamodel.AirLineTransactionReportFilter;
import com.tgs.services.rms.datamodel.AirLineTransactionResponse;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;

import helper.ProcessedPayment;
import helper.ReportOrderItem;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AirLineTransactionReportHandler
		extends TransactionReportHandler<AirLineTransactionReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	UserServiceCommunicator userCommunicator;

	@SuppressWarnings("unchecked")
	@Override
	public List getResultsFromDb() {
		List<AirLineTransactionResponse> finalResponse = new ArrayList<>();

		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null) {
			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder paymentQuery = constructPaymentQuery();
			List<Object[]> payments =
					entityManager.unwrap(Session.class).createNativeQuery(paymentQuery.toString()).getResultList();
			log.debug("ATR : Fetched payment entries from db");
			entityManager.close();
			Set<String> users = new HashSet<>();
			// Query Optimization : Done for the time being to save all partition scans. These dates will be passed in
			// all db queries.
			// Only case which it wont be able to handle is when Booking/Amendment is done 3 months ago - and payment is
			// being done now (which is rarest of all cases)
			LocalDateTime createdOnAfterForAllDbQueries = request.getCreatedOnAfterDateTime().minusMonths(3);
			LocalDateTime createdOnBeforeForAllDbQueries = request.getCreatedOnBeforeDateTime();
			payments.forEach(payment -> {
				ProcessedPayment pPayment = ProcessedPayment.builder().paymentMedium((String) payment[2])
						.userId((String) payment[3]).merchantTxnId((String) payment[4]).build();
				if (payment[0] != null)
					constructPaymentMap((String) payment[0], pPayment, amendmentPaymentMap);
				String bookingId = (String) payment[1];
				if (bookingId != null && (bookingId.matches("\\D*10.*") || bookingId.matches("\\D*11.*")
						|| bookingId.matches("\\D*50.*") || bookingId.matches("\\D*51.*"))) {
					constructPaymentMap(bookingId, pPayment, bookingPaymentMap);
				}
				users.add((String) payment[3]);
			});

			bookingUsers = userCommunicator
					.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(users)).build());
			filterPaymentByRoles(amendmentPaymentMap, bookingPaymentMap);
			salesHierarchy = userCommunicator.getUserRelations(Lists.newArrayList(users));
			Set<String> totalBookingIds = new HashSet<>(bookingPaymentMap.keySet());
			Set<String> totalAmendments = new HashSet<>(amendmentPaymentMap.keySet());

			log.debug("[Reports] Booking ids found {} with size {} ", totalBookingIds.toString(),
					totalBookingIds.size());
			log.debug("[Reports] Amendment ids found {} with size {}  ", totalAmendments.toString(),
					totalAmendments.size());
			if (MapUtils.isNotEmpty(bookingPaymentMap)) {

				// Booking ids with amendments. Fetch First amendment corresponding to booking id,and booking from old
				// snapshot of amendment
				StringBuilder bookingWithAmendment = constructAmendmentQuery(totalBookingIds,
						createdOnBeforeForAllDbQueries.plusMonths(3), createdOnAfterForAllDbQueries);
				entityManager = em.getEntityManagerFactory().createEntityManager();
				List<DbAmendment> bookingAmendments =
						entityManager.unwrap(Session.class).createNativeQuery(bookingWithAmendment.toString())
								.addEntity("a", DbAmendment.class).getResultList();
				log.debug("ATR : Fetched bookings with amendments from db with size{} ", bookingAmendments.size());
				// Query Optimization : If payment of amendment was done today and booking was done 6 months ago, then
				// order fields should come for that booking. If booking was done
				// before 6 months,orde fields will not come.
				Map<String, DbOrder> ordersMap = constructOrdersMap(totalBookingIds, em, createdOnBeforeForAllDbQueries,
						createdOnAfterForAllDbQueries.minusMonths(3));
				log.debug("ATR : Fetched orders from db with size {} ", ordersMap.size());
				bookingAmendments.forEach(bookingAmendment -> {
					finalResponse.addAll(constructRowsforBookingsWithAmendments(
							StringUtils.isBlank(request.getSelectionType())
									|| request.getSelectionType().equals("amendment"),
							totalBookingIds, totalAmendments, bookingAmendment, ordersMap,
							request.getCreatedOnAfterDateTime(), request.getCreatedOnBeforeDateTime()));
				});
				log.debug("ATR : Constructed booking rows with amendments");
				entityManager.close();

				// Booking ids without amendments
				if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("booking")) {
					StringBuilder orderQuery = constructOrderQuery(totalBookingIds, createdOnBeforeForAllDbQueries,
							createdOnAfterForAllDbQueries);
					entityManager = em.getEntityManagerFactory().createEntityManager();
					List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
							.addEntity("a", DbAirOrderItem.class).addEntity("b", DbOrder.class).getResultList();
					entityManager.close();
					log.debug("[Reports] Booking ids without amendments {} ", totalBookingIds);
					// Construct map with trip info for all passengers
					Map<String, List<ReportOrderItem>> map = new HashMap<>();
					Map<String, DbOrder> orderMap = new HashMap<>();
					for (Object[] order : orders) {
						DbAirOrderItem orderItem = (DbAirOrderItem) order[0];
						DbOrder dbOrder = (DbOrder) order[1];
						orderMap.put(dbOrder.getBookingId(), dbOrder);
						constructAirOrderRow(map, orderItem, orderItem.getBookingId());
					}

					if (orderMap.size() != totalBookingIds.size()) {
						List<String> difference = totalBookingIds.stream().filter(e -> !orderMap.keySet().contains(e))
								.collect(Collectors.toList());
						log.info("ATR : Booking ids not in results are {} ", difference.toString());
					}
					map.forEach((k, v) -> {
						v.forEach(row -> {
							String key = k.split("_")[0];
							AirLineTransactionResponse response = constructRow(row, null, bookingPaymentMap.get(key),
									orderMap.get(key), bookingUsers, salesHierarchy);
							if (response != null)
								finalResponse.add(response);
						});
					});
					log.debug("ATR : Constructed booking rows without amendments");
				}
			}

			// Only amendments
			if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("amendment")) {
				
				if (CollectionUtils.isNotEmpty(totalAmendments)) {
					entityManager = em.getEntityManagerFactory().createEntityManager();
					log.debug("[Reports] Amendment ids without bookings {} ", totalAmendments);
					StringBuilder amendMentQuery = new StringBuilder(
							"SELECT {a.*},{b.*} from amendment a JOIN orders b ON a.bookingid = b.bookingid where a.status = 'S' and b.ordertype ='A' and a.amendmentId IN ('")
									.append(Joiner.on("','").join(totalAmendments)).append("')")
									.append(" and a.createdon >= '").append(createdOnAfterForAllDbQueries.toString())
									.append("'").append(" and a.createdon <= '")
									.append(createdOnBeforeForAllDbQueries.toString()).append("'")
									.append(" and b.createdon >= '")
									.append(createdOnAfterForAllDbQueries.minusMonths(3).toString()).append("'")
									.append(" and b.createdon <= '").append(createdOnBeforeForAllDbQueries.toString())
									.append("';");
					List<Object[]> amendMentsWithOrder =
							entityManager.unwrap(Session.class).createNativeQuery(amendMentQuery.toString())
									.addEntity("a", DbAmendment.class).addEntity("b", DbOrder.class).getResultList();
					log.debug("ATR : Fetched amendments from db");
					entityManager.close();

					List<String> amendmentIdsInResults = new ArrayList<>();
					amendMentsWithOrder.forEach(amendMentOrder -> {
						DbAmendment amendMent = (DbAmendment) amendMentOrder[0];
						DbOrder dbOrder = (DbOrder) amendMentOrder[1];
						amendmentIdsInResults.add(amendMent.getAmendmentId());
						// Fetch new state from amendment and put in results
						List<AirOrderItem> airOrders =
								airAmendmentManager.getAmendmentSnapshot(amendMent.toDomain(), false);
						if (CollectionUtils.isNotEmpty(airOrders))
							constructRowForAmendmentSnapshots(finalResponse, amendMent, airOrders,
									amendMent.getAmendmentId(), dbOrder);
					});

					// Adding INFO log only to ensure that after adding 3 months check we are not missing any amendment
					if (amendmentIdsInResults.size() != totalAmendments.size()) {
						List<String> difference = totalAmendments.stream()
								.filter(e -> !amendmentIdsInResults.contains(e)).collect(Collectors.toList());
						log.info("ATR : Amendment ids not in results are {} ", difference.toString());
					}
					log.debug("ATR : Constructed booking rows of only amendments");
				}
			}
		}

		return sortResponse(finalResponse);
	}

	private StringBuilder constructOrderQuery(Set<String> totalBookingIds, LocalDateTime createdOnBeforeDateTime,
			LocalDateTime createdOnAfterDateTime) {
		StringBuilder orderQuery = new StringBuilder(
				"SELECT {a.*}, {b.*} FROM airorderitem a JOIN orders b ON a.bookingid = b.bookingid WHERE b.bookingid IN ('")
						.append(Joiner.on("','").join(totalBookingIds)).append("')").append(" and a.createdon >= '")
						.append(createdOnAfterDateTime.toString()).append("'").append(" and a.createdon <= '")
						.append(createdOnBeforeDateTime.toString()).append("'").append(" and b.createdon >= '")
						.append(createdOnAfterDateTime.toString()).append("'").append(" and b.createdon <= '")
						.append(createdOnBeforeDateTime.toString()).append("'")
						.append(" and ordertype ='A' and b.status IN ('S','C') order by a.bookingid,a.id;");
		return orderQuery;
	}

	private StringBuilder constructPaymentQuery() {
		StringBuilder paymentQuery = new StringBuilder(
				"SELECT amendmentid,refid,paymentmedium,payUserId,merchantTxnId FROM payment where status = 'S' and createdOn>='")
						.append(request.getCreatedOnAfterDateTime().toString()).append("' and createdOn<='")
						.append(request.getCreatedOnBeforeDateTime().toString()).append("'");
		List<String> allowedUserIds = getUserIds();
		if (CollectionUtils.isNotEmpty(allowedUserIds))
			paymentQuery.append("and payUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
		paymentQuery.append(";");
		return paymentQuery;
	}

	private void filterPaymentByRoles(Map<String, ProcessedPayment> amendmentPaymentMap,
			Map<String, ProcessedPayment> bookingPaymentMap) {
		List<String> allowedRoles = getUserRoles();
		if (CollectionUtils.isNotEmpty(allowedRoles)) {

			if (MapUtils.isNotEmpty(amendmentPaymentMap)) {
				amendmentPaymentMap.entrySet().removeIf(
						entry -> (!allowedRoles.contains(bookingUsers.get(entry.getValue().getUserId()) != null
								? bookingUsers.get(entry.getValue().getUserId()).getRole().getRoleDisplayName()
								: StringUtils.EMPTY)));
			}

			if (MapUtils.isNotEmpty(bookingPaymentMap)) {
				bookingPaymentMap.entrySet().removeIf(
						entry -> (!allowedRoles.contains(bookingUsers.get(entry.getValue().getUserId()) != null
								? bookingUsers.get(entry.getValue().getUserId()).getRole().getRoleDisplayName()
								: StringUtils.EMPTY)));
			}
		}
	}

	private void constructRowForAmendmentSnapshots(List<AirLineTransactionResponse> finalResponse,
			DbAmendment amendMent, List<AirOrderItem> airOrders, String id, DbOrder order) {
		Map<String, List<ReportOrderItem>> ordersMap = new HashMap<>();
		List<DbAirOrderItem> dbairOrders = new DbAirOrderItem().toDbList(airOrders);
		dbairOrders.forEach(orderItem -> {
			constructAirOrderRow(ordersMap, orderItem, id);
		});
		ordersMap.forEach((k, v) -> {
			v.forEach(row -> {
				AirLineTransactionResponse response =
						constructRow(row, amendMent, amendMent != null ? amendmentPaymentMap.get(k.split("_")[0])
								: bookingPaymentMap.get(k.split("_")[0]), order, bookingUsers, salesHierarchy);
				if (response != null)
					finalResponse.add(response);
			});
		});
	}

	@Override
	public String getCollDocTypeForFetchingFields() {
		return "reports_airline_transaction";
	}

	@Override
	public String getValuePartForEmail() {
		// TODO Auto-generated method stub
		return null;
	}

}
