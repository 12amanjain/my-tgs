package com.tgs.services.rms.servicehandler;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.base.Joiner;
import com.tgs.services.base.communicator.CorporateTripServiceCommunicator;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.oms.datamodel.AirItemDetailInfo;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.air.DbAirItemDetail;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.pms.dbmodel.DbPayment;
import com.tgs.services.rms.datamodel.AmexReportHeader;
import com.tgs.services.rms.datamodel.AmexReportResponse;
import com.tgs.services.rms.datamodel.AmexReportResponse.AirSectorData;
import com.tgs.services.rms.datamodel.AmexReportResponse.AmexReportResponseBuilder;
import com.tgs.services.rms.datamodel.AmexReportTrailer;
import com.tgs.services.rms.datamodel.ReportFilter;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.trip.datamodel.CorporateTrip;
import com.tgs.services.ums.datamodel.User;
import helper.AmexReportUtil;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AmexReportHandler extends ReportHandler<ReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	CorporateTripServiceCommunicator corpTripCommunicator;

	private static String DATE_FORMAT = "ddMMYY";

	private static Map<String, String> airlineCodeMap;

	static {
		airlineCodeMap = new HashMap<>();
		airlineCodeMap.put("UK", "228");
		airlineCodeMap.put("I5", "009");
		airlineCodeMap.put("6E", "312");
		airlineCodeMap.put("SG", "775");
		airlineCodeMap.put("G8", "008");
	}

	@SuppressWarnings("unchecked")
	@Override
	public List getResultsFromDb() {
		List<AmexReportResponse> finalResponse = new ArrayList<>();

		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		String paymentQuery = constructPaymentQuery();
		List<DbPayment> payments = entityManager.unwrap(Session.class).createNativeQuery(paymentQuery)
				.addEntity("p", DbPayment.class).getResultList();
		entityManager.close();
		log.info("Payment size is {}", payments.size());
		Map<String, List<DbPayment>> amendmentPaymentMap = new HashMap<>();
		Map<String, List<DbPayment>> bookingPaymentMap = new HashMap<>();
		Map<String, List<DbAmendment>> amendmentsMap = new HashMap<>();
		Map<String, AirItemDetailInfo> airItemDetailMap = new HashMap<>();
		Map<String, List<DbAirOrderItem>> amendmentOrdersMap = new HashMap<>();
		Map<String, List<DbAirOrderItem>> bookingWithAmendmentMap = new HashMap<>();
		payments.forEach(payment -> {
			if (payment.getAmendmentId() != null) {
				List<DbPayment> existingPayments =
						amendmentPaymentMap.getOrDefault(payment.getAmendmentId(), new ArrayList<>());
				existingPayments.add(payment);
				amendmentPaymentMap.put(payment.getAmendmentId(), existingPayments);
			} else if (payment.getRefId() != null) {
				List<DbPayment> existingPayments =
						bookingPaymentMap.getOrDefault(payment.getRefId(), new ArrayList<>());
				existingPayments.add(payment);
				bookingPaymentMap.put(payment.getRefId(), existingPayments);
			}
		});

		Set<String> bookingIds = new HashSet<>(bookingPaymentMap.keySet());
		Set<String> amendmentIds = amendmentPaymentMap.keySet();

		entityManager = em.getEntityManagerFactory().createEntityManager();
		String amendmentQuery = constructAmendmentQuery(amendmentIds, request.getCreatedOnBeforeDateTime().toString(),
				request.getCreatedOnAfterDateTime().minusMonths(5).toString());
		List<DbAmendment> amendmentDetails = entityManager.unwrap(Session.class).createNativeQuery(amendmentQuery)
				.addEntity("a", DbAmendment.class).getResultList();
		log.info("Amendments size is {}", amendmentDetails.size());
		entityManager.close();
		amendmentIds = new HashSet<>();
		for (DbAmendment amendment : amendmentDetails) {
			amendmentIds.add(amendment.getAmendmentId());
			List<DbAmendment> existingItems = amendmentsMap.getOrDefault(amendment.getBookingId(), new ArrayList<>());
			existingItems.add(amendment);
			amendmentsMap.put(amendment.getBookingId(), existingItems);
		}

		log.info("Amendments ids are {}", amendmentIds.toString());
		Set<String> bookingIdsFromAmendments = new HashSet<>(amendmentsMap.keySet());
		Set<String> bookingIdsCovered = new HashSet<>();
		Map<String, Map<String, Double>> paxWiseAmendmentCharges = new HashMap<>();
		/*
		 * Suppose b1, a1 and a2 happened on same day, Then fetch b1 from prev snapshot of a1 (prev data considered only
		 * for 1st amendment ); and new data of a1 and a2 from modified info If only a1 and a2 happened on same day,
		 * then just fetch modified infos.
		 */
		bookingIdsFromAmendments.forEach(bookingIdWithAmendment -> {
			List<DbAmendment> amendments = amendmentsMap.get(bookingIdWithAmendment);
			amendments.sort(Comparator.comparing(DbAmendment::getCreatedOn));
			amendments.forEach(amendment -> {
				String key = amendment.getBookingId().concat("_").concat(amendment.getAmendmentId());
				if (bookingIds.contains(amendment.getBookingId())
						&& !bookingIdsCovered.contains((amendment.getBookingId()))) {
					// Previous booking data . Fetch from 1st amendment only
					bookingIdsCovered.add(amendment.getBookingId());
					List<AirOrderItem> previousSnapShot =
							amendment.getAdditionalInfo().getAirAdditionalInfo().getOrderPreviousSnapshot();
					bookingWithAmendmentMap.put(amendment.getBookingId(),
							new DbAirOrderItem().toDbList(previousSnapShot));
				}
				List<DbAirOrderItem> existingItems = amendmentOrdersMap.getOrDefault(key, new ArrayList<>());
				// Current data of amendment
				List<AirOrderItem> airOrders = amendment.getModifiedInfo().getAirOrderItems();
				existingItems.addAll(new DbAirOrderItem().toDbList(airOrders));
				amendmentOrdersMap.put(key, existingItems);
				paxWiseAmendmentCharges.put(amendment.getAmendmentId(), amendment.getModifiedInfo().getPaxWiseAmount());
			});
		});

		log.info("bookingIdsFromAmendments are {} ", bookingIdsFromAmendments.toString());
		log.info("bookingIds are {} ", bookingIds.toString());
		bookingIds.addAll(bookingIdsFromAmendments);
		Map<String, CorporateTrip> corpTripMap =
				corpTripCommunicator.findByBookingIds(bookingIds.stream().collect(Collectors.toList()));
		Map<String, List<DbAirOrderItem>> airOrderItemsMap = constructAirItemDetailMap(airItemDetailMap, bookingIds);
		finalResponse.addAll(constructResponseForBookings(bookingPaymentMap, airOrderItemsMap, airItemDetailMap,
				bookingIdsFromAmendments, corpTripMap));
		finalResponse.addAll(constructResponseForAmendments(amendmentPaymentMap, airItemDetailMap, amendmentOrdersMap,
				paxWiseAmendmentCharges, corpTripMap));
		finalResponse.addAll(constructResponseForBookingsWithAmendment(bookingPaymentMap, bookingWithAmendmentMap,
				airItemDetailMap, corpTripMap));
		return finalResponse;
	}

	private Map<String, List<DbAirOrderItem>> constructAirItemDetailMap(Map<String, AirItemDetailInfo> airItemDetailMap,
			Set<String> bookingIds) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		Map<String, List<DbAirOrderItem>> airOrderItemsMap = new HashMap<>();
		String ordersQuery = constructBookingQuery(bookingIds, request.getCreatedOnBeforeDateTime().toString(),
				request.getCreatedOnAfterDateTime().minusMonths(1).toString());
		List<Object[]> airOrderItemDetails = entityManager.unwrap(Session.class).createNativeQuery(ordersQuery)
				.addEntity("a", DbAirOrderItem.class).addEntity("b", DbAirItemDetail.class).getResultList();
		airOrderItemDetails.forEach(airOrderItemDetail -> {
			DbAirOrderItem airOrderItem = (DbAirOrderItem) airOrderItemDetail[0];
			DbAirItemDetail airItemDetail = (DbAirItemDetail) airOrderItemDetail[1];
			List<DbAirOrderItem> existingItems =
					airOrderItemsMap.getOrDefault(airOrderItem.getBookingId(), new ArrayList<>());
			existingItems.add(airOrderItem);
			airOrderItemsMap.put(airOrderItem.getBookingId(), existingItems);
			if (airItemDetail != null)
				airItemDetailMap.put(airOrderItem.getBookingId(), airItemDetail.getInfo());
		});
		entityManager.close();
		return airOrderItemsMap;
	}

	private List<AmexReportResponse> constructResponseForAmendments(Map<String, List<DbPayment>> amendmentPaymentMap,
			Map<String, AirItemDetailInfo> airItemDetailMap,
			Map<String, List<DbAirOrderItem>> airOrdersWithAmendmentMap,
			Map<String, Map<String, Double>> paxWiseAmendmentCharges, Map<String, CorporateTrip> corpTripMap) {
		List<AmexReportResponse> amendmentRows = new ArrayList<>();
		for (Map.Entry<String, List<DbAirOrderItem>> entry : airOrdersWithAmendmentMap.entrySet()) {
			String amendmentId = entry.getKey().split("_")[1];
			String bookingId = entry.getKey().split("_")[0];
			List<DbPayment> paymentEntries = amendmentPaymentMap.get(amendmentId);
			Map<String, Map<String, List<AmexReportResponseBuilder>>> map = getAirOrderItemFields(entry.getValue(),
					airItemDetailMap.get(bookingId), paxWiseAmendmentCharges.get(amendmentId));
			map.forEach((k, val) -> {
				val.forEach((key, vList) -> {
					vList.forEach(v -> {
						getPaymentFields(v, paymentEntries.get(0));
						getCorpTripResponse(v, corpTripMap.get(bookingId));
						amendmentRows.add(v.build());
					});
				});
			});
		}
		return amendmentRows;
	}

	private List<AmexReportResponse> constructResponseForBookings(Map<String, List<DbPayment>> bookingPaymentMap,
			Map<String, List<DbAirOrderItem>> airOrderItemsMap, Map<String, AirItemDetailInfo> airItemDetailMap,
			Set<String> bookingIdsFromAmendments, Map<String, CorporateTrip> corpTripMap) {
		List<AmexReportResponse> bookingRows = new ArrayList<>();
		for (Map.Entry<String, List<DbAirOrderItem>> entry : airOrderItemsMap.entrySet()) {
			String bookingId = entry.getKey();
			if (!bookingIdsFromAmendments.contains(bookingId)) {
				List<DbPayment> paymentEntries = bookingPaymentMap.get(bookingId);
				Map<String, Map<String, List<AmexReportResponseBuilder>>> map =
						getAirOrderItemFields(entry.getValue(), airItemDetailMap.get(bookingId), null);
				map.forEach((k, val) -> {
					val.forEach((key, vList) -> {
						vList.forEach(v -> {
							getPaymentFields(v, paymentEntries.get(0));
							getCorpTripResponse(v, corpTripMap.get(bookingId));
							bookingRows.add(v.build());
						});
					});
				});
			}
		}
		return bookingRows;
	}

	private List<AmexReportResponse> constructResponseForBookingsWithAmendment(
			Map<String, List<DbPayment>> bookingPaymentMap, Map<String, List<DbAirOrderItem>> airOrderItemsMap,
			Map<String, AirItemDetailInfo> airItemDetailMap, Map<String, CorporateTrip> corpTripMap) {
		List<AmexReportResponse> bookingWithAmendmentRows = new ArrayList<>();
		for (Map.Entry<String, List<DbAirOrderItem>> entry : airOrderItemsMap.entrySet()) {
			String bookingId = entry.getKey();
			List<DbPayment> paymentEntries = bookingPaymentMap.get(bookingId);
			Map<String, Map<String, List<AmexReportResponseBuilder>>> map =
					getAirOrderItemFields(entry.getValue(), airItemDetailMap.get(bookingId), null);
			map.forEach((k, val) -> {
				val.forEach((key, vList) -> {
					vList.forEach(v -> {
						getPaymentFields(v, paymentEntries.get(0));
						getCorpTripResponse(v, corpTripMap.get(bookingId));
						bookingWithAmendmentRows.add(v.build());
					});
				});
			});
		}
		return bookingWithAmendmentRows;
	}

	// Constructs pax wise data for each airorderitem
	private Map<String, Map<String, List<AmexReportResponseBuilder>>> getAirOrderItemFields(
			List<DbAirOrderItem> airorderItems, AirItemDetailInfo airItemDetail,
			Map<String, Double> paxWiseAmendmentCharges) {
		Map<String, Map<String, List<AmexReportResponseBuilder>>> paxWiseMap = new HashMap<>();
		airorderItems.forEach(airorderItem -> {
			Map<String, AmexReportResponseBuilder> paxMap = new HashMap<>();
			String key = airorderItem.getAirlinecode();
			AmexReportResponseBuilder response = AmexReportResponse.builder();
			AirSectorData sectorData = response.build().new AirSectorData();
			sectorData.setAirSectorNumber(airorderItem.getAdditionalInfo().getSegmentNo() + 1);
			sectorData.setAirCarrierCode(airorderItem.getAirlinecode());
			sectorData.setDeparturePort(airorderItem.getSource());
			sectorData.setArrivalPort(airorderItem.getDest());
			AirlineInfo airlineInfo = fmsCommunicator.getAirlineInfo(airorderItem.getAirlinecode());
			boolean isLCCAirline = airlineInfo.getIsLcc();
			String airLineCode = ObjectUtils.firstNonNull(airlineCodeMap.get(airorderItem.getAirlinecode()),
					airlineInfo.getAccountingCode());
			response.airlineCode(airLineCode)
					.departureDate(TgsDateUtils.convertDateToString(airorderItem.getDepartureTime(), DATE_FORMAT))
					.airCarrierCode(airorderItem.getAirlinecode()).domOrIntl(airorderItem.getAdditionalInfo().getType())
					.destinationDescription(airorderItem.getSource().concat("/").concat(airorderItem.getDest()));
			airorderItem.getTravellerInfo().forEach(traveller -> {
				Map<FareComponent, Double> fareComponents = traveller.getFareDetail().getFareComponents();
				Double amount = ObjectUtils.firstNonNull(fareComponents.get(FareComponent.TF), 0.0);
				if (paxWiseAmendmentCharges != null) {
					amount = paxWiseAmendmentCharges.getOrDefault(airorderItem.getId() + "_" + traveller.getId(), 0.0);
				}
				response.creditOrDebit(amount < 0 ? "C" : "D");
				response.transactionCode(amount < 0 ? "SR" : "S");
				BigDecimal amt = BigDecimal.valueOf(amount).setScale(2, BigDecimal.ROUND_HALF_EVEN).abs();
				// String travelAmount = String.format("%.2f", amount);
				String travelAmount = String.valueOf(amt).replace(".", "");
				AmexReportResponseBuilder travellerResponse = response.build().toBuilder();
				travellerResponse.ticketNumber(isLCCAirline ? traveller.getPnr() : traveller.getTicketNumber())
						.travellerName(StringUtils.join(traveller.getLastName(), " ", traveller.getFirstName(), " ",
								traveller.getTitle()))
						.merchantAgentOrAirline(isLCCAirline ? "Y" : "N").travelAmount(travelAmount);
				sectorData.setAirPrimaryTicketNumber(isLCCAirline ? traveller.getPnr() : traveller.getTicketNumber());
				sectorData.setAirClass(traveller.getFareDetail().getClassOfBooking());
				getAirItemDetailFields(travellerResponse, airItemDetail, traveller.getPaxKey());
				String airSectorData = AmexReportUtil.getFieldValue(sectorData, 324);
				log.info("Sector data is {}", airSectorData);
				paxMap.put(traveller.getPaxKey(), travellerResponse);
				if (paxWiseMap.get(key) != null) {
					Map<String, List<AmexReportResponseBuilder>> paxWiseData = paxWiseMap.get(key);
					if (paxWiseData.get(traveller.getPaxKey()) != null) {
						List<AmexReportResponseBuilder> existingValues = paxWiseData.get(traveller.getPaxKey());
						// Merge rows with same carrier and return journey
						if (airorderItem.getAdditionalInfo().getSearchType().equals(SearchType.RETURN)) {
							AmexReportResponseBuilder updatedValue = travellerResponse;
							Iterator<AmexReportResponseBuilder> itr = existingValues.iterator();
							while (itr.hasNext()) {
								log.debug("Merging segment");
								AmexReportResponseBuilder existingValue = itr.next();
								if (!isNotNextSegment(existingValue.build(), airorderItem.getSource())) {
									itr.remove();
									updatedValue = existingValue;
									if (existingValue.build().getAirSectorData1() == null)
										updatedValue.airSectorData1(airSectorData);
									else if (existingValue.build().getAirSectorData2() == null)
										updatedValue.airSectorData2(airSectorData);
									else
										updatedValue.airSectorData3(airSectorData);
									String destDesc = existingValue.build().getDestinationDescription();
									String existingFare = existingValue.build().getTravelAmount();
									Double totalFare = Double.valueOf(existingFare)
											+ Double.valueOf(travellerResponse.build().getTravelAmount());
									updatedValue.destinationDescription(
											destDesc.concat("/").concat(airorderItem.getDest()));
									updatedValue.travelAmount(String.valueOf(totalFare.intValue()));
								}
							}
							existingValues.add(updatedValue);
						} else {
							existingValues.add(travellerResponse);
						}
						paxWiseData.put(traveller.getPaxKey(), existingValues);
					} else {
						travellerResponse.airSectorData(airSectorData);
						paxWiseData.put(traveller.getPaxKey(),
								Stream.of(travellerResponse).collect(Collectors.toList()));
					}
				} else {
					Map<String, List<AmexReportResponseBuilder>> paxWiseData = new HashMap<>();
					travellerResponse.airSectorData(airSectorData);
					paxWiseData.put(traveller.getPaxKey(), Stream.of(travellerResponse).collect(Collectors.toList()));
					paxWiseMap.put(key, paxWiseData);

				}
			});
		});
		return paxWiseMap;
	}

	private boolean isNotNextSegment(AmexReportResponse existingVal, String currentDeparture) {
		return existingVal.getAirSectorData3() != null
				|| (existingVal.getAirSectorData2() != null
						&& existingVal.getAirSectorData2().substring(45, 47).equals(currentDeparture))
				|| (existingVal.getAirSectorData1() != null
						&& existingVal.getAirSectorData1().substring(45, 47).equals(currentDeparture))
				|| (existingVal.getAirSectorData() != null
						&& existingVal.getAirSectorData().substring(45, 47).equals(currentDeparture));
	}

	private AmexReportResponseBuilder getPaymentFields(AmexReportResponseBuilder response, DbPayment payment) {
		response.transactionDate(TgsDateUtils.convertDateToString(payment.getCreatedOn(), DATE_FORMAT))
				.transDate(TgsDateUtils.convertDateToString(payment.getCreatedOn(), "yyyyMMdd"));
		return response;
	}

	private AmexReportResponseBuilder getCorpTripResponse(AmexReportResponseBuilder response, CorporateTrip corpTrip) {
		if (corpTrip != null) {
			response.tripId(corpTrip.getTripId());
		}
		return response;
	}

	private AmexReportResponseBuilder getAirItemDetailFields(AmexReportResponseBuilder response,
			AirItemDetailInfo itemDetail, String paxKey) {
		if (itemDetail != null) {
			Map<String, Map<String, Object>> profileData = itemDetail.getProfileData();
			Map<String, Object> paxData = profileData.getOrDefault(paxKey, new HashMap<>());
			Object department = paxData.get("departmentCodeName") != null ? paxData.get("departmentCodeName")
					: paxData.get("Department");
			response.department(department != null ? department.toString() : "");
			Object costCenter =
					paxData.get("costCenter") != null ? paxData.get("costCenter") : paxData.get("Cost Center");
			response.costCenter(costCenter != null ? costCenter.toString() : "");
			Object employeeId = paxData.get("employeeId");
			response.employeeCode(employeeId != null ? employeeId.toString() : "");
		}
		return response;
	}

	private String constructPaymentQuery() {
		StringBuilder paymentQuery = new StringBuilder(
				"SELECT * FROM payment p where status = 'S' and paymentMedium = 'VP' and additionalInfo->'ic' IS NULL and createdOn>='")
						.append(request.getCreatedOnAfterDateTime().toString()).append("' and createdOn<='")
						.append(request.getCreatedOnBeforeDateTime().toString()).append("'");
		// Can take this from job. it will be only for selective corporate users right now
		Map<String, Object> queryFields = ObjectUtils.firstNonNull(request.getQueryFields(), new HashMap<>());
		List<String> userIds = (List) queryFields.get("userIds");
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		String loggedInUserId = loggedInUser == null ? UserUtils.EMPTY_USERID : loggedInUser.getUserId();
		List<String> allowedUserIds = UserServiceHelper.checkAndReturnAllowedUserId(loggedInUserId, userIds);
		if (CollectionUtils.isNotEmpty(allowedUserIds))
			paymentQuery.append("and payUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
		paymentQuery.append(";");
		log.info("Payment query is {} ", paymentQuery.toString());
		return paymentQuery.toString();
	}

	private String constructBookingQuery(Set<String> bookingIds, String createdOnBeforeDateTime,
			String createdOnAfterDateTime) {
		StringBuilder orderQuery = new StringBuilder(
				"SELECT * from airorderitem a join airitemdetail b ON a.bookingid = b.bookingid where a.bookingid IN ('")
						.append(Joiner.on("','").join(bookingIds)).append("')").append(" and a.createdon >= '")
						.append(createdOnAfterDateTime).append("'").append(" and a.createdon <= '")
						.append(createdOnBeforeDateTime).append("' order by a.bookingid,a.id;");
		log.info("Order query is {} ", orderQuery.toString());
		return orderQuery.toString();
	}

	private String constructAmendmentQuery(Set<String> amendmentIds, String createdOnBeforeDateTime,
			String createdOnAfterDateTime) {
		StringBuilder orderQuery =
				new StringBuilder("SELECT * from amendment a where a.status = 'S' and a.amendmentid IN ('")
						.append(Joiner.on("','").join(amendmentIds)).append("')").append(" and a.createdon >= '")
						.append(createdOnAfterDateTime).append("'").append(" and a.createdon <= '")
						.append(createdOnBeforeDateTime).append("'");
		log.info("Amendment query is {} ", orderQuery.toString());
		return orderQuery.toString();
	}

	private String getAmexReportHeader() {
		String fileSequenceNo = "0000" + Calendar.getInstance().get(Calendar.DAY_OF_YEAR) + "01";
		AmexReportHeader reportHeader = AmexReportHeader.builder().recordType("0000").agentRefCode("JACK")
				.createDate(TgsDateUtils.convertDateToString(LocalDateTime.now(), DATE_FORMAT)).currencyCode("INR")
				.merchantNumber("8191486893").agentName("Tripjack PVT LTD").fileSequenceNumber(fileSequenceNo).build();
		return AmexReportUtil.getFieldValue(reportHeader);
	}

	private String getAmexReportTrailer(int airCount) {
		AmexReportTrailer trailer = AmexReportTrailer.builder().recordType("9999").airCount(String.valueOf(airCount))
				.createDate(TgsDateUtils.convertDateToString(LocalDateTime.now(), DATE_FORMAT)).build();
		return AmexReportUtil.getFieldValue(trailer);
	}

	@Override
	public String getValuePartForEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void process() throws Exception {
		String header = getAmexReportHeader();
		log.info("Header is {} ", header);
		StringBuilder filedata = new StringBuilder(header);
		filedata.append(System.getProperty("line.separator"));
		List queryResults = getResultsFromDb();
		queryResults.forEach(result -> {
			log.info("Object is {} ", GsonUtils.getGson().toJson(result));
			String str = AmexReportUtil.getFieldValue(result);
			log.info("Row {}", str);
			filedata.append(str.subSequence(0, 818));
			filedata.append(System.lineSeparator());
		});
		String trailer = getAmexReportTrailer(queryResults.size());
		filedata.append(trailer);
		log.info("Trailer is {} ", trailer);
		writeToFile(filedata.toString());

	}

	public void writeToFile(String data) {
		log.info("String is {}", data);
		String fileName = "AmexBtr_Report_"
				.concat(DateFormatterHelper.formatDateTime(LocalDateTime.now(), DateFormatType.REPORT_FORMAT))
				.concat(".").concat(".txt");
		try {
			sendMail(fileName, data.getBytes());
		} catch (Exception e) {
			log.info("Unable to send  email due to {} ", e.getMessage(), e);
		}
	}
}
