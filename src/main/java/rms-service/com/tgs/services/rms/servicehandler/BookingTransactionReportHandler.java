package com.tgs.services.rms.servicehandler;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.rms.datamodel.AirBookingTransactionReportFilter;
import com.tgs.services.rms.datamodel.AirLineTransactionResponse;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;

import helper.ProcessedPayment;
import helper.ReportOrderItem;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BookingTransactionReportHandler
		extends TransactionReportHandler<AirBookingTransactionReportFilter, ReportResponse> {

	@PersistenceContext
	private EntityManager em;

	@Autowired
	UserServiceCommunicator userCommunicator;

	@Autowired
	FMSCommunicator fmsCommunicator;

	private Set<String> users = new HashSet<>();

	@Override
	public List getResultsFromDb() {
		List<AirLineTransactionResponse> finalResponse = new ArrayList<>();
		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null
				|| request.getDepartedOnAfterDate() != null && request.getDepartedOnBeforeDate() != null
				|| request.getBookedOnAfterDateTime() != null && request.getBookedOnBeforeDateTime() != null) {

			// Query Optimization
			LocalDateTime createdOnAfterForAllDbQueries = request.getCreatedOnAfterDateTime();
			LocalDateTime createdOnBeforeForAllDbQueries = request.getCreatedOnBeforeDateTime();
			if (createdOnAfterForAllDbQueries == null) {
				if (request.getBookedOnAfterDateTime() != null)
					createdOnAfterForAllDbQueries = request.getBookedOnAfterDateTime().minusMonths(3);
				else
					createdOnAfterForAllDbQueries =
							LocalDateTime.of(request.getDepartedOnAfterDate().minusMonths(3), LocalTime.MIN);
			}
			if (createdOnBeforeForAllDbQueries == null) {
				if (request.getBookedOnBeforeDateTime() != null)
					createdOnBeforeForAllDbQueries = request.getBookedOnBeforeDateTime();
				else
					createdOnBeforeForAllDbQueries = LocalDateTime.of(request.getDepartedOnBeforeDate(), LocalTime.MAX);
			}

			EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder orderQuery =
					constructOrderQuery(createdOnBeforeForAllDbQueries, createdOnAfterForAllDbQueries);
			List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
					.addEntity("a", DbAirOrderItem.class).addEntity("b", DbOrder.class).getResultList();
			entityManager.close();
			log.debug("BTR : Fetched orders entries from db");
			Map<String, DbOrder> orderMap = new HashMap<>();
			if (CollectionUtils.isNotEmpty(orders)) {
				orders.forEach(order -> {
					DbOrder dbOrder = (DbOrder) order[1];
					orderMap.put(dbOrder.getBookingId(), dbOrder);
				});
			}
			Set<String> bookingids = new HashSet<>(orderMap.keySet());
			log.debug("BTR : Constructed order map");

			// Query Optimization : CreatedOn should never be null
			LocalDateTime createdBfr =
					request.getCreatedOnBeforeDateTime() != null ? request.getCreatedOnBeforeDateTime()
							: request.getBookedOnBeforeDateTime() != null ? request.getBookedOnBeforeDateTime()
									: createdOnBeforeForAllDbQueries;
			LocalDateTime createdAftr =
					request.getCreatedOnAfterDateTime() != null ? request.getCreatedOnAfterDateTime()
							: request.getBookedOnAfterDateTime() != null ? request.getBookedOnAfterDateTime()
									: createdOnAfterForAllDbQueries;

			// Fetch amendments and corresponding booking status from old snapshot of amendment
			entityManager = em.getEntityManagerFactory().createEntityManager();
			StringBuilder amendMentQuery = constructAmendmentQuery(
					CollectionUtils.isNotEmpty(bookingids) ? bookingids : null, createdBfr, createdAftr);
			List<DbAmendment> bookingAmendments = entityManager.unwrap(Session.class)
					.createNativeQuery(amendMentQuery.toString()).addEntity("a", DbAmendment.class).getResultList();
			entityManager.close();
			log.debug("BTR : Fetched amendment entries from db");

			Map<String, DbAmendment> amendmentWithBookings = new HashMap<>();
			Map<String, DbAmendment> onlyAmendments = new HashMap<>();
			Set<String> bookingIdsOfOnlyAmendments = new HashSet<>();

			bookingAmendments.forEach(bookingAmendment -> {
				if (bookingids.contains(bookingAmendment.getBookingId()))
					amendmentWithBookings.put(bookingAmendment.getAmendmentId(), bookingAmendment);
				else {
					onlyAmendments.put(bookingAmendment.getAmendmentId(), bookingAmendment);
					bookingIdsOfOnlyAmendments.add(bookingAmendment.getBookingId());
				}
			});

			bookingids.addAll(bookingIdsOfOnlyAmendments);
			orderMap.putAll(constructOrdersMap(bookingIdsOfOnlyAmendments, em, createdOnBeforeForAllDbQueries,
					createdOnAfterForAllDbQueries));

			log.debug("BTR : Amendments processed");

			if (BooleanUtils.isNotFalse(request.getFetchPayments())) {
				processPayments(bookingids, createdOnBeforeForAllDbQueries, createdOnAfterForAllDbQueries);
			}
			log.debug("BTR : Fetched payment entries from db");

			bookingUsers = userCommunicator
					.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(users)).build());
			salesHierarchy = userCommunicator.getUserRelations(Lists.newArrayList(users));

			amendmentWithBookings.forEach((k, v) -> {
				Set<String> set = new HashSet<>();
				set.add(k);
				finalResponse.addAll(
						constructRowsforBookingsWithAmendments(BooleanUtils.isNotFalse(request.getFetchAmendments()),
								bookingids, set, v, orderMap, null, null));
			});

			log.debug("BTR : Constructed rows of booking ids with amendments");

			onlyAmendments.forEach((k, v) -> {
				Set<String> set = new HashSet<>();
				set.add(k);
				log.debug("BTR : Fetching amendment for amendment id {} , id {} ", v.getAmendmentId(), v.getId());
				List<AirOrderItem> airOrders = airAmendmentManager.getAmendmentSnapshot(v.toDomain(), false);
				if (CollectionUtils.isNotEmpty(airOrders) && orderMap.get(v.getBookingId()) != null)
					finalResponse
							.addAll(constructRowForAmendmentSnapshots(v, airOrders, k, orderMap.get(v.getBookingId())));
			});
			log.debug("BTR : Constructed rows of only amendments");

			// Construct map with trip info for all passengers
			Map<String, List<ReportOrderItem>> map = constructTripInfo(orders, bookingids);
			map.forEach((k, v) -> {
				v.forEach(row -> {
					String key = k.split("_")[0];
					AirLineTransactionResponse response = constructRow(row, null, bookingPaymentMap.get(key),
							orderMap.get(key), bookingUsers, salesHierarchy);
					if (response != null)
						finalResponse.add(response);
				});
			});

		} else {
			throw new CustomGeneralException(SystemError.INVALID_QUERY_FILTER);
		}
		log.debug("BTR : Constructed trip info");
		List<String> allowedRoles = getUserRoles();

		// This is done to apply airline code/ depature date filter in amendment rows because while fetching amendments
		// , we can't make join on airorderitem since common fields like createdon will then have same value,
		// and also we need to take care while fetching airorder rows from amendments snapshots
		if (CollectionUtils.isNotEmpty(request.getAirlineCodes()) || request.getDepartedOnAfterDate() != null
				|| CollectionUtils.isNotEmpty(allowedRoles)) {

			return finalResponse.stream().filter(f -> {
				boolean isValid = true;
				if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
					isValid = isValid && request.getAirlineCodes().contains(f.getAirlineCode());
				if (request.getDepartedOnAfterDate() != null && request.getDepartedOnBeforeDate() != null)
					isValid = isValid
							&& (LocalDate.parse(f.getDepartureDate()).isAfter(request.getDepartedOnAfterDate())
									|| LocalDate.parse(f.getDepartureDate()).isEqual(request.getDepartedOnAfterDate()))
							&& (LocalDate.parse(f.getDepartureDate()).isBefore(request.getDepartedOnBeforeDate())
									|| LocalDate.parse(f.getDepartureDate())
											.isEqual(request.getDepartedOnBeforeDate()));
				if (CollectionUtils.isNotEmpty(allowedRoles)) {
					String bookingUserId = f.getBookingUserId();
					UserRole bookingUserRole =
							bookingUsers.getOrDefault(bookingUserId, User.builder().build()).getRole();
					if (StringUtils.isNotBlank(bookingUserId)) {
						isValid = isValid && allowedRoles.contains(
								bookingUserRole != null ? bookingUserRole.getRoleDisplayName() : StringUtils.EMPTY);
					}
				}
				return isValid;
			}).sorted(getComparatorForSorting()).collect(Collectors.toList());

		} else {
			return sortResponse(finalResponse);
		}
	}


	private void processPayments(Set<String> bookingids, LocalDateTime createdOnBeforeDate,
			LocalDateTime createdOnAfterDateTime) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		StringBuilder paymentQuery = constructPaymentQuery(bookingids, createdOnBeforeDate, createdOnAfterDateTime);
		List<Object[]> payments =
				entityManager.unwrap(Session.class).createNativeQuery(paymentQuery.toString()).getResultList();
		payments.forEach(payment -> {
			ProcessedPayment pPayment = ProcessedPayment.builder().paymentMedium((String) payment[1])
					.userId((String) payment[2]).merchantTxnId((String) payment[3]).build();
			constructPaymentMap((String) payment[0], pPayment, bookingPaymentMap);
			if (payment[4] != null)
				constructPaymentMap((String) payment[4], pPayment, amendmentPaymentMap);
			users.add((String) payment[2]);
		});
		entityManager.close();
	}


	private StringBuilder constructPaymentQuery(Set<String> bookingids, LocalDateTime createdOnBeforeDateTime,
			LocalDateTime createdOnAfterDateTime) {
		StringBuilder paymentQuery = new StringBuilder(
				"SELECT refid,paymentmedium,payUserId,merchantTxnId,amendmentid FROM payment where status = 'S' and refid IN ('")
						.append(Joiner.on("','").join(bookingids)).append("')").append(" and createdon >= '")
						.append(createdOnAfterDateTime.toString()).append("'").append(" and createdon <= '")
						.append(createdOnBeforeDateTime.toString()).append("';");
		return paymentQuery;
	}

	private Map<String, List<ReportOrderItem>> constructTripInfo(List<Object[]> orders, Set<String> bookingids) {
		Map<String, List<ReportOrderItem>> map = new HashMap<>();
		for (Object[] order : orders) {
			DbAirOrderItem orderItem = (DbAirOrderItem) order[0];
			DbOrder dbOrder = (DbOrder) order[1];
			users.add(dbOrder.getBookingUserId());
			users.add(dbOrder.getLoggedInUserId());
			if (dbOrder.getAdditionalInfo().getAssignedUserId() != null)
				users.add(dbOrder.getAdditionalInfo().getAssignedUserId());
			if (bookingids.contains(dbOrder.getBookingId()))
				constructAirOrderRow(map, orderItem, orderItem.getBookingId());
		}
		return map;
	}

	private StringBuilder constructOrderQuery(LocalDateTime createdOnBeforeDateTime,
			LocalDateTime createdOnAfterDateTime) {
		StringBuilder orderQuery = new StringBuilder(
				"SELECT {a.*}, {b.*} FROM airorderitem a JOIN orders b ON a.bookingid = b.bookingid WHERE ordertype ='A' and b.status IN ('S','C') ");
		orderQuery.append(" and b.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and b.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		if (request.getDepartedOnAfterDate() != null)
			orderQuery.append(" and a.departureTime >= '")
					.append(LocalDateTime.of(request.getDepartedOnAfterDate(), LocalTime.MIN).toString()).append("' ");
		if (request.getDepartedOnBeforeDate() != null)
			orderQuery.append(" and a.departureTime <= '")
					.append(LocalDateTime.of(request.getDepartedOnBeforeDate(), LocalTime.MAX).toString()).append("' ");
		if (request.getBookedOnAfterDateTime() != null)
			orderQuery.append(" and b.additionalinfo#>>'{fut,S}' >= '")
					.append(request.getBookedOnAfterDateTime().toString()).append("' ");
		if (request.getBookedOnBeforeDateTime() != null)
			orderQuery.append(" and b.additionalinfo#>>'{fut,S}' <= '")
					.append(request.getBookedOnBeforeDateTime().toString()).append("' ");
		if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
			orderQuery.append(" and a.airlinecode IN ('").append(Joiner.on("','").join(request.getAirlineCodes()))
					.append("') ");
		if (request.getCabinClass() != null)
			orderQuery.append(
					" and EXISTS ( SELECT FROM jsonb_array_elements(a.travellerinfo) tinfo WHERE tinfo -> 'fd' @> '{\"cc\":\"")
					.append(request.getCabinClass().getCode()).append("\"}')");
		List<String> allowedUserIds = getUserIds();
		if (CollectionUtils.isNotEmpty(allowedUserIds))
			orderQuery.append("and b.bookingUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");

		orderQuery.append(" order by a.bookingid,a.id;");
		return orderQuery;
	}

	protected Map<String, DbOrder> constructOrdersMap(Set<String> totalBookingIds, EntityManager em,
			LocalDateTime createdOnBeforeDateTime, LocalDateTime createdOnAfterDateTime) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		StringBuilder orderQuery = new StringBuilder(
				"SELECT {o.*}, {a.*} from orders o join airorderitem a on a.bookingid = o.bookingid where o.bookingId IN ('")
						.append(Joiner.on("','").join(totalBookingIds)).append("')");
		orderQuery.append(" and o.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and o.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ");
		orderQuery.append(" and a.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ");
		if (request.getDepartedOnAfterDate() != null)
			orderQuery.append(" and a.departureTime >= '").append(request.getDepartedOnAfterDate().toString())
					.append("' ");
		if (request.getDepartedOnBeforeDate() != null)
			orderQuery.append(" and a.departureTime <= '").append(request.getDepartedOnBeforeDate().toString())
					.append("' ");
		if (CollectionUtils.isNotEmpty(request.getAirlineCodes()))
			orderQuery.append(" and a.airlinecode IN ('").append(Joiner.on("','").join(request.getAirlineCodes()))
					.append("') ");
		if (request.getCabinClass() != null)
			orderQuery.append(
					" and EXISTS ( SELECT FROM jsonb_array_elements(a.travellerinfo) tinfo WHERE tinfo -> 'fd' @> '{\"cc\":\"")
					.append(request.getCabinClass().getCode()).append("\"}')");
		List<String> allowedUserIds = getUserIds();
		if (CollectionUtils.isNotEmpty(allowedUserIds))
			orderQuery.append("and o.bookingUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");

		orderQuery.append(" order by a.bookingid,a.id;");

		List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
				.addEntity("o", DbOrder.class).addEntity("a", DbAirOrderItem.class).getResultList();
		entityManager.close();
		return orders.stream().map(orderItem -> {
			DbOrder order = (DbOrder) orderItem[0];
			return new SimpleEntry<>(order.getBookingId(), order);
		}).collect(Collectors.toMap(Entry::getKey, Entry::getValue, (e1, e2) -> e1));
	}

	@Override
	public String getValuePartForEmail() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected String getCollDocTypeForFetchingFields() {
		return "reports_booking_transaction";
	}

}
