package com.tgs.services.rms.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.oms.datamodel.AirItemDetail;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.dbmodel.DbAmendment;
import com.tgs.services.oms.dbmodel.DbOrder;
import com.tgs.services.oms.dbmodel.air.DbAirItemDetail;
import com.tgs.services.oms.dbmodel.air.DbAirOrderItem;
import com.tgs.services.oms.jparepository.GstInfoService;
import com.tgs.services.rms.datamodel.AirLineTransactionResponse;
import com.tgs.services.rms.datamodel.DSRFilter;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.datamodel.UserProfileFields;

import helper.ProcessedPayment;
import helper.ReportOrderItem;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class DSRHandler extends TransactionReportHandler<DSRFilter, ReportResponse> {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    UserServiceCommunicator userCommunicator;

    @Autowired
    private GstInfoService gstInfoService;

    @SuppressWarnings("unchecked")
    @Override
    public List getResultsFromDb() {
        itemDetailMap = new HashMap<>();
        profileMeta = null;
        visibleCustomFields = new HashMap<>();
        gstInfoMap = new HashMap<>();
        List<AirLineTransactionResponse> finalResponse = new ArrayList<>();
        if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null) {
            EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
            StringBuilder paymentQuery = constructPaymentQuery();
            LocalDateTime createdOnAfterForAllDbQueries = request.getCreatedOnAfterDateTime().minusMonths(3);
			LocalDateTime createdOnBeforeForAllDbQueries = request.getCreatedOnBeforeDateTime();
            List<Object[]> payments = entityManager.unwrap(Session.class).createNativeQuery(paymentQuery.toString())
                    .getResultList();
            log.debug("ATR: Fetched payment entries from db");
            entityManager.close();
            Set<String> users = new HashSet<>();
            payments.forEach(payment -> {
                ProcessedPayment pPayment = ProcessedPayment.builder().paymentMedium((String) payment[2])
                        .userId((String) payment[3]).merchantTxnId((String) payment[4]).build();
                if (payment[0] != null)
                    constructPaymentMap((String) payment[0], pPayment, amendmentPaymentMap);
                if (payment[1] != null)
                    constructPaymentMap((String) payment[1], pPayment, bookingPaymentMap);
                users.add((String) payment[3]);
            });

            bookingUsers = userCommunicator.getUsersFromCache(UserCacheFilter.builder().userIds(Lists.newArrayList(users)).build());
            salesHierarchy = userCommunicator.getUserRelations(Lists.newArrayList(users));
            parentUser = getParentUser();
            CollectionServiceFilter filter = CollectionServiceFilter.builder().key(parentUser.getUserId()).build();
            List<Document> documentList = gsCommunicator.fetchGeneralDocument(filter);
            if (CollectionUtils.isNotEmpty(documentList)) {
                profileMeta = GsonUtils.getGson().fromJson(documentList.get(0).getData(), UserProfileFields.class);
                profileMeta.InitMap();
                visibleCustomFields = getVisibleFields();
                log.debug("DSR: visibleCustomFields = {}", new Gson().toJson(visibleCustomFields));
                request.getOutputFields().addAll(visibleCustomFields.values());
            }
            Set<String> totalBookingIds = new HashSet<>(bookingPaymentMap.keySet());
            Set<String> totalAmendments = new HashSet<>(amendmentPaymentMap.keySet());
            List<GstInfo> gstInfoList = gstInfoService.findByBookingIdIn(new ArrayList<>(totalBookingIds));
            gstInfoMap = gstInfoList.stream().collect(Collectors.groupingBy(GstInfo::getBookingId));

            log.debug("[Reports] Booking ids found {} ", totalBookingIds.toString());
            log.debug("[Reports] Amendment ids found {} ", totalAmendments.toString());
            if (MapUtils.isNotEmpty(bookingPaymentMap)) {
                // Booking ids with amendments. Fetch First amendment corresponding to booking id,and booking from old snapshot of amendment
            	StringBuilder bookingWithAmendment = constructAmendmentQuery(totalBookingIds, createdOnBeforeForAllDbQueries.plusMonths(3),createdOnAfterForAllDbQueries);
                entityManager = em.getEntityManagerFactory().createEntityManager();
                List<DbAmendment> bookingAmendments = entityManager.unwrap(Session.class)
                        .createNativeQuery(bookingWithAmendment.toString()).addEntity("a",DbAmendment.class)
                        .getResultList();
                log.debug("ATR : Fetched bookings with amendments from db");
                Map<String, DbOrder> ordersMap = constructOrdersMap(totalBookingIds,em,request.getCreatedOnBeforeDateTime(),request.getCreatedOnAfterDateTime());
                log.debug("ATR : Fetched orders from db");
                bookingAmendments.forEach(bookingAmendment -> {
                    finalResponse.addAll(constructRowsforBookingsWithAmendments(
                            StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("amendment"),
                            totalBookingIds, totalAmendments, bookingAmendment,ordersMap,request.getCreatedOnAfterDateTime(),
                            request.getCreatedOnBeforeDateTime()));
                });
                log.debug("ATR : Constructed booking rows with amendments");
                entityManager.close();

                // Booking ids without amendments
                if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("booking")) {
                    StringBuilder orderQuery = constructOrderQuery(totalBookingIds, createdOnBeforeForAllDbQueries, createdOnAfterForAllDbQueries);
                    log.debug("DSR Orders Query: {}", orderQuery);
                    entityManager = em.getEntityManagerFactory().createEntityManager();
                    List<Object[]> orders = entityManager.unwrap(Session.class).createNativeQuery(orderQuery.toString())
                            .addEntity("a", DbAirOrderItem.class).addEntity("b", DbOrder.class)
                            .addEntity("c", DbAirItemDetail.class).getResultList();
                    entityManager.close();
                    log.debug("[Reports] Booking ids without amendments {} ", totalBookingIds);
                    // Construct map with trip info for all passengers
                    Map<String, List<ReportOrderItem>> map = new HashMap<>();
                    Map<String, DbOrder> orderMap = new HashMap<>();
                    for (Object[] order : orders) {
                        DbAirOrderItem orderItem = (DbAirOrderItem) order[0];
                        DbOrder dbOrder = (DbOrder) order[1];
                        DbAirItemDetail dbAirItemDetail = order[2] == null ? null : (DbAirItemDetail) order[2];
                        AirItemDetail itemDetail = dbAirItemDetail == null ? null : dbAirItemDetail.toDomain();
                        orderMap.put(dbOrder.getBookingId(), dbOrder);
                        itemDetailMap.put(dbOrder.getBookingId(), itemDetail);
                        constructAirOrderRow(map, orderItem, orderItem.getBookingId());
                    }
                    map.forEach((k, v) -> {
                        v.forEach(row -> {
                            String key = k.split("_")[0];
                            AirLineTransactionResponse response = constructRow(row, null, bookingPaymentMap.get(key),
                                    orderMap.get(key), bookingUsers, salesHierarchy);
                            if (response != null)
                                finalResponse.add(response);
                        });
                    });
                    log.debug("ATR : Constructed booking rows without amendments");
                }
            }

            // Only amendments
            if (StringUtils.isBlank(request.getSelectionType()) || request.getSelectionType().equals("amendment")) {
                if (CollectionUtils.isNotEmpty(totalAmendments)) {
                    entityManager = em.getEntityManagerFactory().createEntityManager();
                    log.debug("[Reports] Amendment ids without bookings {} ", totalAmendments);
					StringBuilder amendMentQuery = new StringBuilder(
							"SELECT {a.*},{b.*} from amendment a JOIN orders b ON a.bookingid = b.bookingid where a.status = 'S' and "
									+ "b.ordertype ='A' and a.amendmentId IN ('")
											.append(Joiner.on("','").join(totalAmendments)).append("')")
											.append(" and b.createdOn>=' ")
											.append(createdOnAfterForAllDbQueries.toString()).append("' ")
											.append(" and b.createdOn<='")
											.append(createdOnBeforeForAllDbQueries.toString()).append("' ")
											.append(" and a.createdOn>=' ")
											.append(createdOnAfterForAllDbQueries.toString()).append("' ")
											.append(" and a.createdOn<='")
											.append(createdOnBeforeForAllDbQueries.toString()).append("' ");
					List<Object[]> amendMentsWithOrder = entityManager.unwrap(Session.class)
							.createNativeQuery(amendMentQuery.toString()).addEntity("a", DbAmendment.class)
							.addEntity("b", DbOrder.class).getResultList();
                    log.debug("ATR : Fetched amendments from db");
                    entityManager.close();
                    amendMentsWithOrder.forEach(amendMentOrder -> {
                        DbAmendment amendMent = (DbAmendment) amendMentOrder[0];
                        DbOrder dbOrder = (DbOrder) amendMentOrder[1];
                        // Fetch new state from amendment and put in results
                        List<AirOrderItem> airOrders = airAmendmentManager.getAmendmentSnapshot(amendMent.toDomain(), false);
                        if (CollectionUtils.isNotEmpty(airOrders))
                            constructRowForAmendmentSnapshots(finalResponse, amendMent, airOrders,amendMent.getAmendmentId(), dbOrder);
                    });
                    log.debug("ATR : Constructed booking rows of only amendments");
                }
            }
        }
        return sortResponse(finalResponse);
    }

    private StringBuilder constructOrderQuery(Set<String> totalBookingIds, LocalDateTime createdOnBeforeDateTime , LocalDateTime createdOnAfterDateTime) {
        StringBuilder orderQuery = new StringBuilder(
                "SELECT {a.*}, {b.*}, {c.*} FROM airorderitem a JOIN orders b ON a.bookingid = b.bookingid LEFT JOIN airitemdetail c ON b.bookingid = c.bookingid " +
                        "WHERE a.bookingid IN ('")
                .append(Joiner.on("','").join(totalBookingIds)).append("')")
                .append(" and b.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ")
        		.append(" and b.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ")
        		.append(" and a.createdOn>=' ").append(createdOnAfterDateTime.toString()).append("' ")
        		.append(" and a.createdOn<='").append(createdOnBeforeDateTime.toString()).append("' ")
                .append(" and ordertype ='A' and b.status IN ('S','C') order by a.bookingid,a.id;");
        return orderQuery;
    }

    private StringBuilder constructPaymentQuery() {
        StringBuilder paymentQuery = new StringBuilder(
                "SELECT amendmentid,refid,paymentmedium,payUserId,merchantTxnId FROM payment where status = 'S' and createdOn>='")
                .append(request.getCreatedOnAfterDateTime().toString()).append("' and createdOn<='")
                .append(request.getCreatedOnBeforeDateTime().toString()).append("'");
        List<String> allowedUserIds = getUserIds();
        if (CollectionUtils.isNotEmpty(allowedUserIds))
            paymentQuery.append("and payUserId IN ('").append(Joiner.on("','").join(allowedUserIds)).append("')");
        paymentQuery.append(";");
        return paymentQuery;
    }

    private void constructRowForAmendmentSnapshots(List<AirLineTransactionResponse> finalResponse, DbAmendment amendMent
            , List<AirOrderItem> airOrders, String id, DbOrder order) {
        Map<String, List<ReportOrderItem>> ordersMap = new HashMap<>();
        List<DbAirOrderItem> dbairOrders = new DbAirOrderItem().toDbList(airOrders);
        dbairOrders.forEach(orderItem -> {
            constructAirOrderRow(ordersMap, orderItem, id);
        });
        ordersMap.forEach((k, v) -> v.forEach(row -> {
            AirLineTransactionResponse response = constructRow(row, amendMent,
                    amendMent != null ? amendmentPaymentMap.get(k.split("_")[0]) : bookingPaymentMap.get(k.split("_")[0]),
                    order, bookingUsers, salesHierarchy);
            if (response != null)
                finalResponse.add(response);
        }));
    }

	private User getParentUser() {
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		if (UserRole.corporate(loggedInUser.getRole())) {
			return loggedInUser;
		} else {
			List<String> userIds = request.getQueryFields() != null ? (List) request.getQueryFields().get("userIds")
					: Collections.emptyList();
			if (userIds.size() != 1) {
				throw new CustomGeneralException("Select exactly one corporate user!");
			}
			return userCommunicator.getUserFromCache(userIds.get(0));
		}
	}
	
	@Override
	protected String getCollDocTypeForFetchingFields() {
		return "reports_dsr";
	}

    @Override
    public String getValuePartForEmail() {
        return null;
    }
}
