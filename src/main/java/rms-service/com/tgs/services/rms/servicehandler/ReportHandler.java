package com.tgs.services.rms.servicehandler;

import java.io.File;
import java.math.BigInteger;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AttachmentMetadata;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.enums.DateFormatType;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.DateFormatterHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.rms.common.IReport;
import com.tgs.services.rms.datamodel.ReportFilter;
import com.tgs.services.rms.datamodel.ReportFormat;
import com.tgs.services.rms.restmodel.ReportResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.file.TgsFileUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class ReportHandler<T extends ReportFilter, RT extends ReportResponse>
		extends ServiceHandler<T, ReportResponse> implements IReport {

	@Autowired
	MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	UserServiceCommunicator userService;

	@Override
	public void beforeProcess() throws Exception {

	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@SuppressWarnings("unchecked")
	public void handleRestResponse(List object, List<String> outputFields) {
		List<Map<String, Object>> restResponse = new ArrayList<>();
		object.forEach(obj -> {
			// multiple columns in select clause
			if (obj instanceof Object[]) {
				Object[] objArr = (Object[]) obj;
				Map<String, Object> row = new HashMap<>();
				for (int index = 0; index < outputFields.size(); index++) {
					row.put(outputFields.get(index), objArr[index]);
				}
				restResponse.add(row);
			}
			// only one column in select clause
			else if (obj instanceof String || obj instanceof BigInteger) {
				Map<String, Object> row = new HashMap<>();
				row.put(request.getOutputFields().get(0), obj);
				restResponse.add(row);
			}
		});
		response.setRestResponse(restResponse);
	}

	public void writeToFile(List object, List<String> outputFields) {
		StringBuilder filePath = new StringBuilder("./");
		String name = "";
		if (request.getReportType() != null) {
			name = StringUtils.capitalize(request.getReportType().name().toLowerCase());
		}
		String fileName = name.concat("_Report_")
				.concat(DateFormatterHelper.formatDateTime(LocalDateTime.now(), DateFormatType.REPORT_FORMAT))
				.concat(".").concat(request.getOutputFormat().getCode());
		File file = new File(filePath.append(fileName).toString());
		TgsFileUtils.writeToFile(request.getOutputFormat(), object, outputFields, file);
		byte[] readData = TgsFileUtils.convertToByteStream(file);
		sendMail(fileName, readData);
		// response.setUrl(AwsUtils.uploadFileToAws(file));
		response.setByteStream(readData);
	}

	public void sendMail(String fileName, byte[] readData) {
		if (CollectionUtils.isNotEmpty(request.getEmailIds())) {
			request.getEmailIds().forEach(emailId -> {
				if (EmailValidator.getInstance().isValid(emailId)) {
					AbstractMessageSupplier<EmailAttributes> msgAttributes =
							new AbstractMessageSupplier<EmailAttributes>() {
								@Override
								public EmailAttributes get() {
									AttachmentMetadata attachmentData = AttachmentMetadata.builder().fileData(readData)
											.fileName(fileName.toString()).build();
									UserRole role = getUserRole(SystemContextHolder.getContextData().getUser());
									EmailAttributes msgAttr = EmailAttributes.builder().toEmailId(emailId)
											.attachmentData(attachmentData).addDateTimeInSubject(true).role(role)
											.key(EmailTemplateKey.REPORT_EMAIL.name()).value(getValuePartForEmail())
											.build();
									return msgAttr;
								}
							};
					msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
				}
			});
		}
	}

	private UserRole getUserRole(User user) {
		if (user == null) {
			return null;
		}
		return user.getRole();
	}

	private String getResponseAsJson(List response) {
		Gson gson = GsonUtils.buildGsonToSerializeSomeFields(request.getOutputFields(), null);
		return gson.toJson(response);
	}

	public List<Map<String, Object>> getListOfRows(List response) {
		String jsonString = getResponseAsJson(response);
		if (ReportFormat.CSV.equals(request.getOutputFormat()))
			jsonString = jsonString.replaceAll(",", ";");
		List<Map<String, Object>> report =
				new Gson().fromJson(jsonString, new TypeToken<List<Map<String, Object>>>() {}.getType());
		if (isDSR()) {
			log.debug("DSR before flattening: {}", report);
			report.forEach(row -> {
				row.forEach((col, val) -> {
					if (col.equals("cfMap") && val instanceof Map) {
						LinkedTreeMap<String, Object> cfMap = (LinkedTreeMap<String, Object>) val;
						cfMap.forEach(row::put);
					}
				});
				row.remove("cfMap");
			});
			log.debug("DSR after flattening: {}", report);
		}
		return report;
	}

	public List<Object[]> constructRows(List<Map<String, Object>> outputData, List<String> outPutFields) {
		log.debug("DSR: constructRows called. OutPutFields = {}", new Gson().toJson(outPutFields));
		List<Object[]> output = new ArrayList<>();
		outputData.forEach(row -> {
			int i = 0;
			Object[] obj = new Object[outPutFields.size()];
			for (String field : outPutFields) {
				obj[i++] = row.get(field);
			}
			output.add(obj);
		});
		return output;
	}

	boolean isDSR() {
		return this instanceof DSRHandler;
	}

	boolean isATR() {
		return this instanceof AirLineTransactionReportHandler;
	}

	boolean isBTR() {
		return this instanceof BookingTransactionReportHandler;
	}
}
