package helper;

import com.tgs.services.oms.dbmodel.hotel.DbHotelOrderItem;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class HotelReportOrderItem extends DbHotelOrderItem {

	private String city;
	private String country;
	private String atoBookingId;
	private String hotelCategory;
	private Integer noOfNights;
	private Integer totalNoOfRooms;
	private Integer totalRoomNights;
	
}
