package helper;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class ProcessedPayment {

	private String paymentMedium;
	
	private String userId;
	
	private String merchantTxnId;
	
}
