package com.tgs.services.ss.servicehandler;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.microsoft.sqlserver.jdbc.StringUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.OTPCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.JWTHelper;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;
import com.tgs.services.ss.restmodel.TgsInternalLoginRequest;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.restmodel.SignInResponse;

@Service
public class TgsInternalLoginHandler extends ServiceHandler<TgsInternalLoginRequest, SignInResponse> {

	@Autowired
	private UserServiceCommunicator usrSrvCommunicator;

	@Autowired
	private GeneralServiceCommunicator generalSrvCommunicator;

	@Autowired
	private OTPCommunicator otpCommunicator;

	@Override
	public void beforeProcess() throws Exception {
		if (!request.getEmail().endsWith("@technogramsolutions.com"))
			throw new CustomGeneralException(SystemError.INVALID_EMAIL);
	}

	@Override
	public void process() throws Exception {
		ConfiguratorInfo configuratorInfo = generalSrvCommunicator.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO);
		ClientGeneralInfo clientIfo = (ClientGeneralInfo) configuratorInfo.getOutput();
		List<String> userIds = clientIfo.getInternalUserIds();
		Map<String, Set<String>> userIdWiseJwtTokens = usrSrvCommunicator.getJWTTokensOfUsers(userIds);
		Set<String> inUseUserIds = userIdWiseJwtTokens.keySet();
		String availableUserId =
				userIds.stream().filter(userid -> !inUseUserIds.contains(userid)).findFirst().orElse("");
		if (StringUtils.isEmpty(availableUserId))
			throw new CustomGeneralException(SystemError.USER_NOT_AVAILABLE);
		User user = usrSrvCommunicator.getUserFromCache(availableUserId);
		if (user == null)
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		OtpToken otpToken = otpCommunicator.validateOtp(request.getRequestId(), request.getOtp(), 3);
		if (Objects.isNull(otpToken) || !request.getEmail().equals(otpToken.getEmail())) {
			throw new CustomGeneralException(SystemError.INVALID_OTP);
		}
		String accessToken = JWTHelper.generateAndStoreAccessToken(user,
				SystemContextHolder.getContextData().getHttpResponse(), 1800);
		ContextData contextData = SystemContextHolder.getContextData();
		SystemAudit audit = SystemAudit.builder().auditType(AuditAction.LOGIN).userId(availableUserId)
				.ip(contextData.getHttpHeaders().getIp()).loggedInUserId(availableUserId).build();
		generalSrvCommunicator.addToSystemAudit(audit);
		response.setUser(user);
		response.setAccessToken(accessToken);
	}

	@Override
	public void afterProcess() throws Exception {

	}

}
