package com.tgs.services.base;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;
import org.hibernate.envers.query.AuditEntity;
import org.hibernate.envers.query.AuditQuery;
import org.hibernate.envers.query.criteria.AuditCriterion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.runtime.database.CustomRevision;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.utils.masking.MaskingUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter
@Setter
@Slf4j
public class AuditsHandler {

	@PersistenceContext
	EntityManager em;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	public List<AuditResult> fetchAudits(AuditsRequest request, Class className, String primarySearchKey) {
		return fetchAudits(request, className, primarySearchKey, false);
	}

	public List<AuditResult> fetchAudits(AuditsRequest request, Class className, String primarySearchKey,
			boolean getCreateRevType) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		AuditReader reader = AuditReaderFactory.get(entityManager.unwrap(org.hibernate.Session.class));
		long beforeTime = request.getBeforeDate() != null
				? request.getBeforeDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
				: Long.MAX_VALUE;
		long afterTime = request.getAfterDate() != null
				? request.getAfterDate().atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()
				: Long.MIN_VALUE;

		AuditCriterion time = AuditEntity.revisionProperty("timestamp").between(afterTime, beforeTime);

		AuditQuery query = reader.createQuery().forRevisionsOfEntity(className, false, true).add(time);
		if (request.getUserId() != null) {
			query.add(AuditEntity.revisionProperty("userId").eq(request.getUserId()));
		}
		if (request.getUserName() != null) {
			query.add(AuditEntity.revisionProperty("userName").eq(request.getUserName()));
		}
		if (request.getUserIp() != null) {
			query.add(AuditEntity.revisionProperty("userIp").eq(request.getUserIp()));
		}
		if (request.getCreatedOnAfterDateTime() != null && request.getCreatedOnBeforeDateTime() != null) {
			AuditCriterion createdOnTime = AuditEntity.property("createdOn")
					.between(request.getCreatedOnAfterDateTime(), request.getCreatedOnBeforeDateTime());
			query.add(createdOnTime);
		}
		if (StringUtils.isEmpty(primarySearchKey)) {
			primarySearchKey = "id";
		}
		if (StringUtils.isNotEmpty(request.getId())) {
			query.add(AuditEntity.property(primarySearchKey)
					.eq("id".equals(primarySearchKey) ? Long.valueOf(request.getId()) : request.getId()));
			query.addOrder(AuditEntity.revisionNumber().asc());
		} else {
			query.addOrder(AuditEntity.property(primarySearchKey).asc());
			query.addOrder(AuditEntity.revisionProperty("timestamp").asc());
		}

		List<Object[]> list = query.getResultList();
		List<AuditResult> results = new ArrayList<>(list.size());
		if (CollectionUtils.isNotEmpty(list)) {
			List<String> notAuditedFields = getFieldsToRemove(className.getSimpleName());
			for (int index = 1; index < list.size(); index++) {
				CustomRevision revInfo = (CustomRevision) list.get(index)[1];
				Object current = list.get(index)[0];
				Object prev = list.get(index - 1)[0];
				Object currentDataModel = convertToDataModel(current);
				Object prevDataModel = convertToDataModel(prev);
				Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build()
						.buildGson();
				MaskedFields maskedFields = getFieldsToMask();
				String currStr = MaskingUtils.getMaskedString(gson.toJson(currentDataModel), maskedFields, false);
				String prevStr = MaskingUtils.getMaskedString(gson.toJson(prevDataModel), maskedFields, false);
				Map<String, Object> currFields = TgsObjectUtils.getNotNullFieldValueMap(
						convertStringToObject(currStr, currentDataModel.getClass()), true, true);
				Map<String, Object> prevFields = TgsObjectUtils
						.getNotNullFieldValueMap(convertStringToObject(prevStr, prevDataModel.getClass()), true, true);
				// We are fetching modified time from customrev
				currFields.remove("processedOn");
				prevFields.remove("processedOn");
				notAuditedFields.forEach(field -> {
					currFields.remove(field);
					prevFields.remove(field);
				});
				if (currFields.get("id").equals(prevFields.get("id"))) {
					AuditResult result = AuditResult.builder()
							.modifiedTime(revInfo.getRevisionDate().toInstant().atZone(ZoneId.systemDefault())
									.toLocalDateTime())
							.userId(revInfo.getUserId()).userIp(revInfo.getUserIp()).actualValue(current).build();
					List<AuditChange> changes = new ArrayList<>();
					currFields.forEach((key, value) -> {
						if (!prevFields.containsKey(key) || !value.equals(prevFields.get(key))) {
							changes.add(AuditChange.builder().fieldName(key).oldValue(prevFields.remove(key))
									.newValue(currFields.get(key)).build());
						} else if (value.equals(prevFields.get(key)))
							prevFields.remove(key);
					});
					prevFields.forEach((k, v) -> changes.add(
							AuditChange.builder().fieldName(k).oldValue(prevFields.get(k)).newValue(null).build()));
					if (CollectionUtils.isNotEmpty(changes)) {
						result.setDifference(changes);
						results.add(result);
					}
				}
			}
			if (getCreateRevType) {
				CustomRevision revInfo = (CustomRevision) list.get(0)[1];
				Object current = list.get(0)[0];
				AuditResult result = AuditResult.builder()
						.modifiedTime(
								revInfo.getRevisionDate().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime())
						.userId(revInfo.getUserId()).userIp(revInfo.getUserIp()).actualValue(current)
						.difference(new ArrayList<>()).build();
				results.add(result);
			}
		}
		entityManager.close();
		results.sort((AuditResult r1, AuditResult r2) -> r2.getModifiedTime().compareTo(r1.getModifiedTime()));
		return results;
	}


	private Object convertToDataModel(Object obj) {
		try {
			return obj.getClass().getDeclaredMethod("toDomain").invoke(obj);
		} catch (Exception e) {
			log.error("Unable to convert to data model of class {}", obj.getClass(), e);
		}
		return obj;
	}

	private MaskedFields getFieldsToMask() {
		List<Document> docList = gsCommunicator.fetchRoleSpecificDocument(CollectionServiceFilter.builder()
				.isConsiderHierarchy(true).types(Arrays.asList("masked_fields")).build());
		if (CollectionUtils.isNotEmpty(docList))
			return new Gson().fromJson(docList.get(0).getData(), MaskedFields.class);
		return null;
	}

	private List<String> getFieldsToRemove(String key) {
		List<Document> docList = gsCommunicator.fetchRoleSpecificDocument(CollectionServiceFilter.builder()
				.isConsiderHierarchy(true).types(Arrays.asList("unaudited_fields")).build());
		if (CollectionUtils.isNotEmpty(docList)) {
			Map<String, List<NotAuditedFields>> map = new Gson().fromJson(docList.get(0).getData(),
					new TypeToken<Map<String, List<NotAuditedFields>>>() {}.getType());
			List<NotAuditedFields> notAuditedFields = map.get("data");
			for (NotAuditedFields nonAuditField : notAuditedFields) {
				if (key.equals(nonAuditField.getEntity()))
					return nonAuditField.getFields();
			}
		}
		return Collections.emptyList();
	}

	private <T> T convertStringToObject(String str, Class<T> className) {
		// Register run time polymorphism adapter
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build()
				.buildGson(str, className);
		return gson.fromJson(str, className);
	}
}
