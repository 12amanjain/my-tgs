package com.tgs.services.base;

import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.filter.LevelFilter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.spi.FilterReply;

@Service
public class CustomLogBackFilter extends LevelFilter {

	@Autowired
	private GeneralCachingCommunicator cacheCommunicator;

	private static String debugUserId = "NA";

	private static String env;

	@Value("${env}")
	public void setEnv(String e) {
		env = e;
	}

	@Override
	public FilterReply decide(ILoggingEvent event) {
		if ("prod".equals(env)) {
			if (debugUserId.equals(MDC.get("userid")) && event.getLevel().equals(Level.DEBUG)) {
				return FilterReply.ACCEPT;
			}
			if (event.getLevel().equals(Level.DEBUG)) {
				return FilterReply.DENY;
			}
		}
		return super.decide(event);
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 30 * 1000)
	public void fetchDebugUserId() {
		Map<String, Map<String, String>> debugUserIdMap = cacheCommunicator.get(CacheMetaInfo.builder()
				.set(CacheSetName.DEBUGUSER_ID.getName()).keys(new String[] {"debuguserid"}).build(), String.class);
		if (!debugUserIdMap.isEmpty()) {
			if (!debugUserIdMap.get("debuguserid").isEmpty()) {
				if (StringUtils.isNotBlank(debugUserIdMap.get("debuguserid").get("value"))) {
					debugUserId = debugUserIdMap.get("debuguserid").get("value");
				}
			}
		} else {
			debugUserId = "NA";
		}
	}

	public static String getDebugUserId() {
		return debugUserId;
	}

}
