package com.tgs.services.base;


import org.springframework.stereotype.Service;

import com.tgs.services.base.communicator.ElasticSearchCommunicator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public abstract class ESStackInitializer {

    public void initialize(String name) {
        this.deleteExistingData();
        this.process();
    }


    public abstract void process();

    public abstract void deleteExistingData();


}