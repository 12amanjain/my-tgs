package com.tgs.services.base;

import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.tgs.services.base.runtime.SystemContextHolder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EnumTypeAdapterFactory implements TypeAdapterFactory {


	@SuppressWarnings("unchecked")
	@Override
	public <T> TypeAdapter<T> create(Gson gson, TypeToken<T> type) {
		Class<T> rawType = (Class<T>) type.getRawType();
		/**
		 * rawType.getName().contains("$") case is to handle enums which consist of functions. In that scenarios rawType
		 * consist of enumName along with object position
		 */
		if (!(rawType.isEnum() || rawType.getName().contains("$"))) {
			return null;
		}

		return new TypeAdapter<T>() {
			@Override
			public void write(JsonWriter out, T value) throws IOException {
				if (value != null) {
					try {
						out.value((String) type.getRawType().getMethod("getCode").invoke(value));
					} catch (Exception e) {
						out.value(value.toString());
					}
				} else {
					out.nullValue();
				}
			}

			@Override
			public T read(JsonReader reader) throws IOException {
				if (reader.peek() == JsonToken.NULL) {
					reader.nextNull();
					return null;
				} else {
					Object str = reader.nextString();
					T value = null;
					try {

						if (SystemContextHolder.getContextData() != null
								&& SystemContextHolder.getContextData().getValue(getKey(type, str)) != null) {
							value = (T) SystemContextHolder.getContextData().getValue(getKey(type, str)).get();
						} else {

							value = (T) type.getRawType().getMethod("getEnumFromCode", String.class).invoke(null, str);
							if (value == null) {
								value = (T) type.getRawType().getMethod("valueOf", String.class).invoke(null, str);
							}
						}
					} catch (Exception e) {
						try {
							value = (T) type.getRawType().getMethod("valueOf", String.class).invoke(null, str);
						} catch (Exception e1) {

						}
					} finally {
						if (value == null) {
							log.error("Value is null for str {} , type is {}", str, type.getRawType());
						}
						if (SystemContextHolder.getContextData() != null && value != null) {
							SystemContextHolder.getContextData().setValue(getKey(type, str), value);
						}
					}
					return value;
				}
			}
		};
	}

	public String getKey(TypeToken type, Object str) {
		return str + type.getRawType().getName();
	}
}
