package com.tgs.services.base;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@ApiIgnore
@Slf4j
public class HealthCheckController {

	public static boolean isAppReady = false;

	// for ElB to check the health of the service
	@RequestMapping(value = "/status", method = RequestMethod.GET)
	protected void status(HttpServletRequest request, HttpServletResponse response) throws Exception {
		if (!isAppReady) {
			log.info("Server is Not initialized");
			response.sendRedirect("/abc");
		} else {
			response.getWriter().println("OK");
		}
	}

}
