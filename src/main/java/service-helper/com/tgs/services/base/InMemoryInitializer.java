package com.tgs.services.base;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public abstract class InMemoryInitializer {

	public static final int THIRTY_DAYS_EXPIRATION = 30 * 24 * 60 * 60 * 1000;
	public static final int NEVER_EXPIRE = -1;

	protected static CustomInMemoryHashMap configurationHashMap;

	private static String env;

	@Value("${env}")
	public void setEnv(String e) {
		env = e;
	}

	public abstract void process();

	public abstract void deleteExistingInitializer();

	public void initialize(String name) {
		log.info("Initializing {}", name);
		if(!"prod".equals(env)) {
			this.deleteExistingInitializer();
		}
		this.process();
		log.info("Initialized {}", name);
	}

	@Autowired
	public InMemoryInitializer(CustomInMemoryHashMap configurationHashMap) {
		InMemoryInitializer.configurationHashMap = configurationHashMap;
	}


}
