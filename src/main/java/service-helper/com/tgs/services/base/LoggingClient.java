package com.tgs.services.base;

import java.util.List;

public interface LoggingClient {

	public void store(List<LogData> logDataList);

	public void store(LogData logData);
}
