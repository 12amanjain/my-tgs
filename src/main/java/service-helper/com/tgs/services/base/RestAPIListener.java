package com.tgs.services.base;

import java.time.LocalDateTime;
import java.util.Arrays;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class RestAPIListener {

	private String key;
	private String type;
	private String supplier;

	public RestAPIListener(String bookingId) {
		this.key = bookingId;
	}

	public RestAPIListener(String bookingId, String type) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
	}

	public RestAPIListener(String bookingId, String type, String supplier) {
		this.key = bookingId;
		this.type = ObjectUtils.firstNonNull(type, "");
		this.supplier = supplier;
	}

	public void addLog(LogData data) {
		try {
			User user = SystemContextHolder.getContextData().getUser();
			if (StringUtils.isEmpty(data.getUserId())) {
				data.setUserId(UserUtils.getUserId(user));
			}

			data.setUserRole(UserUtils.getEmulatedUserRoleOrUserRole(user));
			data.setLogType("AirSupplierAPILogs");
			if (data.getGenerationTime() == null)
				data.setGenerationTime(LocalDateTime.now());
			LogUtils.store(Arrays.asList(data));
		} catch (Exception e) {
			log.error("Unable to add Log for key {}", data.getKey(), e);
		}
	}

	public static void addInternalLog(LogData data) {
		try {
			User user = SystemContextHolder.getContextData().getUser();
			if (StringUtils.isEmpty(data.getUserId())) {
				data.setUserId(UserUtils.getUserId(user));
			}

			data.setUserRole(UserUtils.getEmulatedUserRoleOrUserRole(user));
			data.setLogType("RestAPILogs");
			if (data.getGenerationTime() == null)
				data.setGenerationTime(LocalDateTime.now());
			LogUtils.store(Arrays.asList(data));
		} catch (Exception e) {
			log.error("Unable to add Log for key {}", data.getKey(), e);
		}
	}
}
