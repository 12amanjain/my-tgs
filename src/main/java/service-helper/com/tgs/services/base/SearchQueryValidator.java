package com.tgs.services.base;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.datamodel.RouteInfo;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.restmodel.AirSearchRequest;

@Component("beforeCreateSearchQueryValidator")
public class SearchQueryValidator implements Validator {

	@Autowired
	private OrderServiceCommunicator orderCommunicator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (target instanceof AirSearchRequest) {
			AirSearchRequest searchRequest = (AirSearchRequest) target;
			AirSearchQuery searchQuery = searchRequest.getSearchQuery();
			if (Objects.isNull(searchQuery.getCabinClass()))
				errors.rejectValue("searchQuery.cabinClass", SystemError.INVALID_CABIN_CLASS.errorCode(),
						SystemError.INVALID_CABIN_CLASS.getMessage());

			if (searchQuery.getPaxInfo() != null
					&& searchQuery.getPaxInfo().get(PaxType.INFANT) > searchQuery.getPaxInfo().get(PaxType.ADULT)) {
				errors.rejectValue("searchQuery.paxInfo", SystemError.INFANT_VALIDATION.errorCode(),
						SystemError.INFANT_VALIDATION.getMessage());
			}
			// if (searchQuery.getPaxInfo().get(PaxType.CHILD) >
			// searchQuery.getPaxInfo().get(PaxType.ADULT)) {
			// errors.rejectValue("searchQuery.paxInfo",
			// SystemError.CHILD_VALIDATION.errorCode(),
			// SystemError.CHILD_VALIDATION.getMessage());
			// }
			if (searchQuery.getRouteInfos() != null) {
				LocalDate previousDate = LocalDate.now();
				LocalDate yearDate = LocalDate.now().plusYears(1);
				Map<String, Boolean> routeMap = new HashMap<>();
				for (RouteInfo routeInfo : searchQuery.getRouteInfos()) {
					if (Objects.isNull(routeInfo.getTravelDate()) || !(routeInfo.getTravelDate().isAfter(previousDate)
							|| routeInfo.getTravelDate().isEqual(previousDate))) {
						errors.rejectValue("searchQuery.routeInfos", SystemError.TRAVEL_DATE_VALIDATION.errorCode(),
								SystemError.TRAVEL_DATE_VALIDATION.getMessage());
						break;
					}

					if (routeInfo.getTravelDate().isAfter(yearDate)) {
						errors.rejectValue("searchQuery.routeInfos",
								SystemError.TRAVEL_DATE_YEAR_VALIDATION.errorCode(),
								SystemError.TRAVEL_DATE_YEAR_VALIDATION.getMessage());
						break;
					}

					if (routeInfo.getFromCityOrAirport() == null || routeInfo.getFromCityOrAirport().getCode() == null
							|| routeInfo.getFromCityOrAirport().getCode()
									.equals(routeInfo.getToCityOrAirport().getCode())) {
						errors.rejectValue("searchQuery.routeInfos",
								SystemError.ORIGIN_DESTINATION_VALIDATION.errorCode(),
								SystemError.ORIGIN_DESTINATION_VALIDATION.getMessage());
						break;
					}
					String key = routeInfo.getFromCityOrAirport().getCode() + routeInfo.getFromCityOrAirport().getCode()
							+ routeInfo.getTravelDate();
					Boolean previousValue = routeMap.put(key, true);
					if (previousValue != null) {
						errors.rejectValue("searchQuery.routeInfos",
								SystemError.SAME_SECTOR_SAME_DAY_NOT_ALLOWED.errorCode(),
								SystemError.SAME_SECTOR_SAME_DAY_NOT_ALLOWED.getMessage());
						break;
					}

					previousDate = routeInfo.getTravelDate();
				}
			}
			if (searchQuery.getPaxInfo() != null && (searchQuery.getPaxInfo().get(PaxType.ADULT)
					+ searchQuery.getPaxInfo().get(PaxType.CHILD)) > 9) {
				errors.rejectValue("searchQuery.paxInfo", SystemError.PASSENGER_VALIDATION.errorCode(),
						SystemError.PASSENGER_VALIDATION.getMessage());
			}

			if (searchQuery.isPNRCreditSearch()) {
				try {
					orderCommunicator.validateSearchQueryOnPNR(searchQuery);
					LocalDateTime csExpiryDate = searchQuery.getSearchModifiers().getPnrCreditInfo().getCSExpiryDate();
					if (LocalDateTime.now().isAfter(csExpiryDate)) {
						errors.rejectValue("searchQuery", "Credit Shell Expired Already");
					}
				} catch (CustomGeneralException e) {
					errors.rejectValue("searchQuery", e.getError().errorCode(), e.getError().getMessage());
				}
			}
		}
	}
}
