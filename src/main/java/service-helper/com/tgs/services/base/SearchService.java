package com.tgs.services.base;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.Predicate;

import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.database.DependantSearchResolver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.base.runtime.database.SqlPredicate.PredicateBuilder;
import com.tgs.utils.springframework.data.SpringDataUtils;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public abstract class SearchService<M extends SuperBaseModel> {

	@Autowired
	private PredicateBuilder predicateBuilder;

	public List<M> search(QueryFilter filter) {
		return new ArrayList<>();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<M> search(QueryFilter filter, JpaSpecificationExecutor repo) {

		DependantSearchResolver.searchParentEntities(filter);

		log.debug("QueryFilter after DependantSearch {}", GsonUtils.getGson().toJson(filter));

		List<M> entityList = new ArrayList<>();

		PageRequest request = SpringDataUtils.getPageRequestFromFilter(filter);

		Page<M> pageList = repo.findAll((root, query, criteriaBuilder) -> {

			List<Predicate> predicates;

			predicates = predicateBuilder.buildPredicates(root, filter.getClass(), criteriaBuilder, filter, new ArrayList<>());

			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));

		}, request);

		if (pageList != null) {

			pageList.forEach(entity -> entityList.add(entity));
		}

		return entityList;
	}
}
