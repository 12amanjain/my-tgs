package com.tgs.services.base;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.stereotype.Service;

import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.restmodel.BaseResponse;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Service
public abstract class ServiceHandler<T, RT extends BaseResponse> {

	protected T request;
	protected RT response;
	protected ContextData contextData;

	public ServiceHandler() {

	}

	public void initData(T request, RT response) {
		this.request = request;
		this.response = response;
		contextData = SystemContextHolder.getContextData();
	}

	public RT getResponse() throws Exception {
		beforeProcess();
		if (CollectionUtils.isEmpty(response.getErrors())) {
			process();
			if (CollectionUtils.isEmpty(response.getErrors())) {
				afterProcess();
			}
		}
		return response;
	}

	/** Took an example from com.amazonaws.handlers.RequestHandler2 */

	public abstract void beforeProcess() throws Exception;

	public abstract void process() throws Exception;

	public abstract void afterProcess() throws Exception;

}
