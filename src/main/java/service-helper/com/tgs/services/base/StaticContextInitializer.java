package com.tgs.services.base;

import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.LogServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.utils.BaseHotelUtils;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.base.utils.HotelMissingInfoLoggingUtil;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.ServiceUtils;

@Service
public class StaticContextInitializer {

	@Autowired
	LogServiceCommunicator logServiceCommunicator;

	@Autowired
	GeneralServiceCommunicator gnComm;

	@Autowired
	GeneralCachingCommunicator gnCachingComm;

	@Autowired
	FMSCommunicator fmsComm;

	@Autowired
	UserServiceCommunicator userSrvComm;

	@Autowired
	HMSCommunicator hmsComm;

	@Autowired
	OrderServiceCommunicator omsComm;

	@PostConstruct
	public void init() {
		HotelMissingInfoLoggingUtil.init(logServiceCommunicator);
		LogUtils.init(logServiceCommunicator, gnCachingComm);
		ServiceUtils.init(gnComm, userSrvComm);
		BaseUtils.init(gnComm, fmsComm, omsComm, userSrvComm);
		BulkUploadUtils.init(gnCachingComm);
		BaseHotelUtils.init(hmsComm, gnComm);
	}

}
