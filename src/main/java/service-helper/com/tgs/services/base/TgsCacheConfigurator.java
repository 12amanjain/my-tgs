package com.tgs.services.base;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Service
@EnableCaching(mode = AdviceMode.PROXY)
public class TgsCacheConfigurator {

    @Bean
    public CacheManager cacheManager() {
        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        Caffeine<Object, Object> cacheBuilder = Caffeine.newBuilder().maximumSize(11).expireAfterWrite(2, TimeUnit.HOURS);
        cacheManager.setCacheNames(Arrays.asList("default", "fms", "gms", "es", "oms", "pms", "cms", "log", "msg", "ums"));
        cacheManager.setAllowNullValues(false);
        cacheManager.setCaffeine(cacheBuilder);
        return cacheManager;
    }

    @Bean("customKeyGenerator")
    public KeyGenerator keyGenerator() {
        return new CustomKeyGenerator();
    }
}
