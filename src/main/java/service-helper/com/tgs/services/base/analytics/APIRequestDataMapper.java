package com.tgs.services.base.analytics;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.APIRequestData;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Setter
@Getter
public class APIRequestDataMapper extends Mapper<APIRequestData> {

	private User user;
	private ContextData contextData;
	private String endPoint;
	private Long timeInMillsec;
	private Integer errorCode;
	private String errMsg;
	private Object requestObj;
	private String combination;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = APIRequestData.builder().build();
		}
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output.setEndpoint(endPoint);
		// output.setRequestObj(requestObj);
		output.setTimeinmillsec(timeInMillsec);
		output.setErrorcode(errorCode);
		output.setErrmsg(errMsg);
		output.setCombination(combination);
	}

}
