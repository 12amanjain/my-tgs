package com.tgs.services.base.analytics;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.datamodel.APIRequestData;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.LogExcludeStrategy;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class BaseAnalyticsHelper {

	@Autowired
	ElasticSearchCommunicator elasticSearchCommunicator;

	@Autowired
	KafkaServiceCommunicator kafkaService;

	public void store(Mapper<APIRequestData> mapper) {
		ExecutorUtils.getAnalyticsThreadPool().submit(() -> {
			try {
				APIRequestData requestData = mapper.convert();
				QueueData queueData = QueueData.builder().key(ESMetaInfo.LOG.getIndex())
						.value(GsonUtils.getGson(Arrays.asList(new LogExcludeStrategy()), null).toJson(requestData))
						.build();
				kafkaService.queue(Arrays.asList(QueueDataType.ELASTICSEARCH), queueData);
				// elasticSearchCommunicator.addDocument(requestData, ESMetaInfo.LOG);
			} catch (Exception e) {
				log.error("Unable to add data in elasticsearch {}", GsonUtils.getGson().toJson(mapper), e);
			}
		});
	}
}
