package com.tgs.services.base.analytics;

import java.time.LocalDateTime;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.ObjectUtils;
import com.tgs.services.base.BaseAnalyticsQuery;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.extern.slf4j.Slf4j;

@Builder
@Slf4j
public class BaseAnalyticsQueryMapper extends Mapper<BaseAnalyticsQuery> {

	private User user;
	private ContextData contextData;

	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = BaseAnalyticsQuery.builder().build();
		}
		output = new GsonMapper<>(user, output, BaseAnalyticsQuery.class).convert();
		if (user != null) {
			output.setUsername(user.getName());
			output.setUserid(user.getUserId());
			output.setGenerationtime(LocalDateTime.now());
			User emulateUser = user.getEmulateUser();
			if (!ObjectUtils.isEmpty(emulateUser)) {
				output.setEmulateuserid(emulateUser.getUserId());
				output.setEmulateusername(emulateUser.getName());
			}
		}
		if (contextData != null && contextData.getHttpHeaders() != null) {
			output.setIp(contextData.getHttpHeaders().getIp());
			output.setDevice(contextData.getHttpHeaders().getDevice());
			output.setBrowserversion(contextData.getHttpHeaders().getBrowserVersion());
			output.setBrowser(contextData.getHttpHeaders().getBrowser());
			output.setDeviceid(contextData.getHttpHeaders().getDeviceid());

			/**
			 * This is to track any additional values coming in API request
			 */
			output.setKey1(StringUtils.defaultIfBlank(contextData.getHttpHeaders().getKey1(), null));
			output.setKey2(StringUtils.defaultIfBlank(contextData.getHttpHeaders().getKey2(), null));
			output.setKey3(StringUtils.defaultIfBlank(contextData.getHttpHeaders().getKey3(), null));

			try {
				output.setResponsetimems(contextData.calculateCheckPointTimeDiff(
						SystemCheckPoint.REQUEST_STARTED.name(), SystemCheckPoint.REQUEST_FINISHED.name()));

				output.setExternalapitimems(contextData.calculateCheckPointTimeDiff(
						SystemCheckPoint.EXTERNAL_API_STARTED.name(), SystemCheckPoint.EXTERNAL_API_FINISHED.name()));

				output.setExternalapiparsingtimems(
						contextData.calculateCheckPointTimeDiff(SystemCheckPoint.EXTERNAL_API_FINISHED.name(),
								SystemCheckPoint.EXTERNAL_API_PARSING_FINISHED.name()));

			} catch (Exception e) {
				// Temp logs it should be removed post stable system
				log.error("Unable to calculate timings ", e);
			}
		}
		try {
			output.setHostname(ServiceUtils.getHostName());
		} catch (Exception e) {
			log.info("Error occured in host name ", e);
		}
	}
}
