package com.tgs.services.base.analytics;

import org.apache.commons.lang.BooleanUtils;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.SignInAttemptQuery;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class SignInAttempQueryMapper extends Mapper<SignInAttemptQuery> {

	private User user;
	private ContextData contextData;
	private Integer attempts;
	private boolean wrongPassword;
	private boolean limitCrossed;


	@Override
	protected void execute() throws CustomGeneralException {
		if (output == null) {
			output = SignInAttemptQuery.builder().build();
		}
		BaseAnalyticsQueryMapper.builder().user(user).contextData(contextData).build().setOutput(output).convert();
		output.setAttempts(attempts);
		output.setWrongpassword(wrongPassword);
		output.setLimitcrossed(limitCrossed);
	}
}
