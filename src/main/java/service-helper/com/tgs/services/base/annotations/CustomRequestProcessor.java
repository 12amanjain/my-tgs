package com.tgs.services.base.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.AreaRole;

@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface CustomRequestProcessor {

	AreaRole areaRole();

	UserRole[] includedRoles() default {UserRole.ADMIN};

	UserRole[] excludedRoles() default {};

	UserRole[] emulatedAllowedRoles() default {};

	boolean isMidOfficeAllowed() default false;
}
