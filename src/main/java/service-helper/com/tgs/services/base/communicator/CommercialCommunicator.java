package com.tgs.services.base.communicator;

import java.util.List;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.cms.datamodel.commission.CommissionPlan;
import com.tgs.services.cms.datamodel.creditcard.CreditCardFilter;
import com.tgs.services.cms.datamodel.creditcard.CreditCardInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.ums.datamodel.User;

@Service
public interface CommercialCommunicator {

	public void resetCommission(TripInfo tripInfo, User user);

	public void resetCommission(List<TripInfo> tripInfos, User user);

	public void processUserCommission(TripInfo tripInfo, User user, AirSearchQuery searchQuery);

	public void processUserCommission(List<TripInfo> tripInfos, User user, AirSearchQuery searchQuery);

	public boolean processUserCommissionInHotel(HotelInfo hotelInfo, User user, HotelSearchQuery searchQuery);

	public boolean processUserCommissionInHotels(List<HotelInfo> hotelInfo, User user, HotelSearchQuery searchQuery);

	public void processTourCode(TripInfo tripInfo, User user, AirSearchQuery searchQuery);

	public void processAirCommission(TripInfo tripInfo, User user);

	public void processAirCommission(List<TripInfo> tripInfos, User user);

	public CreditCardInfo getCreditCardInfo(TripInfo tripInfo, User user);

	public CreditCardInfo getCreditCardById(Long id);

	public List<CreditCardInfo> getCreditCardInfoList(CreditCardFilter cardFilter);

	public CommissionPlan getCommissionPlanFromCache(String planId);

	public CreditCardInfo getVirtualCreditCard(TripInfo tripInfo, User user, GstInfo billingEntity);

	public boolean isVirtualCreditCardSupported(String bookingId, User user, GstInfo billingEntity);

	public boolean isVirtualCreditCardSupported(List<TripInfo> trips, User user, GstInfo billingEntity);

}
