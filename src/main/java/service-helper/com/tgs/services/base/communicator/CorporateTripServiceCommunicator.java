package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.trip.datamodel.CorporateTrip;

@Service
public interface CorporateTripServiceCommunicator {

	void linkBookingIdWithTrip(String tripId, String bookingId, String status);

	Map<String, CorporateTrip> findByBookingIds(List<String> bookingIds);
}
