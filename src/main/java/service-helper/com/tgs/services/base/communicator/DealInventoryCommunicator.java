package com.tgs.services.base.communicator;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import com.tgs.filters.InventoryOrderFilter;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.ims.datamodel.InventoryOrder;

import com.tgs.services.ims.datamodel.SeatAllocation;
import com.tgs.services.ims.datamodel.air.AirInventory;
import com.tgs.services.ims.datamodel.air.AirInventoryInfo;
import com.tgs.services.ims.datamodel.air.AirInventoryTripTiming;
import com.tgs.services.ims.restmodel.air.AirSeatInventoryFilter;

public interface DealInventoryCommunicator {

	public List<TripInfo> findTripInfos(AirSeatInventoryFilter filter);

	public void revertSeatsSold(String inventoryId, LocalDate validOn, Integer seatsSold);

	public String fetchPnrAndUpdateSeatsIfAvailable(String inventoryId, LocalDate validOn, int paxCount);

	public Map<PaxType, Double> fetchFareBeforeBooking(String inventoryId, LocalDate validOn);

	public void saveInventoryOrder(InventoryOrder order);

	public String getInventoryName(String inventoryId);

	public List<InventoryOrder> fetchInventoryOrders(InventoryOrderFilter filter);

	//public  DbSeatAllocation getSeatAllocationFromDb(Long id);

	public SeatAllocation getSeatAllocationFromDb(String inventoryId, LocalDate validOn);
	
	public Integer getTotalSeat(Long id);

	public Map<PaxType, FareDetail> getRatePlanFareDetails(String inventoryId, LocalDate validOn,
			Integer seatRemaining);

	public AirInventory getAirInventory(Long id);

	public AirInventoryTripTiming getFlightsTiming(String searchKey);

	public void storeFlightTimings(String tripkey,  Map<String, AirInventoryInfo> airlineSegmentWiseTimings);

}
