package com.tgs.services.base.communicator;

import java.util.List;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.es.datamodel.ESSearchRequest;
import com.tgs.services.es.restmodel.ESAutoSuggestionResponse;

public interface ElasticSearchCommunicator {

	public void addDocument(@NotEmpty Object input, ESMetaInfo metaInfo);

	public void addBulkDocument(@NotEmpty List<? extends Object> input, ESMetaInfo metaInfo);

	public void deleteIndex(ESMetaInfo metaInfo);

	public void addBulkDocuments(@NotNull List<? extends Object> documents, ESMetaInfo esMetaInfo);

	public ESAutoSuggestionResponse getSuggestions(ESSearchRequest searchRequest);

}
