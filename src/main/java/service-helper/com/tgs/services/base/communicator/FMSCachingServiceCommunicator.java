package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Map;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.restmodel.AirReviewResponse;

public interface FMSCachingServiceCommunicator {

	/**
	 * expiration will in seconds
	 */
	public <V> boolean store(String key, String bin, V value, CacheMetaInfo metaInfo);

	/**
	 * expiration will in seconds
	 * 
	 * @param trips
	 * @param key
	 * @param expiration
	 * @return
	 */
	public boolean cacheFlightListing(List<TripInfo> trips, String key, CacheMetaInfo metaInfo);

	public <V> List<V> fetchValue(String key, Class<V> classofV, CacheMetaInfo metaInfo);
	
	public <V> List<V> fetchPartionValues(String key, Class<V> classofV, CacheMetaInfo metaInfo);

	/**
	 * If you want to fetch value corresponding to a particular bin
	 * 
	 * @param key
	 * @param classofV
	 * @param binValue
	 * @return
	 */
	public <V> V fetchValue(String key, Class<V> classofV, String set,String binValue);

	/**
	 * If you want to fetch value corresponding to list of bins
	 * 
	 * @param key
	 * @param classofV
	 * @param binValue
	 * @return
	 */
	public <V> Map<String, V> fetchValue(String key, Class<V> classofV, String set,String... binValue);

	public boolean deleteRecord(CacheMetaInfo metaInfo);

	/**
	 * Fetch {@code TripInfo} for given priceId.
	 * 
	 * @param priceId
	 * @return
	 */
	public TripInfo getTripInfo(String priceId);

	/**
	 * Fetch {@code AirSearchQuery} for given searchId.
	 * 
	 * @param searchId
	 * @return
	 */
	public AirSearchQuery getSearchQuery(String searchId);

	/**
	 * Fetch {@code AirRevieResponse} for given bookingId.
	 * 
	 * @param bookingId
	 * @return
	 */
	public AirReviewResponse getAirReviewResponse(String bookingId);
	
	public void scanAndDelete(CacheMetaInfo metaInfo);

}
