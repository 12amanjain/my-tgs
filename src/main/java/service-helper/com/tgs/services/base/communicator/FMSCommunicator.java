package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Map;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.enums.AmendmentType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.fms.datamodel.AirAnalyticsType;
import com.tgs.services.fms.datamodel.AirImportPnrBooking;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.AirlineInfo;
import com.tgs.services.fms.datamodel.BookingSegments;
import com.tgs.services.fms.datamodel.ProcessedTripInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorInfo;
import com.tgs.services.fms.datamodel.airconfigurator.AirConfiguratorRuleType;
import com.tgs.services.fms.datamodel.airconfigurator.NameLengthLimit;
import com.tgs.services.fms.datamodel.farerule.FareRuleInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierConfiguration;
import com.tgs.services.fms.datamodel.supplier.SupplierInfo;
import com.tgs.services.fms.datamodel.supplier.SupplierRule;
import com.tgs.services.fms.restmodel.AirReviewResponse;
import com.tgs.services.fms.restmodel.AirSearchRequest;
import com.tgs.services.fms.restmodel.AirSearchResponse;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.air.AirOrderItem;
import com.tgs.services.oms.restmodel.AirCreditShellRequest;
import com.tgs.services.oms.restmodel.AirCreditShellResponse;
import com.tgs.services.oms.restmodel.air.AirBookingRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationRequest;
import com.tgs.services.oms.restmodel.air.AirCancellationResponse;
import com.tgs.services.oms.restmodel.air.AirImportPnrBookingRequest;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.points.datamodel.AirRedeemPointsData;
import com.tgs.services.points.restmodel.AirRedeemPointResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.vms.restmodel.FlightVoucherValidateRequest;
import com.tgs.services.vms.restmodel.FlightVoucherValidateResponse;

public interface FMSCommunicator {

	public List<TripInfo> getTripInfos(AirBookingRequest bookingRequest);

	public List<TripInfo> getTripInfosFromReviewResponse(String bookingId);

	public ProcessedTripInfo getProcessTripInfo(TripInfo tripInfo);

	public AirReviewResponse getAirReviewResponse(String bookingId, String oldBookingId);

	public AirlineInfo getAirlineInfo(String airlineCode);

	public AirportInfo getAirportInfo(String airportCode);

	public List<AirportInfo> fetchByCity(String city);

	public SupplierInfo getSupplierInfo(String supplierId);

	public SupplierRule getSupplierRule(SupplierInfo supplierInfo);

	public SupplierConfiguration getSupplierConfiguration(String supplierId);

	public AirConfiguratorInfo getAirConfigRule(AirConfiguratorRuleType ruleType, User user);

	AirConfiguratorInfo getAirConfigRuleInfo(AirConfiguratorRuleType ruleType, FlightBasicFact fact);

	public NameLengthLimit getAirNameLength(FlightBasicFact fact);

	public FareRuleInfo getFareRule(long id);

	// Booking Related
	public void doBooking(List<TripInfo> tripInfos, Order order);

	public void doConfirmBooking(List<TripInfo> tripInfos, Order order);

	public AirImportPnrBooking retrieveBooking(AirImportPnrBookingRequest pnrRequest) throws Exception;

	public boolean doConfirmFare(Order order);

	public void updateCancellationAndRescheduleFees(TripInfo tripInfo, User bookingUser);

	public void updateCancellationAndRescheduleFees(SegmentInfo segmentInfo, User bookingUser);

	public boolean isValidSource(Integer sourceId);

	public List<String> getTermsAndConditions(List<TripInfo> tripInfos, User bookingUser);

	public double getTotalFareComponentAmount(String bookingId, String oldBookingId, FareComponent fareComponent);

	public double getTotalFareComponentAmount(TripInfo tripInfo, FareComponent fareComponent);

	public void addBookToAnalytics(Order order, List<TripInfo> tripInfos, List<PaymentRequest> paymentRequests,
			String errorMsg, AirAnalyticsType analyticsType, AirSearchQuery searchQuery);

	public List<SupplierInfo> getSupplierForSourceId(String sourceId);

	public AirSearchQuery getSearchQueryFromReviewResponse(AirBookingRequest request);

	public void releasePnr(Map<String, BookingSegments> supplierWiseBookingSegments, Order order);

	public AirCancellationResponse getCancellationFare(AirCancellationRequest cancellationRequest, Order order)
			throws Exception;

	public void sendCancellationDataToAnalytics(AirCancellationRequest cancellationRequest, Order order,
			AirCancellationResponse cancellationReviewResponse, Double amountToBeRefunded, boolean isReview);

	public AirCancellationResponse confirmCancellation(AirCancellationRequest cancellationRequest,
			AirCancellationResponse airCancellationResponse, Order order);

	public AirSearchQuery getSearchQueryFromTripInfos(List<TripInfo> tripInfos);

	public AirSearchResponse searchFlights(AirSearchRequest airSearchRequest) throws Exception;

	public Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingPNR(List<TripInfo> tripInfos);

	public Map<String, BookingSegments> getSupplierWiseBookingSegmentsUsingSession(List<TripInfo> trips,
			String bookingId, User bookingUser);

	public SearchType getSearchTypeFromTrips(List<TripInfo> tripInfoList);

	public boolean isSSRRefundable(AirOrderItem airOrderItem, User bookingUser);

	public void updateAmendmentClientFee(AmendmentType amendmentType, SegmentInfo segmentInfo, User userFromCache,
			FareComponent fc);

	public AirCreditShellResponse fetchCreditBalance(AirCreditShellRequest creditShellRequest);

	public boolean isGeneralAndSourceApplicable(List<TripInfo> tripInfoList, User bookingUser);

	public AirRedeemPointResponse calcOrApplyPoints(AirRedeemPointsData request, boolean modifyfareDetail);

	public FlightVoucherValidateResponse applyVoucher(FlightVoucherValidateRequest request, boolean modifyFareDetail);

	public boolean isSSRRefundable(TripInfo tripInfo, User bookingUser);

}
