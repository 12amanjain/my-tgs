package com.tgs.services.base.communicator;

import java.io.File;
import java.util.List;
import java.util.Map;
import com.aerospike.client.Operation;
import com.aerospike.client.query.Filter;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;

public interface GeneralCachingCommunicator {

	public <V> void store(CacheMetaInfo metaInfo, Map<String, V> binMap, boolean compress, boolean plainData,
			int expiration);

	public <V> void asyncStore(CacheMetaInfo metaInfo, Map<String, Map<String, V>> keyValueMap);

	public <V> Map<String, Map<String, V>> getResultSet(CacheMetaInfo metaInfo, Class<V> classofV, Filter filter);

	public List<Map<String, Object>> getResultSetFromUDF(CacheMetaInfo metaInfo, String functionName,
			String methodName);

	public <V> Map<String, Map<String, V>> get(CacheMetaInfo metaInfo, Class<V> classofV);

	/**
	 * Use this method to delete record for a specified key.
	 */
	public boolean delete(CacheMetaInfo metaInfo);

	/**
	 * Use this method to remove records from a namespace/ truncate
	 */
	public void truncate(CacheMetaInfo metaInfo);

	public <V> Map<String, V> get(CacheMetaInfo metaInfo, Class<V> classofV, Boolean compress, Boolean plainData,
			String... bin);

	public <V> Map<String, List<V>> getList(CacheMetaInfo metaInfo, Class<V> classofV, Boolean compress,
			Boolean plainData, String... bin);

	public <V> V getBinValue(CacheMetaInfo metaInfo, Class<V> classofV, Boolean compress, Boolean plainData,
			String bin);

	/**
	 * This method append {@values} in aerospike list
	 * 
	 * @param metaInfo
	 * @param bin
	 * @param values
	 * @param compress
	 * @param plainData
	 * @param expiration
	 */
	public <V> void store(CacheMetaInfo metaInfo, String bin, List<V> values, boolean compress, boolean plainData,
			int expiration);

	public <V> void storeInQueue(CacheMetaInfo metaInfo);

	public <V> V fetchFromQueue(CacheMetaInfo metaInfo);

	public <K, V> void storeInMap(CacheMetaInfo metaInfo, String bin, Map<K, V> map);

	/**
	 * This method is used to perform multiple operations in aerospike bin in the same transaction. Like, incrementing
	 * the bin value and fetching bin value in one go.
	 */
	public <V> V performOperationsAndFetchBinValue(CacheMetaInfo metaInfo, String bin, Class<V> classofV,
			int expiration, Operation... operations);

}
