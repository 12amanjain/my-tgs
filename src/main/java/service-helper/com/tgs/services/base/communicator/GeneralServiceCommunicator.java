package com.tgs.services.base.communicator;

import java.time.LocalDateTime;
import java.util.List;

import com.tgs.filters.GstInfoFilter;
import com.tgs.filters.IncidenceFilter;
import com.tgs.filters.NoteFilter;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.datamodel.GstInfo;
import com.tgs.services.base.datamodel.TravellerInfo;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.CountryInfo;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.gms.datamodel.Note;
import com.tgs.services.gms.datamodel.PolicyFilter;
import com.tgs.services.gms.datamodel.Incidence.Incidence;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.restmodel.PolicyInfoResponse;
import com.tgs.services.ums.datamodel.User;

public interface GeneralServiceCommunicator {

	public List<Document> fetchDocument(CollectionServiceFilter filter);

	public List<Document> fetchGeneralDocument(CollectionServiceFilter filter);

	LocalDateTime nextWorkingDay(LocalDateTime dateTime);

	public List<Document> fetchRoleSpecificDocument(CollectionServiceFilter filter);

	public PolicyInfoResponse fetchPolicies(PolicyFilter filter);

	public List<ConfiguratorInfo> getConfiguratorInfos(ConfiguratorRuleType ruleType);

	public List<ConfiguratorInfo> getConfiguratorInfosFromDB(ConfiguratorRuleType ruleType);

	public ConfiguratorInfo getConfiguratorInfo(ConfiguratorRuleType ruleType);

	public <T> T getConfigRule(ConfiguratorRuleType ruleType, IFact fact);

	public void addToSystemAudit(com.tgs.services.gms.datamodel.systemaudit.SystemAudit systemAudit);

	public void addNote(Note noteInfo);

	public List<Note> getNotes(NoteFilter filter);

	public CountryInfo getCountryInfo(String code);

	public String getCountryCode(String countryName);

	List<Incidence> getIncidences(IncidenceFilter filter);

	public void saveBillingEntity(GstInfo gstInfo);

	public void saveTravellerInfo(TravellerInfo travellerInfo, DeliveryInfo deliveryInfo, User user);

	public String getUIConf();

	public List<GstInfo> searchBillingEntity(GstInfoFilter filter);

	public List<Document> fetchGeneralRoleSpecificDocument(CollectionServiceFilter filter);

}
