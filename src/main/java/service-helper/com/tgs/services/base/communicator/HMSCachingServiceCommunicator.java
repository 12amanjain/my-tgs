package com.tgs.services.base.communicator;

import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;

public interface HMSCachingServiceCommunicator {

	public <V> boolean store(CacheMetaInfo metaInfo);

	public <V> V fetchValue(Class<V> classofV, CacheMetaInfo metaInfo);

	public boolean delete(CacheMetaInfo metaInfo);
	
	public void truncate(CacheMetaInfo metaInfo);
}
