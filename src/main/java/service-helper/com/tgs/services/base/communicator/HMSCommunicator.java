package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Set;
import com.tgs.services.hms.HotelBasicFact;
import com.tgs.services.hms.analytics.HotelBookingAnalyticsInfo;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelImportedBookingInfo;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.HotelSearchResult;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import com.tgs.services.hms.datamodel.supplier.HotelSupplierInfo;
import com.tgs.services.hms.restmodel.HotelReviewResponse;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelImportBookingParams;
import com.tgs.services.points.datamodel.HotelRedeemPointData;
import com.tgs.services.points.restmodel.HotelRedeemPointResponse;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;
import com.tgs.services.vms.restmodel.HotelVoucherValidateRequest;
import com.tgs.services.vms.restmodel.HotelVoucherValidateResponse;

public interface HMSCommunicator {

	public HotelInfo getHotelInfo(String bookingId);
	
	public HotelReviewResponse getHotelReviewResponse(String bookingId);

	public double getMarkup(String bookingId);

	public void callSupplierBook(HotelInfo hInfo, String bookingId);
	
	public boolean callSupplierConfirmBook(HotelInfo hInfo , String bookingId);

	public boolean callSupplierCancelBooking(HotelInfo hInfo, String bookingId);
	
    public HotelImportedBookingInfo retrieveBooking(HotelImportBookingParams importBookingParams, User user) throws Exception;
	
    public HotelConfiguratorInfo getHotelConfigRule(HotelConfiguratorRuleType ruleType);
    
    public HotelConfiguratorInfo getHotelConfigRule(HotelConfiguratorRuleType ruleType, HotelBasicFact fact);

	public HotelSupplierInfo getHotelSupplierInfo(String supplierId);
	
    public void addBookingToAnalytics(HotelBookingAnalyticsInfo analyticsInfo);
    
    public void updateUserFee(HotelInfo hInfo , UserFeeAmount userFeeAmount);

	public HotelSearchResult getCrossSellHotelSearchResult(HotelSearchQuery searchQuery);

	public List<HotelRoomCategory> fetchRoomCategoriesById(Set<Long> ids);
	
	public List<HotelRoomTypeInfo> fetchRoomTypesById(Set<Long> ids);

	public HotelRedeemPointResponse calcOrApplyPoints(HotelRedeemPointData request, boolean modifyfareDetail);

	public HotelVoucherValidateResponse applyVoucher(HotelVoucherValidateRequest request, boolean modifyFareDetail);
	
	public void updateBookingStatus(HotelInfo hInfo, Order order);

	public void updateHotelConfirmationNumber(HotelInfo hInfo, Order order);
	}

