package com.tgs.services.base.communicator;

import com.tgs.services.oms.datamodel.hotel.HotelOrderItem;
import org.springframework.stereotype.Service;

import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.oms.datamodel.Order;
import com.tgs.services.oms.datamodel.hotel.HotelItemStatus;

import java.util.List;

@Service
public interface HotelOrderItemCommunicator {

	void updateOrderItem(HotelInfo hInfo, Order order, HotelItemStatus itemStatus);

	HotelInfo getHotelInfo(String bookingId);

	List<HotelOrderItem> getHotelOrderItems(List<String> bookingIds);
	
	void updateOrderItemStatus(Order order, HotelItemStatus itemStatus);
	
}
