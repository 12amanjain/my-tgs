package com.tgs.services.base.communicator;

import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;

import java.util.List;

public interface KafkaServiceCommunicator {

    public void queue(QueueDataType queuetype, QueueData queueData);

    public void queue(List<QueueDataType> queueTypes, QueueData queueData);

}
