package com.tgs.services.base.communicator;

import com.tgs.services.base.LogData;

import java.util.List;

public interface LogServiceCommunicator {

	public void store(List<LogData> logData);

	public void store(LogData logData);
}
