package com.tgs.services.base.communicator;

import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.whatsApp.WhatsAppAttributes;

public interface MsgServiceCommunicator {

	public <T extends EmailAttributes> void sendMail(AbstractMessageSupplier<T> msgAttr);

	public void sendMail(EmailAttributes msgAttr);

	public void sendMessage(SmsAttributes msgAttr);

	public void sendWhatsAppMessage(WhatsAppAttributes waapAttr);
}
