package com.tgs.services.base.communicator;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.ums.restmodel.ResetPasswordRequest;

public interface OTPCommunicator {

	public BaseResponse vaidateOtp(ResetPasswordRequest resetRequest) throws Exception;

	public BaseResponse validateOtp(String requestId, String otp, String email, String mobile);

	public void updateConsumedOtp(String requestId);

	public void generateOtp(OtpToken otpToken);

	public OtpToken validateOtp(String requestId, String otp, int attemptCount);

}
