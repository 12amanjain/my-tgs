package com.tgs.services.base.communicator;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import com.tgs.filters.PaymentFilter;
import com.tgs.services.base.datamodel.UserBalanceSummary;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.enums.PointsType;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.CreditPolicy;
import com.tgs.services.pms.datamodel.NRCredit;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentConfigurationRule;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentRuleType;
import com.tgs.services.pms.datamodel.UserPoint;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.pms.datamodel.WalletPaymentRequest;
import com.tgs.services.pms.restmodel.PaymentModeRequest;
import com.tgs.services.pms.restmodel.PaymentModeResponse;
import com.tgs.services.pms.ruleengine.PaymentFact;
import com.tgs.services.ums.datamodel.UserDuesSummary;
import com.tgs.utils.exception.PaymentException;

public interface PaymentServiceCommunicator {

	CreditPolicy getPolicy(String policyId);

	List<Payment> doPaymentsUsingPaymentRequests(List<PaymentRequest> paymentRequests) throws PaymentException;

	Map<String, List<CreditLine>> fetchCreditLineFromCache(List<String> userIds);

	List<CreditLine> fetchCreditLineFromCache(String userId);

	Map<String, List<NRCredit>> fetchNRCreditsFromCache(List<String> userIds);

	List<NRCredit> fetchNRCreditsFromCache(String userId);

	boolean hasSufficientBalance(WalletPaymentRequest request);

	List<Payment> fetchPaymentByBookingId(String bookingId);

	List<PaymentConfigurationRule> getApplicableRulesOutput(PaymentFact fact, PaymentRuleType type,
			PaymentMedium medium);

	List<Payment> search(PaymentFilter paymentFilter);

	Map<String, BigDecimal> getUserBalanceFromCache(PaymentMetaInfo paymentMetaInfos);

	Map<String, Map<PointsType, BigDecimal>> getUserPoints(List<String> userIds);

	UserWallet updateUserWallet(UserWallet wallet);

	UserPoint updateUserPoints(UserPoint userPoints);

	UserBalanceSummary getUserBalanceSummary(String userId);

	UserDuesSummary getUserDueSummary(String userId);

	void deleteFailedPaymentsByRefId(String bookingId);

	PaymentModeResponse getPaymentModeResponse(PaymentModeRequest paymentModeRequest);


}
