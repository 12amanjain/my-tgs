package com.tgs.services.base.communicator;

import com.tgs.services.pms.restmodel.RedeemPointsResponse;
import com.tgs.services.points.datamodel.AirRedeemPointsData;
import com.tgs.services.points.datamodel.HotelRedeemPointData;
import com.tgs.services.points.datamodel.ReedemPointsData;
import com.tgs.services.points.restmodel.AirRedeemPointResponse;
import com.tgs.services.points.restmodel.HotelRedeemPointResponse;

public interface PointsServiceCommunicator {


	public RedeemPointsResponse validatePoints(ReedemPointsData request, boolean modifyfareDetail);

	public AirRedeemPointResponse validateAirRewardPoints(AirRedeemPointsData request, boolean modifyfareDetail);

	public HotelRedeemPointResponse validateHotelRewardPoints(HotelRedeemPointData request,
			boolean modifyfareDetail);

}
