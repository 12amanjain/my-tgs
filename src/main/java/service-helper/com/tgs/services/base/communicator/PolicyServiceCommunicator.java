package com.tgs.services.base.communicator;

import java.util.List;
import java.util.Map;
import com.tgs.services.base.ruleengine.IPolicyOutput;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ps.datamodel.UserPolicy;

public interface PolicyServiceCommunicator {

	public UserPolicy fetchUserPolicies(String userId);
	
	public <T extends IPolicyOutput> T fetchPolicyOutput(String userId, String policyId);
	
	public Map<String,  List<Policy>> fetchUserPolicies (List<String> userIds, List<String> labels);

}
