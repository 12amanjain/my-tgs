package com.tgs.services.base.communicator;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.DailyUsageInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import com.tgs.services.ums.restmodel.RegisterRequest;

public interface UserServiceCommunicator {

	public List<User> getUsers(UserFilter userFilter);

	public User getUser(UserFilter userFilter);

	public User getUserFromUsrService(UserFilter userFilter);

	public User getUserFromCache(String userId);

	String getUserPolicy(String userId);

	public boolean updateUserInCache(String userId);

	public Map<String, User> getUsersFromCache(UserCacheFilter userFilter);

	public List<UserFee> getUserFee(String userId, Product product, UserFeeType feeType);

	public List<String> getAllowedUserIds(String userId);

	public List<String> getAllowedPartnerIds(String userId, boolean canSeePartnerData);

	public DailyUsageInfo getDailyUsageInfo(String userId);

	public void storeDailyUsageInfo(DailyUsageInfo usageInfo);

	List<AreaRoleMapping> getAreaRoleMapByAreaRole(Collection<AreaRole> areaRoleList);

	Set<AreaRole> getAllowedAreaRoles(ApplicationArea area, User user);

	void lockAccount(String userId);

	void unlockAccount(String userId);

	Map<String, Set<User>> getUserRelations(List<String> userIds);

	public User updateUser(User user);

	public User getUser(String apiKey);

	public User registerUser(RegisterRequest request) throws Exception;

	void updateProfile(String userId, UserProfile profile);

	Map<String, Set<String>> getJWTTokensOfUsers(List<String> userIds);

}
