package com.tgs.services.base.communicator;

import java.util.List;
import com.tgs.filters.VoucherConfigurationFilter;
import com.tgs.services.vms.restmodel.FlightVoucherValidateRequest;
import com.tgs.services.vms.restmodel.FlightVoucherValidateResponse;
import com.tgs.services.vms.restmodel.HotelVoucherValidateRequest;
import com.tgs.services.vms.restmodel.HotelVoucherValidateResponse;
import com.tgs.services.vms.restmodel.VoucherValidateRequest;
import com.tgs.services.vms.restmodel.VoucherValidateResponse;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;

public interface VoucherServiceCommunicator {

	VoucherConfiguration saveVoucherConfiguration(VoucherConfiguration voucherConfiguration);

	List<VoucherConfiguration> findAllWithDescPriority(VoucherConfigurationFilter configurationFilter);

	VoucherValidateResponse applyVoucher(VoucherValidateRequest request, boolean modifyFareDetail);

	FlightVoucherValidateResponse applyAirVoucher(FlightVoucherValidateRequest voucherRq, boolean b);

	HotelVoucherValidateResponse applyHotelVoucher(HotelVoucherValidateRequest voucherRq, boolean b);
}
