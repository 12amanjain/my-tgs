package com.tgs.services.base.communicator.markup;

import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.ums.datamodel.User;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import java.util.Map;

@Service
public interface UserFeeServiceCommunicator {

	/**
	 * @param fareDetail - each paxtype fareDetail
	 * @param userFee information -fixed/percentage amount to add on faredetail
	 */
	default void setUserFeeAndUpdateTotalFare(User user, User partner, FareDetail fareDetail, double userFee,
			UserFeeType feeType) {
		Map<FareComponent, Double> fareComp = fareDetail.getFareComponents();
		double initialTotalAmount =
				fareComp.getOrDefault(FareComponent.TF, 0.0) - fareComp.getOrDefault(feeType.getTargetComponent(), 0.0);
		Double totalUserFee = initialTotalAmount + userFee;

		fareComp.put(feeType.getTargetComponent(), userFee);
		if (partner != null) {
			// This TDS is based on partner User which Partner earnings
			BaseUtils.updateDependentFareComponents(fareComp, feeType.getTargetComponent(), userFee, partner);
		}
		if (feeType.updateTotalFare()) {
			fareComp.put(FareComponent.TF, totalUserFee);
		}
	}
}
