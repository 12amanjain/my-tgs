package com.tgs.services.base.helper;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.utils.exception.UnAuthorizedException;

@Service
public class UserServiceHelper {

	@Autowired
	private static UserServiceCommunicator umsService;

	@Autowired
	public UserServiceHelper(UserServiceCommunicator umsService) throws UnAuthorizedException {
		UserServiceHelper.umsService = umsService;
	}

	public static List<String> checkAndReturnAllowedUserId(String loggedInUserId, List<String> requestedUserIdList) {
		if (UserUtils.isEmptyUserId(loggedInUserId)) {
			return requestedUserIdList;
		}

		if (TgsCollectionUtils.isEmptyStringCollection(requestedUserIdList)) {
			return umsService.getAllowedUserIds(loggedInUserId);
		}

		List<String> allowedUserIds = umsService.getAllowedUserIds(loggedInUserId);
		if (CollectionUtils.isNotEmpty(allowedUserIds)) {
			for (String userId : requestedUserIdList) {
				if (!allowedUserIds.contains(userId)) {
					throw new CustomGeneralException(SystemError.UNAUTHORIZED_ACCESS);
				}
			}
		}
		return requestedUserIdList;
	}

	public static List<String> returnPartnerIds(String loggedInUserId, boolean canSeePartnerData,
			List<String> requestedPartnerIdList) {
		List<String> allowedUserIds = umsService.getAllowedPartnerIds(loggedInUserId, canSeePartnerData);
		if (TgsCollectionUtils.isEmptyStringCollection(requestedPartnerIdList)) {
			return allowedUserIds;
		}
		if (CollectionUtils.isNotEmpty(allowedUserIds)) {
			for (String userId : requestedPartnerIdList) {
				if (!allowedUserIds.contains(userId)) {
					throw new CustomGeneralException(SystemError.UNAUTHORIZED_ACCESS);
				}
			}
		}
		return requestedPartnerIdList;
	}

}
