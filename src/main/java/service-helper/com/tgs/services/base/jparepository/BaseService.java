package com.tgs.services.base.jparepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class BaseService {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public Long getNextReferenceIdSequence() {
		return jdbcTemplate.queryForObject("select nextval('reference_id_seq')", Long.class);
	}
}
