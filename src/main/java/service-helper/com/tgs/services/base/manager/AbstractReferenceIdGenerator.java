package com.tgs.services.base.manager;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.stereotype.Service;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.utils.TgsStringUtils;
import com.tgs.services.gms.datamodel.ReferenceIdConfiguration;

@Service
public abstract class AbstractReferenceIdGenerator {

	public final String generateReferenceId(ProductMetaInfo productMetaInfo, ReferenceIdConfiguration idConfiguration) {
		String prefix = StringUtils.defaultString(getPrefix(productMetaInfo, idConfiguration));
		String suffix = StringUtils.defaultString(getSuffix(productMetaInfo, idConfiguration));
		String combination = getNextCombination(idConfiguration);
		return new StringBuilder(prefix).append(combination).append(suffix).toString();
	}

	protected abstract String getPrefix(ProductMetaInfo productMetaInfo, ReferenceIdConfiguration idConfiguration);

	protected abstract String getSuffix(ProductMetaInfo productMetaInfo, ReferenceIdConfiguration idConfiguration);

	private String getNextCombination(ReferenceIdConfiguration idConfiguration) {
		String randomString = generateRandomString(idConfiguration);
		Integer minSequenceLength = ObjectUtils.firstNonNull(idConfiguration.getMinSequenceLength(), 0);
		String nextSequence = StringUtils.leftPad(getNextSequence(), minSequenceLength, '0');
		return new StringBuilder(randomString).append(nextSequence).toString();
	}

	private String generateRandomString(ReferenceIdConfiguration idConfiguration) {
		if (idConfiguration.getRandomStringLength() == null) {
			return StringUtils.EMPTY;
		}
		return TgsStringUtils.generateRandomNumber(idConfiguration.getRandomStringLength());
	}

	protected abstract String getNextSequence();
}
