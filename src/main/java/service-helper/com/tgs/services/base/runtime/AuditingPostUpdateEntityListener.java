package com.tgs.services.base.runtime;

import java.util.Map;

import com.tgs.services.base.runtime.envers.NRCreditEnversListener;
import org.hibernate.envers.boot.internal.EnversService;
import org.hibernate.envers.event.spi.EnversPostUpdateEventListenerImpl;
import org.hibernate.event.spi.PostUpdateEvent;
import com.google.common.collect.ImmutableMap;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.runtime.envers.CreditLineEnversListener;
import com.tgs.services.base.runtime.envers.NoChangeDetectionEnversListener;
import com.tgs.services.base.runtime.envers.UserEnversListener;

public class AuditingPostUpdateEntityListener extends EnversPostUpdateEventListenerImpl{

	private static final long serialVersionUID = 1L;

	private static Map<String, Class> CUSTOM_LISTENERS_MAP = ImmutableMap.<String, Class>builder()
			.put("com.tgs.services.oms.dbmodel.DbOrder", NoChangeDetectionEnversListener.class)
			.put("com.tgs.services.ums.dbmodel.DbUser", UserEnversListener.class)
			.put("com.tgs.services.oms.dbmodel.air.DbAirOrderItem", NoChangeDetectionEnversListener.class)
			.put("com.tgs.services.ims.dbmodel.DbRatePlan", NoChangeDetectionEnversListener.class)
			//.put("com.tgs.services.pms.dbmodel.DbCreditLine", CreditLineEnversListener.class)
			.put("com.tgs.services.ums.dbmodel.DbNRCredit", NRCreditEnversListener.class)
			.build();

	public AuditingPostUpdateEntityListener(EnversService enversService) {
		super(enversService);
	}

	@Override
	public void onPostUpdate(PostUpdateEvent event) {
		final String entityName = event.getPersister().getEntityName();

		if(CUSTOM_LISTENERS_MAP.get(entityName)!= null) {
			PostUpdateEnversListener postUpdateListener = (PostUpdateEnversListener) SpringContext.getApplicationContext().
					getBean(CUSTOM_LISTENERS_MAP.get(entityName));
			if(!postUpdateListener.isValidEventForAudit(event)) {
				return;
			}
		}
		super.onPostUpdate(event);
	}

}
