package com.tgs.services.base.runtime;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.time.StopWatch;
import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.tgs.services.base.ApiResponseFlow;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.HttpHeader;
import com.tgs.services.base.LogData;
import com.tgs.services.base.datamodel.Alert;
import com.tgs.services.base.datamodel.ContextMetaInfo;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.Exclude;
import com.tgs.services.base.helper.RequestContextHolder;
import com.tgs.services.base.utils.BaseUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.SuperBuilder;
import lombok.extern.slf4j.Slf4j;

@Data
@SuperBuilder
@ToString
@Slf4j
public class ContextData {
	@Exclude
	private HttpServletRequest httpRequest;
	@Exclude
	private HttpServletResponse httpResponse;
	private User user;
	@Exclude
	private StopWatch stopWatch;
	@Exclude
	private StopWatch splitWatch;
	@Exclude
	private RequestContextHolder requestContextHolder;
	@Exclude
	private HttpHeader httpHeaders;
	private String requestJson;
	private Object requestObj;
	public Map<String, LogMetaInfo> logMap;
	private String bookingId;
	private String alreadyExistingBookingId;
	
	/**
	 * To add alerts received at different points
	 */
	private List<Alert> alerts; 
	
	/**
	 * To add inmemory layer on top of Aerospike Layer
	 */
	@Exclude
	private Map<String, Object> valueMap;
	private ContextMetaInfo metaInfo;

	/**
	 * This is being used to persist log Info according to uniqueId
	 */
	private List<String> reqIds;
	private List<LogData> logs;
	@Exclude
	private Gson dbGson;
	@Exclude
	private Gson gson;
	@Exclude
	private List<ExclusionStrategy> exclusionStrategys;
	private StopWatch searchWatch;
	Map<String, List<CheckPointData>> checkPointInfo;


	/**
	 * To add errors received at different points
	 */
	protected List<String> errorMessages;

	public void addAlert(Alert alert) {
		if (alert == null) {
			return;
		}
		if (alerts == null) {
			alerts = new ArrayList<>();
		}
		alerts.add(alert);
	}

	@SuppressWarnings("unchecked")
	public <V> Optional<V> getValue(String key) {
		if (valueMap == null || !valueMap.containsKey(key)) {
			return null;
		}
		return Optional.ofNullable((V) valueMap.get(key));
	}

	public <V> void setValue(String key, V value) {
		if (valueMap == null) {
			valueMap = new HashMap<>();
		}
		valueMap.put(key, value);
	}

	public Map<String, LogMetaInfo> getLogMap() {
		if (logMap == null) {
			logMap = new HashMap<>();
		}
		return logMap;
	}

	public List<String> getReqIds() {
		if (reqIds == null)
			reqIds = new ArrayList<>();
		return reqIds;
	}

	public void addReqIds(String v1, String v2) {
		getReqIds().add(v1);
		getReqIds().add(v2);
	}

	public void addReqIds(String v1, String v2, String v3) {
		getReqIds().add(v1);
		getReqIds().add(v2);
		getReqIds().add(v3);
	}

	public List<LogData> getLogs() {
		if (logs == null)
			logs = new ArrayList<>();
		return logs;
	}

	public String getEmulateOrLoggedInUserId() {
		return getUser() == null ? null : getUser().getEmulateOrLoggedInUserId();
	}

	/*
	 * Emulate User consist only limited information( For example : Role,UserId,Name).
	 */
	public User getEmulateOrLoggedInUser() {
		return getUser() != null ? (getUser().getEmulateUser() != null ? getUser().getEmulateUser() : getUser()) : null;
	}

	public Map<String, List<CheckPointData>> getCheckPointInfo() {
		if (checkPointInfo == null)
			checkPointInfo = new HashMap<>();
		return checkPointInfo;
	}

	public void addCheckPoint(CheckPointData checkPoint) {
		List<CheckPointData> checkPoints = null;
		if ((checkPoints = getCheckPointInfo().get(checkPoint.getType())) == null) {
			checkPoints = new ArrayList<>();
			checkPointInfo.put(checkPoint.getType(), checkPoints);
		}
		checkPoints.add(checkPoint);
	}

	public void addCheckPoints(List<CheckPointData> checkPoints) {
		if (CollectionUtils.isEmpty(checkPoints))
			return;
		checkPoints.forEach(checkPoint -> addCheckPoint(checkPoint));
	}

	public String calculateApiWiseTime(String type1, String type2) {
		Set<ApiResponseFlow> responseFlowList = BaseUtils.calculateApiDiff(type1, type2);
		if (CollectionUtils.isNotEmpty(responseFlowList)) {
			StringBuilder apiFlow = new StringBuilder();
			List<ApiResponseFlow> flowList = new ArrayList<>(responseFlowList);
			flowList.sort(Comparator.comparingLong(ApiResponseFlow::getStartTime));
			flowList.stream().forEach(apiResponseFlow -> apiFlow.append(apiResponseFlow.getType()).append("-")
					.append(apiResponseFlow.getApitime()).append(";"));
			return apiFlow.toString();
		}
		return null;
	}

	public Long calculateCheckPointTimeDiff(String type1, String type2) {
		Set<ApiResponseFlow> responseFlowList = BaseUtils.calculateApiDiff(type1, type2);
		long totalTime = 0;
		if (CollectionUtils.isNotEmpty(responseFlowList)) {
			for (ApiResponseFlow responseFlow : responseFlowList) {
				totalTime += responseFlow.getApitime();
			}
		}
		return totalTime;
	}

	public List<String> getErrorMessages() {
		if (errorMessages == null)
			errorMessages = new ArrayList<>();
		return errorMessages;
	}

	public ContextData deepCopy() {
		List<ExclusionStrategy> exclusionStrategyList = new ArrayList<>();
		exclusionStrategyList.add(new AnnotationExclusionStrategy());
		Gson gson = GsonUtils.builder().strategies(exclusionStrategyList).build().buildGsonBuilder().create();
		ContextData newContextData = gson.fromJson(gson.toJson(this), ContextData.class);
		newContextData.setHttpHeaders(this.httpHeaders);
		newContextData.setHttpRequest(httpRequest);
		newContextData.setRequestContextHolder(requestContextHolder);
		newContextData.setExclusionStrategys(exclusionStrategyList);
		return newContextData;
	}
	
	public List<ExclusionStrategy> getExclusionStrategys() {
		if (exclusionStrategys == null) {
			exclusionStrategys = new ArrayList<>();
		}
		return exclusionStrategys;
	}

}
