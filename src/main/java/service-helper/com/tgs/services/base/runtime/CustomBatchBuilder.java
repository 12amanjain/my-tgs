package com.tgs.services.base.runtime;

import org.hibernate.engine.jdbc.batch.internal.BatchBuilderImpl;
import org.hibernate.engine.jdbc.batch.internal.BatchingBatch;
import org.hibernate.engine.jdbc.batch.spi.Batch;
import org.hibernate.engine.jdbc.batch.spi.BatchKey;
import org.hibernate.engine.jdbc.spi.JdbcCoordinator;

public class CustomBatchBuilder extends BatchBuilderImpl {

    public CustomBatchBuilder(int jdbcBatchSize) {
        super(jdbcBatchSize);
        this.jdbcBatchSize = jdbcBatchSize;
    }

    public CustomBatchBuilder() {
        super(fetchBatchSize());
        this.jdbcBatchSize = tmpBatchSize;
    }

    private static int fetchBatchSize() {
        tmpBatchSize = 1; 
        return tmpBatchSize;
    }

    private int jdbcBatchSize;
    private static int tmpBatchSize;

    @Override
    public Batch buildBatch(BatchKey key, JdbcCoordinator jdbcCoordinator) {
        final Integer sessionJdbcBatchSize = jdbcCoordinator.getJdbcSessionOwner().getJdbcBatchSize();
        final int jdbcBatchSizeToUse = sessionJdbcBatchSize == null ? this.jdbcBatchSize : sessionJdbcBatchSize;
        return jdbcBatchSizeToUse > 1 ? new BatchingBatch(key, jdbcCoordinator, jdbcBatchSizeToUse)
                : new CustomBatchingBatcher(key, jdbcCoordinator);
    }

}
