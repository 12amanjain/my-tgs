package com.tgs.services.base.runtime;

public class CustomBatchValidation {

	private static final ThreadLocal<Boolean> routeContext = new ThreadLocal<>();

	public static void clearValidationReqde() {
		routeContext.remove();
	}

	public static void setValidationReqd() {
		routeContext.set(Boolean.TRUE);
	}

	public static Boolean determineIfValidationIsReqd() {
		return routeContext.get();
	}


}
