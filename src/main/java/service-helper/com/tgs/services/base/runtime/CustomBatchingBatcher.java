package com.tgs.services.base.runtime;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Map;

import org.hibernate.HibernateException;
import org.hibernate.engine.jdbc.batch.internal.AbstractBatchImpl;
import org.hibernate.engine.jdbc.batch.spi.BatchKey;
import org.hibernate.engine.jdbc.spi.JdbcCoordinator;

public class CustomBatchingBatcher extends AbstractBatchImpl {

	public CustomBatchingBatcher(BatchKey key, JdbcCoordinator jdbcCoordinator) {
		super(key, jdbcCoordinator);
		this.jdbcCoordinator = jdbcCoordinator;
	}

	private JdbcCoordinator jdbcCoordinator;

	@Override
	public void addToBatch() throws RuntimeException {
		notifyObserversImplicitExecution();
		for (Map.Entry<String, PreparedStatement> entry : getStatements().entrySet()) {
			try {
				final PreparedStatement statement = entry.getValue();
				final int rowCount = jdbcCoordinator.getResultSetReturn().executeUpdate(statement);
				if (Boolean.TRUE.equals(CustomBatchValidation.determineIfValidationIsReqd()))
					getKey().getExpectation().verifyOutcome(rowCount, statement, 0);
				jdbcCoordinator.getResourceRegistry().release(statement);
				jdbcCoordinator.afterStatementExecution();
			} catch (HibernateException | SQLException e) {
				abortBatch();
				throw new RuntimeException("Error while performing batch ",e);
			}
		}

		getStatements().clear();
	}

	@Override
	protected void doExecuteBatch() {

	}
}
