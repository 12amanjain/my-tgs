package com.tgs.services.base.runtime;

import java.lang.reflect.Type;

import com.google.gson.InstanceCreator;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomInstanceCreator<T> implements InstanceCreator<T> {

	private T object;
	private boolean newInstance;
	Class<T> clazz;

	public CustomInstanceCreator(T object, Class<T> clazz) {
		this.object = object;
		this.newInstance = false;
		this.clazz = clazz;
	}

	@Override
	public T createInstance(Type type) {
		T instance = object;
		if (newInstance) {
			try {
				instance = clazz.newInstance();
			} catch (Exception e) {
				log.error("Unable to create Instance");
			}
		}

		newInstance = true;
		return instance;
	}
}
