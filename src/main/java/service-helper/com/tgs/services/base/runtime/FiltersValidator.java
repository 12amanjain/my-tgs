package com.tgs.services.base.runtime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;
import org.springframework.validation.Validator;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.FieldValue;
import com.tgs.services.base.FilterValidation;
import com.tgs.services.base.ValidateCriterion;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.datamodel.FetchType;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.CollectionServiceFilter;
import com.tgs.services.gms.datamodel.Document;
import com.tgs.services.ums.datamodel.User;

@Component
public class FiltersValidator implements Validator {

	@Autowired
	private GeneralServiceCommunicator gmsCommunicator;

	@Override
	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {
		Map<String, Object> nonNullFieldValueMap = TgsObjectUtils.getNotNullFieldValueMap(target, true, true);
		isValidRequest(nonNullFieldValueMap, target.getClass().getSimpleName(), errors);
	}

	public boolean validateDateDuration(LocalDate createdOnAfterDateTime, LocalDate createdOnBeforeDateTime,
			Errors errors, int days) {

		if (createdOnAfterDateTime != null && createdOnBeforeDateTime == null) {
			errors.rejectValue("createdOnBeforeDateTime", SystemError.CREATED_ON_DATE_CHECK.errorCode(),
					SystemError.CREATED_ON_DATE_CHECK.getMessage());
		} else if (createdOnAfterDateTime == null && createdOnBeforeDateTime != null) {
			errors.rejectValue("createdOnAfterDateTime", SystemError.CREATED_ON_DATE_CHECK.errorCode(),
					SystemError.CREATED_ON_DATE_CHECK.getMessage());
		} else if (createdOnAfterDateTime != null && createdOnBeforeDateTime != null) {
			long gap = ChronoUnit.DAYS.between(createdOnAfterDateTime, createdOnBeforeDateTime);
			return gap < days;
		}
		return false;
	}

	public boolean isValidRequest(Map<String, Object> nonNullFieldMap, String type, Errors errors) {
		User user = SystemContextHolder.getContextData().getUser();
		if (user == null || (user != null && StringUtils.endsWith(user.getEmail(), "@technogramsolutions.com")))
			return true;
		List<Document> docList = gmsCommunicator.fetchDocument(CollectionServiceFilter.builder()
				.fetchType(FetchType.CACHE).isConsiderHierarchy(true).types(Arrays.asList(type)).build());
		docList = getDocumentList(docList, user);
		Set<String> nonNullFields =
				nonNullFieldMap.keySet().stream().map(s -> s.split("\\[")[0]).collect(Collectors.toSet());
		List<ValidateCriterion> validateCriterion = new ArrayList<ValidateCriterion>();
		if (CollectionUtils.isNotEmpty(docList)) {
			String data = docList.get(0).getData();
			FilterValidation v2 = new Gson().fromJson(data, FilterValidation.class);
			validateCriterion = v2.getValidationCriterion();
		} else {
			validateCriterion = getDefaultCombinationsOfFilters(type, UserUtils.isMidOfficeRole(user.getRole()));
		}
		List<String> fieldsInErrorMessage = new ArrayList<>();
		outer: for (ValidateCriterion criteria : validateCriterion) {
			List<FieldValue> fieldValuePairs = new ArrayList<>(criteria.getCombination());
			List<String> fields = new ArrayList<>();
			for (FieldValue pair : fieldValuePairs) {
				if (StringUtils.isNotEmpty(pair.getValue())) {
					String valueInRequest = String.valueOf(nonNullFieldMap.get(pair.getField()));
					if (!valueInRequest.equals(pair.getValue())) {
						// If this criteria is not applicable because of value mismatch then proceed for
						// other criteria
						continue outer;
					}
				} else {
					fields.add(pair.getField());
				}
			}
			fields.removeAll(nonNullFields);
			if (CollectionUtils.isEmpty(fields)) {
				if (criteria.getDays() > 0) {
					LocalDate afterDate = getDateFieldValue(nonNullFieldMap, fieldValuePairs.get(0).getField());
					LocalDate beforeDate = getDateFieldValue(nonNullFieldMap, fieldValuePairs.get(1).getField());
					if (validateDateDuration(afterDate, beforeDate, errors, criteria.getDays()))
						return true;
				} else
					return true;
			}
			if (criteria.getCombination().size() > fields.size() || fieldsInErrorMessage.isEmpty()) {
				fieldsInErrorMessage = criteria.getCombination().stream().filter(p -> StringUtils.isEmpty(p.getValue()))
						.map(p -> p.getField()).collect(Collectors.toList());;
				if (criteria.getDays() > 0) {
					fieldsInErrorMessage
							.add("and days difference should be less than ".concat(String.valueOf(criteria.getDays())));
				}
			}
		}
		if (CollectionUtils.isNotEmpty(fieldsInErrorMessage)) {
			errors.rejectValue("", SystemError.FILTER_VALIDATION.errorCode(),
					new String[] {fieldsInErrorMessage.toString()}, SystemError.FILTER_VALIDATION.getMessage());
			return false;
		}
		return true;
	}

	private List<Document> getDocumentList(List<Document> docList, User user) {
		if (docList == null)
			return null;
		List<Document> docs = new ArrayList<Document>();
		String userId = UserUtils.isPartnerUser(user) ? user.getPartnerId() : user.getUserId();
		List<String> userHierarchy = new ArrayList<String>();
		userHierarchy.add(userId);
		if (UserUtils.isApiUserRequest(user))
			userHierarchy.add("API");
		userHierarchy.add(user.getRole().name());
		outer: for (String hierarchy : userHierarchy) {
			for (Document doc : docList) {
				if (doc.getKey().contains(hierarchy)) {
					docs.add(doc);
					break outer;
				}
			}
		}
		return docs;
	}

	private LocalDate getDateFieldValue(Map<String, Object> nonNullFieldMap, String dateField) {
		if (dateField != null && nonNullFieldMap.get(dateField) != null) {
			if (nonNullFieldMap.get(dateField) instanceof LocalDate)
				return (LocalDate) nonNullFieldMap.get(dateField);
			else if (nonNullFieldMap.get(dateField) instanceof LocalDateTime)
				return ((LocalDateTime) nonNullFieldMap.get(dateField)).toLocalDate();

		}
		return null;
	}

	public List<ErrorDetail> getErrorDetailFromBindingResult(BindingResult result) {
		List<ObjectError> errors = result.getAllErrors();
		List<ErrorDetail> errorDetails = new ArrayList<>();
		errors.forEach(err -> {
			ErrorDetail e = ErrorDetail.builder().errCode(err.getCode())
					.message(String.format(err.getDefaultMessage(), err.getArguments())).build();
			errorDetails.add(e);
		});
		return errorDetails;
	}

	public List<ValidateCriterion> getDefaultCombinationsOfFilters(String type, Boolean isMidOfficeRole) {
		switch (type) {
			case "OrderFilter":
				if (isMidOfficeRole)
					return getDefaultOrderFilterForMidOfficeRole();
				return getDefaultOrderFilter();
			case "UserFilter":
				if (isMidOfficeRole)
					return getDefaultUserFilterForMidOfficeRole();
				return getDefaultUserFilter();
			case "AmendmentFilter":
				if (isMidOfficeRole)
					return getDefaultAmendmentFilterForMidOfficeRole();
				return getDefaultAmendmentFilter();
			case "CreditBillFilter":
				if (isMidOfficeRole)
					return getDefaultCreditBillFilterForMidOfficeRole();
				return getDefaultCreditBillFilter();
			case "SystemAuditFilter":
				if (isMidOfficeRole)
					return getDefaultDateFilter(45);
				return getDefaultDateFilter(90);
			case "AirLineTransactionReportFilter":
				if (isMidOfficeRole)
					return getDefaultAirLineDSRReportFilterForMidOfficeRole();
				return getDefaultDateTimeFilter(90);
			case "AirBookingTransactionReportFilter":
				if (isMidOfficeRole)
					return getDefaultAirBookingReportFilterForMidOffice();
				return getDefaultAirBookingReportFilter();
			case "ReportFilter":
				if (isMidOfficeRole)
					return getDefaultReportFilterForMidOfficeRole();
				return getDefaultDateTimeFilter(90);
			case "PaymentFilter":
				if (isMidOfficeRole)
					return getDefaultPaymentFilterForMidOfficeRole();
				return getDefaultPaymentFilter();
			case "HotelTransactionReportFilter":
				if (isMidOfficeRole)
					return getDefaultDateTimeFilter(45);
				return getDefaultDateTimeFilter(90);
			case "DSRFilter":
				if (isMidOfficeRole)
					return getDefaultAirLineDSRReportFilterForMidOfficeRole();
				return getDefaultDateTimeFilter(90);
			case "DigiToolReportFilter":
				if (isMidOfficeRole)
					return getDefaultDateTimeFilter(45);
				return getDefaultDateTimeFilter(90);
			default:
				return getDefaultDateFilter(90);
		}
	}

	private List<ValidateCriterion> getDefaultCreditBillFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDate"),
				buildFieldValue("createdOnBeforeDate"), buildFieldValue("isSettled", "true")), 90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("createdOnAfterDate"), buildFieldValue("createdOnBeforeDate")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("userIdIn"))));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultUserFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("createdOnAfterDate"), buildFieldValue("createdOnBeforeDate")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("roles"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("name"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("userId"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("userIds"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("groups"))));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultOrderFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("products[0]", "AIR")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("products[0]", "HOTEL")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("processedOnAfterDateTime"),
				buildFieldValue("processedOnBeforeDateTime"), buildFieldValue("products[0]", "AIR")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("processedOnAfterDateTime"),
				buildFieldValue("processedOnBeforeDateTime"), buildFieldValue("products[0]", "HOTEL")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("itemFilter.departedOnAfterDate"),
				buildFieldValue("itemFilter.departedOnBeforeDate")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("hotelItemFilter.checkInAfterDate"),
				buildFieldValue("hotelItemFilter.checkInBeforeDate")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingIds"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingUserIds"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingId"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.pnr"))));
		criterian.add(
				getValidateCriterian(Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.gdsPnr"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.ticketNumber"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.passengerFirstName"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.passengerLastName"))));
		return criterian;
	}

	private FieldValue buildFieldValue(String field, String value) {
		return FieldValue.builder().field(field).value(value).build();
	}

	private ValidateCriterion getValidateCriterian(List<FieldValue> combination, Integer days) {
		return ValidateCriterion.builder().combination(combination).days(days).build();
	}

	private FieldValue buildFieldValue(String field) {
		return FieldValue.builder().field(field).build();
	}

	private ValidateCriterion getValidateCriterian(List<FieldValue> combination) {
		return ValidateCriterion.builder().combination(combination).build();
	}

	private List<ValidateCriterion> getDefaultPaymentFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDate"),
				buildFieldValue("createdOnBeforeDate"), buildFieldValue("payUserIdIn")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("refIdIn"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("refId"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDate"),
				buildFieldValue("createdOnBeforeDate"), buildFieldValue("typeIn")), 60));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("payUserIdIn"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("createdOnAfterDate"), buildFieldValue("createdOnBeforeDate")), 45));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultPaymentFilter() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("refIdIn"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("refId"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("createdOnAfterDate"), buildFieldValue("createdOnBeforeDate")), 90));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultReportFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), 45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("queryFields.userIds")), 90));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultAirBookingReportFilterForMidOffice() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("queryFields.userIds")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), 45));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("departedOnAfterDate"), buildFieldValue("departedOnBeforeDate")),
				45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("departedOnAfterDate"),
				buildFieldValue("departedOnBeforeDate"), buildFieldValue("queryFields.userIds")), 90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("bookedOnAfterDateTime"), buildFieldValue("bookedOnBeforeDateTime")),
				45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookedOnAfterDateTime"),
				buildFieldValue("bookedOnBeforeDateTime"), buildFieldValue("queryFields.userIds")), 90));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultAirBookingReportFilter() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), 90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("departedOnAfterDate"), buildFieldValue("departedOnBeforeDate")),
				90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("bookedOnAfterDateTime"), buildFieldValue("bookedOnBeforeDateTime")),
				90));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultDateTimeFilter(int days) {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), days));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultAirLineDSRReportFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("queryFields.userIds")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), 45));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultAmendmentFilterForMidOfficeRole() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingIdIn"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("bookingUserIdIn")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("amendmentIdIn"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), 45));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("processedOnGreaterThan"), buildFieldValue("processedOnLessThan")),
				45));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("assignedUserIdIn")), 90));
		return criterian;
	}

	private List<ValidateCriterion> getDefaultDateFilter(int days) {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();

		return criterian;
	}

	public List<ValidateCriterion> getDefaultOrderFilter() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("products[0]", "AIR")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("products[0]", "HOTEL")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("processedOnAfterDateTime"),
				buildFieldValue("processedOnBeforeDateTime"), buildFieldValue("products[0]", "AIR")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("processedOnAfterDateTime"),
				buildFieldValue("processedOnBeforeDateTime"), buildFieldValue("products[0]", "HOTEL")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("itemFilter.departedOnAfterDate"),
				buildFieldValue("itemFilter.departedOnBeforeDate")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("hotelItemFilter.checkInAfterDate"),
				buildFieldValue("hotelItemFilter.checkInBeforeDate")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingIds"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingUserIds"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingId"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.pnr"))));
		criterian.add(
				getValidateCriterian(Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.gdsPnr"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.ticketNumber"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.passengerFirstName"))));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("itemFilter.travellerInfoFilter.passengerLastName"))));
		return criterian;
	}

	public List<ValidateCriterion> getDefaultAmendmentFilter() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime")), 90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("processedOnGreaterThan"), buildFieldValue("processedOnLessThan")),
				90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("bookingUserIdIn")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDateTime"),
				buildFieldValue("createdOnBeforeDateTime"), buildFieldValue("assignedUserIdIn")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("bookingIdIn"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("amendmentIdIn"))));
		return criterian;
	}

	public List<ValidateCriterion> getDefaultUserFilter() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("createdOnAfterDate"), buildFieldValue("createdOnBeforeDate")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("roles"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("name"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("userId"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("userIds"))));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("groups"))));
		return criterian;
	}

	public List<ValidateCriterion> getDefaultCreditBillFilter() {
		List<ValidateCriterion> criterian = new ArrayList<ValidateCriterion>();
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("createdOnAfterDate"),
				buildFieldValue("createdOnBeforeDate"), buildFieldValue("isSettled", "true")), 90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("createdOnAfterDate"), buildFieldValue("createdOnBeforeDate")), 90));
		criterian.add(getValidateCriterian(
				Lists.newArrayList(buildFieldValue("lockDateGreaterThan"), buildFieldValue("lockDateLessThan")), 90));
		criterian.add(getValidateCriterian(Lists.newArrayList(buildFieldValue("userIdIn"))));
		return criterian;
	}


}
