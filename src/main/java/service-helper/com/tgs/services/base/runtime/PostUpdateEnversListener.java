package com.tgs.services.base.runtime;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.persister.entity.EntityPersister;

import com.tgs.services.base.utils.TgsObjectUtils;

public interface PostUpdateEnversListener {

	boolean isValidEventForAudit(PostUpdateEvent event) ;

	default Set<String> getDifferentFields(PostUpdateEvent event){
		final EntityPersister entityPersister = event.getPersister();
		final String[] propertiesNames = entityPersister.getPropertyNames();
		final Map<String, Object> oldMap = new HashMap<>();
		final Map<String, Object> currentMap = new HashMap<>();

		for (int i = 0; i < propertiesNames.length; i++) {
			String propertyName = propertiesNames[i];
			oldMap.put(propertyName, event.getOldState()[i]);
			currentMap.put(propertyName, event.getState()[i]);
		}
		return TgsObjectUtils.getFieldsWithDifferentValues(oldMap, currentMap);
	}

}
