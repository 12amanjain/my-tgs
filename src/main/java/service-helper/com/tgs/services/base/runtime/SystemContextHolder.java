package com.tgs.services.base.runtime;

import com.tgs.services.base.MessageLogger;
import com.tgs.services.base.enums.ChannelType;

import org.apache.commons.lang3.ObjectUtils;

import com.tgs.services.base.runtime.database.config.DatabaseEnvironment;

import java.util.List;

public class SystemContextHolder {

	public static ThreadLocal<ContextData> contextDataThreadLocale = new ThreadLocal<>();

	public static ThreadLocal<DatabaseEnvironment> dbContext = new ThreadLocal<>();

	public static ThreadLocal<MessageLogger> messageLogger = new ThreadLocal<>();

	public static ContextData getContextData() {
		if (contextDataThreadLocale.get() == null) {
			return ContextData.builder().build();
		}
		return contextDataThreadLocale.get();
	}

	public static ContextData getContextData(ContextData defaultValue) {
		return ObjectUtils.firstNonNull(contextDataThreadLocale.get(), defaultValue);
	}

	public static void setContextData(ContextData contextData) {
		contextDataThreadLocale.set(contextData);
	}

	public static MessageLogger getMessageLogger() {
		if (messageLogger.get() == null) {
			return MessageLogger.builder().build();
		}
		return messageLogger.get();
	}

	public static void addMessageToLogger(List<String> messages) {
		messageLogger.get().getMessages().addAll(messages);
	}

	public static void setMessageLogger(MessageLogger logger) {
		messageLogger.set(logger);
	}

	public static DatabaseEnvironment getDatabaseSourceType() {
		return dbContext.get();
	}

	public static void setDatabaseSourceType(DatabaseEnvironment sourceType) {
		dbContext.set(sourceType);
	}

	public static void clear() {
		setContextData(null);
		setDatabaseSourceType(null);
		setMessageLogger(null);
	}
	
	public static ChannelType getChannelType() {
		return  ObjectUtils.firstNonNull(getContextData().getHttpHeaders().getChannelType(), ChannelType.DESKTOP);
	}
}
