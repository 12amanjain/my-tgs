package com.tgs.services.base.runtime.database.CustomTypes;

import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

public class FlightBasicRuleCriteriaType extends CustomUserType {

    @Override
     public Class returnedClass() {
        return FlightBasicRuleCriteria.class;
    }

}
