package com.tgs.services.base.runtime.database.CustomTypes;

import com.tgs.services.hms.HotelBasicRuleCriteria;

public class HotelBasicRuleCriteriaType extends CustomUserType {

    @Override
     public Class returnedClass() {
        return HotelBasicRuleCriteria.class;
    }

}

