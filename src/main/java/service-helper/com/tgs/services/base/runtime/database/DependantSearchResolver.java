package com.tgs.services.base.runtime.database;

import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.annotations.FilterMapping;
import com.tgs.services.base.annotations.Inject;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.SystemError;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.CollectionUtils;
import org.reflections.Reflections;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
public final class DependantSearchResolver {

    private static Set<Class<? extends BaseModel>> dbEntities;
    private static Set<Class<? extends SearchService>> services;

    private static void initSubTypes() {
        log.info("Collecting subTypes of BaseModel & SearchService for DependantSearchResolver !");
        Reflections reflections = new Reflections("com.tgs.services");
        dbEntities = reflections.getSubTypesOf(BaseModel.class);
        services = reflections.getSubTypesOf(SearchService.class);
    }

    public static void searchParentEntities(QueryFilter queryFilter) {
        try {
            List<Field> searchFields = new ArrayList<>(Arrays.asList(queryFilter.getClass().getFields()));
            List<Object> domainResult = new ArrayList<>();
            Map<String, Collection<Field>> map = new HashMap<>();
            for (Field field: searchFields) {
                FilterMapping filterMapping = field.getAnnotation(FilterMapping.class);
                if (filterMapping != null && field.get(queryFilter) != null) {
                    map.computeIfAbsent(filterMapping.dbEntity(), k -> new HashSet<>());
                    map.get(filterMapping.dbEntity()).add(field);
                }
            }
            if (map.isEmpty()) return;
            if (CollectionUtils.isEmpty(dbEntities))  initSubTypes();
            log.info("Dependant Search Query => {} ", new Gson().toJson(queryFilter));
            for (Map.Entry<String, Collection<Field>> entry : map.entrySet()) {
                Map<String, Object> keyValues = getFieldValues(entry.getValue(), queryFilter);
                String dbEntity = entry.getKey();
                Class dbClass = dbEntity(dbEntity);
                Class domainClass = domainClass(dbClass);
                List<Object> dbResult = dynamicSearch(dbClass, domainClass, keyValues);
                for (Object db : dbResult) {
                    Object dom = dbClass.getMethod("toDomain").invoke(db);
                    domainResult.add(dom);
                }
                for (Field f : entry.getValue()) {
                    f.set(queryFilter, null);
                }
                if (CollectionUtils.isNotEmpty(domainResult)) {
                    sinkData(queryFilter, domainResult, dbEntity, domainClass);
                }
            }
        } catch (Exception ex) {
            log.error(ex.getMessage(), ex);
            throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
        }
    }

    private static List<Object> dynamicSearch(Class sourceDbEntity, Class domainClass, Map<String, Object> keyValues)
            throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchFieldException {
        List<Object> result = new ArrayList<>();
        Class filterClass = Class.forName("com.tgs.filters." + domainClass.getSimpleName() + "Filter");
        Object filter = filterClass.newInstance();
        for (Map.Entry<String, Object> entry : keyValues.entrySet()) {
            Field field = filterClass.getField(entry.getKey());
            field.setAccessible(true);
            field.set(filter, entry.getValue());
        }
        for (Class<? extends SearchService> service : services) {
            Class dbEntity = (Class) ((ParameterizedType) service.getGenericSuperclass()).getActualTypeArguments()[0];
            if (sourceDbEntity.equals(dbEntity)) {
                SearchService searchService = SpringContext.getApplicationContext().getBean(service);
                result = searchService.search((QueryFilter) filter);
            }
        }
        return result;
    }

    private static void sinkData(QueryFilter queryFilter, List<Object> domainResult, String dbEntity, Class domainClass)
            throws NoSuchFieldException, IllegalAccessException {
        List<Field> searchFields = new ArrayList<>(Arrays.asList(queryFilter.getClass().getFields()));
        Map<String, Field> map = new HashMap<>();
        for (Field queryFilterField : searchFields) {
            Inject injectFrom = queryFilterField.getAnnotation(Inject.class);
            Inject.List injectContainer = queryFilterField.getAnnotation(Inject.List.class);
            List<Inject> annList = new ArrayList<>();
            if (injectFrom != null) annList.add(injectFrom);
            if (injectContainer != null) annList.addAll(Stream.of(injectContainer.value()).collect(Collectors.toList()));
            for (Inject ann : annList) {
                if (ann != null && ann.dbEntity().equals(dbEntity)) {
                    if (checkMultipleSinkFields(map, ann.field(), queryFilter)) {
                        Field domField = domainClass.getDeclaredField(ann.field());
                        domField.setAccessible(true);
                        if (Collection.class.isAssignableFrom(queryFilterField.getType())) {
                            List sourceList = new ArrayList();
                            for (Object o : domainResult) {
                                sourceList.add(domField.get(o));
                            }
                            if (queryFilterField.get(queryFilter) != null) {    // intersection with original API request values
                                List apiRequestList = (List) queryFilterField.get(queryFilter);
                                sourceList = intersection(apiRequestList, sourceList);
                            }
                            queryFilterField.set(queryFilter, sourceList);
                        } else {
                            queryFilterField.set(queryFilter, domField.get(domainResult.get(0)));
                        }
                        map.put(ann.field(), queryFilterField);
                    }
                }
            }
        }
    }

    private static List intersection(List apiRequestList, List sourceList) {
        HashSet sourceSet = new HashSet(sourceList);
        apiRequestList.removeIf(x -> !sourceSet.contains(x));
        return apiRequestList;
    }

    // In case of multiple sinks, if prev sink field is a collection then skip current sink field, else clear prev field and process current
    private static boolean checkMultipleSinkFields(Map<String, Field> processedFields, String sourceField, QueryFilter queryFilter) throws IllegalAccessException {
        if (processedFields.containsKey(sourceField)) {
            Field prevSinkField = processedFields.get(sourceField);
            if (Collection.class.isAssignableFrom(prevSinkField.getType())) return false;
            else {
                prevSinkField.setAccessible(true);
                prevSinkField.set(queryFilter, null);
            }
        }
        return true;
    }

    private static Class dbEntity(String dbEntity) {
        for (Class<? extends BaseModel> db : dbEntities) {
            if (db.getSimpleName().equals(dbEntity)) return db;
        }
        log.error("DbEntity defined for dependant search not found: {}", dbEntity);
        throw new CustomGeneralException(SystemError.SERVICE_EXCEPTION);
    }

    private static Class domainClass(Class dbEntity) {
        return  (Class) ((ParameterizedType) dbEntity.getGenericSuperclass()).getActualTypeArguments()[1];
    }

    private static Map<String, Object> getFieldValues(Collection<Field> fields, QueryFilter queryFilter) throws IllegalAccessException {
        Map<String, Object> map  = new HashMap<>();
        for (Field f : fields) {
            FilterMapping filterMapping = f.getAnnotation(FilterMapping.class);
            map.put(filterMapping.filterField(), f.get(queryFilter));
        }
        return map;
    }
}
