package com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls;

import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import com.tgs.services.base.runtime.database.SqlPredicate.IPredicate;

public class EqualsPredicate extends IPredicate {

	@Override
	public BiFunction<Expression<?>, Expression<?>, Predicate> getFunctionForPredicateWithExpression(
			CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder::equal;
	}

	@Override
	public BiFunction<Expression<?>, Object, Predicate> getFunctionForPredicateWithObject(
			CriteriaBuilder criteriaBuilder) {
		return criteriaBuilder::equal;
	}

	@Override
	protected Class<?> targetValueType() {
		return null;
	}
}
