package com.tgs.services.base.runtime.database.SqlPredicate.PredicateImpls;

import java.util.Collection;
import java.util.function.BiFunction;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;

import com.tgs.services.base.runtime.database.SqlPredicate.IPredicate;

public class InPredicate extends IPredicate {

	@SuppressWarnings("unchecked")
	@Override
	protected BiFunction<Expression<?>, Expression<Collection<?>>, Predicate> getFunctionForPredicateWithExpression(
			CriteriaBuilder criteriaBuilder) {
		return Expression::in;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected BiFunction<Expression<?>, Collection<?>, Predicate> getFunctionForPredicateWithObject(
			CriteriaBuilder criteriaBuilder) {
		return Expression::in;
	}

	@Override
	protected Class<?> targetValueType() {
		return Collection.class;
	}
}
