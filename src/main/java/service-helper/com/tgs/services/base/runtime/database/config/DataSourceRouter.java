package com.tgs.services.base.runtime.database.config;

import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;

import com.tgs.services.base.runtime.SystemContextHolder;

public class DataSourceRouter extends AbstractRoutingDataSource {

	@Override
	protected Object determineCurrentLookupKey() {
		return SystemContextHolder.getDatabaseSourceType();
	}

}
