package com.tgs.services.base.runtime.envers;

import java.util.Set;

import org.hibernate.event.spi.PostUpdateEvent;
import org.springframework.stereotype.Component;

import com.tgs.services.base.runtime.PostUpdateEnversListener;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CreditLineEnversListener implements PostUpdateEnversListener {

	@Override
	public boolean isValidEventForAudit(PostUpdateEvent event) {
		Object[] oldState = event.getOldState();
		if (oldState != null) {
			Set<String> differentFields = getDifferentFields(event);
			log.debug("[CreditLine] Different fields are : {}", differentFields.toString());
			differentFields.remove("products");
			if (differentFields.isEmpty() || differentFields.contains("outstandingBalance")) {
				return false;
			}
		}
		return true;
	}

}
