package com.tgs.services.base.runtime.envers;

import com.tgs.services.base.runtime.PostUpdateEnversListener;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.event.spi.PostUpdateEvent;
import org.springframework.stereotype.Component;

import java.util.Set;

@Component
@Slf4j
public class NRCreditEnversListener implements PostUpdateEnversListener {

    @Override
    public boolean isValidEventForAudit(PostUpdateEvent event) {
        Object[] oldState = event.getOldState();
        if (oldState != null) {
            Set<String> differentFields = getDifferentFields(event);
            log.debug("[NRCredit] Different fields are : {}", differentFields.toString());
            differentFields.remove("products");
            if (differentFields.isEmpty() || (differentFields.contains("utilized"))) {
                return false;
            }
        }
        return true;
    }
}
