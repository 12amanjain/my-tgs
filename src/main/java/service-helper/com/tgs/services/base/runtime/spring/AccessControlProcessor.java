package com.tgs.services.base.runtime.spring;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;

@Service
public class AccessControlProcessor {

	@Autowired
	private UserServiceCommunicator userService;

	public void process(AreaRole arearole, UserRole[] allowedRoles, UserRole[] excludedRoles,
			UserRole[] emulatedAllowedRoles, boolean isMidOfficeAllowed) {
		List<UserRole> includedRoles = getRole(allowedRoles);
		List<UserRole> emulatedAllowedRole = getRole(emulatedAllowedRoles);
		List<UserRole> excludedRole = getRole(excludedRoles);
		boolean isAccessGranted =
				validateAccess(arearole, isMidOfficeAllowed, includedRoles, emulatedAllowedRole, excludedRole);
		if (!isAccessGranted)
			throw new CustomGeneralException(SystemError.ACCESS_DENIED);
	}

	private boolean validateAccess(AreaRole arearole, boolean isMidOfficeAllowed, List<UserRole> includedRoles,
			List<UserRole> emulatedAllowedRole, List<UserRole> excludedRole) {
		User user = SystemContextHolder.getContextData().getUser();
		List<AreaRoleMapping> mappingList = userService.getAreaRoleMapByAreaRole(Arrays.asList(arearole));
		Map<UserRole, Set<AreaRole>> userRoleMap = AreaRoleMapping.getMapByUserRole(mappingList);
		Map<UserRole, Set<AreaRole>> emulatedUserRoleMap = AreaRoleMapping.getMapByEmulatedRoleSet(mappingList);
		boolean flag = false;
		// Check midOfficeAllowed, included and excluded roles first
		if ((isMidOfficeAllowed && UserUtils.isMidOfficeRole(user.getRole()))
				|| (CollectionUtils.isNotEmpty(includedRoles) && includedRoles.contains(user.getRole()))
				|| (user.getEmulateUser() != null && CollectionUtils.isNotEmpty(emulatedAllowedRole)
						&& emulatedAllowedRole.contains(user.getEmulateUser().getRole()))) {
			return true;
		}
		flag = flag || CollectionUtils.isNotEmpty(excludedRole) && excludedRole.contains(user.getRole());

		// If access not granted, check the overridden configuration from arearolemap
		if (MapUtils.isNotEmpty(userRoleMap)) {
			flag = flag
					|| (userRoleMap.get(user.getRole()) != null && userRoleMap.get(user.getRole()).contains(arearole));
		}

		if (MapUtils.isNotEmpty(emulatedUserRoleMap) && user.getEmulateUser() != null) {
			flag = flag || (emulatedUserRoleMap.get(user.getEmulateUser().getRole()) != null
					&& emulatedUserRoleMap.get(user.getEmulateUser().getRole()).contains(arearole));
		}
		return flag;
	}

	private List<UserRole> getRole(UserRole[] allowedRoles) {
		return Arrays.stream(allowedRoles).collect(Collectors.toList());
	}
}
