package com.tgs.services.base.runtime.spring;

import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.helper.Airline;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.fms.datamodel.AirlineInfo;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AirlineResponseProcessor implements ResponseProcessor {

    @Autowired
    private FMSCommunicator fmsCommunicator;

    private static final String META_KEY = "Airlines";

    @Override
    public void process(BaseResponse obj, String[] metaInfo) {
        Set<Set<String>> codeSet = TgsObjectUtils.findAnnotationValues(obj, Airline.class);
        Set<String> codes = codeSet.stream().flatMap(Collection::stream).collect(Collectors.toSet());
        Map<String, AirlineInfo> map = new HashMap<>();
        codes.forEach(code -> map.put(code, fmsCommunicator.getAirlineInfo(code)));
        obj.getMetaInfo().put(META_KEY, map);

    }
}

