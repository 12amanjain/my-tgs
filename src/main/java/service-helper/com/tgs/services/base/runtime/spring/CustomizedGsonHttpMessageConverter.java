package com.tgs.services.base.runtime.spring;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.tgs.services.base.ruleengine.GeneralBasicFact;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractGenericHttpMessageConverter;
import org.springframework.http.converter.GenericHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.google.gson.ExclusionStrategy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.LogData;
import com.tgs.services.base.SystemCheckPoint;
import com.tgs.services.base.analytics.APIRequestDataMapper;
import com.tgs.services.base.analytics.BaseAnalyticsHelper;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.gson.APIUserExcludeStrategy;
import com.tgs.services.base.gson.AirAPIExcludeStrategyV1;
import com.tgs.services.base.gson.AnnotationExclusionStrategy;
import com.tgs.services.base.gson.FieldExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.HotelAPIExcludeStrategyV1;
import com.tgs.services.base.gson.LogExcludeStrategy;
import com.tgs.services.base.helper.RequestContextHolder;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.ruleengine.IFact;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.AccessControlInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.ruleengine.AccessControlOutput;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import springfox.documentation.spring.web.json.Json;
import springfox.documentation.spring.web.json.SpringfoxJsonToGsonAdapter;

@Slf4j
@Service
public class CustomizedGsonHttpMessageConverter extends AbstractGenericHttpMessageConverter<Object>
		implements GenericHttpMessageConverter<Object> {

	public static final Charset DEFAULT_CHARSET = Charset.forName("UTF-8");

	private String jsonPrefix;

	@Autowired
	private BaseAnalyticsHelper analyticsHelper;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	private static Map<Integer, ExclusionStrategy> airApiExclusionStrategyMap = null;
	private static Map<Integer, ExclusionStrategy> hotelApiExclusionStrategyMap = null;

	static {
		airApiExclusionStrategyMap = new HashMap<Integer, ExclusionStrategy>();
		airApiExclusionStrategyMap.put(1, new AirAPIExcludeStrategyV1());
		
		hotelApiExclusionStrategyMap = new HashMap<Integer, ExclusionStrategy>();
		hotelApiExclusionStrategyMap.put(1, new HotelAPIExcludeStrategyV1());
	}

	public ExclusionStrategy getAirAPIExclusionStrategy(Integer version) {
		if (version == null)
			return null;
		return airApiExclusionStrategyMap.get(version);
	}
	
	public ExclusionStrategy getHotelAPIExclusionStrategy(Integer version) {
		if (version == null)
			return null;
		return hotelApiExclusionStrategyMap.get(version);
	}

	// private ApiContext apiContext = SystemThreadLocals.getApiContext();

	/**
	 * Construct a new {@code GsonHttpMessageConverter}.
	 */
	public CustomizedGsonHttpMessageConverter() {
		super(MediaType.APPLICATION_JSON_UTF8, new MediaType("application", "*+json", DEFAULT_CHARSET));
	}

	/**
	 * Set the {@code Gson} instance to use. If not set, a default {@link Gson#Gson() Gson} instance is used.
	 * <p>
	 * Setting a custom-configured {@code Gson} is one way to take further control of the JSON serialization process.
	 */
	public void setGson(Gson gson) {
		Assert.notNull(gson, "'gson' is required");
	}

	/**
	 * Specify a custom prefix to use for JSON output. Default is none.
	 * 
	 * @see #setPrefixJson
	 */
	public void setJsonPrefix(String jsonPrefix) {
		this.jsonPrefix = jsonPrefix;
	}

	/**
	 * Indicate whether the JSON output by this view should be prefixed with ")]}', ". Default is {@code false}.
	 * <p>
	 * Prefixing the JSON string in this manner is used to help prevent JSON Hijacking. The prefix renders the string
	 * syntactically invalid as a script so that it cannot be hijacked. This prefix should be stripped before parsing
	 * the string as JSON.
	 * 
	 * @see #setJsonPrefix
	 */
	public void setPrefixJson(boolean prefixJson) {
		this.jsonPrefix = (prefixJson ? ")]}', " : null);
	}

	@Override
	public boolean canRead(Class<?> clazz, MediaType mediaType) {
		return canRead(mediaType);
	}

	@Override
	public boolean canWrite(Class<?> clazz, MediaType mediaType) {
		return canWrite(mediaType);
	}

	@Override
	protected boolean supports(Class<?> clazz) {
		// should not be called, since we override canRead/Write instead
		throw new UnsupportedOperationException();
	}

	@Override
	protected Object readInternal(Class<?> clazz, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		TypeToken<?> token = getTypeToken(clazz);
		return readTypeToken(token, inputMessage);
	}

	@Override
	public Object read(Type type, Class<?> contextClass, HttpInputMessage inputMessage)
			throws IOException, HttpMessageNotReadableException {
		try {
			TypeToken<?> token = getTypeToken(type);
			return readTypeToken(token, inputMessage);
		} catch (JsonIOException ex) {
			throw new HttpMessageNotReadableException("Could not read JSON: " + ex.getMessage(), ex);
		} finally {
			LogUtils.clearLogList(SystemContextHolder.getContextData().getReqIds(),
					SystemContextHolder.getContextData().getLogs());
		}
	}

	/**
	 * Return the Gson {@link TypeToken} for the specified transactionType.
	 * <p>
	 * The default implementation returns {@code TypeToken.get(transactionType)}, but this can be overridden in
	 * subclasses to allow for custom generic collection handling. For instance:
	 * 
	 * <pre class="code">
	 * protected TypeToken&lt;?&gt; getTypeToken(Type transactionType) {
	 * 	if (transactionType instanceof Class &amp;&amp; List.class.isAssignableFrom((Class&lt;?&gt;) transactionType)) {
	 * 		return new TypeToken&lt;ArrayList&lt;MyBean&gt;&gt;() {};
	 * 	} else {
	 * 		return super.getTypeToken(transactionType);
	 * 	}
	 * }
	 * </pre>
	 * 
	 * @param type the transactionType for which to return the TypeToken
	 * @return the transactionType token
	 */

	protected TypeToken<?> getTypeToken(Type type) {
		return TypeToken.get(type);
	}

	private <T> Gson gson(String str, Class<T> classType, Optional<Class> typeArgClass) {
		List<ExclusionStrategy> exclusionStrategyList = new ArrayList<>();
		exclusionStrategyList.add(new AnnotationExclusionStrategy());
		if (SystemContextHolder.getContextData() != null
				&& SystemContextHolder.getContextData().getRequestContextHolder() != null) {
			String endPointUrl = SystemContextHolder.getContextData().getRequestContextHolder().getEndPoint();
			if (StringUtils.isNotBlank(endPointUrl)) {
				setExclusionInclusionStrategy(endPointUrl);
				if (!CollectionUtils.isEmpty(SystemContextHolder.getContextData().getExclusionStrategys())) {
					exclusionStrategyList.addAll(SystemContextHolder.getContextData().getExclusionStrategys());
				}
			}
		}
		if (StringUtils.isBlank(str)) {
			final GsonBuilder builder =
					GsonUtils.builder().strategies(exclusionStrategyList).build().buildGsonBuilder();
			builder.registerTypeAdapter(Json.class, new SpringfoxJsonToGsonAdapter());
			return builder.create();
		}

		return GsonUtils.builder().strategies(exclusionStrategyList).build().buildGson(str, classType, typeArgClass);
	}

	private Object readTypeToken(TypeToken<?> token, HttpInputMessage inputMessage) throws IOException {
		Reader json = new InputStreamReader(inputMessage.getBody(), getCharset(inputMessage.getHeaders()));
		RequestContextHolder rContextHolder = SystemContextHolder.getContextData().getRequestContextHolder();
		Object reqObj = null;
		String jsonStr = "";
		try {
			// Gson with registered polymorphism mapping and adapter
			jsonStr = IOUtils.toString(json);
			Optional<Class> typeArgClass = Optional.empty();
			if (token.getType() instanceof ParameterizedType) {
				typeArgClass = Optional.of((Class) ((ParameterizedType) token.getType()).getActualTypeArguments()[0]);
			}
			Gson gson = gson(jsonStr, token.getRawType(), typeArgClass);
			reqObj = gson.fromJson(jsonStr, token.getType());

			addRequestLogging(reqObj);
			return reqObj;
		} catch (JsonParseException | JsonIOException | JsonSyntaxException | NumberFormatException ex) {
			log.error("Bad Json , Unable to parse it , Request body is {}, End Point is {}", jsonStr,
					rContextHolder.getEndPoint(), ex);
			throw new CustomGeneralException(SystemError.BAD_REQUEST, ex.getMessage());
		} finally {
			SystemContextHolder.getContextData().setRequestObj(reqObj);
		}
	}

	private Charset getCharset(HttpHeaders headers) {
		if (headers == null || headers.getContentType() == null || headers.getContentType().getCharset() == null) {
			return DEFAULT_CHARSET;
		}
		return headers.getContentType().getCharset();
	}

	@Override
	protected void writeInternal(Object o, Type type, HttpOutputMessage outputMessage)
			throws IOException, HttpMessageNotWritableException {

		Charset charset = getCharset(outputMessage.getHeaders());
		OutputStreamWriter writer = new OutputStreamWriter(outputMessage.getBody(), charset);
		try {
			if (this.jsonPrefix != null) {
				writer.append(this.jsonPrefix);
			}
			Gson gson = gson(null, null, null);
			if (type != null) {
				gson.toJson(o, type, writer);
			} else {
				gson.toJson(o, writer);
			}
			writer.close();
			addLogging(gson.toJson(o, type));
		} catch (JsonIOException ex) {
			throw new HttpMessageNotWritableException("Could not write JSON: " + ex.getMessage(), ex);
		} finally {
			LogUtils.clearLogList(SystemContextHolder.getContextData().getReqIds(),
					SystemContextHolder.getContextData().getLogs());
		}
	}

	public void addRequestLogging(Object reqObj) {
		String jsonStr = GsonUtils.getGson(Arrays.asList(new LogExcludeStrategy()), null).toJson(reqObj);
		ContextData contextData = SystemContextHolder.getContextData();
		contextData.addCheckPoint(CheckPointData.builder().type(SystemCheckPoint.REQUEST_STARTED.name())
				.time(System.currentTimeMillis()).build());
		RequestContextHolder rContextHolder = contextData.getRequestContextHolder();
		String jsonWithoutSpace = jsonStr.replaceAll("[\\s]", "");
		contextData.getLogs()
				.add(LogData.builder().type("API Request-" + rContextHolder.getEndPoint()).supplier("Tgs")
						.generationTime(LocalDateTime.now()).logType("RestAPILogs")
						.key(rContextHolder.getEndPoint() + "_" + LocalDate.now().toString()).logData(jsonWithoutSpace)
						.loggedInUserId(UserUtils.getUserId(contextData.getUser())).build());
		contextData.setRequestJson(jsonWithoutSpace);
	}

	public void addLogging(String response) {
		ContextData contextData = SystemContextHolder.getContextData();
		contextData.getCheckPointInfo().put(SystemCheckPoint.REQUEST_FINISHED.name(),
				new ArrayList<>(Arrays.asList(CheckPointData.builder().type(SystemCheckPoint.REQUEST_FINISHED.name())
						.time(System.currentTimeMillis()).build())));

		RequestContextHolder rContextHolder = contextData.getRequestContextHolder();
		if (rContextHolder != null) {
			contextData.getLogs().add(LogData.builder().type("API Response-" + rContextHolder.getEndPoint())
					.key(rContextHolder.getEndPoint() + "_" + LocalDate.now().toString()).supplier("Tgs")
					.generationTime(LocalDateTime.now()).loggedInUserId(UserUtils.getUserId(contextData.getUser()))
					.logType("RestAPILogs").logData(response).build());
			contextData.getStopWatch().stop();
			log.debug("Total time took to process Rest API {} is {} millisecond", rContextHolder.getEndPoint(),
					contextData.getStopWatch().getTime());
			BaseResponse baseResponse = GsonUtils.getGson().fromJson(response, BaseResponse.class);
			APIRequestDataMapper requestData =
					APIRequestDataMapper.builder().user(contextData.getUser()).contextData(contextData)
							.endPoint(rContextHolder.getEndPoint()).requestObj(contextData.getRequestObj())
							.timeInMillsec(contextData.getStopWatch().getTime()).build();
			if (BooleanUtils.isFalse(baseResponse.getStatus().getSuccess())
					&& !CollectionUtils.isEmpty(baseResponse.getErrors())) {
				requestData.setErrMsg(baseResponse.getErrorMessage());
				requestData.setErrorCode(Integer.valueOf(baseResponse.getErrors().get(0).getErrCode()));
			}
			analyticsHelper.store(requestData);
		}
	}

	public void setExclusionInclusionStrategy(String endPointUrl) {
		User user = SystemContextHolder.getContextData().getUser();
		if (user != null) {
			GeneralBasicFact fact = GeneralBasicFact.builder().userId(user.getUserId()).role(user.getRole())
					.channelType(SystemContextHolder.getChannelType()).build();
			AccessControlInfo accessControlInfo =
					getAccessControlInfo(ConfiguratorRuleType.ACCESSCONTROL, fact, endPointUrl);
			if (accessControlInfo != null && !CollectionUtils.isEmpty(accessControlInfo.getExclusionKeys())) {
				SystemContextHolder.getContextData().setExclusionStrategys(new ArrayList<ExclusionStrategy>(
						Arrays.asList(new FieldExclusionStrategy(null, accessControlInfo.getExclusionKeys()))));
			}
			if (UserUtils.isApiUserRequest(user)) {
				SystemContextHolder.getContextData().getExclusionStrategys().add(new APIUserExcludeStrategy());
				if (user.getUserConf().getApiConfiguration().getAirVersion() != null)
					SystemContextHolder.getContextData().getExclusionStrategys()
							.add(getAirAPIExclusionStrategy(user.getUserConf().getApiConfiguration().getAirVersion()));
				if (user.getUserConf().getApiConfiguration().getHotelVersion() != null)
					SystemContextHolder.getContextData().getExclusionStrategys()
							.add(getHotelAPIExclusionStrategy(user.getUserConf().getApiConfiguration().getHotelVersion()));

			}
		}
	}

	public AccessControlInfo getAccessControlInfo(ConfiguratorRuleType ruleType, IFact fact, String api) {

		AccessControlOutput output = (AccessControlOutput) gmsComm.getConfigRule(ruleType, fact);
		if (output != null) {
			List<AccessControlInfo> accessControlInfos = output.getAccessControlInformation();
			for (AccessControlInfo accessControlInfo : accessControlInfos) {
				if (accessControlInfo.getEndPointUrl().equalsIgnoreCase(api)) {
					return accessControlInfo;
				}
			}
		}
		return null;
	}
}
