
package com.tgs.services.base.runtime.spring;

import java.util.List;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.gson.JsonSyntaxException;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.CustomAuthenticationException;

import com.tgs.utils.exception.DuplicateException;
import com.tgs.utils.exception.ResourceNotFoundException;
import com.tgs.utils.exception.UnAuthorizedException;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		log.error("Unable to serve request for {} ", SystemContextHolder.getContextData().getRequestJson(), ex);
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_400));
		res.addError(new ErrorDetail(SystemError.SERVICE_EXCEPTION));
		return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler(ResourceNotFoundException.class)
	public final ResponseEntity<Object> handleResourceNotFoundExceptions(Exception ex, WebRequest request) {
		log.error("Resource not found for {}", SystemContextHolder.getContextData().getRequestJson(), ex);
		BaseResponse res = new BaseResponse();
		ResourceNotFoundException rX = (ResourceNotFoundException) ex;
		res.setStatus(new Status(HttpStatusCode.HTTP_404));
		res.addError(new ErrorDetail(rX.getError(), rX.getMessage()));
		return new ResponseEntity(res, HttpStatus.NOT_FOUND);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler(CustomGeneralException.class)
	public final ResponseEntity<Object> handleGeneralExceptions(Exception ex, WebRequest request) {
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_400));
		CustomGeneralException cX = (CustomGeneralException) ex;
		log.info("Custom exception message is {}, request json is {}", cX.getError().getMessage(),
				SystemContextHolder.getContextData().getRequestJson());
		if (SystemError.DUPLICATE_ORDER.equals(cX.getError())) {
			res.addError(new ErrorDetail(String.valueOf(cX.getError().getErrorCode()),
					cX.getMessage(), SystemContextHolder.getContextData().getAlreadyExistingBookingId()));
		} else if (StringUtils.isNotEmpty(cX.getMessage())) {
			res.addError(new ErrorDetail(String.valueOf(cX.getError().getErrorCode()), cX.getError().getMessage(),
					cX.getMessage()));
		} else {
			res.addError(new ErrorDetail(cX.getError(), cX.getMessage()));
		}
		return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler(UnAuthorizedException.class)
	public final ResponseEntity<Object> handleUnAuthorizedExceptionExceptions(Exception ex, WebRequest request) {
		log.error("UnAuthorized Exception thrown by system for {} ",
				SystemContextHolder.getContextData().getRequestJson(), ex);
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_200));
		return new ResponseEntity(res, HttpStatus.OK);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler({ DuplicateException.class, JsonSyntaxException.class })
	public final ResponseEntity<Object> handleDuplicateExceptions(Exception ex, WebRequest request) {
		log.error("Unable to serve request due to duplicate params {}",
				SystemContextHolder.getContextData().getRequestJson(), ex);
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_400));
		res.addError(new ErrorDetail(HttpStatusCode.HTTP_400.getHttpStatus(), ex.getMessage()));
		return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler(AccessDeniedException.class)
	public final ResponseEntity<Object> handleAccessDeniedException(Exception ex, WebRequest request) {
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		res.addError(new ErrorDetail(SystemError.FORBIDDEN.getErrorCode(), ex.getMessage()));
		return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@ExceptionHandler(CustomAuthenticationException.class)
	public final ResponseEntity<Object> handleCustomAuthenticationExceptions(Exception ex, WebRequest request) {
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		res.addError(new ErrorDetail(SystemError.FORBIDDEN.getErrorCode(), ex.getMessage()));
		return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<org.springframework.validation.FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
		BaseResponse res = new BaseResponse();
		processFieldErrors(fieldErrors, res);
		res.setStatus(new Status(HttpStatusCode.HTTP_400));
		return new ResponseEntity(res, HttpStatus.BAD_REQUEST);
	}

	private void processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors, BaseResponse res) {
		for (org.springframework.validation.FieldError fieldError : fieldErrors) {
			res.addError(new ErrorDetail(fieldError.getCode(),
					StringUtils.join(fieldError.getField(), " : ", fieldError.getDefaultMessage())));
		}
	}
}
