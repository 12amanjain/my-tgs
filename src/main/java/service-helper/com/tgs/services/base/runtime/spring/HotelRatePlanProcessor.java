package com.tgs.services.base.runtime.spring;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.helper.RoomCategoryId;
import com.tgs.services.base.helper.RoomTypeId;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.hms.datamodel.inventory.HotelRoomCategory;
import com.tgs.services.hms.datamodel.inventory.HotelRoomTypeInfo;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class HotelRatePlanProcessor implements ResponseProcessor {

	@Autowired
	GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	HMSCommunicator hmsCommunicator;

	private static final String META_ROOM_CATEGORY_KEY = "ROOMCATEGORY_INFO";
	private static final String META_ROOM_TYPE_KEY = "ROOMTYPE_INFO";
	private static final String META_KEY = "hotel";


	@Override
	public void process(BaseResponse obj, String[] metaInfo) {
		Map<String, Map<String, String>> responseMap = new HashMap<>();
		Map<RoomCategoryId, Set<String>> roomCategoryIdMap =
				TgsObjectUtils.findAnnotationWithValues(obj, RoomCategoryId.class);

		if (MapUtils.isNotEmpty(roomCategoryIdMap)) {

			Set<Long> roomCategoryIds = roomCategoryIdMap.values().stream().flatMap(Collection::stream)
					.mapToLong(Long::valueOf).boxed().collect(Collectors.toSet());

			log.debug("Room Category Ids found with size {} ", roomCategoryIds.size());
			if (CollectionUtils.isNotEmpty(roomCategoryIds)) {

				List<HotelRoomCategory> roomCategories = hmsCommunicator.fetchRoomCategoriesById(roomCategoryIds);
				if (CollectionUtils.isNotEmpty(roomCategories)) {
					Map<String, String> newRoomCategoryMap = new HashMap<>();
					roomCategories.forEach(roomCategory -> {
						newRoomCategoryMap.put(String.valueOf(roomCategory.getId()), roomCategory.getRoomCategory());
					});
					responseMap.put(META_ROOM_CATEGORY_KEY, newRoomCategoryMap);
				}
			}
		}
		
		Map<RoomTypeId, Set<String>> roomTypeIdMap =
				TgsObjectUtils.findAnnotationWithValues(obj, RoomTypeId.class);
		
		if (MapUtils.isNotEmpty(roomTypeIdMap)) {

			Set<Long> roomTypeIds = roomTypeIdMap.values().stream().flatMap(Collection::stream)
					.mapToLong(Long::valueOf).boxed().collect(Collectors.toSet());

			log.debug("Room Category Ids found with size {} ", roomTypeIds.size());
			if (CollectionUtils.isNotEmpty(roomTypeIds)) {

				List<HotelRoomTypeInfo> roomTypes = hmsCommunicator.fetchRoomTypesById(roomTypeIds);
				if (CollectionUtils.isNotEmpty(roomTypes)) {
					Map<String, String> newRoomTypeMap = new HashMap<>();
					roomTypes.forEach(roomType -> {
						newRoomTypeMap.put(String.valueOf(roomType.getId()), roomType.getRoomTypeName());
					});
					responseMap.put(META_ROOM_TYPE_KEY, newRoomTypeMap);
				}
			}
		}

		obj.getMetaInfo().put(META_KEY, responseMap);
	}
}
