package com.tgs.services.base.runtime.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

public class PrototypeScopedBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {
		for (String beanName : factory.getBeanDefinitionNames()) {
			BeanDefinition beanDef = factory.getBeanDefinition(beanName);
			String explicitScope = beanDef.getScope();
			/**
			 * In case you are adding any additional pattern , ensure that it shouldn't consist of any file from base
			 * folder. Otherwise it will break the whole system. For example any class having package
			 * com.tgs.services.base.* shouldn't by added in pattern any time.
			 */
			if ("singleton".equals(explicitScope) && beanDef.getBeanClassName() != null
					&& beanDef.getBeanClassName().matches(
							"com.tgs.services.*.(restcontroller|manager|servicehandler|sources|impl|ffts\\.client).*")) {
				beanDef.setScope("prototype");
			}
		}
	}
}
