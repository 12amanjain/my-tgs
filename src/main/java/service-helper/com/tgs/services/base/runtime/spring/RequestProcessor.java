package com.tgs.services.base.runtime.spring;

public interface RequestProcessor {

	public void process(Object requestObject);
}
