package com.tgs.services.base.runtime.spring;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.helper.RequestContextHolder;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;

@Order(1000)
@ControllerAdvice
public class ResponseProcessingAdvice implements ResponseBodyAdvice<BaseResponse> {

	@Autowired
	private UserIdResponseProcessor userIdProcessor;

	@Autowired
	private AirlineResponseProcessor airlineResponseProcessor;

	@Autowired
	private CommercialResponseProcessor commercialResponseProcessor;

	@Autowired
	private HotelRatePlanProcessor hotelRatePlanProcessor;

	@Autowired
	private GeneralServiceCommunicator gmsComm;

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return BaseResponse.class.isAssignableFrom(returnType.getParameterType());
	}

	@Override
	public BaseResponse beforeBodyWrite(BaseResponse body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		// If HttpResponse is null somehow, like end point does not exist and its not set by SystemContextFilter, then
		// we should receive the appropriate error, instead of NPE here
		if (SystemContextHolder.getContextData().getHttpResponse() != null)
			SystemContextHolder.getContextData().getHttpResponse().addHeader("ui_conf", gmsComm.getUIConf());
		RequestContextHolder rContextHolder = SystemContextHolder.getContextData().getRequestContextHolder();
		if (rContextHolder != null) {
			if (rContextHolder.getDefinedAnnotations() != null) {
				CustomRequestMapping ann =
						(CustomRequestMapping) rContextHolder.getAnnotation((CustomRequestMapping.class));
				if (ann != null) {
					for (ResponseProcessor processor : getResponseProcessorList())
						for (Class<?> pro : ann.responseProcessors()) {
							if (pro.getCanonicalName().equals(processor.getClass().getCanonicalName()))
								processor.process(body, ann.metaInfo());
						}
				}
			}

		}
		return body;
	}

	public List<ResponseProcessor> getResponseProcessorList() {
		return Arrays.asList(userIdProcessor, airlineResponseProcessor, commercialResponseProcessor,
				hotelRatePlanProcessor);
	}

}
