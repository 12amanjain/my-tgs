package com.tgs.services.base.runtime.spring;


import com.tgs.services.base.restmodel.BaseResponse;

public interface ResponseProcessor {

	public void process(BaseResponse obj, String[] metaInfo);
}
