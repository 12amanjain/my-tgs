package com.tgs.services.base.runtime.spring;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.Order;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import com.google.gson.annotations.SerializedName;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.validator.response.ResponseValidator;
import com.tgs.services.base.validator.response.RoleWiseUserMetaInfoCache;
import lombok.extern.slf4j.Slf4j;

@Order(1001)
@ControllerAdvice
@Slf4j
public class ResponseValidationAdvice implements ResponseBodyAdvice<BaseResponse> {

	@Value("${responseValidationConfig:{}}")
	private String configurationJson;

	private ResponseValidationConfig configuration;

	@Autowired
	private Map<String, ResponseValidator> responseValidators;

	@PostConstruct
	private void init() {
		try {
			configuration = GsonUtils.getGson().fromJson(configurationJson, ResponseValidationConfig.class);
		} catch (Exception e) {
			log.error("Failed to initialise Response Validation Configuration with Json '{}'", configurationJson);
		}
	}

	@Override
	public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
		return BaseResponse.class.isAssignableFrom(returnType.getParameterType());
	}

	@Override
	public BaseResponse beforeBodyWrite(BaseResponse body, MethodParameter returnType, MediaType selectedContentType,
			Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
			ServerHttpResponse response) {
		if (responseValidators == null) {
			return body;
		}
		RoleWiseUserMetaInfoCache.clearCache();
		Set<ResponseValidator> applicableValidators = new HashSet<>(responseValidators.values());
		SuppressionConfig suppressionConfig = getSuppressionConfig(request);
		if (suppressionConfig != null) {
			if (CollectionUtils.isEmpty(suppressionConfig.suppressedValidations)) {
				// Suppress all validations
				return body;
			}
			suppressionConfig.suppressedValidations
					.forEach(beanName -> applicableValidators.remove(responseValidators.get(beanName)));
		}
		applicableValidators.forEach(validator -> validator.validateResponse(body));
		return body;
	}

	private SuppressionConfig getSuppressionConfig(ServerHttpRequest request) {
		if (configuration == null || configuration.suppressionConfigurations == null) {
			return null;
		}
		for (SuppressionConfig suppressionConfig : configuration.suppressionConfigurations) {
			if (isSuppressionConfigApplicable(suppressionConfig, request)) {
				return suppressionConfig;
			}
		}
		return null;
	}

	private boolean isSuppressionConfigApplicable(SuppressionConfig suppressionConfig, ServerHttpRequest request) {
		boolean isApplicable = false;
		String url = "";
		try {
			url = request.getURI().toString();
			if (suppressionConfig.urls != null) {
				for (String urlPattern : suppressionConfig.urls) {
					isApplicable |= url.matches(urlPattern);
				}
			} else {
				isApplicable = true;
			}
			String ip = request.getRemoteAddress().getHostString();
			if (suppressionConfig.allowedIps != null) {
				isApplicable &= suppressionConfig.allowedIps.contains(ip);
			}
			if (isApplicable) {
				log.debug("Suppressing response validations {} for endpoint {}, remote IP {}",
						suppressionConfig.suppressedValidations, url, ip);
			}
		} catch (Exception e) {
			log.error("Failed to evaluate suppressionConfig's applicability for {} due to {}", url, e.getMessage(), e);
		}
		return isApplicable;
	}

	private static class ResponseValidationConfig {
		@SerializedName("sc")
		private List<SuppressionConfig> suppressionConfigurations;
	}

	private static class SuppressionConfig {
		private List<String> urls;
		private List<String> allowedIps;
		private List<String> suppressedValidations;
	}

}
