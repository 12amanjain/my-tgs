package com.tgs.services.base.runtime.spring;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserIdRequestProcessor implements RequestProcessor {

	@Autowired
	private UserServiceCommunicator umsService;

	@Override
	public void process(Object reqObj) {
		Map<UserId, Set<String>> map = TgsObjectUtils.findAnnotationWithValues(reqObj, UserId.class);
		if (MapUtils.isNotEmpty(map)) {
			List<String> userIds = map.values().stream().flatMap(Collection::stream).collect(Collectors.toList());
			User user = SystemContextHolder.getContextData().getUser();
			if (user != null) {
				List<String> allowedUserIds = umsService.getAllowedUserIds(user.getUserId());
				if (CollectionUtils.isNotEmpty(allowedUserIds)) {
					userIds.removeAll(allowedUserIds);
					if (CollectionUtils.isNotEmpty(userIds)) {
						log.info("{} is able to update data of following userids : {} in the request {} ",
								user.getUserId(), userIds, GsonUtils.getGson().toJson(reqObj));
						throw new CustomGeneralException(SystemError.UPDATE_DENIED);
					}
				}
			}
		}

	}
}
