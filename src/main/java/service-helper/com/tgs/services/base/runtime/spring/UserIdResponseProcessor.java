package com.tgs.services.base.runtime.spring;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.PolicyServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.UserId;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.TgsObjectUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.ps.datamodel.Policy;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.datamodel.UserConfiguration;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserIdResponseProcessor implements ResponseProcessor {

	@Autowired
	private UserServiceCommunicator userService;

	@Autowired
	private PaymentServiceCommunicator paymentService;

	@Autowired
	private PolicyServiceCommunicator policyService;

	public static final String META_KEY = "Users";

	@Override
	public void process(BaseResponse obj, String[] metaInfo) {
		Map<UserId, Set<String>> map = TgsObjectUtils.findAnnotationWithValues(obj, UserId.class);
		if (MapUtils.isNotEmpty(map)) {
			Set<String> userIds = map.values().stream().flatMap(Collection::stream).collect(Collectors.toSet());
			log.debug("[UserListing] Userids found with size {} ", userIds.size());
			if (CollectionUtils.isNotEmpty(userIds)) {
				List<String> userList =
						Lists.newArrayList(userIds).stream().filter(Objects::nonNull).collect(Collectors.toList());
				Map<String, User> userMap =
						userService.getUsersFromCache(UserCacheFilter.builder().userIds(userList).build());
				UserId userIdConf = Lists.newArrayList(map.keySet()).get(0);

				List<String> parentUserIds =
						userMap.values().stream().filter(user -> !user.getUserId().equals(user.getParentUserId()))
								.map(User::getParentUserId).distinct().collect(Collectors.toList());
				Map<String, User> parentUsersMap =
						userService.getUsersFromCache(UserCacheFilter.builder().userIds(parentUserIds).build());
				if (MapUtils.isNotEmpty(parentUsersMap)) {
					userList.addAll(parentUsersMap.keySet());
					userMap.putAll(parentUsersMap);
				}

				Map<String, Set<User>> relations =
						userIdConf.userRelation() ? userService.getUserRelations(userList) : new HashMap<>();
				log.debug("[UserListing] Fetched relations map");
				if (MapUtils.isNotEmpty(relations)) {
					Set<String> salesUserIds = relations.values().stream().flatMap(Collection::stream)
							.map(x -> x.getUserId()).collect(Collectors.toSet());
					Map<String, User> salesUserMap = userService.getUsersFromCache(
							UserCacheFilter.builder().userIds(new ArrayList<>(salesUserIds)).build());
					for (Map.Entry<String, Set<User>> entry : relations.entrySet()) {
						Set<User> users = entry.getValue();
						if (CollectionUtils.isNotEmpty(users))
							users.forEach(user -> user.setEmail(salesUserMap.get(user.getUserId()).getEmail()));
						entry.setValue(users);
					}
				}
				Map<String, List<Policy>> policyMap = metaInfo != null
						? policyService.fetchUserPolicies(Lists.newArrayList(userIds), Arrays.asList(metaInfo))
						: new HashMap<>();
				log.debug("[UserListing] Fetched Policy map");
				Map<String, BigDecimal> userBalance =
						paymentService.getUserBalanceFromCache(PaymentMetaInfo.builder().userIds(userList).build());
				log.debug("[UserListing] Fetched user balance");
				Map<String, BigDecimal> walletBalance = paymentService.getUserBalanceFromCache(PaymentMetaInfo.builder()
						.userIds(userList).fetchCreditLineBalance(false).fetchNRCredit(false).build());

				Map<String, User> responseMap = new HashMap<>();
				for (Entry<String, User> entry : userMap.entrySet()) {
					if (userIdConf.userBalance() && !UserUtils.isAgentStaff(entry.getValue())) {
						entry.getValue().setTotalBalance(userBalance.get(entry.getValue().getUserId()).doubleValue());
					}
					if (userIdConf.walletBalance() && !UserUtils.isAgentStaff(entry.getValue())) {
						entry.getValue()
								.setWalletBalance(walletBalance.get(entry.getValue().getUserId()).doubleValue());
					}
					UserRole role = userIdConf.userRole() ? entry.getValue().getRole() : null;
					String email = userIdConf.userEmail() ? entry.getValue().getEmail() : null;
					String mobile = userIdConf.userMobile() ? entry.getValue().getMobile() : null;
					Set<User> userRelations = relations.get(entry.getKey());
					entry.getValue().setUserRelations(userRelations);
					List<Policy> policies = policyMap.get(entry.getKey());
					if (userIdConf.userPolicies()) {
						entry.getValue().getAdditionalInfo().setPolicies(policies);
					}
					User.UserBuilder userBuilder =
							User.builder().name(entry.getValue().getName()).userId(entry.getValue().getUserId())
									.userRelations(entry.getValue().getUserRelations()).role(role)
									.additionalInfo(UserAdditionalInfo.builder()
											.grade(entry.getValue().getAdditionalInfo().getGrade())
											.policies(entry.getValue().getAdditionalInfo().getPolicies()).build())
									.totalBalance(entry.getValue().getTotalBalance())
									.walletBalance(entry.getValue().getWalletBalance())
									.parentUserId(entry.getValue().getParentUserId())
									.partnerId(entry.getValue().getPartnerId()).email(email).mobile(mobile);
					userBuilder.userConf(UserConfiguration.builder()
							.distributorId(entry.getValue().getUserConf().getDistributorId()).build());
					responseMap.put(entry.getKey(), userBuilder.build());
					obj.getMetaInfo().put(META_KEY, responseMap);
				}
				log.debug("[UserListing]Final response");

			}
		}
	}
}
