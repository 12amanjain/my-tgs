package com.tgs.services.base.sd;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.DescribeInstancesRequest;
import com.amazonaws.services.ec2.model.DescribeInstancesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.Instance;
import com.amazonaws.services.ec2.model.Reservation;
import com.amazonaws.services.ec2.model.Tag;

@Service
public class AwsInstanceDiscovery {

	public List<String> getIpAddresses(String nameTag) {
		final List<String> addresses = new ArrayList<>();

		if (StringUtils.isBlank(nameTag)) {
			return addresses;
		}

		final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();
		boolean done = false;

		DescribeInstancesRequest request = new DescribeInstancesRequest();
		request.withFilters(new Filter("instance-state-name", Arrays.asList("running")));

		while (!done) {
			DescribeInstancesResult response = ec2.describeInstances(request);
			filterInstancesAndAddTo(nameTag, response, addresses);

			/**
			 * Token in response is used to retrieve the next page of results. This value is <code>null</code> when
			 * there are no more results to return.
			 */
			request.setNextToken(response.getNextToken());
			done = response.getNextToken() == null;
		}

		return addresses;
	}

	private void filterInstancesAndAddTo(String nameTag, DescribeInstancesResult response, List<String> addresses) {
		for (Reservation reservation : response.getReservations()) {
			for (Instance instance : reservation.getInstances()) {
				for (Tag tag : instance.getTags()) {
					if (nameTag.equals(tag.getValue())) {
						addresses.add(instance.getPrivateIpAddress());
					}
				}
			}
		}
	}

}
