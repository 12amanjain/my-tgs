package com.tgs.services.base.sd;

import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.gson.GsonUtils;
import lombok.Getter;

@Component
public class ServiceDiscoveryConfiguration {

	@Value("${sd.conf}")
	private String conf;

	private ServiceDiscoveryConfigurationData sdConfigData;
	
	@PostConstruct
	private void init() {
		sdConfigData = GsonUtils.getGson().fromJson(conf, ServiceDiscoveryConfigurationData.class);
	}

	public String getServiceType(String serviceName) {
		if (getConfigData() == null) {
			return null;
		}
		String defaultServiceType = getConfigData().getDefaultServiceType();
		return getConfigData().getServiceTypeMapping().getOrDefault(serviceName, defaultServiceType);
	}
	
	public AwsInstanceDiscovery getSdService() {
		if (getConfigData() == null || getConfigData().getSdBean() == null) {
			return null;
		}
		return SpringContext.getApplicationContext().getBean(getConfigData().getSdBean(), AwsInstanceDiscovery.class);
	}

	private ServiceDiscoveryConfigurationData getConfigData() {
		return sdConfigData;
	}

	@Getter
	private static class ServiceDiscoveryConfigurationData {
		private String sdBean;
		private String defaultServiceType;
		private Map<String, String> serviceTypeMapping;

		Map<String, String> getServiceTypeMapping() {
			if (serviceTypeMapping == null) {
				serviceTypeMapping = new HashMap<>();
			}
			return serviceTypeMapping;
		}
	}
}
