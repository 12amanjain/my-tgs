package com.tgs.services.base.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserStatus;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class APIKeyAuthorizationFilter extends BasicAuthenticationFilter {

	public APIKeyAuthorizationFilter(AuthenticationManager authenticationManager) {
		super(authenticationManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		try {
			String apiKey = getAPIKey(req);
			if (StringUtils.isBlank(apiKey)) {
				chain.doFilter(req, res);
				return;
			}
			UsernamePasswordAuthenticationToken authentication = getAuthentication(req);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			chain.doFilter(req, res);
		} catch (CustomAuthenticationException e) {
			log.error("Exception occured while servicing request {}, {}, {} , due to {}", req.getServletPath(),
					req.getPathInfo(), req.getQueryString(), e.getMessage(), e);
			onUnsuccessfulAuthentication(req, res, new CustomAuthenticationException(e.getError()));
		}
	}

	private static String getAPIKey(HttpServletRequest request) {
		return request.getHeader("apikey");
	}


	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest req) {
		User user = APIKeyHelper.getAPIUser(getAPIKey(req));
		if (Objects.isNull(user)) {
			throw new CustomAuthenticationException(SystemError.UNAUTHORIZED_APIKEY);
		} else if (!UserStatus.ENABLED.equals(user.getStatus())) {
			throw new CustomAuthenticationException(SystemError.INACTIVE_ACCOUNT);
		} else {
			return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
		}
	}

	@Override
	protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException {
		SecurityContextHolder.clearContext();
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		CustomAuthenticationException cX = (CustomAuthenticationException) failed;
		res.addError(new ErrorDetail(cX.getError(), cX.getMessage()));
		response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));
	}
}
