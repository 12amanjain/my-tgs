package com.tgs.services.base.security;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.aerospike.client.Bin;
import com.aerospike.client.Operation;
import com.google.common.reflect.TypeToken;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.abusecontrol.AbuseControlInfo;
import com.tgs.services.gms.datamodel.abusecontrol.ApiRateLimit;
import com.tgs.services.gms.datamodel.abusecontrol.ApplicationRateLimit;
import com.tgs.services.gms.datamodel.abusecontrol.RateLimit;
import com.tgs.services.gms.datamodel.abusecontrol.RateLimitCombination;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.gms.ruleengine.AbuseControlOutput;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class AbuseControlFilter extends BaseFilter {

	@Autowired
	private GeneralServiceCommunicator gmsComm;

	@Autowired
	private GeneralCachingCommunicator generalCachingComm;

	private static List<ConfiguratorInfo> configInfoList = new ArrayList<>();

	private static List<ApiRateLimit> API_RATE_LIMITS = new ArrayList<>();;
	private static List<ApiRateLimit> ENV_ACCESS_LIMIT = new ArrayList<>();;

	@Value("${abuseControlConfig}")
	public void setEnvAccessLimit(String abuseControlConfigFromEnv) {
		if (StringUtils.isNotEmpty(abuseControlConfigFromEnv)
				&& !"@abuseControlConfig@".equals(abuseControlConfigFromEnv)) {
			ENV_ACCESS_LIMIT =
					GsonUtils.getGson().fromJson(abuseControlConfigFromEnv, new TypeToken<List<ApiRateLimit>>() {
						private static final long serialVersionUID = 1L;
					}.getType());
		} else {
			ENV_ACCESS_LIMIT = new ArrayList<>();
		}
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		try {
			User user = SystemContextHolder.getContextData().getUser();
			String userId = user != null ? user.getUserId() : "";
			String ip = SystemContextHolder.getContextData().getHttpHeaders().getIp();
			String deviceId = SystemContextHolder.getContextData().getHttpHeaders().getDeviceid();
			String requestUrl = httpRequest.getRequestURL().toString();
			List<String> combinations = Arrays.asList(userId, ip, deviceId, userId + "_" + deviceId,
					userId + "_" + requestUrl, deviceId + "_" + requestUrl, ip + "_" + requestUrl, ip + "_" + deviceId,
					userId + "_" + deviceId + "_" + requestUrl);
			// Default configs from code
			List<ApiRateLimit> apiRateLimits = new ArrayList<>(getDefaultAccessLimit());
			// Overridden configs from env
			apiRateLimits.addAll(ENV_ACCESS_LIMIT);
			if (CollectionUtils.isNotEmpty(configInfoList)) {
				for (AbuseControlInfo abuseControlInfo : ((AbuseControlOutput) configInfoList.get(0).getOutput())
						.getAbuseControlInfos()) {
					// If any of the combinations in black-listed, take action (throw error for now)
					String blackListed = combinations.stream().filter(
							p -> p != null && matchesAnyElement(abuseControlInfo.getBlackListedCombinations(), p))
							.findFirst().orElse("");
					if (StringUtils.isNotEmpty(blackListed))
						accessDeniedResponse(response, requestUrl, blackListed);

					// If any of the combinations is white-listed, allow api access irrespective of any restriction
					String whiteListed = combinations.stream().filter(
							p -> p != null && matchesAnyElement(abuseControlInfo.getWhiteListedCombinations(), p))
							.findFirst().orElse("");
					if (StringUtils.isNotEmpty(whiteListed))
						break;

					// Overridden configs in cache from ABUSECONTROL ruleType
					List<ApiRateLimit> apiRateLimitsInCache = new ArrayList<>(abuseControlInfo.getApiAccessLimit());
					apiRateLimits.addAll(apiRateLimitsInCache);
				}
			}
			// If request is not validated by any rule, which can happen if new service is added, consider a default
			// check
			boolean requestValidated = false;

			Collections.sort(apiRateLimits,
					(Comparator<ApiRateLimit>) (first, second) -> second.getPriority() - first.getPriority());

			for (ApiRateLimit apiAcessLimit : apiRateLimits) {
				if (requestUrl.matches(apiAcessLimit.getEndPointUrl())) {
					requestValidated = true;
					String combination = validateUrlAccess(user, ip, deviceId, requestUrl, apiAcessLimit);
					if (StringUtils.isNotEmpty(combination))
						accessDeniedResponse(response, requestUrl, combination);
					else
						break;
				}

			}

			if (!requestValidated && false) {
				ApiRateLimit apiAcessLimit = constructApiRateLimit(".*/default/.*", -1, true, null, 60);
				String combination = validateUrlAccess(user, ip, deviceId, requestUrl, apiAcessLimit);
				if (StringUtils.isNotEmpty(combination))
					accessDeniedResponse(response, requestUrl, combination);
			}
			chain.doFilter(httpRequest, response);
		} catch (CustomGeneralException e) {

		} catch (Exception e) {
			log.error("Error occurred while checking for abuse control with request {} due to {} ",
					httpRequest.getRequestURL().toString(), e.getMessage(), e);
			chain.doFilter(httpRequest, response);
		}
	}

	private String validateUrlAccess(User user, String ip, String deviceId, String requestUrl,
			ApiRateLimit apiAcessLimit) throws IOException {
		long currentTimeinMs = System.currentTimeMillis();
		boolean isValidAccess = true;
		String idInKey = user != null ? UserUtils.isPartnerUser(user) ? user.getPartnerId() : user.getUserId() : "";
		// This code verifies access limit for specific combinations
		List<RateLimitCombination> rateLimitCombinations = apiAcessLimit.getRateLimitCombinations();
		String abuseCombination = "";
		for (RateLimitCombination combination : rateLimitCombinations) {

			String key = getKeyFromCombination(combination, apiAcessLimit.getEndPointUrl(), idInKey, ip, deviceId);
			Map<TimeUnit, RateLimit> rateLimit = combination.getAllowedApiLimit();
			// If rate limit rule is written for API user
			if (UserUtils.isApiUserRequest(user) && MapUtils.isNotEmpty(combination.getAllowedApiLimitForApiUser())) {
				rateLimit = combination.getAllowedApiLimitForApiUser();
			}
			// If rate limit rule is written for Partner users
			if (UserUtils.isPartnerUser(user) && MapUtils.isNotEmpty(combination.getAllowedApiLimitForPartnerUser())) {
				rateLimit = combination.getAllowedApiLimitForPartnerUser();
			}
			// If rate limit rule is written Guest user on b2c platform
			if (user != null && UserRole.GUEST.equals(user.getRole())
					&& MapUtils.isNotEmpty(combination.getAllowedApiLimitForGuest())) {
				rateLimit = combination.getAllowedApiLimitForGuest();
			}
			// If rate limit rule is written for Customer user on b2c platform
			if (user != null && UserRole.CUSTOMER.equals(user.getRole())
					&& MapUtils.isNotEmpty(combination.getAllowedApiLimitForCustomer())) {
				rateLimit = combination.getAllowedApiLimitForCustomer();
			}
			// If rate limit is overridden for any specific combination, then we will give priority to overridden rule.
			Map<String, Map<TimeUnit, RateLimit>> allowedLimitForCombinations =
					combination.getAllowedLimitForCombinations();
			if (MapUtils.isNotEmpty(allowedLimitForCombinations) && allowedLimitForCombinations.containsKey(key)) {
				rateLimit = allowedLimitForCombinations.get(key);
			}
			abuseCombination = validateAccessLimit(currentTimeinMs, key, rateLimit);
			isValidAccess = isValidAccess && StringUtils.isEmpty(abuseCombination);
		}

		if (!isValidAccess)
			return abuseCombination;

		// This code verifies access limit for each host
		List<ApplicationRateLimit> hostWiseRateLimit = apiAcessLimit.getHostWiseRateLimit();
		for (ApplicationRateLimit arl : hostWiseRateLimit) {
			String key = apiAcessLimit.getEndPointUrl() + "_" + ServiceUtils.getHostName();
			Map<TimeUnit, RateLimit> rateLimit = arl.getAllowedApiLimit();
			abuseCombination = validateAccessLimit(currentTimeinMs, key, rateLimit);
			isValidAccess = isValidAccess && StringUtils.isEmpty(abuseCombination);
		}
		if (!isValidAccess)
			return abuseCombination;

		// This code verifies access limit for overall application
		List<ApplicationRateLimit> applicationRateLimit = apiAcessLimit.getApplicationRateLimit();
		for (ApplicationRateLimit arl : applicationRateLimit) {
			Map<TimeUnit, RateLimit> rateLimit = arl.getAllowedApiLimit();
			abuseCombination = validateAccessLimit(currentTimeinMs, apiAcessLimit.getEndPointUrl(), rateLimit);
			isValidAccess = isValidAccess && StringUtils.isEmpty(abuseCombination);;
		}

		return abuseCombination;
	}

	private String validateAccessLimit(long currentTimeinMs, String key, Map<TimeUnit, RateLimit> rateLimit) {
		for (Map.Entry<TimeUnit, RateLimit> entry : rateLimit.entrySet()) {
			Integer apiRateLimit = entry.getValue().getApiLimit();
			Integer apiNotifyLimit = entry.getValue().getNotifyLimit();
			if (apiRateLimit != null) {
				if (apiNotifyLimit == null || apiNotifyLimit.intValue() == 0)
					apiNotifyLimit = apiRateLimit;
				String uniqueId = key + "_" + getStringRepresentation(currentTimeinMs, entry.getKey());
				CacheMetaInfo metaInfo =
						CacheMetaInfo.builder().key(uniqueId).namespace(CacheNameSpace.GENERAL_PURPOSE.name())
								.plainData(true).set(CacheSetName.API_REQUEST_DATA.getName()).build();
				Bin bin = new Bin(BinName.COUNT.getName(), 1);
				Long accessCount = generalCachingComm.performOperationsAndFetchBinValue(metaInfo,
						BinName.COUNT.getName(), Long.class, getTTLBasedOnTimeUnit(entry.getKey()), Operation.add(bin),
						Operation.get(BinName.COUNT.getName()));
				if (accessCount > apiRateLimit) {
					if (accessCount > apiNotifyLimit) {
						log.info("Access count per {} i.e, {} has crossed the limit {}  for key {} ",
								entry.getKey().name(), accessCount, apiNotifyLimit, key);
					}
					return key;
				}
			}
		}
		return "";
	}

	private String getStringRepresentation(long timestamp, TimeUnit timeunit) {
		Date d = new Date(timestamp);
		if (TimeUnit.SECONDS.equals(timeunit)) {
			DateFormat f = new SimpleDateFormat("HHmmss");
			return f.format(d);
		}
		if (TimeUnit.MINUTES.equals(timeunit)) {
			DateFormat f = new SimpleDateFormat("HHmm");
			return f.format(d);
		}
		if (TimeUnit.HOURS.equals(timeunit)) {
			DateFormat f = new SimpleDateFormat("HH");
			return f.format(d);
		}
		return "";
	}

	private int getTTLBasedOnTimeUnit(TimeUnit timeunit) {
		if (TimeUnit.SECONDS.equals(timeunit))
			return 1;
		if (TimeUnit.MINUTES.equals(timeunit))
			return 60;
		if (TimeUnit.HOURS.equals(timeunit))
			return 60 * 60;
		if (TimeUnit.DAYS.equals(timeunit))
			return 24 * 60;
		return 0;
	}

	private String getKeyFromCombination(RateLimitCombination combination, String requestUrl, String userId, String ip,
			String deviceId) {
		StringBuilder sb = new StringBuilder(requestUrl);
		if (BooleanUtils.isTrue(combination.getConsiderUserId()) && StringUtils.isNotEmpty(userId))
			sb.append("_").append(userId);
		if (BooleanUtils.isTrue(combination.getConsiderIp()) && StringUtils.isNotEmpty(ip))
			sb.append("_").append(ip);
		if (BooleanUtils.isTrue(combination.getConsiderDeviceId()) && StringUtils.isNotEmpty(deviceId))
			sb.append("_").append(deviceId);
		return sb.toString();
	}

	/**
	 * Returns true if list contains any element whose regex matches with element
	 */
	private boolean matchesAnyElement(List<String> list, String element) {
		String anyMatch = list.stream().filter(c -> element.matches(c)).findFirst().orElse("");
		return StringUtils.isNotEmpty(anyMatch);
	}

	@Scheduled(initialDelay = 5 * 1000, fixedDelay = 60 * 1000)
	public void setConfigInfoList() {
		SystemContextHolder.setContextData(null);
		configInfoList = gmsComm.getConfiguratorInfos(ConfiguratorRuleType.ABUSECONTROL);
	}

	private void accessDeniedResponse(ServletResponse response, String requestUrl, String combination)
			throws IOException {
		log.info("Abuse control detected for requestUrl {} and combination {} ", requestUrl, combination);
		addToAnalytics(requestUrl, combination, SystemError.ACCESS_DENIED);
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		res.addError(new ErrorDetail(SystemError.ACCESS_DENIED.errorCode(), SystemError.ACCESS_DENIED.getMessage()));
		response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));
		throw new CustomGeneralException(SystemError.ACCESS_DENIED);
	}

	private List<ApiRateLimit> getDefaultAccessLimit() {
		if (CollectionUtils.isEmpty(API_RATE_LIMITS)) {
			API_RATE_LIMITS = new ArrayList<>();
			API_RATE_LIMITS.add(constructApiRateLimit(".*/ums/v1/users", 1, true, null, 60));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/ums/v1/sign-in", 1, null, true, 5));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/gms/v1/generate-otp", 1, null, true, 15));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/oms/v1/orders", 1, true, null, 25));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/oms/v1/amendments/search", 1, true, null, 25));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/hms/v1/hotel-searchquery-list", 1, true, null, 15));
			/**
			 * air-search-all url is used for API only. It's limit is high due to tripjack hits are higher on Tripsasta
			 * and Travelimpression. TI & TS only sells deal inventory therefore such high hits are allowed
			 */
			API_RATE_LIMITS.add(constructApiRateLimit(".*/fms/v1/air-search-all", 1, true, null, 100));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/fms/v1/air-searchquery-list*", 1, true, null, 15));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/ums/.*", -10, true, null, 30));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/ims/.*", -10, true, null, 40));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/rms/.*", -10, true, null, 5));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/hms/.*", -10, true, null, 600));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/pms/.*", -10, true, null, 50));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/cms/.*", -10, true, null, 15));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/ffts/.*", -10, null, true, 3));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/ps/.*", -10, true, null, 30));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/fms/.*", -10, true, null, 600));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/oms/.*", -10, true, null, 80));
			API_RATE_LIMITS.add(constructApiRateLimit(".*/trip/.*", -10, true, null, 20));
		}
		return API_RATE_LIMITS;
	}

	private ApiRateLimit constructApiRateLimit(String url, int priority, Boolean considerUser, Boolean considerDeviceId,
			int minuteLimit) {
		Map<TimeUnit, RateLimit> allowedApiLimit = new HashMap<>();
		allowedApiLimit.put(TimeUnit.MINUTES, RateLimit.builder().apiLimit(minuteLimit).build());
		RateLimitCombination rlc = RateLimitCombination.builder().considerUserId(considerUser)
				.considerDeviceId(considerDeviceId).allowedApiLimit(allowedApiLimit).build();
		return ApiRateLimit.builder().endPointUrl(url).priority(priority)
				.rateLimitCombinations(Collections.singletonList(rlc)).build();
	}

}
