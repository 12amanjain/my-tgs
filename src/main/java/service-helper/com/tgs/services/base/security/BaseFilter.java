package com.tgs.services.base.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.filter.GenericFilterBean;
import com.tgs.services.base.analytics.APIRequestDataMapper;
import com.tgs.services.base.analytics.BaseAnalyticsHelper;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class BaseFilter extends GenericFilterBean {

	@Autowired
	private BaseAnalyticsHelper analyticsHelper;

	public void addToAnalytics(String requestUrl, String combination, SystemError error) {
		try {
			ContextData contextData = SystemContextHolder.getContextData();
			APIRequestDataMapper requestData = APIRequestDataMapper.builder().user(contextData.getUser())
					.contextData(contextData).endPoint(requestUrl).requestObj(contextData.getRequestObj())
					.timeInMillsec(contextData.getStopWatch().getTime()).build();
			requestData.setCombination(combination);
			requestData.setErrMsg(error.getMessage());
			requestData.setErrorCode(Integer.valueOf(error.errorCode()));
			analyticsHelper.store(requestData);
		} catch (Exception e) {
			log.error("Exception occured while storing filter logs in ES due to {} ", e);
		}
	}
}
