package com.tgs.services.base.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.HttpHeader;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.OTPCommunicator;
import com.tgs.services.base.datamodel.DeviceInfo;
import com.tgs.services.base.datamodel.SecurityConfiguration;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.JwtExcludeStrategy;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.OtpToken;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.base.gms.OtpValidateRequest;

import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.restmodel.SignInRequest;
import com.tgs.services.ums.restmodel.SignInResponse;
import com.tgs.utils.common.HttpUtils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

	private AuthenticationManager authenticationManager;
	private OTPCommunicator otpComm;
	private GeneralServiceCommunicator gnComm;
	private GeneralCachingCommunicator cachingCommunicator;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager) {
		this.authenticationManager = authenticationManager;
		setFilterProcessesUrl(SecurityConstants.LOGIN_URL);
		otpComm = SpringContext.getApplicationContext().getBean(OTPCommunicator.class);
		gnComm = SpringContext.getApplicationContext().getBean(GeneralServiceCommunicator.class);
		cachingCommunicator = SpringContext.getApplicationContext().getBean(GeneralCachingCommunicator.class);
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res)
			throws AuthenticationException {
		try {
			SignInRequest creds = new ObjectMapper().readValue(req.getInputStream(), SignInRequest.class);
			UsernamePasswordAuthenticationToken userNamePasswordToken = new UsernamePasswordAuthenticationToken(
					creds.getUsername(), creds.getPassword(), new ArrayList<>());
			userNamePasswordToken.setDetails(creds);
			ContextData contextData = ContextData.builder().httpRequest(req).httpResponse(res)
					.httpHeaders(HttpUtils.buildHttpHeader(req)).build();
			SystemContextHolder.setContextData(contextData);
			return authenticationManager.authenticate(userNamePasswordToken);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain,
			Authentication auth) throws IOException, ServletException {
		SignInResponse signInResponse = (SignInResponse) auth.getDetails();
		Gson gson = GsonUtils.getGsonBuilder().setPrettyPrinting().create();
		try {
			if (!validate2dAuthenication(signInResponse)) {
				signInResponse = new SignInResponse();
				signInResponse.setTwoDAuthRequired(true);
				signInResponse.setStatus(new Status(HttpStatusCode.HTTP_200));
			} else {
				String accessToken = JWTHelper.generateAndStoreAccessToken(signInResponse.getUser(), res);
				signInResponse.setAccessToken(accessToken);
			}
			res.getWriter().write(gson.toJson(signInResponse));
		} catch (CustomGeneralException e) {
			unsuccessfulAuthentication(req, res, new CustomAuthenticationException(e.getError()));
		}
	}

	/**
	 * Validate OTP if otpValidateRequest passed in signInResponse
	 * 
	 * @param signInResponse
	 * @return
	 */
	private boolean validate2dAuthenication(SignInResponse signInResponse) {
		ClientGeneralInfo clientConf = gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		User user = signInResponse.getUser();
		if (clientConf.getSecurityConfiguration().isLoginInUsing2d()
				&& clientConf.getSecurityConfiguration().getIncludedPartnerIds().contains(user.getPartnerId())
				&& !clientConf.getSecurityConfiguration().getExpemted2dLoginInUserIds().contains(user.getUserId())
				&& !clientConf.getSecurityConfiguration().getExempted2dLoginUserRoles().contains(user.getRole())) {
			ContextData contextData = SystemContextHolder.getContextData();
			String deviceId = HttpUtils.getValueFromCookieOrHeader(contextData.getHttpRequest(), "deviceid");
			if (StringUtils.isBlank(deviceId)) {
				throw new CustomGeneralException(SystemError.INVALID_DEVICE_ID);
			}
			OtpValidateRequest otpValidateRequest = signInResponse.getOtpValidateRequest();
			/**
			 * This is to support back compatibility
			 */
			DeviceInfo deviceInfo = getDeviceInfo(deviceId);
			deviceId = StringUtils.join(deviceId, "_", user.getUserId());
			deviceInfo = getDeviceInfo(deviceId);

			if (deviceInfo != null) {
				storeDeviceInfo(deviceId, user, clientConf.getSecurityConfiguration());
				log.info("Deviceid {} , userId {} is already whitelisted", deviceId, user.getUserId());
				return true;
			}

			if (deviceInfo == null
					&& (otpValidateRequest == null || StringUtils.isBlank(otpValidateRequest.getOtp()))) {
				otpComm.generateOtp(
						OtpToken.builder().requestId(deviceId).type("SIGNIN_OTP").key(user.getMobile()).build());
				return false;
			} else {
				OtpToken otpToken = otpComm.validateOtp(deviceId, otpValidateRequest.getOtp(), 50);
				if (otpToken != null && otpToken.getMobile().equals(user.getMobile())
						&& otpToken.getEmail().equals(user.getEmail())) {
					otpComm.updateConsumedOtp(deviceId);
					storeDeviceInfo(deviceId, user, clientConf.getSecurityConfiguration());
					log.info("2d authentication validated for Deviceid {} , userId {}", deviceId, user.getUserId());
					return true;
				}
				log.error("Failed to login due to invalid otp for userId {}, requestId {},otp {}", user.getUserId(),
						deviceId, otpValidateRequest.getOtp());
				throw new CustomGeneralException(SystemError.INVALID_OTP);
			}
		}
		return true;

	}

	public DeviceInfo getDeviceInfo(String deviceId) {
		return cachingCommunicator.getBinValue(
				CacheMetaInfo.builder().set(CacheSetName.DEVICE_INFO.getName())
						.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).key(deviceId).build(),
				DeviceInfo.class, false, false, BinName.DEVICEINFO.getName());
	}

	public void storeDeviceInfo(String deviceId, User user, SecurityConfiguration securityConf) {
		ContextData contextData = SystemContextHolder.getContextData();
		HttpHeader header = contextData.getHttpHeaders();
		Map<String, Object> binMap = new HashMap<>();
		DeviceInfo deviceInfo = DeviceInfo.builder().deviceId(deviceId).browser(header.getBrowser())
				.userId(user.getUserId()).browserVersion(header.getBrowserVersion())
				.channelType(header.getChannelType()).os(header.getDevice()).build();
		binMap.put(BinName.DEVICEINFO.getName(), deviceInfo);
		binMap.put(BinName.USERID.getName(), user.getUserId());
		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.DEVICE_INFO.getName())
						.namespace(CacheNameSpace.GENERAL_PURPOSE.getName()).key(deviceId).build(),
				binMap, false, false, securityConf.getDeviceInactivityExpriration());
	}

	public static String generateAccessToken(User user) {
		Gson gson = GsonUtils.builder().strategies(Arrays.asList(new JwtExcludeStrategy())).build().buildGson();
		String token = Jwts.builder().setSubject(gson.toJson(user))
				.setExpiration(new Date(System.currentTimeMillis() + SecurityConstants.ACCESSTOKEN_EXPIRATION_TIME))
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.getSecretKey().getBytes()).compact();
		return token;
	}

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		SecurityContextHolder.clearContext();
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		CustomAuthenticationException cX = (CustomAuthenticationException) failed;
		res.addError(new ErrorDetail(cX.getError(), cX.getMessage()));
		response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));
	}
}
