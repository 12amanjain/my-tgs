package com.tgs.services.base.security;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.HttpStatusCode;
import com.tgs.services.base.restmodel.Status;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class JWTAuthorizationFilter extends BasicAuthenticationFilter {

	public JWTAuthorizationFilter(AuthenticationManager authManager) {
		super(authManager);
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		try {
			String header = ObjectUtils.firstNonNull(req.getHeader(SecurityConstants.HEADER_STRING),
					HttpUtils.getValueFromCookie(req, "uuid"));
			if (header == null || !header.startsWith(SecurityConstants.TOKEN_PREFIX)) {
				chain.doFilter(req, res);
				return;
			}

			UsernamePasswordAuthenticationToken authentication = getAuthentication(req, res);
			SecurityContextHolder.getContext().setAuthentication(authentication);
			chain.doFilter(req, res);
		} catch (ExpiredJwtException | UnsupportedJwtException | MalformedJwtException | SignatureException ex) {
			log.info("Unable to validate JWT {}", ex.getLocalizedMessage());
			onUnsuccessfulAuthentication(req, res, null);
		} catch (Exception e) {
			log.error("Exception occured while servicing request {}, {}, {} , due to {}", req.getServletPath(),
					req.getPathInfo(), req.getQueryString(), e.getMessage(), e);
			onUnsuccessfulAuthentication(req, res, null);
		}
	}

	private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request, HttpServletResponse res) {
		User user = JWTHelper.validateAndGenerateNewTokenIfExpired(request, res);
		if (user != null)
			return new UsernamePasswordAuthenticationToken(user, null, new ArrayList<>());
		return null;
	}

	@Override
	protected void onUnsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException {
		SecurityContextHolder.clearContext();
		BaseResponse res = new BaseResponse();
		res.setStatus(new Status(HttpStatusCode.HTTP_403));
		res.addError(new ErrorDetail(SystemError.FORBIDDEN));
		response.getWriter().write(GsonUtils.getGsonBuilder().setPrettyPrinting().create().toJson(res));
	}
}
