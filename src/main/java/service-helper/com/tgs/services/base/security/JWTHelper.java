package com.tgs.services.base.security;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.JwtExcludeStrategy;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.base.utils.TgsDateUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.utils.common.HttpUtils;
import com.tgs.utils.exception.UnAuthorizedException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class JWTHelper {

	public static GeneralCachingCommunicator cachingCommunicator;

	public static UserServiceCommunicator userService;

	static PaymentServiceCommunicator paymentService;

	protected static List<String> JWT_VALIDATE_URLS;

	@Autowired
	public JWTHelper(GeneralCachingCommunicator cachingCommunicator, UserServiceCommunicator userService,
			PaymentServiceCommunicator paymentService) {
		JWTHelper.cachingCommunicator = cachingCommunicator;
		JWTHelper.userService = userService;
		JWTHelper.paymentService = paymentService;

	}


	public static String generateAndStoreAccessToken(User user, HttpServletResponse res, long expirySec) {
		Gson gson = GsonUtils.builder().strategies(Arrays.asList(new JwtExcludeStrategy())).build().buildGson();
		boolean longterm = (expirySec != SecurityConstants.ACCESSTOKEN_EXPIRATION_TIME) ? true : false;
		String token = Jwts.builder().setSubject(gson.toJson(user))
				.setExpiration(
						new Date(System.currentTimeMillis() + SecurityConstants.ACCESSTOKEN_EXPIRATION_TIME * 1000))
				.setHeaderParam("longterm", longterm)
				.signWith(SignatureAlgorithm.HS512, SecurityConstants.getSecretKey().getBytes()).compact();
		if (res != null) {
			res.setHeader(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + token);
		}
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.USERID.getName(), user.getUserId());
		binMap.put(BinName.AT.getName(), token);
		cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.JWT.getName())
				.namespace(CacheNameSpace.USERS.getName()).key(token).build(), binMap, false, true, (int) expirySec);
		return token;
	}

	public static String generateAndStoreAccessToken(User user) {
		return generateAndStoreAccessToken(user, null);
	}

	public static String generateAndStoreAccessToken(User user, HttpServletResponse res) {
		return generateAndStoreAccessToken(user, res, SecurityConstants.ACCESSTOKEN_EXPIRATION_TIME);
	}

	public static String getJWTToken(HttpServletRequest request) {
		String token = request.getHeader(SecurityConstants.HEADER_STRING);
		if (StringUtils.isBlank(token)) {
			token = HttpUtils.getValueFromCookie(request, "uuid");
			if (StringUtils.isBlank(token)) {
				return null;
			}
		}
		return token.replace(SecurityConstants.TOKEN_PREFIX, "");
	}

	public static User validateAndGenerateNewTokenIfExpired(HttpServletRequest request, HttpServletResponse res) {
		String token = getJWTToken(request);
		if (StringUtils.isBlank(token)) {
			return null;
		}

		LocalDateTime expirationTime = TgsDateUtils
				.convertDateToLocalDateTime(Jwts.parser().setSigningKey(SecurityConstants.getSecretKey().getBytes())
						.parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, "")).getBody().getExpiration());
		Jws<Claims> claims = Jwts.parser().setSigningKey(SecurityConstants.getSecretKey().getBytes())
				.parseClaimsJws(token.replace(SecurityConstants.TOKEN_PREFIX, ""));

		boolean isLongTerm = BooleanUtils.isTrue((Boolean) claims.getHeader().get("longterm"));

		String userStr = claims.getBody().getSubject();
		User user = GsonUtils.getGson().fromJson(userStr, User.class);


		if (ServiceUtils.isUrlMatchesRegex(JWT_VALIDATE_URLS, request.getRequestURI())) {
			Map<String, String> binMap =
					cachingCommunicator.get(
							CacheMetaInfo.builder().set(CacheSetName.JWT.getName())
									.namespace(CacheNameSpace.USERS.getName()).key(token).build(),
							String.class, false, true, new String[0]);
			if (MapUtils.isEmpty(binMap)) {
				return null;
			}
			User nUser = userService.getUser(
					UserFilter.builder().userId(user.getUserId()).statuses(Arrays.asList(UserStatus.ENABLED)).build());
			if (nUser == null) {
				return null;
			}
		}


		if (!isLongTerm) {
			LocalDateTime now = LocalDateTime.now();
			now = now.plusSeconds(
					SecurityConstants.ACCESSTOKEN_EXPIRATION_TIME - SecurityConstants.ACCESSTOKEN_REFRESH_TIME);

			if (now.isAfter(expirationTime)) {
				user = generateNewToken(user, token, res, false);
			}
		}
		return user;
	}


	public static User generateNewToken(User oldUser, String token, HttpServletResponse res, boolean forceRefresh) {
		User user = userService.getUser(
				UserFilter.builder().userId(oldUser.getUserId()).statuses(Arrays.asList(UserStatus.ENABLED)).build());
		if (user == null) {
			return null;
		}

		/**
		 * This is to ensure that old user and new user is always same
		 */
		if (!oldUser.getUserId().equals(user.getUserId())) {
			log.error("Old UserId and new UserId there is a mismatch, olduserId is {}, new userId is {}",
					oldUser.getUserId(), user.getUserId());
			throw new UnAuthorizedException(SystemError.FORBIDDEN);
		}

		Map<String, String> binMap =
				cachingCommunicator.get(
						CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
								.set(CacheSetName.JWT.getName()).key(token).build(),
						String.class, false, true, new String[0]);
		if (binMap == null || binMap.get(BinName.AT.getName()) == null) {
			log.info("Token is already expired , expired token is {}", token);
			throw new UnAuthorizedException(SystemError.FORBIDDEN);
		}
		ServiceUtils.updateUserInfoforJWTToken(user, paymentService, oldUser);
		String newToken = generateAndStoreAccessToken(user, res);
		binMap.put(BinName.AT.getName(), newToken);
		binMap.put(BinName.USERID.getName(), user.getUserId());

		/**
		 * {@link SecurityConstants#ACCESSTOKEN_EXPIRATION_TIME}
		 */
		log.debug("Removing old token from system {}", token);
		cachingCommunicator.store(CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.JWT.getName()).key(token).build(), binMap, false, true,
				(int) SecurityConstants.ACCESSTOKEN_REFRESH_TIME);

		return user;
	}

	public static String getDefaultToken() {
		User user = userService.getUser(UserFilter.builder().roles(Arrays.asList(UserRole.GUEST)).build());
		// 5 years expiry
		return generateAndStoreAccessToken(user, SystemContextHolder.getContextData().getHttpResponse(), -1);
	}

	@Value("${jwt.jwtvalidateurls}")
	public void setDisabledUrls(String[] jwtValidateUrls) {
		JWT_VALIDATE_URLS = jwtValidateUrls != null ? Arrays.asList(jwtValidateUrls) : Collections.emptyList();
	}

}
