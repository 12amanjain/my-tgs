package com.tgs.services.base.security;

import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.time.StopWatch;
import org.apache.log4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.GenericFilterBean;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.enums.ChannelType;
import com.tgs.services.base.helper.RequestContextHolder;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogTypes;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SystemContextFilter extends GenericFilterBean {
	
	@Autowired
	private UserServiceCommunicator userSrvComm;
	
	public SystemContextFilter() {
		this.userSrvComm = SpringContext.getApplicationContext().getBean(UserServiceCommunicator.class);
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		try {
			HttpServletRequest httpRequest = (HttpServletRequest) request;
			StopWatch stopWatch = new StopWatch();
			stopWatch.start();
			StopWatch splitWatch = new StopWatch();
			splitWatch.start();
			ContextData contextData = ContextData.builder().httpRequest(httpRequest).stopWatch(stopWatch)
					.splitWatch(splitWatch).httpResponse((HttpServletResponse) response)
					.requestContextHolder(RequestContextHolder.builder().build())
					.httpHeaders(HttpUtils.buildHttpHeader(httpRequest)).build();
			contextData.getHttpHeaders().setJwtToken(JWTHelper.getJWTToken(httpRequest));
			SystemContextHolder.setContextData(contextData);
			LogUtils.log(LogTypes.SYSTEM_FILTER, LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
					null);

			if (SecurityContextHolder.getContext().getAuthentication() != null
					&& SecurityContextHolder.getContext().getAuthentication().getPrincipal() instanceof User) {
				User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
				SystemContextHolder.getContextData().setUser(user);
				if (UserUtils.isApiUserRequest(user)) {
					SystemContextHolder.getContextData().getHttpHeaders().setChannelType(ChannelType.API);
				}
			}
			setMDCProperties();
			chain.doFilter(httpRequest, response);
		} catch (Exception e) {
			log.error("Error occurred while setting system context ", e);
		} finally {
			SystemContextHolder.clear();
		}
	}

	public static void setMDCProperties() {
		MDC.put("userid", UserUtils.getUserId(SystemContextHolder.getContextData().getUser()));
		MDC.put("ip", SystemContextHolder.getContextData().getHttpHeaders().getIp());
	}

}
