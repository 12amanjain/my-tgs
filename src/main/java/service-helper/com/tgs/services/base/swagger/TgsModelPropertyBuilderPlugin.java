package com.tgs.services.base.swagger;

import static springfox.documentation.schema.Annotations.findPropertyAnnotation;
import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.util.function.Function;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import com.fasterxml.jackson.databind.introspect.BeanPropertyDefinition;
import com.google.common.base.Optional;
import com.google.gson.annotations.SerializedName;
import io.swagger.annotations.ApiModelProperty;
import springfox.documentation.builders.ModelPropertyBuilder;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.schema.ModelPropertyBuilderPlugin;
import springfox.documentation.spi.schema.contexts.ModelPropertyContext;
import springfox.documentation.swagger.common.SwaggerPluginSupport;

@Component
@Order(SwaggerPluginSupport.SWAGGER_PLUGIN_ORDER)
public class TgsModelPropertyBuilderPlugin implements ModelPropertyBuilderPlugin {

	@Override
	public boolean supports(DocumentationType delimiter) {
		return true;
	}

	@Override
	public void apply(ModelPropertyContext context) {
		SerializedName serializedNameAnn = findAnnotation(context, SerializedName.class);
		if (serializedNameAnn != null) {
			ModelPropertyBuilder modelPropertyBuilder = context.getBuilder();
			modelPropertyBuilder.name(serializedNameAnn.value());
			ApiModelProperty apiModelPropertyAnn = findAnnotation(context, ApiModelProperty.class);
			if (apiModelPropertyAnn == null || StringUtils.isBlank(apiModelPropertyAnn.value())) {
				String fieldName = getFieldName(context);
				if (fieldName != null) {
					modelPropertyBuilder.description(fieldName);
				}
			}
		}
	}

	private <A extends Annotation> A findAnnotation(ModelPropertyContext context, Class<A> annClass) {
		return contextFunction(beanToAnnotation(annClass), annotatedElementToAnnotation(annClass)).apply(context);
	}

	private String getFieldName(ModelPropertyContext context) {
		return contextFunction(beanToFieldName(), annotatedElementToFieldName()).apply(context);
	}

	private <A> Function<ModelPropertyContext, A> contextFunction(Function<BeanPropertyDefinition, A> beanFunction,
			Function<AnnotatedElement, A> annotatedElementFunction) {
		return context -> {
			Optional<BeanPropertyDefinition> propertyDefinitionOptional = context.getBeanPropertyDefinition();
			if (propertyDefinitionOptional.isPresent()) {
				return beanFunction.apply(propertyDefinitionOptional.get());
			}
			Optional<AnnotatedElement> annotatedElementOptional = context.getAnnotatedElement();
			if (annotatedElementOptional.isPresent()) {
				return annotatedElementFunction.apply(annotatedElementOptional.get());
			}
			return null;
		};
	}

	private Function<BeanPropertyDefinition, String> beanToFieldName() {
		return BeanPropertyDefinition::getInternalName;
	}

	private Function<AnnotatedElement, String> annotatedElementToFieldName() {
		return annotatedElement -> {
			if (annotatedElement instanceof Field) {
				return ((Field) annotatedElement).getName();
			}
			return null;
		};
	}

	private <A extends Annotation> Function<BeanPropertyDefinition, A> beanToAnnotation(Class<A> annotationClass) {
		return propertyDefinition -> findPropertyAnnotation(propertyDefinition, annotationClass).orNull();
	}

	private <A extends Annotation> Function<AnnotatedElement, A> annotatedElementToAnnotation(
			Class<A> annotationClass) {
		return annotatedElement -> annotatedElement.getAnnotation(annotationClass);
	}

}
