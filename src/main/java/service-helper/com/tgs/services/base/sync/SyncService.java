package com.tgs.services.base.sync;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.HttpHeader;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.sd.AwsInstanceDiscovery;
import com.tgs.services.base.sd.ServiceDiscoveryConfiguration;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class SyncService {

	private final static List<String> HTTP_METHODS_SUPPORTING_POSTDATA = Lists.newArrayList("PUT", "POST", "PATCH");

	@Value("${server.port}")
	private String serverPort;

	@Autowired
	private ServiceDiscoveryConfiguration sdConfiguration;

	/**
	 * HTTP Request Method is captured using
	 * {@code SystemContextHolder.getContextData().getRequestContextHolder().getHttpRequestMethod()}.
	 * <p>
	 * If request body is not supported by the Request Method, then {@code request} is not considered.
	 * 
	 * @param serviceName Must be non-{@code null}
	 * @param request request body, if supported. May be {@code null}.
	 */
	public void sync(String serviceName, Object request) {
		String httpRequestMethod =
				SystemContextHolder.getContextData().getRequestContextHolder().getHttpRequestMethod();
		sync(serviceName, request, httpRequestMethod);
	}

	/**
	 * If request body is not supported by {@code requestMethod}, then {@code request} is not considered.
	 * 
	 * @param serviceName Must be non-{@code null}
	 * @param request May be {@code null}
	 * @param requestMethod May be {@code null}
	 */
	public void sync(String serviceName, Object request, String requestMethod) {
		/**
		 * do not propagate sync request to avoid infinite propagation
		 */
		if (SystemContextHolder.getContextData().getHttpHeaders().isSyncRequest()) {
			return;
		}
		List<String> ipAddresses = getIpAddresses(serviceName);
		if (CollectionUtils.isEmpty(ipAddresses)) {
			log.debug("No servers found for sync");
			return;
		}
		final String postData = request != null ? GsonUtils.getGson().toJson(request) : null;
		log.debug("Syncing data {} with {} ", postData, ipAddresses);
		ipAddresses.forEach(ip -> {
			try {
				sendRequest(postData, ip, requestMethod);
			} catch (IOException e) {
				log.error("Failed to sync request {} with ip {} due to {} ", postData, ip, e.getMessage(), e);
			}
		});
	}

	private List<String> getIpAddresses(String serviceName) {
		String nameTag = sdConfiguration.getServiceType(serviceName);
		AwsInstanceDiscovery instanceDiscovery = sdConfiguration.getSdService();
		if (nameTag == null || instanceDiscovery == null) {
			return null;
		}
		return instanceDiscovery.getIpAddresses(nameTag);
	}

	private void sendRequest(String postData, String ipAddress, String requestMethod) throws IOException {
		HttpHeader httpHeaders = SystemContextHolder.getContextData().getHttpHeaders();
		Map<String, String> headerParams = new HashMap<>();
		headerParams.put(SecurityConstants.HEADER_STRING, SecurityConstants.TOKEN_PREFIX + httpHeaders.getJwtToken());
		headerParams.put("Content-Type", "application/json");
		headerParams.put(HttpUtils.SYNC_HEADER, "true");
		String url = getCurrentServiceUrlFor(ipAddress);
		HttpUtils.HttpUtilsBuilder httpUtilsBuilder =
				HttpUtils.builder().urlString(url).headerParams(headerParams).requestMethod(requestMethod);

		if (isRequestBodySupported(requestMethod)) {
			httpUtilsBuilder.postData(postData);
		}

		httpUtilsBuilder.build().getResponse(BaseResponse.class);
	}

	private boolean isRequestBodySupported(String requestMethod) {
		if (StringUtils.isBlank(requestMethod)) {
			return true;
		}
		return HTTP_METHODS_SUPPORTING_POSTDATA.contains(requestMethod);
	}

	private String getCurrentServiceUrlFor(String ipAddress) {
		String endPoint = SystemContextHolder.getContextData().getRequestContextHolder().getRequestURI();
		if (!endPoint.startsWith("/"))
			endPoint = "/".concat(endPoint);
		return new StringBuilder().append("http://").append(ipAddress).append(":").append(serverPort).append(endPoint)
				.toString();
	}

}
