package com.tgs.services.base.utils;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.datamodel.DeliveryInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.HotelCancellationPolicy;
import com.tgs.services.hms.datamodel.HotelConfiguratorInfo;
import com.tgs.services.hms.datamodel.HotelFareComponent;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.Option;
import com.tgs.services.hms.datamodel.PenaltyDetails;
import com.tgs.services.hms.datamodel.PriceInfo;
import com.tgs.services.hms.datamodel.RoomInfo;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelClientFeeOutput;
import com.tgs.services.hms.datamodel.hotelconfigurator.HotelConfiguratorRuleType;
import com.tgs.services.oms.datamodel.hotel.RoomTravellerInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BaseHotelUtils {

	private static HMSCommunicator hmsComm;

	private static GeneralServiceCommunicator gmsComm;

	public static void init(HMSCommunicator comm, GeneralServiceCommunicator gmsComm) {
		BaseHotelUtils.hmsComm = comm;
		BaseHotelUtils.gmsComm = gmsComm;
	}

	public static Map<String, Integer> generateTravellerKeyWithTTL(HotelInfo hInfo, ContextData contextData,
			DeliveryInfo deliveryInfo, List<RoomTravellerInfo> roomTravellerInfo, User bookingUser) {
		Map<String, Integer> keysWithTtl = new HashMap<>();
		String key = generateTravellerKey(hInfo, contextData, deliveryInfo, roomTravellerInfo, bookingUser);
		keysWithTtl.put(key,
				(int) LocalDateTime.now().until(
						hInfo.getOptions().get(0).getRoomInfos().get(0).getCheckOutDate().atTime(12, 00),
						ChronoUnit.SECONDS));
		log.info("Keys generated for {} hotel and {} travellers are {}", hInfo.getName(), keysWithTtl.keySet());
		return keysWithTtl;
	}

	public static String generateTravellerKey(HotelInfo hInfo, ContextData contextData, DeliveryInfo deliveryInfo,
			List<RoomTravellerInfo> roomTravellerInfos, User bookingUser) {
		if (hInfo == null)
			return null;

		StringBuilder stringBuilder = new StringBuilder(bookingUser.getUserId());
		RoomInfo roomInfo = hInfo.getOptions().get(0).getRoomInfos().get(0);
		DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		String contacts = "";
		String emails = "";
		if (deliveryInfo != null) {
			if (CollectionUtils.isNotEmpty(deliveryInfo.getContacts()))
				contacts = deliveryInfo.getContacts().stream().collect(Collectors.joining("-"));
			if (CollectionUtils.isNotEmpty(deliveryInfo.getEmails()))
				emails = deliveryInfo.getEmails().stream().collect(Collectors.joining("-"));
		}
		String roomTravellers = "";
		if (CollectionUtils.isNotEmpty(roomTravellerInfos)) {
			roomTravellers = StringUtils.lowerCase(roomTravellerInfos.get(0).getTravellerInfo().get(0).getFirstName());
		}
		return stringBuilder.append(hInfo.getName().replaceAll("[^a-zA-Z]", "").toLowerCase())
				.append(roomInfo.getCheckInDate().format(dateTimeFormatter))
				.append(roomInfo.getCheckOutDate().format(dateTimeFormatter)).append(contextData.getUser().getUserId())
				.append(contacts).append(roomTravellers).append(emails).toString();
	}

	public static double totalFareComponentsAmount(Map<HotelFareComponent, Double> fareComponents,
			Set<HotelFareComponent> gstFareComponents) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (CollectionUtils.isNotEmpty(gstFareComponents) && !fareComponents.isEmpty()) {
			gstFareComponents.forEach(fc -> totalAmount.addAndGet(fareComponents.getOrDefault(fc, 0.0)));
		}
		return totalAmount.doubleValue();
	}

	public static BigDecimal getTotalSupplierBookingAmount(Option option) {

		BigDecimal amount = BigDecimal.ZERO;
		List<RoomInfo> roomInfoList = option.getRoomInfos();
		for (RoomInfo roomInfo : roomInfoList) {
			BigDecimal roomAmount =
					BigDecimal.valueOf(roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
							+ roomInfo.getTotalFareComponents().getOrDefault(HotelFareComponent.SP, 0.0));
			amount = amount.add(roomAmount);
		}
		amount = amount.setScale(2, BigDecimal.ROUND_HALF_EVEN);
		return amount;
	}

	public static void updateManagementFee(RoomInfo roomInfo) {
		HotelConfiguratorInfo configInfo = hmsComm.getHotelConfigRule(HotelConfiguratorRuleType.CLIENTFEE, null);

		if (configInfo == null) {
			log.info("Client Fee Can't be updated due to empty rule");
			return;
		}
		HotelClientFeeOutput clientFeeOutput = (HotelClientFeeOutput) configInfo.getOutput();
		if (clientFeeOutput != null) {
			roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
				priceInfo.getFareComponents().put(HotelFareComponent.MF, clientFeeOutput.getManagementFee());
				priceInfo.getFareComponents().put(HotelFareComponent.MFT,
						clientFeeOutput.getManagementFee() * clientFeeOutput.getManagementFeeTax());
			});
			BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
		}
	}

	/**
	 * expression which given for Commission type will be evaluated
	 */
	public static Double evaluateHotelExpression(String commExpression, PriceInfo priceInfo) {
		commExpression = commExpression.replaceAll("\\s+", "");
		ExpressionParser expressionParser = new SpelExpressionParser();
		String commercialComponent = getHotelFareComponentExpression(commExpression, priceInfo);
		Expression expression = expressionParser.parseExpression(commercialComponent);
		double value = expression.getValue(Double.class);
		if (commExpression.matches(".*[a-zA-Z]+.*"))
			value = value / 100;
		return value;
	}

	public static String getHotelFareComponentExpression(String expression, PriceInfo priceInfo) {
		for (HotelFareComponent fareComponent : HotelFareComponent.values()) {
			Double amount = priceInfo.getFareComponents().getOrDefault(fareComponent, 0.0);
			expression =
					expression.replaceAll("%\\*\\b" + fareComponent.name() + "\\b", "*" + String.valueOf(amount / 100));
			expression = expression.replaceAll("\\b" + fareComponent.name() + "\\b", String.valueOf(amount));
		}
		return expression;
	}

	public static void updateCancellationPolicyFromDeadlineDatetimeInOption(Option option,
			HotelSearchQuery searchQuery) {

		LocalDateTime deadlineDateTime = option.getDeadlineDateTime();
		if (deadlineDateTime == null)
			return;
		Double totalPrice = getTotalSupplierBookingAmount(option).doubleValue();

		LocalDateTime fromDate = LocalDateTime.now();
		List<PenaltyDetails> pds = new ArrayList<>();
		if (deadlineDateTime.toLocalDate().isAfter(LocalDate.now())) {
			PenaltyDetails pd1 = PenaltyDetails.builder().fromDate(LocalDateTime.now()).toDate(deadlineDateTime)
					.penaltyAmount(0.0).build();
			pds.add(pd1);
			fromDate = deadlineDateTime;
		}
		PenaltyDetails pd2 = PenaltyDetails.builder().fromDate(fromDate)
				.toDate(searchQuery.getCheckinDate().atTime(LocalTime.NOON)).penaltyAmount(totalPrice).build();
		pds.add(pd2);
		HotelCancellationPolicy cp = option.getCancellationPolicy();
		if (cp == null)
			cp = HotelCancellationPolicy.builder().build();
		cp.setPenalyDetails(pds);
	}


	public static boolean isPerNightSupplierPricePresent(RoomInfo roomInfo) {

		List<PriceInfo> priceInfoList = roomInfo.getPerNightPriceInfos();
		if (CollectionUtils.isNotEmpty(priceInfoList)) {
			PriceInfo priceInfo = priceInfoList.get(0);
			return priceInfo.getFareComponents().containsKey(HotelFareComponent.BF) 
					|| priceInfo.getFareComponents().containsKey(HotelFareComponent.SP);
		}
		return false;
	}


	public static boolean isPerNightManagementFeePresent(RoomInfo roomInfo) {
		
		List<PriceInfo> priceInfoList = roomInfo.getPerNightPriceInfos();
		if (CollectionUtils.isNotEmpty(priceInfoList)) {
			PriceInfo priceInfo = priceInfoList.get(0);
			if(priceInfo.getAddlFareComponents().containsKey(HotelFareComponent.TAF)) {
				Map<HotelFareComponent, Double> afcs = priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
				return afcs.containsKey(HotelFareComponent.MF);
			}
		}
		return false;
	}


	public static void updateManagementFeeTax(RoomInfo roomInfo) {

		HotelConfiguratorInfo configuratorInfo = hmsComm.getHotelConfigRule(HotelConfiguratorRuleType.CLIENTFEE, null);

		if (configuratorInfo == null) {
			log.info("Client Fee Can't be updated due to empty rule");
			return;
		}

		HotelClientFeeOutput clientFeeOutput = (HotelClientFeeOutput) configuratorInfo.getOutput();
		if (clientFeeOutput != null) {
			roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
				Map<HotelFareComponent, Double> afcs = priceInfo.getAddlFareComponents().get(HotelFareComponent.TAF);
				if(afcs != null) {
					Double mf = afcs.get(HotelFareComponent.MF);
					afcs.put(HotelFareComponent.MFT, mf * clientFeeOutput.getManagementFeeTax());
				}
			});
			BaseHotelUtils.updateRoomTotalFareComponents(roomInfo);
		}
	}

	public static void updateTotalFareComponents(HotelInfo hotelInfo) {

		hotelInfo.getOptions().forEach(option -> {
			double totalPrice = 0.0;
			for (RoomInfo roomInfo : option.getRoomInfos()) {
				updateRoomTotalFareComponents(roomInfo);
				totalPrice += roomInfo.getTotalPrice();
			}
			option.setTotalPrice(totalPrice);
		});
	}

	public static void updateTotalFareComponents(Option option) {

		double totalPrice = 0.0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			updateRoomTotalFareComponents(roomInfo);
			totalPrice += roomInfo.getTotalPrice();
		}
		option.setTotalPrice(totalPrice);
	}

	public static void updateRoomTotalFareComponents(RoomInfo roomInfo) {
		updatePerNightTotalFareComponents(roomInfo);
		Map<HotelFareComponent, Double> fareComponents = new HashMap<>();
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareParentComponents = new HashMap<>();

		roomInfo.getPerNightPriceInfos().forEach(priceInfo -> {
			Map<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponentsMap =
					priceInfo.getAddlFareComponents();

			additionalFareComponentsMap.forEach((addlFareParentComponent, addlFareComponentMap) -> {
				addlFareComponentMap.forEach((addlFareComponent, value) -> {
					addlFareParentComponents.computeIfAbsent(addlFareParentComponent, val -> new HashMap<>()).put(
							addlFareComponent,
							addlFareParentComponents.get(addlFareParentComponent).getOrDefault(addlFareComponent, 0.0)
									+ addlFareComponentMap.getOrDefault(addlFareComponent, 0.0));
				});
			});
			Iterator<Entry<HotelFareComponent, Double>> fareComponentIter =
					priceInfo.getFareComponents().entrySet().iterator();

			while (fareComponentIter.hasNext()) {
				HotelFareComponent fareComponent = fareComponentIter.next().getKey();
				fareComponents.put(fareComponent, fareComponents.getOrDefault(fareComponent, 0.0)
						+ priceInfo.getFareComponents().get(fareComponent));
			}
		});
		roomInfo.setTotalPrice(fareComponents.get(HotelFareComponent.TF));
		roomInfo.setTotalFareComponents(fareComponents);
		roomInfo.setTotalAddlFareComponents(addlFareParentComponents);
	}

	public static void updatePerNightTotalFareComponents(RoomInfo roomInfo) {
		mapAddlFareComponents(roomInfo);
		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			Map<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponentsMap =
					priceInfo.getAddlFareComponents();
			final double totalPrice[] = new double[1];
			final double totalMarkup[] = new double[1];
			final double taxAndAddlFees[] = new double[1];

			additionalFareComponentsMap.forEach((addlFareParentComponent, addlFareChildComponent) -> {
				addlFareChildComponent.forEach((fareComponent, value) -> {

					HotelFareComponent mapComponent = fareComponent.mapComponent();

					if (fareComponent.equals(HotelFareComponent.MU)) {
						totalMarkup[0] +=
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent);
					}
					if (fareComponent.keepCurrentComponent() && fareComponent.isComputationRequired()) {
						taxAndAddlFees[0] +=
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent);
						totalPrice[0] +=
								fareComponent.getAmount(priceInfo.getAddlFareComponents().get(addlFareParentComponent)
										.get(fareComponent));
					}
					if (priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent) != null
							&& mapComponent != null) {
						double fareComponentValue = fareComponent.getAmount(
								priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
						double mapComponentValue = priceInfo.getFareComponents().getOrDefault(mapComponent, 0.0);
						priceInfo.getFareComponents().put(mapComponent, mapComponentValue + fareComponentValue);
					}
				});
			});

			if (!ObjectUtils.isEmpty(priceInfo.getFareComponents().get(HotelFareComponent.CMU))) {
				if (ObjectUtils.isEmpty(priceInfo.getMiscInfo().getSupplierPrice())) {
					priceInfo.getMiscInfo()
							.setSupplierPrice((priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
									+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.SP, 0.0)));
				}
				priceInfo.getFareComponents().put(HotelFareComponent.BF, priceInfo.getMiscInfo().getSupplierPrice()
						+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.CMU, 0.0));
			}

			Double totalFare = totalPrice[0] + (priceInfo.getFareComponents().getOrDefault(HotelFareComponent.BF, 0.0)
					+ priceInfo.getFareComponents().getOrDefault(HotelFareComponent.SP, 0.0));
			priceInfo.getFareComponents().put(HotelFareComponent.TAF, taxAndAddlFees[0]);
			priceInfo.getFareComponents().put(HotelFareComponent.TF, totalFare);
			priceInfo.getFareComponents().put(HotelFareComponent.NF, totalFare - totalMarkup[0]);
		}
	}

	private static void mapAddlFareComponents(RoomInfo roomInfo) {

		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			// Map<HotelFareComponent, Map<HotelFareComponent, Double>> filteredAddlParentFareComponents = new
			// HashMap<>();
			Map<HotelFareComponent, Double> fareComponentsMap = priceInfo.getFareComponents();
			Set<HotelFareComponent> mapComponentKeys = new HashSet<>();

			// Map all the fare components into their respective additional fare components
			// key.
	
			for (Map.Entry<HotelFareComponent, Double> fareComponents : fareComponentsMap.entrySet()) {
				HotelFareComponent fareComponent = fareComponents.getKey();
				HotelFareComponent mapComponent = fareComponent.mapComponent();

				if (!ObjectUtils.isEmpty(mapComponent)) {
					if (MapUtils.isNotEmpty(priceInfo.getAddlFareComponents().get(mapComponent))) {
						priceInfo.getAddlFareComponents().get(mapComponent).put(fareComponent,
								fareComponents.getValue());
					} else {
						Map<HotelFareComponent, Double> map = new HashMap<>();
						map.put(fareComponent, fareComponents.getValue());
						priceInfo.getAddlFareComponents().put(mapComponent, map);
					}
					mapComponentKeys.add(fareComponent);
				}
			}

			for (HotelFareComponent fareComponent : mapComponentKeys) {
				priceInfo.getFareComponents().remove(fareComponent);
			}

			// // Filter additional fare component based on keep current element.
			// Map<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponentsMap =
			// priceInfo.getAddlFareComponents();
			// for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> additionalFareComponents :
			// additionalFareComponentsMap
			// .entrySet()) {
			// Map<HotelFareComponent, Double> filteredAddlFareComponents = new HashMap<>();
			// HotelFareComponent addlFareParentComponent = additionalFareComponents.getKey();
			// Iterator<Entry<HotelFareComponent, Double>> addlFareComponentIter =
			// additionalFareComponents.getValue().entrySet().iterator();
			// while (addlFareComponentIter.hasNext()) {
			// HotelFareComponent fareComponent = addlFareComponentIter.next().getKey();
			// if (!fareComponent.keepCurrentComponent())
			// filteredAddlFareComponents.put(HotelFareComponent.OT,
			// priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
			// filteredAddlFareComponents.put(fareComponent,
			// priceInfo.getAddlFareComponents().get(addlFareParentComponent).get(fareComponent));
			// }
			// filteredAddlParentFareComponents.put(addlFareParentComponent, filteredAddlFareComponents);
			// priceInfo.setAddlFareComponents(filteredAddlParentFareComponents);
			// }
		}
	}

	@Deprecated
	public static void updateTotalFareComponents(RoomInfo roomInfo) {
		Map<HotelFareComponent, Double> totalFareComponents = new HashMap<>();
		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			for (Map.Entry<HotelFareComponent, Double> dayWiseFareComponent : priceInfo.getFareComponents()
					.entrySet()) {
				if (dayWiseFareComponent.getValue() == null)
					continue;
				totalFareComponents.put(dayWiseFareComponent.getKey(),
						totalFareComponents.getOrDefault(dayWiseFareComponent.getKey(), 0.0)
								+ dayWiseFareComponent.getValue());
			}
		}
		Double totalPrice = 0.0;
		for (HotelFareComponent hotelFareComponent : HotelFareComponent.getTotalFareComponents()) {
			totalPrice += totalFareComponents.getOrDefault(hotelFareComponent, 0d);
		}
		roomInfo.setTotalPrice(totalPrice);
		totalFareComponents.put(HotelFareComponent.TF, roomInfo.getTotalPrice());
		roomInfo.setTotalFareComponents(totalFareComponents);
	}

	public static AirType getTripType(HotelInfo hotelInfo) {
		ClientGeneralInfo generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (hotelInfo.isDomesticTrip(generalInfo.getCountry())) {
			return AirType.DOMESTIC;
		} else if (!hotelInfo.isDomesticTrip(generalInfo.getCountry())) {
			return AirType.INTERNATIONAL;
		}
		return AirType.ALL;
	}

	public static AirType getTripType(HotelSearchQuery searchQuery) {
		ClientGeneralInfo generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (searchQuery.getSearchCriteria().isDomesticTrip(generalInfo.getCountry())) {
			return AirType.DOMESTIC;
		} else if (!searchQuery.getSearchCriteria().isDomesticTrip(generalInfo.getCountry())) {
			return AirType.INTERNATIONAL;
		}
		return AirType.ALL;
	}

	public static void flattenFareComponents(RoomInfo roomInfo) {
		Map<HotelFareComponent, Double> fareComponent = roomInfo.getTotalFareComponents();
		fareComponent.remove(HotelFareComponent.TAF);
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponents =
				roomInfo.getTotalAddlFareComponents();

		for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponent : addlFareComponents
				.entrySet()) {
			fareComponent.putAll(addlFareComponent.getValue());
		}
		roomInfo.setTotalAddlFareComponents(null);

		for (PriceInfo priceInfo : roomInfo.getPerNightPriceInfos()) {
			flattenFareComponents(priceInfo);
		}
	}

	private static void flattenFareComponents(PriceInfo priceInfo) {
		Map<HotelFareComponent, Double> fareComponent = priceInfo.getFareComponents();
		fareComponent.remove(HotelFareComponent.TAF);
		Map<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponents = priceInfo.getAddlFareComponents();

		for (Map.Entry<HotelFareComponent, Map<HotelFareComponent, Double>> addlFareComponent : addlFareComponents
				.entrySet()) {
			fareComponent.putAll(addlFareComponent.getValue());
		}
		priceInfo.setAddlFareComponents(null);
	}

	public static double getTotalOptionValue(Option option, double perRooomNight) {

		double totalOptionValue = 0.0;
		int roomNights = option.getRoomInfos().get(0).getPerNightPriceInfos().size();
		int roomCount = option.getRoomInfos().size();
		totalOptionValue = perRooomNight * (roomNights * roomCount);
		return totalOptionValue;
	}

	public static double getPerRoomNightFareComponent(Option option, double totalAmount) {

		double perRoomNight = 0.0;
		int roomNights = option.getRoomInfos().get(0).getPerNightPriceInfos().size();
		int roomCount = option.getRoomInfos().size();
		perRoomNight = totalAmount / (roomNights * roomCount);
		return perRoomNight;
	}
	
	public static String getSupplierBinName(String supplierName) {
		/**
		 * Due to the limitation of aerospike binName
		 */

		if (supplierName.length() > 9) {
			supplierName = supplierName.substring(0, 9);
		}
		return "S_" + supplierName + "_ID";
	}

	public static String getSupplierSetName(String supplierName) {
		return "S_" + supplierName;
	}
	
	public static String getCurrency() {
		return "INR";
	}

	public static int getTotalRoomNights(Option option) {

		int totalRoomNights = 0;
		for (RoomInfo roomInfo : option.getRoomInfos()) {
			totalRoomNights += roomInfo.getPerNightPriceInfos().size();
		}
		return totalRoomNights;
	}
}
