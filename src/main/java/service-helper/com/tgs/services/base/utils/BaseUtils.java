package com.tgs.services.base.utils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorInfo;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.expression.EvaluationException;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.ParseException;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.stereotype.Service;
import com.google.common.util.concurrent.AtomicDouble;
import com.tgs.services.base.ApiResponseFlow;
import com.tgs.services.base.CheckPointData;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.OrderServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AirportInfo;
import com.tgs.services.base.enums.AirType;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.base.enums.PaxType;
import com.tgs.services.base.enums.SearchType;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.fms.datamodel.AirSearchModifiers;
import com.tgs.services.fms.datamodel.AirSearchQuery;
import com.tgs.services.fms.datamodel.FareDetail;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.PriceInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import com.tgs.services.fms.ruleengine.FlightBasicFact;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.hms.datamodel.CrossSellParameter;
import com.tgs.services.hms.datamodel.HotelSearchCriteria;
import com.tgs.services.hms.datamodel.HotelSearchPreferences;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import com.tgs.services.hms.datamodel.RoomSearchInfo;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;
import javax.annotation.PostConstruct;

@Slf4j
@Service
public class BaseUtils {

	/**
	 * This won't be static. All tax rates for mentioned components are handled here. DO NOT DELETE IT WITHOUT PROPER
	 * TESTING. Contact Ashu Sir for any advise on this.
	 */
	private final static Map<FareComponent, Double> taxRates;

	static {
		taxRates = new HashMap<>();
		taxRates.put(FareComponent.TDS, 5.0);
		taxRates.put(FareComponent.PMTDS, 5.0);
		taxRates.put(FareComponent.PCTDS, 5.0);
		taxRates.put(FareComponent.MFT, 18.0);
		taxRates.put(FareComponent.CCFT, 18.0);
		taxRates.put(FareComponent.ACFT, 5.0);
		taxRates.put(FareComponent.ARFT, 5.0);
		taxRates.put(FareComponent.CRFT, 18.0);
		taxRates.put(FareComponent.CAFT, 18.0);
	}

	private static GeneralServiceCommunicator gmsComm;

	private static FMSCommunicator fmsComm;

	private static OrderServiceCommunicator omsComm;

	private static UserServiceCommunicator umsComm;

	public static void init(GeneralServiceCommunicator generalServiceCommunicator, FMSCommunicator fmsCommunicator,
			OrderServiceCommunicator omsComm, UserServiceCommunicator umsComm) {
		BaseUtils.gmsComm = generalServiceCommunicator;
		BaseUtils.fmsComm = fmsCommunicator;
		BaseUtils.omsComm = omsComm;
		BaseUtils.umsComm = umsComm;
		initTaxRateComponent();
	}


	public static void initTaxRateComponent() {
		ConfiguratorInfo generalInfo = gmsComm.getConfiguratorInfo(ConfiguratorRuleType.CLIENTINFO);
		ClientGeneralInfo clientInfo = (ClientGeneralInfo) generalInfo.getOutput();
		if (clientInfo != null && MapUtils.isNotEmpty(clientInfo.getTaxRates())) {
			for (FareComponent key : clientInfo.getTaxRates().keySet()) {
				taxRates.put(key, clientInfo.getTaxRates().get(key));
			}
		}
	}

	// all taxes and tds are user-dependent
	public static Map<FareComponent, Double> getTaxRates(User user) {
		double tdsRate = 0;
		tdsRate = user != null && user.getAdditionalInfo() != null
				? ObjectUtils.defaultIfNull(user.getAdditionalInfo().getTdsRate(), 0.0)
				: 0d;
		taxRates.put(FareComponent.TDS, tdsRate);
		return taxRates;
	}

	public static double calculateDependentFareComponent(FareComponent dependentFareComponent, double amount,
			User user) {
		Double taxRate = getTaxRates(user).get(dependentFareComponent);
		// i.e., if it is a tax component
		if (taxRate != null) {
			return (amount * taxRate) / 100;
		}
		return dependentFareComponent.getAmount(amount);
	}

	public static void updateFareComponent(Map<FareComponent, Double> fareComponents, FareComponent fareComponent,
			Double amount, User user) {
		if (fareComponent == null || fareComponents == null || amount == null) {
			return;
		}

		fareComponents.put(fareComponent, fareComponents.getOrDefault(fareComponent, 0.0) + amount);
		if (fareComponent.isUpdateTotalFare()) {
			fareComponents.put(FareComponent.TF, fareComponents.getOrDefault(FareComponent.TF, 0.0) + amount);
		}

		updateDependentFareComponents(fareComponents, fareComponent, amount, user);
	}

	public static void updateFareComponent(Map<FareComponent, Double> fareComponents, FareComponent fareComponent,
			Double amount, User user, boolean isDependentToUpdate) {
		if (fareComponent == null || fareComponents == null || amount == null) {
			return;
		}

		fareComponents.put(fareComponent, fareComponents.getOrDefault(fareComponent, 0.0) + amount);
		if (fareComponent.isUpdateTotalFare()) {
			fareComponents.put(FareComponent.TF, fareComponents.getOrDefault(FareComponent.TF, 0.0) + amount);
		}
		if (isDependentToUpdate) {
			updateDependentFareComponents(fareComponents, fareComponent, amount, user);
		}
	}

	/**
	 * Update dependent components of {@code fareComponent}.
	 * 
	 * @param fareComponents
	 * @param fareComponent
	 * @param amount
	 * @param user
	 */
	public static void updateDependentFareComponents(Map<FareComponent, Double> fareComponents,
			FareComponent fareComponent, Double amount, User user) {
		for (FareComponent dependentComponent : fareComponent.dependentComponents()) {
			if (dependentComponent != null) {
				double dependentComponentAmount = calculateDependentFareComponent(dependentComponent, amount, user);
				fareComponents.put(dependentComponent,
						fareComponents.getOrDefault(dependentComponent, 0.0) + dependentComponentAmount);
				if (dependentComponent.isUpdateTotalFare()) {
					fareComponents.put(FareComponent.TF,
							fareComponents.getOrDefault(FareComponent.TF, 0.0) + dependentComponentAmount);
				}
			}
		}
	}

	public static List<FlightTravellerInfo> getParticularPaxTravellerInfo(List<FlightTravellerInfo> travellerInfos,
			PaxType paxType) {
		List<FlightTravellerInfo> travellerInfo = new ArrayList<>();
		travellerInfos.forEach(traveler -> {
			if (traveler.getPaxType().equals(paxType)) {
				travellerInfo.add(traveler);
			}
		});
		return travellerInfo;
	}

	public static Map<PaxType, Integer> getPaxInfo(TripInfo tripInfo) {
		if (tripInfo.getPaxInfo() != null) {
			return tripInfo.getPaxInfo();
		}

		Map<PaxType, Integer> paxInfo = new HashMap<>();

		if (tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo() != null) {
			List<FlightTravellerInfo> travellerInfos =
					tripInfo.getSegmentInfos().get(0).getBookingRelatedInfo().getTravellerInfo();
			paxInfo = getPaxInfo(travellerInfos);
		}
		return paxInfo;
	}

	public static Map<PaxType, Integer> getPaxInfo(List<FlightTravellerInfo> travellerInfos) {
		Map<PaxType, Integer> paxInfo = new HashMap<>();

		if (CollectionUtils.isEmpty(travellerInfos)) {
			return paxInfo;
		}

		paxInfo.put(PaxType.ADULT, getParticularPaxTravellerInfo(travellerInfos, PaxType.ADULT).size());
		paxInfo.put(PaxType.CHILD, getParticularPaxTravellerInfo(travellerInfos, PaxType.CHILD).size());
		paxInfo.put(PaxType.INFANT, getParticularPaxTravellerInfo(travellerInfos, PaxType.INFANT).size());

		return paxInfo;
	}

	public static Map<String, Integer> generatePaxTravelKeysWithTTL(List<TripInfo> tripInfos,
			List<FlightTravellerInfo> travellerInfos, User bookingUser) {
		log.info("Generating keys for {} trips and {} paxes", tripInfos.size(), travellerInfos.size());
		Map<String, Integer> keysWithTtl = new HashMap<>();
		for (TripInfo tripInfo : tripInfos) {
			for (TripInfo actualTrip : tripInfo.splitTripInfo(true)) {
				for (SegmentInfo segmentInfo : actualTrip.getSegmentInfos()) {
					for (FlightTravellerInfo travellerInfo : travellerInfos) {
						String key = generatePaxTravelKey(segmentInfo, travellerInfo, bookingUser);
						if (key != null)
							keysWithTtl.put(key, (int) LocalDateTime.now()
									.until(actualTrip.getDepartureTime().plusDays(1), ChronoUnit.SECONDS));
					}
				}
			}
		}
		log.info("Keys generated are {}", keysWithTtl.keySet());
		return keysWithTtl;
	}

	/**
	 * Generate key using airline code, flightNumber, departure airport, arrival airport and departure time of segment,
	 * and first name, last name and DOB (if provided) of traveller.
	 *
	 * @param tripInfo
	 * @param travellerInfo
	 * @return
	 */
	public static String generatePaxTravelKey(SegmentInfo segmentInfo, FlightTravellerInfo travellerInfo,
			User bookingUser) {
		if (segmentInfo == null || travellerInfo == null)
			return null;

		LocalDate dob = travellerInfo.getDob();
		String dobText = "";
		if (Objects.nonNull(dob)) {
			dobText = dob.toString();
		}

		StringBuilder stringBuilder = new StringBuilder(bookingUser.getUserId());
		return stringBuilder.append(segmentInfo.getAirlineCode(false)).append(segmentInfo.getFlightNumber())
				.append(segmentInfo.getDepartureAirportCode()).append(segmentInfo.getArrivalAirportCode())
				.append(segmentInfo.getDepartTime().toLocalDate())
				.append(StringUtils.lowerCase(travellerInfo.getFirstName()))
				.append(StringUtils.lowerCase(travellerInfo.getLastName())).append(dobText).toString();
	}

	public static List<PriceInfo> getPriceInfos(List<TripInfo> tripInfos) {
		List<PriceInfo> priceInfos = new ArrayList<>();
		for (TripInfo tripInfo : tripInfos) {
			priceInfos.add(tripInfo.getTripPriceInfos().get(0));
		}
		return priceInfos;
	}

	public static List<AirSearchQuery> getSearchQueryFromTripInfos(List<TripInfo> tripInfos) {
		List<AirSearchQuery> searchQueries = new ArrayList<>();
		tripInfos.forEach(tripInfo -> {
			AirSearchQuery sQuery = getSearchQueryFromTripInfo(tripInfo);
			searchQueries.add(sQuery);
		});
		return searchQueries;
	}

	// This requires bookingRelatedInfo in TripInfo to fill PaxInfo in search Query
	public static AirSearchQuery getSearchQueryFromTripInfo(TripInfo tripInfo) {
		List<TripInfo> tripInfos = tripInfo.splitTripInfo(false);
		if (tripInfos.size() > 1) {
			List<AirSearchQuery> searchQueries = new ArrayList<>();
			tripInfos.forEach(trip -> {
				searchQueries.add(getSearchQueryFromTripInfo(trip));
			});
			return combineSearchQuery(searchQueries);
		} else {
			AirSearchQuery searchQuery = new AirSearchQuery();
			searchQuery.setRouteInfos(Arrays.asList(tripInfo.getRouteInfo()));
			searchQuery.setCabinClass(tripInfo.getCabinClass());
			if (tripInfo.isPriceInfosNotEmpty()) {
				searchQuery.setSourceIds(Arrays.asList(tripInfo.getSupplierInfo().getSourceId()));
			}
			searchQuery.setPaxInfo(getPaxInfo(tripInfo));
			AirSearchModifiers searchModifiers = new AirSearchModifiers();
			searchModifiers.setSourceId(tripInfo.getSupplierInfo().getSourceId());
			searchQuery.setSearchModifiers(searchModifiers);
			ClientGeneralInfo clientInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
			searchQuery.populateMissingParametersInAirSearchQuery(clientInfo.getCountry(),
					clientInfo.getClientDepentCountriesAir());
			return searchQuery;
		}
	}

	public static AirSearchQuery combineSearchQuery(List<AirSearchQuery> searchQueries) {
		/**
		 * Only in case domestic multiCity we will have separate Queries and will have to combine into Route List.
		 * Otherwise it will be single query.<br>
		 * Also, searchQueries need to be sorted according to their route's travel date, so they can be used for
		 * ordering tripsInfos wherever needed.
		 */
		AirSearchQuery combineQuery = null;
		if ((searchQueries.get(0).isMultiCity() && searchQueries.get(0).getIsDomestic())
				|| (searchQueries.get(0).getRouteInfos().size() == 1)) {
			searchQueries.sort(new Comparator<AirSearchQuery>() {
				@Override
				public int compare(AirSearchQuery o1, AirSearchQuery o2) {
					return o1.getRouteInfos().get(0).getTravelDate()
							.compareTo(o2.getRouteInfos().get(0).getTravelDate());
				}
			});
			combineQuery = new GsonMapper<>(searchQueries.get(0), AirSearchQuery.class).convert();
			combineQuery.setRouteInfos(new ArrayList<>());
			for (int i = 0; i < searchQueries.size(); i++) {
				combineQuery.getRouteInfos().add(searchQueries.get(i).getRouteInfos().get(0));
			}
		} else {
			combineQuery = new GsonMapper<>(searchQueries.get(0), AirSearchQuery.class).convert();
			return combineQuery;
		}
		ClientGeneralInfo generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		combineQuery.populateMissingParametersInAirSearchQuery(generalInfo.getCountry(),
				generalInfo.getClientDepentCountriesAir());
		return combineQuery;
	}

	/**
	 * Counting number of trips with pricing, i.e., trips having any one segment with pricing (FareDetail with non-zero
	 * base-fare for Adult).
	 *
	 * @param tripInfos
	 * @return Number of trips with pricing
	 */
	public static int countTripsWithPricing(List<TripInfo> tripInfos) {
		int count = 0;
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (segmentInfo.hasPricing()) {
					count++;
					break;
				}
			}
		}
		return count;
	}

	public static double totalFareComponentsAmount(FareDetail fd, Set<FareComponent> fareComponents) {
		AtomicDouble totalAmount = new AtomicDouble(0);
		if (CollectionUtils.isNotEmpty(fareComponents) && fd.getFareComponents() != null) {
			fareComponents.forEach(fc -> totalAmount.addAndGet(fd.getFareComponents().getOrDefault(fc, 0.0)));
		}
		return totalAmount.doubleValue();
	}

	public static AirType getTripType(TripInfo tripInfo) {
		return getTripType(tripInfo, false);
	}

	public static AirType getTripType(TripInfo tripInfo, boolean considerSOTO) {
		// ClientGeneralInfo generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		String country = "India";
		if (tripInfo.isDomesticTrip(country)) {
			return AirType.DOMESTIC;
		} else if (!tripInfo.isDomesticTrip(country)) {
			if (considerSOTO
					&& !tripInfo.getSegmentInfos().get(0).getDepartAirportInfo().getCountry().equals(country)) {
				return AirType.SOTO;
			}
			return AirType.INTERNATIONAL;
		}
		return AirType.ALL;
	}

	public static boolean isDomesticTrip(TripInfo tripInfo) {
		ClientGeneralInfo generalInfo = gmsComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		return tripInfo.isDomesticTrip(generalInfo.getCountry());
	}

	// To compare segments (considering depart and arrival time not date)
	public static boolean matchSegmentInfos(List<SegmentInfo> list1, List<SegmentInfo> list2) {
		boolean isDuplicateSegment = true;
		if (list1.size() != list2.size()) {
			return false;
		}
		for (int index = 0; index < list1.size(); index++) {
			SegmentInfo segment1 = list1.get(index);
			SegmentInfo segment2 = list2.get(index);
			isDuplicateSegment =
					isDuplicateSegment && segment1.getDepartureAirportCode().equals(segment2.getDepartureAirportCode())
							&& segment1.getArrivalAirportCode().equals(segment2.getArrivalAirportCode())
							&& segment1.getAirlineCode(false).equals(segment2.getAirlineCode(false))
							&& segment1.getDeparture().equals(segment2.getDeparture())
							&& segment1.getArrival().equals(segment2.getArrival())
							&& segment1.getFlightNumber().equals(segment2.getFlightNumber());
		}
		return isDuplicateSegment;
	}

	public static boolean isUserShowPublicBookPrivate(User user) {
		return user == null || (UserUtils.getParentUserConf(user) == null
				|| BooleanUtils.isNotTrue(UserUtils.getParentUserConf(user).getAllowPrivateFare()));
	}

	public static String getCleanGstName(String registeredName) {
		if (StringUtils.isNotEmpty(registeredName)) {
			return registeredName.replaceAll("&", " and ").replaceAll(",", " ").replaceAll("-", " ")
					.replaceAll("\\.", " ").replaceAll("  ", " ");
		}
		return "";
	}

	public static LocalDateTime min(LocalDateTime a, LocalDateTime b) {
		if (a.isBefore(b))
			return a;
		else
			return b;
	}

	public static String getPaymentMediums(List<PaymentRequest> paymentRequests) {


		Set<String> paymentModes = new HashSet<>();
		if (CollectionUtils.isNotEmpty(paymentRequests)) {
			paymentRequests.forEach(paymentRequest -> {
				paymentModes.add(paymentRequest.getPaymentMedium().name());
			});
			return paymentModes.stream().distinct().collect(Collectors.toList()).toString();
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public static Set<ApiResponseFlow> calculateApiDiff(String type1, String type2) {
		Set<ApiResponseFlow> apiResponseFlowList = new HashSet<>();
		ContextData contextData = SystemContextHolder.getContextData();

		List<CheckPointData> checkPointsForType1 = contextData.getCheckPointInfo().get(type1);
		List<CheckPointData> checkPointsForType2 = contextData.getCheckPointInfo().get(type2);

		if (CollectionUtils.isEmpty(checkPointsForType1) || CollectionUtils.isEmpty(checkPointsForType2)) {
			return null;
		}

		if (checkPointsForType1.size() != checkPointsForType2.size()) {
			log.error("CheckPoints size are not equal for type1 {} , type2 {}, sizes are {},{} ", type1, type2,
					checkPointsForType1.size(), checkPointsForType2.size());
			return null;
		}

		List<CheckPointData> parallelCheckPointsForType1 =
				checkPointsForType1.stream().filter(checkPoints -> checkPoints.isParallel())
						.sorted(Comparator.comparingLong(CheckPointData::getTime)).collect(Collectors.toList());

		List<CheckPointData> parallelCheckPointsForType2 =
				checkPointsForType2.stream().filter(checkPoints -> checkPoints.isParallel())
						.sorted(Comparator.comparingLong(CheckPointData::getTime)).collect(Collectors.toList());

		List<CheckPointData> sequentialCheckPointsForType1 =
				(List<CheckPointData>) CollectionUtils.disjunction(checkPointsForType1, parallelCheckPointsForType1);
		sequentialCheckPointsForType1.sort(Comparator.comparingLong(CheckPointData::getTime));

		List<CheckPointData> sequentialCheckPointsForType2 =
				(List<CheckPointData>) CollectionUtils.disjunction(checkPointsForType2, parallelCheckPointsForType2);
		sequentialCheckPointsForType2.sort(Comparator.comparingLong(CheckPointData::getTime));

		if (CollectionUtils.isNotEmpty(parallelCheckPointsForType1)
				&& CollectionUtils.isNotEmpty(parallelCheckPointsForType2)
				&& (parallelCheckPointsForType1.size() == parallelCheckPointsForType2.size())) {
			Map<String, CheckPointData> minTimeCheckpoints =
					parallelCheckPointsForType1.stream().collect(Collectors.toMap(CheckPointData::getSubType,
							Function.identity(), BinaryOperator.minBy(Comparator.comparing(CheckPointData::getTime))));

			Map<String, CheckPointData> maxTimeCheckpoints =
					parallelCheckPointsForType2.stream().collect(Collectors.toMap(CheckPointData::getSubType,
							Function.identity(), BinaryOperator.maxBy(Comparator.comparing(CheckPointData::getTime))));

			for (Map.Entry<String, CheckPointData> entry : maxTimeCheckpoints.entrySet()) {
				CheckPointData checkPointData = entry.getValue();
				long apiTime = checkPointData.getTime() - minTimeCheckpoints.get(entry.getKey()).getTime();
				apiResponseFlowList.add(ApiResponseFlow.builder().type(checkPointData.getSubType()).apitime(apiTime)
						.startTime(minTimeCheckpoints.get(entry.getKey()).getTime()).build());
			}
		}

		if (CollectionUtils.isNotEmpty(sequentialCheckPointsForType1)
				&& CollectionUtils.isNotEmpty(sequentialCheckPointsForType2)
				&& (sequentialCheckPointsForType1.size() == sequentialCheckPointsForType2.size())) {
			for (int i = 0; i < sequentialCheckPointsForType1.size(); i++) {
				CheckPointData checkPointData = sequentialCheckPointsForType2.get(i);
				long apiTime =
						sequentialCheckPointsForType2.get(i).getTime() - sequentialCheckPointsForType1.get(i).getTime();
				apiResponseFlowList.add(ApiResponseFlow.builder().type(checkPointData.getSubType()).apitime(apiTime)
						.startTime(sequentialCheckPointsForType1.get(i).getTime()).build());
			}
		}
		return apiResponseFlowList;
	}

	public static String appendCrossSellInId(String id) {

		String searchId = String.join("", id + "_CS");
		return searchId;
	}

	public static HotelSearchQuery getHotelSearchQuery(List<TripInfo> tripInfoList, String bookingId) {
		SearchType searchType = fmsComm.getSearchTypeFromTrips(tripInfoList);
		TripInfo tripInfo = null;
		SegmentInfo segmentInfo = null;
		LocalDate checkinDate = null;
		LocalDate checkoutDate = null;
		AirportInfo airportInfo = null;
		String cabinClass = "";
		Map<PaxType, Integer> paxInfo = new HashMap<>();
		switch (searchType) {
			case ONEWAY:
				tripInfo = tripInfoList.get(0);
				segmentInfo = tripInfo.getSegmentInfos().get(tripInfo.getSegmentInfos().size() - 1);
				checkinDate = segmentInfo.getArrivalTime().toLocalDate();
				checkoutDate = checkinDate.plusDays(2);
				airportInfo = segmentInfo.getArrivalAirportInfo();
				cabinClass = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass().name();
				paxInfo = !MapUtils.isEmpty(tripInfo.getPaxInfo()) ? tripInfo.getPaxInfo() : getPaxInfo(tripInfo);
				break;
			case MULTICITY:
				tripInfo = tripInfoList.get(0);
				segmentInfo = tripInfo.getSegmentInfos().get(tripInfo.getSegmentInfos().size() - 1);
				checkinDate = segmentInfo.getArrivalTime().toLocalDate();
				checkoutDate = checkinDate.plusDays(2);
				airportInfo = segmentInfo.getArrivalAirportInfo();
				cabinClass = segmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass().name();
				paxInfo = !MapUtils.isEmpty(tripInfo.getPaxInfo()) ? tripInfo.getPaxInfo() : getPaxInfo(tripInfo);
				break;
			case RETURN:
				TripInfo firstTripInfo = tripInfoList.get(0);
				SegmentInfo firstSegmentInfo =
						firstTripInfo.getSegmentInfos().get(firstTripInfo.getSegmentInfos().size() - 1);

				TripInfo secondTripInfo = tripInfoList.get(1);
				SegmentInfo secondSegmentInfo =
						secondTripInfo.getSegmentInfos().get(secondTripInfo.getSegmentInfos().size() - 1);

				checkinDate = firstSegmentInfo.getArrivalTime().toLocalDate();
				checkoutDate = secondSegmentInfo.getDepartTime().toLocalDate();
				airportInfo = firstSegmentInfo.getArrivalAirportInfo();
				cabinClass = firstSegmentInfo.getPriceInfo(0).getFareDetail(PaxType.ADULT).getCabinClass().name();
				paxInfo = !MapUtils.isEmpty(firstTripInfo.getPaxInfo()) ? firstTripInfo.getPaxInfo()
						: getPaxInfo(firstTripInfo);
				break;
			default:
				log.error("Unable to create hotel search query due to invalid search type {} for booking id {}",
						bookingId);
				throw new CustomGeneralException(SystemError.INVALID_ID);
		}

		HotelSearchPreferences searchPreferences = new HotelSearchPreferences();
		CrossSellParameter crossSellParameter = new CrossSellParameter();
		crossSellParameter.setCabinClass(cabinClass);
		crossSellParameter.setIataCode(airportInfo.getCode());
		searchPreferences.setCrossSellParameter(crossSellParameter);
		List<RoomSearchInfo> roomInfoList = new ArrayList<>();
		RoomSearchInfo roomInfo = RoomSearchInfo.builder().numberOfAdults(paxInfo.get(PaxType.ADULT))
				.numberOfChild(paxInfo.get(PaxType.CHILD)).build();
		roomInfoList.add(roomInfo);
		HotelSearchCriteria searchCriteria = new HotelSearchCriteria();
		searchCriteria.setCityName(airportInfo.getCity());
		searchCriteria.setCountryName(airportInfo.getCountry());

		HotelSearchQuery searchQuery = HotelSearchQuery.builder().searchId(BaseUtils.appendCrossSellInId(bookingId))
				.checkinDate(checkinDate).checkoutDate(checkoutDate).searchPreferences(searchPreferences)
				.searchCriteria(searchCriteria).roomInfo(roomInfoList).build();
		return searchQuery;
	}

	public static boolean isRecalCommission(boolean isRecall, UserRole role) {
		return isRecall && !UserRole.corporate(role);
	}

	public static double getGrossCommission(TripInfo tripInfo, int priceIndex, User user,
			boolean includePartnerCommission) {
		double totalCommission = 0;
		if (CollectionUtils.isNotEmpty(tripInfo.getSegmentInfos())) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				if (CollectionUtils.isNotEmpty(segmentInfo.getPriceInfoList())
						&& segmentInfo.getPriceInfoList().size() >= priceIndex) {
					for (Entry<PaxType, FareDetail> entry : segmentInfo.getPriceInfo(priceIndex).getFareDetails()
							.entrySet()) {
						int paxCount = tripInfo.getPaxInfo().getOrDefault(entry.getKey(), 1);
						totalCommission += (paxCount * getGrossCommission(entry.getValue().getFareComponents(), user,
								includePartnerCommission));
					}
				}
			}
		}
		return totalCommission;
	}

	public static double getGrossCommission(Map<FareComponent, Double> fareComponents, User user,
			boolean includePartnerCommission) {
		return omsComm.getGrossCommission(fareComponents, user, includePartnerCommission);
	}

	public static double calculateAvailablePNRCredit(List<FlightTravellerInfo> travellerInfos) {
		double totalCreditBalance = 0;
		if (CollectionUtils.isNotEmpty(travellerInfos)) {
			for (FlightTravellerInfo traveller : travellerInfos) {
				totalCreditBalance += calculateAvailablePNRCredit(traveller.getFareDetail());
			}
		}
		return totalCreditBalance;
	}

	public static double calculateAvailablePNRCredit(FareDetail fd) {
		double totalCreditBalance = 0;
		if (fd != null && MapUtils.isNotEmpty(fd.getFareComponents())) {
			totalCreditBalance += fd.getFareComponents().getOrDefault(FareComponent.CS, 0.0);
		}
		return totalCreditBalance;
	}

	/**
	 * expression which given for Commission type will be evaluated
	 */
	public static Double evaluateExpression(String commExpression, FareDetail fareDetail, String type,
			boolean areCoefficientsPercentage) {
		commExpression = commExpression.replaceAll("\\s+", "");
		ExpressionParser expressionParser = new SpelExpressionParser();
		String commercialComponent = getFareComponentExpression(commExpression, fareDetail, areCoefficientsPercentage);
		Expression expression = expressionParser.parseExpression(commercialComponent);
		double value = expression.getValue(Double.class);
		if (areCoefficientsPercentage && commExpression.matches(".*[a-zA-Z]+.*"))
			value = value / 100;
		return value;
	}

	public static String getFareComponentExpression(String expression, FareDetail fareDetail,
			boolean areCoefficientsPercentage) {
		for (FareComponent fareComponent : FareComponent.values()) {
			Double amount = fareDetail.getFareComponents().getOrDefault(fareComponent, 0.0);
			expression =
					expression.replaceAll("%\\*\\b" + fareComponent.name() + "\\b", "*" + String.valueOf(amount / 100));
			expression = expression.replaceAll("\\b" + fareComponent.name() + "\\b", String.valueOf(amount));
		}
		return expression;
	}


	public static FlightBasicFact createFactOnUser(FlightBasicFact fact, User user) {
		if (fact == null) {
			fact = FlightBasicFact.builder().build();
		}
		User parentUser = getParentUser(user);
		if (parentUser != null) {
			fact.setRole(parentUser.getRole());
			if (UserUtils.isPartnerUser(parentUser)) {
				fact.setUserId(UserUtils.getPartnerUserId(parentUser));
			} else {
				fact.setUserId(parentUser.getUserId());
			}
		}
		return fact;
	}

	public static User getParentUser(User user) {
		User parentUser = null;
		if (user == null) {
			user = SystemContextHolder.getContextData().getUser();
		}
		if (user != null) {
			parentUser = umsComm.getUserFromCache(user.getParentUserId());
			if (parentUser == null) {
				printStackTrace(Thread.currentThread().getStackTrace(), "Parent User");
			}
		} else {
			printStackTrace(Thread.currentThread().getStackTrace(), "User");
		}
		return parentUser;
	}

	// Only for debugging.
	public static void printStackTrace(StackTraceElement[] stacks, String user) {
		StringJoiner joiner = new StringJoiner("\n");
		for (StackTraceElement stack : stacks) {
			joiner.add(stack.toString());
		}
		log.debug("{} Fact is Empty {}", user, joiner.toString());
	}

	/**
	 * @param commExpression expression to be validated
	 * @return {@code true} if expression is valid. {@code false}, otherwise.
	 */
	public static boolean validateExpression(String commExpression) {
		if (StringUtils.isBlank(commExpression)) {
			log.error("Couldn't parse \"{}\" because it is a blank expression", commExpression);
			return false;
		}
		commExpression = commExpression.replaceAll("\\s+", "");
		for (FareComponent fareComponent : FareComponent.values()) {
			commExpression = commExpression.replaceAll("%\\*", "*");
			commExpression = commExpression.replaceAll("\\b" + fareComponent.name() + "\\b", "1");
		}
		try {
			ExpressionParser expressionParser = new SpelExpressionParser();
			Expression expression = expressionParser.parseExpression(commExpression);
			expression.getValue(Double.class);
		} catch (ParseException | EvaluationException e) {
			log.error("Couldn't parse expression \"{}\" due to {}", commExpression, e);
			return false;
		}
		return true;
	}
}
