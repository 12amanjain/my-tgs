package com.tgs.services.base.utils;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.FareComponent;
import com.tgs.services.fms.datamodel.FlightTravellerInfo;
import com.tgs.services.fms.datamodel.SegmentInfo;
import com.tgs.services.fms.datamodel.TripInfo;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BookingUtils {

	/**
	 * @implSpec : This Method will return PNR is Valid or Not. Across on all GDS & Some Supplier will return 6 Digit
	 *           Reference Number
	 * @implNote :in AirArabia : 8 Digit Number,if any changes please confirm with Ashu Sir
	 */
	public static boolean isValidPnr(String pnr) {
		if (StringUtils.isBlank(pnr)) {
			// For inventory bookings,pnr can be empty.
			log.info("Invalid pnr {}", pnr);
			return false;
		}
		return Pattern.matches("\\w{5,8}", pnr);
	}

	public static void updateTicketNumber(List<SegmentInfo> segmentInfos, String ticketNumber,
			FlightTravellerInfo travellerInfo) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			for (FlightTravellerInfo tInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
				if (isSameTravellerInfo(travellerInfo, tInfo)) {
					tInfo.setTicketNumber(ticketNumber);
				}
			}
		}
	}

	public static void updateAirlinePnr(List<SegmentInfo> segmentInfos, String pnr) {
		segmentInfos.forEach(segmentInfo -> {
			segmentInfo.updateAirlinePnr(pnr);
		});
	}

	public static boolean isSameTravellerInfo(FlightTravellerInfo travller1, FlightTravellerInfo traveller2) {
		if (StringUtils.equalsIgnoreCase(travller1.getFirstName(), traveller2.getFirstName())
				&& StringUtils.equalsIgnoreCase(travller1.getLastName(), traveller2.getLastName())) {
			if (travller1.getDob() != null && travller1.getDob().isEqual(traveller2.getDob())) {
				return true;
			}
			return true;
		}
		return false;
	}

	public static void updateSupplierBookingId(List<SegmentInfo> segmentInfos, String crsPnr) {
		for (SegmentInfo segmentInfo : segmentInfos) {
			segmentInfo.getBookingRelatedInfo().getTravellerInfo().forEach(travellerInfo -> {
				travellerInfo.setSupplierBookingId(crsPnr);
			});
		}
	}

	public static void updateTicketNumber(List<TripInfo> tripInfos, String ticketNumber) {
		for (TripInfo tripInfo : tripInfos) {
			for (SegmentInfo segmentInfo : tripInfo.getSegmentInfos()) {
				for (FlightTravellerInfo tInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
					tInfo.setTicketNumber(ticketNumber);
				}
			}
		}
	}

	public static double getAmountChargedByAirline(List<SegmentInfo> segmentInfoList) {
		double totalAmount = 0;
		for (SegmentInfo segmentInfo : segmentInfoList) {
			for (FlightTravellerInfo travellerInfo : segmentInfo.getBookingRelatedInfo().getTravellerInfo()) {
				totalAmount += getAmountChargedByAirline(travellerInfo);
			}
		}
		return totalAmount;
	}

	public static double getAmountChargedByAirline(FlightTravellerInfo travellerInfo) {
		double totalAmount = 0;
		Set<FareComponent> fcs = getAirlineComponents();
		for (Map.Entry<FareComponent, Double> entry : travellerInfo.getFareDetail().getFareComponents().entrySet()) {
			if (fcs.contains(entry.getKey()))
				totalAmount += entry.getValue();
		}
		return totalAmount;
	}

	public static double getPaxTotalFare(FlightTravellerInfo travellerInfo) {
		double totalAmount = 0;
		Set<FareComponent> commissionComponents = FareComponent.getAllCommisionComponents();
		for (Map.Entry<FareComponent, Double> entry : travellerInfo.getFareDetail().getFareComponents().entrySet()) {
			if (commissionComponents.contains(entry.getKey()) || entry.getKey().equals(FareComponent.MU)
					|| entry.getKey().equals(FareComponent.TDS) || entry.getKey().equals(FareComponent.PMTDS))
				continue;
			totalAmount += entry.getValue();
		}
		return totalAmount;
	}

	private static Set<FareComponent> getAirlineComponents() {
		return Arrays.stream(FareComponent.values()).filter(FareComponent::airlineComponent)
				.collect(Collectors.toSet());
	}

	public static double isFareDiff(double amountCharged, double supplierAmount) {
		return Math.abs(amountCharged - supplierAmount);
	}

	/**
	 * This will return whether trips has ticketNumber or not
	 */
	public static boolean hasTicketNumberInAllTrip(List<TripInfo> trips) {
		// Default Consider Trip Has No Ticket Number
		AtomicBoolean hasTicketNumber = new AtomicBoolean();
		for (TripInfo tripInfo : trips) {
			boolean hasTktNumber = tripInfo.hasTicketNumber();
			if (!hasTktNumber) {
				hasTicketNumber.set(Boolean.FALSE);
				return hasTicketNumber.get();
			}
			hasTicketNumber.set(hasTktNumber);
		}
		return hasTicketNumber.get();
	}

	/**
	 * This will return whether trips has ticketNumber or not
	 */
	public static boolean hasPNRInAllTrip(List<TripInfo> trips) {
		// Default Consider Trip Has No Ticket Number
		AtomicBoolean hasPNR = new AtomicBoolean();
		for (TripInfo tripInfo : trips) {
			boolean hasTktNumber = tripInfo.hasPNR();
			if (!hasTktNumber) {
				hasPNR.set(Boolean.FALSE);
				return hasPNR.get();
			}
			hasPNR.set(hasTktNumber);
		}
		return hasPNR.get();
	}

	/**
	 * This will return whether trips has ticketNumber or not
	 */
	public static boolean hasTicketNumberInAllSegments(List<SegmentInfo> segmentInfos) {
		// Default Consider Trip Has No Ticket Number
		AtomicBoolean hasTicketNumber = new AtomicBoolean();
		for (SegmentInfo segmentInfo : segmentInfos) {
			boolean hasTktNumber = segmentInfo.hasTicketNumberForAllPax();
			if (!hasTktNumber) {
				hasTicketNumber.set(Boolean.FALSE);
				return hasTicketNumber.get();
			}
			hasTicketNumber.set(hasTktNumber);
		}
		return hasTicketNumber.get();
	}

	/**
	 * This will return whether trips has ticketNumber or not
	 */
	public static boolean hasPNRInAllSegments(List<SegmentInfo> segmentInfos) {
		// Default Consider Trip Has No Ticket Number
		AtomicBoolean hasPNR = new AtomicBoolean();
		for (SegmentInfo segmentInfo : segmentInfos) {
			boolean hasPNRNumber = segmentInfo.hasPNRForAllPax();
			if (!hasPNRNumber) {
				hasPNR.set(Boolean.FALSE);
				return hasPNR.get();
			}
			hasPNR.set(hasPNRNumber);
		}
		return hasPNR.get();
	}

	public static List<TripInfo> createTripListFromSegmentList(List<SegmentInfo> segmentList) {
		List<TripInfo> tripList = new ArrayList<>();
		TripInfo tripInfo = null;
		SegmentInfo previousSegmentInfo = null;
		List<SegmentInfo> sortedList = segmentList.stream()
				.sorted(Comparator.comparing(SegmentInfo::getId, Comparator.nullsFirst(Comparator.naturalOrder())))
				.collect(Collectors.toList());
		for (SegmentInfo segmentInfo : sortedList) {
			if (segmentInfo.getSegmentNum() == 0) {
				tripInfo = new TripInfo();
				tripList.add(tripInfo);
			}
			if (segmentInfo.getSegmentNum() > 0) {
				if (previousSegmentInfo == null) {
					log.error("Segment number is " + segmentInfo.getSegmentNum() + "segmentInfo is"
							+ segmentInfo.toString());
				}

				if (segmentInfo.getDepartTime() != null && previousSegmentInfo.getArrivalTime() != null)
					previousSegmentInfo.setConnectingTime(Duration
							.between(previousSegmentInfo.getArrivalTime(), segmentInfo.getDepartTime()).toMinutes());
			}
			tripInfo.getSegmentInfos().add(segmentInfo);
			previousSegmentInfo = segmentInfo;
		}
		return tripList;
	}

	public static List<SegmentInfo> getSegmentInfos(List<TripInfo> tripInfos) {
		List<SegmentInfo> segmentInfos = new ArrayList<>();
		for (TripInfo tripInfo : tripInfos) {
			segmentInfos.addAll(tripInfo.getSegmentInfos());
		}
		return segmentInfos;
	}

	public static List<FlightTravellerInfo> getTravellerInfos(List<TripInfo> tripInfos) {
		List<FlightTravellerInfo> travellers = new ArrayList<FlightTravellerInfo>();
		for (TripInfo trip : tripInfos) {
			for (SegmentInfo segment : trip.getSegmentInfos()) {
				travellers.addAll(segment.getTravellerInfo());
			}
		}
		return travellers;
	}

}
