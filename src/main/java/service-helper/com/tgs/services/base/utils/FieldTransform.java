package com.tgs.services.base.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Map;
import com.tgs.services.base.datamodel.EncryptionData;
import com.tgs.services.base.datamodel.Mask;
import com.tgs.services.base.helper.EncryptedField;
import com.tgs.services.base.helper.MaskedField;
import com.tgs.utils.encryption.EncryptionUtils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Transforms object of classes managed by TGS by manipulating its fields according to annotations they are annotated
 * with and order of transformation passed.
 * 
 * @author Abhineet Kumar, Technogram Solutions
 *
 */
@Slf4j
public class FieldTransform {

	public static <T> T mask(T obj) {
		if (isMaskingRequired()) {
			return transform(obj, TransformationType.MASK.getTransformation());
		}
		return obj;
	}

	private static boolean isMaskingRequired() {
		return true;
	}

	public static <T> T encrypt(T obj) {
		return transform(obj, TransformationType.ENCRYPT.getTransformation());
	}

	public static <T> T decrypt(T obj) {
		return transform(obj, TransformationType.DECRYPT.getTransformation());
	}

	/**
	 * Transform an object using series of {@code Transformation}s in order.
	 * 
	 * @param obj
	 * @param transformation
	 * @throws IllegalArgumentException
	 * @throws IllegalAccessException
	 */
	@SuppressWarnings({"rawtypes"})
	public static <T> T transform(T obj, Transformation... transformations) {
		if (obj == null) {
			return null;
		}

		try {
			Class<?> clazz = obj.getClass();

			/*
			 * Prevents infinite recursion. Each enum constant is considered which is again of same type, hence,
			 * creating infinite recursion.
			 */
			if (obj instanceof Enum) {
				return obj;
			}

			// supports classes managed by TGS only
			if (clazz.getPackage().getName().startsWith(ApplicationConstant.ADAPTABLE_PACKAGE)) {
				for (Field f : clazz.getDeclaredFields()) {
					f.setAccessible(true);
					for (Transformation transformation : transformations) {
						transformation.transform(f, obj);
					}

					Object fieldVal = f.get(obj);
					transform(fieldVal, transformations);
				}
			}

			if (Collection.class.isAssignableFrom(clazz)) {
				for (Object item : (Collection) obj) {
					transform(item, transformations);
				}
			}

			if (Map.class.isAssignableFrom(clazz)) {
				for (Object item : ((Map) obj).values()) {
					transform(item, transformations);
				}
			}
		} catch (IllegalArgumentException | IllegalAccessException e) {
			log.error("Failed to transform {} due to ", obj, e);
			throw new RuntimeException("Failed to transform");
		}

		// if (clazz.isArray()) {
		// clazz.get
		// }
		return obj;
	}

	public static enum TransformationType {

		MASK(MaskedField.class) {
			public Transformation getTransformation() {
				return new Transformation() {

					protected Object getTransformedValue(Annotation annotation, Object oldValue) {
						MaskedField maskedFieldAnnot = (MaskedField) annotation;
						Mask mask = Mask.get(maskedFieldAnnot);
						String val = (String) oldValue;
						return mask.mask(val);
					}

					@Override
					protected boolean isApplicableTo(Class<?> type) {
						return type.isAssignableFrom(String.class);
					}

					protected Annotation getAnnotationOnField(Field f) {
						return f.getAnnotation(getAnnotationType());
					}
				};
			}

			public Transformation getTransformation(Object transformationData) {
				Mask mask = (Mask) transformationData;
				return new Transformation() {

					protected Object getTransformedValue(Annotation annotation, Object oldValue) {
						return mask.mask((String) oldValue);
					}

					@Override
					protected boolean isApplicableTo(Class<?> type) {
						return type.isAssignableFrom(String.class);
					}

					protected Annotation getAnnotationOnField(Field f) {
						return f.getAnnotation(getAnnotationType());
					}
				};
			}
		},
		ENCRYPT(EncryptedField.class) {
			public Transformation getTransformation() {
				return new Transformation() {

					@Override
					protected Object getTransformedValue(Annotation annotation, Object oldValue) {
						try {
							return TgsSecurityUtils.encryptData((String) oldValue);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}

					@Override
					protected boolean isApplicableTo(Class<?> type) {
						return type.isAssignableFrom(String.class);
					}

					protected Annotation getAnnotationOnField(Field f) {
						return f.getAnnotation(getAnnotationType());
					}
				};
			}

			public Transformation getTransformation(Object transformationData) {
				EncryptionData encryptionData = (EncryptionData) transformationData;
				return new Transformation() {

					@Override
					protected Object getTransformedValue(Annotation annotation, Object oldValue) {
						try {
							return EncryptionUtils.encryptData((String) oldValue, encryptionData);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}

					@Override
					protected boolean isApplicableTo(Class<?> type) {
						return type.isAssignableFrom(String.class);
					}

					protected Annotation getAnnotationOnField(Field f) {
						return f.getAnnotation(getAnnotationType());
					}
				};
			}
		},
		DECRYPT(EncryptedField.class) {
			public Transformation getTransformation() {
				return new Transformation() {

					@Override
					protected Object getTransformedValue(Annotation annotation, Object oldValue) {
						try {
							return TgsSecurityUtils.decryptData((String) oldValue);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}

					@Override
					protected boolean isApplicableTo(Class<?> type) {
						return type.isAssignableFrom(String.class);
					}

					protected Annotation getAnnotationOnField(Field f) {
						return f.getAnnotation(getAnnotationType());
					}
				};
			}

			public Transformation getTransformation(Object transformationData) {
				EncryptionData encryptionData = (EncryptionData) transformationData;
				return new Transformation() {

					@Override
					protected Object getTransformedValue(Annotation annotation, Object oldValue) {
						try {
							return EncryptionUtils.decryptData((String) oldValue, encryptionData);
						} catch (Exception e) {
							throw new RuntimeException(e);
						}
					}

					@Override
					protected boolean isApplicableTo(Class<?> type) {
						return type.isAssignableFrom(String.class);
					}

					protected Annotation getAnnotationOnField(Field f) {
						return f.getAnnotation(getAnnotationType());
					}
				};
			}
		};

		@Getter
		private Class<? extends Annotation> annotationType;

		private TransformationType(Class<? extends Annotation> annotationType) {
			this.annotationType = annotationType;
		}

		public abstract Transformation getTransformation();

		public abstract Transformation getTransformation(Object transformationData);
	}

	public static abstract class Transformation {

		protected abstract Object getTransformedValue(Annotation annotation, Object oldValue);

		protected abstract boolean isApplicableTo(Class<?> type);

		protected abstract Annotation getAnnotationOnField(Field f);

		void transform(Field f, Object obj) throws IllegalArgumentException, IllegalAccessException {
			Annotation maskedFieldAnnot = getAnnotationOnField(f);
			if (maskedFieldAnnot != null && this.isApplicableTo(f.getType())) {
				Object value = f.get(obj);
				Object transformedValue = getTransformedValue(maskedFieldAnnot, value);
				f.set(obj, transformedValue);

			}
		}
	}
}
