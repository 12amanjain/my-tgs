package com.tgs.services.base.utils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.LogData;
import com.tgs.services.base.communicator.LogServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.hms.datamodel.HotelInfo;
import com.tgs.services.hms.datamodel.HotelMissingInfo;
import com.tgs.services.hms.datamodel.HotelSearchQuery;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class HotelMissingInfoLoggingUtil {

	@Autowired
	static LogServiceCommunicator logServiceComm;

	public static void init(LogServiceCommunicator logServiceCommunicator) {
		logServiceComm = logServiceCommunicator;
	}

	public static ThreadLocal<List<String>> logDataList = new ThreadLocal<>();

	public static void addToLogList(HotelMissingInfo missingInfo, HotelSearchQuery searchQuery) {
		List<String> nonNullMissingFields = TgsObjectUtils.getNotNullFields(missingInfo, false, false);
		if (nonNullMissingFields.isEmpty()
				|| (nonNullMissingFields.size() == 1 && nonNullMissingFields.contains("operationType")))
			return;
		if (logDataList.get() == null) {
			logDataList.set(new ArrayList<>());
		}
		missingInfo.setSearchId(searchQuery.getSearchId());
		logDataList.get().add(GsonUtils.getGson().toJson(missingInfo));
	}

	public static void addToLogList(HotelMissingInfo missingInfo, HotelInfo hInfo, HotelSearchQuery searchQuery,
			String supplierHotelId) {
		List<String> nonNullMissingFields = TgsObjectUtils.getNotNullFields(missingInfo, false, false);
		if (nonNullMissingFields.isEmpty()
				|| (nonNullMissingFields.size() == 1 && nonNullMissingFields.contains("operationType")))
			return;
		if (logDataList.get() == null) {
			logDataList.set(new ArrayList<>());
		}
		if (StringUtils.isNotBlank(supplierHotelId)) {
			missingInfo.setHotelId(supplierHotelId);
		}
		missingInfo.setHotelName(hInfo.getName());
		missingInfo.setSupplierName(hInfo.getOptions().get(0).getMiscInfo().getSupplierId());
		missingInfo.setSearchId(searchQuery.getSearchId());

		logDataList.get().add(GsonUtils.getGson().toJson(missingInfo));
	}

	public static void clearLog(String type) {
		List<String> logList = logDataList.get();
		if (CollectionUtils.isEmpty(logList))
			return;
		try {
			LogData logData = LogData.builder().generationTime(LocalDateTime.now()).logDataList(logList).key(type)
					.type("LOGINFO").logType("MissingInfoLogs").build();
			logServiceComm.store(logData);
		} catch (Exception e) {
			log.error("Unable to log info for {}", logList, e);
		} finally {
			logDataList.set(null);
		}
	}

}
