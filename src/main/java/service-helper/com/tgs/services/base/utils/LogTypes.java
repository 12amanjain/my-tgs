package com.tgs.services.base.utils;

public class LogTypes {

	public static final String SYSTEM_FILTER = "system#filter";

	public static final String USERDBSTART = "user#db#start";
	public static final String USERDBEND = "user#db#end";
	public static final String AIRSEARCH = "airsearch";

	public static final String AIRSEARCH_RESPONSE_START = "airsearch#parseresponse#start";
	public static final String AIRSEARCH_RESPONSE_END = "airsearch#parseresponse#end";


	public static final String AIRSEARCH_DOSEARCH = "airsearch#dosearch";
	public static final String AIRSEARCH_PROCESS = "airsearch#process";
	public static final String AIRSEARCH_STORE = "airsearch#store";
	public static final String AIR_SUPPLIER_SEARCH_START = "air#supplier#search#start";
	public static final String AIR_SUPPLIER_SEARCH_END = "air#supplier#search#end";

	public static final String AIR_REVIEW_REVIEWTRIP = "airreview#reviewtrip";
	public static final String AIR_REVIEW_SUPPLIERBEGIN = "airreview#supplier#begin";
	public static final String AIR_REVIEW_SUPPLIEREND = "airreview#supplier#end";

	public static final String AIR_SUPPLIER_CHANGE_CLASS_SEARCH_START = "air#supplier#changeclasssearch#start";
	public static final String AIR_SUPPLIER_CHANGE_CLASS_END_START = "air#supplier#changeclassend#end";

	public static final String AIR_SUPPLIER_CHANGE_CLASS_FARE_SEARCH_START = "air#supplier#changeclasssearch#start";
	public static final String AIR_SUPPLIER_CHANGE_CLASS_FARE_END_START = "air#supplier#changeclassend#end";

	/*
	 * Hotel Logs
	 */

	public static final String HOTEL_SUPPLIER_SEARCH_START = "hotel#supplier#search#start";
	public static final String HOTEL_SUPPLIER_SEARCH_END = "hotel#supplier#search#end";
	public static final String HOTELSEARCH_PROCESS_START = "hotelsearch#process#start";
	public static final String HOTELSEARCH_PROCESS_END = "hotelsearch#process#end";


	public static final String HOTEL_SINGLE_SUPPLIER_PROCESS_START = "process#singlesupplier#start";
	public static final String HOTEL_SINGLE_SUPPLIER_PROCESS_END = "process#singlesupplier#end";

	public static final String HOTEL_PROCESS_OPTIONS_PROCESS_START = "process#singlesupplier#options#start";
	public static final String HOTEL_PROCESS_OPTIONS_PROCESS_END = "process#singlesupplier#options#end";

	public static final String HOTEL_APPLY_PROPERTY_TYPE_PROCESS_START =
			"process#singlesupplier#applypropertytype#start";
	public static final String HOTEL_APPLY_PROPERTY_TYPE_PROCESS_END = "process#singlesupplier#applypropertytype#end";

	public static final String HOTEL_APPLY_PROMOTION_PROCESS_START = "process#singlesupplier#applypromotion#start";
	public static final String HOTEL_APPLY_PROMOTION_PROCESS_END = "process#singlesupplier#applypromotion#end";

	public static final String HOTEL_COMBINE_PREVIOUS_RESULT_PROCESS_START =
			"process#singlesupplier#combinepreviousresult#start";
	public static final String HOTEL_COMBINE_PREVIOUS_RESULT_PROCESS_END =
			"process#singlesupplier#combinepreviousresult#end";

	public static final String HOTEL_AGGREGATE_HOTEL_PROCESS_START = "process#singlesupplier#aggregatehotel#start";
	public static final String HOTEL_AGGREGATE_HOTEL_PROCESS_END = "process#singlesupplier#aggregatehotel#end";

	public static final String HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_START =
			"process#singlesupplier#aggregatebyuniqueykey#start";
	public static final String HOTEL_AGGREGATE_BY_UNIQUE_KEY_HOTEL_PROCESS_END =
			"process#singlesupplier#aggregatebyuniqueykey#end";

	public static final String HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_START =
			"process#singlesupplier#aggregatebyconstraint#start";
	public static final String HOTEL_AGGREGATE_BY_CONSTRAINT_HOTEL_PROCESS_END =
			"process#singlesupplier#aggregatebyconstraint#end";

	public static final String HOTEL_MERGE_OPTIONS_PROCESS_START = "process#singlesupplier#mergeoptions#start";
	public static final String HOTEL_MERGE_OPTIONS_PROCESS_END = "process#singlesupplier#mergeoptions#end";

	public static final String HOTEL_MERGE_SORTED_OPTIONS_PROCESS_START =
			"process#singlesupplier#mergesortedoptions#start";
	public static final String HOTEL_MERGE_SORTED_OPTIONS_PROCESS_END = "process#singlesupplier#mergesortedoptions#end";

	public static final String HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_START =
			"process#singlesupplier#buildprocessedoptions#start";
	public static final String HOTEL_BUILD_PROCESSED_OPTIONS_PROCESS_END =
			"process#singlesupplier#buildprocessedoptions#end";

	public static final String HOTEL_APPLY_COMMERCIAL_PROCESS_START = "process#singlesupplier#applycommercial#start";
	public static final String HOTEL_APPLY_COMMERCIAL_PROCESS_END = "process#singlesupplier#applycommercial#end";

	public static final String HOTEL_POST_APPLY_COMMERCIAL_PROCESS_START =
			"process#singlesupplier#postapplycommercial#start";
	public static final String HOTEL_POST_APPLY_COMMERCIAL_PROCESS_END =
			"process#singlesupplier#postapplycommercial#end";

	/*
	 * Hotel Cache Related Logs
	 */

	public static final String HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_START =
			"process#cache#processandstore#singlesupplier#start";
	public static final String HOTEL_PROCESS_AND_STORE_IN_CACHE_PROCESS_END =
			"process#cache#processandstore#singlesupplier#end";

	public static final String HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_START =
			"process#cache#fetchsearchresult#singlesupplier#start";
	public static final String HOTEL_FETCH_SEARCH_RESULT_IN_CACHE_PROCESS_END =
			"process#cache#fetchsearchresult#singlesupplier#end";

	public static final String HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_START =
			"process#cache#storesearchresult#singlesupplier#start";
	public static final String HOTEL_STORE_SEARCH_RESULT_IN_CACHE_PROCESS_END =
			"process#cache#storesearchresult#singlesupplier#end";

	public static final String HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_START = "process#cache#meal#singlesupplier#start";
	public static final String HOTEL_FETCH_MEAL_SUPPLIER_PROCESS_END = "process#cache#meal#singlesupplier#end";

	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_START =
			"process#cache#staticdata#singlesupplier#start";
	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_PROCESS_END =
			"process#cache#staticdata#singlesupplier#end";

	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_START =
			"process#cache#staticdata#suppliermapping#singlesupplier#start";
	public static final String HOTEL_FETCH_STATIC_DATA_SUPPLIER_MAPPING_PROCESS_END =
			"process#cache#staticdata#suppliermapping#singlesupplier#end";

	public static final String HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_START =
			"process#cache#staticdata#hotelmapping#singlesupplier#start";
	public static final String HOTEL_FETCH_STATIC_DATA_HOTEL_MAPPING_PROCESS_END =
			"process#cache#staticdata#hotelmapping#singlesupplier#end";

	public static final String MISSING_INFO = "missinginfo";
}