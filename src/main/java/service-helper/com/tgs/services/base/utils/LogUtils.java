package com.tgs.services.base.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomLogBackFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.LogData;
import com.tgs.services.base.RestAPIListener;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.communicator.LogServiceCommunicator;
import com.tgs.services.base.enums.LogDataVisibilityGroup;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.utils.common.HttpUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class LogUtils {

	@Autowired
	static LogServiceCommunicator logServiceComm;

	@Autowired
	static GeneralCachingCommunicator generalCachingComm;

	public static void init(LogServiceCommunicator logServiceCommunicator,
			GeneralCachingCommunicator generalCachingCommunicator) {
		logServiceComm = logServiceCommunicator;
		generalCachingComm = generalCachingCommunicator;
	}

	public static ThreadLocal<List<LogData>> logDataList = new ThreadLocal<>();

	public static void addToLogList(LogData logData) {
		if (logDataList.get() == null) {
			logDataList.set(new ArrayList<>());
		}
		if (logData.getGenerationTime() == null) {
			logData.setGenerationTime(LocalDateTime.now());
		}
		logDataList.get().add(logData);
	}

	public static void clearLogList() {
		try {
			logServiceComm.store(logDataList.get());
		} finally {
			logDataList.set(null);
		}
	}

	public static void clearLogList(List<String> keys, List<LogData> logs) {
		if (CollectionUtils.isEmpty(keys)) {
			store(logs);
			return;
		}

		for (String key : keys) {
			for (LogData logData : logs) {
				LogData logDatacP = logData;
				if (keys.size() > 1)
					logDatacP = new GsonMapper<>(logData, LogData.class).convert();
				logDatacP.setKey(key);
				logServiceComm.store(logDatacP);
			}
		}
	}

	public static void store(List<LogData> logDataList) {
		logServiceComm.store(logDataList);
	}

	public static void log(String type, LogMetaInfo logMetaInfo, String previousType, Long threshold) {
		try {
			ContextData contextData = SystemContextHolder.getContextData();
			if (contextData == null || contextData.getRequestContextHolder() == null)
				return;
			if (logMetaInfo == null) {
				logMetaInfo = LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build();
			}
			if (CustomLogBackFilter.getDebugUserId().equals(UserUtils.getUserId(contextData.getUser()))) {
				threshold = 20L;
			}
			logMetaInfo.setEndPoint(contextData.getRequestContextHolder().getEndPoint());
			Map<String, LogMetaInfo> logMap = contextData.getLogMap();
			logMap.put(type, logMetaInfo);
			if (StringUtils.isNotBlank(previousType) && logMap.get(previousType) != null) {
				long diff = logMetaInfo.getTimeInMs() - logMap.get(previousType).getTimeInMs();
				if (threshold == null || diff > threshold) {
					String message = StringUtils.join("Total time took to execute from ", previousType, " To ", type,
							" is ", diff, " metaInfo ", logMetaInfo.toString());
					LogData logData = LogData.builder().generationTime(LocalDateTime.now()).logData(message).key(type)
							.userId(UserUtils.getUserId(contextData.getUser()))
							.userRole(UserUtils.getRole(contextData.getUser())).type("LOGINFO")
							.logType("ServiceHealthLogs").supplier("TGSSUPPLIER").build();
					logData.setDiff(diff);
					logServiceComm.store(logData);
				}
			}
		} catch (Exception e) {
			log.error("Unable to log info for type {}", type, e);
		}
	}

	public static void copyLog(String from, String to) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.LOGS.getName()).kyroCompress(true)
				.set(CacheSetName.LOGS.getName()).key(from).build();
		final String bookId = to;
		Map<String, List<LogData>> result =
				generalCachingComm.getList(metaInfo, LogData.class, true, false, new String[0]);
		if (MapUtils.isNotEmpty(result)) {
			result.forEach((key, value) -> {
				for (LogData data : value) {
					data.setKey(bookId);
					LogUtils.store(Arrays.asList(data));
				}
			});
		}
	}

	public static void log(String type, LogMetaInfo logMetaInfo, String previousType) {
		log(type, logMetaInfo, previousType, null);
	}

	public static void log(String prefix, HttpUtils httpUtils) {
		for (String reqId : SystemContextHolder.getContextData().getReqIds()) {
			log(reqId, prefix, httpUtils);
		}
	}

	public static void log(String key, String prefix, HttpUtils httpUtils) {
		RestAPIListener listener = new RestAPIListener(key);
		listener.addLog(LogData.builder().key(key)
				.logData(StringUtils.isBlank(httpUtils.getPostData()) ? httpUtils.getUrlString()
						: httpUtils.getPostData())
				.type(prefix + "Request").generationTime(LocalDateTime.now()).build());
		listener.addLog(LogData.builder().key(key).logData(httpUtils.getResponseString()).type(prefix + "Response")
				.generationTime(LocalDateTime.now()).build());
	}

	/**
	 * Log if reqId present in ContextData
	 * 
	 * @param prefix
	 * @param e
	 */
	public static void log(String prefix, Exception e) {
		for (String reqId : SystemContextHolder.getContextData().getReqIds()) {
			log(reqId, prefix, e);
		}
	}

	public static void log(String key, String prefix, Exception e) {
		RestAPIListener listener = new RestAPIListener(key);
		StringWriter sw = new StringWriter();
		User user = SystemContextHolder.getContextData().getUser();
		e.printStackTrace(new PrintWriter(sw));
		String stackTrace = sw.toString();
		listener.addLog(LogData.builder().key(key).logData(stackTrace).type(prefix + "Error").supplier("Tgs")
				.userId(UserUtils.getUserId(user)).visibilityGroups(Lists.newArrayList(LogDataVisibilityGroup.DEVELOPER.name()))
				.generationTime(LocalDateTime.now()).build());
	}

}
