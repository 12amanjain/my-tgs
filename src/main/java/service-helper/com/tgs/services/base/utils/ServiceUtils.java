package com.tgs.services.base.utils;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang.WordUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.BeanOutput;
import com.tgs.services.base.datamodel.BeanType;
import com.tgs.services.base.datamodel.CityInfo;
import com.tgs.services.base.datamodel.ProductMetaInfo;
import com.tgs.services.base.manager.AbstractReferenceIdGenerator;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.pms.datamodel.CreditLine;
import com.tgs.services.pms.datamodel.PaymentMetaInfo;
import com.tgs.services.ums.datamodel.User;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ServiceUtils {

	@Autowired
	private static GeneralServiceCommunicator gnComm;

	@Autowired
	private static UserServiceCommunicator userSrvComm;

	public static void init(GeneralServiceCommunicator gnsComm, UserServiceCommunicator usrSrvComm) {
		gnComm = gnsComm;
		userSrvComm = usrSrvComm;
	}

	public static boolean hasError(BaseResponse response) {
		return Objects.nonNull(response) && Objects.nonNull(response.getErrors());
	}

	public static String generateId(ProductMetaInfo metaInfo) {
		ClientGeneralInfo cInfo = gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		AbstractReferenceIdGenerator referenceIdGenerator = (AbstractReferenceIdGenerator) SpringContext
				.getApplicationContext().getBean(cInfo.getRefIdConfiguration().getReferenceIdManagerBean());
		return referenceIdGenerator.generateReferenceId(metaInfo, cInfo.getRefIdConfiguration());
	}

	/**
	 * Whenever you do any change in this function make sure to do changes in JWTHelper#generateNewToken
	 *
	 * @param user
	 * @param payService
	 */
	public static void updateUserInfoforJWTToken(User user, PaymentServiceCommunicator payService, User oldUser) {
		user.setDailyUsageInfo(TgsObjectUtils.firstNonNull(() -> userSrvComm.getDailyUsageInfo(user.getUserId()),
				() -> UserUtils.getUsageInfo(user)));

		try {
			BigDecimal balance = payService.getUserBalanceFromCache(PaymentMetaInfo.builder()
					.userIds(Arrays.asList(user.getUserId())).fetchCreditLineBalance(false).build())
					.get(user.getUserId());
			/**
			 * Show only usage in case usage is configured otherwise show all type of wallet / creditline etc.
			 */
			if (user.getDailyUsageInfo() != null) {
				user.setTotalBalance(user.getDailyUsageInfo().getBalance());
			} else {
				List<CreditLine> creditLines = payService.fetchCreditLineFromCache(user.getParentUserId());

				if (CollectionUtils.isNotEmpty(creditLines)) {
					for (CreditLine cline : creditLines) {
						balance = balance.add(cline.getBalance());
					}
				}
				user.setTotalBalance(balance.doubleValue());
				user.setBalanceSummary(payService.getUserBalanceSummary(user.getParentUserId()));
				user.setDueSummary(payService.getUserDueSummary(user.getParentUserId()));
				if (!isUserDuesApplicable() && user.getDueSummary() != null) {
					user.getDueSummary().setOldDueInfo(null);
				}
			}
			user.setBalance(balance.doubleValue());
			Map<String, Set<User>> relations = userSrvComm.getUserRelations(Arrays.asList(user.getUserId()));
			Set<User> userRelations = relations.get(user.getUserId());
			if (userRelations != null) {
				user.setUserRelations(userRelations);
			}

			if (oldUser != null) {
				user.setEmulateUser(oldUser.getEmulateUser());
				user.setParentUser(oldUser.getParentUser());
				user.setParentConf(oldUser.getParentConf());
			}
		} catch (Exception e) {
			log.error("Unable to update userinfo for userId {}", UserUtils.getUserId(user), e);
		}
	}

	public static Boolean isUserDuesApplicable() {
		ClientGeneralInfo clientInfo = gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (clientInfo != null && BooleanUtils.isFalse(clientInfo.getUserDuesApplicable())) {
			return false;
		}
		return true;
	}

	public static Boolean isGstApplicable() {
		ClientGeneralInfo clientInfo = gnComm.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		if (clientInfo != null && BooleanUtils.isTrue(clientInfo.getIsGstAllowed())) {
			return true;
		}
		return null;
	}

	/**
	 * @implSpec : at any point of time @param key and @param defaultValue should not be null
	 */
	public static String getDependencyBeanName(BeanType beanType, String key, Class<?> defaultValue) {
		try {
			GeneralBasicFact fact = GeneralBasicFact.builder().build();
			BeanOutput beanInfo = gnComm.getConfigRule(ConfiguratorRuleType.BEANINFO, fact);
			if (beanInfo != null && beanInfo.getBeanMap() != null && beanInfo.getBeanMap().get(beanType) != null
					&& MapUtils.getObject(beanInfo.getBeanMap().get(beanType), key) != null) {
				return WordUtils.uncapitalize(beanInfo.getBeanMap().get(beanType).get(key));
			}
		} catch (Exception e) {
			log.error("Exception occurred on creating class of BeanType {} for key {}", beanType, key);
		}
		return WordUtils.uncapitalize(defaultValue.getSimpleName());
	}

	public static AddressInfo getContactAddressInfo(String bookingUserId, ClientGeneralInfo clientInfo) {
		AddressInfo addressInfo = AddressInfo.builder().build();
		User bookingUser = userSrvComm.getUserFromCache(bookingUserId);
		if (!BooleanUtils.isTrue(clientInfo.getUseClientAddress()) && bookingUser.getAddressInfo() != null
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getAddress())
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getPincode())
				&& bookingUser.getAddressInfo().getCityInfo() != null
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getCityInfo().getName())
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getCityInfo().getCountry())
				&& StringUtils.isNotEmpty(bookingUser.getAddressInfo().getCityInfo().getState())) {
			addressInfo = bookingUser.getAddressInfo();
		} else {
			addressInfo.setAddress(StringUtils.join(clientInfo.getAddress1(), clientInfo.getAddress2()));
			CityInfo cityInfo = CityInfo.builder().name(clientInfo.getCity()).country(clientInfo.getCountry())
					.state(clientInfo.getState()).build();
			addressInfo.setCityInfo(cityInfo);
			addressInfo.setPincode(clientInfo.getPostalCode());
		}
		return addressInfo;
	}

	public static String getHostName() {
		return System.getenv("HOSTNAME");
	}

	public static boolean isUrlMatchesRegex(List<String> regexList, String requestUrl) {
		for (String endPoint : regexList) {
			if (requestUrl.matches(endPoint)) {
				return true;
			}
		}
		log.debug("End Point {} not matched", requestUrl);
		return false;
	}

}
