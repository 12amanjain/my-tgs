package com.tgs.services.base.utils.msg;
import java.util.function.Supplier;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractMessageSupplier<T> implements Supplier<T> {

	public final T getAttributes() {
		try {
			return get();
		}catch(Exception e) {
			log.error("Error occured while fetching message attributes ",   e);
			return null;
		}
	}


}
