package com.tgs.services.base.utils.thread;

public interface AsyncExecutionExceptionHandler {

	void handleException(Exception e, int taskIndex);
	
}
