package com.tgs.services.base.utils.thread;

import java.util.Map;
import java.util.concurrent.Callable;

import org.slf4j.MDC;

public interface CustomCallable<V> extends Callable<V> {
	@Override
	default V call() throws Exception {
		Map<String, String> previousContext = MDC.getCopyOfContextMap();
		if (getParentContext() == null) {
			MDC.clear();
		} else {
			MDC.setContextMap(getParentContext());
		}
		try {
			return this.customCall();
		} finally {
			if (previousContext == null) {
				MDC.clear();
			} else {
				MDC.setContextMap(previousContext);
			}
		}
	}

	V customCall() throws Exception;

	Map<String, String> getParentContext();

	String getThreadName();

}
