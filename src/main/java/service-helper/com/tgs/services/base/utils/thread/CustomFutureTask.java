package com.tgs.services.base.utils.thread;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

import org.slf4j.MDC;

import com.tgs.services.base.runtime.ContextData;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomFutureTask<V> extends FutureTask<V> {

	private String originalName;
	private long startTime;
	private String caller;
	private Map<String, String> context;
	private ContextData contextData;

	public CustomFutureTask(Callable<V> callable) {
		super(callable);
	}

	public CustomFutureTask(Runnable runnable, V result) {
		super(runnable, result);
	}

	@Override
	public void run() {
		Map<String, String> previous = MDC.getCopyOfContextMap();
		if (context == null) {
			MDC.clear();
		} else {
			MDC.setContextMap(context);
		}
		try {
			super.run();
		} finally {
			if (previous == null) {
				MDC.clear();
			} else {
				MDC.setContextMap(previous);
			}
		}
	}
}
