package com.tgs.services.base.utils.thread;

import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.RejectedExecutionHandler;
import java.util.concurrent.RunnableFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.MDC;
import com.tgs.services.base.runtime.SystemContextHolder;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomThreadPoolExecutor extends ThreadPoolExecutor {

	final private boolean useFixedContext;
	final private Map<String, String> fixedContext;
	
	public static CustomThreadPoolExecutor newWithInheritedMdc(int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
		return newWithInheritedMdc(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, null);
	}

	public static CustomThreadPoolExecutor newWithCurrentMdc(int corePoolSize, int maximumPoolSize, long keepAliveTime,
			TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
		return new CustomThreadPoolExecutor(MDC.getCopyOfContextMap(), corePoolSize, maximumPoolSize, keepAliveTime,
				unit, workQueue, threadFactory);
	}

	public static CustomThreadPoolExecutor newWithFixedMdc(Map<String, String> fixedContext, int corePoolSize,
			int maximumPoolSize, long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue,
			ThreadFactory threadFactory) {
		return new CustomThreadPoolExecutor(fixedContext, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				threadFactory);
	}
	
	public static CustomThreadPoolExecutor newWithInheritedMdc(int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, 
			RejectedExecutionPolicy rejectedExecutionPolicy) {
		if (rejectedExecutionPolicy == null) {
			rejectedExecutionPolicy = RejectedExecutionPolicy.DEFAULT;
		}
		return new CustomThreadPoolExecutor(null, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue,
				threadFactory, rejectedExecutionPolicy);
	}

	private CustomThreadPoolExecutor(Map<String, String> fixedContext, int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory) {
		this(fixedContext, corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, 
				RejectedExecutionPolicy.DEFAULT);
	}

	private CustomThreadPoolExecutor(Map<String, String> fixedContext, int corePoolSize, int maximumPoolSize,
			long keepAliveTime, TimeUnit unit, BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, 
			RejectedExecutionPolicy rejectedExecutionPolicy) {
		super(corePoolSize, maximumPoolSize, keepAliveTime, unit, workQueue, threadFactory, 
				rejectedExecutionPolicy.getRejectedExecutionHandler());
		this.fixedContext = fixedContext;
		useFixedContext = (fixedContext != null);
		allowCoreThreadTimeOut(true);
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(Runnable runnable, T value) {
		return new CustomFutureTask<T>(runnable, value) {
			@Override
			public String toString() {
				return Thread.currentThread().getName();
			}
		};
	}

	@Override
	protected <T> RunnableFuture<T> newTaskFor(final Callable<T> callable) {
		return new CustomFutureTask<T>(callable) {
			@Override
			public String toString() {
				String tName = Thread.currentThread().getName();
				if (callable instanceof CustomCallable) {
					String newThreadName = ((CustomCallable<?>) callable).getThreadName();
					if (!tName.endsWith(newThreadName)) {
						return tName + "-" + newThreadName;
					}
				}
				return tName;
			}
		};
	}

	@Override
	protected void beforeExecute(Thread t, Runnable r) {
		super.beforeExecute(t, r);
		CustomFutureTask<?> task = (CustomFutureTask<?>) r;
		task.setOriginalName(t.getName());
		t.setName(task.toString());
		SystemContextHolder.setContextData(task.getContextData());
	}

	@Override
	public void execute(Runnable command) {
		CustomFutureTask<?> task = (CustomFutureTask<?>) command;
		task.setStartTime(System.currentTimeMillis());
		task.setCaller(ThreadUtils.getThreadStackTrace(4, 7));
		task.setContextData(SystemContextHolder.getContextData());
		task.setContext(getContextForTask());
		super.execute(command);
	}

	private Map<String, String> getContextForTask() {
		return useFixedContext ? fixedContext : MDC.getCopyOfContextMap();
	}

	@Override
	protected void afterExecute(Runnable r, Throwable t) {
		super.afterExecute(r, t);
		CustomFutureTask<?> task = (CustomFutureTask<?>) r;
		if (t == null) {
			try {
				if (task.isDone()) {
					task.get();
				}
			} catch (CancellationException ce) {
				t = ce;
			} catch (ExecutionException ee) {
				t = ee.getCause();
			} catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
			int totalExecutionTime = (int) (System.currentTimeMillis() - task.getStartTime()) / 1000;
			int maxExecutionTimeAllowed =
					ThreadPool.valueOf(StringUtils.substringBetween(task.getOriginalName(), "-")).getMaxExecutionTime();
			if (totalExecutionTime > maxExecutionTimeAllowed) {
				log.info(
						" Total execution time exceded defined max execution time , totalExecutionTime {} , maxExecutionTime {}, caller {} ",
						totalExecutionTime, maxExecutionTimeAllowed, task.getCaller());
			}
		}

		if (t != null) {
			log.info("Exception occurred while thread execution ", t);
		}

		Thread.currentThread().setName(task.getOriginalName());
	}
	
	public static enum RejectedExecutionPolicy {
		DEFAULT(new ThreadPoolExecutor.AbortPolicy()),
		CALLER_RUN(new ThreadPoolExecutor.CallerRunsPolicy() {
			@Override
			public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
				CustomFutureTask<?> task = (CustomFutureTask<?>) r;
				log.info("Task {} was rejected and now running on caller thread.", task.getOriginalName());
				super.rejectedExecution(r, e);
	        }
		}),
		DISCARD(new ThreadPoolExecutor.DiscardPolicy() {
			@Override
			public void rejectedExecution(Runnable r, ThreadPoolExecutor e) {
				CustomFutureTask<?> task = (CustomFutureTask<?>) r;
				log.info("Task {} was rejected and discarded.", task.getOriginalName());
				super.rejectedExecution(r, e);
	        }
		});
		
		@Getter
		final private RejectedExecutionHandler rejectedExecutionHandler;
		
		RejectedExecutionPolicy(RejectedExecutionHandler rejectedExecutionHandler) {
			this.rejectedExecutionHandler = rejectedExecutionHandler;
		}
	}
}
