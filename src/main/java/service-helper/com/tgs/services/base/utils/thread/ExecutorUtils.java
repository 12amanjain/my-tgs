package com.tgs.services.base.utils.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import com.tgs.services.base.utils.thread.CustomThreadPoolExecutor.RejectedExecutionPolicy;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ExecutorUtils {

	private static ExecutorService generalpurposeThreadPool = null;

	private static ExecutorService logThreadPool = null;

	private static ExecutorService flightSearchThreadPool = null;

	private static ExecutorService hotelSearchThreadPool = null;

	private static ExecutorService analyticsThreadPool = null;

	private static ExecutorService fareTrackerThreadPool = null;

	private static ExecutorService communicationThreadPool = null;
	
	private static ExecutorService hotelUserReviewSearchThreadPool = null;

	private static ExecutorService noShowThreadPool = null;

	private static ExecutorService newFixedThreadPool(int nThreads, int maxThreads, String threadPoolName,
			RejectedExecutionPolicy rejectedExecutionPolicy) {
		return newFixedThreadPool(nThreads, maxThreads, threadPoolName, 200, rejectedExecutionPolicy);
	}

	private static ExecutorService newFixedThreadPool(int nThreads, int maxThreads, String threadPoolName,
			int queueSize, RejectedExecutionPolicy rejectedExecutionPolicy) {
		return CustomThreadPoolExecutor.newWithInheritedMdc(nThreads, maxThreads, 60L, TimeUnit.SECONDS,
				new LinkedBlockingQueue<Runnable>(queueSize), customThreadFactory(threadPoolName),
				rejectedExecutionPolicy);
	}

	public static ExecutorService getAnalyticsThreadPool() {
		String poolName = ThreadPool.AnalyticsThreadPool.name();
		if (analyticsThreadPool == null) {
			analyticsThreadPool = newFixedThreadPool(1000, 1000, poolName, null);

		}
		checkMaxActiveCount(analyticsThreadPool, poolName);
		return analyticsThreadPool;
	}

	public static ExecutorService getCommunicationThreadPool() {
		String poolName = ThreadPool.CommunicationThreadPool.name();
		if (communicationThreadPool == null) {
			communicationThreadPool = newFixedThreadPool(200, 200, poolName, null);

		}
		checkMaxActiveCount(communicationThreadPool, poolName);
		return communicationThreadPool;
	}

	public static ExecutorService getFlightSearchThreadPool() {
		String poolName = ThreadPool.FlightSearch.name();
		if (flightSearchThreadPool == null) {
			flightSearchThreadPool = newFixedThreadPool(1000, 1000, poolName, null);
		}
		checkMaxActiveCount(flightSearchThreadPool, poolName);
		return flightSearchThreadPool;
	}

	public static ExecutorService getHotelSearchThreadPool() {
		String poolName = ThreadPool.HotelSearch.name();
		if (hotelSearchThreadPool == null) {
			hotelSearchThreadPool = newFixedThreadPool(1000, 1000, poolName, null);
		}
		checkMaxActiveCount(hotelSearchThreadPool, poolName);
		return hotelSearchThreadPool;
	}
	
	public static ExecutorService getHotelUserReviewSearchThreadPool() {
		String poolName = ThreadPool.HotelUserReview.name();
		if (hotelUserReviewSearchThreadPool == null) {
			hotelUserReviewSearchThreadPool = newFixedThreadPool(100, 100, poolName, null);
		}
		checkMaxActiveCount(hotelUserReviewSearchThreadPool, poolName);
		return hotelUserReviewSearchThreadPool;
	}

	public static ExecutorService getGeneralPurposeThreadPool() {
		String poolName = ThreadPool.GeneralPurpose.name();
		if (generalpurposeThreadPool == null) {
			generalpurposeThreadPool = newFixedThreadPool(1000, 1000, poolName, null);
		}
		checkMaxActiveCount(generalpurposeThreadPool, poolName);
		return generalpurposeThreadPool;
	}

	public static ExecutorService getLogThreadPool() {
		String poolName = ThreadPool.logThreadPool.name();
		if (logThreadPool == null) {
			logThreadPool = newFixedThreadPool(1000, 1000, poolName, RejectedExecutionPolicy.DISCARD);
		}
		checkMaxActiveCount(logThreadPool, poolName);
		return logThreadPool;
	}

	public static ExecutorService getNoShowThreadPool() {

		String poolName = ThreadPool.NoShowThreadPool.name();
		if (noShowThreadPool == null) {
			noShowThreadPool = newFixedThreadPool(50, 50, poolName, null);
		}
		checkMaxActiveCount(noShowThreadPool, poolName);
		return noShowThreadPool;
	}

	public static ExecutorService getFareTrackerThreadPool() {
		return getFareTrackerThreadPool(100, 1000);
	}

	public static ExecutorService getFareTrackerThreadPool(int poolSize, int queueSize) {
		String poolName = ThreadPool.FareTrackerThreadPool.name();
		if (fareTrackerThreadPool == null) {
			fareTrackerThreadPool =
					newFixedThreadPool(poolSize, poolSize, poolName, queueSize, RejectedExecutionPolicy.DISCARD);
		}
		checkMaxActiveCount(fareTrackerThreadPool, poolName);
		return fareTrackerThreadPool;
	}

	public static void checkMaxActiveCount(ExecutorService threadPool, String poolName) {
		int activeAcount = ((ThreadPoolExecutor) threadPool).getActiveCount();
		int maxCount = ((ThreadPoolExecutor) threadPool).getMaximumPoolSize();
		if (activeAcount > ((0.75) * maxCount)) {
			log.info(
					"Maximum currency of {} thread pool has reached greater than 75% , need to reevaluate max size, max size is {}, active count is {}",
					poolName, maxCount, activeAcount);
		}
	}

	private static ThreadFactory customThreadFactory(String threadPoolName) {
		return new CustomThreadFactory(threadPoolName);
	}

	static class CustomThreadFactory implements ThreadFactory {
		private final ThreadGroup group;
		private final AtomicInteger threadNumber = new AtomicInteger(1);
		private final String namePrefix;

		CustomThreadFactory(String threadPoolName) {
			SecurityManager s = System.getSecurityManager();
			group = (s != null) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
			namePrefix = "pool-" + threadPoolName + "-thread-";
		}

		@Override
		public Thread newThread(Runnable r) {
			Thread t = new Thread(group, r, namePrefix + threadNumber.getAndIncrement(), 0);
			if (t.isDaemon())
				t.setDaemon(false);
			if (t.getPriority() != Thread.NORM_PRIORITY)
				t.setPriority(Thread.NORM_PRIORITY);
			return t;
		}
	}

	private ExecutorUtils() {}

}
