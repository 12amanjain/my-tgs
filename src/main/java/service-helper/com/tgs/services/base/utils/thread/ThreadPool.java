package com.tgs.services.base.utils.thread;

import lombok.Getter;

@Getter
public enum ThreadPool {
	FlightSearch(120),
	HotelSearch(180),
	GeneralPurpose(20),
	logThreadPool(10),
	AnalyticsThreadPool(50),
	FareTrackerThreadPool(120),
	CommunicationThreadPool(180),
	HotelUserReview(20),
	NoShowThreadPool(80);


	private int maxExecutionTime;

	private ThreadPool(int maxExecutionTime) {
		this.maxExecutionTime = maxExecutionTime;
	}
}
