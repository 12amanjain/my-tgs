package com.tgs.services.base.utils.thread;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Supplier;
import com.tgs.services.base.CustomGeneralException;

public class UnboundedAsynchronousExecutor {

	private final ExecutorService executorService;
	private int maxAsynchronousTasks, timeout, consecutiveUnhealthyExecutionsAllowed;
	private AsyncExecutionExceptionHandler exceptionHandler;
	private int counter = 0;
	final private AtomicBoolean busy = new AtomicBoolean(false);

	/**
	 * 
	 * @param executorService must be non-null
	 * @param maxAsynchronousTasks must be > 0
	 * @param timeout (in seconds) must be > 0
	 * @param consecutiveUnhealthyExecutionsAllowed must be > 0
	 * @param exceptionHandler (may be null) for handling exception if any task fails to complete execution
	 * @throws IllegalArgumentException if maxAsynchronousTasks <= 0 or timeout <= 0 or
	 *         consecutiveUnhealthyExecutionsAllowed <= 0 or executorService is null
	 */
	public UnboundedAsynchronousExecutor(ExecutorService executorService, int maxAsynchronousTasks, int timeout,
			int consecutiveUnhealthyExecutionsAllowed, AsyncExecutionExceptionHandler exceptionHandler) {
		if (maxAsynchronousTasks <= 0) {
			throw new IllegalArgumentException("maxAsynchronousTasks must be greater than zero");
		}
		if (timeout <= 0) {
			throw new IllegalArgumentException("timeout must be greater than zero");
		}
		if (consecutiveUnhealthyExecutionsAllowed <= 0) {
			throw new IllegalArgumentException("consecutiveUnhealthyExecutionsAllowed must be greater than zero");
		}
		if (executorService == null) {
			throw new IllegalArgumentException("executorService must not be null");
		}
		this.executorService = executorService;
		this.maxAsynchronousTasks = maxAsynchronousTasks;
		this.timeout = timeout;
		this.consecutiveUnhealthyExecutionsAllowed = consecutiveUnhealthyExecutionsAllowed;
		this.exceptionHandler = exceptionHandler;
	}

	public void executeAsynchronously(final Supplier<Callable<?>> taskSupplier) {
		executeAsynchronously(new Iterator<Callable<?>>() {
			Callable<?> current;

			@Override
			public Callable<?> next() {
				Callable<?> peeked = peek();
				current = null;
				return peeked;
			}

			@Override
			public boolean hasNext() {
				return peek() != null;
			}

			private Callable<?> peek() {
				if (current == null) {
					current = taskSupplier.get();
				}
				return current;
			}
		});
	}

	/**
	 * 
	 * @param taskIterator
	 * @throws RejectedExecutionException if the task cannot be scheduled for execution
	 * @throws CustomGeneralException if consecutive batch executions fails or if already running
	 */
	public void executeAsynchronously(final Iterator<Callable<?>> taskIterator) {
		if (!busy.compareAndSet(false, true)) {
			throw new CustomGeneralException("Executor already running");
		}
		final UnboundedAsynchronousExecutor executor = this;
		executeInBackground(() -> {
			int consecutiveUnhealthyExecutions = 0;
			final List<Future<?>> futures = new ArrayList<>();
			while (taskIterator.hasNext()) {
				futures.add(executorService.submit(taskIterator.next()));
				if (futures.size() == maxAsynchronousTasks) {
					boolean unhealthyExecution = !waitFor(futures);
					if (unhealthyExecution) {
						if (++consecutiveUnhealthyExecutions == consecutiveUnhealthyExecutionsAllowed) {
							throw new CustomGeneralException(
									"Consecutive unhealthy batch executions: " + consecutiveUnhealthyExecutionsAllowed);
						}
					} else {
						consecutiveUnhealthyExecutions = 0;
					}
					futures.clear();
				}
			}
			if (!futures.isEmpty()) {
				waitFor(futures);
			}
			/**
			 * reset executor when execution has completed
			 */
			reset();
			/**
			 * notify executor when execution has completed. become the owner of the executor's monitor to notify
			 */
			synchronized (executor) {
				executor.notify();
			}
		});
	}

	private void executeInBackground(Runnable r) {
		Thread thread = new Thread(r);
		thread.start();
	}

	/**
	 * reset counter and set busy flag to false
	 */
	private void reset() {
		counter = 0;
		busy.set(false);
	}

	/**
	 * 
	 * @param requests
	 * @return {@code true} if next pool should be executed
	 */
	private boolean waitFor(Collection<Future<?>> futures) {
		boolean healthyExecution = true;
		for (Future<?> future : futures) {
			try {
				future.get(timeout, TimeUnit.SECONDS);
			} catch (InterruptedException | ExecutionException | TimeoutException e) {
				if (exceptionHandler != null) {
					exceptionHandler.handleException(e, counter);
				}
				healthyExecution = false;
			}
			counter++;
		}
		return healthyExecution;
	}
}
