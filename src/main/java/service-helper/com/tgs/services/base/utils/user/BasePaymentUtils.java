package com.tgs.services.base.utils.user;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.PaymentMedium;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.pms.datamodel.Payment;
import com.tgs.services.pms.datamodel.PaymentAdditionalInfo;
import com.tgs.services.pms.datamodel.PaymentOpType;
import com.tgs.services.pms.datamodel.PaymentRequest;
import com.tgs.services.pms.datamodel.PaymentTransactionType;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BasePaymentUtils {

	public static long minBalanceAllowed;

	protected static boolean storeClosingBalance;

	public static long minPointsBalanceAllowed;

	@Value("${minBalanceAllowed}")
	public void setMinBalanceAllowed(String minBalance) {
		minBalanceAllowed = Long.valueOf(minBalance);
	}

	@Value("${minPointsBalanceAllowed}")
	public void setMinPointsBalanceAllowed(String minPointsBalance) {
		if (StringUtils.isNotEmpty(minPointsBalance) && !"@minPointsBalanceAllowed@".equals(minPointsBalance))
			minPointsBalanceAllowed = Long.valueOf(minPointsBalance);
	}

	@Value("${storeclosingBalance}")
	public void storeclosingBalance(String storeclosingBalance) {
		storeClosingBalance = Boolean.parseBoolean(storeclosingBalance);
		log.info("Value of closing Balance is {} , {}", storeclosingBalance, storeClosingBalance);
	}

	public static boolean isStoreClosingBalance() {
		return storeClosingBalance;
	}

	public static long getMinBalanceAllowed(PaymentOpType opType, PaymentTransactionType txnType) {
		if (opType.equals(PaymentOpType.DEBIT) && txnType.equals(PaymentTransactionType.COMMISSION))
			return minBalanceAllowed;
		return 0;
	}

	public static long getMinPointsBalanceAllowed(boolean isExternalPaymentMedium) {
		return isExternalPaymentMedium ? minPointsBalanceAllowed : 0;
	}

	public static List<PaymentRequest> club(List<PaymentRequest> paymentRequestList) {
		HashMap<String, List<PaymentRequest>> map = new HashMap<>();
		paymentRequestList.forEach(p -> {
			String key = StringUtils.join(p.getPaymentMedium() != null ? p.getPaymentMedium().getCode() : "", "_",
					p.getTransactionType().getCode());
			if (!map.containsKey(key)) {
				List<PaymentRequest> value = new ArrayList<>();
				value.add(p);
				map.put(key, value);
			} else {
				map.get(key).add(p);
			}
		});
		List<PaymentRequest> out = new ArrayList<>();
		for (Map.Entry<String, List<PaymentRequest>> entry : map.entrySet()) {
			List<PaymentRequest> paymentList = entry.getValue();
			PaymentRequest paymentReq = checkAndClub(paymentList);
			if (paymentReq != null)
				out.add(paymentReq);
		}
		return out;
	}

	private static PaymentRequest checkAndClub(List<PaymentRequest> paymentRequestList) {
		PaymentRequest payment = paymentRequestList.get(0);
		paymentRequestList.forEach(p -> {
			if (!validateClub(payment, p)) {
				throw new CustomGeneralException(SystemError.PAYMENT_FAILED);
			}
		});
		PaymentAdditionalInfo additionalInfo =
				PaymentAdditionalInfo.builder().pointsType(payment.getAdditionalInfo().getPointsType()).build();
		PaymentRequest out = PaymentRequest.builder().build().setPayUserId(payment.getPayUserId())
				.setProduct(payment.getProduct()).setRefId(payment.getRefId())
				.setPaymentMedium(payment.getPaymentMedium()).setOriginalPaymentRefId(payment.getOriginalPaymentRefId())
				.setTransactionType(payment.getTransactionType()).setNoOfPoints(payment.getNoOfPoints())
				.setAdditionalInfo(additionalInfo).setTds(payment.getTds() != null ? payment.getTds().negate() : null);
		BigDecimal net = sum(paymentRequestList);
		if (net.compareTo(BigDecimal.ZERO) == 0)
			return null;
		out.setOpType(net.compareTo(BigDecimal.ZERO) > 0 ? PaymentOpType.DEBIT : PaymentOpType.CREDIT);
		out.setAmount(net.abs());
		return out;
	}

	private static boolean validateClub(PaymentRequest paymentRequest1, PaymentRequest paymentRequest2) {
		return (paymentRequest1.getPaymentMedium() == null && paymentRequest2.getPaymentMedium() == null)
				|| (paymentRequest1.getPaymentMedium() != null
						&& paymentRequest1.getPaymentMedium().equals(paymentRequest2.getPaymentMedium()));
	}

	public static BigDecimal sum(List<PaymentRequest> payments) {
		BigDecimal totalAmount = BigDecimal.ZERO;
		for (PaymentRequest payment : payments) {
			if (payment.getOpType().equals(PaymentOpType.DEBIT)) {
				totalAmount = totalAmount.add(payment.getAmount().abs());
			} else {
				totalAmount = totalAmount.subtract(payment.getAmount().abs());
			}
		}
		return totalAmount;
	}

	/**
	 * If client is not comfortable with negative balance (i.e, minBalanceAllowed =0 ), then we will refund the net
	 * amount in case of aborted carts i.e, TF- commission. In this case, txnType will be REVERSE only to calculate the
	 * net logic and there will be only 1 txn. If minBalanceAllowed <0 , then txnType will be COMMISSION, and the txn
	 * entries will be exact reverse of payment entries during order creation
	 * 
	 * @param opType
	 * @param txnType
	 * @return
	 */
	public static PaymentTransactionType getReverseTransactionType(PaymentOpType opType,
			PaymentTransactionType txnType) {
		if (PaymentTransactionType.PARTNER_MARKUP.equals(txnType))
			return txnType;
		long minBalanceAllowed = getMinBalanceAllowed(opType, txnType);
		return minBalanceAllowed < 0 ? txnType : PaymentTransactionType.REVERSE;
	}

	public static PaymentRequest createDebitPaymentRequest(Payment payment) {
		PaymentRequest paymentRequest = PaymentRequest.builder().build().setPayUserId(payment.getPayUserId())
				.setPartnerId(payment.getPartnerId()).setProduct(payment.getProduct()).setOpType(PaymentOpType.DEBIT)
				.setRefId(payment.getRefId()).setOriginalPaymentRefId(payment.getPaymentRefId())
				.setAmount(payment.getAmount().abs()).setTds(payment.getTds())
				.setTransactionType(getReverseTransactionType(PaymentOpType.DEBIT, payment.getType()));
		if (PaymentMedium.POINTS.equals(payment.getPaymentMedium())) {
			paymentRequest.setPaymentMedium(PaymentMedium.POINTS);
			paymentRequest.setNoOfPoints(payment.getAdditionalInfo().getNoOfPoints());
			paymentRequest.getAdditionalInfo().setPointsType(payment.getAdditionalInfo().getPointsType());
		}
		return paymentRequest;
	}


}
