package com.tgs.services.base.utils.user;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.APIKeyHelper;
import com.tgs.services.base.security.SecurityConstants;
import com.tgs.services.base.utils.AtlasUtils;
import com.tgs.services.ums.datamodel.DailyUsageInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserConfiguration;

public class UserUtils {

	public static String EMPTY_USERID = "";

	public static String DEFAULT_PARTNERID = "0";

	public String generateUserId(String prefix, long serialNumber, String suffix) {
		return prefix + serialNumber + suffix;
	}

	public static String generateUserId(long id, UserRole role) {
		return AtlasUtils.generateUserId(id, role);
	}

	public static String getUserId(User user) {
		return user != null ? user.getUserId() : EMPTY_USERID;
	}

	public static boolean isEmptyUserId(String userId) {
		return EMPTY_USERID.equals(userId);
	}

	public static String getRole(User user) {
		return user != null ? user.getRole().getRoleDisplayName() : null;
	}

	public static UserRole getEmulateRole(User user) {
		return user.getEmulateUser() != null ? user.getEmulateUser().getRole() : null;
	}

	public static String getEmulatedUserRoleOrUserRole(User user) {
		return user != null ? ObjectUtils.firstNonNull(getEmulateRole(user), user.getRole()).getRoleDisplayName()
				: null;
	}

	public static String getEmulatedUserRoleOrUserRoleCode(User user) {
		return user != null ? ObjectUtils.firstNonNull(getEmulateRole(user), user.getRole()).getCode() : null;
	}

	public static String getRoleCode(User user) {
		return user != null ? user.getRole().getCode() : null;
	}

	/**
	 * @implSpec : This is has been moved to data modal layer changes should also be there
	 */
	public static double getTdsRate(User user) {
		return user != null && user.getAdditionalInfo() != null && user.getAdditionalInfo().getTdsRate() != null
				? user.getAdditionalInfo().getTdsRate()
				: 0;
	}

	public static double getGstRate(User user) {
		return 0.18;
	}

	public static boolean isMidOfficeRole(String roleCode) {
		if (UserRole.ACCOUNTS.getCode().equals(roleCode) || UserRole.ADMIN.getCode().equals(roleCode)
				|| UserRole.CALLCENTER.getCode().equals(roleCode) || UserRole.SALES.getCode().equals(roleCode)
				|| UserRole.SUPERVISOR.getCode().equals(roleCode)) {
			return true;
		}
		return false;
	}

	public static boolean isMidOfficeRole(UserRole role) {
		if (role == null) {
			return false;
		}
		return isMidOfficeRole(role.getCode());
	}

	public static boolean isMidOfficeUser(User user) {
		if (user == null)
			return false;
		return isMidOfficeRole(user.getRole());
	}

	public static String distributorId(User user) {
		if (user != null && user.getUserConf() != null) {
			return user.getUserConf().getDistributorId();
		}
		return null;
	}

	public static List<String> getUserHierarchy(User user) {
		if (user == null)
			return new ArrayList<>();
		return Arrays.asList(user.getUserId(), user.getRole().name(), "");
	}

	public static DailyUsageInfo getUsageInfo(User user) {
		/**
		 * Don't remove null check because if you will remove null check UI will start showing limit in header for
		 * normal(AGENT and other) users.
		 */
		if (user.getAdditionalInfo().getDailyUsageLimit() != null) {
			return DailyUsageInfo.builder().userId(user.getUserId())
					.balance(user.getAdditionalInfo().getDailyUsageLimit())
					.limit(user.getAdditionalInfo().getDailyUsageLimit()).build();
		}
		return null;
	}

	public static String getPayUserId(User user) {
		if (user == null) {
			return null;
		}
		return ObjectUtils.firstNonNull(user.getParentUserId(), user.getUserId());
	}

	public static String getParentUserId(User user) {
		return user == null ? null : user.getParentUserId();
	}

	public static String getPartnerUserId(User user) {
		return user == null ? null : user.getPartnerId();
	}

	public static UserConfiguration getParentUserConf(User user) {
		return user == null ? null : ObjectUtils.firstNonNull(user.getParentConf(), user.getUserConf());
	}

	public static boolean isAgentStaff(User user) {
		return user != null && StringUtils.isNotBlank(user.getParentUserId())
				&& UserRole.AGENT_STAFF.equals(user.getRole()) ? true : false;
	}

	public static boolean isCorporate(User user) {
		return user != null
				&& (UserRole.CORPORATE.equals(user.getRole()) || UserRole.CORPORATE_STAFF.equals(user.getRole()));
	}

	public static boolean isApiUserRequest(User user) {
		/**
		 * Validating from userConf#ApiConf and Header both due to reason that someone can spoof the request with API
		 * Key then it will be considered as API Partner. Therefore it's required to check from conf and header both.
		 */
		if (user != null && user.getUserConf() != null && user.getUserConf().getApiConfiguration() != null
				&& ((SystemContextHolder.getContextData().getHttpHeaders() != null
						&& SystemContextHolder.getContextData().getHttpHeaders().getApikey() != null
						&& user.getUserConf().getApiConfiguration().getApiKey() != null
						&& SystemContextHolder.getContextData().getHttpHeaders().getApikey()
								.contains(user.getUserConf().getApiConfiguration().getApiKey()))
						|| SystemContextHolder.getContextData().getHttpRequest()
								.getHeader(SecurityConstants.API_HEADER_KEY) != null)) {
			String apiKey = user.getUserConf().getApiConfiguration().getApiKey();
			if (StringUtils.isNotBlank(apiKey) && APIKeyHelper.getAPIUser(apiKey) != null) {
				return true;
			}
		}
		return false;
	}

	public static Integer getUserAirApiVersion(User user) {
		Integer version = null;
		if (user != null && user.getUserConf() != null && user.getUserConf().getApiConfiguration() != null
				&& user.getUserConf().getApiConfiguration().getAirVersion() != null) {
			version = user.getUserConf().getApiConfiguration().getAirVersion();
		}
		return version;
	}

	public static Integer getUserHotelApiVersion(User user) {

		Integer version = null;
		if (user != null && user.getUserConf() != null && user.getUserConf().getApiConfiguration() != null
				&& user.getUserConf().getApiConfiguration().getHotelVersion() != null) {
			version = user.getUserConf().getApiConfiguration().getHotelVersion();
		}
		return version;

	}

	public static boolean isApiUser(User user) {
		if (user != null && user.getUserConf() != null && user.getUserConf().getApiConfiguration() != null) {
			String apiKey = user.getUserConf().getApiConfiguration().getApiKey();
			if (StringUtils.isNotBlank(apiKey) && APIKeyHelper.getAPIUser(apiKey) != null) {
				return true;
			}
		}
		return false;
	}

	public static boolean isPartnerUser(User user) {
		return user != null && !DEFAULT_PARTNERID.equals(user.getPartnerId());
	}

	public static boolean isB2CUser(User user) {
		return user != null && (UserRole.CUSTOMER.equals(user.getRole()) || UserRole.GUEST.equals(user.getRole()));
	}
}
