package com.tgs.services.base.validator;

import java.util.Collection;
import java.util.StringJoiner;
import java.util.function.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.utils.exception.UnAuthorizedException;

@Service
public class ListValidator {

	private static FMSCommunicator fmsCommunicator;
	private static UserServiceCommunicator usCommunicator;

	private static Predicate<String> isAirlineCodeValid, isSupplierIdValid, isUserIdValid, isAirportCodeValid;
	private static Predicate<Integer> isSourceIdValid;

	static {
		isAirlineCodeValid = airlineCode -> StringUtils.isNotBlank(airlineCode)
				&& fmsCommunicator.getAirlineInfo(airlineCode) != null;

		isSupplierIdValid =
				supplierId -> StringUtils.isNotBlank(supplierId) && fmsCommunicator.getSupplierInfo(supplierId) != null;

		isSourceIdValid = sourceId -> sourceId != null && fmsCommunicator.isValidSource(sourceId);

		isUserIdValid = userId -> StringUtils.isNotBlank(userId) && usCommunicator.getUserFromCache(userId) != null;

		isAirportCodeValid = airportCode -> StringUtils.isNotBlank(airportCode)
				&& fmsCommunicator.getAirportInfo(airportCode) != null;
	}

	@Autowired
	public ListValidator(UserServiceCommunicator usCommunicator, FMSCommunicator fmsCommunicator)
			throws UnAuthorizedException {
		ListValidator.usCommunicator = usCommunicator;
		ListValidator.fmsCommunicator = fmsCommunicator;
	}

	public void validateAirlines(Collection<String> airlines, String fieldName, Errors errors,
			SystemError systemError) {
		validateStringList(airlines, fieldName, errors, systemError, isAirlineCodeValid);
	}

	public void validateSupplierIds(Collection<String> supplierIds, String fieldName, Errors errors,
			SystemError systemError) {
		validateStringList(supplierIds, fieldName, errors, systemError, isSupplierIdValid);
	}

	public void validateUserIds(Collection<String> userIds, String fieldName, Errors errors, SystemError systemError) {
		validateStringList(userIds, fieldName, errors, systemError, isUserIdValid);
	}

	public void validateSourceIds(Collection<Integer> sourceIds, String fieldName, Errors errors,
			SystemError systemError) {
		validateList(sourceIds, fieldName, errors, systemError, isSourceIdValid);
	}

	public void validateAirports(Collection<String> airports, String fieldName, Errors errors,
			SystemError systemError) {
		validateList(airports, fieldName, errors, systemError, isAirportCodeValid);
	}

	public void validateStringList(Collection<String> stringList, String fieldName, Errors errors,
			SystemError systemError, Predicate<String> validator) {
		// separate function because of
		// TgsCollectionUtils.isEmptyStringCollection(stringList)
		if (!TgsCollectionUtils.isEmptyStringCollection(stringList)) {
			StringJoiner stringJoiner = new StringJoiner("', '", "'", "'");
			boolean isValid = true;
			for (String str : stringList) {
				str = StringUtils.trim(str);
				if (!validator.test(str)) {
					isValid = false;
					stringJoiner.add(str);
				}
			}
			if (!isValid) {
				rejectValue(errors, fieldName, systemError, stringJoiner.toString());
			}
		}
	}

	public <E> void validateListForNullElements(Collection<E> paxTypes, String fieldName, Errors errors,
			SystemError systemError) {
		validateList(paxTypes, fieldName, errors, systemError, val -> val != null);
	}

	public <E> void validateList(Collection<E> list, String fieldName, Errors errors, SystemError systemError,
			Predicate<E> validator) {
		if (!TgsCollectionUtils.isEmpty(list)) {
			StringJoiner stringJoiner = new StringJoiner("', '", "'", "'");
			boolean isValid = true;
			for (E element : list) {
				if (!validator.test(element)) {
					isValid = false;
					stringJoiner.add(String.valueOf(element));
				}
			}
			if (!isValid) {
				rejectValue(errors, fieldName, systemError, stringJoiner.toString());
			}
		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
