package com.tgs.services.base.validator.response;

import static com.tgs.services.base.enums.UserRole.AGENT;
import static com.tgs.services.base.enums.UserRole.CORPORATE;
import static com.tgs.services.base.enums.UserRole.CORPORATE_EMPLOYEE;
import static com.tgs.services.base.enums.UserRole.DISTRIBUTOR;
import static com.tgs.services.base.enums.UserRole.DISTRIBUTOR_STAFF;
import static java.util.stream.Collectors.toList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import com.google.common.collect.Lists;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;

/*
 * Disabled because Distributor is not able to see payment data after he/she is unassigned from some agents.
 */
// @Service
public class DistributorResponseUserMetaInfoValidator extends ResponseUserMetaInfoValidator {

	final static private List<UserRole> TARGET_ROLES = Lists.newArrayList(DISTRIBUTOR, DISTRIBUTOR_STAFF);
	final static private List<UserRole> FORBIDDEN_ROLES = Lists.newArrayList(CORPORATE, CORPORATE_EMPLOYEE);

	@Override
	protected void validateUserMetaInfo(User targetUser, Map<String, User> usersMetaInfo,
			ResponseValidationResult result) {
		Map<UserRole, List<User>> roleWiseUsers = RoleWiseUserMetaInfoCache.processIfAbsentAndGet(usersMetaInfo);
		List<User> distributorList = roleWiseUsers.getOrDefault(DISTRIBUTOR, Collections.emptyList());
		int distributorCount = distributorList.size();
		User distributorUser = distributorCount > 0 ? distributorList.get(0) : null;

		List<String> parentUserIdList = roleWiseUsers.getOrDefault(DISTRIBUTOR_STAFF, Collections.emptyList()).stream()
				.map(User::getParentUserId).distinct().collect(toList());
		int parentUserCount = parentUserIdList.size();
		String parentUserId = parentUserCount > 0 ? parentUserIdList.get(0) : null;

		/**
		 * if targetUser is neither a DISTRIBUTOR/DISTRIBUTOR_STAFF nor a midOffice user
		 */
		if (!TARGET_ROLES.contains(targetUser.getRole())) {
			NonMidOfficeValidator.builder().distributorCount(distributorCount).distributorUser(distributorUser)
					.parentUserCount(parentUserCount).parentUserId(parentUserId).targetUser(targetUser)
					.usersMetaInfo(usersMetaInfo).build().validate(result);
			return;
		}

		/**
		 * Agent staff doesn't have distributorId. Only agents have distributorId. Parent user of Agent Staff must be
		 * present in usersMetaInfo if possible.
		 */
		List<User> agentList = roleWiseUsers.get(AGENT);

		Validator.builder().distributorCount(distributorCount).distributorUser(distributorUser)
				.parentUserCount(parentUserCount).parentUserId(parentUserId).targetUser(targetUser).agentList(agentList)
				.build().validate(result);
	}

	@Override
	public boolean supports(User targetUser) {
		return targetUser != null && targetUser.getRole() != null && (TARGET_ROLES.contains(targetUser.getRole())
				|| !UserRole.getMidOfficeRoles().contains(targetUser.getRole()));
	}

	@Builder
	private static class Validator {

		private final int distributorCount;
		private final int parentUserCount;
		private User distributorUser;
		private String parentUserId;
		private User targetUser;
		private List<User> agentList;

		private void validate(ResponseValidationResult result) {
			if (distributorCount > 1 || parentUserCount > 1) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of more than 1 DISTRIBUTORs").toString());
			}
			if (distributorUser != null && !distributorUser.getUserId().equals(targetUser.getParentUserId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(userToString(distributorUser)).toString());
			}
			if (parentUserId != null && !parentUserId.equals(targetUser.getParentUserId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(parentUserId).toString());
			}
			if (agentList != null) {
				agentList.forEach(agent -> {
					if (agent.getUserConf() != null
							&& !targetUser.getParentUserId().equals(agent.getUserConf().getDistributorId())) {
						result.addError(new StringBuilder(userToString(targetUser))
								.append(" is able to see data of user ").append(userToString(agent)).toString());
					}
				});
			}
		}
	}

	@Builder
	private static class NonMidOfficeValidator {

		private final int distributorCount;
		private final int parentUserCount;
		private User distributorUser;
		private String parentUserId;
		private User targetUser;
		private Map<String, User> usersMetaInfo;

		private void validate(ResponseValidationResult result) {
			if ((distributorCount + parentUserCount) == 0) {
				return;
			}
			if (FORBIDDEN_ROLES.contains(targetUser.getRole())) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of DISTRIBUTOR/DISTRIBUTOR_STAFF").toString());
				return;
			}
			if (usersMetaInfo.get(targetUser.getParentUserId()) == null) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see DISTRIBUTOR/DISTRIBUTOR_STAFF data that does not belong to him/her")
						.toString());
			}
			if (distributorUser != null
					&& !distributorUser.getUserId().equals(targetUser.getUserConf().getDistributorId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(userToString(distributorUser)).toString());
			}
			if (parentUserId != null && !parentUserId.equals(targetUser.getUserConf().getDistributorId())) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(parentUserId).toString());
			}
			if (distributorCount > 1 || parentUserCount > 1) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of more than 1 DISTRIBUTORs").toString());
			}
		}
	}

	private static String userToString(User user) {
		return new StringBuilder(user.getUserId()).append('(').append(user.getRole()).append(')').toString();
	}

}
