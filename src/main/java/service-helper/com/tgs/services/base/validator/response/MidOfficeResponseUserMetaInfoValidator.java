package com.tgs.services.base.validator.response;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;

/*
 * Disabled because Distributor is not able to see payment data where loggedinUser is MidOffice.
 */
// @Service
public class MidOfficeResponseUserMetaInfoValidator extends ResponseUserMetaInfoValidator {

	@Override
	protected void validateUserMetaInfo(User targetUser, Map<String, User> usersMetaInfo,
			ResponseValidationResult result) {
		Map<UserRole, List<User>> roleWiseUsers = RoleWiseUserMetaInfoCache.processIfAbsentAndGet(usersMetaInfo);
		int midofficeUsersCount = 0;
		for (UserRole role : UserRole.getMidOfficeRoles()) {
			midofficeUsersCount += roleWiseUsers.getOrDefault(role, Collections.emptyList()).size();
		}
		/**
		 * A non-mid-office user cannot see any data which belongs to mid-office user(s) and not to him/her.
		 */
		if (midofficeUsersCount > 0 && usersMetaInfo.get(targetUser.getParentUserId()) == null) {
			result.addError(
					userToString(targetUser) + "is able to see Mid-office data that does not belong to him/her");
		}
	}

	@Override
	public boolean supports(User targetUser) {
		return targetUser != null && !UserRole.getMidOfficeRoles().contains(targetUser.getRole());
	}

	private static String userToString(User user) {
		return new StringBuilder(user.getUserId()).append('(').append(user.getRole()).append(')').toString();
	}

}
