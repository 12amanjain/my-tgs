package com.tgs.services.base.validator.response;

import java.util.Map;
import org.apache.commons.collections.MapUtils;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.ums.datamodel.User;

public abstract class ResponseUserMetaInfoValidator extends ResponseMetaInfoValidator {

	@SuppressWarnings("unchecked")
	@Override
	protected void validateMetaInfo(Map<String, Object> metaInfo, ResponseValidationResult result) {
		Map<String, User> usersMetaInfo = (Map<String, User>) metaInfo.get(UserIdResponseProcessor.META_KEY);

		if (MapUtils.isEmpty(usersMetaInfo)) {
			return;
		}

		// emulated user?
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		if (!supports(loggedInUser)) {
			return;
		}
		validateUserMetaInfo(loggedInUser, usersMetaInfo, result);
	}

	/**
	 * 
	 * @param targetUser
	 * @param usersMetaInfo non-empty map of userId and users
	 * @param result
	 */
	protected abstract void validateUserMetaInfo(User targetUser, Map<String, User> usersMetaInfo,
			ResponseValidationResult result);

	public abstract boolean supports(User targetUser);

}
