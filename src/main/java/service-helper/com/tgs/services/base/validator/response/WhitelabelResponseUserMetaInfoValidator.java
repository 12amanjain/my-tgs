package com.tgs.services.base.validator.response;

import static com.tgs.services.base.enums.UserRole.*;
import static java.util.stream.Collectors.*;
import java.util.List;
import java.util.Map;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.ums.datamodel.User;
import lombok.Builder;

@Service
public class WhitelabelResponseUserMetaInfoValidator extends ResponseUserMetaInfoValidator {

	final static private List<UserRole> TARGET_ROLES = Lists.newArrayList(WHITELABEL_PARTNER, WHITELABEL_STAFF);

	@Override
	protected void validateUserMetaInfo(User targetUser, Map<String, User> usersMetaInfo,
			ResponseValidationResult result) {
		List<String> whitelabelAdminList =
				usersMetaInfo.entrySet().stream().filter(entry -> WHITELABEL_PARTNER.equals(entry.getValue().getRole()))
						.map(Map.Entry::getKey).distinct().collect(toList());
		int whitelabelAdminCount = whitelabelAdminList.size();
		String whitelabelAdminUserId = whitelabelAdminCount > 0 ? whitelabelAdminList.get(0) : null;

		// Mid office users can perform actions on Whitelabel bookings.
		List<String> partnerIdList = usersMetaInfo.values().stream()
				.filter(user -> !WHITELABEL_PARTNER.equals(user.getRole())
						&& !UserRole.getMidOfficeRoles().contains(user.getRole()))
				.map(User::getPartnerId).distinct().collect(toList());
		int partnerIdCount = partnerIdList.size();
		String partnerId = partnerIdCount > 0 ? partnerIdList.get(0) : null;

		Validator.builder().whitelabelAdminCount(whitelabelAdminCount).whitelabelAdminUserId(whitelabelAdminUserId)
				.partnerIdCount(partnerIdCount).partnerId(partnerId).targetUser(targetUser).build().validate(result);
	}

	@Override
	public boolean supports(User targetUser) {
		return targetUser != null && targetUser.getRole() != null && (TARGET_ROLES.contains(targetUser.getRole())
				|| !UserRole.getMidOfficeRoles().contains(targetUser.getRole()));
	}

	@Builder
	private static class Validator {

		private final int whitelabelAdminCount;
		private final int partnerIdCount;
		private String whitelabelAdminUserId;
		private String partnerId;
		private User targetUser;

		private void validate(ResponseValidationResult result) {
			if (whitelabelAdminCount > 1 || partnerIdCount > 1) {
				result.addError(new StringBuilder(userToString(targetUser))
						.append(" is able to see data of more than 1 WHITELABEL PARTNERs").toString());
			}

			String targetUserPartnerId = targetUser.getRole().equals(WHITELABEL_PARTNER) ? targetUser.getUserId()
					: targetUser.getPartnerId();

			if (whitelabelAdminUserId != null && !whitelabelAdminUserId.equals(targetUserPartnerId)) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(whitelabelAdminUserId).toString());
			}
			if (partnerId != null && !partnerId.equals(targetUserPartnerId)) {
				result.addError(new StringBuilder(userToString(targetUser)).append(" is able to see data of user ")
						.append(partnerId).toString());
			}
		}
	}

	private static String userToString(User user) {
		return new StringBuilder(user.getUserId()).append('(').append(user.getRole()).append(')').toString();
	}

}
