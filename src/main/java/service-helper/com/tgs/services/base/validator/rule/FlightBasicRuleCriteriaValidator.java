package com.tgs.services.base.validator.rule;

import java.util.Arrays;
import java.util.List;
import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.PriceFareRange;
import com.tgs.services.base.datamodel.TimePeriod;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.BasicRuleCriteria;
import com.tgs.services.base.validator.ListValidator;
import com.tgs.services.fms.datamodel.Route;
import com.tgs.services.fms.ruleengine.FlightBasicRuleCriteria;

@Service
public class FlightBasicRuleCriteriaValidator {

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	UserServiceCommunicator usCommunicator;

	@Autowired
	ListValidator listValidator;

	public void validateCriteria(Errors errors, String fieldName, BasicRuleCriteria criteria) {
		if (criteria == null) {
			return;
		}

		if (criteria instanceof FlightBasicRuleCriteria) {
			FlightBasicRuleCriteria flightCriteria = (FlightBasicRuleCriteria) criteria;

			List<Route> routes = flightCriteria.getRouteInfoList(); // How? this is just a null check
			if (routes != null) {
				for (Route route : routes) {
					if (route == null) {
						rejectValue(errors, fieldName + ".routeInfoList", SystemError.INVALID_FBRC_ROUTE);
					}
				}
			}

			List<TimePeriod> travelPeriod = flightCriteria.getTravelPeriod();
			if (travelPeriod != null) {
				for (TimePeriod period : travelPeriod) {
					if (period == null || !period.isValid()) {
						rejectValue(errors, fieldName + ".travelPeriod", SystemError.INVALID_FBRC_TRAVELPERIOD);
					}
				}
			}

			List<TimePeriod> bookingPeriod = flightCriteria.getBookingPeriod();
			if (bookingPeriod != null) {
				for (TimePeriod period : bookingPeriod) {
					if (period == null || !period.isValid()) {
						rejectValue(errors, fieldName + ".travelPeriod", SystemError.INVALID_FBRC_BOOKINGPERIOD);
					}
				}
			}

			/**
			 * If airlineList is non-empty and contains wildcard ("*"), it is valid.
			 */
			if (CollectionUtils.isEmpty(flightCriteria.getAirlineList())
					|| !flightCriteria.getAirlineList().contains("*")) {
				listValidator.validateAirlines(flightCriteria.getAirlineList(), fieldName + ".airlineList", errors,
						SystemError.INVALID_FBRC_AIRLINE);
			}

			listValidator.validateListForNullElements(flightCriteria.getPaxTypes(), fieldName + ".paxTypes", errors,
					SystemError.INVALID_FBRC_PAXTYPE);

			listValidator.validateSourceIds(flightCriteria.getSourceIds(), fieldName + ".sourceIds", errors,
					SystemError.INVALID_FBRC_SOURCEID);

			listValidator.validateSupplierIds(flightCriteria.getSupplierIds(), fieldName + ".supplierIds", errors,
					SystemError.INVALID_FBRC_SUPPLIERID);

			listValidator.validateListForNullElements(flightCriteria.getCabinClasses(), fieldName + ".cabinClasses",
					errors, SystemError.INVALID_FBRC_CABIN_CLASS);

			listValidator.validateUserIds(flightCriteria.getUserIds(), fieldName + ".userIds", errors,
					SystemError.INVALID_FBRC_USERID);

			listValidator.validateAirlines(flightCriteria.getOperatingCarriers(), fieldName + ".operatingCarriers",
					errors, SystemError.INVALID_FBRC_OPERATINGAIRLINE);

			String layoverRange = flightCriteria.getLayoverRangeInMin();
			if (StringUtils.isNotBlank(layoverRange)) {
				AtomicBoolean isValid = new AtomicBoolean(false);
				// the regex checks whether the string contains only numeric value
				if (layoverRange.contains("-") && layoverRange.matches("\\d+\\-\\d+")) {
					List<String> range = Arrays.asList(layoverRange.split("-"));
					if (range.size() >= 2
							&& !(StringUtils.isEmpty(range.get(0)) || StringUtils.isEmpty(range.get(1)))) {
						isValid.set(true);
					}
				}
				if (!isValid.get()) {
					rejectValue(errors, fieldName + ".layoverRangeInMin", SystemError.INVALID_LAYOVER_RANGE);
				}
			}

			String allowedDaysOfWeek = flightCriteria.getAllowedDaysOfWeek();
			if (StringUtils.isNotBlank(allowedDaysOfWeek)) {
				listValidator.validateStringList(Arrays.asList(allowedDaysOfWeek.split(",")),
						fieldName + ".allowedDaysOfWeek", errors, SystemError.INVALID_FBRC_ALLOWEDDAYSOFWEEK, day -> {
							try {
								return Integer.valueOf(day) >= 1 && Integer.valueOf(day) <= 7;
							} catch (NumberFormatException e) {
								return false;
							}
						});
			}

			PriceFareRange fareRange = flightCriteria.getPriceFareRange();
			if (fareRange != null) {
				StringJoiner stringJoiner = new StringJoiner("', '", "'", "'");
				AtomicBoolean isValid = new AtomicBoolean(true);
				if (fareRange.getMinAmount() < 0) {
					isValid.set(false);
					stringJoiner.add("minimum Amount cant be negative");
				}
				if (fareRange.getMaxAmount() != fareRange.getMinAmount()
						&& fareRange.getMaxAmount() < fareRange.getMinAmount()) {
					isValid.set(false);
					stringJoiner.add("maximum Amount cant be less than minimum amount");
				}
				if (fareRange.getMaxAmount() != fareRange.getMinAmount()
						&& fareRange.getMinAmount() > fareRange.getMaxAmount()) {
					isValid.set(false);
					stringJoiner.add("minimum Amount cant be greater than maximum amount");
				}
				if (!isValid.get()) {
					rejectValue(errors, fieldName + ".priceFareRange", SystemError.INVALID_FBRC_FARERANGE,
							stringJoiner.toString());
				}
			}

		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}
}
