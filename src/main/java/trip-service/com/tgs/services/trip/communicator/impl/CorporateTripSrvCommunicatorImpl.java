package com.tgs.services.trip.communicator.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.CorporateTripFilter;
import com.tgs.services.base.communicator.CorporateTripServiceCommunicator;
import com.tgs.services.trip.datamodel.CorporateTrip;
import com.tgs.services.trip.dbmodel.DbCorporateTrip;
import com.tgs.services.trip.hibernate.CorporateTripService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CorporateTripSrvCommunicatorImpl implements CorporateTripServiceCommunicator {

	@Autowired
	CorporateTripService tripRequestService;

	@Override
	public void linkBookingIdWithTrip(String tripId, String bookingId, String status) {
		try {
			DbCorporateTrip dbTripRequest = tripRequestService.findByTripId(tripId);
			// Order order = orderSrvCommunicator.findByBookingId(bookingId);
			dbTripRequest.setBookingId(bookingId);
			// Cancellation changes will be done in next phase. Will change this code as per the cancellation flow
			dbTripRequest.setStatus(status);
			tripRequestService.save(dbTripRequest);
		} catch (Exception e) {
			log.error("Exception occurred while linking booking id {} with trip id {} because {} ", bookingId, tripId,
					e.getMessage(), e);
		}
	}

	@Override
	public Map<String, CorporateTrip> findByBookingIds(List<String> bookingIds) {
		Map<String, CorporateTrip> map = new HashMap<>();
		if (CollectionUtils.isNotEmpty(bookingIds)) {
			List<DbCorporateTrip> trips =
					tripRequestService.findAll(CorporateTripFilter.builder().bookingIdIn(bookingIds).build());
			if (CollectionUtils.isNotEmpty(trips)) {
				trips.forEach(trip -> map.put(trip.getBookingId(), trip.toDomain()));
			}
		}
		return map;
	}

}
