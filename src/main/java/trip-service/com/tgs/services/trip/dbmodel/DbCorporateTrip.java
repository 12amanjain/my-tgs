package com.tgs.services.trip.dbmodel;

import java.time.LocalDateTime;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.annotations.UpdateTimestamp;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.trip.datamodel.CorporateAdditionalInfo;
import com.tgs.services.trip.datamodel.CorporateTrip;
import com.tgs.services.trip.datamodel.CorporateTripSearchQuery;
import com.tgs.services.trip.jparepository.CorporateAdditionalInfoType;
import com.tgs.services.trip.jparepository.CorporateTripSearchQueryType;
import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "corptrip")
@TypeDefs({@TypeDef(name = "CorporateTripSearchQueryType", typeClass = CorporateTripSearchQueryType.class),
	@TypeDef(name = "CorporateAdditionalInfoType", typeClass = CorporateAdditionalInfoType.class)})
@Setter
@Getter
public class DbCorporateTrip extends BaseModel<DbCorporateTrip, CorporateTrip> {

	@Column
	private String userId;
	
	@Column
	private String modificationType;
	
	@Column
	private String tripId;
	
	@Column
	private String bookingId;
	
	@Column
	private String status;
	
	@Type(type = "CorporateTripSearchQueryType")
	@Column
	private CorporateTripSearchQuery tripSearchQuery;
	
	@Type(type = "CorporateAdditionalInfoType")
	@Column
	private CorporateAdditionalInfo additionalInfo;
	
	@CreationTimestamp
	private LocalDateTime createdOn;
	
	@UpdateTimestamp
	private LocalDateTime processedOn;
	
	@Column
	private Boolean isPushed;

	public CorporateTrip toDomain() {
		return new GsonMapper<>(this, CorporateTrip.class).convert();
	}

	public DbCorporateTrip from(CorporateTrip dataModel) {
		return new GsonMapper<>(dataModel, this, DbCorporateTrip.class).convert();
	}
}
