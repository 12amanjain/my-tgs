package com.tgs.services.trip.hibernate;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import com.tgs.services.trip.dbmodel.DbCorporateTrip;

public interface CorporateTripRepository extends JpaRepository<DbCorporateTrip, Long>, JpaSpecificationExecutor<DbCorporateTrip> {

	DbCorporateTrip findById(Long id);
	
	DbCorporateTrip findByTripId(String tripId);

}
