package com.tgs.services.trip.hibernate;

import java.time.LocalDateTime;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.CorporateTripFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.trip.dbmodel.DbCorporateTrip;

@Service
public class CorporateTripService extends SearchService<DbCorporateTrip> {

	@Autowired
	private CorporateTripRepository repo;

	public DbCorporateTrip save(DbCorporateTrip tripRequest) {
		tripRequest.setProcessedOn(LocalDateTime.now());
		tripRequest = repo.save(tripRequest);
		return tripRequest;
	}

	public DbCorporateTrip findByTripId(String tripId) {
		DbCorporateTrip tripRequest = repo.findByTripId(tripId);
		return tripRequest;
	}

	public List<DbCorporateTrip> findAll(CorporateTripFilter filter) {
		return search(filter, repo);
	}
}
