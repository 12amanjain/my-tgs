package com.tgs.services.trip.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.CorporateTripFilter;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.trip.datamodel.CorporateTrip;
import com.tgs.services.trip.restmodel.CorporateTripResponse;
import com.tgs.services.trip.restmodel.CorporateTripStatusUpdate;
import com.tgs.services.trip.servicehandler.CorporateTripHandler;

@RestController
@RequestMapping("/trip/v1")
public class CorporateTripController {

	@Autowired
	private CorporateTripHandler tripHandler;
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected @ResponseBody CorporateTripResponse saveTrip(HttpServletRequest request, HttpServletResponse response, @RequestBody CorporateTrip tripRequest) throws Exception {
		tripHandler.initData(tripRequest, new CorporateTripResponse());
		return tripHandler.getResponse();
	}
	
	@RequestMapping(value = "/list", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = UserIdResponseProcessor.class)
	protected @ResponseBody CorporateTripResponse viewTrip(HttpServletRequest request, HttpServletResponse response, @RequestBody CorporateTripFilter filter) {
		filter.setUserIdIn(UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), filter.getUserIdIn()));
		return tripHandler.list(filter);
	}
	
	@RequestMapping(value = "/status", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse updateStatus(HttpServletRequest request, HttpServletResponse response, @RequestBody CorporateTripStatusUpdate tripStatusUpdate) throws Exception {
		return tripHandler.updateTripStatus(tripStatusUpdate.getTripIdIn());
	}
}
