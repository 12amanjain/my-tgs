package com.tgs.services.ums.communicator.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.UserServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.DailyUsageInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.helper.AreaRoleHelper;
import com.tgs.services.ums.helper.UserFeeHelper;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.helper.UserRelationshipHelper;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.servicehandler.RegistrationHandler;
import com.tgs.services.ums.servicehandler.UserFetchHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserServiceCommunicatorImpl implements UserServiceCommunicator {


	@Autowired
	private UserService usrService;

	@Autowired
	private RegistrationHandler registrationHandler;

	@Override
	public List<User> getUsers(UserFilter userFilter) {
		try {
			UserFetchHandler userFetchHandler = SpringContext.getApplicationContext().getBean(UserFetchHandler.class);
			userFetchHandler.initData(userFilter, new UserResponse());
			return userFetchHandler.getResponse().getUsers();
		} catch (Exception e) {
			return new ArrayList<>();
		}
	}

	@Override
	public User getUser(UserFilter userFilter) {
		List<User> users = getUsers(userFilter);
		if (CollectionUtils.isNotEmpty(users)) {
			return users.get(0);
		}
		return null;
	}

	@Override
	public User getUserFromCache(String userId) {
		return UserHelper.getUserFromCache(userId);
	}

	@Override
	public String getUserPolicy(String userId) {
		return UserHelper.getUserFromCache(userId).getUserConf().getCreditPolicyMap().get(Product.AIR);
	}

	@Override
	public boolean updateUserInCache(String userId) {
		UserHelper.saveInCache(usrService.findByUserId(userId), null);
		return true;
	}

	@Override
	public List<UserFee> getUserFee(String userId, Product product, UserFeeType feeType) {
		return UserFeeHelper.getUserFee(userId, product, feeType);
	}

	@Override
	public List<String> getAllowedUserIds(String userId) {
		User user = getUserFromCache(userId);
		// WhiteLabel staff should be able to see all the data of WL Partner.
		if (UserRole.WHITELABEL_STAFF.equals(user.getRole())) {
			return getAllowedUserIds(user.getPartnerId());
		}
		// Corporate Staff can see all employees data
		if (UserRole.CORPORATE_STAFF.equals(user.getRole())) {
			return getAllowedUserIds(user.getParentUserId());
		}
		List<String> allowedIds = UserHelper.getAllowedUserIds(userId);
		/**
		 * This is to ensure that apart from mid office team , no one would be able to see data of any other users which
		 * are not mapped to that userId
		 */
		if (CollectionUtils.isEmpty(allowedIds) && !UserUtils.isMidOfficeRole(
				UserUtils.getEmulatedUserRoleOrUserRoleCode(SystemContextHolder.getContextData().getUser()))) {
			allowedIds = Arrays.asList(userId);
		}
		return allowedIds;
	}

	@Override
	public List<String> getAllowedPartnerIds(String userId, boolean canSeePartnerData) {
		return UserHelper.getAllowedWhiteLabelUserIds(userId, canSeePartnerData);
	}

	@Override
	public Map<String, User> getUsersFromCache(UserCacheFilter userFilter) {
		return UserHelper.getUserMap(userFilter);
	}

	@Override
	public List<AreaRoleMapping> getAreaRoleMapByAreaRole(Collection<AreaRole> areaRoleList) {
		return AreaRoleHelper.getMappingsByAreaRoleIn(areaRoleList);
	}

	@Override
	public Set<AreaRole> getAllowedAreaRoles(ApplicationArea area, User user) {
		List<AreaRoleMapping> areaRoleMappingList = getAreaRoleMapByAreaRole(area.getAreaRoles());
		return AreaRoleMapping.allowedAreaRoles(areaRoleMappingList, user);
	}

	@Override
	public void lockAccount(String userId) {
		usrService.lockUser(userId);
	}

	@Override
	public void unlockAccount(String userId) {
		usrService.unlockUser(userId);
	}

	@Override
	public DailyUsageInfo getDailyUsageInfo(String userId) {
		return UserHelper.getUsageInfo(userId);
	}

	@Override
	public void storeDailyUsageInfo(DailyUsageInfo usageInfo) {
		UserHelper.updateDailyInfo(usageInfo);
	}

	@Override
	public Map<String, Set<User>> getUserRelations(List<String> userIds) {
		return UserRelationshipHelper.getUserRelationsForUserId1(userIds);
	}

	@Override
	public User getUserFromUsrService(UserFilter userFilter) {
		DbUser dbuser = usrService.findByUserId(userFilter.getUserId());
		if (dbuser != null) {
			return dbuser.toDomain();
		} else
			return null;
	}

	@Override
	public User updateUser(User user) {
		DbUser dbUser = usrService.findByUserId(user.getUserId());
		dbUser = new GsonMapper<>(user, dbUser, DbUser.class).convert();
		return usrService.save(dbUser).toDomain();
	}

	@Override
	public User getUser(String apiKey) {
		List<User> users = usrService.find(UserCacheFilter.builder().apiKey(apiKey).build());
		if (CollectionUtils.isNotEmpty(users)) {
			return users.get(0);
		}
		return null;
	}

	@Override
	public User registerUser(RegisterRequest request) throws Exception {
		registrationHandler.initData(request, new UserResponse());
		UserResponse response = registrationHandler.getResponse();
		if (response != null)
			return response.getUsers().get(0);
		return null;
	}

	@Override
	public void updateProfile(String userId, UserProfile profile) {
		usrService.updateProfile(userId, profile);
	}

	@Override
	public Map<String, Set<String>> getJWTTokensOfUsers(List<String> userIds) {
		return UserHelper.getJWTTokensOfUsers(userIds);
	}

}
