package com.tgs.services.ums.dbmodel;

import java.time.LocalDateTime;
import java.util.Objects;
import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.datamodel.AddressInfo;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.dbmodel.SuperBaseModel;
import com.tgs.services.ums.datamodel.ContactPersonInfo;
import com.tgs.services.ums.datamodel.GSTInfo;
import com.tgs.services.ums.datamodel.PanInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import com.tgs.services.ums.datamodel.UserConfiguration;
import com.tgs.services.ums.hibernate.AddressInfoUserType;
import com.tgs.services.ums.hibernate.ContactPersonInfoUserType;
import com.tgs.services.ums.hibernate.GSTInfoUserType;
import com.tgs.services.ums.hibernate.PanInfoUserType;
import com.tgs.services.ums.hibernate.UserAdditionalInfoUserType;
import com.tgs.services.ums.hibernate.UserConfigurationType;
import com.tgs.services.ums.hibernate.UserProfileType;
import lombok.Data;

@Entity(name = "users")
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = {"email", "mobile", "role"})})
@Audited
@Data
@TypeDefs({@TypeDef(name = "GSTInfoUserType", typeClass = GSTInfoUserType.class),
		@TypeDef(name = "AddressInfoUserType", typeClass = AddressInfoUserType.class),
		@TypeDef(name = "PanInfoUserType", typeClass = PanInfoUserType.class),
		@TypeDef(name = "ContactPersonInfoUserType", typeClass = ContactPersonInfoUserType.class),
		@TypeDef(name = "UserAdditionalInfoUserType", typeClass = UserAdditionalInfoUserType.class),
		@TypeDef(name = "UserConfigurationType", typeClass = UserConfigurationType.class),
		@TypeDef(name = "UserProfileType", typeClass = UserProfileType.class)})
public class DbUser extends SuperBaseModel<DbUser, User> {

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_generator")
	@SequenceGenerator(name = "user_generator", sequenceName = "users_id_seq", allocationSize = 1)
	private Long id;
	@Column
	private String userId;
	@Column
	private String partnerId;
	@Column
	private String name;
	@Column
	private String email;
	@Column(length = 1000)
	private String password;
	@Column
	private String mobile;
	@Column
	private String phone;

	@Column
	private String status;

	private String role;


	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;

	@Column
	@Type(type = "GSTInfoUserType")
	private GSTInfo gstInfo;

	@Column
	@Type(type = "AddressInfoUserType")
	private AddressInfo addressInfo;

	@Column
	@Type(type = "PanInfoUserType")
	private PanInfo panInfo;

	@Column
	@Type(type = "ContactPersonInfoUserType")
	private ContactPersonInfo contactPersonInfo;

	@Column
	@Type(type = "UserAdditionalInfoUserType")
	private UserAdditionalInfo additionalInfo;

	@Column
	@Type(type = "UserConfigurationType")
	private UserConfiguration userConf;

	@Column
	private String parentUserId;

	@Column
	@Type(type = "UserProfileType")
	private UserProfile userProfile;

	@Column
	private String employeeId;

	@Access(value = AccessType.PROPERTY)
	@Column
	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;

	}

	public UserAdditionalInfo getAdditionalInfo() {
		if (Objects.isNull(additionalInfo)) {
			additionalInfo = UserAdditionalInfo.builder().build();
		}
		return additionalInfo;
	}

	public AddressInfo getAddressInfo() {
		if (Objects.isNull(addressInfo)) {
			addressInfo = AddressInfo.builder().build();
		}
		return addressInfo;
	}

	public PanInfo getPanInfo() {
		if (Objects.isNull(panInfo)) {
			panInfo = new PanInfo();
		}
		return panInfo;
	}

	public GSTInfo getGSTInfo() {
		if (Objects.isNull(gstInfo)) {
			gstInfo = GSTInfo.builder().build();
		}
		return gstInfo;
	}

	public ContactPersonInfo getContactPersonInfo() {
		if (Objects.isNull(contactPersonInfo)) {
			contactPersonInfo = new ContactPersonInfo();
		}
		return contactPersonInfo;
	}

	public void cleanData() {
		if (additionalInfo != null)
			additionalInfo.cleanData();
		if (userConf != null)
			userConf.cleanData();
	}

	public UserProfile getUserProfile() {
		return userProfile == null ? UserProfile.builder().build() : userProfile;
	}

	@Override
	public User toDomain() {
		return new GsonMapper<>(this, User.class).convert();
	}

	@Override
	public DbUser from(User dataModel) {
		return new GsonMapper<>(dataModel, this, DbUser.class).convert();
	}
}
