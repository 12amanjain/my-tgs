package com.tgs.services.ums.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import com.google.gson.annotations.JsonAdapter;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.JsonStringSerializer;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeAmount;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name="userfee")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@TypeDefs({@TypeDef(name = "UserFeeAmountType", typeClass = UserFeeAmountType.class)})
public class DbUserFee extends BaseModel<DbUserFee, UserFee> {

    @Column
    @CreationTimestamp
    private LocalDateTime createdOn;
    
    @CreationTimestamp
	private LocalDateTime processedOn;
    @Column
    private String userId;
    @Column
    private String product;
    @Column
    private String feeType;

    @Column
    private Boolean enabled;
    
    @Column
    private Boolean isDeleted;

    @Column
    @JsonAdapter(JsonStringSerializer.class)
    private String inclusionCriteria;

    @Type(type = "UserFeeAmountType")
    private UserFeeAmount userFee;
    
    @Override
	public boolean equals(Object obj) {
		if (super.equals(obj)) {
			return true;
		}

		if (obj == null || !(obj instanceof DbUserFee)) {
			return false;
		}

		DbUserFee otherRequest = (DbUserFee) obj;

		return getKey().equals(otherRequest.getKey());
	}

	private String getKey() {
		StringBuilder sb = new StringBuilder();
		sb.append(userId).append(product).append(feeType).append(inclusionCriteria);
		return sb.toString();
	}

	@Override
	public UserFee toDomain() {
		return new GsonMapper<>(this, UserFee.class, true).convert();
	}
	
	@Override
	public DbUserFee from(UserFee dataModel) {
		return new GsonMapper<>(dataModel, this, DbUserFee.class).convert();
	}
}
