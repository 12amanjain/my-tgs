package com.tgs.services.ums.dbmodel;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.envers.Audited;

import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ums.datamodel.UserRelation;

import lombok.Getter;
import lombok.Setter;

@Entity(name = "userrelation")
@Table(name = "userrelation")
@Setter
@Getter
@Audited
public class DbUserRelation extends BaseModel<DbUserRelation, UserRelation> {

	@Column
	private String userId1;
	
	@Column
	private String userId2;
	
	@Column
	private String userName1;
	
	@Column
	private String userName2;
	
	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;
	
	@Column
	private Integer depth;
	
	@Column
	private Integer priority;
	
	@Override
    public UserRelation toDomain() {
        return new GsonMapper<>(this, UserRelation.class).convert();
    }

    @Override
    public DbUserRelation from(UserRelation dataModel) {
        return new GsonMapper<>(dataModel, this, DbUserRelation.class).convert();
    }
	
}
