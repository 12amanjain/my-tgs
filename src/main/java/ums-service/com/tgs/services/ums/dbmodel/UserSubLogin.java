package com.tgs.services.ums.dbmodel;

import javax.persistence.Column;
import javax.persistence.Entity;

import com.tgs.services.base.dbmodel.BaseModel;

@Entity
public class UserSubLogin extends BaseModel {

	@Column
	private String userId;
	@Column
	private String userName;
	@Column
	private String password;
	@Column
	private Boolean enabled;

}
