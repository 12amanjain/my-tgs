package com.tgs.services.ums.helper;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ims.datamodel.air.AirRatePlan;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.AreaRoleMappingInfo;
import com.tgs.services.ums.jparepository.AreaRoleMapService;

@Service
public class AreaRoleHelper extends InMemoryInitializer {

    private static final String FIELD = "arearole";

    private static CustomInMemoryHashMap areaRoleMappingHashMap;

    @Autowired
    private AreaRoleMapService roleMapService;

    @Autowired
    public AreaRoleHelper(CustomInMemoryHashMap inMemoryHashMap, CustomInMemoryHashMap areaRoleMappingHashMap) {
        super(inMemoryHashMap);
        AreaRoleHelper.areaRoleMappingHashMap = areaRoleMappingHashMap;
    }

    @Override
    public void process() {
        List<AreaRoleMapping> areaRoleMappingList = roleMapService.getAll();
        areaRoleMappingList.forEach(mapping -> updateAreaRoleMapping(mapping));
    }
    
    public static void updateAreaRoleMapping(AreaRoleMapping areaRoleMapping) {
    	areaRoleMappingHashMap.put(areaRoleMapping.getAreaRole().name(), FIELD,
    			areaRoleMapping.getMappingInfo(), CacheMetaInfo.builder().set(CacheSetName.AREA_ROLE.getName())
                        .expiration(InMemoryInitializer.NEVER_EXPIRE).compress(false).build());
    }
    
    public static void deleteAreaRoleMappingFromCache(AreaRoleMapping areaRoleMapping) {
    	areaRoleMappingHashMap.delete(areaRoleMapping.getAreaRole().name(), CacheMetaInfo.builder().set(CacheSetName.AREA_ROLE.getName()).build());
    }

    @Override
    public void deleteExistingInitializer() {
    	areaRoleMappingHashMap.truncate(CacheSetName.AREA_ROLE.getName());
    }


    public static List<AreaRoleMapping> getMappingsByAreaRoleIn(Collection<AreaRole> areaRoles) {
        List<AreaRoleMapping> areaRoleMappingList = new ArrayList<>();
        areaRoles.forEach(role -> {
            AreaRoleMapping roleMapping = new AreaRoleMapping();
            if(areaRoleMappingHashMap.get(role.name(), FIELD, AreaRoleMappingInfo.class,
                    CacheMetaInfo.builder().set(CacheSetName.AREA_ROLE.getName()).build()) != null) {
            	roleMapping.setAreaRole(role);
                roleMapping.setMappingInfo(areaRoleMappingHashMap.get(role.name(), FIELD, AreaRoleMappingInfo.class,
                        CacheMetaInfo.builder().set(CacheSetName.AREA_ROLE.getName()).build()));
                areaRoleMappingList.add(roleMapping);
            }
        });
        return areaRoleMappingList;
    }
}
