package com.tgs.services.ums.helper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import com.tgs.services.ums.dbmodel.DbUserFee;
import com.tgs.services.ums.jparepository.UserFeeService;

@Service
public class UserFeeHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap userFeeRequests;
	private static final String FIELD = "user_fee";
	private static Map<String, List<UserFee>> inMemoryUserFeeMap = new HashMap<>();

	@Autowired
	UserFeeService userFeeService;

	public UserFeeHelper(CustomInMemoryHashMap configurationHashMap, CustomInMemoryHashMap userFeeRequests) {
		super(configurationHashMap);
		UserFeeHelper.userFeeRequests = userFeeRequests;
	}

	@Override
	public void process() {
		processInMemory();
		if (MapUtils.isNotEmpty(inMemoryUserFeeMap)) {
			inMemoryUserFeeMap.forEach((key, value) -> {
				userFeeRequests.put(key, FIELD, value, CacheMetaInfo.builder().set(CacheSetName.USER_FEE.getName())
						.compress(true).expiration(InMemoryInitializer.NEVER_EXPIRE).build());
			});
		}
	}

	@Scheduled(fixedDelay = 300 * 1000, initialDelay = 5 * 1000)
	public void processInMemory() {
		Runnable task = () -> {
			store(DbUserFee.toDomainList(userFeeService.findByLastInserted()));
		};
		Thread taskThread = new Thread(task);
		taskThread.start();
	}

	public void store(List<UserFee> userRequests) {
		if (CollectionUtils.isNotEmpty(userRequests)) {
			Map<String, List<UserFee>> requestMap =
					userRequests.stream().collect(Collectors.groupingBy(userRequest -> getKey(userRequest)));
			inMemoryUserFeeMap.putAll(requestMap);
		}
	}

	public void update(DbUserFee dbUserFee) {
		List<UserFee> userRequests =
				DbUserFee.toDomainList(userFeeService.find(dbUserFee.getFeeType(), dbUserFee.getUserId()));
		inMemoryUserFeeMap.remove(getKey(dbUserFee.toDomain()));
		if (!CollectionUtils.isEmpty(userRequests)) {
			store(userRequests);
		}
	}

	/**
	 * @param userFeeRequest
	 * @return
	 */
	public static String getKey(UserFee userFeeRequest) {
		return StringUtils.join(userFeeRequest.getUserId(), userFeeRequest.getProduct().getCode(),
				userFeeRequest.getFeeType().getCode());
	}

	@Override
	public void deleteExistingInitializer() {
		if (MapUtils.isNotEmpty(inMemoryUserFeeMap))
			inMemoryUserFeeMap.clear();
		userFeeRequests.truncate(CacheSetName.USER_FEE.getName());
	}

	public static List<UserFee> getUserFee(String userId, Product product, UserFeeType feeType) {
		List<UserFee> list = new ArrayList<>();
		String key = StringUtils.join(userId, product.getCode(), feeType.getCode());
		if (MapUtils.isNotEmpty(inMemoryUserFeeMap)) {
			List<UserFee> cachedUserfees = inMemoryUserFeeMap.get(key);
			if (CollectionUtils.isNotEmpty(cachedUserfees)) {
				cachedUserfees.forEach(item -> {
					if (!BooleanUtils.isTrue(item.getIsDeleted())) {
						UserFee uf = item.toBuilder().build();
						list.add(uf);
					}
				});
			}
		}
		return list;


	}

}
