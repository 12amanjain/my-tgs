package com.tgs.services.ums.helper;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.aerospike.client.query.Filter;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.utils.TgsMapUtils;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.datamodel.DailyUsageInfo;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	private UserService userService;

	@Autowired
	public UserHelper(CustomInMemoryHashMap supplierHashMap, GeneralCachingCommunicator cachingCommunicator) {
		super(configurationHashMap);
		UserHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		List<DbUser> userList = new ArrayList<>();
		Runnable task = () -> {
			for (int i = 0; i < 500; i++) {

				UserFilter filter = UserFilter.builder().build();
				PageAttributes pageAttr = new PageAttributes();
				pageAttr.setPageNumber(i);
				pageAttr.setSize(5000);
				SortByAttributes srtBy = new SortByAttributes();
				srtBy.setParams(Arrays.asList("id"));
				srtBy.setOrderBy("asc");
				pageAttr.setSortByAttr(Arrays.asList(srtBy));
				filter.setPageAttr(pageAttr);
				List<DbUser> userChunk = userService.search(filter);
				log.info("[UserHelper Thread] Fetched user info from database, info list size is {} ",
						userChunk.size());
				if (CollectionUtils.isEmpty(userChunk))
					break;
				userList.addAll(userChunk);
			}
		};
		Thread dataThread = new Thread(task);
		dataThread.start();
		try {
			dataThread.join();
		} catch (InterruptedException e) {
			log.error("Unable to join UserHelper thread ", e);
		}

		Map<String, Set<String>> allowedUserIds = getAllowUserMap(userList);
		for (DbUser user : userList) {
			saveInCache(user, allowedUserIds.get(user.getUserId()));
		}
	}

	public static Map<String, Set<String>> getAllowUserMap(List<DbUser> userList) {
		Map<String, Set<String>> allowedUserIds = new HashMap<>();

		Set<String> supplierIds = new HashSet<>();
		for (DbUser dbUser : userList) {
			User user = dbUser.toDomain();
			if (dbUser.getRole().equals(UserRole.CUSTOMER.getCode())
					|| dbUser.getRole().equals(UserRole.GUEST.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).add(dbUser.getUserId());
			} else if (dbUser.getRole().equals(UserRole.AGENT_STAFF.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getParentUserId(), new HashSet<>())
						.add(dbUser.getUserId());
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>())
						.add(dbUser.getParentUserId());
				allowedUserIds.get(dbUser.getUserId()).add(dbUser.getUserId());
			} else if (dbUser.getRole().equals(UserRole.CORPORATE_EMPLOYEE.getCode())|| 
					dbUser.getRole().equals(UserRole.CORPORATE_STAFF.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getParentUserId(), new HashSet<>())
						.add(dbUser.getUserId());
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).add(dbUser.getUserId());
			} else if (dbUser.getRole().equals(UserRole.AGENT.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).add(dbUser.getUserId());
				if (StringUtils.isNotBlank(UserUtils.distributorId(user))) {
					TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserConf().getDistributorId(), new HashSet<>())
							.add(dbUser.getUserId());
				} else if (!UserUtils.DEFAULT_PARTNERID.equals(dbUser.getPartnerId())) {
					TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getPartnerId(), new HashSet<>())
							.add(dbUser.getUserId());
				}
			} else if (dbUser.getRole().equals(UserRole.WHITELABEL_STAFF.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).add(dbUser.getUserId());
				if (!UserUtils.DEFAULT_PARTNERID.equals(dbUser.getPartnerId())) {
					TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getPartnerId(), new HashSet<>())
							.add(dbUser.getUserId());
				}
			} else if (dbUser.getRole().equals(UserRole.SUPPLIER.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).add(dbUser.getUserId());
				supplierIds.add(dbUser.getUserId());
			} else if (dbUser.getRole().equals(UserRole.SUPPLIER_ADMIN.getCode())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).addAll(supplierIds);
			} else if (dbUser.getRole().equals(UserRole.SALES.getCode())) {
				Map<String, Set<User>> relations = UserRelationshipHelper
						.getUserRelationsForUserId2(Arrays.asList(dbUser.getUserId()));
				Set<String> userIds = relations.get(dbUser.getUserId()) != null
						? relations.get(dbUser.getUserId()).stream().map(u -> u.getUserId()).collect(Collectors.toSet())
						: new HashSet<>();
				userIds.add(dbUser.getUserId());
				allowedUserIds.put(dbUser.getUserId(), userIds);
			} else if (!UserUtils.isMidOfficeRole(dbUser.getRole())) {
				TgsMapUtils.getOrDefault(allowedUserIds, dbUser.getUserId(), new HashSet<>()).add(dbUser.getUserId());
			}
		}
		return allowedUserIds;
	}

	public void updateCache(DbUser user) {
		Map<String, Set<String>> allowedUserIdMap = getAllowUserMap(Arrays.asList(user));

		if (MapUtils.isEmpty(allowedUserIdMap)) {
			saveInCache(user, new HashSet<>());
		} else {
			allowedUserIdMap.forEach((k, v) -> {
				try {
					Set<String> existingAllowedIds = new HashSet<String>(getAllowedUserIds(k, new ArrayList<>()));
					if (CollectionUtils.isEmpty(existingAllowedIds)) {
						existingAllowedIds = new HashSet<>();
					}
					existingAllowedIds.addAll(v);
					saveInCache(userService.search(UserFilter.builder().userId(k).build()).get(0), existingAllowedIds);
				} catch (Exception e) {
					log.error("Unable to update cache for userId {}", k, e);
				}
			});
		}
	}

	public void updateCache(String userId) {
		updateCache(userService.findByUserId(userId));
	}

	public static void saveInCache(DbUser dbUser, Set<String> allowedUserIds) {
		if (dbUser == null) {
			return;
		}
		User user = dbUser.toDomain();
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.USERID.getName(), dbUser.getUserId());
		binMap.put(BinName.ROLE.getName(), dbUser.getRole());
		binMap.put(BinName.USERINFO.getName(), GsonUtils.getGson().toJson(user));
		if (Objects.nonNull(dbUser.getUserConf()) && dbUser.getUserConf().getApiConfiguration() != null) {
			binMap.put(BinName.APIKEY.getName(), dbUser.getUserConf().getApiConfiguration().getApiKey());
		}
		if (CollectionUtils.isNotEmpty(allowedUserIds)) {
			binMap.put(BinName.ALLOWEDUSERID.getName(), GsonUtils.getGson().toJson(allowedUserIds));
		}
		cachingCommunicator.store(CacheMetaInfo.builder().set(CacheSetName.USER_DATA.getName())
				.namespace(CacheNameSpace.USERS.getName()).key(dbUser.getUserId()).build(), binMap, false, true, -1);
	}

	public static User getUser(User user) {
		return getUserFromCache(user.getUserId());
	}

	public static User getUserFromCache(String userId) {
		return getUserMap(UserCacheFilter.builder().userIds(Arrays.asList(userId)).build()).get(userId);
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_DATA.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	/**
	 * This method returns list of users from cache by passing the list of user ids
	 * 
	 * @param userFilter
	 * @return
	 */
	public static Map<String, User> getUserMap(UserCacheFilter userFilter) {
		Map<String, User> users = new HashMap<>();
		Map<String, Map<String, String>> userMap = null;
		CacheMetaInfo metaInfo = null;
		if (CollectionUtils.isNotEmpty(userFilter.getUserIds())) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
					.set(CacheSetName.USER_DATA.getName()).keys(userFilter.getUserIds().toArray(new String[0])).build();
			userMap = cachingCommunicator.get(metaInfo, String.class);
		} else if (userFilter.getRole() != null) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
					.set(CacheSetName.USER_DATA.getName()).build();
			userMap = cachingCommunicator.getResultSet(metaInfo, String.class,
					Filter.equal(BinName.ROLE.getName(), userFilter.getRole()));
		} else if (userFilter.getApiKey() != null) {
			metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
					.set(CacheSetName.USER_DATA.getName()).build();
			userMap = cachingCommunicator.getResultSet(metaInfo, String.class,
					Filter.equal(BinName.APIKEY.getName(), userFilter.getApiKey()));
		}

		if (MapUtils.isNotEmpty(userMap)) {
			for (Entry<String, Map<String, String>> entrySet : userMap.entrySet()) {
				users.put(entrySet.getKey(),
						GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.USERINFO.getName()), User.class));
			}
		}

		return users;
	}

	public static List<String> getAllowedUserIds(String userId) {
		User user = getUserFromCache(userId);
		//Whitelabel staff can see all data of Whitelabel partner
		if (UserRole.WHITELABEL_STAFF.equals(user.getRole())) {
			userId = user.getPartnerId();
		}
		return getAllowedUserIds(userId, null);
	}

	@SuppressWarnings("unchecked")
	public static List<String> getAllowedUserIds(String userId, List<String> defaultValue) {
		List<String> allowedUserIds = null;
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.USER_DATA.getName()).keys(Arrays.asList(userId).toArray(new String[0])).build();
		Map<String, Map<String, String>> userMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(userMap)) {
			for (Entry<String, Map<String, String>> entrySet : userMap.entrySet()) {
				if (entrySet.getValue().get(BinName.ALLOWEDUSERID.getName()) != null) {
					allowedUserIds = new ArrayList<>();
					return GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.ALLOWEDUSERID.getName()),
							allowedUserIds.getClass());
				}
			}
		}
		return Optional.ofNullable(allowedUserIds).orElse(defaultValue);
	}

	public static List<String> getAllowedWhiteLabelUserIds(String userId, boolean canSeePartnerData) {
		List<String> allowedPartnerIds = null;
		User user = getUserFromCache(userId);
		if (UserRole.WHITELABEL_PARTNER.equals(user.getRole())) {
			// UserUtils.DEFAULT_PARTNERID is added so that WL Partner is able to view his own
			// data. He is not able to view other user;s data will be handled through
			// getAllowedUserIds check
			allowedPartnerIds = Arrays.asList(user.getUserId(),UserUtils.DEFAULT_PARTNERID);
		} else if (UserRole.WHITELABEL_STAFF.equals(user.getRole())) {
			allowedPartnerIds = Arrays.asList(user.getPartnerId());
		}else if (UserRole.getMidOfficeRoles().contains(user.getRole()) && !canSeePartnerData) {
			allowedPartnerIds = Arrays.asList(UserUtils.DEFAULT_PARTNERID);
		}
		return allowedPartnerIds;
	}

	/**
	 * This method will return users from Cache
	 * 
	 * @param userCacheFilter
	 * @return
	 */
	public List<User> getUsersFromCache(UserCacheFilter userCacheFilter) {
		Map<String, User> userMap = getUserMap(userCacheFilter);
		List<User> userList = new ArrayList<>();
		if (MapUtils.isNotEmpty(userMap)) {
			for (Map.Entry<String, User> entry : userMap.entrySet()) {
				userList.add(entry.getValue());
			}
		}
		return userList;
	}

	public static void updateDailyInfo(DailyUsageInfo usage) {
		Map<String, String> binMap = new HashMap<>();
		binMap.put(BinName.USAGE.getName(), GsonUtils.getGson().toJson(usage));
		long expiration = ChronoUnit.SECONDS.between(LocalDateTime.now(), LocalDate.now().atTime(LocalTime.MAX));

		cachingCommunicator.store(
				CacheMetaInfo.builder().set(CacheSetName.DAILY_USAGE.getName())
						.namespace(CacheNameSpace.USERS.getName()).key(usage.getUserId()).build(),
				binMap, false, true, (int) expiration);
	}

	public static DailyUsageInfo getUsageInfo(String userId) {
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
				.set(CacheSetName.DAILY_USAGE.getName()).keys(Arrays.asList(userId).toArray(new String[0])).build();

		Map<String, Map<String, String>> userMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(userMap)) {
			for (Entry<String, Map<String, String>> entrySet : userMap.entrySet()) {
				if (entrySet.getValue().get(BinName.USAGE.getName()) != null) {
					return GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.USAGE.getName()),
							DailyUsageInfo.class);
				}
			}
		}
		return null;
	}

	public static void removeUserJWTTokens(String userId) {
		Map<String, Set<String>> userIdWiseTokens = getJWTTokensOfUsers(Arrays.asList(userId));
		if (MapUtils.isNotEmpty(userIdWiseTokens) && userIdWiseTokens.get(userId) != null) {
			Set<String> accessTokens = userIdWiseTokens.get(userId);
			accessTokens.forEach(token -> {
				cachingCommunicator.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
						.set(CacheSetName.JWT.getName()).key(token).build());
			});
		}
	}

	public static Map<String, Set<String>> getJWTTokensOfUsers(List<String> userIds) {
		Map<String, Set<String>> userIdWiseTokens = new HashMap<>();
		userIds.forEach(userId -> {
			Map<String, Map<String, String>> accessTokens = cachingCommunicator
					.getResultSet(
							CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
									.set(CacheSetName.JWT.getName()).build(),
							String.class, Filter.equal(BinName.USERID.getName(), userId));
			if (MapUtils.isNotEmpty(accessTokens)) {
				userIdWiseTokens.put(userId, accessTokens.keySet());
			}
		});
		return userIdWiseTokens;
	}
}
