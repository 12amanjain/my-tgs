package com.tgs.services.ums.helper;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.gson.reflect.TypeToken;
import com.tgs.filters.UserRelationFilter;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.cacheservice.datamodel.BinName;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.jparepository.UserRelationService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserRelationshipHelper extends InMemoryInitializer {

	@Autowired
	private static GeneralCachingCommunicator cachingCommunicator;

	@Autowired
	UserRelationService userService;

	public UserRelationshipHelper(GeneralCachingCommunicator cachingCommunicator) {
		super(null);
		UserRelationshipHelper.cachingCommunicator = cachingCommunicator;
	}

	@Override
	public void process() {
		storeUserRelationships(UserRelationFilter.builder().depth(1).build(),
				CacheSetName.USER_RELATION_USERID1.getName());
		storeUserRelationships(UserRelationFilter.builder().depth(1).build(),
				CacheSetName.USER_RELATION_USERID2.getName());
	}

	@Override
	public void deleteExistingInitializer() {
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_RELATION_USERID1.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
		cachingCommunicator.truncate(CacheMetaInfo.builder().set(CacheSetName.USER_RELATION_USERID2.getName())
				.namespace(CacheNameSpace.USERS.getName()).build());
	}

	public void storeUserRelationships(UserRelationFilter filter, String setName) {
		Runnable fetchUsrRltnTask = () -> {
			List<DbUserRelation> userList = new ArrayList<>();
			for (int i = 0; i < 500; i++) {
				PageAttributes pageAttr = new PageAttributes();
				pageAttr.setPageNumber(i);
				pageAttr.setSize(1000);
				SortByAttributes srtBy = new SortByAttributes();
				srtBy.setParams(CacheSetName.USER_RELATION_USERID1.getName().equals(setName)
						? Arrays.asList("userId1", "priority","id")
						: Arrays.asList("userId2","id"));
				srtBy.setOrderBy("asc");
				pageAttr.setSortByAttr(Arrays.asList(srtBy));
				filter.setPageAttr(pageAttr);
				List<DbUserRelation> batchList = userService.search(filter);
				if (CollectionUtils.isEmpty(batchList))
					break;
				else
					userList.addAll(batchList);
			}
			Map<String, LinkedHashSet<User>> userMap = new HashMap<>();
			userList.forEach(user -> {
				User temp1 = User.builder()
						.userId(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? user.getUserId2()
								: user.getUserId1())
						.name(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? user.getUserName2()
								: user.getUserName1())
						.build();
				updateMap(CacheSetName.USER_RELATION_USERID1.getName().equals(setName) ? user.getUserId1()
						: user.getUserId2(), temp1, userMap);
			});
			userMap.forEach((k, v) -> {
				saveInCache(k, v, setName);
			});
		};
		Thread fetchusrRltnThread = new Thread(fetchUsrRltnTask);
		fetchusrRltnThread.start();
		try {
			fetchusrRltnThread.join();
		} catch (InterruptedException e) {
			log.error("Unable to join UserRelationshipHelper thread ", e);
		}
	}
	
	private void updateMap(String key, User user, Map<String, LinkedHashSet<User>> userMap) {
		if (userMap.get(key) == null) {
			LinkedHashSet<User> userSet = new LinkedHashSet<>();
			userSet.add(user);
			userMap.put(key, userSet);
		} else {
			userMap.get(key).add(user);
		}
	}

	public static void deleteFromCacheForUserId1(DbUserRelation userRelation) {
		deleteFromCache(CacheSetName.USER_RELATION_USERID1.getName(), userRelation.getUserId1(),
				userRelation.getUserId2());
	}

	public static void deleteFromCacheForUserId2(DbUserRelation userRelation) {
		deleteFromCache(CacheSetName.USER_RELATION_USERID2.getName(), userRelation.getUserId2(),
				userRelation.getUserId1());
	}

	public static void deleteFromCache(String setName, String key, String value) {
		Map<String, Set<User>> existingRelations = getUserRelations(Arrays.asList(key), setName);
		Set<User> existingUsers = existingRelations.get(key);
		existingUsers.removeIf(r -> r.getUserId().equals(value));
		if (!existingUsers.isEmpty())
			saveInCache(key, existingUsers, setName);
		else
			deleteEntryFromCache(key, setName);
	}

	public static void saveInCache(String userId, Set<User> userRelations, String setName) {
		if (StringUtils.isBlank(userId)) {
			return;
		}
		if (CollectionUtils.isNotEmpty(userRelations)) {
			Map<String, String> binMap = new HashMap<>();
			binMap.put(BinName.USERRELATION.getName(), GsonUtils.getGson().toJson(userRelations));
			cachingCommunicator.store(
					CacheMetaInfo.builder().set(setName).namespace(CacheNameSpace.USERS.getName()).key(userId).build(),
					binMap, false, true, -1);
		}
	}

	public static void deleteEntryFromCache(String userId, String setName) {
		cachingCommunicator.delete(
				CacheMetaInfo.builder().set(setName).namespace(CacheNameSpace.USERS.getName()).key(userId).build());
	}

	public static Map<String, Set<User>> getUserRelationsForUserId1(List<String> userIds) {
		return getUserRelations(userIds, CacheSetName.USER_RELATION_USERID1.getName());
	}

	public static Map<String, Set<User>> getUserRelationsForUserId2(List<String> userIds) {
		return getUserRelations(userIds, CacheSetName.USER_RELATION_USERID2.getName());
	}

	public static Map<String, Set<User>> getUserRelations(List<String> userIds, String setName) {
		Map<String, Set<User>> superiors = new HashMap<>();
		CacheMetaInfo metaInfo = CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName()).set(setName)
				.keys(userIds.toArray(new String[0])).build();
		Map<String, Map<String, String>> userMap = cachingCommunicator.get(metaInfo, String.class);
		if (MapUtils.isNotEmpty(userMap)) {
			for (Entry<String, Map<String, String>> entrySet : userMap.entrySet()) {
				if (entrySet.getValue().get(BinName.USERRELATION.getName()) != null) {
					Set<User> userRelations =
							GsonUtils.getGson().fromJson(entrySet.getValue().get(BinName.USERRELATION.getName()),
									new TypeToken<Set<User>>() {}.getType());
					superiors.put(entrySet.getKey(), userRelations);
				}
			}
		}
		return superiors;
	}
}
