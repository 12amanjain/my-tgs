package com.tgs.services.ums.helper.analytics;

import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.analytics.QueueData;
import com.tgs.services.analytics.QueueDataType;
import com.tgs.services.base.BaseAnalyticsQuery;
import com.tgs.services.base.communicator.ElasticSearchCommunicator;
import com.tgs.services.base.communicator.KafkaServiceCommunicator;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.gson.Mapper;
import com.tgs.services.base.utils.thread.ExecutorUtils;
import com.tgs.services.es.datamodel.ESMetaInfo;
import com.tgs.services.ums.datamodel.UserAnalyticsType;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserAnalyticsHelper {

	@Autowired
	ElasticSearchCommunicator esStackCommunicator;

	@Autowired
	KafkaServiceCommunicator kafkaService;

	public <T extends BaseAnalyticsQuery> void pushToAnalytics(Mapper<T> mapper, UserAnalyticsType userAnalyticsType) {
		pushToAnalytics(mapper, userAnalyticsType, ESMetaInfo.USER_INFO);
	}

	public <T extends BaseAnalyticsQuery> void pushToAnalytics(Mapper<T> mapper, UserAnalyticsType userAnalyticsType,
			ESMetaInfo metaInfo) {
		ExecutorUtils.getAnalyticsThreadPool().submit(() -> {
			T userQuery = null;
			try {
				userQuery = mapper.convert();
				userQuery.setAnalyticstype(userAnalyticsType.name());
				QueueData queueData = QueueData.builder().key(metaInfo.getIndex())
						.value(GsonUtils.getGson().toJson(userQuery)).build();
				List<QueueDataType> queueTypes = Arrays.asList(QueueDataType.ELASTICSEARCH);
				kafkaService.queue(queueTypes, queueData);
			} catch (Exception e) {
				log.error("Failed to Push Analytics type {} ", userAnalyticsType.name(), e);
			}
		});
	}

}
