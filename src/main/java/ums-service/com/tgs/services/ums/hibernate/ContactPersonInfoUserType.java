package com.tgs.services.ums.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ums.datamodel.ContactPersonInfo;

public class ContactPersonInfoUserType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return ContactPersonInfo.class;
	}

}
