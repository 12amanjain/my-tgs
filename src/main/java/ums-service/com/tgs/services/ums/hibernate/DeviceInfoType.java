package com.tgs.services.ums.hibernate;

import com.tgs.services.base.datamodel.DeviceInfo;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;

public class DeviceInfoType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return DeviceInfo.class;
	}
}
