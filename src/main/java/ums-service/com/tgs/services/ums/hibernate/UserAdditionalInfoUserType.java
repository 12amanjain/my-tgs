package com.tgs.services.ums.hibernate;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;
import org.springframework.util.ObjectUtils;

import com.google.gson.Gson;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;

public class UserAdditionalInfoUserType implements UserType {

	@Override
	public int[] sqlTypes() {
		return new int[] { Types.JAVA_OBJECT };
	}

	@Override
	public Class returnedClass() {
		return UserAdditionalInfo.class;
	}

	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		return ObjectUtils.nullSafeEquals(x, y);
	}

	@Override
	public int hashCode(Object x) throws HibernateException {
		if (x == null) {
			return 0;
		}

		return x.hashCode();
	}

	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
			throws HibernateException, SQLException {
		Gson gson = GsonUtils.getGson();
		PGobject o = (PGobject) rs.getObject(names[0]);
		if (o != null && o.getValue() != null) {
			return gson.fromJson(o.getValue(), UserAdditionalInfo.class);
		}

		return null;
	}

	@Override
	public void nullSafeSet(PreparedStatement st, Object value, int index, SharedSessionContractImplementor session)
			throws HibernateException, SQLException {
		Gson gson = GsonUtils.getGson();
		if (value == null) {
			st.setNull(index, Types.OTHER);
		} else {
			st.setObject(index, gson.toJson(value, UserAdditionalInfo.class), Types.OTHER);
		}
	}

	@Override
	public Object deepCopy(Object value) throws HibernateException {
		Gson gson = GsonUtils.getGson();
		return gson.fromJson(gson.toJson(value), UserAdditionalInfo.class);
	}

	@Override
	public boolean isMutable() {
		return true;
	}

	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		Object copy = deepCopy(value);

		if (copy instanceof Serializable) {
			return (Serializable) copy;
		}
		return null;
	}

	@Override
	public Object assemble(Serializable cached, Object owner) throws HibernateException {
		return deepCopy(cached);
	}

	@Override
	public Object replace(Object original, Object target, Object owner) throws HibernateException {
		return deepCopy(original);
	}

}
