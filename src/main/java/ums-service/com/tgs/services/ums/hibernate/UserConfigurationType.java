package com.tgs.services.ums.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.ums.datamodel.UserConfiguration;

public class UserConfigurationType extends CustomUserType {

	@Override
	public Class returnedClass() {
		return UserConfiguration.class;
	}

}