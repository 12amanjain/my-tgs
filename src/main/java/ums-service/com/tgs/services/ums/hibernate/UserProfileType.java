package com.tgs.services.ums.hibernate;

import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.base.datamodel.UserProfile;

public class UserProfileType extends CustomUserType {

    @Override
    public Class returnedClass() {
        return UserProfile.class;
    }
}
