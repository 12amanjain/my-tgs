package com.tgs.services.ums.jparepository;

import com.tgs.services.ums.dbmodel.DbAreaRoleMapping;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.List;
import java.util.Optional;

public interface AreaRoleMapRepository extends JpaRepository<DbAreaRoleMapping, Long>, JpaSpecificationExecutor<DbAreaRoleMapping> {

    List<DbAreaRoleMapping> findByAreaRoleIn(List<String> areaRoleList);

    DbAreaRoleMapping findByAreaRole(String key);
}
