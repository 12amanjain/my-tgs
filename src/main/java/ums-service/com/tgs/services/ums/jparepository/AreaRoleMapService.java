package com.tgs.services.ums.jparepository;

import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.dbmodel.DbAreaRoleMapping;

import org.springframework.beans.factory.annotation.Autowired;
import com.tgs.services.base.SearchService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class AreaRoleMapService extends SearchService<DbAreaRoleMapping> {

	@Autowired
	private AreaRoleMapRepository repository;

	public List<AreaRoleMapping> getMappingsByAreaRoleIn(Collection<AreaRole> areaRoleList) {
		List<String> roleListString = areaRoleList.stream().map(Enum::name).collect(Collectors.toList());
		return BaseModel.toDomainList(repository.findByAreaRoleIn(roleListString));
	}
	
	public DbAreaRoleMapping save(DbAreaRoleMapping areaRoleMapping) {
		areaRoleMapping = repository.save(areaRoleMapping);
		return  areaRoleMapping;
	}

	public List<AreaRoleMapping> getAll() {
		return BaseModel.toDomainList(repository.findAll());
	}
	
	public DbAreaRoleMapping getMappingsByAreaRole(AreaRole areaRole) {
		DbAreaRoleMapping mapping = repository.findByAreaRole(areaRole.toString());
		return mapping;
	}

	public void deleteMappingsById(Long id) {
		repository.delete(id);
	}

	public DbAreaRoleMapping findById(Long id) {
		return repository.findOne(id);
	}
}