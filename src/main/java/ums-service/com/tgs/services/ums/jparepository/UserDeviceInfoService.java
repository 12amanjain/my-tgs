package com.tgs.services.ums.jparepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.filters.UserDeviceInfoFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.ums.dbmodel.DBUserDeviceInfo;
import com.tgs.services.ums.restmodel.UserDeviceListResponse;

@Service
public class UserDeviceInfoService extends SearchService<DBUserDeviceInfo> {

	@Autowired
	private UserDeviceInfoRepository deviceRepo;

	public DBUserDeviceInfo save(DBUserDeviceInfo userDeviceInfo) {
		userDeviceInfo = deviceRepo.saveAndFlush(userDeviceInfo);
		return userDeviceInfo;
	}

	public UserDeviceListResponse findAll(UserDeviceInfoFilter filter) {
		UserDeviceListResponse response = new UserDeviceListResponse();
		List<DBUserDeviceInfo> dbUserDeviceInfoList = super.search(filter, deviceRepo);
		response.setUserDeviceInfo(DBUserDeviceInfo.toDomainList(dbUserDeviceInfoList));
		return response;
	}

}
