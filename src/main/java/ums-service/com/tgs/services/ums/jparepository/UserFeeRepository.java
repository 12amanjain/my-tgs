package com.tgs.services.ums.jparepository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.tgs.services.ums.dbmodel.DbUserFee;

@Repository
public interface UserFeeRepository extends JpaRepository<DbUserFee, Long>, JpaSpecificationExecutor<DbUserFee> {

	public List<DbUserFee> findByOrderByProcessedOnDesc();

	public List<DbUserFee> findByFeeTypeAndUserIdAndIsDeletedOrderByProcessedOnDesc(String feeType, String userId, Boolean isDeleted);

	@Query(value = "SELECT a.* FROM userfee a WHERE a.userid = :userId and isDeleted = false", nativeQuery = true)
	public List<DbUserFee> getUserFeesForUser(@Param("userId") String userId);

}
