package com.tgs.services.ums.jparepository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import com.tgs.services.ums.dbmodel.DbUserFee;
import com.tgs.services.ums.helper.UserFeeHelper;

@Service
public class UserFeeService {

	@Autowired
	private UserFeeRepository userFeeRepository;

	@Autowired
	private UserFeeHelper userFeeHelper;

	public DbUserFee save(DbUserFee dbRequest) {
		if (!SystemContextHolder.getContextData().getHttpHeaders().isPersistenceNotRequired()) {
			dbRequest.setProcessedOn(LocalDateTime.now());
			dbRequest = userFeeRepository.save(dbRequest);
		}
		userFeeHelper.update(dbRequest);
		return dbRequest;
	}

	public DbUserFee fetchById(Long id) {
		return userFeeRepository.findOne(id);
	}

	public List<DbUserFee> find(String type, String userId) {
		return userFeeRepository.findByFeeTypeAndUserIdAndIsDeletedOrderByProcessedOnDesc(UserFeeType.getEnumFromCode(type).getCode(),
				userId, false);
	}

	public List<DbUserFee> findByLastInserted() {
		return userFeeRepository.findByOrderByProcessedOnDesc();
	}
	
	public List<DbUserFee> getUserFeesForUser(String userId) {
		return userFeeRepository.getUserFeesForUser(userId);
	}

}
