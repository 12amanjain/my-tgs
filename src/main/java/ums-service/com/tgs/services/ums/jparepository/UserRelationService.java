package com.tgs.services.ums.jparepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.UserRelationFilter;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.helper.UserRelationshipHelper;

@Service
public class UserRelationService extends SearchService<DbUserRelation> {

	@Autowired
	private UserRelationRepository relationsRepo;

	@Autowired
	private UserRelationshipHelper relationHelper;
	
	@PersistenceContext
	EntityManager em;

	public DbUserRelation save(final DbUserRelation relations) {
		relationsRepo.saveAndFlush(relations);
		String userId1 = relations.getUserId1();
		List<DbUserRelation> existingRelations = relationsRepo.findByUserId2(userId1);
		if (!existingRelations.isEmpty()) {
			existingRelations.forEach(relation -> {
				DbUserRelation relationship = ObjectUtils.firstNonNull(relationsRepo.findByUserId1AndUserId2(relation.getUserId1(), relations.getUserId2()), new DbUserRelation());
				relationship.setUserId2(relations.getUserId2());
				relationship.setUserName2(relations.getUserName2());
				relationship.setDepth(relation.getDepth() + 1);
				relation.setPriority(relation.getPriority() + 1);
				relationship.setUserId1(relation.getUserId1());
				relationship.setUserName1(relation.getUserName1());
				relationsRepo.saveAndFlush(relationship);
			});
		}
		relationHelper.storeUserRelationships(UserRelationFilter.builder()
				.userId1In(Collections.singletonList(relations.getUserId1())).depth(1).build(),CacheSetName.USER_RELATION_USERID1.getName());
		relationHelper.storeUserRelationships(UserRelationFilter.builder()
				.userId2In(Collections.singletonList(relations.getUserId2())).depth(1).build(),CacheSetName.USER_RELATION_USERID2.getName());
		return relations;
	}

	@Override
	public List<DbUserRelation> search(QueryFilter filter) {
		return super.search(filter, relationsRepo);
	}

	public DbUserRelation findByUserId1AndDepthAndPriority(String userId1, int depth, int priority) {
		return relationsRepo.findByUserId1AndDepthAndPriority(userId1, depth, priority);
	}

	public DbUserRelation findByUserId1AndDepthAndUserId2(String userId1, int depth, String userId2) {
		return relationsRepo.findByUserId1AndDepthAndUserId2(userId1, depth, userId2);
	}
	
	public DbUserRelation findByUserId1AndUserId2(String userId1, String userId2) {
		return relationsRepo.findByUserId1AndUserId2(userId1, userId2);
	}
	
	public List<DbUser> findRelatedAncestors(String childId) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		String query = "Select {u.*}, {r.*} FROM userrelation r JOIN users u ON r.userid1 = u.userid where r.userid2 = '"
				+ childId + "' order by depth";
		List<Object[]> items = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
				.addEntity("u", DbUser.class).addEntity("r", DbUserRelation.class).getResultList();
		List<DbUser> users = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(items))
			items.forEach(item -> users.add((DbUser) item[0]));
		entityManager.close();
		return users;
	}

	public List<DbUser> findRelatedDescendents(String parentId) {
		EntityManager entityManager = em.getEntityManagerFactory().createEntityManager();
		String query = "Select {u.*}, {r.*} FROM userrelation r JOIN users u ON r.userid2 = u.userid where r.userid2 = '"
				+ parentId + "' order by depth";
		List<Object[]> items = entityManager.unwrap(Session.class).createNativeQuery(query.toString())
				.addEntity("u", DbUser.class).addEntity("r", DbUserRelation.class).getResultList();
		List<DbUser> users = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(items))
			items.forEach(item -> users.add((DbUser) item[0]));
		entityManager.close();
		return users;
	}

	public List<DbUserRelation> findByUserId1(String userId) {
		return relationsRepo.findByUserId1(userId);
	}

	public void delete(DbUserRelation relation) {
		relationsRepo.delete(relation);
	}

}
