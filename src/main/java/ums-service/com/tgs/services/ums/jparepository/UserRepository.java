package com.tgs.services.ums.jparepository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.tgs.services.ums.dbmodel.DbUser;

@Repository
public interface UserRepository extends JpaRepository<DbUser, Long>, JpaSpecificationExecutor<DbUser> {

	public DbUser findByEmail(String email);

	public DbUser findByMobile(String mobile);

	public DbUser findByMobileAndRole(String mobile, String role);

	public DbUser findByEmailOrMobileAndRole(String email, String mobile, String role);

	public DbUser findByUserIdOrEmailOrMobile(String userId, String email, String mobile);

	public DbUser findByEmailAndMobileAndRole(String mobile, String email, String role);

	public DbUser findByEmailAndRole(String email, String role);

	public DbUser findByPartnerIdAndEmail(String partnerId, String email);

	public DbUser findByPartnerIdAndMobile(String partnerId, String mobile);

	public DbUser findByPartnerIdAndUserId(String partnerId, String userId);

	public DbUser findByPartnerIdAndEmailAndRole(String partnerId, String email, String role);

	public DbUser findByPartnerIdAndEmailAndRoleNot(String partnerId, String email, String role);

	public DbUser findByPartnerIdAndMobileAndRole(String partnerId, String mobile, String role);

	public DbUser findByPartnerIdAndMobileAndRoleNot(String partnerId, String mobile, String role);

	public DbUser findByPartnerIdAndUserIdAndRole(String partnerId, String userId, String role);

	public DbUser findByPartnerIdAndUserIdAndRoleNot(String partnerId, String userId, String role);


	public DbUser findByEmailOrMobile(String email, String mobile);

	public DbUser findByNameLike(String name);

	public DbUser findByUserId(String userId);

	public DbUser findByRole(String role);

	@Query(value = "SELECT nextval('users_id_seq')", nativeQuery = true)
	public Long getNextSeriesId();

	// @Query("SELECT a FROM Article a WHERE a.title=:title and
	// a.category=:category")
	// List<Article> fetchArticles(@Param("title") String title, @Param("category")
	// String category);

	// To Configure timeout as 8 seconds to execute query without readOnly flag for
	// search() method.
	// @Override
	// @Transactional(timeout = 8)
	// Iterable<Article> search();

	// @Async
	// @Query("SELECT t.title FROM Todo t where t.id = :id")
	// Future<String> findTitleById(@Param("id") Long id);
}
