package com.tgs.services.ums.jparepository;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.apache.commons.collections.CollectionUtils;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.runtime.database.PsqlFunction;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.utils.springframework.data.SpringDataUtils;

public enum UserSearchPredicate {
	ROLE {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getRoles())) {
				Expression<String> exp = root.get("role");
				List<String> userRoles = UserRole.getCodes(filter.getRoles());
				if (!userRoles.isEmpty())
					predicates.add(exp.in(userRoles));
			}
		}
	},
	ROLE_NOT {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (!CollectionUtils.isEmpty(filter.getRoleNotIn())) {
				Expression<String> exp = root.get("role");
				List<String> userRoles = UserRole.getCodes(filter.getRoleNotIn());
				if (!userRoles.isEmpty())
					predicates.add(exp.in(userRoles).not());
			}
		}
	},
	NAME {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getName() != null) {
				predicates.add(criteriaBuilder.or(
						criteriaBuilder.like(criteriaBuilder.lower(root.get("userId")),
								"%" + filter.getName().toLowerCase() + "%"),
						criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
								"%" + filter.getName().toLowerCase() + "%")));
			}
		}
	},
	USER_ID {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getUserId() != null)
				predicates.add(criteriaBuilder.equal(root.get("userId"), filter.getUserId()));
		}
	},
	EMPLOYEE_LIKE {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getEmployeeLike() != null)
				predicates.add(criteriaBuilder.or(
						criteriaBuilder.like(criteriaBuilder.lower(root.get("userId")),
								"%" + filter.getEmployeeLike().toLowerCase() + "%"),
						criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
								"%" + filter.getEmployeeLike().toLowerCase() + "%"),
						criteriaBuilder.like(criteriaBuilder.lower(root.get("email")),
								"%" + filter.getEmployeeLike().toLowerCase() + "%"),
						criteriaBuilder.like(criteriaBuilder.lower(root.get("employeeId")),
								"%" + filter.getEmployeeLike().toLowerCase() + "%")));
		}
	},
	EMPLOYEE_ID {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getEmployeeId() != null)
				predicates.add(criteriaBuilder.equal(root.get("employeeId"), filter.getEmployeeId()));
		}
	},
	PARENT_USER_ID {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getParentUserId() != null)
				predicates.add(criteriaBuilder.equal(root.get("parentUserId"), filter.getParentUserId()));
		}
	},
	DISTRIBUTOR_ID {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getDistributorId() != null) {
				Expression<String> didExpression = PsqlFunction.JSONB_EXTRACT_PATH_TEXT.getExpression(criteriaBuilder,
						String.class, root.get("userConf"), criteriaBuilder.literal("dId"));
				predicates.add(criteriaBuilder.equal(didExpression, filter.getDistributorId()));
			}
		}
	},
	EMAIL {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getEmail() != null)
				predicates.add(criteriaBuilder.equal(root.get("email"), filter.getEmail()));
		}
	},
	MOBILE {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (filter.getMobile() != null)
				predicates.add(criteriaBuilder.equal(root.get("mobile"), filter.getMobile()));
		}
	},
	STATUS {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getStatuses())) {
				Expression<String> exp = root.get("status");
				List<String> userStatuses = UserStatus.getCodes(filter.getStatuses());
				if (!userStatuses.isEmpty())
					predicates.add(exp.in(userStatuses));
			}
		}
	},
	USER_IDS {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getUserIds())) {
				Expression<String> exp = root.get("userId");
				predicates.add(exp.in(filter.getUserIds()));
			}
		}
	},
	PARTNER_IDS {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			if (CollectionUtils.isNotEmpty(filter.getPartnerIdIn())) {
				Expression<String> exp = root.get("partnerId");
				predicates.add(exp.in(filter.getPartnerIdIn()));
			}
		}
	},
	CREATED_ON {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {
			SpringDataUtils.setCreatedOnPredicateUsingQueryFilter(root, query, criteriaBuilder, filter, predicates);
		}
	},
	PROCESSED_ON {
		@Override
		public void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
				UserFilter filter, List<Predicate> predicates) {

			if (filter.getProcessedOnLessThan() != null) {
				predicates.add(criteriaBuilder.lessThan(root.get("processedOn"), filter.getProcessedOnLessThan()));
			}
			if (filter.getProcessedOnGreaterThan() != null) {
				predicates
						.add(criteriaBuilder.greaterThan(root.get("processedOn"), filter.getProcessedOnGreaterThan()));
			}
		}
	};


	public abstract void addPredicate(Root<DbUser> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder,
			UserFilter filter, List<Predicate> predicates);

	public static List<Predicate> getPredicateListBasedOnUserFilter(Root<DbUser> root, CriteriaQuery<?> query,
			CriteriaBuilder criteriaBuilder, UserFilter filter) {
		List<Predicate> predicates = new ArrayList<>();
		for (UserSearchPredicate userCol : UserSearchPredicate.values()) {
			userCol.addPredicate(root, query, criteriaBuilder, filter, predicates);
		}
		return predicates;
	}
}
