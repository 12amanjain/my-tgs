package com.tgs.services.ums.jparepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.Predicate;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.tgs.filters.UserFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.datamodel.QueryFilter;
import com.tgs.services.base.datamodel.UserProfile;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.database.DependantSearchResolver;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.utils.springframework.data.SpringDataUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class UserService extends SearchService<DbUser> {

	@Autowired
	private UserRepository userRepo;

	@Autowired
	private UserHelper userHelper;

	@PersistenceContext
	EntityManager em;

	@Transactional
	public DbUser save(DbUser user) {
		user.setProcessedOn(LocalDateTime.now());
		user.cleanData();
		if (StringUtils.isEmpty(user.getPartnerId()))
			user.setPartnerId(UserUtils.DEFAULT_PARTNERID);
		user = userRepo.saveAndFlush(user);
		userHelper.updateCache(user);
		return user;
	}

	@Transactional
	public void updateProfile(String userId, UserProfile profile) {
		DbUser user = findByUserId(userId);
		if (user == null)
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		if (profile.getData() == null)
			return; // profile can be empty if no field is visible on pax page
		user.getUserProfile().getData().putAll(profile.getData());
		user.setProcessedOn(LocalDateTime.now());
		userRepo.saveAndFlush(user);
		userHelper.updateCache(user);
	}

	@Transactional
	public DbUser update(DbUser user) {
		user.setProcessedOn(LocalDateTime.now());
		user = userRepo.saveAndFlush(user);
		userHelper.updateCache(user);
		return user;
	}

	@Transactional
	public void save(User user) {
		user.setProcessedOn(LocalDateTime.now());
		DbUser dbUser = new DbUser().from(user);
		dbUser = userRepo.saveAndFlush(dbUser);
		userHelper.updateCache(dbUser);
	}

	public void lockUser(String userId) {
		DbUser dbUser = findByUserId(userId);
		dbUser.setStatus(UserStatus.DEACTIVATED.getCode());
		save(dbUser);
	}

	public void unlockUser(String userId) {
		DbUser dbUser = findByUserId(userId);
		if (dbUser.getStatus().equals(UserStatus.DEACTIVATED.getCode())) {
			dbUser.setStatus(UserStatus.ENABLED.getCode());
			save(dbUser);
		}
	}


	public DbUser findByEmail(String email) {
		return userRepo.findByEmail(email);
	}

	public DbUser findByMobile(String mobile) {
		return userRepo.findByMobile(mobile);
	}

	public DbUser findByMobileAndRole(String mobile, String role) {
		return userRepo.findByMobileAndRole(mobile, role);
	}

	public DbUser findByEmailAndRole(String email, String role) {
		return userRepo.findByEmailAndRole(email, role);
	}

	public DbUser findByNameLike(String name) {
		return userRepo.findByNameLike(name);
	}

	public DbUser findByUserId(String userId) {
		return userRepo.findByUserId(userId);
	}

	public DbUser findByRole(String role) {
		return userRepo.findByRole(role);
	}

	public Long getNextSeriesId() {
		return userRepo.getNextSeriesId();
	}

	public DbUser findByEmailOrMobileAndRole(String email, String mobile, String role) {
		return userRepo.findByEmailOrMobileAndRole(email, mobile, role);
	}

	public DbUser findByUserIdOrEmailOrMobile(String userId, String email, String mobile) {
		return userRepo.findByUserIdOrEmailOrMobile(userId, email, mobile);
	}

	public DbUser findByPartnerIdAndEmailAndRole(String partnerId, String email, UserRole role) {
		if (role == null || !UserRole.CUSTOMER.equals(role))
			return userRepo.findByPartnerIdAndEmailAndRoleNot(partnerId, email, UserRole.CUSTOMER.getCode());
		return userRepo.findByPartnerIdAndEmailAndRole(partnerId, email, role.getCode());
	}

	public DbUser findByPartnerIdAndMobileAndRole(String partnerId, String mobile, UserRole role) {
		if (role == null || !UserRole.CUSTOMER.equals(role))
			return userRepo.findByPartnerIdAndMobileAndRoleNot(partnerId, mobile, UserRole.CUSTOMER.getCode());
		return userRepo.findByPartnerIdAndMobileAndRole(partnerId, mobile, role.getCode());
	}

	public DbUser findByPartnerIdAndUserIdAndRole(String partnerId, String userId, UserRole role) {
		if (role == null)
			return userRepo.findByPartnerIdAndUserIdAndRoleNot(partnerId, userId, UserRole.CUSTOMER.getCode());
		return userRepo.findByPartnerIdAndUserIdAndRole(partnerId, userId, role.getCode());
	}

	public DbUser findByEmailOrMobile(String email, String mobile) {
		return userRepo.findByEmailOrMobile(email, mobile);
	}

	public List<DbUser> search(QueryFilter filter) {
		DependantSearchResolver.searchParentEntities(filter);
		log.debug("UserFilter after DependantSearch {}", GsonUtils.getGson().toJson(filter));
		PageRequest request = SpringDataUtils.getPageRequestFromFilter(filter);
		List<DbUser> userList = new ArrayList<>();
		Page<DbUser> pageList = userRepo.findAll((root, query, criteriaBuilder) -> {
			List<Predicate> predicates = UserSearchPredicate.getPredicateListBasedOnUserFilter(root, query,
					criteriaBuilder, (UserFilter) filter);
			return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
		}, request);
		pageList.forEach(user -> userList.add(user));
		return userList;
	}

	public List<User> find(UserCacheFilter userCacheFilter) {
		return userHelper.getUsersFromCache(userCacheFilter);
	}

}
