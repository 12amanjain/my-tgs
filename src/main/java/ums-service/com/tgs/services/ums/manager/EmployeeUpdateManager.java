package com.tgs.services.ums.manager;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.api.client.repackaged.com.google.common.base.Objects;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.ums.datamodel.bulkUpload.UserProfileUpdateQuery;
import com.tgs.services.ums.restmodel.Employee;
import com.tgs.services.ums.restmodel.EmployeeUploadRequest;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.bulkupload.UserProfileUpdateRequest;
import com.tgs.services.ums.restmodel.bulkupload.UserRegistrationRequest;
import lombok.extern.slf4j.Slf4j;

/**
 * This class is used to register / update profile of CORPORATE_EMPLOYEE. This is used while bulk uploading corporate
 * employees under a corporate, to update profile fields of corporate employee from dashboard, and after "Save profile "
 * option during booking.
 * 
 * Assumption of this class is that if USER_ID is present in upload request, then update user will be considered,
 * otherwise user will be registered
 * 
 * In any case, if client is trying to register a user by passing userid, that case will not work here.
 *
 */

@Service
@Slf4j
public class EmployeeUpdateManager {

	public static final String FIRST_NAME = "firstName";
	public static final String LAST_NAME = "lastName";
	private static final String EMAIL = "email";
	private static final String MOBILE = "mobile";
	private static final String USER_ID = "userId";
	private static final String EMPLOYEE_ID = "employeeId";
	private static final String PARENT_USER_ID = "parentUserId";
	private static final String ENABLED = "enabled";

	@Autowired
	private UserRegistrationManager userRegistrationManager;

	@Autowired
	private UserUpdateManager userUpdateManager;


	public BulkUploadResponse process(EmployeeUploadRequest uploadRequest) throws Exception {
		List<RegisterRequest> registerQueries = new ArrayList<>();
		List<UserProfileUpdateQuery> updateQueries = new ArrayList<>();
		uploadRequest.getRequest().forEach(employee -> {
			log.debug("Employee request is {} ", GsonUtils.getGson().toJson(employee));
			if (StringUtils.isNotEmpty(getString(employee.getData().get(USER_ID)))) {
				UserProfileUpdateQuery profileQuery = new UserProfileUpdateQuery();
				profileQuery.setUserId(employee.getData().get(USER_ID).toString());
				profileQuery.setEmployeeId(getString(employee.getData().get(EMPLOYEE_ID)));
				profileQuery.setMobile(getString(employee.getData().get(MOBILE)));
				profileQuery.setRowId(employee.getRowId());
				removeExtraFields(employee);
				profileQuery.setData(employee.getData());
				updateQueries.add(profileQuery);
			} else {
				registerQueries.add(map(employee));
			}
		});

		BulkUploadResponse registerResponse = null;
		BulkUploadResponse updateResponse = null;

		log.debug("Register queries are {} ", GsonUtils.getGson().toJson(registerQueries));
		if (CollectionUtils.isNotEmpty(registerQueries)) {
			UserRegistrationRequest registrationRequest = new UserRegistrationRequest();
			registrationRequest.setUploadId(uploadRequest.getUploadId());
			registrationRequest.setUploadType(uploadRequest.getUploadType());
			registrationRequest.setRegisterQuery(registerQueries);
			registerResponse = userRegistrationManager.registerUsers(registrationRequest);
		}

		log.debug("Update queries are {} ", GsonUtils.getGson().toJson(updateQueries));
		if (CollectionUtils.isNotEmpty(updateQueries)) {
			UserProfileUpdateRequest profileUpdateRequest = new UserProfileUpdateRequest();
			profileUpdateRequest.setUploadId(uploadRequest.getUploadId());
			profileUpdateRequest.setUploadType(uploadRequest.getUploadType());
			profileUpdateRequest.setUpdateQuery(updateQueries);
			updateResponse = userUpdateManager.updateUsers(profileUpdateRequest);
		}

		// Right now this api is being used to either register or update; not both.
		return Objects.firstNonNull(registerResponse, updateResponse);
	}

	private RegisterRequest map(Employee employee) {
		String fullName = employee.getData().getOrDefault(FIRST_NAME, "").toString() + " "
				+ employee.getData().getOrDefault(LAST_NAME, "").toString();
		fullName = StringUtils.isBlank(fullName) ? null : fullName;
		boolean enabled = Boolean.parseBoolean(employee.getData().getOrDefault(ENABLED, "").toString());
		RegisterRequest registerRequest = RegisterRequest.builder().name(fullName).enabled(enabled)
				.email(getString(employee.getData().get(EMAIL))).mobile(getString(employee.getData().get(MOBILE)))
				.userId(getString(employee.getData().get(USER_ID)))
				.employeeId(getString(employee.getData().get(EMPLOYEE_ID)))
				.parentUserId(getString(employee.getData().get(PARENT_USER_ID))).build();
		removeExtraFields(employee);
		registerRequest.getUserProfile().setData(employee.getData());
		registerRequest.setRowId(employee.getRowId());
		registerRequest.setRole(UserRole.CORPORATE_EMPLOYEE);
		return registerRequest;
	}

	private void removeExtraFields(Employee employee) {
		employee.getData().remove(EMAIL);
		employee.getData().remove(MOBILE);
		employee.getData().remove(USER_ID);
		employee.getData().remove(EMPLOYEE_ID);
		employee.getData().remove(PARENT_USER_ID);
		employee.getData().remove(ENABLED);
	}

	private static String getString(Object obj) {
		if (obj == null)
			return null;
		String str = obj.toString();
		if (StringUtils.isBlank(str))
			return null;
		return str;
	}

}
