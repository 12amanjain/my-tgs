package com.tgs.services.ums.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.ContextData;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.restmodel.bulkupload.UserRegistrationRequest;
import com.tgs.services.ums.servicehandler.RegistrationHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserRegistrationManager {

	public BulkUploadResponse registerUsers(UserRegistrationRequest userRegisterRequest) throws Exception {
		if (userRegisterRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(userRegisterRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		final ContextData contextData = SystemContextHolder.getContextData();
		Runnable userRegistertask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (RegisterRequest registerQuery : userRegisterRequest.getRegisterQuery()) {
				Future<?> f = executor.submit(() -> {
					SystemContextHolder.setContextData(contextData);
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(registerQuery.getRowId());
					try {
						log.info("Employee-upload: uploading employee {}", registerQuery.getName());
						saveUser(registerQuery);
						log.info("Employee-upload: success for employee {}", registerQuery.getName());
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to save user with userid {} ", registerQuery.getUserId(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting user relationship for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(userRegistertask, userRegisterRequest.getUploadType(), jobId);
	}

	private void saveUser(RegisterRequest user) throws Exception {
		RegistrationHandler registrationHandler =
				(RegistrationHandler) SpringContext.getApplicationContext().getBean("registrationHandler");
		registrationHandler.initData(user, new UserResponse());
		registrationHandler.getResponse();
	}

}
