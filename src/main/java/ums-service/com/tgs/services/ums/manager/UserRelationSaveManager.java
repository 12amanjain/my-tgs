package com.tgs.services.ums.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.ums.restmodel.RelationshipsRequest;
import com.tgs.services.ums.restmodel.bulkupload.UserRelationRequest;
import com.tgs.services.ums.servicehandler.UserRelationshipHandler;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserRelationSaveManager {

	public BulkUploadResponse saveUserRelationsList(UserRelationRequest userRelationRequest) throws Exception {
		if (userRelationRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(userRelationRequest.getUploadId());
		}

		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable saveUserRelationshipTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			for (RelationshipsRequest relationQuery : userRelationRequest.getRelations()) {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(relationQuery.getRowId());
					try {
						saveUserRelation(relationQuery);
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to save user relationship for userid {} and related user ids {}",
								relationQuery.getUserId(), relationQuery.getRelatedUserIds(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			}
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting user relationship for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(saveUserRelationshipTask, userRelationRequest.getUploadType(),
				jobId);
	}

	private void saveUserRelation(RelationshipsRequest relationsRequestData) throws Exception {
		UserRelationshipHandler relationHandler =
				(UserRelationshipHandler) SpringContext.getApplicationContext().getBean("userRelationshipHandler");
		relationHandler.initData(relationsRequestData, new BaseResponse());
		relationHandler.getResponse();
	}

}
