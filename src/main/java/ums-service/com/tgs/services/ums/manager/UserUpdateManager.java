package com.tgs.services.ums.manager;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.ums.restmodel.bulkupload.UserUpdateRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.datamodel.BulkUploadInfo;
import com.tgs.services.base.datamodel.UploadStatus;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BulkUploadUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserUpdateManager {

	@Autowired
	private UserService service;

	public <T extends UserUpdateRequest> BulkUploadResponse updateUsers(T userUpdateRequest) throws Exception {
		if (userUpdateRequest.getUploadId() != null) {
			return BulkUploadUtils.getCachedBulkUploadResponse(userUpdateRequest.getUploadId());
		}
		String jobId = BulkUploadUtils.generateRandomJobId();
		Runnable saveUserRelationshipTask = () -> {
			List<Future<?>> futures = new ArrayList<Future<?>>();
			ExecutorService executor = Executors.newFixedThreadPool(10);
			userUpdateRequest.getUpdateQuery().forEach(updateQuery -> {
				Future<?> f = executor.submit(() -> {
					BulkUploadInfo uploadInfo = new BulkUploadInfo();
					uploadInfo.setId(updateQuery.getRowId());
					try {
						saveUser(updateQuery.getUserFromRequest());
						uploadInfo.setStatus(UploadStatus.SUCCESS);
					} catch (Exception e) {
						uploadInfo.setErrorMessage(e.getMessage());
						uploadInfo.setStatus(UploadStatus.FAILED);
						log.error("Unable to update user with userid {} ", updateQuery.getUserId(), e);
					} finally {
						BulkUploadUtils.cacheBulkInfoResponse(uploadInfo, jobId);
						log.debug("Uploaded {} into cache", GsonUtils.getGson().toJson(uploadInfo));
					}
				});
				futures.add(f);
			});
			try {
				for (Future<?> future : futures)
					future.get();
				BulkUploadUtils.storeBulkInfoResponseCompleteAt(jobId, BulkUploadUtils.INITIALIZATION_NOT_REQUIRED);
			} catch (InterruptedException | ExecutionException e) {
				log.error("Interrupted exception while persisting user relationship for {} ", jobId, e);
			}
		};
		return BulkUploadUtils.processBulkUploadRequest(saveUserRelationshipTask, userUpdateRequest.getUploadType(),
				jobId);
	}

	private void saveUser(User user) throws Exception {
		DbUser dbUser = service.findByUserId(user.getUserId());
		if (dbUser == null)
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		dbUser = dbUser.from(user);
		service.save(dbUser);
	}

}
