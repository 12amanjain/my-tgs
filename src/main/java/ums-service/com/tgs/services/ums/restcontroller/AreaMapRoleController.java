package com.tgs.services.ums.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;

import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.filters.AreaRoleMappingFilter;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.restmodel.AreaRoleMapResponse;
import com.tgs.services.ums.servicehandler.AreaRoleMapHandler;

@RestController
@RequestMapping("/ums/v1/arearolemap")
public class AreaMapRoleController {

	@Autowired
	private AreaRoleMapHandler areaRoleMapHandler;

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	protected BaseResponse saveAreaRoleMap(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AreaRoleMapping areaRoleMap) throws Exception {
		if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.ADMIN)) {
			areaRoleMapHandler.initData(areaRoleMap, new AreaRoleMapResponse());
			return areaRoleMapHandler.getResponse();
		} else {
			throw new CustomGeneralException(SystemError.ACCESS_DENIED);
		}

	}

	@RequestMapping(value = "/fetch", method = RequestMethod.POST)
	protected AreaRoleMapResponse fetchAreaRoleMap(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AreaRoleMappingFilter filter) throws Exception {
		if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.ADMIN)) {
			return areaRoleMapHandler.fetchAreaRoleMap(filter);
		} else {
			throw new CustomGeneralException(SystemError.ACCESS_DENIED);
		}
	}

	@RequestMapping(value = "/delete/{areaRole}", method = RequestMethod.DELETE)
	protected BaseResponse delete(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("areaRole") AreaRole areaRole) throws Exception {
		if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.ADMIN)) {
			return areaRoleMapHandler.deleteAreaRoleMap(areaRole);
		} else {
			throw new CustomGeneralException(SystemError.ACCESS_DENIED);
		}
	}

}
