package com.tgs.services.ums.restcontroller;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.UserDeviceInfoFilter;
import com.tgs.filters.UserRelationFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.communicator.GeneralCachingCommunicator;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.restmodel.BulkUploadResponse;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.base.security.JWTHelper;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheNameSpace;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserRelation;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.jparepository.UserDeviceInfoService;
import com.tgs.services.ums.jparepository.UserRelationService;
import com.tgs.services.ums.manager.EmployeeUpdateManager;
import com.tgs.services.ums.manager.RazorpayVirtualAccountManager;
import com.tgs.services.ums.manager.UserRegistrationManager;
import com.tgs.services.ums.manager.UserRelationSaveManager;
import com.tgs.services.ums.manager.UserUpdateManager;
import com.tgs.services.ums.restmodel.EmployeeUploadRequest;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.RelationshipsRequest;
import com.tgs.services.ums.restmodel.ResetPasswordRequest;
import com.tgs.services.ums.restmodel.SignInRequest;
import com.tgs.services.ums.restmodel.SignInResponse;
import com.tgs.services.ums.restmodel.UserDeviceListResponse;
import com.tgs.services.ums.restmodel.UserRelationResponse;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.restmodel.bulkupload.CommissionPlanUpdateRequest;
import com.tgs.services.ums.restmodel.bulkupload.DistributorIdUpdateRequest;
import com.tgs.services.ums.restmodel.bulkupload.GradeUpdateRequest;
import com.tgs.services.ums.restmodel.bulkupload.UserRegistrationRequest;
import com.tgs.services.ums.restmodel.bulkupload.UserRelationRequest;
import com.tgs.services.ums.servicehandler.EmulateHandler;
import com.tgs.services.ums.servicehandler.PartnerRegistrationHandler;
import com.tgs.services.ums.servicehandler.RegistrationHandler;
import com.tgs.services.ums.servicehandler.ResetPasswordHandler;
import com.tgs.services.ums.servicehandler.UserRelationshipHandler;
import com.tgs.services.ums.servicehandler.UserUpdateHandler;

@RestController
@RequestMapping("/ums/v1")
@Scope(value = "session")
public class UserController {

	@Autowired
	private EmulateHandler emulateHandler;

	@Autowired
	private RegistrationHandler regisHandler;

	@Autowired
	private PartnerRegistrationHandler partnerRegisHandler;

	@Autowired
	private EmployeeUpdateManager employeeUpdateManager;

	@Autowired
	private UserUpdateHandler updateHandler;

	@Autowired
	private ResetPasswordHandler resetHandler;

	@Autowired
	private GeneralCachingCommunicator cachingService;

	@Autowired
	private AuditsHandler auditHandler;

	@Autowired
	private UserRelationshipHandler relationHandler;

	@Autowired
	private UserRelationService userRelationService;

	@Autowired
	private UserHelper userHelper;

	@Autowired
	private RegisterRequestValidator registerRequestValidator;

	@Autowired
	private UserRelationSaveManager relationSaveManager;

	@Autowired
	private UserRegistrationManager registrationManager;

	@Autowired
	private UserUpdateManager updateManager;

	@Autowired
	private RazorpayVirtualAccountManager razorpayVAM;

	@Autowired
	private UserDeviceInfoService deviceInfoService;

	@InitBinder("registerRequest")
	void setRegistrationValidator(WebDataBinder binder) {
		binder.setValidator(registerRequestValidator);
	}

	@RequestMapping(value = "/emulate", method = RequestMethod.POST)
	protected @ResponseBody SignInResponse emulate(HttpServletRequest request, HttpServletResponse response,
			@RequestBody SignInRequest signInRequestData) throws Exception {
		emulateHandler.initData(signInRequestData, new SignInResponse());
		return emulateHandler.getResponse();
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST)
	protected UserResponse register(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Validated RegisterRequest registrationRequest) throws Exception {
		if (!UserUtils.DEFAULT_PARTNERID.equals(SystemContextHolder.getContextData().getHttpHeaders().getPartnerId())) {
			partnerRegisHandler.initData(registrationRequest, new UserResponse());
			return partnerRegisHandler.getResponse();
		} else {
			regisHandler.initData(registrationRequest, new UserResponse());
			return regisHandler.getResponse();
		}
	}

	@RequestMapping(value = "/upload-users", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadUsers(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody UserRegistrationRequest registrationRequest) throws Exception {
		return registrationManager.registerUsers(registrationRequest);
	}

	@RequestMapping(value = "/upload-grade", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadUserGrade(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody GradeUpdateRequest updateRequest) throws Exception {
		return updateManager.updateUsers(updateRequest);

	}

	@RequestMapping(value = "/upload-distributorid", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadDistributorId(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody DistributorIdUpdateRequest updateRequest)
			throws Exception {
		return updateManager.updateUsers(updateRequest);

	}

	@RequestMapping(value = "/upload-plan", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadCommericialPlan(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody CommissionPlanUpdateRequest updateRequest)
			throws Exception {
		return updateManager.updateUsers(updateRequest);
	}

	@Deprecated
	@RequestMapping(value = {"/logout", "/logout/{userId:.+}"})
	protected BaseResponse logout(HttpServletRequest request, HttpServletResponse response,
			@PathVariable Optional<String> userId) throws Exception {
		if (userId.isPresent()) {
			UserHelper.removeUserJWTTokens(userId.get());
		} else {
			String token = JWTHelper.getJWTToken(request);
			cachingService.delete(CacheMetaInfo.builder().namespace(CacheNameSpace.USERS.getName())
					.set(CacheSetName.JWT.getName()).key(token).build());
		}
		return new BaseResponse();
	}

	@RequestMapping(value = "/update", method = RequestMethod.POST)
	protected UserResponse update(HttpServletRequest request, HttpServletResponse response, @RequestBody User user)
			throws Exception {
		UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), Arrays.asList(user.getUserId()));
		updateHandler.initData(user, new UserResponse());
		return updateHandler.getResponse();
	}

	@RequestMapping(value = "/reset-password", method = RequestMethod.POST)
	protected BaseResponse reset(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody ResetPasswordRequest resetPasswordRequest) throws Exception {
		resetHandler.initData(resetPasswordRequest, new BaseResponse());
		return resetHandler.getResponse();
	}

	@RequestMapping(value = "/refresh-token", method = RequestMethod.GET)
	protected BaseResponse refreshToken(HttpServletRequest request, HttpServletResponse response) throws Exception {
		JWTHelper.generateNewToken(SystemContextHolder.getContextData().getUser(), JWTHelper.getJWTToken(request),
				response, true);
		return new BaseResponse();
	}

	@RequestMapping(value = "/long-term-token/{expirySec}", method = RequestMethod.GET)
	protected BaseResponse longTermToken(HttpServletRequest request, HttpServletResponse response,
			@PathVariable int expirySec) throws Exception {
		JWTHelper.generateAndStoreAccessToken(SystemContextHolder.getContextData().getUser(), response, expirySec);
		return new BaseResponse();
	}

	@RequestMapping(value = "/save-relationships", method = RequestMethod.POST)
	protected @ResponseBody BaseResponse saveRelationships(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody RelationshipsRequest relationsRequestData) throws Exception {
		relationHandler.initData(relationsRequestData, new BaseResponse());
		return relationHandler.getResponse();
	}

	@RequestMapping(value = "/relation/search", method = RequestMethod.POST)
	protected @ResponseBody UserRelationResponse searchRelation(@Valid @RequestBody UserRelationFilter filter) {
		List<UserRelation> userRelations = BaseModel.toDomainList(userRelationService.search(filter));
		return new UserRelationResponse(userRelations);
	}

	@RequestMapping(value = "/upload-relationships", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadUserRelations(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody UserRelationRequest relationsRequestData)
			throws Exception {
		return relationSaveManager.saveUserRelationsList(relationsRequestData);
	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(
				auditHandler.fetchAudits(auditsRequestData, com.tgs.services.ums.dbmodel.DbUser.class, "userId"));
		return auditResponse;
	}

	@RequestMapping(value = "/refreshcache/{userId}", method = RequestMethod.GET)
	protected BaseResponse refreshCache(@PathVariable("userId") String userId) {
		userHelper.updateCache(userId);
		return new BaseResponse();
	}

	@RequestMapping(value = "/default-token", method = RequestMethod.GET)
	protected BaseResponse getDefaultToken() throws Exception {
		JWTHelper.getDefaultToken();
		return new BaseResponse();
	}

	@RequestMapping(value = "/upload-employee", method = RequestMethod.POST)
	protected @ResponseBody BulkUploadResponse uploadUsers(@Valid @RequestBody EmployeeUploadRequest request)
			throws Exception {
		return employeeUpdateManager.process(request);
	}

	@RequestMapping(value = "/job/update/razorpay-va/{razorpayId}", method = RequestMethod.GET)
	protected BaseResponse createRazorpayVirtualAccounts(@PathVariable("razorpayId") String razorpayId)
			throws Exception {
		razorpayVAM.createVirtualAccounts(razorpayId);
		return new BaseResponse();
	}

	@RequestMapping(value = "/device/get-deviceId", method = RequestMethod.POST)
	protected @ResponseBody UserDeviceListResponse getTokenList(HttpServletRequest request,
			HttpServletResponse response, @Valid @RequestBody UserDeviceInfoFilter filter) throws Exception {
		UserDeviceListResponse userDeviceList = deviceInfoService.findAll(filter);
		return userDeviceList;
	}

}
