package com.tgs.services.ums.restcontroller;

import com.tgs.services.base.ruleengine.UserAirFeeRuleCriteria;
import org.apache.commons.lang3.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.TgsValidator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.fee.UserFee;

@Component
public class UserFeeRequestValidator extends TgsValidator implements Validator {
	
	@Autowired
	UserAirFeeRuleValidator userAirFeeRuleValidator;

	@Override
	public boolean supports(Class<?> clazz) {
		return UserFee.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		if (!(target instanceof UserFee)) {
			return;
		}
		User user = SystemContextHolder.getContextData().getUser();
		if (BooleanUtils.isTrue(user.getAdditionalInfo().getDisableMarkup())) {
			throw new CustomGeneralException(SystemError.PERMISSION_DENIED);
		}

		UserFee userFee = (UserFee) target;
		registerErrors(errors, "", userFee);

		if (Product.AIR.equals(userFee.getProduct())) {
			UserAirFeeRuleCriteria airFeeRuleCriteria = (UserAirFeeRuleCriteria) userFee.getInclusionCriteria();
			
			if (airFeeRuleCriteria != null) {
				registerErrors(errors, "inclusionCriteria", airFeeRuleCriteria);
				userAirFeeRuleValidator.validateCriteria(errors, "inclusionCriteria", airFeeRuleCriteria);
			}
		}
	}

}
