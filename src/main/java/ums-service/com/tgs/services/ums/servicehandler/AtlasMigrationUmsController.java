package com.tgs.services.ums.servicehandler;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.tgs.filters.UserFilter;
import com.tgs.services.base.restmodel.BaseResponse;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tgs.services.base.datamodel.BankAccountInfo;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.AtlasUtils;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;

@RestController
@RequestMapping("/ums/v1")
public class AtlasMigrationUmsController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "/generate-dbs-accountnumber", method = RequestMethod.GET)
	protected @ResponseBody BaseResponse emulate(HttpServletRequest request, HttpServletResponse response) {
		List<DbUser> userList = userService.search(UserFilter.builder().roles(java.util.Arrays.asList(UserRole.AGENT, UserRole.DISTRIBUTOR)).build());
		for (DbUser dbUser : userList) {
			boolean isDbsAccountAlreadyPresent = false;
			if (CollectionUtils.isNotEmpty(dbUser.getAdditionalInfo().getDepositBankAccounts())) {
				for (BankAccountInfo bai : dbUser.getAdditionalInfo().getDepositBankAccounts()) {
					if (bai.getBankName().toLowerCase().contains("dbs")) {
						isDbsAccountAlreadyPresent = true;
					}
				}
			}

			if (!isDbsAccountAlreadyPresent) {
				List<BankAccountInfo> bankAccounts = dbUser.getAdditionalInfo().getDepositBankAccounts();
				if (CollectionUtils.isEmpty(bankAccounts))
					bankAccounts = new ArrayList<>();
				bankAccounts.add(AtlasUtils.generateDBSAccountNumber(dbUser.getUserId()));
				dbUser.getAdditionalInfo().setDepositBankAccounts(bankAccounts);
			}
			userService.save(dbUser);
		}

		return new BaseResponse();
	}
}
