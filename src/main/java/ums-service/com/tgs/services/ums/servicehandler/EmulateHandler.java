package com.tgs.services.ums.servicehandler;

import java.util.Arrays;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.helper.UserServiceHelper;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.security.JWTHelper;
import com.tgs.services.base.utils.ServiceUtils;
import com.tgs.services.gms.datamodel.systemaudit.AuditAction;
import com.tgs.services.gms.datamodel.systemaudit.SystemAudit;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.SignInRequest;
import com.tgs.services.ums.restmodel.SignInResponse;
import com.tgs.services.ums.validator.EmulateValidator;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Service
@Getter
@Setter
@Slf4j
public class EmulateHandler extends ServiceHandler<SignInRequest, SignInResponse> {
	private DbUser dbuser;
	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Autowired
	GeneralServiceCommunicator gmsComm;

	@Autowired
	EmulateValidator emulateValidator;

	@Autowired
	PaymentServiceCommunicator payService;

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		dbuser = userService.findByUserId(request.getUsername());
		if (dbuser == null) {
			throw new CustomGeneralException(SystemError.INVALID_USERID);
		}

		User loggedInUser = contextData.getUser();

		SystemAudit audit = SystemAudit.builder().auditType(AuditAction.EMULATE).userId(request.getUsername())
				.ip(contextData.getHttpHeaders().getIp()).loggedInUserId(loggedInUser.getEmulateOrLoggedInUserId())
				.build();
		gmsComm.addToSystemAudit(audit);

		User user = dbuser.toDomain();

		if (user == null) {
			response.addError(new ErrorDetail(SystemError.INVALID_USERID));
		} else if (!user.getStatus().equals(UserStatus.ENABLED)) {
			response.addError(new ErrorDetail(SystemError.INACTIVE_ACCOUNT));
		} else if (user.getRole().equals(loggedInUser.getRole())) {
			response.addError(new ErrorDetail(SystemError.EMULATE_SAME_ROLE_NOT_ALLOWED));
		} else if (loggedInUser.getRole().equals(UserRole.AGENT) && !(user.getRole().equals(UserRole.AGENT_STAFF)
				|| user.getParentUserId().equals(loggedInUser.getUserId()))) {
			response.addError(new ErrorDetail(SystemError.EMULATE_AGENT_ROLE));
		}

		UserServiceHelper.checkAndReturnAllowedUserId(
				SystemContextHolder.getContextData().getUser().getLoggedInUserId(), Arrays.asList(user.getUserId()));

		if (!emulateValidator.validate(loggedInUser, user))
			throw new CustomGeneralException(SystemError.INVALID_EMULATE_ACCESS);
		
		if (dbuser.getParentUserId() != null) {
			User parentUser = userService.findByUserId(dbuser.getParentUserId()).toDomain();
			user.setParentUser(parentUser);
			user.setParentConf(parentUser.getUserConf());
		}

		if (CollectionUtils.isEmpty(response.getErrors())) {
			ServiceUtils.updateUserInfoforJWTToken(user, payService, null);
			user.setEmulateUser(User.builder().role(loggedInUser.getRole()).userId(loggedInUser.getUserId())
					.name(loggedInUser.getName()).build());
			response.setUser(user);
			String accessToken = JWTHelper.generateAndStoreAccessToken(user,
					SystemContextHolder.getContextData().getHttpResponse());
			response.setAccessToken(accessToken);
			
			
		}

	}

	@Override
	public void afterProcess() throws Exception {
	}
}
