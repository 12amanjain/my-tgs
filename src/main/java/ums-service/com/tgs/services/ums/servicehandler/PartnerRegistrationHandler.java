package com.tgs.services.ums.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.ClientDefaultConfiguration;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.ums.datamodel.RegisterEmailAttributes;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import com.tgs.services.ums.datamodel.UserConfiguration;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.manager.AbstractRegistrationManager;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.UserResponse;

@Service
public class PartnerRegistrationHandler extends ServiceHandler<RegisterRequest, UserResponse> {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Autowired
	PaymentServiceCommunicator paymentCommunicator;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	@Autowired
	MsgServiceCommunicator msgSrvCommunicator;

	private DbUser existingUser = null;

	@Override
	public void beforeProcess() throws Exception {
		if (SystemContextHolder.getContextData().getUser() != null) {
			if (!request.getRole().equals(UserRole.AGENT) && !request.getRole().equals(UserRole.AGENT_STAFF)
					&& !request.getRole().equals(UserRole.WHITELABEL_STAFF)) {
				throw new CustomGeneralException(SystemError.REGISTRATION_NOT_ALLOWED);
			}
		}
		try {
			// This means that WL Partner can not create his agents from our client's (TripJack) portal
			String partnerId = SystemContextHolder.getContextData().getHttpHeaders().getPartnerId();
			existingUser = userService.findByPartnerIdAndEmailAndRole(partnerId, request.getEmail(), request.getRole());
			if (existingUser == null)
				existingUser =
						userService.findByPartnerIdAndMobileAndRole(partnerId, request.getMobile(), request.getRole());

		} catch (Exception e) {
			throw new CustomGeneralException(SystemError.USER_EXISTS);
		}

		if (existingUser != null) {
			throw new CustomGeneralException(SystemError.USER_EXISTS);
		}
	}

	@Override
	public void process() throws Exception {
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		long id = userService.getNextSeriesId();
		DbUser dbUser = gson.fromJson(gson.toJson(request), DbUser.class);
		dbUser.setUserConf(UserConfiguration.builder().build());
		if (request.getAdditionalInfo() == null) {
			dbUser.setAdditionalInfo(UserAdditionalInfo.builder().build());
		}
		dbUser.setEmail(dbUser.getEmail().toLowerCase());
		String encodedPass =
				StringUtils.isEmpty(request.getPassword()) ? "" : passEncoder.encode((request.getPassword()));
		dbUser.setPassword(encodedPass);
		dbUser.setStatus(UserStatus.DISABLED.getCode());
		dbUser.setId(id);
		if (StringUtils.isEmpty(dbUser.getUserId()))
			dbUser.setUserId(com.tgs.services.base.utils.user.UserUtils.generateUserId(id, request.getRole()));
		if (dbUser.getCreatedOn() == null)
			dbUser.setCreatedOn(LocalDateTime.now());
		setDefaults(dbUser);
		userService.save(dbUser);
		paymentCommunicator.updateUserWallet(UserWallet.builder().userId(dbUser.getUserId()).build());
		sendSms(dbUser);
		sendEmail(dbUser);
		response.setUsers(new ArrayList<>());
		response.getUsers().add(dbUser.toDomain());
	}

	@Override
	public void afterProcess() throws Exception {

	}

	private void sendSms(DbUser user) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("userId", user.getUserId());
				attributes.put("userName", user.getName());
				attributes.put("userEmail", user.getEmail());
				attributes.put("userId", user.getUserId());
				attributes.put("partnerId", user.getPartnerId());
				SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.USER_REGISTERATION_SMS.name())
						.recipientNumbers(Arrays.asList(user.getMobile())).attributes(attributes)
						.partnerId(user.getPartnerId()).role(UserRole.getEnumFromCode(user.getRole())).build();
				return smsAttr;
			}
		};
		msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());
	}

	private void sendEmail(DbUser dbUser) {
		User user = dbUser.toDomain();
		AbstractMessageSupplier<RegisterEmailAttributes> msgAttributes =
				new AbstractMessageSupplier<RegisterEmailAttributes>() {
					@Override
					public RegisterEmailAttributes get() {
						RegisterEmailAttributes emailAttributes = RegisterEmailAttributes.builder()
								.toEmailId(user.getEmail()).email(user.getEmail()).role(user.getRole())
								.mobile(user.getMobile()).name(user.getName()).userId(user.getUserId())
								.partnerId(user.getPartnerId()).status(StringUtils.capitalize(user.getStatus().name()))
								.key(EmailTemplateKey.SIGN_UP_EMAIL.name()).build();
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}

	private void setDefaults(DbUser dbUser) {
		ClientGeneralInfo clientGeneralInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		ClientDefaultConfiguration defaultConfiguration = clientGeneralInfo.getDefaultConfiguration();

		if (StringUtils.isNotBlank(clientGeneralInfo.getRegistrationBean())) {
			AbstractRegistrationManager registrationManager = (AbstractRegistrationManager) SpringContext
					.getApplicationContext().getBean(clientGeneralInfo.getRegistrationBean());
			registrationManager.process(dbUser);
		}

		if (SystemContextHolder.getContextData().getUser() != null
				&& !UserRole.WHITELABEL_PARTNER.equals(request.getRole())) {
			String partnerGroup = clientGeneralInfo.getPartnerGroup();
			// Right now hard coding this to because frontend access control is based on this.
			if (StringUtils.isEmpty(partnerGroup))
				partnerGroup = "WHITELABEL";
			dbUser.getAdditionalInfo().setGroups(Lists.newArrayList(partnerGroup));
			User partnerUser = SystemContextHolder.getContextData().getUser();
			dbUser.setPartnerId(partnerUser.getUserId());
			dbUser.getAdditionalInfo().setLogoURL(partnerUser.getAdditionalInfo().getLogoURL());
			dbUser.getAdditionalInfo().setTdsRate(partnerUser.getAdditionalInfo().getTdsRate());
			if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.AGENT)
					|| UserRole.WHITELABEL_STAFF.equals(request.getRole())) {
				dbUser.setParentUserId(SystemContextHolder.getContextData().getUser().getUserId());
			}
		} else {
			// partnerId mandatory
			dbUser.setPartnerId(SystemContextHolder.getContextData().getHttpHeaders().getPartnerId());
			dbUser.getAdditionalInfo().setTdsRate(defaultConfiguration.getTdsRate());
		}
	}

}
