package com.tgs.services.ums.servicehandler;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.google.gson.Gson;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.SpringContext;
import com.tgs.services.base.communicator.GeneralServiceCommunicator;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.communicator.PaymentServiceCommunicator;
import com.tgs.services.base.datamodel.ClientDefaultConfiguration;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.ruleengine.GeneralBasicFact;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.LogUtils;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.gms.datamodel.ClientGeneralInfo;
import com.tgs.services.gms.datamodel.configurator.ConfiguratorRuleType;
import com.tgs.services.logging.datamodel.LogMetaInfo;
import com.tgs.services.messagingService.datamodel.sms.SmsAttributes;
import com.tgs.services.messagingService.datamodel.sms.SmsTemplateKey;
import com.tgs.services.pms.datamodel.UserWallet;
import com.tgs.services.ums.datamodel.RegisterEmailAttributes;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserAdditionalInfo;
import com.tgs.services.ums.datamodel.UserConfiguration;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.manager.AbstractRegistrationManager;
import com.tgs.services.ums.restmodel.RegisterRequest;
import com.tgs.services.ums.restmodel.UserResponse;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RegistrationHandler extends ServiceHandler<RegisterRequest, UserResponse> {

	@Autowired
	private UserService userService;

	@Autowired
	private BCryptPasswordEncoder passEncoder;

	@Autowired
	@Lazy
	MsgServiceCommunicator msgSrvCommunicator;

	@Autowired
	GeneralServiceCommunicator gsCommunicator;

	@Autowired
	UserHelper userHelper;

	@Autowired
	@Lazy
	PaymentServiceCommunicator paymentCommunicator;

	private DbUser existingUser;

	@Override
	public void beforeProcess() throws Exception {
		if (SystemContextHolder.getContextData().getUser() != null) {
			if (UserUtils.isMidOfficeRole(request.getRole())
					&& !UserUtils.isMidOfficeUser(SystemContextHolder.getContextData().getUser())) {
				throw new CustomGeneralException(SystemError.ACCESS_DENIED);
			}
			if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.AGENT)
					&& !request.getRole().equals(UserRole.AGENT_STAFF)) {
				throw new CustomGeneralException(SystemError.REGISTRATION_NOT_ALLOWED);
			}
			if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.DISTRIBUTOR)
					&& !(request.getRole().equals(UserRole.DISTRIBUTOR_STAFF)
							|| request.getRole().equals(UserRole.AGENT))) {
				throw new CustomGeneralException(SystemError.REGISTRATION_NOT_ALLOWED);
			}
			if (SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.CORPORATE)
					&& !UserRole.CORPORATE_EMPLOYEE.equals(request.getRole())
					&& !UserRole.CORPORATE_STAFF.equals(request.getRole())) {
				throw new CustomGeneralException(SystemError.REGISTRATION_NOT_ALLOWED);
			}
			if (request.getRole().equals(UserRole.WHITELABEL_STAFF)) {
				throw new CustomGeneralException(SystemError.REGISTRATION_NOT_ALLOWED);
			}

			if (!request.getRole().equals(UserRole.CORPORATE) && request.getAdditionalInfo() != null
					&& StringUtils.isNotEmpty(request.getAdditionalInfo().getCcEmailIds())) {
				throw new CustomGeneralException(SystemError.ADDITIONAL_EMAIL_NOT_ALLOWED);
			}
		} else {
			ClientGeneralInfo clientInfo =
					gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, GeneralBasicFact.builder().build());
			List<UserRole> rolesAllowedWithoutAuth = clientInfo.getAllowedRegistrationWithoutAuth();
			if (!rolesAllowedWithoutAuth.contains(request.getRole()))
				throw new CustomGeneralException(SystemError.ACCESS_DENIED);

		}

		if (StringUtils.isNotEmpty(request.getPassword()) && request.getPassword().length() < 6) {
			throw new CustomGeneralException(SystemError.WEAK_PASSWORD);
		}

		existingUser = null;
		try {
			existingUser = userService.findByPartnerIdAndEmailAndRole(UserUtils.DEFAULT_PARTNERID, request.getEmail(),
					request.getRole());
			if (Objects.isNull(existingUser))
				existingUser = userService.findByPartnerIdAndMobileAndRole(UserUtils.DEFAULT_PARTNERID,
						request.getMobile(), request.getRole());
			if (Objects.isNull(existingUser) && StringUtils.isNotEmpty(request.getUserId()))
				existingUser = userService.findByPartnerIdAndUserIdAndRole(UserUtils.DEFAULT_PARTNERID,
						request.getUserId(), request.getRole());
		} catch (Exception e) {
			log.info("[RegistrationHandler] Exception is {} ", e.getMessage(), e);
			// If 2 users exist, hibernate throws exception : query returned 2 results
			throw new CustomGeneralException(SystemError.USER_EXISTS);
		}
		if (existingUser != null) {
			throw new CustomGeneralException(SystemError.USER_EXISTS);
		}
	}

	@Override
	public void process() throws Exception {
		Gson gson = GsonUtils.builder().typeFactories(Arrays.asList(new EnumTypeAdapterFactory())).build().buildGson();
		log.debug("Register User Request: {}", request);
		LogUtils.log("registration#start", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), null);

		// userService = context.getBean(UserService.class);
		long id = userService.getNextSeriesId();

		DbUser dbUser = gson.fromJson(gson.toJson(request), DbUser.class);
		dbUser.setUserConf(UserConfiguration.builder().build());
		if (request.getAdditionalInfo() == null) {
			dbUser.setAdditionalInfo(UserAdditionalInfo.builder().build());
		}
		if (StringUtils.isBlank(request.getParentUserId()) && SystemContextHolder.getContextData().getUser() != null
				&& SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.DISTRIBUTOR)
				&& request.getRole().equals(UserRole.AGENT)) {
			dbUser.getUserConf().setDistributorId(SystemContextHolder.getContextData().getUser().getUserId());
		}
		if (StringUtils.isBlank(request.getParentUserId()) && SystemContextHolder.getContextData().getUser() != null
				&& SystemContextHolder.getContextData().getUser().getRole().equals(UserRole.CORPORATE)
				&& (request.getRole().equals(UserRole.CORPORATE_EMPLOYEE)
						|| request.getRole().equals(UserRole.CORPORATE_STAFF))) {
			dbUser.setParentUserId(SystemContextHolder.getContextData().getUser().getUserId());
		}
		dbUser.setEmail(dbUser.getEmail().toLowerCase());
		String encodedPass =
				StringUtils.isEmpty(request.getPassword()) ? "" : passEncoder.encode((request.getPassword()));
		dbUser.setPassword(encodedPass);
		// dbUser.setPassword(EncryptionUtils.encryptUsingKMS(request.getPassword(),
		// Constants.KEYID));

		if (UserRole.CUSTOMER.equals(request.getRole())) {
			dbUser.setStatus(UserStatus.ENABLED.getCode());
		} else {
			dbUser.setStatus(UserStatus.DISABLED.getCode());
		}
		if (UserRole.CORPORATE_EMPLOYEE.equals(request.getRole()) && request.isEnabled()) {
			dbUser.setStatus(UserStatus.ENABLED.getCode());
		}

		dbUser.setId(id);
		if (StringUtils.isEmpty(dbUser.getUserId()))
			dbUser.setUserId(com.tgs.services.base.utils.user.UserUtils.generateUserId(id, request.getRole()));
		if (dbUser.getCreatedOn() == null)
			dbUser.setCreatedOn(LocalDateTime.now());
		// dbUser.setCreated_by(getContextData().getUser().getUserId());
		LogUtils.log("registation#beforedefault", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"registation#start");
		setDefaults(dbUser);
		userService.save(dbUser);
		LogUtils.log("registation#save", LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(),
				"registation#start");
		// ExecutorUtils.getGeneralPurposeThreadPool().submit(() ->
		// userHelper.process());
		paymentCommunicator.updateUserWallet(UserWallet.builder().userId(dbUser.getUserId()).build());
		LogUtils.log("registation#walletregistration",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), "registation#start");

		if (BooleanUtils.isNotFalse(request.getSendEmailSms())) {
			sendEmail(dbUser);
			sendSms(dbUser);
		}
		LogUtils.log("registation#postcommunication",
				LogMetaInfo.builder().timeInMs(System.currentTimeMillis()).build(), "registation#start");

		response.setUsers(new ArrayList<>());
		response.getUsers().add(dbUser.toDomain());
	}

	@Override
	public void afterProcess() throws Exception {}

	private void sendSms(DbUser user) {
		AbstractMessageSupplier<SmsAttributes> msgAttributes = new AbstractMessageSupplier<SmsAttributes>() {
			@Override
			public SmsAttributes get() {
				Map<String, String> attributes = new HashMap<>();
				attributes.put("userId", user.getUserId());
				attributes.put("userName", user.getName());
				attributes.put("userEmail", user.getEmail());
				attributes.put("userId", user.getUserId());
				SmsAttributes smsAttr = SmsAttributes.builder().key(SmsTemplateKey.USER_REGISTERATION_SMS.name())
						.recipientNumbers(Arrays.asList(user.getMobile())).attributes(attributes)
						.partnerId(user.getPartnerId()).role(UserRole.getEnumFromCode(user.getRole())).build();
				return smsAttr;
			}
		};
		msgSrvCommunicator.sendMessage(msgAttributes.getAttributes());
	}

	private void sendEmail(DbUser dbUser) {
		User user = dbUser.toDomain();
		AbstractMessageSupplier<RegisterEmailAttributes> msgAttributes =
				new AbstractMessageSupplier<RegisterEmailAttributes>() {
					@Override
					public RegisterEmailAttributes get() {
						RegisterEmailAttributes emailAttributes = RegisterEmailAttributes.builder()
								.toEmailId(user.getEmail()).email(user.getEmail()).role(user.getRole())
								.mobile(user.getMobile()).name(user.getName()).userId(user.getUserId())
								.partnerId(user.getPartnerId()).status(StringUtils.capitalize(user.getStatus().name()))
								.key(EmailTemplateKey.SIGN_UP_EMAIL.name()).build();
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(msgAttributes.getAttributes());
	}

	private void setDefaults(DbUser dbUser) {
		ClientGeneralInfo clientGeneralInfo = gsCommunicator.getConfigRule(ConfiguratorRuleType.CLIENTINFO, null);
		ClientDefaultConfiguration defaultConfiguration = clientGeneralInfo.getDefaultConfiguration();

		if (StringUtils.isNotBlank(clientGeneralInfo.getRegistrationBean())) {
			AbstractRegistrationManager registrationManager = (AbstractRegistrationManager) SpringContext
					.getApplicationContext().getBean(clientGeneralInfo.getRegistrationBean());
			registrationManager.process(dbUser);
		}
		dbUser.setPartnerId(UserUtils.DEFAULT_PARTNERID);
		if (isParentUser(request.getRole())) {
			User parentUser = SystemContextHolder.getContextData().getUser();
			dbUser.setParentUserId(parentUser.getUserId());
			dbUser.getAdditionalInfo().setLogoURL(parentUser.getAdditionalInfo().getLogoURL());
			dbUser.getAdditionalInfo().setTdsRate(parentUser.getAdditionalInfo().getTdsRate());
		} else {
			dbUser.getAdditionalInfo().setTdsRate(defaultConfiguration.getTdsRate());
		}
	}

	private boolean isParentUser(UserRole roleOfUserBeingCreated) {
		User loggedInUser = SystemContextHolder.getContextData().getUser();
		if (loggedInUser != null) {
			UserRole loggedInUserRole = loggedInUser.getRole();
			switch (loggedInUserRole) {
				case DISTRIBUTOR:
					return UserRole.DISTRIBUTOR_STAFF.equals(roleOfUserBeingCreated);
				case AGENT:
					return UserRole.AGENT_STAFF.equals(roleOfUserBeingCreated);
				case CORPORATE:
					return UserRole.CORPORATE_EMPLOYEE.equals(roleOfUserBeingCreated)
							|| UserRole.CORPORATE_STAFF.equals(roleOfUserBeingCreated);
				default:
					return false;
			}
		}
		return false;
	}
}
