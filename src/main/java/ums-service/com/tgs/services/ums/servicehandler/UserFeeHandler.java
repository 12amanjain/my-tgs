package com.tgs.services.ums.servicehandler;

import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.ums.datamodel.fee.UserFeeType;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.ums.datamodel.fee.UserFee;
import com.tgs.services.ums.dbmodel.DbUserFee;
import com.tgs.services.ums.jparepository.UserFeeService;
import com.tgs.services.ums.restmodel.UserFeeResponse;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserFeeHandler extends ServiceHandler<UserFee, UserFeeResponse> {

	@Autowired
	UserFeeService userFeeService;

	@Autowired
	SyncService syncService;
	
	@Override
	public void beforeProcess() throws Exception {
		DbUserFee dbRequest = null;
		if (contextData.getUser() != null) {
			request.setUserId(contextData.getUser().getUserId());
			if (Objects.nonNull(request.getId())) {
				dbRequest = userFeeService.fetchById(request.getId());
			}
			dbRequest = Optional.ofNullable(dbRequest).orElse(new DbUserFee()).from(request);
			if (Objects.isNull(request.getId())) {
				for (DbUserFee userFee : userFeeService.getUserFeesForUser(dbRequest.getUserId())) {
					if (dbRequest.equals(userFee)) {
						throw new CustomGeneralException(SystemError.DUPLICATE_REQUEST);
					}
				}
			}
			dbRequest.setIsDeleted(false);
			dbRequest = userFeeService.save(dbRequest);
			request.setId(dbRequest.getId());
			syncService.sync("ums", dbRequest.toDomain());
		}
	}

	@Override
	public void process() throws Exception {
	}

	@Override
	public void afterProcess() throws Exception {

		if (request.getId() == null || SystemContextHolder.getContextData().getUser() == null) {
			ErrorDetail errorDetail = new ErrorDetail();
			errorDetail.setMessage(SystemError.INVALID_USERID.getMessage());
			errorDetail.setErrCode(SystemError.INVALID_USERID.errorCode());
			response.setErrors(Arrays.asList(errorDetail));
		} else {
			response.getUserFees().add(request);
		}
	}

	/**
	 * @param type- UserFee Type Ex: MarkUp
	 */
	public UserFeeResponse getUserFeeList(String type) {
		UserFeeResponse feeResponse = new UserFeeResponse();
		userFeeService
				.find(UserFeeType.valueOf(type).getCode(), SystemContextHolder.getContextData().getUser().getUserId())
				.forEach(feeRQ -> {
					feeResponse.getUserFees().add(feeRQ.toDomain());
				});
		return feeResponse;
	}

	/**
	 * @param id- UserFee id
	 */
	public BaseResponse deleteFee(String id) {
		if (StringUtils.isNotBlank(id)) {
			DbUserFee userFee = userFeeService.fetchById(NumberUtils.toLong(id));
			if (userFee.getUserId().equals(SystemContextHolder.getContextData().getUser().getUserId())) {
				userFee.setIsDeleted(true);
				userFee.setEnabled(false);
				log.info("Deleted UserFee for userId {} id {}",
						SystemContextHolder.getContextData().getUser().getUserId(), id);
				userFeeService.save(userFee);
				syncService.sync("ums", null);
			} else {
				throw new CustomGeneralException(SystemError.INVALID_USERID);
			}
		}
		return new BaseResponse();
	}
}
