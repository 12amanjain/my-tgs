package com.tgs.services.ums.servicehandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.tgs.filters.UserFilter;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.utils.TgsCollectionUtils;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserStatus;
import com.tgs.services.ums.dbmodel.DbUser;
import com.tgs.services.ums.jparepository.UserService;
import com.tgs.services.ums.restmodel.UserResponse;
import com.tgs.services.ums.validator.EmulateValidator;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class UserFetchHandler extends ServiceHandler<UserFilter, UserResponse> {

	@Autowired
	private UserService usrService;

	@Autowired
	private EmulateValidator emulateValidator;

	@Override
	public void beforeProcess() throws Exception {
	}

	@Override
	public void process() throws Exception {
		User loggedInUser = contextData.getUser();
		log.debug("[UserListing] In UserFetchHandler ");
		List<DbUser> userList = new ArrayList<>();

		for (int i = 0; i < 500; i++) {
			
			PageAttributes pageAttr = new PageAttributes();
			pageAttr.setPageNumber(i);
			pageAttr.setSize(5000);
			SortByAttributes srtBy = new SortByAttributes();
			srtBy.setParams(Arrays.asList("id"));
			srtBy.setOrderBy("asc");
			pageAttr.setSortByAttr(Arrays.asList(srtBy));
			request.setPageAttr(pageAttr);
			List<DbUser> userChunk = usrService.search(request);
			if (CollectionUtils.isEmpty(userChunk))
				break;
			userList.addAll(userChunk);
		}
		
		log.debug("[UserListing] Fetched userlisting from db");
		List<User> users = new ArrayList<>();
		if (CollectionUtils.isNotEmpty(userList)) {
			userList.forEach(dbuser -> {
				User user = dbuser.toDomain();
				user.setCanBeEmulated(canBeEmulated(loggedInUser, user));
				/**
				 * No filtering on the basis of group if it is empty string list.
				 */
				if (TgsCollectionUtils.isEmptyStringCollection(request.getGroups()) || TgsCollectionUtils.haveNonNullIntersection(user.getAdditionalInfo().getGroups(), request.getGroups())) {
					users.add(user);
				}
			});
			response.setUsers(users);
		}
	}

	@Override
	public void afterProcess() throws Exception {
	}

	private boolean canBeEmulated(User loggedInUser, User emulateUser) {
		if (loggedInUser == null || !emulateUser.getStatus().equals(UserStatus.ENABLED)
				|| emulateUser.getRole().equals(loggedInUser.getRole()))
			return false;
		if (loggedInUser.getRole().equals(UserRole.AGENT) && !(emulateUser.getRole().equals(UserRole.AGENT_STAFF)
				|| emulateUser.getParentUserId().equals(loggedInUser.getUserId())))
			return false;
		return emulateValidator.validate(loggedInUser, emulateUser);
	}
}
