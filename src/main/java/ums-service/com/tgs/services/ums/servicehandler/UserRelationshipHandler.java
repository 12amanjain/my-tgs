package com.tgs.services.ums.servicehandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.collect.Lists;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.communicator.MsgServiceCommunicator;
import com.tgs.services.base.datamodel.EmailAttributes;
import com.tgs.services.base.enums.EmailTemplateKey;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.utils.msg.AbstractMessageSupplier;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.datamodel.UserCacheFilter;
import com.tgs.services.ums.dbmodel.DbUserRelation;
import com.tgs.services.ums.helper.UserHelper;
import com.tgs.services.ums.helper.UserRelationshipHelper;
import com.tgs.services.ums.jparepository.UserRelationService;
import com.tgs.services.ums.restmodel.RelationshipsRequest;

@Service
public class UserRelationshipHandler extends ServiceHandler<RelationshipsRequest, BaseResponse> {

	@Autowired
	UserRelationService service;

	@Autowired
	private UserHelper userHelper;

	@Autowired
	MsgServiceCommunicator msgSrvCommunicator;

	@Override
	public void beforeProcess() throws Exception {}

	@Override
	public void process() throws Exception {

		List<String> userIdList = Lists.newArrayList(request.getRelatedUserIds());
		userIdList.add(request.getUserId());
		Map<String, User> userMap = UserHelper.getUserMap(UserCacheFilter.builder().userIds(userIdList).build());
		List<String> invalidUserIds = new ArrayList<>();
		userIdList.forEach(userId -> {
			if (userMap.get(userId) == null)
				invalidUserIds.add(userId);
		});
		if (CollectionUtils.isNotEmpty(invalidUserIds)) {
			throw new CustomGeneralException(SystemError.INVALID_USERIDS,
					SystemError.INVALID_USERIDS.getMessage(invalidUserIds.toString()));
		}

		int priority = 0;
		List<DbUserRelation> existingRelations = service.findByUserId1(request.getUserId());
		for (String child : request.getRelatedUserIds()) {
			DbUserRelation relation = ObjectUtils
					.firstNonNull(service.findByUserId1AndUserId2(request.getUserId(), child), new DbUserRelation());
			relation.setUserId2(child);
			relation.setUserName2(userMap.get(child).getName());
			relation.setDepth(1);
			relation.setPriority(++priority);
			relation.setUserId1(request.getUserId());
			relation.setUserName1(userMap.get(request.getUserId()).getName());
			existingRelations.removeIf(r -> r.getUserId2().equals(child));
			service.save(relation);
			if (priority == 1)
				sendHierachyUpdateEmail(request.getUserId(), child);
		}

		existingRelations.forEach(rel -> {
			UserRelationshipHelper.deleteFromCacheForUserId1(rel);
			service.delete(rel);
		});

		// To set/reset allowed user ids
		userIdList.forEach(userId -> {
			userHelper.updateCache(userId);
		});
	}

	private void sendHierachyUpdateEmail(String userdId1, String userId2) {
		AbstractMessageSupplier<EmailAttributes> emailAttributeSupplier =
				new AbstractMessageSupplier<EmailAttributes>() {
					@Override
					public EmailAttributes get() {
						User user = UserHelper.getUserFromCache(userdId1);
						EmailAttributes emailAttributes = EmailAttributes.builder().build();
						emailAttributes.setToEmailId(user.getEmail());
						emailAttributes.setKey(EmailTemplateKey.SALES_HIERACHY_EMAIL.name());
						User u = UserHelper.getUserFromCache(userId2);
						emailAttributes.setSalesRepName(u.getName());
						emailAttributes.setSalesRepMobile(u.getMobile());
						emailAttributes.setSalesRepEmail(u.getEmail());
						emailAttributes.setRole(user.getRole());
						return emailAttributes;
					}
				};
		msgSrvCommunicator.sendMail(emailAttributeSupplier.getAttributes());
	}

	@Override
	public void afterProcess() throws Exception {
		// TODO Auto-generated method stub

	}

}
