package com.tgs.services.ums.validator;

import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.stereotype.Service;
import com.tgs.services.base.enums.UserRole;
import com.tgs.services.base.runtime.SystemContextHolder;
import com.tgs.services.base.utils.user.UserUtils;
import com.tgs.services.ums.datamodel.ApplicationArea;
import com.tgs.services.ums.datamodel.AreaRole;
import com.tgs.services.ums.datamodel.AreaRoleMapping;
import com.tgs.services.ums.datamodel.User;
import com.tgs.services.ums.helper.AreaRoleHelper;

@Service
public class EmulateValidator {

	public boolean validate(User loggedInUser, User emulateUser) {

		String partnerId = SystemContextHolder.getContextData().getHttpHeaders().getPartnerId();
		
		//WL staff can not be emulated on client's platform
		if(UserUtils.DEFAULT_PARTNERID.equals(partnerId)) {
			String emulateUserPartnerId = emulateUser.getPartnerId();
			if(!UserUtils.DEFAULT_PARTNERID.equals(emulateUserPartnerId))
				return false;
		}
		
		List<AreaRoleMapping> areaRoleMappingList = AreaRoleHelper.getMappingsByAreaRoleIn(ApplicationArea.EMULATE.getAreaRoles());
		Map<UserRole, Set<AreaRole>> map = AreaRoleMapping.getMapByUserRole(areaRoleMappingList);

		if (map == null)
			return false;
		UserRole role = loggedInUser.getRole();
		if (map.get(role) != null
				&& (emulateUser.getRole().equals(UserRole.AGENT) || emulateUser.getRole().equals(UserRole.AGENT_STAFF))
				&& !map.get(role).contains(AreaRole.EMULATE_AGENT)) {
			return false;
		}

		if (map.get(role) != null
				&& (emulateUser.getRole().equals(UserRole.DISTRIBUTOR)
						|| emulateUser.getRole().equals(UserRole.DISTRIBUTOR_STAFF))
				&& !map.get(role).contains(AreaRole.EMULATE_DISTRIBUTOR)) {
			return false;
		}

		return true;
	}
}
