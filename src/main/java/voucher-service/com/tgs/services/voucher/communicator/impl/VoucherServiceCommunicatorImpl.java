package com.tgs.services.voucher.communicator.impl;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.VoucherConfigurationFilter;
import com.tgs.services.base.communicator.FMSCommunicator;
import com.tgs.services.base.communicator.HMSCommunicator;
import com.tgs.services.base.communicator.VoucherServiceCommunicator;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.vms.restmodel.FlightVoucherValidateRequest;
import com.tgs.services.vms.restmodel.FlightVoucherValidateResponse;
import com.tgs.services.vms.restmodel.HotelVoucherValidateRequest;
import com.tgs.services.vms.restmodel.HotelVoucherValidateResponse;
import com.tgs.services.vms.restmodel.VoucherValidateRequest;
import com.tgs.services.vms.restmodel.VoucherValidateResponse;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.dbmodel.DbVoucherConfiguration;
import com.tgs.services.voucher.jparepository.VoucherConfigurationService;
import com.tgs.services.voucher.servicehandler.VoucherConfigurationHandler;

@Service
public class VoucherServiceCommunicatorImpl implements VoucherServiceCommunicator {

	@Autowired
	VoucherConfigurationService voucherService;

	@Autowired
	VoucherConfigurationHandler configurationHandler;

	@Autowired
	FMSCommunicator fmsCommunicator;

	@Autowired
	HMSCommunicator hmsCommunicator;

	@Override
	public List<VoucherConfiguration> findAllWithDescPriority(VoucherConfigurationFilter configurationFilter) {
		return configurationHandler.findAllWithDescPriority(configurationFilter);
	}

	@Override
	public VoucherValidateResponse applyVoucher(VoucherValidateRequest request, boolean modifyFareDetail) {
		request = preProcessVoucherRules(request);
		if (CollectionUtils.isNotEmpty(request.getConfigurations())) {
			if (Product.AIR.equals(request.getProduct())) {
				FlightVoucherValidateRequest voucherRq =
						new FlightVoucherValidateRequest(request.getBookingId(), request.getVoucherCode(),
								request.getProduct(), request.getMediums(), request.getConfigurations());
				return fmsCommunicator.applyVoucher(voucherRq, modifyFareDetail);
			} else if (Product.HOTEL.equals(request.getProduct())) {
				HotelVoucherValidateRequest voucherRq =
						new HotelVoucherValidateRequest(request.getBookingId(), request.getVoucherCode(),
								request.getProduct(), request.getMediums(), request.getConfigurations());
				return hmsCommunicator.applyVoucher(voucherRq, modifyFareDetail);
			}
		}
		return null;
	}

	private VoucherValidateRequest preProcessVoucherRules(VoucherValidateRequest request) {
		// To do fetch from cache
		VoucherConfigurationFilter filter = VoucherConfigurationFilter.builder().enabled(true).deleted(false)
				.voucherCodeIn(Arrays.asList(request.getVoucherCode().toUpperCase())).build();
		// Later on for voucher codes listing, we can iterate on all the codes. For now, only one voucher code
		// will be applicable with highest priority
		List<VoucherConfiguration> voucherConfigurations = findAllWithDescPriority(filter);
		if (org.apache.commons.collections4.CollectionUtils.isNotEmpty(voucherConfigurations)) {
			LocalDateTime now = LocalDateTime.now();
			voucherConfigurations = voucherConfigurations.stream().filter(config -> now.isBefore(config.getExpiry()))
					.collect(Collectors.toList());
		}
		request.setConfigurations(CollectionUtils.isNotEmpty(voucherConfigurations) ? voucherConfigurations : null);
		return request;
	}

	@Override
	public FlightVoucherValidateResponse applyAirVoucher(FlightVoucherValidateRequest request, boolean modifyDetail) {
		VoucherValidateRequest vrRequest = preProcessVoucherRules(request);
		request.setConfigurations(vrRequest.getConfigurations());
		if (CollectionUtils.isNotEmpty(request.getConfigurations())) {
			FlightVoucherValidateResponse voucherResponse = fmsCommunicator.applyVoucher(request, modifyDetail);
			return voucherResponse;
		}
		return null;
	}

	@Override
	public HotelVoucherValidateResponse applyHotelVoucher(HotelVoucherValidateRequest request, boolean modifyDetail) {
		VoucherValidateRequest vrRequest = preProcessVoucherRules(request);
		request.setConfigurations(vrRequest.getConfigurations());
		if (CollectionUtils.isNotEmpty(request.getConfigurations())) {
			HotelVoucherValidateResponse voucherResponse = hmsCommunicator.applyVoucher(request, modifyDetail);
			return voucherResponse;
		}
		return null;
	}

	@Override
	public VoucherConfiguration saveVoucherConfiguration(VoucherConfiguration voucherConfiguration) {
		DbVoucherConfiguration voucherConfig =
				voucherService.saveOrUpdate(new DbVoucherConfiguration().from(voucherConfiguration));
		return voucherConfig.toDomain();
	}
}
