package com.tgs.services.voucher.dbmodel;

import java.time.LocalDateTime;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;
import org.hibernate.envers.Audited;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.dbmodel.BaseModel;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.datamodel.VoucherConfigurationRule;
import com.tgs.services.voucher.hibernate.VoucherConfigurationRuleType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
@Getter
@Setter
@Audited
@Table(name = "voucherconfig")
@TypeDefs({@TypeDef(name = "VoucherConfigurationRuleType", typeClass = VoucherConfigurationRuleType.class)})
public class DbVoucherConfiguration extends BaseModel<DbVoucherConfiguration, VoucherConfiguration> {

	@Column
	@NonNull
	private String voucherCode;

	@Column
	private String description;

	@Column
	private boolean enabled;

	@Column
	private boolean isDeleted;

	@Column
	private Double priority;

	@Column
	private Integer quantityConsumed;

	@Column
	@Type(type = "VoucherConfigurationRuleType")
	private List<VoucherConfigurationRule> voucherRules;

	@Column
	private LocalDateTime expiry;

	@Column(updatable = false)
	@CreationTimestamp
	private LocalDateTime createdOn;

	@CreationTimestamp
	private LocalDateTime processedOn;


	@Override
	public VoucherConfiguration toDomain() {
		return new GsonMapper<>(this, VoucherConfiguration.class, true).convert();
	}

	@Override
	public DbVoucherConfiguration from(VoucherConfiguration dataModel) {
		return new GsonMapper<>(dataModel, this, DbVoucherConfiguration.class, true).convert();
	}

}
