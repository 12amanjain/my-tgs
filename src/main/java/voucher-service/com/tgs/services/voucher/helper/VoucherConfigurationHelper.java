package com.tgs.services.voucher.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.google.common.reflect.TypeToken;
import com.tgs.filters.VoucherConfigurationFilter;
import com.tgs.services.base.CustomInMemoryHashMap;
import com.tgs.services.base.InMemoryInitializer;
import com.tgs.services.cacheservice.datamodel.CacheMetaInfo;
import com.tgs.services.cacheservice.datamodel.CacheSetName;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.jparepository.VoucherConfigurationService;

@Service
public class VoucherConfigurationHelper extends InMemoryInitializer {

	private static CustomInMemoryHashMap voucherRules;

	private static final String FIELD = "voucher_rules";

	@Autowired
	private VoucherConfigurationService voucherService;


	public VoucherConfigurationHelper(CustomInMemoryHashMap configurationHashMap,
			CustomInMemoryHashMap voucherConfigRules) {
		super(configurationHashMap);
		VoucherConfigurationHelper.voucherRules = voucherConfigRules;
	}

	@Override
	public void process() {
		List<VoucherConfiguration> voucherConfigurations =
				voucherService.findAll(VoucherConfigurationFilter.builder().build());
		Map<String, List<VoucherConfiguration>> ruleMap =
				voucherConfigurations.stream().collect(Collectors.groupingBy(VoucherConfiguration::getVoucherCode));
		ruleMap.forEach((key, value) -> voucherRules.put(key, FIELD, value, CacheMetaInfo.builder().compress(false)
				.expiration(InMemoryInitializer.NEVER_EXPIRE).set(CacheSetName.VOUCHER_CONFIG.getName()).build()));

	}

	@Override
	public void deleteExistingInitializer() {
		voucherRules.truncate(CacheSetName.VOUCHER_CONFIG.getName());
	}

	@SuppressWarnings("unchecked")
	public static List<VoucherConfiguration> getVoucherConfigOnCode(String voucherCode) {
		voucherCode = voucherCode.toUpperCase();
		List<VoucherConfiguration> ruleList = new ArrayList<>();
		return voucherRules.get(voucherCode, FIELD, ruleList.getClass(),
				CacheMetaInfo.builder().set(CacheSetName.VOUCHER_CONFIG.getName()).compress(false)
						.typeOfT(new TypeToken<List<VoucherConfiguration>>() {
							private static final long serialVersionUID = 1L;
						}.getType()).build());
	}

}
