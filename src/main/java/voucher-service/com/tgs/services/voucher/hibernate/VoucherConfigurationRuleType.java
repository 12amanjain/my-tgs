package com.tgs.services.voucher.hibernate;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.postgresql.util.PGobject;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tgs.services.base.EnumTypeAdapterFactory;
import com.tgs.services.base.gson.DBExclusionStrategy;
import com.tgs.services.base.gson.GsonUtils;
import com.tgs.services.base.runtime.database.CustomTypes.CustomUserType;
import com.tgs.services.voucher.datamodel.VoucherConfigurationRule;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VoucherConfigurationRuleType extends CustomUserType {

	@Override
	public Class returnedClass() {
		List<VoucherConfigurationRule> voucherRules = new ArrayList<>();
		return voucherRules.getClass();
	}

	@Override
	public Type returnedType() {
		return new TypeToken<List<VoucherConfigurationRule>>() {}.getType();
	}

	@SuppressWarnings({"unchecked", "rawtypes"})
	@Override
	public Object nullSafeGet(ResultSet rs, String[] names, SharedSessionContractImplementor session, Object owner)
			throws HibernateException, SQLException {
		Gson gson = getGson();
		PGobject o = (PGobject) rs.getObject(names[0]);
		if (o != null && o.getValue() != null) {
			try {
				if (returnedType() != null) {
					GsonBuilder gsonBuilder = GsonUtils.getGsonBuilder(Arrays.asList(new DBExclusionStrategy()),
							Arrays.asList(new EnumTypeAdapterFactory()));
					String str = o.getValue();
					Optional<Class> typeArgClass = Optional.empty();
					if (returnedType() instanceof ParameterizedType) {
						typeArgClass =
								Optional.of((Class) ((ParameterizedType) returnedType()).getActualTypeArguments()[0]);
					}
					GsonUtils.registerAdapter(str, returnedClass(), gsonBuilder, null, typeArgClass);
					Object obj = gsonBuilder.create().fromJson(str, returnedType());
					return obj;
				}
				return gson.fromJson(o.getValue(), returnedClass());
			} catch (Exception e) {
				log.error("[VoucherConfigurationRuleType]Unable to deserialize value {}", o.getValue(), e);
			}
		}

		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Object deepCopy(Object value) throws HibernateException {
		Gson gson = getGson();
		String str = gson.toJson(value);
		if (returnedType() != null) {
			GsonBuilder gsonBuilder = GsonUtils.getGsonBuilder(Arrays.asList(new DBExclusionStrategy()),
					Arrays.asList(new EnumTypeAdapterFactory()));
			Optional<Class> typeArgClass = Optional.empty();
			if (returnedType() instanceof ParameterizedType) {
				typeArgClass = Optional.of((Class) ((ParameterizedType) returnedType()).getActualTypeArguments()[0]);
			}
			GsonUtils.registerAdapter(str, returnedClass(), gsonBuilder, null, typeArgClass);
			Object obj = gsonBuilder.create().fromJson(str, returnedType());
			return obj;
		}
		return gson.fromJson(gson.toJson(value), returnedClass());
	}

}

