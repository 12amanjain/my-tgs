package com.tgs.services.voucher.jparepository;

import java.time.LocalDateTime;
import java.util.List;
import com.tgs.filters.VoucherConfigurationFilter;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.dbmodel.DbVoucherConfiguration;
import com.tgs.services.voucher.helper.VoucherConfigurationHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.services.base.SearchService;
import com.tgs.services.base.dbmodel.BaseModel;

@Service
public class VoucherConfigurationService extends SearchService<DbVoucherConfiguration> {

	@Autowired
	VoucherConfigurationRepository voucherRepo;

	@Autowired
	VoucherConfigurationHelper configurationHelper;

	public DbVoucherConfiguration saveOrUpdate(DbVoucherConfiguration voucherRule) {
		voucherRule.setProcessedOn(LocalDateTime.now());
		voucherRule = voucherRepo.save(voucherRule);
		configurationHelper.process();
		return voucherRule;
	}

	public DbVoucherConfiguration fetchById(Long id) {
		return voucherRepo.findOne(id);
	}

	public List<VoucherConfiguration> findAll(VoucherConfigurationFilter voucherFilter) {
		return BaseModel.toDomainList(super.search(voucherFilter, voucherRepo));
	}

}
