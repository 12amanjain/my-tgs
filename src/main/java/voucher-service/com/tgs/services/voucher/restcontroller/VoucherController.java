package com.tgs.services.voucher.restcontroller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import com.tgs.services.vms.restmodel.VoucherRuleResponse;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.dbmodel.DbVoucherConfiguration;
import com.tgs.services.voucher.servicehandler.VoucherConfigurationHandler;
import com.tgs.services.voucher.validator.rule.VoucherRuleCriteriaValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.tgs.filters.VoucherConfigurationFilter;
import com.tgs.services.base.AuditsHandler;
import com.tgs.services.base.annotations.CustomRequestMapping;
import com.tgs.services.base.annotations.CustomRequestProcessor;
import com.tgs.services.base.restmodel.AuditResponse;
import com.tgs.services.base.restmodel.AuditsRequest;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.runtime.spring.UserIdResponseProcessor;
import com.tgs.services.ums.datamodel.AreaRole;
import java.util.List;

@RestController
@RequestMapping("/pms/v1/voucher")
public class VoucherController {

	@Autowired
	private VoucherConfigurationHandler voucherConfigurationHandler;

	@Autowired
	VoucherRuleCriteriaValidator ruleValidator;

	@Autowired
	private AuditsHandler auditHandler;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(ruleValidator);
	}

	@RequestMapping(value = "/save", method = RequestMethod.POST)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT, isMidOfficeAllowed = true)
	protected VoucherRuleResponse saveVoucherConfigRule(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid VoucherConfiguration confRule) throws Exception {
		voucherConfigurationHandler.initData(confRule, new VoucherRuleResponse());
		return voucherConfigurationHandler.getResponse();
	}


	@RequestMapping(value = "/list", method = RequestMethod.POST)
	protected VoucherRuleResponse listVoucherConfigRules(HttpServletRequest request, HttpServletResponse response,
			@RequestBody @Valid VoucherConfigurationFilter voucherConfigFilter) throws Exception {
		List<VoucherConfiguration> rules = voucherConfigurationHandler.findAll(voucherConfigFilter);
		VoucherRuleResponse ruleResponse = new VoucherRuleResponse();
		ruleResponse.getVoucherConfigurations().addAll(rules);
		return ruleResponse;
	}

	@RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT, isMidOfficeAllowed = true)
	protected BaseResponse deleteVoucherConfigRule(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id) throws Exception {
		return voucherConfigurationHandler.deleteVoucherRule(id);
	}

	@RequestMapping(value = "/status/{id}/{status}", method = RequestMethod.GET)
	@CustomRequestProcessor(areaRole = AreaRole.CONFIG_EDIT, isMidOfficeAllowed = true)
	protected BaseResponse changeVoucherRuleStatus(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("id") Long id, @PathVariable("status") Boolean status) throws Exception {
		return voucherConfigurationHandler.updateVoucherRuleStatus(id, status);

	}

	@RequestMapping(value = "/audits", method = RequestMethod.POST)
	@CustomRequestMapping(responseProcessors = {UserIdResponseProcessor.class})
	protected @ResponseBody AuditResponse fetchAudits(HttpServletRequest request, HttpServletResponse response,
			@Valid @RequestBody AuditsRequest auditsRequestData) throws Exception {
		AuditResponse auditResponse = new AuditResponse();
		auditResponse.setResults(auditHandler.fetchAudits(auditsRequestData, DbVoucherConfiguration.class, ""));
		return auditResponse;
	}
}

