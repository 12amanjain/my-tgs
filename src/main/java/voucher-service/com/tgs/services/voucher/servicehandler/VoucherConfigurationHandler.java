package com.tgs.services.voucher.servicehandler;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tgs.filters.VoucherConfigurationFilter;
import com.tgs.services.base.CustomGeneralException;
import com.tgs.services.base.ErrorDetail;
import com.tgs.services.base.GsonMapper;
import com.tgs.services.base.ServiceHandler;
import com.tgs.services.base.datamodel.PageAttributes;
import com.tgs.services.base.datamodel.SortByAttributes;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.restmodel.BaseResponse;
import com.tgs.services.base.sync.SyncService;
import com.tgs.services.vms.restmodel.VoucherRuleResponse;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.dbmodel.DbVoucherConfiguration;
import com.tgs.services.voucher.jparepository.VoucherConfigurationService;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class VoucherConfigurationHandler extends ServiceHandler<VoucherConfiguration, VoucherRuleResponse> {

	@Autowired
	protected VoucherConfigurationService voucherService;


	@Autowired
	SyncService syncService;

	@Override
	public void beforeProcess() throws Exception {
		// TODO Auto-generated method stub
	}

	@Override
	public void process() throws Exception {
		DbVoucherConfiguration voucherConfig = null;
		try {
			if (request != null && Objects.nonNull(request.getId())) {
				voucherConfig = voucherService.fetchById(Long.valueOf(request.getId()));
				voucherConfig = new DbVoucherConfiguration().from(request);
			} else {
				voucherConfig = new GsonMapper<>(request, DbVoucherConfiguration.class, true).convert();
				voucherConfig.setEnabled(Boolean.TRUE);
				voucherConfig.setQuantityConsumed(0);
			}
			voucherConfig = voucherService.saveOrUpdate(voucherConfig);
			syncService.sync("vms", voucherConfig.toDomain());
			response.getVoucherConfigurations().add(voucherConfig.toDomain());
		} catch (Exception e) {
			log.error("Unable to save voucher rule with voucher code {} because {}", request.getVoucherCode(),
					e.getMessage(), e);
			throw new CustomGeneralException("Save Or Update of Voucher Rule Failed {}" + e.getMessage());
		}
	}

	@Override
	public void afterProcess() throws Exception {

	}

	public List<VoucherConfiguration> findAllWithDescPriority(VoucherConfigurationFilter voucherConfigFilter) {
		List<VoucherConfiguration> voucherConfigurations = voucherService.findAll(voucherConfigFilter);
		if (CollectionUtils.isNotEmpty(voucherConfigurations)) {
			voucherConfigurations.sort(Comparator
					.comparing(VoucherConfiguration::getPriority, Comparator.nullsFirst(Comparator.naturalOrder()))
					.reversed());
		}
		return voucherConfigurations;
	}

	public List<VoucherConfiguration> findAll(VoucherConfigurationFilter voucherConfigFilter) {
		if (voucherConfigFilter.getPageAttr() == null) {
			PageAttributes pageAttr = new PageAttributes();
			SortByAttributes srtBy = new SortByAttributes();
			srtBy.setParams(Arrays.asList("createdOn"));
			srtBy.setOrderBy("desc");
			pageAttr.setSortByAttr(Arrays.asList(srtBy));
			voucherConfigFilter.setPageAttr(pageAttr);
		}
		return voucherService.findAll(voucherConfigFilter);
	}

	public BaseResponse deleteVoucherRule(Long id) {
		BaseResponse baseResponse = new BaseResponse();
		DbVoucherConfiguration voucherRule = voucherService.fetchById(id);
		if (Objects.nonNull(voucherRule)) {
			voucherRule.setEnabled(Boolean.FALSE);
			voucherRule.setDeleted(Boolean.TRUE);
			voucherService.saveOrUpdate(voucherRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

	public BaseResponse updateVoucherRuleStatus(Long id, Boolean status) {
		BaseResponse baseResponse = new BaseResponse();
		DbVoucherConfiguration voucherRule = voucherService.fetchById(id);
		if (Objects.nonNull(voucherRule)) {
			voucherRule.setEnabled(status);
			voucherService.saveOrUpdate(voucherRule);
		} else {
			baseResponse.addError(new ErrorDetail(SystemError.INVALID_ID));
		}
		return baseResponse;
	}

}
