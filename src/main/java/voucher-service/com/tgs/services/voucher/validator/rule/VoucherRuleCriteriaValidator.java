package com.tgs.services.voucher.validator.rule;

import java.util.List;
import com.tgs.services.base.validator.rule.BasicRuleCriteriaValidator;
import com.tgs.services.base.validator.rule.FlightBasicRuleCriteriaValidator;
import com.tgs.services.voucher.datamodel.VoucherConfiguration;
import com.tgs.services.voucher.datamodel.VoucherConfigurationRule;
import com.tgs.services.voucher.datamodel.VoucherRuleCriteriaOutput;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.amazonaws.util.CollectionUtils;
import com.tgs.services.base.datamodel.Product;
import com.tgs.services.base.helper.SystemError;
import com.tgs.services.base.utils.BaseUtils;

@Service
public class VoucherRuleCriteriaValidator implements Validator {

	@Autowired
	FlightBasicRuleCriteriaValidator flightBasicRuleCriteriaValidator;

	@Autowired BasicRuleCriteriaValidator generalVoucherCriteriaValidator;

	public boolean supports(Class<?> clazz) {
		return true;
	}

	@Override
	public void validate(Object target, Errors errors) {

		if (target == null) {
			return;
		}

		if (target instanceof VoucherConfiguration) {
			VoucherConfiguration voucherConfig = (VoucherConfiguration) target;

			if (StringUtils.isBlank(voucherConfig.getDescription())) {
				rejectValue(errors, "description", SystemError.BLANK_DESCRIPTION);
			}

			if (StringUtils.isBlank(voucherConfig.getVoucherCode())) {
				rejectValue(errors, "voucherCode", SystemError.BLANK_VOUCHER_CODE);
			}

			List<VoucherConfigurationRule> voucherRules = voucherConfig.getVoucherRules();

			if (CollectionUtils.isNullOrEmpty(voucherRules)) {
				rejectValue(errors, "voucherRules", SystemError.EMPTY_RULES);
			}

			for (int i = 0; i < voucherRules.size(); i++) {

				VoucherConfigurationRule voucherRule = voucherRules.get(i);
				String fieldName = "voucherRules[" + i + "]";

				if (voucherRule.getProduct() == null) {
					rejectValue(errors, fieldName + ".product", SystemError.INVALID_PRODUCT);
				}

				if (Product.AIR.equals(voucherRule.getProduct())) {
					flightBasicRuleCriteriaValidator.validateCriteria(errors, fieldName + ".inclusionCriteria",
							voucherRule.getInclusionCriteria());
					flightBasicRuleCriteriaValidator.validateCriteria(errors, fieldName + ".exclusionCriteria",
							voucherRule.getExclusionCriteria());
				}

				generalVoucherCriteriaValidator.validateCriteria(errors, fieldName + ".inclusionCriteria",
						voucherRule.getInclusionCriteria());
				generalVoucherCriteriaValidator.validateCriteria(errors, fieldName + ".exclusionCriteria",
						voucherRule.getExclusionCriteria());

				VoucherRuleCriteriaOutput voucherCriteria = voucherRule.getOutput();

				if (voucherRule.getTotalQuantity() == null || voucherRule.getTotalQuantity() <= 0) {
					rejectValue(errors, fieldName + ".voucherCriteria.quantity", SystemError.INVALID_QUANTITY);
				}

				if (voucherCriteria.getThresholdAmount() == null) {
					rejectValue(errors, fieldName + ".voucherCriteria.thresholdAmount",
							SystemError.NULL_COMPONENT_THRESHOLD);
				}

				if (!BaseUtils.validateExpression(voucherCriteria.getExpression())) {
					rejectValue(errors, fieldName + ".voucherCriteria.expression",
							SystemError.INVALID_COMPONENT_EXPRESSION, voucherCriteria.getExpression());
				}

			}


		}
	}

	private void rejectValue(Errors errors, String fieldname, SystemError sysError, Object... args) {
		errors.rejectValue(fieldname, sysError.errorCode(), sysError.getMessage(args));
	}


}
