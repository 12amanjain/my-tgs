CREATE INDEX user_userid_idx ON users(userid) ;
CREATE INDEX user_date_idx ON users(createdon) ;

CREATE INDEX dr_userid_idx ON depositrequest(userid) ;
CREATE INDEX dr_date_idx ON depositrequest(createdon) ;

CREATE INDEX paymt_date_idx ON payment(createdon) ;
CREATE INDEX paymt_rdefid_idx ON payment(refid) ;
CREATE INDEX paymt_type_idx ON payment(type) ;
CREATE INDEX paymt_userid_idx ON payment(payuserid) ;

CREATE INDEX cl_date_idx ON creditline(createdon) ;
CREATE INDEX cl_status_idx ON creditline(status) ;

CREATE INDEX sysaudit_type_idx ON systemaudit(audittype) ;
CREATE INDEX sysaudit_date_idx ON systemaudit(createdon) ;

CREATE INDEX bill_settled_idx ON creditbill(issettled) ;
CREATE INDEX bill_date_idx ON creditbill(createdon) ;


