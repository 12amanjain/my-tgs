alter table users drop constraint if exists users_uniq_idx;
alter table users add constraint users_uniq_idx unique (email);