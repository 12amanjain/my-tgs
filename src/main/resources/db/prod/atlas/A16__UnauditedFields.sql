DELETE FROM collection_documents WHERE type = 'unaudited_fields';

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type", "owner")
VALUES(current_timestamp, '{"data":[{"fields":["additionalInfo.lastLoginTime","additionalInfo.lastTxnTime.AIR","additionalInfo.lastTxnTime.HOTEL"],"entity":"DbUser"},{"fields":["departAirportInfo.code[0]","arrivalAirportInfo.code[0]"],"entity":"DbAirInventory"},{"fields":["additionalInfo.firstUpdateTime.S","additionalInfo.firstUpdateTime.P","additionalInfo.firstUpdateTime.AB"],"entity":"DbOrder"}]}', true, 'NOTAUDITED_FIELDS', current_timestamp, 'unaudited_fields','Backend');

