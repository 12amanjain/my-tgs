update creditline set additionalinfo = additionalinfo || jsonb_build_object('maxTemporaryExt', to_jsonb((additionalinfo->>'maxTemporaryExt')::DOUBLE PRECISION/100));

update creditline set additionalinfo = additionalinfo || jsonb_build_object('curTemporaryExt', to_jsonb((additionalinfo->>'curTemporaryExt')::DOUBLE PRECISION/100));