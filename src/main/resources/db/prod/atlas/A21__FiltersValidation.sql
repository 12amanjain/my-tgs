DELETE FROM collection_documents where key like '%FILTER%';


INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"},{"field":"bookingUserIdIn"}],"days":31},{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":15},{"combination":[{"field":"bookingIdIn"}]}]}', true, 'AMENDMENT_FILTER_ADMIN', current_timestamp, 'AmendmentFilter');


INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"bookingIdIn"}]},{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":31}]}', true, 'AMENDMENT_FILTER', current_timestamp, 'AmendmentFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"refIdIn"}]},{"combination":[{"field":"refId"}]},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"},{"field":"typeIn"}],"days":32},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"},{"field":"payUserIdIn"}],"days":32},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":2}]}', true, 'PAYMENT_FILTER_ADMIN', current_timestamp, 'PaymentFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"refIdIn"}]},{"combination":[{"field":"refId"}]},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":32}]}', true, 'PAYMENT_FILTER', current_timestamp, 'PaymentFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"isSettled","value":"true"},{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":31}]}', true, 'CREDITBILL_FILTER', current_timestamp, 'CreditBillFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":15}]}', true, 'SYSTEM_AUDIT_FILTER_ADMIN', current_timestamp, 'SystemAuditFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":31}]}', true, 'SYSTEM_AUDIT_FILTER', current_timestamp, 'SystemAuditFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":31},{"combination":[{"field":"itemFilter.departedOnAfterDate"},{"field":"itemFilter.departedOnBeforeDate"}],"days":31},{"combination":[{"field":"hotelItemFilter.checkInAfterDate"},{"field":"hotelItemFilter.checkInBeforeDate"}],"days":31},{"combination":[{"field":"bookingIds"}]},{"combination":[{"field":"bookingUserIds"}]},{"combination":[{"field":"bookingId"}]}]}', true, 'ORDER_FILTER_AGENT', current_timestamp, 'OrderFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":10},{"combination":[{"field":"itemFilter.departedOnAfterDate"},{"field":"itemFilter.departedOnBeforeDate"}],"days":10},{"combination":[{"field":"hotelItemFilter.checkInAfterDate"},{"field":"hotelItemFilter.checkInBeforeDate"}],"days":10},{"combination":[{"field":"bookingIds"}]},{"combination":[{"field":"bookingUserIds"}]},{"combination":[{"field":"bookingId"}]}]}', true, 'ORDER_FILTER', current_timestamp, 'OrderFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":10},{"combination":[{"field":"itemFilter.departedOnAfterDate"},{"field":"itemFilter.departedOnBeforeDate"}],"days":10},{"combination":[{"field":"hotelItemFilter.checkInAfterDate"},{"field":"hotelItemFilter.checkInBeforeDate"}],"days":10},{"combination":[{"field":"bookingIds"}]},{"combination":[{"field":"bookingUserIds"}]},{"combination":[{"field":"bookingId"}]}]}', true, 'ORDER_FILTER_ADMIN', current_timestamp, 'OrderFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":10}]}', true, 'AIRLINE_TRANSACTION_FILTER_ADMIN', current_timestamp, 'AirLineTransactionReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":30}]}', true, 'AIRLINE_TRANSACTION_FILTER', current_timestamp, 'AirLineTransactionReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":10},{"combination":[{"field":"departedOnAfterDate"},{"field":"departedOnBeforeDate"}],"days":10},{"combination":[{"field":"bookedOnAfterDateTime"},{"field":"bookedOnBeforeDateTime"}],"days":10}]}', true, 'BOOKING_TRANSACTION_FILTER_ADMIN', current_timestamp, 'AirBookingTransactionReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":30},{"combination":[{"field":"departedOnAfterDate"},{"field":"departedOnBeforeDate"}],"days":30},{"combination":[{"field":"bookedOnAfterDateTime"},{"field":"bookedOnBeforeDateTime"}],"days":30}]}', true, 'BOOKING_TRANSACTION_FILTER', current_timestamp, 'AirBookingTransactionReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":10}]}', true, 'HOTEL_TRANSACTION_FILTER_ADMIN', current_timestamp, 'HotelTransactionReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":30}]}', true, 'HOTEL_TRANSACTION_FILTER', current_timestamp, 'HotelTransactionReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":10}]}', true, 'DYNAMIC_REPORT_FILTER_ADMIN', current_timestamp, 'ReportFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":30}]}', true, 'DYNAMIC_REPORT_FILTER', current_timestamp, 'ReportFilter');
