DELETE FROM collection_documents where key IN ('AMENDMENT_FILTER_ADMIN', 'AMENDMENT_FILTER','PAYMENT_FILTER_ADMIN','PAYMENT_FILTER','CREDITBILL_FILTER','SYSTEM_AUDIT_FILTER_ADMIN','SYSTEM_AUDIT_FILTER','ORDER_FILTER_AGENT,''ORDER_FILTER');


INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"bookingIdIn"}]},{"combination":[{"field":"bookingUserIdIn"},{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":30},{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":15}]}', true, 'AMENDMENT_FILTER_ADMIN', current_timestamp, 'AmendmentFilter');


INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"bookingIdIn"}]},{"combination":[{"field":"createdOnAfterDateTime"},{"field":"createdOnBeforeDateTime"}],"days":30}]}', true, 'AMENDMENT_FILTER', current_timestamp, 'AmendmentFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"refIdIn"}]},{"combination":[{"field":"refId"}]},{"combination":[{"field":"type"},{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":30},{"combination":[{"field":"payUserIdIn"},{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":30},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":2}]}', true, 'PAYMENT_FILTER_ADMIN', current_timestamp, 'PaymentFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"refIdIn"}]},{"combination":[{"field":"refId"}]},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":30}]}', true, 'PAYMENT_FILTER', current_timestamp, 'PaymentFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"isSettled","value":"true"},{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":30}]}', true, 'CREDITBILL_FILTER', current_timestamp, 'CreditBillFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":15}]}', true, 'SYSTEM_AUDIT_FILTER_ADMIN', current_timestamp, 'SystemAuditFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":30}]}', true, 'SYSTEM_AUDIT_FILTER', current_timestamp, 'SystemAuditFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"bookingIds"}]},{"combination":[{"field":"bookingUserIds"}]},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":30}]}', true, 'ORDER_FILTER_AGENT', current_timestamp, 'OrderFilter');

INSERT INTO collection_documents
(created_on, "data", enabled, "key", expiry, "type")
VALUES(current_timestamp, '{"validationCriterion":[{"combination":[{"field":"bookingIds"}]},{"combination":[{"field":"bookingUserIds"}]},{"combination":[{"field":"createdOnAfterDate"},{"field":"createdOnBeforeDate"}],"days":10}]}', true, 'ORDER_FILTER', current_timestamp, 'OrderFilter');
