 drop table IF EXISTS PaymentConfiguration;

 CREATE TABLE PaymentConfiguration (
 id bigserial not null,
 createdon timestamp DEFAULT now(),
 enabled boolean DEFAULT false,
 type character varying(255),
 medium character varying(255),
 inclusionCriteria jsonb  ,
 exclusionCriteria jsonb  ,
 output jsonb,
 priority float8,
 PRIMARY KEY (id)
 );
