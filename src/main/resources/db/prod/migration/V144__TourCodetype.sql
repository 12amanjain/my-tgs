alter table  tourcode drop column tourcodecriteria;
alter table tourcode add column tourcodecriteria jsonb;
alter table  aircommissionrule drop column commissionCriteria;
alter table aircommissionrule add column commissionCriteria jsonb;