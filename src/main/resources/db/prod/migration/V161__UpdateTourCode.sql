alter table tourcode add column processedon timestamp;
alter table fareruleinfo add column processedon timestamp;
update tourcode set processedon = createdon;
update fareruleinfo set processedon = createdon;