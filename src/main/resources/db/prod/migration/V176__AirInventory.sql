create table rateplan (
        id serial not null,
		name varchar(1000),
		rateplaninfo jsonb,
		description varchar(1000),
		createdon timestamp DEFAULT now(),
		product varchar(20),
		supplierId varchar(255) not null,
		isdeleted boolean default false,
		primary key (id)
);

create table rateplan_AUD (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
		name varchar(1000),
		rateplaninfo jsonb,
		description varchar(1000),
		createdon timestamp DEFAULT now(),
		product varchar(20),
		supplierId varchar(255) not null,
		isdeleted boolean default false,
        primary key (id, REV)
 );
 
create table airinventory (
        id serial not null,
		fromAirport varchar(25),
		toAirport varchar(25),
		tripType varchar(25),
		name varchar(1000),
		segmentInfos jsonb,
		carrier varchar(20),
		createdon timestamp DEFAULT now(),
		supplierId varchar(255) not null,
		disableBfrDept int4,
		isenabled boolean default true,
		isreturn boolean default false,
		primary key (id)
);

create table airinventory_AUD (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
		fromAirport varchar(25),
		toAirport varchar(25),
		tripType varchar(25),
		name varchar(1000),
		segmentInfos jsonb,
		carrier varchar(20),
		createdon timestamp DEFAULT now(),
		supplierId varchar(255),
		disableBfrDept int4,
		isenabled boolean default true,
		isreturn boolean default false,
        primary key (id, REV)
 );
 
 create table seatallocation (
         id serial not null,
         inventoryid varchar(255),
         createdon timestamp DEFAULT now(),
         validon date not null,
         rateplanid varchar(255) not null,
         totalSeats int4,
         seatsSold int4,
         supplierId varchar(255),
         additionalInfo jsonb,
         isDeleted boolean default false
 );
 
 create table seatallocation_AUD (
         id int4 not null,
         REV int8 not null,
         REVTYPE int2,
         inventoryid varchar(255),
         createdon timestamp DEFAULT now(),
         validon date,
         rateplanid varchar(255),
         totalSeats int4,
         seatsSold int4,
         additionalInfo jsonb,
         supplierId varchar(255),
         isDeleted boolean default false,
         primary key (id, REV)
 );
 
alter table rateplan_AUD add constraint rateplan_foreign_idx foreign key (REV) references customrev;

alter table airinventory_AUD add constraint airinventory_foreign_idx foreign key (REV) references customrev;

alter table seatallocation_AUD add constraint seat_foreign_idx foreign key (REV) references customrev;

CREATE INDEX on rateplan(name);

CREATE INDEX on rateplan(supplierId);

CREATE INDEX on rateplan(product);

CREATE INDEX on airinventory(name);

CREATE INDEX on airinventory(supplierId);

CREATE INDEX on airinventory(fromAirport);

CREATE INDEX on airinventory(toAirport);

CREATE INDEX on seatallocation(inventoryid);

