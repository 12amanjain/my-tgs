drop table if exists amendment;
drop table if exists amendment_aud;

create table amendment (
id BIGINT,
amendmentid VARCHAR(30),
bookingid VARCHAR(30),
amendmenttype VARCHAR(30),
status VARCHAR(30),
amount BIGINT,
info jsonb DEFAULT '{}',
modifiedInfo jsonb DEFAULT '{}',
additionalInfo jsonb DEFAULT '{}',
loggedinuserId VARCHAR(30),
bookingUserId VARCHAR(30),
assignedUserId VARCHAR(30),
createdon TIMESTAMP,
PRIMARY KEY (id)
);

create table amendment_aud (
id BIGINT,
rev BIGINT,
revtype SMALLINT,
amendmentid VARCHAR(30),
bookingid VARCHAR(30),
amendmenttype VARCHAR(30),
status VARCHAR(30),
amount BIGINT,
info jsonb DEFAULT '{}',
modifiedInfo jsonb DEFAULT '{}',
additionalInfo jsonb DEFAULT '{}',
loggedinuserId VARCHAR(30),
bookingUserId VARCHAR(30),
assignedUserId VARCHAR(30),
createdon TIMESTAMP,
PRIMARY KEY (id, rev)
);