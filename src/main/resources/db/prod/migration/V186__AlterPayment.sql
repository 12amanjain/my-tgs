alter table payment drop column if exists merchanttxnid;
alter table payment add column merchanttxnid varchar(255);

alter table payment_aud drop column if exists merchanttxnid;
alter table payment_aud add column merchanttxnid varchar(255);


alter table depositrequest_aud drop constraint IF EXISTS depositrequest_aud_pkey;
alter table depositrequest_aud add constraint depositrequest_aud_pkey primary key (id,rev);

