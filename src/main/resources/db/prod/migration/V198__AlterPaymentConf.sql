alter table paymentconfiguration rename to paymentconfigurationrule;
alter table paymentconfigurationrule rename column type to ruleType;
alter table paymentconfigurationrule alter column output type character varying(5000);
update paymentconfigurationrule set ruleType = 'PAYMENT_MEDIUM' where ruleType = 'M';
