CREATE INDEX order_booking on orders(bookingid);

CREATE INDEX order_user on orders(bookinguserid);

CREATE INDEX order_created on orders(createdon);

CREATE INDEX airorder_dept on airorderitem(departuretime);

alter table airorderitem add column additionalinfo jsonb;

CREATE INDEX json_additional ON airorderitem USING gin(additionalInfo);

CREATE INDEX json_traveller ON airorderitem USING gin(travellerInfo);
