alter table inventoryorders drop column if exists validon, add column validon date;

alter table inventoryorders drop column if exists pnr, add column pnr varchar(50);

alter table inventoryorders_aud drop column if exists validon, add column validon date;

alter table inventoryorders_aud drop column if exists pnr, add column pnr varchar(50);

alter table inventoryorders rename column bookingid to referenceid;

alter table inventoryorders_aud rename column bookingid to referenceid;
