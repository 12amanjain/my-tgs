drop table IF EXISTS paymentconfigurationrule_aud;
 
 CREATE TABLE paymentconfigurationrule_aud (
 id bigserial not null,
 rev bigint,
 revtype smallint,
 createdon timestamp DEFAULT now(),
 processedon timestamp DEFAULT now(),
 enabled boolean DEFAULT false,
 ruletype character varying(255),
 medium character varying(255),
 inclusionCriteria jsonb  ,
 exclusionCriteria jsonb  ,
 output character varying(5000),
 description character varying(500),
 priority float8,
 isdeleted boolean DEFAULT false,
 PRIMARY KEY (id,rev)
 );
 
 alter table paymentconfigurationrule_aud add constraint paymentconfig_foreign_idx foreign key (rev) references customrev;
