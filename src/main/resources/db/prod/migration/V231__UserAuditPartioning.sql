CREATE OR REPLACE FUNCTION create_partition_users_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
      partition_date TEXT;
      partition TEXT;
    BEGIN
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := 'users_aud' || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS (users_aud);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || 'users_aud' || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

CREATE TRIGGER users_aud_insert_trigger
BEFORE INSERT ON users_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_users_aud_insert();
