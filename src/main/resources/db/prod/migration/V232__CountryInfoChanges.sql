create table countryinfo (
        id  bigserial not null,
        code varchar(255),
        name varchar(255),
        isocode varchar(255),
        mobileCode varchar(255),
        primary key (id)
    );