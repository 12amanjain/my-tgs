alter table depositrequest drop column if exists depositbank, add column depositbank  character varying (255);
alter table depositrequest_aud drop column if exists depositbank, add column depositbank  character varying (255);
 