drop table IF EXISTS SupplierSessionManagement;


CREATE TABLE SupplierSession (
 id  bigserial not null,
 bookingId varchar(255),
 supplierSessionInfo jsonb ,
 sourceId int4,
 supplierId character varying(255) NOT NULL DEFAULT '',
 created_on timestamp DEFAULT now(),
 expiry_on timestamp,
 PRIMARY KEY (bookingId,sourceId,supplierId)
);


