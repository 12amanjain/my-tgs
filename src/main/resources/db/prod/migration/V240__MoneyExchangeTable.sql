CREATE TABLE moneyexchange (id  bigserial not null,
type varchar(255),
fromcurrency varchar(255),
tocurrency varchar(255),
markup varchar(255),
exchangerate double precision default 0,
processedon timestamp without time zone,
createdon timestamp without time zone);


CREATE TABLE moneyexchange_aud (id  bigserial not null,
type varchar(255),
rev bigint,
revtype smallint,
fromcurrency varchar(255),
tocurrency varchar(255),
markup varchar(255),
exchangerate double precision default 0,
processedon timestamp without time zone,
createdon timestamp without time zone);



CREATE OR REPLACE FUNCTION create_partition_moneyexchange_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
    tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
    tablename := 'moneyexchange_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
    
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
    DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
    ''')) INHERITS ('|| tablename || ');';
      
    END IF;
    EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
    RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS moneyexchange_aud_insert_trigger ON moneyexchange_aud;

CREATE TRIGGER moneyexchange_aud_insert_trigger
BEFORE INSERT ON moneyexchange_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_moneyexchange_aud_insert();