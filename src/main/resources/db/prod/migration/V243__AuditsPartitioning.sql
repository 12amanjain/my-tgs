CREATE OR REPLACE FUNCTION create_partition_amendment_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'amendment_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS amendment_aud_insert_trigger ON amendment_aud;

CREATE TRIGGER amendment_aud_insert_trigger
BEFORE INSERT ON amendment_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_amendment_aud_insert();



CREATE OR REPLACE FUNCTION create_partition_inventoryorders_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'inventoryorders_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
				
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS inventoryorders_aud_insert_trigger ON inventoryorders_aud;

CREATE TRIGGER inventoryorders_aud_insert_trigger
BEFORE INSERT ON inventoryorders_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_inventoryorders_aud_insert();


CREATE OR REPLACE FUNCTION create_partition_systemaudit_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'systemaudit';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
				
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS systemaudit_insert_trigger ON systemaudit;

CREATE TRIGGER systemaudit_insert_trigger
BEFORE INSERT ON systemaudit
FOR EACH ROW EXECUTE PROCEDURE create_partition_systemaudit_insert();



CREATE OR REPLACE FUNCTION create_partition_seatallocation_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'seatallocation_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS seatallocation_aud_insert_trigger ON seatallocation_aud;

CREATE TRIGGER seatallocation_aud_insert_trigger
BEFORE INSERT ON seatallocation_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_seatallocation_aud_insert();



CREATE OR REPLACE FUNCTION create_partition_depositrequest_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'depositrequest_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
      	EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';

	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS depositrequest_aud_insert_trigger ON depositrequest_aud;

CREATE TRIGGER depositrequest_aud_insert_trigger
BEFORE INSERT ON depositrequest_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_depositrequest_aud_insert();



CREATE OR REPLACE FUNCTION create_partition_creditbill_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'creditbill_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
      	EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';

	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS creditbill_aud_insert_trigger ON creditbill_aud;

CREATE TRIGGER creditbill_aud_insert_trigger
BEFORE INSERT ON creditbill_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_creditbill_aud_insert();


CREATE OR REPLACE FUNCTION create_partition_creditline_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'creditline_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
      	EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';

	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS creditline_aud_insert_trigger ON creditline_aud;

CREATE TRIGGER creditline_aud_insert_trigger
BEFORE INSERT ON creditline_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_creditline_aud_insert();