CREATE OR REPLACE FUNCTION create_partition_amendment_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'amendment';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS amendment_insert_trigger ON amendment;

CREATE TRIGGER amendment_insert_trigger
BEFORE INSERT ON amendment
FOR EACH ROW EXECUTE PROCEDURE create_partition_amendment_insert();