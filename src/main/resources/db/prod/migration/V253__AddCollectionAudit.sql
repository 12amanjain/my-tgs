DROP TABLE IF EXISTS collection_documents_aud;

CREATE TABLE  collection_documents_aud(
id int8 not null,
REV int8 not null,
REVTYPE int2,created_on timestamp,
expiry timestamp,
data text,
enabled boolean,
key varchar(255),
type varchar(500),
primary key (id, REV)
);

alter table collection_documents_aud
      add constraint collection_documents_aud_foreign_idx
      foreign key (REV)
      references customrev ;