
drop table if EXISTS incidence;
drop table if EXISTS incidence_aud;

create table incidence(
	id bigserial not null,
	incidenceid varchar(30) UNIQUE not null,
	name varchar(100) UNIQUE not null,
	incidencetype varchar(30) not null,
	incidenceData varchar(10000),
	createdon TIMESTAMP,
	processedon TIMESTAMP,
	primary key (id)
);

create table incidence_aud(
	id bigserial not null,
	rev int8 not null,
    revtype int2,
	incidenceid varchar(30) not null,
	name varchar(100) not null,
	incidencetype varchar(30) not null,
	incidenceData varchar(10000),
	createdon TIMESTAMP,
	processedon TIMESTAMP,
	primary key (id, rev)

);

drop index if exists idx_incidence_id;
create index idx_incidence_id on incidence(incidenceid);