DROP INDEX if exists idx_bill_userId;
CREATE INDEX idx_bill_userId on creditbill(userid);


ALTER TABLE incidence DROP CONSTRAINT if exists incidence_name_uniq;
ALTER TABLE incidence ADD CONSTRAINT incidence_name_uniq UNIQUE (name);


ALTER TABLE payment DROP CONSTRAINT if exists payment_payuserId_uniq;
delete from payment where payuserid is NULL;
delete from payment_aud where payuserid is NULL;
ALTER TABLE payment alter column payuserid SET NOT NULL;
