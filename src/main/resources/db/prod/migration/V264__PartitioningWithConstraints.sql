CREATE OR REPLACE FUNCTION create_partition_users_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'users';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_uniq_idx UNIQUE (email,mobile,role);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';

      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS users_insert_trigger ON users;

CREATE TRIGGER users_insert_trigger
BEFORE INSERT ON users
FOR EACH ROW EXECUTE PROCEDURE create_partition_users_insert();



CREATE OR REPLACE FUNCTION create_partition_orders_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'orders';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
		EXECUTE 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_uniq_idx UNIQUE (bookingid);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
				
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS orders_insert_trigger ON orders;

CREATE TRIGGER orders_insert_trigger
BEFORE INSERT ON orders
FOR EACH ROW EXECUTE PROCEDURE create_partition_orders_insert();




CREATE OR REPLACE FUNCTION create_partition_airorderitem_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'airorderitem';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
				
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS airorderitem_insert_trigger ON airorderitem;

CREATE TRIGGER airorderitem_insert_trigger
BEFORE INSERT ON airorderitem
FOR EACH ROW EXECUTE PROCEDURE create_partition_airorderitem_insert();



CREATE OR REPLACE FUNCTION create_partition_payment_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'payment';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      	EXECUTE 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_uniq_idx UNIQUE (merchanttxnid,status,paymentmedium,amount);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS payment_insert_trigger ON payment;

CREATE TRIGGER payment_insert_trigger
BEFORE INSERT ON payment
FOR EACH ROW EXECUTE PROCEDURE create_partition_payment_insert();


CREATE OR REPLACE FUNCTION create_partition_amendment_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'amendment';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS amendment_insert_trigger ON amendment;

CREATE TRIGGER amendment_insert_trigger
BEFORE INSERT ON amendment
FOR EACH ROW EXECUTE PROCEDURE create_partition_amendment_insert();


CREATE OR REPLACE FUNCTION create_partition_systemaudit_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'systemaudit';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
      
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
		EXECUTE 'ALTER TABLE ' || partition || ' ADD PRIMARY KEY (id);';
				
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS systemaudit_insert_trigger ON systemaudit;

CREATE TRIGGER systemaudit_insert_trigger
BEFORE INSERT ON systemaudit
FOR EACH ROW EXECUTE PROCEDURE create_partition_systemaudit_insert();
