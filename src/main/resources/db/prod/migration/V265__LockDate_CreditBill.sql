alter table creditbill
drop COLUMN if EXISTS originalpayduedate,
drop COLUMN if EXISTS lockdate,
drop COLUMN if EXISTS extensioncount,
add column lockdate TIMESTAMP,
add column extensioncount int default 0;

alter table creditbill_aud
drop COLUMN if EXISTS originalpayduedate,
drop COLUMN if EXISTS lockdate,
drop COLUMN if EXISTS extensioncount,
add column lockdate TIMESTAMP,
add column extensioncount int default 0;