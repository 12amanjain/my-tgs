drop table if EXISTS hotelsupplierinfo;

CREATE TABLE hotelsupplierinfo (
	id bigserial NOT NULL,
	credentialinfo jsonb NULL,
	createdon timestamp NULL DEFAULT now(),
	enabled bool NULL DEFAULT true,
	"name" varchar(255) NOT NULL,
	sourceid int4 NOT NULL,
	isdeleted bool NULL DEFAULT false,
	processedon timestamp NULL,
	CONSTRAINT hotelsupplierinfo_pkey PRIMARY KEY (id, name),
	CONSTRAINT uniq_hotelsupplierinfo_name UNIQUE (name)
);

