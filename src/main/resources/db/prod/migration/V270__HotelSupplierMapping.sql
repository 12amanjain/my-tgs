CREATE TABLE HotelInfoMapping(
id bigserial PRIMARY KEY , 
hotelId bigserial NOT NULL , 
supplierHotelId varchar(50) NOT NULL ,
supplierName varchar(50) NOT NULL);
