drop table if exists hotelorderitem;
drop table if exists hotelorderitem_AUD;
create table hotelorderitem (
       id int8 not null,
        additionalInfo jsonb,
        amount float8,
        bookingId varchar(255),
        checkInDate date,
        checkOutDate date,
        createdOn timestamp,
        hotel varchar(255),
        markup float8,
        roomInfo jsonb,
        roomName varchar(255),
        status varchar(255),
        supplierId varchar(255),
        travellerInfo jsonb,
        primary key (id)
    );

create table hotelorderitem_AUD (
       id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        additionalInfo jsonb,
        amount float8,
        bookingId varchar(255),
        checkInDate date,
        checkOutDate date,
        createdOn timestamp,
        hotel varchar(255),
        markup float8,
        roomInfo jsonb,
        roomName varchar(255),
        status varchar(255),
        supplierId varchar(255),
        travellerInfo jsonb,
        primary key (id, REV)
    );   
