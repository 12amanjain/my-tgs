drop table if exists supplierrule_AUD;
drop table if exists fareruleinfo_AUD;
create table supplierrule_AUD (
       id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        createdon timestamp,
        enabled boolean,
        supplierAdditionalInfo jsonb,
        inclusionCriteria jsonb,
        exclusionCriteria jsonb,
        exitOnMatch boolean,
        priority float8,
        sourceId int4,
        supplierId varchar(255),
        description varchar(255),
        isdeleted boolean,
        airtype varchar(255),
        searchType varchar(255),
        processedon timestamp,
        primary key (id, REV)
    );  
    
CREATE TABLE fareRuleInfo_AUD (
 		id int8 not null,
        REV int8 not null,
        REVTYPE int2,
 		createdon timestamp DEFAULT now(),
 		airType CHARACTER VARYING(255),
 		airline CHARACTER VARYING(255),
 		priority float8,
 		enabled boolean DEFAULT false,
 		inclusionCriteria jsonb,
 		exclusionCriteria jsonb,
 		fareRuleInformation jsonb,
 		description varchar(255),
        isdeleted boolean,
        processedon timestamp,
        primary key (id, REV)
 );
 
 alter table supplierrule_AUD add constraint supprule_foreign_idx foreign key (REV) references customrev;

 alter table fareRuleInfo_AUD add constraint farerule_foreign_idx foreign key (REV) references customrev;

