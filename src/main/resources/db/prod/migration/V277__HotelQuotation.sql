DROP TABLE IF EXISTS hotelquotation;

CREATE TABLE hotelquotation(
id bigserial,
name text NOT NULL,
userId text NOT NULL,
createdOn timestamp NOT NULL DEFAULT now(),
quotationInfo jsonb NOT null,
CONSTRAINT hotelquotation_pkey PRIMARY KEY (id),
CONSTRAINT uniq_hotelquotation_name_userId UNIQUE (name, userId)
)

