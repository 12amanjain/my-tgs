drop table if exists HotelInfo;
drop table if exists hotelsuppliermapping;

CREATE TABLE hotelsuppliermapping(
id bigserial PRIMARY KEY , 
hotelId bigserial NOT NULL , 
supplierHotelId text NOT NULL ,
supplierName varchar(50) NOT NULL,
createdOn timestamp,
UNIQUE(hotelId , supplierName));
