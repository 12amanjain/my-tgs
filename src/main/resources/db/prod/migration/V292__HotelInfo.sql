drop table if EXISTS hotelinfo;

CREATE TABLE hotelinfo(
id bigserial PRIMARY KEY,
name text UNIQUE NOT NULL,
thumbnails varchar[] NULL,
description text NULL,
longDescription text NULL,
rating text NULL,
geolocation jsonb NULL,
address jsonb NULL,
facility varchar[] NULL,
phone text NULL,
website text NULL,
type text null,
createdOn timestamp
);
