DROP TABLE if EXISTS hotelcityinfo;

CREATE TABLE hotelcityinfo(
id bigserial PRIMARY KEY,
cityName text NOT NULL , 
countryId text,
countryName text NOT NULL,
createdOn timestamp
);
