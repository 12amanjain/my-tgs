drop table if exists cityinfomapping;
drop table if exists hotelcityinfomapping;

CREATE TABLE hotelcityinfomapping(
id bigserial PRIMARY KEY,
cityId bigserial NOT NULL, 
supplierName text NOT NULL, 
supplierCity text NOT NULL,
supplierCountry text,
createdOn timestamp,
UNIQUE(cityId , supplierName));
