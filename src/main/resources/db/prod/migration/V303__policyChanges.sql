alter table creditbill drop column if exists processedon;
alter table creditbill add column processedon timestamp without time zone;
alter table creditbill_aud drop column if exists processedon;
alter table creditbill_aud add column processedon timestamp without time zone;



