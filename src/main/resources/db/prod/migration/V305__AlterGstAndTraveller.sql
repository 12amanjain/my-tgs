alter table gstdata drop column if exists createdon , add column createdon timestamp DEFAULT now();
alter table gstdata drop column if exists enabled , add column enabled boolean DEFAULT true;
alter table travellerinfo drop column if exists createdon, add column createdon timestamp DEFAULT now();
