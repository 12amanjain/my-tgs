alter sequence if exists atlas_invoice_id_seq rename to atlas_air_invoice_id_seq;
create sequence if not exists atlas_air_invoice_id_seq increment by 1 minvalue 1 maxvalue 9999999 start with 1;

create sequence if not exists atlas_hotel_invoice_id_seq increment by 1 minvalue 1 maxvalue 9999999 start with 1;