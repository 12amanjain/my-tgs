delete from airportinfo where code='VID';
delete from airportinfo where code='IHX';
delete from airportinfo where code='PUT';
delete from airportinfo where code='DHM';
delete from airportinfo where code='XLL';
delete from airportinfo where code='XKU';
delete from airportinfo where code='PGH';
delete from airportinfo where code='REW';
delete from airportinfo where code='UJA';
delete from airportinfo where code='MUI';
delete from airportinfo where code='XAM';
delete from airportinfo where code='AIP';
delete from airportinfo where code='KQH';
delete from airportinfo where code='PYG';


insert into airportinfo(city,code,country,name,countrycode,citycode) values('Bellary','VID','India','Bellary','IN','VID');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Kailashahar','IHX','India','Kailashahar','IN','IHX');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Puttaprathe','PUT','India','Puttaprathe','IN','PUT');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Dharamsala','DHM','India','Dharamsala','IN','DHM');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Alleppey','XLL','India','Alleppey','IN','XLL');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Kumarakom','XKU','India','Kumarakom','IN','XKU');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Pantnagar','PGH','India','Pantnagar','IN','PGH');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Rewa','REW','India','Rewa','IN','REW');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Muirpur Airport','MUI','India','Muirpur Airport','IN','MUI');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Amreli','XAM','India','Amreli','IN','XAM');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Adampur','AIP','India','Adampur','IN','AIP');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Kishangarh','KQH','India','Kishangarh','IN','KQH');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Pakyong','PYG','India','Pakyong','IN','PYG');
insert into airportinfo(city,code,country,name,countrycode,citycode) values('Ujjain','UJA','India','Ujjain','IN','UJA');


delete from airlineinfo  where code = 'IC';


