delete from airlineinfo where code='FA';
delete from airlineinfo where code='5H';
delete from airlineinfo where code='7T';
delete from airlineinfo where code='E2';
delete from airlineinfo where code='F2';
delete from airlineinfo where code='O2';
delete from airlineinfo where code='GH';


insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('FA','Fly Safair',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('7T','Tobruk Air',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('E2','EuroWings',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('5H','Fly540',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('F2','SafariLink',true,false,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('O2','Linear Air',false,true,'');
insert into airlineinfo (code,name,islcc,istkrequired,accountingcode) values ('GH','Globus LLC',false,true,'');


delete from airportinfo where code='IRT';
delete from airportinfo where code='IBT';
delete from airportinfo where code='IMR';
delete from airportinfo where code='BHR';


insert into airportinfo(name,code,citycode,country,city,countrycode) values ('Roma Tiburtina Rail Station','IRT','IRT','Italy','Rome','IT');
insert into airportinfo(name,code,citycode,country,city,countrycode) values ('Bologona Central Rail Station','IBT','IBT','Italy','Bologona','IT');
insert into airportinfo(name,code,citycode,country,city,countrycode) values ('Milano Rogoredo Railway Station','IMR','IMR','Italy','Milan','IT');
insert into airportinfo(name,code,citycode,country,city,countrycode) values ('Bharatpur Airport','BHR','BHR','Nepal','Bharatpur','NP');

update airlineinfo set name ='Yeti Airlines' where code='YT';


