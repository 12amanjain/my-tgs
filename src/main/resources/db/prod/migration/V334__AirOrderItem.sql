UPDATE airorderitem SET travellerinfo = to_json(t.newValue)
FROM (WITH temp AS (SELECT id, jsonb_array_elements(travellerinfo) b FROM airorderitem)
    SELECT id, array_agg(b - 'ssrBaggageInfo' || jsonb_build_object('sbi', b -> 'ssrBaggageInfo')) AS newValue
    FROM temp GROUP BY id) AS t
WHERE airorderitem.id = t.id;

UPDATE airorderitem SET travellerinfo = to_json(t.newValue)
FROM (WITH temp AS (SELECT id, jsonb_array_elements(travellerinfo) b FROM airorderitem)
    SELECT id, array_agg(b - 'ssrMealInfo' || jsonb_build_object('smi', b -> 'ssrMealInfo')) AS newValue
    FROM temp GROUP BY id) AS t
WHERE airorderitem.id = t.id;

UPDATE airorderitem SET travellerinfo = to_json(t.newValue)
FROM (WITH temp AS (SELECT id, jsonb_array_elements(travellerinfo) b FROM airorderitem)
    SELECT id, array_agg(b - 'ssrSeatInfo' || jsonb_build_object('ssi', b -> 'ssrSeatInfo')) AS newValue
    FROM temp GROUP BY id) AS t
WHERE airorderitem.id = t.id;

UPDATE airorderitem SET travellerinfo = to_json(t.newValue)
FROM (WITH temp AS (SELECT id, jsonb_array_elements(travellerinfo) b FROM airorderitem)
    SELECT id, array_agg(b - 'extraServices' || jsonb_build_object('es', b -> 'extraServices')) AS newValue
    FROM temp GROUP BY id) AS t
WHERE airorderitem.id = t.id;


