alter table moneyexchange drop constraint if exists moneyexchange_uniq_idx;

alter table moneyexchange add constraint moneyexchange_uniq_idx unique(type, fromcurrency, tocurrency);

alter table moneyexchange drop constraint if exists moneyexchange_pkey;

alter table moneyexchange add constraint moneyexchange_pkey primary key (id);