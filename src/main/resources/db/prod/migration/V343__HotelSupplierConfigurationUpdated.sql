delete from hotelconfiguratorrule where ruletype = 'SUPPLIERCONFIG';

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJclertrip","TJagoda","TJMiki","TJDesiya","TJDOTW","TJHotelbeds","TJTAMTOUR","TJLOCAL","TJExpedia"]}', 15, false, '{"countryNames": ["India"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJ24x7","TJTotalstay","TJclertrip","TJagoda","TJMiki","TJHotelbeds","TJExpedia","TJTravco","TJRTS","TJRESTEL","TJW2M"]}', 15, false, '{"countryNames": ["Albania","Austria","Belgium","Bosnia and Herzegovina","Croatia","Czech Republic","Denmark","Estonia","Finland","France","Germany","Greece","Greenland","Hungary","Iceland","Ireland","Isle of Man","Italy","Latvia","Liechtenstein","Lithuania","Luxembourg","Macedonia","Malta","Monaco","Montenegro","Montserrat","Netherlands","Norway","Poland","Portugal","Slovakia","Slovenia","Spain","Sweden","Switzerland","Ukraine","United Kingdom","Vatican City"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJ24x7","TJDOTW","TJTotalstay","TJclertrip","TJagoda","TJHOTELSPRO","TJMiki","TJHotelbeds","TJExpedia","TJTravco","TJRTS","TJRESTEL","TJQUANTUM","TJW2M"]}', 15, false, '{"countryNames": ["Hong Kong","Indonesia","Macau","Malaysia","Singapore","Thailand"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJ24x7","TJDOTW","TJTotalstay","TJclertrip","TJagoda","TJMiki","TJHotelbeds","TJExpedia","TJW2M","TJTravco"]}', 15, false, '{"countryNames": ["Bahrain"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJ24x7","TJDOTW","TJTotalstay","TJclertrip","TJagoda","TJMiki","TJHotelbeds","TJExpedia","TJW2M","TJTravco","TJHOTELSPRO","TJRTS","TJRESTEL"]}', 15, false, '{"countryNames": ["Argentina","Bahamas","Brazil","Canada","Chile","Mexico","Peru","United States","United States Virgin Islands"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJ24x7","TJDOTW","TJDARINA","TJTotalstay","TJclertrip","TJagoda","TJMiki","TJHotelbeds","TJExpedia","TJW2M","TJTravco","TJHOTELSPRO","TJRTS","TJRESTEL"]}', 15, false, '{"countryNames": ["United Arab Emirates"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJ24x7","TJDOTW","TJDARINA","TJTotalstay","TJclertrip","TJagoda","TJHotelbeds","TJExpedia","TJW2M","TJTravco","TJHOTELSPRO","TJRTS","TJRESTEL","TJQUANTUM","TJDesiya"]}', 10, false, '{}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);