CREATE OR REPLACE FUNCTION create_partition_note_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'note';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
		EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';
	
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS note_insert_trigger ON note;

CREATE TRIGGER note_insert_trigger
BEFORE INSERT ON note
FOR EACH ROW EXECUTE PROCEDURE create_partition_note_insert();


CREATE OR REPLACE FUNCTION create_partition_hotelorderitem_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'hotelorderitem_aud';
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
      	EXECUTE 'CREATE INDEX ON ' || partition || '(createdon);';

	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS hotelorderitem_aud_insert_trigger ON hotelorderitem_aud;

CREATE TRIGGER hotelorderitem_aud_insert_trigger
BEFORE INSERT ON hotelorderitem_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_hotelorderitem_aud_insert();



CREATE OR REPLACE FUNCTION create_partition_collection_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
    BEGIN
	  tablename := 'collection_documents_aud';
      partition_date := to_char(NEW.created_on,'YYYY_MM');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (created_on >= ''' || 
		DATE_TRUNC('month',NEW.created_on) ||'''AND created_on< ''' || DATE_TRUNC('month',NEW.created_on)+interval '1 month' || 
		''')) INHERITS ('|| tablename || ');';
		
      	EXECUTE 'CREATE INDEX ON ' || partition || '(created_on);';

	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS collection_aud_insert_trigger ON collection_documents_aud;

CREATE TRIGGER collection_aud_insert_trigger
BEFORE INSERT ON collection_documents_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_collection_aud_insert();

