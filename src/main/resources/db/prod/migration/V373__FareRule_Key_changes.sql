update fareruleinfo set fareruleinformation = fareruleinformation - 'refundableType' || jsonb_build_object('rT',fareruleinformation -> 'refundableType') where fareruleinformation ? 'refundableType';

update fareruleinfo set fareruleinformation = fareruleinformation - 'isMealIndicator' || jsonb_build_object('isML',fareruleinformation -> 'isMealIndicator') where fareruleinformation ? 'isMealIndicator';

update fareruleinfo set fareruleinformation = fareruleinformation - 'isHandBaggageIndicator' || jsonb_build_object('isHB',fareruleinformation -> 'isHandBaggageIndicator') where fareruleinformation ? 'isHandBaggageIndicator';

update fareruleinfo set fareruleinformation = fareruleinformation - 'miscInfo' || jsonb_build_object('mi',fareruleinformation -> 'miscInfo') where fareruleinformation ? 'miscInfo';

update fareruleinfo set fareruleinformation = fareruleinformation - 'dateChangePolicy' || jsonb_build_object('dcp',fareruleinformation -> 'dateChangePolicy') where fareruleinformation ? 'dateChangePolicy';

update fareruleinfo set fareruleinformation = fareruleinformation - 'cancellationPolicy' || jsonb_build_object('cp',fareruleinformation -> 'cancellationPolicy') where fareruleinformation ? 'cancellationPolicy';

update fareruleinfo set fareruleinformation = fareruleinformation - 'fareRuleInfo' || jsonb_build_object('fr',fareruleinformation -> 'fareRuleInfo') where fareruleinformation ? 'fareRuleInfo';

update fareruleinfo set fareruleinformation = fareruleinformation - 'checkedInBaggage' || jsonb_build_object('cB',fareruleinformation -> 'checkedInBaggage') where fareruleinformation ? 'checkedInBaggage';

update fareruleinfo set fareruleinformation = fareruleinformation - 'handBaggage' || jsonb_build_object('hB',fareruleinformation -> 'handBaggage') where fareruleinformation ? 'handBaggage';