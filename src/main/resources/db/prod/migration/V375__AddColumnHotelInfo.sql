DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE hotelinfo ADD COLUMN notes text;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column notes already exists in hotelinfo.';
        END;
    END;
$$