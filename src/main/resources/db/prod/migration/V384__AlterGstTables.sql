drop table IF EXISTS billing_entity ;
drop index IF EXISTS billing_entity_idx;

alter table gstinfo RENAME TO order_billing;
alter table gstdata RENAME TO billing_entity;

alter table order_billing drop column if exists info;
alter table order_billing add column info jsonb;

alter table billing_entity drop column if exists info;
alter table billing_entity drop column if exists pincode;
alter table billing_entity drop column if exists state;
alter table billing_entity drop column if exists cityname;

alter table billing_entity add column pincode varchar(50);
alter table billing_entity add column state varchar(100);
alter table billing_entity add column cityname varchar(100);
alter table billing_entity add column info jsonb;