DO $$ 
    BEGIN
        BEGIN
            ALTER TABLE hotelcityinfo ADD COLUMN iatacode text;
        EXCEPTION
            WHEN duplicate_column THEN RAISE NOTICE 'column iatacode already exists in hotelcityinfo';
        END;
    END;
$$