Update orders set status = 'C' where bookingid in (
select ab.bookingid from
(select bookingid,sum(cancelledCount) as cancelledCount from airorderitem a , lateral (select count(*) as cancelledCount
from jsonb_array_elements(travellerinfo) as d(elem) where d.elem->>'st'='CAN' group by d.elem,bookingid) d
group by bookingid order by bookingid desc)ab
LEFT OUTER JOIN
(SELECT bookingid,SUM(jsonb_array_length(travellerinfo)) AS totalCount from airorderitem GROUP by bookingid) b
on ab.bookingid = b.bookingid where b.totalCount=ab.cancelledCount
)
and status = 'S';


Update airorderitem set status = 'C' where bookingid in (select bookingid from orders where status = 'C');
