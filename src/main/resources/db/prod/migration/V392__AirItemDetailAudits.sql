create table airitemdetail_aud (
        rev bigint,
		revtype smallint,
        id BIGSERIAL PRIMARY KEY,
		bookingid VARCHAR(100) NOT NULL,
		info jsonb,
		createdon timestamp
);

CREATE OR REPLACE FUNCTION create_partition_airitemdetails_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
      partition_date TEXT;
      partition TEXT;
    BEGIN
      partition_date := to_char(NEW.createdon,'YYYY_MM');
      partition := 'airitemdetail_aud' || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		
        EXECUTE 'CREATE TABLE ' || partition || ' (check (createdon >= ''' || 
		DATE_TRUNC('month',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('month',NEW.createdon)+interval '1 month' || 
		''')) INHERITS (airitemdetail_aud);';
      
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || 'airitemdetail_aud' || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;

DROP TRIGGER IF EXISTS airitemdetails_aud_insert_trigger ON airitemdetail_aud;

CREATE TRIGGER airitemdetails_aud_insert_trigger
BEFORE INSERT ON airitemdetail_aud
FOR EACH ROW EXECUTE PROCEDURE create_partition_airitemdetails_aud_insert();