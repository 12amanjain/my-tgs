Drop table IF exists corptrip;

CREATE TABLE corptrip(
        id serial not null,
        modificationtype varchar(100),
        userid varchar(100),
        tripid varchar(100),
        bookingid varchar(100),
        status varchar(50),
        ispushed varchar(50),
		tripSearchQuery jsonb,
		createdon timestamp DEFAULT now(),
		processedon timestamp DEFAULT now(),
        primary key (id)
);

CREATE INDEX on corptrip(tripid);

ALTER TABLE corptrip add constraint corptrip_uniq_idx unique (tripid);
