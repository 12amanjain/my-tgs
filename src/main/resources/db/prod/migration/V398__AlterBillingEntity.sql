alter table billing_entity drop column if exists billingcompanyname;
alter table billing_entity add column billingcompanyname varchar(255) default '';
alter table billing_entity drop column if exists isdeleted;
alter table billing_entity add column isdeleted boolean default false;
alter table order_billing drop column if exists billingcompanyname;
alter table order_billing add column billingcompanyname varchar(255) default '';
alter table billing_entity drop constraint if exists gstdata_pkey;
alter table billing_entity drop constraint if exists billing_entity_pkey;
alter table billing_entity add primary key (userid, gstnumber, billingcompanyname);