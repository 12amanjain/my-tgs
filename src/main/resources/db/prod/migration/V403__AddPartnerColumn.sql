alter table users drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table depositrequest drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table orders drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table amendment drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table payment drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table nonrevolvingcredit drop column if exists partnerid, add column partnerid varchar(50) default 0;



alter table users_aud drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table depositrequest_aud drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table orders_aud drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table amendment_aud drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table payment_aud drop column if exists partnerid, add column partnerid varchar(50) default 0;
alter table nonrevolvingcredit_aud drop column if exists partnerid, add column partnerid varchar(50) default 0;

alter table users drop constraint if exists users_uniq_idx;
alter table users add constraint users_uniq_idx unique (partnerid,email);




