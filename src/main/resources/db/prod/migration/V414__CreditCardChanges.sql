alter table creditcardinfo drop column if exists billingentityid;

alter table creditcardinfo add column billingentityid integer;

alter table creditcardinfo drop column if exists userid;

alter table creditcardinfo add column userid varchar(255);

alter table creditcardinfo alter column priority set default 0;

alter table creditcardinfo_aud drop column if exists billingentityid;

alter table creditcardinfo_aud add column billingentityid integer;

alter table creditcardinfo_aud drop column if exists userid;

alter table creditcardinfo_aud add column userid varchar(255);