ALTER TABLE hotelsuppliermapping DROP COLUMN IF EXISTS sourceName;
ALTER TABLE hotelsuppliermapping RENAME COLUMN supplierName TO sourceName;

ALTER TABLE hotelsuppliermapping DROP COLUMN IF EXISTS supplierName;
ALTER TABLE hotelsuppliermapping ADD COLUMN supplierName varchar(50);

UPDATE hotelsuppliermapping set supplierName = sourceName;