update supplierrule set inclusioncriteria='{"airlineList":["G8"],"isDomestic":true}' where sourceid=1;
update supplierrule set inclusioncriteria='{"airlineList":["FZ"],"isDomestic":false}' where sourceid=3;
update supplierrule set inclusioncriteria='{"airlineList":["SG"]}' where sourceid=4;
update supplierrule set inclusioncriteria='{"airlineList":["6E"]}' where sourceid=5;
update supplierrule set inclusioncriteria='{"airlineList":["IX"]}' where sourceid=8;
update supplierrule set inclusioncriteria='{"airlineList":["G9"],"isDomestic":false}' where sourceid=7;
update supplierrule set inclusioncriteria='{"airlineList":["TR","TZ"],"isDomestic":false}' where sourceid=6;
update supplierrule set exclusioncriteria='{"airlineList": ["SG","6E","G8"]}' where sourceid=2;
