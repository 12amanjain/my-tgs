CREATE TABLE CommissionRule (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
 enabled boolean DEFAULT false,
 product varchar(255),
 code varchar(255),
 priority float8,
 inclusionCriteria varchar(5000),
 exclusionCriteria varchar(5000) ,
 commercialCriteria varchar(5000),
 PRIMARY KEY (id)
);

CREATE TABLE CommissionRule_aud (
 id  bigserial not null,
 rev bigint,
 revtype smallint,
 createdon timestamp DEFAULT now(),
 enabled boolean DEFAULT false,
 product varchar(255),
 code varchar(255),
 priority float8,
 inclusionCriteria varchar(5000),
 exclusionCriteria varchar(5000) ,
 commercialCriteria varchar(5000),
 PRIMARY KEY (id)
);

CREATE TABLE CommissionPlanMapper (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
  enabled boolean DEFAULT false,
  commplanid int4,
  commissionid int4,
   PRIMARY KEY (id,commissionid)
);



CREATE TABLE CommissionPlan (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
  enabled boolean DEFAULT false,
  product varchar(5000),
  commPlanId int4,
  description varchar(255),
   PRIMARY KEY (id,commPlanId)
);



alter table users add column commissionPlan jsonb ;

alter table users_aud add column commissionPlan jsonb ;


alter table users alter column commissionPlan set default '{}'::jsonb;

alter table users_aud alter column commissionPlan set default '{}'::jsonb;
