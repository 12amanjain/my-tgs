
drop table if exists commissionplanmapper;
drop table if exists commissionplan;

CREATE TABLE CommissionPlanMapper (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
  commplanid int4,
  commissionid int4,
   PRIMARY KEY (commissionid,commplanid)
);


CREATE TABLE CommissionPlan (
 id  bigserial not null,
 createdon timestamp DEFAULT now(),
  product varchar(5000),
  commPlanId int4,
  description varchar(255),
   PRIMARY KEY (commPlanId)
);


