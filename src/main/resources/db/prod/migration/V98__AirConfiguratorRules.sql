
delete from airconfiguratorrule;

alter table airconfiguratorrule drop column inclusioncriteria;
alter table airconfiguratorrule drop column exclusioncriteria;
alter table airconfiguratorrule add column inclusioncriteria jsonb;
alter table airconfiguratorrule add column exclusioncriteria jsonb;

alter table airconfiguratorrule rename created_on to createdon;

