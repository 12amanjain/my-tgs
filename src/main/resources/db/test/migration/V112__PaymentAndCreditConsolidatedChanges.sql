drop table IF EXISTS creditline;
drop table IF EXISTS creditline_aud;
drop table IF EXISTS creditbill;
drop table IF EXISTS creditbill_aud;

CREATE TABLE creditline (
	id bigserial NOT NULL,
	creditNumber character varying(30) NOT NULL,
	userId character varying(255) NOT NULL,
	creditLimit BIGINT NOT NULL,
	products character varying(10)[],
	lastBilled timestamp without time zone,
	billCycleDays SMALLINT default 30,
	paymentDueDays SMALLINT default 20,
	outstandingBalance BIGINT default 0,
	isDisabled BOOLEAN Default false,
	disableReason character varying(10),
	createdon timestamp without time zone,
	primary key (id)
);

CREATE TABLE creditline_aud (
	id bigserial NOT NULL,
	rev bigint NOT NULL,
	revtype smallint,
	creditNumber character varying(30) NOT NULL,
	userId character varying(255) NOT NULL,
	creditLimit BIGINT NOT NULL,
	products character varying(10)[],
	lastBilled timestamp without time zone,
	billCycleDays SMALLINT default 30,
	paymentDueDays SMALLINT default 20,
	outstandingBalance BIGINT default 0,
	isDisabled BOOLEAN Default false,
	disableReason character varying(10),
	createdon timestamp without time zone,
	primary key (id, rev)
);

CREATE TABLE creditbill (
	id bigserial NOT NULL,
	billNumber character varying(30) NOT NULL,
	creditId BIGINT NOT NULL,
	userId character varying(255) NOT NULL,
	chargedPaymentIds BIGINT[] NOT NULL,
	settlementPaymentIds BIGINT[] NOT NULL,
	billAmount BIGINT NOT NULL,
	settledAmount BIGINT default 0,
	isSettled BOOLEAN Default false NOT NULL,
	createdon timestamp without time zone,
	paymentduedate timestamp without time zone,
	primary key (id)
);

CREATE TABLE creditbill_aud (
	id bigserial NOT NULL,
	rev bigint NOT NULL,
	revtype smallint,
	billNumber character varying(30) NOT NULL,
	creditId BIGINT NOT NULL,
	userId character varying(255) NOT NULL,
	chargedPaymentIds BIGINT[] NOT NULL,
	settlementPaymentIds BIGINT[] NOT NULL,
	billAmount BIGINT NOT NULL,
	settledAmount BIGINT default 0,
	isSettled BOOLEAN Default false NOT NULL,
	createdon timestamp without time zone,
	paymentduedate timestamp without time zone,
	primary key (id, rev)
);

Alter Table payment DROP COLUMN if exists additionalinfo, ADD COLUMN additionalinfo jsonb DEFAULT '{}'::jsonb, DROP COLUMN if exists paymentRefId, add column paymentRefId character varying(30), DROP COLUMN if exists walletId, add column walletId BIGINT;

Alter Table payment_aud DROP COLUMN if exists additionalinfo, ADD COLUMN additionalinfo jsonb DEFAULT '{}'::jsonb, DROP COLUMN if exists paymentRefId, add column paymentRefId character varying(30), DROP COLUMN if exists walletId, add column walletId BIGINT;
