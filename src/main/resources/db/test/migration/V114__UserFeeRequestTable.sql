DROP TABLE IF EXISTS UserFeeRequest;

create table UserFeeRequest(

id bigserial not null,
userid  varchar(255),
createdon timestamp,
product varchar(255),
feetype varchar(255),	
enabled boolean,
inclusionCriteria character varying(5000),
userFee jsonb,
primary key (id)

);


alter table users drop column markup;
alter table users_aud drop column markup;