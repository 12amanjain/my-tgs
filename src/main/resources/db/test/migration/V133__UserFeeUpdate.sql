DROP TABLE IF EXISTS UserFeeRequest;

create table UserFee(

id bigserial not null,
userid  varchar(255) not null,
createdon timestamp default now(),
product varchar(255) not null,
feetype varchar(255) not null,
enabled boolean default true,
inclusionCriteria character varying(5000),
userFee jsonb not null,
primary key (id)
);
