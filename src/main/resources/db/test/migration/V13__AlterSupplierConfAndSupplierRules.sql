alter table supplierconfigurationinfo alter column supplierid type character varying(255);
alter table supplierconfigurationinfo alter column  configurationdata type character varying(5000);
alter table supplierconfigurationinfo add column name  character varying(200);


create table supplier_rules (
        id  bigserial not null,
        created_on timestamp,
        enabled boolean,
        ruleInfo jsonb,
        sourceId int4,
        supplierId varchar(255),
        primary key (id)
);
