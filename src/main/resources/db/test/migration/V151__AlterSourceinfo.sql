alter table supplierinfo drop column supplierid;
alter table supplierinfo_aud drop column supplierid;
alter table supplierinfo alter column createdon set default now();
alter table supplierinfo alter column enabled set default true;
alter table supplierinfo alter column sourceid SET NOT NULL;
ALTER TABLE supplierinfo DROP CONSTRAINT supplierinfo_pkey;
ALTER TABLE supplierinfo ADD CONSTRAINT supplierinfo_pkey  PRIMARY KEY(id,name);
ALTER TABLE supplierinfo_aud DROP CONSTRAINT supplierinfo_aud_pkey;
ALTER TABLE supplierinfo_aud ADD CONSTRAINT supplierinfo_aud_pkey PRIMARY KEY(id,name,rev);

alter table supplierrule alter column createdon set default now();
alter table supplierrule alter column enabled set default true;
alter table supplierrule alter column sourceid SET NOT NULL;
alter table supplierrule alter column supplierid SET NOT NULL;