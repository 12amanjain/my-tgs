
alter table aircommissionrule drop column if exists processedon, add column processedon timestamp;
alter table airconfiguratorrule drop column if exists processedon, add column processedon timestamp;
alter table airconfiguratorrule_aud drop column if exists processedon, add column processedon timestamp;
alter table commissionplan drop column if exists processedon, add column processedon timestamp;
alter table commissionrule drop column if exists processedon, add column processedon timestamp;
alter table commissionrule_aud drop column if exists processedon, add column processedon timestamp;
alter table commissionplanmapper drop column if exists processedon, add column processedon timestamp;
alter table configuratorrule drop column if exists processedon, add column processedon timestamp;
alter table configuratorrule_aud drop column if exists processedon, add column processedon timestamp;
alter table supplierinfo drop column if exists processedon, add column processedon timestamp;
alter table supplierinfo_aud drop column if exists processedon, add column processedon timestamp;
alter table supplierrule drop column if exists processedon, add column processedon timestamp;
alter table paymentconfiguration drop column if exists processedon, add column processedon timestamp;
alter table payment drop column if exists processedon, add column processedon timestamp;
alter table payment_aud drop column if exists processedon, add column processedon timestamp;


update aircommissionrule set processedon = createdon;
update airconfiguratorrule set processedon = createdon ;
update airconfiguratorrule_aud set  processedon = createdon ;
update commissionplan set processedon= createdon  ;
update commissionrule set processedon = createdon ;
update commissionrule_aud set processedon = createdon ;
update commissionplanmapper set processedon = createdon ;
update configuratorrule set processedon = createdon ;
update configuratorrule_aud set processedon = createdon ;
update supplierinfo set processedon  = createdon;
update supplierinfo_aud set processedon = createdon ;
update supplierrule set processedon = createdon ;
update paymentconfiguration set processedon = createdon ;
update payment set processedon = createdon;
update payment_aud set processedon = createdon ;


alter table users rename column created_on to createdon;
alter table travellerinfo rename column created_on to createdon;