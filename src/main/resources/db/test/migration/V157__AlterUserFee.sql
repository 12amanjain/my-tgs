alter table userfee drop column if exists processedon, add column processedon timestamp;

update userfee set processedon = createdon;