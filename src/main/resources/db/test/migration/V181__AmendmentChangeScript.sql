alter table payment
drop COLUMN if EXISTS amendmentId,
add column amendmentId varchar(30);

alter table payment_aud
drop COLUMN if EXISTS amendmentId,
add column amendmentId varchar(30);

alter table amendment
drop COLUMN if EXISTS processedOn,
drop COLUMN if EXISTS info,
add COLUMN processedOn TIMESTAMP;

alter table amendment_aud
drop COLUMN if EXISTS processedOn,
drop COLUMN if EXISTS info,
add COLUMN processedOn TIMESTAMP;