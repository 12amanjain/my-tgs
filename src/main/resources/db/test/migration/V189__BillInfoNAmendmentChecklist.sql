alter table creditbill
drop COLUMN if EXISTS chargedpaymentids,
drop COLUMN if EXISTS settlementpaymentids,
drop COLUMN if EXISTS originalPayDueDate,
drop COLUMN if EXISTS additionalInfo,
add COLUMN originalPayDueDate timestamp,
add COLUMN additionalInfo jsonb default '{}'::jsonb;


alter table creditbill_aud
drop COLUMN if EXISTS chargedpaymentids,
drop COLUMN if EXISTS settlementpaymentids,
drop COLUMN if EXISTS originalPayDueDate,
drop COLUMN if EXISTS additionalInfo,
add COLUMN originalPayDueDate timestamp,
add COLUMN additionalInfo jsonb default '{}'::jsonb;