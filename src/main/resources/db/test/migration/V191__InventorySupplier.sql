
DROP TABLE IF EXISTS inventoryorders;
DROP TABLE IF EXISTS inventoryorders_aud;

create table inventoryorders (
        id serial not null,
        inventoryId varchar(50),
		bookingId varchar(1000),
		createdon timestamp DEFAULT now(),
		travellerInfo jsonb,
		supplierId varchar(255) not null,
		primary key (id)
);

create table inventoryorders_aud (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
        inventoryId varchar(50),
		bookingId varchar(1000),
		createdon timestamp DEFAULT now(),
		travellerInfo jsonb,
		supplierId varchar(255) not null,
		primary key (id,REV)
);

alter table inventoryorders_aud add constraint invorders_foreign_idx foreign key (REV) references customrev;

CREATE INDEX on inventoryorders(inventoryId);