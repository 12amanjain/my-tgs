alter table seatallocation drop COLUMN if EXISTS ratePlanName, add COLUMN ratePlanName varchar(1000);

alter table seatallocation_aud drop COLUMN if EXISTS ratePlanName, add COLUMN ratePlanName varchar(1000);