drop table if EXISTS reportsquery;

create table reportsquery(
id bigserial,
query varchar(5000),
columns varchar(5000),
primary key(id)
);