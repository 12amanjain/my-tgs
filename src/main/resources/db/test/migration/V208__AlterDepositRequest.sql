alter table depositrequest drop column if EXISTS additionalinfo;
alter table depositrequest add column  additionalinfo jsonb DEFAULT '{}'::jsonb;

alter table depositrequest_aud drop column if EXISTS additionalinfo;
alter table depositrequest_aud add column  additionalinfo jsonb DEFAULT '{}'::jsonb;
