create table TravellerInfo (
        id  bigserial not null,
        created_on timestamp,
        enabled boolean,
        travellerData varchar(5000),
        userId varchar(255),
        primary key (id)
    );

create table orders_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        amount float8,
        bookingId varchar(255),
        created_on timestamp,
        orderType varchar(255),
        sourceType varchar(255),
        userId varchar(255),
        primary key (id, REV)
   );

create table orders (
        id  bigserial not null,
        amount float8,
        bookingId varchar(255),
        created_on timestamp,
        orderType varchar(255),
        sourceType varchar(255),
        userId varchar(255),
        primary key (id)
    );

create table AirOrderItem (
        id  bigserial not null,
        additionalSegmentInfo jsonb,
        airlinecode varchar(255),
        amount float8,
        arrivalTime timestamp,
        bookingId varchar(255),
        departureTime timestamp,
        dest varchar(255),
        flightNumber varchar(255),
        markup float8,
        ruleId int8,
        source varchar(255),
        status varchar(255),
        supplierId varchar(255),
        travellerInfo jsonb,
        primary key (id)
    );


               


