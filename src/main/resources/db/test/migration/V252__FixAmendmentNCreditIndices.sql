drop INDEX if exists idx_amendment_createdon;
drop INDEX if exists idx_amendment_status;
drop INDEX if exists idx_amendment_bookingId;
drop INDEX if exists idx_amendmentId;

create index idx_amendment_createdon on amendment(createdon);
create index idx_amendment_status on amendment(status);
create index idx_amendment_bookingId on amendment(bookingid);
create unique index idx_amendmentId on amendment(amendmentid);


drop INDEX if exists idx_bill_number;
drop INDEX if exists idx_bill_duedate;
drop INDEX if exists idx_bill_userId;

create unique index idx_bill_number on creditbill(billnumber);
create index idx_bill_duedate on creditbill(paymentduedate);
create index idx_bill_userId on creditbill(paymentduedate);


drop index if EXISTS idx_creditline_number;
drop index if EXISTS idx_creditline_userId;
drop index if EXISTS idx_creditline_cycleEnddate;

create unique index idx_creditline_number on creditline(creditnumber);
create index idx_creditline_userId on creditline(userid);
create index idx_creditline_cycleEnddate on creditline(billcycleend);