drop table suppliersession;

CREATE TABLE suppliersession (
    id bigserial NOT NULL,
    bookingid character varying(255) NOT NULL,
    suppliersessioninfo jsonb,
    sourceid integer NOT NULL,
    supplierid character varying(255) DEFAULT ''::character varying NOT NULL,
    createdon timestamp without time zone DEFAULT now(),
    expiryon timestamp without time zone,
    primary key (id)
);



