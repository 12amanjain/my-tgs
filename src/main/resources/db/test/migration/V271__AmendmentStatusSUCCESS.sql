update amendment set status = 'S' where status = 'M';
update amendment_aud set status = 'S' where status = 'M';

alter table arearolemap drop CONSTRAINT if exists arearole_unique;
ALTER TABLE arearolemap ADD CONSTRAINT arearole_unique UNIQUE (arearole);