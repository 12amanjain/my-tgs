drop table if exists nonrevolvingcredit;
drop table if exists nonrevolvingcredit_aud;

create table nonrevolvingcredit(
id bigserial not null,
creditid varchar(50) not null,
userid varchar(50) not null,
creditamount bigint not null,
utilized bigint not null,
creditexpiry timestamp,
status varchar(50) not null,
products varchar(50)[],
additionalinfo jsonb DEFAULT '{}'::jsonb,
issuedByUserId varchar(50) not null,
expiryextcount SMALLINT,
createdon TIMESTAMP not null,
processedon TIMESTAMP not null,
PRIMARY key (id)
);

create table nonrevolvingcredit_aud(
rev bigint NOT NULL,
revtype smallint not null,
id bigserial not null,
creditid varchar(50) not null,
userid varchar(50) not null,
creditamount bigint not null,
utilized bigint not null,
creditexpiry timestamp,
status varchar(50) not null,
products varchar(50)[],
additionalinfo jsonb DEFAULT '{}'::jsonb,
issuedByUserId varchar(50) not null,
expiryextcount SMALLINT,
createdon TIMESTAMP not null,
processedon TIMESTAMP not null,
PRIMARY key (id,rev)
);