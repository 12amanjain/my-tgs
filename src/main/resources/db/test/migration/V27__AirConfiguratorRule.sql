
 CREATE TABLE AirConfiguratorRule (
 id bigserial not null,
 created_on timestamp DEFAULT now(),
 enabled boolean DEFAULT false,
 ruleType character varying(255) not null,
 inclusionCriteria character varying(50000)  ,
 exclusionCriteria character varying(50000)  ,
 output character varying(50000),
 priority float8,
 exitOnMatch boolean,
 PRIMARY KEY (id)
 );
