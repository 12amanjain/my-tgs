alter table creditline drop COLUMN if EXISTS status;
alter table creditline add COLUMN status varchar(30);
update creditline set status = 'A' where enabled = true;
update creditline set status = 'B' where enabled = false;
alter table creditline drop COLUMN enabled;

alter table creditline_aud drop COLUMN if EXISTS status;
alter table creditline_aud add COLUMN status varchar(30);
update creditline_aud set status = 'A' where enabled = true;
update creditline_aud set status = 'B' where enabled = false;
alter table creditline_aud drop COLUMN enabled;