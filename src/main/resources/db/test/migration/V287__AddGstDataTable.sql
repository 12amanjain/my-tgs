drop table if EXISTS gstdata;

create table gstdata(
	 id bigserial not null,
	 userid   character varying(255)  not null,
	 gstnumber       character varying(255)  not null,
	 address         character varying(255),
	 email           character varying(255), 
	 mobile          character varying(255),
	 registeredname  character varying(255), 
	 primary key (userid,gstnumber)
);