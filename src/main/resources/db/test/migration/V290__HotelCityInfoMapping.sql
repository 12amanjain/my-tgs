DROP TABLE if EXISTS cityinfomapping;

CREATE TABLE cityinfomapping(
id bigserial PRIMARY KEY,
cityId bigserial NOT NULL, 
supplierName text NOT NULL, 
supplierCity text NOT NULL,
supplierCountry text,
createdOn timestamp,
UNIQUE(cityId , supplierName));
