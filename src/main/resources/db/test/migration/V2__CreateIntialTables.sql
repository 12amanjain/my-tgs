create sequence hibernate_sequence start 1 increment 1;

 create table airports (
        id  bigserial not null,
        city varchar(255),
        code varchar(255),
        country varchar(255),
        name varchar(255),
        primary key (id)
    );


create table CityInfo (
        id  serial not null,
        country varchar(255),
        enabled boolean,
        name varchar(255),
        state varchar(255),
        primary key (id)
    );


  create table client_configurations (
        id  bigserial not null,
        clientId varchar(255),
        configuration varchar(5000),
        created_on timestamp,
        enabled boolean,
        serviceHelperClassPath varchar(255),
        type varchar(255),
        primary key (id)
    );

    create table client_configurations_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        clientId varchar(255),
        configuration varchar(5000),
        created_on timestamp,
        enabled boolean,
        serviceHelperClassPath varchar(255),
        type varchar(255),
        primary key (id, REV)
    );

    create table collection_documents (
        id  bigserial not null,
        created_on timestamp,
        data varchar(10000),
        enabled boolean,
        key varchar(255),
        primary key (id)
    );

     create table commission_rules (
        id  serial not null,
        created_on timestamp,
        description varchar(255),
        enabled boolean,
        input varchar(6000),
        key varchar(255),
        output varchar(255),
        priority int4,
        updatedBy int4,
        updated_on timestamp,
        primary key (id)
    );

    create table commission_rules_AUD (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
        created_on timestamp,
        description varchar(255),
        enabled boolean,
        input varchar(6000),
        key varchar(255),
        output varchar(255),
        priority int4,
        updatedBy int4,
        updated_on timestamp,
        primary key (id, REV)
    );

    create table customrev (
        id int8 not null,
        timestamp int8 not null,
        userId varchar(255),
        userName varchar(255),
        primary key (id)
    );


     create table OtpToken (
        id  serial not null,
        attemptCount int4 default 0,
        created_on timestamp,
        email varchar(255),
        emailreq boolean,
        key varchar(255),
        mobile varchar(255),
        otp varchar(255),
        requestId varchar(255),
        smsreq boolean,
        type varchar(255),
        primary key (id)
    );


    create table PolicyInfo (
        id  serial not null,
        created_on timestamp,
        data varchar(1000),
        description varchar(255),
        enabled boolean,
        groupName varchar(255),
        key varchar(255),
        name varchar(255),
        type varchar(255),
        primary key (id)
    );

     create table PolicyInfo_AUD (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
        created_on timestamp,
        data varchar(1000),
        description varchar(255),
        enabled boolean,
        groupName varchar(255),
        key varchar(255),
        name varchar(255),
        type varchar(255),
        primary key (id, REV)
    );

     create table source_routes_info (
        id  bigserial not null,
        dest varchar(255),
        enabled boolean,
        sourceId int4,
        src varchar(255),
        primary key (id)
    );


     create table SupplierConfigurationInfo (
        id  bigserial not null,
        configurationdata varchar(2000),
        created_on timestamp,
        enabled boolean,
        sourceId int4,
        supplierId int4,
        primary key (id)
    );


     create table SupplierConfigurationInfo_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        configurationdata varchar(2000),
        created_on timestamp,
        enabled boolean,
        sourceId int4,
        supplierId int4,
        primary key (id, REV)
    );


    create table users (
        id  serial not null,
        additionalInfo jsonb,
        addressInfo jsonb,
        contactPersonInfo jsonb,
        created_on timestamp,
        email varchar(255),
        gstInfo jsonb,
        mobile varchar(255),
        name varchar(255),
        panInfo jsonb,
        password varchar(1000),
        phone varchar(255),
        role varchar(255),
        status varchar(255),
        userId varchar(255),
        primary key (id)
    );

     create table users_AUD (
        id int4 not null,
        REV int8 not null,
        REVTYPE int2,
        additionalInfo jsonb,
        addressInfo jsonb,
        contactPersonInfo jsonb,
        created_on timestamp,
        email varchar(255),
        gstInfo jsonb,
        mobile varchar(255),
        name varchar(255),
        panInfo jsonb,
        password varchar(1000),
        phone varchar(255),
        role varchar(255),
        status varchar(255),
        userId varchar(255),
        primary key (id, REV)
    );
    

    alter table airports
        add constraint airports_uniq_idx unique (code);

    alter table PolicyInfo
        add constraint policyinfo_uniq_idx unique (key, type);


    alter table collection_documents
        add constraint collections_key_uniq unique (key);

    alter table OtpToken
        add constraint otptoken_uniq_idx unique (requestId);

      alter table source_routes_info
        add constraint source_routes_info_uniq_idx unique (src, dest, sourceId);

     alter table users
        add constraint users_uniq_idx unique (email, mobile, role);


	  alter table client_configurations_AUD
        add constraint client_configurations_aud_foreign_idx
        foreign key (REV)
        references customrev ;

	 alter table commission_rules_AUD
        add constraint commission_rules_foreign_idx
        foreign key (REV)
        references customrev  ;        

        alter table PolicyInfo_AUD
        add constraint policyinfo_foreign_idx
        foreign key (REV)
        references customrev ;

         alter table SupplierConfigurationInfo_AUD
        add constraint supplierconfigurationinfo_foreign_idx
        foreign key (REV)
        references customrev;

        alter table users_AUD
        add constraint users_foreign_idx
        foreign key (REV)
        references customrev;




  
