alter table orders_aud add column status character varying (255);
alter table orders add constraint orders_uniq_idx unique (bookingid);
alter table airorderitem add column created_on timestamp;
alter table airorderitem_aud add column created_on timestamp;
