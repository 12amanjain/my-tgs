drop table airorderitem , airorderitem_aud,orders,orders_aud,travellerinfo;


 ALTER TABLE airline_info rename to airlineinfo;

 alter table airports rename to airportinfo;

 alter table client_configurations rename to clientconfiguration;

 alter table client_configurations_aud rename to clientconfiguration_aud;

 alter table supplier_rules rename to supplierrule;

 alter table source_routes_info rename to sourcerouteinfo;

