delete from hotelconfiguratorrule where ruletype = 'SUPPLIERCONFIG';

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TECHNOGRAM2","Technogram"]}', 5, true, '{}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJTravco","TJRTS","TJRESTEL","TJW2M","TJHOTELSPRO"]}', 10, true, '{}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJHotelbeds","TJ24x7","TJTotalstay","TJRTS","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["Egypt","Ghana","Kenya","Nigeria","South Africa","Tanzania","Zimbabwe"]}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJRTS","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["Australia","New Zealand"]}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJTravco","TJRTS","TJRESTEL","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["Albania","Austria","Belgium","Bosnia and Herzegovina","Croatia","Czech Republic","Denmark","Estonia","Finland","France","Germany","Greece","Greenland","Hungary","Iceland","Ireland","Isle of Man","Italy","Latvia","Liechtenstein","Lithuania","Luxembourg","Macedonia","Malta","Monaco","Montenegro","Montserrat","Netherlands","Norway","Poland","Portugal","Slovakia","Slovenia","Spain","Sweden","Switzerland","Ukraine","United Kingdom","Vatican City"]}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJclertrip","TJagoda","TJMiki","TJ Desiya","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJRTS","TJTAMTOUR","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["India"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJTravco","TJRTS","TJ QUANTUM","TJRESTEL","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["Hong Kong","Indonesia","Macau","Malaysia","Singapore","Thailand"]}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as": ["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJTravco","TJ DARINA","TJRESTEL","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["United Arab Emirates"]}', '{}', false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJHotelbeds","TJ24x7","TJTravco","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["China","Japan","North Korea","South Korea"]}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJTravco","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["Jordan","Kazakhstan","Kuwait","Oman","Saudi Arabia"]}', NULL, false,'Hotel search grouping for suppliers', current_timestamp);

INSERT INTO hotelconfiguratorrule
(createdon, enabled, ruletype, output, priority, exitonmatch, inclusioncriteria, exclusioncriteria, isdeleted, description, processedon)
VALUES(current_timestamp, true, 'SUPPLIERCONFIG', '{"as":["TJclertrip","TJagoda","TJMiki","TJDOTW","TJHotelbeds","TJ24x7","TJTotalstay","TJTravco","TJRTS","TJRESTEL","TJW2M","TJHOTELSPRO"]}', 15, false, '{"countryName": ["Argentina","Bahamas","Brazil","Canada","Chile","Mexico","Peru","United States","United States Virgin Islands"]}', NULL, false, 'Hotel search grouping for suppliers', current_timestamp);

