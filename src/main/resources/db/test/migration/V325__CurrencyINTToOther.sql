delete from moneyexchange;
alter table moneyexchange alter COLUMN createdon set default now();
alter table moneyexchange alter COLUMN processedon set default now();
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','AFN','0.9532');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','AMD','0.1487');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','AZN','42.455');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BDT','0.8572');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','KHR','0.01758');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','CNY','10.513');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','GEL','27.539');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','HKD','9.1813');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','IDR','0.005');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','JPY','0.6422');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','KZT','0.1974');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','LAK','0.0084');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MYR','17.412');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','KRW','0.0642');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','LKR','0.4286');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MVR','4.674');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','PKR','0.5832');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','PHP','1.332');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','THB','2.226');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','TWD','2.341');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','VND','0.0030');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BHD','191.60');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','AED','19.617');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','SAR','19.21');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','OMR','187.44');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','ILS','20.115');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','IQD','0.0603');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','IRR','0.0017');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','ISK','0.653');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','KWD','237.93');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','LBP','0.0477');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','QAR','19.80');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','LYD','52.277');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BAM','43.058');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','EUR','84.323');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BGN','43.067');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','CZK','3.3007');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','HRK','11.343');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','DKK','11.3046');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','HUF','0.2605');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','ISK','0.6530');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MDL','4.2899');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MKD','1.3686');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','NOK','8.8406');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','PLN','19.642');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','RON','18.104');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','RSD','0.7134');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','RUB','1.0818');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','NOK','8.8534');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','SEK','8.1303');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','CHF','74.565');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','TRY','11.506');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','UAH','2.5631');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','GBP','94.762');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','ARS','1.8343');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BOB','10.437');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BRL','17.468');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','CLP','0.1061');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','COP','0.0237');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','PEN','21.830');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','PYG','0.0123');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','UYU','2.1711');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','VEF','7.217');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','USD','72.04');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','CAD','55.795');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','AWG','40.265');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BBD','36.037');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BMD','72.074');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','BSD','72.07');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','DOP','1.437');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','JMD','0.5291');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MXN','3.842');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','ZAR','4.917');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','EGP','4.026');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','GHS','14.4894');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','GMD','1.4869');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','KES','0.7150');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MAD','7.684');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','MUR','2.098');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','NAD','4.9210');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','NGN','0.1988');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','SCR','5.3274');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','TND','25.933');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','UGX','0.0188');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','XAF','0.12845');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','XOF','0.1284');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','AUD','52.3572');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','FJD','33.94');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','NZD','47.95');
insert into moneyexchange(type,tocurrency,fromcurrency,exchangerate) values('General','INR','XPF','0.705');
update moneyexchange set isenabled=true;