create table airorderitem (
        id  bigserial not null,
        bookingId varchar(255),
        status varchar(255),
        createdOn timestamp,
        airlinecode varchar(255),
        amount float8,
        markup float8,
        source varchar(255),
        dest varchar(255),
        flightNumber varchar(255),
        arrivalTime timestamp,
        departureTime timestamp,
        ruleId int8,
        supplierId varchar(255),
        additionalSegmentInfo jsonb,
        travellerInfo jsonb,
        primary key (id)
);

create table airorderitem_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        bookingId varchar(255),
        status varchar(255),
        createdOn timestamp,
        airlinecode varchar(255),
        amount float8,
        markup float8,
        source varchar(255),
        dest varchar(255),
        flightNumber varchar(255),
        arrivalTime timestamp,
        departureTime timestamp,
        ruleId int8,
        supplierId varchar(255),
        additionalSegmentInfo jsonb,
        travellerInfo jsonb,
        primary key (id, REV)
);

create table orders (
        id  bigserial not null,
        bookingId varchar(255),
        createdOn timestamp,
        orderType varchar(255),
        status varchar(255),
        paymentMedium varchar(255),
        amount float8,
        bookingUserId varchar(255),
        loggedInUserId varchar(255),
        sublogin varchar(255),
        channelType varchar(255),
        additionalInfo jsonb,
        deliveryInfo jsonb,
        primary key (id)
    ); 

 create table orders_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        bookingId varchar(255),
        createdOn timestamp,
        orderType varchar(255),
        status varchar(255),
        paymentMedium varchar(255),
        amount float8,
        bookingUserId varchar(255),
        loggedInUserId varchar(255),
        sublogin varchar(255),
        channelType varchar(255),
        additionalInfo jsonb,
        deliveryInfo jsonb,
        primary key (id, REV)
    );

  create table payment (
        id  bigserial not null,
        amount float8,
        createdOn timestamp,
        paymentMedium varchar(255),
        referenceId varchar(255),
        status varchar(255),
        type varchar(255),
        markup int8,
        payUserId varchar(255),
        reason varchar(255),
        sublogin varchar(255),
        primary key (id)
    );

     create table payment_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        amount float8,
        createdOn timestamp,
        paymentMedium varchar(255),
        referenceId varchar(255),
        status varchar(255),
        type varchar(255),
        markup int8,
        payUserId varchar(255),
        reason varchar(255),
        sublogin varchar(255),
        primary key (id, REV)
    );


    create table paymentdetail (
        id  bigserial not null,
        createdOn timestamp,
        paymentId int8,
        amount float8,
        payUserId varchar(255),
        status varchar(255),
        type varchar(255),
        addtionalParams varchar(255),
        currentBalance int8,
        primary key (id)
    );

    create table paymentdetail_AUD (
        id int8 not null,
        REV int8 not null,
        REVTYPE int2,
        createdOn timestamp,
        paymentId int8,
        amount float8,
        payUserId varchar(255),
        status varchar(255),
        type varchar(255),
        addtionalParams varchar(255),
        currentBalance int8,
        primary key (id, REV)
    );

    create table travellerinfo (
        id  bigserial not null,
        bookingUserId varchar(255),
        created_on timestamp,
        enabled boolean,
        travellerData varchar(5000),
        primary key (id)
    );

    create table usersublogin (
        id  bigserial not null,
        enabled boolean,
        userId varchar(255),
        userName varchar(255),
        password varchar(255),
        primary key (id)
    );

    alter table airorderitem_AUD 
        add constraint FKtdlpnxf9ge00qv437ficlwk8i 
        foreign key (REV) 
        references customrev;
     
     alter table orders_AUD 
        add constraint FKfj97jp5uhwhi2qfica6pmsivf 
        foreign key (REV) 
        references customrev;
        
      alter table Payment_AUD 
        add constraint FK785geijylovlcgsdc0h437sw9 
        foreign key (REV) 
        references customrev;
        
        alter table PaymentDetail_AUD 
        add constraint FKjnnmlqckf0ugs3knt368ua37g 
        foreign key (REV) 
        references customrev ;


alter table orders add constraint orders_uniq_idx unique (bookingid);   


alter table users add column balance float8;
