delete from hotelsupplierinfo where name = 'TJExpedia';

UPDATE hotelconfiguratorrule
SET output = jsonb_set(
  output::jsonb,
  array['as'],
  (output::jsonb->'as')::jsonb - 'TJExpedia') 
WHERE ruletype = 'SUPPLIERCONFIG';

INSERT INTO hotelsupplierinfo
(credentialinfo, createdon, enabled, "name", sourceid, isdeleted, processedon)
VALUES('{"url": "http://hotel.atlastravelsonline.com/ws/index.php", "password": "Atlas@1234", "userName": "TJExpedia"}', current_timestamp, true, 'TJExpedia', 1, false, current_timestamp);

UPDATE hotelconfiguratorrule
SET output = jsonb_set(
  output::jsonb,
  array['as'],
  (output::jsonb->'as')::jsonb || '["TJExpedia"]'::jsonb) 
WHERE ruletype = 'SUPPLIERCONFIG';