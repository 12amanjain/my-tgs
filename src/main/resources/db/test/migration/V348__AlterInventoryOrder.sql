alter table inventoryorders drop column if exists airline, add column airline varchar(50);

alter table inventoryorders_aud drop column if exists airline, add column airline varchar(50);

alter table inventoryorders drop column if exists bookinguserid, add column bookinguserid varchar(50);

alter table inventoryorders_aud drop column if exists bookinguserid, add column bookinguserid varchar(50);

update airorderitem a set additionalinfo = jsonb_set(additionalinfo, '{scid}', to_jsonb(s.sourceid)) 
from supplierinfo s where a.supplierid = s.name;

update inventoryorders as i set bookinguserid = o.bookinguserid from orders o where i.referenceid = o.bookingid;

update inventoryorders as i set airline = a.carrier from airinventory a where i.inventoryid = a.id::varchar;
