delete from collection_documents where key like '%HOTEL%SMS%';

INSERT INTO collection_documents
(created_on, data, enabled, key, expiry, type, owner)
VALUES(current_timestamp, '{"value": "Dear Travel Partner, Cancellation Deadlines for Booking Ref. No.: $bookingId$ are in the next $deadlineDateHours$ hours. It will be cancelled automatically if you do not change the status to be VOUCHERED.\nFor further assistance kindly contact on $contact$"}', true, 'HOTEL_BOOKING_CANCELLATION_DEADLINE_SMS', '2019-07-30 15:48:31.235', 'Hotel_SMS', NULL);

INSERT INTO collection_documents
(created_on, data, enabled, key, expiry, type, owner)
VALUES(current_timestamp, '{"value":"$bookingUser$:Your cart $bookingId$ is confirmed. \nHotel Name: $hname$ \nCheck In Date: $checkindate$ \nCheck Out Date: $checkoutdate$ \nTotal Nights: $totalnights$ \nPAX:$passengers$"}', true, 'HOTEL_BOOKING_SUCCESS_SMS', '2019-07-30 15:51:45.281', 'Hotel_SMS', NULL);

INSERT INTO collection_documents
(created_on, data, enabled, key, expiry, type, owner)
VALUES(current_timestamp, '{"value": "Dear Travel Partner,Booking Ref. No.: $bookingId$ is cancelled.For further assistance kindly contact on $contact$"}', true, 'HOTEL_BOOKING_CANCELLATION_SMS', '2019-07-30 15:29:42.105', 'Hotel_SMS', NULL);

INSERT INTO collection_documents
(created_on, data, enabled, key, expiry, type, owner)
VALUES(current_timestamp, '{"value": "Dear Travel Partner,Booking Ref. No. is $bookingId$.\nDirect hotel confirmation no is $hotelBookingRef$.\nFor further assistance kindly contact on $contact$"}', true, 'HOTEL_BOOKING_CONFIRMATION_SMS', '2019-07-30 15:35:04.080', 'Hotel_SMS', NULL);