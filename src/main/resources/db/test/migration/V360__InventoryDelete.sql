alter table airinventory drop column if exists deleted, add column deleted boolean default false;

alter table airinventory_aud drop column if exists deleted, add column deleted boolean default false;

