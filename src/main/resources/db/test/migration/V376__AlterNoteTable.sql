alter table note drop column if exists additionalinfo;
alter table note add column additionalinfo jsonb;