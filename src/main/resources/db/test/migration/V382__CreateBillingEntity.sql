drop table IF EXISTS billing_entity ;
drop index IF EXISTS billing_entity_idx;

CREATE TABLE billing_entity (
 id  bigserial not null,
 userId varchar(255),
 info jsonb,
 createdon timestamp,
 PRIMARY KEY (id)
);

CREATE INDEX billing_entity_idx ON billing_entity (userId);