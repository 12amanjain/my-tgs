CREATE TABLE IF NOT EXISTS hotelconfiguratorrule_aud (
	id bigserial NOT NULL,
	rev bigint,
 	revtype smallint,
	createdon timestamp NULL DEFAULT now(),
	enabled bool NULL DEFAULT true,
	ruletype varchar(255) NOT NULL,
	output varchar(50000) NULL,
	priority float8 NULL,
	exitonmatch bool NULL DEFAULT false,
	inclusioncriteria jsonb NULL,
	exclusioncriteria jsonb NULL,
	isdeleted bool NULL DEFAULT false,
	description varchar(5000) NULL,
	processedon timestamp NULL,
	PRIMARY KEY (id,rev)
);