ALTER TABLE amendment DROP COLUMN IF EXISTS ordertype, ADD COLUMN ordertype VARCHAR;
UPDATE amendment set ordertype = 'A';
ALTER TABLE amendment_aud DROP COLUMN IF EXISTS ordertype, ADD COLUMN ordertype VARCHAR;
UPDATE amendment_aud set ordertype = 'A';
