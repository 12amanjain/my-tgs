ALTER TABLE creditcardinfo DROP COLUMN IF EXISTS processedOn;
ALTER TABLE creditcardinfo ADD COLUMN processedOn timestamp without time zone DEFAULT NOW();


ALTER TABLE creditcardinfo_aud DROP COLUMN IF EXISTS processedOn;
ALTER TABLE creditcardinfo_aud ADD COLUMN processedOn timestamp without time zone DEFAULT NOW();
