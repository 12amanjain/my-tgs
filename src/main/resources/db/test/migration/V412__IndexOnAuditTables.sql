CREATE INDEX ON users_aud(userid);
CREATE INDEX ON depositrequest_aud(reqId);
CREATE INDEX ON airorderitem_aud(bookingId);
CREATE INDEX ON orders_aud(bookingId);
CREATE INDEX ON hotelorderitem_aud(bookingId);
CREATE INDEX ON creditBill_aud(billNumber);
CREATE INDEX ON creditLine_aud(creditNumber);
CREATE INDEX ON creditpolicy_aud(policyId);
CREATE INDEX ON nonrevolvingcredit_aud(creditId);

CREATE INDEX ON users_aud(rev);
CREATE INDEX ON depositrequest_aud(rev);
CREATE INDEX ON airorderitem_aud(rev);
CREATE INDEX ON orders_aud(rev);
CREATE INDEX ON hotelorderitem_aud(rev);
CREATE INDEX ON creditBill_aud(rev);
CREATE INDEX ON creditLine_aud(rev);
CREATE INDEX ON creditpolicy_aud(rev);
CREATE INDEX ON nonrevolvingcredit_aud(rev);

CREATE INDEX ON customrev(id);
CREATE INDEX ON customrev(timestamp);




