delete from hotelinfo where rating is null;

drop INDEX if exists hotelinfo_rating_idx;
drop INDEX if exists hotel_info_unique_name_rating_index_notnull;
drop INDEX if exists hotel_info_unique_name_rating_index_null;

CREATE UNIQUE INDEX hotel_info_unique_name_rating_index_notnull ON public.hotelinfo USING btree (name, rating) WHERE (rating IS NOT NULL);
CREATE UNIQUE INDEX hotel_info_unique_name_rating_index_null ON public.hotelinfo USING btree (name) WHERE (rating IS NULL);