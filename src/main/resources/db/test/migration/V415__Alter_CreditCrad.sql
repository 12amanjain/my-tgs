alter table creditcardinfo drop column if exists billingentityid;

alter table creditcardinfo drop column if exists additionalinfo;

alter table creditcardinfo add column additionalinfo jsonb;

alter table creditcardinfo_aud drop column if exists billingentityid;

alter table creditcardinfo_aud drop column if exists additionalinfo;

alter table creditcardinfo_aud add column additionalinfo jsonb;