Drop table IF exists roomCategory;

CREATE TABLE roomCategory(
		id serial not null,
		roomCategory varchar(200),
		UNIQUE(roomCategory)
);