CREATE OR REPLACE FUNCTION create_partition_users_aud_insert() RETURNS trigger AS
  $BODY$
    DECLARE
	  tablename TEXT;
      partition_date TEXT;
      partition TEXT;
	  createScript TEXT;
	  updateScript TEXT;
    BEGIN
	  tablename := 'users_aud';
      partition_date := to_char(NEW.createdon,'YYYY');
      partition := tablename || '_' || partition_date;
      IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
        RAISE NOTICE 'A partition has been created %',partition;
		createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
        EXECUTE createScript;
		RAISE NOTICE 'Create Script is %',createScript;
		updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= ''' || 
		DATE_TRUNC('year',NEW.createdon) ||'''AND createdon< ''' || DATE_TRUNC('year',NEW.createdon)+interval '1 year' || 
		''');';
		EXECUTE updateScript;
		RAISE NOTICE 'Update Script is %',updateScript;
	  END IF;
	  EXECUTE 'INSERT INTO ' || partition || ' SELECT(' || tablename || ' ' || quote_literal(NEW) || ').*;';
	  RETURN NULL;
    END;
  $BODY$
LANGUAGE plpgsql VOLATILE
COST 100;


DO
$do$
DECLARE
 tablename TEXT;
 rec  RECORD;
 rel text;
 partition text;
 createScript TEXT;
 updateScript TEXT;
 splitPart TEXT;
 startDate TEXT;
 endDateScript TEXT;
 endDate TEXT;
BEGIN
 tablename := 'users_aud';
FOR rec IN (SELECT child.relname  AS child
FROM pg_inherits
    JOIN pg_class parent ON pg_inherits.inhparent = parent.oid
    JOIN pg_class child             ON pg_inherits.inhrelid   = child.oid
    JOIN pg_namespace nmsp_parent   ON nmsp_parent.oid  = parent.relnamespace
    JOIN pg_namespace nmsp_child    ON nmsp_child.oid   = child.relnamespace
WHERE parent.relname='users_aud') LOOP
rel = regexp_replace(rec::text, '[()]', '', 'g');
execute format('SELECT split_part(%L,%L, 3)',rel,'_') into splitPart;
partition := tablename || '_' || splitPart;
IF NOT EXISTS(SELECT relname FROM pg_class WHERE relname=partition) THEN
   createScript := 'CREATE TABLE ' || partition || ' (like '|| tablename || ' including all) inherits ('|| tablename || ');';
   RAISE NOTICE 'Create Script is %',createScript;
   EXECUTE createScript;
   startDate:= splitPart || '-01-01';
   execute format('SELECT date %L + time %L', startDate, '00:00') into startDate;
   execute format('SELECT date %L + interval %L', startDate, '1 year') into endDate;
   updateScript := 'ALTER TABLE ' || partition || ' ADD CONSTRAINT ' || partition||'_createdon check (createdon >= date ''' || 
    startDate ||''' AND createdon< date ''' || endDate || 
    ''');';
  RAISE NOTICE 'Update Script is %',updateScript;
  EXECUTE updateScript;
END IF;
EXECUTE 'with t as (delete from ' ||rel ||(' returning *) 
    insert into ' || partition || ' select * from t;');
EXECUTE 'DROP TABLE ' || rel;
END LOOP;
END
$do$;
