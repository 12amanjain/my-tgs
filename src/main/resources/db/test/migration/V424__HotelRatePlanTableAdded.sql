CREATE TABLE IF NOT EXISTS hotelrateplan (
	id serial NOT NULL,
	name varchar(255) NULL,
	rateplaninfo jsonb NULL,
	inventoryallocationinfo jsonb NULL,
	createdon timestamp NULL DEFAULT now(),
	processedon timestamp NULL DEFAULT now(),
	supplierid varchar(255) NOT NULL,
	supplierhotelid varchar(255) NULL,
	roomcategoryid varchar(255) NOT NULL,
	mealbasisid varchar(255) NOT NULL,
	roomtypeid varchar(255) NOT NULL,
	sellPolicy varchar(255) NULL,
	validon date NOT NULL,
	isdeleted bool NULL DEFAULT false,
	enabled bool NULL DEFAULT false,
	CONSTRAINT hotelrateplan_pkey PRIMARY KEY (id)
);

drop index if exists hotelrateplan_sid_shid_validon_isdeleted_idx;
drop index if exists hotelrateplan_sid_shid_rcid_validon_isdeleted_idx;
drop index if exists hotelrateplan_sid_shid_rcid_mbid_validon_isdeleted_idx;
drop index if exists hotelrateplan_sid_shid_rcid_mbid_rtid_validon_isdeleted_idx;

CREATE INDEX hotelrateplan_sid_shid_validon_isdeleted_idx ON hotelrateplan USING btree (supplierid, supplierhotelid, validon, isdeleted);
CREATE INDEX hotelrateplan_sid_shid_rcid_validon_isdeleted_idx ON hotelrateplan USING btree (supplierid, supplierhotelid, roomcategoryid, validon, isdeleted);
CREATE INDEX hotelrateplan_sid_shid_rcid_mbid_validon_isdeleted_idx ON hotelrateplan USING btree (supplierid, supplierhotelid, roomcategoryid, mealbasisid, validon, isdeleted);
CREATE UNIQUE INDEX hotelrateplan_sid_shid_rcid_mbid_rtid_validon_isdeleted_idx ON hotelrateplan USING btree (supplierid, supplierhotelid, roomcategoryid, mealbasisid, roomtypeid, validon, isdeleted);

CREATE TABLE IF NOT EXISTS hotelrateplan_aud (
	id int4 NOT NULL,
	rev int8 NOT NULL,
	revtype int2 NULL,
	name varchar(255) NULL,
	rateplaninfo jsonb NULL,
	inventoryallocationinfo jsonb NULL,
	createdon timestamp NULL DEFAULT now(),
	processedon timestamp NULL DEFAULT now(),
	supplierid varchar(255) NOT NULL,
	supplierhotelid varchar(255) NULL,
	roomcategoryid varchar(255) NOT NULL,
	mealbasisid varchar(255) NOT NULL,
	roomtypeid varchar(255) NOT NULL,
	sellPolicy varchar(255) NULL,
	validon date NOT NULL,
	isdeleted bool NULL DEFAULT false,
	enabled bool NULL DEFAULT false,
	CONSTRAINT hotelrateplan_aud_pkey PRIMARY KEY (id, rev),
	CONSTRAINT hotelrateplan_foreign_idx FOREIGN KEY (rev) REFERENCES customrev(id)
);