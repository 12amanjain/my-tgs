ALTER TABLE hotelcityinfomapping DROP COLUMN IF EXISTS suppliercityname;
ALTER TABLE hotelcityinfomapping ADD COLUMN suppliercityname varchar(255);