DROP TABLE IF EXISTS externalpayment;

CREATE TABLE externalpayment (
	id BIGSERIAL PRIMARY KEY,
	createdon TIMESTAMP DEFAULT now(),
	refid VARCHAR(255) UNIQUE NOT NULL,
	status VARCHAR(255) NOT NULL,
	payuserid VARCHAR(255) NOT NULL,
	additionalinfo jsonb
);

CREATE INDEX ON externalpayment(createdon);

DROP TABLE IF EXISTS externalpaymentstatus;

CREATE TABLE externalpaymentstatus (
	id BIGSERIAL PRIMARY KEY,
	createdon TIMESTAMP DEFAULT now(),
	refid VARCHAR(255) NOT NULL,
	status VARCHAR(255) NOT NULL,
	comment VARCHAR(255)
);

CREATE INDEX ON externalpaymentstatus(refid);