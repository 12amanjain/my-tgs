CREATE UNIQUE INDEX ON users (mobile) where role = 'CR';
alter table users drop constraint if exists users_uniq_idx;
CREATE UNIQUE INDEX ON users (partnerid,email) where role != 'CR';
CREATE UNIQUE INDEX ON users (partnerid,email) where role = 'CR';
