create sequence if not exists amendment_dom_invoice_id_seq increment by 1 minvalue 2321 maxvalue 9999999 start with 2321;

create sequence if not exists amendment_int_invoice_id_seq increment by 1 minvalue 1 maxvalue 9999999 start with 1;