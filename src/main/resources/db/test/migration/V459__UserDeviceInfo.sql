
DROP TABLE if exists userdeviceinfo;

CREATE TABLE userdeviceinfo (
id serial NOT NULL ,
userid varchar(50) NULL,
deviceinfo jsonb NULL,
createdon timestamp NULL
);
CREATE INDEX on userdeviceinfo(userid);
