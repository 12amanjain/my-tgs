alter table users alter column additionalinfo set default '{}';
alter table users alter column addressinfo set default '{}';
alter table users alter column contactpersoninfo set default '{}';
alter table users alter column paninfo set default '{}';
