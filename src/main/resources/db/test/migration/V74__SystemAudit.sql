CREATE TABLE SystemAudit (
 id bigserial not null,
 createdOn timestamp DEFAULT now(),
 userId CHARACTER VARYING(255),
 auditType CHARACTER VARYING(255),
 ip CHARACTER VARYING(255),
 bookingId CHARACTER VARYING(255),
 loggedinuserid CHARACTER VARYING(255),
 PRIMARY KEY (id)
 );


 CREATE UNIQUE INDEX systemaudit_uniq_idx on systemaudit (bookingid,createdon,userid,loggedinuserid) ;