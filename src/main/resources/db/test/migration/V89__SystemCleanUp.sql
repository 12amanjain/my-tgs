alter table airorderitem drop column if exists ruleid;
alter table airorderitem_aud drop column if exists ruleid;

alter table airorderitem rename column additionalSegmentInfo to additionalinfo;
alter table airorderitem_aud rename column additionalSegmentInfo to additionalinfo;

alter table users drop column if exists commissionplan;
alter table users_aud drop column if exists commissionplan;

alter table users add column userconf jsonb;
alter table users_aud add column userconf jsonb;

update users set userconf = '{}' where userconf is null;

update users set userconf = jsonb_set(userconf,'{commPlanMap}','{"AIR":1}',true);


update airorderitem set additionalinfo = jsonb_set(additionalinfo,'{intl}', 'true', true) where bookingid in (select bookingid from orders where ordertype = 'IA');
update orders set ordertype = 'A';


delete from commissionplan where product = 'IA';

update commissionplan set product = 'A';