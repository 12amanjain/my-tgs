alter table payment drop column if exists type;
alter table payment rename column optype to type;
alter table payment alter column amount type bigint;
alter table payment alter column markup type int;
alter table payment alter column tds type int;
alter table payment alter column paymentfee type int; 


alter table payment_aud drop column if exists type;
alter table payment_aud rename column optype to type;
alter table payment_aud alter column amount type bigint;
alter table payment_aud alter column markup type int;
alter table payment_aud alter column tds type int;
alter table payment_aud alter column paymentfee type int; 

update payment set type = 'C' where type = 'CM';
update payment set type = 'O' where type = 'CH';
