<head>
	<title>Technogram Probe</title>
	<style type="text/css">
		div {
			width: 800px;
			height: 70px;
			
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
			
			margin: auto;
		}
		
		td, input {
			font-size: 32px;
		}		
		
		td {
			padding: 10px;
		}
	</style>
</head>

<body>
	<div>
		<form action="/jsp/probe/results" method="GET">
			<table>
				<tr>
					<td><input name="key" placeholder="Booking/Search ID" autofocus></td>
					<td><input type="submit" value="Submit"></td>
				</tr>
			</table>
		</form>
	</div>
</body>