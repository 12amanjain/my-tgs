<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<html>

	<head>
		<title>Technogram Probe</title>
	
		<script>
		
			function showText() {
				var shownval = document.getElementById("myselect").value;
				var element = document.querySelector("#browsers option[value='"+shownval+"']");
				
				if(element) {
					var val = element.dataset.value;
					document.getElementById("field2").value = val;
					document.getElementById("myselect").value = '';
					document.getElementById("realtxt").innerHTML = shownval;
				}
			}

			function xmlFormatter(xml) {
                var formatted = '';
                var reg = /(>)(<)(\/*)/g;
                xml = xml.replace(reg, '$1\r\n$2$3');
                var pad = 0;
                jQuery.each(xml.split('\r\n'), function(index, node) {
                    var indent = 0;
                    if (node.match( /.+<\/\w[^>]*>$/ )) {
                        indent = 0;
                    } else if (node.match( /^<\/\w/ )) {
                        if (pad != 0) {
                            pad -= 1;
                        }
                    } else if (node.match( /^<\w[^>]*[^\/]>.*$/ )) {
                        indent = 1;
                    } else {
                        indent = 0;
                    }

                    var padding = '';
                    for (var i = 0; i < pad; i++) {
                        padding += '  ';
                    }

                    formatted += padding + node + '\r\n';
                    pad += indent;
                });

                return formatted;
            }
		
			function downloadAll() {
				window.location = "/jsp/probe/downloadAll/${keyValue}";
			}
			
			function searchSel() {
				var input=document.getElementById('realtxt').value.toLowerCase();
  				var output=document.getElementById('myselect').options;
				  for(var i=0;i<output.length;i++) {
				  	console.log(output[i].value);
				    if(output[i].text.toLowerCase().includes(input)){
				      output[i].selected=true;
				      break;
				    }
				    if(document.getElementById('realtxt').value==''){
				      output[0].selected=true;
				    }
				  }
			}	
			
		</script>
	</head>

	<body>
		<div>
		
 	    <c:if test="${not empty results}">
 	    	<center>Search Logs 
 	    	<input oninput="showText()" id="myselect" list="browsers" style="font-size : 16px;width: 400px;height: 35px;">
 			<datalist id="browsers">
    			<c:forEach var='listValue'	 items="${results}">
        			<option data-value='${listValue.value}' value='${listValue.key}'></option>
    			</c:forEach>
 			</datalist>
 			</center>
 			</br>
 	    	<label id="realtxt" style = "width: 420px;height: 31px;margin-left: 22px;font-size: 19px;font-weight: bold;" />
    	</c:if>
		</div>
		<br>
		<div align="center"   position= "absolute">
		    <textarea rows="40	" cols="180" id="field2" readonly></textarea>
		</div>
		<br>
		<div align="center">
			<button onclick="downloadAll()" style="font-size : 20px;">Download All</button>
		</div>
	</body>

</html>