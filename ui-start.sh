#!/usr/bin/env bash
dir=$(pwd)

check_run() {
	echo "$changed_files" | grep --quiet "$1" && eval "$2"
}

service=$1

#Running on port 3030
start_flight(){
cd ../flight-ui
echo "Air Build"
git stash
git checkout master
git reset --hard origin/master
git pull origin master
git stash pop
export SERVICE_DOMAIN=dev.technogramsolutions.com:8080
pid=`ps -ef | grep 'flight-ui' | grep -v 'grep' | awk '{print $2}'`
kill -9 $pid
changed_files="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"
check_run package.json "npm i"
npm run dev &
node server.js --name flight-ui &
}

if [ -z "$service" ] || [ "$service" = "flight-ui" ]
then
start_flight
fi

#Running on port 9001
start_hotel(){
cd ../hotel-ui
echo "Hotel Build"
git stash
git checkout master
git reset --hard origin/master
git pull origin master
git stash pop
export SERVICE_DOMAIN=dev.technogramsolutions.com:8080
pid=`ps -ef | grep 'node hotel-ui' | grep -v 'grep' | awk '{print $2}'`
kill -9 $pid
changed_files="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"
check_run package.json "npm i"
npm run dev-server --name hotel-ui &
}

if [ -z "$service" ] || [ "$service" = "hotel" ]
then
start_hotel
fi

#Running on port 4000
start_xms(){
cd ../xms
echo "XMS Build"
git stash
git checkout master
git reset --hard origin/master
git pull origin master
git stash pop
export SERVICE_DOMAIN=dev.technogramsolutions.com:8080
pid=`ps -ef | grep 'name xms' | grep -v 'grep' | awk '{print $2}'`
kill -9 $pid
changed_files="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"
check_run package.json "npm i"
node server.js --name xms &
}

if [ -z "$service" ] || [ "$service" = "xms" ]
then
start_xms
fi

#Running on port 9000
start_pluto(){
cd ../pluto
echo "Pluto Build"
git stash
git checkout master
git reset --hard origin/master
git pull origin master
git stash pop
export SERVICE_DOMAIN=dev.technogramsolutions.com:8080
pid=`ps -ef | grep 'pluto' | grep -v 'grep' | awk '{print $2}'`
kill -9 $pid
changed_files="$(git diff-tree -r --name-only --no-commit-id ORIG_HEAD HEAD)"
check_run package.json "npm i"
node app.js --name pluto &
npm run dev &
}
if [ -z "$service" ] || [ "$service" = "pluto" ]
then
start_pluto
fi
